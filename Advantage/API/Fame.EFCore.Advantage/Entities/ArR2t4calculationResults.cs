﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArR2t4calculationResults.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArR2t4calculationResults definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArR2t4calculationResults</summary>*/
    public partial class ArR2t4calculationResults
    {
        /*<summary>The get and set for R2t4calculationResultsId</summary>*/
        public Guid R2t4calculationResultsId { get; set; }
        /*<summary>The get and set for TerminationId</summary>*/
        public Guid TerminationId { get; set; }
        /*<summary>The get and set for TotalCharges</summary>*/
        public decimal TotalCharges { get; set; }
        /*<summary>The get and set for TotalTitleIvaid</summary>*/
        public decimal TotalTitleIvaid { get; set; }
        /*<summary>The get and set for TotalTitleIvaidDisbursed</summary>*/
        public decimal TotalTitleIvaidDisbursed { get; set; }
        /*<summary>The get and set for PercentageOfTitleIvaidEarned</summary>*/
        public decimal PercentageOfTitleIvaidEarned { get; set; }
        /*<summary>The get and set for TotalTitleIvaidToReturn</summary>*/
        public decimal TotalTitleIvaidToReturn { get; set; }
        /*<summary>The get and set for UnsubDirectLoanReturnedBySchool</summary>*/
        public decimal UnsubDirectLoanReturnedBySchool { get; set; }
        /*<summary>The get and set for SubDirectLoanReturnedBySchool</summary>*/
        public decimal SubDirectLoanReturnedBySchool { get; set; }
        /*<summary>The get and set for PerkinsLoanReturnedBySchool</summary>*/
        public decimal PerkinsLoanReturnedBySchool { get; set; }
        /*<summary>The get and set for DirectGraduatePlusLoanReturnedBySchool</summary>*/
        public decimal DirectGraduatePlusLoanReturnedBySchool { get; set; }
        /*<summary>The get and set for DirectParentPlusLoanReturnedBySchool</summary>*/
        public decimal DirectParentPlusLoanReturnedBySchool { get; set; }
        /*<summary>The get and set for PellGrantReturnedBySchool</summary>*/
        public decimal PellGrantReturnedBySchool { get; set; }
        /*<summary>The get and set for FseogreturnedBySchool</summary>*/
        public decimal FseogreturnedBySchool { get; set; }
        /*<summary>The get and set for TeachGrantReturnedBySchool</summary>*/
        public decimal TeachGrantReturnedBySchool { get; set; }
        /*<summary>The get and set for IraqAfgGrantReturnedBySchool</summary>*/
        public decimal IraqAfgGrantReturnedBySchool { get; set; }
        /*<summary>The get and set for TotalAmountToBeReturnedBySchool</summary>*/
        public decimal TotalAmountToBeReturnedBySchool { get; set; }
        /*<summary>The get and set for UnsubDirectLoanReturnedByStudent</summary>*/
        public decimal UnsubDirectLoanReturnedByStudent { get; set; }
        /*<summary>The get and set for SubDirectLoanReturnedByStudent</summary>*/
        public decimal SubDirectLoanReturnedByStudent { get; set; }
        /*<summary>The get and set for PerkinsLoanReturnedByStudent</summary>*/
        public decimal PerkinsLoanReturnedByStudent { get; set; }
        /*<summary>The get and set for DirectGraduatePlusLoanReturnedByStudent</summary>*/
        public decimal DirectGraduatePlusLoanReturnedByStudent { get; set; }
        /*<summary>The get and set for DirectParentPlusLoanReturnedByStudent</summary>*/
        public decimal DirectParentPlusLoanReturnedByStudent { get; set; }
        /*<summary>The get and set for FseogreturnedByStudent</summary>*/
        public decimal FseogreturnedByStudent { get; set; }
        /*<summary>The get and set for TeachGrantReturnedByStudent</summary>*/
        public decimal TeachGrantReturnedByStudent { get; set; }
        /*<summary>The get and set for IraqAfgGrantReturnedByStudent</summary>*/
        public decimal IraqAfgGrantReturnedByStudent { get; set; }
        /*<summary>The get and set for TotalAmountToBeReturnedByStudent</summary>*/
        public decimal TotalAmountToBeReturnedByStudent { get; set; }
        /*<summary>The get and set for CreatedById</summary>*/
        public Guid? CreatedById { get; set; }
        /*<summary>The get and set for CreatedDate</summary>*/
        public DateTime? CreatedDate { get; set; }
        /*<summary>The get and set for UpdatedById</summary>*/
        public Guid? UpdatedById { get; set; }
        /*<summary>The get and set for UpdatedDate</summary>*/
        public DateTime? UpdatedDate { get; set; }

        /*<summary>The navigational property for CreatedBy</summary>*/
        public virtual SyUsers CreatedBy { get; set; }
        /*<summary>The navigational property for Termination</summary>*/
        public virtual ArR2t4terminationDetails Termination { get; set; }
        /*<summary>The navigational property for UpdatedBy</summary>*/
        public virtual SyUsers UpdatedBy { get; set; }
    }
}