﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyPeriods.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyPeriods definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyPeriods</summary>*/
    public partial class SyPeriods
    {
        /*<summary>The constructor for SyPeriods</summary>*/
        public SyPeriods()
        {
            ArClsSectMeetings = new HashSet<ArClsSectMeetings>();
            SyPeriodsWorkDays = new HashSet<SyPeriodsWorkDays>();
        }

        /*<summary>The get and set for PeriodId</summary>*/
        public Guid PeriodId { get; set; }
        /*<summary>The get and set for PeriodCode</summary>*/
        public string PeriodCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for PeriodDescrip</summary>*/
        public string PeriodDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for StartTimeId</summary>*/
        public Guid StartTimeId { get; set; }
        /*<summary>The get and set for EndTimeId</summary>*/
        public Guid EndTimeId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for StartTimeAndEndTimeAreFixed</summary>*/
        public bool StartTimeAndEndTimeAreFixed { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for EndTime</summary>*/
        public virtual CmTimeInterval EndTime { get; set; }
        /*<summary>The navigational property for StartTime</summary>*/
        public virtual CmTimeInterval StartTime { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArClsSectMeetings</summary>*/
        public virtual ICollection<ArClsSectMeetings>ArClsSectMeetings { get; set; }
        /*<summary>The navigational property for SyPeriodsWorkDays</summary>*/
        public virtual ICollection<SyPeriodsWorkDays>SyPeriodsWorkDays { get; set; }
    }
}