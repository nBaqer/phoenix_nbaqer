﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyConfigAppSetValues.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyConfigAppSetValues definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyConfigAppSetValues</summary>*/
    public partial class SyConfigAppSetValues
    {
        /*<summary>The get and set for ValueId</summary>*/
        public Guid ValueId { get; set; }
        /*<summary>The get and set for SettingId</summary>*/
        public int? SettingId { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid? CampusId { get; set; }
        /*<summary>The get and set for Value</summary>*/
        public string Value { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Active</summary>*/
        public bool Active { get; set; }
    }
}