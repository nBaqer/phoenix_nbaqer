﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySysRoles.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySysRoles definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySysRoles</summary>*/
    public partial class SySysRoles
    {
        /*<summary>The constructor for SySysRoles</summary>*/
        public SySysRoles()
        {
            SyRoles = new HashSet<SyRoles>();
            SyWidgetResourceRoles = new HashSet<SyWidgetResourceRoles>();
        }

        /*<summary>The get and set for SysRoleId</summary>*/
        public int SysRoleId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for RoleTypeId</summary>*/
        public int RoleTypeId { get; set; }
        /*<summary>The get and set for Permission</summary>*/
        public string Permission { get; set; }

        /*<summary>The navigational property for RoleType</summary>*/
        public virtual SyUserType RoleType { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SyRoles</summary>*/
        public virtual ICollection<SyRoles>SyRoles { get; set; }
        /*<summary>The navigational property for SyWidgetResourceRoles</summary>*/
        public virtual ICollection<SyWidgetResourceRoles>SyWidgetResourceRoles { get; set; }
    }
}