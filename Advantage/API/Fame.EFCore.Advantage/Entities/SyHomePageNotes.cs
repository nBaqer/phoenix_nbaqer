﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyHomePageNotes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyHomePageNotes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyHomePageNotes</summary>*/
    public partial class SyHomePageNotes
    {
        /*<summary>The get and set for HomePageNoteId</summary>*/
        public Guid HomePageNoteId { get; set; }
        /*<summary>The get and set for ModuleId</summary>*/
        public int ModuleId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for Title</summary>*/
        public string Title { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}