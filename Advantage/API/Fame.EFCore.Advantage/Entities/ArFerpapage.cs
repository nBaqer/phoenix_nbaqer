﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArFerpapage.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArFerpapage definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArFerpapage</summary>*/
    public partial class ArFerpapage
    {
        /*<summary>The get and set for FerpapageId</summary>*/
        public Guid FerpapageId { get; set; }
        /*<summary>The get and set for FerpacategoryId</summary>*/
        public Guid FerpacategoryId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public short ResourceId { get; set; }

        /*<summary>The navigational property for Ferpacategory</summary>*/
        public virtual ArFerpacategory Ferpacategory { get; set; }
        /*<summary>The navigational property for Resource</summary>*/
        public virtual SyResources Resource { get; set; }
    }
}