﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlSkills.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlSkills definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlSkills</summary>*/
    public partial class PlSkills
    {
        /*<summary>The constructor for PlSkills</summary>*/
        public PlSkills()
        {
            AdLeadSkills = new HashSet<AdLeadSkills>();
        }

        /*<summary>The get and set for SkillId</summary>*/
        public Guid SkillId { get; set; }
        /*<summary>The get and set for SkillCode</summary>*/
        public string SkillCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for SkillDescrip</summary>*/
        public string SkillDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for SkillGrpId</summary>*/
        public Guid? SkillGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for SkillGrp</summary>*/
        public virtual PlSkillGroups SkillGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeadSkills</summary>*/
        public virtual ICollection<AdLeadSkills>AdLeadSkills { get; set; }
    }
}