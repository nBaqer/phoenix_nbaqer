﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArPeriod.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArPeriod definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArPeriod</summary>*/
    public partial class ArPeriod
    {
        /*<summary>The get and set for PrdId</summary>*/
        public Guid PrdId { get; set; }
        /*<summary>The get and set for PrdCode</summary>*/
        public string PrdCode { get; set; }
        /*<summary>The get and set for PrdDescrip</summary>*/
        public string PrdDescrip { get; set; }
        /*<summary>The get and set for PrdLen</summary>*/
        public int? PrdLen { get; set; }
        /*<summary>The get and set for Mon</summary>*/
        public bool? Mon { get; set; }
        /*<summary>The get and set for Tue</summary>*/
        public bool? Tue { get; set; }
        /*<summary>The get and set for Wed</summary>*/
        public bool? Wed { get; set; }
        /*<summary>The get and set for Thur</summary>*/
        public bool? Thur { get; set; }
        /*<summary>The get and set for Fri</summary>*/
        public bool? Fri { get; set; }
        /*<summary>The get and set for Sat</summary>*/
        public bool? Sat { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for TimeIntervalId</summary>*/
        public Guid TimeIntervalId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for TimeInterval</summary>*/
        public virtual CmTimeInterval TimeInterval { get; set; }
    }
}