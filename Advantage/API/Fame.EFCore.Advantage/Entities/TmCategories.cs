﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="TmCategories.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The TmCategories definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for TmCategories</summary>*/
    public partial class TmCategories
    {
        /*<summary>The constructor for TmCategories</summary>*/
        public TmCategories()
        {
            TmTasks = new HashSet<TmTasks>();
        }

        /*<summary>The get and set for CategoryId</summary>*/
        public Guid CategoryId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for CampGroupId</summary>*/
        public Guid? CampGroupId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public Guid? ModUser { get; set; }
        /*<summary>The get and set for Active</summary>*/
        public byte? Active { get; set; }

        /*<summary>The navigational property for TmTasks</summary>*/
        public virtual ICollection<TmTasks>TmTasks { get; set; }
    }
}