﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyStateBoardCourses.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyStateBoardCourses definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyStateBoardCourses</summary>*/
    public partial class SyStateBoardCourses
    {
        /*<summary>The constructor for SyStateBoardCourses</summary>*/
        public SyStateBoardCourses()
        {
            SyStateBoardProgramCourseMappings = new HashSet<SyStateBoardProgramCourseMappings>();
        }

        /*<summary>The get and set for StateBoardCourseId</summary>*/
        public int StateBoardCourseId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for StateId</summary>*/
        public Guid StateId { get; set; }
        /*<summary>The get and set for CreatedById</summary>*/
        public Guid CreatedById { get; set; }
        /*<summary>The get and set for CreatedDate</summary>*/
        public DateTime CreatedDate { get; set; }

        /*<summary>The navigational property for CreatedBy</summary>*/
        public virtual SyUsers CreatedBy { get; set; }
        /*<summary>The navigational property for State</summary>*/
        public virtual SyStates State { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SyStateBoardProgramCourseMappings</summary>*/
        public virtual ICollection<SyStateBoardProgramCourseMappings>SyStateBoardProgramCourseMappings { get; set; }
    }
}