﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyWapiSettings.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyWapiSettings definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyWapiSettings</summary>*/
    public partial class SyWapiSettings
    {
        /*<summary>The get and set for Id</summary>*/
        public int Id { get; set; }
        /*<summary>The get and set for CodeOperation</summary>*/
        public string CodeOperation { get; set; }
        /*<summary>The get and set for ExternalUrl</summary>*/
        public string ExternalUrl { get; set; }
        /*<summary>The get and set for IdExtCompany</summary>*/
        public int IdExtCompany { get; set; }
        /*<summary>The get and set for IdExtOperation</summary>*/
        public int IdExtOperation { get; set; }
        /*<summary>The get and set for IdAllowedService</summary>*/
        public int IdAllowedService { get; set; }
        /*<summary>The get and set for IdSecondAllowedService</summary>*/
        public int? IdSecondAllowedService { get; set; }
        /*<summary>The get and set for FirstAllowedServiceQueryString</summary>*/
        public string FirstAllowedServiceQueryString { get; set; }
        /*<summary>The get and set for SecondAllowedServiceQueryString</summary>*/
        public string SecondAllowedServiceQueryString { get; set; }
        /*<summary>The get and set for ConsumerKey</summary>*/
        public string ConsumerKey { get; set; }
        /*<summary>The get and set for PrivateKey</summary>*/
        public string PrivateKey { get; set; }
        /*<summary>The get and set for OperationSecondTimeInterval</summary>*/
        public int OperationSecondTimeInterval { get; set; }
        /*<summary>The get and set for PollSecondForOnDemandOperation</summary>*/
        public int PollSecondForOnDemandOperation { get; set; }
        /*<summary>The get and set for FlagOnDemandOperation</summary>*/
        public bool FlagOnDemandOperation { get; set; }
        /*<summary>The get and set for FlagRefreshConfiguration</summary>*/
        public bool FlagRefreshConfiguration { get; set; }
        /*<summary>The get and set for IsActive</summary>*/
        public bool? IsActive { get; set; }
        /*<summary>The get and set for DateMod</summary>*/
        public DateTime DateMod { get; set; }
        /*<summary>The get and set for DateLastExecution</summary>*/
        public DateTime DateLastExecution { get; set; }
        /*<summary>The get and set for UserMod</summary>*/
        public string UserMod { get; set; }
        /*<summary>The get and set for UserName</summary>*/
        public string UserName { get; set; }

        /*<summary>The navigational property for IdAllowedServiceNavigation</summary>*/
        public virtual SyWapiAllowedServices IdAllowedServiceNavigation { get; set; }
        /*<summary>The navigational property for IdExtCompanyNavigation</summary>*/
        public virtual SyWapiExternalCompanies IdExtCompanyNavigation { get; set; }
        /*<summary>The navigational property for IdExtOperationNavigation</summary>*/
        public virtual SyWapiExternalOperationMode IdExtOperationNavigation { get; set; }
        /*<summary>The navigational property for IdSecondAllowedServiceNavigation</summary>*/
        public virtual SyWapiAllowedServices IdSecondAllowedServiceNavigation { get; set; }
    }
}