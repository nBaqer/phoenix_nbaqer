﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyInstFldsDdreq.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyInstFldsDdreq definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyInstFldsDdreq</summary>*/
    public partial class SyInstFldsDdreq
    {
        /*<summary>The get and set for DdfldReqId</summary>*/
        public Guid DdfldReqId { get; set; }
        /*<summary>The get and set for FldId</summary>*/
        public int FldId { get; set; }
    }
}