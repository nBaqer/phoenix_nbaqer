﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdvProgTyps.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdvProgTyps definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdvProgTyps</summary>*/
    public partial class AdvProgTyps
    {
        /*<summary>The get and set for AdvProgTypId</summary>*/
        public byte AdvProgTypId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
    }
}