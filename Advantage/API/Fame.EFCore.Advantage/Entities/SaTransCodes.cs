﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaTransCodes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaTransCodes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaTransCodes</summary>*/
    public partial class SaTransCodes
    {
        /*<summary>The constructor for SaTransCodes</summary>*/
        public SaTransCodes()
        {
            AdLeadTransactions = new HashSet<AdLeadTransactions>();
            ArPrgVersionFees = new HashSet<ArPrgVersionFees>();
            SaCourseFees = new HashSet<SaCourseFees>();
            SaLateFees = new HashSet<SaLateFees>();
            SaPeriodicFees = new HashSet<SaPeriodicFees>();
            SaProgramVersionFees = new HashSet<SaProgramVersionFees>();
            SaTransCodeGlaccounts = new HashSet<SaTransCodeGlaccounts>();
            SaTransactionsPaymentCode = new HashSet<SaTransactions>();
            SaTransactionsTransCode = new HashSet<SaTransactions>();
            SyAwardTypes9010Mapping = new HashSet<SyAwardTypes9010Mapping>();
        }

        /*<summary>The get and set for TransCodeId</summary>*/
        public Guid TransCodeId { get; set; }
        /*<summary>The get and set for TransCodeCode</summary>*/
        public string TransCodeCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for TransCodeDescrip</summary>*/
        public string TransCodeDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for BillTypeId</summary>*/
        public Guid BillTypeId { get; set; }
        /*<summary>The get and set for DefEarnings</summary>*/
        public bool DefEarnings { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for IsInstCharge</summary>*/
        public bool IsInstCharge { get; set; }
        /*<summary>The get and set for SysTransCodeId</summary>*/
        public int? SysTransCodeId { get; set; }
        /*<summary>The get and set for Is1098T</summary>*/
        public bool Is1098T { get; set; }

        /*<summary>The navigational property for BillType</summary>*/
        public virtual SaBillTypes BillType { get; set; }
        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SysTransCode</summary>*/
        public virtual SaSysTransCodes SysTransCode { get; set; }
        /*<summary>The navigational property for AdLeadTransactions</summary>*/
        public virtual ICollection<AdLeadTransactions>AdLeadTransactions { get; set; }
        /*<summary>The navigational property for ArPrgVersionFees</summary>*/
        public virtual ICollection<ArPrgVersionFees>ArPrgVersionFees { get; set; }
        /*<summary>The navigational property for SaCourseFees</summary>*/
        public virtual ICollection<SaCourseFees>SaCourseFees { get; set; }
        /*<summary>The navigational property for SaLateFees</summary>*/
        public virtual ICollection<SaLateFees>SaLateFees { get; set; }
        /*<summary>The navigational property for SaPeriodicFees</summary>*/
        public virtual ICollection<SaPeriodicFees>SaPeriodicFees { get; set; }
        /*<summary>The navigational property for SaProgramVersionFees</summary>*/
        public virtual ICollection<SaProgramVersionFees>SaProgramVersionFees { get; set; }
        /*<summary>The navigational property for SaTransCodeGlaccounts</summary>*/
        public virtual ICollection<SaTransCodeGlaccounts>SaTransCodeGlaccounts { get; set; }
        /*<summary>The navigational property for SaTransactionsPaymentCode</summary>*/
        public virtual ICollection<SaTransactions>SaTransactionsPaymentCode { get; set; }
        /*<summary>The navigational property for SaTransactionsTransCode</summary>*/
        public virtual ICollection<SaTransactions>SaTransactionsTransCode { get; set; }
        /*<summary>The navigational property for SyAwardTypes9010Mapping</summary>*/
        public virtual ICollection<SyAwardTypes9010Mapping>SyAwardTypes9010Mapping { get; set; }
    }
}