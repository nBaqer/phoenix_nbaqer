﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArProgramVersionType.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArProgramVersionType definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArProgramVersionType</summary>*/
    public partial class ArProgramVersionType
    {
        /*<summary>The constructor for ArProgramVersionType</summary>*/
        public ArProgramVersionType()
        {
            ArStuEnrollments = new HashSet<ArStuEnrollments>();
        }

        /*<summary>The get and set for ProgramVersionTypeId</summary>*/
        public int ProgramVersionTypeId { get; set; }
        /*<summary>The get and set for CodeProgramVersionType</summary>*/
        public string CodeProgramVersionType { get; set; }
        /*<summary>The get and set for DescriptionProgramVersionType</summary>*/
        public string DescriptionProgramVersionType { get; set; }

        /*<summary>The navigational property for ArStuEnrollments</summary>*/
        public virtual ICollection<ArStuEnrollments>ArStuEnrollments { get; set; }
    }
}