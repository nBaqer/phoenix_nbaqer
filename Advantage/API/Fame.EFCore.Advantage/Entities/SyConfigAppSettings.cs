﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyConfigAppSettings.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyConfigAppSettings definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyConfigAppSettings</summary>*/
    public partial class SyConfigAppSettings
    {
        /*<summary>The get and set for KeyName</summary>*/
        public string KeyName { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for CampusSpecific</summary>*/
        public bool? CampusSpecific { get; set; }
        /*<summary>The get and set for ExtraConfirmation</summary>*/
        public bool ExtraConfirmation { get; set; }
        /*<summary>The get and set for SettingId</summary>*/
        public int SettingId { get; set; }
    }
}