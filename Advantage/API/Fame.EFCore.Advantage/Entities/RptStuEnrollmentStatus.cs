﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="RptStuEnrollmentStatus.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The RptStuEnrollmentStatus definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for RptStuEnrollmentStatus</summary>*/
    public partial class RptStuEnrollmentStatus
    {
        /*<summary>The get and set for StatusCodeId</summary>*/
        public Guid StatusCodeId { get; set; }
        /*<summary>The get and set for StatusCodeDescrip</summary>*/
        public string StatusCodeDescrip { get; set; }
    }
}