﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyStatusCodes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyStatusCodes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyStatusCodes</summary>*/
    public partial class SyStatusCodes
    {
        /*<summary>The constructor for SyStatusCodes</summary>*/
        public SyStatusCodes()
        {
            AdLeads = new HashSet<AdLeads>();
            ArR2t4terminationDetails = new HashSet<ArR2t4terminationDetails>();
            ArStuEnrollments = new HashSet<ArStuEnrollments>();
            SyLeadStatusChangesNewStatus = new HashSet<SyLeadStatusChanges>();
            SyLeadStatusChangesOrigStatus = new HashSet<SyLeadStatusChanges>();
            SyLeadStatusesChangesNewStatus = new HashSet<SyLeadStatusesChanges>();
            SyLeadStatusesChangesOrigStatus = new HashSet<SyLeadStatusesChanges>();
            SyStatusChangesDeletedNewStatus = new HashSet<SyStatusChangesDeleted>();
            SyStatusChangesDeletedOrigStatus = new HashSet<SyStatusChangesDeleted>();
            SyStudentStatusChangesNewStatus = new HashSet<SyStudentStatusChanges>();
            SyStudentStatusChangesOrigStatus = new HashSet<SyStudentStatusChanges>();
            TmTasks = new HashSet<TmTasks>();
        }

        /*<summary>The get and set for StatusCodeId</summary>*/
        public Guid StatusCodeId { get; set; }
        /*<summary>The get and set for StatusCode</summary>*/
        public string StatusCode { get; set; }
        /*<summary>The get and set for StatusCodeDescrip</summary>*/
        public string StatusCodeDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for SysStatusId</summary>*/
        public int SysStatusId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for AcadProbation</summary>*/
        public bool? AcadProbation { get; set; }
        /*<summary>The get and set for DiscProbation</summary>*/
        public bool? DiscProbation { get; set; }
        /*<summary>The get and set for IsDefaultLeadStatus</summary>*/
        public bool? IsDefaultLeadStatus { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SysStatus</summary>*/
        public virtual SySysStatus SysStatus { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for ArR2t4terminationDetails</summary>*/
        public virtual ICollection<ArR2t4terminationDetails>ArR2t4terminationDetails { get; set; }
        /*<summary>The navigational property for ArStuEnrollments</summary>*/
        public virtual ICollection<ArStuEnrollments>ArStuEnrollments { get; set; }
        /*<summary>The navigational property for SyLeadStatusChangesNewStatus</summary>*/
        public virtual ICollection<SyLeadStatusChanges>SyLeadStatusChangesNewStatus { get; set; }
        /*<summary>The navigational property for SyLeadStatusChangesOrigStatus</summary>*/
        public virtual ICollection<SyLeadStatusChanges>SyLeadStatusChangesOrigStatus { get; set; }
        /*<summary>The navigational property for SyLeadStatusesChangesNewStatus</summary>*/
        public virtual ICollection<SyLeadStatusesChanges>SyLeadStatusesChangesNewStatus { get; set; }
        /*<summary>The navigational property for SyLeadStatusesChangesOrigStatus</summary>*/
        public virtual ICollection<SyLeadStatusesChanges>SyLeadStatusesChangesOrigStatus { get; set; }
        /*<summary>The navigational property for SyStatusChangesDeletedNewStatus</summary>*/
        public virtual ICollection<SyStatusChangesDeleted>SyStatusChangesDeletedNewStatus { get; set; }
        /*<summary>The navigational property for SyStatusChangesDeletedOrigStatus</summary>*/
        public virtual ICollection<SyStatusChangesDeleted>SyStatusChangesDeletedOrigStatus { get; set; }
        /*<summary>The navigational property for SyStudentStatusChangesNewStatus</summary>*/
        public virtual ICollection<SyStudentStatusChanges>SyStudentStatusChangesNewStatus { get; set; }
        /*<summary>The navigational property for SyStudentStatusChangesOrigStatus</summary>*/
        public virtual ICollection<SyStudentStatusChanges>SyStudentStatusChangesOrigStatus { get; set; }
        /*<summary>The navigational property for TmTasks</summary>*/
        public virtual ICollection<TmTasks>TmTasks { get; set; }
    }
}