﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyModuleEntities.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyModuleEntities definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyModuleEntities</summary>*/
    public partial class SyModuleEntities
    {
        /*<summary>The get and set for ModuleEntityId</summary>*/
        public Guid ModuleEntityId { get; set; }
        /*<summary>The get and set for ModuleId</summary>*/
        public byte? ModuleId { get; set; }
        /*<summary>The get and set for EntityId</summary>*/
        public Guid? EntityId { get; set; }

        /*<summary>The navigational property for Entity</summary>*/
        public virtual SyEntities Entity { get; set; }
    }
}