﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyAuditHist.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyAuditHist definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyAuditHist</summary>*/
    public partial class SyAuditHist
    {
        /*<summary>The constructor for SyAuditHist</summary>*/
        public SyAuditHist()
        {
            SyAuditHistDetail = new HashSet<SyAuditHistDetail>();
        }

        /*<summary>The get and set for AuditHistId</summary>*/
        public Guid AuditHistId { get; set; }
        /*<summary>The get and set for TableName</summary>*/
        public string TableName { get; set; }
        /*<summary>The get and set for Event</summary>*/
        public string Event { get; set; }
        /*<summary>The get and set for EventRows</summary>*/
        public int EventRows { get; set; }
        /*<summary>The get and set for EventDate</summary>*/
        public DateTime? EventDate { get; set; }
        /*<summary>The get and set for AppName</summary>*/
        public string AppName { get; set; }
        /*<summary>The get and set for UserName</summary>*/
        public string UserName { get; set; }

        /*<summary>The navigational property for SyAuditHistDetail</summary>*/
        public virtual ICollection<SyAuditHistDetail>SyAuditHistDetail { get; set; }
    }
}