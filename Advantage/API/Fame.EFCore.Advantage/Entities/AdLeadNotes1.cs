﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadNotes1.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadNotes1 definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadNotes1</summary>*/
    public partial class AdLeadNotes1
    {
        /*<summary>The get and set for LeadNoteId</summary>*/
        public Guid LeadNoteId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for LeadNoteDescrip</summary>*/
        public string LeadNoteDescrip { get; set; }
        /*<summary>The get and set for ModuleCode</summary>*/
        public string ModuleCode { get; set; }
        /*<summary>The get and set for UserId</summary>*/
        public Guid? UserId { get; set; }
        /*<summary>The get and set for CreatedDate</summary>*/
        public DateTime CreatedDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}