﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLevel.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLevel definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLevel</summary>*/
    public partial class AdLevel
    {
        /*<summary>The constructor for AdLevel</summary>*/
        public AdLevel()
        {
            AdExtraCurricular = new HashSet<AdExtraCurricular>();
            AdSkills = new HashSet<AdSkills>();
            SyInstitutions = new HashSet<SyInstitutions>();
        }

        /*<summary>The get and set for LevelId</summary>*/
        public int LevelId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }

        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdExtraCurricular</summary>*/
        public virtual ICollection<AdExtraCurricular>AdExtraCurricular { get; set; }
        /*<summary>The navigational property for AdSkills</summary>*/
        public virtual ICollection<AdSkills>AdSkills { get; set; }
        /*<summary>The navigational property for SyInstitutions</summary>*/
        public virtual ICollection<SyInstitutions>SyInstitutions { get; set; }
    }
}