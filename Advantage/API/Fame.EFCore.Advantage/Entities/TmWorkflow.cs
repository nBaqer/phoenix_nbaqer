﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="TmWorkflow.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The TmWorkflow definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for TmWorkflow</summary>*/
    public partial class TmWorkflow
    {
        /*<summary>The get and set for WorkflowId</summary>*/
        public Guid WorkflowId { get; set; }
        /*<summary>The get and set for Name</summary>*/
        public string Name { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for CmpGroupid</summary>*/
        public Guid? CmpGroupid { get; set; }
        /*<summary>The get and set for ModuleEntityId</summary>*/
        public Guid? ModuleEntityId { get; set; }
        /*<summary>The get and set for StartTaskId</summary>*/
        public Guid? StartTaskId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for IsActive</summary>*/
        public byte? IsActive { get; set; }
    }
}