﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyModCaptions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyModCaptions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyModCaptions</summary>*/
    public partial class SyModCaptions
    {
        /*<summary>The get and set for ModCapId</summary>*/
        public int ModCapId { get; set; }
        /*<summary>The get and set for ModuleId</summary>*/
        public byte ModuleId { get; set; }
        /*<summary>The get and set for LangId</summary>*/
        public byte LangId { get; set; }
        /*<summary>The get and set for Caption</summary>*/
        public string Caption { get; set; }
        /*<summary>The get and set for ModuleCode</summary>*/
        public string ModuleCode { get; set; }
    }
}