﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyInstitutionImportTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyInstitutionImportTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyInstitutionImportTypes</summary>*/
    public partial class SyInstitutionImportTypes
    {
        /*<summary>The constructor for SyInstitutionImportTypes</summary>*/
        public SyInstitutionImportTypes()
        {
            SyInstitutions = new HashSet<SyInstitutions>();
        }

        /*<summary>The get and set for ImportTypeId</summary>*/
        public int ImportTypeId { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }

        /*<summary>The navigational property for SyInstitutions</summary>*/
        public virtual ICollection<SyInstitutions>SyInstitutions { get; set; }
    }
}