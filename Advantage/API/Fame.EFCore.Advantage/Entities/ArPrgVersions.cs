﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArPrgVersions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArPrgVersions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArPrgVersions</summary>*/
    public partial class ArPrgVersions
    {
        /*<summary>The constructor for ArPrgVersions</summary>*/
        public ArPrgVersions()
        {
            AdLeads = new HashSet<AdLeads>();
            AdPrgVerTestDetails = new HashSet<AdPrgVerTestDetails>();
            ArCampusPrgVersions = new HashSet<ArCampusPrgVersions>();
            ArCourseReqs = new HashSet<ArCourseReqs>();
            ArPrgChargePeriodSeq = new HashSet<ArPrgChargePeriodSeq>();
            ArPrgMinorCertsChild = new HashSet<ArPrgMinorCerts>();
            ArPrgMinorCertsParent = new HashSet<ArPrgMinorCerts>();
            ArPrgVerInstructionType = new HashSet<ArPrgVerInstructionType>();
            ArPrgVersionFees = new HashSet<ArPrgVersionFees>();
            ArProgSchedules = new HashSet<ArProgSchedules>();
            ArProgVerDef = new HashSet<ArProgVerDef>();
            ArStuEnrollments = new HashSet<ArStuEnrollments>();
            ArTerm = new HashSet<ArTerm>();
            SaPeriodicFees = new HashSet<SaPeriodicFees>();
            SaPrgVerDefaultChargePeriods = new HashSet<SaPrgVerDefaultChargePeriods>();
            SaProgramVersionFees = new HashSet<SaProgramVersionFees>();
            SyApprovedNaccasprogramVersion = new HashSet<SyApprovedNaccasprogramVersion>();
        }

        /*<summary>The get and set for PrgVerId</summary>*/
        public Guid PrgVerId { get; set; }
        /*<summary>The get and set for ProgId</summary>*/
        public Guid? ProgId { get; set; }
        /*<summary>The get and set for PrgVerCode</summary>*/
        public string PrgVerCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for PrgGrpId</summary>*/
        public Guid? PrgGrpId { get; set; }
        /*<summary>The get and set for PrgVerDescrip</summary>*/
        public string PrgVerDescrip { get; set; }
        /*<summary>The get and set for DegreeId</summary>*/
        public Guid? DegreeId { get; set; }
        /*<summary>The get and set for Sapid</summary>*/
        public Guid? Sapid { get; set; }
        /*<summary>The get and set for ThGrdScaleId</summary>*/
        public Guid? ThGrdScaleId { get; set; }
        /*<summary>The get and set for TestingModelId</summary>*/
        public byte? TestingModelId { get; set; }
        /*<summary>The get and set for Weeks</summary>*/
        public short Weeks { get; set; }
        /*<summary>The get and set for Terms</summary>*/
        public short Terms { get; set; }
        /*<summary>The get and set for Hours</summary>*/
        public decimal Hours { get; set; }
        /*<summary>The get and set for Credits</summary>*/
        public decimal Credits { get; set; }
        /*<summary>The get and set for LthalfTime</summary>*/
        public short? LthalfTime { get; set; }
        /*<summary>The get and set for HalfTime</summary>*/
        public short? HalfTime { get; set; }
        /*<summary>The get and set for ThreeQuartTime</summary>*/
        public short? ThreeQuartTime { get; set; }
        /*<summary>The get and set for FullTime</summary>*/
        public bool FullTime { get; set; }
        /*<summary>The get and set for DeptId</summary>*/
        public Guid DeptId { get; set; }
        /*<summary>The get and set for GrdSystemId</summary>*/
        public Guid GrdSystemId { get; set; }
        /*<summary>The get and set for WeightedGpa</summary>*/
        public bool? WeightedGpa { get; set; }
        /*<summary>The get and set for BillingMethodId</summary>*/
        public Guid? BillingMethodId { get; set; }
        /*<summary>The get and set for TuitionEarningId</summary>*/
        public Guid TuitionEarningId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for AttendanceLevel</summary>*/
        public bool? AttendanceLevel { get; set; }
        /*<summary>The get and set for CustomAttendance</summary>*/
        public bool? CustomAttendance { get; set; }
        /*<summary>The get and set for ProgTypId</summary>*/
        public Guid ProgTypId { get; set; }
        /*<summary>The get and set for IsContinuingEd</summary>*/
        public bool? IsContinuingEd { get; set; }
        /*<summary>The get and set for UnitTypeId</summary>*/
        public Guid UnitTypeId { get; set; }
        /*<summary>The get and set for TrackTardies</summary>*/
        public bool TrackTardies { get; set; }
        /*<summary>The get and set for TardiesMakingAbsence</summary>*/
        public int? TardiesMakingAbsence { get; set; }
        /*<summary>The get and set for SchedMethodId</summary>*/
        public int SchedMethodId { get; set; }
        /*<summary>The get and set for UseTimeClock</summary>*/
        public bool? UseTimeClock { get; set; }
        /*<summary>The get and set for TotalCost</summary>*/
        public decimal? TotalCost { get; set; }
        /*<summary>The get and set for AcademicYearLength</summary>*/
        public decimal? AcademicYearLength { get; set; }
        /*<summary>The get and set for AcademicYearWeeks</summary>*/
        public short? AcademicYearWeeks { get; set; }
        /*<summary>The get and set for PayPeriodPerAcYear</summary>*/
        public short? PayPeriodPerAcYear { get; set; }
        /*<summary>The get and set for Fasapid</summary>*/
        public Guid? Fasapid { get; set; }
        /*<summary>The get and set for IsBillingMethodCharged</summary>*/
        public bool IsBillingMethodCharged { get; set; }
        /*<summary>The get and set for DoCourseWeightOverallGpa</summary>*/
        public bool DoCourseWeightOverallGpa { get; set; }
        /*<summary>The get and set for ProgramRegistrationType</summary>*/
        public int? ProgramRegistrationType { get; set; }

        /*<summary>The navigational property for BillingMethod</summary>*/
        public virtual SaBillingMethods BillingMethod { get; set; }
        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Degree</summary>*/
        public virtual ArDegrees Degree { get; set; }
        /*<summary>The navigational property for Dept</summary>*/
        public virtual ArDepartments Dept { get; set; }
        /*<summary>The navigational property for Fasap</summary>*/
        public virtual ArSap Fasap { get; set; }
        /*<summary>The navigational property for GrdSystem</summary>*/
        public virtual ArGradeSystems GrdSystem { get; set; }
        /*<summary>The navigational property for PrgGrp</summary>*/
        public virtual ArPrgGrp PrgGrp { get; set; }
        /*<summary>The navigational property for Prog</summary>*/
        public virtual ArPrograms Prog { get; set; }
        /*<summary>The navigational property for ProgTyp</summary>*/
        public virtual ArProgTypes ProgTyp { get; set; }
        /*<summary>The navigational property for Sap</summary>*/
        public virtual ArSap Sap { get; set; }
        /*<summary>The navigational property for SchedMethod</summary>*/
        public virtual SySchedulingMethods SchedMethod { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ThGrdScale</summary>*/
        public virtual ArGradeScales ThGrdScale { get; set; }
        /*<summary>The navigational property for TuitionEarning</summary>*/
        public virtual SaTuitionEarnings TuitionEarning { get; set; }
        /*<summary>The navigational property for UnitType</summary>*/
        public virtual ArAttUnitType UnitType { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for AdPrgVerTestDetails</summary>*/
        public virtual ICollection<AdPrgVerTestDetails>AdPrgVerTestDetails { get; set; }
        /*<summary>The navigational property for ArCampusPrgVersions</summary>*/
        public virtual ICollection<ArCampusPrgVersions>ArCampusPrgVersions { get; set; }
        /*<summary>The navigational property for ArCourseReqs</summary>*/
        public virtual ICollection<ArCourseReqs>ArCourseReqs { get; set; }
        /*<summary>The navigational property for ArPrgChargePeriodSeq</summary>*/
        public virtual ICollection<ArPrgChargePeriodSeq>ArPrgChargePeriodSeq { get; set; }
        /*<summary>The navigational property for ArPrgMinorCertsChild</summary>*/
        public virtual ICollection<ArPrgMinorCerts>ArPrgMinorCertsChild { get; set; }
        /*<summary>The navigational property for ArPrgMinorCertsParent</summary>*/
        public virtual ICollection<ArPrgMinorCerts>ArPrgMinorCertsParent { get; set; }
        /*<summary>The navigational property for ArPrgVerInstructionType</summary>*/
        public virtual ICollection<ArPrgVerInstructionType>ArPrgVerInstructionType { get; set; }
        /*<summary>The navigational property for ArPrgVersionFees</summary>*/
        public virtual ICollection<ArPrgVersionFees>ArPrgVersionFees { get; set; }
        /*<summary>The navigational property for ArProgSchedules</summary>*/
        public virtual ICollection<ArProgSchedules>ArProgSchedules { get; set; }
        /*<summary>The navigational property for ArProgVerDef</summary>*/
        public virtual ICollection<ArProgVerDef>ArProgVerDef { get; set; }
        /*<summary>The navigational property for ArStuEnrollments</summary>*/
        public virtual ICollection<ArStuEnrollments>ArStuEnrollments { get; set; }
        /*<summary>The navigational property for ArTerm</summary>*/
        public virtual ICollection<ArTerm>ArTerm { get; set; }
        /*<summary>The navigational property for SaPeriodicFees</summary>*/
        public virtual ICollection<SaPeriodicFees>SaPeriodicFees { get; set; }
        /*<summary>The navigational property for SaPrgVerDefaultChargePeriods</summary>*/
        public virtual ICollection<SaPrgVerDefaultChargePeriods>SaPrgVerDefaultChargePeriods { get; set; }
        /*<summary>The navigational property for SaProgramVersionFees</summary>*/
        public virtual ICollection<SaProgramVersionFees>SaProgramVersionFees { get; set; }
        /*<summary>The navigational property for SyApprovedNaccasprogramVersion</summary>*/
        public virtual ICollection<SyApprovedNaccasprogramVersion>SyApprovedNaccasprogramVersion { get; set; }
    }
}