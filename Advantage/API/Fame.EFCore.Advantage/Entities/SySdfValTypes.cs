﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySdfValTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySdfValTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySdfValTypes</summary>*/
    public partial class SySdfValTypes
    {
        /*<summary>The get and set for ValTypeId</summary>*/
        public byte ValTypeId { get; set; }
        /*<summary>The get and set for ValType</summary>*/
        public string ValType { get; set; }
    }
}