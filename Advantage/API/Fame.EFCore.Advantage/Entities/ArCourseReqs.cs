﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArCourseReqs.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArCourseReqs definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArCourseReqs</summary>*/
    public partial class ArCourseReqs
    {
        /*<summary>The get and set for CourseReqId</summary>*/
        public Guid CourseReqId { get; set; }
        /*<summary>The get and set for ReqId</summary>*/
        public Guid ReqId { get; set; }
        /*<summary>The get and set for PreCoReqId</summary>*/
        public Guid? PreCoReqId { get; set; }
        /*<summary>The get and set for CourseReqTypId</summary>*/
        public byte? CourseReqTypId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for PrgVerId</summary>*/
        public Guid? PrgVerId { get; set; }

        /*<summary>The navigational property for PreCoReq</summary>*/
        public virtual ArReqs PreCoReq { get; set; }
        /*<summary>The navigational property for PrgVer</summary>*/
        public virtual ArPrgVersions PrgVer { get; set; }
        /*<summary>The navigational property for Req</summary>*/
        public virtual ArReqs Req { get; set; }
    }
}