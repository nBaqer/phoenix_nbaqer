﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArProgScheduleDetails.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArProgScheduleDetails definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArProgScheduleDetails</summary>*/
    public partial class ArProgScheduleDetails
    {
        /*<summary>The get and set for ScheduleDetailId</summary>*/
        public Guid ScheduleDetailId { get; set; }
        /*<summary>The get and set for ScheduleId</summary>*/
        public Guid? ScheduleId { get; set; }
        /*<summary>The get and set for Dw</summary>*/
        public short? Dw { get; set; }
        /*<summary>The get and set for Total</summary>*/
        public decimal? Total { get; set; }
        /*<summary>The get and set for Timein</summary>*/
        public DateTime? Timein { get; set; }
        /*<summary>The get and set for Lunchin</summary>*/
        public DateTime? Lunchin { get; set; }
        /*<summary>The get and set for Lunchout</summary>*/
        public DateTime? Lunchout { get; set; }
        /*<summary>The get and set for Timeout</summary>*/
        public DateTime? Timeout { get; set; }
        /*<summary>The get and set for Maxnolunch</summary>*/
        public decimal? Maxnolunch { get; set; }
        /*<summary>The get and set for AllowEarlyin</summary>*/
        public bool? AllowEarlyin { get; set; }
        /*<summary>The get and set for AllowLateout</summary>*/
        public bool? AllowLateout { get; set; }
        /*<summary>The get and set for AllowExtrahours</summary>*/
        public bool? AllowExtrahours { get; set; }
        /*<summary>The get and set for CheckTardyin</summary>*/
        public bool? CheckTardyin { get; set; }
        /*<summary>The get and set for MaxBeforetardy</summary>*/
        public DateTime? MaxBeforetardy { get; set; }
        /*<summary>The get and set for TardyIntime</summary>*/
        public DateTime? TardyIntime { get; set; }
        /*<summary>The get and set for MinimumHoursToBePresent</summary>*/
        public decimal? MinimumHoursToBePresent { get; set; }

        /*<summary>The navigational property for Schedule</summary>*/
        public virtual ArProgSchedules Schedule { get; set; }
    }
}