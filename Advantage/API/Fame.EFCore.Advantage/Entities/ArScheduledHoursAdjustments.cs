﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArScheduledHoursAdjustments.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArScheduledHoursAdjustments definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArScheduledHoursAdjustments</summary>*/
    public partial class ArScheduledHoursAdjustments
    {
        /*<summary>The get and set for ScheduledHoursAdjustmentId</summary>*/
        public Guid ScheduledHoursAdjustmentId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for RecordDate</summary>*/
        public DateTime RecordDate { get; set; }
        /*<summary>The get and set for AdjustmentAmount</summary>*/
        public decimal? AdjustmentAmount { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
    }
}