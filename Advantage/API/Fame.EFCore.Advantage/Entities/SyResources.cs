﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyResources.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyResources definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyResources</summary>*/
    public partial class SyResources
    {
        /*<summary>The constructor for SyResources</summary>*/
        public SyResources()
        {
            ArFerpapage = new HashSet<ArFerpapage>();
            ArGrdComponentTypes = new HashSet<ArGrdComponentTypes>();
            SyAdvantageResourceRelationsRelatedResource = new HashSet<SyAdvantageResourceRelations>();
            SyAdvantageResourceRelationsResource = new HashSet<SyAdvantageResourceRelations>();
            SyNavigationNodes = new HashSet<SyNavigationNodes>();
            SyRlsResLvls = new HashSet<SyRlsResLvls>();
            SyRolesModules = new HashSet<SyRolesModules>();
            SyRptAdHocFields = new HashSet<SyRptAdHocFields>();
            SyRptParams = new HashSet<SyRptParams>();
            SyRptProps = new HashSet<SyRptProps>();
            SyUniversalSearchModules = new HashSet<SyUniversalSearchModules>();
            SyUserResources = new HashSet<SyUserResources>();
            SyWidgetResourceRoles = new HashSet<SyWidgetResourceRoles>();
            SyWidgetUserResourceSettings = new HashSet<SyWidgetUserResourceSettings>();
        }

        /*<summary>The get and set for ResourceId</summary>*/
        public short ResourceId { get; set; }
        /*<summary>The get and set for Resource</summary>*/
        public string Resource { get; set; }
        /*<summary>The get and set for ResourceTypeId</summary>*/
        public byte ResourceTypeId { get; set; }
        /*<summary>The get and set for ResourceUrl</summary>*/
        public string ResourceUrl { get; set; }
        /*<summary>The get and set for SummListId</summary>*/
        public short? SummListId { get; set; }
        /*<summary>The get and set for ChildTypeId</summary>*/
        public byte? ChildTypeId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for AllowSchlReqFlds</summary>*/
        public bool? AllowSchlReqFlds { get; set; }
        /*<summary>The get and set for MrutypeId</summary>*/
        public short? MrutypeId { get; set; }
        /*<summary>The get and set for UsedIn</summary>*/
        public int? UsedIn { get; set; }
        /*<summary>The get and set for TblFldsId</summary>*/
        public int? TblFldsId { get; set; }
        /*<summary>The get and set for DisplayName</summary>*/
        public string DisplayName { get; set; }

        /*<summary>The navigational property for ResourceType</summary>*/
        public virtual SyResourceTypes ResourceType { get; set; }
        /*<summary>The navigational property for ArFerpapage</summary>*/
        public virtual ICollection<ArFerpapage>ArFerpapage { get; set; }
        /*<summary>The navigational property for ArGrdComponentTypes</summary>*/
        public virtual ICollection<ArGrdComponentTypes>ArGrdComponentTypes { get; set; }
        /*<summary>The navigational property for SyAdvantageResourceRelationsRelatedResource</summary>*/
        public virtual ICollection<SyAdvantageResourceRelations>SyAdvantageResourceRelationsRelatedResource { get; set; }
        /*<summary>The navigational property for SyAdvantageResourceRelationsResource</summary>*/
        public virtual ICollection<SyAdvantageResourceRelations>SyAdvantageResourceRelationsResource { get; set; }
        /*<summary>The navigational property for SyNavigationNodes</summary>*/
        public virtual ICollection<SyNavigationNodes>SyNavigationNodes { get; set; }
        /*<summary>The navigational property for SyRlsResLvls</summary>*/
        public virtual ICollection<SyRlsResLvls>SyRlsResLvls { get; set; }
        /*<summary>The navigational property for SyRolesModules</summary>*/
        public virtual ICollection<SyRolesModules>SyRolesModules { get; set; }
        /*<summary>The navigational property for SyRptAdHocFields</summary>*/
        public virtual ICollection<SyRptAdHocFields>SyRptAdHocFields { get; set; }
        /*<summary>The navigational property for SyRptParams</summary>*/
        public virtual ICollection<SyRptParams>SyRptParams { get; set; }
        /*<summary>The navigational property for SyRptProps</summary>*/
        public virtual ICollection<SyRptProps>SyRptProps { get; set; }
        /*<summary>The navigational property for SyUniversalSearchModules</summary>*/
        public virtual ICollection<SyUniversalSearchModules>SyUniversalSearchModules { get; set; }
        /*<summary>The navigational property for SyUserResources</summary>*/
        public virtual ICollection<SyUserResources>SyUserResources { get; set; }
        /*<summary>The navigational property for SyWidgetResourceRoles</summary>*/
        public virtual ICollection<SyWidgetResourceRoles>SyWidgetResourceRoles { get; set; }
        /*<summary>The navigational property for SyWidgetUserResourceSettings</summary>*/
        public virtual ICollection<SyWidgetUserResourceSettings>SyWidgetUserResourceSettings { get; set; }
    }
}