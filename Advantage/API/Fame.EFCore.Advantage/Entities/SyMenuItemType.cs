﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyMenuItemType.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyMenuItemType definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyMenuItemType</summary>*/
    public partial class SyMenuItemType
    {
        /*<summary>The constructor for SyMenuItemType</summary>*/
        public SyMenuItemType()
        {
            SyMenuItems = new HashSet<SyMenuItems>();
        }

        /*<summary>The get and set for MenuItemTypeId</summary>*/
        public short MenuItemTypeId { get; set; }
        /*<summary>The get and set for MenuItemType</summary>*/
        public string MenuItemType { get; set; }

        /*<summary>The navigational property for SyMenuItems</summary>*/
        public virtual ICollection<SyMenuItems>SyMenuItems { get; set; }
    }
}