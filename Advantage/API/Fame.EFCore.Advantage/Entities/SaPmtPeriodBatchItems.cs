﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaPmtPeriodBatchItems.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaPmtPeriodBatchItems definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaPmtPeriodBatchItems</summary>*/
    public partial class SaPmtPeriodBatchItems
    {
        /*<summary>The get and set for PmtPeriodBatchItemId</summary>*/
        public int PmtPeriodBatchItemId { get; set; }
        /*<summary>The get and set for PmtPeriodBatchHeaderId</summary>*/
        public int PmtPeriodBatchHeaderId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for PmtPeriodId</summary>*/
        public Guid PmtPeriodId { get; set; }
        /*<summary>The get and set for CreditsHoursValue</summary>*/
        public decimal? CreditsHoursValue { get; set; }
        /*<summary>The get and set for ChargeAmount</summary>*/
        public decimal ChargeAmount { get; set; }
        /*<summary>The get and set for TransactionReference</summary>*/
        public string TransactionReference { get; set; }
        /*<summary>The get and set for TransactionDescription</summary>*/
        public string TransactionDescription { get; set; }
        /*<summary>The get and set for IncludedInPost</summary>*/
        public bool? IncludedInPost { get; set; }
        /*<summary>The get and set for TransactionId</summary>*/
        public Guid? TransactionId { get; set; }
        /*<summary>The get and set for TermId</summary>*/
        public Guid? TermId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for PmtPeriodBatchHeader</summary>*/
        public virtual SaPmtPeriodBatchHeaders PmtPeriodBatchHeader { get; set; }
    }
}