﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArResults.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArResults definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArResults</summary>*/
    public partial class ArResults
    {
        /*<summary>The get and set for ResultId</summary>*/
        public Guid ResultId { get; set; }
        /*<summary>The get and set for TestId</summary>*/
        public Guid? TestId { get; set; }
        /*<summary>The get and set for Score</summary>*/
        public decimal? Score { get; set; }
        /*<summary>The get and set for GrdSysDetailId</summary>*/
        public Guid? GrdSysDetailId { get; set; }
        /*<summary>The get and set for Cnt</summary>*/
        public int? Cnt { get; set; }
        /*<summary>The get and set for Hours</summary>*/
        public int? Hours { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for IsInComplete</summary>*/
        public bool? IsInComplete { get; set; }
        /*<summary>The get and set for DroppedInAddDrop</summary>*/
        public bool? DroppedInAddDrop { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for IsTransfered</summary>*/
        public bool? IsTransfered { get; set; }
        /*<summary>The get and set for IsClinicsSatisfied</summary>*/
        public bool? IsClinicsSatisfied { get; set; }
        /*<summary>The get and set for DateDetermined</summary>*/
        public DateTime? DateDetermined { get; set; }
        /*<summary>The get and set for IsCourseCompleted</summary>*/
        public bool IsCourseCompleted { get; set; }
        /*<summary>The get and set for IsGradeOverridden</summary>*/
        public bool IsGradeOverridden { get; set; }
        /*<summary>The get and set for GradeOverriddenBy</summary>*/
        public string GradeOverriddenBy { get; set; }
        /*<summary>The get and set for GradeOverriddenDate</summary>*/
        public DateTime? GradeOverriddenDate { get; set; }
        /*<summary>The get and set for DateCompleted</summary>*/
        public DateTime? DateCompleted { get; set; }

        /*<summary>The navigational property for GrdSysDetail</summary>*/
        public virtual ArGradeSystemDetails GrdSysDetail { get; set; }
        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
        /*<summary>The navigational property for Test</summary>*/
        public virtual ArClassSections Test { get; set; }
    }
}