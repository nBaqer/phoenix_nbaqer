﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="MsgTemplates.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The MsgTemplates definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for MsgTemplates</summary>*/
    public partial class MsgTemplates
    {
        /*<summary>The constructor for MsgTemplates</summary>*/
        public MsgTemplates()
        {
            MsgMessages = new HashSet<MsgMessages>();
            MsgRules = new HashSet<MsgRules>();
        }

        /*<summary>The get and set for TemplateId</summary>*/
        public Guid TemplateId { get; set; }
        /*<summary>The get and set for GroupId</summary>*/
        public Guid? GroupId { get; set; }
        /*<summary>The get and set for ModuleEntityId</summary>*/
        public short? ModuleEntityId { get; set; }
        /*<summary>The get and set for CampGroupId</summary>*/
        public Guid? CampGroupId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for Data</summary>*/
        public string Data { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public Guid? ModUser { get; set; }
        /*<summary>The get and set for Active</summary>*/
        public byte? Active { get; set; }

        /*<summary>The navigational property for CampGroup</summary>*/
        public virtual SyCampGrps CampGroup { get; set; }
        /*<summary>The navigational property for MsgMessages</summary>*/
        public virtual ICollection<MsgMessages>MsgMessages { get; set; }
        /*<summary>The navigational property for MsgRules</summary>*/
        public virtual ICollection<MsgRules>MsgRules { get; set; }
    }
}