﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ParamDetail.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ParamDetail definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ParamDetail</summary>*/
    public partial class ParamDetail
    {
        /*<summary>The get and set for DetailId</summary>*/
        public long DetailId { get; set; }
        /*<summary>The get and set for SectionId</summary>*/
        public long SectionId { get; set; }
        /*<summary>The get and set for ItemId</summary>*/
        public long ItemId { get; set; }
        /*<summary>The get and set for CaptionOverride</summary>*/
        public string CaptionOverride { get; set; }
        /*<summary>The get and set for ItemSeq</summary>*/
        public int ItemSeq { get; set; }
    }
}