﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRptPrefsIpedsSfapr.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRptPrefsIpedsSfapr definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRptPrefsIpedsSfapr</summary>*/
    public partial class SyRptPrefsIpedsSfapr
    {
        /*<summary>The get and set for RptPrefId</summary>*/
        public Guid RptPrefId { get; set; }
        /*<summary>The get and set for PrefId</summary>*/
        public Guid PrefId { get; set; }
        /*<summary>The get and set for FundSourceId</summary>*/
        public Guid FundSourceId { get; set; }
        /*<summary>The get and set for RptCatId</summary>*/
        public int RptCatId { get; set; }
    }
}