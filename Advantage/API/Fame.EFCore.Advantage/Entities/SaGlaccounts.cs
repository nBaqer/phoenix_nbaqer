﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaGlaccounts.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaGlaccounts definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaGlaccounts</summary>*/
    public partial class SaGlaccounts
    {
        /*<summary>The constructor for SaGlaccounts</summary>*/
        public SaGlaccounts()
        {
            SaGldistributions = new HashSet<SaGldistributions>();
        }

        /*<summary>The get and set for GlaccountId</summary>*/
        public Guid GlaccountId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for GlaccountCode</summary>*/
        public string GlaccountCode { get; set; }
        /*<summary>The get and set for GlaccountDescription</summary>*/
        public string GlaccountDescription { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SaGldistributions</summary>*/
        public virtual ICollection<SaGldistributions>SaGldistributions { get; set; }
    }
}