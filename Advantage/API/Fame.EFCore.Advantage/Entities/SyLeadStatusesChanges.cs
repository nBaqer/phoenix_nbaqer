﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyLeadStatusesChanges.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyLeadStatusesChanges definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyLeadStatusesChanges</summary>*/
    public partial class SyLeadStatusesChanges
    {
        /*<summary>The get and set for StatusChangeId</summary>*/
        public Guid StatusChangeId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for OrigStatusId</summary>*/
        public Guid? OrigStatusId { get; set; }
        /*<summary>The get and set for NewStatusId</summary>*/
        public Guid NewStatusId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for DateOfChange</summary>*/
        public DateTime? DateOfChange { get; set; }

        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
        /*<summary>The navigational property for NewStatus</summary>*/
        public virtual SyStatusCodes NewStatus { get; set; }
        /*<summary>The navigational property for OrigStatus</summary>*/
        public virtual SyStatusCodes OrigStatus { get; set; }
    }
}