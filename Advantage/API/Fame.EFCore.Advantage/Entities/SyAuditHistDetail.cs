﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyAuditHistDetail.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyAuditHistDetail definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyAuditHistDetail</summary>*/
    public partial class SyAuditHistDetail
    {
        /*<summary>The get and set for AuditHistDetailId</summary>*/
        public Guid AuditHistDetailId { get; set; }
        /*<summary>The get and set for AuditHistId</summary>*/
        public Guid AuditHistId { get; set; }
        /*<summary>The get and set for RowId</summary>*/
        public Guid RowId { get; set; }
        /*<summary>The get and set for ColumnName</summary>*/
        public string ColumnName { get; set; }
        /*<summary>The get and set for OldValue</summary>*/
        public string OldValue { get; set; }
        /*<summary>The get and set for NewValue</summary>*/
        public string NewValue { get; set; }

        /*<summary>The navigational property for AuditHist</summary>*/
        public virtual SyAuditHist AuditHist { get; set; }
    }
}