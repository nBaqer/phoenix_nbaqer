﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArOverridenConflicts.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArOverridenConflicts definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArOverridenConflicts</summary>*/
    public partial class ArOverridenConflicts
    {
        /*<summary>The get and set for OverridenConflictId</summary>*/
        public Guid OverridenConflictId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for LeftClsSectionId</summary>*/
        public Guid LeftClsSectionId { get; set; }
        /*<summary>The get and set for RightClsSectionId</summary>*/
        public Guid RightClsSectionId { get; set; }

        /*<summary>The navigational property for LeftClsSection</summary>*/
        public virtual ArClassSections LeftClsSection { get; set; }
        /*<summary>The navigational property for RightClsSection</summary>*/
        public virtual ArClassSections RightClsSection { get; set; }
    }
}