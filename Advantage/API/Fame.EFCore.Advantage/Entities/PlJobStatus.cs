﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlJobStatus.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlJobStatus definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlJobStatus</summary>*/
    public partial class PlJobStatus
    {
        /*<summary>The constructor for PlJobStatus</summary>*/
        public PlJobStatus()
        {
            AdLeadEmployment = new HashSet<AdLeadEmployment>();
            PlStudentsPlaced = new HashSet<PlStudentsPlaced>();
        }

        /*<summary>The get and set for JobStatusId</summary>*/
        public Guid JobStatusId { get; set; }
        /*<summary>The get and set for JobStatusDescrip</summary>*/
        public string JobStatusDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for JobCode</summary>*/
        public string JobCode { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeadEmployment</summary>*/
        public virtual ICollection<AdLeadEmployment>AdLeadEmployment { get; set; }
        /*<summary>The navigational property for PlStudentsPlaced</summary>*/
        public virtual ICollection<PlStudentsPlaced>PlStudentsPlaced { get; set; }
    }
}