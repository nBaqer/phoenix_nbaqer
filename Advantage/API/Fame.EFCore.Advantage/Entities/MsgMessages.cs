﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="MsgMessages.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The MsgMessages definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for MsgMessages</summary>*/
    public partial class MsgMessages
    {
        /*<summary>The get and set for MessageId</summary>*/
        public Guid MessageId { get; set; }
        /*<summary>The get and set for TemplateId</summary>*/
        public Guid? TemplateId { get; set; }
        /*<summary>The get and set for FromId</summary>*/
        public Guid? FromId { get; set; }
        /*<summary>The get and set for DeliveryType</summary>*/
        public string DeliveryType { get; set; }
        /*<summary>The get and set for RecipientType</summary>*/
        public string RecipientType { get; set; }
        /*<summary>The get and set for RecipientId</summary>*/
        public Guid? RecipientId { get; set; }
        /*<summary>The get and set for ReType</summary>*/
        public string ReType { get; set; }
        /*<summary>The get and set for ReId</summary>*/
        public Guid? ReId { get; set; }
        /*<summary>The get and set for ChangeId</summary>*/
        public Guid? ChangeId { get; set; }
        /*<summary>The get and set for MsgContent</summary>*/
        public string MsgContent { get; set; }
        /*<summary>The get and set for CreatedDate</summary>*/
        public DateTime? CreatedDate { get; set; }
        /*<summary>The get and set for DeliveryDate</summary>*/
        public DateTime? DeliveryDate { get; set; }
        /*<summary>The get and set for LastDeliveryAttempt</summary>*/
        public DateTime? LastDeliveryAttempt { get; set; }
        /*<summary>The get and set for LastDeliveryMsg</summary>*/
        public string LastDeliveryMsg { get; set; }
        /*<summary>The get and set for DateDelivered</summary>*/
        public DateTime? DateDelivered { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public Guid? ModUser { get; set; }

        /*<summary>The navigational property for Template</summary>*/
        public virtual MsgTemplates Template { get; set; }
    }
}