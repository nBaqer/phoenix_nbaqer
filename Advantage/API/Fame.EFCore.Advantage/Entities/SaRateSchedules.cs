﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaRateSchedules.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaRateSchedules definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaRateSchedules</summary>*/
    public partial class SaRateSchedules
    {
        /*<summary>The constructor for SaRateSchedules</summary>*/
        public SaRateSchedules()
        {
            ArPrgVersionFees = new HashSet<ArPrgVersionFees>();
            SaCourseFees = new HashSet<SaCourseFees>();
            SaPeriodicFees = new HashSet<SaPeriodicFees>();
            SaProgramVersionFees = new HashSet<SaProgramVersionFees>();
            SaRateScheduleDetails = new HashSet<SaRateScheduleDetails>();
        }

        /*<summary>The get and set for RateScheduleId</summary>*/
        public Guid RateScheduleId { get; set; }
        /*<summary>The get and set for RateScheduleCode</summary>*/
        public string RateScheduleCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for RateScheduleDescrip</summary>*/
        public string RateScheduleDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArPrgVersionFees</summary>*/
        public virtual ICollection<ArPrgVersionFees>ArPrgVersionFees { get; set; }
        /*<summary>The navigational property for SaCourseFees</summary>*/
        public virtual ICollection<SaCourseFees>SaCourseFees { get; set; }
        /*<summary>The navigational property for SaPeriodicFees</summary>*/
        public virtual ICollection<SaPeriodicFees>SaPeriodicFees { get; set; }
        /*<summary>The navigational property for SaProgramVersionFees</summary>*/
        public virtual ICollection<SaProgramVersionFees>SaProgramVersionFees { get; set; }
        /*<summary>The navigational property for SaRateScheduleDetails</summary>*/
        public virtual ICollection<SaRateScheduleDetails>SaRateScheduleDetails { get; set; }
    }
}