﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PriorWorkContact.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PriorWorkContact definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PriorWorkContact</summary>*/
    public partial class PriorWorkContact
    {
        /*<summary>The get and set for Id</summary>*/
        public int Id { get; set; }
        /*<summary>The get and set for StEmploymentId</summary>*/
        public Guid StEmploymentId { get; set; }
        /*<summary>The get and set for JobTitle</summary>*/
        public string JobTitle { get; set; }
        /*<summary>The get and set for FirstName</summary>*/
        public string FirstName { get; set; }
        /*<summary>The get and set for MiddleName</summary>*/
        public string MiddleName { get; set; }
        /*<summary>The get and set for LastName</summary>*/
        public string LastName { get; set; }
        /*<summary>The get and set for PrefixId</summary>*/
        public Guid? PrefixId { get; set; }
        /*<summary>The get and set for IsPhoneInternational</summary>*/
        public bool IsPhoneInternational { get; set; }
        /*<summary>The get and set for Phone</summary>*/
        public string Phone { get; set; }
        /*<summary>The get and set for Email</summary>*/
        public string Email { get; set; }
        /*<summary>The get and set for IsActive</summary>*/
        public bool IsActive { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for IsDefault</summary>*/
        public bool IsDefault { get; set; }

        /*<summary>The navigational property for Prefix</summary>*/
        public virtual SyPrefixes Prefix { get; set; }
        /*<summary>The navigational property for StEmployment</summary>*/
        public virtual AdLeadEmployment StEmployment { get; set; }
    }
}