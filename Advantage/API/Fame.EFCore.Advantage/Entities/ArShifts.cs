﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArShifts.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArShifts definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArShifts</summary>*/
    public partial class ArShifts
    {
        /*<summary>The constructor for ArShifts</summary>*/
        public ArShifts()
        {
            AdLeads = new HashSet<AdLeads>();
            ArClassSections = new HashSet<ArClassSections>();
            ArSdateSetup = new HashSet<ArSdateSetup>();
            ArStuEnrollments = new HashSet<ArStuEnrollments>();
            ArTerm = new HashSet<ArTerm>();
        }

        /*<summary>The get and set for ShiftId</summary>*/
        public Guid ShiftId { get; set; }
        /*<summary>The get and set for ShiftCode</summary>*/
        public string ShiftCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for ShiftDescrip</summary>*/
        public string ShiftDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for ArClassSections</summary>*/
        public virtual ICollection<ArClassSections>ArClassSections { get; set; }
        /*<summary>The navigational property for ArSdateSetup</summary>*/
        public virtual ICollection<ArSdateSetup>ArSdateSetup { get; set; }
        /*<summary>The navigational property for ArStuEnrollments</summary>*/
        public virtual ICollection<ArStuEnrollments>ArStuEnrollments { get; set; }
        /*<summary>The navigational property for ArTerm</summary>*/
        public virtual ICollection<ArTerm>ArTerm { get; set; }
    }
}