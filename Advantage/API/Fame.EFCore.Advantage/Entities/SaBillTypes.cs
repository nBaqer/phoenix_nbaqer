﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaBillTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaBillTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaBillTypes</summary>*/
    public partial class SaBillTypes
    {
        /*<summary>The constructor for SaBillTypes</summary>*/
        public SaBillTypes()
        {
            SaTransCodes = new HashSet<SaTransCodes>();
        }

        /*<summary>The get and set for BillTypeId</summary>*/
        public Guid BillTypeId { get; set; }
        /*<summary>The get and set for BillTypeDescrip</summary>*/
        public string BillTypeDescrip { get; set; }

        /*<summary>The navigational property for SaTransCodes</summary>*/
        public virtual ICollection<SaTransCodes>SaTransCodes { get; set; }
    }
}