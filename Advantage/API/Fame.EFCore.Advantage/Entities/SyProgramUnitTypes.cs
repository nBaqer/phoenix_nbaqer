﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyProgramUnitTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyProgramUnitTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyProgramUnitTypes</summary>*/
    public partial class SyProgramUnitTypes
    {
        /*<summary>The constructor for SyProgramUnitTypes</summary>*/
        public SyProgramUnitTypes()
        {
            SaProgramVersionFees = new HashSet<SaProgramVersionFees>();
        }

        /*<summary>The get and set for UnitId</summary>*/
        public short UnitId { get; set; }
        /*<summary>The get and set for UnitDescrip</summary>*/
        public string UnitDescrip { get; set; }

        /*<summary>The navigational property for SaProgramVersionFees</summary>*/
        public virtual ICollection<SaProgramVersionFees>SaProgramVersionFees { get; set; }
    }
}