﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdEntrTestOverRide.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdEntrTestOverRide definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdEntrTestOverRide</summary>*/
    public partial class AdEntrTestOverRide
    {
        /*<summary>The get and set for EntrTestOverRideId</summary>*/
        public Guid EntrTestOverRideId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid? LeadId { get; set; }
        /*<summary>The get and set for EntrTestId</summary>*/
        public Guid? EntrTestId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for OverRide</summary>*/
        public string OverRide { get; set; }
        /*<summary>The get and set for StudentId</summary>*/
        public Guid? StudentId { get; set; }

        /*<summary>The navigational property for EntrTest</summary>*/
        public virtual AdReqs EntrTest { get; set; }
        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
    }
}