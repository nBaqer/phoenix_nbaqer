﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyStuRestrictions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyStuRestrictions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyStuRestrictions</summary>*/
    public partial class SyStuRestrictions
    {
        /*<summary>The get and set for StuRestrictionId</summary>*/
        public Guid StuRestrictionId { get; set; }
        /*<summary>The get and set for SgroupId</summary>*/
        public Guid SgroupId { get; set; }
        /*<summary>The get and set for RestrictionTypeId</summary>*/
        public Guid RestrictionTypeId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for Reason</summary>*/
        public string Reason { get; set; }
        /*<summary>The get and set for DepartmentId</summary>*/
        public Guid DepartmentId { get; set; }
        /*<summary>The get and set for UserId</summary>*/
        public Guid UserId { get; set; }
        /*<summary>The get and set for CreateDate</summary>*/
        public DateTime CreateDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for Department</summary>*/
        public virtual SyDepartments Department { get; set; }
        /*<summary>The navigational property for RestrictionType</summary>*/
        public virtual SyStuRestrictionTypes RestrictionType { get; set; }
        /*<summary>The navigational property for User</summary>*/
        public virtual SyUsers User { get; set; }
    }
}