﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyReportTabs.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyReportTabs definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyReportTabs</summary>*/
    public partial class SyReportTabs
    {
        /*<summary>The get and set for ReportId</summary>*/
        public Guid ReportId { get; set; }
        /*<summary>The get and set for PageViewId</summary>*/
        public string PageViewId { get; set; }
        /*<summary>The get and set for UserControlName</summary>*/
        public string UserControlName { get; set; }
        /*<summary>The get and set for SetId</summary>*/
        public long SetId { get; set; }
        /*<summary>The get and set for ControllerClass</summary>*/
        public string ControllerClass { get; set; }
        /*<summary>The get and set for DisplayName</summary>*/
        public string DisplayName { get; set; }
        /*<summary>The get and set for ParameterSetLookUp</summary>*/
        public string ParameterSetLookUp { get; set; }
        /*<summary>The get and set for TabName</summary>*/
        public string TabName { get; set; }
    }
}