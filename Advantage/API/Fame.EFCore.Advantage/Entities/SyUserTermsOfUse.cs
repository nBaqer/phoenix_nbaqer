﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyUserTermsOfUse.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyUserTermsOfUse definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyUserTermsOfUse</summary>*/
    public partial class SyUserTermsOfUse
    {
        /*<summary>The get and set for UserTermsOfUseId</summary>*/
        public int UserTermsOfUseId { get; set; }
        /*<summary>The get and set for UserId</summary>*/
        public Guid UserId { get; set; }
        /*<summary>The get and set for Version</summary>*/
        public string Version { get; set; }
        /*<summary>The get and set for AcceptedDate</summary>*/
        public DateTime AcceptedDate { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
    }
}