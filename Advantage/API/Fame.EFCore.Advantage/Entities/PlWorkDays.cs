﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlWorkDays.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlWorkDays definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlWorkDays</summary>*/
    public partial class PlWorkDays
    {
        /*<summary>The constructor for PlWorkDays</summary>*/
        public PlWorkDays()
        {
            ArClsSectMeetings = new HashSet<ArClsSectMeetings>();
            SyPeriodsWorkDays = new HashSet<SyPeriodsWorkDays>();
        }

        /*<summary>The get and set for WorkDaysId</summary>*/
        public Guid WorkDaysId { get; set; }
        /*<summary>The get and set for WorkDaysDescrip</summary>*/
        public string WorkDaysDescrip { get; set; }
        /*<summary>The get and set for ViewOrder</summary>*/
        public int ViewOrder { get; set; }

        /*<summary>The navigational property for ArClsSectMeetings</summary>*/
        public virtual ICollection<ArClsSectMeetings>ArClsSectMeetings { get; set; }
        /*<summary>The navigational property for SyPeriodsWorkDays</summary>*/
        public virtual ICollection<SyPeriodsWorkDays>SyPeriodsWorkDays { get; set; }
    }
}