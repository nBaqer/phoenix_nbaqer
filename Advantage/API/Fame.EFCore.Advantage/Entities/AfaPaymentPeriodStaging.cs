﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AfaPaymentPeriodStaging.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AfaPaymentPeriodStaging definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AfaPaymentPeriodStaging</summary>*/
    public partial class AfaPaymentPeriodStaging
    {
        /*<summary>The get and set for Id</summary>*/
        public Guid Id { get; set; }
        /*<summary>The get and set for AfaStudentId</summary>*/
        public long AfaStudentId { get; set; }
        /*<summary>The get and set for StudentEnrollmentId</summary>*/
        public Guid StudentEnrollmentId { get; set; }
        /*<summary>The get and set for AcademicYearSequenceNum</summary>*/
        public int AcademicYearSequenceNum { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime StartDate { get; set; }
        /*<summary>The get and set for EndDate</summary>*/
        public DateTime EndDate { get; set; }
        /*<summary>The get and set for WeeksInstructionalTime</summary>*/
        public decimal WeeksInstructionalTime { get; set; }
        /*<summary>The get and set for CreditOrHours</summary>*/
        public decimal CreditOrHours { get; set; }
        /*<summary>The get and set for Status</summary>*/
        public string Status { get; set; }
        /*<summary>The get and set for CreditOrHoursEarned</summary>*/
        public decimal? CreditOrHoursEarned { get; set; }
        /*<summary>The get and set for CreditOrHoursEarnedEffectiveDate</summary>*/
        public DateTime? CreditOrHoursEarnedEffectiveDate { get; set; }
        /*<summary>The get and set for TitleIvsapresult</summary>*/
        public string TitleIvsapresult { get; set; }
        /*<summary>The get and set for DateCreated</summary>*/
        public DateTime DateCreated { get; set; }
        /*<summary>The get and set for UserCreated</summary>*/
        public string UserCreated { get; set; }
        /*<summary>The get and set for DeleteRecord</summary>*/
        public bool DeleteRecord { get; set; }

        /*<summary>The navigational property for StudentEnrollment</summary>*/
        public virtual ArStuEnrollments StudentEnrollment { get; set; }
    }
}