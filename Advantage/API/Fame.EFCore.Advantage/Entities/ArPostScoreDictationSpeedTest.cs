﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArPostScoreDictationSpeedTest.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArPostScoreDictationSpeedTest definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArPostScoreDictationSpeedTest</summary>*/
    public partial class ArPostScoreDictationSpeedTest
    {
        /*<summary>The get and set for Id</summary>*/
        public Guid Id { get; set; }
        /*<summary>The get and set for Stuenrollid</summary>*/
        public Guid? Stuenrollid { get; set; }
        /*<summary>The get and set for Termid</summary>*/
        public Guid? Termid { get; set; }
        /*<summary>The get and set for Datepassed</summary>*/
        public DateTime? Datepassed { get; set; }
        /*<summary>The get and set for Grdcomponenttypeid</summary>*/
        public Guid? Grdcomponenttypeid { get; set; }
        /*<summary>The get and set for Accuracy</summary>*/
        public decimal? Accuracy { get; set; }
        /*<summary>The get and set for Speed</summary>*/
        public decimal? Speed { get; set; }
        /*<summary>The get and set for GrdSysDetailId</summary>*/
        public Guid? GrdSysDetailId { get; set; }
        /*<summary>The get and set for InstructorId</summary>*/
        public Guid? InstructorId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Istestmentorproctored</summary>*/
        public string Istestmentorproctored { get; set; }
    }
}