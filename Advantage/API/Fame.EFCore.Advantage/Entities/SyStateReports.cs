﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyStateReports.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyStateReports definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyStateReports</summary>*/
    public partial class SyStateReports
    {
        /*<summary>The get and set for StateReportId</summary>*/
        public Guid StateReportId { get; set; }
        /*<summary>The get and set for StateId</summary>*/
        public Guid StateId { get; set; }
        /*<summary>The get and set for ReportId</summary>*/
        public Guid ReportId { get; set; }
        /*<summary>The get and set for ModifiedUser</summary>*/
        public string ModifiedUser { get; set; }
        /*<summary>The get and set for ModifiedDate</summary>*/
        public DateTime? ModifiedDate { get; set; }

        /*<summary>The navigational property for Report</summary>*/
        public virtual SyReports Report { get; set; }
        /*<summary>The navigational property for State</summary>*/
        public virtual SyStates State { get; set; }
    }
}