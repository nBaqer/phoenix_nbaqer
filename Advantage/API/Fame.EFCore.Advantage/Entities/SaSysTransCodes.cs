﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaSysTransCodes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaSysTransCodes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaSysTransCodes</summary>*/
    public partial class SaSysTransCodes
    {
        /*<summary>The constructor for SaSysTransCodes</summary>*/
        public SaSysTransCodes()
        {
            SaPrgVerDefaultChargePeriods = new HashSet<SaPrgVerDefaultChargePeriods>();
            SaTransCodes = new HashSet<SaTransCodes>();
        }

        /*<summary>The get and set for SysTransCodeId</summary>*/
        public int SysTransCodeId { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SaPrgVerDefaultChargePeriods</summary>*/
        public virtual ICollection<SaPrgVerDefaultChargePeriods>SaPrgVerDefaultChargePeriods { get; set; }
        /*<summary>The navigational property for SaTransCodes</summary>*/
        public virtual ICollection<SaTransCodes>SaTransCodes { get; set; }
    }
}