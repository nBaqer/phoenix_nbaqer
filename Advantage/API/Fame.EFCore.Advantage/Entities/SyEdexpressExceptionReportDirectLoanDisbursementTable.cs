﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyEdexpressExceptionReportDirectLoanDisbursementTable.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyEdexpressExceptionReportDirectLoanDisbursementTable definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyEdexpressExceptionReportDirectLoanDisbursementTable</summary>*/
    public partial class SyEdexpressExceptionReportDirectLoanDisbursementTable
    {
        /*<summary>The get and set for DetailId</summary>*/
        public Guid DetailId { get; set; }
        /*<summary>The get and set for ExceptionReportId</summary>*/
        public Guid ExceptionReportId { get; set; }
        /*<summary>The get and set for DbIn</summary>*/
        public string DbIn { get; set; }
        /*<summary>The get and set for Filter</summary>*/
        public string Filter { get; set; }
        /*<summary>The get and set for DisbDate</summary>*/
        public string DisbDate { get; set; }
        /*<summary>The get and set for DisbGrossAmount</summary>*/
        public string DisbGrossAmount { get; set; }
        /*<summary>The get and set for DisbIntRebAmount</summary>*/
        public string DisbIntRebAmount { get; set; }
        /*<summary>The get and set for DisbLoanFeeAmount</summary>*/
        public string DisbLoanFeeAmount { get; set; }
        /*<summary>The get and set for DisbNetAdjAmount</summary>*/
        public string DisbNetAdjAmount { get; set; }
        /*<summary>The get and set for DisbNetAmount</summary>*/
        public string DisbNetAmount { get; set; }
        /*<summary>The get and set for DisbNum</summary>*/
        public string DisbNum { get; set; }
        /*<summary>The get and set for DisbSeqNum</summary>*/
        public string DisbSeqNum { get; set; }
        /*<summary>The get and set for DisbStatusRelInd</summary>*/
        public string DisbStatusRelInd { get; set; }
        /*<summary>The get and set for DisbType</summary>*/
        public string DisbType { get; set; }
        /*<summary>The get and set for LoanId</summary>*/
        public string LoanId { get; set; }
        /*<summary>The get and set for OriginalSsn</summary>*/
        public string OriginalSsn { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ErrorMsg</summary>*/
        public string ErrorMsg { get; set; }
        /*<summary>The get and set for FileName</summary>*/
        public string FileName { get; set; }
        /*<summary>The get and set for ExceptionGuid</summary>*/
        public Guid? ExceptionGuid { get; set; }
    }
}