﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArBooks.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArBooks definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArBooks</summary>*/
    public partial class ArBooks
    {
        /*<summary>The constructor for ArBooks</summary>*/
        public ArBooks()
        {
            ArCourseBks = new HashSet<ArCourseBks>();
        }

        /*<summary>The get and set for BkId</summary>*/
        public Guid BkId { get; set; }
        /*<summary>The get and set for BkTitle</summary>*/
        public string BkTitle { get; set; }
        /*<summary>The get and set for BkAuthor</summary>*/
        public string BkAuthor { get; set; }
        /*<summary>The get and set for CategoryId</summary>*/
        public Guid CategoryId { get; set; }
        /*<summary>The get and set for Isbn</summary>*/
        public string Isbn { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Category</summary>*/
        public virtual ArBkCategories Category { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArCourseBks</summary>*/
        public virtual ICollection<ArCourseBks>ArCourseBks { get; set; }
    }
}