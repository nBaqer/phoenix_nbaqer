﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaGldistributions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaGldistributions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaGldistributions</summary>*/
    public partial class SaGldistributions
    {
        /*<summary>The get and set for GldistributionId</summary>*/
        public Guid GldistributionId { get; set; }
        /*<summary>The get and set for Gldate</summary>*/
        public DateTime Gldate { get; set; }
        /*<summary>The get and set for Glamount</summary>*/
        public decimal Glamount { get; set; }
        /*<summary>The get and set for GlaccountId</summary>*/
        public Guid GlaccountId { get; set; }
        /*<summary>The get and set for Gldescrip</summary>*/
        public string Gldescrip { get; set; }
        /*<summary>The get and set for ViewOrder</summary>*/
        public int ViewOrder { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for TransactionId</summary>*/
        public Guid TransactionId { get; set; }

        /*<summary>The navigational property for Glaccount</summary>*/
        public virtual SaGlaccounts Glaccount { get; set; }
        /*<summary>The navigational property for Transaction</summary>*/
        public virtual SaTransactions Transaction { get; set; }
    }
}