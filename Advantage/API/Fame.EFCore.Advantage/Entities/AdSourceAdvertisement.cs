﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdSourceAdvertisement.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdSourceAdvertisement definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdSourceAdvertisement</summary>*/
    public partial class AdSourceAdvertisement
    {
        /*<summary>The constructor for AdSourceAdvertisement</summary>*/
        public AdSourceAdvertisement()
        {
            AdLeads = new HashSet<AdLeads>();
        }

        /*<summary>The get and set for SourceAdvId</summary>*/
        public Guid SourceAdvId { get; set; }
        /*<summary>The get and set for SourceAdvDescrip</summary>*/
        public string SourceAdvDescrip { get; set; }
        /*<summary>The get and set for Startdate</summary>*/
        public DateTime Startdate { get; set; }
        /*<summary>The get and set for Enddate</summary>*/
        public DateTime Enddate { get; set; }
        /*<summary>The get and set for Cost</summary>*/
        public decimal Cost { get; set; }
        /*<summary>The get and set for SourceTypeId</summary>*/
        public Guid SourceTypeId { get; set; }
        /*<summary>The get and set for AdvIntervalId</summary>*/
        public Guid AdvIntervalId { get; set; }
        /*<summary>The get and set for SourceAdvCode</summary>*/
        public string SourceAdvCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }

        /*<summary>The navigational property for AdvInterval</summary>*/
        public virtual AdAdvInterval AdvInterval { get; set; }
        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for SourceType</summary>*/
        public virtual AdSourceType SourceType { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
    }
}