﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArGrdComponentTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArGrdComponentTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArGrdComponentTypes</summary>*/
    public partial class ArGrdComponentTypes
    {
        /*<summary>The constructor for ArGrdComponentTypes</summary>*/
        public ArGrdComponentTypes()
        {
            ArBridgeGradeComponentTypesCourses = new HashSet<ArBridgeGradeComponentTypesCourses>();
            ArExternshipAttendance = new HashSet<ArExternshipAttendance>();
            ArGrdBkWgtDetails = new HashSet<ArGrdBkWgtDetails>();
        }

        /*<summary>The get and set for GrdComponentTypeId</summary>*/
        public Guid GrdComponentTypeId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for SysComponentTypeId</summary>*/
        public short? SysComponentTypeId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SysComponentType</summary>*/
        public virtual SyResources SysComponentType { get; set; }
        /*<summary>The navigational property for ArBridgeGradeComponentTypesCourses</summary>*/
        public virtual ICollection<ArBridgeGradeComponentTypesCourses>ArBridgeGradeComponentTypesCourses { get; set; }
        /*<summary>The navigational property for ArExternshipAttendance</summary>*/
        public virtual ICollection<ArExternshipAttendance>ArExternshipAttendance { get; set; }
        /*<summary>The navigational property for ArGrdBkWgtDetails</summary>*/
        public virtual ICollection<ArGrdBkWgtDetails>ArGrdBkWgtDetails { get; set; }
    }
}