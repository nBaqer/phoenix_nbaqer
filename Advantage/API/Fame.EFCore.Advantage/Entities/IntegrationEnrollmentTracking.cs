﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="IntegrationEnrollmentTracking.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The IntegrationEnrollmentTracking definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for IntegrationEnrollmentTracking</summary>*/
    public partial class IntegrationEnrollmentTracking
    {
        /*<summary>The get and set for EnrollmentTrackingId</summary>*/
        public Guid EnrollmentTrackingId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for EnrollmentStatus</summary>*/
        public string EnrollmentStatus { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for Processed</summary>*/
        public bool Processed { get; set; }
        /*<summary>The get and set for RetryCount</summary>*/
        public int RetryCount { get; set; }

        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
    }
}