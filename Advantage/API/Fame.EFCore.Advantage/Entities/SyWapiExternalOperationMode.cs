﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyWapiExternalOperationMode.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyWapiExternalOperationMode definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyWapiExternalOperationMode</summary>*/
    public partial class SyWapiExternalOperationMode
    {
        /*<summary>The constructor for SyWapiExternalOperationMode</summary>*/
        public SyWapiExternalOperationMode()
        {
            SyWapiSettings = new HashSet<SyWapiSettings>();
        }

        /*<summary>The get and set for Id</summary>*/
        public int Id { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for IsActive</summary>*/
        public bool? IsActive { get; set; }

        /*<summary>The navigational property for SyWapiSettings</summary>*/
        public virtual ICollection<SyWapiSettings>SyWapiSettings { get; set; }
    }
}