﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyInstitutions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyInstitutions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyInstitutions</summary>*/
    public partial class SyInstitutions
    {
        /*<summary>The constructor for SyInstitutions</summary>*/
        public SyInstitutions()
        {
            AdLeads = new HashSet<AdLeads>();
            SyInstitutionAddresses = new HashSet<SyInstitutionAddresses>();
            SyInstitutionContacts = new HashSet<SyInstitutionContacts>();
            SyInstitutionPhone = new HashSet<SyInstitutionPhone>();
        }

        /*<summary>The get and set for Hsid</summary>*/
        public Guid Hsid { get; set; }
        /*<summary>The get and set for Hscode</summary>*/
        public string Hscode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for Hsname</summary>*/
        public string Hsname { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for LevelId</summary>*/
        public int? LevelId { get; set; }
        /*<summary>The get and set for TypeId</summary>*/
        public int? TypeId { get; set; }
        /*<summary>The get and set for ImportTypeId</summary>*/
        public int? ImportTypeId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for ImportType</summary>*/
        public virtual SyInstitutionImportTypes ImportType { get; set; }
        /*<summary>The navigational property for Level</summary>*/
        public virtual AdLevel Level { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for Type</summary>*/
        public virtual SyInstitutionTypes Type { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for SyInstitutionAddresses</summary>*/
        public virtual ICollection<SyInstitutionAddresses>SyInstitutionAddresses { get; set; }
        /*<summary>The navigational property for SyInstitutionContacts</summary>*/
        public virtual ICollection<SyInstitutionContacts>SyInstitutionContacts { get; set; }
        /*<summary>The navigational property for SyInstitutionPhone</summary>*/
        public virtual ICollection<SyInstitutionPhone>SyInstitutionPhone { get; set; }
    }
}