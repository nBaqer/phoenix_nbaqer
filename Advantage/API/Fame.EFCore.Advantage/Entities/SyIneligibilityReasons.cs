﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyIneligibilityReasons.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyIneligibilityReasons definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyIneligibilityReasons</summary>*/
    public partial class SyIneligibilityReasons
    {
        /*<summary>The constructor for SyIneligibilityReasons</summary>*/
        public SyIneligibilityReasons()
        {
            PlExitInterview = new HashSet<PlExitInterview>();
        }

        /*<summary>The get and set for InelReasonId</summary>*/
        public Guid InelReasonId { get; set; }
        /*<summary>The get and set for InelReasonName</summary>*/
        public string InelReasonName { get; set; }
        /*<summary>The get and set for InelReasonCode</summary>*/
        public string InelReasonCode { get; set; }

        /*<summary>The navigational property for PlExitInterview</summary>*/
        public virtual ICollection<PlExitInterview>PlExitInterview { get; set; }
    }
}