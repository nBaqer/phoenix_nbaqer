﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyEdexpNotPostPellAcgSmartTeachStudentTable.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyEdexpNotPostPellAcgSmartTeachStudentTable definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyEdexpNotPostPellAcgSmartTeachStudentTable</summary>*/
    public partial class SyEdexpNotPostPellAcgSmartTeachStudentTable
    {
        /*<summary>The get and set for ParentId</summary>*/
        public Guid ParentId { get; set; }
        /*<summary>The get and set for StudentAwardId</summary>*/
        public Guid? StudentAwardId { get; set; }
        /*<summary>The get and set for AwardTypeId</summary>*/
        public Guid? AwardTypeId { get; set; }
        /*<summary>The get and set for DbIn</summary>*/
        public string DbIn { get; set; }
        /*<summary>The get and set for Filter</summary>*/
        public string Filter { get; set; }
        /*<summary>The get and set for FirstName</summary>*/
        public string FirstName { get; set; }
        /*<summary>The get and set for LastName</summary>*/
        public string LastName { get; set; }
        /*<summary>The get and set for Ssn</summary>*/
        public string Ssn { get; set; }
        /*<summary>The get and set for CampusName</summary>*/
        public string CampusName { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid? CampusId { get; set; }
        /*<summary>The get and set for StuEnrollmentId</summary>*/
        public Guid? StuEnrollmentId { get; set; }
        /*<summary>The get and set for DbIndicator</summary>*/
        public string DbIndicator { get; set; }
        /*<summary>The get and set for AwardAmount</summary>*/
        public decimal? AwardAmount { get; set; }
        /*<summary>The get and set for AwardId</summary>*/
        public string AwardId { get; set; }
        /*<summary>The get and set for GrantType</summary>*/
        public string GrantType { get; set; }
        /*<summary>The get and set for AddDate</summary>*/
        public DateTime? AddDate { get; set; }
        /*<summary>The get and set for AddTime</summary>*/
        public string AddTime { get; set; }
        /*<summary>The get and set for OriginalSsn</summary>*/
        public string OriginalSsn { get; set; }
        /*<summary>The get and set for UpdateDate</summary>*/
        public string UpdateDate { get; set; }
        /*<summary>The get and set for UpdateTime</summary>*/
        public string UpdateTime { get; set; }
        /*<summary>The get and set for OriginationStatus</summary>*/
        public string OriginationStatus { get; set; }
        /*<summary>The get and set for AcademicYearId</summary>*/
        public Guid? AcademicYearId { get; set; }
        /*<summary>The get and set for AwardYearStartDate</summary>*/
        public DateTime? AwardYearStartDate { get; set; }
        /*<summary>The get and set for AwardYearEndDate</summary>*/
        public DateTime? AwardYearEndDate { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for FileName</summary>*/
        public string FileName { get; set; }
        /*<summary>The get and set for IsInSchool</summary>*/
        public bool? IsInSchool { get; set; }
    }
}