﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRptAdhocRelations.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRptAdhocRelations definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRptAdhocRelations</summary>*/
    public partial class SyRptAdhocRelations
    {
        /*<summary>The get and set for RelationId</summary>*/
        public int RelationId { get; set; }
        /*<summary>The get and set for FkTable</summary>*/
        public string FkTable { get; set; }
        /*<summary>The get and set for FkColumn</summary>*/
        public string FkColumn { get; set; }
        /*<summary>The get and set for PkTable</summary>*/
        public string PkTable { get; set; }
        /*<summary>The get and set for PkColumn</summary>*/
        public string PkColumn { get; set; }
    }
}