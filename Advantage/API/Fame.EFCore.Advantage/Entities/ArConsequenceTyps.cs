﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArConsequenceTyps.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArConsequenceTyps definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArConsequenceTyps</summary>*/
    public partial class ArConsequenceTyps
    {
        /*<summary>The get and set for ConsequenceTypId</summary>*/
        public byte ConsequenceTypId { get; set; }
        /*<summary>The get and set for ConseqTypDesc</summary>*/
        public string ConseqTypDesc { get; set; }
    }
}