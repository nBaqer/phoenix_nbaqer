﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyFameEspexceptionReport.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyFameEspexceptionReport definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyFameEspexceptionReport</summary>*/
    public partial class SyFameEspexceptionReport
    {
        /*<summary>The get and set for ExceptionReportId</summary>*/
        public Guid ExceptionReportId { get; set; }
        /*<summary>The get and set for Ssn</summary>*/
        public string Ssn { get; set; }
        /*<summary>The get and set for Faid</summary>*/
        public string Faid { get; set; }
        /*<summary>The get and set for Fund</summary>*/
        public string Fund { get; set; }
        /*<summary>The get and set for GrossAmount</summary>*/
        public decimal? GrossAmount { get; set; }
        /*<summary>The get and set for FileName</summary>*/
        public string FileName { get; set; }
        /*<summary>The get and set for Message</summary>*/
        public string Message { get; set; }
        /*<summary>The get and set for Moduser</summary>*/
        public string Moduser { get; set; }
        /*<summary>The get and set for Moddate</summary>*/
        public DateTime? Moddate { get; set; }
        /*<summary>The get and set for ExceptionGuid</summary>*/
        public Guid? ExceptionGuid { get; set; }
        /*<summary>The get and set for Success</summary>*/
        public int? Success { get; set; }
        /*<summary>The get and set for Failed</summary>*/
        public int? Failed { get; set; }
        /*<summary>The get and set for IsInitialImportSuccessful</summary>*/
        public bool? IsInitialImportSuccessful { get; set; }
        /*<summary>The get and set for IsEligibleForReprocess</summary>*/
        public bool? IsEligibleForReprocess { get; set; }
        /*<summary>The get and set for DisbursementDate</summary>*/
        public string DisbursementDate { get; set; }
        /*<summary>The get and set for Awardyear</summary>*/
        public string Awardyear { get; set; }
        /*<summary>The get and set for LoanFees</summary>*/
        public decimal? LoanFees { get; set; }
        /*<summary>The get and set for Loanstartdate</summary>*/
        public DateTime? Loanstartdate { get; set; }
        /*<summary>The get and set for Loanenddate</summary>*/
        public DateTime? Loanenddate { get; set; }
        /*<summary>The get and set for Hide</summary>*/
        public bool? Hide { get; set; }
        /*<summary>The get and set for RecordPosition</summary>*/
        public int? RecordPosition { get; set; }
    }
}