﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaFundSources.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaFundSources definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaFundSources</summary>*/
    public partial class SaFundSources
    {
        /*<summary>The constructor for SaFundSources</summary>*/
        public SaFundSources()
        {
            FaStudentAwards = new HashSet<FaStudentAwards>();
            SaBatchPayments = new HashSet<SaBatchPayments>();
            SaRefunds = new HashSet<SaRefunds>();
            SaTransactions = new HashSet<SaTransactions>();
            SyAwardTypes9010Mapping = new HashSet<SyAwardTypes9010Mapping>();
        }

        /*<summary>The get and set for FundSourceId</summary>*/
        public Guid FundSourceId { get; set; }
        /*<summary>The get and set for FundSourceCode</summary>*/
        public string FundSourceCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for FundSourceDescrip</summary>*/
        public string FundSourceDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for TitleIv</summary>*/
        public bool? TitleIv { get; set; }
        /*<summary>The get and set for AwardTypeId</summary>*/
        public int? AwardTypeId { get; set; }
        /*<summary>The get and set for AwardYear</summary>*/
        public string AwardYear { get; set; }
        /*<summary>The get and set for AdvFundSourceId</summary>*/
        public byte? AdvFundSourceId { get; set; }
        /*<summary>The get and set for CutoffDate</summary>*/
        public DateTime? CutoffDate { get; set; }
        /*<summary>The get and set for Ipedsvalue</summary>*/
        public int? Ipedsvalue { get; set; }

        /*<summary>The navigational property for AdvFundSource</summary>*/
        public virtual SyAdvFundSources AdvFundSource { get; set; }
        /*<summary>The navigational property for AwardType</summary>*/
        public virtual SaAwardTypes AwardType { get; set; }
        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for FaStudentAwards</summary>*/
        public virtual ICollection<FaStudentAwards>FaStudentAwards { get; set; }
        /*<summary>The navigational property for SaBatchPayments</summary>*/
        public virtual ICollection<SaBatchPayments>SaBatchPayments { get; set; }
        /*<summary>The navigational property for SaRefunds</summary>*/
        public virtual ICollection<SaRefunds>SaRefunds { get; set; }
        /*<summary>The navigational property for SaTransactions</summary>*/
        public virtual ICollection<SaTransactions>SaTransactions { get; set; }
        /*<summary>The navigational property for SyAwardTypes9010Mapping</summary>*/
        public virtual ICollection<SyAwardTypes9010Mapping>SyAwardTypes9010Mapping { get; set; }
    }
}