﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyEntities.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyEntities definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyEntities</summary>*/
    public partial class SyEntities
    {
        /*<summary>The constructor for SyEntities</summary>*/
        public SyEntities()
        {
            SyModuleEntities = new HashSet<SyModuleEntities>();
        }

        /*<summary>The get and set for EntityId</summary>*/
        public Guid EntityId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }

        /*<summary>The navigational property for SyModuleEntities</summary>*/
        public virtual ICollection<SyModuleEntities>SyModuleEntities { get; set; }
    }
}