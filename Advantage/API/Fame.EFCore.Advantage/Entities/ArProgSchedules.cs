﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArProgSchedules.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArProgSchedules definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArProgSchedules</summary>*/
    public partial class ArProgSchedules
    {
        /*<summary>The constructor for ArProgSchedules</summary>*/
        public ArProgSchedules()
        {
            AdLeads = new HashSet<AdLeads>();
            ArProgScheduleDetails = new HashSet<ArProgScheduleDetails>();
            ArStudentSchedules = new HashSet<ArStudentSchedules>();
        }

        /*<summary>The get and set for ScheduleId</summary>*/
        public Guid ScheduleId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for PrgVerId</summary>*/
        public Guid? PrgVerId { get; set; }
        /*<summary>The get and set for Active</summary>*/
        public bool? Active { get; set; }
        /*<summary>The get and set for UseFlexTime</summary>*/
        public bool? UseFlexTime { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for PrgVer</summary>*/
        public virtual ArPrgVersions PrgVer { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for ArProgScheduleDetails</summary>*/
        public virtual ICollection<ArProgScheduleDetails>ArProgScheduleDetails { get; set; }
        /*<summary>The navigational property for ArStudentSchedules</summary>*/
        public virtual ICollection<ArStudentSchedules>ArStudentSchedules { get; set; }
    }
}