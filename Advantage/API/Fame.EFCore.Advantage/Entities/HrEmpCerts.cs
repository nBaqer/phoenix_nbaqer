﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="HrEmpCerts.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The HrEmpCerts definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for HrEmpCerts</summary>*/
    public partial class HrEmpCerts
    {
        /*<summary>The get and set for EmpCertId</summary>*/
        public Guid EmpCertId { get; set; }
        /*<summary>The get and set for EmpId</summary>*/
        public Guid EmpId { get; set; }
        /*<summary>The get and set for CertId</summary>*/
        public Guid CertId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for Cert</summary>*/
        public virtual SyCertifications Cert { get; set; }
        /*<summary>The navigational property for Emp</summary>*/
        public virtual HrEmployees Emp { get; set; }
    }
}