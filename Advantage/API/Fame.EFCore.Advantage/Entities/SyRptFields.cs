﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRptFields.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRptFields definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRptFields</summary>*/
    public partial class SyRptFields
    {
        /*<summary>The constructor for SyRptFields</summary>*/
        public SyRptFields()
        {
            SyRptAgencyFields = new HashSet<SyRptAgencyFields>();
        }

        /*<summary>The get and set for RptFldId</summary>*/
        public int RptFldId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for Ddl</summary>*/
        public string Ddl { get; set; }
        /*<summary>The get and set for FldId</summary>*/
        public int? FldId { get; set; }

        /*<summary>The navigational property for Fld</summary>*/
        public virtual SyFields Fld { get; set; }
        /*<summary>The navigational property for SyRptAgencyFields</summary>*/
        public virtual ICollection<SyRptAgencyFields>SyRptAgencyFields { get; set; }
    }
}