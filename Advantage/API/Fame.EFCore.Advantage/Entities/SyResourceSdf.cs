﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyResourceSdf.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyResourceSdf definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyResourceSdf</summary>*/
    public partial class SyResourceSdf
    {
        /*<summary>The get and set for PkId</summary>*/
        public Guid PkId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public int? ResourceId { get; set; }
        /*<summary>The get and set for SdfId</summary>*/
        public Guid? SdfId { get; set; }
        /*<summary>The get and set for Sdfvisibilty</summary>*/
        public string Sdfvisibilty { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for EntityId</summary>*/
        public int? EntityId { get; set; }
        /*<summary>The get and set for Position</summary>*/
        public int Position { get; set; }

        /*<summary>The navigational property for Sdf</summary>*/
        public virtual SySdf Sdf { get; set; }
    }
}