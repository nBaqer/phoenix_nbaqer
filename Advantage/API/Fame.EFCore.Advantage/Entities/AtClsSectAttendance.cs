﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AtClsSectAttendance.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AtClsSectAttendance definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AtClsSectAttendance</summary>*/
    public partial class AtClsSectAttendance
    {
        /*<summary>The get and set for ClsSectAttId</summary>*/
        public Guid ClsSectAttId { get; set; }
        /*<summary>The get and set for StudentId</summary>*/
        public Guid? StudentId { get; set; }
        /*<summary>The get and set for ClsSectionId</summary>*/
        public Guid ClsSectionId { get; set; }
        /*<summary>The get and set for ClsSectMeetingId</summary>*/
        public Guid? ClsSectMeetingId { get; set; }
        /*<summary>The get and set for MeetDate</summary>*/
        public DateTime MeetDate { get; set; }
        /*<summary>The get and set for Actual</summary>*/
        public decimal Actual { get; set; }
        /*<summary>The get and set for Tardy</summary>*/
        public bool Tardy { get; set; }
        /*<summary>The get and set for Excused</summary>*/
        public bool Excused { get; set; }
        /*<summary>The get and set for Comments</summary>*/
        public string Comments { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid? StuEnrollId { get; set; }
        /*<summary>The get and set for Scheduled</summary>*/
        public decimal? Scheduled { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for ClsSectMeeting</summary>*/
        public virtual ArClsSectMeetings ClsSectMeeting { get; set; }
        /*<summary>The navigational property for ClsSection</summary>*/
        public virtual ArClassSections ClsSection { get; set; }
        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
    }
}