﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdImportLeadsFields.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdImportLeadsFields definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdImportLeadsFields</summary>*/
    public partial class AdImportLeadsFields
    {
        /*<summary>The constructor for AdImportLeadsFields</summary>*/
        public AdImportLeadsFields()
        {
            AdImportLeadsAdvFldMappings = new HashSet<AdImportLeadsAdvFldMappings>();
            AdImportLeadsLookupValsMap = new HashSet<AdImportLeadsLookupValsMap>();
        }

        /*<summary>The get and set for FldName</summary>*/
        public string FldName { get; set; }
        /*<summary>The get and set for Caption</summary>*/
        public string Caption { get; set; }
        /*<summary>The get and set for Ddlname</summary>*/
        public string Ddlname { get; set; }
        /*<summary>The get and set for IlfieldId</summary>*/
        public int IlfieldId { get; set; }

        /*<summary>The navigational property for AdImportLeadsAdvFldMappings</summary>*/
        public virtual ICollection<AdImportLeadsAdvFldMappings>AdImportLeadsAdvFldMappings { get; set; }
        /*<summary>The navigational property for AdImportLeadsLookupValsMap</summary>*/
        public virtual ICollection<AdImportLeadsLookupValsMap>AdImportLeadsLookupValsMap { get; set; }
    }
}