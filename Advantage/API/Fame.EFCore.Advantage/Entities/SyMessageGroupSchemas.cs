﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyMessageGroupSchemas.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyMessageGroupSchemas definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyMessageGroupSchemas</summary>*/
    public partial class SyMessageGroupSchemas
    {
        /*<summary>The get and set for MessageSchemaId</summary>*/
        public Guid MessageSchemaId { get; set; }
        /*<summary>The get and set for SpsData</summary>*/
        public string SpsData { get; set; }
        /*<summary>The get and set for XmlDefaultData</summary>*/
        public string XmlDefaultData { get; set; }
        /*<summary>The get and set for SqlStatement</summary>*/
        public string SqlStatement { get; set; }
        /*<summary>The get and set for HtmlXslt</summary>*/
        public string HtmlXslt { get; set; }

        /*<summary>The navigational property for MessageSchema</summary>*/
        public virtual SyMessageSchemas MessageSchema { get; set; }
    }
}