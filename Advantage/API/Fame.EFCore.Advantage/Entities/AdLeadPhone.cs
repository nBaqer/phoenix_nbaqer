﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadPhone.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadPhone definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadPhone</summary>*/
    public partial class AdLeadPhone
    {
        /*<summary>The get and set for LeadPhoneId</summary>*/
        public Guid LeadPhoneId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for PhoneTypeId</summary>*/
        public Guid PhoneTypeId { get; set; }
        /*<summary>The get and set for Phone</summary>*/
        public string Phone { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for Position</summary>*/
        public int Position { get; set; }
        /*<summary>The get and set for Extension</summary>*/
        public string Extension { get; set; }
        /*<summary>The get and set for IsForeignPhone</summary>*/
        public bool IsForeignPhone { get; set; }
        /*<summary>The get and set for IsBest</summary>*/
        public bool IsBest { get; set; }
        /*<summary>The get and set for IsShowOnLeadPage</summary>*/
        public bool IsShowOnLeadPage { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }

        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
        /*<summary>The navigational property for PhoneType</summary>*/
        public virtual SyPhoneType PhoneType { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}