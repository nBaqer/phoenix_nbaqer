﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArClsSectionTimeClockPolicy.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArClsSectionTimeClockPolicy definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArClsSectionTimeClockPolicy</summary>*/
    public partial class ArClsSectionTimeClockPolicy
    {
        /*<summary>The get and set for ClsSectionPolicyId</summary>*/
        public Guid ClsSectionPolicyId { get; set; }
        /*<summary>The get and set for ClsSectionId</summary>*/
        public Guid ClsSectionId { get; set; }
        /*<summary>The get and set for AllowEarlyIn</summary>*/
        public bool? AllowEarlyIn { get; set; }
        /*<summary>The get and set for AllowLateOut</summary>*/
        public bool? AllowLateOut { get; set; }
        /*<summary>The get and set for AllowExtraHours</summary>*/
        public bool? AllowExtraHours { get; set; }
        /*<summary>The get and set for CheckTardyIn</summary>*/
        public bool? CheckTardyIn { get; set; }
        /*<summary>The get and set for MaxInBeforeTardy</summary>*/
        public DateTime? MaxInBeforeTardy { get; set; }
        /*<summary>The get and set for AssignTardyInTime</summary>*/
        public DateTime? AssignTardyInTime { get; set; }

        /*<summary>The navigational property for ClsSection</summary>*/
        public virtual ArClassSections ClsSection { get; set; }
    }
}