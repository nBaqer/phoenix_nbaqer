﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyUserSpecQuestions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyUserSpecQuestions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyUserSpecQuestions</summary>*/
    public partial class SyUserSpecQuestions
    {
        /*<summary>The get and set for UserName</summary>*/
        public string UserName { get; set; }
        /*<summary>The get and set for UserQuestionId</summary>*/
        public Guid UserQuestionId { get; set; }
        /*<summary>The get and set for UserAnswer</summary>*/
        public string UserAnswer { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for UserSpecQuestionId</summary>*/
        public Guid UserSpecQuestionId { get; set; }

        /*<summary>The navigational property for UserQuestion</summary>*/
        public virtual SyUserQuestions UserQuestion { get; set; }
    }
}