﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyGrdPolicyParams.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyGrdPolicyParams definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyGrdPolicyParams</summary>*/
    public partial class SyGrdPolicyParams
    {
        /*<summary>The get and set for GrdPolParamId</summary>*/
        public Guid GrdPolParamId { get; set; }
        /*<summary>The get and set for GrdPolicyId</summary>*/
        public int GrdPolicyId { get; set; }
        /*<summary>The get and set for Param</summary>*/
        public byte Param { get; set; }
        /*<summary>The get and set for Seq</summary>*/
        public byte Seq { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for GrdPolicy</summary>*/
        public virtual SyGrdPolicies GrdPolicy { get; set; }
    }
}