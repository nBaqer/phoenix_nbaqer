﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySysStatus.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySysStatus definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySysStatus</summary>*/
    public partial class SySysStatus
    {
        /*<summary>The constructor for SySysStatus</summary>*/
        public SySysStatus()
        {
            SyStatusCodes = new HashSet<SyStatusCodes>();
            SySystemStatusWorkflowRulesStatusIdFromNavigation = new HashSet<SySystemStatusWorkflowRules>();
            SySystemStatusWorkflowRulesStatusIdToNavigation = new HashSet<SySystemStatusWorkflowRules>();
        }

        /*<summary>The get and set for SysStatusId</summary>*/
        public int SysStatusId { get; set; }
        /*<summary>The get and set for SysStatusDescrip</summary>*/
        public string SysStatusDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for StatusLevelId</summary>*/
        public int? StatusLevelId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for InSchool</summary>*/
        public int? InSchool { get; set; }
        /*<summary>The get and set for GeprogramStatus</summary>*/
        public string GeprogramStatus { get; set; }
        /*<summary>The get and set for PostAcademics</summary>*/
        public bool PostAcademics { get; set; }

        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for StatusLevel</summary>*/
        public virtual SyStatusLevels StatusLevel { get; set; }
        /*<summary>The navigational property for SyStatusCodes</summary>*/
        public virtual ICollection<SyStatusCodes>SyStatusCodes { get; set; }
        /*<summary>The navigational property for SySystemStatusWorkflowRulesStatusIdFromNavigation</summary>*/
        public virtual ICollection<SySystemStatusWorkflowRules>SySystemStatusWorkflowRulesStatusIdFromNavigation { get; set; }
        /*<summary>The navigational property for SySystemStatusWorkflowRulesStatusIdToNavigation</summary>*/
        public virtual ICollection<SySystemStatusWorkflowRules>SySystemStatusWorkflowRulesStatusIdToNavigation { get; set; }
    }
}