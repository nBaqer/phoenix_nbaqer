﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySdfDtypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySdfDtypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySdfDtypes</summary>*/
    public partial class SySdfDtypes
    {
        /*<summary>The get and set for DtypeId</summary>*/
        public byte DtypeId { get; set; }
        /*<summary>The get and set for Dtype</summary>*/
        public string Dtype { get; set; }
    }
}