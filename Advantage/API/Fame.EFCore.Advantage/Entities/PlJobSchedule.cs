﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlJobSchedule.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlJobSchedule definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlJobSchedule</summary>*/
    public partial class PlJobSchedule
    {
        /*<summary>The constructor for PlJobSchedule</summary>*/
        public PlJobSchedule()
        {
            PlEmployerJobs = new HashSet<PlEmployerJobs>();
            PlStudentsPlaced = new HashSet<PlStudentsPlaced>();
        }

        /*<summary>The get and set for JobScheduleId</summary>*/
        public Guid JobScheduleId { get; set; }
        /*<summary>The get and set for JobScheduleCode</summary>*/
        public string JobScheduleCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for JobScheduleDescrip</summary>*/
        public string JobScheduleDescrip { get; set; }
        /*<summary>The get and set for CamGrpId</summary>*/
        public Guid? CamGrpId { get; set; }

        /*<summary>The navigational property for CamGrp</summary>*/
        public virtual SyCampGrps CamGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for PlEmployerJobs</summary>*/
        public virtual ICollection<PlEmployerJobs>PlEmployerJobs { get; set; }
        /*<summary>The navigational property for PlStudentsPlaced</summary>*/
        public virtual ICollection<PlStudentsPlaced>PlStudentsPlaced { get; set; }
    }
}