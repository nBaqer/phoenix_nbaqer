﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyReqTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyReqTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyReqTypes</summary>*/
    public partial class SyReqTypes
    {
        /*<summary>The get and set for ReqTypeId</summary>*/
        public int ReqTypeId { get; set; }
        /*<summary>The get and set for ReqType</summary>*/
        public string ReqType { get; set; }
    }
}