﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRlsResLvls.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRlsResLvls definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRlsResLvls</summary>*/
    public partial class SyRlsResLvls
    {
        /*<summary>The get and set for Rrlid</summary>*/
        public Guid Rrlid { get; set; }
        /*<summary>The get and set for RoleId</summary>*/
        public Guid RoleId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public short ResourceId { get; set; }
        /*<summary>The get and set for AccessLevel</summary>*/
        public short AccessLevel { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ParentId</summary>*/
        public int? ParentId { get; set; }

        /*<summary>The navigational property for Resource</summary>*/
        public virtual SyResources Resource { get; set; }
        /*<summary>The navigational property for Role</summary>*/
        public virtual SyRoles Role { get; set; }
    }
}