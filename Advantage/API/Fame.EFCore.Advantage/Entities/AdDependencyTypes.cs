﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdDependencyTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdDependencyTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdDependencyTypes</summary>*/
    public partial class AdDependencyTypes
    {
        /*<summary>The constructor for AdDependencyTypes</summary>*/
        public AdDependencyTypes()
        {
            AdLeads = new HashSet<AdLeads>();
        }

        /*<summary>The get and set for DependencyTypeId</summary>*/
        public Guid DependencyTypeId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
    }
}