﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="TmTaskResults.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The TmTaskResults definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for TmTaskResults</summary>*/
    public partial class TmTaskResults
    {
        /*<summary>The get and set for TaskResultId</summary>*/
        public Guid TaskResultId { get; set; }
        /*<summary>The get and set for TaskId</summary>*/
        public Guid? TaskId { get; set; }
        /*<summary>The get and set for ResultId</summary>*/
        public Guid? ResultId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public Guid? ModUser { get; set; }

        /*<summary>The navigational property for Result</summary>*/
        public virtual TmResults Result { get; set; }
        /*<summary>The navigational property for Task</summary>*/
        public virtual TmTasks Task { get; set; }
    }
}