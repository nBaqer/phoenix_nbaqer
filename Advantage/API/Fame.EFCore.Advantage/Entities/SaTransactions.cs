﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaTransactions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaTransactions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaTransactions</summary>*/
    public partial class SaTransactions
    {
        /*<summary>The constructor for SaTransactions</summary>*/
        public SaTransactions()
        {
            SaDeferredRevenues = new HashSet<SaDeferredRevenues>();
            SaGldistributions = new HashSet<SaGldistributions>();
        }

        /*<summary>The get and set for TransactionId</summary>*/
        public Guid TransactionId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for TermId</summary>*/
        public Guid? TermId { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid? CampusId { get; set; }
        /*<summary>The get and set for TransDate</summary>*/
        public DateTime TransDate { get; set; }
        /*<summary>The get and set for TransCodeId</summary>*/
        public Guid? TransCodeId { get; set; }
        /*<summary>The get and set for TransReference</summary>*/
        public string TransReference { get; set; }
        /*<summary>The get and set for AcademicYearId</summary>*/
        public Guid? AcademicYearId { get; set; }
        /*<summary>The get and set for TransDescrip</summary>*/
        public string TransDescrip { get; set; }
        /*<summary>The get and set for TransAmount</summary>*/
        public decimal TransAmount { get; set; }
        /*<summary>The get and set for TransTypeId</summary>*/
        public int TransTypeId { get; set; }
        /*<summary>The get and set for IsPosted</summary>*/
        public bool IsPosted { get; set; }
        /*<summary>The get and set for CreateDate</summary>*/
        public DateTime? CreateDate { get; set; }
        /*<summary>The get and set for BatchPaymentId</summary>*/
        public Guid? BatchPaymentId { get; set; }
        /*<summary>The get and set for ViewOrder</summary>*/
        public int? ViewOrder { get; set; }
        /*<summary>The get and set for IsAutomatic</summary>*/
        public bool IsAutomatic { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Voided</summary>*/
        public bool Voided { get; set; }
        /*<summary>The get and set for FeeLevelId</summary>*/
        public byte? FeeLevelId { get; set; }
        /*<summary>The get and set for FeeId</summary>*/
        public Guid? FeeId { get; set; }
        /*<summary>The get and set for PaymentCodeId</summary>*/
        public Guid? PaymentCodeId { get; set; }
        /*<summary>The get and set for FundSourceId</summary>*/
        public Guid? FundSourceId { get; set; }
        /*<summary>The get and set for StuEnrollPayPeriodId</summary>*/
        public Guid? StuEnrollPayPeriodId { get; set; }
        /*<summary>The get and set for DisplaySequence</summary>*/
        public int? DisplaySequence { get; set; }
        /*<summary>The get and set for SecondDisplaySequence</summary>*/
        public int? SecondDisplaySequence { get; set; }
        /*<summary>The get and set for PmtPeriodId</summary>*/
        public Guid? PmtPeriodId { get; set; }
        /*<summary>The get and set for PrgChrPeriodSeqId</summary>*/
        public Guid? PrgChrPeriodSeqId { get; set; }
        /*<summary>The get and set for PaymentPeriodNumber</summary>*/
        public short? PaymentPeriodNumber { get; set; }

        /*<summary>The navigational property for AcademicYear</summary>*/
        public virtual SaAcademicYears AcademicYear { get; set; }
        /*<summary>The navigational property for BatchPayment</summary>*/
        public virtual SaBatchPayments BatchPayment { get; set; }
        /*<summary>The navigational property for FundSource</summary>*/
        public virtual SaFundSources FundSource { get; set; }
        /*<summary>The navigational property for PaymentCode</summary>*/
        public virtual SaTransCodes PaymentCode { get; set; }
        /*<summary>The navigational property for PmtPeriod</summary>*/
        public virtual SaPmtPeriods PmtPeriod { get; set; }
        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
        /*<summary>The navigational property for StuEnrollPayPeriod</summary>*/
        public virtual ArPrgChargePeriodSeq StuEnrollPayPeriod { get; set; }
        /*<summary>The navigational property for Term</summary>*/
        public virtual ArTerm Term { get; set; }
        /*<summary>The navigational property for TransCode</summary>*/
        public virtual SaTransCodes TransCode { get; set; }
        /*<summary>The navigational property for TransType</summary>*/
        public virtual SaTransTypes TransType { get; set; }
        /*<summary>The navigational property for SaPayments</summary>*/
        public virtual SaPayments SaPayments { get; set; }
        /*<summary>The navigational property for SaRefunds</summary>*/
        public virtual SaRefunds SaRefunds { get; set; }
        /*<summary>The navigational property for SaDeferredRevenues</summary>*/
        public virtual ICollection<SaDeferredRevenues>SaDeferredRevenues { get; set; }
        /*<summary>The navigational property for SaGldistributions</summary>*/
        public virtual ICollection<SaGldistributions>SaGldistributions { get; set; }
    }
}