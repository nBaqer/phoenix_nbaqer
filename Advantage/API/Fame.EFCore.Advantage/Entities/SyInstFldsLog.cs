﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyInstFldsLog.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyInstFldsLog definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyInstFldsLog</summary>*/
    public partial class SyInstFldsLog
    {
        /*<summary>The get and set for InstId</summary>*/
        public int InstId { get; set; }
        /*<summary>The get and set for FldId</summary>*/
        public int FldId { get; set; }
    }
}