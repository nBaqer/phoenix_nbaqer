﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyAdvBuild.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyAdvBuild definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyAdvBuild</summary>*/
    public partial class SyAdvBuild
    {
        /*<summary>The get and set for BuildNumber</summary>*/
        public string BuildNumber { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for AdvantageBuildId</summary>*/
        public Guid AdvantageBuildId { get; set; }
    }
}