﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyReportDescrip.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyReportDescrip definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyReportDescrip</summary>*/
    public partial class SyReportDescrip
    {
        /*<summary>The get and set for ReportId</summary>*/
        public Guid ReportId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public int? ResourceId { get; set; }
        /*<summary>The get and set for ActualRptName</summary>*/
        public string ActualRptName { get; set; }
        /*<summary>The get and set for FriendlyRptName</summary>*/
        public string FriendlyRptName { get; set; }
        /*<summary>The get and set for RptDescrip</summary>*/
        public string RptDescrip { get; set; }
        /*<summary>The get and set for ReportDescriptionId</summary>*/
        public Guid ReportDescriptionId { get; set; }
    }
}