﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaTuitionEarnings.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaTuitionEarnings definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaTuitionEarnings</summary>*/
    public partial class SaTuitionEarnings
    {
        /*<summary>The constructor for SaTuitionEarnings</summary>*/
        public SaTuitionEarnings()
        {
            ArPrgVersions = new HashSet<ArPrgVersions>();
            SaTuitionEarningsPercentageRanges = new HashSet<SaTuitionEarningsPercentageRanges>();
        }

        /*<summary>The get and set for TuitionEarningId</summary>*/
        public Guid TuitionEarningId { get; set; }
        /*<summary>The get and set for TuitionEarningsCode</summary>*/
        public string TuitionEarningsCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for TuitionEarningsDescrip</summary>*/
        public string TuitionEarningsDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for RevenueBasisIdx</summary>*/
        public byte RevenueBasisIdx { get; set; }
        /*<summary>The get and set for PercentageRangeBasisIdx</summary>*/
        public byte PercentageRangeBasisIdx { get; set; }
        /*<summary>The get and set for PercentToEarnIdx</summary>*/
        public byte PercentToEarnIdx { get; set; }
        /*<summary>The get and set for FullMonthBeforeDay</summary>*/
        public byte? FullMonthBeforeDay { get; set; }
        /*<summary>The get and set for HalfMonthBeforeDay</summary>*/
        public byte? HalfMonthBeforeDay { get; set; }
        /*<summary>The get and set for IsAttendanceRequired</summary>*/
        public bool? IsAttendanceRequired { get; set; }
        /*<summary>The get and set for RequiredAttendancePercent</summary>*/
        public byte? RequiredAttendancePercent { get; set; }
        /*<summary>The get and set for RequiredCumulativeHoursAttended</summary>*/
        public short? RequiredCumulativeHoursAttended { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for TakeHolidays</summary>*/
        public bool? TakeHolidays { get; set; }
        /*<summary>The get and set for RemainingMethod</summary>*/
        public bool? RemainingMethod { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArPrgVersions</summary>*/
        public virtual ICollection<ArPrgVersions>ArPrgVersions { get; set; }
        /*<summary>The navigational property for SaTuitionEarningsPercentageRanges</summary>*/
        public virtual ICollection<SaTuitionEarningsPercentageRanges>SaTuitionEarningsPercentageRanges { get; set; }
    }
}