﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArOverridenPreReqs.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArOverridenPreReqs definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArOverridenPreReqs</summary>*/
    public partial class ArOverridenPreReqs
    {
        /*<summary>The get and set for OverridenPreReqId</summary>*/
        public Guid OverridenPreReqId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for ReqId</summary>*/
        public Guid ReqId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for Req</summary>*/
        public virtual ArReqs Req { get; set; }
        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
    }
}