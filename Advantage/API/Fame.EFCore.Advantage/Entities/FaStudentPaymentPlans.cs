﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="FaStudentPaymentPlans.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The FaStudentPaymentPlans definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for FaStudentPaymentPlans</summary>*/
    public partial class FaStudentPaymentPlans
    {
        /*<summary>The constructor for FaStudentPaymentPlans</summary>*/
        public FaStudentPaymentPlans()
        {
            FaStuPaymentPlanSchedule = new HashSet<FaStuPaymentPlanSchedule>();
        }

        /*<summary>The get and set for PaymentPlanId</summary>*/
        public Guid PaymentPlanId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for PayPlanStartDate</summary>*/
        public DateTime PayPlanStartDate { get; set; }
        /*<summary>The get and set for PayPlanEndDate</summary>*/
        public DateTime? PayPlanEndDate { get; set; }
        /*<summary>The get and set for AcademicYearId</summary>*/
        public Guid? AcademicYearId { get; set; }
        /*<summary>The get and set for TotalAmountDue</summary>*/
        public decimal TotalAmountDue { get; set; }
        /*<summary>The get and set for PayPlanDescrip</summary>*/
        public string PayPlanDescrip { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Disbursements</summary>*/
        public int? Disbursements { get; set; }
        /*<summary>The get and set for AwardId</summary>*/
        public int? AwardId { get; set; }

        /*<summary>The navigational property for AcademicYear</summary>*/
        public virtual SaAcademicYears AcademicYear { get; set; }
        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
        /*<summary>The navigational property for FaStuPaymentPlanSchedule</summary>*/
        public virtual ICollection<FaStuPaymentPlanSchedule>FaStuPaymentPlanSchedule { get; set; }
    }
}