﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdStudentGroups.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdStudentGroups definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdStudentGroups</summary>*/
    public partial class AdStudentGroups
    {
        /*<summary>The get and set for StuGrpId</summary>*/
        public Guid StuGrpId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for OwnerId</summary>*/
        public Guid OwnerId { get; set; }
        /*<summary>The get and set for TypeId</summary>*/
        public Guid TypeId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for IsPublic</summary>*/
        public bool IsPublic { get; set; }
        /*<summary>The get and set for IsRegHold</summary>*/
        public bool IsRegHold { get; set; }
        /*<summary>The get and set for IsTransHold</summary>*/
        public bool IsTransHold { get; set; }
    }
}