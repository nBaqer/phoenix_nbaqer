﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArProbWarningTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArProbWarningTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArProbWarningTypes</summary>*/
    public partial class ArProbWarningTypes
    {
        /*<summary>The constructor for ArProbWarningTypes</summary>*/
        public ArProbWarningTypes()
        {
            ArStuProbWarnings = new HashSet<ArStuProbWarnings>();
        }

        /*<summary>The get and set for ProbWarningTypeId</summary>*/
        public int ProbWarningTypeId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for ArStuProbWarnings</summary>*/
        public virtual ICollection<ArStuProbWarnings>ArStuProbWarnings { get; set; }
    }
}