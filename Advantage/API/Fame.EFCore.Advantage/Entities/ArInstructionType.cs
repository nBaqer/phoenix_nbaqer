﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArInstructionType.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArInstructionType definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArInstructionType</summary>*/
    public partial class ArInstructionType
    {
        /*<summary>The constructor for ArInstructionType</summary>*/
        public ArInstructionType()
        {
            ArClsSectMeetings = new HashSet<ArClsSectMeetings>();
            ArPrgVerInstructionType = new HashSet<ArPrgVerInstructionType>();
            ArSapquantByInstruction = new HashSet<ArSapquantByInstruction>();
            ArSapquantResults = new HashSet<ArSapquantResults>();
            ArTimeClockSpecialCode = new HashSet<ArTimeClockSpecialCode>();
        }

        /*<summary>The get and set for InstructionTypeId</summary>*/
        public Guid InstructionTypeId { get; set; }
        /*<summary>The get and set for InstructionTypeCode</summary>*/
        public string InstructionTypeCode { get; set; }
        /*<summary>The get and set for InstructionTypeDescrip</summary>*/
        public string InstructionTypeDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for IsDefault</summary>*/
        public bool? IsDefault { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for ArClsSectMeetings</summary>*/
        public virtual ICollection<ArClsSectMeetings>ArClsSectMeetings { get; set; }
        /*<summary>The navigational property for ArPrgVerInstructionType</summary>*/
        public virtual ICollection<ArPrgVerInstructionType>ArPrgVerInstructionType { get; set; }
        /*<summary>The navigational property for ArSapquantByInstruction</summary>*/
        public virtual ICollection<ArSapquantByInstruction>ArSapquantByInstruction { get; set; }
        /*<summary>The navigational property for ArSapquantResults</summary>*/
        public virtual ICollection<ArSapquantResults>ArSapquantResults { get; set; }
        /*<summary>The navigational property for ArTimeClockSpecialCode</summary>*/
        public virtual ICollection<ArTimeClockSpecialCode>ArTimeClockSpecialCode { get; set; }
    }
}