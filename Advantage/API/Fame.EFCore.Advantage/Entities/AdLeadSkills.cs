﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadSkills.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadSkills definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadSkills</summary>*/
    public partial class AdLeadSkills
    {
        /*<summary>The get and set for LeadSkillId</summary>*/
        public Guid LeadSkillId { get; set; }
        /*<summary>The get and set for SkillId</summary>*/
        public Guid? SkillId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid? LeadId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
        /*<summary>The navigational property for Skill</summary>*/
        public virtual PlSkills Skill { get; set; }
    }
}