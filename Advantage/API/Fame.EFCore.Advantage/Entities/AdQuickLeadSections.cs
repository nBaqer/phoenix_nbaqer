﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdQuickLeadSections.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdQuickLeadSections definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdQuickLeadSections</summary>*/
    public partial class AdQuickLeadSections
    {
        /*<summary>The get and set for SectionId</summary>*/
        public int SectionId { get; set; }
        /*<summary>The get and set for SectionName</summary>*/
        public string SectionName { get; set; }
    }
}