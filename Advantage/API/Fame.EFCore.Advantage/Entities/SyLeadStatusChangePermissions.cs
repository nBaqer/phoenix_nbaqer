﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyLeadStatusChangePermissions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyLeadStatusChangePermissions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyLeadStatusChangePermissions</summary>*/
    public partial class SyLeadStatusChangePermissions
    {
        /*<summary>The get and set for LeadStatusChangePermissionId</summary>*/
        public Guid LeadStatusChangePermissionId { get; set; }
        /*<summary>The get and set for LeadStatusChangeId</summary>*/
        public Guid LeadStatusChangeId { get; set; }
        /*<summary>The get and set for RoleId</summary>*/
        public Guid RoleId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for LeadStatusChange</summary>*/
        public virtual SyLeadStatusChanges LeadStatusChange { get; set; }
        /*<summary>The navigational property for Role</summary>*/
        public virtual SyRoles Role { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}