﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyAwardTypes9010.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyAwardTypes9010 definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyAwardTypes9010</summary>*/
    public partial class SyAwardTypes9010
    {
        /*<summary>The get and set for AwardType9010Id</summary>*/
        public Guid AwardType9010Id { get; set; }
        /*<summary>The get and set for Name</summary>*/
        public string Name { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for DisplayOrder</summary>*/
        public int? DisplayOrder { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
    }
}