﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdVendorCampaign.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdVendorCampaign definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdVendorCampaign</summary>*/
    public partial class AdVendorCampaign
    {
        /*<summary>The constructor for AdVendorCampaign</summary>*/
        public AdVendorCampaign()
        {
            AdLeads = new HashSet<AdLeads>();
        }

        /*<summary>The get and set for CampaignId</summary>*/
        public int CampaignId { get; set; }
        /*<summary>The get and set for VendorId</summary>*/
        public int VendorId { get; set; }
        /*<summary>The get and set for CampaignCode</summary>*/
        public string CampaignCode { get; set; }
        /*<summary>The get and set for AccountId</summary>*/
        public string AccountId { get; set; }
        /*<summary>The get and set for IsActive</summary>*/
        public bool? IsActive { get; set; }
        /*<summary>The get and set for DateCampaignBegin</summary>*/
        public DateTime DateCampaignBegin { get; set; }
        /*<summary>The get and set for DateCampaignEnd</summary>*/
        public DateTime DateCampaignEnd { get; set; }
        /*<summary>The get and set for Cost</summary>*/
        public double Cost { get; set; }
        /*<summary>The get and set for PayForId</summary>*/
        public int PayForId { get; set; }
        /*<summary>The get and set for FilterRejectByCounty</summary>*/
        public bool FilterRejectByCounty { get; set; }
        /*<summary>The get and set for FilterRejectDuplicates</summary>*/
        public bool FilterRejectDuplicates { get; set; }
        /*<summary>The get and set for IsDeleted</summary>*/
        public bool IsDeleted { get; set; }

        /*<summary>The navigational property for PayFor</summary>*/
        public virtual AdVendorPayFor PayFor { get; set; }
        /*<summary>The navigational property for Vendor</summary>*/
        public virtual AdVendors Vendor { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
    }
}