﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySuffixes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySuffixes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySuffixes</summary>*/
    public partial class SySuffixes
    {
        /*<summary>The constructor for SySuffixes</summary>*/
        public SySuffixes()
        {
            AdLeadOtherContacts = new HashSet<AdLeadOtherContacts>();
            AdLeads = new HashSet<AdLeads>();
            HrEmployees = new HashSet<HrEmployees>();
            PlEmployerContact = new HashSet<PlEmployerContact>();
            SyInstitutionContacts = new HashSet<SyInstitutionContacts>();
        }

        /*<summary>The get and set for SuffixId</summary>*/
        public Guid SuffixId { get; set; }
        /*<summary>The get and set for SuffixCode</summary>*/
        public string SuffixCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for SuffixDescrip</summary>*/
        public string SuffixDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeadOtherContacts</summary>*/
        public virtual ICollection<AdLeadOtherContacts>AdLeadOtherContacts { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for HrEmployees</summary>*/
        public virtual ICollection<HrEmployees>HrEmployees { get; set; }
        /*<summary>The navigational property for PlEmployerContact</summary>*/
        public virtual ICollection<PlEmployerContact>PlEmployerContact { get; set; }
        /*<summary>The navigational property for SyInstitutionContacts</summary>*/
        public virtual ICollection<SyInstitutionContacts>SyInstitutionContacts { get; set; }
    }
}