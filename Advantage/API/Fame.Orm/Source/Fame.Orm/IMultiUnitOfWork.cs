﻿namespace Fame.Orm
{
    public interface IMultiUnitOfWork : IUnitOfWork
    {
        IUnitOfWork GetUnitOfWork<TOrm>()
            where TOrm : IFameOrm;

        TRepository GetRegisteredRepository<TOrm, TRepository>()
            where TRepository : class
            where TOrm : IFameOrm;

        IRepository<TEntity> GetRepositoryForEntity<TOrm, TEntity>()
            where TEntity : class, IEntity, new()
            where TOrm : IFameOrm;
    }
}
