﻿using System;

namespace Fame.Orm.Helpers
{
    /// <summary>
    /// A class implementing IKeyProps will contain the key properies from the specified entity.
    /// Used to specify composite keys for use in ORM auto mapping and Get() calls
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IKeyProps<TEntity> where TEntity : class
    {
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class KeyPropertyAttribute : Attribute { }

}
