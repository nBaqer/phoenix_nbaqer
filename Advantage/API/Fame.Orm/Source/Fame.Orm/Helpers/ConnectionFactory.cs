﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using StackExchange.Profiling;
using StackExchange.Profiling.Data;

namespace Fame.Orm.Helpers
{
    /// <summary>
    /// Creates <see cref="IDbConnection"/> instances from a specified connectionstring.
    /// </summary>
    public class ConnectionFactory : IConnectionFactory
    {
        private const string DefaultProvider = "System.Data.SqlClient";

        private readonly DbProviderFactory _provider;
        private readonly string _connectionString;
        private readonly bool _useMiniProfiler;

        /// <summary>
        /// Creates a new ConnectionFactory instance
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="providerName">Provider </param>
        /// <param name="useMiniProfiler">Enables MiniProfiler.  ProfiledDbConnection will be used instead of DbConnection</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="connectionString"/> or <paramref name="providerName"/> is null.</exception>
        public ConnectionFactory(string connectionString, string providerName, bool? useMiniProfiler)
        {
            _connectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
            _provider = DbProviderFactories.GetFactory(providerName);
            _useMiniProfiler = useMiniProfiler ?? MiniProfilerHelper.GetMiniProfilerEnabledAppSetting();
        }

        /// <summary>
        /// Creates a new ConnectionFactory instance.  MiniProfiler use will be determined by MiniProfilerEnabled config setting.
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="providerName">Provider </param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="connectionString"/> or <paramref name="providerName"/> is null.</exception>
        public ConnectionFactory(string connectionString, string providerName)
            : this(connectionString, providerName, null)
        {
        }

        /// <summary>
        /// Creates a new ConnectionFactory instance.  SqlClient provider is assumed.
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="useMiniProfiler">Enables MiniProfiler.  ProfiledDbConnection will be used instead of DbConnection.  Pass null to look in config for MiniProfilerEnabled setting.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="connectionString"/> is null.</exception>
        public ConnectionFactory(string connectionString, bool? useMiniProfiler)
            : this(connectionString, DefaultProvider, useMiniProfiler)
        {
        }

        /// <summary>
        /// Creates a new ConnectionFactory instance.  SqlClient provider is assumed.  MiniProfiler use will be determined by MiniProfilerEnabled config setting.
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="connectionString"/> is null.</exception>
        public ConnectionFactory(string connectionString)
            : this(connectionString, DefaultProvider, null)
        {
        }

        /// <summary>
        /// Creates a new instance of <see cref="IDbConnection"/>
        /// </summary>
        /// <exception cref="ConfigurationErrorsException">Thrown if connection cannot be created with specified connection string and provider.</exception>
        /// <returns></returns>
        public IDbConnection GetConnection()
        {
            var connection = _provider.CreateConnection();

            if (_useMiniProfiler && MiniProfiler.Current != null)
            {
                MiniProfiler.Settings.MaxJsonResponseSize = 2147483647;
                connection = new ProfiledDbConnection(connection, MiniProfiler.Current);
            }

            if (connection == null)
                throw new ConfigurationErrorsException(
                    $"Failed to create a connection using the specified connection string and provider");

            connection.ConnectionString = _connectionString;
            return connection;
        }
    }
}