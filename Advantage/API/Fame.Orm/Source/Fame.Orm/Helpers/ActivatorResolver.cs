﻿using System;
using System.Collections.Generic;
using System.Linq;
using DapperExtensions;
using Fame.Orm.Dapper;

namespace Fame.Orm.Helpers
{
    /// <summary>
    /// A simple repository resolver where repositories will be registered by adding them in code to the Dictionary during application bootstrapping.
    /// </summary>
    public class ActivatorResolver : IRepositoryResolver
    {
        protected readonly IDapperExtensionsConfiguration DapperConfig;

        public Dictionary<Type, Type> Repositories = new Dictionary<Type, Type>();

        public ActivatorResolver(IDapperExtensionsConfiguration dapperConfig)
        {
            DapperConfig = dapperConfig;
        }

        public void Register(Type entityType, Type repositoryType)
        {
            Repositories.Add(entityType, repositoryType);
        }

        public TRepository GetRegisteredRepository<TRepository>(IUnitOfWork unitOfWork) where TRepository : class
        {
            if (Repositories.Count == 0) 
                throw new ApplicationException("No repositories have been registered.");

            var repoType = typeof(TRepository);
            var foundType = Repositories.Single(x => x.Value == repoType);

            if (foundType.Value == null) 
                throw new ApplicationException(string.Format("Unable to find registered repository for type {0}", repoType));

            var repository = Activator.CreateInstance(foundType.Value, unitOfWork, DapperConfig);

            return (TRepository)repository;
        }

        public IRepository<TEntity> GetRepositoryForEntity<TEntity>(IUnitOfWork unitOfWork) where TEntity : class, IEntity, new()
        {
            IRepository<TEntity> repository = null;

            var entityType = typeof(TEntity);

            if (Repositories.Any(x => x.Key == entityType))
            {
                var foundInterface = Repositories.Single(x => x.Key == entityType);

                if (foundInterface.Value != null)
                    repository = (IRepository<TEntity>)Activator.CreateInstance(foundInterface.Value, unitOfWork as DapperUnitOfWork, DapperConfig);
            }

            return repository ?? CreatetDefaultRepository<TEntity>(unitOfWork);
        }

        public virtual IRepository<TEntity> CreatetDefaultRepository<TEntity>(IUnitOfWork unitOfWork) where TEntity : class, IEntity
        {
            return new DapperRepositoryBase<TEntity>(unitOfWork as DapperUnitOfWork, DapperConfig);
        }
    }
}
