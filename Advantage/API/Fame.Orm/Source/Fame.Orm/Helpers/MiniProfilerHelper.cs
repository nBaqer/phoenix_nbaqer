﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.Principal;
using System.Text;
using FAME.Extensions;
using FAME.Extensions.Helpers;
using StackExchange.Profiling;

namespace Fame.Orm.Helpers
{
    /// <summary>
    /// A static helper class to simplify using MiniProfiler in non-web applications (Console, Winforms, etc.)
    /// <para/>Supports automatic configuration via the following AppSettings:
    /// <para/>MiniProfilerEnabled, MiniProfilerOutputMode, MiniProfilerOutputFile
    /// </summary>
    public static class MiniProfilerHelper
    {
        private const string MiniProfilerAppKeyEnabled    = "MiniProfilerEnabled";
        private const string MiniProfilerAppKeyOutputMode = "MiniProfilerOutputMode";
        private const string MiniProfilerAppKeyOutputFile = "MiniProfilerOutputFile";

        [Flags]
        public enum OutputModes
        {
            Debug = 1,
            Console = 2,
            File = 4,
            Default = Debug | Console,
            All = Debug | Console | File
        }

        /// <summary>
        /// Enables or disables profiling.  Will default to UseMiniProfiler AppSetting.
        /// </summary>
        public static bool Enabled { get; set; }

        /// <summary>
        /// Specifies where output will be sent.  Mulitple values can be OR'd together.
        /// <para/>Default is Debug | Console.
        /// </summary>
        public static OutputModes OutputMode { get; set; }

        /// <summary>
        /// If OutputMode includes File, specify output file name here.
        /// <para/>Default is &lt;ExeName&gt;.MiniProfiler.txt in the user's temp folder
        /// </summary>
        public static string OutputFile { get; set; }

        static MiniProfilerHelper()
        {
            OutputMode = AppSettings.Get<OutputModes>(MiniProfilerAppKeyOutputMode, OutputModes.Default.ToString());
            OutputFile = AppSettings.Get<string>(MiniProfilerAppKeyOutputFile, Path.Combine(Path.GetTempPath(), String.Format("{0}.MiniProfiler.txt", AppDomain.CurrentDomain.FriendlyName)));
            Enabled = GetMiniProfilerEnabledAppSetting();
        }

        /// <summary>
        /// Convienience method to retrieve the value of MiniProfilerEnabled from AppSettings.  Useful for checking the value in FameORM.
        /// </summary>
        /// <returns></returns>
        public static bool GetMiniProfilerEnabledAppSetting()
        {
            return AppSettings.Get<bool>(MiniProfilerAppKeyEnabled, "false");
        }

        /// <summary>
        /// Start profiling
        /// </summary>
        public static void Start()
        {
            if (Enabled)
            {
                MiniProfiler.Settings.ProfilerProvider = new FameProfilerProvider();
                MiniProfiler.Start();
            }
        }

        /// <summary>
        /// Call from a Using block to time the code between it's creation and disposal
        /// </summary>
        /// <param name="name">The name of the step.  Used in profiler output.</param>
        /// <returns></returns>
        public static IDisposable Step(string name)
        {
            if (Enabled)
                return MiniProfiler.Current.Step(name);
            else
                return null;
        }

        /// <summary>
        /// Stop profiling.  Call before WriteOutput.
        /// </summary>
        public static void Stop()
        {
            if (Enabled)
                MiniProfiler.Stop();
        }

        /// <summary>
        /// Stop profiling and write output to the destinations specified in OutputMode
        /// </summary>
        public static void StopAndWriteOutput()
        {
            Stop();
            WriteOutput();
        }

        /// <summary>
        /// Write output to the destinations specified in OutputMode
        /// </summary>
        public static void WriteOutput()
        {
            if (Enabled)
            {
                var output = GetOutput();

                if (OutputMode.HasFlag(OutputModes.Debug))
                    Debug.WriteLine(output);

                if (OutputMode.HasFlag(OutputModes.Console))
                    System.Console.WriteLine(output);

                if (OutputMode.HasFlag(OutputModes.File))
                    output.ToFile(OutputFile, true);
            }
        }

        /// <summary>
        /// Returns the formatted output string that will be written to the destinations specified in OutputMode.
        /// </summary>
        /// <returns></returns>
        public static string GetOutput()
        {
            var output = "";

            if (Enabled)
            {
                var stringBuilder = new StringBuilder();

                stringBuilder.AppendLine();
                stringBuilder.AppendLine("MiniProfiler Timings:");
                stringBuilder.AppendLine();

                stringBuilder.AppendFormat("{0} on {1} at {2} as {3}", MiniProfiler.Current.Head.Name, MiniProfiler.Current.MachineName, DateTime.Now, MiniProfiler.Current.User);

                RenderTiming(stringBuilder, MiniProfiler.Current.Head);

                output = stringBuilder.ToString();
            }

            return output;
        }

        private static void RenderTiming(StringBuilder stringBuilder, Timing timing)
        {
            var SqlStringBuilder = new StringBuilder();
            decimal? SqlDurationMs = 0m;
            var SqlCmdCount = 0;

            var indnet = timing.Depth > 1 ? "".PadRight(timing.Depth * 2) : "";
            var subindent = timing.Depth > 0 ? "  " : "";

            if (timing.HasCustomTimings)
                foreach (var custTimings in timing.CustomTimings.Values)
                    foreach (var custTiming in custTimings)
                    {
                        SqlDurationMs = SqlDurationMs + custTiming.DurationMilliseconds;
                        SqlCmdCount++;

                        SqlStringBuilder.AppendLine(Environment.NewLine);
                        SqlStringBuilder.AppendFormat("{0}{1}Duration: {2}ms{3}", indnet, subindent, custTiming.DurationMilliseconds, Environment.NewLine);
                        SqlStringBuilder.AppendFormat("{0}{1}Stack:    {2}{3}", indnet, subindent, custTiming.StackTraceSnippet, Environment.NewLine);
                        SqlStringBuilder.AppendFormat("{0}{1}Command:  {2}", indnet, subindent, custTiming.CommandString);
                    }

            if (!timing.IsRoot)
            {
                stringBuilder.AppendLine();
                stringBuilder.AppendFormat("{0}Begin Step: {1} = {2}ms (SQL = {3}ms in {4} commands)", indnet, timing.Name, timing.DurationMilliseconds, SqlDurationMs, SqlCmdCount);
            }

            stringBuilder.AppendLine(SqlStringBuilder.ToString());

            if (timing.HasChildren)
                foreach (var child in timing.Children)
                    RenderTiming(stringBuilder, child);

            if (!timing.IsRoot)
            {
                stringBuilder.AppendLine();
                stringBuilder.AppendFormat("{0}End Step: {1} = {2}ms (SQL = {3}ms in {4} commands{5}", indnet, timing.Name, timing.DurationMilliseconds, SqlDurationMs, SqlCmdCount, Environment.NewLine);
            }
        }


        public class FameProfilerProvider : BaseProfilerProvider
        {
            private MiniProfiler _profiler;

            public override MiniProfiler GetCurrentProfiler()
            {
                return _profiler;
            }

            public override MiniProfiler Start(string sessionName = null)
            {
                if (_profiler == null)
                    _profiler = new MiniProfiler(sessionName ?? AppDomain.CurrentDomain.FriendlyName);
                _profiler.User = WindowsIdentity.GetCurrent().Name;
                SetProfilerActive(_profiler);
                return _profiler;
            }

            [Obsolete("Please use the Start(string sessionName) overload instead of this one. ProfileLevel is going away.")]
            public override MiniProfiler Start(ProfileLevel level, string sessionName = null)
            {
                return Start(sessionName);
            }

            public override void Stop(bool discardResults)
            {
                if ((_profiler != null) && !discardResults)
                    SaveProfiler(_profiler);
            }
        }
    }
}
