﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Fame.Orm.Helpers
{
    public static class Filter
    {
        public static FilterField Build<TEntity>(Expression<Func<TEntity, object>> property, object value, FilterFieldOperator op = FilterFieldOperator.Eq, bool not = false) where TEntity : class
        {
            var propertyName = "";
            if (property.Body is MemberExpression)
                propertyName = ((MemberExpression)property.Body).Member.Name;
            else if (property.Body is UnaryExpression)
            {
                var operand = ((UnaryExpression)property.Body).Operand;
                propertyName = ((MemberExpression)operand).Member.Name;
            }
            else
                throw new ArgumentException("Invalid expression.  Must be a Member or Unary expression.", nameof(property));

            return Build(propertyName, value, op, not);
        }

        public static FilterField Build(string propertyName, object value, FilterFieldOperator op = FilterFieldOperator.Eq, bool not = false)
        {
            return new FilterField { PropertyName = propertyName, Operator = op, Value = value, Not = not };
        }

        public static List<FilterField> BuildList(params FilterField[] filters)
        {
            return filters.ToList();
        }
    }

    public static class Sort
    {
        public static SortField Build<TEntity>(Expression<Func<TEntity, object>> property, bool descending = false) where TEntity : class
        {
            string propertyName = "";
            if (property.Body is MemberExpression)
                propertyName = ((MemberExpression)property.Body).Member.Name;
            else if (property.Body is UnaryExpression)
            {
                var operand = ((UnaryExpression)property.Body).Operand;
                propertyName = ((MemberExpression)operand).Member.Name;
            }
            else
                throw new ArgumentException("Invalid expression.  Must be a Member or Unary expression.", nameof(property));

            return Build(propertyName, descending);
        }

        public static SortField Build(string propertyName, bool descending = false)
        {
            return new SortField { PropertyName = propertyName, Descending = descending };
        }

        public static List<SortField> BuildList(params SortField[] filters)
        {
            return filters.ToList();
        }

        public static List<SortField> Simple<TEntity>(params Expression<Func<TEntity, object>>[] properties) where TEntity : class
        {
            var sortFields = new List<SortField>();
            foreach (var property in properties)
                sortFields.Add(Build(property));
            return sortFields;
        }
    }
}
