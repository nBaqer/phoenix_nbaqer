﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DapperExtensions.Mapper;

namespace Fame.Orm.Helpers
{
    /// <summary>
    /// Default auto mapper used by EspOrm if no entity specific mapper is found.
    /// Should handle any tables where the following are true:
    /// 1) Entity class name equals the DataDic table ID
    /// 2) Table is directly in folder specified in DataDic (ie. not a school folder table)
    /// 3) A class implementing IKeyProps\&lt;EntityType\&gt; exists whose members are the key properties
    /// or the key properties have been tagged with the KeyProperty attribute
    /// or the primary key is the first property ending in "id" (add/mod fields are ignored)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FameAutoClassMapper<T> : ClassMapper<T> where T : class
    {
        public FameAutoClassMapper() : this(true)
        {
        }

        public FameAutoClassMapper(bool autoMap)
        {
            SetKey(typeof(T));
            if (autoMap)
                AutoMap();
        }

        /// <summary>
        /// Call the base class AutoMap method.
        /// This call used to be made from the constructor, but it is not good practice
        /// to call virtual methods from constructor, and resharper considers is an error
        /// </summary>
        protected sealed override void AutoMap()
        {
            base.AutoMap();
        }

        private void SetKey(Type entityType)
        {
            //Try to find an IKeyProps<> class for the entity
            var keyProps = GetKeyProps(entityType);

            if (!keyProps.Any())
            {
                //If we couldn't find an IKeyProps<> class for the entity
                //try to find properties decorated with KeyProperty attribute
                var entityProps = entityType.GetProperties();
                keyProps = GetKeyPropsByAttrib(entityProps);

                //If we couldn't find any properties decorated with KeyProperty attribute
                //try to find the first property who's name ends with "id"
                if (!keyProps.Any())
                    keyProps = GetKeyPropsByName(entityProps, entityType);
            }

            foreach (var keyProp in keyProps)
                Map(keyProp).Key(SetKeyType(keyProp));
        }

        public virtual KeyType SetKeyType(PropertyInfo keyProp)
        {
            if (keyProp.PropertyType == typeof(Guid))
                return KeyType.Guid;
            
            if (keyProp.PropertyType == typeof(long) || keyProp.PropertyType == typeof(int))
                return KeyType.Identity;
                
            return KeyType.Assigned;
        }

        private static List<PropertyInfo> GetKeyProps(Type entityType)
        {
            var types = entityType.Assembly.GetTypes();

            var keyPropsType = (from type in types
                        let interfaceType = type.GetInterface(typeof(IKeyProps<>).FullName)
                        where
                            interfaceType != null &&
                            interfaceType.GetGenericArguments()[0] == entityType
                        select type).SingleOrDefault();
            
            var keyProps = new List<PropertyInfo>();

            if (keyPropsType == null) return keyProps;

            foreach (var keyProp in keyPropsType.GetProperties())
                keyProps.Add(entityType.GetProperty(keyProp.Name));

            return keyProps;
        }

        private static List<PropertyInfo> GetKeyPropsByAttrib(PropertyInfo[] entityProps)
        {
            var keyProps = new List<PropertyInfo>();
            var propsKeyAttrib = entityProps.Where(pi => Attribute.IsDefined(pi, typeof(KeyPropertyAttribute))).ToList();

            if (propsKeyAttrib.Any())
                keyProps.AddRange(propsKeyAttrib);

            return keyProps;
        }

        private static List<PropertyInfo> GetKeyPropsByName(PropertyInfo[] entityProps, Type entityType)
        {
            var keyProps = new List<PropertyInfo>();

            //First, find a property with the entity type name -- newer SQL tables will have the PK as "[TableName]Id"
            var pkProp = entityProps.FirstOrDefault(pi => pi.Name.ToLower().Trim() == entityType.Name.ToLower().Trim() + "Id");
            if (pkProp != null)
            {
                keyProps.Add(pkProp);
                return keyProps;
            }

            //PK was not found, find other properties ending in "Id"
            var idFields = "created_id,updated_id,add_id,mod_id,addid,modid".Split(',');
            var idProp = entityProps.FirstOrDefault(pi => pi.Name.ToLower().EndsWith("id") && !idFields.Contains(pi.Name.ToLower()));

            if (idProp == null) return keyProps;

            keyProps.Add(idProp);
            return keyProps;
        }
    }
}
