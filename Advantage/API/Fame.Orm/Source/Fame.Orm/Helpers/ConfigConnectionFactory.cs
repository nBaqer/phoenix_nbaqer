﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;

namespace Fame.Orm.Helpers
{
    /// <summary>
    /// Creates <see cref="IDbConnection"/> instances from a connection defined in the connectionstrings section of the app/web.config file.
    /// </summary>
    public class ConfigConnectionFactory : IConnectionFactory
    {
        private readonly ConnectionFactory _connectionFactory;

        /// <summary>
        /// Creates a new ConfigConnectionFactory instance
        /// </summary>
        /// <param name="connectionStringName">A key of one of the ConnectionString settings inside the ConnectionStrings section of an app/web.config file, or a key to an AppSetting that contains the name of the connection string.</param>
        /// <param name="useMiniProfiler">Enables MiniProfiler.  ProfiledDbConnection will be used instead of DbConnection.  Pass null to look in config for MiniProfilerEnabled setting.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="connectionStringName"/> is null and config contains more than one connection string.</exception>
        /// <exception cref="ConfigurationErrorsException">Thrown if <paramref name="connectionStringName"/> is not found in any app/web.config file available to the application.</exception>
        public ConfigConnectionFactory(string connectionStringName, bool? useMiniProfiler)
        {
            var conStr = GetConnectionStringSettings(connectionStringName);

            if (string.IsNullOrWhiteSpace(conStr.ConnectionString))
                throw new ConfigurationErrorsException(
                    $"Failed to find connection string named '{connectionStringName}' in app.config or web.config.");

            _connectionFactory = new ConnectionFactory(conStr.ConnectionString, conStr.ProviderName, useMiniProfiler ?? MiniProfilerHelper.GetMiniProfilerEnabledAppSetting());
        }

        /// <summary>
        /// Creates a new ConnectionFactory instance.  MiniProfiler use will be determined by MiniProfilerEnabled config setting.
        /// </summary>
        /// <param name="connectionStringName">A key of one of the ConnectionString settings inside the ConnectionStrings section of an app/web.config file, or a key to an AppSetting that contains the name of the connection string.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="connectionStringName"/> is null.</exception>
        /// <exception cref="ConfigurationErrorsException">Thrown if <paramref name="connectionStringName"/> is not found in any app/web.config file available to the application.</exception>
        public ConfigConnectionFactory(string connectionStringName)
            : this(connectionStringName, null)
        {
        }

        /// <summary>
        /// Creates a new ConfigConnectionFactory instance.  Requires a single connection string to be included in config.
        /// </summary>
        /// <param name="useMiniProfiler">Enables MiniProfiler.  ProfiledDbConnection will be used instead of DbConnection.  Pass null to look in config for MiniProfilerEnabled setting.</param>
        /// <exception cref="ConfigurationErrorsException">Thrown if single connection string is not found in config.</exception>
        public ConfigConnectionFactory(bool? useMiniProfiler)
            : this(null, useMiniProfiler)
        {
        }

        /// <summary>
        /// Creates a new ConfigConnectionFactory instance.  Requires a single connection string to be included in config.  MiniProfiler use will be determined by MiniProfilerEnabled config setting.
        /// </summary>
        /// <exception cref="ConfigurationErrorsException">Thrown if single connection string is not found in config.</exception>
        public ConfigConnectionFactory()
            : this(null, null)
        {
        }

        private ConnectionStringSettings GetConnectionStringSettings(string connectionStringName)
        {
            ConnectionStringSettings conStr = null;

            if (connectionStringName != null)
            {

                //Check first for connection string name in ConnectionStrings section
                if (ConfigurationManager.ConnectionStrings.Cast<ConnectionStringSettings>().Any(cs => cs.Name == connectionStringName))
                    conStr = ConfigurationManager.ConnectionStrings[connectionStringName];

                // If not found in ConnectionStrings, check if AppSetting exists, get connection name from there and then try ConnectionStrings
                if (conStr == null)
                    if (ConfigurationManager.AppSettings.AllKeys.Any(key => key == connectionStringName))
                    {
                        var key = ConfigurationManager.AppSettings[connectionStringName];
                        if (ConfigurationManager.ConnectionStrings.Cast<ConnectionStringSettings>().Any(s => s.Name == key))
                            conStr = ConfigurationManager.ConnectionStrings[key];
                    }

            }

            if (conStr != null) return conStr;

            // If connectionStringName not passed, or the one passed was not found
            // check if only one connection defined in config and use it
            if (ConfigurationManager.ConnectionStrings.Count == 1)
            {
                connectionStringName = ConfigurationManager.ConnectionStrings[0].Name;
                conStr = ConfigurationManager.ConnectionStrings[connectionStringName];
            }
            else
            {
                throw new ArgumentNullException(nameof(connectionStringName));
            }

            return conStr;
        }

        /// <summary>
        /// Creates a new instance of <see cref="IDbConnection"/>
        /// </summary>
        /// <exception cref="ConfigurationErrorsException">Thrown if the connectionstring entry in the app/web.config file is missing information, contains errors or is missing entirely.</exception>
        /// <returns></returns>
        public IDbConnection GetConnection()
        {
            return _connectionFactory.GetConnection();
        }
    }
}