﻿using System.Collections.Generic;
using System.Reflection;
using DapperExtensions;
using DapperExtensions.Sql;
using Fame.Orm.Dapper;
using Fame.Orm.Helpers;

namespace Fame.Orm
{
    public interface IFameOrm
    {
        IUnitOfWork GetUnitOfWork();
    }

    public class FameOrm
    {
        private IRepositoryResolver _repositoryResolver;
        private IConnectionFactory _connectionFactory;

        public FameOrm(string connectionStringName, bool useMiniProfiler)
        {
            Initialize(new ConfigConnectionFactory(connectionStringName, useMiniProfiler));
        }

        public FameOrm(string connectionStringName) : this(connectionStringName, false)
        {
        }

        public FameOrm(IConnectionFactory connectionFactory)
        {
            Initialize(connectionFactory);
        }

        public void Initialize(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
            var dapperConfig = new DapperExtensionsConfiguration(typeof(FameAutoClassMapper<>), new List<Assembly>() { Assembly.GetExecutingAssembly() }, new SqlServerDialect());
            _repositoryResolver = new ActivatorResolver(dapperConfig);
        }

        public virtual IUnitOfWork GetUnitOfWork()
        {
            return new DapperUnitOfWork(_connectionFactory, _repositoryResolver);
        }
    }
}
