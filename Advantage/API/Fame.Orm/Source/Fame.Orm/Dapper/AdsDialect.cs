﻿using System;
using System.Collections.Generic;
using System.Text;
using DapperExtensions.Sql;

namespace Fame.Orm.Dapper
{
    public class AdsDialect : SqlDialectBase
    {

        public override char ParameterPrefix { get{ return ':'; } }
        
        public override string GetIdentitySql(string tableName)
        {
            return "SELECT LASTAUTOINC(CONNECTION) AS [Id]";
        }

        public override string GetPagingSql(string sql, int page, int resultsPerPage, IDictionary<string, object> parameters)
        {
            int startValue = ((page - 1) * resultsPerPage) + 1;
            return GetSetSql(sql, startValue, resultsPerPage, parameters);
        }

        public override string GetSetSql(string sql, int firstResult, int maxResults, IDictionary<string, object> parameters)
        {
            if (string.IsNullOrEmpty(sql))
            {
                throw new ArgumentNullException("SQL");
            }

            if (parameters == null)
            {
                throw new ArgumentNullException("Parameters");
            }

            var result = sql.Replace("SELECT ", string.Format("SELECT TOP {0} START AT {1} ", maxResults, firstResult));
            return result;
        }

        public override string GetColumnName(string prefix, string columnName, string alias)
        {
            if (string.IsNullOrWhiteSpace(columnName))
            {
                throw new ArgumentNullException(columnName, "columnName cannot be null or empty.");
            }

            var result = new StringBuilder();
            result.AppendFormat("[{0}]", columnName);

            if (!string.IsNullOrWhiteSpace(alias))
            {
                result.AppendFormat(" AS {0}", QuoteString(alias));
            }
            return result.ToString();
        }
    }
}