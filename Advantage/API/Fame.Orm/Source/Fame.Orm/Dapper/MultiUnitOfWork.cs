﻿using System;
using System.Collections.Generic;
using System.Linq;
using FAME.Extensions;

namespace Fame.Orm.Dapper
{
    public class MultiUnitOfWork : IMultiUnitOfWork
    {
        private readonly Dictionary<string, IUnitOfWork> _uows;

        public MultiUnitOfWork(params IFameOrm[] orms)
        {
            _uows = new Dictionary<string, IUnitOfWork>();

            foreach (var orm in orms)
            {
                var uow = orm.GetUnitOfWork();
                _uows.Add(orm.GetType().FullName, uow);
            }
        }

        public IUnitOfWork GetUnitOfWork<TOrm>() where TOrm : IFameOrm
        {
            return (from uow in _uows where (uow.Key == typeof(TOrm).FullName || uow.Key == typeof(TOrm).FullName.Replace(".I", ".")) select uow).FirstOrDefault().Value;
        }

        public TRepository GetRegisteredRepository<TOrm, TRepository>()
            where TRepository : class 
            where TOrm : IFameOrm
        {
            return GetUnitOfWork<TOrm>().GetRegisteredRepository<TRepository>();
        }

        public IRepository<TEntity> GetRepositoryForEntity<TOrm, TEntity>()
            where TEntity : class, IEntity, new()
            where TOrm : IFameOrm
        {
            return GetUnitOfWork<TOrm>().GetRepositoryForEntity<TEntity>();
        }

        public TRepository GetRegisteredRepository<TRepository>() where TRepository : class
        {
            return _uows.First().Value.GetRegisteredRepository<TRepository>();
        }

        public IRepository<TEntity> GetRepositoryForEntity<TEntity>() where TEntity : class, IEntity, new()
        {
            return _uows.First().Value.GetRepositoryForEntity<TEntity>();
        }

        public bool WasRolledBack {
            get { return _uows.Any(uow => uow.Value.WasRolledBack); }
        }

        public void ManageTransaction(Action action)
        {
            _uows.Each(uow => uow.Value.ManageTransaction(action));
        }

        public void BeginTransaction()
        {
            _uows.Each(uow => uow.Value.BeginTransaction());
        }

        public void Commit()
        {
            _uows.Each(uow => uow.Value.Commit());
        }

        public void Rollback()
        {
            _uows.Each(uow => uow.Value.Rollback());
        }

        public void Dispose()
        {
            _uows.Each(uow => uow.Value.Dispose());
        }

        public IEnumerable<TEntity> Execute<TEntity>(ICommand<TEntity> command) where TEntity : class
        {
            return _uows.First().Value.Execute(command);
        }

        public IEnumerable<TOut> Execute<TEntity, TOut>(ICommand<TEntity, TOut> command) where TEntity : class
        {
            return _uows.First().Value.Execute(command);
        }

        public T Execute<T>(ICommandScalar<T> command) where T : struct
        {
            return _uows.First().Value.Execute(command);
        }

        public void Execute(ICommand command)
        {
            _uows.First().Value.Execute(command);
        }
    }
}
