﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using DapperExtensions;
using DapperExtensions.Sql;

namespace Fame.Orm.Dapper
{
    public enum StringCompareModes
    {
        /// <summary>
        /// String comparisons in filter criteria will be unaltered in generated SQL
        /// </summary>
        Default,
        /// <summary>
        /// Both sides of string comparisons in filter criteria will be replaced with the format specified in DefaultStringCompareFormatUpper in generated SQL
        /// </summary>
        ForceUpper,
        /// <summary>
        /// Both sides of string comparisons in filter criteria will be replaced with the format specified in DefaultStringCompareFormatLower in generated SQL
        /// </summary>
        ForceLower
    }

    public class DapperRepositoryBase
    {
        internal const string DefaultStringCompareFormatLower = "LOWER({0})";
        internal const string DefaultStringCompareFormatUpper = "UPPER({0})";

        internal readonly DapperUnitOfWork UnitOfWork;

        protected readonly DapperImplementor DapperImp;
        protected readonly IDapperExtensionsConfiguration DapperConfig;

        public bool SortLocal { get; set; }

        public StringCompareModes StringCompareMode { get; set; }
        public string StringCompareFormatLower { get; set; }
        public string StringCompareFormatUpper { get; set; }

        public DapperRepositoryBase(DapperUnitOfWork unitOfWork, IDapperExtensionsConfiguration dapperConfig)
            : this(unitOfWork, StringCompareModes.Default, DefaultStringCompareFormatLower, DefaultStringCompareFormatUpper, dapperConfig) { }

        public DapperRepositoryBase(DapperUnitOfWork unitOfWork, StringCompareModes stringCompareMode, string stringCompareFormatLower, string stringCompareFormatUpper, IDapperExtensionsConfiguration dapperConfig)
        {
            UnitOfWork = unitOfWork;
            SortLocal = false;
            StringCompareMode = stringCompareMode;
            StringCompareFormatLower = stringCompareFormatLower;
            StringCompareFormatLower = stringCompareFormatUpper;

            DapperConfig = dapperConfig;
            DapperImp = new DapperImplementor(new SqlGeneratorImpl(dapperConfig));
        }
    }

    public class DapperRepositoryBase<TEntity> : DapperRepositoryBase, IRepository<TEntity> where TEntity : class, IEntity
    {
        public DapperRepositoryBase(DapperUnitOfWork unitOfWork, IDapperExtensionsConfiguration dapperConfig)
            : base(unitOfWork, dapperConfig) { }

        public DapperRepositoryBase(DapperUnitOfWork unitOfWork, StringCompareModes stringCompareMode, string stringCompareFormatLower, string stringCompareFormatUpper, IDapperExtensionsConfiguration dapperConfig)
            : base(unitOfWork, stringCompareMode, stringCompareFormatLower, stringCompareFormatUpper, dapperConfig) { }

        public virtual TEntity GetById<TKey>(TKey id)
        {
            UnitOfWork.GetOpenConnection();
            return DapperImp.Get<TEntity>(UnitOfWork.DbConnection, id, UnitOfWork.DbTransaction, null);
        }

        public virtual TEntity Create(TEntity entity)
        {
            UnitOfWork.GetOpenConnection();
            DapperImp.Insert(UnitOfWork.DbConnection, entity, UnitOfWork.DbTransaction, null);
            return entity;
        }

        public virtual void Create(IEnumerable<TEntity> entities)
        {
            UnitOfWork.GetOpenConnection();
            DapperImp.Insert(UnitOfWork.DbConnection, entities, UnitOfWork.DbTransaction, null);
        }

        public virtual TEntity Update(TEntity entity)
        {
            UnitOfWork.GetOpenConnection();
            DapperImp.Update(UnitOfWork.DbConnection, entity, UnitOfWork.DbTransaction, null);
            return entity;
        }

        public virtual void Update(IEnumerable<TEntity> entities)
        {
            UnitOfWork.GetOpenConnection();
            foreach (var entity in entities)
                DapperImp.Update(UnitOfWork.DbConnection, entity, UnitOfWork.DbTransaction, null);
        }

        public virtual TEntity Delete(TEntity entity)
        {
            UnitOfWork.GetOpenConnection();
            DapperImp.Delete(UnitOfWork.DbConnection, entity, UnitOfWork.DbTransaction, null);
            return entity;
        }

        public virtual void Delete(IEnumerable<TEntity> entities)
        {
            UnitOfWork.GetOpenConnection();
            foreach (var entity in entities)
                DapperImp.Delete(UnitOfWork.DbConnection, entity, UnitOfWork.DbTransaction, null);
        }

        public virtual void Delete(IEnumerable<FilterField> filter)
        {
            UnitOfWork.GetOpenConnection();
            Delete(new List<IEnumerable<FilterField>>() { filter });
        }

        public virtual void Delete(IEnumerable<IEnumerable<FilterField>> filter)
        {
            UnitOfWork.GetOpenConnection();
            DapperImp.Delete<TEntity>(UnitOfWork.DbConnection, BuildPredicateGroupList(filter), UnitOfWork.DbTransaction, null);
        }

        public virtual IEnumerable<TEntity> GetList()
        {
            return GetList(new List<FilterField>());
        }

        public virtual IEnumerable<TEntity> GetList(IEnumerable<FilterField> filter)
        {
            return GetList(filter, null);
        }

        public virtual IEnumerable<TEntity> GetList(IEnumerable<SortField> sort)
        {
            return GetList(new List<FilterField>(), sort);
        }

        public virtual IEnumerable<TEntity> GetList(IEnumerable<IEnumerable<FilterField>> filter)
        {
            return GetList(filter, null);
        }

        public virtual IEnumerable<TEntity> GetList(IEnumerable<FilterField> filter, IEnumerable<SortField> sort)
        {
            return GetList(new List<IEnumerable<FilterField>>() { filter }, sort);
        }

        public virtual IEnumerable<TEntity> GetList(IEnumerable<IEnumerable<FilterField>> filter, IEnumerable<SortField> sort)
        {
            UnitOfWork.GetOpenConnection();
            return SortLocal
                ? DapperImp.GetList<TEntity>(UnitOfWork.DbConnection, BuildPredicateGroupList(filter), null, UnitOfWork.DbTransaction, null, true).AsQueryable().OrderBy(BuildSortLocal(sort)).AsEnumerable()
                : DapperImp.GetList<TEntity>(UnitOfWork.DbConnection, BuildPredicateGroupList(filter), BuildSort(sort), UnitOfWork.DbTransaction, null, true);
        }

        public virtual IEnumerable<TEntity> GetPage(IEnumerable<SortField> sort, int page, int resultsPerPage)
        {
            return GetPage(null as IEnumerable<IEnumerable<FilterField>>, sort, page, resultsPerPage);
        }

        public virtual IEnumerable<TEntity> GetPage(IEnumerable<FilterField> filter, IEnumerable<SortField> sort, int page, int resultsPerPage)
        {
            return GetPage(new List<IEnumerable<FilterField>>() { filter }, sort, page, resultsPerPage);
        }

        public virtual IEnumerable<TEntity> GetPage(IEnumerable<IEnumerable<FilterField>> filter, IEnumerable<SortField> sort, int page, int resultsPerPage)
        {
            UnitOfWork.GetOpenConnection();
            return SortLocal
                ? DapperImp.GetPage<TEntity>(UnitOfWork.DbConnection, BuildPredicateGroupList(filter), null, page, resultsPerPage, UnitOfWork.DbTransaction, null, true).AsQueryable().OrderBy(BuildSortLocal(sort)).AsEnumerable()
                : DapperImp.GetPage<TEntity>(UnitOfWork.DbConnection, BuildPredicateGroupList(filter), BuildSort(sort), page, resultsPerPage, UnitOfWork.DbTransaction, null, true);
        }

        public virtual IEnumerable<TEntity> GetSet(IEnumerable<SortField> sort, int firstResult, int maxResults)
        {
            return GetSet(null as IEnumerable<IEnumerable<FilterField>>, sort, firstResult, maxResults);
        }

        public virtual IEnumerable<TEntity> GetSet(IEnumerable<FilterField> filter, IEnumerable<SortField> sort, int firstResult, int maxResults)
        {
            return GetSet(new List<IEnumerable<FilterField>>() { filter }, sort, firstResult, maxResults);
        }

        public virtual IEnumerable<TEntity> GetSet(IEnumerable<IEnumerable<FilterField>> filter, IEnumerable<SortField> sort, int firstResult, int maxResults)
        {
            UnitOfWork.GetOpenConnection();
            return SortLocal
                ? DapperImp.GetSet<TEntity>(UnitOfWork.DbConnection, BuildPredicateGroupList(filter), null, firstResult, maxResults, UnitOfWork.DbTransaction, null, true).AsQueryable().OrderBy(BuildSortLocal(sort)).AsEnumerable()
                : DapperImp.GetSet<TEntity>(UnitOfWork.DbConnection, BuildPredicateGroupList(filter), BuildSort(sort), firstResult, maxResults, UnitOfWork.DbTransaction, null, true);
        }

        public virtual int Count(IEnumerable<FilterField> filter)
        {
            return Count(new List<IEnumerable<FilterField>>() { filter });
        }

        public virtual int Count(IEnumerable<IEnumerable<FilterField>> filter)
        {
            UnitOfWork.GetOpenConnection();
            return DapperImp.Count<TEntity>(UnitOfWork.DbConnection, BuildPredicateGroupList(filter), UnitOfWork.DbTransaction, null);
        }

        public IEnumerable<TEntity> Query(IQuery<TEntity> query)
        {
            return GetList(query.Filters, query.Sorts);
        }

        public IEnumerable<TEntity> QueryPage(IQuery<TEntity> query, int page, int resultsPerPage)
        {
            return GetPage(query.Filters, query.Sorts, page, resultsPerPage);
        }

        public IEnumerable<TEntity> QuerySet(IQuery<TEntity> query, int firstResult, int maxResults)
        {
            return GetSet(query.Filters, query.Sorts, firstResult, maxResults);
        }

        /// <summary>
        /// Execute a native provider command that returns a list of entities
        /// </summary>
        /// <param name="command">A command definition object</param>
        /// <returns>A list of entities</returns>
        public IEnumerable<TEntity> Execute(ICommand<TEntity> command)
        {
            return UnitOfWork.Execute(command);
        }

        /// <summary>
        /// Execute a native provider command that returns a list of objects.
        /// <para>This method should be used for cases where the caller wants to map the return/output
        /// of the Command to an object/data type that is different than TEntity</para>
        /// </summary>
        /// <param name="command">A command definition object</param>
        /// <returns>A list of objects</returns>
        public IEnumerable<TOut> Execute<TOut>(ICommand<TEntity, TOut> command)
        {
            return UnitOfWork.Execute(command);
        }

        private PredicateGroup BuildPredicateGroupList(IEnumerable<IEnumerable<FilterField>> filter)
        {
            if (filter == null) return null;

            var filterList = filter.ToList();
            if (filterList.First() == null || !filterList.First().Any()) return null;

            var pg = new PredicateGroup { Operator = GroupOperator.Or, Predicates = new List<IPredicate>() };

            foreach (var group in filterList)
                pg.Predicates.Add(BuildPredicateGroup(group));

            return pg;
        }

        private PredicateGroup BuildPredicateGroup(IEnumerable<FilterField> filter)
        {
            if (filter == null) return null;

            var filterList = filter.ToList();
            if (!filterList.Any()) return null;
            
            var pg = new PredicateGroup { Operator = GroupOperator.And, Predicates = new List<IPredicate>() };

            foreach (var field in filterList)
            {
                IFieldPredicate predicate;

                if (StringCompareMode == StringCompareModes.Default)
                    predicate = new FieldPredicate<TEntity>();
                else
                    predicate = new FameFieldPredicate<TEntity>
                    {
                        StringCompareMode = StringCompareMode,
                        StringCompareFormatLower = StringCompareFormatLower,
                        StringCompareFormatUpper = StringCompareFormatUpper
                    };

                predicate.PropertyName = field.PropertyName;
                predicate.Operator = GetPredicateOperator(field.Operator);
                predicate.Value = field.Value;
                predicate.Not = field.Not;

                pg.Predicates.Add(predicate);
            }
            return pg;
        }

        private static Operator GetPredicateOperator(FilterFieldOperator op)
        {
            switch (op)
            {
                case FilterFieldOperator.Eq:
                    return Operator.Eq;
                case FilterFieldOperator.Ge:
                    return Operator.Ge;
                case FilterFieldOperator.Gt:
                    return Operator.Gt;
                case FilterFieldOperator.Le:
                    return Operator.Le;
                case FilterFieldOperator.Lt:
                    return Operator.Lt;
                case FilterFieldOperator.Like:
                    return Operator.Like;
                default:
                    return Operator.Eq;
            }
        }

        private static IList<ISort> BuildSort(IEnumerable<SortField> sortFields)
        {
            if (sortFields == null) return null;

            var result = new List<ISort>();

            foreach (var field in sortFields)
            {
                var sort = new DapSortField { PropertyName = field.PropertyName, Ascending = !field.Descending };
                result.Add(sort);
            }

            return result;
        }

        private static string BuildSortLocal(IEnumerable<SortField> sortFields)
        {
            if (sortFields == null) return null;

            var result = "";

            foreach (var field in sortFields)
            {
                var sort = string.Format("{0}{1}", field.PropertyName, field.Descending ? " DESC" : "");
                result = result + (result.Length > 0 ? ", " : "") + sort;
            }

            return result;
        }

        private class DapSortField : ISort
        {
            public bool Ascending {get; set;}
            public string PropertyName { get; set; }
        }

    }

    public class FameFieldPredicate<T> : FieldPredicate<T>
        where T : class
    {
        public StringCompareModes StringCompareMode { get; set; }
        public string StringCompareFormatLower { get; set; }
        public string StringCompareFormatUpper { get; set; }

        public override string GetSql(ISqlGenerator sqlGenerator, IDictionary<string, object> parameters)
        {
            if (StringCompareMode == StringCompareModes.Default || !(Value is string))
                return base.GetSql(sqlGenerator, parameters);

            var columnName = GetColumnName(typeof (T), sqlGenerator, PropertyName);
            var parameterName = parameters.SetParameterName(PropertyName, Value, sqlGenerator.Configuration.Dialect.ParameterPrefix);
            return string.Format("({0} {1} {2})", FormatCompareMode(columnName), GetOperatorString(), FormatCompareMode(parameterName));
        }

        private string FormatCompareMode(string name)
        {
            switch (StringCompareMode)
            {
                case StringCompareModes.ForceLower:
                    name = string.Format(StringCompareFormatLower, name);
                    break;
                case StringCompareModes.ForceUpper:
                    name = string.Format(StringCompareFormatUpper, name);
                    break;
            }
            return name;
        }
    }
}