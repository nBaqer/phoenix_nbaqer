﻿using System;
using System.Collections.Generic;
using System.Data;
using Dapper;

namespace Fame.Orm.Dapper
{
    public class DapperUnitOfWork : IUnitOfWork
    {
        private readonly IRepositoryResolver _resolver;
        private readonly IConnectionFactory _connectionFactory;

        public IDbConnection DbConnection { get; private set; }
        public IDbTransaction DbTransaction { get; private set; }
        public bool WasRolledBack { get; private set; }

        public DapperUnitOfWork(IConnectionFactory connectionFactory, IRepositoryResolver resolver)
        {
            WasRolledBack = false;
            _resolver = resolver;
            _connectionFactory = connectionFactory;
        }

        public virtual void GetOpenConnection()
        {
            if (DbConnection != null) return;

            DbConnection = _connectionFactory.GetConnection();
            DbConnection.Open();
        }

        public virtual TRepository GetRegisteredRepository<TRepository>() where TRepository : class
        {
            return _resolver.GetRegisteredRepository<TRepository>(this);
        }

        public virtual IRepository<TEntity> GetRepositoryForEntity<TEntity>() where TEntity : class, IEntity, new()
        {
            return _resolver.GetRepositoryForEntity<TEntity>(this);
        }

        public virtual void BeginTransaction()
        {
            WasRolledBack = false;
            GetOpenConnection();
            if (DbTransaction != null) return;
            DbTransaction = DbConnection.BeginTransaction();
        }

        public virtual void Commit()
        {
            WasRolledBack = false;
            DbTransaction?.Commit();
        }

        public virtual void Rollback()
        {
            if (DbTransaction == null) return;
            DbTransaction.Rollback();
            WasRolledBack = true;
        }

        public virtual void Dispose()
        {
            DbTransaction?.Dispose();
            DbConnection?.Dispose();

            DbTransaction = null;
            DbConnection = null;
        }

        public virtual void ManageTransaction(Action action)
        {
            GetOpenConnection();
            var outerTxStarted = (DbTransaction != null);
            if (!outerTxStarted) BeginTransaction();
            action();
            if (!outerTxStarted) Commit();
        }

        /// <summary>
        /// Execute a native provider command that returns a list of entities
        /// </summary>
        /// <param name="command">A command definition object</param>
        /// <returns>A list of entities</returns>
        public IEnumerable<TEntity> Execute<TEntity>(ICommand<TEntity> command) where TEntity : class
        {
            GetOpenConnection();
            return DbConnection.Query<TEntity>(command.Command, command.Parameters, DbTransaction, true, null, command.CommandType);
        }

        /// <summary>
        /// Execute a native provider command that returns a list of objects.
        /// <para>This method should be used for cases where the caller wants to map the return/output
        /// of the Command to an object/data type that is different than TEntity.</para>
        /// </summary>
        /// <param name="command">A command definition object</param>
        /// <returns>A list of objects</returns>
        public IEnumerable<TOut> Execute<TEntity, TOut>(ICommand<TEntity, TOut> command) where TEntity : class
        {
            GetOpenConnection();
            return DbConnection.Query<TOut>(command.Command, command.Parameters, DbTransaction, true, null, command.CommandType);
        }

        /// <summary>
        /// Execute a native provider command that returns a single value
        /// </summary>
        /// <typeparam name="T">The type of the return value</typeparam>
        /// <param name="command">A command definition object</param>
        /// <returns></returns>
        public T Execute<T>(ICommandScalar<T> command) where T : struct
        {
            GetOpenConnection();
            return DbConnection.ExecuteScalar<T>(command.Command, command.Parameters, DbTransaction, null, command.CommandType);
        }

        /// <summary>
        /// Execute a native provider command
        /// </summary>
        /// <param name="command">A command definition object</param>
        public void Execute(ICommand command)
        {
            GetOpenConnection();
            DbConnection.Execute(command.Command, command.Parameters, DbTransaction, null, command.CommandType);
        }
    }
}