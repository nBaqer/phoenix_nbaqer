﻿using System.Data;

namespace Fame.Orm.Infrastructure
{
    /// <summary>
    /// Base class for a command that returns nothing.  Command should be implemented to return 
    /// a native provider command string, usually a SQL statement, stored procedure, etc.
    /// </summary>
    public class CommandBase : ICommand
    {
        /// <summary>
        /// The command type
        /// </summary>
        public string Command { get; protected set; }

        /// <summary>
        /// Returns the native provider command, usually a SQL statement, stored proc, etc.  The 
        /// command can contain parameters in the form "@Parm1", where "Parm1" is a property of the
        /// Parameters object
        /// </summary>
        public CommandType CommandType { get; protected set; }

        /// <summary>
        /// Returns an anonymous object whose property values will replace the parameters in the Command string.
        /// For example, {A = 1, B = "b"} - @A in the command will be replaced with 1, @B with "b"
        /// </summary>
        public object Parameters { get; protected set; }

        public CommandBase()
        {
            CommandType = CommandType.Text;
        }
    }

    public class CommandBase<TEntity> : CommandBase, ICommand<TEntity> where TEntity : class
    {
    }

    /// <summary>
    /// This constructor should be used for cases where the return/output
    /// of the Command needs to be an object/data type that is different than TEntity
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TOut"></typeparam>
    public class CommandBase<TEntity, TOut> : CommandBase, ICommand<TEntity, TOut> where TEntity : class
    {
    }

    public class CommandScalarBase<T> : CommandBase, ICommandScalar<T> where T : struct
    {
    }
}
