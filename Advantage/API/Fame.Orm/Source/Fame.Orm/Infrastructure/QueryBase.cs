﻿using System.Collections.Generic;

namespace Fame.Orm.Infrastructure
{
   /// <summary>
    /// Base class for queries that return one or more entities.  Filter and Sort should be
    /// implemented in the same manner as the arguments for the Repository GetList method.
    /// </summary>
    /// <typeparam name="TEntity">The entity type returned by the query</typeparam>   
    public class QueryBase<TEntity> : IQuery<TEntity> where TEntity : class, IEntity
    {
        /// <summary>
        /// A list of filter criteria lists.  See Reposity GetList for details.
        /// </summary>
        public IEnumerable<IEnumerable<FilterField>> Filters { get; protected set; }

        /// <summary>
        /// A list of sort fields.  See Reposity GetList for details.
        /// </summary>
        public IEnumerable<SortField> Sorts { get; protected set; }

    }
}
