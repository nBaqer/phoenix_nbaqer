﻿using System;
using System.Collections.Generic;

namespace Fame.Orm
{
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Get a reportitory based on it's type
        /// </summary>
        /// <typeparam name="TRepository">The repository's type</typeparam>
        /// <returns>A repository</returns>
        TRepository GetRegisteredRepository<TRepository>() where TRepository : class;

        /// <summary>
        /// Get a repository based on the entity type the repository handles
        /// </summary>
        /// <typeparam name="TEntity">The entity type</typeparam>
        /// <returns>A repository that handles the specified entity type</returns>
        IRepository<TEntity> GetRepositoryForEntity<TEntity>() where TEntity : class, IEntity, new();

        /// <summary>
        /// Indicates if the unit of work's transaction has already been rolled back
        /// </summary>
        bool WasRolledBack { get; }

        /// <summary>
        /// Perform an action before committing the transaction
        /// </summary>
        /// <param name="action"></param>
        void ManageTransaction(Action action);

        /// <summary>
        /// Begin the transaction
        /// </summary>
        void BeginTransaction();

        /// <summary>
        /// Commit the transaction
        /// </summary>
        void Commit();

        /// <summary>
        /// Rollback the transaction
        /// </summary>
        void Rollback();

        /// <summary>
        /// Execute a native provider command that returns a list of entities
        /// </summary>
        /// <param name="command">A command definition object</param>
        /// <returns>A list of entities</returns>
        IEnumerable<TEntity> Execute<TEntity>(ICommand<TEntity> command) where TEntity : class;

        /// <summary>
        /// Execute a native provider command that returns a list of objects.
        /// <para>This method should be used for cases where the caller wants to map the return/output
        /// of the Command to an object/data type that is different than TEntity.</para>
        /// </summary>
        /// <param name="command">A command definition object</param>
        /// <returns>A list of objects</returns>
        IEnumerable<TOut> Execute<TEntity, TOut>(ICommand<TEntity, TOut> command) where TEntity : class;

        /// <summary>
        /// Execute a native provider command that returns a single value
        /// </summary>
        /// <typeparam name="T">The type of the return value</typeparam>
        /// <param name="command">A command definition object</param>
        /// <returns></returns>
        T Execute<T>(ICommandScalar<T> command) where T : struct;

        /// <summary>
        /// Execute a native provider command
        /// </summary>
        /// <param name="command">A command definition object</param>
        void Execute(ICommand command);
    }
}
