﻿namespace Fame.Orm
{
    /// <summary>
    /// The UnitOfWork class will provide repository instances to the application by internally asking
    /// the resolver to create an instance for a specified inerface type.  The resolver could handle
    /// this via an IoC container that can determine the right concrete class for a specified interface, or 
    /// it could use a simple dictionary to map interfaces to classes and use the Activator to create instances.
    /// An ActivatorResolver class is provided in the Helper namespace enable this simple case.
    /// </summary>
    public interface IRepositoryResolver
    {
        TRepository GetRegisteredRepository<TRepository>(IUnitOfWork unitOfWork) where TRepository : class;
        IRepository<TEntity> GetRepositoryForEntity<TEntity>(IUnitOfWork unitOfWork) where TEntity : class, IEntity, new();
    }
}
