﻿namespace Fame.Orm
{
    /// <summary>
    /// Interface for a command that returns nothing.  Command should be implemented to return 
    /// a native provider command string, usually a SQL statement, stored procedure, etc.
    /// </summary>
    public interface ICommand
    {
        /// <summary>
        /// The command type
        /// </summary>
        System.Data.CommandType CommandType { get; }

        /// <summary>
        /// Returns the native provider command, usually a SQL statement, stored proc, etc.  The 
        /// command can contain parameters in the form "@Param1", where "Param1" is a property of the
        /// Parameters object
        /// </summary>
        string Command { get; }

        /// <summary>
        /// Returns an anonymous object whose property values will replace the parameters in the Command string or the arguments of a stored procedure
        /// </summary>
        object Parameters { get; }
    }

    /// <summary>
    /// Interface for commands that return one or more entities
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface ICommand<TEntity> : ICommand where TEntity : class
    {
    }

    /// <summary>
    /// Interface for commands that return one or more entities.
    /// <para>This interface should be used for cases where the return/output
    /// of the Command needs to be an object/data type that is different than TEntity</para>
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TOut"></typeparam>
    public interface ICommand<TEntity, TOut> : ICommand where TEntity : class
    {
    }

    /// <summary>
    /// Interface for commands that return a scalar value
    /// </summary>
    public interface ICommandScalar<T> : ICommand where T : struct
    {
    }
}
