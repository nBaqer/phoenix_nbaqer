﻿using System.Collections.Generic;

namespace Fame.Orm
{
    /// <summary>
    /// Interface for queries that return one or more entities  Filter and Sort should be
    /// implemented in the same manner as the arguments for the Repository GetList method.
    /// </summary>
    /// <typeparam name="TEntity">The entity type returned by the query</typeparam>
    public interface IQuery<TEntity>  where TEntity : class, IEntity
    {
        /// <summary>
        /// A list of filter criteria lists.  See Reposity GetList for details.
        /// </summary>
        IEnumerable<IEnumerable<FilterField>> Filters { get; }

        /// <summary>
        /// A list of sort fields.  See Reposity GetList for details.
        /// </summary>
        IEnumerable<SortField> Sorts { get; }
    }
}
