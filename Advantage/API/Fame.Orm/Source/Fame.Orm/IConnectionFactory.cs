﻿using System.Data;

namespace Fame.Orm
{
    /// <summary>
    /// A ConnectionFactory must provide an instance of a DbConnection.  A typical implementation would
    /// lookup the connection string in the app/web config file.  A ConnectionFactory class is provided
    /// in the Helpers namespace that enabled this case.
    /// </summary>
    public interface IConnectionFactory
    {
        IDbConnection GetConnection();
    }
}
