using System;

namespace FAME.Extensions
{
    public static class NullHandlerExtensions
    {

        public static string EmptyOrDefaultToNull(this string input)
        {
            return (input == string.Empty || input == default(string)) ? null : input;
        }

        public static DateTime? EmptyOrDefaultToNull(this DateTime? input)
        {
            return (input == DateTime.MinValue || input == default(DateTime)) ? null : input;
        }
        
        public static string NullToEmpty(this string input)
        {
            if ((input == null) || (input.Trim() == "-1"))
                return "";
            else
                return input;
        }

        public static decimal? NullToEmpty(this decimal? input)
        {
            if ((input == null))
                return 0;
            else
                return NullToEmpty((decimal)input);
        }

        public static decimal NullToEmpty(this decimal input)
        {
            if (input == -1)
                return 0;
            else
                return input;
        }
        public static double? NullToEmpty(this double? input)
        {
            if ((input == null))
                return 0;
            else
                return NullToEmpty((double)input);
        }

        public static double NullToEmpty(this double input)
        {
            if ( input.Equals(-1.0))
                return 0;
            else
                return input;
        }

        public static int? NullToEmpty(this int? input)
        {
            if ((input == null))
                return 0;
            else
                return NullToEmpty((int)input);
        }

        public static int NullToEmpty(this int input)
        {
            if (input == -1)
                return 0;
            else
                return input;
        }

        public static long? NullToEmpty(this long? input)
        {
            if ((input == null))
                return 0;
            else
                return NullToEmpty((long)input);
        }

        public static long NullToEmpty(this long input)
        {
            if (input == -1)
                return 0;
            else
                return input;
        }

        public static bool? NullToEmpty(this bool? input)
        {
            if (input == null)
                return false;
            else
                return input;
        }

        public static DateTime? NullToEmpty(this DateTime? input)
        {
            if (input == null)
                return DateTime.MinValue;
            else
                return input;
        }
    }
}