using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace FAME.Extensions
{
    public static class EnumerableExtensions
    {
        [DebuggerStepThrough]
        public static void Each<T>(this IEnumerable<T> values, Action<T, int> eachAction)
        {
            var index = 0;
            foreach (var item in values)
            {
                eachAction(item, index++);
            }
        }

        [DebuggerStepThrough]
        public static void Each<T>(this IEnumerable<T> values, Action<T> eachAction)
        {
            foreach (var item in values)
            {
                eachAction(item);
            }
        }

        [DebuggerStepThrough]
        public static void Each(this IEnumerable values, Action<object> eachAction)
        {
            foreach (var item in values)
            {
                eachAction(item);
            }
        }

        public static IEnumerable<TParent> Join<TParent, TChild, TKey>
        (
            this IEnumerable<TParent> parent,
            IEnumerable<TChild> child,
            Func<TParent, TKey> parentKey,
            Func<TChild, TKey> childKey,
            Action<TParent, IEnumerable<TChild>> addChildren
        )
        {
            parent = parent.ToList();
            var childMap = child
                .GroupBy(s => childKey(s))
                .ToDictionary(g => g.Key, g => g.AsEnumerable());

            foreach (var item in parent)
            {
                IEnumerable<TChild> children;
                if (childMap.TryGetValue(parentKey(item), out children))
                    addChildren(item, children);
            }

            return parent;
        }
    }
}