﻿using System.Collections.Generic;

namespace FAME.Extensions
{
    public static class CollectionExtensions
    {
        public static T2 GetDictionaryValueOrDefault<T1, T2>(this Dictionary<T1, T2> input, T1 key)
        {
            T2 result;
            return input.TryGetValue(key, out result) ? result : default(T2);
        }
    }
}