namespace FAME.Extensions
{
    public static class BooleanExtensions
    {
        public static string ToYesNo(this bool value)
        {
            return value ? "Yes" : "No";
        }

        public static string ToYN(this bool value)
        {
            return value.ToYesNo().Left(1);
        }

        public static bool YNToBool(this string value)
        {
            return value.Trim() == "Y" ? true : false;
        }

        public static bool YesNoToBool(this string value)
        {
            return value.Left(1).YNToBool();
        }
    }
}