using System;
using System.Collections.Generic;
using System.Globalization;

namespace FAME.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool IsEmpty(this DateTime input)
        {
            return (input == DateTime.MinValue);
        }

        public static bool IsNullOrEmpty(this DateTime? input)
        {
            return (input == null || input == DateTime.MinValue);
        }

        public static DateTime IfEmpty(this DateTime input, DateTime defaultVal)
        {
            return (input.IsEmpty() ? defaultVal : input);
        }

        public static DateTime? IfNullOrEmpty(this DateTime? input, DateTime defaultVal)
        {
            return (input.IsNullOrEmpty() ? defaultVal : input);
        }

        public static DateTime? IfNullOrEmpty(this DateTime? input, DateTime? defaultVal)
        {
            return (input.IsNullOrEmpty() ? defaultVal : input);
        }

        public static string ToDateString(this DateTime? input)
        {
            return input.ToDateString("");
        }

        public static string ToDateString(this DateTime? input, string separator)
        {
            return ((DateTime)(input == null ? DateTime.MinValue : input)).ToDateString(separator);
        }

        public static string ToDateString(this DateTime input)
        {
            return input.ToDateString("");
        }

        public static string ToDateString(this DateTime input, string separator)
        {
            return input > DateTime.MinValue ? string.Format("{0:0000}" + separator + "{1:00}" + separator + "{2:00}", input.Year, input.Month, input.Day) : "";
        }

        public static string ToTimeString(this DateTime? input)
        {
            return input.ToTimeString(":");
        }

        public static string ToTimeString(this DateTime? input, string separator)
        {
            return ((DateTime)(input == null ? DateTime.MinValue : input)).ToTimeString(separator);
        }

        public static string ToTimeString(this DateTime input)
        {
            return input.ToTimeString(":");
        }

        public static string ToTimeString(this DateTime input, string separator = ":", bool milliseconds = false)
        {
            var result = "";
            if (input > DateTime.MinValue)
            {
                result = string.Format("{0:00}" + separator + "{1:00}" + separator + "{2:00}", input.Hour, input.Minute, input.Second);
                if (milliseconds)
                    result = result + string.Format(separator + "{0:000}", input.Millisecond);
            }
            return result;
        }

        public static string ToStringYYYYMMDD(this DateTime input)
        {
            return input.ToString("yyyyMMdd");
        }

        public static string ToStringMMDDYYYY(this DateTime input, string separator = "")
        {
            return input.ToString(string.Format("MM{0}dd{1}yyyy", separator, separator));
        }

        public static string ToStringMMYYYY(this DateTime input, string separator = "")
        {
            return input.ToString(string.Format("MM{0}yyyy", separator));
        }

        public static string ToStringSqlFormat(this DateTime input)
        {
            return input.ToStringSqlFormat(false);
        }

        public static string ToStringSqlFormat(this DateTime? input)
        {
            return input == null ? "" : ((DateTime)input).ToStringSqlFormat(false);
        }

        public static string ToStringSqlFormat(this DateTime input, bool dateOnly)
        {
            return dateOnly ? input.ToString("yyyy-MM-dd") : input.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static string ToStringSqlFormat(this DateTime? input, bool dateOnly)
        {
            return input == null ? "" : ((DateTime)input).ToStringSqlFormat(dateOnly);
        }

        public static DateTime FromNow(this TimeSpan span)
        {
            return From(span, DateTime.Now);
        }

        public static DateTime FromToday(this TimeSpan span)
        {
            return From(span, DateTime.Today);
        }

        public static DateTime From(this TimeSpan span, DateTime startDate)
        {
            return startDate.Add(span);
        }

        public static DateTime Ago(this TimeSpan span)
        {
            return Since(span, DateTime.Now);
        }

        public static DateTime Since(this TimeSpan span, DateTime startDate)
        {
            return startDate.Add(-span);
        }

        public static DateTime ToTheDay(this DateTime date)
        {
            return date.RoundTicks(TimeSpan.TicksPerDay);
        }

        public static DateTime ToTheHour(this DateTime date)
        {
            return date.RoundTicks(TimeSpan.TicksPerHour);
        }

        public static DateTime ToTheMinute(this DateTime date)
        {
            return date.RoundTicks(TimeSpan.TicksPerMinute);
        }

        public static DateTime ToTheSecond(this DateTime date)
        {
            return date.RoundTicks(TimeSpan.TicksPerSecond);
        }

        public static DateTime ToTheMillisecond(this DateTime date)
        {
            return date.RoundTicks(TimeSpan.TicksPerMillisecond);
        }

        public static DateTime? ToTheDay(this DateTime? date)
        {
            if (!date.HasValue)
            {
                return null;
            }

            return date.Value.ToTheDay();
        }

        public static DateTime? ToTheHour(this DateTime? date)
        {
            if (!date.HasValue)
            {
                return null;
            }

            return date.Value.ToTheHour();
        }

        public static DateTime? ToTheMinute(this DateTime? date)
        {
            if (!date.HasValue)
            {
                return null;
            }

            return date.Value.ToTheMinute();
        }

        public static DateTime? ToTheSecond(this DateTime? date)
        {
            if (!date.HasValue)
            {
                return null;
            }

            return date.Value.ToTheSecond();
        }

        public static DateTime? ToTheMillisecond(this DateTime? date)
        {
            if (!date.HasValue)
            {
                return null;
            }

            return date.Value.ToTheMillisecond();
        }

        public static DateTime RoundTicks(this DateTime date, long interval)
        {
            return new DateTime(date.Ticks - (date.Ticks % interval), date.Kind);
        }

        public static bool IsInTheFuture(this DateTime date)
        {
            return date > DateTime.Now;
        }

        public static bool IsInThePast(this DateTime date)
        {
            return date < DateTime.Now;
        }

        public static bool IsNowOrInTheFuture(this DateTime date)
        {
            return date >= DateTime.Now;
        }

        public static bool IsNowOrInThePast(this DateTime date)
        {
            return date <= DateTime.Now;
        }

        public static DateTime? AddTime(this DateTime? date, string time)
        {
            if (date == null) return null;
            if (string.IsNullOrEmpty(time)) return date;
            var newDate = (DateTime)date;
            return newDate.AddTime(time);
        }

        public static DateTime AddTime(this DateTime date, string time)
        {
            if (string.IsNullOrEmpty(time)) return date;

            var dateAndTime = date.ToShortDateString() + " " + time;
            var returnValue = date;
            try { returnValue = DateTime.ParseExact(dateAndTime, "M/d/yyyy HH:mm:ss", CultureInfo.InvariantCulture); }
            catch { try { returnValue = DateTime.ParseExact(dateAndTime, "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture); } catch { } }
            return returnValue;
        }

        public static DateTime GetNextWeekday(this DateTime date)
        {
            var adjDates = new Dictionary<System.DayOfWeek, int>
            {
                {DayOfWeek.Saturday, 2},
                {DayOfWeek.Sunday, 1},
                {DayOfWeek.Monday, 0},
                {DayOfWeek.Tuesday, 0},
                {DayOfWeek.Wednesday, 0},
                {DayOfWeek.Thursday, 0},
                {DayOfWeek.Friday, 0}
            };

            return date.AddDays(adjDates[date.DayOfWeek]);
        }


        public static int DateDiffDays(DateTime startDate, DateTime endDate)
        {
            var timespan = (endDate - startDate);

            return Convert.ToInt16(timespan.TotalDays);

        }

        public static int DateDiffMonths(DateTime startDate, DateTime endDate)
        {
            return Math.Abs((startDate.Month - endDate.Month) + 12 * (startDate.Year - endDate.Year));

        }

        public static DateTime GetEndDate(this DateTime date, double numberofweeks)
        {
            return date.AddDays((Math.Ceiling(numberofweeks) * 7) - 1);
        }

        /// <summary>
        /// returns the total number of days in between, including the start and End Dates
        /// </summary>
        /// <param name="endDate"></param>
        /// <param name="startDate"></param>
        /// <returns></returns>
        public static double TotalDaysIncludesStartEndDates(this DateTime endDate, DateTime startDate)
        {
           
            return endDate.Subtract(startDate).TotalDays + 1;

        }


        public static bool IsDate(string date)
        {
            try
            {
                var dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static DateTime EndOfTheMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1).AddMonths(1).AddDays(-1);
        }

        public static DateTime FirstOfTheMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }



    }
}