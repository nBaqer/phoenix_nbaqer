﻿namespace FAME.Extensions
{
    public static class RegExPatterns
    {
        public static class Inclusion
        {
            public const string DigitsOnly = "[^0-9]";

            public static class Client
            {
                public const string AltId = "^[a-zA-Z]{3}[0-9]{3}";
                public const string KissId = "^[0-9]{4}";
                public const string SchoolId = "^[0-9]{6}";
                public const string SchoolIdAndContCode = "^[0-9]{6}[0-9a-zA-Z ]{3}"; //Space is part of a valid ContCode here
                public const string VendorCode = "^[e|gE|G]{1}[0-9]{5}";
            }
        }

        public static class Exclusion
        {
            public const string Default = @"[?:\*:/'\\[\]]";
            public const string Currency = @"[?:\*:$,/'\\[\]]";
            public const string ForFameConnectFolder = @"[?:\*:/'\\]";
        }

    }
}
