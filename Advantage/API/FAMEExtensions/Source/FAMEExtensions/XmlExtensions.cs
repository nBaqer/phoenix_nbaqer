using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace FAME.Extensions
{
    public static class XmlExtensions
    {
        public static string ToXml<T>(this T value) where T : class
        {
            return ToXml(value, typeof(T));
        }

        public static string ToXml(this object value, Type type)
        {
            var serializer = new XmlSerializer(type);
            var stringWriter = new StringWriter();
            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add("", "");

            var xmlWriter = XmlWriter.Create(stringWriter, new XmlWriterSettings
            {
                OmitXmlDeclaration = true,
                Indent = true,
                IndentChars = "\t",
            });

            using (xmlWriter)
            {
                serializer.Serialize(xmlWriter, value, namespaces);
            }

            return stringWriter.ToString();
        }
    }
}