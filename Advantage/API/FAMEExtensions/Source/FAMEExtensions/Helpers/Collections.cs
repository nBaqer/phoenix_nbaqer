﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FAME.Extensions.Helpers
{
    public static class Collections
    {

        /// <summary>
        /// Generic method to get the value from a Dictionary
        /// Usage: Collections.GetDictionaryValue&lt;string, string&gt;(DictionaryObject, KeyValue)
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static TValue GetDictionaryValue<TKey, TValue>(Dictionary<TKey, TValue> dictionary, TKey key)
        {
            TValue result;
            dictionary.TryGetValue(key, out result);
            return result;
        }

        /// <summary>
        /// Generic method to get the value from a Dictionary
        /// Usage: Collections.GetDictionaryValue&lt;string, string&gt;(DictionaryObject, KeyValue, "")
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static TValue GetDictionaryValue<TKey, TValue>(Dictionary<TKey, TValue> dictionary, TKey key, TValue defaultValue)
        {
            TValue result;
            return (dictionary.TryGetValue(key, out result)) ? result : defaultValue;
        }



    }
}
