﻿/*
 *  Date        By          Story/Defect #      Description        
 * 2016-05-23   cbryant     US8972              Fixed issue with reflection when types cannot be found
 * 
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace FAME.Extensions.Helpers
{
    public static class Reflection
    {
        /// <summary>
        /// Scans loaded assemblies and returns a list of class types that implement the specified interface
        /// </summary>
        /// <typeparam name="T">The interface that must be implemented for the class to be included</typeparam>
        /// <returns></returns>
        public static List<Type> FindClassesImplementing<T>() where T : class
        {
            return FindClassesImplementing<T>(t => true);
        }

        /// <summary>
        /// Scans loaded assemblies and returns a list of class types that implement the specified interface
        /// </summary>
        /// <typeparam name="T">The interface that must be implemented for the class to be included</typeparam>
        /// <param name="nameSpace">The class' namespace must start with this to be included</param>
        /// <returns></returns>
        public static List<Type> FindClassesImplementing<T>(string nameSpace) where T : class
        {
            return FindClassesImplementing<T>(t => t.Namespace != null && t.Namespace.StartsWith(nameSpace));
        }

        /// <summary>
        /// Scans loaded assemblies and returns a list of class types that implement the specified interface
        /// </summary>
        /// <typeparam name="T">The interface that must be implemented for the class to be included</typeparam>
        /// <param name="includeType">Function that accepts a type and returns a bool determining if the class should be included or not</param>
        /// <returns></returns>
        public static List<Type> FindClassesImplementing<T>(Func<Type, bool> includeType) where T : class
        {
            var type = typeof(T);
            return AppDomain.CurrentDomain.GetAssemblies().ToList()
                .SelectMany(GetTypesSafely)
                .Where(t => type.IsAssignableFrom(t) && t.IsClass && includeType.Invoke(t)).ToList();
        }

        /// <summary>
        /// Scans loaded assemblies and returns a list of class types
        /// </summary>
        /// <param name="nameSpace">The class' namespace must start with this to be included</param>
        /// <returns>List of classes in the specified namespace</returns>
        public static List<Type> FindClasses(string nameSpace)
        {
            return FindClasses(t => t.Namespace != null && t.Namespace.StartsWith(nameSpace));
        }

        /// <summary>
        /// Scans loaded assemblies and returns a list of class types
        /// </summary>
        /// <param name="includeType">Function that accepts a type and returns a bool determining if the class should be included or not</param>
        /// <returns>List of classes meeting the specified include filter</returns>
        public static List<Type> FindClasses(Func<Type, bool> includeType)
        {
            return AppDomain.CurrentDomain.GetAssemblies().ToList()
                .SelectMany(GetTypesSafely)
                .Where(t => t.IsClass && includeType.Invoke(t)).ToList();
        }

        /// <summary>
        /// Invokes a static void method of a given type
        /// </summary>
        /// <param name="type">The type containing the method</param>
        /// <param name="method">The name of the method</param>
        /// <param name="parameters">An array of parameters to be passed to the method</param>
        public static void InvokeVoidStaticMethod(Type type, string method, object[] parameters = null)
        {
            var methodInfo = type.GetMethod(method);

            if (methodInfo != null)
                methodInfo.Invoke(null, parameters);
        }

        /// <summary>
        /// Invokes a static method of a given type that returns type TReturn
        /// </summary>
        /// <typeparam name="TReturn">The type the method returns</typeparam>
        /// <param name="type">The type containing the method</param>
        /// <param name="method">The name of the method</param>
        /// <param name="parameters">An array of parameters to be passed to the method</param>
        /// <returns></returns>
        public static TReturn InvokeStaticMethod<TReturn>(Type type, string method, object[] parameters = null)
        {
            TReturn result;
            var methodInfo = type.GetMethod(method);

            if (methodInfo != null)
                result = (TReturn)methodInfo.Invoke(null, parameters);
            else
                result = Activator.CreateInstance<TReturn>();

            return result;
        }

        private static IEnumerable<Type> GetTypesSafely(Assembly assembly)
        {
            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException ex)
            {
                return ex.Types.Where(x => x != null);
            }
        }



        /// <summary>
        /// Invokes a method of a given type that returns type TReturn
        /// </summary>
        /// <typeparam name="TReturn">The type the method returns</typeparam>
        /// <param name="instance">Instance of the object containing the method</param>
        /// <param name="method">The name of the method</param>
        /// <param name="parameters">An array of parameters to be passed to the method</param>
        /// <returns></returns>
        public static TReturn InvokeNonStaticMethod<TReturn>(object instance, string method, object[] parameters = null)
        {
            TReturn result;

            Type type = instance.GetType();
            var methodInfo = type.GetMethod(method);

            if (methodInfo != null)
                result = (TReturn)methodInfo.Invoke(instance, parameters);
            else
                result = Activator.CreateInstance<TReturn>();

            return result;
        }




    }
}
