﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace FAME.Extensions.Helpers
{
    public static class AppSettings
    {
        /// <summary>
        /// Gets a string value from AppSettings, defaulting to the specified value if setting not found
        /// </summary>
        /// <param name="settingName">The name of the setting to retrieve</param>
        /// <param name="defaultValue">The default value to use if seeting not found</param>
        /// <returns>The value of the setting</returns>
        public static string Get(string settingName, string defaultValue = "")
        {
            return Get<string>(settingName, defaultValue);
        }

        /// <summary>
        /// Gets a typed value from AppSettings, defaulting to the specified value if setting not found.  The string value of the AppSetting will be converted to the type specified by T.
        /// </summary>
        /// <param name="settingName">The name of the setting to retrieve</param>
        /// <param name="defaultValue">The default value to use if seeting not found</param>
        /// <returns>The value of the setting</returns>
        public static T Get<T>(string settingName, string defaultValue = "")
        {
            string settingValue = "";

            try
            { settingValue = ConfigurationManager.AppSettings[settingName].ToString(); }
            catch
            { }
            finally
            { if (string.IsNullOrWhiteSpace(settingValue)) settingValue = defaultValue; }

            return (T)(TypeDescriptor.GetConverter(typeof(T)).ConvertFromInvariantString(settingValue));
        }

        /// <summary>
        /// Checks if specified setting is present in AppSettings
        /// </summary>
        /// <param name="settingName">The name of the setting to check</param>
        /// <returns>true/false is setting is present/missing</returns>
        public static bool Has(string settingName)
        {
            var has = false;

            try
            { has = ConfigurationManager.AppSettings.AllKeys.Contains(settingName); }
            catch
            { has = false; }

            return has;
        }
    }
}
