﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


#region Change History
/******************************************************************************************************************
    '*      Ref#    Date                By          Action              Change Description                                                                                             
    '*      --------------------------------------------------------------------------------------------------------------                                                                                                                        
    '*       1      May.26.2016         AGS        Created             US8954 - added missing AcademicCalendar entries.
    '******************************************************************************************************************/
#endregion


namespace FAME.Extensions.Helpers
{
    public static class CodeLookup
    {
        public enum CodeSource { AFA, ESP }

        public enum CodeTypes
        {
            AcademicCalendar,
            AwardStatus,
            /// <summary>AFA AwardRuleCodes and  ESP award types</summary>
            AwardType,
            /// <summary>AFA AwardRuleCodes and  ESP sub-fund values</summary>
            AwardTypeSub,
            /// <summary>AFA AwardRuleCodes and  ESP sub-fund codes</summary>
            AwardTypeSubCodes,
            CODDisbursementStatus,
            CODLoanResponseStatus,
            CODLoanResponseCreditStatus,
            DependencyStatus,
            DisbursementStatusType,
            EnrollmentStatus,
            FameResponseStatusDL,
            FameResponseStatusPell,
            Gender,
            GradeLevel,
            HousingType,
            MaritalStatus,
            StudentProgramStatus,
            VerificationCode,
        }

        private enum AddMode { Both, AfaOnly, EspOnly }
        
        private static Dictionary<CodeTypes, Dictionary<string, Code>> _codesByAfa = new Dictionary<CodeTypes,Dictionary<string,Code>>();
        private static Dictionary<CodeTypes, Dictionary<string, Code>> _codesByEsp = new Dictionary<CodeTypes, Dictionary<string, Code>>();

        static CodeLookup()
        {
            LoadCodes();
        }

        private static void LoadCodes()
        {
            AddCode(new Code { Type = CodeTypes.AcademicCalendar, AfaCode = "1", EspCode = "1", Description = "Non-Standard Semester -Trimester" });
            AddCode(new Code { Type = CodeTypes.AcademicCalendar, AfaCode = "2", EspCode = "2", Description = "Quarter" });
            AddCode(new Code { Type = CodeTypes.AcademicCalendar, AfaCode = "3", EspCode = "3", Description = "Semester" });
            AddCode(new Code { Type = CodeTypes.AcademicCalendar, AfaCode = "4", EspCode = "4", Description = "Trimester" });
            AddCode(new Code { Type = CodeTypes.AcademicCalendar, AfaCode = "5", EspCode = "5", Description = "Clock Hour" });
            AddCode(new Code { Type = CodeTypes.AcademicCalendar, AfaCode = "6", EspCode = "1", Description = "Non-Term Quarter" }, AddMode.AfaOnly);
            AddCode(new Code { Type = CodeTypes.AcademicCalendar, AfaCode = "7", EspCode = "1", Description = "Non-Standard Quarter" }, AddMode.AfaOnly);
            AddCode(new Code { Type = CodeTypes.AcademicCalendar, AfaCode = "8", EspCode = "1", Description = "Non-Term Semester" }, AddMode.AfaOnly);
            AddCode(new Code { Type = CodeTypes.AcademicCalendar, AfaCode = "9", EspCode = "1", Description = "Non Standard Equal Length (NSE9W)" }, AddMode.AfaOnly);
            AddCode(new Code { Type = CodeTypes.AcademicCalendar, AfaCode = "10", EspCode = "1", Description = "Non-Standard Quarter - NSE9W" }, AddMode.AfaOnly);
            AddCode(new Code { Type = CodeTypes.AcademicCalendar, AfaCode = "12", EspCode = "1", Description = "Non Term-Trimester" }, AddMode.AfaOnly);
            AddCode(new Code { Type = CodeTypes.AcademicCalendar, AfaCode = "13", EspCode = "1", Description = "Non Standard Term-Trimester SE9W" }, AddMode.AfaOnly);
            AddCode(new Code { Type = CodeTypes.AcademicCalendar, AfaCode = "14", EspCode = "1", Description = "Non Standard Term-Trimester NSE9W" }, AddMode.AfaOnly);

            AddCode(new Code { Type = CodeTypes.AwardStatus, AfaCode = "1", EspCode = "NR", Description = "Pending" });
            AddCode(new Code { Type = CodeTypes.AwardStatus, AfaCode = "2", EspCode = "FT", Description = "Sent for Processing" });
            AddCode(new Code { Type = CodeTypes.AwardStatus, AfaCode = "3", EspCode = "QC", Description = "In Processing" });
            AddCode(new Code { Type = CodeTypes.AwardStatus, AfaCode = "4", EspCode = "QA", Description = "QC Approved" });
            AddCode(new Code { Type = CodeTypes.AwardStatus, AfaCode = "5", EspCode = "QR", Description = "QC Rejected" });
            AddCode(new Code { Type = CodeTypes.AwardStatus, AfaCode = "6", EspCode = "GA", Description = "COD Accepted" });
            AddCode(new Code { Type = CodeTypes.AwardStatus, AfaCode = "7", EspCode = "GR", Description = "COD Rejected" });
            AddCode(new Code { Type = CodeTypes.AwardStatus, AfaCode = "8", EspCode = "",   Description = "Originated" }, AddMode.AfaOnly);

            AddCode(new Code { Type = CodeTypes.AwardType, AfaCode = "DLPLUS",  EspCode = "P", Description = "DL PLUS Loan" });
            AddCode(new Code { Type = CodeTypes.AwardType, AfaCode = "DLSUB",   EspCode = "S", Description = "DL Subsidized Stafford Loan" });
            AddCode(new Code { Type = CodeTypes.AwardType, AfaCode = "DLUNSUB", EspCode = "U", Description = "DL Unsubsidized Stafford Loan" });
            AddCode(new Code { Type = CodeTypes.AwardType, AfaCode = "FSEOG",   EspCode = "E", Description = "FSEOG" });
            AddCode(new Code { Type = CodeTypes.AwardType, AfaCode = "FWS",     EspCode = "F", Description = "FWS" });
            AddCode(new Code { Type = CodeTypes.AwardType, AfaCode = "IASG",    EspCode = "I", Description = "IASG" });
            AddCode(new Code { Type = CodeTypes.AwardType, AfaCode = "PELL",    EspCode = "L", Description = "Pell Grant" });
            AddCode(new Code { Type = CodeTypes.AwardType, AfaCode = "PERKINS", EspCode = "K", Description = "Perkins Loan" });

            AddCode(new Code { Type = CodeTypes.AwardTypeSub, AfaCode = "DLPLUS",   EspCode = "06", Description = "DL PLUS Loan" });
            AddCode(new Code { Type = CodeTypes.AwardTypeSub, AfaCode = "DLSUB",    EspCode = "07", Description = "DL Subsidized Stafford Loan" });
            AddCode(new Code { Type = CodeTypes.AwardTypeSub, AfaCode = "DLUNSUB",  EspCode = "08", Description = "DL Unsubsidized Stafford Loan" });
            AddCode(new Code { Type = CodeTypes.AwardTypeSub, AfaCode = "FSEOG",    EspCode = "03", Description = "FSEOG" });
            AddCode(new Code { Type = CodeTypes.AwardTypeSub, AfaCode = "FWS",      EspCode = "05", Description = "FWS" });
            AddCode(new Code { Type = CodeTypes.AwardTypeSub, AfaCode = "IASG",     EspCode = "14", Description = "IASG" });
            AddCode(new Code { Type = CodeTypes.AwardTypeSub, AfaCode = "PELL",     EspCode = "02", Description = "Pell Grant" });
            AddCode(new Code { Type = CodeTypes.AwardTypeSub, AfaCode = "PERKINS",  EspCode = "04", Description = "Perkins Loan" });

            AddCode(new Code { Type = CodeTypes.AwardTypeSubCodes, AfaCode = "DLPLUS",  EspCode = "DL - PLUS",  Description = "DL PLUS Loan" });
            AddCode(new Code { Type = CodeTypes.AwardTypeSubCodes, AfaCode = "DLSUB",   EspCode = "DL - SUB",   Description = "DL Subsidized Stafford Loan" });
            AddCode(new Code { Type = CodeTypes.AwardTypeSubCodes, AfaCode = "DLUNSUB", EspCode = "DL - UNSUB", Description = "DL Unsubsidized Stafford Loan" });
            AddCode(new Code { Type = CodeTypes.AwardTypeSubCodes, AfaCode = "FSEOG",   EspCode = "SEOG",       Description = "FSEOG" });
            AddCode(new Code { Type = CodeTypes.AwardTypeSubCodes, AfaCode = "FWS",     EspCode = "CWS",        Description = "FWS" });
            AddCode(new Code { Type = CodeTypes.AwardTypeSubCodes, AfaCode = "IASG",    EspCode = "IASG",       Description = "IASG" });
            AddCode(new Code { Type = CodeTypes.AwardTypeSubCodes, AfaCode = "PELL",    EspCode = "PELL",       Description = "Pell Grant" });
            AddCode(new Code { Type = CodeTypes.AwardTypeSubCodes, AfaCode = "PERKINS", EspCode = "PERK",       Description = "Perkins Loan" });

            AddCode(new Code { Type = CodeTypes.CODDisbursementStatus, AfaCode = "5", EspCode = "R", Description = "COD Rejected" });
            AddCode(new Code { Type = CodeTypes.CODDisbursementStatus, AfaCode = "6", EspCode = " ", Description = "COD Accepted" });

            AddCode(new Code { Type = CodeTypes.CODLoanResponseStatus, AfaCode = "6", EspCode = "A", Description = "COD Accepted (Sub,Unsub)" });                                  //AFA codes are from AwardStatus - COD Accepted
            AddCode(new Code { Type = CodeTypes.CODLoanResponseStatus, AfaCode = "7", EspCode = "B", Description = "COD Rejected (Sub,Unsub,PLUS)" }, AddMode.EspOnly);            //AFA codes are from AwardStatus - COD Rejected
            AddCode(new Code { Type = CodeTypes.CODLoanResponseStatus, AfaCode = "6", EspCode = "C", Description = "COD Loan and Credit Accepted (PLUS)" }, AddMode.EspOnly);      //AFA codes are from AwardStatus - COD Accepted
            AddCode(new Code { Type = CodeTypes.CODLoanResponseStatus, AfaCode = "6", EspCode = "D", Description = "COD Loan Accepted, Credit Denied (PLUS)" }, AddMode.EspOnly);  //AFA codes are from AwardStatus - COD Accepted
            AddCode(new Code { Type = CodeTypes.CODLoanResponseStatus, AfaCode = "6", EspCode = "X", Description = "COD Loan Accepted, Credit Pending (PLUS)" }, AddMode.EspOnly); //AFA codes are from AwardStatus - COD Accepted

            AddCode(new Code { Type = CodeTypes.CODLoanResponseCreditStatus, AfaCode = "",  EspCode = "A", Description = "Loan Accepted" }, AddMode.EspOnly);
            AddCode(new Code { Type = CodeTypes.CODLoanResponseCreditStatus, AfaCode = "",  EspCode = "B", Description = "Loan Rejected" }, AddMode.EspOnly);
            AddCode(new Code { Type = CodeTypes.CODLoanResponseCreditStatus, AfaCode = "A", EspCode = "C", Description = "Credit Accepted" });
            AddCode(new Code { Type = CodeTypes.CODLoanResponseCreditStatus, AfaCode = "S", EspCode = "D", Description = "Credit Denied" });
            AddCode(new Code { Type = CodeTypes.CODLoanResponseCreditStatus, AfaCode = "P", EspCode = "X", Description = "Credit Pending" });

            AddCode(new Code { Type = CodeTypes.DependencyStatus, AfaCode = "D", EspCode = "D", Description = "Dependent" });
            AddCode(new Code { Type = CodeTypes.DependencyStatus, AfaCode = "I", EspCode = "I", Description = "Independent" });
            AddCode(new Code { Type = CodeTypes.DependencyStatus, AfaCode = "X", EspCode = "D", Description = "Dependent - Rejected" }, AddMode.AfaOnly);
            AddCode(new Code { Type = CodeTypes.DependencyStatus, AfaCode = "Y", EspCode = "I", Description = "Independent - Rejected" }, AddMode.AfaOnly);

            AddCode(new Code { Type = CodeTypes.DisbursementStatusType, AfaCode = "1", EspCode = "N", Description = "Pending" });
            AddCode(new Code { Type = CodeTypes.DisbursementStatusType, AfaCode = "2", EspCode = "S", Description = "Cash Order Sent" }, AddMode.AfaOnly);
            AddCode(new Code { Type = CodeTypes.DisbursementStatusType, AfaCode = "3", EspCode = "S", Description = "Pending Acceptance" }, AddMode.AfaOnly);
            AddCode(new Code { Type = CodeTypes.DisbursementStatusType, AfaCode = "4", EspCode = "S", Description = "Acceptance Sent" });
            AddCode(new Code { Type = CodeTypes.DisbursementStatusType, AfaCode = "5", EspCode = "E", Description = "COD Rejected" });
            AddCode(new Code { Type = CodeTypes.DisbursementStatusType, AfaCode = "6", EspCode = "A", Description = "Disbursed" });

            AddCode(new Code { Type = CodeTypes.EnrollmentStatus, AfaCode = "1", EspCode = "F", Description = "FullTime" });
            AddCode(new Code { Type = CodeTypes.EnrollmentStatus, AfaCode = "2", EspCode = "3", Description = "3QuarterTime" });
            AddCode(new Code { Type = CodeTypes.EnrollmentStatus, AfaCode = "3", EspCode = "1", Description = "HalfTime" });
            AddCode(new Code { Type = CodeTypes.EnrollmentStatus, AfaCode = "4", EspCode = "L", Description = "LessThanHalfTime" });
            AddCode(new Code { Type = CodeTypes.EnrollmentStatus, AfaCode = "4", EspCode = "B", Description = "LessThanHalfTime - FAComp" }, AddMode.EspOnly);

            AddCode(new Code { Type = CodeTypes.FameResponseStatusDL, AfaCode = "1", EspCode = "NR", Description = "Pending" });
            AddCode(new Code { Type = CodeTypes.FameResponseStatusDL, AfaCode = "2", EspCode = "FT", Description = "Sent for Processing" });
            AddCode(new Code { Type = CodeTypes.FameResponseStatusDL, AfaCode = "3", EspCode = "QC", Description = "In Processing" });
            AddCode(new Code { Type = CodeTypes.FameResponseStatusDL, AfaCode = "4", EspCode = "QA", Description = "Approved by QC" });
            AddCode(new Code { Type = CodeTypes.FameResponseStatusDL, AfaCode = "5", EspCode = "QR", Description = "Rejected by QC" });
            AddCode(new Code { Type = CodeTypes.FameResponseStatusDL, AfaCode = "6", EspCode = "GA", Description = "COD Accepted" });
            AddCode(new Code { Type = CodeTypes.FameResponseStatusDL, AfaCode = "7", EspCode = "GR", Description = "COD Rejected" });

            AddCode(new Code { Type = CodeTypes.FameResponseStatusPell, AfaCode = "1", EspCode = "NR", Description = "Not sent" });                    //AFA codes are from AwardStatus - Pending
            AddCode(new Code { Type = CodeTypes.FameResponseStatusPell, AfaCode = "2", EspCode = "NA", Description = "Sent" });                        //AFA codes are from AwardStatus - Sent for Processing 
            AddCode(new Code { Type = CodeTypes.FameResponseStatusPell, AfaCode = "4", EspCode = "  ", Description = "Approved" });                    //AFA codes are from AwardStatus - QC Accepted
            AddCode(new Code { Type = CodeTypes.FameResponseStatusPell, AfaCode = "5", EspCode = "RJ", Description = "Rejected" });                    //AFA codes are from AwardStatus - QC Rejected

            AddCode(new Code { Type = CodeTypes.Gender, AfaCode = "1", EspCode = "M", Description = "Male" });
            AddCode(new Code { Type = CodeTypes.Gender, AfaCode = "2", EspCode = "F", Description = "Female" });

            AddCode(new Code { Type = CodeTypes.GradeLevel, AfaCode = "0", EspCode = "U1", Description = "Undergrad - 1st year" }, AddMode.AfaOnly);
            AddCode(new Code { Type = CodeTypes.GradeLevel, AfaCode = "1", EspCode = "U1", Description = "Undergrad - 1st year" });
            AddCode(new Code { Type = CodeTypes.GradeLevel, AfaCode = "2", EspCode = "U2", Description = "Undergrad - 2nd year" });
            AddCode(new Code { Type = CodeTypes.GradeLevel, AfaCode = "3", EspCode = "U3", Description = "Undergrad - 3rd year" });
            AddCode(new Code { Type = CodeTypes.GradeLevel, AfaCode = "4", EspCode = "U4", Description = "Undergrad - 4th year" });
            AddCode(new Code { Type = CodeTypes.GradeLevel, AfaCode = "5", EspCode = "U5", Description = "Undergrad - 5th year" });
            AddCode(new Code { Type = CodeTypes.GradeLevel, AfaCode = "6", EspCode = "G1", Description = "Graduate - 1st year" });
            AddCode(new Code { Type = CodeTypes.GradeLevel, AfaCode = "7", EspCode = "G2", Description = "Graduate - 2nd year" });

            AddCode(new Code { Type = CodeTypes.HousingType, AfaCode = "1", EspCode = "O", Description = "On Campus" });
            AddCode(new Code { Type = CodeTypes.HousingType, AfaCode = "2", EspCode = "W", Description = "With Parent" });
            AddCode(new Code { Type = CodeTypes.HousingType, AfaCode = "3", EspCode = "N", Description = "Not With Parents" });

            AddCode(new Code { Type = CodeTypes.MaritalStatus, AfaCode = "1", EspCode = "U", Description = "Unarried - Single" });
            AddCode(new Code { Type = CodeTypes.MaritalStatus, AfaCode = "2", EspCode = "M", Description = "Married" });
            AddCode(new Code { Type = CodeTypes.MaritalStatus, AfaCode = "3", EspCode = "U", Description = "Unarried - Separated" }, AddMode.AfaOnly);
            AddCode(new Code { Type = CodeTypes.MaritalStatus, AfaCode = "4", EspCode = "U", Description = "Unarried - Divorced or widowed Blank" }, AddMode.AfaOnly);

            AddCode(new Code { Type = CodeTypes.StudentProgramStatus, AfaCode = "2", EspCode = "",  Description = "Enrolled" }, AddMode.AfaOnly);
            AddCode(new Code { Type = CodeTypes.StudentProgramStatus, AfaCode = "3", EspCode = "D", Description = "Dropped" });
            AddCode(new Code { Type = CodeTypes.StudentProgramStatus, AfaCode = "4", EspCode = "E", Description = "Deceased" });
            AddCode(new Code { Type = CodeTypes.StudentProgramStatus, AfaCode = "5", EspCode = "G", Description = "Graduated" });
            AddCode(new Code { Type = CodeTypes.StudentProgramStatus, AfaCode = "6", EspCode = "",  Description = "Returned from LOA" }, AddMode.AfaOnly);
            AddCode(new Code { Type = CodeTypes.StudentProgramStatus, AfaCode = "7", EspCode = "L", Description = "Leave of Absence" });
            AddCode(new Code { Type = CodeTypes.StudentProgramStatus, AfaCode = "8", EspCode = "X", Description = "No Show" });

            AddCode(new Code { Type = CodeTypes.VerificationCode, AfaCode = "V", EspCode = "A", Description = "Accurate (ISIR transaction number is 01)" });
            AddCode(new Code { Type = CodeTypes.VerificationCode, AfaCode = "V", EspCode = "C", Description = "Calculated (no longer used)" }, AddMode.EspOnly);
            AddCode(new Code { Type = CodeTypes.VerificationCode, AfaCode = " ", EspCode = "N", Description = "Not Selected" }, AddMode.EspOnly);
            AddCode(new Code { Type = CodeTypes.VerificationCode, AfaCode = "V", EspCode = "R", Description = "Reprocessed (ISIR transaction number is 02 or higher)" }, AddMode.EspOnly);
            AddCode(new Code { Type = CodeTypes.VerificationCode, AfaCode = "S", EspCode = "S", Description = "Selected, not verified (no longer used)" });
            AddCode(new Code { Type = CodeTypes.VerificationCode, AfaCode = "V", EspCode = "T", Description = "Within Tolerance" }, AddMode.EspOnly);
            AddCode(new Code { Type = CodeTypes.VerificationCode, AfaCode = " ", EspCode = "W", Description = "Processed Without Documentation" }, AddMode.EspOnly);
            AddCode(new Code { Type = CodeTypes.VerificationCode, AfaCode = " ", EspCode = " ", Description = "Incomplete or Ineligible" });
        }

        private static void AddCode(Code code, AddMode addMode = AddMode.Both)
        {
            if (addMode != AddMode.EspOnly)
            {
                if (!_codesByAfa.ContainsKey(code.Type))
                    _codesByAfa.Add(code.Type, new Dictionary<string, Code>());
                _codesByAfa[code.Type].Add(code.AfaCode, code);
            }

            if (addMode != AddMode.AfaOnly)
            {
                if (!_codesByEsp.ContainsKey(code.Type))
                    _codesByEsp.Add(code.Type, new Dictionary<string, Code>());
                _codesByEsp[code.Type].Add(code.EspCode, code);
            }
        }

        /// <summary>
        /// Get code by AFA value
        /// </summary>
        /// <param name="type">The code type</param>
        /// <param name="afaCode">The AFA code value</param>
        /// <returns>A Code object for the specified AFA value</returns>
        public static Code GetCodeByAfa(CodeTypes type, string afaCode)
        {
            if (_codesByAfa.ContainsKey(type) && _codesByAfa[type].ContainsKey(afaCode))
                return _codesByAfa[type][afaCode];
            else
                return new Code { Type = type, AfaCode = afaCode, EspCode = "", Description = "" };
        }

        /// <summary>
        /// Get code by ESP value
        /// </summary>
        /// <param name="type">The code type</param>
        /// <param name="espCode">The ESP code value</param>
        /// <returns>A Code object for the specified ESP value</returns>
        public static Code GetCodeByEsp(CodeTypes type, string espCode)
        {
            if (_codesByEsp.ContainsKey(type) && _codesByEsp[type].ContainsKey(espCode))
                return _codesByEsp[type][espCode];
            else
                return new Code { Type = type, AfaCode = "", EspCode = espCode, Description = "" };
        }

        /// <summary>
        /// Get all codes for the specified type
        /// </summary>
        /// <param name="type">The code type</param>
        /// <param name="source">The code source</param>
        /// <returns>A dictionary of of the requested codes</returns>
        public static Dictionary<string, Code> GetCodesByType(CodeTypes type, CodeSource source)
        {
            if ((source == CodeSource.AFA) && _codesByAfa.ContainsKey(type))
                return _codesByAfa[type];
            else if ((source == CodeSource.ESP) && _codesByEsp.ContainsKey(type))
                return _codesByEsp[type];
            else
                return new Dictionary<string, Code>();
        }

        /// <summary>
        /// Checks if the passed code is valid for the specified code type and source
        /// </summary>
        /// <param name="type">The code type</param>
        /// <param name="source">The code source</param>
        /// <param name="code">The code value to validate</param>
        /// <returns>True if code is valid for the specified code type and source</returns>
        public static bool ValidCode(CodeTypes type, CodeSource source, string code)
        {
            bool result;
            try   { result = CodeLookup.GetCodesByType(type, source).ContainsKey(code); }
            catch { result = false; }
            return result;
        }

        public class Code
        {
            public CodeTypes Type { get; internal set; }
            public string AfaCode { get; internal set; }
            public string EspCode { get; internal set; }
            public string Description { get; internal set; }
        }
    }


    public static class CodeLookupExtensions
    {
        public static string ToEspCode(this string afaCode, CodeLookup.CodeTypes codeType)
        {
            return CodeLookup.GetCodeByAfa(codeType, afaCode).EspCode;
        }

        public static string ToAfaCode(this string espCode, CodeLookup.CodeTypes codeType)
        {
            return CodeLookup.GetCodeByEsp(codeType, espCode).AfaCode;
        }

        public static bool IsValidEspCode(this string espCode, CodeLookup.CodeTypes codeType)
        {
            return CodeLookup.ValidCode(codeType, CodeLookup.CodeSource.ESP, espCode);
        }

        public static bool IsValidAfaCode(this string afaCode, CodeLookup.CodeTypes codeType)
        {
            return CodeLookup.ValidCode(codeType, CodeLookup.CodeSource.AFA, afaCode);
        }

    }
}
