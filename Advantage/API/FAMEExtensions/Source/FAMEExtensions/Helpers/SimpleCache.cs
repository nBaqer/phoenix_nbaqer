﻿using System;
using System.Collections.Specialized;
using System.Runtime.Caching;

namespace FAME.Extensions.Helpers
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Simple thread-safe wrapper for System.Runtime.Caching.MemoryCache.  The following
    /// cache settings can be specifed in in AppSettings config:
    /// SimpleCache\Enabled
    /// SimpleCache\CacheMemoryLimitMegabytes
    /// SimpleCache\PhysicalMemoryLimitPercentage
    /// SimpleCache\PollingInterval
    /// See <a href="https://msdn.microsoft.com/en-us/library/dd941872(v=vs.110).aspx">this MSDN article</a> 
    /// for more information.
    /// </summary>
    public static class SimpleCache
    {
        private const string CacheName = "SimpleCache";

        private static readonly NameValueCollection CacheConfig;

        private static MemoryCache _cache;
        private static bool _enabled;

        /// <summary>
        /// Enable/Disable the cache.  When disabled any existing cash will be 
        /// cleared and new items will not be added.  Allows the cache to be 
        /// globally disabled without having to alter any existing code.  This 
        /// property can be intialized from the "SimpleCache\Enabled" bool 
        /// AppSettings config item (defaults to true).
        /// </summary>
        public static bool Enabled {
            get { return _enabled; }
            set { if (!value) Clear(); _enabled = value; }
        }

        static SimpleCache()
        {
            Enabled = AppSettings.Get<bool>($@"{CacheName}\Enabled", "true");

            CacheConfig = new NameValueCollection()
            {
                { "cacheMemoryLimitMegabytes", AppSettings.Get($@"{CacheName}\CacheMemoryLimitMegabytes", "0") },
                { "physicalMemoryLimitPercentage", AppSettings.Get($@"{CacheName}\PhysicalMemoryLimitPercentage", "0") },
                { "pollingInterval", AppSettings.Get($@"{CacheName}\PollingInterval", "00:02:00") }
            };

            InitCache();
        }

        private static void InitCache()
        {
            _cache?.Dispose();
            _cache = new MemoryCache($"{CacheName}", CacheConfig);
        }

        /// <summary>
        /// Retrieve an item from the cache, adding the item if it doesn't exist
        /// </summary>
        /// <typeparam name="T">The type of the item to be retrieved</typeparam>
        /// <param name="key">The item's key in the cache</param>
        /// <param name="valueFactory">A function that will create the item if it doesn't exist in the cache</param>
        /// <param name="expirationSeconds">The number of seconds before the cached item expires.  Default is 0 (no expiration).</param>
        /// <returns>The cached item</returns>
        public static T GetOrAddExisting<T>(string key, Func<T> valueFactory, int expirationSeconds = 0)
        {
            var newValue = new Lazy<T>(valueFactory);
            if (!Enabled) return newValue.Value;

            var absoluteExpiration = expirationSeconds > 0 ? DateTimeOffset.Now.AddSeconds(expirationSeconds) : ObjectCache.InfiniteAbsoluteExpiration;
            var oldValue = _cache.AddOrGetExisting(key, newValue, absoluteExpiration) as Lazy<T>;

            try
            {
                return (oldValue ?? newValue).Value;
            }
            catch(Exception ex)
            {
                _cache.Remove(key);  //If lazy creation threw an error, need to remove key from cache
                throw ex;
            }
        }

        /// <summary>
        /// Get the specified item from the cache
        /// </summary>
        /// <typeparam name="T">The type of the item to retrieve</typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T Get<T>(string key) where T : class
        {
            var item = _cache.Get(key);
            return ((Lazy<T>) item)?.Value;
        }

        /// <summary>
        /// Remove the specified item from the cache
        /// </summary>
        /// <param name="key"></param>
        public static void Remove(string key)
        {
            _cache.Remove(key);
        }

        public static void RemoveLike(string key)
        {
            List<string> cacheKeys = _cache.Where(kvp => kvp.Key.Contains(key)).Select(kvp => kvp.Key).ToList();
            foreach (string cacheKey in cacheKeys)
            {
                _cache.Remove(cacheKey);
            }
        }
        /// <summary>
        /// Clears all items from the cache
        /// </summary>
        public static void Clear()
        {
            InitCache();
        }

        /// <summary>
        /// Check if the cache contains the specified item
        /// </summary>
        /// <param name="key">The item's key in the cache</param>
        /// <returns>True if cache contains the item, otherwise false</returns>
        public static bool Contains(string key)
        {
            return _cache.Contains(key);
        }

        /// <summary>
        /// Add an item to cache. It will update item if it exists.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expirationSeconds"></param>
        public static void Set<T>(string key, T value, int expirationSeconds = 0)
        {
            if (!Enabled) return;

            var absoluteExpiration = expirationSeconds > 0 ? DateTimeOffset.Now.AddSeconds(expirationSeconds) : ObjectCache.InfiniteAbsoluteExpiration;

            try
            {
                if (value == null) return;
                var newValue = new Lazy<T>(() => value);
                _cache.Set(key, newValue, absoluteExpiration);
            }
            catch
            {
                _cache.Remove(key);  
                throw;
            }
        }
        
        /// <summary>
        /// Returns a string to be used as Key for caching.
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public static string GetCacheKey(params string[] keys)
        {
            var result = string.Join("", keys).Trim();
            return result;
        }
        
    }
}