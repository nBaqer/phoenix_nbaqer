﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FAME.Extensions.Helpers
{
    public static class AwardYears
    {
        public enum Formats
        {
            /// <summary>2013-2014 = "4"</summary>
            OneDigit = 1,
            /// <summary>2013-2014 = "14"</summary>
            TwoDigit = 2,
            /// <summary>2013-2014 = "1314"</summary>
            FourDigit = 4,
            /// <summary>2013-2014 = "2014"</summary>
            FourDigitCalendarYear = -4,
            /// <summary>2013-2014 = "13-14"</summary>
            FourDigitFormatted = 5,
            /// <summary>2013-2014 = "2013-14"</summary>
            SixDigitFormatted = 7,
            /// <summary>2013-2014 = "2013-2014"</summary>
            EightDigitFormatted = 9
        }

        /// <summary>
        /// Return the current award year as of today in the specified format
        /// </summary>
        /// <param name="format">The desired award year format</param>
        /// <returns></returns>
        public static string GetCurrent(Formats format = Formats.FourDigitFormatted)
        {
            return GetCurrent(DateTime.Now, format);
        }

        /// <summary>
        /// Return the current award year as of the specified date in the specified format
        /// </summary>
        /// <param name="asOfDate">The date to determine the current award year</param>
        /// <param name="format">The desired award year format</param>
        /// <returns></returns>
        public static string GetCurrent(DateTime asOfDate, Formats format = Formats.FourDigitFormatted)
        {
            var year = asOfDate.Year;

            if (asOfDate.Month > 6)
                year = year + 1;
            
            return Format(year.ToString(), asOfDate, format);
        }

        /// <summary>
        /// Returns a list of 5 award years from the current award year in the specified format
        /// </summary>
        /// <returns></returns>
        public static List<string> GetList(Formats format = Formats.FourDigitFormatted)
        {
            return GetList(5, format);
        }

        /// <summary>
        /// Returns a list of award years, for the specified number, from the current award year in the specified format
        /// </summary>
        /// <param name="numberOfAwardYears"></param>
        /// <param name="format"></param>
        /// <param name="includeNextAwardYear">If month is less than 7, and this is true, we will include the new award year</param>
        /// <returns></returns>
        public static List<string> GetList(int numberOfAwardYears, Formats format, bool includeNextAwardYear = true)
        {
            var today = DateTime.Now;
            var asOfDate = includeNextAwardYear && today.Month < 7 ? new DateTime(today.Year, 7, 1) : today;
            var current = Convert.ToInt32(GetCurrent(asOfDate, Formats.FourDigitCalendarYear));
            var result = new List<string>();

            for (var year = current; year >= (current - numberOfAwardYears); year--)
                result.Add(Format(year.ToString(), format));

            return result;
        }

        /// <summary>
        /// Returns a list of award years, for the specified range, using the FourDigitFormatted format
        /// </summary>
        /// <param name="beginAwardYear"></param>
        /// <param name="endAwardYear"></param>
        /// <returns></returns>
        public static List<string> BuildList(string beginAwardYear, string endAwardYear)
        {
            return BuildList(beginAwardYear, endAwardYear, Formats.FourDigitFormatted);
        }

        /// <summary>
        /// Returns a list of award years, for the specified range, in the specified format
        /// </summary>
        /// <param name="beginAwardYear"></param>
        /// <param name="endAwardYear"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static List<string> BuildList(string beginAwardYear, string endAwardYear, Formats format)
        {
            var begin = Convert.ToInt32(Format(beginAwardYear, Formats.FourDigitCalendarYear));
            var end = Convert.ToInt32(Format(endAwardYear, Formats.FourDigitCalendarYear));
            var result = new List<string>();

            for (var year = begin; year <= end; year++)
                result.Add(Format(year.ToString(), format));

            return result;
        }

        /// <summary>
        /// Return the begining date of the sepcified award year
        /// </summary>
        /// <param name="awardYear"></param>
        /// <returns>The award year begin date</returns>
        public static DateTime GetBeginDate(string awardYear)
        {
            var year = Convert.ToInt32(Format(awardYear, Formats.FourDigitCalendarYear));
            return new DateTime(year - 1, 7, 1);
        }

        /// <summary>
        /// Return the ending date of the sepcified award year
        /// </summary>
        /// <param name="awardYear"></param>
        /// <returns>The award year end date</returns>
        public static DateTime GetEndDate(string awardYear)
        {
            var year = Convert.ToInt32(Format(awardYear, Formats.FourDigitCalendarYear));
            return new DateTime(year, 6, 30);
        }

        /// <summary>
        /// Checks if the specified date range falls into or overlaps the specified award year
        /// </summary>
        /// <param name="begDate">Period begin date</param>
        /// <param name="endDate">Period end date</param>
        /// <param name="awardYear">Award year to compare period to</param>
        /// <returns>True if period overlaps award year</returns>
        public static bool PeriodOverlaps(DateTime begDate, DateTime endDate, string awardYear)
        {
            return (begDate <= GetEndDate(awardYear) && endDate >= GetBeginDate(awardYear));
        }

        /// <summary>
        /// Compares award years to determine if the first year is less than, equal to or great than the second year
        /// </summary>
        /// <param name="awardYear1">The fist year to compare</param>
        /// <param name="awardYear2">The sacond year to compare</param>
        /// <returns>Same as string.Compare()</returns>
        public static int Compare(string awardYear1, string awardYear2)
        {
            awardYear1 = Format(awardYear1, Formats.FourDigitCalendarYear);
            awardYear2 = Format(awardYear2, Formats.FourDigitCalendarYear);
            return string.Compare(awardYear1, awardYear2);
        }

        /// <summary>
        /// Converts any award year format to any other. For example, FourDigit (1314) to SixDigitFormatted (2013-14).
        /// </summary>
        /// <param name="awardYear">The award year to covert</param>
        /// <param name="format">The format to convert to</param>
        /// <returns>The award year in the specified format</returns>
        public static string Format(string awardYear, Formats format = Formats.FourDigitFormatted)
        {
            var curentDate = DateTime.Now;
            return Format(awardYear, curentDate, format);
        }

        /// <summary>
        /// Converts any award year format to any other. For example, FourDigit (1314) to SixDigitFormatted (2013-14).
        /// </summary>
        /// <param name="awardYear">The award year to covert</param>
        /// <param name="curentDate">Used when awardYear is in a century ambiguous format and you don't want to assume current year</param>
        /// <param name="format">The format to convert to</param>
        /// <returns>The award year in the specified format</returns>
        public static string Format(string awardYear, DateTime curentDate, Formats format = Formats.FourDigitFormatted)
        {
            //Make sure award year can be converted to a number, if not, return the input value
            var testAwardYear = awardYear.ToDigits();
            if (!testAwardYear.Any()) return awardYear;

            var formattedAwardYear = "";
            var awdYr = 0;

            awardYear = awardYear.Trim();
            
            // Convert passed-in award year to a 4-digit award year
            if (awardYear.Length == 1)
            {
                awdYr = Convert.ToInt32(curentDate.Year.ToString().Left(3) + awardYear.Right(1));
                if (awdYr > curentDate.Year + 1)
                    awdYr = awdYr - 10;
                if (awdYr < curentDate.Year - 8) 
                    awdYr = awdYr + 10;
            }
            else if (awardYear.Length == 2 || awardYear.Length == 4 || awardYear.Length == 5)
            {
                awdYr = Convert.ToInt32(curentDate.Year.ToString().Left(2) + awardYear.Right(2));
                if (Math.Abs(awdYr - curentDate.Year) > 50)
                    if (awdYr < curentDate.Year)
                        awdYr = awdYr + 100;
                    else
                        awdYr = awdYr - 100;
            }
            else if (awardYear.Length == 7)
                awdYr = Convert.ToInt32(awardYear.Left(4)) + 1;
            else if (awardYear.Length == 9)
                awdYr = Convert.ToInt32(awardYear.Right(4));
   
            // And then convert calculated 4-digit year to the desired award year format
            if (awdYr > 0)
            {
                var firstLen = 0;
                var secondLen = 0;

                switch (format)
                {
                    case Formats.OneDigit:
                        firstLen = 0;
                        secondLen = 1;
                        break;
      
                    case Formats.TwoDigit:
                        firstLen = 0;
                        secondLen = 2;
                        break;
      
                    case Formats.FourDigitCalendarYear:
                        firstLen = 0;
                        secondLen = 4;
                        break;
      
                    case Formats.FourDigit:
                        firstLen = 2;
                        secondLen = 2;
                        break;
      
                    case Formats.FourDigitFormatted:
                        firstLen = 2;
                        secondLen = 2;
                        break;
      
                    case Formats.SixDigitFormatted:
                        firstLen = 4;
                        secondLen = 2;
                        break;
      
                    case Formats.EightDigitFormatted:
                        firstLen = 4;
                        secondLen = 4;
                        break;
      
                    default:
                        firstLen = 0;
                        secondLen = 0;
                        break;
                }

                if (firstLen > 0)
                    formattedAwardYear = formattedAwardYear + (awdYr - 1).ToString().Right(firstLen);
   
                if (Math.Abs((double) format) - (firstLen + secondLen) >= 1)
                    formattedAwardYear = formattedAwardYear + "-";
   
                if (secondLen > 0)
                    formattedAwardYear = formattedAwardYear + awdYr.ToString().Right(secondLen);
            }

            return formattedAwardYear;
        }
    }
}
