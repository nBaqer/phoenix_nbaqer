﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace FAME.Extensions.Helpers
{
    public static class Retry
    {
        private static int _defaultNumTries = 3;
        private static int _defaultDelaySecs = 15;

        /// <summary>
        /// Retries the method passed in as the argument (tryFunc). 
        /// <para />Note: Default value for number of retries is 3 and 
        /// for the delay seconds is 5.
        /// </summary>
        /// <typeparam name="T">Return type of method to be retried</typeparam>
        /// <param name="tryFunc">Method to be retried (Lambda expression)</param>
        /// <returns>The result of the lambda expression passed in</returns>
        public static T Execute<T>(Func<T> tryFunc)
        {
            return Execute<T>(tryFunc, null, _defaultNumTries, _defaultDelaySecs);
        }

        /// <summary>
        /// Retries the method passed in as the argument (tryFunc). 
        /// <para />Note: Default value for number of retries is 3 and 
        /// for the delay seconds is 5.
        /// </summary>
        /// <typeparam name="T">Return type of method to be retried</typeparam>
        /// <param name="tryFunc">Method to be retried (Lambda expression)</param>
        /// <param name="retryExceptionFilter">Exception filter to be retried (Lambda expression)</param>
        /// <returns>The result of the lambda expression passed in</returns>
        public static T Execute<T>(Func<T> tryFunc, Func<Exception, bool> retryExceptionFilter)
        {
            return Execute<T>(tryFunc, retryExceptionFilter, _defaultNumTries, _defaultDelaySecs);
        }

        /// <summary>
        /// Retries the method passed in as the argument (tryFunc). 
        /// </summary>
        /// <typeparam name="T">Return type of method to be retried</typeparam>
        /// <param name="tryFunc">Method to be retried (Lambda expression)</param>
        /// <param name="retryExceptionFilter">Exception filter to be retried (Lambda expression)</param>
        /// <param name="numTries"></param>
        /// <param name="delaySecs"></param>
        /// <returns>The result of the lambda expression passed in</returns>
        public static T Execute<T>(Func<T> tryFunc, Func<Exception, bool> retryExceptionFilter, int numTries, int delaySecs)
        {
            T result = default(T);

            for (int attempt = 1; attempt <= numTries; attempt++)
            {
                try
                {
                    result = tryFunc.Invoke();
                    break;
                }
                catch (Exception ex)
                {
                    if (retryExceptionFilter == null 
                        || retryExceptionFilter.Invoke(new Exception(ex.ToString() + "\n\n Error invoking: \n\n" + tryFunc.ToString())))
                        Thread.Sleep(delaySecs * 1000);
                    else
                        throw ex;
                }
            }

            return result;
        }
    }
}
