﻿using System;
using System.Linq;

namespace FAME.Extensions
{
    public static class ReflectionExtensions
    {
        public static T GetAttribute<T>(this Type type)
        {
            var attributeType = typeof(T);
            var attributes = type.GetCustomAttributes(attributeType, true);

            if (attributes.Length == 0)
            {
                throw new ApplicationException("Type does not have attribute of type {0}".ToFormat(attributeType.Name));
            }

            return (T)attributes[0];
        }

        public static object GetPropertyValue(this object input, string propertyName)
        {
            return input.GetType().GetProperty(propertyName).GetValue(input, null);
        }

        public static PropertyType GetPropertyValue<PropertyType>(this object input, string propertyName)
        {
            return (PropertyType)input.GetPropertyValue(propertyName);
        }

        public static PropertyType TryGetPropertyValue<PropertyType>(this object input, string propertyName, PropertyType defaultValue)
        {
            PropertyType result;
            try
            {
                result = (PropertyType)input.GetPropertyValue(propertyName);
            }
            catch
            {
                result = defaultValue;
            }
            return result;
        }

        public static bool HasAttribute(this object input, Type attributeType)
        {
            return input.GetType().GetCustomAttributes(false).Any(a => a.GetType() == attributeType);
        }
    }
}