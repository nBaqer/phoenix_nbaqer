﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace FAME.Extensions
{
    public static class NumericExtensions
    {
        public static bool InRange(this int value, int lowerBound, int upperBound)
        {
            return (value >= lowerBound && value <= upperBound);
        }

        public static bool Between(this int value, int lowerBound, int upperBound)
        {
            return (value > lowerBound && value < upperBound);
        }

        [DebuggerStepThrough]
        public static IEnumerable<int> Times(this int i)
        {
            return Enumerable.Range(0, i);
        }

        [DebuggerStepThrough]
        public static void Times(this int i, Action action)
        {
            Enumerable.Range(0, i).Each(c => action());
        }

        [DebuggerStepThrough]
        public static void Times(this int i, Action<int> action)
        {
            Enumerable.Range(0, i).Each(action);
        }

        [DebuggerStepThrough]
        public static bool IsNumericType(this object value)
        {
            return value is sbyte
                   || value is byte
                   || value is short
                   || value is ushort
                   || value is int
                   || value is uint
                   || value is long
                   || value is ulong
                   || value is float
                   || value is double
                   || value is decimal;
        }

        public static bool IsNumeric(this string value)
        {
            float output;
            return float.TryParse(value, out output);
        }

        public static double GetNextUnitStart(this double value)
        {
            double precisionFactor = 1;
            var remainder = (value) - Math.Floor(value);
            var remainderPrecisionSecondLevel = (remainder * 10) - Math.Floor(remainder * 10);
            if (remainder > 0)
            {
                precisionFactor = remainderPrecisionSecondLevel <= 0 ? 0.1 : 0.01;
            }
            return value + precisionFactor;
        }
    }
}