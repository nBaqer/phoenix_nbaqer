using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace FAME.Extensions
{
    public static class StringExtensions
    {
        
        [DebuggerStepThrough]
        public static string ToFormat(this string input, params object[] args)
        {
            return string.Format(input, args);
        }

        [DebuggerStepThrough]
        public static string ToFormat(this string input, IFormatProvider provider, params object[] args)
        {
            return string.Format(provider, input, args);
        }

        [DebuggerStepThrough]
        public static string Left(this string input, int length)
        {
            return input.Substring(0, input.Length < length ? input.Length : length);
        }

        [DebuggerStepThrough]
        public static string Right(this string input, int length)
        {
            return input.Substring(input.Length - (input.Length < length ? input.Length : length));
        }

        [DebuggerStepThrough]
        public static bool GreaterThan(this string input, string compare)
        {
            return (string.CompareOrdinal(input, compare) > 0);
        }

        [DebuggerStepThrough]
        public static bool GreaterThanOrEqual(this string input, string compare)
        {
            return (string.CompareOrdinal(input, compare) >= 0);
        }

        [DebuggerStepThrough]
        public static bool LessThan(this string input, string compare)
        {
            return (string.CompareOrdinal(input, compare) < 0);
        }

        [DebuggerStepThrough]
        public static bool LessThanOrEqual(this string input, string compare)
        {
            return (string.CompareOrdinal(input, compare) <= 0);
        }

        [DebuggerStepThrough]
        public static bool Between(this string input, string from, string to)
        {
            return (input.GreaterThanOrEqual(from) && input.LessThanOrEqual(to));
        }

        [DebuggerStepThrough]
        public static bool InList(this string input, List<string> list)
        {
            return list != null && list.Count > 0 && list.Contains(input);
        }

        [DebuggerStepThrough]
        public static string IfNullOrEmpty(this string input, string defaultVal)
        {
            return string.IsNullOrEmpty(input) ? defaultVal : input;
        }

        [DebuggerStepThrough]
        public static string IfNullOrWhitespace(this string input, string defaultVal)
        {
            return string.IsNullOrWhiteSpace(input) ? defaultVal : input;
        }

        [DebuggerStepThrough]
        public static string IfNonInteger(this string input, string defaultVal)
        {
            int tmpResult;
            if (int.TryParse(input, out tmpResult))
            {
                return input;
            }
            return defaultVal;
        }

        [DebuggerStepThrough]
        public static decimal ToDecimal(this string input)
        {
            decimal successResult;
            return decimal.TryParse(input, out successResult) ? successResult : 0;
        }

        [DebuggerStepThrough]
        public static bool IsNullOrEmpty(this string input)
        {
            return string.IsNullOrEmpty(input);
        }

        [DebuggerStepThrough]
        public static bool IsNullOrWhiteSpace(this string input)
        {
            return string.IsNullOrWhiteSpace(input);
        }

        /// <summary>
        /// Removes all characters from a string that match the invalidRegExp (regular expression)
        /// </summary>
        /// <param name="input"></param>
        /// <param name="invalidRegExp"></param>
        /// <returns></returns>
        public static string CleanString(this string input, string invalidRegExp = RegExPatterns.Exclusion.Default)
        {
            if (string.IsNullOrEmpty(input)) return string.Empty;
            var result = Regex.Replace(input, invalidRegExp, "");
            return result;
        }

        /// <summary>
        /// Removes all characters from a string that does not match the whiteListRegExp (regular expression)
        /// </summary>
        /// <param name="input"></param>
        /// <param name="whiteListRegExp"></param>
        /// <returns></returns>
        private static string KeepOnly(this string input, string whiteListRegExp)
        {
            if (string.IsNullOrEmpty(input)) return string.Empty;
            var result = Regex.Replace(input, whiteListRegExp, "").Trim();
            return result;
        }

        /// <summary>
        /// Removes all non numeric characters from a string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToDigits(this string input)
        {
            return input.KeepOnly(RegExPatterns.Inclusion.DigitsOnly);
        }

        /// <summary>
        /// Formats a string to SSN format
        ///     i.e.: 666-77-8888
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToFormatSsn(this string input)
        {
            if (string.IsNullOrEmpty(input)) return string.Empty;

            var digitsOnly = input.ToDigits().PadRight(9);
            return digitsOnly.Length < 9 ? input : $"{digitsOnly.Left(3)}-{digitsOnly.Substring(3, 2)}-{digitsOnly.Right(4)}";
        }

        /// <summary>
        ///  Formats a number string into currency.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>Currency formatted number or original input string.</returns>
        public static string ToFormatCurrency(this string input)
        {
            if (string.IsNullOrEmpty(input)) return string.Empty;

            decimal parseResult;
            return decimal.TryParse(input, out parseResult) ? parseResult.ToString("C2") : input;
        }

        /// <summary>
        /// Formats a string to phone number format
        ///     i.e.: 555-666-7777
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToFormatPhone(this string input)
        {
            var digitsOnly = input.ToDigits().PadRight(10);
            return string.Format("{0}-{1}-{2}", digitsOnly.Left(3), digitsOnly.Substring(3, 3), digitsOnly.Right(4));
        }

        /// <summary>
        /// Writes the string to the specified file.  The full directory path will be created if it doesn't already exist.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="path">Output file path</param>
        /// <param name="append">Append to existing file</param>
        /// <returns></returns>
        public static void ToFile(this string input, string path, bool append = false)
        {

            var fileInfo = new FileInfo(path);

            if (fileInfo.DirectoryName != null && !Directory.Exists(fileInfo.DirectoryName))
                Directory.CreateDirectory(fileInfo.DirectoryName);

            var file = new StreamWriter(path, append);
            file.Write(input);
            file.Close();
        }

        /// <summary>
        /// Tests if the string is a case-insesnsitive match for the specified RegEx expression
        /// </summary>
        /// <param name="input"></param>
        /// <param name="regEx"></param>
        /// <returns></returns>
        public static bool IsMatch(this string input, string regEx)
        {
            return input.IsMatch(regEx, false);
        }

        /// <summary>
        /// Tests if the string matches the specified RegEx expression
        /// </summary>
        /// <param name="input"></param>
        /// <param name="regEx"></param>
        /// <param name="caseSensitive"></param>
        /// <returns></returns>
        public static bool IsMatch(this string input, string regEx, bool caseSensitive)
        {
            return new Regex(regEx, caseSensitive ? RegexOptions.None : RegexOptions.IgnoreCase).IsMatch(input);
        }

        /// <summary>
        /// Tests if the string is a case-insesnsitive match for the specified wildcard pattern
        /// </summary>
        /// <param name="input"></param>
        /// <param name="wildcardPattern"></param>
        /// <returns></returns>
        public static bool IsMatchWildcard(this string input, string wildcardPattern)
        {
            return input.IsMatchWildcard(wildcardPattern, false);
        }

        /// <summary>
        /// Tests if the string matches the specified wildcard pattern
        /// </summary>
        /// <param name="input"></param>
        /// <param name="wildcardPattern"></param>
        /// <param name="caseSensitive"></param>
        /// <returns></returns>
        public static bool IsMatchWildcard(this string input, string wildcardPattern, bool caseSensitive)
        {
            var regEx = wildcardPattern;
            if (!regEx.StartsWith("^")) regEx = "^" + regEx;
            regEx = regEx.Replace(".", @"\.");
            regEx = regEx.Replace("?", ".");
            regEx = regEx.Replace("*", ".*?");
            regEx = regEx.Replace(@"\", @"\\");
            regEx = regEx.Replace(" ", @"\s");
            if (!regEx.EndsWith("$")) regEx = regEx + "$";
            return input.IsMatch(regEx, caseSensitive);
        }

        /// <summary>
        /// The trim.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string Trim(this string input)
        {
            return input?.Trim();
        }

        /// <summary>
        /// The trim event if null.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string TrimEventIfNull(this string input)
        {
            return input?.Trim();
        }

        /// <summary>
        /// Tests if two strings are the same using trimmed, invariant culture, case-insensitive comparison
        /// </summary>
        /// <param name="input"></param>
        /// <param name="compare"></param>
        /// <returns></returns>
        [DebuggerStepThrough]
        public static bool SameAs(this string input, string compare)
        {
            return (string.Compare(input.Trim(), compare.Trim(), StringComparison.InvariantCultureIgnoreCase) == 0);
        }

        public static string RemoveSpaces(this string input)
        {
            return string.IsNullOrEmpty(input) ? input : input.Replace(" ", "");
        }

        public static DateTime FromYyyyMmDdToDate(this string input)
        {
            if (string.IsNullOrEmpty(input)) return default(DateTime);
            if (input.Length != 8) return default(DateTime);

            var newDate = Convert.ToDateTime($"{input.Substring(4, 2)}/{input.Right(2)}/{input.Left(4)}");
            return newDate;
        }

        public static string Replace(this string input, string oldValue, string newValue, StringComparison comparisonType)
        {
            var result = input;
            var pos = result.IndexOf(oldValue, comparisonType);

            while (pos > -1)
            {
                result = result.Remove(pos, oldValue.Length).Insert(pos, newValue);
                pos = result.IndexOf(oldValue, pos + newValue.Length, comparisonType);
            }

            return result;
        }

        /// <summary>
        /// Word wraps the given text to fit within the specified width.
        /// </summary>
        /// <param name="text">Text to be word wrapped</param>
        /// <param name="width">Width, in characters, to which the text should be word wrapped</param>
        /// <returns>The word wrapped text</returns>
        public static string WordWrap(this string text, int width)
        {
            return text.WordWrap(width, 0);
        }

        /// <summary>
        /// Word wraps the given text to fit within the specified width, 
        /// inserting the specified indent at the beginning of each line
        /// </summary>
        /// <param name="text">Text to be word wrapped</param>
        /// <param name="width">Width, in characters, to which the text</param>
        /// <param name="indentLevel">The number of indentChars to include before each line</param>
        /// <param name="indentString">String that represents one indent level</param>
        /// <returns>The word wrapped and indented text</returns>
        public static string WordWrap(this string text, int width, int indentLevel, string indentString = "    ")
        {
            return text.WordWrap(width, indentLevel, indentLevel, indentString);
        }

        /// <summary>
        /// Word wraps the given text to fit within the specified width, 
        /// inserting the specified indent at the beginning of each line
        /// </summary>
        /// <param name="text">Text to be word wrapped</param>
        /// <param name="width">Width, in characters, to which the text should be word wrapped</param>
        /// <param name="firstIndentLevel">The number of indentStrings to include before the first line</param>
        /// <param name="subsequentIndentLevel">The number of indentStrings to include before subsequent line</param>
        /// <param name="indentString">String that represents one indent level</param>
        /// <returns>The word wrapped and indented text</returns>
        public static string WordWrap(this string text, int width, int firstIndentLevel, int subsequentIndentLevel, string indentString = "    ")
        {
            if (width < 1)
                return text;

            var indent = "";
            var indent2 = "";

            for (var indentLen = 1; indentLen <= firstIndentLevel; indentLen++)
                indent = indent + indentString;

            for (var indentLen = 1; indentLen <= subsequentIndentLevel; indentLen++)
                indent2 = indent2 + indentString;

            int pos, next;
            var sb = new StringBuilder();

            // Parse each line of text
            for (pos = 0; pos < text.Length; pos = next)
            {
                // Find end of line
                var eol = text.IndexOf(Environment.NewLine, pos, StringComparison.Ordinal);
                if (eol == -1)
                    next = eol = text.Length;
                else
                    next = eol + Environment.NewLine.Length;

                // Copy this line of text, breaking into smaller lines as needed
                if (eol > pos)
                {
                    do
                    {
                        var len = eol - pos;
                        if (len > width - indent.Length)
                            len = BreakLine(text, pos, width - indent.Length);
                        sb.Append(indent);
                        sb.Append(text, pos, len);
                        sb.Append(Environment.NewLine);

                        // Trim whitespace following break
                        pos += len;
                        while (pos < eol && char.IsWhiteSpace(text[pos]))
                            pos++;

                        indent = indent2;

                    } while (eol > pos);
                }
                else sb.Append(indent + Environment.NewLine); // Empty line
            }

            if (!text.EndsWith(Environment.NewLine))
                sb.Remove(sb.Length - Environment.NewLine.Length, Environment.NewLine.Length);

            return sb.ToString();
        }

        /// <summary>
        /// Converts the specified string to Title Case (except for words that are entirely in uppercase, which are considered to be acronyms)
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string ToTitleCase(this string text)
        {
            if (string.IsNullOrEmpty(text)) return string.Empty;

            var textLowerCase = text.ToLower();
            var ti = CultureInfo.CurrentCulture.TextInfo;
            var textTitleCase = ti.ToTitleCase(textLowerCase);
            return textTitleCase;
        }

        /// <summary>
        /// Locates position to break the given line so as to avoid
        /// breaking words.
        /// </summary>
        /// <param name="text">String that contains line of text</param>
        /// <param name="pos">Index where line of text starts</param>
        /// <param name="max">Maximum line length</param>
        /// <returns>The modified line length</returns>
        private static int BreakLine(string text, int pos, int max)
        {
            // Find last whitespace in line
            var i = max;
            while (i >= 0 && !char.IsWhiteSpace(text[pos + i]))
                i--;

            // If no whitespace found, break at maximum length
            if (i < 0)
                return max;

            // Find start of whitespace
            while (i >= 0 && char.IsWhiteSpace(text[pos + i]))
                i--;

            // Return length of text before whitespace
            return i + 1;
        }
    }
}