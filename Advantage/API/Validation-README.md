# Advantage API Validation
The Advantage API has built in functionality for validating models before an action from the controller is called. This functionality is what we are calling the DynamicValidation. DynamicValidation uses a fluent validation to accomplish complex business rules validations.

Sometimes the models requires certain rules to be combined with data from the Database that is not provided in the model and we would like to keep unified the validation aspect in one place that is not the service itself. 

Certain types of validations are more simple and using writing a full DynamicValidation it would be too much to code, which is why we could use Data Annotation for simple validations.
In case you require certain validation to happen on an specific type of request, let's say you want the Id to be required on the Update, Delete actions and not required on the Insert action,then we can use a custom required data annotation attribute.


## Validation Thru Data Annotations

A key benefit that Data Annotation Validation attributes bring to the table is that we can put our validation code by simply adding a tag on top of the property in a class, as opposed to
to Validation thru fluent validation where per each model we need to create a different validation and per property.

See Comparison below:

#### Validation Attribute

See required tag on lines 28 and 31.
![PackageConsole](_Documentation/images/DataValidationAtribute.PNG)

#### Fluent Vaidation

First we need to register Dynamic Validation
![PackageConsole](_Documentation/images/RegisterDynamicValidation.PNG)

Then we need to add code to validate each property to check if it is required
![PackageConsole](_Documentation/images/AddValidationPerProperty.PNG)

## More on Validation Attribute

Another key benefit is that many of the validations we need are already implemented for us.
A few example are Required,  Range, Regular Expression, StringLength, DataType.

In case we need to add custom validation we can do so, and we will only have the code in a single place for reusability.

#### Creating Custom Validation

We can easily create custom validation by inheriting from ValidationAttribute from System.ComponentModel.DataAnnotations

![PackageConsole](_Documentation/images/InheritingFromValidationAttribute.PNG)

The you will need to create a constructor based on the parameters you need and then finally override the ValidationResult IsValid(object value, ValidationContext validationContext) method, which will return ValidationResult.Success; when validation is successfull or a new ValidationResult(errorMessage: "error") if validation fails.

See below an example where the type of the request is passed to the model via constructor and is valid check wheather the type of request being executed matches the constructor type what is passed. Is so return ValidationResult.Success; otherwise, error is returned.

![PackageConsole](_Documentation/images/SampleCustomValidationAttribute.PNG)

Below how the custom validation attribute is applied to the model, on line 26
![PackageConsole](_Documentation/images/DataValidationAtribute.PNG)

#### In-model Validation

In case we need a really complex validation that would require passing multiple properties ( 2 or more ) we have the option to create validation inside the model class by simply making your model implement the IValidatableObject and its method IEnumerable`<ValidationResult`> Validate(ValidationContext validationContext) which will return an IEnumerable of all errors that your custom validation has failed to validate

See example below

The Lead DTO implements IValidatableObject
![PackageConsole](_Documentation/images/IValidatableObject.PNG)

The Lead class implements Validate method to check that at least an email or phone has been provided; otherwise, return an error validation result that was added to the list for validation results.

![PackageConsole](_Documentation/images/ValidateMethodSample.PNG)

Simple enough :) Happy Coding!
