﻿namespace FAME.Advantage.Models
{
    public class LeadSearchAddress
    {
        public string Address { get; set; }
        public string City { get; set; }
        public int Page { get; set; }
        public string[] Sort { get; set; }
    }
}
