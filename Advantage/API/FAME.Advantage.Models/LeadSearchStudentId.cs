﻿using System;

namespace FAME.Advantage.Models
{
    public class LeadSearchStudentId
    {
        public Guid StudentId { get; set; }
        public int Page { get; set; }
        public string[] Sort { get; set; }
    }
}
