﻿namespace FAME.Advantage.Models
{
    public class LeadSearchStudentNumber
    {
        public string StudentNumber { get; set; }
        public int Page { get; set; }
        public string[] Sort { get; set; }
    }
}
