﻿namespace FAME.Advantage.Models
{
    public class LeadSearchSsn
    {
        public string Ssn { get; set; }
        public int Page { get; set; }
        public string[] Sort { get; set; }
    }
}
