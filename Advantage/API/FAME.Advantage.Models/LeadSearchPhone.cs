﻿namespace FAME.Advantage.Models
{
    public class LeadSearchPhone
    {
        public string Phone { get; set; }
        public int Page { get; set; }
        public string[] Sort { get; set; }
    }
}
