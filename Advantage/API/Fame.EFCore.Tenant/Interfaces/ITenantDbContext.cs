﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fame.EFCore.Tenant.Interfaces
{
    using Fame.EFCore.Interfaces;

    /// <summary>
    /// The TenantDbContext interface.
    /// </summary>
    public interface ITenantDbContext : IDatabaseContext, IDisposable
    {
        void SaveChanges();
    }
}
