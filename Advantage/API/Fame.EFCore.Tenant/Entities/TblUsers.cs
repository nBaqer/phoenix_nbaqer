﻿using System;
using System.Collections.Generic;

namespace Fame.EFCore.Tenant.Entities
{
    public partial class TblUsers
    {
        public int Id { get; set; }
        public string Databasename { get; set; }
        public string UserId { get; set; }
        public string Email { get; set; }
        public bool? AccountActive { get; set; }
    }
}
