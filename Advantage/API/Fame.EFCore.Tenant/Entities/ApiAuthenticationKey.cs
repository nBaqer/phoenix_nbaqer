﻿using System;
using System.Collections.Generic;

namespace Fame.EFCore.Tenant.Entities
{
    public partial class ApiAuthenticationKey
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string Key { get; set; }
        public DateTime CreatedDate { get; set; }

        public Tenant Tenant { get; set; }
    }
}
