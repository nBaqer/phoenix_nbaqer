﻿using System;
using System.Collections.Generic;

namespace Fame.EFCore.Tenant.Entities
{
    public partial class Tenant
    {
        public Tenant()
        {
            ApiAuthenticationKey = new HashSet<ApiAuthenticationKey>();
            TenantAccess = new HashSet<TenantAccess>();
            TenantUsers = new HashSet<TenantUsers>();
            WapitenantCompanySecret = new HashSet<WapitenantCompanySecret>();
        }

        public int TenantId { get; set; }
        public string TenantName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ServerName { get; set; }
        public string DatabaseName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int EnvironmentId { get; set; }

        public Environment Environment { get; set; }
        public ICollection<ApiAuthenticationKey> ApiAuthenticationKey { get; set; }
        public ICollection<TenantAccess> TenantAccess { get; set; }
        public ICollection<TenantUsers> TenantUsers { get; set; }
        public ICollection<WapitenantCompanySecret> WapitenantCompanySecret { get; set; }
    }
}
