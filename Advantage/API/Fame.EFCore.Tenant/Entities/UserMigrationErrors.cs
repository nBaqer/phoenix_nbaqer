﻿using System;
using System.Collections.Generic;

namespace Fame.EFCore.Tenant.Entities
{
    public partial class UserMigrationErrors
    {
        public int MigrationId { get; set; }
        public string DatabaseName { get; set; }
        public string UserId { get; set; }
        public string Email { get; set; }
        public int? ErrorNumber { get; set; }
        public int? ErrorSeverity { get; set; }
        public string ErrorState { get; set; }
        public string ErrorProcedure { get; set; }
        public string ErrorLine { get; set; }
        public string ErrorMessage { get; set; }
        public string ModUser { get; set; }
        public DateTime? ModDate { get; set; }
        public Guid UserMigrationErrorId { get; set; }
    }
}
