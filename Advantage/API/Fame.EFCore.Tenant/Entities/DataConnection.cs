﻿using System;
using System.Collections.Generic;

namespace Fame.EFCore.Tenant.Entities
{
    public partial class DataConnection
    {
        public DataConnection()
        {
            TenantAccess = new HashSet<TenantAccess>();
        }

        public int ConnectionStringId { get; set; }
        public string ServerName { get; set; }
        public string DatabaseName { get; set; }
        public string UserName { get; set; }
        public byte[] PasswordEnc { get; set; }
        public string Password { get; set; }

        public ICollection<TenantAccess> TenantAccess { get; set; }
    }
}
