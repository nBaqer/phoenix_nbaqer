﻿using System;
using System.Collections.Generic;

namespace Fame.EFCore.Tenant.Entities
{
    public partial class RebusQueue
    {
        public long Id { get; set; }
        public string Recipient { get; set; }
        public int Priority { get; set; }
        public DateTime Expiration { get; set; }
        public DateTime Visible { get; set; }
        public byte[] Headers { get; set; }
        public byte[] Body { get; set; }
    }
}
