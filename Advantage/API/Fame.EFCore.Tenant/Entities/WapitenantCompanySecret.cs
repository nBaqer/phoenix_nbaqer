﻿using System;
using System.Collections.Generic;

namespace Fame.EFCore.Tenant.Entities
{
    public partial class WapitenantCompanySecret
    {
        public int Id { get; set; }
        public string ExternalCompanyCode { get; set; }
        public int TenantId { get; set; }

        public Tenant Tenant { get; set; }
    }
}
