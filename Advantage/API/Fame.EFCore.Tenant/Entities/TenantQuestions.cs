﻿using System;
using System.Collections.Generic;

namespace Fame.EFCore.Tenant.Entities
{
    public partial class TenantQuestions
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
