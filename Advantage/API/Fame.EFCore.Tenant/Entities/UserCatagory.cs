﻿using System;
using System.Collections.Generic;

namespace Fame.EFCore.Tenant.Entities
{
    public partial class UserCatagory
    {
        public int UserCategoryId { get; set; }
        public Guid UserId { get; set; }
        public bool? IsSupportUser { get; set; }
    }
}
