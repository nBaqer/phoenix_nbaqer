﻿using System;
using System.Collections.Generic;

namespace Fame.EFCore.Tenant.Entities
{
    public partial class Environment
    {
        public Environment()
        {
            Tenant = new HashSet<Tenant>();
            TenantAccess = new HashSet<TenantAccess>();
        }

        public int EnvironmentId { get; set; }
        public string EnvironmentName { get; set; }
        public string VersionNumber { get; set; }
        public string ApplicationUrl { get; set; }

        public ICollection<Tenant> Tenant { get; set; }
        public ICollection<TenantAccess> TenantAccess { get; set; }
    }
}
