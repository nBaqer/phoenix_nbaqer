﻿using System;
using System.Collections.Generic;

namespace Fame.EFCore.Tenant.Entities
{
    public partial class TenantAccess
    {
        public int TenantAccessId { get; set; }
        public int TenantId { get; set; }
        public int? EnvironmentId { get; set; }
        public int? ConnectionStringId { get; set; }

        public DataConnection ConnectionString { get; set; }
        public Environment Environment { get; set; }
        public Tenant Tenant { get; set; }
    }
}
