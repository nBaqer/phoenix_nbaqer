﻿using System;
using System.Collections.Generic;

namespace Fame.EFCore.Tenant.Entities
{
    public partial class TenantUsers
    {
        public int TenantUserId { get; set; }
        public int TenantId { get; set; }
        public Guid UserId { get; set; }
        public bool? IsDefaultTenant { get; set; }
        public bool? IsSupportUser { get; set; }

        public Tenant Tenant { get; set; }
        public AspnetUsers User { get; set; }
    }
}
