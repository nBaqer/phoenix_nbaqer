﻿using System;
using System.Collections.Generic;

namespace Fame.EFCore.Tenant.Entities
{
    public partial class WapiLog
    {
        public long Id { get; set; }
        public string TenantName { get; set; }
        public DateTime DateMod { get; set; }
        public int NumberOfRecords { get; set; }
        public string Company { get; set; }
        public string ServiceInvoqued { get; set; }
        public string OperationDescription { get; set; }
        public bool IsOk { get; set; }
        public string Comment { get; set; }
    }
}
