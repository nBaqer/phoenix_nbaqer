﻿using System;
using System.Collections.Generic;

namespace Fame.EFCore.Tenant.Entities
{
    public partial class ArchiveArGrdBkResults
    {
        public long IdArchive { get; set; }
        public DateTime? ArchiveDate { get; set; }
        public string ArchiveUser { get; set; }
        public Guid? GrdBkResultId { get; set; }
        public Guid? ClsSectionId { get; set; }
        public Guid? InstrGrdBkWgtDetailId { get; set; }
        public decimal? Score { get; set; }
        public string Comments { get; set; }
        public Guid? StuEnrollId { get; set; }
        public string ModUser { get; set; }
        public DateTime? ModDate { get; set; }
        public int? ResNum { get; set; }
        public DateTime? PostDate { get; set; }
        public bool? IsCompGraded { get; set; }
        public bool? IsCourseCredited { get; set; }
    }
}
