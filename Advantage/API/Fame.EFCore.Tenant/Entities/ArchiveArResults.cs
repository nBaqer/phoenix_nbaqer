﻿using System;
using System.Collections.Generic;

namespace Fame.EFCore.Tenant.Entities
{
    public partial class ArchiveArResults
    {
        public long IdArchive { get; set; }
        public DateTime? ArchiveDate { get; set; }
        public string ArchiveUser { get; set; }
        public Guid ResultId { get; set; }
        public Guid? TestId { get; set; }
        public decimal? Score { get; set; }
        public Guid? GrdSysDetailId { get; set; }
        public int? Cnt { get; set; }
        public int? Hours { get; set; }
        public Guid? StuEnrollId { get; set; }
        public bool? IsInComplete { get; set; }
        public bool? DroppedInAddDrop { get; set; }
        public string ModUser { get; set; }
        public DateTime? ModDate { get; set; }
        public bool? IsTransfered { get; set; }
        public bool? IsClinicsSatisfied { get; set; }
        public DateTime? DateDetermined { get; set; }
        public bool? IsCourseCompleted { get; set; }
        public bool? IsGradeOverridden { get; set; }
        public DateTime? GradeOverriddenDate { get; set; }
    }
}
