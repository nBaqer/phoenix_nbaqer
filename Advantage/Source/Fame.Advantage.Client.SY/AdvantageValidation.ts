﻿module SY {
    /*
     * To use this function you need to introduce in the master page the CSS
     * used by the function!!!!
     */

    // Messages on rules fail............................................
    const xValidatorMail = "Enter valid email";
    const xValidatorTel = "Enter valid phone number";
    const xValidatorInternaionalPhone = "Enter valid international phone number (min. 7 digit phone number)";
    const xValidatorDate = "Date format is not valid";
    const xValidatorDateTime = "Data time is not valid";
    const xValidatorTime = "Time entered is not valid";
    const xValidatorZip = "Enter Zip Code";
    const xValidatorSsn = "Enter a valid SSN";
    const xValidatorLetters = "Enter only letters";
    const xValidatorLettersNumbers = "Special symbol not allowed";
    const xValidatorNumbers = "Enter only numbers";
    const xValidatorRange = "must be between";
    const xValidatorDecimal = "Enter numbers or decimal";

    export function ADVANTAGE_VALIDATOR(wrapperName: string): kendo.ui.Validator {
        var validator = $(`#${wrapperName}`)
            .kendoValidator({
                validateOnBlur: true,
                errorTemplate: "",
                rules: {
                    required: (input) => {
                        if (!input.is("[required]")) {
                            return true;
                        }

                        let html = input;
                        if (input.is('[data-role="numerictextbox"]')) {
                            html = input.siblings("input").addClass("valueRequired");
                        }

                        if (input.is('[data-role="dropdownlist"]')) {
                            let parentItem = input.siblings(".k-dropdown-wrap");
                            html = parentItem.children(".k-input");
                        }

                        if (input.is('[data-role="combobox"]')) {
                            let parentItem = input.siblings(".k-dropdown-wrap");
                            html = parentItem.children("input");
                        }

                        let value = input.val();
                        if (value === undefined || value === null || value === "") {
                            html.addClass("valueRequired");
                            return false;
                        } else {
                            html.removeClass("valueRequired");
                            return true;
                        }
                    },

                    email: (input) => {

                        if (!input.is("[type=email]")) {
                            return true;
                        }

                        let email: boolean = isEmail($(input).val());
                        if (email) {
                            destroyTooltip(input);
                        } else {
                            createTooltip(xValidatorMail, input);
                        }
                        return email;
                    },

                    tel: (input) => {
                        if (!input.is("[type=tel]")) {
                            return true;
                        }
                        if (input.is("[data-role=maskedtextbox]")) {
                            //  Analysis of masked
                            var maskedtextbox = input.data("kendoMaskedTextBox");
                            // Mask can be American or international. International no has dashed
                            if (maskedtextbox.value().indexOf("-") === -1) {
                                destroyTooltip(input);
                                return true; // no validated if other than American
                            }
                            let masked = maskedtextbox.value().indexOf(maskedtextbox.options.promptChar) === -1;
                            if (masked) {
                                destroyTooltip(input);
                            } else {
                                createTooltip(xValidatorTel, input);
                            }
                            return masked;

                        }
                        if (input.is("[data-role=international]")) {
                            let isValidInternational = isInternationalPhone($(input).val().replace(/[\s()+\_\-\.]|ext/gi, ""));
                            if (isValidInternational) {
                                destroyTooltip(input);
                            } else {
                                createTooltip(xValidatorInternaionalPhone, input);
                            }
                            return isValidInternational;
                        } else {
                            // Fixed analysis to American format.
                            let tel: boolean = isTel($(input).val());
                            if (tel) {
                                destroyTooltip(input);
                            } else {
                                createTooltip(xValidatorTel, input);
                            }
                            return tel;
                        }
                    },

                    date: (input) => {
                        if (input.is("[type=date]") || input.is("[data-validation=date]")) {

                            let date: boolean = isdate1($(input).val());
                            if (date) {
                                destroyTooltip(input);
                            } else {
                                createTooltip(xValidatorDate, input);
                            }
                            return date;
                        }
                        return true;
                    },

                    datetime: (input) => {
                        if (input.is("[type=datetime]") || input.is("[data-validation=datetime]")) {
                            let isTime: boolean = isDateTime($(input).val());
                            if (isTime) {
                                destroyTooltip(input);
                            } else {
                                createTooltip(xValidatorDateTime, input);
                            }
                            return isTime;
                        }
                        return true;
                    },

                    time: (input) => {
                        if (input.is("[type=time]") || input.is("[data-validation=time]")) {
                            let isTime: boolean = isTime12Am($(input).val());
                            if (isTime) {
                                destroyTooltip(input);
                            } else {
                                createTooltip(xValidatorTime, input);
                            }
                            return isTime;
                        }
                        return true;
                    },

                    // Custom validation for Advantage

                    zip: (input) => {
                        if (!input.is("[data-validation=zip]")) {
                            return true;
                        }

                        if (input.is("[data-role=maskedtextbox]")) {
                            //  Analysis of masked
                            let maskedtextbox = input.data("kendoMaskedTextBox");
                            if (maskedtextbox.value().length > 5) {
                                destroyTooltip(input);
                                return true; // no validated if the ZIP is other than American
                            }
                            let masked = maskedtextbox.value().indexOf(maskedtextbox.options.promptChar) === -1;
                            if (masked) {
                                destroyTooltip(input);
                            } else {
                                createTooltip(xValidatorZip, input);
                            }
                            return masked;

                        } else {
                            // Fixed analysis to American format.
                            let zip: boolean = isZip($(input).val());
                            if (zip) {
                                destroyTooltip(input);
                            } else {
                                createTooltip(xValidatorZip, input);
                            }
                            return zip;
                        }
                    },

                    ssn: (input) => {
                        if (!input.is("[data-validation=ssn]")) {
                            return true;
                        }

                        if (input.is("[data-role=maskedtextbox]")) {
                            //  Analysis of masked
                            let maskedtextbox = input.data("kendoMaskedTextBox");
                            let masked = maskedtextbox.value().indexOf(maskedtextbox.options.promptChar) === -1;
                            if (masked) {
                                destroyTooltip(input);
                            } else {
                                createTooltip(xValidatorSsn, input);
                            }
                            return masked;

                        } else {
                            // Fixed analysis to American format.
                            let ssn: boolean = isSsn($(input).val());
                            if (ssn) {
                                destroyTooltip(input);
                            } else {
                                createTooltip(xValidatorSsn, input);
                            }
                            return ssn;
                        }
                    },

                    letters: (input) => {
                        if (!input.is("[data-validation=letters]")) {
                            return true;
                        }

                        // Fixed analysis to American format.
                        let letters: boolean = isLetters($(input).val());
                        if (letters) {
                            destroyTooltip(input);
                        } else {
                            createTooltip(xValidatorLetters, input);
                        }
                        return letters;
                    },

                    lettersnumbers: (input) => {
                        if (!input.is("[data-validation=lettersnumbers]")) {
                            return true;
                        }

                        // Fixed analysis to American format.
                        let letters: boolean = isLettersNumber($(input).val());
                        if (letters) {
                            destroyTooltip(input);
                        } else {
                            createTooltip(xValidatorLettersNumbers, input);
                        }
                        return letters;
                    },
                    dependRequired: (input) => {
                        let depcontrol: any;
                        if (!input.is("[data-validation=dependRequired]")) {
                            return true;
                        }   // Check if the second attribute exists
                        if (input.is("[data-dependable]") === undefined) {
                            return true;
                        }
                        // Check if the dependable exists
                        depcontrol = $("#" + input.attr("data-dependable"));
                        if (depcontrol.length === 0) {
                            return true;
                        }

                        // Begin validation of the rule
                        let html = input;
                        if (input.is('[data-role="numerictextbox"]')) {
                            html = input.siblings("input").addClass("valueRequired");
                        }

                        if (input.is('[data-role="dropdownlist"]')) {
                            let parentItem = input.siblings(".k-dropdown-wrap");
                            html = parentItem.children(".k-input");
                        }

                        if (input.is('[data-role="combobox"]')) {
                            let parentItem = input.siblings(".k-dropdown-wrap");
                            html = parentItem.children("input");
                        }

                        let value = depcontrol.val();
                        if (value === undefined || value === null || value === "") {
                            // if the value of dependable is empty or null rule is not apply
                            html.removeClass("valueRequired");
                            return true;
                        }

                        // Exists a value in the dependable control. check if the control is empty
                        let emptyVal = input.attr("data-empty");
                        if (emptyVal === html.text()) {
                            // MArk the control that require a value
                            html.addClass("valueRequired");
                            return false;
                        } else {
                            html.removeClass("valueRequired");
                            return true;
                        }
                    },
                    oneChecked: (input) => {
                        let cb = $(`#${input.prop("id")}`);
                        let cb0 = cb[0] as HTMLInputElement; // The checkbox element...
                        if (cb0 === undefined || cb0 === null) {
                            return true;
                        }
                        if (cb0.type !== "checkbox") {
                            return true;
                        }

                        //// Get all values....
                        let wrapper = $(`#${cb0.id}`).closest("div[data-validation=oneChecked]");
                        if (wrapper[0] === undefined || wrapper[0] === null) {
                            return true;
                        }

                        let listCheckbox: any = $(`#${wrapper[0].id}`).find("input");
                        if (listCheckbox === undefined || listCheckbox === null) {
                            return true;
                        }

                        for (var i = 0; i < listCheckbox.length; i++) {
                            if (listCheckbox[i].checked) {
                                wrapper.removeClass("valueRequired");
                                return true;
                            }
                        }
                        wrapper.addClass("valueRequired");
                        return false;
                    },

                    number: (input) => {
                        if (input.is("[data-validation=number]")) {

                            let num: boolean = isNumber($(input).val());
                            if (num) {
                                destroyTooltip(input);
                            } else {
                                createTooltip(xValidatorNumbers, input);
                            }
                            return num;
                        }
                        return true;
                    },
                    rangeNumber: (input) => {
                        if (input.is("[data-validation=rangeNumber]")) {

                            destroyTooltip(input);
                            let isRange: boolean =
                                isRangeNum($(input).val(), ($(input).attr("data-min")), ($(input).attr("data-max")));
                            if (isRange) {
                                destroyTooltip(input);
                            } else {
                                createTooltip($(input).attr("data-name") +
                                    " " +
                                    xValidatorRange +
                                    " " +
                                    $(input).attr("data-min") +
                                    " and " +
                                    $(input).attr("data-max"),
                                    input);
                            }
                            return isRange;
                        }
                        return true;
                    },
                    rangeDate: (input) => {
                        if (input.is("[data-validation=rangeDate]")) {

                            let num: boolean = isdate1($(input).val());
                            if (!num) {
                                createTooltip(xValidatorDate, input);
                            } else {
                                destroyTooltip(input);
                                let isRange: boolean =
                                    isRangeDate($(input).val(), ($(input).attr("data-min")), ($(input).attr("data-max")));
                                if (isRange) {
                                    destroyTooltip(input);
                                } else {
                                    createTooltip($(input).attr("data-name") +
                                        " " +
                                        xValidatorRange +
                                        " " +
                                        $(input).attr("data-min") +
                                        " and " +
                                        $(input).attr("data-max"),
                                        input);
                                }
                                return isRange;
                            }
                        }
                        return true;
                    },
                    decimal: (input) => {
                        if (input.is("[data-validation=decimal]")) {

                            let num: boolean = isAllowDecimal($(input).val());
                            if (num) {
                                destroyTooltip(input);
                            } else {
                                createTooltip(xValidatorDecimal, input);
                            }
                            return num;
                        }
                        return true;
                    }
                },

                messages: {
                    date: "",
                    mail: "",
                    required: ""
                }
            })
            .data("kendoValidator") as kendo.ui.Validator;
        return validator;
    }

    export function ADVANTAGE_VALIDATOR_HIDDEN_VALIDATION(wrapperComponent: string) {
        let childs = $(`#${wrapperComponent} .advantageTooltipClass`);
        childs.remove();
    }

    /**
     * Reset the form input fields to it's default state. Removing any tooltip and the required class
     * @param formName
     */
    export function advantageValitorReset(formName: string) {
        $("#" + formName + " .valueRequired").removeClass('valueRequired');
        ADVANTAGE_VALIDATOR_HIDDEN_VALIDATION(formName);
    }

    function createTooltip(message: any, input: any) {
        let nam = input.prop("name");
        let position: any = input.position();
        let topO = position.top;
        let topN = topO + input.outerHeight() + 1;
        let ident = nam + "tt";
        let test = $(`#${ident}`).length;
        if (test === 0) {
            let element: HTMLElement = document.createElement("div");
            let html = $(element);
            html.prop("id", ident);
            html.css({ top: topN });
            html.css({ left: position.left });
            //html.width(input.width());
            html.addClass("advantageTooltipClass");
            html.append(message);
            let lastElem = input.last();
            lastElem.after(html);
        }
    }

    function destroyTooltip(input: any) {
        let tt: string = input.prop("name") + "tt";
        let tool = $(`#${tt}`);
        if (tool.length > 0) {
            tool.remove();
        }
    }

    function isdate1(dateString: string) {
        if (dateString === "") {
            return true;
        }
        let reg = new RegExp("\\d{1,2}/\\d{1,2}/\\d{4,4}");
        return reg.test(dateString);
    }

    function isNumber(numberString: string) {
        if (numberString === "") {
            return true;
        }
        let reg = new RegExp("^[0-9]*$");
        return reg.test(numberString);
    }

    function isAllowDecimal(numberString: string) {
        if (numberString === "") {
            return true;
        }
        let reg = new RegExp("\\d+\\.?\\d*$");
        return reg.test(numberString);
    }

    function isTime12Am(val: string): boolean {

        if (val === "") {
            return true;
        }
        let reg = new RegExp("(1[012]|[1-9]):[0-5][0-9](\\s)?(AM|PM|am|pm)");
        return reg.test(val);
    }

    function isDateTime(val: string): boolean {

        if (val === "") {
            return true;
        }
        let reg = new RegExp("\\d{1,2}/\\d{1,2}/\\d{4,4}\\s(1[012]|[1-9]):[0-5][0-9](\\s)?(AM|PM|am|pm)");
        return reg.test(val);
    }

    function isEmail(val: string): boolean {
        if (val === "") {
            return true;
        }

        let reg = new RegExp("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$");
        return reg.test(val);
    }

    function isTel(val: string): boolean {
        if (val === "") {
            return true;
        }
        /* Admit the followings formats
         * 123-456-7890
         * (123) 456-7890
         * 123 456 7890
         * 123.456.7890
         * +91 (123) 456-7890
         */
        let reg = new RegExp("^(\\+\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}$");
        return reg.test(val);
    }

    function isInternationalPhone(value: string): boolean {
        if (value === "") {
            return true;
        }
        return (/^\d{7,}$/).test(value) || isTel(value);
    }

    function isZip(val: string): boolean {
        if (val === "") {
            return true;
        }

        let reg = new RegExp("^\\d{5}$");
        return reg.test(val);
    }

    function isSsn(val: string): boolean {
        if (val === "") {
            return true;
        }

        let reg = new RegExp("^\\d{3}-?\\d{2}-?\\d{4}$");
        return reg.test(val);
    }

    function isLetters(val: string): boolean {
        if (val === "") {
            return true;
        }

        let reg = new RegExp("^[a-zA-Z]+$");
        return reg.test(val);
    }

    function isLettersNumber(val: string): boolean {
        if (val === "") {
            return true;
        }

        let reg = new RegExp("^[-_ a-zA-Z0-9]+$");
        return reg.test(val);
    }

    function isRangeNum(val: string, minVal: string, maxVal: string): boolean {
        if (val === "") {
            return true;
        }
        let valN = Number(val);
        let minValN = Number(minVal);
        let maxValN = Number(maxVal);
        if (valN !== NaN && minValN !== NaN && maxValN !== NaN) {
            return valN >= minValN && valN <= maxValN;
        } else {
            return false;
        }
    }

    function isRangeDate(val: string, minVal: string, maxVal: string): boolean {
        if (val === "") {
            return true;
        }
        let valN = new Date(val);
        let minValN = new Date(minVal);
        let maxValN = new Date(maxVal);
        let c = new Date(maxValN.getTime());
        return valN >= minValN && valN < c;
    }
}