﻿module SY_FAST_REPORT {

    export class FastReportDb {

        /*
         * Get the report list from server
         */
        getReportListFromServer(directory: string): kendo.data.HierarchicalDataSource {
            var ssrsReport = new kendo.data.HierarchicalDataSource({
                transport: {
                    read: {
                        url: SY.XGET_SERVICE_LAYER_FAST_REPORT_GET,
                        type: "GET",
                        dataType: "json",
                        data: { Directory: directory, Command: 1 }
                    }
                },
                schema: {
                    data: "ReportInfo",
                    model: {
                        id: "Address",
                        children: "ChildrenList",
                        hasChildren:
                        e => {
                            if (e.ChildrenList != null) {
                                if (e.ChildrenList.length > 0) {
                                    return true;
                                }
                            }
                            
                            return false;
                        }
                    }
                }
            });

            return ssrsReport;
        }
    }
}  