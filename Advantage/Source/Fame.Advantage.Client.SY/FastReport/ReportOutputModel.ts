﻿module SY_FAST_REPORT { 
    // ReSharper disable InconsistentNaming 

    /*
     * Implementation of IReport Output Model
     */
    export interface IReportOutputModel {
        /*
         * List of report that are in the SSRS
         */
        ReportInfo: ReportInformation[];
 
        /*
         * Report Server name
         */
        ReportServerName: string;

        /*
         * Page HTML of the SSRS server
         */
        SsrsHtmlPage:string;
 
        /*
         * The input parameters of the report service
         */
        Filter: IReportInputModel;

    }

    /*
     * Implementation of IReport Output Model
     */
    export class ReportOutputModel implements IReportOutputModel {
        /*
         * List of report that are in the SSRS
         */
        ReportInfo: ReportInformation[];
 
        /*
         * Report Server name
         */
        ReportServerName: string;

         /*
         * Page HTML of the SSRS server
         */
        SsrsHtmlPage: string; 
        /*
         * The input parameters of the report service
         */
        Filter: IReportInputModel;

    }

 // ReSharper restore InconsistentNaming

}

