﻿module SY_FAST_REPORT {

    // ReSharper disable InconsistentNaming
    declare var __doPostBack;
    // ReSharper restore InconsistentNaming

    export class FastReportBo {

        public db: FastReportDb;
        public ssrsReport: kendo.data.HierarchicalDataSource;
        constructor() {

            this.db = new FastReportDb();
            var value = MasterPage.GET_QUERY_STRING_PARAMETER_BY_NAME("desc");
            this.ssrsReport = this.db.getReportListFromServer(value);
        }

        /*
         * Receive the click on the tree view. if the click is over 
         * a directory do nothing, if the click is over a report 
         * call the report service
         */
        launchReport(e: kendo.ui.TreeViewSelectEvent) {
            var tree: kendo.ui.TreeView = e.sender;
            var item: any = tree.dataItem(e.node);
            if (item.Description != null && item.Description !== "") {
                $("#fastReportTitle").text(item.Description);

            } else {
                $("#fastReportTitle").text("Select a Report");
            }

            if (item.hasChildren) {
                $("#hideReportDiv").css("visibility", "hidden");
                return;
            }

            sessionStorage.setItem("fastReport", item.Description);

            // Sen the request to server.........
            __doPostBack("GoAndChangeReport", item.Address);
        }
    }
} 