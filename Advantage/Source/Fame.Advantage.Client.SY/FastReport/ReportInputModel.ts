﻿module SY_FAST_REPORT { 

    // ReSharper disable InconsistentNaming 

    /*
  * Input command for the Service
  */
    export interface IReportInputModel {
        /*
         * Command to service
         *  For now 1: Get list of report
         */
        Command: number;
 
        /*
         * Send the directory to begin the exploration
         *  Can also have the command incorporate.
         */
        Directory: string;

    }

    /*
     * Input command for the Service
     */
    export class ReportInputModel implements IReportInputModel {
        /*
         * Command to service
         *  For now 1: Get list of report
         */
        Command: number;
 
        /*
         * Send the directory to begin the exploration
         *  Can also have the command incorporate.
         */
        Directory: string;

    }
    // ReSharper restore InconsistentNaming
}

