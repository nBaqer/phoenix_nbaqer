﻿module SY_FAST_REPORT { 

    // ReSharper disable InconsistentNaming 
    /*
    * Get the information about the address of the report in the server report
    */
    export interface IReportInformation {
        /*
         * Display name of the report
         */

        Name: string;
 
        /*
         * Address of the report in
         */
        Address: string;

        /*
         * Hold the element type
         * 1 Folder 2 Report others are filtered
         */
        Type: number;

        
        /*
         * Optional string parameters separated by comma
         */
        Children: IReportInformation[];

    }

    /*
     * Get the information about the address of the report in the server report
     */
    export class ReportInformation implements IReportInformation {
        /*
         * Display name of the report
         */
        Name: string;
 
        /*
         * Address of the report in
         */
        Address: string;

        /*
        * Hold the element type
        * 1 Folder 2 Report others are filtered
        */
        Type: number; 

        

        /*
         * Optional string parameters separated by comma
         */
        Children: ReportInformation[];

    }
    // ReSharper restore InconsistentNaming
}
 