﻿module SY_FAST_REPORT {

    export class FastReport {

        public bo: FastReportBo;

        constructor() {
            this.bo = new FastReportBo();

            var description = sessionStorage.getItem("fastReport");

            // Case Report is not running from server
            if (window["runningReport"] == undefined) {

                // Report should be selected in the tree-view
                var tree = $("#FastReportTreeView").kendoTreeView({
                    dataSource: this.bo.ssrsReport,
                    dataTextField: "Name",
                    select: e => {
                        sessionStorage.setItem("fastReportText", e.node.textContent);
                        this.bo.launchReport(e);
                    },
                    dataBound: e => {
                        var treeview = $("#FastReportTreeView").data("kendoTreeView");
                        treeview.expand(".k-item");

                        if (window["isPostBack"]) {
                            let node = sessionStorage.getItem("fastReportText");
                            if (node != null && node !== "") {
                                let snode = treeview.findByText(node);
                                treeview.select(snode);
                            }
                        } else {
                            let desc = MasterPage.GET_QUERY_STRING_PARAMETER_BY_NAME("desc");
                            let snode = treeview.findByText(desc);
                            treeview.select(snode);
                        }

                    }
                }).data("kendoTreeView");
                tree.setDataSource(this.bo.ssrsReport);
            }
            else {
                // Windows is specific report
                var title: string = MasterPage.GET_QUERY_STRING_PARAMETER_BY_NAME("desc");
                MasterPage.Common.setTitle(title);
                $("#panelF").html("");
            }
            if (window["isPostBack"]) {
                if (description == null) {
                    $("#fastReportTitle").text("Report has not description");
                } else {
                    $("#fastReportTitle").text(description);

                }
            } else {
                if (window["runningReport"] != undefined) {

                    //Report running directly from Server
                    $("#hideReportDiv").css("visibility", "visible");
                    $("#fastReportTitle").text(window["runningReport"]);
                } else {


                    $("#fastReportTitle").text("Select a Report");
                    sessionStorage.setItem("fastReportText", "");
                }

            }

            sessionStorage.setItem("fastReport", "");
        }
    }
}