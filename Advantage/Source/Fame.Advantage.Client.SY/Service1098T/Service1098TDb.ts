﻿/// <reference path="../ServiceUrls.ts" />

module Sy1098T {
    import XgetTofame1098Url = SY.XGET_TOFAME_1098_URL;

    export class Service1098TDb {

        //#region Common function to display errors  .
        public showDataSourceError(e) {
            try {
                if (e.xhr != undefined) {
                    if (this.showSessionFinished(e.xhr.statusText)) {
                        return;
                    }
                    if (e.xhr.responseText == undefined) {

                        alert("Error: " + e.xhr.statusText + ", " + e.xhr.responseXML);
                    } else {

                        alert("Error: " + e.xhr.statusText + ", " + e.xhr.responseText);
                    }
                } else {
                    if (this.showSessionFinished(e.statusText)) {
                        return;
                    }
                    if (e.responseText == undefined) {
                        alert("Error: " + e.statusText + ", " + e.responseXML);
                    } else {
                        alert("Error: " + e.statusText + ", " + e.responseText);
                    }
                }
            } catch (ex) {
                alert("Server returned an undefined error");
            }
        }


        /* Session Expired if return a fail and status is OK. This happen 
       * when the expected result is not of the specific type.
       * That should only happen when the server redirect a login page
       * because the session is expired
      */
        private showSessionFinished(statusText: string): boolean {
            if (statusText === "OK") {
                alert("Session expired");
                return true;
            }
            return false;
        }

        //#endregion

        /* 
         * Send the order to server layer that create the report and send it to FAME
         */
        public sendToFame1098T(year: number, campusId: string, userid: string, mcontext: any) {

            return $.ajax({
                context: mcontext,
                url: XgetTofame1098Url + "?Year=" + year + "&CampusId=" + campusId + "&UserId=" + userid,
                type: 'GET',
                timeout: 25000,
                dataType: 'json'
            });
        }
    }
}