﻿module Sy1098T {
   
    export class Service1098T {

        public bo: Service1098TBo;

        constructor() {
            this.bo = new Service1098TBo();

            
            $('#T1098Go').click(() => {

                this.bo.sendInfoToServer();
            });


            $("#FtpYearCombobox").kendoDropDownList(
                {
                    dataTextField: "text",
                    dataValueField: "text",
                    dataSource: this.bo.getLastTenYears()
                });
        }
    }
}