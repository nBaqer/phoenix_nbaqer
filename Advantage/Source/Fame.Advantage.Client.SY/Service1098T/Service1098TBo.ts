﻿module Sy1098T {

    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;

    export class Service1098TBo {

        //Call Service Layer and activate Service FTP sending the info
        sendInfoToServer() {
            $("body").css("cursor", "progress");
            var that = this;

            // Get the necessary information to request that report should be create and send to FAME
            var campusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            var userId: string = $("#hdnUserId").val();
            var year: string = $("#FtpYearCombobox").data("kendoDropDownList").value();
            var check1 = <HTMLInputElement>document.getElementById("checkbox1");
            var ischeck1Checked = check1.checked;
            var check2 = <HTMLInputElement>document.getElementById("checkbox2");
            var ischeck2Checked = check2.checked;

           if (year === '') {
                alert("Year can not be empty");
            }

            if (ischeck1Checked === false || ischeck2Checked === false) {
                alert("Select check boxes to confirm Programs and Transaction Types have been verified"); 
                return;
            }

            var yearNumber: number = Number(year);
            var db = new Service1098TDb();
            db.sendToFame1098T(yearNumber, campusId, userId, this)
                .done(msg => {
                    if (msg != undefined) {
                        var studentProcessed = msg.NumberOfStudentProcessed;
                        let text = "1098T Information from " +
                            studentProcessed.toString() +
                            " Students transmitted to FAME. \nMessage From FAME:\n";
                        var filtered = msg.Message.replace(/--/g, "\n");
                         alert(text + filtered);
                    } else {
                        alert("No information was returned from server");
                    }
                    $("body").css("cursor", "default");
                    return;
                })
                .fail(msg => {
                    $("body").css("cursor", "default");
                    db.showDataSourceError(msg);
                    return;
                });
        }


        getLastTenYears(): kendo.data.DataSource {

            var year: number = new Date().getFullYear();
            var db: Array<string> = new Array(8);
            db[0] = year.toString();
            for (var i = 1; i < 7; i++) {

                db[i] = (year - i).toString();
            }
            var dataSource = new kendo.data.DataSource();
            dataSource.data(
                [{ text: db[0] }
                    , { text: db[1] }
                    , { text: db[2] }
                    , { text: db[3] }
                    , { text: db[5] }
                    , { text: db[6] }
                ]
            );
            return dataSource;
        }
    }
}
 