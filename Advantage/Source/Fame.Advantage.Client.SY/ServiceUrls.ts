// This has all references to API.WEB in Advantage.....
// ReSharper disable InconsistentNaming
module SY {

    export var XGET_WAPI_WINDOWS_SERVICE_STATUS_URL = "../proxy/api/SystemStuff/Maintenance/WapiWindowsService/Get";
    export var XPOST_WAPI_WINDOWS_SERVICE_OPERATION_URL = "../proxy/api/SystemStuff/Maintenance/WapiWindowsService/Post";
    export var XGET_WAPI_OPERATION_SETTINGS_URL = "../proxy/api/SystemStuff/Maintenance/WapiOperationSettings/Get";
    
    export var XPOST_DELETE_WAPI_OPERATION_URL = "../proxy/api/SystemStuff/Maintenance/WapiOperationSettings/PostDeleteOperation";
    export var XGET_WAPI_CATALOGS_URL = "../proxy/api/SystemStuff/Maintenance/WapiOperationSettings/GetCatalog";
    export var XPOST_WAPI_CATALOGS_URL = "../proxy/api/SystemStuff/Maintenance/WapiOperationSettings/PostUpdateCatalog";

    export var XPOST_WAPI_SERVICE_OPERATION_UPDATE_URL = "../proxy/api/SystemStuff/Maintenance/WapiOperationSettings/PostUpdateOperation";
    export var XPOST_WAPI_SERVICE_OPERATION_NEW_URL = "../proxy/api/SystemStuff/Maintenance/WapiOperationSettings/PostInsertOperation";
    // use 1,2,3 options create, delete, retrieve
    export var XGET_WAPI_SECURITY_KEY_URL = "../proxy/api/SystemStuff/Maintenance/WapiOperationSettings/GetSecurityKeyService";
    
    //use ID as parameters if you want a specific flag, it is not omit parameter, you get a list of all.
    export var XGET_WAPI_ON_DEMAND_FLAGS = "../proxy/api/SystemStuff/Maintenance/WapiOperationSettings/GetOnDemandFlags";
    export var XPOST_WAPI_ON_DEMAND_FLAGS = "../proxy/api/SystemStuff/Maintenance/WapiOperationSettings/PostOnDemandFlags";
    export var XGET_SINGLE_WAPI_ON_DEMAND_FLAG = "../proxy/api/SystemStuff/Maintenance/WapiOperationSettings/GetSingleOnDemandFlag";

    //WapiOperationCode: ALL/Code operation, BeginDate, EndDate, LogType: ERROR/SUCCESS/ALL
    export var XGET_WAPI_LOGGER_OPERATION = "../proxy/api/SystemStuff/Maintenance/WapiOperationSettings/GetLoggerRecords";
    export var XPOST_WAPI_TRUNC_LOGGER_TABLE = "../proxy/api/SystemStuff/Maintenance/WapiOperationSettings/PostDeleteLoggerRecords";

    //Wapi Leads 
    export var XGET_WAPI_VENDORS_LIST_URL = "../proxy/api/SystemStuff/Maintenance/WapiVendorsLead/Get";
    export var XPOST_WAPI_UPDATE_VENDOR_URL = "../proxy/api/SystemStuff/Maintenance/WapiVendorsLead/PostUpdateVendor";

    export var XGET_WAPI_CAMPAIGN_LIST_URL = "../proxy/api/SystemStuff/Maintenance/WapiVendorsLead/GetCampaign";
    export var XPOST_WAPI_INSERT_CAMPAIGN_URL = "../proxy/api/SystemStuff/Maintenance/WapiVendorsLead/PostInsertCampaign";
    export var XPOST_WAPI_DELETE_CAMPAIGN_URL = "../proxy/api/SystemStuff/Maintenance/WapiVendorsLead/PostDeleteCampaign";

    export var XGET_WAPI_PAYFOR_LIST_URL = "../proxy/api/SystemStuff/Maintenance/WapiVendorsLead/GetPayForList";

    //1098T
    export var XGET_TOFAME_1098_URL = "../proxy/api/SystemStuff/Maintenance/T1098/Get";

    //SMTP
    export var XGET_SERVICE_LAYER_SMTP_GET = "../proxy/api/SystemStuff/Maintenance/Smtp/Get";
    export var XPOST_SERVICE_LAYER_SMTP_POST = "../proxy/api/SystemStuff/Maintenance/Smtp/Post";
    export var XPOST_SERVICE_LAYER_SMTP_POSTFILE = "../proxy/api/SystemStuff/Maintenance/Smtp/PostFile";
    export var XPOST_SERVICE_LAYER_SMTP_SEND = "../proxy/api/SystemStuff/Maintenance/SmtpSendEmail/Post";
    
    //Fast Report
    export var XGET_SERVICE_LAYER_FAST_REPORT_GET = "../proxy/api/SystemStuff/FastReports/FastReport/Get";

    //Institutions
    export var XGET_INSTITUTION = "../proxy/api/System/Institution/Get";

    export var XFIND_INSTITUTION = "../proxy/api/System/Institution/Find";
    export var XFIND_INSTITUTION_CONTACT = "../proxy/api/System/Institution/FindContact";
    export var XPOST_INSTITUTION_CONTACT = "../proxy/api/System/Institution/PostContact";

    export var XPUT_INSTITUTION_CONTACT = "../proxy/api/System/Institution/PutContact";

    export var XDELETE_INSTITUTION_CONTACT = "../proxy/api/System/Institution/DeleteContact";

    export var XFIND_INSTITUTION_ADDRESS = "../proxy/api/System/Institution/FindAddress";

    export var XPOST_INSTITUTION_ADDRESS = "../proxy/api/System/Institution/PostAddress";

    export var XPUT_INSTITUTION_ADDRESS = "../proxy/api/System/Institution/PutAddress";

    export var XDELETE_INSTITUTION_ADDRESS = "../proxy/api/System/Institution/DeleteAddress";

    export var XFIND_INSTITUTION_PHONE = "../proxy/api/System/Institution/FindPhone";

    export var XPOST_INSTITUTION_PHONE = "../proxy/api/System/Institution/PostPhone";

    export var XPUT_INSTITUTION_PHONE = "../proxy/api/System/Institution/PutPhone";

    export var XDELETE_INSTITUTION_PHONE = "../proxy/api/System/Institution/DeletePhone";

    export var XEXIST_INSTITUTION_BY_TYPE = "../proxy/api/System/Institution/GetExistByImportType";

    export var XEXIST_INSTITUTION_BY_NAME = "../proxy/api/System/Institution/GetExistByName";

    export var XGET_CATALOG = "../proxy/api/Catalogs/Catalogs/Get";

    //Mobile Application
    export var XGET_KLASS_APP_CONFIGURATION_SETTINGS_URL = "../proxy/api/SystemStuff/Mobile/KlassAppUi/Get";
    export var XPOST_KLASS_APP_CONFIGURATION_SETTINGS_URL = "../proxy/api/SystemStuff/Mobile/KlassAppUi/Post";

}
  // ReSharper restore InconsistentNaming