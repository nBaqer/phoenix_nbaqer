﻿/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../IQueryString.ts" />

declare var XMASTER_GET_BASE_URL: string;


module Wapi {

    var WapiIntervalTimer1: number;  // Used to stop to wait for on demand operation.
    var WapiSelectedOperationSettingId1: number;

    export class WapiLoggerVm extends kendo.Observable {

        public WapiDataServices: WapiLoggerDb;

        // Grid Data-logger GET
        public WapiLoggerDataSource: kendo.data.DataSource;
        public WapicbOperationTypeDataSource: kendo.data.DataSource;
        public WapiOperationNameDataSource: kendo.data.DataSource;
        public WapidpInitialDayDataSource: kendo.data.DataSource;
        public WapiActionOperationTypeDataSource: kendo.data.DataSource;

        private ActiveService: boolean;
        private ActiveOperation: number;
        //public maxTime: number;

        constructor() {
            super();
            this.WapiDataServices = new WapiLoggerDb();
            this.WapiLoggerDataSource = new kendo.data.DataSource();

            this.WapiOperationNameDataSource = this.WapiDataServices.getOperationNameDataSource();

            this.WapiActionOperationTypeDataSource = this.WapiDataServices.getActiveOperationNameDataSource();
        }


        //#region Filters

        ClickGetLoggerRows() {

            //this.WapiLoggerDataSource.read();
            var grid = $('#WapiLoggerGrid').data('kendoGrid');
            grid.setDataSource(this.WapiDataServices.GetWapiLoggerDataSource());
            // grid.dataSource.read();
            // grid.datasource.refresh();

        }

        //#endregion

        //#region Windows Service..................................
        scanWindowsService() {
            //  Get the status of the service....
            var db = this.WapiDataServices;
            db.GetWindowsServiceStatus();
        }

        StartService(e: Object) {
            var db = this.WapiDataServices;
            db.StartwindowsService();
        }

        StopService(e: Object) {
            var db = this.WapiDataServices;
            db.StopwindowsService();
        }
        //#endregion

        //#region Operation Manager..................

        ManageActiveOperation(e: boolean) {
            var db = new WapiLoggerDb();
            var idStr = $('#cbWapiActionOperationType').data("kendoDropDownList").value();
            var id: number = parseInt(idStr);
            if (id !== NaN && id !== 0) {
                db.getWapiSettingValue(id, this)
                    .done(function (msg) {
                        if (msg.length > 0) {
                            msg[0].IsActiveOperation = e; // set value
                            // Update value
                            db.postExternalOperationSettingEdit(msg[0], this)
                                .always(function (ms) {
                                    if (ms.status === 200) {
                                        // Refresh Grid
                                        // Update button status...
                                        this.SetButtonStates();
                                    } else {
                                        MasterPage.SHOW_ERROR_WINDOW(`Error Received: Code:${msg.status} ${msg.statusText}. ${msg.responseText}`);
                                       // alert("Error Received: Code:" + msg.status + " " + msg.statusText + ". " + msg.responseText);
                                    }
                                });
                        }
                    })
                    .fail(msg => {
                        MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    });



            }
        }

        InactiveOperation(e: Object) {
            throw new Error("Not implemented");
        }

        public executeOnDemandOperation() {
            var idText = $("#cbWapiActionOperationType").data("kendoDropDownList").value();
            var db = new WapiLoggerDb();
            var id = parseInt(idText);
            WapiSelectedOperationSettingId1 = id;
            // Send to server the On-demand Flag to 1
            MasterPage.Common.displayLoading(document.body, true);
            db.postOnDemandFlag(WapiSelectedOperationSettingId1, 1, this)
                .done(msg => {
                    MasterPage.Common.displayLoading(document.body, false);
                    var windowx = $("#WindowsWapiLogOnDemandOperation").data("kendoWindow");
                    windowx.setOptions({
                        visible: true
                    });
                    windowx.open();
                    windowx.center();

                    // Begin process to wait for operation complexion
                    WapiIntervalTimer1 = setInterval(function () {

                        // make the call....
                        db.getOnDemandFlag(id, this)
                            .done(flag => {
                                if (flag.OnDemandOperation === 0) {
                                    clearInterval(WapiIntervalTimer1);
                                    var winx = $("#WindowsWapiLogOnDemandOperation").data("kendoWindow");
                                    winx.setOptions({
                                        visible: false
                                    });
                                    winx.close();
                                    MasterPage.SHOW_INFO_WINDOW(flag.Result);
                                    return;
                                }

                                // elapsedTime += 2;
                            })
                            .fail(msg1 => {
                                clearInterval(WapiIntervalTimer1);
                                var winx = $("#WindowsWapiLogOnDemandOperation").data("kendoWindow");
                                winx.setOptions({
                                    visible: false
                                });
                                winx.close();
                                MasterPage.SHOW_DATA_SOURCE_ERROR(msg1);
                            });
                    },
                        2000);
                })
                .fail(error => {
                    MasterPage.Common.displayLoading(document.body, false);
                    //var window = $("#WindowsWapiLogOnDemandOperation").data("kendoWindow");
                    //window.setOptions({
                    //    visible: false
                    //});
                    ////window.visible = false;
                    //window.close();
                    MasterPage.SHOW_DATA_SOURCE_ERROR(error);
                    // alert("On demand Operation Fail to set the Flag: " + error.statusText);
                    // return; 
                });
        }

        public stopOnDemandOperation() {
            clearInterval(WapiIntervalTimer1);
            var window = $("#WindowsWapiLogOnDemandOperation").data("kendoWindow");
            window.setOptions({
                visible: false
            });
            window.close();
            var db = new WapiConfigDb();
            db.PostOnDemandFlag(WapiSelectedOperationSettingId1, 0, this)
                .done(msg => {
                    MasterPage.SHOW_INFO_WINDOW("The On Demand Operation was canceled");
                })
                .fail(error => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(error);
                });
        }

        //#endregion

        //#region Page Manager...........................

        ActionGoConfiguration(e: any) {
            e.preventDefault();
            var qs;
            //var loc = location.href;
            var campusId = $.fn.QueryString["cmpid"];
            if (campusId != null) {
                qs = XMASTER_GET_BASE_URL + "/SY/WapiConfig.aspx?resid=810&mod=SY&cmpid=" + campusId + "&desc=Wapi Configuration";

            }
            else {
                qs = XMASTER_GET_BASE_URL + "/SY/WapiConfig.aspx?resid=810&mod=SY&desc=Wapi Configuration";

            }

            location.href = qs;
        }

        actionClearLogger(e: Object) {
            $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure that you want to delete the logger information?"))
                .then((confirmed) => {
                    if (confirmed) {
                        // YES was selected
                        var db: WapiLoggerDb = new WapiLoggerDb();
                        db.PostTruncLoggerTable(this)
                            .done( () => {
                                var grid = $('#WapiLoggerGrid').data('kendoGrid');
                                grid.setDataSource(this.WapiDataServices.GetWapiLoggerDataSource());
                                MasterPage.SHOW_INFO_WINDOW("Logger Deleted...");
                            })
                            .fail(msg => {
                                MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                            });
                    }
                });



            //if (confirm("Are you sure that you want to delete the logger information?")) {
            //    var db: WapiLoggerDb = new WapiLoggerDb();
            //    db.PostTruncLoggerTable(this)
            //        .done(function () {
            //            var grid = $('#WapiLoggerGrid').data('kendoGrid');
            //            grid.setDataSource(this.WapiDataServices.GetWapiLoggerDataSource());
            //            MasterPage.SHOW_INFO_WINDOW("Logger Deleted...");
            //        })
            //        .fail(msg => {
            //            MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
            //        });
            //}
        }

        //#endregion

    } // End Class
} // End module 