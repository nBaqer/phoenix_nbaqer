﻿/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />

var WapiFlagOperation = 0;


module Wapi {

    var WapiLoggerTimer: number = 0;

    export class WapiLogger {

        public ViewModel: WapiLoggerVm;
        public flagAll: number = 0;

        ////public SelectedEnrollment: kendo.Observable;

        // Constructor Class
        constructor() {
            this.ViewModel = new WapiLoggerVm();
            var viewModel = this.ViewModel;

            //#region Configure Filters ......................................................

            //Configure Start Button
            $("#btnLoggerSearch").kendoButton({
                enable: true,
                click: e => { viewModel.ClickGetLoggerRows(); }
            });

            $('#cbOperationType').kendoDropDownList(
                {
                    dataSource: this.ViewModel.WapiOperationNameDataSource,
                    dataTextField: "Code",
                    dataValueField: "Id",
                    dataBound: e => {
                        if (WapiFlagOperation === 0) {
                            WapiFlagOperation = 1;
                            var dda = $('#cbOperationType').data('kendoDropDownList');
                            dda.dataSource.add({ Id: 0, Code: "ALL" });
                            dda.select(dataItem => dataItem.Id === 0);
                        }
                    }
                });

            var d = new Date().setDate(-10);

            $('#cbInitialDay').kendoDatePicker(
                {
                    value: new Date(d),
                    format: "MM/dd/yyyy"
                });

            $('#cbEndDay').kendoDatePicker(
                {
                    value: new Date(),
                    format: "MM/dd/yyyy"

                });
            $('#cbLogType').kendoDropDownList(
                {
                    dataSource: [{ Code: "All", Id: -1 }, { Code: "Error", Id: 1 }, { Code: "Success", Id: 0 }],
                    dataTextField: "Code",
                    dataValueField: "Id"

                });

            //#endregion 

            //#region Control Windows Service
            // Configure Start Button
            $("#WapiServiceStartButton").kendoButton({
                enable: false,
                click: e => { viewModel.StartService(e); }
            });

            // Configure Stop Button
            $("#WapiServiceStopButton").kendoButton({
                enable: false,
                click: e => { viewModel.StopService(e); }
            });

            viewModel.scanWindowsService();
            //    ///#endregion

            //#region Logger Grid............................................
            $("#WapiLoggerGrid").kendoGrid({
                dataSource: {
                    data: viewModel.WapiLoggerDataSource,
                    schema: {
                        model: {
                            fields: {
                                Id: { type: "number" },
                                ServiceCode: { type: "string" },
                                CompanyCode: { type: "string" },
                                DateExecution: { type: "date" },
                                NextPlanningDate: { type: "date" },
                                IsError: { type: "number" },
                                Comment: { type: "string" }
                            }
                        }
                    }
                },
                toolbar: ["excel", "pdf"],
                pdf: {
                    fileName: "Kendo UI Grid Export.pdf",

                },
                excel: {
                    fileName: "Kendo UI Grid Export.xlsx"
                },
               
                // pageSize: 20,
                //serverPaging: true,
                // serverFiltering: true,
                // serverSorting: true
                // ,
                filterable: true,
                resizable:true,
                sortable: true,
                // page able: true,
                columns: [{
                    field: "Id",
                    width: 40,
                    filterable: false
                },
                    {
                        field: "ServiceCode",
                        title: "Service Code",
                        filterable: true
                    },
                    {
                        field: "CompanyCode",
                        title: "Company Code",
                        filterable: true
                    }, {
                        field: "DateExecution",
                        title: "Date Execution",
                        format: "{0:MM/dd/yyyy}"
                    }, {
                        field: "NextPlanningDate",
                        title: "Next Planning Date",
                        format: "{0:MM/dd/yyyy}"
                    }, {
                        field: "IsError",
                        title: "Is Error",
                        filterable: false,
                        width: 70,
                    }, {
                        field: "Comment",
                        title: "Comment",
                        filterable: false
                    }
                ]
            });

                 //#endregion  
            
            //#region Manage Operation

            $('#cbWapiActionOperationType').kendoDropDownList(
                {
                    dataSource: this.ViewModel.WapiActionOperationTypeDataSource,
                    dataTextField: "Code",
                    dataValueField: "Id"
                });
           

             // Configure invoke a on demand operation
            $("#WapiActionInitOnDemandOperation").kendoButton({
                enable: true,
                click: e => {
                    var intervalControl: kendo.ui.DropDownList = $('#cbWapiActionRefreshInterval').data("kendoDropDownList");
                    intervalControl.select(0);
                    intervalControl.refresh();
                    clearInterval(WapiLoggerTimer);
                    viewModel.executeOnDemandOperation();
                }
            });

            //#endregion

            //#region Manage Page control
            $('#cbWapiActionRefreshInterval').kendoDropDownList(
                {
                    dataSource: [{ Code: "Never", Id: 0 }, { Code: "5 seconds", Id: 5000 }, { Code: "10 seconds", Id: 10000 }, { Code: "20 seconds", Id: 20000 }],
                    dataTextField: "Code",
                    dataValueField: "Id",
                    change: function () {
                        var val = this.value();
                        if (WapiLoggerTimer !== 0) {
                            clearInterval(WapiLoggerTimer);
                            WapiLoggerTimer = 0;
                        }

                        if (val === 0) {
                            return;
                        }
                        WapiLoggerTimer = setInterval(() => {
                            viewModel.ClickGetLoggerRows();
                        }, val);
                    }
                });

            // Configure Add New Configuration Button
            $("#WapiActionGoConfiguration").kendoButton({
                enable: true,
                click: e => { viewModel.ActionGoConfiguration(e); }
            });


            // Clear (truncate) logger Table
            $("#WapiActionClearLogger").kendoButton({
                enable: true,
                click: e => { viewModel.actionClearLogger(e); }
            });

            //#endregion

            //#region On demand Operation Window

            $("#WindowsWapiLogOnDemandOperation").kendoWindow(
                {
                    actions: [],
                    title: "On Demand Operation In Progress...",
                    width: "400px",
                    height: "120px",
                    visible: false,
                    modal: true,
                    resizable: false
                });

            // Configure Add New Configuration Button
            $("#btnLogCancelOnDemand").kendoButton({
                enable: true,
                click: e => {
                    viewModel.stopOnDemandOperation();
                }
            });

            //#endregion
    

            // This bind the view model with all.-------------------------------------------------------------------------
            kendo.init($("#KendoContainerLogger"));
            kendo.bind($("#KendoContainerLogger"), this.ViewModel);
        }

    }

    //This is in WapiLogger.aspx page in document ready.....
    //$(document).ready(() => {

    //    var manager = new WapiLogger();

    //});

}