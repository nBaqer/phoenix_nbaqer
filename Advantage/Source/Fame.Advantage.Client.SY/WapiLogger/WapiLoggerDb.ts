﻿/// <reference path="../ServiceUrls.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />

module Wapi {
    export class WapiLoggerDb {

        xhrDataSourceError(e) {
            MasterPage.SHOW_ERROR_WINDOW(e.xhr.responseText);
        }
        //#endregion

        //#region Get the WAPI Operation Settings 
        public GetWapiLoggerDataSource() {
            var url = SY.XGET_WAPI_LOGGER_OPERATION;
            // Get filters....
            url += "?WapiOperationCode=" + $('#cbOperationType').data('kendoDropDownList').text();

            url += "&BeginDate=" + kendo.toString($('#cbInitialDay').data('kendoDatePicker').value(), "MM/dd/yyyy");
            url += "&EndDate=" + kendo.toString($('#cbEndDay').data('kendoDatePicker').value(), "MM/dd/yyyy");
            url += "&LogType=" + $('#cbLogType').data('kendoDropDownList').text();



            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }
                },
                error: this.xhrDataSourceError,

                schema: {
                    model: {
                        id: "Id",
                        ServiceCode: "ServiceCode",
                        CompanyCode: "CompanyCode",
                        DateExecution: "DateExecution",
                        NextPlanningDate: "NextPlanningDate",
                        IsError: "IsError",
                        Comment: "Comment",
                    }
                },
                //pageSize: 10,
            });
            return ds;
        }

        //#endregion

        //#region Manage Logger Table
        public PostTruncLoggerTable(contex: any) {
            return $.ajax({
                url: SY.XPOST_WAPI_TRUNC_LOGGER_TABLE,
                context: contex,
                type: 'POST',
                timeout: 5000,
                dataType: 'text'
            });
        }
        //#endregion

        //#region Windows Services Data Sources....
        public GetWindowsServiceStatus() {

            $.ajax(
                {
                    url: SY.XGET_WAPI_WINDOWS_SERVICE_STATUS_URL,
                    type: "GET",
                    dataType: "text",
                    data: { ServiceName: 'WapiWindowsService' }
                }).done(msg => {
                    $("#WapiTextServiceStatus1").text(msg);
                    //Clean message
                    msg = msg.replace('"', '');
                    msg = msg.replace('"', '');
                    msg = msg.trim();
                    switch (msg.toLowerCase()) {
                        case "running":
                            {
                                $("#WapiServiceStopButton").data("kendoButton").enable(true);
                                $("#WapiServiceStartButton").data("kendoButton").enable(false);
                                $("#WapiActionInitOnDemandOperation").data("kendoButton").enable(true);
                                $("#WapiImageServiceStatus1").attr("src", "../images/geargreen.jpg");

                                break;
                            }
                        case "stopped":
                        case "paused":
                            {
                                $("#WapiServiceStopButton").data("kendoButton").enable(false);
                                $("#WapiServiceStartButton").data("kendoButton").enable(true);
                                $("#WapiActionInitOnDemandOperation").data("kendoButton").enable(false);
                                $("#WapiImageServiceStatus1").attr("src", "../images/gearred.jpg");


                                break;
                            }
                        case "uninstalled":
                            {
                                $("#WapiServiceStopButton").data("kendoButton").enable(false);
                                $("#WapiServiceStartButton").data("kendoButton").enable(false);
                                $("#WapiActionInitOnDemandOperation").data("kendoButton").enable(false);
                                $("#WapiImageServiceStatus1").attr("src", "../images/geargray.jpg");

                                break;
                            }
                        default:
                            {
                                // Intermediate status please call again
                                var self = this;
                                window.setTimeout(function () { self.GetWindowsServiceStatus(); }, 2000);
                            }
                    }



                }).fail(msg => {
                    window.alert("Error Received: Code:" + msg.status + " " + msg.statusText + ". " + msg.responseText);
                    //this.GetWindowsServiceStatus();

                });
        }

        StopwindowsService() {
            $("#WapiServiceStopButton").data("kendoButton").enable(false);
            $("#WapiServiceStartButton").data("kendoButton").enable(false);
            $("#WapiActionInitOnDemandOperation").data("kendoButton").enable(false);

            var filter = {};
            filter["ServiceName"] = "WapiWindowsService";
            filter["Command"] = "stop";
            $.ajax(
                {
                    url: SY.XPOST_WAPI_WINDOWS_SERVICE_OPERATION_URL,
                    type: "POST",
                    dataType: "text",
                    contentType: "application/json",
                    data: JSON.stringify(filter),
                }).done(msg => {
                    $("#WapiTextServiceStatus1").text(msg);
                    this.GetWindowsServiceStatus();
                }).fail(msg => {
                    alert("Error Received: Code:" + msg.status + " " + msg.statusText + ". " + msg.responseText);
                    this.GetWindowsServiceStatus();
                });
        }

        StartwindowsService() {
            $("#WapiServiceStopButton").data("kendoButton").enable(false);
            $("#WapiServiceStartButton").data("kendoButton").enable(false);
            $("#WapiActionInitOnDemandOperation").data("kendoButton").enable(false);
            var filter = {};
            filter["ServiceName"] = "WapiWindowsService";
            filter["Command"] = "start";
            $.ajax(
                {
                    url: SY.XPOST_WAPI_WINDOWS_SERVICE_OPERATION_URL,
                    type: "POST",
                    dataType: "text",
                    data: JSON.stringify(filter),
                }).done(msg => {
                    $("#WapiTextServiceStatus1").text(msg);
                    this.GetWindowsServiceStatus();
                }).fail(msg => {
                    alert("Error Received: Code:" + msg.status + " " + msg.statusText + ". " + msg.responseText);
                    this.GetWindowsServiceStatus();

                });
        }
        ///#endregion

        //#region On Demand Flag Management
        public postOnDemandFlag(operationId: number, value: number, contex: any) {
            return $.ajax({
                url: SY.XPOST_WAPI_ON_DEMAND_FLAGS + "?Id=" + operationId + "&Value=" + value,
                context: contex,
                type: "POST",
                dataType: "text",
                timeout: 15000
            });

        }

        public getOnDemandFlag(operationId: number, contex: any) {
            return $.ajax({
                url: SY.XGET_SINGLE_WAPI_ON_DEMAND_FLAG + "?id=" + operationId,
                context: contex,
                type: 'GET',
                dataType: 'json'
            });
        }

        //#endregion

        //#region Manage Operation Settings

        public getOperationNameDataSource() {
            var url = SY.XGET_WAPI_CATALOGS_URL + '?CatalogOperation=OPERATION';
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }

                },
                error: MasterPage.SHOW_DATA_SOURCE_ERROR // this.showDataSourceError
            });
            return ds;
        }

        //REturn operation actives
        public getActiveOperationNameDataSource() {
            var url = SY.XGET_WAPI_CATALOGS_URL + '?CatalogOperation=OPERATION_ACTIVES';
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }
                },
                error: MasterPage.SHOW_DATA_SOURCE_ERROR
            });

            return ds;
        }

        public getWapiSettingValue(id: number, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: SY.XGET_WAPI_OPERATION_SETTINGS_URL + "?IdOperation=" + id,
                type: 'GET',
                timeout: 5000,
                dataType: 'json'
                //data: JSON.stringify({ ID: id }),
            });
        }

        public postExternalOperationSettingEdit(msg, scontext: any) {

            return $.ajax({
                context: scontext,
                url: SY.XPOST_WAPI_SERVICE_OPERATION_UPDATE_URL,
                type: 'POST',
                dataType: 'text',
                data: JSON.stringify(msg)
            });
        }

        //#endregion

    }
}