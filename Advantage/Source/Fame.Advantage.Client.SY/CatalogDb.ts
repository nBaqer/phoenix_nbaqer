﻿/// <reference path="../Fame.Advantage.Client.SY/ServiceUrls.ts" />
module SY {
    export class CatalogDb {
        public getData(fieldName: string, campusId: string, additionalFilter: string, context: any): kendo.data.DataSource {
            return new kendo.data.DataSource({
                transport: {
                    read: {
                        url(options) {
                            var uri = SY.XGET_CATALOG +
                                "?FldName=" +
                                fieldName +
                                "&CampusID=" +
                                campusId +
                                "&AdditionalFilter=" +
                                additionalFilter; 
                            delete options.baseUrl;
                            return uri;
                        },
                        dataType: "json",
                        timeout: 20000,
                        type: "GET"
                    }
                }
            });
        }
    }
}            //return $.ajax({
            //    context: context,
            //    url: SY.XGET_CATALOG 
            //    type: 'GET',
            //    timeout: 25000,
            //    dataType: 'json',
            //    async: true
            //});