﻿module SY {

    // ReSharper disable InconsistentNaming
    /*
     * 
     */
    export interface IClassAppInputModel {
        /*
         * The command to be executed
         * 0, 1 Get configuration from table
         */
        Command: number;

        /**
         * The user Id
         */
        UserId: string;
    }

    /*
     * 
     */
    export class ClassAppInputModel {

        /*
         * The command to be executed
         * 0, 1 Get configuration from table
         */
        Command: number;

        /**
         * The user Id
         */
        UserId: string;

    }

    // ReSharper restore InconsistentNaming}
}