﻿module SY {
    // ReSharper disable InconsistentNaming
    /*
     * 
     */
    export interface IClassAppUiConfigurationOutputModel {
        /*
         * 
         */
        ConfigurationSetting: ConfigurationSetting[];

        /*
         * 
         */
        Filter: ClassAppInputModel;

    }

    /*
     * 
     */
    export class ClassAppUiConfigurationOutputModel {
        /*
         * 
         */
        ConfigurationSetting: ConfigurationSetting[];

        /*
         * 
         */
        Filter: ClassAppInputModel;

    }
    // ReSharper restore InconsistentNaming}
}
