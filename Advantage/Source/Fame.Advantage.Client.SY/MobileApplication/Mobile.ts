﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />

namespace SY {

    declare var XMASTER_PAGE_USER_OPTIONS_USERID: string;

    export class Mobile {
        bo: MobileBo;
        deleteUpdateValidation: string = "Delete and Update flag can only be applied if KLASS-ID <> 0";
        insertValidation: string = "\"Insert\" status can only be applied if KLASS-ID is 0";
        askForInactivate: string = "Are you sure to inactivate this setting?";
        imposible:string = "No accepted Command. Please call FAME";

        constructor() {

            this.bo = new MobileBo();

            $("#klassAppGrid").kendoGrid({
                height: 600,
                pageable: {
                    pageSizes: true,
                    refresh: true
                },
                dataSource: this.bo.getKlassAppConfigurationsDataSource(XMASTER_PAGE_USER_OPTIONS_USERID),

                sortable: true,
                editable: "inline",
                columns: [
                    { field: "Id", title: "", width: "1px" },
                    { command: ["edit"], title: "&nbsp", width: "80px" },
                    { field: "KlassAppIdentification", title: "KLASS ID", width: "70px" },
                    { field: "CodeEntity", title: "KLASS Entity", width: "100px" },
                    { field: "CodeOperation", title: "KLASS Code", width: "100px" },
                    { field: "IsCustomField", title: "Is Custom", width: "80px", template: "#= IsCustomField ? 'Yes' : 'No' #" },
                    { field: "IsActive", title: "Active", width: "50px", template: "#= IsActive ? 'Yes' : 'No' #", editable: e => !e.IsActive },
                    { field: "ItemStatus", title: "Status", width: "100px", editor: this.bo.categoryDropDownEditor, template: "#=ItemStatus == 'N'? 'No change': ItemStatus == 'I'? 'Insert' : ItemStatus == 'D'?'Inactive':'Update' #" },
                    { field: "ItemValue", title: "ADV Value", width: "100px" },
                    { field: "ItemLabel", title: "Display Label", width: "100px" },
                    { field: "ItemValueType", title: "Type", width: "60px" },
                    { field: "CreationDate", title: "Created", format: "{0:d}", width: "80px" },
                    { field: "ModDate", title: "Mod Date", format: "{0:d}", width: "80px" },
                    { field: "ModUser", title: "", hidden: true },
                    { field: "LastOperationLog", title: "Operation Log." }
                ],
                save: e => {
                    let data: any = e.model;
                    if (data.ItemStatus === "U") {
                        if (data.KlassAppIdentification !== 0) {
                            // Return is OK the operation
                            return;
                        } else {
                            // Cancel the operation
                            e.preventDefault();
                            MasterPage.SHOW_WARNING_WINDOW(this.deleteUpdateValidation);
                        }
                    }

                    if (data.ItemStatus === "I") {
                        if (data.KlassAppIdentification === 0) {
                            return;
                        } else {
                            // Cancel the operation
                            e.preventDefault();
                            MasterPage.SHOW_WARNING_WINDOW(this.insertValidation);
                        }
                    }

                    // Checking if the new values is valid...


                    let question: boolean = false;
                    if (data.ItemStatus === "D") {
                        if (data.KlassAppIdentification !== 0) {
                            // Ask to prevent accidental delete
                            question = true;
                        } else {
                            // Cancel Operation
                            e.preventDefault();
                            MasterPage.SHOW_WARNING_WINDOW(this.deleteUpdateValidation);
                        }
                    }

                    
                    if (data.ItemStatus === "N") {
                       return;
                    }
                    e.preventDefault();
                    if (question) {
                        $.when(MasterPage
                                .SHOW_CONFIRMATION_WINDOW_PROMISE(this.askForInactivate))
                            .then(confirmed => {
                                if (confirmed) {
                                    $("#klassAppGrid").data("kendoGrid").dataSource.sync();
                                    return;
                                } else {
                                    // Cancel operation
                                    $("#klassAppGrid").data("kendoGrid").dataSource.cancelChanges();
                                    return;
                                }
                            });
                    }
                },
                cancel: e => {
                        $("#klassAppGrid").data("kendoGrid").dataSource.read();
                        $("#klassAppGrid").data("kendoGrid").refresh();
                    //setTimeout(() => {
                    //    $("#klassAppGrid").data("kendoGrid").dataSource.read();
                    //    $("#klassAppGrid").data("kendoGrid").refresh();
                    //}, 1000);
                    //e.preventDefault();
                    //$("#klassAppGrid").data("kendoGrid").dataSource.cancelChanges();
                    //$("#klassAppGrid").data("kendoGrid").dataSource.sync();
                    //$("#klassAppGrid").data("kendoGrid").refresh();

                }
                //,
                //edit: function(e) {
                //    if (e.model.get("IsActive") === 0) {
                //        // Disable the editor of the "id" column when editing data items
                //        var numeric = e.container.find("input[name=IsActive]").data("checkbox");
                //        numeric.enable(false);
                //    }


                //}
            } as any);


            /*
             *  Help button
             */
            $("#klassAppHelp").click(() => {
                let message: string = "<p><b>Setting Status Flag:</b></p>";
                message += "<ul><li><b>Insert</b> (Initial KLASS APP setting. ​The KLASS APP ID is received and stored in the setting or if it does not exist it is inserted in KLASS APP and the KLASS APP​ ID ​ is stored)";
                message += "<li><b>Update</b> (setting will be updated if exists, or inserted if does not exists in KLASS APP)</li>";
                message += "<li><b>Inactivate</b> (Setting is inactivated in KLASS APP​ and flagged as not active)</li>";
                message += "<li><b>No change</b> (No action will occur)</li></ul>";

                MasterPage.SHOW_INFO_WINDOW(message);
            });

            /**
             * Button to send truncate and initialize the KLASSAPP configuration in Advantage Table
             */
            var sendKlassAppConfigurationToAdvantage = this.bo.sendKlassAppConfigurationToAdvantage;
            $("#klassBtnInitialize").kendoButton({
                click: () => {
                    let message =
                        "<p><b>WARNING</b></p> <p>This initialize your Advantage KLASS-APP Setting Configuration Table. Your local settings will be deleted and created again. After this, you should click SendConfiguration button to resend the information to KLASS-APP </p><p>Are you sure to proceed?</p>";
                    $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(message))
                        .then(confirmed => {
                            if (confirmed) {
                                // YES was selected
                                // put here the code used when the user response YES
                                sendKlassAppConfigurationToAdvantage();
                            }
                        });
                }
            });

            var configure = this.bo.sentConfigurationToKlassApp;
            $("#klassBtnConfiguration").kendoButton({
                click: () => {
                    let message =
                        "<p><b>INFO</b></p> <p>This send your configuration changes to KLASSAPP  </p><p>do you want to proceed?</p>";
                    $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(message))
                        .then(confirmed => {

                            if (confirmed) {
                                $('body').css("cursor", "wait");
                                // YES was selected
                                configure();
                                $('body').css("cursor", "default");
                            }
                        });


                }
            });

            $("#klassBtnStudents").kendoButton({
                click: () => {
                    MasterPage.SHOW_INFO_WINDOW("Not Implemented yet.");
                }
            });

            var sendKlassAppClearToken = this.bo.sendToKlassAppClearToken;
            $("#klassBtnResetKlassApp").kendoButton({
                click: () => {
                    let message =
                        "<p>Selecting this option will delete all Advantage data for this school on the Klass App server. This includes deleting all student accounts from Klass App. This can not be undone!  </p><p>Do you wish to continue?</p>";
                    $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(message))
                        .then(confirmed => {
                            if (confirmed) {
                                // YES was selected
                                // put here the code used when the user response YES
                                sendKlassAppClearToken();
                            }
                        });
                }
            });
        }

        varlidationContinue(data: any, e:any) {

            if (data.ItemStatus === "U") {
                if (data.KlassAppIdentification !== 0) {
                    // Return is OK the operation
                    return;
                } else {
                    // Cancel the operation
                    e.preventDefault();
                    MasterPage.SHOW_WARNING_WINDOW(this.deleteUpdateValidation);
                }
            }

            if (data.ItemStatus === "I") {
                if (data.KlassAppIdentification === 0) {
                    return;
                } else {
                    // Cancel the operation
                    e.preventDefault();
                    MasterPage.SHOW_WARNING_WINDOW(this.insertValidation);
                }

            }

            // Should not go to here
            e.preventDefault();
            MasterPage.SHOW_WARNING_WINDOW(this.imposible);
        }
    }
}