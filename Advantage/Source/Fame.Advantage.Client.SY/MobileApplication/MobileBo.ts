﻿/// <reference path="../../fame.advantage.client.masterpage/advantagemessageboxs.ts" />
/// <reference path="../../fame.advantage.client.masterpage/commonfunctions.ts" />

namespace SY {

    declare var XMASTER_PAGE_USER_OPTIONS_USERID: string;

    export class MobileBo {

        db: MobileDb;

        constructor() {
            this.db = new MobileDb();
        }

        getKlassAppConfigurationsDataSource(userId: string) {
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: XGET_KLASS_APP_CONFIGURATION_SETTINGS_URL + "?Command=1&UserId=" + userId,
                        dataType: "json",
                        type: "GET"
                    },

                    ////create: {
                    ////    url: SY.XPOST_WAPI_INSERT_CAMPAIGN_URL,
                    ////    dataType: "json",
                    ////    type: "POST"

                    ////},
                    update: {
                        url: SY.XPOST_KLASS_APP_CONFIGURATION_SETTINGS_URL,
                        dataType: "json",
                        type: "POST"

                    },
                    ////destroy: {
                    ////    url: SY.XPOST_WAPI_DELETE_CAMPAIGN_URL,
                    ////    dataType: "json",
                    ////    type: "POST"
                    ////},
                    parameterMap: function (data, action) {
                        var dat: any;
                        if (action === "update") {
                            dat = data;
                            dat.ModUser = userId;
                            if (dat.Status === 'D') {
                                dat.IsActive = false;
                            }
                            ////dat.VendorId = vendorid;
                            ////dat.IsActive = (dat.IsActive) ? 1 : 0;
                            ////dat.FilterRejectByCounty = (dat.FilterRejectByCounty) ? 1 : 0;
                            ////dat.FilterRejectDuplicates = (dat.FilterRejectDuplicates) ? 1 : 0;
                            return kendo.stringify(dat);
                        }
                        ////if (action == "create") {
                        ////    dat = data;
                        ////    dat.VendorId = vendorid;
                        ////    dat.IsActive = (dat.IsActive) ? 1 : 0;
                        ////    dat.FilterRejectByCounty = (dat.FilterRejectByCounty) ? 1 : 0;
                        ////    dat.FilterRejectDuplicates = (dat.FilterRejectDuplicates) ? 1 : 0;
                        ////    return kendo.stringify(dat);
                        ////}

                        ////if (action == "destroy") {
                        ////    dat = data;
                        ////    return kendo.stringify(dat.ID);
                        ////}


                        return data;
                    }
                },
                error: MasterPage.SHOW_DATA_SOURCE_ERROR,
                serverPaging: false,
                serverSorting: false,
                serverFiltering: false,

                pageSize: 10,
                //filter: { field: "ID", operator: "eq", value: e.data.ID },
                schema: {
                    data: response => response.ConfigurationSetting,
                    total: response => response.ConfigurationSetting.length,
                    model: {
                        id: "Id",
                        fields: {
                            Id: { editable: false, visible: false },
                            KlassAppIdentification: { editable: false, visible: true },
                            CodeEntity: { editable: false, validation: { required: true } },
                            CodeOperation: { editable: false, validation: { required: true } },
                            IsCustomField: { editable: false, type: "boolean" },
                            IsActive: { editable: true, type: "boolean" },
                            ItemStatus: { editable: true, validation: { required: true } },
                            ItemValue: { editable: false, validation: { required: false } },
                            ItemLabel: { editable: true, validation: { required: true } },
                            ItemValueType: { editable: false, validation: { required: true } },
                            CreationDate: { editable: false, type: "date" },
                            ModDate: { editable: false, type: "date" },
                            ModUser: { editable: false, visible: false },
                            LastOperationLog: { editable: false, visible: true }
                        }
                    }
                }
                ////,
                ////requestEnd: function(e) {

                ////    if (e.type === "update" && !e.response.Errors) {
                ////        let response = e.response;
                ////        let grid = $("#klassAppGrid").data("kendoGrid");
                ////        if (response.IsActive) {
                ////             grid.columns[6].editable = false;
                ////        }

                ////        if (response.isActive === 0)
                ////           MasterPage.SHOW_INFO_WINDOW("Record Updated");
                ////        }


                ////}
            });

            return ds;
        }

        sentConfigurationToKlassApp() {
            let mdb: MobileDb = new MobileDb();
            MasterPage.Common.displayLoading(document.body, true);
            mdb.sendConfiguration(2, XMASTER_PAGE_USER_OPTIONS_USERID, this)
                .done(msg => {
                    if (msg != undefined) {
                        let settings: ConfigurationSetting[] = msg.ConfigurationSetting;
                        let grid = $("#klassAppGrid").data("kendoGrid");
                        grid.dataSource.data(settings);
                        grid.refresh();
                        let error: boolean = false;
                        for (var i = 0; i < settings.length; i++) {
                            if (settings[i].LastOperationLog !== "OK") {
                                error = true;
                            }
                        }
                        let message = error
                            ? "Some errors happen, Please see log in the table to more information"
                            : "Configuration updated successfully";
                        MasterPage.SHOW_INFO_WINDOW(message);
                    }
                })
                .always(() => {
                    MasterPage.Common.displayLoading(document.body, false);
                })
                .fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });


        };

        sendKlassAppConfigurationToAdvantage() {
            let mdb: MobileDb = new MobileDb();
            MasterPage.Common.displayLoading(document.body, true);
            mdb.sendConfiguration(3, XMASTER_PAGE_USER_OPTIONS_USERID, this)
                .done(msg => {
                    if (msg != undefined) {
                        let settings = msg.ConfigurationSetting;
                        let grid = $("#klassAppGrid").data("kendoGrid") as kendo.ui.Grid;
                        grid.dataSource.data(settings);
                        grid.refresh();
                        MasterPage.SHOW_INFO_WINDOW("Configuration Initialized successfully");
                    }
                })
                .always(() => {
                    MasterPage.Common.displayLoading(document.body, false);
                })
                .fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }

        categoryDropDownEditor(container, options) {
            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: true,
                    dataTextField: "Text",
                    dataValueField: "Value",
                    dataSource: [{ "Text": "Insert", "Value": "I" }, { "Text": "Update", "Value": "U" }, { "Text": "Inactivate", "Value": "D" }, { "Text": "No Change", "Value": "N" }]

                });
        }

        isActiveEditable(dataItem) {

            return !dataItem;

        }

        sendToKlassAppClearToken() {
            let mdb: MobileDb = new MobileDb();
            MasterPage.Common.displayLoading(document.body, true);
            mdb.sendConfiguration(10, XMASTER_PAGE_USER_OPTIONS_USERID, this)
                .done(msg => {
                    if (msg != undefined) {
                        // Clear also Advantage Configuration....
                        mdb.sendConfiguration(3, XMASTER_PAGE_USER_OPTIONS_USERID, this)
                            .done(msg1 => {
                                if (msg1 != undefined) {
                                    let settings1 = msg1.ConfigurationSetting;
                                    let grid1 = $("#klassAppGrid").data("kendoGrid") as kendo.ui.Grid;
                                    grid1.dataSource.data(settings1);
                                    grid1.refresh();
                                    MasterPage.SHOW_INFO_WINDOW("Klass App. Configuration Deleted successfully");
                                }
                            })
                            .always(() => {
                                MasterPage.Common.displayLoading(document.body, false);
                            })
                            .fail(msg1 => {
                               
                                let grid1 = $("#klassAppGrid").data("kendoGrid") as kendo.ui.Grid;
                                let data: any = grid1.dataSource.data();
                                for (let i = 0; i < data.length; i++) {

                                    data[i].LastOperationLog = msg1.responseText;
                                }
                                grid1.refresh();
                                MasterPage.SHOW_DATA_SOURCE_ERROR(msg1);
                            });
                    }
                })
                .always(() => {
                    MasterPage.Common.displayLoading(document.body, false);
                })
                .fail(msg => {
                    let grid1 = $("#klassAppGrid").data("kendoGrid") as kendo.ui.Grid;
                    let data: any = grid1.dataSource.data();
                    for (let i = 0; i < data.length; i++) {
                        let cleantest = msg.responseText;
                        cleantest = cleantest.replace(/<\p>/gm, "");
                        cleantest = cleantest.replace(/<\/p>/gm, "");
                        data[i].LastOperationLog = cleantest;
                     }
                    grid1.refresh();
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });




        };
    }
}