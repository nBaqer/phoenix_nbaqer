﻿module SY {
    // ReSharper disable InconsistentNaming
    /*
     * 
     */
    export interface IConfigurationSetting {
        /*
         * 
         */

        AdvantageIdentification: string;

        /*
         * 
         */
        KlassAppIdentification: number;

        /*
         * 
         */
        CodeEntity: string;

        /*
         * 
         */
        CodeOperation: string;

        /*
         * 
         */
        IsCustomField: number;

        /*
         * 
         */
        IsActive: number;

        /*
         * 
         */
        ItemStatus: string;

        /*
         * 
         */
        ItemValue: string;

        /*
         * 
         */
        ItemLabel: string;

        /*
         * 
         */
        ItemValueType: string;

        /*
         * 
         */
        CreationDate: Date;

        /*
         * 
         */
        ModDate: Date;

        /*
         * 
         */
        ModUser: string;

        /**
         * The result of the last operation with the server
         */
        LastOperationLog: string;

    }

    /*
     * 
     */
    export class ConfigurationSetting implements IConfigurationSetting{
        /*
         * 
         */
        AdvantageIdentification: string;

        /*
         * 
         */
        KlassAppIdentification: number;

        /*
         * 
         */
        CodeEntity: string;

        /*
         * 
         */
        CodeOperation: string;

        /*
         * 
         */
        IsCustomField: number;

        /*
         * 
         */
        IsActive: number;

        /*
         * 
         */
        ItemStatus: string;

        /*
         * 
         */
        ItemValue: string;

        /*
         * 
         */
        ItemLabel: string;

        /*
         * 
         */
        ItemValueType: string;

        /*
         * 
         */
        CreationDate: Date;

        /*
         * 
         */
        ModDate: Date;

        /*
         * 
         */
        ModUser: string;

        /**
         * The result of the last operation with the server
         */
        LastOperationLog: string;
    }
    // ReSharper restore InconsistentNaming
}
