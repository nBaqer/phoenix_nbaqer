﻿namespace SY {

    export class MobileDb {

        constructor() {

        }

        // Data Source for pages drop down list
        public sendConfiguration(command:number, userId: string, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_KLASS_APP_CONFIGURATION_SETTINGS_URL + "?Command=" + command + "&UserId=" + userId,
                type: "GET",
                timeout: 60000,
                dataType: "json"
            });
        }


    }
}