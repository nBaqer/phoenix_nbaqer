﻿/// <reference path="../ServiceUrls.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
module Wapi {
    export class WapiConfigDb {

        //DataSourceError(e) {
        //    alert(e.xhr.responseText);
        //}
        //#endregion

        //#region CRUD the WAPI Operation Settings

        public getWapiSettingConfigDataSource(id: number) {
            var url = SY.XGET_WAPI_OPERATION_SETTINGS_URL + "?IdOperation=" + id;
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }
                },
                error: e => { MasterPage.SHOW_DATA_SOURCE_ERROR(e); },

                schema: {
                    model: {
                        id: "ID",
                        CodeOperation: "CodeOperation",
                        CodeExtCompany: "CodeExtCompany",
                        DescripExtCompany: "DescripExtCompany",
                        CodeExtOperationMode: "CodeExtOperationMode",
                        DescripExtOperationMode: "DescripExtOperationMode",
                        CodeWapiServ: "CodeWapiServ",
                        DescripWapiServ: "DescripWapiServ",
                        SecondCodeWapiServ: "SecondCodeWapiServ",
                        SecondDescWapiServ: "SecondDescWapiServ",
                        IsActiveWapiServ: "IsActiveWapiServ",
                        ConsumerKey: "ConsumerKey",
                        PrivateKey: "PrivateKey",
                        OperationSecInterval: "OperationSecInterval",
                        PollSecOnDemandOperation: "PollSecOnDemandOperation",
                        FlagOnDemandOperation: "FlagOnDemandOperation",
                        FlagRefreshConfig: "FlagRefreshConfig",
                        ExternalUrl: "ExternalUrl",
                        DateLastExecution: "DateLastExecution",
                        IsActiveOperation: "IsActiveOperation",
                        UserName: "UserName"
                    }
                },
                pageSize: 10
            });
            return ds;
        }
        public DeleteOperationSettings(idOperation: number, contex: Object) {
            return $.ajax({
                url: SY.XPOST_DELETE_WAPI_OPERATION_URL + "?id=" + idOperation,
                context: contex,
                type: 'POST',
                timeout: 5000,
                dataType: 'text'
            });

        }

        public postExternalOperationSettingEdit(
            id, codeExtCompany, codeExtOperationMode, codeWapiServ,
            externalUrl, consumerKey, privateKey,
            operationSecInterval, pollSecOnDemandOperation,
            secondCodeWapiServ, dateLastExecution, isActiveOperation, codeOperation,userName) {

            $.ajax({
                url: SY.XPOST_WAPI_SERVICE_OPERATION_UPDATE_URL,
                type: "POST",
                dataType: "text",
                data: JSON.stringify({
                    ID: id,
                    CodeExtCompany: codeExtCompany,
                    CodeExtOperationMode: codeExtOperationMode,
                    CodeWapiServ: codeWapiServ,
                    ExternalUrl: externalUrl,
                    ConsumerKey: consumerKey,
                    PrivateKey: privateKey,
                    OperationSecInterval: operationSecInterval,
                    PollSecOnDemandOperation: pollSecOnDemandOperation,
                    SecondCodeWapiServ: secondCodeWapiServ,
                    DateLastExecution: dateLastExecution,
                    IsActiveOperation: isActiveOperation,
                    CodeOperation: codeOperation,
                    UserName: userName
                })
            }).always(msg => {
                var window = $("#WindowsWapiOperationSettingEditGrid").data("kendoWindow");
                window.close();
                window.setOptions({
                    visible: false
                });
            })
                .done(msg => {
                    MasterPage.SHOW_INFO_WINDOW("Operation Successful! The new configuration has not effect until you restart WAPI Windows Service!!");
                    //// alert("Operation Successful! The new configuration has not effect until you restart WAPI Windows Service!!");
                    $('#WapiOperationSettingGrid').data('kendoGrid').dataSource.read();
                    $('#WapiOperationSettingGrid').data('kendoGrid').refresh();
                })
                .fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    ////alert("Error Received: Code:" + msg.status + " " + msg.statusText + ". " + msg.responseText);
                });

        }

        postExternalOperationSettingNew(operation: Object) {
            $.ajax({
                url: SY.XPOST_WAPI_SERVICE_OPERATION_NEW_URL,
                type: "POST",
                dataType: "text",
                data: JSON.stringify(operation)
            }).done(msg => {
                
                var window = $("#WindowWapiAddNewExternalOperation").data("kendoWindow");
                window.close();
                window.setOptions({
                    visible: false
                });
                // Refresh Grid
                MasterPage.SHOW_INFO_WINDOW("Operation Successful! The new configuration has not effect until you restart WAPI Windows Service!!");
                ////alert("Operation Successful! The new configuration has not effect until you restart WAPI Windows Service!!");
                $('#WapiOperationSettingGrid').data('kendoGrid').dataSource.read();
                $('#WapiOperationSettingGrid').data('kendoGrid').refresh();

            }).fail(msg => {

                MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
            });
        }

        //#endregion

        //#region Windows Services Data Sources....
        public GetWindowsServiceStatus(contex: Object) {
            return $.ajax(
                {
                    url: SY.XGET_WAPI_WINDOWS_SERVICE_STATUS_URL,
                    context: contex,
                    type: "GET",
                    dataType: "text",
                    data: { ServiceName: 'WapiWindowsService' }
                });
        }

        StopwindowsService() {
            $("#WapiConfigStopButton").data("kendoButton").enable(false);
            $("#WapiConfigStartButton").data("kendoButton").enable(false);
            var filter = {};
            filter["ServiceName"] = "WapiWindowsService";
            filter["Command"] = "stop";
            return $.ajax(
                {
                    url: SY.XPOST_WAPI_WINDOWS_SERVICE_OPERATION_URL,
                    type: "POST",
                    dataType: "text",
                    contentType: "application/json",
                    data: JSON.stringify(filter)
                });
        }

        StartwindowsService() {
            $("#WapiConfigStopButton").data("kendoButton").enable(false);
            $("#WapiConfigStartButton").data("kendoButton").enable(false);
            var filter = {};
            filter["ServiceName"] = "WapiWindowsService";
            filter["Command"] = "start";
            return $.ajax(
                {
                    url: SY.XPOST_WAPI_WINDOWS_SERVICE_OPERATION_URL,
                    type: "POST",
                    dataType: "text",
                    data: JSON.stringify(filter)
                });
        }
        ///#endregion

        //#region Get Catalogs

        // The entered values has the following format
        //Id, Code, Description, Url, IsActive
        public getWapiExternalCompaniesDataSource() {
            var url = SY.XGET_WAPI_CATALOGS_URL + '?CatalogOperation=COMPANY';
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }
                },
                error: (e) => MasterPage.SHOW_DATA_SOURCE_ERROR(e),
                pageSize: 10
            });
            return ds;
        }

        public GetOperationModeDataSource() {
            var url = SY.XGET_WAPI_CATALOGS_URL + '?CatalogOperation=MODE';
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }
                },
                error: (e) => MasterPage.SHOW_DATA_SOURCE_ERROR(e)
            });
            return ds;
        }

        public GetAllowedOperationDataSource() {
            var url = SY.XGET_WAPI_CATALOGS_URL + '?CatalogOperation=WSERVICES';
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }
                },
                schema: {
                    model: {
                        id: "Id",
                        fields: {
                            Code: { editable: false, nullable: false },
                            IsActive: { type: "boolean", editable: true },
                            Description: { editable: true }
                        }
                    }
                },

                error: (e) => MasterPage.SHOW_DATA_SOURCE_ERROR(e)
            });
            return ds;
        }

        //#endregion

        public postSynchro(contex: Object) {
            return $.ajax({
                url: SY.XPOST_WAPI_CATALOGS_URL,
                context: contex,
                type: 'POST',
                data: JSON.stringify({ IdParent: 0, CatalogOperation: "SYNCHRO_COMPANIES", Changes: null }),
                timeout: 10000,
                dataType: 'json'
            });
        }

        //#region On Demand Flag
        public GetOnDemandFlag(id: number) {
            return $.ajax({
                url: SY.XGET_WAPI_ON_DEMAND_FLAGS + "?Id=" + id,
                type: 'GET',
                timeout: 5000,
                dataType: 'json'
                //data: JSON.stringify({ ID: id }),
            });
        }

        public PostOnDemandFlag(id: number, value: number, contex: Object) {
            return $.ajax({
                url: SY.XPOST_WAPI_ON_DEMAND_FLAGS + "?Id=" + id + "&Value=" + value,
                context: contex,
                type: 'POST',
                timeout: 5000,
                dataType: 'text'
            });
        }
        //#endregion

        //#region Security Key
        CreateSecurityKey() {
            $.ajax({
                url: SY.XGET_WAPI_SECURITY_KEY_URL + "?operation=1",
                type: 'GET',
                dataType: 'json'
            }).done(msg => {
                // Put result in Text field
                $('#textAreaSecurity').text(msg);
                }).fail(msg => {
                MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                ////alert("Error Received: Code:" + msg.status + " " + msg.statusText + ". " + msg.responseText);
            });
        }

        DeleteSecurityKey() {
            $.ajax({
                url: SY.XGET_WAPI_SECURITY_KEY_URL + "?operation=2",
                type: 'GET',
                dataType: 'json'
            }).done(msg => {
                // Put result in Text field
                $('#textAreaSecurity').text(msg);
                }).fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                ////alert("Error Received: Code:" + msg.status + " " + msg.statusText + ". " + msg.responseText);
            });
        }

        GetSecurityKey() {
            $.ajax({
                url: SY.XGET_WAPI_SECURITY_KEY_URL + "?operation=3",
                type: 'GET',
                dataType: 'json'
            }).done(msg => {
                // Put result in Text field
                $('#textAreaSecurity').text(msg);
            }).fail(msg => {
                prompt("Copy To Clipboard: Ctrl+C, Enter", "Error Received: Code:" + msg.status + " " + msg.statusText + ". " + msg.responseText);
            });
        }
        //#endregion
    }
}