﻿/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../IQueryString.ts" />
/// <reference path="../../fame.advantage.client.masterpage/advantagevalidation.ts" />

module Wapi {

    var wapiSelectedOperationSettingId: number;
    
    export class WapiConfigVm extends kendo.Observable {

        public WapiDataServices: WapiConfigDb;
        public WapiSettingConfigDataSource: kendo.data.DataSource;
        public WapiExternalCompaniesDataSource: kendo.data.DataSource;
        public WapiOperationModeDataSource: kendo.data.DataSource;
        public wapiAllowedOperationDataSource: kendo.data.DataSource;
        private SelectedRow: any;
        public ElapsedTime: number;
        public maxTime: number;
        private WapiIntervalTimer: number;  // used to stop to wait for on demand operation.
        private windowsServiceStatus: string;

        constructor() {
            super();
            /////this.wapiValidator = MasterPage.ADVANTAGE_VALIDATOR("WapiConfigContainer");
            this.WapiDataServices = new WapiConfigDb();
            this.WapiSettingConfigDataSource = this.WapiDataServices.getWapiSettingConfigDataSource(0);
            this.WapiExternalCompaniesDataSource = this.WapiDataServices.getWapiExternalCompaniesDataSource();
            this.WapiOperationModeDataSource = this.WapiDataServices.GetOperationModeDataSource();
            this.wapiAllowedOperationDataSource = this.WapiDataServices.GetAllowedOperationDataSource();
            this.WapiIntervalTimer = 0;
        }

        //#region TAB Windows Service ..................................
        ScanWindowsService() {
            // Get the status of the service....
            var db = new WapiConfigDb(); //this.WapiDataServices;
            db.GetWindowsServiceStatus(this)
                .done(msg => {
                    $("#WapiTextServiceStatus").text(msg);
                    //Clean message
                    msg = msg.replace("\"", '');
                    msg = msg.replace("\"", '');
                    msg = msg.trim();
                    this.windowsServiceStatus = msg.toLowerCase();
                    switch (this.windowsServiceStatus) {
                        case "running":
                            {
                                $("#WapiConfigStopButton").data("kendoButton").enable(true);
                                $("#WapiConfigStartButton").data("kendoButton").enable(false);
                                $("#WapiImageServiceStatus").attr("src", "../images/geargreen.jpg");

                                break;
                            }
                        case "stopped":
                        case "paused":
                            {
                                $("#WapiConfigStopButton").data("kendoButton").enable(false);
                                $("#WapiConfigStartButton").data("kendoButton").enable(true);
                                $("#WapiImageServiceStatus").attr("src", "../images/gearred.jpg");


                                break;
                            }
                        case "uninstalled":
                            {
                                $("#WapiConfigStopButton").data("kendoButton").enable(false);
                                $("#WapiConfigStartButton").data("kendoButton").enable(false);
                                $("#WapiImageServiceStatus").attr("src", "../images/geargray.jpg");

                                break;
                            }
                        default:
                            {
                                // Intermediate status please call again
                                var self = this;
                                window.setTimeout(function () { self.ScanWindowsService(); }, 2000);//self.WapiDataServices.GetWindowsServiceStatus(self); 
                            }
                    }



                }).fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    //this.GetWindowsServiceStatus();

                });


        }

        StartService(e: Object) {
            var db = this.WapiDataServices;
            db.StartwindowsService()
                .done(msg => {
                    $("#WapiTextServiceStatus").text(msg);
                    this.ScanWindowsService();
                }).fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    this.ScanWindowsService();
                });
        }

        StopService(e: Object) {
            var db = this.WapiDataServices;
            db.StopwindowsService()
                .done(msg => {
                    $("#WapiTextServiceStatus").text(msg);
                }).fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);

                }).always(msg => {
                    this.ScanWindowsService();

                });
        }

        //Change the page to logger
        GoLoggerPageButton(e: any) {
            e.preventDefault();
            var qs;
            //var loc = location.href;
            var campusId = $.fn.QueryString["cmpid"];
            if (campusId != null) {
                qs = XMASTER_GET_BASE_URL + "/SY/WapiLogger.aspx?resid=811&mod=SY&cmpid=" + campusId + "&desc=Wapi Logger";

            }
            else {
                qs = XMASTER_GET_BASE_URL + "/SY/WapiLogger.aspx?resid=811&mod=SY&desc=Wapi Logger";

            }

            location.href = qs;
        }
        ///#endregion

        //#region TAB Operation Settings: Window Edit External Operation Settings

        // Wapi Operation Settings Manager TAP
        public AddNewExternalOperation(e: Object) {
            var window = $("#WindowWapiAddNewExternalOperation").data("kendoWindow");
            // Activate TAB 1
            var tabToActivate = $("#wapitab1");
            $("#WapiWindowTabstripNewOperation").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);
            // Clear Name
            $('#tbNewOperationSettingName').val("");
            $('#NewwindowsServicePollInterval').val("60");
            $('#NewWindowOnDemandTimeOut').val("240");
            var ddaSecllo = $('#ddNewSecondWapiService').data('kendoDropDownList');
            ddaSecllo.text("NONE");
            window.setOptions({
                visible: true
            });
            // window.visible = true;
            window.open();
            window.center();
        }

        public CreateNewExternalOperation(e: Object) {

            var operation = {
                CodeOperation: $('#tbNewOperationSettingName').val(),
                CodeExtCompany: $('#ddNewCodeCompany').data('kendoDropDownList').text(),
                CodeExtOperationMode: $('#ddNewCodeModeOperation').data('kendoDropDownList').text(),
                CodeWapiServ: $('#ddNewCodeWapiService').data('kendoDropDownList').text(),
                SecondCodeWapiServ: $('#ddNewSecondWapiService').data('kendoDropDownList').text(),
                ConsumerKey: $('#tbNewwindowwapiConsumerKey').val(),
                PrivateKey: $('#tbwNewindowwapiPrivateKey').val(),
                OperationSecInterval: $('#NewwindowsServicePollInterval').val(),
                PollSecOnDemandOperation: $('#NewWindowOnDemandTimeOut').val(),
                ExternalUrl: $('#tbNewExternalUrl').val(),
                DateLastExecution: new Date(),
                IsActiveOperation: $("#cbNewWapiActiveOperation").prop("checked"),
                UserName: $('#tbwNewindowwapiUserName').val()
            };

            this.WapiDataServices.postExternalOperationSettingNew(operation);
        }

        public EditOperationSettings(row: any) {
            this.SelectedRow = row;
            //var db = new WapiConfigDb();
            //this.WapiExternalCompaniesDataSource = db.GetWapiExternalCompaniesDataSource();
            var window = $("#WindowsWapiOperationSettingEditGrid").data("kendoWindow");
            window.setOptions({
                visible: true
            });
            //window.visible = true;
            window.open();
            window.center();

            var ddop = $('#windowsWapiOperationCompany').data('kendoDropDownList');
            ddop.text(row.CodeExtCompany);
            var ddmod = $('#windowsWapiMode').data('kendoDropDownList');
            ddmod.text(row.CodeExtOperationMode);
            var ddallo = $('#ddCodeWapiService').data('kendoDropDownList');
            ddallo.text(row.CodeWapiServ);
            var ddaSecllo = $('#ddSecondWapiService').data('kendoDropDownList');
            ddaSecllo.dataSource.add({ Id: "0", Code: "NONE", Description: "None", Url: "None", IsActive: true });
            ddaSecllo.text(row.SecondCodeWapiServ);
            $('#tbExternalUrl').val(row.ExternalUrl);
            //$('#windowWapiUrl').val(row.UrlWapiServ);
            $('#tbwindowwapiConsumerKey').val(row.ConsumerKey);
            $('#tbwindowwapiPrivateKey').val(row.PrivateKey);
            //$("#windowIsActiveOperation").prop("checked", row.IsActiveWapiServ);
            $('#windowsServicePollInterval').val(row.OperationSecInterval);
            $('#windowRefreshConfigurationInterval').val(row.PollSecOnDemandOperation);
            $('#tbOperationSettingName').val(row.CodeOperation);
            $('#dtLastExecutedDateTime').data("kendoDateTimePicker").value(kendo.parseDate(row.DateLastExecution));
            $("#cbWapiActiveOperation").prop("checked", row.IsActiveOperation);
            $('#tbwindowwapiUserName').val(row.UserName);
            if (row.CodeExtOperationMode === "WAPI_TEST_SYSTEM" || row.CodeExtOperationMode === "AFA_INTEGRATION_SERVICE") {
                $("#windowsServicePollInterval").attr("data-min", 60);
            } else {
                $("#windowsServicePollInterval").attr("data-min", 1800);

            }
            return 0;
        }

        public DeleteOperationSetting(row: any) {

            //var conf = window.confirm("Are you sure to delete " + row.CodeOperation + "?");
            //if (conf === false) {
            //    return;
            //}
            var that = this;
            $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(`Are you sure to delete ${row.CodeOperation}?`)
                .then(function (confirmed) {
                    if (confirmed) {
                        // YES was selected
                        that.WapiDataServices.DeleteOperationSettings(row.ID, that)
                            .done((msg, status) => {
                                // Update the grid..........
                                var grid = $("#WapiOperationSettingGrid").data("kendoGrid");
                                grid.setDataSource(that.WapiDataServices.getWapiSettingConfigDataSource(0));
                                MasterPage.SHOW_INFO_WINDOW("The Operation was deleted");
                            })
                            .fail((msg) => {

                                MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                            });

                    } else {
                        // NO was selected
                        return;
                    }
                })
            );
        }

        // Click over Save button in Edit Dialog
        public UpdateExternalOperationInServer(object: Object) {
            this.SelectedRow.CodeOperation = $('#tbOperationSettingName').val();
            this.SelectedRow.ExternalUrl = $('#tbExternalUrl').val();
            //this.SelectedRow.UrlWapiServ = $('#windowWapiUrl').val();
            this.SelectedRow.ConsumerKey = $('#tbwindowwapiConsumerKey').val();
            this.SelectedRow.PrivateKey = $('#tbwindowwapiPrivateKey').val();
            //this.SelectedRow.IsActiveWapiServ = $("#windowIsActiveOperation").prop("checked");
            this.SelectedRow.OperationSecInterval = $('#windowsServicePollInterval').val();
            this.SelectedRow.PollSecOnDemandOperation = $('#windowRefreshConfigurationInterval').val();
            this.SelectedRow.CodeExtCompany = $('#windowsWapiOperationCompany').data('kendoDropDownList').text();
            this.SelectedRow.CodeExtOperationMode = $('#windowsWapiMode').data('kendoDropDownList').text();
            this.SelectedRow.CodeWapiServ = $('#ddCodeWapiService').data('kendoDropDownList').text();
            this.SelectedRow.SecondCodeWapiServ = $('#ddSecondWapiService').data('kendoDropDownList').text();
            var date: Date = $('#dtLastExecutedDateTime').data('kendoDateTimePicker').value();
            this.SelectedRow.DateLastExecution = kendo.toString(date, "u");
            // this.SelectedRow.DateLastExecution = $('#dtLastExecutedDateTime').data('kendoDateTimePicker').value();
            this.SelectedRow.IsActiveOperation = $("#cbWapiActiveOperation").prop("checked");
            this.SelectedRow.UserName = $('#tbwindowwapiUserName').val();

            this.WapiDataServices.postExternalOperationSettingEdit(
                this.SelectedRow.ID,
                this.SelectedRow.CodeExtCompany, this.SelectedRow.CodeExtOperationMode, this.SelectedRow.CodeWapiServ,
                this.SelectedRow.ExternalUrl, this.SelectedRow.ConsumerKey,
                this.SelectedRow.PrivateKey, this.SelectedRow.OperationSecInterval,
                this.SelectedRow.PollSecOnDemandOperation,
                this.SelectedRow.SecondCodeWapiServ,
                this.SelectedRow.DateLastExecution,
                this.SelectedRow.IsActiveOperation,
                this.SelectedRow.CodeOperation,
                this.SelectedRow.UserName
            );
        }

        //#endregion

        //#region On Demand Operations

        public ExecuteOnDemandOperation(row: any) {
            var db = new WapiConfigDb();
            wapiSelectedOperationSettingId = row.ID;
            // Send to server the On-demand Flag to 1
            db.PostOnDemandFlag(wapiSelectedOperationSettingId, 1, this)
                .fail(function (msg) {
                    var window = $("#WindowsWapiOnDemandOperation").data("kendoWindow");
                    window.setOptions({
                        visible: false
                    });
                    // window.visible = false;
                    window.close();
                    MasterPage.SHOW_ERROR_WINDOW("On demand Operation Fail to set the Flag: " + msg.statusText);
                    return;
                })
                .done(function () {

                    var windowx = $("#WindowsWapiOnDemandOperation").data("kendoWindow");
                    windowx.setOptions({
                        visible: true
                    });
                    //windowx.visible = true;
                    windowx.open();
                    windowx.center();
                    // Begin process to wait for operation complexion
                    var maxTim = row.PollSecOnDemandOperation;
                    var elapsedTime = 0;
                    var db1 = new WapiConfigDb();
                    this.WapiIntervalTimer = setInterval(function () {
                        if (elapsedTime > maxTim) {
                            var interval = this.WapiIntervalTimer;
                            clearInterval(interval);
                            var window = $("#WindowsWapiOnDemandOperation").data("kendoWindow");
                            window.setOptions({
                                visible: false
                            });
                            // window.visible = false;
                            window.close();

                            db1.PostOnDemandFlag(wapiSelectedOperationSettingId, 0, this);
                            MasterPage.SHOW_WARNING_WINDOW("The On Demand Operation was canceled");
                            return;
                        }
                        // make the call....
                        db1.GetOnDemandFlag(wapiSelectedOperationSettingId)
                            .done(function (flag) {
                                if (flag[0].OnDemandOperation == false) {
                                    clearInterval(this.WapiIntervalTimer);
                                    var win = $("#WindowsWapiOnDemandOperation").data("kendoWindow");
                                    win.setOptions({
                                        visible: false
                                    });
                                    //win.visible = false;
                                    win.close();
                                    MasterPage.SHOW_INFO_WINDOW("The On Demand Operation was successful");
                                    return;
                                }

                                elapsedTime += 2;
                            });
                    }, 2000);
                });
        }

        public pollOnDemandFlag(id) {


        }

        public StopOnDemandOperation() {
            clearInterval(this.WapiIntervalTimer);
            var window = $("#WindowsWapiOnDemandOperation").data("kendoWindow");
            window.setOptions({
                visible: false
            });
            //window.visible = false;
            window.close();
            var db = new WapiConfigDb();
            db.PostOnDemandFlag(wapiSelectedOperationSettingId, 0, this);
            MasterPage.SHOW_INFO_WINDOW("The On Demand Operation was canceled");
        }

        //#endregion

        //#region TAB External Companies Management
        WapiGetExternalCompanies() {
            throw new Error("Not implemented");
        }

        AddNewExternalCompany(e) {
            throw new Error("Not implemented");
        }


        public SyncronizeWithTenant(e) {
            // Get table information

            this.WapiDataServices.postSynchro(this)
                .done(function (msg) {
                    var grid = $("#WapiCompaniesSettingGrid").data("kendoGrid");
                    grid.dataSource.read();
                    grid.refresh();
                    MasterPage.SHOW_INFO_WINDOW("The Synchronize operation was successful");
                })
                .fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);

                });
            // Sent information to server to synchrony
            // Get returned information and show in grid
        }

        //#endregion
        //#region TAB: Security services

        SecurityService(e: any, operation: number) {
            var db = this.WapiDataServices;
            switch (operation) {
                case 1: // Create key
                    {
                        db.CreateSecurityKey();
                        break;
                    }
                case 2: //Delete Key
                    {
                        var res = confirm("Are you sure to delete your key? If you delete it your actual clients can not work any more with WAPI, unless you create other and give it to them");
                        if (res) {
                            var res1 = confirm("Please confirm again...");
                            if (res1) {
                                db.DeleteSecurityKey();
                            }
                        }

                        break;
                    }
                case 3: //Show Key
                    {
                        db.GetSecurityKey();
                        break;
                    }

                default: // Show Error operation does not exist
                    {
                        $('#textAreaSecurity').text("Operation Invalid");
                    }
            }

        }

        // Helper: Select all the text in the text area 
        // of public key in security TAB. Compatible for IE9 or higher only!
        selectAll(e: Object) {
            if (window.getSelection) {
                var range1 = window.document.createRange();
                range1.selectNode(window.document.getElementById('textAreaSecurity'));
                window.getSelection().addRange(range1);
            }
        }

        //#endregion

        //#region Helper General

        public MessageOkBoxIIS(ex: any) {
            if (typeof ex.status != "undefined") {
                if (ex.status == null) {
                    if (ex.type !== "read") {
                        MasterPage.SHOW_INFO_WINDOW(`The ${ex.type} operation was successful. The new configuration has not effect until you restart IIS!!`);
                    }
                }
            }
        }

        //#endregion
    } // End Class
} // End module