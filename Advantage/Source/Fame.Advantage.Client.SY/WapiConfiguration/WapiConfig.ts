﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />

module Wapi {

    export class WapiConfig {

        public ViewModel: WapiConfigVm;
        public wapiValidator: kendo.ui.Validator;

        // public SelectedEnrollment: kendo.Observable;

        // Class Constructor
        constructor() {
            this.ViewModel = new WapiConfigVm();
            var viewModel = this.ViewModel;
 
            //#region Configure Advantage Windows Service Control Tab Strip......................................................
            $("#wapiconfigtabstrip").kendoTabStrip({
                animation: { open: { effects: "fadeIn" } }
            });

            // Configure Start Button
            $("#WapiConfigStartButton").kendoButton({
                enable: false,
                click: e => { viewModel.StartService(e); }
            });

            // Configure Stop Button
            $("#WapiConfigStopButton").kendoButton({
                enable: false,
                click: e => { viewModel.StopService(e); }
            });

            // Configure Go to Logger Page Button
            $("#WapiGoLoggerPageButton").kendoButton({
                enable: true,
                click: e => { viewModel.GoLoggerPageButton(e); }
            });

            viewModel.ScanWindowsService();
            ///#endregion

            //#region Configure WAPI Operation Settings Manager Tab Strip .......................................................

            $("#WapiOperationSettingGrid").kendoGrid({
                dataSource: this.ViewModel.WapiSettingConfigDataSource,
                rowTemplate: kendo.template($("#WapiOperationSettingGrid_rowTemplate").html()),
                altRowTemplate: kendo.template($("#WapiOperationSettingGrid_altrowTemplate").html()),
                resizable:true,
                //width: 800,

                // height: 500,
                pageable: {
                    pageSize: 5,
                    pageSizes: true
                }
            });

            // Configure Add New Configuration Button
            $("#btnWapiSettingAddNew").kendoButton({
                enable: true,
                click: e => { viewModel.AddNewExternalOperation(e); }
            });


            // Edit Row button in Operation Setting Grid
            $("#WapiOperationSettingGrid").on("click", ".k-grid-edit", e => {
                var row = $(e.target).closest("tr");
                var grid = $("#WapiOperationSettingGrid").data("kendoGrid");
                var dataItem = grid.dataItem(row);
                viewModel.EditOperationSettings(dataItem);
            });

            // Execute Delete row button in Operation Setting Grid
            $("#WapiOperationSettingGrid").on("click", ".k-grid-delete", e => {
                var row = $(e.target).closest("tr");
                var grid = $("#WapiOperationSettingGrid").data("kendoGrid");
                var dataItem = grid.dataItem(row);
                viewModel.DeleteOperationSetting(dataItem);
            });

            //#endregion

            //#region Edit Windows for Operation Setting table
            this.wapiValidator = MasterPage.ADVANTAGE_VALIDATOR("WindowsWapiOperationSettingEditGrid");

            $("#WindowsWapiOperationSettingEditGrid").kendoWindow(
                {
                    actions: ["Close"],
                    title: "Advantage WAPI: Configure External Operation Settings",
                    width: "690px",
                    height: "500px",
                    visible: false,
                    modal: true,
                    resizable: false,
                    close: (e) => {
                        if (e.userTriggered) {
                            MasterPage.ADVANTAGE_VALIDATOR_HIDDEN_VALIDATION("WindowsWapiOperationSettingEditGrid");
                        }
                    }
                });

            $("#dtLastExecutedDateTime").kendoDateTimePicker({

            });

            $("#windowsWapiOperationCompany").kendoDropDownList(
                {
                    dataTextField: "Code",
                    dataValueField: "Id",
                    dataSource: this.ViewModel.WapiExternalCompaniesDataSource,
                });

            $("#windowsWapiMode").kendoDropDownList(
                {
                    dataSource: this.ViewModel.WapiOperationModeDataSource,
                    dataTextField: "Code",
                    dataValueField: "Id"

                });

            $('#ddCodeWapiService').kendoDropDownList(
                {
                    dataSource: this.ViewModel.wapiAllowedOperationDataSource,
                    dataTextField: "Code",
                    dataValueField: "Id"
                   
                });

            // set to lengthly item
            $('#ddCodeWapiService').data("kendoDropDownList").list.width("auto");

            $('#ddSecondWapiService').kendoDropDownList(
                {
                    dataSource: this.ViewModel.wapiAllowedOperationDataSource,
                    dataTextField: "Code",
                    dataValueField: "Id"
                });

            $('#ddSecondWapiService').data("kendoDropDownList").list.width("auto");


            // Save Button in the windows of update image
            $("#windowbtnEditSave").kendoButton({
                click: e => {
                    if (this.wapiValidator.validate()) {
                        viewModel.UpdateExternalOperationInServer(e);
                    }
                }
            });

            //#endregion

            //#region New Operation Setting Windows

            $("#WindowWapiAddNewExternalOperation").kendoWindow(
                {
                    actions: ["Close"],
                    title: "Advantage WAPI: Configure External Operation Settings",
                    width: "745px",
                    height: "410px",
                    visible: false,
                    modal: true,
                    resizable: false
                });

            $("#WapiWindowTabstripNewOperation").kendoTabStrip({
              //  width: "100%",
              //  height: "400px"

            });

            $($("#WapiWindowTabstripNewOperation").data("kendoTabStrip").items()[0]).attr("style", "display:none");
            $($("#WapiWindowTabstripNewOperation").data("kendoTabStrip").items()[1]).attr("style", "display:none");
            $($("#WapiWindowTabstripNewOperation").data("kendoTabStrip").items()[2]).attr("style", "display:none");
            $($("#WapiWindowTabstripNewOperation").data("kendoTabStrip").items()[3]).attr("style", "display:none");

            // Wizard button operation

            $("#btnWapiWizardNext1").kendoButton({
                click: function () {
                    var tabToActivate = $("#wapitab2");
                    $("#WapiWindowTabstripNewOperation").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);
                }
            });

            $("#btnWapiWizardNext2").kendoButton({
                click: function () {
                    var tabToActivate = $("#wapitab3");
                    $("#WapiWindowTabstripNewOperation").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);
                }
            });


            $("#btnWapiWizardPrevius1").kendoButton({
                click: function () {
                    var tabToActivate = $("#wapitab1");
                    $("#WapiWindowTabstripNewOperation").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);
                }
            });

            $("#btnWapiWizardPrevius2").kendoButton({
                click: function () {
                    var tabToActivate = $("#wapitab2");
                    $("#WapiWindowTabstripNewOperation").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);
                }
            });

            $("#btnWapiWizardFinish").kendoButton({
                click: function (e) {
                    viewModel.CreateNewExternalOperation(e);
                }
            });


            $('#ddNewCodeModeOperation').kendoDropDownList(
                {
                    dataSource: this.ViewModel.WapiOperationModeDataSource,
                    dataTextField: "Code",
                    dataValueField: "Id"
                });

            $('#ddNewCodeCompany').kendoDropDownList(
                {
                    dataSource: this.ViewModel.WapiExternalCompaniesDataSource,
                    dataTextField: "Code",
                    dataValueField: "Id"
                });

            $('#ddNewCodeWapiService').kendoDropDownList(
                {
                    dataSource: this.ViewModel.wapiAllowedOperationDataSource,
                    dataTextField: "Code",
                    dataValueField: "Id"
                });

            $('#ddNewSecondWapiService').kendoDropDownList(
                {
                    dataSource: this.ViewModel.wapiAllowedOperationDataSource,
                    dataTextField: "Code",
                    dataValueField: "Id"
                });


            //#endregion

            //#region TAB External Companies Management

            $("#WapiCompaniesSettingGrid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: SY.XGET_WAPI_CATALOGS_URL + '?CatalogOperation=COMPANY',
                            dataType: "json",
                            type: "GET"
                        },
                        update: {
                            url: SY.XPOST_WAPI_CATALOGS_URL,
                            type: "POST",
                            dataType: "json"
                        },
                        create: {
                            url: SY.XPOST_WAPI_CATALOGS_URL,
                            type: "POST",
                            dataType: "json"
                        },
                        destroy: {
                            url: SY.XPOST_WAPI_CATALOGS_URL,
                            type: "POST",
                            dataType: "json"
                        },
                        parameterMap: function (data, action) {
                            var dat: any;
                            if (action === "update") {
                                dat = { "IdParent": 0, "CatalogOperation": "UPDATE_COMPANIES", "Changes": data.models };
                                return kendo.stringify(dat);
                            }
                            if (action === "create") {
                                dat = { "IdParent": 0, "CatalogOperation": "INSERT_COMPANIES", "Changes": data.models };
                                return kendo.stringify(dat);
                            }

                            if (action === "destroy") {
                                dat = { "IdParent": 0, "CatalogOperation": "DELETE_COMPANIES", "Changes": data.models };
                                return kendo.stringify(dat);
                            }

                            return data;
                        }

                    },
                    schema: {
                        model: {
                            id: "Id",
                            fields: {
                                Code: { editable: true, nullable: false },
                                IsActive: { type: "boolean", editable: true },
                                Description: { editable: true }
                            }
                        }
                    },
                    pageSize: 10,
                    error: e => { MasterPage.SHOW_DATA_SOURCE_ERROR(e); },
                    batch: true,
                    requestEnd: function (e) { viewModel.MessageOkBoxIIS(e); }

                }, // DataSource End

                height: 600,
                toolbar: ["create", "save", "cancel"],

                //pageSize: 10,
                sortable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                detailInit: detailInitOp,
                dataBound: function () {
                    // this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                columns: [
                    { field: "Id", title: "", width: "1px" },
                    { command: [{ name: "destroy", className: "command-destroy" }] },
                    { field: "Code", title: "Code", width: "150px" },
                    { field: "IsActive", title: "Active", width: "50px" },
                    {
                        field: "Description"
                        //width: "230px"
                    }

                ],
                editable: true

            });


            function detailInitOp(e) {
                $("<div/>").appendTo(e.detailCell).kendoGrid({
                    dataSource: {
                        transport: {
                            read: {
                                url: SY.XGET_WAPI_CATALOGS_URL + "?CatalogOperation=SERVICES_ASSOCIATED_WITH_COMPANY&IdParent=" + e.data.Id,
                                dataType: "json"
                            },
                            update: {
                                url: SY.XPOST_WAPI_CATALOGS_URL,
                                type: "POST",
                                dataType: "json"
                            },

                            parameterMap: function (data, action) {
                                if (action !== "read") {
                                    var dat = { "IdParent": e.data.Id, "CatalogOperation": "ALLOWSERVICE_COMPANY_RELATION", "Changes": data.models };
                                    return kendo.stringify(dat);
                                }

                                return data;
                            }

                        },

                        error: ex => {
                            MasterPage.SHOW_ERROR_WINDOW("An Error happen when accessing server: " + ex.status);
                            ////alert("An Error happen when accessing server: " + ex.status);
                        },

                        requestEnd: function (ex) { viewModel.MessageOkBoxIIS(ex); },
                        batch: true,
                        pageSize: 10,

                        schema: {
                            model: {
                                id: "Id",
                                fields: {
                                    Code: { editable: false, nullable: false },
                                    IsActive: { editable: false },
                                    //  Url: { editable: false },
                                    Authorized: { type: "boolean", editable: true },
                                    Description: { editable: false }
                                }
                            }
                        }
                    },
                    toolbar: ["save", "cancel"],

                    scrollable: false,
                    sortable: true,
                    pageable: true,
                    columns: [
                        { field: "Id", title: "", width: "1px" },
                        { field: "Code", title: "Code", width: "190px" },
                        { field: "Authorized", title: "Authorized", width: "90px" },
                        { field: "IsActive", title: "Active", width: "50px" },
                        { field: "Description" }
                    ],
                    editable: true
                });
            }



            //Configure Add New Configuration Button
            $("#btnWapiCompaniesSynchro").kendoButton({
                enable: true,
                click: e => { viewModel.SyncronizeWithTenant(e); }
            });

            //#endregion

            //#region TAB Wapi Security Manager

            // Create Button 
            $("#btnCreate").kendoButton({
                click: e => {
                    e.preventDefault();
                    viewModel.SecurityService(e, 1);
                }
            });

            // Delete button 
            $("#btnDelete").kendoButton({
                click: e => {
                    e.preventDefault();
                    viewModel.SecurityService(e, 2);
                }
            });

            // View Security Key.
            $("#btnShow").kendoButton({
                click: e => {
                    e.preventDefault();
                    viewModel.SecurityService(e, 3);
                }
            });

            // Select All Button
            $("#btnSelectAll").kendoButton({
                click: e => {
                    e.preventDefault();
                    viewModel.selectAll(e);
                }
            });


            ///#endregion

            //#region TAB Allowed Services

            $("#WapiAllowedGrid").kendoGrid({
                resizable:true,
                dataSource: {
                    transport: {
                        read: {
                            url: SY.XGET_WAPI_CATALOGS_URL + '?CatalogOperation=WSERVICES',
                            dataType: "json",
                            type: "GET"
                        },
                        update: {
                            url: SY.XPOST_WAPI_CATALOGS_URL,
                            type: "POST",
                            dataType: "json"
                        },
                        create: {
                            url: SY.XPOST_WAPI_CATALOGS_URL,
                            type: "POST",
                            dataType: "json"
                        },
                        destroy: {
                            url: SY.XPOST_WAPI_CATALOGS_URL,
                            type: "POST",
                            dataType: "json"
                        },

                        parameterMap: function (data, action) {
                            var dat: any;

                            if (action === "update") {
                                dat = { "IdParent": 0, "CatalogOperation": "UPDATE_ALLOWED_SERVICES", "Changes": data.models };
                                return kendo.stringify(dat);
                            }
                            if (action === "create") {
                                dat = { "IdParent": 0, "CatalogOperation": "INSERT_ALLOWED_SERVICES", "Changes": data.models };
                                return kendo.stringify(dat);
                            }
                            if (action === "destroy") {
                                dat = { "IdParent": 0, "CatalogOperation": "DELETE_ALLOWED_SERVICES", "Changes": data.models };
                                return kendo.stringify(dat);
                            }

                            return data;
                        }

                    },
                    schema: {
                        model: {
                            id: "Id",
                            fields: {
                                Code: { editable: true, nullable: false },
                                IsActive: { type: "boolean", editable: true },
                                Description: { editable: true }
                            }
                        }
                    },
                    pageSize: 10,
                    error: e => {
                        MasterPage.SHOW_DATA_SOURCE_ERROR(e);
                    },
                    batch: true,
                    requestEnd: e => { viewModel.MessageOkBoxIIS(e); }
                },

                height: 600,
                toolbar: ["create", "save", "cancel"],

                //pageSize: 10,
                sortable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                detailInit: detailInit,
                dataBound: function () {
                    //this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                columns: [
                    { field: "Id", title: "", width: "1px" },
                    { command: [{ name: "destroy", className: "command-destroy" }] },
                    { field: "Code", title: "Code", width: "190px" },
                    { field: "IsActive", title: "Active", width: "50px" },
                    { field: "Url", title: "URL" },
                    { field: "Description", width: "230px" }
                ],
                editable: true

            });


            function detailInit(e) {
                $("<div/>").appendTo(e.detailCell).kendoGrid({
                    dataSource: {
                        transport: {
                            read: {
                                url: SY.XGET_WAPI_CATALOGS_URL + "?CatalogOperation=COMPANIES_ASSOCIATED_WITH_SERVICE&IdParent=" + e.data.Id,
                                dataType: "json"
                            },
                            update: {
                                url: SY.XPOST_WAPI_CATALOGS_URL,
                                type: "POST",
                                dataType: "json"
                            },

                            parameterMap: function (data, action) {
                                if (action !== "read") {
                                    var dat = { "IdParent": e.data.Id, "CatalogOperation": "COMPANY_ALLOWSERVICE_RELATION", "Changes": data.models };
                                    return kendo.stringify(dat);

                                }

                                return data;
                            },

                        },


                        error: ex => {
                            MasterPage.SHOW_ERROR_WINDOW("An Error happen when accessing server: " + ex.status);
                        },

                        requestEnd: function(ex) {
                             viewModel.MessageOkBoxIIS(ex);
                        },
                        batch: true,
                        pageSize: 10,

                        schema: {
                            model: {
                                id: "Id",
                                fields: {
                                    Code: { editable: false, nullable: false },
                                    IsActive: { editable: false },
                                    Authorized: { type: "boolean" },
                                    Description: { editable: false }
                                }
                            }
                        }
                    },
                    toolbar: ["save", "cancel"],

                    scrollable: false,
                    sortable: true,
                    pageable: true,
                    columns: [

                        { field: "Id", title: "", width: "1px" },
                        { field: "Code", title: "Code Company", width: "170px" },
                        { field: "IsActive", title: "Active", width: "100px" },
                        { field: "Authorized", title: "Allowed", width: "100px" },
                        { field: "Description", title: "Description" }
                    ],
                    editable: true
                });
            }
            //#endregion

            // This bind the view model with all.-------------------------------------------------------------------------
            kendo.init($("#WapiConfigContainer"));
            kendo.bind($("#WapiConfigContainer"), this.ViewModel);
            //    //#endregion


        }
    }

    //This is in WapiConfiguration.aspx page in document ready.....

    //$(document).ready(() => {



    //});
}