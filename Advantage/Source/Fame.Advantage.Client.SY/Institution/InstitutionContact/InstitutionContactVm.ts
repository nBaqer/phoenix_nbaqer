﻿/// <reference path="../../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="InstitutionContactDb.ts"/>
module SystemInstitution {
    export class InstitutionContactVm {
        filter: IInstitutionContactModel;
        gridId: string;
        db: InstitutionContactDb;
        templateId: string;
        dataSource: any;
        grid: any;
        addLinkId: string;
        isNewRow: boolean;
        popup: kendo.ui.Window;
        formValidator: kendo.ui.Validator;
        form: any;
        catalogDb: SY.CatalogDb;
        modelEditing: IInstitutionContactModel;
        messageBox: SY.MessageBox;

        constructor(filter: IInstitutionContactModel, gridId: string, templateId: string, addLinkId: string) {
            this.filter = filter;
            this.gridId = gridId;
            this.templateId = templateId;
            this.db = new InstitutionContactDb();
            this.catalogDb = new SY.CatalogDb();
            this.messageBox = new SY.MessageBox();
            this.modelEditing = {} as IInstitutionContactModel;
            if (addLinkId.substr(0, 1) !== "#") {
                this.addLinkId = "#" + addLinkId;
            }
        }

        public dataBind(that: InstitutionContactVm) {
            that.reBindGrid(that);
        }
        bindGrid(that: InstitutionContactVm) {
            if (that.grid === undefined || that.grid === null) {
                let gridOptions = {
                    dataSource: that.db.findData(that.filter),
                    sortable: false,
                    selectable: false,
                    scrollable: true,
                    pageable: true,
                    resizable: true,
                    width: 500,
                    cancel: (e: any) => {
                        that.isNewRow = false;
                        that.messageBox.showPageLoading(false);
                    },
                    columns: this.getGridColumns(that)
                } as kendo.ui.GridOptions;

                gridOptions.editable = {
                    mode: "popup",
                    template: kendo.template($(this.templateId).html())
                };
                gridOptions.edit = (e: any) => {
                    that.onGridEditingMode(e, that);
                };

                $(that.gridId).kendoGrid(gridOptions);

                that.grid = $(this.gridId).data("kendoGrid") as kendo.ui.Grid;
                that.grid.refresh();



                $(that.addLinkId).unbind("click");
                $(that.addLinkId)
                    .bind("click",
                    (e: any) => {
                        that.isNewRow = true;
                        that.grid.addRow();
                    });

            }

        }

        getGridColumns(that: InstitutionContactVm): Array<kendo.ui.GridColumn> {
            let columns = [
                { field: "Id", width: "0", hidden: true },
                { field: "Title", title: "Title", width: "150px" },
                {
                    field: "FirstName",
                    title: "Name ",
                    width: "200px",
                    template:
                    '#if (data.Prefix !== undefined ){ # <span>#=data.Prefix# </span> #}# #if (data.FirstName !== undefined ){ # <span>#=data.FirstName# </span> #}# #if (data.LastName !== undefined ){ # <span>#=data.LastName#</span> #}#'
                },
                { field: "Phone", title: "Phone ", width: "100px" },
                { field: "Email", title: "Email ", width: "150px" },
                { command: [{ name: "edit", text: " " }], title: "Edit", width: "50px" },
                {
                    command: [
                        {
                            name: "remove",
                            click: (e) => {
                                this.onGridDeleting(e, that);
                            },
                            text: "X"
                        }
                    ],
                    title: "",
                    width: "50px"
                }
            ] as Array<kendo.ui.GridColumn>;

            return columns;
        }

        onGridEditingMode(e: any, that: InstitutionContactVm) {
            e.preventDefault();
            that.popup = e.container.data("kendoWindow");
            that.form = e.container;
            that.formValidator = SY.ADVANTAGE_VALIDATOR("institutionContactFormValidator");

            console.log(e.model);

            let btoSave = e.container.find(".k-grid-update");
            btoSave.unbind("click");
            btoSave.bind("click", (e) => {
                that.onGridSaving(e, that);
            });

            if (that.popup !== undefined) {
                that.popup.setOptions({
                    title: "Institution Contact",
                    width: 500,
                    actions: ["close"],
                    resizable: false,
                    draggable: true,
                    modal: true
                });
                that.popup.center().open();
            } else {
                that.popup.center().open();
            }

            that.form.find("#ddlSuffix")
                .kendoDropDownList({
                    dataSource: that.catalogDb.getData("Suffix", that.filter.CampusId, "", this),
                    dataTextField: "Description",
                    dataValueField: "ID",
                    optionLabel: "Select"
                });

            that.form.find("#ddlPrefix")
                .kendoDropDownList({
                    dataSource: that.catalogDb.getData("Prefix", that.filter.CampusId, "", this),
                    dataTextField: "Description",
                    dataValueField: "ID",
                    optionLabel: "Select"
                });

            that.form.find("#txtContactPhone")
                .kendoMaskedTextBox({
                    mask: "(000)-000-0000"
                });

            if (!that.isNewRow) {
                that.modelEditing = e.model;
                that.form.find("#txtTitle").val(that.modelEditing.Title);
                that.form.find("#ddlPrefix").data("kendoDropDownList").value(that.modelEditing.PrefixId);
                that.form.find("#ddlSuffix").data("kendoDropDownList").value(that.modelEditing.SuffixId);
                that.form.find("#txtFirstName").val(that.modelEditing.FirstName);
                that.form.find("#txtLastName").val(that.modelEditing.LastName);
                that.form.find("#txtMiddleName").val(that.modelEditing.MiddleName);
                that.form.find("#txtContactPhone").data("kendoMaskedTextBox").value(that.modelEditing.Phone);
                that.form.find("#txtContactEmail").val(that.modelEditing.Email);
            }
        }

        onGridDeleting(e: any, that: InstitutionContactVm) {
            e.preventDefault();
            let row = $(e.target).closest("tr");
            let data = that.grid.dataItem(row) as any;
            if (data !== undefined && data !== null) {
                $.when(that.messageBox
                    .SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure you want to delete this record?"))
                    .then(confirmed => {
                        if (confirmed) {
                            //call the Delete here
                            that.messageBox.showPageLoading(true);
                            let input = { Id: data.Id, CampusId: that.filter.CampusId, InstitutionId: that.filter.InstitutionId, UserId: that.filter.UserId } as IInstitutionContactModel;
                            that.db.deleteData(input).done((msg) => {
                                if (msg !== undefined && msg !== null) {
                                    if (msg.ResponseCode !== 200) {
                                        e.preventDefault();
                                        that.messageBox.showPageLoading(false);
                                        if (msg.ResponseCode === 417) {
                                            that.messageBox.SHOW_WARNING_WINDOW_PROMISE(msg.ResponseMessage);
                                        } else {
                                            that.messageBox.SHOW_ERROR_WINDOW_PROMISE(msg.ResponseMessage);
                                        }
                                    } else {
                                        that.messageBox.showPageLoading(false);
                                        that.reBindGrid(that);
                                    }
                                } else {
                                    that.messageBox.showPageLoading(false);
                                    that.reBindGrid(that);
                                }
                            }).fail((msg) => {
                                //Show Error Here
                                e.preventDefault();
                                that.messageBox.showPageLoading(false);
                                that.messageBox.SHOW_DATA_SOURCE_ERROR(msg);
                            });
                        }
                    });
            }
        }

        onGridSaving(e: any, that: InstitutionContactVm) {
            that.messageBox.showPageLoading(true);
            if (that.formValidator.validate()) {
                let model = {} as IInstitutionContactModel;
                model.CampusId = that.filter.CampusId;
                model.InstitutionId = that.filter.InstitutionId;
                model.UserId = that.filter.UserId;
                model.Title = that.form.find("#txtTitle").val();
                model.FirstName = that.form.find("#txtFirstName").val();
                model.LastName = that.form.find("#txtLastName").val();
                model.MiddleName = that.form.find("#txtMiddleName").val();
                model.Email = that.form.find("#txtContactEmail").val();
                model.PrefixId = that.form.find("#ddlPrefix").data("kendoDropDownList").value();
                model.SuffixId = that.form.find("#ddlSuffix").data("kendoDropDownList").value();
                model.Phone = that.form.find("#txtContactPhone").data("kendoMaskedTextBox").value();

                if (that.isNewRow) {
                    that.db.postData(model).done((msg) => {
                        if (msg !== undefined && msg !== null) {
                            if (msg.ResponseCode !== 200) {
                                e.preventDefault();
                                that.messageBox.showPageLoading(false);
                                if (msg.ResponseCode === 417) {
                                    that.messageBox.SHOW_WARNING_WINDOW_PROMISE(msg.ResponseMessage);
                                } else {
                                    that.messageBox.SHOW_ERROR_WINDOW_PROMISE(msg.ResponseMessage);
                                }
                            } else {
                                that.popup.close();
                                that.messageBox.showPageLoading(false);
                                that.reBindGrid(that);
                            }
                        } else {
                            that.popup.close();
                            that.messageBox.showPageLoading(false);
                            that.reBindGrid(that);
                        }
                    }).fail((msg) => {
                        //Show Fail Message
                        e.preventDefault();
                        that.messageBox.showPageLoading(false);
                        that.messageBox.SHOW_DATA_SOURCE_ERROR(msg);
                    });
                } else {
                    model.Id = that.modelEditing.Id;

                    that.db.putData(model).done((msg) => {
                        if (msg !== undefined && msg !== null) {
                            if (msg.ResponseCode !== 200) {
                                e.preventDefault();
                                that.messageBox.showPageLoading(false);
                                if (msg.ResponseCode === 417) {
                                    that.messageBox.SHOW_WARNING_WINDOW_PROMISE(msg.ResponseMessage);
                                } else {
                                    that.messageBox.SHOW_ERROR_WINDOW_PROMISE(msg.ResponseMessage);
                                }
                            } else {
                                that.popup.close();
                                that.messageBox.showPageLoading(false);
                                that.reBindGrid(that);
                            }
                        } else {
                            that.popup.close();
                            that.messageBox.showPageLoading(false);
                            that.reBindGrid(that);
                        }
                    }).fail((msg) => {
                        //Show Fail Message
                        e.preventDefault();
                        that.messageBox.showPageLoading(false);
                        that.messageBox.SHOW_DATA_SOURCE_ERROR(msg);
                    });
                }
            } else {
                that.messageBox.showPageLoading(false);
            }
        }

        reBindGrid(that: InstitutionContactVm) {
            if (that.grid !== undefined && that.grid !== null) {
                that.grid.destroy();
                that.grid = undefined;
                $(that.grid).children().remove();

            }

            if (that.popup !== undefined && that.popup !== null) {
                that.popup.destroy();
            }

            if (that.form !== undefined && that.form !== null) {

                that.form.find(that.templateId + "Validator").remove();
            }

            //that.rawDataSource = undefined;
            that.dataSource = undefined;
            that.popup = undefined;
            that.form = undefined;
            that.formValidator = undefined;
            that.isNewRow = false;
            that.bindGrid(that);
        }
    }
}
