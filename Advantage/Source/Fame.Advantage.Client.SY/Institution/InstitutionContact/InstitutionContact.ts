﻿/// <reference path="../InstitutionContact/InstitutionContactVm.ts" />
module SystemInstitution {

    export class InstitutionContact {
        filter: IInstitutionContactModel;
        public viewModel: InstitutionContactVm;

        constructor(filter: IInstitutionContactModel) {
            this.filter = filter;
        }

        public initialize(gridId: string, template: string, addLink: string) {
            this.viewModel = new InstitutionContactVm(this.filter, gridId, template, addLink);
            this.viewModel.dataBind(this.viewModel);
        }

    }
}