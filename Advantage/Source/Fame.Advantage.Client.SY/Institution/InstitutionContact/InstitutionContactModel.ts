﻿module SystemInstitution {
    export interface IInstitutionContactModel {
        Id: string;
        CampusId: string;
        InstitutionId: string;
        InstitutionTypeId: number;
        FirstName: string;
        LastName: string;
        MiddleName: string;
        StatusId: string;
        Status: string;
        PrefixId: string;
        Prefix: string;
        SuffixId: string;
        Suffix: string;
        Title: string;
        Phone: string;
        PhoneExtension: boolean;
        Email: string;
        IsDefault: string;
        UserId: string;
        IsForeignPhone: boolean;
    }
}