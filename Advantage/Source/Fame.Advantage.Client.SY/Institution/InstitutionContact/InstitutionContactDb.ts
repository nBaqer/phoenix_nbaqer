﻿module SystemInstitution {
    import XPOST = SY.XPOST_INSTITUTION_CONTACT;
    import XPUT = SY.XPUT_INSTITUTION_CONTACT;
    import XDELETE = SY.XDELETE_INSTITUTION_CONTACT;
    import XFIND = SY.XFIND_INSTITUTION_CONTACT;

    export class InstitutionContactDb {

        public findData(input: IInstitutionContactModel): kendo.data.DataSource {
            return new kendo.data.DataSource({
                pageSize: 5,
                transport: {
                    read: {
                        url(options) {
                            var uri = XFIND;
                            delete options.baseUrl;
                            return uri;
                        },
                        dataType: "json",
                        timeout: 20000,
                        type: "GET",
                        data: input
                    }
                }
            });
        }

        public postData(input: IInstitutionContactModel) {
            return $.ajax({
                data: JSON.stringify(input),
                url: XPOST,
                type: 'POST',
                timeout: 20000,
                dataType: 'json'
            });
        }
        public putData(input: IInstitutionContactModel) {
            return $.ajax({
                data: JSON.stringify(input),
                url: XPUT,
                type: 'PUT',
                timeout: 20000,
                dataType: 'json'
            });
        }
        public deleteData(input: IInstitutionContactModel) {
            return $.ajax({
                data: JSON.stringify(input),
                url: XDELETE + "?" + $.param(input),
                type: 'DELETE',
                timeout: 20000,
                dataType: 'json'
            });
        }
    }
}