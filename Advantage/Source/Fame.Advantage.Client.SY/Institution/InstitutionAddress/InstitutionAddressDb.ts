﻿module SystemInstitution {
    import XPOST_INSTITUTION_ADDRESS = SY.XPOST_INSTITUTION_ADDRESS;
    import XPUT_INSTITUTION_ADDRESS = SY.XPUT_INSTITUTION_ADDRESS;
    import XDELETE_INSTITUTION_ADDRESS = SY.XDELETE_INSTITUTION_ADDRESS;
    import XFIND_INSTITUTION_ADDRESS = SY.XFIND_INSTITUTION_ADDRESS;

    export class InstitutionAddressDb {

        public findData(input: IInstitutionAddressModel): kendo.data.DataSource {
            return new kendo.data.DataSource({
                pageSize: 5,
                transport: {
                    read: {
                        url(options) {
                            var uri = XFIND_INSTITUTION_ADDRESS;
                            delete options.baseUrl;
                            return uri;
                        },
                        dataType: "json",
                        timeout: 20000,
                        type: "GET",
                        data: input
                    }
                }
            });
        }

        public postData(input: IInstitutionAddressModel) {
            return $.ajax({
                data: JSON.stringify(input),
                url: XPOST_INSTITUTION_ADDRESS,
                type: 'POST',
                timeout: 20000,
                dataType: 'json'
            });
        }
        public putData(input: IInstitutionAddressModel) {
            return $.ajax({
                data: JSON.stringify(input),
                url: XPUT_INSTITUTION_ADDRESS,
                type: 'PUT',
                timeout: 20000,
                dataType: 'json'
            });
        }
        public deleteData(input: IInstitutionAddressModel) {
            return $.ajax({
                data: JSON.stringify(input),
                url: XDELETE_INSTITUTION_ADDRESS + "?" + $.param(input),
                type: 'DELETE',
                timeout: 20000,
                dataType: 'json'
            });
        }
    }
}