﻿/// <reference path="../InstitutionAddress/InstitutionAddressVm.ts" />
module SystemInstitution {

    export class InstitutionAddress {
        filter: IInstitutionAddressModel;
        public viewModel: InstitutionAddressVm;

        constructor( filter: IInstitutionAddressModel) {
            this.filter = filter;
        }

        public initialize(gridId: string, template: string, addLink: string) {
            this.viewModel = new InstitutionAddressVm( this.filter, gridId, template, addLink);
            this.viewModel.dataBind(this.viewModel);
        }

    }
}