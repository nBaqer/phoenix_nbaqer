﻿/// <reference path="../../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="InstitutionAddressDb.ts"/>

module SystemInstitution {
    export class InstitutionAddressVm {
        filter: IInstitutionAddressModel;
        gridId: string;
        db: InstitutionAddressDb;
        templateId: string;
        dataSource: any;
        grid: any;
        addLinkId: string;
        isNewRow: boolean;
        popup: kendo.ui.Window;
        formValidator: kendo.ui.Validator;
        form: any;
        catalogDb: SY.CatalogDb;
        modelEditing: IInstitutionAddressModel;
        messageBox: SY.MessageBox;

        constructor(filter: IInstitutionAddressModel, gridId: string, templateId: string, addLinkId: string) {
            this.filter = filter;
            this.gridId = gridId;
            this.templateId = templateId;
            this.db = new InstitutionAddressDb();
            this.catalogDb = new SY.CatalogDb();
            this.messageBox = new SY.MessageBox();
            this.modelEditing = {} as IInstitutionAddressModel;
            if (addLinkId.substr(0, 1) !== "#") {
                this.addLinkId = "#" + addLinkId;
            }
        }

        public dataBind(that: InstitutionAddressVm) {
            that.reBindGrid(that);
        }

        bindGrid(that: InstitutionAddressVm) {
            if (that.grid === undefined || that.grid === null) {
                let gridOptions = {
                    dataSource: that.db.findData(that.filter),
                    sortable: false,
                    selectable: false,
                    scrollable: true,
                    pageable: true,
                    resizable: true,
                    width: 500,
                    cancel: (e: any) => {
                        that.isNewRow = false;
                        that.messageBox.showPageLoading(false);
                    },
                    columns: this.getGridColumns(that)
                } as kendo.ui.GridOptions;

                if (that.filter.InstitutionTypeId === 1) {
                    gridOptions.editable = {
                        mode: "popup",
                        template: kendo.template($(this.templateId).html())
                    };
                    gridOptions.edit = (e: any) => {
                        if (that.filter.InstitutionTypeId === 1) {
                            that.onGridEditingMode(e, that);
                        } else {
                            e.preventDefault();
                        }
                    };
                }


                $(that.gridId).kendoGrid(gridOptions);

                that.grid = $(this.gridId).data("kendoGrid") as kendo.ui.Grid;
                that.grid.refresh();
                if (that.filter.InstitutionTypeId === 1) {
                    $(that.addLinkId).removeClass("hidden");
                    $(that.addLinkId).unbind("click");
                    $(that.addLinkId)
                        .bind("click",
                        (e: any) => {
                            that.isNewRow = true;
                            that.grid.addRow();
                        });
                } else {
                    $(that.addLinkId).addClass("hidden");
                }
            }

        }

        getGridColumns(that: InstitutionAddressVm): Array<kendo.ui.GridColumn> {
            let columns = [
                { field: "Id", width: "0", hidden: true },
                { field: "Type", title: "Type", width: "80px" },
                { field: "Address1", title: "Address ", width: "150px", template: "#if (data.Address1 !== undefined && data.Address1 !== null){# #=data.Address1# #}# #if (data.Address2 !== undefined && data.Address2 !== null){#, #=data.Address2# #}#" },
                { field: "City", title: "City, State, Zip", width: "150px", template: "#if (data.City !== undefined && data.City !== null){# #=data.City# #}# #if (data.State !== undefined && data.State !== null){#, #=data.State# #}# #if (data.ZipCode !== undefined && data.ZipCode !== null){#, #=data.ZipCode# #}#" },
                { field: "Country", title: "Country", width: "100px" },
                { field: "IsInternational", title: "Is International", width: "70px", template: "# if (data.IsInternational === true) {# Yes #} else {# No #}#" }
            ] as Array<kendo.ui.GridColumn>;

            if (that.filter.InstitutionTypeId === 1) {
                columns.push({ command: [{ name: "edit", text: " " }], title: "Edit", width: "50px" });
                columns.push({
                    command: [
                        {
                            name: "remove",
                            click: (e) => {
                                this.onGridDeleting(e, that);
                            },
                            text: "X"
                        }
                    ],
                    title: "",
                    width: "50px"
                });

            } else {
                columns.push({
                    title: "",
                    width: "50px"
                });
            }

            return columns;
        }

        onGridEditingMode(e: any, that: InstitutionAddressVm) {
            e.preventDefault();
            that.popup = e.container.data("kendoWindow");
            that.form = e.container;
            that.formValidator = SY.ADVANTAGE_VALIDATOR("institutionAddressFormValidator");

            let btoSave = e.container.find(".k-grid-update");
            btoSave.unbind("click");
            btoSave.bind("click", (e) => {
                that.onGridSaving(e, that);
            });

            if (that.popup !== undefined) {
                that.popup.setOptions({
                    title: "Institution Address",
                    width: 500,
                    actions: ["close"],
                    resizable: false,
                    draggable: true,
                    modal: true
                });
                that.popup.center().open();
            } else {
                that.popup.center().open();
            }

            that.form.find("#ddlAddressType")
                .kendoDropDownList({
                    dataSource: that.catalogDb.getData("AddressType", that.filter.CampusId, "", this),
                    dataTextField: "Description",
                    dataValueField: "ID",
                    optionLabel: "Select"
                });

            if (!that.isNewRow) {
                that.modelEditing = e.model;
                that.onIsInternational(that.modelEditing.IsInternational, that);
            } else {
                that.onIsInternational(false, that);
            }

            that.form.find("#chkPhoneIsInternational").change((e) => {
                that.onIsInternational($(e.currentTarget).prop("checked"), that);
            });

            if (!that.isNewRow) {
                that.form.find("#ddlAddressType").data("kendoDropDownList").value(that.modelEditing.TypeId);
                that.form.find("#txtAddress").val(that.modelEditing.Address1);
                that.form.find("#txtCity").val(that.modelEditing.City);
                that.form.find("#chkPhoneIsInternational").prop("checked", that.modelEditing.IsInternational);
                that.form.find("#txtAddressZip").data("kendoMaskedTextBox").value(that.modelEditing.ZipCode);

                if (that.modelEditing.IsInternational) {
                    that.form.find("#txtAddressState").val(that.modelEditing.State);
                    that.form.find("#txtAddressCountry").val(that.modelEditing.Country);
                    that.form.find("#txtAddressCounty").val(that.modelEditing.County);
                }
            }
        }

        onIsInternational(isInternational: boolean, viewModel: InstitutionAddressVm) {
            let that = viewModel;
            var maskedtextbox = that.form.find("#txtAddressZip").data("kendoMaskedTextBox");
            if (maskedtextbox === undefined || maskedtextbox === null) {
                that.form.find("#txtAddressZip")
                    .kendoMaskedTextBox({
                        mask: "00000"
                    });
                maskedtextbox = that.form.find("#txtAddressZip").data("kendoMaskedTextBox");
            }

            if (isInternational === false) {

                that.form.find("#ddlAddressState")
                    .kendoDropDownList({
                        dataSource: that.catalogDb.getData("StatesWithCode", that.filter.CampusId, "", this),
                        dataTextField: "Description",
                        dataValueField: "ID",
                        optionLabel: "Select"
                    });
                that.form.find("#ddlAddressState").data("kendoDropDownList").wrapper.removeClass("hidden");

                that.form.find("#ddlAddressCountry")
                    .kendoDropDownList({
                        dataSource: that.catalogDb.getData("CountryWithCode", that.filter.CampusId, "", this),
                        dataTextField: "Description",
                        dataValueField: "ID",
                        optionLabel: "Select"
                    });

                that.form.find("#ddlAddressCountry").data("kendoDropDownList").wrapper.removeClass("hidden");

                that.form.find("#ddlAddressCounty")
                    .kendoDropDownList({
                        dataSource: that.catalogDb.getData("County", that.filter.CampusId, "", this),
                        dataTextField: "Description",
                        dataValueField: "ID",
                        optionLabel: "Select"
                    });

                that.form.find("#ddlAddressCounty").data("kendoDropDownList").wrapper.removeClass("hidden");
                
                that.form.find("#txtAddressState, #txtAddressCountry, #txtAddressCounty").addClass("hidden");

                that.form.find("#ddlAddressState").attr("required", "required");
                that.form.find("#ddlAddressCountry").attr("required", "required");

                that.form.find("#txtAddressState").removeAttr("required");
                that.form.find("#txtAddressCountry").removeAttr("required");

                if (maskedtextbox !== undefined && maskedtextbox !== null) {
                    maskedtextbox.setOptions({ mask: "00000" });
                }

                if (that.modelEditing !== undefined && that.modelEditing !== null) {
                    if (that.modelEditing.StateId !== undefined && that.modelEditing.StateId !== null) {
                        let stateId = that.modelEditing.StateId;
                        if (stateId !== undefined) {
                            that.form.find("#ddlAddressState")
                                .data("kendoDropDownList")
                                .value(stateId.toUpperCase());
                        }
                    }
                    if (that.modelEditing.CountryId !== undefined && that.modelEditing.CountryId !== null) {
                        that.form.find("#ddlAddressCountry").data("kendoDropDownList").value(that.modelEditing.CountryId.toUpperCase());
                    }
                    that.form.find("#ddlAddressCounty").data("kendoDropDownList").value(that.modelEditing.CountyId);
                }

            } else {
                if (maskedtextbox !== undefined && maskedtextbox !== null) {
                    maskedtextbox.setOptions({ mask: "aaaaaaaaaa" });
                }
                let state = that.form.find("#ddlAddressState").data("kendoDropDownList");

                if (state !== undefined && state !== undefined) {
                    state.wrapper.addClass("hidden");
                } else {
                    that.form.find("#ddlAddressState").addClass("hidden");
                }
                let country = that.form.find("#ddlAddressCountry").data("kendoDropDownList");

                if (country !== undefined && country !== undefined) {
                    country.wrapper.addClass("hidden");
                } else {
                    that.form.find("#ddlAddressCountry").addClass("hidden");
                }
                let county = that.form.find("#ddlAddressCounty").data("kendoDropDownList");

                if (county !== undefined && county !== undefined) {
                    county.wrapper.addClass("hidden");
                } else {
                    that.form.find("#ddlAddressCounty").addClass("hidden");
                }

                $("#txtAddressState, #txtAddressCountry, #txtAddressCounty").removeClass("hidden");

                that.form.find("#txtAddressState").attr("required", "required");
                that.form.find("#txtAddressCountry").attr("required", "required");

                that.form.find("#ddlAddressState").removeAttr("required");
                that.form.find("#ddlAddressCountry").removeAttr("required");
            }
        }

        onGridDeleting(e: any, that: InstitutionAddressVm) {
            e.preventDefault();
            let row = $(e.target).closest("tr");
            let data = that.grid.dataItem(row) as any;
            if (data !== undefined && data !== null) {
                $.when(that.messageBox
                    .SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure you want to delete this record?"))
                    .then(confirmed => {
                        if (confirmed) {
                            //call the Delete here
                            that.messageBox.showPageLoading(true);
                            let input = { Id: data.Id, CampusId: that.filter.CampusId, InstitutionId: that.filter.InstitutionId, UserId: that.filter.UserId } as IInstitutionAddressModel;
                            that.db.deleteData(input).done((msg) => {
                                if (msg !== undefined && msg !== null) {
                                    if (msg.ResponseCode !== 200) {
                                        e.preventDefault();
                                        that.messageBox.showPageLoading(false);
                                        if (msg.ResponseCode === 417) {
                                            that.messageBox.SHOW_WARNING_WINDOW_PROMISE(msg.ResponseMessage);
                                        } else {
                                            that.messageBox.SHOW_ERROR_WINDOW_PROMISE(msg.ResponseMessage);
                                        }
                                    } else {
                                        that.messageBox.showPageLoading(false);
                                        that.reBindGrid(that);
                                    }
                                } else {
                                    that.messageBox.showPageLoading(false);
                                    that.reBindGrid(that);
                                }
                            }).fail((msg) => {
                                //Show Error Here
                                e.preventDefault();
                                that.messageBox.showPageLoading(false);
                                that.messageBox.SHOW_DATA_SOURCE_ERROR(msg);
                            });
                        }
                    });
            }
        }

        onGridSaving(e: any, that: InstitutionAddressVm) {
            that.messageBox.showPageLoading(true);
            if (that.formValidator.validate()) {
                let model = {} as IInstitutionAddressModel;
                model.CampusId = that.filter.CampusId;
                model.InstitutionId = that.filter.InstitutionId;
                model.UserId = that.filter.UserId;
                model.Address1 = that.form.find("#txtAddress").val();
                model.TypeId = that.form.find("#ddlAddressType").data("kendoDropDownList").value();
                model.City = that.form.find("#txtCity").val();
                model.IsInternational = that.form.find("#chkPhoneIsInternational").prop("checked");
                model.ZipCode = that.form.find("#txtAddressZip").data("kendoMaskedTextBox").value();

                if (!model.IsInternational) {
                    model.StateId = that.form.find("#ddlAddressState").data("kendoDropDownList").value();
                    model.CountryId = that.form.find("#ddlAddressCountry").data("kendoDropDownList").value();
                    model.CountyId = that.form.find("#ddlAddressCounty").data("kendoDropDownList").value();
                } else {
                    model.State = that.form.find("#txtAddressState").val();
                    model.Country = that.form.find("#txtAddressCountry").val();
                    model.County = that.form.find("#txtAddressCounty").val();
                }

                if (that.isNewRow) {
                    that.db.postData(model).done((msg) => {
                        if (msg !== undefined && msg !== null) {
                            if (msg.ResponseCode !== 200) {
                                e.preventDefault();
                                that.messageBox.showPageLoading(false);
                                that.messageBox.SHOW_ERROR_WINDOW_PROMISE(msg.ResponseMessage);
                            } else {
                                that.popup.close();
                                that.messageBox.showPageLoading(false);
                                that.reBindGrid(that);
                            }
                        } else {
                            that.popup.close();
                            that.messageBox.showPageLoading(false);
                            that.reBindGrid(that);
                        }
                    }).fail((msg) => {
                        //Show Fail Message
                        e.preventDefault();
                        that.messageBox.showPageLoading(false);
                        that.messageBox.SHOW_DATA_SOURCE_ERROR(msg);
                    });
                } else {
                    model.Id = that.modelEditing.Id;

                    that.db.putData(model).done((msg) => {
                        if (msg !== undefined && msg !== null) {
                            if (msg.ResponseCode !== 200) {
                                e.preventDefault();
                                that.messageBox.showPageLoading(false);
                                if (msg.ResponseCode === 417) {
                                    that.messageBox.SHOW_WARNING_WINDOW_PROMISE(msg.ResponseMessage);
                                } else {
                                    that.messageBox.SHOW_ERROR_WINDOW_PROMISE(msg.ResponseMessage);
                                }
                            } else {
                                that.popup.close();
                                that.messageBox.showPageLoading(false);
                                that.reBindGrid(that);
                            }
                        } else {
                            that.popup.close();
                            that.messageBox.showPageLoading(false);
                            that.reBindGrid(that);
                        }
                    }).fail((msg) => {
                        //Show Fail Message
                        e.preventDefault();
                        that.messageBox.showPageLoading(false);
                        that.messageBox.SHOW_DATA_SOURCE_ERROR(msg);
                    });
                }
            } else {
                that.messageBox.showPageLoading(false);
            }
        }

        reBindGrid(that: InstitutionAddressVm) {
            if (that.grid !== undefined && that.grid !== null) {
                that.grid.destroy();
                that.grid = undefined;
                $(that.grid).children().remove();

            }
            if (that.popup !== undefined && that.popup !== null) {
                that.popup.destroy();
            }

            if (that.form !== undefined && that.form !== null) {

                that.form.find(that.templateId + "Validator").remove();
            }
            //that.rawDataSource = undefined;
            that.dataSource = undefined;
            that.popup = undefined;
            that.form = undefined;
            that.formValidator = undefined;
            that.isNewRow = false;
            that.bindGrid(that);
        }

    }
}
