﻿module SystemInstitution {
    export interface IInstitutionAddressModel {
        Id: string;
        CampusId: string;
        InstitutionId: string;
        InstitutionTypeId: number;
        TypeId: string;
        Type: string;
        Address1: string;
        Address2: string;
        City: string;
        StateId: string;
        ZipCode: string;
        CountryId: string;
        StatusId: string;
        IsMailingAddress: boolean;
        UserId: string;
        State: string;
        IsInternational: boolean;
        CountyId: string;
        Country: string;
        County: string;
    }
}