﻿/// <reference path="Models/InstitutionOutputModel.ts"/>
/// <reference path="../../Fame.Advantage.Client.SY/Institution/InstitutionVm.ts" />
module SystemInstitution {
    export class Institution {
        institution: IInstitutionOutputModel;
        filter: IInstitutionFilterInputModel;
        templateId: string;
        viewModel: InstitutionVm;


        constructor() {
            
        }

        public initialize(institution: IInstitutionOutputModel, filter: IInstitutionFilterInputModel, templateId: string) {
            this.institution = institution;
            this.filter = filter;
            this.templateId = this.getJqueryId(templateId);
            this.viewModel = new InstitutionVm(this.institution, filter);
            $(this.templateId).find(".institutionName span").html(this.institution.Name);
            $(this.templateId).removeClass("hidden");
        }

        public bindPhones(gridId: string, formTemplateId: string, addLink: string) {
            this.viewModel.bindPhonesGrid(this.getJqueryId(gridId), (this.getJqueryId(formTemplateId)), addLink);
        }

        public bindAddresses(gridId: string, formTemplateId: string, addLink: string) {
            this.viewModel.bindAddressesGrid(this.getJqueryId(gridId), (this.getJqueryId(formTemplateId)), addLink);
        }

        public bindContacts(gridId: string, formTemplateId: string, addLink: string) {
           this.viewModel.bindContactsGrid(this.getJqueryId(gridId), (this.getJqueryId(formTemplateId)), addLink);
        }
        
        getJqueryId(id: string) {
            if (id.substr(0, 1) !== "#") {
                id = "#" + id;
            }
            return id;
        }

    }
}