﻿/// <reference path="Models/InstitutionFilterInputModel.ts" />
/// <reference path="../AdvantageMessageBoxs.ts" />
module SystemInstitution {
    import XfindInstitution = SY.XFIND_INSTITUTION;
    import XgetInstitution = SY.XGET_INSTITUTION;
    import XexistInstitution = SY.XEXIST_INSTITUTION_BY_TYPE;
    import XexistInstitutionByName = SY.XEXIST_INSTITUTION_BY_NAME;
    export class InstitutionDb {
        //form: any;
        //constructor(form: any) {
        //    this.form = form;
        //}

        public getData(filter: IInstitutionFilterInputModel): JQueryXHR {
            return $.ajax({
                url: XgetInstitution + "?" + $.param(filter),
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }

        public findData(input: IInstitutionFilterInputModel): kendo.data.DataSource {
            let dataSource: kendo.data.DataSource;
            dataSource = new kendo.data.DataSource({
                pageSize: 10,
                serverFiltering: true,
                transport: {
                    read: {
                        url(options) {
                            var uri = XfindInstitution;
                            delete options.baseUrl;
                            return uri;
                        },
                        dataType: "json",
                        timeout: 20000,
                        type: "GET",
                        data: input
                    }
                }
            });

            dataSource.bind("error",
                (e) => {
                    let exception = JSON.parse(e.xhr.responseText);
                    let messageBox = new SY.MessageBox();
                    messageBox.SHOW_ERROR_WINDOW(exception.ExceptionMessage);
                });

            return dataSource;
        }

        public getExistByImportType(filter: IInstitutionFilterInputModel): JQueryXHR {
            return $.ajax({
                url: XexistInstitution + "?" + $.param(filter),
                type: 'GET',
                timeout: 20000,
                dataType: 'json'    
            });
        }

        public getExistByName(filter: IInstitutionFilterInputModel): JQueryXHR {
            return $.ajax({
                url: XexistInstitutionByName + "?" + $.param(filter),
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }
    }
}