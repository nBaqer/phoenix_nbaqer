﻿/// <reference path="../InstitutionAddress/InstitutionAddressModel.ts" />
/// <reference path="../InstitutionContact/InstitutionContactModel.ts" />
/// <reference path="../InstitutionPhone/InstitutionPhoneModel.ts" />
module SystemInstitution {
    export interface IInstitutionOutputModel {
        Id: string;
        CampusId: string;
        Name: string;
        TypeId: number;
        Addresses: Array<IInstitutionAddressModel>;
        Contacts: Array<IInstitutionContactModel>;
        Phones: Array<IInstitutionPhoneModel>;
        Comments: string;
    }
}