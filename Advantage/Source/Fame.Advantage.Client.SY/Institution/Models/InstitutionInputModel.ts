﻿module SystemInstitution {
    export interface IInstitutionInputModel {
        name: string;
        levelId: number;
        typeId: number;
        userId: string;
        campusId: string;
    }
}