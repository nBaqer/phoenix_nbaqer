﻿module SystemInstitution {
    export interface IInstitutionFilterInputModel {
        Id: string;
        Name: string;
        UserId: string;
        CampusId: string;
        Level: string; 
        InstitutionType: string;
    }
}