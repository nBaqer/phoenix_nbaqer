﻿/// <reference path="Models/InstitutionOutputModel.ts"/>
/// <reference path="../../Fame.Advantage.Client.SY/Institution/InstitutionPhone/InstitutionPhone.ts" />
/// <reference path="../../Fame.Advantage.Client.SY/Institution/InstitutionAddress/InstitutionAddress.ts" />
/// <reference path="../../Fame.Advantage.Client.SY/Institution/InstitutionContact/InstitutionContact.ts" />
module SystemInstitution {
    export class InstitutionVm {

        db: InstitutionDb;

        institutionAddress: InstitutionAddress;
        institutionContact: InstitutionContact;
        institutionPhone: InstitutionPhone;

        public constructor(institution: IInstitutionOutputModel, filter: IInstitutionFilterInputModel) {
            this.db = new InstitutionDb();
            let addressFilter = { CampusId: filter.CampusId, InstitutionId: institution.Id, UserId: filter.UserId, InstitutionTypeId: institution.TypeId } as IInstitutionAddressModel;
            this.institutionAddress = new InstitutionAddress(addressFilter);

            let phoneFilter = { CampusId: filter.CampusId, InstitutionId: institution.Id, UserId: filter.UserId, InstitutionTypeId: institution.TypeId } as IInstitutionPhoneModel;
            this.institutionPhone = new InstitutionPhone(phoneFilter);

            let contactFilter = { CampusId: filter.CampusId, InstitutionId: institution.Id, UserId: filter.UserId, InstitutionTypeId: institution.TypeId } as IInstitutionContactModel;
            this.institutionContact = new InstitutionContact(contactFilter);
        }

        public getData(filter: IInstitutionFilterInputModel): any {
            return this.db.getData(filter);
        }

        public bindPhonesGrid(gridId: string, formTemplateId: string, addLink: string) {
            this.institutionPhone.initialize(gridId, formTemplateId, addLink);
        }

        public bindAddressesGrid(gridId: string, formTemplateId: string, addLink: string) {
            this.institutionAddress.initialize(gridId, formTemplateId, addLink);
        }

        public bindContactsGrid(gridId: string, formTemplateId: string, addLink: string) {
            this.institutionContact.initialize(gridId, formTemplateId, addLink);
        }

    }
}