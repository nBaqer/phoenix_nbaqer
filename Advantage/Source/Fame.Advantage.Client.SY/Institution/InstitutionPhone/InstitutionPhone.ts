﻿/// <reference path="../InstitutionPhone/InstitutionPhoneModel.ts" />
/// <reference path="../InstitutionPhone/InstitutionPhoneVm.ts" />

module SystemInstitution {

    export class InstitutionPhone {
        filter: IInstitutionPhoneModel;
        public viewModel: InstitutionPhoneVm;

        constructor(filter: IInstitutionPhoneModel) {
            this.filter = filter;
        }

        public initialize(gridId: string, template: string, addLink: string) {
            this.viewModel = new InstitutionPhoneVm(this.filter, gridId, template, addLink);
            this.viewModel.dataBind(this.viewModel);
        }

    }
}