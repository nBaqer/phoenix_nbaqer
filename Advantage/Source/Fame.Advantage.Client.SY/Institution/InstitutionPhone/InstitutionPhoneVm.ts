﻿/// <reference path="InstitutionPhoneDb.ts"/>
/// <reference path="../../AdvantageValidation.ts"/>
/// <reference path="../../AdvantageMessageBoxs.ts"/>
module SystemInstitution {
    export class InstitutionPhoneVm {
        filter: IInstitutionPhoneModel;
        gridId: string;
        db: InstitutionPhoneDb;
        templateId: string;
        dataSource: any;
        grid: any;
        addLinkId: string;
        isNewRow: boolean;
        popup: kendo.ui.Window;
        formValidator: kendo.ui.Validator;
        form: any;
        catalogDb: SY.CatalogDb;
        modelEditing: any;
        messageBox: SY.MessageBox;

        constructor(filter: IInstitutionPhoneModel, gridId: string, templateId: string, addLinkId: string) {
            this.filter = filter;
            this.gridId = gridId;
            this.templateId = templateId;
            this.db = new InstitutionPhoneDb();
            this.catalogDb = new SY.CatalogDb();
            this.messageBox = new SY.MessageBox();
            this.modelEditing = {};
            if (addLinkId.substr(0, 1) !== "#") {
                this.addLinkId = "#" + addLinkId;
            }
        }

        public dataBind(that: InstitutionPhoneVm) {
            that.reBindGrid(that);
        }

        bindGrid(that: InstitutionPhoneVm) {
            if (that.grid === undefined || that.grid === null) {
                let gridOptions = {
                    dataSource: that.db.findData(that.filter),
                    sortable: false,
                    selectable: false,
                    scrollable: true,
                    pageable: true,
                    resizable: true,
                    cancel: (e: any) => {
                        that.isNewRow = false;
                        that.messageBox.showPageLoading(false);
                    },
                    columns: this.getGridColumns(that)
                } as kendo.ui.GridOptions;

                if (that.filter.InstitutionTypeId === 1) {
                    gridOptions.editable = {
                        mode: "popup",
                        template: kendo.template($(this.templateId).html())
                    };
                    gridOptions.edit = (e: any) => {
                        if (that.filter.InstitutionTypeId === 1) {
                            that.onGridEditingMode(e, that);
                        } else {
                            e.preventDefault();
                        }
                    };
                }

                $(that.gridId).kendoGrid(gridOptions);

                that.grid = $(this.gridId).data("kendoGrid") as kendo.ui.Grid;
                that.grid.refresh();

                if (that.filter.InstitutionTypeId === 1) {
                    $(that.addLinkId).removeClass("hidden");
                    $(that.addLinkId).unbind("click");
                    $(that.addLinkId)
                        .bind("click",
                        (e: any) => {
                            that.isNewRow = true;
                            that.grid.addRow();
                        });
                } else {
                    $(that.addLinkId).addClass("hidden");
                }
            }

        }

        getGridColumns(that: InstitutionPhoneVm): Array<kendo.ui.GridColumn> {
            let columns = [
                { field: "Id", width: "0", hidden: true },
                { field: "Type", title: "Type", width: "80px" },
                { field: "Phone", title: "Phone", width: "150px" }
            ] as Array<kendo.ui.GridColumn>;
            if (that.filter.InstitutionTypeId === 1) {
                columns.push({ command: [{ name: "edit", text: " " }], title: "Edit", width: "50px" });
                columns.push({
                    command: [
                        {
                            name: "remove",
                            click: (e) => {
                                this.onGridDeleting(e, that);
                            },
                            text: "X"
                        }
                    ],
                    title: "",
                    width: "50px"
                });
            } else {
                columns.push({
                    title: "",
                    width: "50px"
                });
            }

            return columns;
        }

        onGridEditingMode(e: any, that: InstitutionPhoneVm) {
            e.preventDefault();
            that.popup = e.container.data("kendoWindow");
            that.form = e.container;
            that.formValidator = SY.ADVANTAGE_VALIDATOR("institutionPhoneFormValidator");

            let btoSave = e.container.find(".k-grid-update");
            btoSave.unbind("click");
            btoSave.bind("click", (e) => {
                that.onGridSaving(e, that);
            });

            if (that.popup !== undefined) {
                that.popup.setOptions({
                    title: "Institution Phone",
                    width: 500,
                    height: 300,
                    actions: ["close"],
                    resizable: false,
                    draggable: true,
                    modal: true
                });
                that.popup.center().open();
            } else {
                that.popup.center().open();
            }

            that.form.find("#ddlPhoneType").kendoDropDownList({
                dataSource: that.catalogDb.getData("PhoneType", that.filter.CampusId, "", this),
                dataTextField: "Description",
                dataValueField: "ID",
                optionLabel: "Select"
            });

            that.form.find("#chkPhoneIsInternational").change((e) => {
                var maskedtextbox = that.form.find("#txtPhone").data("kendoMaskedTextBox");
                if ($(e.currentTarget).prop("checked")) {
                    // detach events
                    if (maskedtextbox != undefined) {
                        maskedtextbox.setOptions({ mask: "+9999999999999999999999999" });
                    }
                    that.form.find("#txtPhone").attr("data-role", "international");
                } else {
                    if (that.form.find("#txtPhone").data("kendoMaskedTextBox") === undefined) {
                        that.form.find("#txtPhone")
                            .kendoMaskedTextBox({
                                mask: "(000)-000-0000"
                            });
                    } else {
                        maskedtextbox.setOptions({ mask: "(000)-000-0000" });
                    }
                }
            });

            if (!that.isNewRow) {
                that.modelEditing = e.model;

                if (that.modelEditing.IsForeignPhone) {
                    that.form.find("#txtPhone")
                        .kendoMaskedTextBox({
                            mask: "+9999999999999999999999999"
                        });
                } else {
                    that.form.find("#txtPhone")
                        .kendoMaskedTextBox({
                            mask: "(000)-000-0000"
                        });
                }

                that.form.find("#txtPhone").data("kendoMaskedTextBox").value(that.modelEditing.Phone);
                that.form.find("#ddlPhoneType").data("kendoDropDownList").value(that.modelEditing.TypeId);
                that.form.find("#chkPhoneIsInternational").prop("checked", that.modelEditing.IsForeignPhone);

            } else {

                that.form.find("#txtPhone")
                    .kendoMaskedTextBox({
                        mask: "(000)-000-0000"
                    });
            }
        }

        onGridDeleting(e: any, that: InstitutionPhoneVm) {
            e.preventDefault();
            let row = $(e.target).closest("tr");
            let data = that.grid.dataItem(row) as any;
            if (data !== undefined && data !== null) {
                $.when(that.messageBox
                    .SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure you want to delete this record?"))
                    .then(confirmed => {
                        if (confirmed) {
                            //call the Delete here
                            that.messageBox.showPageLoading(true);
                            let input = { Id: data.Id, CampusId: that.filter.CampusId, InstitutionId: that.filter.InstitutionId, UserId: that.filter.UserId } as IInstitutionPhoneModel;
                            that.db.deleteData(input).done((msg) => {
                                if (msg !== undefined && msg !== null) {
                                    if (msg.ResponseCode !== 200) {
                                        e.preventDefault();
                                        that.messageBox.showPageLoading(false);
                                        if (msg.ResponseCode === 417) {
                                            that.messageBox.SHOW_WARNING_WINDOW_PROMISE(msg.ResponseMessage);
                                        } else {
                                            that.messageBox.SHOW_ERROR_WINDOW_PROMISE(msg.ResponseMessage);
                                        }
                                    } else {
                                        that.messageBox.showPageLoading(false);
                                        that.reBindGrid(that);
                                    }
                                } else {
                                    that.messageBox.showPageLoading(false);
                                    that.reBindGrid(that);
                                }
                            }).fail((msg) => {
                                //Show Error Here
                                e.preventDefault();
                                that.messageBox.showPageLoading(false);
                                that.messageBox.SHOW_DATA_SOURCE_ERROR(msg);
                            });
                        }
                    });
            }
        }

        onGridSaving(e: any, that: InstitutionPhoneVm) {
            that.messageBox.showPageLoading(true);
            if (that.formValidator.validate()) {
                let model = {} as IInstitutionPhoneModel;
                model.CampusId = that.filter.CampusId;
                model.InstitutionId = that.filter.InstitutionId;
                model.IsForeignPhone = that.form.find("#chkPhoneIsInternational").prop("checked");
                model.Phone = that.form.find("#txtPhone").data("kendoMaskedTextBox").value();
                model.UserId = that.filter.UserId;
                model.TypeId = that.form.find("#ddlPhoneType").data("kendoDropDownList").value();

                if (that.isNewRow) {
                    that.db.postData(model).done((msg) => {
                        if (msg !== undefined && msg !== null) {
                            if (msg.ResponseCode !== 200) {
                                e.preventDefault();
                                that.messageBox.showPageLoading(false);
                                if (msg.ResponseCode === 417) {
                                    that.messageBox.SHOW_WARNING_WINDOW_PROMISE(msg.ResponseMessage);
                                } else {
                                    that.messageBox.SHOW_ERROR_WINDOW_PROMISE(msg.ResponseMessage);
                                }
                            } else {
                                that.popup.close();
                                that.messageBox.showPageLoading(false);
                                that.reBindGrid(that);
                            }
                        } else {
                            that.popup.close();
                            that.messageBox.showPageLoading(false);
                            that.reBindGrid(that);
                        }
                    }).fail((msg) => {
                        //Show Fail Message
                        e.preventDefault();
                        that.messageBox.showPageLoading(false);
                        that.messageBox.SHOW_DATA_SOURCE_ERROR(msg);
                    });
                } else {
                    model.Id = that.modelEditing.Id;

                    that.db.putData(model).done((msg) => {
                        if (msg !== undefined && msg !== null) {
                            if (msg.ResponseCode !== 200) {
                                e.preventDefault();
                                that.messageBox.showPageLoading(false);
                                if (msg.ResponseCode === 417) {
                                    that.messageBox.SHOW_WARNING_WINDOW_PROMISE(msg.ResponseMessage);
                                } else {
                                    that.messageBox.SHOW_ERROR_WINDOW_PROMISE(msg.ResponseMessage);
                                }
                            } else {
                                that.popup.close();
                                that.messageBox.showPageLoading(false);
                                that.reBindGrid(that);
                            }
                        } else {
                            that.popup.close();
                            that.messageBox.showPageLoading(false);
                            that.reBindGrid(that);
                        }
                    }).fail((msg) => {
                        //Show Fail Message
                        e.preventDefault();
                        that.messageBox.showPageLoading(false);
                        that.messageBox.SHOW_DATA_SOURCE_ERROR(msg);
                    });
                }
            } else {
                that.messageBox.showPageLoading(false);
            }
        }

        reBindGrid(that: InstitutionPhoneVm) {
            if (that.grid !== undefined && that.grid !== null) {
                that.grid.destroy();
                that.grid = undefined;
                $(that.grid).children().remove();

            }
            if (that.popup !== undefined && that.popup !== null) {
                that.popup.destroy();
            }

            if (that.form !== undefined && that.form !== null) {

                that.form.find(that.templateId + "Validator").remove();
            }
            //that.rawDataSource = undefined;
            that.dataSource = undefined;
            that.popup = undefined;
            that.form = undefined;
            that.formValidator = undefined;
            that.isNewRow = false;
            that.bindGrid(that);
        }

    }
}
