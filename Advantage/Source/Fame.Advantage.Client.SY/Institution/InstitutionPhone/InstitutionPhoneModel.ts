﻿module SystemInstitution {
    export interface IInstitutionPhoneModel {
        Id: string;
        CampusId: string;
        InstitutionId: string;
        UserId: string;
        TypeId: string;
        StatusId: string;
        Phone: string;
        IsForeignPhone: string;
        InstitutionTypeId: number;
    }
}