﻿/// <reference path="../InstitutionPhone/InstitutionPhoneModel.ts"/>
/// <reference path="../../ServiceUrls.ts"/>
module SystemInstitution {
    import XfindInstitutionPhone = SY.XFIND_INSTITUTION_PHONE;
    import XpostInstitutionPhone = SY.XPOST_INSTITUTION_PHONE;
    import XputInstitutionPhone = SY.XPUT_INSTITUTION_PHONE;
    import XdeleteInstitutionPhone = SY.XDELETE_INSTITUTION_PHONE;

    export class InstitutionPhoneDb {

        public findData(input: IInstitutionPhoneModel): kendo.data.DataSource {
            return new kendo.data.DataSource({
                pageSize: 5,
                transport: {
                    read: {
                        url(options) {
                            var uri = XfindInstitutionPhone;
                            delete options.baseUrl;
                            return uri;
                        },
                        dataType: "json",
                        timeout: 20000,
                        type: "GET",
                        data: input
                    }
                }
            });
        }

        public postData(input: IInstitutionPhoneModel): JQueryXHR {
            return $.ajax({
                data: JSON.stringify(input),
                url: XpostInstitutionPhone,
                type: 'POST',
                timeout: 20000,
                dataType: 'json'
            });
        }

        public putData(input: IInstitutionPhoneModel): JQueryXHR {
            return $.ajax({
                data: JSON.stringify(input),
                url: XputInstitutionPhone,
                type: 'PUT',
                timeout: 20000,
                dataType: 'json'
            });
        }

        public deleteData(input: IInstitutionPhoneModel): JQueryXHR {
            return $.ajax({
                data: JSON.stringify(input),
                url: XdeleteInstitutionPhone + "?" + $.param(input),
                type: 'DELETE',
                timeout: 20000,
                dataType: 'json'
            });
        }
    }
}