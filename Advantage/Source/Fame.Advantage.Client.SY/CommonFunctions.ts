﻿module SY {


    declare var XMASTER_GET_BASE_URL: string;
    declare var $find: (input: string) => any;

    /*
     * Get a query string parameter
     */
    export function GET_QUERY_STRING_PARAMETER_BY_NAME(name: string) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }


    /*
     * Calculate the Age based in birth data as input
     * input: birth date
     */
    export function GET_AGE(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }


    /// entityId: the entity to populate the email receptor
    /// mod: the type of entity 4: Lead 
    /// only Lead as type of entity is programming!!!!
    export function OPEN_TASK_RAD_EMAIL_WINDOW_WITH_ENTITY(entityId, mod) {
        var oWnd: any = $find('radPop');
        oWnd.setSize(800, 780);
        oWnd.setUrl(XMASTER_GET_BASE_URL + '/defaultTMAddNew.html?addnew=true&entityId=' + entityId + "&mod=" + mod.toString());
        oWnd.show();
    }

    export function OPEN_TASK_RAD_TASK_WINDOW_WITH_ENTITY(entityId, mod) {
        var oWnd: any = $find('radPop');
        oWnd.setSize(800, 780);
        oWnd.setUrl(XMASTER_GET_BASE_URL + '/defaultTM.html?addnew=true&entityId=' + entityId + "&mod=" + mod.toString());
        oWnd.show();
    }

    export function OPEN_TASK_RAD_SCHEDULE_WINDOW_WITH_ENTITY(entityId, mod) {
        var oWnd: any = $find('radPop');
        oWnd.setSize(800, 780);
        oWnd.setUrl(XMASTER_GET_BASE_URL + '/defaultTMSchedule.html?addnew=true&entityId=' + entityId + "&mod=" + mod.toString());
        oWnd.show();
    }

    /* Show error*/
    export function SHOW_DATA_SOURCE_ERROR(e): string {
        try {
            if (e.xhr != undefined) {
                if (showSessionFinished(e.xhr.statusText)) { return ""; }
                let display: string = "";
                // Status 100 Continue (Informative Error)
                if (e.xhr.statusText !== "OK") {
                    display = "Error: " + e.xhr.statusText + ", ";
                }

                display += (e.xhr.responseText == undefined) ? e.xhr.responseXML : e.xhr.responseText;
                alert(display);
            } else {
                let display: string = "";
                // Status 100 Continue (Informative Error)
                if (e.statusText !== "OK") {
                    display = "Error: " + e.statusText + ", ";
                }
                display += (e.responseText == undefined) ? e.responseXML : e.responseText;
                alert(display);
            }
        } catch (ex) {
            alert("Server returned an undefined error");
        }
    }


    function showSessionFinished(statusText: string): boolean {
        if (statusText === "OK") {
            alert("Session expired");
            return true;
        }
        return false;
    }

    export function showPageLoading(show) {
        var pageLoading = $(document).find(".page-loading");
        if (pageLoading.length === 0)
            $(document).find("body").append('<div class="page-loading"></div>');
        kendo.ui.progress($(".page-loading"), show);
    }

    /* Return date in format MM/DD/YYYY */
    export function FORMAT_DATE_TO_MM_DD_YYYY( d:Date):string {
        var currDate = d.getDate().toString();
        if (currDate.length < 2) { currDate = "0" + currDate }
        var currMonth = (d.getMonth() + 1).toString();
        if (currMonth.length < 2) { currMonth = "0" + currMonth }
        var currYear = d.getFullYear();
        return (currMonth + "/" + currDate + "/" + currYear);
    }
  
}

$.fn.bindUp = function (type, fn) {

    type = type.split(/\s+/);

    this.each(function () {
        var len = type.length;
        while (len--) {
            $(this).bind(type[len], fn);

            var evt = $.data(this, 'events')[type[len]];
            evt.splice(0, 0, evt.pop());
        }
    });
};