﻿/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />


module Wapi {

    export class WapiVendorsVm extends kendo.Observable {

        public db: WapiVendorsDb;

        public VendorGridMasterDataSource: kendo.data.DataSource;
       
        public CampaignPayForDataSource: kendo.data.DataSource;

        constructor() {
            super();
            this.db = new WapiVendorsDb();
           // this.CampaignPayForDataSource = this.db.GetCampaignPayForDataSource();
        }

        public GetCampaignGridDataSource(vendorid:number) : kendo.data.DataSource  {

            var ds = this.db.GetCampaignGridDataSource(vendorid);
            return ds;
        }

        public GetVendorGridDataSource(): kendo.data.DataSource {

            var ds = this.db.GetVendorGridDataSource();
            return ds;
        }

        

    }
}