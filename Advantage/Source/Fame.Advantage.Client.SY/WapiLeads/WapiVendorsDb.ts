﻿/// <reference path="../ServiceUrls.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
module Wapi {
    export class WapiVendorsDb {

        //#region Common function to display errors  .
        public ShowDataSourceError(e) {
            try {
                if (e.xhr != undefined) {
                    if (e.xhr.responseText == undefined) {
                        alert("Error: " + e.xhr.statusText + ", " + e.xhr.responseXML);
                    } else {
                        alert("Error: " + e.xhr.statusText + ", " + e.xhr.responseText);
                    }
                } else {
                    if (e.responseText == undefined) {
                        alert("Error: " + e.statusText + ", " + e.responseXML);
                    } else {
                        alert("Error: " + e.statusText + ", " + e.responseText);
                    }
                }
            } catch (ex) {
                alert("Server returned an undefined error");
            }
        }

        DataSourceError(e) {
            alert(e.xhr.responseText);
        }
        //#endregion

        //#region Vendor Data-sources

        public GetVendorGridDataSource(): kendo.data.DataSource {
            var ds = new kendo.data.DataSource(
                {
                    transport: {
                        read: {
                            url: SY.XGET_WAPI_VENDORS_LIST_URL,
                            dataType: "json",
                            type: "GET"
                        },
                        update: {
                            url: SY.XPOST_WAPI_UPDATE_VENDOR_URL,
                            dataType: "json",
                            type: "POST"

                        },

                        parameterMap: function (data, action) {
                            var dat: any;
                            if (action == "update") {
                                dat = data;
                                dat.IsActive = (dat.IsActive) ? 1 : 0;
                                return kendo.stringify(dat);
                            }

                            return data;
                        },
                    },
                    pageSize: 10,
                    error: this.ShowDataSourceError,
                    serverPaging: false,
                    serverSorting: false,

                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                ID: { editable: false, visible: false },
                                VendorCode: { editable: false, visible: true },
                                VendorName: { editable: true, validation: { required: true } },
                                IsActive: { editable: true, type: "boolean" },
                                DateOperationBegin: { editable: true, type: "date" },
                                DateOperationEnd: { editable: true, type: "date" },
                                Description: { editable: true },
                            }
                        }
                    }

                });

            return ds;
        }

        //#endregion

        //#region Campaign Data-sources

        public GetCampaignGridDataSource(vendorid: number): kendo.data.DataSource {

            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: SY.XGET_WAPI_CAMPAIGN_LIST_URL + "?VendorId=" + vendorid,
                        dataType: "json",
                        type: "GET"
                    },

                    create: {
                        url: SY.XPOST_WAPI_INSERT_CAMPAIGN_URL,
                        dataType: "json",
                        type: "POST"

                    },
                    update: {
                        url: SY.XPOST_WAPI_INSERT_CAMPAIGN_URL,
                        dataType: "json",
                        type: "POST"

                    },
                    destroy: {
                        url: SY.XPOST_WAPI_DELETE_CAMPAIGN_URL,
                        dataType: "json",
                        type: "POST"
                    },
                    parameterMap: function (data, action) {
                        var dat: any;
                        if (action == "update") {
                            dat = data;
                            dat.VendorId = vendorid;
                            dat.IsActive = (dat.IsActive) ? 1 : 0;
                            dat.FilterRejectByCounty = (dat.FilterRejectByCounty) ? 1 : 0;
                            dat.FilterRejectDuplicates = (dat.FilterRejectDuplicates) ? 1 : 0;
                            return kendo.stringify(dat);
                        }
                        if (action == "create") {
                            dat = data;
                            dat.VendorId = vendorid;
                            dat.IsActive = (dat.IsActive) ? 1 : 0;
                            dat.FilterRejectByCounty = (dat.FilterRejectByCounty) ? 1 : 0;
                            dat.FilterRejectDuplicates = (dat.FilterRejectDuplicates) ? 1 : 0;
                            return kendo.stringify(dat);
                        }

                        if (action == "destroy") {
                            dat = data;
                            return kendo.stringify(dat.ID);
                        }


                        return data;
                    },
                },
                error: this.ShowDataSourceError,
                serverPaging: false,
                serverSorting: false,
                serverFiltering: false,

                pageSize: 10,
                //filter: { field: "ID", operator: "eq", value: e.data.ID },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            ID: { editable: false, visible: false },
                            VendorId: { editable: false, visible: false },
                            CampaignCode: { editable: true, validation: { required: true } },
                            AccountId: { editable: true, validation: { required: true } },
                            IsActive: { editable: true, type: "boolean" },
                            DateCampaignBegin: { editable: true, type: "date" },
                            DateCampaignEnd: { editable: true, type: "date" },
                            Cost: { editable: true, type: "number" },
                            PayFor: { editable: true, defaultValue: { ID: 1, Description: "Per Lead Enrolled" } },
                            FilterRejectByCounty: { editable: true, type: "boolean" },
                            FilterRejectDuplicates: { editable: true, type: "boolean" },

                        }
                    }
                }
            });

            return ds;

        }

        //#endregion
    }
} 