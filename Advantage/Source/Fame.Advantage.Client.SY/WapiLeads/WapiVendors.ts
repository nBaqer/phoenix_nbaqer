﻿module Wapi {


    export class WapiVendors {

        private viewModel: WapiVendorsVm;

        constructor() {

            this.viewModel = new Wapi.WapiVendorsVm();
            var vm = this.viewModel;

            var element = $("#VendorGrid").kendoGrid({
                dataSource: vm.GetVendorGridDataSource(),

                sortable: true,
                pageable: true,
                editable: "inline",
                detailInit: this.detailInit,
                dataBound: function () {
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                columns: [
                    { field: "ID", title: "", width: "1px" },
                    { command: ["edit"], title: "&nbsp", width: "170px" },
                    { field: "VendorName", title: "Vendor Name", width: "110px" },
                    { field: "VendorCode", title: "Vendor Code", width: "110px" },
                    { field: "IsActive", title: "Active", width:"80px" },
                    { field: "DateOperationBegin", title: "Date Begin", format: "{0:d}", width: "100px" },
                    { field: "DateOperationEnd", title: "Date End", format: "{0:d}", width: "100px"  },
                    { field: "Description", title: "Description" }
                ]
            } as any);

            // This bind the view model with all.-------------------------------------------------------------------------
            kendo.init($("#ContentVendor"));
            kendo.bind($("#ContentVendor"), vm);
        }



        detailInit(e) {

            var editordd = function (container, options) {
                $('<input required data-text-field="Description" data-value-field="ID" data-bind="value:' + options.field + '"/>')
                    .appendTo(container)
                    .kendoDropDownList({
                        autoBind: false,
                        dataSource: {
                            transport: {
                                read: {
                                    url: SY.XGET_WAPI_PAYFOR_LIST_URL,
                                    dataType: "json",
                                    type: "GET"
                                }
                            }
                        }
                    });
            };

            var vmod = new Wapi.WapiVendorsVm();

            $("<div/>").appendTo(e.detailCell).kendoGrid({

                dataSource: {
                    data: vmod.GetCampaignGridDataSource(e.data.ID),
                    batch: true
                },

                //dataSource: vmod.GetCampaignGridDataSource(e.data.ID),

                toolbar: ["create"],
                editable: "inline",
                //batch: true,
                scrollable: false,
                sortable: true,
                pageable: true,
                columns: [

                    { field: "ID", width: "1px" },
                    { field: "VendorId", width: "1px" },
                    //    { field: "VendorCode" },  
                    { command: ["edit", "destroy"], title: "&nbsp", width: "170px" },
                    { field: "CampaignCode", title: "Campaign Code" },
                    { field: "AccountId", title: "Account" },
                    { field: "IsActive", title: "Active", width: "80px" },
                    { field: "DateCampaignBegin", title: "Date Begin", format: "{0:d}", width: "100px" },
                    { field: "DateCampaignEnd", title: "Date End", format: "{0:d}", width: "100px" },
                    { field: "Cost", title: "Cost", width: "80px", format: "{0:c}" },
                    { field: "PayFor", width: "140px", title: "Cost Model", editor: function (c, o) { editordd(c, o); }, template: "#=PayFor.Description#" },
                    { field: "FilterRejectByCounty", title: "Filter County", width: "90px" },
                    { field: "FilterRejectDuplicates", title: "Filter Dup.", width: "80px" }

                ]
            } as any);

        }





    }
}