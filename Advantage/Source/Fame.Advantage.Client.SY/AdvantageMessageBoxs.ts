﻿module SY {
    export class MessageBox {
        /* Show error*/
        public SHOW_DATA_SOURCE_ERROR(e) {
            try {
                if (e.xhr != undefined) {
                    if (this.showSessionFinished(e.xhr.statusText)) { return ""; }
                    let display: string = "";
                    // Status 406 Continue (Informative Error Not Acceptable)

                    if (e.xhr.statusText !== "OK" && e.xhr.status !== 406) {
                        display = "Error: " + e.xhr.statusText + ", ";
                    }

                    display += (e.xhr.responseText == undefined) ? e.xhr.responseXML : e.xhr.responseText;
                    if (e.xhr.status === 406) {
                        this.SHOW_WARNING_WINDOW(display);
                    } else {
                        this. SHOW_ERROR_WINDOW(display);
                    }

                } else {
                    let display: string = "";
                    // Status 406 Continue (Informative Error Not Acceptable)
                    if (e.statusText !== "OK" && e.status !== 406) {
                        display = "Error: " + e.statusText + ", ";
                    }
                    display += (e.responseText == undefined) ? e.responseXML : e.responseText;
                    if (e.status === 406) {
                        this.SHOW_WARNING_WINDOW(display);
                    } else {
                        if ((e.readyState !== 0 && e.responseText !== "" && e.status !== 0 && e.statusText !== "error")) {
                            this.SHOW_ERROR_WINDOW(display);
                        }
                    }

                }
            } catch (ex) {
                this.SHOW_ERROR_WINDOW("Server returned an undefined error");
                //alert("Server returned an undefined error");
            }
            return "";
        };

        /**
         * Create a Confirmation Dialog Message that way for user selection
         * You are able to put your code in the yes or not case.
         * Use The Promise syntax to use it.
         * @param message
         */
        public SHOW_CONFIRMATION_WINDOW_PROMISE(message: any) {
            return this.showWindow('#confirmationTemplate', message);
        };

        /**
         * Create a Warning Windows that way for the confirmation
         * Use the promise syntax to use it
         * @param message
         */
        public SHOW_WARNING_WINDOW_PROMISE(message: any) {
            return this.showErrorWarningInfoWindow('#warningDialogTemplate', message);
        };

        /**
         * Create a Info Windows that way for the confirmation
         * Use the promise syntax to use it
         * You are able to put your code in the return of the function.
         * @param message
         */
        public SHOW_INFO_WINDOW_PROMISE(message: any) {
            return this.showErrorWarningInfoWindow('#infoDialogTemplate', message);
        };

        /**
         * Create a Info Windows that way for the confirmation
         * Use the promise syntax to use it
         * You are able to put your code in the return of the function.
         * @param message
         */
        public SHOW_ERROR_WINDOW_PROMISE(message: any) {
            return this.showErrorWarningInfoWindow('#errorDialogTemplate', message);
        };

        /**
         * Create a WARNING Windows that way for the confirmation
         * Use this if you do not need to put code after the dialog.
         * @param message
         */
        public SHOW_WARNING_WINDOW(message: any) {
            $.when(this.SHOW_WARNING_WINDOW_PROMISE(message))
                .then(confirmed => {
                    return true;
                });
        }

        /**
          * Create a Error Windows that way for the confirmation
          * Use this if you do not need to put code after the dialog.
          * @param message
          */
        public SHOW_ERROR_WINDOW(message: any) {
            $.when(this.SHOW_ERROR_WINDOW_PROMISE(message))
                .then(confirmed => {
                    return true;
                });
        }

        /**
         * Create a Info Windows that way for the confirmation
         * Use this if you do not need to put code after the dialog.
         * @param message
         */
        public SHOW_INFO_WINDOW(message: any) {
            $.when(this.SHOW_INFO_WINDOW_PROMISE(message))
                .then(confirmed => {
                    return true;
                });
        }

     showWindow(template, message) {
        var dfd = $.Deferred();
        var result = false;
        var win = $("<div id='popupWindow'></div>")
            .appendTo("body")
            .kendoWindow({
                width: 400,
                resizable: false,
                title: false,
                modal: true,
                visible: false,
                scrollable: false,
                close: function (e) {
                    this.destroy();
                    dfd.resolve(result);
                }
            })
            .data("kendoWindow") as kendo.ui.Window;

        win.content($(template).html()).center().open();
        $('.popupMessage').html(message);

        $("#popupWindow .confirm_yes").val('Yes');
        $("#popupWindow .confirm_no").val('No');

        $("#popupWindow .confirm_no").click(() => {
            win.close();
        });

        $("#popupWindow .confirm_yes").click(() => {
            result = true;
            win.close();
        });

        return dfd.promise();
    };

     showErrorWarningInfoWindow(template, message) {
        var dfd = $.Deferred();
        var result = false;
        var win = $("<div id='popupWindow'></div>")
            .appendTo("body")
            .kendoWindow({
                width: 400,
                resizable: false,
                title: false,
                modal: true,
                visible: false,
                scrollable: false,
                close: function (e) {
                    this.destroy();
                    dfd.resolve(result);
                }
            })
            .data("kendoWindow") as kendo.ui.Window;

        win.content($(template).html()).center().open();
        if (message.length > 200) {
            $("#wrapperMessage").css("overflow-y", "scroll");
        }
        $('.popupMessage').html(message);
        $("#popupWindow .confirm_yes").val('OK');
        $("#popupWindow .confirm_yes").click(() => {
            result = true;
            win.close();
        });

        return dfd.promise();
    };

     showSessionFinished(statusText: string): boolean {
        if (statusText === "OK") {
            alert("Session expired");
            return true;
        }
        return false;
    }

    public showPageLoading(show) {
        var pageLoading = $(document).find(".page-loading");
        if (pageLoading.length === 0)
            $(document).find("body").append('<div class="page-loading"></div>');
        kendo.ui.progress($(".page-loading"), show);
    }
}
}