﻿module SY_SMTP {

    export class SmtpOutputModel implements ISmtpOutputModel {
        // ReSharper disable InconsistentNaming
       
        /// <summary>
        /// The SMTP Settings values
        /// </summary>
        Settings: SmtpConfigurationSettings;  

        /// <summary>
        /// Used to configure the POST operations
        /// </summary>
        Filter: SmtpInputModel;

        /// <summary>
        /// Use to send down the files
        /// </summary>
        Files: any;

    }
}
