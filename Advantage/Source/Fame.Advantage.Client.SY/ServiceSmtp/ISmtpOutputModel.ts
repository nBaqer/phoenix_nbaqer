﻿module SY_SMTP {
    /// <summary>
    ///  DTO for SmtpController
    /// </summary>
    export interface ISmtpOutputModel {
        // ReSharper disable InconsistentNaming

        /// <summary>
        /// The SMTP Settings values
        /// </summary>
        Settings: SmtpConfigurationSettings; 

        /// <summary>
        /// Used to configure the POST operations
        /// </summary>
        Filter: SmtpInputModel;
        
        /// <summary>
        /// Use to send down the files
        /// </summary>
        Files:any;
    }
}

