﻿/// <reference path="../ServiceUrls.ts" />
module SY_SMTP {
    import XGET_SERVICE_LAYER_SMTP_GET = SY.XGET_SERVICE_LAYER_SMTP_GET;
    import XPOST_SERVICE_LAYER_SMTP_POST = SY.XPOST_SERVICE_LAYER_SMTP_POST;
    import XPOST_SERVICE_LAYER_SMTP_SEND = SY.XPOST_SERVICE_LAYER_SMTP_SEND;

    export class ServiceSmtpDb {

        public getConfigurationSettingValues(command: number, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_SMTP_GET + "?Command=" + command,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }

        /* Send the configuration to server */
        postSmtpConfigurationToServer(output: SmtpOutputModel, serviceSmtpBo: ServiceSmtpBo) {
            return $.ajax({
                url: XPOST_SERVICE_LAYER_SMTP_POST,
                type: 'POST',
                dataType: 'text',
                timeout: 20000,
                data: JSON.stringify(output)
            });

        }

        smtpSendMessage(smtpMessage: SmtpSendMessageInputModel, serviceSmtpBo: ServiceSmtpBo) {
            return $.ajax({
                url: XPOST_SERVICE_LAYER_SMTP_SEND,
                type: 'POST',
                dataType: 'json',
                timeout: 20000,
                data: JSON.stringify(smtpMessage)
            });


        }
    }
}