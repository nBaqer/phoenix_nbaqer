﻿
module SY_SMTP {
    import SHOW_DATA_SOURCE_ERROR = SY.SHOW_DATA_SOURCE_ERROR;
    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;
    declare var XMASTER_PAGE_USER_OPTIONS_USERID: string;


    export class ServiceSmtpBo {
        db: ServiceSmtpDb;

        constructor() {
            this.db = new ServiceSmtpDb();
        }

        /* Fill the interface with the configuration settings*/
        getConfigurationSettingValues() {
            this.db.getConfigurationSettingValues(1, this)
                .done(msg => {
                    if (msg != undefined) {
                        // Process the Required information resource and DLL.........................
                        var res1: ISmtpOutputModel = msg;
                        var settings = res1.Settings;
                        if (settings != null) {

                            $("body").css("cursor", "progress");
                            //Fill the interface values
                            $("#smtpEmail").val(settings.Username);
                            $("#smtpPassword").val(settings.Password);
                            $("#smtpServer").val(settings.Server);
                            $("#smtpFrom").val(settings.From);
                            var port = $("#smtpPort").data("kendoNumericTextBox");
                            port.value(settings.Port);
                            if (settings.IsSsl === 1) {
                                $("#smtpSslEnabled").prop("checked", true);
                            } else {
                                $("#smtpSslEnabled").prop("checked", false);
                            }

                            if (settings.IsOauth === 1) {
                                $("#smtpOauthEnabled").prop("checked", true);
                                $(".collapsible").css("display", "block");
                            } else {
                                $("#smtpOauthEnabled").prop("checked", false);
                                $(".collapsible").css("display", "none");
                            }
                            if (settings.AutClientSecret == null || settings.AutClientSecret === "") {
                                $("#smtpOauthFile").val("Configuration file not loaded...");
                            } else {
                                $("#smtpOauthFile").val(settings.AutClientSecret);
                            }
                        }
                        $("body").css("cursor", "default");

                    };
                })
                .fail(msg => {
                    $("body").css("cursor", "default");
                    SHOW_DATA_SOURCE_ERROR(msg);
                    return;
                });


        }

        /*Upload file to Server*/
        onUpload(e: kendo.ui.UploadUploadEvent) {

            var payload = new SmtpOutputModel();
            payload.Files = e.data;
            e.data = payload;
        };

        /* Upload configuration to Server*/
        saveConfigurationToServer(e: JQueryEventObject) {
            let payload = new SmtpOutputModel();
            payload.Filter = new SmtpInputModel();
            payload.Settings = new SmtpConfigurationSettings();
            payload.Filter.Command = 1;
            payload.Filter.UserId = XMASTER_PAGE_USER_OPTIONS_USERID;
            payload.Settings.IsOauth = $("#smtpOauthEnabled").prop("checked") ? 1 : 0;
            payload.Settings.IsSsl = $("#smtpSslEnabled").prop("checked") ? 1 : 0;
            payload.Settings.Password = $("#smtpPassword").val();
            payload.Settings.Port = $("#smtpPort").data("kendoNumericTextBox").value();
            payload.Settings.Server = $("#smtpServer").val();
            payload.Settings.Username = $("#smtpEmail").val();
            payload.Settings.From = $("#smtpFrom").val();

            $("body").css("cursor", "progress");
            var db = new ServiceSmtpDb();
            db.postSmtpConfigurationToServer(payload, this)
                .done(msg => {
                    if (msg != undefined) {
                        alert("parameters were successful saved");
                    }
                    $("body").css("cursor", "default");
                })
                .fail((msg => {
                    $("body").css("cursor", "default");
                    SHOW_DATA_SOURCE_ERROR(msg);
                }));

        }

        testEmailConfiguration(eventObject: JQueryEventObject) {
            var to = prompt("Please Enter a valid email address to send the test message", "example: yourEmail@hotmail.com");
            if (to == null || to === "") {
                return;
            }
            let payload = new SmtpSendMessageInputModel();
            payload.To = to;
            payload.Command = 1;
            var body = "<p style='background-color:blue; color:white'>This is a Test from Advantage</p>";
            payload.Body64 = btoa(body);
            payload.Subject = "Test Message";
            payload.From = $("#smtpFrom").val();
            payload.ContentType = "text/html";
            $("body").css("cursor", "progress");
            var db = new ServiceSmtpDb();
            db.smtpSendMessage(payload, this)
                .done(msg => {
                    if (msg != undefined) {
                        var res: IGenericOutput = msg;
                        alert("Message was successfully sent!");
                        //alert("Message " + res.Note + "... was Send!");
                    }
                    $("body").css("cursor", "default");
                })
                .fail((msg => {
                    $("body").css("cursor", "default");
                    SHOW_DATA_SOURCE_ERROR(msg);
                }));
        }

        /* 
         * Test if exists the file of configuration of OAuth in the server 
        */
        authorizeOauth(eventObject: JQueryEventObject) {
            
            // Ask if you are on the server.....
            let payload = new SmtpSendMessageInputModel();
            
            // Authorize Service
            payload.Command = 2;

            $("body").css("cursor", "progress");
            var db = new ServiceSmtpDb();
            db.smtpSendMessage(payload, this)
                .done(msg => {
                    if (msg != undefined) {
                        var res: IGenericOutput = msg;
                        if (res.IsPassed === 1) {
                            alert("Use of Service OAuth is Configured");
                        } else {
                            alert("Service OAuth not configured");
                        }
                       
                    }
                    $("body").css("cursor", "default");
                })
                .fail((msg => {
                    $("body").css("cursor", "default");
                    SHOW_DATA_SOURCE_ERROR(msg);
                }));
        }

        managePanels(e: JQueryEventObject) {
          
        }
    }
}