﻿module SY_SMTP {

    export class SmtpSendMessageInputModel {
        // ReSharper disable InconsistentNaming
        Body64: string;
        Subject: string;
        To: string;
        Cc: string;
        From: string;
        Command: number;
        /* use text/html for html */   
        ContentType: string;   
        // ReSharper restore InconsistentNaming
    }
}