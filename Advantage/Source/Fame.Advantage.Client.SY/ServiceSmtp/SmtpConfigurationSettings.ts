﻿module SY_SMTP {
    export class SmtpConfigurationSettings
    {
        // ReSharper disable InconsistentNaming

        /// <summary>
        /// The user password
        /// </summary>
          Password: string;

        /// <summary>
        /// The user-name (must be a email address)
        /// </summary>
          Username: string;

        /// <summary>
        /// The server to be used as SMTP server
        /// </summary>
          Server: string;

        /// <summary>
        /// The Port to be used
        /// </summary>
          Port: number;

        /// <summary>
        /// 1 If the connexion must be in SSL
        /// </summary>
         IsSsl: number;  

        /// <summary>
        /// 1 If the connexion used OAuth protocol
        /// </summary>
          IsOauth: number;

        /// <summary>
        /// A fraction of the client secret.
        /// For security reason no all is show.
        /// </summary>
          AutClientSecret: string; 

        ///<summary>
        /// Address used to give a returned address to destination
        ///</summary>
        From:string;
    }
}
