﻿module SY_SMTP {
    /// <summary>
    /// Input filter for SMTP
    /// </summary>
    export class SmtpInputModel {
        // ReSharper disable InconsistentNaming
        /// <summary>
        /// 1: Get all Configuration Setting for SMTP
        /// </summary>
        Command: number;

        /// <summary>
        /// User Id Guid.Use only in Post operation
        /// </summary>
        UserId: string;
    }
}
