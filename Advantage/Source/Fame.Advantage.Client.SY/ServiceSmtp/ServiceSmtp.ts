﻿module SY_SMTP {
    import XPOST_SERVICE_LAYER_SMTP_POSTFILE = SY.XPOST_SERVICE_LAYER_SMTP_POSTFILE;

    export class ServiceSmtp {

        public bo: ServiceSmtpBo;

        constructor() {
            this.bo = new ServiceSmtpBo();

            /* Create NumericTextBox from input HTML element */
            $("#smtpPort").kendoNumericTextBox(
                {
                    min: 1,
                    step: 1,
                    decimals: 0,
                    format: "n0"

                });
            
            /* Define Up-loader of Kendo */
            var files = [{ extension: ".json" }];
            $("#smtpOauthFiles").kendoUpload({
                multiple: false,
                async: {
                    saveUrl: XPOST_SERVICE_LAYER_SMTP_POSTFILE,
                    removeUrl: "remove",
                    autoUpload: false
                },
                files: files,
                select: e => {
                    $.each(e.files, (index, value) => {
                        if (value.extension !== ".json") {
                            e.preventDefault();
                            alert("Please upload json OAuth Configuration files");
                        }
                    });
                }//,
            });

            /* Add Event to button Save Configuration*/
            $("#smtpOauthSave").on("click", this.bo.saveConfigurationToServer);

            /* Add Event to button Test Configuration*/
            $("#smtpOauthTest").on("click", this.bo.testEmailConfiguration);

            /* Add event to button authorize OAuth*/
            $("#smtpOauthAuth").on("click", this.bo.authorizeOauth);

            /* Ad Event to checkbox */
            //$("#smtpOauthEnabled").on("checked", this.bo.managePanels);

            $("#smtpOauthEnabled").change(function() {
                if ($(this).is(":checked")) {
                    $(".collapsible").css("display", "block");
                } else {
                    $(".collapsible").css("display", "none");
                }

            });

           
           /* Load settings form Server*/
            this.bo.getConfigurationSettingValues();

        }

    }
}