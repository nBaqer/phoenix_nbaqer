﻿module SY_SMTP {
// ReSharper disable InconsistentNaming
/* Generic response from server interface*/
    export interface IGenericOutput {

        /// <summary>
        /// Usually value to indicate if the question
        /// asked to the server was true or false (1/0)
        /// </summary>
        IsPassed: number;

        /// <summary>
        /// A arbitrary comment about the operation
        /// </summary>
        Note: string;

    }

/* Generic response from server */
    export class GenericOutput implements IGenericOutput {
        IsPassed: number;
        Note: string;
    }

// ReSharper enable InconsistentNaming
}