﻿Namespace Info

    Public Class StatusChangeHistorySelectionValues
#Region "Variables"
        Private _studentEntrollmentId As String
#End Region
#Region "Properties"
        Public Property StudentEntrollmentId As String
            Get
                Return _studentEntrollmentId
            End Get
            Set(value As String)
                _studentEntrollmentId = value
            End Set
        End Property
#End Region
    End Class
End Namespace

