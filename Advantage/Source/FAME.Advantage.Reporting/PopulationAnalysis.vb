﻿Imports FAME.Advantage.Reporting.Info
Imports FAME.Parameters.Collections
Imports FAME.Parameters.Info

Namespace Logic
    Public Class PopulationAnalysis
        Dim CampGrpId As String = ""
        Dim PrgVerId As String = ""
        Dim StartDate As String = ""
        Dim EndDate As String = ""
        Dim IncludeLOA As Boolean = False
        Public Function BuildReport(ByVal Rpt As ReportInfo, ByVal conn As String, ByVal UserId As String, _
                                                                ByVal strGradingMethod As String, _
                                                                ByVal strGPAMethod As String, _
                                                                ByVal strSchoolName As String, _
                                                                ByVal strTrackAttendanceBy As String, _
                                                                ByVal strGradeBookWeightingLevel As String, _
                                                                 ByVal strStudentIdentifier As String, _
                                                                ByVal DisplayAttendanceUnitForProgressReportByClass As String, _
                                                                 ByVal strReportPath As String) As [Byte]()
            Try
                Dim objReportInfo As ReportInfo = Rpt
                Dim getReportOutput As [Byte]()
                For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                    Select Case tabview.ParamSet.SetType
                        Case "Filter"
                            For Each item As DictionaryEntry In Rpt.Filters
                                Dim Details As DetailDictionary = DirectCast(item.Value, DetailDictionary)
                                For Each d1 As DictionaryEntry In Details
                                    Dim objParamValue As ParamValueDictionary = DirectCast(d1.Value, ParamValueDictionary)
                                    For Each d2 As DictionaryEntry In objParamValue
                                        Dim ParameterModifier As String = d2.Key.ToString()
                                        Dim ModifierDictionary As ModDictionary = DirectCast(d2.Value, ModDictionary)
                                        Dim ModName As String = ModifierDictionary.Name
                                        For Each d3 As DictionaryEntry In ModifierDictionary
                                            Dim ParameterName As String = d3.Key.ToString
                                            Dim objValueList As ValueList = DirectCast(d3.Value, ValueList)
                                            Dim objValueType As String = objValueList.GetType.ToString
                                            If objValueType.Contains("[System.Guid]") Then
                                                Dim SelectedValues As ParamValueList(Of Guid) = DirectCast(objValueList, ParamValueList(Of Guid))
                                                Dim strSelectedFilterValues As String = ""
                                                '  strSelectedFilterValues = "'"  'Start a single quote
                                                For Each selection As Guid In SelectedValues
                                                    strSelectedFilterValues &= selection.ToString & ","
                                                Next
                                                If strSelectedFilterValues.Length >= 36 Then
                                                    strSelectedFilterValues = Mid(strSelectedFilterValues, 1, InStrRev(strSelectedFilterValues, ",") - 1)
                                                Else
                                                    strSelectedFilterValues = ""
                                                End If
                                                BuildParamtersForReport(objParamValue.Name.ToLower, strSelectedFilterValues, "")
                                            ElseIf objValueType.Contains("[System.DateTime]") Then
                                                Dim SelectedValues As ParamValueList(Of Date) = DirectCast(objValueList, ParamValueList(Of Date))
                                                For Each selection As Date In SelectedValues
                                                    Dim strModifier As String = ConvertModifierFromTextToSymbol(ParameterModifier)
                                                    Dim strDateValue As String = selection.ToShortDateString
                                                    BuildParamtersForReport(objParamValue.Name.ToLower, strDateValue, strModifier)
                                                Next
                                            Else
                                                Throw New Exception("Unknown Type")
                                            End If
                                        Next
                                    Next
                                Next
                            Next

                        Case "Sort"
                        Case "Custom"
                            For Each item As ParamItemUserSettingsInfo In Rpt.CustomOptions
                                'Response.Write(item.ControlSettingsCollection.Item)
                                For Each drillItem As ControlSettingInfo In item.ControlSettingsCollection

                                    For Each controlValueItem As ControlValueInfo In drillItem.ControlValueCollection
                                        Dim strKeyName As String = ""
                                        Dim strKeyValue As String = ""
                                        Dim strDisplayText As String = ""

                                        Select Case drillItem.ControlName.Trim.ToString.ToLower
                                            Case Is = "cblcustomoptions"
                                                strKeyName = "includeloa"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                                '''''''''''''''''''''''
                                        End Select
                                    Next
                                Next
                            Next
                        Case Else
                            Throw New Exception("Unknown Type")
                    End Select
                Next
                Dim getSelectedValues As New PopulationSelectionValues
                With getSelectedValues
                    .CampGrpId = CampGrpId.ToString
                    .ProgVerId = PrgVerId
                    .StartDate = StartDate
                    .EndDate = EndDate
                    .IncludeLOA = IncludeLOA
                    .ReportPath = objReportInfo.RDLName
                    .ResourceId = objReportInfo.ResourceId
                    .Environment = strReportPath
                End With
                Dim strFormat As String = ""
                Select Case Rpt.ExportFormat
                    Case ReportInfo.ExportType.PDF
                        strFormat = "Pdf"
                    Case ReportInfo.ExportType.Excel
                        strFormat = "Excel"
                    Case ReportInfo.ExportType.CSV
                        strFormat = "csv"
                End Select
                getReportOutput = CallRenderReport(strFormat, getSelectedValues, strSchoolName)
                Return getReportOutput
            Catch ex As Exception
                Throw ex
            End Try
            Return Nothing
        End Function
        Private Sub BuildParamtersForReport(ByVal strFilterFieldName As String, _
                                            ByVal strFilterSelectedValue As String, _
                                            ByVal strDisplayText As String)
            Select Case strFilterFieldName.ToLower.Trim
                Case "campgrpid"
                    CampGrpId = strFilterSelectedValue.ToString
                Case "progverid"
                    If strFilterSelectedValue.ToString = "" Then
                        PrgVerId = Nothing
                    Else
                        PrgVerId &= strFilterSelectedValue.ToString & ","
                    End If
                Case "daterange"
                    If StartDate = "" Then
                        StartDate = strFilterSelectedValue
                    Else
                        EndDate = strFilterSelectedValue
                    End If
                Case "includeloa"
                    If strFilterSelectedValue.ToLower = "true" Then
                        IncludeLOA = True
                    Else
                        IncludeLOA = False
                    End If
            End Select
        End Sub
        Public Function CallRenderReport(ByVal format As String, ByVal getFilterValues As PopulationSelectionValues, ByVal strSchoolName As String) As [Byte]()
            Dim getReportOutput As [Byte]()
            Dim generateReport As New PopulationReportGenerator
            getReportOutput = generateReport.RenderReport(format, getFilterValues, strSchoolName)
            Return getReportOutput
        End Function
        Private Function ConvertModifierFromTextToSymbol(ByVal Modifier As String) As String
            Dim strReturnSymbol As String = ""
            Select Case Modifier.Trim.ToLower
                Case "isequalto"
                    strReturnSymbol = "="
                Case "greaterthanorequalto"
                    strReturnSymbol = ">="
                Case "greaterthan"
                    strReturnSymbol = ">"
                Case "lessthanorequalto"
                    strReturnSymbol = "<="
                Case "lessthan"
                    strReturnSymbol = "<"
            End Select
            Return strReturnSymbol
        End Function
    End Class
End Namespace