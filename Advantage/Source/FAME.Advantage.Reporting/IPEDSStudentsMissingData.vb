﻿Imports FAME.Advantage.Reporting.Info
Imports FAME.Parameters.Info

Namespace Logic
    Public Class IPEDSStudentsMissingData
        Dim CampusId As String = ""
        Dim ProgId As String = ""
        Dim PrgGrpId As String = ""
        Dim CohortYear As String = ""
        Dim OfficialReportingDate As String = ""
        Dim SortFields As String = ""
        Dim SchoolType As String = ""
        Dim StartDate As String = ""
        Dim EndDate As String = ""
        Dim PriorToDate As String = ""
        Dim MissingDataSortId As Integer = 1
        Dim CampusName As String = ""
        Dim ReportingYear As String = ""
        Dim largeProgramId As String = ""
        Dim largeProgramName As String = ""
        Dim InstitutionType As String = ""
        Dim SchoolTypeList As String = ""
        Dim FullorFall As String = ""
        Dim CohortType As String=""
        Public Function BuildReport(ByVal Rpt As ReportInfo, ByVal conn As String, ByVal UserId As String, _
                                                                ByVal strGradingMethod As String, _
                                                                ByVal strGPAMethod As String, _
                                                                ByVal strSchoolName As String, _
                                                                ByVal strTrackAttendanceBy As String, _
                                                                ByVal strGradeBookWeightingLevel As String, _
                                                                ByVal strStudentIdentifier As String, _
                                                                ByVal DisplayAttendanceUnitForProgressReportByClass As String, _
                                                                ByVal strReportPath As String) As [Byte]()
            'Dim TestText As String = String.Empty
            Try
                Dim objReportInfo As ReportInfo = Rpt
                Dim getReportOutput As [Byte]()
                For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                    Select Case tabview.ParamSet.SetType
                        Case "Filter"
                        Case "Sort"
                        Case "Custom"
                            For Each item As ParamItemUserSettingsInfo In Rpt.CustomOptions
                                'Response.Write(item.ControlSettingsCollection.Item)
                                For Each drillItem As ControlSettingInfo In item.ControlSettingsCollection

                                    For Each controlValueItem As ControlValueInfo In drillItem.ControlValueCollection

                                        ' Protect again empty fields
                                        If drillItem.ControlName = String.Empty Then Continue For
                                        Dim strKeyName As String
                                        Dim strKeyValue As String
                                        Dim strDisplayText As String = ""

                                        Select Case drillItem.ControlName.Trim.ToString.ToLower
                                            Case Is = "radcombocampus"
                                                strKeyName = "campusid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radlistboxprogram2"
                                                strKeyName = "progid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radlistboxareasofinterest2"
                                                strKeyName = "prggrpid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radcomboboxcohortyear"
                                                strKeyName = "cohortyear"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radcomboboxreportingyear"
                                                strKeyName = "reportingperiod"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "rblschooltype"
                                                strKeyName = "schooltype"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "rblfullorfall"
                                                strKeyName = "fullorfall"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "rblschooltypelist"
                                                strKeyName = "schooltypelist"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radsubcohort"
                                                strKeyName = "subcohort"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "raddatereportdate"
                                                strKeyName = "officialreportingdate"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "raddatepriorto"
                                                strKeyName = "priortodate"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "rblsortby"
                                                strKeyName = "sortfields"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radcombolprogram"
                                                strKeyName = "largeprogram"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radcomboboxinstitutiontype"
                                                strKeyName = "institutiontype"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                        End Select
                                    Next
                                Next
                            Next
                        Case Else
                            Throw New Exception("Unknown Type")
                    End Select
                Next
                Dim getSelectedValues As New IPEDReportSelectionValue
                With getSelectedValues
                    .CampusId = CampusId.ToString
                    .CohortYear = CohortYear.ToString
                    If Not PrgGrpId.Trim = "" Then
                        If InStrRev(PrgGrpId, ",") >= 1 Then
                            PrgGrpId = Mid(PrgGrpId.ToString.Trim, 1, (InStrRev(PrgGrpId, ",") - 1))
                        End If
                        .AreasOfInterestId = PrgGrpId.ToString
                    Else
                        .AreasOfInterestId = PrgGrpId.ToString
                    End If
                    .StartDate = StartDate
                    .EndDate = EndDate
                    .OfficialReportingDate = OfficialReportingDate
                    .PriorToDate = PriorToDate
                    If Not ProgId.Trim = "" Then
                        If InStrRev(ProgId, ",") >= 1 Then
                            ProgId = Mid(ProgId.ToString.Trim, 1, (InStrRev(ProgId, ",") - 1))
                        End If
                        .ProgramId = ProgId
                    Else
                        .ProgramId = ProgId
                    End If
                    .SchoolType = SchoolType
                    .SortFields = SortFields
                    .MissingDataSortId = MissingDataSortId
                    .ReportPath = objReportInfo.RDLName
                    .StudentIdentifier = strStudentIdentifier
                    .CampusName = CampusName
                    .ResourceId = Rpt.ResourceId
                    .ReportingYear = ReportingYear
                    .LargeProgramId = largeProgramId
                    .LargeProgramName = largeProgramName
                    .InstitutionType = InstitutionType
                    .Environment = strReportPath
                    .FullorFall = FullorFall
                    .SchoolTypeList = SchoolTypeList
                    .CohortType = CohortType
                End With
                'intCheckDataExists = GetProgressDA.GetProgressReportRecordCount(strStuEnrollId, strCampGrpId, strPrgVerId, strStatusCodeId, strTermId, strStudentGrpId, dtStartString, UserId.ToString, strStartStringModifier)
                'If intCheckDataExists >= 1 Then
                Dim strFormat As String = ""
                Select Case Rpt.ExportFormat
                    Case ReportInfo.ExportType.PDF
                        strFormat = "Pdf"
                    Case ReportInfo.ExportType.Excel
                        strFormat = "xls"
                    Case ReportInfo.ExportType.CSV
                        strFormat = "csv"
                End Select
                getReportOutput = CallRenderReport(strFormat, getSelectedValues, strSchoolName)
                Return getReportOutput
                'Else
                '    Return Nothing
                'End If
                'Return Nothing
            Catch ex As Exception
                Throw ex
            End Try
            Return Nothing
        End Function
        Private Sub BuildParamtersForReport(ByVal strFilterFieldName As String, _
                                            ByVal strFilterSelectedValue As String, _
                                            ByVal strDisplayText As String)
            Select Case strFilterFieldName.ToLower.Trim
                Case "campusid"
                    CampusId = strFilterSelectedValue.ToString
                    CampusName = strDisplayText.ToString
                Case "progid"
                    ProgId &= strFilterSelectedValue.ToString & ","
                Case "prggrpid"
                    PrgGrpId &= strFilterSelectedValue.ToString & ","
                Case "cohortyear"
                    CohortYear = strFilterSelectedValue.ToString
                Case "sortfields"
                    SortFields = strFilterSelectedValue.ToString
                    Select Case SortFields.Trim.ToLower
                        Case "student number"
                            SortFields = "Student Number"
                        Case "enrollmentid"
                            SortFields = "EnrollmentId"
                        Case "lastname"
                            SortFields = "LastName"
                        Case Else
                            SortFields = "SSN"
                    End Select
                    If SortFields.ToString.Trim.ToLower = "lastname" Then
                        MissingDataSortId = 0
                    Else
                        MissingDataSortId = 1 'SSN or StudentNumber
                    End If
                Case "schooltype"
                    SchoolType = strFilterSelectedValue.ToString
                    If SchoolType.ToString.ToLower.Trim = "program" Then
                        StartDate = "08/01/" + CohortYear.ToString.Trim
                        EndDate = "10/31/" + CohortYear.ToString.Trim
                    ElseIf SchoolType.ToString.ToLower.Trim = "full" Then
                        Dim intCohortYear As Integer = CInt(CohortYear.ToString.Trim)
                        StartDate = "09/01/" + (intCohortYear).ToString
                        EndDate = "8/31/" + (intCohortYear + 1).ToString
                    Else
                        Dim intCohortYear As Integer = CInt(CohortYear.ToString.Trim)
                        StartDate = "10/15/" + (intCohortYear).ToString
                        EndDate = "10/15/" + (intCohortYear + 1).ToString
                    End If
                Case "subcohort"
                    CohortType = strFilterSelectedValue.ToString
                Case "fullorfall"
                    FullorFall = strFilterSelectedValue.ToString
                Case "schooltypelist"
                    SchoolTypeList = strFilterSelectedValue.ToString
                Case "officialreportingdate"
                    OfficialReportingDate = strFilterSelectedValue.ToString
                Case "priortodate"
                    PriorToDate = strFilterSelectedValue.ToString
                Case "reportingperiod"
                    If Not strFilterSelectedValue.ToString.Trim = "" Then ReportingYear = strFilterSelectedValue.Substring(0, 4)
                Case "largeprogram"
                    largeProgramId = strFilterSelectedValue.ToString
                    largeProgramName = strDisplayText.ToString
                Case "institutiontype"
                    InstitutionType = strFilterSelectedValue.ToString
            End Select
        End Sub
        Public Function CallRenderReport(ByVal format As String, ByVal getFilterValues As IPEDReportSelectionValue, ByVal strSchoolName As String) As [Byte]()
            Dim getReportOutput As [Byte]()
            Dim generateReport As New IPEDSReportGenerator
            getReportOutput = generateReport.RenderReport(format, getFilterValues, strSchoolName)
            Return getReportOutput
        End Function
    End Class
End Namespace

