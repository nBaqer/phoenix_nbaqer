﻿Imports FAME.Advantage.Reporting.ReportService2005
Imports FAME.Advantage.Reporting.ReportExecution2005
Imports System.Configuration
Imports System.Web
Imports FAME.Advantage.Reporting.Info
Imports FAME.Advantage.Common
Namespace Logic
    Public Class cDailyAbsenceByClassReportGenerator
#Region "Report Variables"
        Private _rptService As New ReportingService2005
        Private _rptExecutionService As New ReportExecution2005.ReportExecutionService

        Private _strReportName As String = Nothing
        Private _strHistoryId As String = Nothing
        Private _boolForRendering As Boolean = False
        Private _credentials As ReportService2005.DataSourceCredentials() = Nothing
        Private _rptParameterValues As ReportService2005.ParameterValue() = Nothing
        Private _rptParameters As ReportService2005.ReportParameter() = Nothing
        Private _rptExecutionServiceParameterValues As ReportExecution2005.ParameterValue() = Nothing
        Private _rptExecutionInfoObj As New ExecutionInfo

        'Variables needed to render report
        Private _historyId As String = Nothing
        Private _deviceInfo As String = Nothing
        Private _format As String = Nothing
        Private _generateReportAsBytes As [Byte]()
        Private _encoding As String = [String].Empty
        Private Shared _strExtension As String = String.Empty
        Private Shared _strMimeType As String = String.Empty
        Private _warnings As ReportExecution2005.Warning() = Nothing
        Private _streamIDs As String() = Nothing

        Dim strServerName As String = ""
        Dim strDBName As String = ""
        Dim strUserName As String = ""
        Dim strPassword As String = ""
#End Region

#Region "Initialize Web Services"
        Private Sub InitializeWebServices()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString).ToLower
            _rptService.Credentials = System.Net.CredentialCache.DefaultCredentials
            _rptService.Url = MyAdvAppSettings.AppSettings("ReportServices").ToString
            'Set the credentials and url for the report execution services
            _rptExecutionService.Credentials = System.Net.CredentialCache.DefaultCredentials
            '_rptExecutionService.SetExecutionCredentials(dsCredentials)
            _rptExecutionService.Url = MyAdvAppSettings.AppSettings("ReportExecutionServices").ToString
        End Sub
#End Region

#Region "Initialize Export Formats"
        Private Shared Sub ConfigureExportFormat(ByRef strType As String)
            Select Case strType.ToUpper()
                Case "PDF"
                    _strExtension = "pdf"
                    _strMimeType = "application/pdf"
                    Exit Select
                Case "EXCEL", "XLS"
                    _strExtension = "xls"
                    _strMimeType = "application/vnd.excel"
                    Exit Select
                Case "WORD"
                    _strExtension = "doc"
                    _strMimeType = "application/vnd.ms-word"
                Case "CSV"
                    _strExtension = "csv"
                    _strMimeType = "text/csv"
                Case Else
                    Throw New Exception("Unrecognized type: " & strType & ". Type must be PDF, Excel or Image, HTML.")
            End Select
        End Sub
#End Region

#Region "Build Report Parameters"
        Private Function BuildReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As DailyConsecutiveAbsenceByClassReportSelectionValues, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}
            Dim strNullValue As String = "Null"

            Dim strCampusId As String = ""
            Dim strCampGrpId As String = ""
            Dim strPrgVerId As String = ""
            Dim strEnrollmentStatus As String = ""
            Dim strAsOfDate As String = ""

            Dim strTermId As String = ""
            Dim strReqId As String = ""
            Dim intNumberofDays As Integer = 0
            Dim dtCutOffDate As DateTime
            Dim showCalendarDays As Boolean = False

            Dim strServerName As String = ""
            Dim strDBName As String = ""
            Dim strUserName As String = ""
            Dim strPassword As String = ""

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString).ToLower
            
            With getReportParametersObj
                strTermId = .TermId
                strReqId = .ReqId
                intNumberofDays = .DaysMissed
                dtCutOffDate = .CutOffDate
                strCampusId = .CampusId
                showCalendarDays = .ShowCalendarDays

                strServerName = GetServerName(strConnectionString)
                strDBName = GetDatabaseName(strConnectionString)
                strUserName = GetUserName(strConnectionString)
                strPassword = GetPassword(strConnectionString)
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then


                'TermId Parameter
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "TermId"
                rptExecutionParamValues(0).Name = "TermId"
                If Not strTermId.Trim = "" Then
                    rptExecutionParamValues(0).Value = strTermId.ToString
                Else
                    rptExecutionParamValues(0).Value = Nothing
                End If

                'ReqId Parameter
                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "ReqId"
                rptExecutionParamValues(1).Name = "ReqId"
                If Not strReqId.Trim = "" Then
                    rptExecutionParamValues(1).Value = strReqId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                'Days Missed
                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "ConsDays"
                rptExecutionParamValues(2).Name = "ConsDays"
                rptExecutionParamValues(2).Value = intNumberofDays.ToString

                'Cutoffdate
                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "CutoffDate"
                rptExecutionParamValues(3).Name = "CutoffDate"
                rptExecutionParamValues(3).Value = dtCutOffDate.ToString


                'Server Name
                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ServerName"
                rptExecutionParamValues(4).Name = "servername"
                rptExecutionParamValues(4).Value = strServerName

                'Database Name
                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "DBName"
                rptExecutionParamValues(5).Name = "databasename"
                rptExecutionParamValues(5).Value = strDBName


                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "UserName"
                rptExecutionParamValues(6).Name = "uid"
                rptExecutionParamValues(6).Value = strUserName

                'password
                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "Password"
                rptExecutionParamValues(7).Name = "password"
                rptExecutionParamValues(7).Value = strPassword

                'CampusId Parameter
                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "CampusId"
                rptExecutionParamValues(8).Name = "CampusId"
                If Not strCampusId.Trim = "" Then
                    rptExecutionParamValues(8).Value = strCampusId.ToString
                Else
                    rptExecutionParamValues(8).Value = Nothing
                End If


                'Show Calendar Days
                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "ShowCalendarDays"
                rptExecutionParamValues(9).Name = "ShowCalendarDays"
                rptExecutionParamValues(9).Value = showCalendarDays.ToString
            End If
            Return rptExecutionParamValues
        End Function
#End Region

#Region "Build Report"
        Public Function RenderReport(ByVal _format As String, _
                                                                    ByVal getReportParametersObj As DailyConsecutiveAbsenceByClassReportSelectionValues, _
                                                                    ByVal strSchoolName As String) As [Byte]()

            'Initialize the report services and report execution services
            InitializeWebServices()



            'Set the path of report
            _strReportName = getReportParametersObj.Environment + getReportParametersObj.ReportPath
            'Use the Report Services: GetReportParameters method to get a list of parameters for a specified report
            'This is the only location where the reportingservices2005 is used
            _rptParameters = _rptService.GetReportParameters(_strReportName, _strHistoryId, _boolForRendering, _rptParameterValues, _credentials)

            'Need to load the specific report before passing the parameter values
            _rptExecutionInfoObj = _rptExecutionService.LoadReport(_strReportName, _strHistoryId)

            ' Prepare report parameter. 
            _rptExecutionServiceParameterValues = BuildReportParameters(_rptParameters, getReportParametersObj, strSchoolName)

            _rptExecutionService.Timeout = System.Threading.Timeout.Infinite

            'Set Report Parameters
            _rptExecutionService.SetExecutionParameters(_rptExecutionServiceParameterValues, "en-us")

            'Call ConfigureExportFormat to get the file extensions
            ConfigureExportFormat(_format)

            'Render the report
            _generateReportAsBytes = _rptExecutionService.Render(_format, _deviceInfo, _strExtension, _encoding, _strMimeType, _warnings, _streamIDs)

            Return _generateReportAsBytes
            'Display the report in the browser
            'DisplayReport(_generateReportAsBytes)
        End Function
#End Region

#Region "DisplayReport"
        Private Sub DisplayReport(ByVal byteReportOutput As [Byte]())
            'Pdf
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.ContentType = _strMimeType
            HttpContext.Current.Response.BinaryWrite(byteReportOutput)
        End Sub
        Function GetDatabaseName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("database=")
            If startIndex > -1 Then startIndex += 9
            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""
            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length
            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetServerName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("server=")
            If startIndex > -1 Then startIndex += 7

            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetUserName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("uid=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetPassword(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("pwd=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
#End Region
    End Class
End Namespace



