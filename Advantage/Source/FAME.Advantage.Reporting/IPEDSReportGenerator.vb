﻿Imports FAME.Advantage.Reporting.ReportService2005
Imports FAME.Advantage.Reporting.ReportExecution2005
Imports System.Web
Imports FAME.Advantage.Reporting.Info
Imports FAME.Advantage.Common
Namespace Logic
    Public Class IPEDSReportGenerator
#Region "Report Variables"
        Private _rptService As New ReportingService2005
        Private _rptExecutionService As New ReportExecutionService

        Private strReportName As String = Nothing
        Private _strHistoryId As String = Nothing
        Private _boolForRendering As Boolean = False
        Private _credentials As ReportService2005.DataSourceCredentials() = Nothing
        Private _rptParameterValues As ReportService2005.ParameterValue() = Nothing
        Private _rptParameters As ReportService2005.ReportParameter() = Nothing
        Private _rptExecutionServiceParameterValues As ReportExecution2005.ParameterValue() = Nothing
        Private _rptExecutionInfoObj As New ExecutionInfo
        Dim _strDateRangeTextPrevious As String
        Dim _strDateRangeText2YrPrevious As String

        'Variables needed to render report
        ' Private _historyId As String = Nothing
        Private _deviceInfo As String = Nothing
        'Private _format As String = Nothing
        Private _generateReportAsBytes As [Byte]()
        Private _encoding As String = [String].Empty
        Private Shared _strExtension As String = String.Empty
        Private Shared _strMimeType As String = String.Empty
        Private _warnings As ReportExecution2005.Warning() = Nothing
        Private _streamIDs As String() = Nothing
        Private _strDateRangeText As String
        Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString) '.ToLower
        Dim strServerName As String = GetServerName(strConnectionString)
        Dim strDBName As String = GetDatabaseName(strConnectionString)
        Dim strUserName As String = GetUserName(strConnectionString)
        Dim strPassword As String = GetPassword(strConnectionString)
#End Region

#Region "Initialize Web Services"
        Private Sub InitializeWebServices()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            'Set the credentials and url for the report services
            'The report service object is needed to get the parameter details associated with a specific report
            _rptService.Credentials = System.Net.CredentialCache.DefaultCredentials
            _rptService.Url = MyAdvAppSettings.AppSettings("ReportServices").ToString

            'Set the credentials and url for the report execution services
            _rptExecutionService.Credentials = System.Net.CredentialCache.DefaultCredentials
            _rptExecutionService.Url = MyAdvAppSettings.AppSettings("ReportExecutionServices").ToString
        End Sub
#End Region

#Region "Initialize Export Formats"
        Private Shared Sub ConfigureExportFormat(ByRef strType As String)
            Select Case strType.ToUpper()
                Case "PDF"
                    _strExtension = "pdf"
                    _strMimeType = "application/pdf"
                    Exit Select
                Case "EXCEL", "XLS"
                    _strExtension = "xls"
                    _strMimeType = "application/vnd.excel"
                    Exit Select
                Case "WORD"
                    _strExtension = "doc"
                    _strMimeType = "application/vnd.ms-word"
                Case "CSV"
                    _strExtension = "csv"
                    _strMimeType = "text/csv"
                Case Else
                    Throw New Exception("Unrecognized type: " & strType & ". Type must be PDF, Excel or Image, HTML.")
            End Select
        End Sub
#End Region
#Region "Build Report Parameters"
        Private Function InstructionsReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                  ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                  ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String

            With getReportParametersObj
                CampusId = .CampusId
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then


                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!


                'StuEnrollId
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CampusId"
                rptExecutionParamValues(0).Name = "CampusId"
                If CampusId.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CampusId
                End If

                'CampGrpId Parameter


                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "SchoolName"
                rptExecutionParamValues(1).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(1).Value = strSchoolName
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                'server name
                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "servername"
                rptExecutionParamValues(2).Name = "servername"
                rptExecutionParamValues(2).Value = strServerName

                'Database Name
                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "databasename"
                rptExecutionParamValues(3).Name = "databasename"
                rptExecutionParamValues(3).Value = strDBName


                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "uid"
                rptExecutionParamValues(4).Name = "uid"
                rptExecutionParamValues(4).Value = strUserName

                'password
                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "password"
                rptExecutionParamValues(5).Name = "password"
                rptExecutionParamValues(5).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function StudentsWithMissingDataReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As Integer
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim boolShowSSN As Boolean = False

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .MissingDataSortId
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then


                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!


                'StuEnrollId
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CampusId"
                rptExecutionParamValues(0).Name = "CampusId"
                If CampusId.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CampusId
                End If

                'CampGrpId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "ProgId"
                rptExecutionParamValues(1).Name = "ProgId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(1).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "OrderBY"
                rptExecutionParamValues(3).Name = "OrderBY"
                rptExecutionParamValues(3).Value = SortFields.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ShowSSN"
                rptExecutionParamValues(4).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(4).Value = "true"
                Else
                    rptExecutionParamValues(4).Value = "false"
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "OffRepDate"
                rptExecutionParamValues(5).Name = "OffRepDate"
                If Not OfficialReportingDate.Trim = "" Then
                    rptExecutionParamValues(5).Value = OfficialReportingDate.ToString
                Else
                    rptExecutionParamValues(5).Value = Nothing
                End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ExcludeStuBefThisDate"
                rptExecutionParamValues(6).Name = "ExcludeStuBefThisDate"
                If Not PriorToDate.Trim = "" Then
                    rptExecutionParamValues(6).Value = PriorToDate.Trim
                Else
                    rptExecutionParamValues(6).Value = Nothing
                End If

                'server name
                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "servername"
                rptExecutionParamValues(7).Name = "servername"
                rptExecutionParamValues(7).Value = strServerName

                'Database Name
                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "databasename"
                rptExecutionParamValues(8).Name = "databasename"
                rptExecutionParamValues(8).Value = strDBName

                'user id
                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "uid"
                rptExecutionParamValues(9).Name = "uid"
                rptExecutionParamValues(9).Value = strUserName

                'password
                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "password"
                rptExecutionParamValues(10).Name = "password"
                rptExecutionParamValues(10).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function GEStudentsWithMissingDataReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As Integer
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim boolShowSSN As Boolean = False

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .MissingDataSortId
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then


                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!


                'StuEnrollId
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CampusId"
                rptExecutionParamValues(0).Name = "CampusId"
                If CampusId.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CampusId
                End If

                'CampGrpId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "ProgId"
                rptExecutionParamValues(1).Name = "ProgId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(1).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                'rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(2).Label = "SchoolName"
                'rptExecutionParamValues(2).Name = "SchoolName"
                'If Not strSchoolName.Trim = "" Then
                '    rptExecutionParamValues(2).Value = strSchoolName
                'Else
                '    rptExecutionParamValues(2).Value = Nothing
                'End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "OrderBY"
                rptExecutionParamValues(2).Name = "OrderBY"
                rptExecutionParamValues(2).Value = SortFields.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                'rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(3).Label = "ShowSSN"
                'rptExecutionParamValues(3).Name = "ShowSSN"
                'If boolShowSSN = True Then
                '    rptExecutionParamValues(3).Value = "true"
                'Else
                '    rptExecutionParamValues(3).Value = "false"
                'End If

                'rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(5).Label = "OffRepDate"
                'rptExecutionParamValues(5).Name = "OffRepDate"
                'If Not OfficialReportingDate.Trim = "" Then
                '    rptExecutionParamValues(5).Value = OfficialReportingDate.ToString
                'Else
                '    rptExecutionParamValues(5).Value = Nothing
                'End If

                'rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(6).Label = "ExcludeStuBefThisDate"
                'rptExecutionParamValues(6).Name = "ExcludeStuBefThisDate"
                'If Not PriorToDate.Trim = "" Then
                '    rptExecutionParamValues(6).Value = PriorToDate.Trim
                'Else
                '    rptExecutionParamValues(6).Value = Nothing
                'End If

                'server name
                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "servername"
                rptExecutionParamValues(3).Name = "servername"
                rptExecutionParamValues(3).Value = strServerName

                'Database Name
                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "databasename"
                rptExecutionParamValues(4).Name = "databasename"
                rptExecutionParamValues(4).Value = strDBName

                'user id
                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "uid"
                rptExecutionParamValues(5).Name = "uid"
                rptExecutionParamValues(5).Value = strUserName

                'password
                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "password"
                rptExecutionParamValues(6).Name = "password"
                rptExecutionParamValues(6).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function CompletionsDetailReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim StudentIdentifier As String
            Dim sReportTitle As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then


                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!


                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "COMPLETIONS"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                'US3158 & US3157
                If getReportParametersObj.ResourceId.ToString = "444" Or
                    getReportParametersObj.ResourceId.ToString = "445" Or
                    getReportParametersObj.ResourceId.ToString = "784" Then
                    sReportTitle = "CIP DATA"
                ElseIf getReportParametersObj.ResourceId.ToString = "785" Then
                    sReportTitle = "ALL COMPLETERS"
                ElseIf getReportParametersObj.ResourceId.ToString = "786" Then
                    sReportTitle = "COMPLETERS BY LEVEL"
                End If
                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = sReportTitle   '"CIP DATA"

                Dim intCohortYear As Integer = CInt(strReportingYear.ToString.Trim)

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = "07/01/" + intCohortYear.ToString

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = "06/30/" + (intCohortYear + 1).ToString



                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "DateRangeText"
                rptExecutionParamValues(9).Name = "DateRangeText"
                'US3158 - 2012 Fall update
                'rptExecutionParamValues(9).Value = "AWARDS/DEGREES CONFERRED BETWEEN JULY 1, " & intCohortYear.ToString.Trim & " AND JUNE 30, " & (intCohortYear + 1).ToString.Trim
                rptExecutionParamValues(9).Value = "JULY 1, " & intCohortYear.ToString.Trim & " THROUGH JUNE 30, " & (intCohortYear + 1).ToString.Trim

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "CampusName"
                rptExecutionParamValues(12).Name = "CampusName"
                rptExecutionParamValues(12).Value = CampusName

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "StudentIdentifier"
                rptExecutionParamValues(13).Name = "StudentIdentifier"
                rptExecutionParamValues(13).Value = StudentIdentifier

                'server name
                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "servername"
                rptExecutionParamValues(14).Name = "servername"
                rptExecutionParamValues(14).Value = strServerName

                'Database Name
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "databasename"
                rptExecutionParamValues(15).Name = "databasename"
                rptExecutionParamValues(15).Value = strDBName


                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "uid"
                rptExecutionParamValues(16).Name = "uid"
                rptExecutionParamValues(16).Value = strUserName

                'password
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "password"
                rptExecutionParamValues(17).Name = "password"
                rptExecutionParamValues(17).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function FallEnrollmentDetailReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            Dim dtStartDate As Date
            Dim dtEndDate As Date
            ''  Dim strSortColumn As String
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                'This is a special case for 12 Month Enrollment Reports
                'The Start Date is between September 2
                If getReportParametersObj.ResourceId.ToString = "643" Or getReportParametersObj.ResourceId.ToString = "644" Then
                    If Not OfficialReportingDate.ToString.Trim = "" Then
                        intReportYear = Year(CDate(OfficialReportingDate))
                        'DE8460 - QA: IPEDS: 12 month enrollment report's start date is incorrect.
                        'dtStartDate = DateAdd(DateInterval.Day, -364, CDate(OfficialReportingDate)) 'DateAdd(DateInterval.Year, -1, CDate(OfficialReportingDate))
                        dtStartDate = DateAdd(DateInterval.Day, 1, DateAdd(DateInterval.Year, -1, CDate(OfficialReportingDate)))

                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = (Month(dtStartDate).ToString & "/" & Day(dtStartDate).ToString & "/" & Year(dtStartDate).ToString).ToString
                        .EndDate = (Month(dtEndDate).ToString & "/" & Day(dtEndDate).ToString & "/" & Year(dtEndDate).ToString).ToString
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then


                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!


                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "12 MONTH ENROLLMENT"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = "PART A - UNDUPLICATED COUNT"

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate

                Dim strDateRange As String = (MonthName(Month(dtStartDate)) & " " & Day(dtStartDate) & ", " & Year(dtStartDate)) & " THROUGH " & (MonthName(Month(dtEndDate)) & " " & Day(dtEndDate) & ", " & Year(dtEndDate))

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "DateRangeText"
                rptExecutionParamValues(9).Name = "DateRangeText"
                rptExecutionParamValues(9).Value = strDateRange.ToUpper

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "CampusName"
                rptExecutionParamValues(12).Name = "CampusName"
                rptExecutionParamValues(12).Value = CampusName

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "StudentIdentifier"
                rptExecutionParamValues(13).Name = "StudentIdentifier"
                rptExecutionParamValues(13).Value = StudentIdentifier

                'server name
                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "servername"
                rptExecutionParamValues(14).Name = "servername"
                rptExecutionParamValues(14).Value = strServerName

                'Database Name
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "databasename"
                rptExecutionParamValues(15).Name = "databasename"
                rptExecutionParamValues(15).Value = strDBName


                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "uid"
                rptExecutionParamValues(16).Name = "uid"
                rptExecutionParamValues(16).Value = strUserName

                'password
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "password"
                rptExecutionParamValues(17).Name = "password"
                rptExecutionParamValues(17).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function PartB4DetailReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                'This is a special case for 12 Month Enrollment Reports
                'The Start Date is between September 2
                If getReportParametersObj.ResourceId.ToString = "641" Or getReportParametersObj.ResourceId.ToString = "642" Or _
                    getReportParametersObj.ResourceId.ToString = "668" Or getReportParametersObj.ResourceId.ToString = "787" Then
                    intReportYear = CInt(CohortYear)
                    If SchoolType.ToString.Trim.ToLower = "program" Then
                        'Program Type Schools - date range is always between 08/1/cohortyear and 10/31/cohortyear
                        .StartDate = "08/01/" + CohortYear.ToString.Trim
                        .EndDate = "10/31/" + CohortYear.ToString.Trim
                        _strDateRangeText = "AUGUST 01, " + CohortYear.ToString.Trim + " THROUGH OCTOBER 31, " + CohortYear.ToString.Trim
                    Else
                        'Academic schools date range is always between 10/15/cohortyear and 10/15/nextyear
                        dtStartDate = CDate(OfficialReportingDate) 'DateAdd(DateInterval.Day, -364, CDate(OfficialReportingDate)) 'DateAdd(DateInterval.Year, -1, CDate(OfficialReportingDate))
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = (Month(dtStartDate).ToString & "/" & Day(dtStartDate).ToString & "/" & Year(dtStartDate).ToString).ToString
                        .EndDate = (Month(dtEndDate).ToString & "/" & Day(dtEndDate).ToString & "/" & Year(dtEndDate).ToString).ToString
                        _strDateRangeText = "ENROLLMENT AS OF " & (MonthName(Month(dtStartDate)) & " " & Day(dtStartDate) & ", " & Year(dtStartDate))
                        '.StartDate = "10/15/" + intReportYear.ToString 'For Academic Year Schools the reports will ignore the start date
                        '.EndDate = "10/15/" + intReportYear.ToString
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "INSTITUTIONAL CHARACTERISTICS"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = "PART B - ESTIMATED FALL ENROLLMENT"

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate



                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "DateRangeText"
                rptExecutionParamValues(9).Name = "DateRangeText"
                rptExecutionParamValues(9).Value = _strDateRangeText.ToUpper

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "CampusName"
                rptExecutionParamValues(12).Name = "CampusName"
                rptExecutionParamValues(12).Value = CampusName

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "StudentIdentifier"
                rptExecutionParamValues(13).Name = "StudentIdentifier"
                rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "servername"
                rptExecutionParamValues(14).Name = "servername"
                rptExecutionParamValues(14).Value = strServerName

                'Database Name
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "databasename"
                rptExecutionParamValues(15).Name = "databasename"
                rptExecutionParamValues(15).Value = strDBName


                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "uid"
                rptExecutionParamValues(16).Name = "uid"
                rptExecutionParamValues(16).Value = strUserName

                'password
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "password"
                rptExecutionParamValues(17).Name = "password"
                rptExecutionParamValues(17).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function PartB4SummaryReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                      ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                      ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                'This is a special case for 12 Month Enrollment Reports
                'The Start Date is between September 2
                If getReportParametersObj.ResourceId.ToString = "641" Or getReportParametersObj.ResourceId.ToString = "642" Or _
                    getReportParametersObj.ResourceId.ToString = "669" Then
                    intReportYear = CInt(CohortYear)
                    If SchoolType.ToString.Trim.ToLower = "program" Then
                        'Program Type Schools - date range is always between 08/1/cohortyear and 10/31/cohortyear
                        .StartDate = "08/01/" + CohortYear.ToString.Trim
                        .EndDate = "10/31/" + CohortYear.ToString.Trim
                        _strDateRangeText = "AUGUST 01, " + CohortYear.ToString.Trim + " THROUGH OCTOBER 31, " + CohortYear.ToString.Trim
                    Else
                        'Academic schools date range is always between 10/15/cohortyear and 10/15/nextyear
                        '.StartDate = "10/15/" + intReportYear.ToString
                        '.EndDate = "10/15/" + (intReportYear).ToString
                        '_strDateRangeText = "ENROLLMENT AS OF OCTOBER 15, " + CohortYear.ToString.Trim
                        'Academic schools date range is always between 10/15/cohortyear and 10/15/nextyear
                        dtStartDate = CDate(OfficialReportingDate) 'DateAdd(DateInterval.Day, -364, CDate(OfficialReportingDate)) 'DateAdd(DateInterval.Year, -1, CDate(OfficialReportingDate))
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = (Month(dtStartDate).ToString & "/" & Day(dtStartDate).ToString & "/" & Year(dtStartDate).ToString).ToString
                        .EndDate = (Month(dtEndDate).ToString & "/" & Day(dtEndDate).ToString & "/" & Year(dtEndDate).ToString).ToString
                        _strDateRangeText = "ENROLLMENT AS OF " & (MonthName(Month(dtStartDate)) & " " & Day(dtStartDate) & ", " & Year(dtStartDate))
                        '.StartDate = "10/15/" + intReportYear.ToString 'For Academic Year Schools the reports will ignore the start date
                        '.EndDate = "10/15/" + intReportYear.ToString
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!


                'CampusId Parameter

                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CampusId"
                rptExecutionParamValues(0).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(0).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(0).Value = Nothing
                End If

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "SchoolName"
                rptExecutionParamValues(1).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(1).Value = strSchoolName
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "ResourceId"
                rptExecutionParamValues(2).Name = "ResourceId"
                rptExecutionParamValues(2).Value = getReportParametersObj.ResourceId.ToString

                'CohortYear
                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "CohortYear"
                rptExecutionParamValues(3).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(3).Value = Nothing
                Else
                    rptExecutionParamValues(3).Value = CohortYear
                End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "OfficialReportingDate"
                rptExecutionParamValues(4).Name = "OfficialReportingDate"
                If OfficialReportingDate.Trim = "" Then
                    rptExecutionParamValues(4).Value = Nothing
                Else
                    rptExecutionParamValues(4).Value = OfficialReportingDate
                End If


                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "ProgId"
                rptExecutionParamValues(5).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(5).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(5).Value = Nothing
                End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "SurveyTitle"
                rptExecutionParamValues(6).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(6).Value = "INSTITUTIONAL CHARACTERISTICS"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "ReportTitle"
                rptExecutionParamValues(7).Name = "ReportTitle"
                rptExecutionParamValues(7).Value = "PART B - ESTIMATED FALL ENROLLMENT"

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "ReportTitleDate"
                rptExecutionParamValues(8).Name = "ReportTitleDate"
                rptExecutionParamValues(8).Value = "ReportTitleDate"

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "OrderBy"
                rptExecutionParamValues(9).Name = "OrderBy"
                rptExecutionParamValues(9).Value = SortFields.ToString

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "StartDate"
                rptExecutionParamValues(10).Name = "StartDate"
                rptExecutionParamValues(10).Value = StartDate

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "EndDate"
                rptExecutionParamValues(11).Name = "EndDate"
                rptExecutionParamValues(11).Value = EndDate

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "DateRangeText"
                rptExecutionParamValues(12).Name = "DateRangeText"
                rptExecutionParamValues(12).Value = _strDateRangeText.ToUpper

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "StudentIdentifier"
                rptExecutionParamValues(13).Name = "StudentIdentifier"
                rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "servername"
                rptExecutionParamValues(14).Name = "servername"
                rptExecutionParamValues(14).Value = strServerName

                'Database Name
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "databasename"
                rptExecutionParamValues(15).Name = "databasename"
                rptExecutionParamValues(15).Value = strDBName


                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "uid"
                rptExecutionParamValues(16).Name = "uid"
                rptExecutionParamValues(16).Value = strUserName

                'password
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "password"
                rptExecutionParamValues(17).Name = "password"
                rptExecutionParamValues(17).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function FallEnrollmentDetailSummaryReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                'This is a special case for 12 Month Enrollment Reports
                'The Start Date is between September 2
                'If getReportParametersObj.ResourceId.ToString = "641" Or getReportParametersObj.ResourceId.ToString = "642" Then
                intReportYear = CInt(strReportingYear)
                'If SchoolType.ToString.Trim.ToLower = "program" Then
                '    'Program Type Schools - date range is always between 08/1/cohortyear and 10/31/cohortyear
                '    .StartDate = "09/01/" + CohortYear.ToString.Trim
                '    .EndDate = "10/31/" + CohortYear.ToString.Trim
                '    _strDateRangeText = "AUGUST 01, " + CohortYear.ToString.Trim + " THROUGH OCTOBER 31, " + CohortYear.ToString.Trim
                'Else
                '    'Academic schools date range is always between 10/15/cohortyear and 10/15/nextyear
                '    .StartDate = "10/15/" + intReportYear.ToString
                '    .EndDate = "10/15/" + (intReportYear + 1).ToString
                '    _strDateRangeText = "ENROLLMENT AS OF OCTOBER 15, " + CohortYear.ToString.Trim
                'End If
                '.StartDate = "09/01/" + strReportingYear.ToString
                '.EndDate = "08/31/" + (intReportYear + 1).ToString
                '_strDateRangeText = "SEPTEMBER 01, " + strReportingYear.ToString.Trim + " THROUGH AUGUST 31, " + (intReportYear + 1).ToString.Trim

                If Not OfficialReportingDate.ToString.Trim = "" Then
                    intReportYear = Year(CDate(OfficialReportingDate))
                    'DE8460 - QA: IPEDS: 12 month enrollment report's start date is incorrect.
                    'dtStartDate = DateAdd(DateInterval.Day, -364, CDate(OfficialReportingDate)) 'DateAdd(DateInterval.Year, -1, CDate(OfficialReportingDate))
                    dtStartDate = DateAdd(DateInterval.Day, 1, DateAdd(DateInterval.Year, -1, CDate(OfficialReportingDate)))
                    dtEndDate = CDate(OfficialReportingDate)
                    .StartDate = (Month(dtStartDate).ToString & "/" & Day(dtStartDate).ToString & "/" & Year(dtStartDate).ToString).ToString
                    .EndDate = (Month(dtEndDate).ToString & "/" & Day(dtEndDate).ToString & "/" & Year(dtEndDate).ToString).ToString
                End If
                ' End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = intReportYear.ToString.Trim
                    ' End If
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "12 MONTH ENROLLMENT"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = "PART B - INSTRUCTIONAL ACTIVITY"

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "ReportEndDate"
                rptExecutionParamValues(9).Name = "ReportEndDate"
                rptExecutionParamValues(9).Value = EndDate

                _strDateRangeText = (MonthName(Month(dtStartDate)) & " " & Day(dtStartDate) & ", " & Year(dtStartDate)) & " THROUGH " & (MonthName(Month(dtEndDate)) & " " & Day(dtEndDate) & ", " & Year(dtEndDate))

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "DateRangeText"
                rptExecutionParamValues(10).Name = "DateRangeText"
                rptExecutionParamValues(10).Value = _strDateRangeText.ToUpper

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "ShowSSN"
                rptExecutionParamValues(11).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(11).Value = "true"
                Else
                    rptExecutionParamValues(11).Value = "false"
                End If

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "OrderBy"
                rptExecutionParamValues(12).Name = "OrderBy"
                rptExecutionParamValues(12).Value = SortFields.ToString

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "CampusName"
                rptExecutionParamValues(13).Name = "CampusName"
                rptExecutionParamValues(13).Value = CampusName

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "StudentIdentifier"
                rptExecutionParamValues(14).Name = "StudentIdentifier"
                rptExecutionParamValues(14).Value = StudentIdentifier

                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "servername"
                rptExecutionParamValues(15).Name = "servername"
                rptExecutionParamValues(15).Value = strServerName

                'Database Name
                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "databasename"
                rptExecutionParamValues(16).Name = "databasename"
                rptExecutionParamValues(16).Value = strDBName


                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "uid"
                rptExecutionParamValues(17).Name = "uid"
                rptExecutionParamValues(17).Value = strUserName

                'password
                rptExecutionParamValues(18) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(18).Label = "password"
                rptExecutionParamValues(18).Name = "password"
                rptExecutionParamValues(18).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function PartC3DetailReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            Dim areasofinterest As String
            Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                areasofinterest = .AreasOfInterestId
                'This is a special case for 12 Month Enrollment Reports
                'The Start Date is between September 2
                If getReportParametersObj.ResourceId.ToString = "452" Or getReportParametersObj.ResourceId.ToString = "458" Then
                    intReportYear = CInt(CohortYear)
                    If SchoolType.ToString.Trim.ToLower = "program" Then
                        'Program Type Schools - date range is always between 08/1/cohortyear and 10/31/cohortyear
                        .StartDate = "10/15/" + CohortYear.ToString.Trim ' For C3 Reports it is 10/15/2009
                        .EndDate = "10/15/" + CohortYear.ToString.Trim
                        _strDateRangeText = "OCTOBER 15, " + CohortYear.ToString.Trim
                    Else
                        'Academic schools date range is always between 10/15/cohortyear and 10/15/nextyear
                        '.StartDate = "10/15/" + intReportYear.ToString
                        '.EndDate = "10/15/" + (intReportYear).ToString
                        '_strDateRangeText = "OCTOBER 15, " + CohortYear.ToString.Trim


                        If Not OfficialReportingDate.ToString.Trim = "" Then
                            intReportYear = Year(CDate(OfficialReportingDate))
                            dtStartDate = CDate(OfficialReportingDate)
                            dtEndDate = CDate(OfficialReportingDate) 'DateAdd(DateInterval.Year, 1, CDate(OfficialReportingDate))
                            .StartDate = (Month(dtStartDate).ToString & "/" & Day(dtStartDate).ToString & "/" & Year(dtStartDate).ToString).ToString
                            .EndDate = (Month(dtEndDate).ToString & "/" & Day(dtEndDate).ToString & "/" & Year(dtEndDate).ToString).ToString
                            _strDateRangeText = (MonthName(Month(dtStartDate)) & " " & Day(dtStartDate) & ", " & Year(dtStartDate))
                        End If
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "ADMISSIONS"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = "ADMISSIONS & ENROLLED STUDENTS"

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate



                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "DateRangeText"
                rptExecutionParamValues(9).Name = "DateRangeText"
                rptExecutionParamValues(9).Value = _strDateRangeText.ToUpper

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "CampusName"
                rptExecutionParamValues(12).Name = "CampusName"
                rptExecutionParamValues(12).Value = CampusName

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "AreasOfInterest"
                rptExecutionParamValues(13).Name = "AreasOfInterest"
                If Not areasofinterest.Trim = "" Then
                    rptExecutionParamValues(13).Value = areasofinterest
                Else
                    rptExecutionParamValues(13).Value = Nothing
                End If

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "StudentIdentifier"
                rptExecutionParamValues(14).Name = "StudentIdentifier"
                rptExecutionParamValues(14).Value = StudentIdentifier

                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "servername"
                rptExecutionParamValues(15).Name = "servername"
                rptExecutionParamValues(15).Value = strServerName

                'Database Name
                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "databasename"
                rptExecutionParamValues(16).Name = "databasename"
                rptExecutionParamValues(16).Value = strDBName


                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "uid"
                rptExecutionParamValues(17).Name = "uid"
                rptExecutionParamValues(17).Value = strUserName

                'password
                rptExecutionParamValues(18) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(18).Label = "password"
                rptExecutionParamValues(18).Name = "password"
                rptExecutionParamValues(18).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function DisabilityServiceReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String
            Dim index AS Integer
            Dim cohortType as String 

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType

                SortFields = .SortFields
                If (SortFields = "SSN")
                    SortFields = "StudentIdentifier"
                End If
                If (SortFields = "LastName")
                    SortFields = "StudentName"
                End If
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                'This is a special case for 12 Month Enrollment Reports
                'The Start Date is between September 2
                If getReportParametersObj.ResourceId.ToString = "836" 
                    intReportYear = CInt(CohortYear)
                    If SchoolType.ToString.Trim.ToLower = "program" Then
                        'Program Type Schools - date range is always between 08/1/cohortyear and 10/31/cohortyear
                        .StartDate = "08/01/" + CohortYear.ToString.Trim
                        .EndDate = "10/31/" + CohortYear.ToString.Trim
                        _strDateRangeText = "AUGUST 01, " + CohortYear.ToString.Trim + " THRU OCTOBER 31, " + CohortYear.ToString.Trim
                    Else
                        'Academic schools date range is always between 10/15/cohortyear and 10/15/nextyear
                        dtStartDate = CDate(OfficialReportingDate) 'DateAdd(DateInterval.Day, -364, CDate(OfficialReportingDate)) 'DateAdd(DateInterval.Year, -1, CDate(OfficialReportingDate))
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = (Month(dtStartDate).ToString & "/" & Day(dtStartDate).ToString & "/" & Year(dtStartDate).ToString).ToString
                        .EndDate = (Month(dtEndDate).ToString & "/" & Day(dtEndDate).ToString & "/" & Year(dtEndDate).ToString).ToString
                        _strDateRangeText = "ENROLLMENT AS OF " & (MonthName(Month(dtStartDate)) & " " & Day(dtStartDate) & ", " & Year(dtStartDate))
                        '.StartDate = "10/15/" + intReportYear.ToString 'For Academic Year Schools the reports will ignore the start date
                        '.EndDate = "10/15/" + intReportYear.ToString
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
                
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                ' Server Name    0   
                index = 0
                rptExecutionParamValues(index) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(index).Label = "servername"
                rptExecutionParamValues(index).Name = "servername"
                rptExecutionParamValues(index).Value = strServerName

                'Database Name   1   
                index += 1
                rptExecutionParamValues(index) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(index).Label = "databasename"
                rptExecutionParamValues(index).Name = "databasename"
                rptExecutionParamValues(index).Value = strDBName

                'User Id        2
                index += 1
                rptExecutionParamValues(index) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(index).Label = "uid"
                rptExecutionParamValues(index).Name = "uid"
                rptExecutionParamValues(index).Value = strUserName

                'password      3
                index += 1
                rptExecutionParamValues(index) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(index).Label = "password"
                rptExecutionParamValues(index).Name = "password"
                rptExecutionParamValues(index).Value = strPassword

                'School Name   4
                index += 1
                rptExecutionParamValues(index) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(index).Label = "School Name"
                rptExecutionParamValues(index).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(index).Value = strSchoolName
                Else
                    rptExecutionParamValues(index).Value = Nothing
                End If

                'Campus Id    5
                index += 1
                rptExecutionParamValues(index) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(index).Label = "CampusId"
                rptExecutionParamValues(index).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(index).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(index).Value = Nothing
                End If

                'Program Id List 6
                index += 1
                rptExecutionParamValues(index) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(index).Label = "ProgId List"
                rptExecutionParamValues(index).Name = "ProgIdList"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(index).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(index).Value = Nothing
                End If

                'CohortYear   7
                index += 1
                rptExecutionParamValues(index) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(index).Label = "CohortYear"
                rptExecutionParamValues(index).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(index).Value = Nothing
                Else
                    rptExecutionParamValues(index).Value = CohortYear
                End If

                'Start Date  8
                index += 1
                rptExecutionParamValues(index) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(index).Label = "Start Date"
                rptExecutionParamValues(index).Name = "StartDate"
                rptExecutionParamValues(index).Value = StartDate

                'End Date   9
                index += 1
                rptExecutionParamValues(index) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(index).Label = "End Date"
                rptExecutionParamValues(index).Name = "EndDate"
                rptExecutionParamValues(index).Value = EndDate

                'Date Range Text 10
                index += 1
                rptExecutionParamValues(index) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(index).Label = "Date Range Text"
                rptExecutionParamValues(index).Name = "DateRangeText"
                rptExecutionParamValues(index).Value = _strDateRangeText.ToUpper

                'Show SSN       11
                index += 1
                rptExecutionParamValues(index) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(index).Label = "Show SSN"
                rptExecutionParamValues(index).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(index).Value = "true"
                Else
                    rptExecutionParamValues(index).Value = "false"
                End If

                'Order by    12
                index += 1
                rptExecutionParamValues(index) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(index).Label = "Order By"
                rptExecutionParamValues(index).Name = "OrderBy"
                rptExecutionParamValues(index).Value = SortFields.ToString

            End If
            Return rptExecutionParamValues
        End Function

        Private Function TestScoresDetailReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim StudentIdentifier As String
            Dim sReportTitle As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then


                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!


                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "ADMISSIONS"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                
                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = "TEST SCORES"   '"CIP DATA"

                Dim intCohortYear As Integer = CInt(strReportingYear.ToString.Trim)

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = "10/15/" + CohortYear.ToString

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = "10/15/" + (Cint(CohortYear) + 1).ToString



                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "DateRangeText"
                rptExecutionParamValues(9).Name = "DateRangeText"
                'US3158 - 2012 Fall update
                'rptExecutionParamValues(9).Value = "AWARDS/DEGREES CONFERRED BETWEEN JULY 1, " & intCohortYear.ToString.Trim & " AND JUNE 30, " & (intCohortYear + 1).ToString.Trim
                rptExecutionParamValues(9).Value = "October 15, " & CohortYear.ToString.Trim 

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "CampusName"
                rptExecutionParamValues(12).Name = "CampusName"
                rptExecutionParamValues(12).Value = CampusName

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "AreasOfInterest"
                rptExecutionParamValues(13).Name = "AreasOfInterest"
                rptExecutionParamValues(13).Value = Nothing

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "StudentIdentifier"
                rptExecutionParamValues(14).Name = "StudentIdentifier"
                rptExecutionParamValues(14).Value = StudentIdentifier

                'server name
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "servername"
                rptExecutionParamValues(15).Name = "servername"
                rptExecutionParamValues(15).Value = strServerName

                'Database Name
                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "databasename"
                rptExecutionParamValues(16).Name = "databasename"
                rptExecutionParamValues(16).Value = strDBName


                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "uid"
                rptExecutionParamValues(17).Name = "uid"
                rptExecutionParamValues(17).Value = strUserName

                'password
                rptExecutionParamValues(18) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(18).Label = "password"
                rptExecutionParamValues(18).Name = "password"
                rptExecutionParamValues(18).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function

        Private Function PellRecipientsDetailReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim StudentIdentifier As String
            Dim sReportTitle As String
            Dim FullorFall as String
            Dim SchoolTypeList as String
            Dim CohortType as String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                StudentIdentifier = .StudentIdentifier
                SchoolTypeList = .SchoolTypeList
                FullorFall = .FullorFall
                CohortType = .CohortType
                if FullorFall.ToLower()="full" Then
                    StartDate = "09/01/" + CohortYear
                ElseIf FullorFall.ToLower()="fall"  Then
                    StartDate=.OfficialReportingDate
                End If
                 if FullorFall.ToLower()="full" Then
                    EndDate = "08/31/" + (CInt(CohortYear)+1).ToString()
                ElseIf FullorFall.ToLower()="fall"  Then
                    EndDate=.OfficialReportingDate
                End If
             End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then


                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!


                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "GRADUATION RATES"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                
                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = "TEST SCORES"   '"CIP DATA"

                Dim intCohortYear As Integer = CInt(strReportingYear.ToString.Trim)

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "CohortPossible"
                rptExecutionParamValues(9).Name = "CohortPossible"
                rptExecutionParamValues(9).Value = FullorFall.ToString

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "StudentIdentifier"
                rptExecutionParamValues(12).Name = "StudentIdentifier"
                rptExecutionParamValues(12).Value = StudentIdentifier

               'server name
                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "ServerName"
                rptExecutionParamValues(13).Name = "ServerName"
                rptExecutionParamValues(13).Value = strServerName

                'Database Name
                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "DBName"
                rptExecutionParamValues(14).Name = "DBName"
                rptExecutionParamValues(14).Value = strDBName


                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "UserName"
                rptExecutionParamValues(15).Name = "UserName"
                rptExecutionParamValues(15).Value = strUserName

                'password
                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "Password"
                rptExecutionParamValues(16).Name = "Password"
                rptExecutionParamValues(16).Value = strPassword

                'Campus Name
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "CampusName"
                rptExecutionParamValues(17).Name = "CampusName"
                rptExecutionParamValues(17).Value = CampusName

                 'School TypeList
                rptExecutionParamValues(18) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(18).Label = "SchoolType"
                rptExecutionParamValues(18).Name = "SchoolType"
                rptExecutionParamValues(18).Value = SchoolTypeList

                  'School TypeList
                rptExecutionParamValues(19) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(19).Label = "Subcohort"
                rptExecutionParamValues(19).Name = "Subcohort"
                rptExecutionParamValues(19).Value = CohortType
            End If
            Return rptExecutionParamValues
        End Function
        #End Region

#Region "Build Report"
        Public Function RenderReport(ByVal _format As String, _
                                                                    ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                    ByVal strSchoolName As String) As [Byte]()

            'Initialize the report services and report execution services
            InitializeWebServices()

            'Set the path of report
            strReportName = getReportParametersObj.Environment & getReportParametersObj.ReportPath   '"/Advantage Reports/StudentAccounts/IPEDS/IPEDSMissingDataReport"

            'Use the Report Services: GetReportParameters method to get a list of parameters for a specified report
            'This is the only location where the reportingservices2005 is used
            _rptParameters = _rptService.GetReportParameters(strReportName, _strHistoryId, _boolForRendering, _rptParameterValues, _credentials)

            'Need to load the specific report before passing the parameter values
            _rptExecutionInfoObj = _rptExecutionService.LoadReport(strReportName, _strHistoryId)

            ' Prepare report parameter. 
            _rptExecutionServiceParameterValues = BuildIPEDSReports(_rptParameters, getReportParametersObj, strSchoolName)

            _rptExecutionService.Timeout = Threading.Timeout.Infinite

            'Set Report Parameters
            _rptExecutionService.SetExecutionParameters(_rptExecutionServiceParameterValues, "en-us")

            'Call ConfigureExportFormat to get the file extensions
            ConfigureExportFormat(_format)

            'Render the report
            _generateReportAsBytes = _rptExecutionService.Render(_format, _deviceInfo, _strExtension, _encoding, _strMimeType, _warnings, _streamIDs)

            Return _generateReportAsBytes

        End Function
#End Region

#Region "Build Report Parameters Based on ResourceId"
        Private Function BuildIPEDSReports(ByVal rptParameters As ReportService2005.ReportParameter(), _
                                           ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                           ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()



            Dim rptIPEDSExecutionServiceParameterValues As ReportExecution2005.ParameterValue() = Nothing
            'Code modified because of resource id conflicts
            If InStr(getReportParametersObj.ReportPath, "Outcome") >= 1 Then
                rptIPEDSExecutionServiceParameterValues = OutcomeMeasuresParameters(_rptParameters, _
                                                                                                     getReportParametersObj, _
                                                                                                     strSchoolName)
                Return rptIPEDSExecutionServiceParameterValues
            ElseIf InStr(getReportParametersObj.ReportPath, "PellRecipients") >= 1 Then
                rptIPEDSExecutionServiceParameterValues = PellRecipientsDetailReportParameters(_rptParameters, _
                                                                                                     getReportParametersObj, _
                                                                                                     strSchoolName)
                Return rptIPEDSExecutionServiceParameterValues
            ElseIf InStr(getReportParametersObj.ReportPath, "TestScores") >= 1 Then
                rptIPEDSExecutionServiceParameterValues = TestScoresDetailReportParameters(_rptParameters, _
                                                                                                     getReportParametersObj, _
                                                                                                     strSchoolName)
                Return rptIPEDSExecutionServiceParameterValues
            End If

            Select Case getReportParametersObj.ResourceId.ToString

                Case Is = "639" 'Instructions Report
                    rptIPEDSExecutionServiceParameterValues = InstructionsReportParameters(_rptParameters, _
                                                                                                    getReportParametersObj, _
                                                                                                    strSchoolName)

                Case Is = "640" 'Students with Missing Data Report
                    rptIPEDSExecutionServiceParameterValues = StudentsWithMissingDataReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "822" 'Students with Missing Data Report
                    rptIPEDSExecutionServiceParameterValues = GEStudentsWithMissingDataReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                    'Completions Detail Report, Completions Summary Report,Completions CIP Detail And Summary Report, All Completers   US3158 & US3157
                Case Is = "444", "445", "784", "785", "786"
                    rptIPEDSExecutionServiceParameterValues = CompletionsDetailReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "452", "458" 'C3 Detail Report, C3 Summary Report"
                    rptIPEDSExecutionServiceParameterValues = PartC3DetailReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "814","815"
                    rptIPEDSExecutionServiceParameterValues = TestScoresDetailReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "837"
                    rptIPEDSExecutionServiceParameterValues = PellRecipientsDetailReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "643", "644"  '12 Month Enrollment Detail and Summary Report
                    rptIPEDSExecutionServiceParameterValues = FallEnrollmentDetailReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)

                Case Is = "641", "668", "787" 'Part B4 Detail  Report
                    rptIPEDSExecutionServiceParameterValues = PartB4DetailReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)

                Case Is = "642", "669" 'Part B4 Summary Report
                    rptIPEDSExecutionServiceParameterValues = PartB4SummaryReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)

                Case Is = "645"  'Part B4 Detail & Summary  Report
                    rptIPEDSExecutionServiceParameterValues = FallEnrollmentDetailSummaryReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                    ''Case Is = "410", "411"  'Fall Enrollment Spring PartA Detail & Summary  Report
                    ''    rptIPEDSExecutionServiceParameterValues = FallEnrollmentSpringPartADetailSummaryReportParameters(_rptParameters, _
                    ''                                                                                      getReportParametersObj, _
                    ''                                                                                      strSchoolName)
                Case Is = "412", "413"  'Fall Enrollment Spring PartB Detail & Summary  Report
                    rptIPEDSExecutionServiceParameterValues = FallEnrollmentSpringPartBDetailSummaryReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "414", "415"  'Fall Enrollment Spring PartC Detail & Summary  Report
                    rptIPEDSExecutionServiceParameterValues = FallEnrollmentSpringPartCDetailSummaryReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "416"  'Fall Enrollment Spring PartD Detail & Summary  Report
                    rptIPEDSExecutionServiceParameterValues = FallEnrollmentSpringPartDDetailSummaryReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "417"  'Fall Enrollment Spring PartE Detail & Summary  Report
                    rptIPEDSExecutionServiceParameterValues = FallEnrollmentSpringPartEDetailSummaryReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "421"  'Financial- All Inst  Report
                    rptIPEDSExecutionServiceParameterValues = FinancialAllInstReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)

                    'Code added by Atul Kamble on Feb 02, 2011.
                Case Is = "659"  'Spring- Grad Rates 2Yr Inst Detail Report
                    rptIPEDSExecutionServiceParameterValues = GraduationRate2YrInstDetailReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "660"  'Spring- Grad Rates 2Yr Inst Summary Report
                    rptIPEDSExecutionServiceParameterValues = GraduationRate2YrInstSummaryReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "661"  'Spring- Grad Rates 4Yr Inst SectionI Detail Report
                    rptIPEDSExecutionServiceParameterValues = GraduationRate4YrInstSectionIDetailReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "662"  'Spring- Grad Rates 4Yr Inst SectionI Summary Report
                    rptIPEDSExecutionServiceParameterValues = GraduationRate4YrInstSectionISummaryReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "663"  'Spring- Grad Rates 4Yr Inst SectionII Detail Report
                    rptIPEDSExecutionServiceParameterValues = GraduationRate4YrInstSectionIIDetailReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "664"  'Spring- Grad Rates 4Yr Inst SectionII Summary Report
                    rptIPEDSExecutionServiceParameterValues = GraduationRate4YrInstSectionIISummaryReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "665"  'Spring- Grad Rates 4Yr Inst SectionIII Detail Report
                    rptIPEDSExecutionServiceParameterValues = GraduationRate4YrInstSectionIIIDetailReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "666"  'Spring- Grad Rates 4Yr Inst SectionIII Summary Report
                    rptIPEDSExecutionServiceParameterValues = GraduationRate4YrInstSectionIIISummaryReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "667"  'Spring- Grad Rates Less Than 2Yr Inst Detail & Summary Report
                    rptIPEDSExecutionServiceParameterValues = GraduationRateLessThan2YrInstDetailAndSummaryReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                    ''code added by Atul Kamble on Feb 17, 2011
                Case Is = "670"  'Spring- Financial Aid Part A Detail & Summary Report
                    rptIPEDSExecutionServiceParameterValues = FinancialAidPartADetailAndSummaryReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "671"  'Spring- Financial Aid Part B Detail & Summary Report
                    rptIPEDSExecutionServiceParameterValues = FinancialAidPartBDetailAndSummaryReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "672", "812"  'Spring- Financial Aid Part C Detail & Summary Report
                    rptIPEDSExecutionServiceParameterValues = FinancialAidPartCDetailAndSummaryReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "673"  'Spring- Financial Aid Part D Detail & Summary Report
                    rptIPEDSExecutionServiceParameterValues = FinancialAidPartDDetailAndSummaryReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "674"  'Spring- Financial Aid Part E Detail & Summary Report
                    rptIPEDSExecutionServiceParameterValues = FinancialAidPartEDetailAndSummaryReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "782" 'Spring - Z Grad Rates 200 - Less than 4 Yr Inst Detail & Summary Report
                    rptIPEDSExecutionServiceParameterValues = ZGraduationRate200LessThan4YrInstDetailAndSummaryReportParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)

                Case Is = "825"
                    rptIPEDSExecutionServiceParameterValues = OutcomeMeasuresParameters(_rptParameters, _
                                                                                                      getReportParametersObj, _
                                                                                                      strSchoolName)
                Case Is = "836" 'Disability Service Report
                    rptIPEDSExecutionServiceParameterValues = DisabilityServiceReportParameters(_rptParameters, _
                                                                                                    getReportParametersObj, _
                                                                                                    strSchoolName)

            End Select
            Return rptIPEDSExecutionServiceParameterValues
        End Function
#End Region

#Region "DisplayReport"
        'While designing the report in BIDS make sure the following rule of thumb is followed
        ' (BodyWidth+LeftMargin+RightMargin) <= (PageWidth).
        'The PageWidth, LeftMargin and RightMargin can be set by selecting report - properties.
        'If the rule is not followed, the table will be truncated or blank pages will be inserted
        Private Sub DisplayReport(ByVal byteReportOutput As [Byte]())
            'Pdf
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.ContentType = _strMimeType
            HttpContext.Current.Response.BinaryWrite(byteReportOutput)

            'CSV
            'Dim name As String = "AdvReport"
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.ContentType = _strMimeType
            'HttpContext.Current.Response.AddHeader("content-disposition", ("attachment; filename=" & name & ".") + _strExtension)
            'HttpContext.Current.Response.BinaryWrite(byteReportOutput)

        End Sub
#End Region

#Region "Winter IPEDS"
        Private Function FallEnrollmentPartB4DetailReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                'This is a special case for 12 Month Enrollment Reports
                'The Start Date is between September 2
                If getReportParametersObj.ResourceId.ToString = "641" Or getReportParametersObj.ResourceId.ToString = "642" Then
                    intReportYear = CInt(CohortYear)
                    If SchoolType.ToString.Trim.ToLower = "program" Then
                        'Program Type Schools - date range is always between 08/1/cohortyear and 10/31/cohortyear
                        .StartDate = "08/01/" + CohortYear.ToString.Trim
                        .EndDate = "10/31/" + CohortYear.ToString.Trim
                        _strDateRangeText = "AUGUST 01, " + CohortYear.ToString.Trim + " THROUGH OCTOBER 31, " + CohortYear.ToString.Trim
                    Else
                        'Academic schools date range is always between 10/15/cohortyear and 10/15/nextyear
                        dtStartDate = CDate(OfficialReportingDate) 'DateAdd(DateInterval.Day, -364, CDate(OfficialReportingDate)) 'DateAdd(DateInterval.Year, -1, CDate(OfficialReportingDate))
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = (Month(dtStartDate).ToString & "/" & Day(dtStartDate).ToString & "/" & Year(dtStartDate).ToString).ToString
                        .EndDate = (Month(dtEndDate).ToString & "/" & Day(dtEndDate).ToString & "/" & Year(dtEndDate).ToString).ToString
                        _strDateRangeText = "ENROLLMENT AS OF " & (MonthName(Month(dtStartDate)) & " " & Day(dtStartDate) & ", " & Year(dtStartDate))
                        '.StartDate = "10/15/" + intReportYear.ToString 'For Academic Year Schools the reports will ignore the start date
                        '.EndDate = "10/15/" + intReportYear.ToString
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "INSTITUTIONAL CHARACTERISTICS"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = "PART B - ESTIMATED FALL ENROLLMENT"

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate



                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "DateRangeText"
                rptExecutionParamValues(9).Name = "DateRangeText"
                rptExecutionParamValues(9).Value = _strDateRangeText.ToUpper

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "CampusName"
                rptExecutionParamValues(12).Name = "CampusName"
                rptExecutionParamValues(12).Value = CampusName

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "StudentIdentifier"
                rptExecutionParamValues(13).Name = "StudentIdentifier"
                rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "servername"
                rptExecutionParamValues(14).Name = "servername"
                rptExecutionParamValues(14).Value = strServerName

                'Database Name
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "databasename"
                rptExecutionParamValues(15).Name = "databasename"
                rptExecutionParamValues(15).Value = strDBName


                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "uid"
                rptExecutionParamValues(16).Name = "uid"
                rptExecutionParamValues(16).Value = strUserName

                'password
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "password"
                rptExecutionParamValues(17).Name = "password"
                rptExecutionParamValues(17).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function FallEnrollmentSpringPartBDetailSummaryReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                     ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                     ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                'This is a special case for 12 Month Enrollment Reports
                'The Start Date is between September 2
                If getReportParametersObj.ResourceId.ToString = "412" Or getReportParametersObj.ResourceId.ToString = "413" Then
                    intReportYear = CInt(CohortYear)
                    If SchoolType.ToString.Trim.ToLower = "program" Then
                        'Program Type Schools - date range is always between 08/1/cohortyear and 10/31/cohortyear
                        .StartDate = "08/01/" + CohortYear.ToString.Trim
                        .EndDate = "10/31/" + CohortYear.ToString.Trim
                        _strDateRangeText = "AUGUST 01, " + CohortYear.ToString.Trim + " THROUGH OCTOBER 31, " + CohortYear.ToString.Trim
                    Else
                        'Academic schools date range is always between 10/15/cohortyear and 10/15/nextyear
                        dtStartDate = CDate(OfficialReportingDate) 'DateAdd(DateInterval.Day, -364, CDate(OfficialReportingDate)) 'DateAdd(DateInterval.Year, -1, CDate(OfficialReportingDate))
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = (Month(dtStartDate).ToString & "/" & Day(dtStartDate).ToString & "/" & Year(dtStartDate).ToString).ToString
                        .EndDate = (Month(dtEndDate).ToString & "/" & Day(dtEndDate).ToString & "/" & Year(dtEndDate).ToString).ToString
                        _strDateRangeText = "ENROLLMENT AS OF " & (MonthName(Month(dtStartDate)) & " " & Day(dtStartDate) & ", " & Year(dtStartDate))
                        '.StartDate = "10/15/" + intReportYear.ToString 'For Academic Year Schools the reports will ignore the start date
                        '.EndDate = "10/15/" + intReportYear.ToString
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "FALL ENROLLMENT"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = "PART B - FALL ENROLLMENT BY AGE AND GENDER"

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "DateRangeText"
                rptExecutionParamValues(9).Name = "DateRangeText"
                rptExecutionParamValues(9).Value = _strDateRangeText.ToUpper

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "CampusName"
                rptExecutionParamValues(12).Name = "CampusName"
                rptExecutionParamValues(12).Value = CampusName

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "StudentIdentifier"
                rptExecutionParamValues(13).Name = "StudentIdentifier"
                rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "servername"
                rptExecutionParamValues(14).Name = "servername"
                rptExecutionParamValues(14).Value = strServerName

                'Database Name
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "databasename"
                rptExecutionParamValues(15).Name = "databasename"
                rptExecutionParamValues(15).Value = strDBName


                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "uid"
                rptExecutionParamValues(16).Name = "uid"
                rptExecutionParamValues(16).Value = strUserName

                'password
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "password"
                rptExecutionParamValues(17).Name = "password"
                rptExecutionParamValues(17).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function FallEnrollmentSpringPartCDetailSummaryReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                     ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                     ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                'This is a special case for 12 Month Enrollment Reports
                'The Start Date is between September 2
                If getReportParametersObj.ResourceId.ToString = "414" Or getReportParametersObj.ResourceId.ToString = "415" Then
                    intReportYear = CInt(CohortYear)
                    If SchoolType.ToString.Trim.ToLower = "program" Then
                        'Program Type Schools - date range is always between 08/1/cohortyear and 10/31/cohortyear
                        .StartDate = "08/01/" + CohortYear.ToString.Trim
                        .EndDate = "10/31/" + CohortYear.ToString.Trim
                        _strDateRangeText = "AUGUST 01, " + CohortYear.ToString.Trim + " THROUGH OCTOBER 31, " + CohortYear.ToString.Trim
                    Else
                        'Academic schools date range is always between 10/15/cohortyear and 10/15/nextyear
                        dtStartDate = CDate(OfficialReportingDate) 'DateAdd(DateInterval.Day, -364, CDate(OfficialReportingDate)) 'DateAdd(DateInterval.Year, -1, CDate(OfficialReportingDate))
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = (Month(dtStartDate).ToString & "/" & Day(dtStartDate).ToString & "/" & Year(dtStartDate).ToString).ToString
                        .EndDate = (Month(dtEndDate).ToString & "/" & Day(dtEndDate).ToString & "/" & Year(dtEndDate).ToString).ToString
                        _strDateRangeText = "ENROLLMENT AS OF " & (MonthName(Month(dtStartDate)) & " " & Day(dtStartDate) & ", " & Year(dtStartDate))
                        '.StartDate = "10/15/" + intReportYear.ToString 'For Academic Year Schools the reports will ignore the start date
                        '.EndDate = "10/15/" + intReportYear.ToString
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "FALL ENROLLMENT"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = "PART C - RESIDENCE OF FIRST-TIME UNDERGRADUATE STUDENTS"

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate



                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "DateRangeText"
                rptExecutionParamValues(9).Name = "DateRangeText"
                rptExecutionParamValues(9).Value = _strDateRangeText.ToUpper

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "CampusName"
                rptExecutionParamValues(12).Name = "CampusName"
                rptExecutionParamValues(12).Value = CampusName

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "StudentIdentifier"
                rptExecutionParamValues(13).Name = "StudentIdentifier"
                rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "servername"
                rptExecutionParamValues(14).Name = "servername"
                rptExecutionParamValues(14).Value = strServerName

                'Database Name
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "databasename"
                rptExecutionParamValues(15).Name = "databasename"
                rptExecutionParamValues(15).Value = strDBName


                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "uid"
                rptExecutionParamValues(16).Name = "uid"
                rptExecutionParamValues(16).Value = strUserName

                'password
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "password"
                rptExecutionParamValues(17).Name = "password"
                rptExecutionParamValues(17).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function FallEnrollmentSpringPartDDetailSummaryReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                     ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                     ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                'This is a special case for 12 Month Enrollment Reports
                'The Start Date is between September 2
                If getReportParametersObj.ResourceId.ToString = "416" Then
                    intReportYear = CInt(CohortYear)
                    If SchoolType.ToString.Trim.ToLower = "program" Then
                        'Program Type Schools - date range is always between 08/1/cohortyear and 10/31/cohortyear
                        .StartDate = "08/01/" + CohortYear.ToString.Trim
                        .EndDate = "10/31/" + CohortYear.ToString.Trim
                        _strDateRangeText = "AUGUST 01, " + CohortYear.ToString.Trim + " THROUGH OCTOBER 31, " + CohortYear.ToString.Trim
                    Else
                        'Academic schools date range is always between 10/15/cohortyear and 10/15/nextyear
                        dtStartDate = CDate(OfficialReportingDate) 'DateAdd(DateInterval.Day, -364, CDate(OfficialReportingDate)) 'DateAdd(DateInterval.Year, -1, CDate(OfficialReportingDate))
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = (Month(dtStartDate).ToString & "/" & Day(dtStartDate).ToString & "/" & Year(dtStartDate).ToString).ToString
                        .EndDate = (Month(dtEndDate).ToString & "/" & Day(dtEndDate).ToString & "/" & Year(dtEndDate).ToString).ToString
                        _strDateRangeText = "ENROLLMENT AS OF " & (MonthName(Month(dtStartDate)) & " " & Day(dtStartDate) & ", " & Year(dtStartDate))
                        '.StartDate = "10/15/" + intReportYear.ToString 'For Academic Year Schools the reports will ignore the start date
                        '.EndDate = "10/15/" + intReportYear.ToString
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "FALL ENROLLMENT"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = "PART D - TOTAL ENTERING CLASS"

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate



                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "DateRangeText"
                rptExecutionParamValues(9).Name = "DateRangeText"
                rptExecutionParamValues(9).Value = _strDateRangeText.ToUpper

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "CampusName"
                rptExecutionParamValues(12).Name = "CampusName"
                rptExecutionParamValues(12).Value = CampusName

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "StudentIdentifier"
                rptExecutionParamValues(13).Name = "StudentIdentifier"
                rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "servername"
                rptExecutionParamValues(14).Name = "servername"
                rptExecutionParamValues(14).Value = strServerName

                'Database Name
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "databasename"
                rptExecutionParamValues(15).Name = "databasename"
                rptExecutionParamValues(15).Value = strDBName


                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "uid"
                rptExecutionParamValues(16).Name = "uid"
                rptExecutionParamValues(16).Value = strUserName

                'password
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "password"
                rptExecutionParamValues(17).Name = "password"
                rptExecutionParamValues(17).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function FallEnrollmentSpringPartEDetailSummaryReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                     ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                     ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                'This is a special case for 12 Month Enrollment Reports
                'The Start Date is between September 2
                If getReportParametersObj.ResourceId.ToString = "417" Then
                    intReportYear = CInt(CohortYear)
                    If SchoolType.ToString.Trim.ToLower = "program" Then
                        'Program Type Schools - date range is always between 08/1/cohortyear and 10/31/cohortyear
                        .StartDate = "08/01/" + CohortYear.ToString.Trim
                        .EndDate = "10/31/" + CohortYear.ToString.Trim
                        _strDateRangeText = "AUGUST 01, " + CohortYear.ToString.Trim + " THROUGH OCTOBER 31, " + CohortYear.ToString.Trim
                    Else
                        'Academic schools date range is always between 10/15/cohortyear and 10/15/nextyear
                        dtStartDate = CDate(OfficialReportingDate) 'DateAdd(DateInterval.Day, -364, CDate(OfficialReportingDate)) 'DateAdd(DateInterval.Year, -1, CDate(OfficialReportingDate))
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = (Month(dtStartDate).ToString & "/" & Day(dtStartDate).ToString & "/" & Year(dtStartDate).ToString).ToString
                        .EndDate = (Month(dtEndDate).ToString & "/" & Day(dtEndDate).ToString & "/" & Year(dtEndDate).ToString).ToString
                        _strDateRangeText = "ENROLLMENT AS OF " & (MonthName(Month(dtStartDate)) & " " & Day(dtStartDate) & ", " & Year(dtStartDate))
                        '.StartDate = "10/15/" + intReportYear.ToString 'For Academic Year Schools the reports will ignore the start date
                        '.EndDate = "10/15/" + intReportYear.ToString
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "FALL ENROLLMENT"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = "PART E - RETENTION RATES"

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate



                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "DateRangeText"
                rptExecutionParamValues(9).Name = "DateRangeText"
                rptExecutionParamValues(9).Value = _strDateRangeText.ToUpper

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "CampusName"
                rptExecutionParamValues(12).Name = "CampusName"
                rptExecutionParamValues(12).Value = CampusName

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "StudentIdentifier"
                rptExecutionParamValues(13).Name = "StudentIdentifier"
                rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "servername"
                rptExecutionParamValues(14).Name = "servername"
                rptExecutionParamValues(14).Value = strServerName

                'Database Name
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "databasename"
                rptExecutionParamValues(15).Name = "databasename"
                rptExecutionParamValues(15).Value = strDBName


                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "uid"
                rptExecutionParamValues(16).Name = "uid"
                rptExecutionParamValues(16).Value = strUserName

                'password
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "password"
                rptExecutionParamValues(17).Name = "password"
                rptExecutionParamValues(17).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function FinancialAllInstReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                     ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                     ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            ''  Dim dtStartDate As Date
            '' Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .OfficialReportingDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                'This is a special case for 12 Month Enrollment Reports
                'The Start Date is between September 2
                If getReportParametersObj.ResourceId.ToString = "421" Then
                    intReportYear = CInt(CohortYear)
                    .StartDate = DateAdd(DateInterval.Day, 1, DateAdd(DateInterval.Year, -1, CDate(EndDate))).ToString
                    .EndDate = CDate(EndDate).ToString
                    _strDateRangeText = "Date Range"
                    'If SchoolType.ToString.Trim.ToLower = "program" Then
                    '    'Program Type Schools - date range is always between 08/1/cohortyear and 10/31/cohortyear
                    '    .StartDate = "08/01/" + CohortYear.ToString.Trim
                    '    .EndDate = "10/31/" + CohortYear.ToString.Trim
                    '    _strDateRangeText = "AUGUST 01, " + CohortYear.ToString.Trim + " THROUGH OCTOBER 31, " + CohortYear.ToString.Trim
                    'Else
                    '    'Academic schools date range is always between 10/15/cohortyear and 10/15/nextyear
                    '    dtStartDate = CDate(OfficialReportingDate) 'DateAdd(DateInterval.Day, -364, CDate(OfficialReportingDate)) 'DateAdd(DateInterval.Year, -1, CDate(OfficialReportingDate))
                    '    dtEndDate = CDate(OfficialReportingDate)
                    '    .StartDate = (Month(dtStartDate).ToString & "/" & Day(dtStartDate).ToString & "/" & Year(dtStartDate).ToString).ToString
                    '    .EndDate = (Month(dtEndDate).ToString & "/" & Day(dtEndDate).ToString & "/" & Year(dtEndDate).ToString).ToString
                    '    _strDateRangeText = "ENROLLMENT AS OF " & (MonthName(Month(dtStartDate)) & " " & Day(dtStartDate) & ", " & Year(dtStartDate))
                    '    '.StartDate = "10/15/" + intReportYear.ToString 'For Academic Year Schools the reports will ignore the start date
                    '    '.EndDate = "10/15/" + intReportYear.ToString
                    'End If
                End If
                StartDate = .StartDate
                ' EndDate = EndDate - NZ 11/19 assignment has no effect
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                'CampusId Parameter
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CampusId"
                rptExecutionParamValues(0).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(0).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(0).Value = Nothing
                End If

                'Start Date
                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "StartDate"
                rptExecutionParamValues(1).Name = "StartDate"
                rptExecutionParamValues(1).Value = StartDate

                'End Date
                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "EndDate"
                rptExecutionParamValues(2).Name = "EndDate"
                rptExecutionParamValues(2).Value = EndDate

                'Order By
                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "OrderBy"
                rptExecutionParamValues(3).Name = "OrderBy"
                rptExecutionParamValues(3).Value = SortFields.ToString

                'ResourceId
                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ResourceId"
                rptExecutionParamValues(4).Name = "ResourceId"
                rptExecutionParamValues(4).Value = getReportParametersObj.ResourceId.ToString

                'Campus Name
                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "CampusName"
                rptExecutionParamValues(5).Name = "CampusName"
                rptExecutionParamValues(5).Value = CampusName

                'School Name
                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "SchoolName"
                rptExecutionParamValues(6).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(6).Value = strSchoolName
                Else
                    rptExecutionParamValues(6).Value = Nothing
                End If

                'Server Name
                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "ServerName"
                rptExecutionParamValues(7).Name = "ServerName"
                rptExecutionParamValues(7).Value = strServerName

                'Database Name
                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "DBName"
                rptExecutionParamValues(8).Name = "DBName"
                rptExecutionParamValues(8).Value = strDBName

                'User Name
                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "UserName"
                rptExecutionParamValues(9).Name = "UserName"
                rptExecutionParamValues(9).Value = strUserName

                'password
                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "Password"
                rptExecutionParamValues(10).Name = "Password"
                rptExecutionParamValues(10).Value = strPassword

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "StudentIdentifier"
                rptExecutionParamValues(11).Name = "StudentIdentifier"
                rptExecutionParamValues(11).Value = StudentIdentifier
            End If
            Return rptExecutionParamValues
        End Function
#End Region

#Region "Spring IPEDS"
        Private Function GraduationRate2YrInstDetailReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim CohortType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            '' Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                If .SchoolType = "program" Then CohortType = "full year" Else CohortType = "fall"
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear

                If getReportParametersObj.ResourceId.ToString = "659" Then
                    intReportYear = CInt(CohortYear)
                    If CohortType.ToString.Trim.ToLower = "full year" Then
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = "08/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)
                        ' _strDateRangeText = "This report uses a Full Year Cohort: students enrolled during the period 09/01/" + CStr(CInt(CohortYear.ToString.Trim)) + " through " + "08/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)
                        '_strDateRangeTextPrevious = "This report shows the status of these students as of 08/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)
                    Else
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = Month(dtEndDate).ToString.Trim + "/" + Day(dtEndDate).ToString.Trim + "/" + CohortYear.ToString.Trim
                        .EndDate = Month(dtEndDate).ToString.Trim + "/" + Day(dtEndDate).ToString.Trim + "/" + CohortYear.ToString.Trim
                        '_strDateRangeText = "This report uses a Fall Cohort: students enrolled as of " + CStr(.EndDate)
                        '_strDateRangeTextPrevious = "This report shows the status of these students as of 08/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "GRADUATION RATES"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = ""

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "CohortPossible"
                rptExecutionParamValues(9).Name = "CohortPossible"
                rptExecutionParamValues(9).Value = CohortType

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "CampusName"
                rptExecutionParamValues(12).Name = "CampusName"
                rptExecutionParamValues(12).Value = CampusName

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "StudentIdentifier"
                rptExecutionParamValues(13).Name = "StudentIdentifier"
                rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "ServerName"
                rptExecutionParamValues(14).Name = "ServerName"
                rptExecutionParamValues(14).Value = strServerName

                'Database Name
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "DBName"
                rptExecutionParamValues(15).Name = "DBName"
                rptExecutionParamValues(15).Value = strDBName


                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "UserName"
                rptExecutionParamValues(16).Name = "UserName"
                rptExecutionParamValues(16).Value = strUserName

                'password
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "Password"
                rptExecutionParamValues(17).Name = "Password"
                rptExecutionParamValues(17).Value = strPassword

                'Date Range Text
                'rptExecutionParamValues(18) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(18).Label = "DateRangeText"
                'rptExecutionParamValues(18).Name = "DateRangeText"
                'rptExecutionParamValues(18).Value = _strDateRangeText

                ''Date Range Text Previous
                'rptExecutionParamValues(19) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(19).Label = "DateRangeTextPrevious"
                'rptExecutionParamValues(19).Name = "DateRangeTextPrevious"
                'rptExecutionParamValues(19).Value = _strDateRangeTextPrevious
            End If
            Return rptExecutionParamValues
        End Function
        Private Function GraduationRate2YrInstSummaryReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim CohortType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            '' Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                If .SchoolType = "program" Then CohortType = "full year" Else CohortType = "fall"
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear

                If getReportParametersObj.ResourceId.ToString = "660" Then
                    intReportYear = CInt(CohortYear)
                    'If CohortType.ToString.Trim.ToLower = "Full Year" Then
                    '    .StartDate = "08/01/" + CohortYear.ToString.Trim
                    '    .EndDate = "10/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)
                    'Else
                    '    dtEndDate = CDate(OfficialReportingDate)
                    '    .StartDate = "09/01/" + CohortYear.ToString.Trim
                    '    .EndDate = dtEndDate.ToString("MM/dd/yyyy")
                    'End If
                    If CohortType.ToString.Trim.ToLower = "full year" Then
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = "08/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)
                        ' _strDateRangeText = "This report uses a Full Year Cohort: students enrolled during the period 09/01/" + CStr(CInt(CohortYear.ToString.Trim)) + " through " + "08/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)
                        ' _strDateRangeTextPrevious = "This report shows the status of these students as of 08/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)

                    Else
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = dtEndDate.ToString("MM/dd/yyyy")
                        '_strDateRangeText = "This report uses a Fall Cohort: students enrolled as of " + CStr(.EndDate)
                        '_strDateRangeTextPrevious = "This report shows the status of these students as of 08/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "GRADUATION RATES"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = ""

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "CohortPossible"
                rptExecutionParamValues(9).Name = "CohortPossible"
                rptExecutionParamValues(9).Value = CohortType

                'rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(10).Label = "ShowSSN"
                'rptExecutionParamValues(10).Name = "ShowSSN"
                'If boolShowSSN = True Then
                '    rptExecutionParamValues(10).Value = "true"
                'Else
                '    rptExecutionParamValues(10).Value = "false"
                'End If

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "OrderBy"
                rptExecutionParamValues(10).Name = "OrderBy"
                rptExecutionParamValues(10).Value = SortFields.ToString

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "CampusName"
                rptExecutionParamValues(11).Name = "CampusName"
                rptExecutionParamValues(11).Value = CampusName

                'rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(13).Label = "StudentIdentifier"
                'rptExecutionParamValues(13).Name = "StudentIdentifier"
                'rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "ServerName"
                rptExecutionParamValues(12).Name = "ServerName"
                rptExecutionParamValues(12).Value = strServerName

                'Database Name
                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "DBName"
                rptExecutionParamValues(13).Name = "DBName"
                rptExecutionParamValues(13).Value = strDBName


                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "UserName"
                rptExecutionParamValues(14).Name = "UserName"
                rptExecutionParamValues(14).Value = strUserName

                'password
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "Password"
                rptExecutionParamValues(15).Name = "Password"
                rptExecutionParamValues(15).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function GraduationRate4YrInstSectionIDetailReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim CohortType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            '' Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                If .SchoolType = "program" Then CohortType = "full year" Else CohortType = "fall"
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear

                If getReportParametersObj.ResourceId.ToString = "661" Then
                    intReportYear = CInt(CohortYear)
                    If CohortType.ToString.Trim.ToLower = "Full Year" Then
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = "08/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)
                    Else
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = dtEndDate.ToString("MM/dd/yyyy")
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "GRADUATION RATES"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = ""

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "CohortPossible"
                rptExecutionParamValues(9).Name = "CohortPossible"
                rptExecutionParamValues(9).Value = CohortType

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "CampusName"
                rptExecutionParamValues(12).Name = "CampusName"
                rptExecutionParamValues(12).Value = CampusName

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "StudentIdentifier"
                rptExecutionParamValues(13).Name = "StudentIdentifier"
                rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "ServerName"
                rptExecutionParamValues(14).Name = "ServerName"
                rptExecutionParamValues(14).Value = strServerName

                'Database Name
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "DBName"
                rptExecutionParamValues(15).Name = "DBName"
                rptExecutionParamValues(15).Value = strDBName


                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "UserName"
                rptExecutionParamValues(16).Name = "UserName"
                rptExecutionParamValues(16).Value = strUserName

                'password
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "Password"
                rptExecutionParamValues(17).Name = "Password"
                rptExecutionParamValues(17).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function GraduationRate4YrInstSectionISummaryReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim CohortType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            '' Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                If .SchoolType = "program" Then CohortType = "full year" Else CohortType = "fall"
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear

                If getReportParametersObj.ResourceId.ToString = "662" Then
                    intReportYear = CInt(CohortYear)
                    If CohortType.ToString.Trim.ToLower = "Full Year" Then
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = "08/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)
                    Else
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = dtEndDate.ToString("MM/dd/yyyy")
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "GRADUATION RATES"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = ""

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "CohortPossible"
                rptExecutionParamValues(9).Name = "CohortPossible"
                rptExecutionParamValues(9).Value = CohortType

                'rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(10).Label = "ShowSSN"
                'rptExecutionParamValues(10).Name = "ShowSSN"
                'If boolShowSSN = True Then
                '    rptExecutionParamValues(10).Value = "true"
                'Else
                '    rptExecutionParamValues(10).Value = "false"
                'End If

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "OrderBy"
                rptExecutionParamValues(10).Name = "OrderBy"
                rptExecutionParamValues(10).Value = SortFields.ToString

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "CampusName"
                rptExecutionParamValues(11).Name = "CampusName"
                rptExecutionParamValues(11).Value = CampusName

                'rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(13).Label = "StudentIdentifier"
                'rptExecutionParamValues(13).Name = "StudentIdentifier"
                'rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "ServerName"
                rptExecutionParamValues(12).Name = "ServerName"
                rptExecutionParamValues(12).Value = strServerName

                'Database Name
                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "DBName"
                rptExecutionParamValues(13).Name = "DBName"
                rptExecutionParamValues(13).Value = strDBName


                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "UserName"
                rptExecutionParamValues(14).Name = "UserName"
                rptExecutionParamValues(14).Value = strUserName

                'password
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "Password"
                rptExecutionParamValues(15).Name = "Password"
                rptExecutionParamValues(15).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function GraduationRate4YrInstSectionIIDetailReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim CohortType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            ''    Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                If .SchoolType = "program" Then CohortType = "full year" Else CohortType = "fall"
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear

                If getReportParametersObj.ResourceId.ToString = "663" Then
                    intReportYear = CInt(CohortYear)
                    If CohortType.ToString.Trim.ToLower = "Full Year" Then
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = "08/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)
                    Else
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = dtEndDate.ToString("MM/dd/yyyy")
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "GRADUATION RATES"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = ""

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "CohortPossible"
                rptExecutionParamValues(9).Name = "CohortPossible"
                rptExecutionParamValues(9).Value = CohortType

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "CampusName"
                rptExecutionParamValues(12).Name = "CampusName"
                rptExecutionParamValues(12).Value = CampusName

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "StudentIdentifier"
                rptExecutionParamValues(13).Name = "StudentIdentifier"
                rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "ServerName"
                rptExecutionParamValues(14).Name = "ServerName"
                rptExecutionParamValues(14).Value = strServerName

                'Database Name
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "DBName"
                rptExecutionParamValues(15).Name = "DBName"
                rptExecutionParamValues(15).Value = strDBName


                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "UserName"
                rptExecutionParamValues(16).Name = "UserName"
                rptExecutionParamValues(16).Value = strUserName

                'password
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "Password"
                rptExecutionParamValues(17).Name = "Password"
                rptExecutionParamValues(17).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function GraduationRate4YrInstSectionIISummaryReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim CohortType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            ' Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                If .SchoolType = "program" Then CohortType = "full year" Else CohortType = "fall"
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear

                If getReportParametersObj.ResourceId.ToString = "664" Then
                    intReportYear = CInt(CohortYear)
                    If CohortType.ToString.Trim.ToLower = "Full Year" Then
                        .StartDate = "08/01/" + CohortYear.ToString.Trim
                        .EndDate = "10/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)
                    Else
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = dtEndDate.ToString("MM/dd/yyyy")
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "GRADUATION RATES"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = ""

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "CohortPossible"
                rptExecutionParamValues(9).Name = "CohortPossible"
                rptExecutionParamValues(9).Value = CohortType

                'rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(10).Label = "ShowSSN"
                'rptExecutionParamValues(10).Name = "ShowSSN"
                'If boolShowSSN = True Then
                '    rptExecutionParamValues(10).Value = "true"
                'Else
                '    rptExecutionParamValues(10).Value = "false"
                'End If

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "OrderBy"
                rptExecutionParamValues(10).Name = "OrderBy"
                rptExecutionParamValues(10).Value = SortFields.ToString

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "CampusName"
                rptExecutionParamValues(11).Name = "CampusName"
                rptExecutionParamValues(11).Value = CampusName

                'rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(13).Label = "StudentIdentifier"
                'rptExecutionParamValues(13).Name = "StudentIdentifier"
                'rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "ServerName"
                rptExecutionParamValues(12).Name = "ServerName"
                rptExecutionParamValues(12).Value = strServerName

                'Database Name
                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "DBName"
                rptExecutionParamValues(13).Name = "DBName"
                rptExecutionParamValues(13).Value = strDBName


                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "UserName"
                rptExecutionParamValues(14).Name = "UserName"
                rptExecutionParamValues(14).Value = strUserName

                'password
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "Password"
                rptExecutionParamValues(15).Name = "Password"
                rptExecutionParamValues(15).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function GraduationRate4YrInstSectionIIIDetailReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim CohortType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            '' Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                If .SchoolType = "program" Then CohortType = "full year" Else CohortType = "fall"
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear

                If getReportParametersObj.ResourceId.ToString = "665" Then
                    intReportYear = CInt(CohortYear)
                    If CohortType.ToString.Trim.ToLower = "Full Year" Then
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = "08/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)
                    Else
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = dtEndDate.ToString("MM/dd/yyyy")
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "GRADUATION RATES"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = ""

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "CohortPossible"
                rptExecutionParamValues(9).Name = "CohortPossible"
                rptExecutionParamValues(9).Value = CohortType

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "CampusName"
                rptExecutionParamValues(12).Name = "CampusName"
                rptExecutionParamValues(12).Value = CampusName

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "StudentIdentifier"
                rptExecutionParamValues(13).Name = "StudentIdentifier"
                rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "ServerName"
                rptExecutionParamValues(14).Name = "ServerName"
                rptExecutionParamValues(14).Value = strServerName

                'Database Name
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "DBName"
                rptExecutionParamValues(15).Name = "DBName"
                rptExecutionParamValues(15).Value = strDBName


                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "UserName"
                rptExecutionParamValues(16).Name = "UserName"
                rptExecutionParamValues(16).Value = strUserName

                'password
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "Password"
                rptExecutionParamValues(17).Name = "Password"
                rptExecutionParamValues(17).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function GraduationRate4YrInstSectionIIISummaryReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim CohortType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            '' Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                If .SchoolType = "program" Then CohortType = "full year" Else CohortType = "fall"
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear

                If getReportParametersObj.ResourceId.ToString = "666" Then
                    intReportYear = CInt(CohortYear)
                    If CohortType.ToString.Trim.ToLower = "Full Year" Then
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = "08/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)
                    Else
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = dtEndDate.ToString("MM/dd/yyyy")
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "GRADUATION RATES"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = ""

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "CohortPossible"
                rptExecutionParamValues(9).Name = "CohortPossible"
                rptExecutionParamValues(9).Value = CohortType

                'rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(10).Label = "ShowSSN"
                'rptExecutionParamValues(10).Name = "ShowSSN"
                'If boolShowSSN = True Then
                '    rptExecutionParamValues(10).Value = "true"
                'Else
                '    rptExecutionParamValues(10).Value = "false"
                'End If

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "OrderBy"
                rptExecutionParamValues(10).Name = "OrderBy"
                rptExecutionParamValues(10).Value = SortFields.ToString

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "CampusName"
                rptExecutionParamValues(11).Name = "CampusName"
                rptExecutionParamValues(11).Value = CampusName

                'rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(13).Label = "StudentIdentifier"
                'rptExecutionParamValues(13).Name = "StudentIdentifier"
                'rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "ServerName"
                rptExecutionParamValues(12).Name = "ServerName"
                rptExecutionParamValues(12).Value = strServerName

                'Database Name
                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "DBName"
                rptExecutionParamValues(13).Name = "DBName"
                rptExecutionParamValues(13).Value = strDBName


                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "UserName"
                rptExecutionParamValues(14).Name = "UserName"
                rptExecutionParamValues(14).Value = strUserName

                'password
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "Password"
                rptExecutionParamValues(15).Name = "Password"
                rptExecutionParamValues(15).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
        Private Function OutcomeMeasuresParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim CohortType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            ''  Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                If .SchoolType = "full" Then CohortType = "full year" Else CohortType = "fall"
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear

                If getReportParametersObj.ResourceId.ToString = "667" Then
                    intReportYear = CInt(CohortYear)
                    If CohortType.ToString.Trim.ToLower = "full year" Then
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = "08/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)
                    Else
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = dtEndDate.ToString("MM/dd/yyyy")
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                If CohortType = "fall" Then
                    StartDate = CDate(OfficialReportingDate).ToString("MM/dd/yyyy")
                    EndDate = "10/15/" + (Year(CDate(OfficialReportingDate)) + 1).ToString()
                End If
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "DateRangeText"
                rptExecutionParamValues(5).Name = "DateRangeText"
                If CohortType = "Fall" Then
                    rptExecutionParamValues(5).Value = " as of " & EndDate
                Else
                    rptExecutionParamValues(5).Value = " between " & StartDate & " through " & EndDate
                End If
                
                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "FullOrPartTime"
                rptExecutionParamValues(6).Name = "FullOrPartTime"
                rptExecutionParamValues(6).Value = "61"

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "CohortType"
                rptExecutionParamValues(9).Name = "CohortType"
                rptExecutionParamValues(9).Value = CohortType

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "CampusName"
                rptExecutionParamValues(12).Name = "CampusName"
                rptExecutionParamValues(12).Value = CampusName

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "StudentIdentifier"
                rptExecutionParamValues(13).Name = "StudentIdentifier"
                rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "ServerName"
                rptExecutionParamValues(14).Name = "ServerName"
                rptExecutionParamValues(14).Value = strServerName

                'Database Name
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "DBName"
                rptExecutionParamValues(15).Name = "DBName"
                rptExecutionParamValues(15).Value = strDBName


                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "UserName"
                rptExecutionParamValues(16).Name = "UserName"
                rptExecutionParamValues(16).Value = strUserName

                'password
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "Password"
                rptExecutionParamValues(17).Name = "Password"
                rptExecutionParamValues(17).Value = strPassword

                rptExecutionParamValues(18) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(18).Label = "FirstTimeOrNot"
                rptExecutionParamValues(18).Name = "FirstTimeOrNot"
                rptExecutionParamValues(18).Value = "1"


            End If
            Return rptExecutionParamValues
        End Function
        Private Function GraduationRateLessThan2YrInstDetailAndSummaryReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim CohortType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            ''  Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                If .SchoolType = "program" Then CohortType = "full year" Else CohortType = "fall"
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear

                If getReportParametersObj.ResourceId.ToString = "667" Then
                    intReportYear = CInt(CohortYear)
                    If CohortType.ToString.Trim.ToLower = "Full Year" Then
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = "08/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)
                    Else
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = dtEndDate.ToString("MM/dd/yyyy")
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "GRADUATION RATES"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = ""

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "CohortPossible"
                rptExecutionParamValues(9).Name = "CohortPossible"
                rptExecutionParamValues(9).Value = CohortType

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "CampusName"
                rptExecutionParamValues(12).Name = "CampusName"
                rptExecutionParamValues(12).Value = CampusName

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "StudentIdentifier"
                rptExecutionParamValues(13).Name = "StudentIdentifier"
                rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "ServerName"
                rptExecutionParamValues(14).Name = "ServerName"
                rptExecutionParamValues(14).Value = strServerName

                'Database Name
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "DBName"
                rptExecutionParamValues(15).Name = "DBName"
                rptExecutionParamValues(15).Value = strDBName


                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "UserName"
                rptExecutionParamValues(16).Name = "UserName"
                rptExecutionParamValues(16).Value = strUserName

                'password
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "Password"
                rptExecutionParamValues(17).Name = "Password"
                rptExecutionParamValues(17).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function

        ''Financial Aid Detail And Summary reports
        Private Function FinancialAidPartADetailAndSummaryReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String
            Dim institutionType As String
            Dim largeProgramName As String
            Dim endDateAcademic As String = ""

            Dim LargeProgramId As String = ""
            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                'This is a special case for 12 Month Enrollment Reports
                'The Start Date is between September 2
                If getReportParametersObj.ResourceId.ToString = "670" Then
                    intReportYear = CInt(CohortYear)
                    If SchoolType.ToString.Trim.ToLower = "program" Then
                        'Program Type Schools - date range is always between 09/01/COHORT YEAR TO 08/31/NEXT YEAR - EXAMPLE: 09/01/2009 TO 08/31/2010
                        .StartDate = "7/01/" + CohortYear.ToString.Trim
                        .EndDate = "6/30/" + (CInt(CohortYear) + 1).ToString.Trim
                        '.StartDate = "07/01/" + (CInt(CohortYear) - 2).ToString.Trim
                        '.EndDate = "06/30/" + (CInt(CohortYear) - 1).ToString.Trim
                        _strDateRangeText = "ENROLLMENT FOR ACAD YR " & (CInt(CohortYear)).ToString.Trim & "-" & Mid((CInt(CohortYear) + 1).ToString.Trim, 3, 2) & " (BETWEEN JULY 1, " + CohortYear.ToString.Trim + " THROUGH JUNE 30, " + (CInt(CohortYear) + 1).ToString.Trim & ")"
                        largeProgramName = .LargeProgramName
                        endDateAcademic = .EndDate
                    Else
                        'Academic schools date range is always between 10/15/cohortyear and 10/15/nextyear
                        dtStartDate = CDate(OfficialReportingDate) 'DateAdd(DateInterval.Day, -364, CDate(OfficialReportingDate)) 'DateAdd(DateInterval.Year, -1, CDate(OfficialReportingDate))
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = "7/01/" + CohortYear.ToString.Trim '(Month(dtStartDate).ToString & "/" & Day(dtStartDate).ToString & "/" & Year(dtStartDate).ToString).ToString
                        .EndDate = "6/30/" + (CInt(CohortYear) + 1).ToString.Trim '(Month(dtEndDate).ToString & "/" & Day(dtEndDate).ToString & "/" & Year(dtEndDate).ToString).ToString
                        _strDateRangeText = "ENROLLMENT AS OF " & (MonthName(Month(dtStartDate)).ToUpper & " " & Day(dtStartDate) & ", " & Year(dtStartDate))
                        '.StartDate = "10/15/" + intReportYear.ToString 'For Academic Year Schools the reports will ignore the start date
                        '.EndDate = "10/15/" + intReportYear.ToString
                        largeProgramName = ""
                        endDateAcademic = dtEndDate.ToString.Trim
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
                institutionType = .InstitutionType
                LargeProgramId = .LargeProgramId
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "STUDENT FINANCIAL AID"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = ""

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "SchoolType"
                rptExecutionParamValues(9).Name = "SchoolType"
                rptExecutionParamValues(9).Value = SchoolType



                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "OrderBy"
                rptExecutionParamValues(10).Name = "OrderBy"
                rptExecutionParamValues(10).Value = SortFields.ToString

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "CampusName"
                rptExecutionParamValues(11).Name = "CampusName"
                rptExecutionParamValues(11).Value = CampusName

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "StudentIdentifier"
                rptExecutionParamValues(12).Name = "StudentIdentifier"
                rptExecutionParamValues(12).Value = StudentIdentifier

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "ServerName"
                rptExecutionParamValues(13).Name = "ServerName"
                rptExecutionParamValues(13).Value = strServerName

                'Database Name
                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "DBName"
                rptExecutionParamValues(14).Name = "DBName"
                rptExecutionParamValues(14).Value = strDBName


                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "UserName"
                rptExecutionParamValues(15).Name = "UserName"
                rptExecutionParamValues(15).Value = strUserName

                'password
                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "Password"
                rptExecutionParamValues(16).Name = "Password"
                rptExecutionParamValues(16).Value = strPassword

                'Date Range text
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "DateRangeText"
                rptExecutionParamValues(17).Name = "DateRangeText"
                rptExecutionParamValues(17).Value = _strDateRangeText

                rptExecutionParamValues(18) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(18).Label = "InstitutionType"
                rptExecutionParamValues(18).Name = "InstitutionType"
                rptExecutionParamValues(18).Value = institutionType

                rptExecutionParamValues(19) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(19).Label = "LargestProgramID"
                rptExecutionParamValues(19).Name = "LargestProgramID"
                rptExecutionParamValues(19).Value = LargeProgramId

                rptExecutionParamValues(20) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(20).Label = "LargeProgramName"
                rptExecutionParamValues(20).Name = "LargeProgramName"
                rptExecutionParamValues(20).Value = largeProgramName

                rptExecutionParamValues(21) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(21).Label = "EndDateAcademic"
                rptExecutionParamValues(21).Name = "EndDateAcademic"
                rptExecutionParamValues(21).Value = endDateAcademic
            End If
            Return rptExecutionParamValues
        End Function
        Private Function FinancialAidPartBDetailAndSummaryReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            ''    Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String
            Dim LargeProgramId As String = ""
            Dim endDateAcademic As String = ""
            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                LargeProgramId = .LargeProgramId
                'This is a special case for 12 Month Enrollment Reports
                'The Start Date is between September 2
                If getReportParametersObj.ResourceId.ToString = "671" Then
                    intReportYear = CInt(CohortYear)
                    If SchoolType.ToString.Trim.ToLower = "program" Then
                        'Program Type Schools - date range is always between 09/01/COHORT YEAR TO 08/31/NEXT YEAR - EXAMPLE: 09/01/2009 TO 08/31/2010
                        'Commented by Balaji on Jan 11, 2012
                        'ED changed the date range ONLY for Program Reporter schools.  It now matches the heading for Academic Reporters.  Therefore, for all schools this label is now:
                        'AID RECEIVED BETWEEN JULY 1, 2008 THRU JUNE 30, 2009

                        '.StartDate = "09/01/" + CohortYear.ToString.Trim
                        '.EndDate = "8/31/" + (CInt(CohortYear) + 1).ToString.Trim
                        '_strDateRangeText = "AID RECEIVED BETWEEN SEPTEMBER 1, " + CohortYear.ToString.Trim + " THROUGH AUGUST 31, " + (CInt(CohortYear) + 1).ToString.Trim

                        .StartDate = "07/01/" + CohortYear.ToString.Trim
                        dtEndDate = CDate("06/30/" + (CInt(CohortYear) + 1).ToString.Trim) 'CDate(OfficialReportingDate)
                        .EndDate = Month(dtEndDate).ToString.Trim + "/" + Day(dtEndDate).ToString.Trim + "/" + (CInt(CohortYear) + 1).ToString.Trim
                        endDateAcademic = .EndDate
                        _strDateRangeText = "AID AWARDED BETWEEN JULY 1," + CohortYear.ToString.Trim + " THROUGH " + MonthName(Month(dtEndDate)).ToUpper & " " & Day(dtEndDate) & ", " & (CInt(CohortYear) + 1).ToString.Trim
                    Else
                        'Academic schools date range is always between JULY 1ST COHORT YEAR THROUGH JUNE 30TH (COHORT YEAR+1) EXAMPLE: JULY 1ST 2009 THROUGH JUNE 30TH 2010
                        .StartDate = "07/01/" + CohortYear.ToString.Trim 'CDate(OfficialReportingDate).ToString.Trim"
                        dtEndDate = CDate(OfficialReportingDate)
                        .EndDate = Month(dtEndDate).ToString.Trim + "/" + Day(dtEndDate).ToString.Trim + "/" + Year(dtEndDate).ToString.Trim
                        endDateAcademic = "06/30/" + (CInt(CohortYear) + 1).ToString.Trim
                        _strDateRangeText = "AID AWARDED BETWEEN JULY 1," + CohortYear.ToString.Trim + " THROUGH " + MonthName(Month(dtEndDate)).ToUpper & " " & Day(dtEndDate) & ", " & (CInt(CohortYear) + 1).ToString.Trim
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "STUDENT FINANCIAL AID"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = ""

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = endDateAcademic

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "SchoolType"
                rptExecutionParamValues(9).Name = "SchoolType"
                rptExecutionParamValues(9).Value = SchoolType



                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "OrderBy"
                rptExecutionParamValues(10).Name = "OrderBy"
                rptExecutionParamValues(10).Value = SortFields.ToString

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "CampusName"
                rptExecutionParamValues(11).Name = "CampusName"
                rptExecutionParamValues(11).Value = CampusName

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "StudentIdentifier"
                rptExecutionParamValues(12).Name = "StudentIdentifier"
                rptExecutionParamValues(12).Value = StudentIdentifier

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "ServerName"
                rptExecutionParamValues(13).Name = "ServerName"
                rptExecutionParamValues(13).Value = strServerName

                'Database Name
                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "DBName"
                rptExecutionParamValues(14).Name = "DBName"
                rptExecutionParamValues(14).Value = strDBName


                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "UserName"
                rptExecutionParamValues(15).Name = "UserName"
                rptExecutionParamValues(15).Value = strUserName

                'password
                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "Password"
                rptExecutionParamValues(16).Name = "Password"
                rptExecutionParamValues(16).Value = strPassword

                'Date Range text
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "DateRangeText"
                rptExecutionParamValues(17).Name = "DateRangeText"
                rptExecutionParamValues(17).Value = _strDateRangeText

                'Date Range text
                rptExecutionParamValues(18) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(18).Label = "EndDate_Academic"
                rptExecutionParamValues(18).Name = "EndDate_Academic"
                rptExecutionParamValues(18).Value = EndDate
            End If
            Return rptExecutionParamValues
        End Function
        Private Function FinancialAidPartCDetailAndSummaryReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            ' '''   Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String
            Dim endDateAcademic As String = ""
            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                'This is a special case for 12 Month Enrollment Reports
                'The Start Date is between September 2
                If getReportParametersObj.ResourceId.ToString = "672" Or getReportParametersObj.ResourceId.ToString = "812" Then
                    intReportYear = CInt(CohortYear)
                    If SchoolType.ToString.Trim.ToLower = "program" Then
                        'Program Type Schools - date range is always between 09/01/COHORT YEAR TO 08/31/NEXT YEAR - EXAMPLE: 09/01/2009 TO 08/31/2010
                        '.StartDate = "09/01/" + CohortYear.ToString.Trim
                        '.EndDate = "8/31/" + (CInt(CohortYear) + 1).ToString.Trim
                        '_strDateRangeText = "AID RECEIVED BETWEEN SEPTEMBER 1, " + CohortYear.ToString.Trim + " THROUGH AUGUST 31, " + (CInt(CohortYear) + 1).ToString.Trim

                        'Program Reporter schools date range is modified to be between JULY 1ST COHORT YEAR THROUGH JUNE 30TH (COHORT YEAR+1) EXAMPLE: JULY 1ST 2009 THROUGH JUNE 30TH 2010
                        .StartDate = "07/01/" + CohortYear.ToString.Trim
                        'dtEndDate = CDate(OfficialReportingDate)
                        dtEndDate = CDate("06/30/" + (CInt(CohortYear) + 1).ToString.Trim) 'CDate(OfficialReportingDate)
                        .EndDate = Month(dtEndDate).ToString.Trim + "/" + Day(dtEndDate).ToString.Trim + "/" + (CInt(CohortYear) + 1).ToString.Trim
                        endDateAcademic = .EndDate
                        _strDateRangeText = "AID RECEIVED BETWEEN JULY 1," + CohortYear.ToString.Trim + " THROUGH " + MonthName(Month(dtEndDate)).ToUpper & " " & Day(dtEndDate) & ", " & (CInt(CohortYear) + 1).ToString.Trim
                    Else
                        'Academic schools date range is always between JULY 1ST COHORT YEAR THROUGH JUNE 30TH (COHORT YEAR+1) EXAMPLE: JULY 1ST 2009 THROUGH JUNE 30TH 2010
                        '.StartDate = "07/01/" + CohortYear.ToString.Trim
                        .StartDate = "07/01/" + CohortYear.ToString.Trim 'CDate(OfficialReportingDate).ToString.Trim
                        dtEndDate = CDate(OfficialReportingDate)
                        .EndDate = Month(dtEndDate).ToString.Trim + "/" + Day(dtEndDate).ToString.Trim + "/" + Year(dtEndDate).ToString.Trim
                        endDateAcademic = "06/30/" + (CInt(CohortYear) + 1).ToString.Trim
                        _strDateRangeText = "AID RECEIVED BETWEEN JULY 1," + CohortYear.ToString.Trim + " THROUGH " + MonthName(Month(dtEndDate)).ToUpper & " " & Day(dtEndDate) & ", " & (CInt(CohortYear) + 1).ToString.Trim
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "STUDENT FINANCIAL AID"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = ""

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = endDateAcademic

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "SchoolType"
                rptExecutionParamValues(9).Name = "SchoolType"
                rptExecutionParamValues(9).Value = SchoolType



                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "OrderBy"
                rptExecutionParamValues(10).Name = "OrderBy"
                rptExecutionParamValues(10).Value = SortFields.ToString

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "CampusName"
                rptExecutionParamValues(11).Name = "CampusName"
                rptExecutionParamValues(11).Value = CampusName

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "StudentIdentifier"
                rptExecutionParamValues(12).Name = "StudentIdentifier"
                rptExecutionParamValues(12).Value = StudentIdentifier

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "ServerName"
                rptExecutionParamValues(13).Name = "ServerName"
                rptExecutionParamValues(13).Value = strServerName

                'Database Name
                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "DBName"
                rptExecutionParamValues(14).Name = "DBName"
                rptExecutionParamValues(14).Value = strDBName


                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "UserName"
                rptExecutionParamValues(15).Name = "UserName"
                rptExecutionParamValues(15).Value = strUserName

                'password
                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "Password"
                rptExecutionParamValues(16).Name = "Password"
                rptExecutionParamValues(16).Value = strPassword

                'Date Range text
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "DateRangeText"
                rptExecutionParamValues(17).Name = "DateRangeText"
                rptExecutionParamValues(17).Value = _strDateRangeText

                rptExecutionParamValues(18) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(18).Label = "EndDate_Academic"
                rptExecutionParamValues(18).Name = "EndDate_Academic"
                rptExecutionParamValues(18).Value = EndDate
            End If
            Return rptExecutionParamValues
        End Function
        Private Function FinancialAidPartDDetailAndSummaryReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            ''  Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String
            Dim dtStartDate2 As String = ""
            Dim dtStartDate3 As String = ""
            Dim dtEndDate2 As String = ""
            Dim dtEndDate3 As String = ""
            Dim institutionType As String
            Dim endDateAcademic As String = ""
            Dim largeProgramId As String = ""
            Dim EndDate_Academic_YearMinus1 As String = ""
            Dim EndDate_Academic_YearMinus2 As String = ""

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                'This is a special case for 12 Month Enrollment Reports
                'The Start Date is between September 2
                If getReportParametersObj.ResourceId.ToString = "673" Then
                    intReportYear = CInt(CohortYear)
                    If SchoolType.ToString.Trim.ToLower = "program" Then
                        'Program Type Schools - date range is always between 09/01/COHORT YEAR TO 08/31/NEXT YEAR - EXAMPLE: 09/01/2009 TO 08/31/2010
                        '.StartDate = "09/01/" + CohortYear.ToString.Trim
                        '.EndDate = "8/31/" + (CInt(CohortYear) + 1).ToString.Trim

                        'dtStartDate2 = "09/01/" + (CInt(CohortYear) - 1).ToString.Trim
                        'dtStartDate3 = "09/01/" + (CInt(CohortYear) - 2).ToString.Trim

                        'dtEndDate2 = "08/31/" + (CInt(CohortYear)).ToString.Trim
                        'dtEndDate3 = "08/31/" + (CInt(CohortYear) - 1).ToString.Trim

                        '_strDateRangeText = "AID RECEIVED BETWEEN SEPTEMBER 1, " + CohortYear.ToString.Trim + " THROUGH AUGUST 31, " + (CInt(CohortYear) + 1).ToString.Trim
                        '_strDateRangeTextPrevious = "AID RECEIVED BETWEEN SEPTEMBER 1, " + (CInt(CohortYear) - 1).ToString.Trim + " THROUGH AUGUST 31, " + CohortYear.ToString.Trim
                        '_strDateRangeText2YrPrevious = "AID RECEIVED BETWEEN SEPTEMBER 1, " + (CInt(CohortYear) - 2).ToString.Trim + " THROUGH AUGUST 31, " + (CInt(CohortYear) - 1).ToString.Trim


                        .StartDate = "07/01/" + CohortYear.ToString.Trim
                        .EndDate = "6/30/" + (CInt(CohortYear) + 1).ToString.Trim

                        endDateAcademic = .EndDate

                        dtStartDate2 = "07/01/" + (CInt(CohortYear) - 1).ToString.Trim
                        dtStartDate3 = "07/01/" + (CInt(CohortYear) - 2).ToString.Trim

                        dtEndDate2 = "06/30/" + (CInt(CohortYear)).ToString.Trim
                        dtEndDate3 = "06/30/" + (CInt(CohortYear) - 1).ToString.Trim

                        EndDate_Academic_YearMinus1 = dtEndDate2
                        EndDate_Academic_YearMinus2 = dtEndDate3

                        _strDateRangeText = "AID AWARDED BETWEEN JULY 1, " + CohortYear.ToString.Trim + " THROUGH JUNE 30, " + (CInt(CohortYear) + 1).ToString.Trim
                        _strDateRangeTextPrevious = "AID AWARDED BETWEEN JULY 1, " + (CInt(CohortYear) - 1).ToString.Trim + " THROUGH JUNE 30, " + CohortYear.ToString.Trim
                        _strDateRangeText2YrPrevious = "AID AWARDED BETWEEN JULY 1, " + (CInt(CohortYear) - 2).ToString.Trim + " THROUGH JUNE 30, " + (CInt(CohortYear) - 1).ToString.Trim
                    Else
                        'Academic schools date range is always between JULY 1ST COHORT YEAR THROUGH JUNE 30TH (COHORT YEAR+1) EXAMPLE: JULY 1ST 2009 THROUGH JUNE 30TH 2010
                        '.StartDate = "07/01/" + CohortYear.ToString.Trim
                        .StartDate = "07/01/" + CohortYear.ToString.Trim 'CDate(OfficialReportingDate).ToString.Trim
                        dtEndDate = CDate(OfficialReportingDate)
                        .EndDate = Month(dtEndDate).ToString.Trim + "/" + Day(dtEndDate).ToString.Trim + "/" + Year(dtEndDate).ToString.Trim

                        dtStartDate2 = "07/01/" + (CInt(CohortYear) - 1).ToString.Trim 'Month(CDate(OfficialReportingDate)).ToString.Trim + "/" + Day(CDate(OfficialReportingDate)).ToString.Trim + "/" + (CInt(CohortYear) - 1).ToString.Trim '"07/01/" + (CInt(CohortYear) - 1).ToString.Trim
                        dtStartDate3 = "07/01/" + (CInt(CohortYear) - 2).ToString.Trim 'Month(CDate(OfficialReportingDate)).ToString.Trim + "/" + Day(CDate(OfficialReportingDate)).ToString.Trim + "/" + (CInt(CohortYear) - 2).ToString.Trim

                        dtEndDate2 = Month(dtEndDate).ToString.Trim + "/" + Day(dtEndDate).ToString.Trim + "/" + (CInt(CohortYear) - 1).ToString.Trim
                        dtEndDate3 = Month(dtEndDate).ToString.Trim + "/" + Day(dtEndDate).ToString.Trim + "/" + (CInt(CohortYear) - 2).ToString.Trim

                        endDateAcademic = "06/30/" + (CInt(CohortYear) + 1).ToString.Trim
                        EndDate_Academic_YearMinus1 = "06/30/" + (CInt(CohortYear)).ToString.Trim
                        EndDate_Academic_YearMinus2 = "06/30/" + (CInt(CohortYear) - 1).ToString.Trim

                        '_strDateRangeText = "AID AWARDED BETWEEN JULY 1," + CohortYear.ToString.Trim + " THROUGH " + MonthName(Month(dtEndDate)).ToUpper & " " & Day(dtEndDate) & ", " & (CInt(CohortYear) + 1).ToString.Trim
                        '_strDateRangeTextPrevious = "AID AWARDED BETWEEN JULY 1, " + (CInt(CohortYear) - 1).ToString.Trim + " THROUGH " + MonthName(Month(dtEndDate)).ToUpper & " " & Day(dtEndDate) & ", " & CohortYear.ToString.Trim
                        '_strDateRangeText2YrPrevious = "AID AWARDED BETWEEN JULY 1, " + (CInt(CohortYear) - 2).ToString.Trim + " THROUGH " + MonthName(Month(dtEndDate)).ToUpper & " " & Day(dtEndDate) & ", " & (CInt(CohortYear) - 1).ToString.Trim

                        _strDateRangeText = "AID AWARDED BETWEEN JULY 1, " + CohortYear.ToString.Trim + " THROUGH JUNE 30, " + (CInt(CohortYear) + 1).ToString.Trim
                        _strDateRangeTextPrevious = "AID AWARDED BETWEEN JULY 1, " + (CInt(CohortYear) - 1).ToString.Trim + " THROUGH JUNE 30, " + CohortYear.ToString.Trim
                        _strDateRangeText2YrPrevious = "AID AWARDED BETWEEN JULY 1, " + (CInt(CohortYear) - 2).ToString.Trim + " THROUGH JUNE 30, " + (CInt(CohortYear) - 1).ToString.Trim
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
                institutionType = .InstitutionType
                largeProgramId = .LargeProgramId
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "STUDENT FINANCIAL AID"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = ""

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = endDateAcademic

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "SchoolType"
                rptExecutionParamValues(9).Name = "SchoolType"
                rptExecutionParamValues(9).Value = SchoolType



                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "OrderBy"
                rptExecutionParamValues(10).Name = "OrderBy"
                rptExecutionParamValues(10).Value = SortFields.ToString

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "CampusName"
                rptExecutionParamValues(11).Name = "CampusName"
                rptExecutionParamValues(11).Value = CampusName

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "StudentIdentifier"
                rptExecutionParamValues(12).Name = "StudentIdentifier"
                rptExecutionParamValues(12).Value = StudentIdentifier

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "ServerName"
                rptExecutionParamValues(13).Name = "ServerName"
                rptExecutionParamValues(13).Value = strServerName

                'Database Name
                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "DBName"
                rptExecutionParamValues(14).Name = "DBName"
                rptExecutionParamValues(14).Value = strDBName


                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "UserName"
                rptExecutionParamValues(15).Name = "UserName"
                rptExecutionParamValues(15).Value = strUserName

                'password
                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "Password"
                rptExecutionParamValues(16).Name = "Password"
                rptExecutionParamValues(16).Value = strPassword

                'Date Range text
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "DateRangeText"
                rptExecutionParamValues(17).Name = "DateRangeText"
                rptExecutionParamValues(17).Value = _strDateRangeText

                'Date Range text
                rptExecutionParamValues(18) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(18).Label = "DateRangeTextPrevious"
                rptExecutionParamValues(18).Name = "DateRangeTextPrevious"
                rptExecutionParamValues(18).Value = _strDateRangeTextPrevious

                'Date Range text
                rptExecutionParamValues(19) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(19).Label = "DateRangeText2YrPrevious"
                rptExecutionParamValues(19).Name = "DateRangeText2YrPrevious"
                rptExecutionParamValues(19).Value = _strDateRangeText2YrPrevious

                'StartDate - Grid2
                rptExecutionParamValues(20) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(20).Label = "StartDate2"
                rptExecutionParamValues(20).Name = "StartDate2"
                rptExecutionParamValues(20).Value = dtStartDate2

                'EndDate - Grid2
                rptExecutionParamValues(21) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(21).Label = "EndDate2"
                rptExecutionParamValues(21).Name = "EndDate2"
                rptExecutionParamValues(21).Value = EndDate_Academic_YearMinus1 'dtEndDate2

                'StartDate - Grid3
                rptExecutionParamValues(22) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(22).Label = "StartDate3"
                rptExecutionParamValues(22).Name = "StartDate3"
                rptExecutionParamValues(22).Value = dtStartDate3

                'EndDate - Grid3
                rptExecutionParamValues(23) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(23).Label = "EndDate3"
                rptExecutionParamValues(23).Name = "EndDate3"
                rptExecutionParamValues(23).Value = EndDate_Academic_YearMinus2 'dtEndDate3

                rptExecutionParamValues(24) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(24).Label = "InstitutionType"
                rptExecutionParamValues(24).Name = "InstitutionType"
                rptExecutionParamValues(24).Value = institutionType

                rptExecutionParamValues(25) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(25).Label = "LargestProgramID"
                rptExecutionParamValues(25).Name = "LargestProgramID"
                rptExecutionParamValues(25).Value = largeProgramId

                rptExecutionParamValues(26) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(26).Label = "EndDate_Academic"
                rptExecutionParamValues(26).Name = "EndDate_Academic"
                rptExecutionParamValues(26).Value = EndDate

                rptExecutionParamValues(27) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(27).Label = "EndDate_Academic_YearMinus1"
                rptExecutionParamValues(27).Name = "EndDate_Academic_YearMinus1"
                rptExecutionParamValues(27).Value = dtEndDate2

                rptExecutionParamValues(28) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(28).Label = "EndDate_Academic_YearMinus2"
                rptExecutionParamValues(28).Name = "EndDate_Academic_YearMinus2"
                rptExecutionParamValues(28).Value = dtEndDate3
            End If
            Return rptExecutionParamValues
        End Function
        Private Function FinancialAidPartEDetailAndSummaryReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim SchoolType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            '' Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String
            Dim institutionType As String
            Dim endDateAcademic As String = ""
            Dim largeProgramId As String = ""
            Dim largeProgramName As String = ""

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                SchoolType = .SchoolType
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear
                'This is a special case for 12 Month Enrollment Reports
                'The Start Date is between September 2
                If getReportParametersObj.ResourceId.ToString = "674" Then
                    intReportYear = CInt(CohortYear)
                    If SchoolType.ToString.Trim.ToLower = "program" Then
                        'Program Type Schools - date range is always between 09/01/COHORT YEAR TO 08/31/NEXT YEAR - EXAMPLE: 09/01/2009 TO 08/31/2010
                        '.StartDate = "09/01/" + CohortYear.ToString.Trim
                        '.EndDate = "8/31/" + (CInt(CohortYear) + 1).ToString.Trim
                        '_strDateRangeText = "AID RECEIVED BETWEEN SEPTEMBER 1, " + CohortYear.ToString.Trim + " THROUGH AUGUST 31, " + (CInt(CohortYear) + 1).ToString.Trim

                        .StartDate = "07/01/" + CohortYear.ToString.Trim
                        'dtEndDate = CDate(OfficialReportingDate)
                        dtEndDate = CDate("06/30/" + (CInt(CohortYear) + 1).ToString.Trim) 'CDate(OfficialReportingDate)
                        .EndDate = Month(dtEndDate).ToString.Trim + "/" + Day(dtEndDate).ToString.Trim + "/" + (CInt(CohortYear) + 1).ToString.Trim
                        endDateAcademic = .EndDate
                        largeProgramId = .LargeProgramId
                        largeProgramName = .LargeProgramName
                        _strDateRangeText = "AID AWARDED BETWEEN JULY 1," + CohortYear.ToString.Trim + " THROUGH " + MonthName(Month(dtEndDate)).ToUpper & " " & Day(dtEndDate) & ", " & (CInt(CohortYear) + 1).ToString.Trim
                    Else
                        'Academic schools date range is always between JULY 1ST COHORT YEAR THROUGH JUNE 30TH (COHORT YEAR+1) EXAMPLE: JULY 1ST 2009 THROUGH JUNE 30TH 2010
                        '.StartDate = "07/01/" + CohortYear.ToString.Trim
                        'dtEndDate = CDate(OfficialReportingDate)
                        '.EndDate = Month(dtEndDate).ToString.Trim + "/" + Day(dtEndDate).ToString.Trim + "/" + (CInt(CohortYear) + 1).ToString.Trim
                        largeProgramId = ""
                        largeProgramName = ""
                        .StartDate = "07/01/" + CohortYear.ToString.Trim
                        dtEndDate = CDate(OfficialReportingDate)
                        .EndDate = Month(dtEndDate).ToString.Trim + "/" + Day(dtEndDate).ToString.Trim + "/" + Year(dtEndDate).ToString.Trim
                        endDateAcademic = "06/30/" + (CInt(CohortYear) + 1).ToString.Trim
                        _strDateRangeText = "AID AWARDED BETWEEN JULY 1," + CohortYear.ToString.Trim + " THROUGH " + " JUNE 30," + (CInt(CohortYear) + 1).ToString.Trim
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
                institutionType = .InstitutionType
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "STUDENT FINANCIAL AID"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = ""

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = endDateAcademic

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "SchoolType"
                rptExecutionParamValues(9).Name = "SchoolTYpe"
                rptExecutionParamValues(9).Value = SchoolType



                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "OrderBy"
                rptExecutionParamValues(10).Name = "OrderBy"
                rptExecutionParamValues(10).Value = SortFields.ToString

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "CampusName"
                rptExecutionParamValues(11).Name = "CampusName"
                rptExecutionParamValues(11).Value = CampusName

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "StudentIdentifier"
                rptExecutionParamValues(12).Name = "StudentIdentifier"
                rptExecutionParamValues(12).Value = StudentIdentifier

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "ServerName"
                rptExecutionParamValues(13).Name = "ServerName"
                rptExecutionParamValues(13).Value = strServerName

                'Database Name
                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "DBName"
                rptExecutionParamValues(14).Name = "DBName"
                rptExecutionParamValues(14).Value = strDBName


                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "UserName"
                rptExecutionParamValues(15).Name = "UserName"
                rptExecutionParamValues(15).Value = strUserName

                'password
                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "Password"
                rptExecutionParamValues(16).Name = "Password"
                rptExecutionParamValues(16).Value = strPassword

                'Date Range text
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "DateRangeText"
                rptExecutionParamValues(17).Name = "DateRangeText"
                rptExecutionParamValues(17).Value = _strDateRangeText

                rptExecutionParamValues(18) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(18).Label = "InstitutionType"
                rptExecutionParamValues(18).Name = "InstitutionType"
                rptExecutionParamValues(18).Value = institutionType

                rptExecutionParamValues(19) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(19).Label = "EndDate_Academic"
                rptExecutionParamValues(19).Name = "EndDate_Academic"
                rptExecutionParamValues(19).Value = EndDate

                rptExecutionParamValues(20) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(20).Label = "LargeProgramId"
                rptExecutionParamValues(20).Name = "LargeProgramId"
                If Not largeProgramId.Trim = "" Then
                    rptExecutionParamValues(20).Value = largeProgramId
                Else
                    rptExecutionParamValues(20).Value = Nothing
                End If

                rptExecutionParamValues(21) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(21).Label = "LargeProgramName"
                rptExecutionParamValues(21).Name = "LargeProgramName"
                If Not largeProgramName.Trim = "" Then
                    rptExecutionParamValues(21).Value = largeProgramName
                Else
                    rptExecutionParamValues(21).Value = Nothing
                End If
            End If
            Return rptExecutionParamValues
        End Function
        Private Function ZGraduationRate200LessThan4YrInstDetailAndSummaryReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As IPEDReportSelectionValue, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}

            Dim strNullValue As String = "Null"
            Dim CampusId As String
            Dim ProgId As String
            Dim PrgGrpId As String
            Dim CohortYear As String
            Dim OfficialReportingDate As String
            Dim SortFields As String
            Dim CohortType As String
            Dim StartDate As String
            Dim EndDate As String
            Dim PriorToDate As String
            Dim CampusName As String
            Dim boolShowSSN As Boolean = False
            Dim strReportingYear As String
            Dim intReportYear As Integer
            ''  Dim dtStartDate As Date
            Dim dtEndDate As Date
            Dim StudentIdentifier As String

            With getReportParametersObj
                CampusId = .CampusId
                CohortYear = .CohortYear.ToString
                PrgGrpId = .AreasOfInterestId.ToString
                StartDate = .StartDate
                EndDate = .EndDate
                OfficialReportingDate = .OfficialReportingDate
                PriorToDate = .PriorToDate
                ProgId = .ProgramId
                If .SchoolType = "program" Then CohortType = "full year" Else CohortType = "fall"
                SortFields = .SortFields
                CampusName = .CampusName
                If .StudentIdentifier.ToString.Trim.ToLower = "ssn" Then boolShowSSN = True
                strReportingYear = .ReportingYear

                If getReportParametersObj.ResourceId.ToString = "782" Then
                    intReportYear = CInt(CohortYear)
                    If CohortType.ToString.Trim.ToLower = "Full Year" Then
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = "08/31/" + CStr(CInt(CohortYear.ToString.Trim) + 1)
                    Else
                        dtEndDate = CDate(OfficialReportingDate)
                        .StartDate = "09/01/" + CohortYear.ToString.Trim
                        .EndDate = dtEndDate.ToString("MM/dd/yyyy")
                    End If
                End If
                StartDate = .StartDate
                EndDate = .EndDate
                StudentIdentifier = .StudentIdentifier
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                'CohortYear
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CohortYear"
                rptExecutionParamValues(0).Name = "CohortYear"
                If CohortYear.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CohortYear
                End If

                'CampusId Parameter

                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not CampusId.Trim = "" Then
                    rptExecutionParamValues(1).Value = CampusId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "SchoolName"
                rptExecutionParamValues(2).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strSchoolName
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If

                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "ResourceId"
                rptExecutionParamValues(3).Name = "ResourceId"
                rptExecutionParamValues(3).Value = getReportParametersObj.ResourceId.ToString
                'If Not SortFields.Trim = "" Then
                '    rptExecutionParamValues(3).Value = SortFields.ToString
                'Else
                '    rptExecutionParamValues(3).Value = Nothing
                'End If

                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgId"
                rptExecutionParamValues(4).Name = "ProgId"
                If Not ProgId.Trim = "" Then
                    rptExecutionParamValues(4).Value = ProgId.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "SurveyTitle"
                rptExecutionParamValues(5).Name = "SurveyTitle"
                'If Not OfficialReportingDate.Trim = "" Then
                rptExecutionParamValues(5).Value = "GRADUATION RATES 200"
                'Else
                'rptExecutionParamValues(5).Value = Nothing
                'End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ReportTitle"
                rptExecutionParamValues(6).Name = "ReportTitle"
                rptExecutionParamValues(6).Value = ""

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StartDate"
                rptExecutionParamValues(7).Name = "StartDate"
                rptExecutionParamValues(7).Value = StartDate

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "EndDate"
                rptExecutionParamValues(8).Name = "EndDate"
                rptExecutionParamValues(8).Value = EndDate

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "CohortPossible"
                rptExecutionParamValues(9).Name = "CohortPossible"
                rptExecutionParamValues(9).Value = CohortType

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "ShowSSN"
                rptExecutionParamValues(10).Name = "ShowSSN"
                If boolShowSSN = True Then
                    rptExecutionParamValues(10).Value = "true"
                Else
                    rptExecutionParamValues(10).Value = "false"
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "OrderBy"
                rptExecutionParamValues(11).Name = "OrderBy"
                rptExecutionParamValues(11).Value = SortFields.ToString

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "CampusName"
                rptExecutionParamValues(12).Name = "CampusName"
                rptExecutionParamValues(12).Value = CampusName

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "StudentIdentifier"
                rptExecutionParamValues(13).Name = "StudentIdentifier"
                rptExecutionParamValues(13).Value = StudentIdentifier

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "ServerName"
                rptExecutionParamValues(14).Name = "ServerName"
                rptExecutionParamValues(14).Value = strServerName

                'Database Name
                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "DBName"
                rptExecutionParamValues(15).Name = "DBName"
                rptExecutionParamValues(15).Value = strDBName


                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "UserName"
                rptExecutionParamValues(16).Name = "UserName"
                rptExecutionParamValues(16).Value = strUserName

                'password
                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "Password"
                rptExecutionParamValues(17).Name = "Password"
                rptExecutionParamValues(17).Value = strPassword
            End If
            Return rptExecutionParamValues
        End Function
#End Region

        Function GetDatabaseName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("database=")
            If startIndex > -1 Then startIndex += 9
            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""
            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length
            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetServerName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("server=")
            If startIndex > -1 Then startIndex += 7

            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetUserName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("uid=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetPassword(ByVal connString As String) As String
            Dim lcConnString = connString '.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("pwd=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function

        Private Function GetAdvAppSettings() As AdvAppSettings
            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            Return MyAdvAppSettings
        End Function
    End Class
End Namespace

