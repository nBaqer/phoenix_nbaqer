﻿Imports FAME.AdvantageV1.BusinessFacade.AR
Imports FAME.Advantage.Reporting.Info
Imports FAME.Parameters.Info
Imports FAME.Advantage.Reporting
Namespace Logic
    Public Class StudentGpaReport
        Dim PrgVerId As String = ""
        Dim CampusId As String = ""
        Dim TermId As String = ""
        Dim NumberofDaysMissed As Integer = 0
        Dim CutOffDate As DateTime
        Dim statuscodeid As String = ""
        Dim studentgrpid As String = ""
        Dim cumulativeGPAGt As Decimal
        Dim cumulativeGPALt As Decimal
        Dim termcreditsGt As Decimal
        Dim termCreditsLt As Decimal
        Dim termGPAGt As Decimal
        Dim termGPALt As Decimal
        'Dim _boolShowGpAbyTerm As Boolean = False
        Public Function BuildReport(ByVal Rpt As ReportInfo, ByVal conn As String, ByVal UserId As String, _
                                    ByVal strGradingMethod As String, _
                                    ByVal strGPAMethod As String, _
                                    ByVal strSchoolName As String, _
                                    ByVal strTrackAttendanceBy As String, _
                                    ByVal strGradeBookWeightingLevel As String, _
                                    ByVal strStudentIdentifier As String, _
                                    ByVal DisplayAttendanceUnitForProgressReportByClass As String, _
                                    ByVal strReportPath As String) As [Byte]()
            Dim TestText As String = String.Empty
            Try
                Dim objReportInfo As ReportInfo = Rpt
                Dim getReportOutput As [Byte]()
                Dim pStudentIdentifier As String = ""
                Dim boolStudentsWithGPA As Boolean = True
                For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                    Select Case tabview.ParamSet.SetType
                        Case "Custom"
                            For Each item As ParamItemUserSettingsInfo In Rpt.CustomOptions
                                'Response.Write(item.ControlSettingsCollection.Item)
                                For Each drillItem As ControlSettingInfo In item.ControlSettingsCollection
                                    For Each controlValueItem As ControlValueInfo In drillItem.ControlValueCollection
                                        Dim strKeyName As String = ""
                                        Dim strKeyValue As String = ""
                                        Dim strDisplayText As String = ""
                                        Select Case drillItem.ControlName.Trim.ToString.ToLower
                                            Case Is = "radcombocampus"
                                                strKeyName = "campusid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radlistboxprogramversion2"
                                                strKeyName = "programversionid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower

                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radlistboxterm2"
                                                strKeyName = "termid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radlistboxenrollmentstatus2"
                                                strKeyName = "statuscodeid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radlistboxstudentgroup2"
                                                strKeyName = "studentgroup"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "cumulativegpa_gt"
                                                strKeyName = "cumulativegpa_gt"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "cumulativegpa_lt"
                                                strKeyName = "cumulativegpa_lt"
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim.ToLower
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radtermcredits_gt"
                                                strKeyName = "termcredits_gt"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radtermcredits_lt"
                                                strKeyName = "termcredits_lt"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radtermgpa_gt"
                                                strKeyName = "termgpa_gt"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radtermgpa_lt"
                                                strKeyName = "termgpa_lt"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                                'Case Is = "chkshowgpabyterm"
                                                '    strKeyName = "showtermbygpa"
                                                '    strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                '    strDisplayText = controlValueItem.DisplayText.ToString.Trim.ToLower
                                                '    BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                        End Select
                                    Next
                                Next
                            Next
                        Case "Sort"
                        Case "Filter"

                        Case Else
                            Throw New Exception("Unknown Type")
                    End Select
                Next
                Dim getSelectedValues As New StudentGpaReportSelectionValues
                With getSelectedValues
                    .PrgVerId = PrgVerId.ToString
                    .CampusId = CampusId.ToString
                    .ReportPath = objReportInfo.RDLName
                    .Environment = strReportPath
                    .TermId = TermId
                    .StatusCodeId = statuscodeid
                    .StudentGrpId = studentgrpid
                    .CumulativeGpagt = cumulativeGPAGt
                    .CumulativeGpalt = cumulativeGPALt
                    .TermGpagt = termGPAGt
                    .TermGpalt = termGPALt
                    .TermCreditGt = termcreditsGt
                    .TermCreditLt = termCreditsLt
                    '.ShowGpAbyTerm = _boolShowGpAbyTerm
                    .StudentIdentifier = strStudentIdentifier
                End With

                If InStrRev(PrgVerId, ",") >= 1 Then
                    PrgVerId = PrgVerId.Substring(0, PrgVerId.Length - 1)
                End If

                If InStrRev(statuscodeid, ",") >= 1 Then
                    statuscodeid = statuscodeid.Substring(0, statuscodeid.Length - 1)
                End If

                If InStrRev(studentgrpid, ",") >= 1 Then
                    studentgrpid = studentgrpid.Substring(0, studentgrpid.Length - 1)
                End If

                'Dim dt As DataTable
                'dt = (New CoursesFacade).GetAvailableStudentsWithGpa(PrgVerId.ToString, statuscodeid, studentgrpid)
                'If dt.Rows.Count = 0 Then
                '    Return Nothing 'This will display "No data exists" message in UI
                'End If
                'intCheckDataExists = GetProgressDA.GetProgressReportRecordCount(strStuEnrollId, strCampGrpId, strPrgVerId, strStatusCodeId, strTermId, strStudentGrpId, dtStartString, UserId.ToString, strStartStringModifier)
                'If intCheckDataExists >= 1 Then
                Dim strFormat As String = ""
                Select Case Rpt.ExportFormat
                    Case ReportInfo.ExportType.PDF
                        strFormat = "Pdf"
                    Case ReportInfo.ExportType.Excel
                        strFormat = "Excel"
                    Case ReportInfo.ExportType.CSV
                        strFormat = "csv"
                    Case ReportInfo.ExportType.Word
                        strFormat = "WORD"
                End Select
                getReportOutput = CallRenderReport(strFormat, getSelectedValues, strSchoolName)
                Return getReportOutput
                'Else
                '    Return Nothing
                'End If
                'Return Nothing
            Catch ex As Exception
                Throw ex
            End Try
            Return Nothing
        End Function
        Private Sub BuildParamtersForReport(ByVal strFilterFieldName As String, _
                                            ByVal strFilterSelectedValue As String, _
                                            ByVal strDisplayText As String)
            Select Case strFilterFieldName.ToLower.Trim
                Case "campusid"
                    CampusId = strFilterSelectedValue.ToString
                Case "programversionid"
                    PrgVerId &= strFilterSelectedValue.ToString & ","
                Case "termid"
                    TermId &= strFilterSelectedValue.ToString & ","
                Case "statuscodeid"
                    statuscodeid &= strFilterSelectedValue.ToString & ","
                Case "studentgroup"
                    studentgrpid &= strFilterSelectedValue.ToString & ","
                Case "cumulativegpa_gt"
                    If String.IsNullOrEmpty(strDisplayText) Then
                        strDisplayText = "0.00"
                    End If
                    cumulativeGPAGt = Convert.ToDecimal(strDisplayText)
                Case "cumulativegpa_lt"
                    If String.IsNullOrEmpty(strDisplayText) Then
                        strDisplayText = "9999.00"
                    End If
                    cumulativeGPALt = Convert.ToDecimal(strDisplayText)
                Case "termcredits_gt"
                    If String.IsNullOrEmpty(strDisplayText) Then
                        strDisplayText = "0.00"
                    End If
                    termcreditsGt = Convert.ToDecimal(strDisplayText)
                Case "termcredits_lt"
                    If String.IsNullOrEmpty(strDisplayText) Then
                        strFilterSelectedValue = "9999.00"
                    End If
                    termCreditsLt = Convert.ToDecimal(strDisplayText)
                Case "termgpa_gt"
                    If String.IsNullOrEmpty(strDisplayText) Then
                        strFilterSelectedValue = "0.00"
                    End If
                    termGPAGt = Convert.ToDecimal(strDisplayText)
                Case "termgpa_lt"
                    If String.IsNullOrEmpty(strDisplayText) Then
                        strFilterSelectedValue = "9999.00"
                    End If
                    termGPALt = Convert.ToDecimal(strDisplayText)
                    'Case "showtermbygpa"
                    '    If strFilterSelectedValue = "true" Then
                    '        _boolShowGpAbyTerm = True
                    '    End If
            End Select
        End Sub
        Public Function CallRenderReport(ByVal format As String, ByVal getFilterValues As StudentGpaReportSelectionValues, ByVal strSchoolName As String) As [Byte]()
            Dim getReportOutput As [Byte]()
            Dim generateReport As New StudentGpaReportGenerator
            getReportOutput = generateReport.RenderReport(format, getFilterValues, strSchoolName)
            Return getReportOutput
        End Function
    End Class
End Namespace