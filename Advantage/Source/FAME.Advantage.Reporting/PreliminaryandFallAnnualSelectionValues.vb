﻿Public Class PreliminaryAndFallAnnualSelectionValues
#Region "Variables"
    Private _reportType As String
    Private _resId As String
    Private _reportYear As String
    Private _campusId As String
    Private _programsList As String

    Private _ReportPath As String
    Private _environment As String
    Private _apiUrl As String
    Private _apiToken As String

#End Region
#Region "Address Initialize"
    Public Sub New()

    End Sub
#End Region

#Region "Properties"
    Public Property ReportType() As String
        Get
            Return _reportType
        End Get
        Set(ByVal value As String)
            _reportType = value
        End Set
    End Property

    Public Property Year() As String
        Get
            Return _reportYear
        End Get
        Set(ByVal value As String)
            _reportYear = value
        End Set
    End Property

    Public Property ResId() As String
        Get
            Return _resId
        End Get
        Set(ByVal value As String)
            _resId = value
        End Set
    End Property

    Public Property CampusId() As String
        Get
            Return _campusId
        End Get
        Set(ByVal value As String)
            _campusId = value
        End Set
    End Property

    Public Property ProgramList() As String
        Get
            Return _programsList
        End Get
        Set(ByVal value As String)
            _programsList = value
        End Set
    End Property

    Public Property ReportPath() As String
        Get
            Return _ReportPath
        End Get
        Set(ByVal value As String)
            _ReportPath = value
        End Set
    End Property
    Public Property Environment() As String
        Get
            Return _environment
        End Get
        Set(ByVal value As String)
            _environment = value
        End Set
    End Property

    Public Property ApiToken() As String
        Get
            Return _apiToken
        End Get
        Set(ByVal value As String)
            _apiToken = value
        End Set
    End Property

    Public Property ApiUrl() As String
        Get
            Return _apiUrl
        End Get
        Set(ByVal value As String)
            _apiUrl = value
        End Set
    End Property
#End Region
End Class
