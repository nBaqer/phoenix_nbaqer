﻿Imports FAME.Advantage.Reporting.Info
Imports FAME.Parameters.Collections
Imports FAME.Parameters.Info
Imports FAME.Advantage.DataAccess.LINQ
Imports System.Web.UI

Namespace Logic
    <Serializable()>
    Public Class TranscriptReport
        'Transcript Report Parameters
        'General 
        Public _temp As TranscriptReportSelectionValues = New TranscriptReportSelectionValues()


        Public Function BuildReport(ByVal pRpt As ReportInfo,
                                    ByVal pConn As String,
                                    ByVal PUserId As String,
                                    ByVal pReportPath As String) As [Byte]()

            Dim TestText As String = String.Empty
            Try
                Dim objReportInfo As ReportInfo = pRpt
                For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                    Select Case tabview.ParamSet.SetType
                        Case "Filter"
                            Dim Filters As MasterDictionary = pRpt.Filters
                            Dim FCollectionName As String = Filters.Name

                            For Each d0 As DictionaryEntry In Filters
                                Dim Details As DetailDictionary = DirectCast(d0.Value, DetailDictionary)
                                Dim DCollectionName As String = Details.Name
                                Dim DName As String = d0.Key.ToString
                                TestText = TestText & " <hr /> Control Name: " & DName & "<br />"
                                For Each d1 As DictionaryEntry In Details
                                    Dim SourceName As String = d1.Key.ToString()
                                    TestText = TestText & "Filter Name: " & SourceName & "<br />"
                                    Dim objParamValue As ParamValueDictionary = DirectCast(d1.Value, ParamValueDictionary)
                                    For Each d2 As DictionaryEntry In objParamValue
                                        Dim ParameterModifier As String = d2.Key.ToString()
                                        TestText = TestText & "Database Key: " & objParamValue.Name & "<br />"
                                        TestText = TestText & "Modifier Type: " & ParameterModifier & "<br />"
                                        Dim ModifierDictionary As ModDictionary = DirectCast(d2.Value, ModDictionary)
                                        Dim ModName As String = ModifierDictionary.Name
                                        For Each d3 As DictionaryEntry In ModifierDictionary
                                            Dim ParameterName As String = d3.Key.ToString

                                            TestText = TestText & "ParameterName: " & ParameterName & "<br />"
                                            Dim objValueList As ValueList = DirectCast(d3.Value, ValueList)
                                            Dim objValueType As String = objValueList.GetType.ToString

                                            If objValueType.Contains("[System.String]") Then
                                                Dim SelectedValues As ParamValueList(Of String) = DirectCast(objValueList, ParamValueList(Of String))
                                                Dim strSelectedFilterValues As String = ""
                                                ' strSelectedFilterValues = "'"  'Start a single quote
                                                For Each selection As String In SelectedValues
                                                    TestText = TestText & "Value: " & selection & "<br />"
                                                    strSelectedFilterValues &= selection.ToString
                                                Next
                                                If strSelectedFilterValues.Length >= 36 Then
                                                    strSelectedFilterValues = Mid(strSelectedFilterValues, 1, InStrRev(strSelectedFilterValues, ",") - 1) & "'"
                                                Else
                                                    strSelectedFilterValues = ""
                                                End If
                                                BuildParamtersForReport(objParamValue.Name, strSelectedFilterValues, "")
                                            ElseIf objValueType.Contains("[System.Guid]") Then
                                                Dim SelectedValues As ParamValueList(Of Guid) = DirectCast(objValueList, ParamValueList(Of Guid))
                                                Dim strSelectedFilterValues As String = ""
                                                '  strSelectedFilterValues = "'"  'Start a single quote
                                                For Each selection As Guid In SelectedValues
                                                    TestText = TestText & "Value: " & selection.ToString & "<br />"
                                                    strSelectedFilterValues &= selection.ToString & ","
                                                Next
                                                If strSelectedFilterValues.Length >= 36 Then
                                                    strSelectedFilterValues = Mid(strSelectedFilterValues, 1, InStrRev(strSelectedFilterValues, ",") - 1)
                                                Else
                                                    strSelectedFilterValues = ""
                                                End If
                                                BuildParamtersForReport(objParamValue.Name, strSelectedFilterValues, "")
                                            ElseIf objValueType.Contains("[System.Integer]") Then
                                                Dim SelectedValues As ParamValueList(Of Integer) = DirectCast(objValueList, ParamValueList(Of Integer))
                                                For Each selection As Integer In SelectedValues
                                                    TestText = TestText & "Value: " & selection.ToString & "<br />"
                                                Next
                                            ElseIf objValueType.Contains("[System.DateTime]") Then
                                                Dim SelectedValues As ParamValueList(Of Date) = DirectCast(objValueList, ParamValueList(Of Date))
                                                For Each selection As Date In SelectedValues
                                                    TestText = TestText & "Value: " & selection.ToString & "<br />"
                                                    Dim strModifier As String = ConvertModifierFromTextToSymbol(ParameterModifier)
                                                    Dim strDateValue As String = selection.ToShortDateString
                                                    BuildParamtersForReport(objParamValue.Name, strDateValue, strModifier)
                                                Next
                                            ElseIf objValueType.Contains("[System.Long]") Then
                                                Dim SelectedValues As ParamValueList(Of Long) = DirectCast(objValueList, ParamValueList(Of Long))
                                                For Each selection As Long In SelectedValues
                                                    TestText = TestText & "Value: " & selection.ToString & "<br />"
                                                Next
                                            ElseIf objValueType.Contains("[System.Boolean]") Then
                                                Dim SelectedValues As ParamValueList(Of Boolean) = DirectCast(objValueList, ParamValueList(Of Boolean))
                                                For Each selection As Boolean In SelectedValues
                                                    TestText = TestText & "Value: " & selection.ToString & "<br />"
                                                Next
                                            Else
                                                Throw New Exception("Unknown Type")
                                            End If
                                        Next
                                    Next
                                Next
                            Next

                        Case "Sort"
                            'Sort by Sort Sequence field
                            pRpt.Sorts.Sort(Function(p1, p2) p1.SortSeq.CompareTo(p2.SortSeq))
                            For Each item As SortItemInfo In pRpt.Sorts
                                _temp.SortFields &= item.FieldName.ToString
                                If item.IsDecending = True Then
                                    _temp.SortFields &= " Desc"
                                Else
                                    _temp.SortFields &= " Asc"
                                End If
                                _temp.SortFields &= ","
                            Next
                            If Not _temp.SortFields.Trim = "" Then _temp.SortFields = Mid(_temp.SortFields, 1, InStrRev(_temp.SortFields, ",") - 1).ToString.Trim
                        Case "Custom"
                            For Each item As ParamItemUserSettingsInfo In pRpt.CustomOptions
                                'Response.Write(item.ControlSettingsCollection.Item)
                                For Each drillItem As ControlSettingInfo In item.ControlSettingsCollection

                                    For Each controlValueItem As ControlValueInfo In drillItem.ControlValueCollection

                                        ' Header
                                        If drillItem.ControlName.ToString().Trim() = "cblHeaderOptions" Then
                                            If controlValueItem.DisplayText.ToString().Trim() = "No logo or School Information" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.NoLogoOrSchooInformation = True
                                                Else
                                                    _temp.NoLogoOrSchooInformation = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Official Transcript" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.IsOfficialTranscript = True
                                                Else
                                                    _temp.IsOfficialTranscript = False
                                                End If
                                            End If

                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Report Date" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowCurrentDate = True
                                                Else
                                                    _temp.ShowCurrentDate = False
                                                End If
                                            End If
                                        End If

                                        ' School Header
                                        If drillItem.ControlName.ToString().Trim() = "cblSchoolInformationOptions" Then
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show School Name" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowSchoolName = True
                                                Else
                                                    _temp.ShowSchoolName = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Campus Address" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowCampusAddress = True
                                                Else
                                                    _temp.ShowCampusAddress = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Phone" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowSchoolPhone = True
                                                Else
                                                    _temp.ShowSchoolPhone = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Fax" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowSchoolFax = True
                                                Else
                                                    _temp.ShowSchoolFax = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Web Site" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowSchoolWebSite = True
                                                Else
                                                    _temp.ShowSchoolWebSite = False
                                                End If
                                            End If
                                        End If
                                        ' Logo Position
                                        If drillItem.ControlName.Trim().ToString() = "radSchoolLogoPosition" Then
                                            _temp.OfficialLogoPosition = controlValueItem.KeyData.ToString()
                                        End If
                                        ' Student
                                        If drillItem.ControlName.ToString().Trim() = "cblStudentOptions" Then
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Phone" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowStudentPhone = True
                                                Else
                                                    _temp.ShowStudentPhone = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Date of Birth" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowStudentDOB = True
                                                Else
                                                    _temp.ShowStudentDOB = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Email Address" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowStudentEmail = True
                                                Else
                                                    _temp.ShowStudentEmail = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show SSN" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowSSN = True
                                                Else
                                                    _temp.ShowSSN = False
                                                End If
                                            End If
                                        End If
                                        ' Program
                                        If drillItem.ControlName.ToString().Trim() = "cblProgramOptions" Then
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Multiple Enrollments on Single Transcript" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowMultipleEnrollments = True
                                                Else
                                                    _temp.ShowMultipleEnrollments = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Graduation Date" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowGraduationDate = True
                                                Else
                                                    _temp.ShowGraduationDate = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Last Day of Attendance" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowLDA = True
                                                Else
                                                    _temp.ShowLDA = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Complete Hours" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowCompletedHours = True
                                                Else
                                                    _temp.ShowCompletedHours = False
                                                End If
                                            End If
                                        End If
                                        ' Courses
                                        If drillItem.ControlName.ToString().Trim() = "rblShowTermCourses" Then
                                            'rblShowTermCourses
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Terms/Modules" Then
                                                _temp.CoursesLayout = "ShowTerms/Modules"
                                                _temp.ShowCreditsColumn = True
                                                _temp.ShowHoursColumn = True
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Courses only" Then
                                                _temp.CoursesLayout = "ShowCoursesOnly"
                                                _temp.ShowCreditsColumn = True
                                                _temp.ShowHoursColumn = True
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Course Categories" Then
                                                _temp.CoursesLayout = "ShowCourseCategories"
                                            End If
                                        End If
                                        'cblTermsModulesOptions
                                        If drillItem.ControlName.ToString().Trim() = "cblTermsModulesOptions" _
                                        And _temp.CoursesLayout = "ShowTerms/Modules" Then
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Term Description" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowTermDescription = True
                                                Else
                                                    _temp.ShowTermDescription = False
                                                End If
                                            End If
                                        End If

                                        'cblShowCourseComponents
                                        If drillItem.ControlName.ToString().Trim() = "cblShowCourseComponents" _
                                        And _temp.CoursesLayout = "ShowCoursesOnly" Then
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show course components" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowCourseComponents = True
                                                Else
                                                    _temp.ShowCourseComponents = False
                                                End If
                                            End If
                                        End If
                                        'cblCourseCategoriesOptions
                                        If drillItem.ControlName.ToString().Trim() = "cblCourseCategoriesOptions" _
                                        And _temp.CoursesLayout = "ShowCourseCategories" Then
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Date Issued" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowDateIssuedColumn = True
                                                Else
                                                    _temp.ShowDateIssuedColumn = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Credits Column" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowCreditsColumn = True
                                                Else
                                                    _temp.ShowCreditsColumn = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Hours Column" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowHoursColumn = True
                                                Else
                                                    _temp.ShowHoursColumn = False
                                                End If
                                            End If
                                        End If
                                        ' Footer
                                        If drillItem.ControlName.ToString().Trim() = "cblFooterOptions" Then
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Courses Summary" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowCourseSummary = True
                                                Else
                                                    _temp.ShowCourseSummary = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Grade Points in Summary" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowGradePoints = True
                                                Else
                                                    _temp.ShowGradePoints = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Grade Scale" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowGradeScale = True
                                                Else
                                                    _temp.ShowGradeScale = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Student Signature Line" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowStudentSignatureLine = True
                                                Else
                                                    _temp.ShowStudentSignatureLine = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show School Official Signature Line" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowSchoolOfficialSignatureLine = True
                                                Else
                                                    _temp.ShowSchoolOfficialSignatureLine = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Page Number" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowPageNumber = True
                                                Else
                                                    _temp.ShowPageNumber = False
                                                End If
                                            End If
                                            If controlValueItem.DisplayText.ToString().Trim() = "Show Disclaimer" Then
                                                If controlValueItem.KeyData.ToString().Trim().ToLower() = "true" Then
                                                    _temp.ShowDisclaimer = True
                                                Else
                                                    _temp.ShowDisclaimer = False
                                                End If
                                            End If
                                        End If
                                        ' Disclaimer
                                        If drillItem.ControlName.ToString().Trim() = "radTextBoxDisclaimer" Then
                                            If controlValueItem.KeyData.ToString().Trim() = "DisclaimerText" Then
                                                _temp.DisclaimerText = controlValueItem.DisplayText.ToString().Trim()
                                            End If
                                        End If
                                    Next
                                Next
                            Next

                        Case Else
                            Throw New Exception("Unknown Type")
                    End Select
                Next
                'If Rpt.PagingOption = ReportInfo.PagingStyle.None Then boolShowPageNumber = False
                'If Rpt.HeadingOption = ReportInfo.HeadingStyle.None Then boolShowHeading = False
                'If Rpt.ReportDateOption = ReportInfo.ReportDateStyle.Yes Then boolShowDateInFooter = True

                Dim getTRSelectedValues As New TranscriptReportSelectionValues
                Dim GetTranscriptReportDA As New TranscriptReportDA(pConn)
                With getTRSelectedValues
                    'general
                    .ReportPath = pRpt.RDLName
                    .Environment = pReportPath
                    '.ServerName = ""
                    '.DatabaseName = ""
                    '.UserName = ""
                    '.Password = ""

                    'Filter Tab
                    .CampGrpId = _temp.CampGrpId
                    .CampusId = _temp.CampusId
                    .PrgVerId = _temp.PrgVerId
                    .StatusCodeIdList = _temp.StatusCodeIdList
                    .StuEnrollIdList = _temp.StuEnrollIdList

                    'Sort Tab
                    .SortFields = _temp.SortFields

                    'Options Tab
                    'Header

                    .NoLogoOrSchooInformation = _temp.NoLogoOrSchooInformation

                    'Logo Position
                    .OfficialLogoPosition = _temp.OfficialLogoPosition

                    'School Header 
                    .ShowSchoolName = _temp.ShowSchoolName
                    .ShowCampusAddress = _temp.ShowCampusAddress
                    .ShowSchoolPhone = _temp.ShowSchoolPhone
                    .ShowSchoolFax = _temp.ShowSchoolFax
                    .ShowSchoolWebSite = _temp.ShowSchoolWebSite

                    'General Header
                    .IsOfficialTranscript = _temp.IsOfficialTranscript
                    .ShowCurrentDate = _temp.ShowCurrentDate

                    'Student
                    .ShowStudentPhone = _temp.ShowStudentPhone
                    .ShowStudentDOB = _temp.ShowStudentDOB
                    .ShowStudentEmail = _temp.ShowStudentEmail
                    .ShowSSN = _temp.ShowSSN

                    'Program
                    .ShowMultipleEnrollments = _temp.ShowMultipleEnrollments
                    .ShowGraduationDate = _temp.ShowGraduationDate
                    .ShowLDA = _temp.ShowLDA
                    .ShowCompletedHours = _temp.ShowCompletedHours

                    'Courses
                    .CoursesLayout = _temp.CoursesLayout
                    .ShowTermDescription = _temp.ShowTermDescription
                    .ShowCourseComponents = _temp.ShowCourseComponents
                    .ShowDateIssuedColumn = _temp.ShowDateIssuedColumn
                    .ShowCreditsColumn = _temp.ShowCreditsColumn
                    .ShowHoursColumn = _temp.ShowHoursColumn

                    'Footer
                    .ShowCourseSummary = _temp.ShowCourseSummary
                    .ShowGradeScale = _temp.ShowGradeScale
                    .ShowStudentSignatureLine = _temp.ShowStudentSignatureLine
                    .ShowSchoolOfficialSignatureLine = _temp.ShowSchoolOfficialSignatureLine
                    .ShowPageNumber = _temp.ShowPageNumber

                    'Disclaimer
                    If (_temp.DisclaimerText <> String.Empty) Then
                        .DisclaimerText = _temp.DisclaimerText
                    End If


                    .ShowDisclaimer = _temp.ShowDisclaimer
                    .ShowGradePoints = _temp.ShowGradePoints
                End With
                Dim intCheckDataExists As Integer = 0
                Dim getReportOutput As [Byte]()

                intCheckDataExists = GetTranscriptReportDA.GetTranscriptReportRecordCount(_temp.StuEnrollIdList, _temp.ShowMultipleEnrollments, _temp.StatusCodeIdList)
                If intCheckDataExists >= 1 Then
                    Dim strFormat As String = ""
                    Select Case pRpt.ExportFormat
                        Case ReportInfo.ExportType.PDF
                            strFormat = "Pdf"
                        Case ReportInfo.ExportType.Excel
                            strFormat = "Excel"
                        Case ReportInfo.ExportType.CSV
                            strFormat = "csv"
                        Case ReportInfo.ExportType.Word
                            strFormat = "WORD"
                    End Select

                    getReportOutput = CallRenderReport(strFormat, getTRSelectedValues)
                    Return getReportOutput
                Else
                    Return Nothing
                End If
                Return Nothing
            Catch ex As Exception
                Throw New ArgumentException(ex.Message)
            End Try
        End Function
        Public Function CallRenderReport(ByVal format As String, ByVal getFilterValues As TranscriptReportSelectionValues) As [Byte]()
            Dim getReportOutput As [Byte]()
            Dim generateReport As New TranscriptReportGenerator
            getReportOutput = generateReport.RenderReport(format, getFilterValues)
            Return getReportOutput
        End Function
        Private Sub BuildParamtersForReport(ByVal strFilterFieldName As String, ByVal strFilterSelectedValue As String, ByVal Modifier As String)
            Select Case strFilterFieldName.ToLower.Trim
                Case "campgrpid"
                    _temp.CampGrpId = strFilterSelectedValue.ToString
                Case "progverid"
                    _temp.PrgVerId = strFilterSelectedValue.ToString
                Case "statuscodeidlist"
                    _temp.StatusCodeIdList = strFilterSelectedValue.ToString
                Case "studentenrollmentid"
                    _temp.StuEnrollIdList = strFilterSelectedValue.ToString
                Case "campusid"
                    _temp.CampusId = strFilterSelectedValue.ToString
            End Select
        End Sub
        Private Function ConvertModifierFromTextToSymbol(ByVal Modifier As String) As String
            Dim strReturnSymbol As String = ""
            Select Case Modifier.Trim.ToLower
                Case "isequalto"
                    strReturnSymbol = "="
                Case "greaterthanorequalto"
                    strReturnSymbol = ">="
                Case "greaterthan"
                    strReturnSymbol = ">"
                Case "lessthanorequalto"
                    strReturnSymbol = "<="
                Case "lessthan"
                    strReturnSymbol = "<"
            End Select
            Return strReturnSymbol
        End Function
    End Class
End Namespace

