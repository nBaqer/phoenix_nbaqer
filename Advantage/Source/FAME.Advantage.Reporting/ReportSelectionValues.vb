Namespace Info
    Public Class ReportSelectionValues
#Region "Variables"
        Private _CampGrpId As String
        Private _PrgVerId As String
        Private _StatusCodeId As String
        Private _TermId As String
        Private _StudentGrpId As String
        Private _SortFields As String
        Private _StuEnrollId As String
        Private _boolGroupByWorkUnits As Boolean = True
        Private _strGrdComponentTypeId As String = ""
        Private _boolShowFinanceCalculations As Boolean = False
        Private _boolShowStudentSignatureLine As Boolean = False
        Private _boolShowSchoolSignatureLine As Boolean = False
        Private _boolShowPageNumber As Boolean = True
        Private _boolShowHeading As Boolean = True
        Private _strStartDateModifier As String = ""
        Private _strStartDate As String = ""
        Private _strExpectedGradDateModifier As String = ""
        Private _strExpectedGradDate As String = ""
        Private _strUserId As String = ""
        Private _strGradingMethod As String = ""
        Private _strGPAMethod As String = ""
        Private _strTrackAttendanceBy As String = ""
        Private _SetGradeBookAt As String = ""
        Private _boolShowWeeklySchedule As Boolean = False
        Private _DisplayAttendanceUnitForProgressReportByClass As String
        Private _boolShowTermModule As Boolean = False
        Private _boolShowDateInFooter As Boolean = False
        Private _ReportPath As String
        Private _TermStartDate As String = ""
        Private _StudentIdentifier As String = ""
        Private _strServerName As String = ""
        Private _strDatabaseName As String = ""
        Private _strUserName As String = ""
        Private _strPassword As String = ""
        Private _strCampusId As String = ""
        Private _Environment As String = ""
        Private _ShowAllEnrollments As Boolean = False
#End Region

#Region "Address Initialize"
        Public Sub New()
            _strServerName = ""
            _strDatabaseName = ""
            _strUserName = ""
            _strPassword = ""
            _CampGrpId = ""
            _PrgVerId = ""
            _StatusCodeId = ""
            _TermId = ""
            _StudentGrpId = ""
            _SortFields = ""
            _StuEnrollId = ""
            _boolGroupByWorkUnits = False
            _strGrdComponentTypeId = ""
            _boolShowFinanceCalculations = False
            _boolShowStudentSignatureLine = False
            _boolShowSchoolSignatureLine = False
            _boolShowPageNumber = True
            _boolShowHeading = True
            _strStartDateModifier = ""
            _strStartDate = ""
            _strExpectedGradDateModifier = ""
            _strExpectedGradDate = ""
            _strUserId = ""
            _strGradingMethod = ""
            _strGPAMethod = ""
            _strTrackAttendanceBy = "class"
            _SetGradeBookAt = "courselevel"
            _boolShowWeeklySchedule = False
            _DisplayAttendanceUnitForProgressReportByClass = ""
            _boolShowTermModule = False
            _boolShowDateInFooter = False
            _ReportPath = ""
            _TermStartDate = ""
            _StudentIdentifier = "ssn"
            _strCampusId = ""
            _ShowAllEnrollments = False
        End Sub
#End Region

#Region "Properties"
        Public Property GroupByWorkUnits() As Boolean
            Get
                Return _boolGroupByWorkUnits
            End Get
            Set(ByVal Value As Boolean)
                _boolGroupByWorkUnits = Value
            End Set
        End Property
        Public Property DisplayAttendanceUnitForProgressReportByClass() As String
            Get
                Return _DisplayAttendanceUnitForProgressReportByClass
            End Get
            Set(ByVal Value As String)
                _DisplayAttendanceUnitForProgressReportByClass = Value
            End Set
        End Property
        Public Property CampusId() As String
            Get
                Return _strCampusId
            End Get
            Set(ByVal Value As String)
                _strCampusId = Value
            End Set
        End Property
        Public Property GrdComponentTypeId() As String
            Get
                Return _strGrdComponentTypeId
            End Get
            Set(ByVal Value As String)
                _strGrdComponentTypeId = Value
            End Set
        End Property
        Public Property SetGradeBookAt() As String
            Get
                Return _SetGradeBookAt
            End Get
            Set(ByVal Value As String)
                _SetGradeBookAt = Value
            End Set
        End Property
        Public Property ShowFinanceCalculations() As Boolean
            Get
                Return _boolShowFinanceCalculations
            End Get
            Set(ByVal Value As Boolean)
                _boolShowFinanceCalculations = Value
            End Set
        End Property
        Public Property ShowSchoolSignatureLine() As Boolean
            Get
                Return _boolShowSchoolSignatureLine
            End Get
            Set(ByVal Value As Boolean)
                _boolShowSchoolSignatureLine = Value
            End Set
        End Property
        Public Property ShowStudentSignatureLine() As Boolean
            Get
                Return _boolShowStudentSignatureLine
            End Get
            Set(ByVal Value As Boolean)
                _boolShowStudentSignatureLine = Value
            End Set
        End Property
        Public Property ShowPageNumber() As Boolean
            Get
                Return _boolShowPageNumber
            End Get
            Set(ByVal Value As Boolean)
                _boolShowPageNumber = Value
            End Set
        End Property
        Public Property ShowHeading() As Boolean
            Get
                Return _boolShowHeading
            End Get
            Set(ByVal Value As Boolean)
                _boolShowHeading = Value
            End Set
        End Property
        Public Property ShowWeeklySchedule() As Boolean
            Get
                Return _boolShowWeeklySchedule
            End Get
            Set(ByVal Value As Boolean)
                _boolShowWeeklySchedule = Value
            End Set
        End Property
        Public Property CampGrpId() As String
            Get
                Return _CampGrpId
            End Get
            Set(ByVal Value As String)
                _CampGrpId = Value
            End Set
        End Property
        Public Property PrgVerId() As String
            Get
                Return _PrgVerId
            End Get
            Set(ByVal Value As String)
                _PrgVerId = Value
            End Set
        End Property
        Public Property StatusCodeId() As String
            Get
                Return _StatusCodeId
            End Get
            Set(ByVal Value As String)
                _StatusCodeId = Value
            End Set
        End Property
        Public Property TermId() As String
            Get
                Return _TermId
            End Get
            Set(ByVal Value As String)
                _TermId = Value
            End Set
        End Property
        Public Property StudentGrpId() As String
            Get
                Return _StudentGrpId
            End Get
            Set(ByVal Value As String)
                _StudentGrpId = Value
            End Set
        End Property
        Public Property StuEnrollId() As String
            Get
                Return _StuEnrollId
            End Get
            Set(ByVal Value As String)
                _StuEnrollId = Value
            End Set
        End Property
        Public Property SortFields() As String
            Get
                Return _SortFields
            End Get
            Set(ByVal Value As String)
                _SortFields = Value
            End Set
        End Property
        Public Property StartDateModifier() As String
            Get
                Return _strStartDateModifier
            End Get
            Set(ByVal Value As String)
                _strStartDateModifier = Value
            End Set
        End Property
        Public Property StartDate() As String
            Get
                Return _strStartDate
            End Get
            Set(ByVal Value As String)
                _strStartDate = Value
            End Set
        End Property
        Public Property ExpectedGradDateModifier() As String
            Get
                Return _strExpectedGradDateModifier
            End Get
            Set(ByVal Value As String)
                _strExpectedGradDateModifier = Value
            End Set
        End Property
        Public Property ExpectedGradDate() As String
            Get
                Return _strExpectedGradDate
            End Get
            Set(ByVal Value As String)
                _strExpectedGradDate = Value
            End Set
        End Property
        Public Property UserId() As String
            Get
                Return _strUserId
            End Get
            Set(ByVal Value As String)
                _strUserId = Value
            End Set
        End Property
        Public Property GradingMethod() As String
            Get
                Return _strGradingMethod
            End Get
            Set(ByVal Value As String)
                _strGradingMethod = Value
            End Set
        End Property
        Public Property GPAMethod() As String
            Get
                Return _strGPAMethod
            End Get
            Set(ByVal Value As String)
                _strGPAMethod = Value
            End Set
        End Property
        Public Property TrackAttendanceBy() As String
            Get
                Return _strTrackAttendanceBy
            End Get
            Set(ByVal Value As String)
                _strTrackAttendanceBy = Value
            End Set
        End Property
        Public Property ShowTermInReport() As Boolean
            Get
                Return _boolShowTermModule
            End Get
            Set(ByVal Value As Boolean)
                _boolShowTermModule = Value
            End Set
        End Property
        Public Property ShowDateInFooter() As Boolean
            Get
                Return _boolShowDateInFooter
            End Get
            Set(ByVal Value As Boolean)
                _boolShowDateInFooter = Value
            End Set
        End Property
        Public Property ReportPath() As String
            Get
                Return _ReportPath
            End Get
            Set(ByVal Value As String)
                _ReportPath = Value
            End Set
        End Property
        Public Property Term_StartDate() As String
            Get
                Return _TermStartDate
            End Get
            Set(ByVal Value As String)
                _TermStartDate = Value
            End Set
        End Property
        Public Property StudentIdentifier() As String
            Get
                Return _StudentIdentifier
            End Get
            Set(ByVal Value As String)
                _StudentIdentifier = Value
            End Set
        End Property
        Public Property ServerName() As String
            Get
                Return _strServerName
            End Get
            Set(ByVal Value As String)
                _strServerName = Value
            End Set
        End Property
        Public Property DatabaseName() As String
            Get
                Return _strDatabaseName
            End Get
            Set(ByVal Value As String)
                _strDatabaseName = Value
            End Set
        End Property
        Public Property UserName() As String
            Get
                Return _strUserName
            End Get
            Set(ByVal Value As String)
                _strUserName = Value
            End Set
        End Property
        Public Property Password() As String
            Get
                Return _strPassword
            End Get
            Set(ByVal Value As String)
                _strPassword = Value
            End Set
        End Property
        Public Property Environment() As String
            Get
                Return _Environment
            End Get
            Set(ByVal Value As String)
                _Environment = Value
            End Set
        End Property
        Public Property ShowAllEnrollments() As Boolean
            Get
                Return _ShowAllEnrollments
            End Get
            Set(ByVal Value As Boolean)
                _ShowAllEnrollments = Value
            End Set
        End Property
#End Region
    End Class
End Namespace

