﻿Public Class ProgramDetailsReportSelectionValues
#Region "Variables"
    Private _termId As String
    Private _reqId As String
    Private _cutOffDate As Date
    Private _numberOfDaysMissed As Integer
    Private _ReportPath As String
    Private _environment As String
    Private _campusId As String
    Private _programversionname As String
    Private _prgverid As String

#End Region
#Region "Address Initialize"
    Public Sub New()

    End Sub
#End Region
#Region "Properties"

    Public Property PrgVerId() As String
        Get
            Return _prgverid
        End Get
        Set(ByVal value As String)
            _prgverid = value
        End Set
    End Property
    Public Property CampusId() As String
        Get

            Return _CampusId
        End Get
        Set(ByVal value As String)
            _CampusId = value
        End Set
    End Property
    Public Property ReqId() As String
        Get
            Return _reqId
        End Get
        Set(ByVal value As String)
            _reqId = value
        End Set
    End Property
    Public Property ReportPath() As String
        Get

            Return _ReportPath
        End Get
        Set(ByVal Value As String)
            _ReportPath = Value
        End Set
    End Property
    Public Property Environment() As String
        Get

            Return _environment
        End Get
        Set(ByVal Value As String)
            _environment = Value
        End Set
    End Property
#End Region
End Class