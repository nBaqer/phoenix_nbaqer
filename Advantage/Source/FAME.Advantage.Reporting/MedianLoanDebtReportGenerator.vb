﻿Imports FAME.Advantage.Reporting.ReportService2005
Imports FAME.Advantage.Reporting.ReportExecution2005
Imports System.Web
Imports FAME.Advantage.Reporting.Info
Imports FAME.Advantage.Common
Namespace Logic
    Public Class MedianLoanDebtReportGenerator
#Region "Report Variables"
        Private _rptService As New ReportingService2005
        Private _rptExecutionService As New ReportExecution2005.ReportExecutionService
        Private _strReportName As String = Nothing
        Private _strHistoryId As String = Nothing
        Private _boolForRendering As Boolean = False
        Private _credentials As ReportService2005.DataSourceCredentials() = Nothing
        Private _rptParameterValues As ReportService2005.ParameterValue() = Nothing
        Private _rptParameters As ReportService2005.ReportParameter() = Nothing
        Private _rptExecutionServiceParameterValues As ReportExecution2005.ParameterValue() = Nothing
        Private _rptExecutionInfoObj As New ExecutionInfo

        'Variables needed to render report
        Private _historyId As String = Nothing
        Private _deviceInfo As String = Nothing
        Private _format As String = Nothing
        Private _generateReportAsBytes As [Byte]()
        Private _encoding As String = [String].Empty
        Private Shared _strExtension As String = String.Empty
        Private Shared _strMimeType As String = String.Empty
        Private _warnings As ReportExecution2005.Warning() = Nothing
        Private _streamIDs As String() = Nothing

        Private strServerName As String = ""
        Private strDBName As String = ""
        Private strUserName As String = ""
        Private strPassword As String = ""
        Private dtStartDate As Date
        Private dtEndDate As Date
        Private strCampusId As String = ""
        Private strProgramIdList As String = ""
        Private strRevGradDateType As String = ""
        Private showPageNumber As Boolean = False
        Private boolShowDateInFooter As Boolean = False

#End Region

#Region "Initialize Web Services"
        Private Sub InitializeWebServices()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString).ToLower
            _rptService.Credentials = System.Net.CredentialCache.DefaultCredentials
            _rptService.Url = MyAdvAppSettings.AppSettings("ReportServices").ToString
            'Set the credentials and url for the report execution services
            _rptExecutionService.Credentials = System.Net.CredentialCache.DefaultCredentials
            '_rptExecutionService.SetExecutionCredentials(dsCredentials)
            _rptExecutionService.Url = MyAdvAppSettings.AppSettings("ReportExecutionServices").ToString
        End Sub
#End Region

#Region "Initialize Export Formats"
        Private Shared Sub ConfigureExportFormat(ByRef strType As String)
            Select Case strType.ToUpper()
                Case "PDF"
                    _strExtension = "pdf"
                    _strMimeType = "application/pdf"
                    Exit Select
                Case "EXCEL", "XLS"
                    _strExtension = "xls"
                    _strMimeType = "application/vnd.excel"
                    Exit Select
                Case "WORD"
                    _strExtension = "doc"
                    _strMimeType = "application/vnd.ms-word"
                Case "CSV"
                    _strExtension = "csv"
                    _strMimeType = "text/csv"
                Case Else
                    Throw New Exception("Unrecognized type: " & strType & ". Type must be PDF, Excel or Image, HTML.")
            End Select
        End Sub
#End Region

#Region "Build Report Parameters"
        Private Function BuildReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As MedianLoanDebtSelectionValues, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}
            Dim strNullValue As String = "Null"
            Dim iParam As Integer = 0

            '  GetConnectionStringValues()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString).ToLower


            With getReportParametersObj
                dtStartDate = .StartDate
                dtEndDate = .EndDate
                strCampusId = .CampusId
                strProgramIdList = .ProgramIdList
                strRevGradDateType = .revGraddateType
                strServerName = GetServerName(strConnectionString)
                strDBName = GetDatabaseName(strConnectionString)
                strUserName = GetUserName(strConnectionString)
                strPassword = GetPassword(strConnectionString)
                ShowPageNumber = .ShowPageNumber
                boolShowDateInFooter = .ShowDateInFooter

            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then
                'Server Name   0
                iParam = 0
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "servername"
                rptExecutionParamValues(iParam).Name = "servername"
                rptExecutionParamValues(iParam).Value = strServerName

                'Database Name   1
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "databasename"
                rptExecutionParamValues(iParam).Name = "databasename"
                rptExecutionParamValues(iParam).Value = strDBName

                'User id      2
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "uid"
                rptExecutionParamValues(iParam).Name = "uid"
                rptExecutionParamValues(iParam).Value = strUserName

                'password     3
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "password"
                rptExecutionParamValues(iParam).Name = "password"
                rptExecutionParamValues(iParam).Value = strPassword

                'Start Date   4
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "StartDate"
                rptExecutionParamValues(iParam).Name = "StartDate"
                rptExecutionParamValues(iParam).Value = dtStartDate.ToString

                'End Date     5
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "EndDate"
                rptExecutionParamValues(iParam).Name = "EndDate"
                rptExecutionParamValues(iParam).Value = dtEndDate.ToString

                'CampusId     6
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "CampusId"
                rptExecutionParamValues(iParam).Name = "CampusId"
                rptExecutionParamValues(iParam).Value = strCampusId

                'ProgramIdList  7
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "ProgramIdList"
                rptExecutionParamValues(iParam).Name = "ProgramIdList"
                rptExecutionParamValues(iParam).Value = strProgramIdList

                'RevGradDateType    8    On Time Graduation Date By Option ('fromprogram', 'graddatefromenroll' )
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "RevGradDateType"
                rptExecutionParamValues(iParam).Name = "RevGradDateType"
                rptExecutionParamValues(iParam).Value = strRevGradDateType

                'ShowPageNumber     9
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "ShowPageNumber"
                rptExecutionParamValues(iParam).Name = "ShowPageNumber"
                If ShowPageNumber = False Then
                    rptExecutionParamValues(iParam).Value = "false"
                Else
                    rptExecutionParamValues(iParam).Value = "true"
                End If

                'ShowDateInFooter  10
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "ShowDateInFooter"
                rptExecutionParamValues(10).Name = "ShowDateInFooter"
                If boolShowDateInFooter = True Then
                    rptExecutionParamValues(iParam).Value = "true"
                Else
                    rptExecutionParamValues(iParam).Value = "false"
                End If

                'SchoolName     11    Title
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "SchoolName"
                rptExecutionParamValues(iParam).Name = "SchoolName"
                If Not strSchoolName.Trim = "" Then
                    rptExecutionParamValues(iParam).Value = strSchoolName
                Else
                    rptExecutionParamValues(iParam).Value = Nothing
                End If
            End If
            Return rptExecutionParamValues
        End Function
#End Region

#Region "Build Report"
        Public Function RenderReport(ByVal _format As String, _
                                     ByVal getReportParametersObj As MedianLoanDebtSelectionValues, _
                                     ByVal strSchoolName As String) As [Byte]()

            'Initialize the report services and report execution services
            InitializeWebServices()


            'Set the path of report
            _strReportName = getReportParametersObj.Environment & getReportParametersObj.ReportPath
            'Use the Report Services: GetReportParameters method to get a list of parameters for a specified report
            'This is the only location where the reportingservices2005 is used
            _rptParameters = _rptService.GetReportParameters(_strReportName, _strHistoryId, _boolForRendering, _rptParameterValues, _credentials)

            'Need to load the specific report before passing the parameter values
            _rptExecutionInfoObj = _rptExecutionService.LoadReport(_strReportName, _strHistoryId)

            ' Prepare report parameter. 
            _rptExecutionServiceParameterValues = BuildReportParameters(_rptParameters, getReportParametersObj, strSchoolName)

            _rptExecutionService.Timeout = System.Threading.Timeout.Infinite

            'Set Report Parameters
            _rptExecutionService.SetExecutionParameters(_rptExecutionServiceParameterValues, "en-us")

            'Call ConfigureExportFormat to get the file extensions
            ConfigureExportFormat(_format)

            'Render the report
            _generateReportAsBytes = _rptExecutionService.Render(_format, _deviceInfo, _strExtension, _encoding, _strMimeType, _warnings, _streamIDs)

            Return _generateReportAsBytes
            'Display the report in the browser
            'DisplayReport(_generateReportAsBytes)
        End Function
#End Region

#Region "DisplayReport"
        Private Sub DisplayReport(ByVal byteReportOutput As [Byte]())
            'Pdf
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.ContentType = _strMimeType
            HttpContext.Current.Response.BinaryWrite(byteReportOutput)
        End Sub
        Function GetDatabaseName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("database=")
            If startIndex > -1 Then startIndex += 9
            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""
            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length
            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetServerName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("server=")
            If startIndex > -1 Then startIndex += 7

            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetUserName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("uid=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetPassword(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("pwd=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
#End Region

    End Class
End Namespace



