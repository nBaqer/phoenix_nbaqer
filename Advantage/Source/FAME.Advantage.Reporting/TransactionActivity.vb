﻿
Imports FAME.Advantage.Reporting.Info
Imports FAME.Parameters.Info

Namespace Logic
    Public Class TransactionActivity
        Dim StartDate As Date
        Dim CampusId As String
        Dim CampDescrip As String
        Dim EndDate As Date
        Dim TransType As String
        Dim boolShowPageNumber As Boolean = True
        Dim boolShowDateInFooter As Boolean = False

        Public Function BuildReport(ByVal Rpt As ReportInfo, ByVal conn As String, ByVal UserId As String, _
                                                                 ByVal strGradingMethod As String, _
                                                                ByVal strGPAMethod As String, _
                                                                ByVal strSchoolName As String, _
                                                                ByVal strTrackAttendanceBy As String, _
                                                                ByVal strGradeBookWeightingLevel As String, _
                                                                 ByVal strStudentIdentifier As String, _
                                                                ByVal DisplayAttendanceUnitForProgressReportByClass As String, _
                                                                ByVal strReportPath As String) As [Byte]()
            Try
                Dim objReportInfo As ReportInfo = Rpt
                Dim getReportOutput As [Byte]()
                For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                    Select Case tabview.ParamSet.SetType
                        Case "Filter"
                        Case "Sort"
                        Case "Custom"
                            For Each item As ParamItemUserSettingsInfo In Rpt.CustomOptions
                                'Response.Write(item.ControlSettingsCollection.Item)
                                For Each drillItem As ControlSettingInfo In item.ControlSettingsCollection

                                    For Each controlValueItem As ControlValueInfo In drillItem.ControlValueCollection
                                        Dim strKeyName As String = ""
                                        Dim strKeyValue As String = ""
                                        Dim strDisplayText As String = ""

                                        Select Case drillItem.ControlName.Trim.ToString.ToLower
                                            Case Is = "radcombocampus"
                                                strKeyName = "CampusId"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "raddatestartdate"
                                                strKeyName = "StartDate"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "raddateenddate"
                                                strKeyName = "EndDate"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radlistboxtranstype2"
                                                strKeyName = "TransTypeId"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)

                                        End Select
                                    Next
                                Next
                            Next
                        Case Else
                            Throw New Exception("Unknown Type")
                    End Select
                Next

                If Rpt.PagingOption = ReportInfo.PagingStyle.None Then boolShowPageNumber = False
                If Rpt.ReportDateOption = ReportInfo.ReportDateStyle.Yes Then boolShowDateInFooter = True

                Dim getSelectedValues As New TransactionActivitySelectionValues
                With getSelectedValues

                    .CampusId = CampusId
                    .CampDescrip = CampDescrip
                    .StartDate = StartDate
                    .EndDate = EndDate
                    .TransTypeId = TransType
                    .ReportPath = objReportInfo.RDLName
                    .ResourceId = objReportInfo.ResourceId
                    .ShowPageNumber = boolShowPageNumber
                    .ShowDateInFooter = boolShowDateInFooter
                    .StudentIdentifier = strStudentIdentifier
                    .Environment = strReportPath
                End With
                Dim strFormat As String = ""
                Select Case Rpt.ExportFormat
                    Case ReportInfo.ExportType.PDF
                        strFormat = "Pdf"
                    Case ReportInfo.ExportType.Excel
                        strFormat = "Excel"
                    Case ReportInfo.ExportType.CSV
                        strFormat = "csv"
                End Select
                getReportOutput = CallRenderReport(strFormat, getSelectedValues, strSchoolName)
                Return getReportOutput
            Catch ex As Exception
                Throw ex
            End Try
            Return Nothing
        End Function
        Private Sub BuildParamtersForReport(ByVal strFilterFieldName As String, _
                                            ByVal strFilterSelectedValue As String, _
                                            ByVal strDisplayText As String)
            Select Case strFilterFieldName.ToLower.Trim
                Case "campusid"
                    If strFilterSelectedValue.ToString = "" Then
                        CampusId = Nothing
                        CampDescrip = Nothing
                    Else
                        CampusId = strFilterSelectedValue.ToString
                        CampDescrip = strDisplayText.ToString
                    End If
                Case "startdate"
                    If strFilterSelectedValue.ToString = "" Then
                        StartDate = CDate("01/01/" + (Date.Now.Year).ToString)
                    Else
                        StartDate = CDate(strFilterSelectedValue.ToString)
                    End If
                Case "enddate"
                    If strFilterSelectedValue.ToString = "" Then
                        EndDate = CDate("12/31/" + (Date.Now.Year).ToString)
                    Else
                        EndDate = CDate(strFilterSelectedValue.ToString)
                    End If
                Case "transtypeid"
                    If strFilterSelectedValue.ToString = "" Then
                        TransType = Nothing
                    Else
                        TransType &= strFilterSelectedValue.ToString & ","
                    End If

            End Select
        End Sub
        Public Function CallRenderReport(ByVal format As String, ByVal getFilterValues As TransactionActivitySelectionValues, ByVal strSchoolName As String) As [Byte]()
            Dim getReportOutput As [Byte]()
            Dim generateReport As New TransactionActivityReportGenerator
            getReportOutput = generateReport.RenderReport(format, getFilterValues, strSchoolName)
            Return getReportOutput
        End Function
        Private Function ConvertModifierFromTextToSymbol(ByVal Modifier As String) As String
            Dim strReturnSymbol As String = ""
            Select Case Modifier.Trim.ToLower
                Case "isequalto"
                    strReturnSymbol = "="
                Case "greaterthanorequalto"
                    strReturnSymbol = ">="
                Case "greaterthan"
                    strReturnSymbol = ">"
                Case "lessthanorequalto"
                    strReturnSymbol = "<="
                Case "lessthan"
                    strReturnSymbol = "<"
            End Select
            Return strReturnSymbol
        End Function
    End Class
End Namespace

