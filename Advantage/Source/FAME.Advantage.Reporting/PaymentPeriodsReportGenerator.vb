﻿Imports FAME.Advantage.Reporting.ReportService2005
Imports FAME.Advantage.Reporting.ReportExecution2005
Imports System.Configuration
Imports System.Web
Imports FAME.Advantage.Reporting.Info
Imports FAME.Advantage.Reporting
Imports FAME.Advantage.Common
Namespace Logic
    Public Class PaymentPeriodsReportGenerator
#Region "Report Variables"
        Private _rptService As New ReportingService2005
        Private _rptExecutionService As New ReportExecution2005.ReportExecutionService

        Private _strReportName As String = Nothing
        Private _strHistoryId As String = Nothing
        Private _boolForRendering As Boolean = False
        Private _credentials As ReportService2005.DataSourceCredentials() = Nothing
        Private _rptParameterValues As ReportService2005.ParameterValue() = Nothing
        Private _rptParameters As ReportService2005.ReportParameter() = Nothing
        Private _rptExecutionServiceParameterValues As ReportExecution2005.ParameterValue() = Nothing
        Private _rptExecutionInfoObj As New ExecutionInfo

        'Variables needed to render report
        Private _historyId As String = Nothing
        Private _deviceInfo As String = Nothing
        Private _format As String = Nothing
        Private _generateReportAsBytes As [Byte]()
        Private _encoding As String = [String].Empty
        Private Shared _strExtension As String = String.Empty
        Private Shared _strMimeType As String = String.Empty
        Private _warnings As ReportExecution2005.Warning() = Nothing
        Private _streamIDs As String() = Nothing

        Dim strServerName As String = ""
        Dim strDBName As String = ""
        Dim strUserName As String = ""
        Dim strPassword As String = ""
#End Region

#Region "Initialize Web Services"
        Private Sub InitializeWebServices()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString).ToLower
            _rptService.Credentials = System.Net.CredentialCache.DefaultCredentials
            _rptService.Url = MyAdvAppSettings.AppSettings("ReportServices").ToString
            'Set the credentials and url for the report execution services
            _rptExecutionService.Credentials = System.Net.CredentialCache.DefaultCredentials
            '_rptExecutionService.SetExecutionCredentials(dsCredentials)
            _rptExecutionService.Url = MyAdvAppSettings.AppSettings("ReportExecutionServices").ToString
        End Sub
#End Region

#Region "Initialize Export Formats"
        Private Shared Sub ConfigureExportFormat(ByRef strType As String)
            Select Case strType.ToUpper()
                Case "PDF"
                    _strExtension = "pdf"
                    _strMimeType = "application/pdf"
                    Exit Select
                Case "EXCEL", "XLS", "EXCELOPENXML", "XLSX"
                    _strExtension = "xlsx"
                    _strMimeType = "application/vnd.excel"
                    Exit Select
                Case "WORD"
                    _strExtension = "doc"
                    _strMimeType = "application/vnd.ms-word"
                Case "CSV"
                    _strExtension = "csv"
                    _strMimeType = "text/csv"
                Case Else
                    Throw New Exception("Unrecognized type: " & strType & ". Type must be PDF, Excel or Image, HTML.")
            End Select
        End Sub
#End Region

#Region "Build Report Parameters"
        Private Function BuildReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As PaymentPeriodsReportSelectionValues, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}
            Dim strNullValue As String = "Null"

            Dim strCampGrpId As String = ""
            Dim strPrgVerId As String = ""
            Dim strEnrollmentStatus As String = ""
            Dim strAsOfDate As String = ""

            Dim strCampusId As String = ""
            Dim strReqId As String = ""
            Dim intNumberofDays As Integer = 0
            Dim dtCutOffDate As DateTime

            Dim strServerName As String = ""
            Dim strDBName As String = ""
            Dim strUserName As String = ""
            Dim strPassword As String = ""
            Dim strProgramId As String = ""
            Dim strTermId As String = ""
            Dim strChargingMethodId As String = ""
            Dim strTransStartDate As String = ""
            Dim strTransEndDate As String = ""
            Dim boolShowSummary As Boolean = False

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString).ToLower

            '  GetConnectionStringValues()

            With getReportParametersObj
                strPrgVerId = .PrgVerId
                strReqId = .ReqId
                strCampusId = .CampusId
                strProgramId = .ProgramId
                strTermId = .TermId
                strTransStartDate = .TransStartDate.ToShortDateString
                strTransEndDate = .TransEndDate.ToShortDateString
                strChargingMethodId = .ChargingMethodId
                strServerName = GetServerName(strConnectionString)
                strDBName = GetDatabaseName(strConnectionString)
                strUserName = GetUserName(strConnectionString)
                strPassword = GetPassword(strConnectionString)
                boolShowSummary = .ShowSummary
            End With

            If InStrRev(strReqId, ",") >= 1 Then
                strReqId = strReqId.Substring(0, strReqId.Length - 1)
            End If

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                'Server Name
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "ServerName"
                rptExecutionParamValues(0).Name = "ServerName"
                rptExecutionParamValues(0).Value = strServerName

                'Database Name
                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "DBName"
                rptExecutionParamValues(1).Name = "DBName"
                rptExecutionParamValues(1).Value = strDBName
                '  rptExecutionParamValues(4).Value = "[" + strDBName + "]"

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "UserName"
                rptExecutionParamValues(2).Name = "UserName"
                rptExecutionParamValues(2).Value = strUserName

                'password
                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "Password"
                rptExecutionParamValues(3).Name = "Password"
                rptExecutionParamValues(3).Value = strPassword

                'CampusId Parameter
                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "CampusId"
                rptExecutionParamValues(4).Name = "CampusId"
                rptExecutionParamValues(4).Value = strCampusId.ToString

                'CampusId Parameter
                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "ProgramId"
                rptExecutionParamValues(5).Name = "ProgramId"
                If String.IsNullOrEmpty(strProgramId) Then
                    rptExecutionParamValues(5).Value = Nothing
                Else
                    rptExecutionParamValues(5).Value = strProgramId.ToString
                End If

                'ProgramVersion Parameter
                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ProgramVersion"
                rptExecutionParamValues(6).Name = "ProgramVersion"
                If String.IsNullOrEmpty(strPrgVerId) Then
                    rptExecutionParamValues(6).Value = Nothing
                Else
                    rptExecutionParamValues(6).Value = strPrgVerId.ToString
                End If


                'TermId Parameter
                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "TermId"
                rptExecutionParamValues(7).Name = "TermId"
                If String.IsNullOrEmpty(strTermId) Then
                    rptExecutionParamValues(7).Value = Nothing
                Else
                    rptExecutionParamValues(7).Value = strTermId.ToString
                End If



                'BillingMethod Parameter
                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "ChargingMethodId"
                rptExecutionParamValues(8).Name = "ChargingMethodId"
                If String.IsNullOrEmpty(strChargingMethodId) Then
                    rptExecutionParamValues(8).Value = Nothing
                Else
                    rptExecutionParamValues(8).Value = strChargingMethodId.ToString
                End If

               'Trans Start Date Parameter
                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "TransStartDate"
                rptExecutionParamValues(9).Name = "TransStartDate"
                If Year(CDate(strTransStartDate)) = 1900 Then
                    rptExecutionParamValues(9).Value = Nothing
                Else
                    rptExecutionParamValues(9).Value = strTransStartDate.ToString
                End If


                'Trans Start Date Parameter
                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "TransEndDate"
                rptExecutionParamValues(10).Name = "TransEndDate"
                If Year(CDate(strTransEndDate)) = 1900 Then
                    rptExecutionParamValues(10).Value = Nothing
                Else
                    rptExecutionParamValues(10).Value = strTransEndDate.ToString
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "ShowSummary"
                rptExecutionParamValues(11).Name = "ShowSummary"
                If boolShowSummary = True Then
                    rptExecutionParamValues(11).Value = "true"
                Else
                    rptExecutionParamValues(11).Value = "false"
                End If

            End If
            Return rptExecutionParamValues
        End Function
#End Region

#Region "Build Report"
        Public Function RenderReport(ByVal _format As String, _
                                                                    ByVal getReportParametersObj As PaymentPeriodsReportSelectionValues, _
                                                                    ByVal strSchoolName As String) As [Byte]()

            'Initialize the report services and report execution services
            InitializeWebServices()



            'Set the path of report
            _strReportName = getReportParametersObj.Environment + getReportParametersObj.ReportPath
            'Use the Report Services: GetReportParameters method to get a list of parameters for a specified report
            'This is the only location where the reportingservices2005 is used
            _rptParameters = _rptService.GetReportParameters(_strReportName, _strHistoryId, _boolForRendering, _rptParameterValues, _credentials)

            'Need to load the specific report before passing the parameter values
            _rptExecutionInfoObj = _rptExecutionService.LoadReport(_strReportName, _strHistoryId)

            ' Prepare report parameter. 
            _rptExecutionServiceParameterValues = BuildReportParameters(_rptParameters, getReportParametersObj, strSchoolName)

            _rptExecutionService.Timeout = System.Threading.Timeout.Infinite

            'Set Report Parameters
            _rptExecutionService.SetExecutionParameters(_rptExecutionServiceParameterValues, "en-us")

            'Call ConfigureExportFormat to get the file extensions
            ConfigureExportFormat(_format)

            'Render the report
            Try
                'If Excel version is 2003
                If _format = "xlsx" Or _format = "xls" Then
                    _generateReportAsBytes = _rptExecutionService.Render("EXCEL", _deviceInfo, _strExtension, _encoding, _strMimeType, _warnings, _streamIDs)
                Else
                    _generateReportAsBytes = _rptExecutionService.Render(_format, _deviceInfo, _strExtension, _encoding, _strMimeType, _warnings, _streamIDs)
                End If
            Catch ex As Exception
                'If Excel 2013 then use EXCELOPENXML 
                If _format = "xlsx" Then
                    _generateReportAsBytes = _rptExecutionService.Render("EXCELOPENXML", _deviceInfo, _strExtension, _encoding, _strMimeType, _warnings, _streamIDs)
                End If
            End Try
            '_generateReportAsBytes = _rptExecutionService.Render("EXCELOPENXML", _deviceInfo, _strExtension, _encoding, _strMimeType, _warnings, _streamIDs)


            Return _generateReportAsBytes
            'Display the report in the browser
            'DisplayReport(_generateReportAsBytes)
        End Function
#End Region

#Region "DisplayReport"
        Private Sub DisplayReport(ByVal byteReportOutput As [Byte]())
            'Pdf
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.ContentType = _strMimeType
            HttpContext.Current.Response.BinaryWrite(byteReportOutput)
        End Sub
        Function GetDatabaseName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("database=")
            If startIndex > -1 Then startIndex += 9
            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""
            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length
            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetServerName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("server=")
            If startIndex > -1 Then startIndex += 7

            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetUserName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("uid=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetPassword(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("pwd=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
#End Region
    End Class
End Namespace



