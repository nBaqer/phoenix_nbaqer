﻿Imports System.Web
#Region "Render Interface"
Public Interface IRenderBehavior
    ReadOnly Property Format() As String
    ReadOnly Property DeviceInfo() As String
    ReadOnly Property FileExtension() As String
    ReadOnly Property Encoding() As String
    ReadOnly Property MimeType() As String
    ReadOnly Property Warnings() As ReportExecution2005.Warning()
    ReadOnly Property StreamId() As String()
    Function Render(ByVal _rptExecutionService As ReportExecution2005.ReportExecutionService) As [Byte]()
    Sub Display(ByVal httpContextObject As HttpContext)
End Interface
#End Region
#Region "Render Implementation"
Public Class RenderToPDF
    Implements IRenderBehavior
    Private _renderOutput As [Byte]() = Nothing
    Public ReadOnly Property DeviceInfo() As String Implements IRenderBehavior.DeviceInfo
        Get
            Return Nothing
        End Get
    End Property
    Public ReadOnly Property Encoding() As String Implements IRenderBehavior.Encoding
        Get
            Return String.Empty
        End Get
    End Property
    Public ReadOnly Property FileExtension() As String Implements IRenderBehavior.FileExtension
        Get
            Return "pdf"
        End Get
    End Property
    Public ReadOnly Property Format() As String Implements IRenderBehavior.Format
        Get
            Return "pdf"
        End Get
    End Property
    Public ReadOnly Property MimeType() As String Implements IRenderBehavior.MimeType
        Get
            Return "application/pdf"
        End Get
    End Property
    Public ReadOnly Property StreamId() As String() Implements IRenderBehavior.StreamId
        Get
            Return Nothing
        End Get
    End Property
    Public ReadOnly Property Warnings() As ReportExecution2005.Warning() Implements IRenderBehavior.Warnings
        Get
            Return Nothing
        End Get
    End Property
    Public Property RenderOutput() As [Byte]()
        Get
            RenderOutput = _renderOutput
        End Get
        Set(ByVal value As [Byte]())
            _renderOutput = value
        End Set
    End Property
    Public Function Render(ByVal _rptExecutionService As ReportExecution2005.ReportExecutionService) As [Byte]() Implements IRenderBehavior.Render
        RenderOutput = _rptExecutionService.Render(Format, DeviceInfo, FileExtension, _
                                                                                Encoding, MimeType, Warnings, StreamId)
        Return RenderOutput
    End Function
    Public Sub Display(ByVal httpContextObject As HttpContext) Implements IRenderBehavior.Display
        httpContextObject.Response.Clear()
        httpContextObject.Response.ContentType = MimeType
        httpContextObject.Response.BinaryWrite(RenderOutput)
    End Sub
End Class
Public Class RenderToExcel
    Implements IRenderBehavior
    Private _renderOutput As [Byte]() = Nothing
    Public ReadOnly Property DeviceInfo() As String Implements IRenderBehavior.DeviceInfo
        Get
            Return Nothing
        End Get
    End Property
    Public ReadOnly Property Encoding() As String Implements IRenderBehavior.Encoding
        Get
            Return String.Empty
        End Get
    End Property
    Public ReadOnly Property FileExtension() As String Implements IRenderBehavior.FileExtension
        Get
            Return "excel"
        End Get
    End Property
    Public ReadOnly Property Format() As String Implements IRenderBehavior.Format
        Get
            Return "excel"
        End Get
    End Property
    Public ReadOnly Property MimeType() As String Implements IRenderBehavior.MimeType
        Get
            Return "application/vnd.excel"
        End Get
    End Property
    Public ReadOnly Property StreamId() As String() Implements IRenderBehavior.StreamId
        Get
            Return Nothing
        End Get
    End Property
    Public ReadOnly Property Warnings() As ReportExecution2005.Warning() Implements IRenderBehavior.Warnings
        Get
            Return Nothing
        End Get
    End Property
    Public Property RenderOutput() As [Byte]()
        Get
            RenderOutput = _renderOutput
        End Get
        Set(ByVal value As [Byte]())
            _renderOutput = value
        End Set
    End Property
    Public Function Render(ByVal _rptExecutionService As ReportExecution2005.ReportExecutionService) As [Byte]() Implements IRenderBehavior.Render
        RenderOutput = _rptExecutionService.Render(Format, DeviceInfo, FileExtension, _
                                                                                Encoding, MimeType, Warnings, StreamId)
        Return RenderOutput
    End Function
    Public Sub Display(ByVal httpContextObject As HttpContext) Implements IRenderBehavior.Display
        Dim name As String = "AdvReport"
        httpContextObject.Response.Clear()
        httpContextObject.Response.ContentType = MimeType
        httpContextObject.Response.AddHeader("content-disposition", ("attachment; filename=" & name & ".") + FileExtension)
        httpContextObject.Response.BinaryWrite(RenderOutput)
        httpContextObject.Response.End()
    End Sub
End Class
Public Class RenderToCSV
    Implements IRenderBehavior
    Private _renderOutput As [Byte]() = Nothing
    Public ReadOnly Property DeviceInfo() As String Implements IRenderBehavior.DeviceInfo
        Get
            Return Nothing
        End Get
    End Property
    Public ReadOnly Property Encoding() As String Implements IRenderBehavior.Encoding
        Get
            Return String.Empty
        End Get
    End Property
    Public ReadOnly Property FileExtension() As String Implements IRenderBehavior.FileExtension
        Get
            Return "csv"
        End Get
    End Property
    Public ReadOnly Property Format() As String Implements IRenderBehavior.Format
        Get
            Return "csv"
        End Get
    End Property
    Public ReadOnly Property MimeType() As String Implements IRenderBehavior.MimeType
        Get
            Return "text/csv"
        End Get
    End Property
    Public ReadOnly Property StreamId() As String() Implements IRenderBehavior.StreamId
        Get
            Return Nothing
        End Get
    End Property
    Public ReadOnly Property Warnings() As ReportExecution2005.Warning() Implements IRenderBehavior.Warnings
        Get
            Return Nothing
        End Get
    End Property
    Public Property RenderOutput() As [Byte]()
        Get
            RenderOutput = _renderOutput
        End Get
        Set(ByVal value As [Byte]())
            _renderOutput = value
        End Set
    End Property
    Public Function Render(ByVal _rptExecutionService As ReportExecution2005.ReportExecutionService) As [Byte]() Implements IRenderBehavior.Render
        RenderOutput = _rptExecutionService.Render(Format, DeviceInfo, FileExtension, _
                                                                                Encoding, MimeType, Warnings, StreamId)
        Return RenderOutput
    End Function
    Public Sub Display(ByVal httpContextObject As HttpContext) Implements IRenderBehavior.Display
        Dim name As String = "AdvReport"
        httpContextObject.Response.Clear()
        httpContextObject.Response.ContentType = MimeType
        httpContextObject.Response.AddHeader("content-disposition", ("attachment; filename=" & name & ".") + FileExtension)
        httpContextObject.Response.BinaryWrite(RenderOutput)
        httpContextObject.Response.End()
    End Sub
End Class
Public Class RenderToWord
    Implements IRenderBehavior
    Private _renderOutput As [Byte]() = Nothing
    Public ReadOnly Property DeviceInfo() As String Implements IRenderBehavior.DeviceInfo
        Get
            Return Nothing
        End Get
    End Property
    Public ReadOnly Property Encoding() As String Implements IRenderBehavior.Encoding
        Get
            Return String.Empty
        End Get
    End Property
    Public ReadOnly Property FileExtension() As String Implements IRenderBehavior.FileExtension
        Get
            Return "doc"
        End Get
    End Property
    Public ReadOnly Property Format() As String Implements IRenderBehavior.Format
        Get
            Return "doc"
        End Get
    End Property
    Public ReadOnly Property MimeType() As String Implements IRenderBehavior.MimeType
        Get
            Return "application/vnd.ms-word"
        End Get
    End Property
    Public ReadOnly Property StreamId() As String() Implements IRenderBehavior.StreamId
        Get
            Return Nothing
        End Get
    End Property
    Public ReadOnly Property Warnings() As ReportExecution2005.Warning() Implements IRenderBehavior.Warnings
        Get
            Return Nothing
        End Get
    End Property
    Public Property RenderOutput() As [Byte]()
        Get
            RenderOutput = _renderOutput
        End Get
        Set(ByVal value As [Byte]())
            _renderOutput = value
        End Set
    End Property
    Public Function Render(ByVal _rptExecutionService As ReportExecution2005.ReportExecutionService) As [Byte]() Implements IRenderBehavior.Render
        RenderOutput = _rptExecutionService.Render(Format, DeviceInfo, FileExtension, _
                                                                                Encoding, MimeType, Warnings, StreamId)
        Return RenderOutput
    End Function
    Public Sub Display(ByVal httpContextObject As HttpContext) Implements IRenderBehavior.Display
        Dim name As String = "AdvReport"
        httpContextObject.Response.Clear()
        httpContextObject.Response.ContentType = MimeType
        httpContextObject.Response.AddHeader("content-disposition", ("attachment; filename=" & name & ".") + FileExtension)
        httpContextObject.Response.BinaryWrite(RenderOutput)
        httpContextObject.Response.End()
    End Sub
End Class
#End Region