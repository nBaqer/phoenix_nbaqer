﻿
Imports FAME.Advantage.Reporting.ReportService2005
Imports FAME.Advantage.Reporting.ReportExecution2005
Imports System.Web
Imports FAME.Advantage.Reporting.Info
Imports FAME.Advantage.Common
Namespace Logic
    Public Class TranscriptReportGenerator
#Region "Report Variables"
        Private _rptService As New ReportingService2005
        Private _rptExecutionService As New ReportExecution2005.ReportExecutionService

        Private _strReportName As String = Nothing
        Private _strHistoryId As String = Nothing
        Private _boolForRendering As Boolean = False
        Private _credentials As ReportService2005.DataSourceCredentials() = Nothing
        Private _rptParameterValues As ReportService2005.ParameterValue() = Nothing
        Private _rptParameters As ReportService2005.ReportParameter() = Nothing
        Private _rptExecutionServiceParameterValues As ReportExecution2005.ParameterValue() = Nothing
        Private _rptExecutionInfoObj As New ExecutionInfo

        'Variables needed to render report
        Private _historyId As String = Nothing
        Private _deviceInfo As String = Nothing
        Private _format As String = Nothing
        Private _generateReportAsBytes As [Byte]()
        Private _encoding As String = [String].Empty
        Private Shared _strExtension As String = String.Empty
        Private Shared _strMimeType As String = String.Empty
        Private _warnings As ReportExecution2005.Warning() = Nothing
        Private _streamIDs As String() = Nothing


#End Region

#Region "Initialize Web Services"
        Private Sub InitializeWebServices()


            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString).ToLower

            _rptService.Credentials = System.Net.CredentialCache.DefaultCredentials
            _rptService.Url = MyAdvAppSettings.AppSettings("ReportServices").ToString
            'Set the credentials and url for the report execution services
            _rptExecutionService.Credentials = System.Net.CredentialCache.DefaultCredentials
            '_rptExecutionService.SetExecutionCredentials(dsCredentials)
            _rptExecutionService.Url = MyAdvAppSettings.AppSettings("ReportExecutionServices").ToString
        End Sub
#End Region

#Region "Initialize Export Formats"
        Private Shared Sub ConfigureExportFormat(ByRef strType As String)
            Select Case strType.ToUpper()
                Case "PDF"
                    _strExtension = "pdf"
                    _strMimeType = "application/pdf"
                    Exit Select
                Case "EXCEL", "XLS"
                    _strExtension = "EXCEL"
                    _strMimeType = "application/vnd.excel"
                    Exit Select
                Case "WORD", "DOC"
                    _strExtension = "WORD"
                    _strMimeType = "application/vnd.ms-word"
                Case "CSV"
                    _strExtension = "csv"
                    _strMimeType = "text/csv"
                Case Else
                    Throw New Exception("Unrecognized type: " & strType & ". Type must be PDF, Excel or Image, HTML.")
            End Select
        End Sub
#End Region

#Region "Build Report Parameters"
        ' ReSharper disable once InconsistentNaming
        Private Function BuildReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                               ByVal getTRSelectedValues As TranscriptReportSelectionValues _
                                               ) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}
            Dim iParam As Integer = 0
            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConnectionString").ToString).ToLower

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                'user name
                'Server Name   0
                iParam = 0
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "servername"
                rptExecutionParamValues(iParam).Name = "servername"
                rptExecutionParamValues(iParam).Value = GetServerName(strConnectionString)

                'Database Name   1
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "databasename"
                rptExecutionParamValues(iParam).Name = "databasename"
                rptExecutionParamValues(iParam).Value = GetDatabaseName(strConnectionString)

                'User id      2
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "uid"
                rptExecutionParamValues(iParam).Name = "uid"
                rptExecutionParamValues(iParam).Value = GetUserName(strConnectionString)

                'password    3
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "password"
                rptExecutionParamValues(iParam).Name = "password"
                rptExecutionParamValues(iParam).Value = GetPassword(strConnectionString)

                'StuEnrollIdList    4
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Student Enrollment Id List"
                rptExecutionParamValues(iParam).Name = "StuEnrollIdList"
                If Not getTRSelectedValues.StuEnrollIdList.Trim() = String.Empty Then
                    rptExecutionParamValues(iParam).Value = getTRSelectedValues.StuEnrollIdList
                Else
                    rptExecutionParamValues(iParam).Value = Nothing
                End If

                'CampusId   5
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Campus Id"
                rptExecutionParamValues(iParam).Name = "CampusId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not getTRSelectedValues.CampusId.Trim = "" Then
                    rptExecutionParamValues(iParam).Value = getTRSelectedValues.CampusId
                Else
                    rptExecutionParamValues(iParam).Value = Nothing
                End If

                'OrderBy    6
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Order By"
                rptExecutionParamValues(iParam).Name = "OrderBy"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.SortFields.ToString()

                'StatusList   7
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Status List"
                rptExecutionParamValues(iParam).Name = "StatusList"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not getTRSelectedValues.StatusCodeIdList.Trim() = "" Then
                    rptExecutionParamValues(iParam).Value = getTRSelectedValues.StatusCodeIdList
                Else
                    rptExecutionParamValues(iParam).Value = Nothing
                End If

                ''No logo or School Information
                'iParam += 1
                'rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                ''rptExecutionParamValues(iParam).Label = "No logo or School Information"
                'rptExecutionParamValues(iParam).Name = "NoLogoOrSchooInformation"
                'rptExecutionParamValues(iParam).Value = getTRSelectedValues.OfficialLogoPosition

                'Logo Position (Logo Options)      8
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Official Logo Position"
                rptExecutionParamValues(iParam).Name = "OfficialLogoPosition"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.OfficialLogoPosition

                'ShowSchoolName      9
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show School Name"
                rptExecutionParamValues(iParam).Name = "ShowSchoolName"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowSchoolName.ToString()

                'ShowCampusAddress    10
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show School Address"
                rptExecutionParamValues(iParam).Name = "ShowCampusAddress"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowCampusAddress.ToString()

                'ShowPhone     11
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Phone"
                rptExecutionParamValues(iParam).Name = "ShowSchoolPhone"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowSchoolPhone.ToString()

                'ShowFax      12
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Fax"
                rptExecutionParamValues(iParam).Name = "ShowSchoolFax"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowSchoolFax.ToString()

                'ShowWebSite    13
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Web Site"
                rptExecutionParamValues(iParam).Name = "ShowSchoolWebSite"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowSchoolWebSite.ToString()

                'IsOfficialTranscript    14
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Is Official Transcript?"
                rptExecutionParamValues(iParam).Name = "IsOfficialTranscript"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.IsOfficialTranscript.ToString()

                'ShowCurrentDate      15
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Current Date"
                rptExecutionParamValues(iParam).Name = "ShowCurrentDate"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowCurrentDate.ToString()

                ''ShowFlodMark
                'iParam += 1
                'rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                ''rptExecutionParamValues(iParam).Label = "Show Flod Mark"
                'rptExecutionParamValues(iParam).Name = "ShowFlodMark"
                'rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowCurrentDate.ToString()


                'ShowSSN        16
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show SSN"
                rptExecutionParamValues(iParam).Name = "ShowSSN"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowSSN.ToString()

                'ShowStudentPhone    17
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Student Phone"
                rptExecutionParamValues(iParam).Name = "ShowStudentPhone"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowStudentPhone.ToString()

                'ShowStudentDOB       18
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Student DOB"
                rptExecutionParamValues(iParam).Name = "ShowStudentDOB"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowStudentDOB.ToString()

                'ShowStudentEmail    19
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Student Email"
                rptExecutionParamValues(iParam).Name = "ShowStudentEmail"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowStudentEmail.ToString()

                'ShowMultipleEnrollments     20
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Multiple Programas on Single Transcript"
                rptExecutionParamValues(iParam).Name = "ShowMultipleEnrollments"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowMultipleEnrollments.ToString()

                'ShowGraduationDate    21
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Graduation Date"
                rptExecutionParamValues(iParam).Name = "ShowGraduationDate"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowGraduationDate.ToString()

                'ShowLDA               22
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Last Day of Atendance"
                rptExecutionParamValues(iParam).Name = "ShowLDA"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowLDA.ToString()

                'ShowCompletedHours     23
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Completed Hours"
                rptExecutionParamValues(iParam).Name = "ShowCompletedHours"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowCompletedHours.ToString()

                'CoursesLayout          24 ' Values: "ShowTerms/Modules", "ShowCoursesOnly" or "ShowCourseCategories" layout
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Courses Layout"
                rptExecutionParamValues(iParam).Name = "CoursesLayout"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.CoursesLayout  ' Values: "ShowTerms/Modules", "ShowCoursesOnly" or "ShowCourseCategories" layout

                ' Show Terms Description     25
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Terms Description"
                rptExecutionParamValues(iParam).Name = "ShowTermDescription"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowTermDescription.ToString()

                ' Show course components      26
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show course components"
                rptExecutionParamValues(iParam).Name = "ShowCourseComponents"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowCourseComponents.ToString()

                'ShowDateIssuedColumn         27
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Date Issued"
                rptExecutionParamValues(iParam).Name = "ShowDateIssuedColumn"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowDateIssuedColumn.ToString()

                'ShowCreditsColumn           28
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Credits Column"
                rptExecutionParamValues(iParam).Name = "ShowCreditsColumn"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowCreditsColumn.ToString()

                'ShowHoursColumn           29
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Hours Column"
                rptExecutionParamValues(iParam).Name = "ShowHoursColumn"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowHoursColumn.ToString()

                'ShowCourseSummary        30
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Course Summary"
                rptExecutionParamValues(iParam).Name = "ShowCourseSummary"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowCourseSummary.ToString()

                'ShowGradePoints         31
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Grade Points in Summary"
                rptExecutionParamValues(iParam).Name = "ShowGradePoints"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowGradePoints.ToString()

                'ShowGradeScale          32
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Grade Scale"
                rptExecutionParamValues(iParam).Name = "ShowGradeScale"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowGradeScale.ToString()

                'ShowStudentSignatureLine     33
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Student Signature Line"
                rptExecutionParamValues(iParam).Name = "ShowStudentSignatureLine"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowStudentSignatureLine.ToString()

                'ShowSchoolOfficialSignatureLine      34
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show School Official Signature Line"
                rptExecutionParamValues(iParam).Name = "ShowSchoolOfficialSignatureLine"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowSchoolOfficialSignatureLine.ToString()

                'ShowPageNumber      35
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show School Official Signature Line"
                rptExecutionParamValues(iParam).Name = "ShowPageNumber"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowPageNumber.ToString()

                'ShowDisclaimer           36
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Show Disclaimer"
                rptExecutionParamValues(iParam).Name = "ShowDisclaimer"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.ShowDisclaimer.ToString()

                'DisclaimerText           37
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(iParam).Label = "Disclaimer Text"
                rptExecutionParamValues(iParam).Name = "DisclaimerText"
                rptExecutionParamValues(iParam).Value = getTRSelectedValues.DisclaimerText.ToString()

            End If

            Return rptExecutionParamValues
        End Function

#End Region

#Region "Build Report"
        Public Function RenderReport(ByVal _format As String, _
                                     ByVal getTRParametersObj As TranscriptReportSelectionValues _
                                    ) As [Byte]()

            'Initialize the report services and report execution services
            InitializeWebServices()

            'Set the path of report
            If (getTRParametersObj.NoLogoOrSchooInformation = False) Then
                _strReportName = getTRParametersObj.Environment & getTRParametersObj.ReportPath.ToString.Trim & "/TR_Sub00_Main"
            Else
                _strReportName = getTRParametersObj.Environment & getTRParametersObj.ReportPath.ToString.Trim & "/TR_Sub00_Main_NoLogoOrSchoolInformation"
            End If

            'Use the Report Services: GetReportParameters method to get a list of parameters for a specified report
            'This is the only location where the reportingservices2005 is used
            _rptParameters = _rptService.GetReportParameters(_strReportName, _strHistoryId, _boolForRendering, _rptParameterValues, _credentials)

            'Need to load the specific report before passing the parameter values
            _rptExecutionInfoObj = _rptExecutionService.LoadReport(_strReportName, _strHistoryId)

            ' Prepare report parameter. 
            _rptExecutionServiceParameterValues = BuildReportParameters(_rptParameters, getTRParametersObj)

            _rptExecutionService.Timeout = System.Threading.Timeout.Infinite

            'Set Report Parameters
            _rptExecutionService.SetExecutionParameters(_rptExecutionServiceParameterValues, "en-us")

            'Call ConfigureExportFormat to get the file extensions
            ConfigureExportFormat(_format)

            'Render the report
            _generateReportAsBytes = _rptExecutionService.Render(_format, _deviceInfo, _strExtension, _encoding, _strMimeType, _warnings, _streamIDs)

            Return _generateReportAsBytes
            'Display the report in the browser
            'DisplayReport(_generateReportAsBytes)
        End Function
#End Region

#Region "DisplayReport"
        'While designing the report in BIDS make sure the following rule of thumb is followed
        ' (BodyWidth+LeftMargin+RightMargin) <= (PageWidth).
        'The PageWidth, LeftMargin and RightMargin can be set by selecting report - properties.
        'If the rule is not followed, the table will be truncated or blank pages will be inserted
        Private Sub DisplayReport(ByVal byteReportOutput As [Byte]())
            'Pdf
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.ContentType = _strMimeType
            HttpContext.Current.Response.BinaryWrite(byteReportOutput)

            'CSV
            'Dim name As String = "AdvReport"
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.ContentType = _strMimeType
            'HttpContext.Current.Response.AddHeader("content-disposition", ("attachment; filename=" & name & ".") + _strExtension)
            'HttpContext.Current.Response.BinaryWrite(byteReportOutput)

        End Sub

        Function GetDatabaseName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("database=")
            If startIndex > -1 Then startIndex += 9
            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""
            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length
            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex).Trim()
        End Function
        Function GetServerName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("server=")
            If startIndex > -1 Then startIndex += 7

            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex).Trim()
        End Function
        Function GetUserName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("uid=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex).Trim()
        End Function
        Function GetPassword(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("password=")
            If startIndex = -1 Then
                startIndex = lcConnString.IndexOf("pwd=")
                If startIndex > -1 Then
                    startIndex += 4
                End If
            Else
                startIndex += 9
            End If
            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex).Trim()
        End Function
#End Region
    End Class
End Namespace

