﻿Public Class StudentGpaReportSelectionValues
#Region "Variables"
    Private _termId As String
    Private _reqId As String
    Private _cutOffDate As Date
    Private _numberOfDaysMissed As Integer
    Private _ReportPath As String
    Private _environment As String
    Private _campusId As String
    Private _programversionname As String
    Private _prgverid As String
    Private _statuscodeid As String
    Private _studentgrpid As String
    Private _cumGpagt As Decimal
    Private _cumGpalt As Decimal
    Private _termCreditGt As Decimal
    Private _termCreditLt As Decimal
    Private _termGpagt As Decimal
    Private _termGpalt As Decimal
    'Private _boolShowGpa As Boolean = False
    Private _pStudentIdentifier As String = ""
#End Region
#Region "Address Initialize"
    Public Sub New()

    End Sub
#End Region
#Region "Properties"
    Public Property StudentIdentifier() As String
        Get
            Return _pStudentIdentifier
        End Get
        Set(ByVal value As String)
            _pStudentIdentifier = value
        End Set
    End Property
    Public Property PrgVerId() As String
        Get
            Return _prgverid
        End Get
        Set(ByVal value As String)
            _prgverid = value
        End Set
    End Property
    Public Property CampusId() As String
        Get

            Return _campusId
        End Get
        Set(ByVal value As String)
            _campusId = value
        End Set
    End Property
    Public Property ReqId() As String
        Get
            Return _reqId
        End Get
        Set(ByVal value As String)
            _reqId = value
        End Set
    End Property
    Public Property ReportPath() As String
        Get

            Return _ReportPath
        End Get
        Set(ByVal Value As String)
            _ReportPath = Value
        End Set
    End Property
    Public Property Environment() As String
        Get

            Return _environment
        End Get
        Set(ByVal Value As String)
            _environment = Value
        End Set
    End Property
    Public Property TermId() As String
        Get
            Return _termId
        End Get
        Set(ByVal Value As String)
            _termId = Value
        End Set
    End Property
    Public Property StatusCodeId() As String
        Get
            Return _statuscodeid
        End Get
        Set(ByVal Value As String)
            _statuscodeid = Value
        End Set
    End Property
    Public Property StudentGrpId() As String
        Get
            Return _studentgrpid
        End Get
        Set(ByVal Value As String)
            _studentgrpid = Value
        End Set
    End Property
    Public Property CumulativeGpagt() As Decimal
        Get
            Return _cumGPAGT
        End Get
        Set(ByVal Value As Decimal)
            _cumGPAGT = Value
        End Set
    End Property
    Public Property CumulativeGpalt() As Decimal
        Get
            Return _cumGPALT
        End Get
        Set(ByVal Value As Decimal)
            _cumGPALT = Value
        End Set
    End Property
    Public Property TermCreditGt() As Decimal
        Get
            Return _termCreditGT
        End Get
        Set(ByVal Value As Decimal)
            _termCreditGT = Value
        End Set
    End Property
    Public Property TermCreditLt() As Decimal
        Get
            Return _termCreditLT
        End Get
        Set(ByVal Value As Decimal)
            _termCreditLT = Value
        End Set
    End Property
    Public Property TermGpagt() As Decimal
        Get
            Return _termGPAGT
        End Get
        Set(ByVal Value As Decimal)
            _termGPAGT = Value
        End Set
    End Property
    Public Property TermGpalt() As Decimal
        Get
            Return _termGPALT
        End Get
        Set(ByVal Value As Decimal)
            _termGPALT = Value
        End Set
    End Property
    'Public Property ShowGpAbyTerm() As Boolean
    '    Get
    '        Return _boolShowGpa
    '    End Get
    '    Set(value As Boolean)
    '        _boolShowGpa = value
    '    End Set
    'End Property
#End Region
End Class