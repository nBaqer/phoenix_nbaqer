﻿Namespace Info
    Public Class MedianLoanDebtSelectionValues
#Region "Variables"
    Private _StartDate As Date
    Private _EndDate As Date
    Private _CampusId As String
    Private _ProgramIdList As String
    Private _RevGradDateType As String
    Private _boolShowPageNumber As Boolean = True
    Private _boolShowDateInFooter As Boolean = False
    Private _Environment As String
    Private _ReportPath As String
    Private _ResourceId As Integer
#End Region
#Region "Address Initialize"
    Public Sub New()
        _StartDate = Date.MinValue
        _EndDate = Date.MinValue
        _CampusId = ""
        _ProgramIdList = ""
        _RevGradDateType = ""
        _boolShowPageNumber = True
        _boolShowDateInFooter = False
        _Environment = ""
        _ReportPath = ""
        _ResourceId = 0
    End Sub
#End Region
#Region "Properties"
    Public Property StartDate() As Date
        Get
            Return _StartDate
        End Get
        Set(ByVal Value As Date)
            _StartDate = Value
        End Set
    End Property
    Public Property EndDate() As Date
        Get
            Return _EndDate
        End Get
        Set(ByVal Value As Date)
            _EndDate = Value
        End Set
    End Property
    Public Property CampusId() As String
        Get
            Return _CampusId
        End Get
        Set(ByVal Value As String)
            _CampusId = Value
        End Set
    End Property
    Public Property ProgramIdList() As String
        Get
            Return _ProgramIdList
        End Get
        Set(ByVal Value As String)
            _ProgramIdList = Value
        End Set
    End Property
    Public Property revGraddateType() As String
        Get
            Return _RevGradDateType
        End Get
        Set(ByVal Value As String)
            _RevGradDateType = Value
        End Set
    End Property
    Public Property ShowPageNumber() As Boolean
        Get
            Return _boolShowPageNumber
        End Get
        Set(ByVal Value As Boolean)
            _boolShowPageNumber = Value
        End Set
    End Property
    Public Property ShowDateInFooter() As Boolean
        Get
            Return _boolShowDateInFooter
        End Get
        Set(ByVal Value As Boolean)
            _boolShowDateInFooter = Value
        End Set
    End Property
    Public Property Environment() As String
        Get
            Return _Environment
        End Get
        Set(ByVal Value As String)
            _Environment = Value
        End Set
    End Property
    Public Property ReportPath() As String
        Get
            Return _ReportPath
        End Get
        Set(ByVal Value As String)
            _ReportPath = Value
        End Set
    End Property
    Public Property ResourceId() As Integer
        Get
            Return _ResourceId
        End Get
        Set(ByVal Value As Integer)
            _ResourceId = Value
        End Set
    End Property
#End Region
    End Class

End Namespace

