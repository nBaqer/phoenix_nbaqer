﻿Imports System.Web
Imports FAME.Advantage.Common
Imports FAME.Advantage.Reporting.ReportExecution2005
Imports FAME.Advantage.Reporting.ReportService2005


Public Class ReportGenerator2
#Region "Report Variables"
    Private myAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private ReadOnly rptService As New ReportingService2005
    Private ReadOnly rptExecutionService As New ReportExecutionService
    'Private rptExecutionInfoObj As New ExecutionInfo

    Private strReportName As String = Nothing
    Private ReadOnly strHistoryId As String = Nothing
    Private Const BOOL_FOR_RENDERING As Boolean = False
    Private ReadOnly credentials As ReportService2005.DataSourceCredentials() = Nothing
    Private ReadOnly rptParameterValues As ReportService2005.ParameterValue() = Nothing
    Private rptParameters As ReportService2005.ReportParameter() = Nothing
    Private rptExecutionServiceParameterValues As ReportExecution2005.ParameterValue() = Nothing
    'Variables needed to render report
    'Private _historyId As String = Nothing
    Private ReadOnly deviceInfo As String = Nothing
    'Private format As String = Nothing
    Private generateReportAsBytes As [Byte]()
    Private encoding As String = [String].Empty
    Private Shared _strExtension As String = String.Empty
    Private Shared _strMimeType As String = String.Empty
    Private warnings As ReportExecution2005.Warning() = Nothing
    Private streamIDs As String() = Nothing
    'Private _strDateRangeText As String
    Dim ReadOnly strConnectionString As String = (myAdvAppSettings.AppSettings("ConString").ToString).ToLower
    'Dim strSchoolName As String = myAdvAppSettings.AppSettings("SchoolName").ToString
    Dim ReadOnly strServerName As String = GetServerName(strConnectionString)
    Dim ReadOnly strDBName As String = GetDatabaseName(strConnectionString)
    Dim ReadOnly strUserName As String = GetUserName(strConnectionString)
    Dim ReadOnly strPassword As String = GetPassword(strConnectionString)
#End Region

#Region "Initialize Web Services"
    Private Sub InitializeWebServices()

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        'Set the credentials and url for the report services
        'The report service object is needed to get the parameter details associated with a specific report
        rptService.Credentials = Net.CredentialCache.DefaultCredentials
        rptService.Url = myAdvAppSettings.AppSettings("ReportServices").ToString

        'Set the credentials and url for the report execution services
        rptExecutionService.Credentials = Net.CredentialCache.DefaultCredentials
        rptExecutionService.Url = myAdvAppSettings.AppSettings("ReportExecutionServices").ToString
    End Sub
#End Region

#Region "Initialize Export Formats"
    Private Shared Sub ConfigureExportFormat(ByRef strType As String)
        Select Case strType.ToUpper()
            Case "PDF"
                _strExtension = "pdf"
                _strMimeType = "application/pdf"
                Exit Select
            Case "EXCEL", "XLS"
                _strExtension = "xls"
                _strMimeType = "application/vnd.excel"
                Exit Select
            Case "WORD"
                _strExtension = "doc"
                _strMimeType = "application/vnd.ms-word"
            Case "CSV"
                _strExtension = "csv"
                _strMimeType = "text/csv"
            Case Else
                Throw New Exception("Unrecognized type: " & strType & ". Type must be PDF, Excel or Image, HTML.")
        End Select
    End Sub
#End Region

#Region "Utility Functions: Extraxt values from the supplied connection string"

    ''' <summary>
    ''' Get the database name from the connection string supplied
    ''' </summary>
    ''' <param name="connString">The connection string</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetDatabaseName(ByVal connString As String) As String
        Dim lcConnString = connString.ToLower()

        ' if this is a Jet database, find the index of the "data source" setting
        Dim startIndex As Integer
        startIndex = lcConnString.IndexOf("database=", StringComparison.Ordinal)
        If startIndex > -1 Then startIndex += 9
        ' if the "database", "data source" or "initial catalog" values are not 
        ' found, return an empty string
        If startIndex = -1 Then Return ""
        ' find where the database name/path ends
        Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex, StringComparison.Ordinal)
        If endIndex = -1 Then endIndex = lcConnString.Length
        ' return the substring with the database name/path
        Return connString.Substring(startIndex, endIndex - startIndex)
    End Function

    ''' <summary>
    ''' Get the server name from connection string.
    ''' </summary>
    ''' <param name="connString">Connection string to be used to comunicate with the service.</param>
    ''' <returns>The report server name, empty string if something is not found.</returns>
    Function GetServerName(ByVal connString As String) As String
        Dim lcConnString = connString.ToLower()

        ' if this is a Jet database, find the index of the "data source" setting
        Dim startIndex As Integer
        startIndex = lcConnString.IndexOf("server=", StringComparison.Ordinal)
        If startIndex > -1 Then startIndex += 7

        ' if the "database", "data source" or "initial catalog" values are not 
        ' found, return an empty string
        If startIndex = -1 Then Return ""

        ' find where the database name/path ends
        Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex, StringComparison.Ordinal)
        If endIndex = -1 Then endIndex = lcConnString.Length

        ' return the substring with the database name/path
        Return connString.Substring(startIndex, endIndex - startIndex)
    End Function

    ''' <summary>
    ''' Get The user name from connection string.
    ''' </summary>
    ''' <param name="connString"></param>
    ''' <returns>The username used in the connection string</returns>
    ''' <remarks></remarks>
    Function GetUserName(ByVal connString As String) As String
        Dim lcConnString = connString.ToLower()

        ' if this is a Jet database, find the index of the "data source" setting
        Dim startIndex As Integer
        startIndex = lcConnString.IndexOf("uid=", StringComparison.Ordinal)
        If startIndex > -1 Then startIndex += 4

        ' if the "uid" values are not 
        ' found, return an empty string
        If startIndex = -1 Then Return ""

        ' find where the database name/path ends
        Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex, StringComparison.Ordinal)
        If endIndex = -1 Then endIndex = lcConnString.Length

        ' return the substring with the database name/path
        Return connString.Substring(startIndex, endIndex - startIndex).Trim()
    End Function

    ''' <summary>
    ''' Get the password fron connection string
    ''' </summary>
    ''' <param name="connString">Connection String</param>
    ''' <returns>The password  extracted from connection string</returns>
    ''' <remarks></remarks>
    Function GetPassword(ByVal connString As String) As String
        Dim lcConnString = connString.ToLower()

        ' if this is a Jet database, find the index of the "data source" setting
        Dim startIndex As Integer
        startIndex = lcConnString.IndexOf("pwd=", StringComparison.Ordinal)
        If startIndex > -1 Then startIndex += 4

        ' if the "uid" values are not 
        ' found, return an empty string
        If startIndex = -1 Then Return ""

        ' find where the database name/path ends
        Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex, StringComparison.Ordinal)
        If endIndex = -1 Then endIndex = lcConnString.Length

        ' return the substring with the database name/path
        Return connString.Substring(startIndex, endIndex - startIndex)
    End Function
#End Region

#Region "Build Report"
    Public Function RenderReport2(ByVal format As String, _
                                  ByVal user As String, _
                                  ByVal strRole As String, _
                                  ByVal strAdvantageUserName As String, _
                                  ByVal strReportPathAndName As String, _
                                  ByRef listReportParameters As List(Of ReportParameters)) As [Byte]()

        'Initialize the report services and report execution services
        InitializeWebServices()

        'Set the path of report
        'StudentPaymentPlan > StudentPaymentPlan 
        strReportName = strReportPathAndName ' strReportName + '"StudentPaymentPlan/StudentPaymentPlan"

        'Use the Report Services: GetReportParameters method to get a list of parameters for a specified report
        'This is the only location where the reportingservices2005 is used
        rptParameters = rptService.GetReportParameters(strReportName, strHistoryId, BOOL_FOR_RENDERING, rptParameterValues, credentials)

        'Need to load the specific report before passing the parameter values
        'rptExecutionInfoObj = 
         rptExecutionService.LoadReport(strReportName, strHistoryId)
       
        rptExecutionServiceParameterValues = BuildSpecificReportParameters(listReportParameters)
        rptExecutionService.Timeout = Threading.Timeout.Infinite

        'Set Report Parameters
        rptExecutionService.SetExecutionParameters(rptExecutionServiceParameterValues, "en-us")

        'Call ConfigureExportFormat to get the file extensions
        ConfigureExportFormat(format)

        'Render the report
        generateReportAsBytes = rptExecutionService.Render(format, deviceInfo, _strExtension, encoding, _strMimeType, warnings, streamIDs)

        Return generateReportAsBytes
    End Function


    Private Function BuildSpecificReportParameters(params As ICollection(Of ReportParameters)) As ReportExecution2005.ParameterValue()
        Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptParameters.Length - 1) {}
        If rptParameters.Length - 4 <> params.Count() Then
            Throw New Exception("Param Number does not match with report parameters")
        End If
        Dim i As Int32 = 0
        For Each p As ReportParameters In params
            rptExecutionParamValues(i) = New ReportExecution2005.ParameterValue()
            rptExecutionParamValues(i).Label = p.Label
            rptExecutionParamValues(i).Name = p.Name
            If Not IsNothing(p.Value) AndAlso p.Value.Trim = "" Then
                rptExecutionParamValues(i).Value = Nothing
            Else
                rptExecutionParamValues(i).Value = p.Value
            End If
            i = i + 1
        Next

        'Enter the four specific parameters to construct the session
        rptExecutionParamValues(i) = New ReportExecution2005.ParameterValue()
        rptExecutionParamValues(i).Label = "servername"
        rptExecutionParamValues(i).Name = "servername"
        rptExecutionParamValues(i).Value = strServerName
        i = i + 1
        'Database Name         
        rptExecutionParamValues(i) = New ReportExecution2005.ParameterValue()
        rptExecutionParamValues(i).Label = "databasename"
        rptExecutionParamValues(i).Name = "databasename"
        rptExecutionParamValues(i).Value = strDBName
        i = i + 1
        rptExecutionParamValues(i) = New ReportExecution2005.ParameterValue()
        rptExecutionParamValues(i).Label = "uid"
        rptExecutionParamValues(i).Name = "uid"
        rptExecutionParamValues(i).Value = strUserName
        i = i + 1

        'password               
        rptExecutionParamValues(i) = New ReportExecution2005.ParameterValue()
        rptExecutionParamValues(i).Label = "password"
        rptExecutionParamValues(i).Name = "password"
        rptExecutionParamValues(i).Value = strPassword

        Return rptExecutionParamValues
    End Function

#End Region

    Private Function GetAdvAppSettings() As AdvAppSettings
        'Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If
        Return myAdvAppSettings
    End Function
End Class