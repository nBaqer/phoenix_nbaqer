﻿Namespace Info
    Public Class StudentInfo

        Private _StudentID As Guid
        Private _FirstName As String
        Private _LastName As String
        Private _StudentNumber As String
        Private _SSN As String
        Private _SSNSearch As String
        Private _StudentType As String

        Public Sub New(ByVal StudentID As Guid, ByVal FirstName As String, _
                       ByVal LastName As String, ByVal StudentNumber As String, _
                       ByVal SSN As String, ByVal SSNSearch As String, ByVal StudentType As String)

            _StudentID = StudentID
            _FirstName = FirstName
            _LastName = LastName
            _StudentNumber = StudentNumber
            _SSN = SSN
            _SSNSearch = SSNSearch
            _StudentType = StudentType
        End Sub

        Public ReadOnly Property StudentID() As Guid
            Get
                Return _StudentID
            End Get
        End Property

        Public ReadOnly Property FirstName() As String
            Get
                Return _FirstName
            End Get
        End Property

        Public ReadOnly Property LastName() As String
            Get
                Return _LastName
            End Get
        End Property

        Public ReadOnly Property StudentNumber() As String
            Get
                Return _StudentNumber
            End Get
        End Property

        Public ReadOnly Property SSN() As String
            Get
                Return _SSN
            End Get
        End Property

        Public ReadOnly Property SSNSearch() As String
            Get
                Return _SSNSearch
            End Get
        End Property

        Public ReadOnly Property StudentType() As String
            Get
                Return _StudentType
            End Get
        End Property
    End Class
End Namespace

