﻿Imports System.Web
Imports FAME.Advantage.Common
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Domain.MultiTenant.Infrastructure.API
Imports FAME.Advantage.Reporting.Info
Imports FAME.Parameters.Info
Imports FAME.Advantage.Api.Library.Models
Imports FAME.Advantage.Reporting
Namespace Logic
    Public Class StateBoardReport
        Dim PrgVerId As String = ""
        Dim CampusId As String = ""
        Dim StateId As String = ""
        Dim ReportId As String = ""
        Dim StudentGroupId As String = ""
        Dim Month As String = ""
        Dim Year As String = ""
        Protected MyAdvAppSettings As AdvAppSettings
        Public Function BuildReport(ByVal Rpt As ReportInfo, ByVal conn As String, ByVal UserId As String,
                                    ByVal strGradingMethod As String,
                                    ByVal strGPAMethod As String,
                                    ByVal strSchoolName As String,
                                    ByVal strTrackAttendanceBy As String,
                                    ByVal strGradeBookWeightingLevel As String,
                                    ByVal strStudentIdentifier As String,
                                    ByVal DisplayAttendanceUnitForProgressReportByClass As String,
                                    ByVal strReportPath As String) As [Byte]()
            Dim TestText As String = String.Empty
            Try
                Dim objReportInfo As ReportInfo = Rpt
                Dim getReportOutput As [Byte]()
                For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                    Select Case tabview.ParamSet.SetType
                        Case "Filter"
                        Case "Sort"
                        Case "Custom"
                            For Each item As ParamItemUserSettingsInfo In Rpt.CustomOptions
                                'Response.Write(item.ControlSettingsCollection.Item)
                                For Each drillItem As ControlSettingInfo In item.ControlSettingsCollection
                                    For Each controlValueItem As ControlValueInfo In drillItem.ControlValueCollection
                                        Dim strKeyName As String = ""
                                        Dim strKeyValue As String = ""
                                        Dim strDisplayText As String = ""
                                        Select Case drillItem.ControlName.Trim.ToString.ToLower
                                            Case Is = "radcombocampus"
                                                strKeyName = "campusid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radcombostate"
                                                strKeyName = "stateid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radcomboreport"
                                                strKeyName = "reportid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radcombomonth"
                                                strKeyName = "month"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radcomboyear"
                                                strKeyName = "year"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radcombostudentgroup"
                                                strKeyName = "studentgroupid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radcomboprogramversion"
                                                strKeyName = "programversionid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                        End Select
                                    Next
                                Next
                            Next
                        Case Else
                            Throw New Exception("Unknown Type")
                    End Select
                Next
                Dim getSelectedValues As New StateBoardReportSelectionValues
                Dim resourceId = 0

                Dim rdlName As String
                Dim canCastResourceId = IsNumeric(ReportId)
                If canCastResourceId Then
                    resourceId = CType(ReportId, Integer)
                    rdlName = GetReportRDL(resourceId)
                Else
                    rdlName = objReportInfo.RDLName
                End If

                If (rdlName Is Nothing) Then
                    Return Nothing
                End If
                Dim token As TokenResponse
                Dim apiToken As String
                Dim apiUrl As String
                token = CType(HttpContext.Current.Session("AdvantageApiToken"), TokenResponse)

                If (Not token Is Nothing) Then
                    apiToken = token.Token
                    apiUrl = token.ApiUrl
                End If

                With getSelectedValues
                    .CampusId = CampusId.ToString
                    .StateId = StateId.ToString()
                    .Month = Month.ToString()
                    .Year = Year.ToString
                    .ApiToken = apiToken
                    .ApiUrl = apiUrl
                    .StudentGroupId = StudentGroupId.ToString()
                    .ReportPath = rdlName
                    .Environment = strReportPath
                    .ResourceId = resourceId
                    If Not PrgVerId.Trim = "-1" Then
                        .PrgVerId = PrgVerId.ToString
                    End If
                End With
                'intCheckDataExists = GetProgressDA.GetProgressReportRecordCount(strStuEnrollId, strCampGrpId, strPrgVerId, strStatusCodeId, strTermId, strStudentGrpId, dtStartString, UserId.ToString, strStartStringModifier)
                'If intCheckDataExists >= 1 Then
                Dim strFormat As String = ""
                Select Case Rpt.ExportFormat
                    Case ReportInfo.ExportType.PDF
                        strFormat = "Pdf"
                    Case ReportInfo.ExportType.Excel
                        strFormat = "xls"
                    Case ReportInfo.ExportType.CSV
                        strFormat = "csv"
                End Select
                getReportOutput = CallRenderReport(strFormat, getSelectedValues, strSchoolName)
                Return getReportOutput
                'Else
                '    Return Nothing
                'End If
                'Return Nothing
            Catch ex As Exception
                Throw ex
            End Try
            Return Nothing
        End Function
        Private Sub BuildParamtersForReport(ByVal strFilterFieldName As String,
                                            ByVal strFilterSelectedValue As String,
                                            ByVal strDisplayText As String)
            Select Case strFilterFieldName.ToLower.Trim
                Case "campusid"
                    CampusId = strFilterSelectedValue.ToString
                Case "reportid"
                    ReportId = strFilterSelectedValue.ToString
                Case "stateid"
                    StateId = strFilterSelectedValue.ToString
                Case "month"
                    Month = strFilterSelectedValue.ToString
                Case "year"
                    Year = strFilterSelectedValue.ToString
                Case "studentgroupid"
                    StudentGroupId = strFilterSelectedValue.ToString
                Case "programversionid"
                    PrgVerId = strFilterSelectedValue.ToString
            End Select
        End Sub


        Public Function CallRenderReport(ByVal format As String, ByVal getFilterValues As StateBoardReportSelectionValues, ByVal strSchoolName As String) As [Byte]()
            Dim getReportOutput As [Byte]()
            Dim generateReport As New StateBoardReportGenerator
            getReportOutput = generateReport.RenderReport(format, getFilterValues, strSchoolName)
            Return getReportOutput
        End Function

        Private Function GetReportRDL(ByVal ResID As Integer) As String
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            Dim rdl = (New ReportsDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)).GetRDL(ResID)
            Return rdl
        End Function
    End Class
End Namespace