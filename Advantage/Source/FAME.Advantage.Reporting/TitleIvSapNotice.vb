﻿Imports System.Web
Imports FAME.Advantage.Common
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Reporting.Info
Imports FAME.Advantage.Reporting.Logic
Imports FAME.Parameters.Info
Namespace Logic
    Public Class TitleIvSapNotice


        Protected MyAdvAppSettings As AdvAppSettings
        Dim StatusId As String = ""
        Dim Studentenrollmentlist As String = ""
        Public Function BuildReport(ByVal Rpt As ReportInfo, ByVal conn As String, ByVal UserId As String,
                                    ByVal strGradingMethod As String,
                                    ByVal strGPAMethod As String,
                                    ByVal strSchoolName As String,
                                    ByVal strTrackAttendanceBy As String,
                                    ByVal strGradeBookWeightingLevel As String,
                                    ByVal strStudentIdentifier As String,
                                    ByVal DisplayAttendanceUnitForProgressReportByClass As String,
                                    ByVal strReportPath As String) As [Byte]()
            Try
                Dim objReportInfo As ReportInfo = Rpt
                Dim getReportOutput As [Byte]()
                For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                    Select Case tabview.ParamSet.SetType
                        Case "Filter"
                        Case "Sort"
                        Case "Custom"
                            For Each item As ParamItemUserSettingsInfo In Rpt.CustomOptions
                                'Response.Write(item.ControlSettingsCollection.Item)
                                For Each drillItem As ControlSettingInfo In item.ControlSettingsCollection
                                    For Each controlValueItem As ControlValueInfo In drillItem.ControlValueCollection
                                        Dim strKeyName As String = ""
                                        Dim strKeyValue As String = ""
                                        Dim strDisplayText As String = ""
                                        Select Case drillItem.ControlName.Trim.ToString.ToLower
                                            'Case Is = "radlistboxcmpgrp2"
                                            '    strKeyName = "campusgroupid"
                                            '    strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                            '    BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            'Case Is = "radlistboxcampus2"
                                            '    strKeyName = "campusid"
                                            '    strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                            '    BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            'Case Is = "radlistboxprogram2"
                                            '    strKeyName = "programid"
                                            '    strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                            '    BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            'Case Is = "radlistboxtitleivstatuses2"
                                            '    strKeyName = "statusId"
                                            '    strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                            '    BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            'Case Is = "radlistboxactiveenrollments2"
                                            '    strKeyName = "enrollmentid"
                                            '    strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                            '    BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radgridselectedstuenrollments"
                                                strKeyName = "studentenrollmentlist"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)

                                        End Select

                                    Next
                                Next
                            Next
                        Case Else
                            Throw New Exception("Unknown Type")
                    End Select
                Next
                Dim getSelectedValues As New TitleIvSapNoticeSelectionValues
                With getSelectedValues
                    .ReportPath = objReportInfo.RDLName
                    .Environment = strReportPath
                    .Studentenrollmentlist = If(String.IsNullOrEmpty(Studentenrollmentlist), Guid.Empty.ToString(), Studentenrollmentlist)
                    .StatusId = StatusId
                End With
                Dim strFormat As String = ""
                Select Case Rpt.ExportFormat
                    Case ReportInfo.ExportType.PDF
                        strFormat = "Pdf"
                    Case ReportInfo.ExportType.Excel
                        strFormat = "xls"
                    Case ReportInfo.ExportType.CSV
                        strFormat = "csv"
                End Select
                getReportOutput = CallRenderReport(strFormat, getSelectedValues, strSchoolName)
                Return getReportOutput
            Catch ex As Exception
                Throw ex
            End Try
            Return Nothing
        End Function

        Private Sub BuildParamtersForReport(ByVal strFilterFieldName As String,
                                            ByVal strFilterSelectedValue As String,
                                            ByVal strDisplayText As String)
            Select Case strFilterFieldName.ToLower.Trim
                Case "studentenrollmentlist"
                    Studentenrollmentlist &= strFilterSelectedValue.ToString & ","
                Case "radlistboxtitleivstatuses2"
                    StatusId &= strFilterSelectedValue.ToString & ","
            End Select
        End Sub
        Public Function CallRenderReport(ByVal format As String, ByVal getFilterValues As TitleIvSapNoticeSelectionValues, ByVal strSchoolName As String) As [Byte]()
            Dim getReportOutput As [Byte]()

            Dim generateReport As New TitleIvSapNoticeReportGenerator
            'Update printed flag
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            'report generation
            getReportOutput = generateReport.RenderReport(format, getFilterValues, strSchoolName)

            ' if report was generated and mark for each enrollment
            If getReportOutput.Length > 0 Then
                Dim DA As New TitleIVResultsDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
                DA.UpdatePrintedFlagForEnrollment(getFilterValues.Studentenrollmentlist)
            End If

            Return getReportOutput
        End Function
    End Class
End Namespace