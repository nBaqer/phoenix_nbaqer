﻿Namespace Info
    Public Class IPEDReportSelectionValue
#Region "Variables"
        Private _CampusId As String
        Private _ProgId As String
        Private _PrgGrpId As String
        Private _CohortYear As String
        Private _OfficialReportingDate As String
        Private _SortFields As String
        Private _SchoolType As String
        Private _StartDate As String
        Private _EndDate As String
        Private _PriorToDate As String
        Private _MissingDataSortId As Integer
        Private _ResourceId As Integer
        Private _ReportPath As String
        Private _CampusName As String
        Private _StudentIdentifier As String
        Private _ReportingYear As String
        Private _institutionType As String
        Private _largeprogramId As String
        Private _largeProgramName As String
        Private _Environment As String
        Private _SchoolTypeList As String
        Private _FullorFall As String
        Private _CohortType As String
#End Region
#Region "Address Initialize"
        Public Sub New()
            _CampusId = ""
            _ProgId = ""
            _PrgGrpId = ""
            _CohortYear = "2009"
            _OfficialReportingDate = ""
            _SortFields = ""
            _SchoolType = ""
            _StartDate = Nothing
            _EndDate = Nothing
            _PriorToDate = Nothing
            _MissingDataSortId = 1
            _ResourceId = 0
            _ReportPath = ""
            _CampusName = ""
            _StudentIdentifier = "ssn"
            _ReportingYear = ""
            _institutionType = ""
            _largeprogramId = ""
            _largeProgramName = ""
            _SchoolTypeList = ""
            _FullorFall = ""
            _CohortType=""
        End Sub
#End Region
#Region "Properties"
        Public Property CohortType() As String
            Get
                Return _CohortType
            End Get
            Set(ByVal Value As String)
                _CohortType = Value
            End Set
        End Property
        Public Property SchoolTypeList() As String
            Get
                Return _SchoolTypeList
            End Get
            Set(ByVal Value As String)
                _SchoolTypeList = Value
            End Set
        End Property
         Public Property FullorFall() As String
            Get
                Return _FullorFall
            End Get
            Set(ByVal Value As String)
                _FullorFall = Value
            End Set
        End Property
        Public Property LargeProgramName() As String
            Get
                Return _largeProgramName
            End Get
            Set(ByVal Value As String)
                _largeProgramName = Value
            End Set
        End Property
        Public Property StartDate() As String
            Get
                Return _StartDate
            End Get
            Set(ByVal Value As String)
                _StartDate = Value
            End Set
        End Property
        Public Property EndDate() As String
            Get
                Return _EndDate
            End Get
            Set(ByVal Value As String)
                _EndDate = Value
            End Set
        End Property
        Public Property PriorToDate() As String
            Get
                Return _PriorToDate
            End Get
            Set(ByVal Value As String)
                _PriorToDate = Value
            End Set
        End Property
        Public Property OfficialReportingDate() As String
            Get
                Return _OfficialReportingDate
            End Get
            Set(ByVal Value As String)
                _OfficialReportingDate = Value
            End Set
        End Property
        Public Property LargeProgramId() As String
            Get
                Return _largeprogramId
            End Get
            Set(ByVal Value As String)
                _largeprogramId = Value
            End Set
        End Property
        Public Property AreasOfInterestId() As String
            Get
                Return _PrgGrpId
            End Get
            Set(ByVal Value As String)
                _PrgGrpId = Value
            End Set
        End Property
        Public Property CohortYear() As String
            Get
                Return _CohortYear
            End Get
            Set(ByVal Value As String)
                _CohortYear = Value
            End Set
        End Property
        Public Property ProgramId() As String
            Get
                Return _ProgId
            End Get
            Set(ByVal Value As String)
                _ProgId = Value
            End Set
        End Property
        Public Property CampusId() As String
            Get
                Return _CampusId
            End Get
            Set(ByVal Value As String)
                _CampusId = Value
            End Set
        End Property
        Public Property SortFields() As String
            Get
                Return _SortFields
            End Get
            Set(ByVal Value As String)
                _SortFields = Value
            End Set
        End Property
        Public Property SchoolType() As String
            Get
                Return _SchoolType
            End Get
            Set(ByVal Value As String)
                _SchoolType = Value
            End Set
        End Property
        Public Property MissingDataSortId() As Integer
            Get
                Return _MissingDataSortId
            End Get
            Set(ByVal Value As Integer)
                _MissingDataSortId = Value
            End Set
        End Property
        Public Property ResourceId() As Integer
            Get
                Return _ResourceId
            End Get
            Set(ByVal Value As Integer)
                _ResourceId = Value
            End Set
        End Property
        Public Property ReportPath() As String
            Get
                Return _ReportPath
            End Get
            Set(ByVal Value As String)
                _ReportPath = Value
            End Set
        End Property
        Public Property CampusName() As String
            Get
                Return _CampusName
            End Get
            Set(ByVal Value As String)
                _CampusName = Value
            End Set
        End Property
        Public Property StudentIdentifier() As String
            Get
                Return _StudentIdentifier
            End Get
            Set(ByVal Value As String)
                _StudentIdentifier = Value
            End Set
        End Property
        Public Property ReportingYear() As String
            Get
                Return _ReportingYear
            End Get
            Set(ByVal Value As String)
                _ReportingYear = Value
            End Set
        End Property
        Public Property InstitutionType() As String
            Get
                Return _institutionType
            End Get
            Set(ByVal Value As String)
                _institutionType = Value
            End Set
        End Property
        Public Property Environment() As String
            Get
                Return _Environment
            End Get
            Set(ByVal Value As String)
                _Environment = Value
            End Set
        End Property
#End Region
    End Class
End Namespace
