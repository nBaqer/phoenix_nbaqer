﻿Public Class TitleIvSapNoticeSelectionValues
#Region "Variables"
    Private _studentenrollmentlist As String
    Private _statusId As String
    Private _ReportPath As String
    Private _environment As String
#End Region
#Region "Address Initialize"
    Public Sub New()

    End Sub
#End Region
#Region "Properties"
    Public Property Studentenrollmentlist() As String
        Get
            Return _studentenrollmentlist
        End Get
        Set(ByVal value As String)
            _studentenrollmentlist = value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal value As String)
            _statusId = value
        End Set
    End Property
    Public Property ReportPath() As String
        Get
            Return _ReportPath
        End Get
        Set(ByVal value As String)
            _ReportPath = value
        End Set
    End Property
    Public Property Environment() As String
        Get
            Return _environment
        End Get
        Set(ByVal value As String)
            _environment = value
        End Set
    End Property
#End Region
End Class
