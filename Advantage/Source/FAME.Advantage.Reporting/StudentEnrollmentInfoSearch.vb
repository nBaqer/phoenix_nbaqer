﻿Namespace Info
    <Serializable()>
    Public Class StudentEnrollmentInfoSearch
        Private _StuEnrollId As Guid
        Private _EnrollmentId As String
        Private _StudentId As System.Nullable(Of System.Guid)
        Private _SearchDisplay As String
        Private _FullName As String
        Private _FirstName As String
        Private _LastName As String
        Private _SSN As String
        Private _SSNSearch As String
        Private _StudentNumber As String
        Private _PrgVerId As System.Nullable(Of System.Guid)
        Private _PrgVerDescrip As String
        Private _ShiftId As System.Nullable(Of System.Guid)
        Private _ShiftDescrip As String
        Private _StatusCodeId As System.Nullable(Of System.Guid)
        Private _StatusCodeDescrip As String
        Private _SysStatusId As Integer
        Private _CampGrpId As System.Nullable(Of System.Guid)
        Private _LeadGrpId As System.Nullable(Of System.Guid)
        Private _StudentType As String
        Private _IsTransHold As Boolean

        Public Sub New(ByVal StuEnrollId As Guid, ByVal EnrollmentId As String, ByVal StudentID As System.Nullable(Of System.Guid), ByVal SearchDisplay As String,
                       ByVal FullName As String, ByVal FirstName As String, ByVal LastName As String, ByVal SSN As String, ByVal SSNSearch As String,
                       ByVal StudentNumber As String, ByVal PrgVerId As System.Nullable(Of System.Guid), ByVal PrgVerDescrip As String,
                       ByVal ShiftId As System.Nullable(Of System.Guid), ByVal ShiftDescrip As String, ByVal StatusCodeId As System.Nullable(Of System.Guid),
                       ByVal StatusCodeDescrip As String, ByVal SysStatusId As Integer, ByVal CampGrpId As System.Nullable(Of System.Guid),
                       ByVal LeadGrpId As System.Nullable(Of System.Guid), ByVal StudentType As String, ByVal IsTransHold As Boolean)

            _StuEnrollId = StuEnrollId
            _EnrollmentId = EnrollmentId
            _StudentId = StudentID
            _SearchDisplay = SearchDisplay
            _FullName = FullName
            _FirstName = FirstName
            _LastName = LastName
            _SSN = SSN
            _SSNSearch = SSNSearch
            _StudentNumber = StudentNumber
            _PrgVerId = PrgVerId
            _PrgVerDescrip = PrgVerDescrip
            _ShiftId = ShiftId
            _ShiftDescrip = ShiftDescrip
            _StatusCodeId = StatusCodeId
            _StatusCodeDescrip = StatusCodeDescrip
            _SysStatusId = SysStatusId
            _CampGrpId = CampGrpId
            _LeadGrpId = LeadGrpId
            _StudentType = StudentType
            _IsTransHold = IsTransHold
        End Sub

        Public ReadOnly Property StuEnrollId() As Guid
            Get
                Return _StuEnrollId
            End Get
        End Property

        Public ReadOnly Property EnrollmentId() As String
            Get
                Return _EnrollmentId
            End Get
        End Property

        Public ReadOnly Property StudentID() As System.Nullable(Of System.Guid)
            Get
                Return _StudentId
            End Get
        End Property

        Public ReadOnly Property SearchDisplay() As String
            Get
                Return _SearchDisplay
            End Get
        End Property

        Public ReadOnly Property FullName() As String
            Get
                Return _FullName
            End Get
        End Property

        Public ReadOnly Property FirstName() As String
            Get
                Return _FirstName
            End Get
        End Property

        Public ReadOnly Property LastName() As String
            Get
                Return _LastName
            End Get
        End Property

        Public ReadOnly Property SSN() As String
            Get
                Return _SSN
            End Get
        End Property

        Public ReadOnly Property SSNSearch() As String
            Get
                Return _SSNSearch
            End Get
        End Property

        Public ReadOnly Property StudentNumber() As String
            Get
                Return _StudentNumber
            End Get
        End Property

        Public ReadOnly Property PrgVerId() As System.Nullable(Of System.Guid)
            Get
                Return _PrgVerId
            End Get
        End Property

        Public ReadOnly Property PrgVerDescrip() As String
            Get
                Return _PrgVerDescrip
            End Get
        End Property

        Public ReadOnly Property ShiftId() As System.Nullable(Of System.Guid)
            Get
                Return _ShiftId
            End Get
        End Property

        Public ReadOnly Property ShiftDescrip() As String
            Get
                Return _ShiftDescrip
            End Get
        End Property

        Public ReadOnly Property StatusCodeId() As System.Nullable(Of System.Guid)
            Get
                Return _StatusCodeId
            End Get
        End Property

        Public ReadOnly Property StatusCodeDescrip() As String
            Get
                Return _StatusCodeDescrip
            End Get
        End Property

        Public ReadOnly Property SysStatusId() As Integer
            Get
                Return _SysStatusId
            End Get
        End Property

        Public ReadOnly Property CampGrpId() As System.Nullable(Of System.Guid)
            Get
                Return _CampGrpId
            End Get
        End Property
        Public ReadOnly Property LeadGrpId() As System.Nullable(Of System.Guid)
            Get
                Return _LeadGrpId
            End Get
        End Property
        Public ReadOnly Property StudentType() As String
            Get
                Return _StudentType
            End Get
        End Property
        Public ReadOnly Property IsTransHold() As Boolean
            Get
                Return _IsTransHold
            End Get
        End Property

    End Class
End Namespace

