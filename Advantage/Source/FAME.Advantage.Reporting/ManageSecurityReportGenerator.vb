﻿Imports FAME.Advantage.Reporting.ReportService2005
Imports FAME.Advantage.Reporting.ReportExecution2005
Imports System.Web
Imports FAME.Advantage.Common
Namespace Logic
    Public Class ManageSecurityReportGenerator
#Region "Report Variables"
        Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        Private _rptService As New ReportingService2005
        Private _rptExecutionService As New ReportExecution2005.ReportExecutionService

        Private _strReportName As String = Nothing
        Private _strHistoryId As String = Nothing
        Private _boolForRendering As Boolean = False
        Private _credentials As ReportService2005.DataSourceCredentials() = Nothing
        Private _rptParameterValues As ReportService2005.ParameterValue() = Nothing
        Private _rptParameters As ReportService2005.ReportParameter() = Nothing
        Private _rptExecutionServiceParameterValues As ReportExecution2005.ParameterValue() = Nothing
        Private _rptExecutionInfoObj As New ExecutionInfo
        Dim _strDateRangeTextPrevious As String
        Dim _strDateRangeText2YrPrevious As String

        'Variables needed to render report
        Private _historyId As String = Nothing
        Private _deviceInfo As String = Nothing
        Private _format As String = Nothing
        Private _generateReportAsBytes As [Byte]()
        Private _encoding As String = [String].Empty
        Private Shared _strExtension As String = String.Empty
        Private Shared _strMimeType As String = String.Empty
        Private _warnings As ReportExecution2005.Warning() = Nothing
        Private _streamIDs As String() = Nothing
        Private _strDateRangeText As String
        Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString).ToLower
        Dim strSchoolName As String = MyAdvAppSettings.AppSettings("SchoolName").ToString
        Dim strServerName As String = GetServerName(strConnectionString)
        Dim strDBName As String = GetDatabaseName(strConnectionString)
        Dim strUserName As String = GetUserName(strConnectionString)
        Dim strPassword As String = GetPassword(strConnectionString)
#End Region

#Region "Initialize Web Services"
        Private Sub InitializeWebServices()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            'Set the credentials and url for the report services
            'The report service object is needed to get the parameter details associated with a specific report
            _rptService.Credentials = System.Net.CredentialCache.DefaultCredentials
            _rptService.Url = MyAdvAppSettings.AppSettings("ReportServices").ToString

            'Set the credentials and url for the report execution services
            _rptExecutionService.Credentials = System.Net.CredentialCache.DefaultCredentials
            _rptExecutionService.Url = MyAdvAppSettings.AppSettings("ReportExecutionServices").ToString
        End Sub
#End Region

#Region "Initialize Export Formats"
        Private Shared Sub ConfigureExportFormat(ByRef strType As String)
            Select Case strType.ToUpper()
                Case "PDF"
                    _strExtension = "pdf"
                    _strMimeType = "application/pdf"
                    Exit Select
                Case "EXCEL", "XLS"
                    _strExtension = "xls"
                    _strMimeType = "application/vnd.excel"
                    Exit Select
                Case "WORD"
                    _strExtension = "doc"
                    _strMimeType = "application/vnd.ms-word"
                Case "CSV"
                    _strExtension = "csv"
                    _strMimeType = "text/csv"
                Case Else
                    Throw New Exception("Unrecognized type: " & strType & ". Type must be PDF, Excel or Image, HTML.")
            End Select
        End Sub
#End Region

#Region "Utility Functions"
        Function GetDatabaseName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("database=")
            If startIndex > -1 Then startIndex += 9
            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""
            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length
            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetServerName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("server=")
            If startIndex > -1 Then startIndex += 7

            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetUserName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("uid=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetPassword(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("pwd=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
#End Region


#Region "Build Report"
        Public Function RenderReport(ByVal _format As String, _
                                                                    ByVal User As String, _
                                                                    ByVal strRoleId As String, _
                                                                    ByVal strRole As String, _
                                                                    ByVal strModuleId As String,
                                                                    ByVal strModule As String, _
                                                                    ByVal strAdvantageUserName As String, _
                                                                    ByVal strPageType As String, _
                                                                    ByVal strReportPath As String, _
                                                                    ByVal intTabId As Integer, _
                                                                    ByVal intResourceTypeId As Integer, _
                                                                    ByVal CampusId As String, _
                                                                    ByVal intSchoolOptions As Integer) As [Byte]()

            'Initialize the report services and report execution services
            InitializeWebServices()

            'Set the path of report
            _strReportName = strReportPath + "Security/ManageSecurity" '"/Advantage Reports/" + getReportParametersObj.Environment + "/Security/ManageSecurity" 'getReportParametersObj.ReportPath   '"/Advantage Reports/StudentAccounts/IPEDS/IPEDSMissingDataReport"

            'Use the Report Services: GetReportParameters method to get a list of parameters for a specified report
            'This is the only location where the reportingservices2005 is used
            _rptParameters = _rptService.GetReportParameters(_strReportName, _strHistoryId, _boolForRendering, _rptParameterValues, _credentials)

            'Need to load the specific report before passing the parameter values
            _rptExecutionInfoObj = _rptExecutionService.LoadReport(_strReportName, _strHistoryId)

            ' Prepare report parameter. 
            _rptExecutionServiceParameterValues = BuildReports(_rptParameters, strRoleId, _
                                                              strRole, strModuleId, strModule, strAdvantageUserName, strPageType, intTabId, intResourceTypeId, CampusId, intSchoolOptions)

            _rptExecutionService.Timeout = System.Threading.Timeout.Infinite

            'Set Report Parameters
            _rptExecutionService.SetExecutionParameters(_rptExecutionServiceParameterValues, "en-us")

            'Call ConfigureExportFormat to get the file extensions
            ConfigureExportFormat(_format)

            'Render the report
            _generateReportAsBytes = _rptExecutionService.Render(_format, _deviceInfo, _strExtension, _encoding, _strMimeType, _warnings, _streamIDs)

            Return _generateReportAsBytes
        End Function
        Private Function BuildReports(ByVal rptParameters As ReportService2005.ReportParameter(), _
                                          ByVal strRoleId As String, _
                                          ByVal strRole As String, _
                                          ByVal strModuleId As String,
                                          ByVal strModule As String, _
                                          ByVal strAdvantageUserName As String, _
                                          ByVal strPageType As String, _
                                          ByVal intTabId As Integer, _
                                          ByVal intResourceTypeId As Integer, _
                                          ByVal CampusId As String, _
                                          ByVal intSchoolOptions As Integer) As ReportExecution2005.ParameterValue()

            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptParameters.Length - 1) {}
            If rptParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                'RoleId
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "RoleId"
                rptExecutionParamValues(0).Name = "RoleId"
                If strRoleId.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = strRoleId
                End If

                'ModuleId Parameter
                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "ModuleResourceId"
                rptExecutionParamValues(1).Name = "ModuleResourceId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not strModuleId.Trim = "" Then
                    rptExecutionParamValues(1).Value = strModuleId.ToString
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If


                'AdvantageUserName Parameter
                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "advantageusername"
                rptExecutionParamValues(2).Name = "advantageusername"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not strAdvantageUserName.Trim = "" Then
                    rptExecutionParamValues(2).Value = strAdvantageUserName.ToString
                Else
                    rptExecutionParamValues(2).Value = Nothing
                End If


                'Role Parameter
                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "role"
                rptExecutionParamValues(3).Name = "role"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not strRole.Trim = "" Then
                    rptExecutionParamValues(3).Value = strRole.ToString
                Else
                    rptExecutionParamValues(3).Value = Nothing
                End If

                'Module Parameter
                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "module"
                rptExecutionParamValues(4).Name = "module"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not strModule.Trim = "" Then
                    rptExecutionParamValues(4).Value = strModule.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                'PageType Parameter
                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "pagetype"
                rptExecutionParamValues(5).Name = "pagetype"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not strPageType.Trim = "" Then
                    rptExecutionParamValues(5).Value = strPageType.ToString
                Else
                    rptExecutionParamValues(5).Value = Nothing
                End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "ServerName"
                rptExecutionParamValues(6).Name = "ServerName"
                rptExecutionParamValues(6).Value = strServerName

                'Database Name
                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "DBName"
                rptExecutionParamValues(7).Name = "DBName"
                rptExecutionParamValues(7).Value = strDBName


                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "UserName"
                rptExecutionParamValues(8).Name = "UserName"
                rptExecutionParamValues(8).Value = strUserName

                'password
                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "Password"
                rptExecutionParamValues(9).Name = "Password"
                rptExecutionParamValues(9).Value = strPassword

                'TabId
                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "tabid"
                rptExecutionParamValues(10).Name = "tabid"
                rptExecutionParamValues(10).Value = intTabId.ToString

                'ResourceTypeId
                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "ResourceTypeId"
                rptExecutionParamValues(11).Name = "ResourceTypeId"
                rptExecutionParamValues(11).Value = intResourceTypeId.ToString

                'CampusId
                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "campusid"
                rptExecutionParamValues(12).Name = "campusid"
                rptExecutionParamValues(12).Value = CampusId.ToString

                'School Option
                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "SchoolEnumerator"
                rptExecutionParamValues(13).Name = "SchoolEnumerator"
                rptExecutionParamValues(13).Value = intSchoolOptions.ToString
            End If
            Return rptExecutionParamValues
        End Function
#End Region

        Private Function GetAdvAppSettings() As AdvAppSettings
            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            Return MyAdvAppSettings
        End Function
    End Class
End Namespace
