﻿Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Reporting.Info
Imports FAME.Parameters.Info

Namespace Logic
    Public Class WeeklyAttendanceMultiple
        Dim CampusId As String = ""
        Dim SelectedDate As DateTime
        Dim TermId As String = ""
        Dim CourseId As String = ""
        Dim InstructorId As String = ""
        Dim ShiftId As String = ""
        Dim SectionId As String = ""
        Dim EnrollmentId As String = ""
        Public Function BuildReport(ByVal rpt As ReportInfo, ByVal conn As String, ByVal userId As String,
                                                                ByVal strGradingMethod As String,
                                                                ByVal strGPAMethod As String,
                                                                ByVal strSchoolName As String,
                                                                ByVal strTrackAttendanceBy As String,
                                                                ByVal strGradeBookWeightingLevel As String,
                                                                ByVal strStudentIdentifier As String,
                                                                ByVal DisplayAttendanceUnitForProgressReportByClass As String,
                                                                ByVal strReportPath As String) As [Byte]()
            Dim testText As String = String.Empty
            Try
                Dim objReportInfo As ReportInfo = rpt
                Dim getReportOutput As [Byte]()
                For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                    Select Case tabview.ParamSet.SetType
                        Case "Filter"
                        Case "Sort"
                        Case "Custom"
                            For Each item As ParamItemUserSettingsInfo In rpt.CustomOptions
                                'Response.Write(item.ControlSettingsCollection.Item) 
                                For Each drillItem As ControlSettingInfo In item.ControlSettingsCollection
                                    For Each controlValueItem As ControlValueInfo In drillItem.ControlValueCollection
                                        Dim strKeyName As String = ""
                                        Dim strKeyValue As String = ""
                                        Dim strDisplayText As String = ""
                                        Select Case drillItem.ControlName.Trim.ToString
                                            Case Is = "RadComboCampus"
                                                strKeyName = "campusid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "RadWeekEndingDate"
                                                strKeyName = "selecteddate"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "RadListBoxTerms2"
                                                strKeyName = "termid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "RadListBoxCourse2"
                                                strKeyName = "courseid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "RadListBoxInstructor2"
                                                strKeyName = "instructorid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "RadListBoxShift2"
                                                strKeyName = "shiftid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "RadListBoxSection2"
                                                strKeyName = "sectionid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "RadListBoxEnrollmentStatus2"
                                                strKeyName = "enrollmentid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)

                                        End Select
                                    Next
                                Next
                            Next
                        Case Else
                            Throw New Exception("Unknown Type")
                    End Select
                Next
                Dim getSelectedValues As New WeeklyAttendanceMultipleSelectionValues
                With getSelectedValues
                    .CampusId = CampusId.ToString
                    .SelectedDate = SelectedDate
                    .TermId = TermId.ToString
                    .CourseId = CourseId.ToString
                    .InstructorId = InstructorId.ToString
                    .ShiftId = ShiftId.ToString
                    .SectionId = SectionId.ToString
                    .EnrollmentId = EnrollmentId.ToString
                    .ReportPath = objReportInfo.RDLName
                    .Environment = strReportPath
                End With
                Dim getWeeklyAttendanceDa = New WeeklyAttendanceMultipleDa(conn)
                Dim intCheckDataExists As Integer
                intCheckDataExists = getWeeklyAttendanceDa.USP_WeeklyAttendance(SelectedDate, Guid.Parse(CampusId), TermId, CourseId, InstructorId, ShiftId, SectionId, EnrollmentId)
                If intCheckDataExists >= 1 Then
                    Dim strFormat As String = ""
                    Select Case rpt.ExportFormat
                        Case ReportInfo.ExportType.PDF
                            strFormat = "Pdf"
                        Case ReportInfo.ExportType.Excel
                            strFormat = "xls"
                        Case ReportInfo.ExportType.CSV
                            strFormat = "csv"
                    End Select
                    getReportOutput = CallRenderReport(strFormat, getSelectedValues)
                    Return getReportOutput
                Else
                    Return Nothing
                End If
                Return Nothing
            Catch ex As Exception
                Throw ex
            End Try
            Return Nothing
        End Function
        Private Sub BuildParamtersForReport(ByVal strFilterFieldName As String,
                                            ByVal strFilterSelectedValue As String,
                                            ByVal strDisplayText As String)
            Select Case strFilterFieldName.ToLower.Trim
                Case "campusid"
                    CampusId = strFilterSelectedValue.ToString
                Case "selecteddate"
                    SelectedDate = CDate(strFilterSelectedValue)
                Case "termid"
                    TermId &= strFilterSelectedValue.ToString & ","
                Case "courseid"
                    CourseId &= strFilterSelectedValue.ToString & ","
                Case "instructorid"
                    InstructorId &= strFilterSelectedValue.ToString & ","
                Case "shiftid"
                    ShiftId &= strFilterSelectedValue.ToString & ","
                Case "sectionid"
                    SectionId &= strFilterSelectedValue.ToString & ","
                Case "enrollmentid"
                    EnrollmentId &= strFilterSelectedValue.ToString & ","

            End Select
        End Sub
        Public Function CallRenderReport(ByVal format As String, ByVal getFilterValues As WeeklyAttendanceMultipleSelectionValues) As [Byte]()
            Dim getReportOutput As [Byte]()
            Dim generateReport As New WeeklyAttendanceMultipleGenerator
            getReportOutput = generateReport.RenderReport(format, getFilterValues)
            Return getReportOutput
        End Function
    End Class
End Namespace

