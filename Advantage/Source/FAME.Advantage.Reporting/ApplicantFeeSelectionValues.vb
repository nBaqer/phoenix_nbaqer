﻿Namespace Info
    Public Class ApplicantFeeSelectionValues
#Region "Variables"
        Private _StartDate As Date
        Private _EndDate As Date
        Private _CampusId As String
        Private _ReportPath As String
        Private _Transaction As String
        Private _Environment As String
        Private _CampDescrip As String
#End Region
#Region "Address Initialize"
        Public Sub New()
            _ReportPath = ""
        End Sub
#End Region
#Region "Properties"
        Public Property CampusDescrip() As String
            Get
                Return _CampDescrip
            End Get
            Set(ByVal Value As String)
                _CampDescrip = Value
            End Set
        End Property
        Public Property Environment() As String
            Get
                Return _Environment
            End Get
            Set(ByVal Value As String)
                _Environment = Value
            End Set
        End Property
        Public Property StartDate() As Date
            Get
                Return _StartDate
            End Get
            Set(ByVal Value As Date)
                _StartDate = Value
            End Set
        End Property
        Public Property EndDate() As Date
            Get
                Return _EndDate
            End Get
            Set(ByVal Value As Date)
                _EndDate = Value
            End Set
        End Property

        Public Property CampusId() As String
            Get
                Return _CampusId
            End Get
            Set(ByVal Value As String)
                _CampusId = Value
            End Set
        End Property
        Public Property Transaction() As String
            Get
                Return _Transaction
            End Get
            Set(value As String)
                _Transaction = value
            End Set
        End Property
        Public Property ReportPath() As String
            Get
                Return _ReportPath
            End Get
            Set(ByVal Value As String)
                _ReportPath = Value
            End Set
        End Property
#End Region

    End Class
End Namespace
