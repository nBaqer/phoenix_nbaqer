﻿Public Class DailyConsecutiveAbsenceReportSelectionValues
#Region "Variables"
    Private _termId As String
    Private _reqId As String
    Private _cutOffDate As Date
    Private _numberOfDaysMissed As Integer
    Private _ReportPath As String
    Private _environment As String
    Private _PrgVerId As String
    Private _resourceId As Integer
    Private _campusId As String
    Private _showCalendarDays As Boolean
#End Region
#Region "Address Initialize"
    Public Sub New()

    End Sub
#End Region
#Region "Properties"
    Public Property PrgVerId() As String
        Get
            Return _PrgVerId
        End Get
        Set(ByVal value As String)
            _PrgVerId = value
        End Set
    End Property
    Public Property ResourceId() As Integer
        Get
            Return _resourceId
        End Get
        Set(ByVal value As Integer)
            _resourceId = value
        End Set
    End Property
    Public Property TermId() As String
        Get
            Return _termId
        End Get
        Set(ByVal value As String)
            _termId = value
        End Set
    End Property
    Public Property CutOffDate() As Date
        Get
            Return _cutOffDate
        End Get
        Set(ByVal value As Date)
            _cutOffDate = value
        End Set
    End Property
    Public Property ReqId() As String
        Get
            Return _reqId
        End Get
        Set(ByVal value As String)
            _reqId = value
        End Set
    End Property
    Public Property DaysMissed() As Integer
        Get
            Return _numberOfDaysMissed
        End Get
        Set(ByVal value As Integer)
            _numberOfDaysMissed = value
        End Set
    End Property
    Public Property ReportPath() As String
        Get

            Return _ReportPath
        End Get
        Set(ByVal Value As String)
            _ReportPath = Value
        End Set
    End Property
    Public Property Environment() As String
        Get

            Return _environment
        End Get
        Set(ByVal Value As String)
            _environment = Value
        End Set
    End Property
    Public Property CampusId() As String
        Get

            Return _campusId
        End Get
        Set(ByVal Value As String)
            _campusId = Value
        End Set
    End Property

    Public Property ShowCalendarDays() As Boolean
        Get

            Return _showCalendarDays
        End Get
        Set(ByVal Value As Boolean)
            _showCalendarDays = Value
        End Set
    End Property
#End Region
End Class