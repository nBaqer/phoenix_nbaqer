﻿Namespace Info
    Public Class PopulationSelectionValues
#Region "Variables"
        Private _CampGrpId As String
        Private _ProgVerId As String
        Private _StartDate As String
        Private _EndDate As String
        Private _IncludeLOA As Boolean
        Private _ResourceId As Integer
        Private _ReportPath As String
        Private _Environment As String
#End Region
#Region "Address Initialize"
        Public Sub New()
            _CampGrpId = ""
            _ProgVerId = ""
            _StartDate = Nothing
            _EndDate = Nothing
            _ResourceId = 0
            _IncludeLOA = False
            _ReportPath = ""
        End Sub
#End Region
#Region "Properties"
        Public Property IncludeLOA() As Boolean
            Get
                Return _IncludeLOA
            End Get
            Set(ByVal Value As Boolean)
                _IncludeLOA = Value
            End Set
        End Property
        Public Property StartDate() As String
            Get
                Return _StartDate
            End Get
            Set(ByVal Value As String)
                _StartDate = Value
            End Set
        End Property
        Public Property EndDate() As String
            Get
                Return _EndDate
            End Get
            Set(ByVal Value As String)
                _EndDate = Value
            End Set
        End Property
        Public Property ProgVerId() As String
            Get
                Return _ProgVerId
            End Get
            Set(ByVal Value As String)
                _ProgVerId = Value
            End Set
        End Property
        Public Property CampGrpId() As String
            Get
                Return _CampGrpId
            End Get
            Set(ByVal Value As String)
                _CampGrpId = Value
            End Set
        End Property
        Public Property ResourceId() As Integer
            Get
                Return _ResourceId
            End Get
            Set(ByVal Value As Integer)
                _ResourceId = Value
            End Set
        End Property
        Public Property ReportPath() As String
            Get
                Return _ReportPath
            End Get
            Set(ByVal Value As String)
                _ReportPath = Value
            End Set
        End Property
        Public Property Environment() As String
            Get
                Return _Environment
            End Get
            Set(ByVal Value As String)
                _Environment = Value
            End Set
        End Property
#End Region
    End Class
End Namespace

