﻿Public Class PaymentPeriodsReportSelectionValues
#Region "Variables"
    Private _termId As String
    Private _reqId As String
    Private _cutOffDate As Date
    Private _numberOfDaysMissed As Integer
    Private _ReportPath As String
    Private _environment As String
    Private _campusId As String
    Private _programversionname As String
    Private _prgverid As String
    Private _ChargingMethodId As String
    Private _transStartDate As Date
    Private _transEndDate As Date
    Private _showSummary As Boolean
    Private _ProgramId As String
    #End Region
#Region "Address Initialize"
    Public Sub New()

    End Sub
#End Region
#Region "Properties"

    Public Property PrgVerId() As String
        Get
            Return _prgverid
        End Get
        Set(ByVal value As String)
            _prgverid = value
        End Set
    End Property
    Public Property ChargingMethodId() As String
        Get
            Return _ChargingMethodId
        End Get
        Set(ByVal value As String)
            _ChargingMethodId = value
        End Set
    End Property

    Public Property CampusId() As String
        Get

            Return _campusId
        End Get
        Set(ByVal value As String)
            _campusId = value
        End Set
    End Property


    Public Property ReqId() As String
        Get
            Return _reqId
        End Get
        Set(ByVal value As String)
            _reqId = value
        End Set
    End Property
    Public Property ProgramId() As String
        Get
            Return _ProgramId
        End Get
        Set(ByVal value As String)
            _ProgramId = value
        End Set
    End Property
    Public Property TermId() As String
        Get
            Return _termId
        End Get
        Set(ByVal value As String)
            _termId = value
        End Set
    End Property
    Public Property TransStartDate() As Date
        Get

            Return _transStartDate
        End Get
        Set(ByVal Value As Date)
            _transStartDate = Value
        End Set
    End Property
    Public Property TransEndDate() As Date
        Get

            Return _transEndDate
        End Get
        Set(ByVal Value As Date)
            _transEndDate = Value
        End Set
    End Property
    Public Property ReportPath() As String
        Get

            Return _ReportPath
        End Get
        Set(ByVal Value As String)
            _ReportPath = Value
        End Set
    End Property
    Public Property Environment() As String
        Get

            Return _environment
        End Get
        Set(ByVal Value As String)
            _environment = Value
        End Set
    End Property
    Public Property ShowSummary() As Boolean

        Get
            Return _showSummary
        End Get
        Set(value As Boolean)
            _showSummary = value
        End Set
    End Property

#End Region
End Class