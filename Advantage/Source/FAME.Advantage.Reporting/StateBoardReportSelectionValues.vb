﻿Public Class StateBoardReportSelectionValues
#Region "Variables"
    Private _termId As String
    Private _reqId As String
    Private _cutOffDate As Date
    Private _numberOfDaysMissed As Integer
    Private _ReportPath As String
    Private _environment As String
    Private _resourceId As Integer
    Private _campusId As String
    Private _apiUrl As String
    Private _apiToken As String
    Private _stateId As String
    'Private _reportId As String
    Private _month As String
    Private _year As String
    Private _programversionname As String
    Private _prgverid As String
    Private _studentGroupId As String
    Private _OfficialReportingDate As String

#End Region
#Region "Address Initialize"
    Public Sub New()

    End Sub
#End Region
#Region "Properties"

    Public Property StateId() As String
        Get
            Return _stateId
        End Get
        Set(ByVal value As String)
            _stateId = value
        End Set
    End Property
    'Public Property ReportId() As String
    '    Get
    '        Return _reportId
    '    End Get
    '    Set(ByVal value As String)
    '        _reportId = value
    '    End Set
    'End Property

    Public Property PrgVerId() As String
        Get
            Return _prgverid
        End Get
        Set(ByVal value As String)
            _prgverid = value
        End Set
    End Property
    Public Property CampusId() As String
        Get

            Return _campusId
        End Get
        Set(ByVal value As String)
            _campusId = value
        End Set
    End Property
    Public Property Month() As String
        Get
            Return _month
        End Get
        Set(ByVal value As String)
            _month = value
        End Set
    End Property
    Public Property Year() As String
        Get
            Return _year
        End Get
        Set(ByVal value As String)
            _year = value
        End Set
    End Property
    Public Property StudentGroupId() As String
        Get
            Return _studentGroupId
        End Get
        Set(ByVal value As String)
            _studentGroupId = value
        End Set
    End Property

    Public Property ApiToken() As String
        Get
            Return _apiToken
        End Get
        Set(ByVal value As String)
            _apiToken = value
        End Set
    End Property

    Public Property ApiUrl() As String
        Get
            Return _apiUrl
        End Get
        Set(ByVal value As String)
            _apiUrl = value
        End Set
    End Property

    Public Property ReportPath() As String
        Get

            Return _ReportPath
        End Get
        Set(ByVal Value As String)
            _ReportPath = Value
        End Set
    End Property
    Public Property OfficialReportingDate() As String
        Get
            Return _OfficialReportingDate
        End Get
        Set(ByVal Value As String)
            _OfficialReportingDate = Value
        End Set
    End Property
    Public Property Environment() As String
        Get

            Return _environment
        End Get
        Set(ByVal Value As String)
            _environment = Value
        End Set
    End Property

    Public Property ResourceId() As Integer
        Get

            Return _resourceId
        End Get
        Set(ByVal Value As Integer)
            _resourceId = Value
        End Set
    End Property
#End Region
End Class