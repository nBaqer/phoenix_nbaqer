﻿Imports FAME.Advantage.Reporting.ReportService2005
Imports FAME.Advantage.Reporting.ReportExecution2005
Imports System.Configuration
Imports System.Web
Imports FAME.Advantage.Reporting.Info
Imports FAME.Advantage.Reporting
Imports FAME.Advantage.Common
Namespace Logic
    Public Class StateBoardReportGenerator
#Region "Report Variables"
        Private _rptService As New ReportingService2005
        Private _rptExecutionService As New ReportExecution2005.ReportExecutionService

        Private _strReportName As String = Nothing
        Private _strHistoryId As String = Nothing
        Private _boolForRendering As Boolean = False
        Private _credentials As ReportService2005.DataSourceCredentials() = Nothing
        Private _rptParameterValues As ReportService2005.ParameterValue() = Nothing
        Private _rptParameters As ReportService2005.ReportParameter() = Nothing
        Private _rptExecutionServiceParameterValues As ReportExecution2005.ParameterValue() = Nothing
        Private _rptExecutionInfoObj As New ExecutionInfo

        'Variables needed to render report
        Private _historyId As String = Nothing
        Private _deviceInfo As String = Nothing
        Private _format As String = Nothing
        Private _generateReportAsBytes As [Byte]()
        Private _encoding As String = [String].Empty
        Private Shared _strExtension As String = String.Empty
        Private Shared _strMimeType As String = String.Empty
        Private _warnings As ReportExecution2005.Warning() = Nothing
        Private _streamIDs As String() = Nothing

        Dim strServerName As String = ""
        Dim strDBName As String = ""
        Dim strUserName As String = ""
        Dim strPassword As String = ""
#End Region

#Region "Initialize Web Services"
        Private Sub InitializeWebServices()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString).ToLower
            _rptService.Credentials = System.Net.CredentialCache.DefaultCredentials
            _rptService.Url = MyAdvAppSettings.AppSettings("ReportServices").ToString
            'Set the credentials and url for the report execution services
            _rptExecutionService.Credentials = System.Net.CredentialCache.DefaultCredentials
            '_rptExecutionService.SetExecutionCredentials(dsCredentials)
            _rptExecutionService.Url = MyAdvAppSettings.AppSettings("ReportExecutionServices").ToString
        End Sub
#End Region

#Region "Initialize Export Formats"
        Private Shared Sub ConfigureExportFormat(ByRef strType As String)
            Select Case strType.ToUpper()
                Case "PDF"
                    _strExtension = "pdf"
                    _strMimeType = "application/pdf"
                    Exit Select
                Case "EXCEL", "XLS"
                    _strExtension = "xls"
                    _strMimeType = "application/vnd.excel"
                    Exit Select
                Case "WORD"
                    _strExtension = "doc"
                    _strMimeType = "application/vnd.ms-word"
                Case "CSV"
                    _strExtension = "csv"
                    _strMimeType = "text/csv"
                Case Else
                    Throw New Exception("Unrecognized type: " & strType & ". Type must be PDF, Excel or Image, HTML.")
            End Select
        End Sub
#End Region

#Region "Build Report Parameters"
        Private Function BuildReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(),
                                                                       ByVal getReportParametersObj As StateBoardReportSelectionValues,
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}
            Dim strNullValue As String = "Null"
            Dim strAsOfDate As String = ""
            Dim strCampusId As String = ""
            Dim strStateId As String = ""
            Dim strMonth As String = ""
            Dim strYear As String = ""
            Dim strStudentGroupId As String = ""
            Dim strPrgVerId As String = ""
            Dim strApiUrl As String = ""
            Dim strApiToken As String = ""


            Dim intNumberofDays As Integer = 0
            Dim dtCutOffDate As DateTime

            Dim strServerName As String = ""
            Dim strDBName As String = ""
            Dim strUserName As String = ""
            Dim strPassword As String = ""

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString).ToLower

            '  GetConnectionStringValues()

            With getReportParametersObj
                strCampusId = .CampusId
                strStateId = .StateId
                strMonth = .Month
                strYear = .Year
                strStudentGroupId = .StudentGroupId
                strPrgVerId = .PrgVerId
                strApiToken = .ApiToken
                strApiUrl = .ApiUrl

                strServerName = GetServerName(strConnectionString)
                strDBName = GetDatabaseName(strConnectionString)
                strUserName = GetUserName(strConnectionString)
                strPassword = GetPassword(strConnectionString)
            End With

            If rptServicesParameters.Length > 0 Then

                'Tennessee stateboard report - pass the parameter values
                If (getReportParametersObj.ResourceId = 808) Then

                    'servername Parameter
                    rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                    rptExecutionParamValues(0).Label = "servername"
                    rptExecutionParamValues(0).Name = "servername"
                    rptExecutionParamValues(0).Value = strServerName

                    'databasename Parameter
                    rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                    rptExecutionParamValues(1).Label = "databasename"
                    rptExecutionParamValues(1).Name = "databasename"
                    rptExecutionParamValues(1).Value = strDBName

                    'uid Parameter
                    rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                    rptExecutionParamValues(2).Label = "uid"
                    rptExecutionParamValues(2).Name = "uid"
                    rptExecutionParamValues(2).Value = strUserName

                    'password Parameter
                    rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                    rptExecutionParamValues(3).Label = "password"
                    rptExecutionParamValues(3).Name = "password"
                    rptExecutionParamValues(3).Value = strPassword

                    'CampusId Parameter
                    rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                    rptExecutionParamValues(4).Label = "CampusId"
                    rptExecutionParamValues(4).Name = "CampusId"
                    rptExecutionParamValues(4).Value = strCampusId

                    'PrgVerId Parameter
                    rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                    rptExecutionParamValues(5).Label = "PrgVerId"
                    rptExecutionParamValues(5).Name = "PrgVerId"
                    rptExecutionParamValues(5).Value = strPrgVerId

                    'ReportDate Parameter
                    rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                    rptExecutionParamValues(6).Label = "ReportDate"
                    rptExecutionParamValues(6).Name = "ReportDate"
                    rptExecutionParamValues(6).Value = Date.Parse(strMonth.ToString() + "/1/" + strYear.ToString()).ToShortDateString()

                    Return rptExecutionParamValues
                End If


                'CampusId Parameter
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CampusId"
                rptExecutionParamValues(0).Name = "CampusId"
                rptExecutionParamValues(0).Value = strCampusId

                ''StateId Parameter
                'rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(1).Label = "StateId"
                'rptExecutionParamValues(1).Name = "StateId"
                'rptExecutionParamValues(1).Value = strStateId.ToString

                'Month Parameter
                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "Month"
                rptExecutionParamValues(1).Name = "Month"
                rptExecutionParamValues(1).Value = strMonth

                'Year Parameter
                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "Year"
                rptExecutionParamValues(2).Name = "Year"
                rptExecutionParamValues(2).Value = strYear

                'Student Group Parameter
                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "StudentGroup"
                rptExecutionParamValues(3).Name = "StudentGroup"
                rptExecutionParamValues(3).Value = strStudentGroupId

                'PrgVerId Parameter
                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ProgramVersion"
                rptExecutionParamValues(4).Name = "ProgramVersion"

                rptExecutionParamValues(4).Value = strPrgVerId


                ''password
                'rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(5).Label = "Password"
                'rptExecutionParamValues(5).Name = "password"
                'rptExecutionParamValues(5).Value = strPassword

                ''Database Name
                'rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(7).Label = "DBName"
                'rptExecutionParamValues(7).Name = "databasename"
                'rptExecutionParamValues(7).Value = strDBName


                'rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(8).Label = "UserName"
                'rptExecutionParamValues(8).Name = "uid"
                'rptExecutionParamValues(8).Value = strUserName

                ''Server Name
                'rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                'rptExecutionParamValues(9).Label = "ServerName"
                'rptExecutionParamValues(9).Name = "servername"
                'rptExecutionParamValues(9).Value = strServerName

                'Api Url
                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "ApiURL"
                rptExecutionParamValues(5).Name = "ApiURL"
                rptExecutionParamValues(5).Value = strApiUrl

                'Api Token
                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "Token"
                rptExecutionParamValues(6).Name = "Token"
                rptExecutionParamValues(6).Value = strApiToken


            End If
            Return rptExecutionParamValues
        End Function
#End Region

#Region "Build Report"
        Public Function RenderReport(ByVal _format As String,
                                    ByVal getReportParametersObj As StateBoardReportSelectionValues,
                                    ByVal strSchoolName As String) As [Byte]()

            'Initialize the report services and report execution services
            InitializeWebServices()


            'Set the path of report
            _strReportName = getReportParametersObj.Environment + getReportParametersObj.ReportPath
            'Use the Report Services: GetReportParameters method to get a list of parameters for a specified report
            'This is the only location where the reportingservices2005 is used
            _rptParameters = _rptService.GetReportParameters(_strReportName, _strHistoryId, _boolForRendering, _rptParameterValues, _credentials)

            'Need to load the specific report before passing the parameter values
            _rptExecutionInfoObj = _rptExecutionService.LoadReport(_strReportName, _strHistoryId)

            ' Prepare report parameter. 
            _rptExecutionServiceParameterValues = BuildReportParameters(_rptParameters, getReportParametersObj, strSchoolName)

            _rptExecutionService.Timeout = System.Threading.Timeout.Infinite

            'Set Report Parameters
            _rptExecutionService.SetExecutionParameters(_rptExecutionServiceParameterValues, "en-us")

            'Call ConfigureExportFormat to get the file extensions
            ConfigureExportFormat(_format)

            'Render the report
            _generateReportAsBytes = _rptExecutionService.Render(_format, _deviceInfo, _strExtension, _encoding, _strMimeType, _warnings, _streamIDs)

            Return _generateReportAsBytes
            'Display the report in the browser
            'DisplayReport(_generateReportAsBytes)
        End Function
#End Region

#Region "DisplayReport"
        Private Sub DisplayReport(ByVal byteReportOutput As [Byte]())
            'Pdf
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.ContentType = _strMimeType
            HttpContext.Current.Response.BinaryWrite(byteReportOutput)
        End Sub
        Function GetDatabaseName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("database=")
            If startIndex > -1 Then startIndex += 9
            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""
            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length
            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetServerName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("server=")
            If startIndex > -1 Then startIndex += 7

            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetUserName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("uid=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetPassword(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("pwd=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
#End Region
    End Class
End Namespace



