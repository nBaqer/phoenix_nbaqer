Namespace Info
    Public Class FasapCheckSelectionValues
#Region "Variables"
        Private _CampGrpId As String
        Private _ProgVerId As String
        Private _EnrollmentStatus As String
        Private _AsOfDate As String
        Private _CampGrpDescrip As String
        Private _ProgVerDescrip As String
        Private _ResourceId As Integer
        Private _ReportPath As String
        Private _Environment As String
        Private _campusId As String
#End Region
#Region "Address Initialize"
        Public Sub New()
            _CampGrpId = ""
            _ProgVerId = ""
            _EnrollmentStatus = Nothing
            _AsOfDate = Nothing
            _CampGrpDescrip = ""
            _ProgVerDescrip = ""
            _ResourceId = 0
            _ReportPath = ""
        End Sub
#End Region
#Region "Properties"

        Public Property CampGrpId() As String
            Get
                Return _CampGrpId
            End Get
            Set(ByVal Value As String)
                _CampGrpId = Value
            End Set
        End Property
        Public Property ProgVerId() As String
            Get
                Return _ProgVerId
            End Get
            Set(ByVal Value As String)
                _ProgVerId = Value
            End Set
        End Property
        Public Property CampusId() As String
            Get

                Return _campusId
            End Get
            Set(ByVal Value As String)
                _campusId = Value
            End Set
        End Property
        Public Property EnrollmentStatus() As String
            Get
                Return _EnrollmentStatus
            End Get
            Set(ByVal Value As String)
                _EnrollmentStatus = Value
            End Set
        End Property
        Public Property AsOfDate() As String
            Get
                Return _AsOfDate
            End Get
            Set(ByVal Value As String)
                _AsOfDate = Value
            End Set
        End Property
        Public Property CampGrpDescrip() As String
            Get
                Return _CampGrpDescrip
            End Get
            Set(ByVal Value As String)
                _CampGrpDescrip = Value
            End Set
        End Property
        Public Property ProgVerDescrip() As String
            Get
                Return _ProgVerDescrip
            End Get
            Set(ByVal Value As String)
                _ProgVerDescrip = Value
            End Set
        End Property
        Public Property ResourceId() As Integer
            Get
                Return _ResourceId
            End Get
            Set(ByVal Value As Integer)
                _ResourceId = Value
            End Set
        End Property
        Public Property ReportPath() As String
            Get
                Return _ReportPath
            End Get
            Set(ByVal Value As String)
                _ReportPath = Value
            End Set
        End Property
        Public Property Environment() As String
            Get
                Return _Environment
            End Get
            Set(ByVal Value As String)
                _Environment = Value
            End Set
        End Property
#End Region
    End Class
End Namespace


