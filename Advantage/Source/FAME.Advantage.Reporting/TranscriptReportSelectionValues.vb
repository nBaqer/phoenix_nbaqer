﻿
Namespace Info
    Public Class TranscriptReportSelectionValues
#Region "Variables"
        'Transcript Report Parameters
        'Access values
        Private _ReportPath As String = ""
        Private _Environment As String = ""
        Private _strServerName As String = ""
        Private _strDatabaseName As String = ""
        Private _strUserName As String = ""
        Private _strPassword As String = ""

        'Filter Tab
        Private _CampGrpId As String
        Private _CampusId As String
        Private _PrgVerId As String
        Private _StatusCodeIdList As String
        Private _StuEnrollIdList As String
        'Sort Tab
        Private _SortFields As String

        'Options Tab
        'Header
        Private _boolNoLogoOrSchooInformation As Boolean

        'Logo Position
        Private _strOfficialLogoPosition As String

        'School Header 
        Private _boolShowSchoolName As Boolean
        Private _boolShowCampusAddress As Boolean
        Private _boolShowSchoolPhone As Boolean
        Private _boolShowSchoolFax As Boolean
        Private _boolShowSchoolWebSite As Boolean

        'General Header
        Private _boolIsOfficialTranscript As Boolean
        Private _boolShowCurrentDate As Boolean

        'Fold Mark 
        Private _boolShowFoldMark As Boolean

        'Student   
        Private _boolShowSSN As Boolean
        Private _boolShowStudentPhone As Boolean
        Private _boolShowStudentDOB As Boolean
        Private _boolShowStudentEmail As Boolean

        'Program
        Private _boolShowMultipleEnrollments As Boolean
        Private _boolShowGraduationDate As Boolean
        Private _boolShowLDA As Boolean
        Private _boolShowCompletedHours As Boolean

        'Courses
        Private _strCoursesLayout As String
        Private _boolShowTermDescription As Boolean
        Private _boolShowCourseComponents As Boolean
        Private _boolShowDateIssuedColumn As Boolean
        Private _boolShowCreditsColumn As Boolean
        Private _boolShowHoursColumn As Boolean

        'Footer
        Private _boolShowCourseSummary As Boolean
        Private _boolShowGradePoints As Boolean
        Private _boolShowGradeScale As Boolean
        Private _boolShowStudentSignatureLine As Boolean
        Private _boolShowSchoolOfficialSignatureLine As Boolean
        Private _boolShowPageNumber As Boolean

        'Disclaimer    
        Private _boolShowDisclaimer As Boolean
        Private _strDisclaimerText As String


#End Region

#Region "Address Initialize"
        Public Sub New()
            'General 
            _ReportPath = ""
            _Environment = ""
            _strServerName = ""
            _strDatabaseName = ""
            _strUserName = ""
            _strPassword = ""

            'Filter Tab
            _CampGrpId = ""
            _CampusId = ""
            _PrgVerId = ""
            _StatusCodeIdList = ""
            _StuEnrollIdList = ""

            'Sort Tab
            _SortFields = "LastName Asc,FirstName Asc,MiddleName Asc"

            'Options Tab
            'Header
            _boolNoLogoOrSchooInformation = False

            'Logo Position
            _strOfficialLogoPosition = "Left"

            'School Header
            _boolShowSchoolName = True
            _boolShowCampusAddress = True
            _boolShowSchoolPhone = True
            _boolShowSchoolFax = True
            _boolShowSchoolWebSite = True

            'General Header
            _boolIsOfficialTranscript = False
            _boolShowCurrentDate = True

            'Fold Mark 
            _boolShowFoldMark = True

            'Student  
            _boolShowSSN = True
            _boolShowStudentPhone = True
            _boolShowStudentDOB = True
            _boolShowStudentEmail = True

            'Program
            _boolShowMultipleEnrollments = False
            _boolShowGraduationDate = True
            _boolShowLDA = True
            _boolShowCompletedHours = True

            'Courses
            _strCoursesLayout = "ShowTerms/Modules"
            _boolShowTermDescription = False
            _boolShowCourseComponents = False
            _boolShowDateIssuedColumn = True
            _boolShowCreditsColumn = True
            _boolShowHoursColumn = True

            'Footer
            _boolShowCourseSummary = True
            _boolShowGradePoints = True
            _boolShowGradeScale = True
            _boolShowStudentSignatureLine = True
            _boolShowSchoolOfficialSignatureLine = True
            _boolShowPageNumber = True

            'Disclaimer   
            _boolShowDisclaimer = True
            _strDisclaimerText = "This Transcript is official only when signed by the school official and embossed with the school’s raised seal.  Federal law prohibits the release of this document to a person or institution without the written consent of the student."
        End Sub
#End Region

#Region "Properties"

        Public Property ReportPath() As String
            Get
                Return _ReportPath
            End Get
            Set(ByVal Value As String)
                _ReportPath = Value
            End Set
        End Property
        Public Property Environment() As String
            Get
                Return _Environment
            End Get
            Set(ByVal Value As String)
                _Environment = Value
            End Set
        End Property
        Public Property ServerName() As String
            Get
                Return _strServerName
            End Get
            Set(ByVal Value As String)
                _strServerName = Value
            End Set
        End Property
        Public Property DatabaseName() As String
            Get
                Return _strDatabaseName
            End Get
            Set(ByVal Value As String)
                _strDatabaseName = Value
            End Set
        End Property
        Public Property UserName() As String
            Get
                Return _strUserName
            End Get
            Set(ByVal Value As String)
                _strUserName = Value
            End Set
        End Property
        Public Property Password() As String
            Get
                Return _strPassword
            End Get
            Set(ByVal Value As String)
                _strPassword = Value
            End Set
        End Property

        'Filter Tab
        Public Property CampGrpId() As String
            Get
                Return _CampGrpId
            End Get
            Set(ByVal Value As String)
                _CampGrpId = Value
            End Set
        End Property
        Public Property CampusId() As String
            Get
                Return _CampusId
            End Get
            Set(ByVal Value As String)
                _CampusId = Value
            End Set
        End Property
        Public Property PrgVerId() As String
            Get
                Return _PrgVerId
            End Get
            Set(ByVal Value As String)
                _PrgVerId = Value
            End Set
        End Property
        Public Property StatusCodeIdList() As String
            Get
                Return _StatusCodeIdList
            End Get
            Set(ByVal Value As String)
                _StatusCodeIdList = Value
            End Set
        End Property
        Public Property StuEnrollIdList() As String
            Get
                Return _StuEnrollIdList
            End Get
            Set(ByVal Value As String)
                _StuEnrollIdList = Value
            End Set
        End Property
        'Sort Tab
        Public Property SortFields() As String
            Get
                Return _SortFields
            End Get
            Set(ByVal Value As String)
                _SortFields = Value
            End Set
        End Property

        'Options Tab

        'Header
        Public Property NoLogoOrSchooInformation() As Boolean
            Get
                Return _boolNoLogoOrSchooInformation
            End Get
            Set(ByVal Value As Boolean)
                _boolNoLogoOrSchooInformation = Value
            End Set
        End Property

        'Logo Position
        Public Property OfficialLogoPosition() As String
            Get
                Return _strOfficialLogoPosition
            End Get
            Set(ByVal Value As String)
                _strOfficialLogoPosition = Value
            End Set
        End Property

        'School Header 
        Public Property ShowSchoolName() As Boolean
            Get
                Return _boolShowSchoolName
            End Get
            Set(ByVal value As Boolean)
                _boolShowSchoolName = value
            End Set
        End Property
        Public Property ShowCampusAddress() As Boolean
            Get
                Return _boolShowCampusAddress
            End Get
            Set(ByVal value As Boolean)
                _boolShowCampusAddress = value
            End Set
        End Property
        Public Property ShowSchoolPhone() As Boolean
            Get
                Return _boolShowSchoolPhone
            End Get
            Set(ByVal value As Boolean)
                _boolShowSchoolPhone = value
            End Set
        End Property
        Public Property ShowSchoolFax() As Boolean
            Get
                Return _boolShowSchoolFax
            End Get
            Set(ByVal value As Boolean)
                _boolShowSchoolFax = value
            End Set
        End Property
        Public Property ShowSchoolWebSite() As Boolean
            Get
                Return _boolShowSchoolWebSite
            End Get
            Set(ByVal value As Boolean)
                _boolShowSchoolWebSite = value
            End Set
        End Property

        'General header
        Public Property IsOfficialTranscript() As Boolean
            Get
                Return _boolIsOfficialTranscript
            End Get
            Set(ByVal Value As Boolean)
                _boolIsOfficialTranscript = Value
            End Set
        End Property
        Public Property ShowCurrentDate() As Boolean
            Get
                Return _boolShowCurrentDate
            End Get
            Set(ByVal Value As Boolean)
                _boolShowCurrentDate = Value
            End Set
        End Property

        'Fold Mark 
        Public Property ShowFoldMark() As Boolean
            Get
                Return _boolShowFoldMark
            End Get
            Set(ByVal value As Boolean)
                _boolShowFoldMark = value
            End Set
        End Property

        'Student  
        Public Property ShowSSN() As Boolean
            Get
                Return _boolShowSSN
            End Get
            Set(ByVal value As Boolean)
                _boolShowSSN = value
            End Set
        End Property
        Public Property ShowStudentPhone() As Boolean
            Get
                Return _boolShowStudentPhone
            End Get
            Set(ByVal value As Boolean)
                _boolShowStudentPhone = value
            End Set
        End Property
        Public Property ShowStudentDOB() As Boolean
            Get
                Return _boolShowStudentDOB
            End Get
            Set(ByVal value As Boolean)
                _boolShowStudentDOB = value
            End Set
        End Property
        Public Property ShowStudentEmail() As Boolean
            Get
                Return _boolShowStudentEmail
            End Get
            Set(ByVal value As Boolean)
                _boolShowStudentEmail = value
            End Set
        End Property


        'Program
        Public Property ShowMultipleEnrollments() As Boolean
            Get
                Return _boolShowMultipleEnrollments
            End Get
            Set(ByVal value As Boolean)
                _boolShowMultipleEnrollments = value
            End Set
        End Property
        Public Property ShowGraduationDate() As Boolean
            Get
                Return _boolShowGraduationDate
            End Get
            Set(ByVal value As Boolean)
                _boolShowGraduationDate = value
            End Set
        End Property
        Public Property ShowLDA() As Boolean
            Get
                Return _boolShowLDA
            End Get
            Set(ByVal value As Boolean)
                _boolShowLDA = value
            End Set
        End Property
        Public Property ShowCompletedHours() As Boolean
            Get
                Return _boolShowCompletedHours
            End Get
            Set(ByVal value As Boolean)
                _boolShowCompletedHours = value
            End Set
        End Property

        'Courses
        Public Property CoursesLayout() As String
            Get
                Return _strCoursesLayout
            End Get
            Set(ByVal value As String)
                _strCoursesLayout = value
            End Set
        End Property
        Public Property ShowTermDescription() As Boolean
            Get
                Return _boolShowTermDescription
            End Get
            Set(ByVal value As Boolean)
                _boolShowTermDescription = value
            End Set
        End Property
        Public Property ShowCourseComponents() As Boolean
            Get
                Return _boolShowCourseComponents
            End Get
            Set(ByVal value As Boolean)
                _boolShowCourseComponents = value
            End Set
        End Property
        Public Property ShowDateIssuedColumn() As Boolean
            Get
                Return _boolShowDateIssuedColumn
            End Get
            Set(ByVal value As Boolean)
                _boolShowDateIssuedColumn = value
            End Set
        End Property
        Public Property ShowCreditsColumn() As Boolean
            Get
                Return _boolShowCreditsColumn
            End Get
            Set(ByVal value As Boolean)
                _boolShowCreditsColumn = value
            End Set
        End Property
        Public Property ShowHoursColumn() As Boolean
            Get
                Return _boolShowHoursColumn
            End Get
            Set(ByVal value As Boolean)
                _boolShowHoursColumn = value
            End Set
        End Property

        'Footer
        Public Property ShowCourseSummary() As Boolean
            Get
                Return _boolShowCourseSummary
            End Get
            Set(ByVal value As Boolean)
                _boolShowCourseSummary = value
            End Set
        End Property
        Public Property ShowGradePoints() As Boolean
            Get
                Return _boolShowGradePoints
            End Get
            Set(ByVal value As Boolean)
                _boolShowGradePoints = value
            End Set
        End Property
        Public Property ShowGradeScale() As Boolean
            Get
                Return _boolShowGradeScale
            End Get
            Set(ByVal value As Boolean)
                _boolShowGradeScale = value
            End Set
        End Property
        Public Property ShowDisclaimer() As Boolean
            Get
                Return _boolShowDisclaimer
            End Get
            Set(ByVal value As Boolean)
                _boolShowDisclaimer = value
            End Set
        End Property
        Public Property ShowStudentSignatureLine() As Boolean
            Get
                Return _boolShowStudentSignatureLine
            End Get
            Set(ByVal value As Boolean)
                _boolShowStudentSignatureLine = value
            End Set
        End Property
        Public Property ShowSchoolOfficialSignatureLine() As Boolean
            Get
                Return _boolShowSchoolOfficialSignatureLine
            End Get
            Set(ByVal value As Boolean)
                _boolShowSchoolOfficialSignatureLine = value
            End Set
        End Property
        Public Property ShowPageNumber() As Boolean
            Get
                Return _boolShowPageNumber
            End Get
            Set(ByVal value As Boolean)
                _boolShowPageNumber = value
            End Set
        End Property

        'Disclaimer
        Public Property DisclaimerText() As String
            Get
                Return _strDisclaimerText
            End Get
            Set(ByVal value As String)
                _strDisclaimerText = value
            End Set
        End Property


#End Region
    End Class
End Namespace

