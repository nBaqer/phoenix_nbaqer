﻿Imports FAME.Advantage.Reporting.Info
Imports FAME.Parameters.Info
Imports FAME.Advantage.Reporting
Namespace Logic
    Public Class PaymentPeriodsReport
        Dim PrgVerId As String = ""
        Dim CampusId As String = ""
        Dim CourseId As String = ""
        Dim NumberofDaysMissed As Integer = 0
        Dim CutOffDate As DateTime
        Dim TransStartDate As Date
        Dim TransEndDate As Date
        Dim ShowSummary As Boolean
        Dim Termid As String
        Dim ProgramId As String = ""
        Dim ChargingMethodId As String = ""
        Public Function BuildReport(ByVal Rpt As ReportInfo, ByVal conn As String, ByVal UserId As String, _
                                    ByVal strGradingMethod As String, _
                                    ByVal strGPAMethod As String, _
                                    ByVal strSchoolName As String, _
                                    ByVal strTrackAttendanceBy As String, _
                                    ByVal strGradeBookWeightingLevel As String, _
                                    ByVal strStudentIdentifier As String, _
                                    ByVal DisplayAttendanceUnitForProgressReportByClass As String, _
                                    ByVal strReportPath As String) As [Byte]()
            Dim TestText As String = String.Empty
            Try
                Dim objReportInfo As ReportInfo = Rpt
                Dim getReportOutput As [Byte]()
                For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                    Select Case tabview.ParamSet.SetType
                        Case "Filter"
                        Case "Sort"
                        Case "Custom"
                            For Each item As ParamItemUserSettingsInfo In Rpt.CustomOptions
                                'Response.Write(item.ControlSettingsCollection.Item)
                                For Each drillItem As ControlSettingInfo In item.ControlSettingsCollection
                                    For Each controlValueItem As ControlValueInfo In drillItem.ControlValueCollection
                                        Dim strKeyName As String
                                        Dim strKeyValue As String
                                        Dim strDisplayText As String = ""

                                        ' Check if the control name is empty
                                        If drillItem.ControlName = String.Empty Then Continue For

                                        Select Case drillItem.ControlName.Trim.ToString.ToLower
                                            Case Is = "radcombocampus"
                                                strKeyName = "campusid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radlistboxprogram2"
                                                strKeyName = "programid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radlistboxprogramversion2"
                                                strKeyName = "programversionid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radlistboxchargingmethod2"
                                                strKeyName = "chargingmethodid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radlistboxtermid2"
                                                strKeyName = "termid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "raddatereportdate"
                                                strKeyName = "raddatereportstartdate"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "raddatepicker1"
                                                strKeyName = "raddatereportenddate"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "chkshowsummary"
                                                strKeyName = "showsummary"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                        End Select
                                    Next
                                Next
                            Next
                        Case Else
                            Throw New Exception("Unknown Type")
                    End Select
                Next
                Dim getSelectedValues As New PaymentPeriodsReportSelectionValues
                With getSelectedValues
                    .PrgVerId = PrgVerId.ToString
                    .ProgramId = ProgramId.toString
                    .CampusId = CampusId.ToString
                    .ChargingMethodId = ChargingMethodId.ToString
                    .ReportPath = objReportInfo.RDLName
                    .Environment = strReportPath
                    .TransStartDate = TransStartDate
                    .TransEndDate = TransEndDate
                    .ShowSummary = ShowSummary
                    .TermId = TermId
                End With
                'intChec = kDataExists = GetProgressDA.GetProgressReportRecordCount(strStuEnrollId, strCampGrpId, strPrgVerId, strStatusCodeId, strTermId, strStudentGrpId, dtStartString, UserId.ToString, strStartStringModifier)
                'If intCheckDataExists >= 1 Then
                Dim strFormat As String = ""
                Select Case Rpt.ExportFormat
                    Case ReportInfo.ExportType.PDF
                        strFormat = "Pdf"
                    Case ReportInfo.ExportType.Excel
                        strFormat = "xlsx"
                    Case ReportInfo.ExportType.CSV
                        strFormat = "csv"
                End Select
                getReportOutput = CallRenderReport(strFormat, getSelectedValues, strSchoolName)
                Return getReportOutput
                'Else
                '    Return Nothing
                'End If
                'Return Nothing
            Catch ex As Exception
                Throw ex
            End Try
            Return Nothing
        End Function
        Private Sub BuildParamtersForReport(ByVal strFilterFieldName As String, _
                                            ByVal strFilterSelectedValue As String, _
                                            ByVal strDisplayText As String)
            Select Case strFilterFieldName.ToLower.Trim
                Case "campusid"
                    CampusId = strFilterSelectedValue.ToString
                Case "programid"
                    ProgramId = strFilterSelectedValue.ToString
                Case "programversionid"
                    PrgVerId &= strFilterSelectedValue.ToString
                Case "chargingmethodid"
                    ChargingMethodId &= strFilterSelectedValue.ToString & ","
                Case Is = "raddatereportstartdate"
                    If String.IsNullOrEmpty(strFilterSelectedValue) Then
                        TransStartDate = CDate("01/01/1900")
                    Else
                        TransStartDate = CDate(strFilterSelectedValue)
                    End If
                Case Is = "raddatereportenddate"
                    If String.IsNullOrEmpty(strFilterSelectedValue) Then
                        TransEndDate = CDate("01/01/1900")
                    Else
                        TransEndDate = CDate(strFilterSelectedValue)
                    End If
                Case Is = "showsummary"
                    ShowSummary = CBool(strFilterSelectedValue)
            End Select
        End Sub




        Public Function CallRenderReport(ByVal format As String, ByVal getFilterValues As PaymentPeriodsReportSelectionValues, ByVal strSchoolName As String) As [Byte]()
            Dim getReportOutput As [Byte]()
            Dim generateReport As New PaymentPeriodsReportGenerator
            getReportOutput = generateReport.RenderReport(format, getFilterValues, strSchoolName)
            Return getReportOutput
        End Function
    End Class
End Namespace