﻿Imports System.Web
Imports FAME.Advantage.Api.Library.Models
Imports FAME.Advantage.Common
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Reporting.Info
Imports FAME.Advantage.Reporting.Logic
Imports FAME.Parameters.Info

Namespace Logic
    Public Class PreliminaryandFallAnnual
        Protected MyAdvAppSettings As AdvAppSettings

        Dim ReportType As String = ""
        Dim ResId As String = ""
        Dim AcademicYear As String = ""
        Dim CampusId As String = ""
        Dim ProgramList As String = ""

        Public Function BuildReport(ByVal Rpt As ReportInfo, ByVal conn As String, ByVal UserId As String,
                                        ByVal strGradingMethod As String,
                                        ByVal strGPAMethod As String,
                                        ByVal strSchoolName As String,
                                        ByVal strTrackAttendanceBy As String,
                                        ByVal strGradeBookWeightingLevel As String,
                                        ByVal strStudentIdentifier As String,
                                        ByVal DisplayAttendanceUnitForProgressReportByClass As String,
                                        ByVal strReportPath As String) As [Byte]()
            Try
                Dim objReportInfo As ReportInfo = Rpt
                Dim getReportOutput As [Byte]()
                For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                    Select Case tabview.ParamSet.SetType
                        Case "Filter"
                        Case "Sort"
                        Case "Custom"
                            For Each item As ParamItemUserSettingsInfo In Rpt.CustomOptions
                                'Response.Write(item.ControlSettingsCollection.Item)
                                For Each drillItem As ControlSettingInfo In item.ControlSettingsCollection
                                    For Each controlValueItem As ControlValueInfo In drillItem.ControlValueCollection
                                        Dim strKeyName As String = ""
                                        Dim strKeyValue As String = ""
                                        Dim strDisplayText As String = ""
                                        Select Case drillItem.ControlName.Trim.ToString.ToLower
                                            Case Is = "radcomboreporttype"
                                                strKeyName = "radcomboreporttype"
                                            Case Is = "radcomboreportname"
                                                strKeyName = "radcomboreportname"
                                            Case Is = "radcomboyear"
                                                strKeyName = "radcomboyear"
                                            Case Is = "radcombocampus"
                                                strKeyName = "radcombocampus"
                                            Case Is = "radlistboxprograms2"
                                                strKeyName = "radlistboxprograms2"
                                        End Select
                                        strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                        BuildParametersForReport(strKeyName, strKeyValue, strDisplayText)
                                    Next
                                Next
                            Next
                        Case Else
                            Throw New Exception("Unknown Type")
                    End Select
                Next

                Dim apiToken As String
                Dim apiUrl As String
                Dim token As TokenResponse = CType(HttpContext.Current.Session("AdvantageApiToken"), TokenResponse)

                If (Not token Is Nothing) Then
                    apiToken = token.Token
                    apiUrl = token.ApiUrl
                End If

                Dim getSelectedValues As New PreliminaryAndFallAnnualSelectionValues
                With getSelectedValues
                    .Environment = strReportPath
                    .ReportType = If(String.IsNullOrEmpty(ReportType), Guid.Empty.ToString(), ReportType)
                    .CampusId = If(String.IsNullOrEmpty(ReportType), Guid.Empty.ToString(), CampusId)
                    .ResId = If(String.IsNullOrEmpty(ReportType), Guid.Empty.ToString(), ResId)
                    .Year = If(String.IsNullOrEmpty(AcademicYear), Guid.Empty.ToString(), AcademicYear)
                    .ProgramList = If(String.IsNullOrEmpty(ProgramList), Nothing, ProgramList)
                    .ApiToken = apiToken
                    .ApiUrl = apiUrl
                End With

                'since this report is 4 in one, set report path based on resId passed as parameter from UI
                getSelectedValues.ReportPath = GetReportPath(ResId)

                Dim strFormat As String = ""
                Select Case Rpt.ExportFormat
                    Case ReportInfo.ExportType.PDF
                        strFormat = "Pdf"
                    Case ReportInfo.ExportType.Excel
                        strFormat = "Excel"
                    Case ReportInfo.ExportType.CSV
                        strFormat = "csv"
                End Select
                If(ResId = "851" And strFormat="Pdf")
                    getSelectedValues.ReportPath = "NACCAS/CohortGrid_Pdf"
                End If
                If(ResId = "855" And strFormat="Pdf")
                    getSelectedValues.ReportPath = "NACCAS/ExemptedStudentsList_Pdf"
                End If
                getReportOutput = CallRenderReport(strFormat, getSelectedValues, strSchoolName)
                Return getReportOutput
            Catch ex As Exception
                Throw ex
            End Try
            Return Nothing
        End Function

        Private Sub BuildParametersForReport(ByVal strFilterFieldName As String,
                                            ByVal strFilterSelectedValue As String,
                                            ByVal strDisplayText As String)

            Select Case strFilterFieldName.ToLower.Trim
                Case = "radcomboreporttype"
                    ReportType = strFilterSelectedValue.ToString
                Case = "radcomboreportname"
                    ResId = strFilterSelectedValue.ToString
                Case = "radcomboyear"
                    AcademicYear = strFilterSelectedValue.ToString
                Case = "radcombocampus"
                    CampusId = strFilterSelectedValue.ToString
                Case "radlistboxprograms2"
                    ProgramList &= strFilterSelectedValue.ToString & ","

            End Select
        End Sub
        Public Function CallRenderReport(ByVal format As String, ByVal getFilterValues As PreliminaryAndFallAnnualSelectionValues, ByVal strSchoolName As String) As [Byte]()
            Dim getReportOutput As [Byte]()
            Dim generateReport As New PreliminaryAndFallAnnualReportGenerator

            'report generation
            getReportOutput = generateReport.RenderReport(format, getFilterValues, strSchoolName)
            Return getReportOutput
        End Function

        Public Function GetReportPath(strResId As String) As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim connectionString = MyAdvAppSettings.AppSettings("connectionString").ToString
            Dim db As AdvantageDataContext = New AdvantageDataContext(connectionString)

            Try
                Dim resId As Integer
                resId = Convert.ToInt32(strResId)

                Dim reportPath = (From Report In db.syReports
                                  Where Report.ResourceId.Equals(resId)
                                  Select Report.RDLName).Single

                Return reportPath
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class

End Namespace