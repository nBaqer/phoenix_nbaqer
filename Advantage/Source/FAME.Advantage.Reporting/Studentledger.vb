﻿Imports FAME.Advantage.Reporting.Info

Public Class StudentLedger

    Public Function BuildReportFromPage( 
                                        ByVal stuenrollId As String,
                                        ByVal strSchoolName As String,
                                        ByVal strReportPath As String) As [Byte]()
        Try
            Dim getReportOutput As [Byte]()
            Dim getSelectedValues As New StudentLedgerSelectionValues
            With getSelectedValues
                .ReportPath = "Ledger/LedgerMain"
                .Environment = strReportPath
                .StudentEnrollmentId = stuenrollId
            End With
          
            getReportOutput = CallRenderReport("Pdf", getSelectedValues, strSchoolName)
            Return getReportOutput
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
    Public Function CallRenderReport(ByVal format As String, ByVal getFilterValues As StudentLedgerSelectionValues, ByVal strSchoolName As String) As [Byte]()
        Dim getReportOutput As [Byte]()

        Dim generateReport As New StudentLedgerReportGenerator
        ''Update printed flag
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If

        'report generation
        getReportOutput = generateReport.RenderReport(format, getFilterValues, strSchoolName)

        '' if report was generated and mark for each enrollment
        'If getReportOutput.Length > 0 Then
        '    Dim DA As New TitleIVResultsDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        '    DA.UpdatePrintedFlagForEnrollment(getFilterValues.Studentenrollmentlist)
        'End If

        Return getReportOutput
    End Function
End Class
