﻿Imports FAME.Advantage.Reporting.ReportService2005
Imports FAME.Advantage.Reporting.ReportExecution2005
Imports System.Web
Imports FAME.Advantage.Common
Namespace Logic
    Public Class ReportGeneratorFor1098T
#Region "Report Variables"
        Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        Private _rptService As New ReportingService2005
        Private _rptExecutionService As New ReportExecution2005.ReportExecutionService

        Private _strReportName As String = Nothing
        Private _strHistoryId As String = Nothing
        Private _boolForRendering As Boolean = False
        Private _credentials As ReportService2005.DataSourceCredentials() = Nothing
        Private _rptParameterValues As ReportService2005.ParameterValue() = Nothing
        Private _rptParameters As ReportService2005.ReportParameter() = Nothing
        Private _rptExecutionServiceParameterValues As ReportExecution2005.ParameterValue() = Nothing
        Private _rptExecutionInfoObj As New ExecutionInfo
        Dim _strDateRangeTextPrevious As String
        Dim _strDateRangeText2YrPrevious As String

        'Variables needed to render report
        Private _historyId As String = Nothing
        Private _deviceInfo As String = Nothing
        Private _format As String = Nothing
        Private _generateReportAsBytes As [Byte]()
        Private _encoding As String = [String].Empty
        Private Shared _strExtension As String = String.Empty
        Private Shared _strMimeType As String = String.Empty
        Private _warnings As ReportExecution2005.Warning() = Nothing
        Private _streamIDs As String() = Nothing
        Private _strDateRangeText As String
        Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString).ToLower
        Dim strSchoolName As String = MyAdvAppSettings.AppSettings("SchoolName").ToString
        Dim strServerName As String = GetServerName(strConnectionString)
        Dim strDBName As String = GetDatabaseName(strConnectionString)
        Dim strUserName As String = GetUserName(strConnectionString)
        Dim strPassword As String = GetPassword(strConnectionString)
#End Region

#Region "Initialize Web Services"
        Private Sub InitializeWebServices()
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            'Set the credentials and url for the report services
            'The report service object is needed to get the parameter details associated with a specific report
            _rptService.Credentials = System.Net.CredentialCache.DefaultCredentials
            _rptService.Url = MyAdvAppSettings.AppSettings("ReportServices").ToString

            'Set the credentials and url for the report execution services
            _rptExecutionService.Credentials = System.Net.CredentialCache.DefaultCredentials
            _rptExecutionService.Url = MyAdvAppSettings.AppSettings("ReportExecutionServices").ToString
        End Sub
#End Region

#Region "Initialize Export Formats"
        Private Shared Sub ConfigureExportFormat(ByRef strType As String)
            Select Case strType.ToUpper()
                Case "PDF"
                    _strExtension = "pdf"
                    _strMimeType = "application/pdf"
                    Exit Select
                Case "EXCEL", "XLS"
                    _strExtension = "xls"
                    _strMimeType = "application/vnd.excel"
                    Exit Select
                Case "WORD"
                    _strExtension = "doc"
                    _strMimeType = "application/vnd.ms-word"
                Case "CSV"
                    _strExtension = "csv"
                    _strMimeType = "text/csv"
                Case Else
                    Throw New Exception("Unrecognized type: " & strType & ". Type must be PDF, Excel or Image, HTML.")
            End Select
        End Sub
#End Region

#Region "Utility Functions"
        Function GetDatabaseName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("database=")
            If startIndex > -1 Then startIndex += 9
            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""
            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length
            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetServerName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("server=")
            If startIndex > -1 Then startIndex += 7

            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetUserName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("uid=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetPassword(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("pwd=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
#End Region


#Region "Build Report"
        Public Function RenderReport(ByVal _format As String, _
                                                                    ByVal User As String, _
                                                                    ByVal strRoleId As String, _
                                                                    ByVal strRole As String, _
                                                                    ByVal strModuleId As String,
                                                                    ByVal strModule As String, _
                                                                    ByVal strAdvantageUserName As String, _
                                                                    ByVal strPageType As String, _
                                                                    ByVal strReportPath As String, _
                                                                    ByVal intTabId As Integer, _
                                                                    ByVal intResourceTypeId As Integer, _
                                                                    ByVal campusId As String, _
                                                                    ByVal intSchoolOptions As Integer) As [Byte]()

            'Initialize the report services and report execution services
            InitializeWebServices()

            'Set the path of report
            _strReportName = strReportPath + "1098T/Exceptions" '"/Advantage Reports/" + getReportParametersObj.Environment + "/Security/ManageSecurity" 'getReportParametersObj.ReportPath   '"/Advantage Reports/StudentAccounts/IPEDS/IPEDSMissingDataReport"

            'Use the Report Services: GetReportParameters method to get a list of parameters for a specified report
            'This is the only location where the reportingservices2005 is used
            _rptParameters = _rptService.GetReportParameters(_strReportName, _strHistoryId, _boolForRendering, _rptParameterValues, _credentials)

            'Need to load the specific report before passing the parameter values
            _rptExecutionInfoObj = _rptExecutionService.LoadReport(_strReportName, _strHistoryId)

            ' Prepare report parameter. 
            _rptExecutionServiceParameterValues = BuildReports(_rptParameters, "", campusId)

            _rptExecutionService.Timeout = System.Threading.Timeout.Infinite

            'Set Report Parameters
            _rptExecutionService.SetExecutionParameters(_rptExecutionServiceParameterValues, "en-us")

            'Call ConfigureExportFormat to get the file extensions
            ConfigureExportFormat(_format)

            'Render the report
            _generateReportAsBytes = _rptExecutionService.Render(_format, _deviceInfo, _strExtension, _encoding, _strMimeType, _warnings, _streamIDs)

            Return _generateReportAsBytes
        End Function
        Private Function BuildReports(ByVal rptParameters As ReportService2005.ReportParameter(), _
                                          ByVal strStudentId As String, _
                                          ByVal CampusId As String) As ReportExecution2005.ParameterValue()

            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptParameters.Length - 1) {}
            If rptParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                'CampusId
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "CampusId"
                rptExecutionParamValues(0).Name = "CampusId"
                If CampusId.Trim = "" Then
                    rptExecutionParamValues(0).Value = Nothing
                Else
                    rptExecutionParamValues(0).Value = CampusId
                End If

                'ModuleId Parameter
                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "StudentIdentifier"
                rptExecutionParamValues(1).Name = "StudentIdentifier"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not strStudentId.Trim = "" Then
                    rptExecutionParamValues(1).Value = ""
                Else
                    rptExecutionParamValues(1).Value = Nothing
                End If

                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "servername"
                rptExecutionParamValues(2).Name = "servername"
                rptExecutionParamValues(2).Value = strServerName

                'Database Name
                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "databasename"
                rptExecutionParamValues(3).Name = "databasename"
                rptExecutionParamValues(3).Value = strDBName


                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "uid"
                rptExecutionParamValues(4).Name = "uid"
                rptExecutionParamValues(4).Value = strUserName

                'password
                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "password"
                rptExecutionParamValues(5).Name = "password"
                rptExecutionParamValues(5).Value = strPassword

          End If
            Return rptExecutionParamValues
        End Function
#End Region

        Private Function GetAdvAppSettings() As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            Return MyAdvAppSettings
        End Function
    End Class
End Namespace

