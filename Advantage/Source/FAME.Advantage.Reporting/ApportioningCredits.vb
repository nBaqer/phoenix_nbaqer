﻿Imports FAME.Advantage.Reporting.Info
Imports FAME.Parameters.Collections
Imports FAME.Parameters
Imports FAME.Parameters.Info
Imports FAME.Advantage.DataAccess.LINQ

Imports FAME.AdvantageV1.BusinessFacade





Namespace Logic
    Public Class ApportioningCredits
        Dim AsOfDate As Date
        Dim CampusGroupId As String
        Dim CampGrpDescrip As String
        Dim ProgramVersionId As String
        Dim ProgramVersionDescrip As String
        Dim EnrollmentStatusId As String
        Dim EnrollmentStatusDescrip As String
        Dim boolShowPageNumber As Boolean = True
        Dim boolShowDateInFooter As Boolean = False

        Public Function BuildReport(ByVal Rpt As ReportInfo, ByVal conn As String, ByVal UserId As String, _
                                                              ByVal strGradingMethod As String, _
                                                              ByVal strGPAMethod As String, _
                                                              ByVal strSchoolName As String, _
                                                              ByVal strTrackAttendanceBy As String, _
                                                              ByVal strGradeBookWeightingLevel As String, _
                                                              ByVal strStudentIdentifier As String, _
                                                              ByVal DisplayAttendanceUnitForProgressReportByClass As String, _
                                                              ByVal strReportPath As String) As [Byte]()

            'Dim strSchoolName As String
            Try
                Dim objReportInfo As ReportInfo = Rpt
                Dim getReportOutput As [Byte]()
                For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                    Select Case tabview.ParamSet.SetType
                        Case "Filter"
                            For Each item As DictionaryEntry In Rpt.Filters
                                Dim Details As DetailDictionary = DirectCast(item.Value, DetailDictionary)
                                For Each d1 As DictionaryEntry In Details
                                    Dim objParamValue As ParamValueDictionary = DirectCast(d1.Value, ParamValueDictionary)
                                    For Each d2 As DictionaryEntry In objParamValue
                                        Dim ParameterModifier As String = d2.Key.ToString()
                                        Dim ModifierDictionary As ModDictionary = DirectCast(d2.Value, ModDictionary)
                                        Dim ModName As String = ModifierDictionary.Name
                                        For Each d3 As DictionaryEntry In ModifierDictionary
                                            Dim ParameterName As String = d3.Key.ToString
                                            Dim objValueList As ValueList = DirectCast(d3.Value, ValueList)
                                            Dim objValueType As String = objValueList.GetType.ToString
                                            If objValueType.Contains("[System.Guid]") Then
                                                Dim SelectedValues As ParamValueList(Of Guid) = DirectCast(objValueList, ParamValueList(Of Guid))
                                                Dim strSelectedFilterValues As String = ""
                                                '  strSelectedFilterValues = "'"  'Start a single quote
                                                For Each selection As Guid In SelectedValues
                                                    strSelectedFilterValues &= selection.ToString & ","
                                                Next
                                                If strSelectedFilterValues.Length >= 36 Then
                                                    strSelectedFilterValues = Mid(strSelectedFilterValues, 1, InStrRev(strSelectedFilterValues, ",") - 1)
                                                Else
                                                    strSelectedFilterValues = ""
                                                End If
                                                BuildParamtersForReport(objParamValue.Name.ToLower, strSelectedFilterValues, "")
                                            ElseIf objValueType.Contains("[System.DateTime]") Then
                                                Dim SelectedValues As ParamValueList(Of Date) = DirectCast(objValueList, ParamValueList(Of Date))
                                                For Each selection As Date In SelectedValues
                                                    Dim strModifier As String = ConvertModifierFromTextToSymbol(ParameterModifier)
                                                    Dim strDateValue As String = selection.ToShortDateString
                                                    BuildParamtersForReport(objParamValue.Name.ToLower, strDateValue, strModifier)
                                                Next
                                            Else
                                                Throw New Exception("Unknown Type")
                                            End If
                                        Next
                                    Next
                                Next
                            Next
                        Case "Sort"
                        Case "Custom"
                            For Each item As ParamItemUserSettingsInfo In Rpt.CustomOptions
                                'Response.Write(item.ControlSettingsCollection.Item)
                                For Each drillItem As ControlSettingInfo In item.ControlSettingsCollection

                                    For Each controlValueItem As ControlValueInfo In drillItem.ControlValueCollection
                                        Dim strKeyName As String = ""
                                        Dim strKeyValue As String = ""
                                        Dim strDisplayText As String = ""

                                        Select Case drillItem.ControlName.Trim.ToString.ToLower
                                            Case Is = "raddateasofdate"
                                                strKeyName = "AsOfDate"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radcombocampusgroup"
                                                strKeyName = "CampusGroupId"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radcomboprogramversions"
                                                strKeyName = "ProgramVersionId"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radcomboenrollmentstatuses"
                                                strKeyName = "EnrollmentStatuses"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                strDisplayText = controlValueItem.DisplayText.ToString.Trim
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                        End Select
                                    Next
                                Next
                            Next
                        Case Else
                            Throw New Exception("Unknown Type")
                    End Select
                Next

                If Rpt.PagingOption = ReportInfo.PagingStyle.None Then boolShowPageNumber = False
                If Rpt.ReportDateOption = ReportInfo.ReportDateStyle.Yes Then boolShowDateInFooter = True

                Dim getSelectedValues As New ApportioningCreditsSelectionValues
                With getSelectedValues
                    .AsOfDate = CStr(AsOfDate)
                    .CampGrpId = CampusGroupId
                    .ProgVerId = ProgramVersionId
                    .EnrollmentStatus = EnrollmentStatusId

                    .ReportPath = objReportInfo.RDLName
                    .ResourceId = objReportInfo.ResourceId
                    '.ShowPageNumber = boolShowPageNumber
                    '.ShowDateInFooter = boolShowDateInFooter
                    .Environment = strReportPath
                End With


                Dim GetApportioningCredits As New ApportioningCreditsFacade
                Dim intCheckDataExists As Integer = 0
                intCheckDataExists = GetApportioningCredits.GetApportioningCreditsCount(New Guid(CampusGroupId), New Guid(ProgramVersionId), New Guid(EnrollmentStatusId), CDate(CStr(AsOfDate)))
                If intCheckDataExists >= 1 Then
                    Dim strFormat As String = ""
                    Select Case Rpt.ExportFormat
                        Case ReportInfo.ExportType.PDF
                            strFormat = "Pdf"
                        Case ReportInfo.ExportType.Excel
                            strFormat = "Excel"
                        Case ReportInfo.ExportType.CSV
                            strFormat = "csv"
                    End Select

                    getReportOutput = CallRenderReport(strFormat, getSelectedValues, strSchoolName)
                    Return getReportOutput
                Else
                    Return Nothing
                End If
                Return Nothing

            Catch ex As Exception
                Throw New ArgumentException(ex.Message)
            End Try
            Return Nothing
        End Function
        Private Sub BuildParamtersForReport(ByVal strFilterFieldName As String, _
                                            ByVal strFilterSelectedValue As String, _
                                            ByVal strDisplayText As String)
            Select Case strFilterFieldName.ToLower.Trim
                Case "asofdate"
                    If strFilterSelectedValue.ToString = "" Then
                        AsOfDate = CDate("01/01/" + (Date.Now.Year).ToString)
                    Else
                        AsOfDate = CDate(strFilterSelectedValue.ToString)
                    End If

                Case "campgrpid"
                    If strFilterSelectedValue.ToString = "" Then
                        CampusGroupId = ""
                    Else
                        CampusGroupId = strFilterSelectedValue.ToString
                    End If

                Case "progverid"
                    If strFilterSelectedValue.ToString = "" Then
                        ProgramVersionId = ""
                    Else
                        ProgramVersionId = strFilterSelectedValue.ToString
                    End If

                Case "statuscodeid"
                    If strFilterSelectedValue.ToString = "" Then
                        EnrollmentStatusId = ""
                    Else
                        EnrollmentStatusId = strFilterSelectedValue.ToString
                    End If
            End Select
        End Sub
        Public Function CallRenderReport(ByVal format As String, ByVal getFilterValues As ApportioningCreditsSelectionValues, ByVal strSchoolName As String) As [Byte]()
            Dim getReportOutput As [Byte]()
            Dim generateReport As New ApportioningCreditsReportGenerator
            getReportOutput = generateReport.RenderReport(format, getFilterValues, strSchoolName)
            Return getReportOutput
        End Function
        Private Function ConvertModifierFromTextToSymbol(ByVal Modifier As String) As String
            Dim strReturnSymbol As String = ""
            Select Case Modifier.Trim.ToLower
                Case "isequalto"
                    strReturnSymbol = "="
                Case "greaterthanorequalto"
                    strReturnSymbol = ">="
                Case "greaterthan"
                    strReturnSymbol = ">"
                Case "lessthanorequalto"
                    strReturnSymbol = "<="
                Case "lessthan"
                    strReturnSymbol = "<"
            End Select
            Return strReturnSymbol
        End Function
    End Class
End Namespace