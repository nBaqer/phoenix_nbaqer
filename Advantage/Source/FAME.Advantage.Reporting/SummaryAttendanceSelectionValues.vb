﻿Public Class SummaryAttendanceSelectionValues
#Region "Variables"
    Private _studentenrollmentlist As String
    Private _programVersionList As String
    Private _studentGroupsList As String
    Private _ReportPath As String
    Private _environment As String
    Private _startDate As DateTime
    Private _endDate As DateTime
#End Region
#Region "Address Initialize"
Public Sub New()

End Sub
#End Region

    #Region "Properties"
    Public Property Studentenrollmentlist() As String
        Get
            Return _studentenrollmentlist
        End Get
        Set(ByVal value As String)
            _studentenrollmentlist = value
        End Set
    End Property

    Public Property ProgramVersionlist() As String
        Get
            Return _programVersionList
        End Get
        Set(ByVal value As String)
            _programVersionList = value
        End Set
    End Property

    Public Property StudentGroupsList() As String
        Get
            Return _studentGroupsList
        End Get
        Set(ByVal value As String)
            _studentGroupsList = value
        End Set
    End Property

    Public Property ReportPath() As String
        Get
            Return _ReportPath
        End Get
        Set(ByVal value As String)
            _ReportPath = value
        End Set
    End Property
    Public Property Environment() As String
        Get
            Return _environment
        End Get
        Set(ByVal value As String)
            _environment = value
        End Set
    End Property

    Public Property StartDate() As DateTime
        Get
            Return _startDate
        End Get
        Set(ByVal value As DateTime)
            _startDate = value
        End Set
    End Property

    Public Property EndDate() As DateTime
        Get
            Return _endDate
        End Get
        Set(ByVal value As DateTime)
            _endDate = value
        End Set
    End Property

#End Region
End Class
