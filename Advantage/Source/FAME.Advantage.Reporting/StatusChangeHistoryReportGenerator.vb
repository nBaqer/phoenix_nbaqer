﻿Imports FAME.Advantage.Reporting.ReportService2005
Imports FAME.Advantage.Reporting.ReportExecution2005
Imports System.Web
Imports FAME.Advantage.Common
Imports FAME.Advantage.Reporting.Info

Namespace Logic

    Public Class StatusChangeHistoryReportGenerator
#Region "Report Variables"
        Private ReadOnly _myAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        Private ReadOnly _rptService As New ReportingService2005
        Private ReadOnly _rptExecutionService As New ReportExecutionService

        Private _strReportName As String = Nothing
        Private ReadOnly _strHistoryId As String = Nothing
        Private _boolForRendering As Boolean = False
        Private ReadOnly _credentials As ReportService2005.DataSourceCredentials() = Nothing
        Private ReadOnly _rptParameterValues As ReportService2005.ParameterValue() = Nothing
        Private _rptParameters As ReportService2005.ReportParameter() = Nothing
        Private _rptExecutionServiceParameterValues As ReportExecution2005.ParameterValue() = Nothing
        Private _rptExecutionInfoObj As New ExecutionInfo
        'Variables needed to render report
        Private ReadOnly _deviceInfo As String = Nothing
        Private _format As String = Nothing
        Private _generateReportAsBytes As [Byte]()
        Private _encoding As String = [String].Empty
        Private Shared _strExtension As String = String.Empty
        Private Shared _strMimeType As String = String.Empty
        Private _warnings As ReportExecution2005.Warning() = Nothing
        Private _streamIDs As String() = Nothing
        Private ReadOnly _strConnectionString As String = (_myAdvAppSettings.AppSettings("ConString").ToString).ToLower
        Private ReadOnly _strServerName As String = GetServerName(_strConnectionString)
        Private ReadOnly _strDbName As String = GetDatabaseName(_strConnectionString)
        Private ReadOnly _strUserName As String = GetUserName(_strConnectionString)
        Private ReadOnly _strPassword As String = GetPassword(_strConnectionString)
#End Region

#Region "Initialize Web Services"
        Private Sub InitializeWebServices()

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            'Set the credentials and url for the report services
            'The report service object is needed to get the parameter details associated with a specific report
            _rptService.Credentials = Net.CredentialCache.DefaultCredentials
            _rptService.Url = MyAdvAppSettings.AppSettings("ReportServices").ToString

            'Set the credentials and url for the report execution services
            _rptExecutionService.Credentials = Net.CredentialCache.DefaultCredentials
            _rptExecutionService.Url = MyAdvAppSettings.AppSettings("ReportExecutionServices").ToString
        End Sub
#End Region

#Region "Initialize Export Formats"
        Private Shared Sub ConfigureExportFormat(ByRef strType As String)
            Select Case strType.ToUpper()
                Case "PDF"
                    _strExtension = "pdf"
                    _strMimeType = "application/pdf"
                    Exit Select
                Case "EXCEL", "XLS"
                    _strExtension = "xls"
                    _strMimeType = "application/vnd.excel"
                    Exit Select
                Case "WORD"
                    _strExtension = "doc"
                    _strMimeType = "application/vnd.ms-word"
                Case "CSV"
                    _strExtension = "csv"
                    _strMimeType = "text/csv"
                Case Else
                    Throw New Exception("Unrecognized type: " & strType & ". Type must be PDF, Excel or Image, HTML.")
            End Select
        End Sub
#End Region

#Region "Utility Functions"
        Function GetDatabaseName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("database=")
            If startIndex > -1 Then startIndex += 9
            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""
            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length
            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetServerName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("server=")
            If startIndex > -1 Then startIndex += 7

            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetUserName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("uid=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetPassword(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("pwd=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
#End Region


#Region "Build Report"
        Public Function RenderReport(ByVal format As String, ByVal getReportParameterObj As StatusChangeHistorySelectionValues, ByVal strReportPath As String) As [Byte]()
            Try


                'Initialize the report services and report execution services
                InitializeWebServices()

                'Set the path of report 
                _strReportName = strReportPath + "StatusChangeHistory/StuStatusChangeHistory"

                'Use the Report Services: GetReportParameters method to get a list of parameters for a specified report
                'This is the only location where the reportingservices2005 is used
                _rptParameters = _rptService.GetReportParameters(_strReportName, _strHistoryId, _boolForRendering, _rptParameterValues, _credentials)

                'Need to load the specific report before passing the parameter values 
                _rptExecutionInfoObj = _rptExecutionService.LoadReport(_strReportName, _strHistoryId)

                ' Prepare report parameter. 
                _rptExecutionServiceParameterValues = BuildReportParameters(_rptParameters, getReportParameterObj)

                _rptExecutionService.Timeout = Threading.Timeout.Infinite

                'Set Report Parameters
                _rptExecutionService.SetExecutionParameters(_rptExecutionServiceParameterValues, "en-us")

                'Call ConfigureExportFormat to get the file extensions
                ConfigureExportFormat(format)

                'Render the report
                _generateReportAsBytes = _rptExecutionService.Render(format, _deviceInfo, _strExtension, _encoding, _strMimeType, _warnings, _streamIDs)
            Catch ex As Exception

            End Try
            Return _generateReportAsBytes
        End Function
        Private Function BuildReportParameters(ByVal rptParameters As ReportService2005.ReportParameter(), ByVal getReportParametersObj As StatusChangeHistorySelectionValues) As ReportExecution2005.ParameterValue()

            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptParameters.Length - 1) {}
            If rptParameters.Length > 0 Then

                '!!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "ServerName"
                rptExecutionParamValues(0).Name = "ServerName"
                rptExecutionParamValues(0).Value = _strServerName

                'Database Name
                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "DatabaseName"
                rptExecutionParamValues(1).Name = "DatabaseName"
                rptExecutionParamValues(1).Value = _strDbName


                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "UserName"
                rptExecutionParamValues(2).Name = "UserName"
                rptExecutionParamValues(2).Value = _strUserName

                'password
                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "Password"
                rptExecutionParamValues(3).Name = "Password"
                rptExecutionParamValues(3).Value = _strPassword

                'StudentenrollmentId
                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "StudentEntrollmentId"
                rptExecutionParamValues(4).Name = "StudentEntrollmentId"
                If getReportParametersObj.StudentEntrollmentId.Trim = "" Then
                    rptExecutionParamValues(4).Value = Nothing
                Else
                    rptExecutionParamValues(4).Value = getReportParametersObj.StudentEntrollmentId
                End If
            End If
            Return rptExecutionParamValues
        End Function
#End Region
       
        Private Function GetAdvAppSettings() As AdvAppSettings
            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If
            Return myAdvAppSettings
        End Function
    End Class
End Namespace
