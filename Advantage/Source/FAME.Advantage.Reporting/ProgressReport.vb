Imports FAME.Advantage.Reporting.Info
Imports FAME.Parameters.Collections
Imports FAME.Parameters.Info
Imports FAME.Advantage.DataAccess.LINQ
Namespace Logic
    Public Class ProgressReport
        Dim strCampGrpId As String = ""
        Dim strPrgVerId As String = ""
        Dim strStatusCodeId As String = ""
        Dim strTermId As String = ""
        Dim strStudentGrpId As String = ""
        Dim strSortString As String = ""
        Dim boolGroupByWorkUnits As Boolean = True
        Dim strGrdComponentTypeId As String = ""
        Dim boolShowFinanceCalculations As Boolean = False
        Dim boolShowStudentSignatureLine As Boolean = False
        Dim boolShowSchoolSignatureLine As Boolean = False
        Dim boolShowPageNumber As Boolean = True
        Dim boolShowHeading As Boolean = True
        Dim strStartDateModifier As String = ""
        Dim strStartDate As String = ""
        Dim strExpectedGradDateModifier As String = ""
        Dim strExpectedGradDate As String = ""
        Dim strStuEnrollId As String = ""
        Dim boolShowWeeklySchedule As Boolean = False
        Dim boolShowTermModule As Boolean = False
        Dim boolShowDateInFooter As Boolean = False
        Dim strTermId_StartDate As String = ""
        Dim strStudent_Identifier As String = ""
        Dim strCampusId As String = ""
        Dim boolShowAllEnrollments As Boolean = False

        Public Function BuildReport(ByVal Rpt As ReportInfo, ByVal conn As String, ByVal UserId As String,
                                                                ByVal strGradingMethod As String,
                                                                ByVal strGPAMethod As String,
                                                                ByVal strSchoolName As String,
                                                                ByVal strTrackAttendanceBy As String,
                                                                ByVal strGradeBookWeightingLevel As String,
                                                                ByVal strStudentIdentifier As String,
                                                                ByVal DisplayAttendanceUnitForProgressReportByClass As String,
                                                                ByVal strReportPath As String) As [Byte]()
            Dim TestText As String = String.Empty
            Try
                Dim objReportInfo As ReportInfo = Rpt
                For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                    Select Case tabview.ParamSet.SetType
                        Case "Filter"
                            Dim Filters As MasterDictionary = Rpt.Filters
                            Dim FCollectionName As String = Filters.Name

                            For Each d0 As DictionaryEntry In Filters
                                Dim Details As DetailDictionary = DirectCast(d0.Value, DetailDictionary)
                                Dim DCollectionName As String = Details.Name
                                Dim DName As String = d0.Key.ToString
                                TestText = TestText & " <hr /> Control Name: " & DName & "<br />"
                                For Each d1 As DictionaryEntry In Details
                                    Dim SourceName As String = d1.Key.ToString()
                                    TestText = TestText & "Filter Name: " & SourceName & "<br />"
                                    Dim objParamValue As ParamValueDictionary = DirectCast(d1.Value, ParamValueDictionary)
                                    For Each d2 As DictionaryEntry In objParamValue
                                        Dim ParameterModifier As String = d2.Key.ToString()
                                        TestText = TestText & "Database Key: " & objParamValue.Name & "<br />"
                                        TestText = TestText & "Modifier Type: " & ParameterModifier & "<br />"
                                        Dim ModifierDictionary As ModDictionary = DirectCast(d2.Value, ModDictionary)
                                        Dim ModName As String = ModifierDictionary.Name
                                        For Each d3 As DictionaryEntry In ModifierDictionary
                                            Dim ParameterName As String = d3.Key.ToString

                                            TestText = TestText & "ParameterName: " & ParameterName & "<br />"
                                            Dim objValueList As ValueList = DirectCast(d3.Value, ValueList)
                                            Dim objValueType As String = objValueList.GetType.ToString

                                            If objValueType.Contains("[System.String]") Then
                                                Dim SelectedValues As ParamValueList(Of String) = DirectCast(objValueList, ParamValueList(Of String))
                                                Dim strSelectedFilterValues As String = ""
                                                ' strSelectedFilterValues = "'"  'Start a single quote
                                                For Each selection As String In SelectedValues
                                                    TestText = TestText & "Value: " & selection & "<br />"
                                                    strSelectedFilterValues &= selection.ToString
                                                Next
                                                If strSelectedFilterValues.Length >= 36 Then
                                                    strSelectedFilterValues = Mid(strSelectedFilterValues, 1, InStrRev(strSelectedFilterValues, ",") - 1) & "'"
                                                Else
                                                    strSelectedFilterValues = ""
                                                End If
                                                BuildParamtersForReport(objParamValue.Name, strSelectedFilterValues, "")
                                            ElseIf objValueType.Contains("[System.Guid]") Then
                                                Dim SelectedValues As ParamValueList(Of Guid) = DirectCast(objValueList, ParamValueList(Of Guid))
                                                Dim strSelectedFilterValues As String = ""
                                                '  strSelectedFilterValues = "'"  'Start a single quote
                                                For Each selection As Guid In SelectedValues
                                                    TestText = TestText & "Value: " & selection.ToString & "<br />"
                                                    strSelectedFilterValues &= selection.ToString & ","
                                                Next
                                                If strSelectedFilterValues.Length >= 36 Then
                                                    strSelectedFilterValues = Mid(strSelectedFilterValues, 1, InStrRev(strSelectedFilterValues, ",") - 1)
                                                Else
                                                    strSelectedFilterValues = ""
                                                End If
                                                BuildParamtersForReport(objParamValue.Name, strSelectedFilterValues, "")
                                            ElseIf objValueType.Contains("[System.Integer]") Then
                                                Dim SelectedValues As ParamValueList(Of Integer) = DirectCast(objValueList, ParamValueList(Of Integer))
                                                For Each selection As Integer In SelectedValues
                                                    TestText = TestText & "Value: " & selection.ToString & "<br />"
                                                Next
                                            ElseIf objValueType.Contains("[System.DateTime]") Then
                                                Dim SelectedValues As ParamValueList(Of Date) = DirectCast(objValueList, ParamValueList(Of Date))
                                                For Each selection As Date In SelectedValues
                                                    TestText = TestText & "Value: " & selection.ToString & "<br />"
                                                    Dim strModifier As String = ConvertModifierFromTextToSymbol(ParameterModifier)
                                                    Dim strDateValue As String = selection.ToShortDateString
                                                    BuildParamtersForReport(objParamValue.Name, strDateValue, strModifier)
                                                Next
                                            ElseIf objValueType.Contains("[System.Long]") Then
                                                Dim SelectedValues As ParamValueList(Of Long) = DirectCast(objValueList, ParamValueList(Of Long))
                                                For Each selection As Long In SelectedValues
                                                    TestText = TestText & "Value: " & selection.ToString & "<br />"
                                                Next
                                            ElseIf objValueType.Contains("[System.Boolean]") Then
                                                Dim SelectedValues As ParamValueList(Of Boolean) = DirectCast(objValueList, ParamValueList(Of Boolean))
                                                For Each selection As Boolean In SelectedValues
                                                    TestText = TestText & "Value: " & selection.ToString & "<br />"
                                                Next
                                            Else
                                                Throw New Exception("Unknown Type")
                                            End If
                                        Next
                                    Next
                                Next
                            Next

                        Case "Sort"
                            'Sort by Sort Sequence field
                            Rpt.Sorts.Sort(Function(p1, p2) p1.SortSeq.CompareTo(p2.SortSeq))
                            For Each item As SortItemInfo In Rpt.Sorts
                                strSortString &= item.FieldName.ToString
                                If item.IsDecending = True Then
                                    strSortString &= " Desc"
                                Else
                                    strSortString &= " Asc"
                                End If
                                strSortString &= ","
                            Next
                            If Not strSortString.Trim = "" Then strSortString = Mid(strSortString, 1, InStrRev(strSortString, ",") - 1).ToString.Trim
                        Case "Custom"
                            For Each item As ParamItemUserSettingsInfo In Rpt.CustomOptions
                                'Response.Write(item.ControlSettingsCollection.Item)
                                For Each drillItem As ControlSettingInfo In item.ControlSettingsCollection

                                    For Each controlValueItem As ControlValueInfo In drillItem.ControlValueCollection

                                        'Layout Options
                                        If drillItem.ControlName.Trim.ToString.ToLower = "rblshowworkunits" AndAlso
                                                            controlValueItem.DisplayText.ToString.Trim.ToLower = "do not show work units under each subject" AndAlso
                                                            controlValueItem.KeyData.ToString.Trim.ToLower = "true" Then
                                            boolGroupByWorkUnits = False
                                        End If
                                        If drillItem.ControlName.Trim.ToString.ToLower = "rblshowworkunits" AndAlso
                                                            controlValueItem.DisplayText.ToString.Trim.ToLower = "show work units under each subject" Then
                                            boolGroupByWorkUnits = True
                                        End If
                                        If boolGroupByWorkUnits = False AndAlso drillItem.ControlName.ToString.ToLower.Trim = "cblgroupbytype" Then
                                            If controlValueItem.KeyData.Trim.ToString.ToLower = "true" Then strGrdComponentTypeId = BuildGradeComponentTypeId(strGrdComponentTypeId, controlValueItem.DisplayText)
                                        End If

                                        ' Misc. options
                                        If drillItem.ControlName.ToString.ToLower.Trim = "cblcustomoptions" Then
                                            If controlValueItem.KeyData.Trim.ToString.ToLower = "true" AndAlso controlValueItem.DisplayText.ToString.Trim.ToString.ToLower = "show financial information" Then
                                                boolShowFinanceCalculations = True
                                            End If
                                            If controlValueItem.KeyData.Trim.ToString.ToLower = "true" AndAlso controlValueItem.DisplayText.ToString.Trim.ToString.ToLower = "show student signature line" Then
                                                boolShowStudentSignatureLine = True
                                            End If
                                            If controlValueItem.KeyData.Trim.ToString.ToLower = "true" AndAlso controlValueItem.DisplayText.ToString.Trim.ToString.ToLower = "show school official signature line" Then
                                                boolShowSchoolSignatureLine = True
                                            End If
                                            If controlValueItem.KeyData.Trim.ToString.ToLower = "true" AndAlso controlValueItem.DisplayText.ToString.Trim.ToString.ToLower = "show weekly schedule" Then
                                                boolShowWeeklySchedule = True
                                            End If
                                            If controlValueItem.KeyData.Trim.ToString.ToLower = "true" AndAlso controlValueItem.DisplayText.ToString.Trim.ToString.ToLower = "show term/module" Then
                                                boolShowTermModule = True
                                            End If
                                            If controlValueItem.KeyData.Trim.ToString.ToLower = "true" AndAlso controlValueItem.DisplayText.ToString.Trim.ToString.ToLower = "show all enrollments" Then
                                                boolShowAllEnrollments = True
                                            End If
                                        End If
                                    Next
                                Next
                            Next
                        Case "Group"
                            'Dim Groupings As MasterDictionary = Rpt.Groupings
                            'Dim GName As String = Groupings.Name
                        Case Else
                            Throw New Exception("Unknown Type")
                    End Select
                Next
                If Rpt.PagingOption = ReportInfo.PagingStyle.None Then boolShowPageNumber = False
                If Rpt.HeadingOption = ReportInfo.HeadingStyle.None Then boolShowHeading = False
                If Rpt.ReportDateOption = ReportInfo.ReportDateStyle.Yes Then boolShowDateInFooter = True

                Dim getSelectedValues As New ReportSelectionValues
                Dim GetProgressDA As New ProgressReportDA(conn)
                With getSelectedValues
                    .CampGrpId = strCampGrpId
                    .PrgVerId = strPrgVerId
                    .StatusCodeId = strStatusCodeId
                    .TermId = strTermId
                    .StudentGrpId = strStudentGrpId
                    .SortFields = strSortString
                    .GrdComponentTypeId = strGrdComponentTypeId
                    .GroupByWorkUnits = boolGroupByWorkUnits
                    .ShowFinanceCalculations = boolShowFinanceCalculations
                    .ShowStudentSignatureLine = boolShowStudentSignatureLine
                    .ShowSchoolSignatureLine = boolShowSchoolSignatureLine
                    .ShowPageNumber = boolShowPageNumber
                    .ShowHeading = boolShowHeading
                    .StartDateModifier = strStartDateModifier
                    .StartDate = strStartDate
                    .ExpectedGradDate = strExpectedGradDate
                    .ExpectedGradDateModifier = strExpectedGradDateModifier
                    .StuEnrollId = strStuEnrollId
                    .UserId = UserId.ToString
                    .GradingMethod = strGradingMethod.ToString
                    .GPAMethod = strGPAMethod.ToString
                    If strTrackAttendanceBy.ToString.ToLower = "byclass" Then
                        .TrackAttendanceBy = "class"
                    Else
                        .TrackAttendanceBy = "day"
                    End If
                    .SetGradeBookAt = strGradeBookWeightingLevel.ToString
                    .ShowWeeklySchedule = boolShowWeeklySchedule
                    .DisplayAttendanceUnitForProgressReportByClass = DisplayAttendanceUnitForProgressReportByClass.ToString
                    .ShowTermInReport = boolShowTermModule
                    .ShowDateInFooter = boolShowDateInFooter
                    .ReportPath = Rpt.RDLName
                    .ShowAllEnrollments = boolShowAllEnrollments
                    If Not strTermId.ToString.Trim = "" Then
                        Try
                            strTermId_StartDate = GetProgressDA.GetTermStartDate(strTermId.ToString.Trim).ToString("MM/dd/yyyy")
                        Catch ex As System.Exception
                            strTermId_StartDate = ""
                        End Try
                        .Term_StartDate = strTermId_StartDate
                    End If
                    .StudentIdentifier = strStudentIdentifier.ToString.Trim.ToLower
                    .CampusId = strCampusId.ToString.Trim.ToLower
                    .Environment = strReportPath
                End With
                Dim intCheckDataExists As Integer = 0
                Dim generateReport As New ReportGenerator
                Dim getReportOutput As [Byte]()
                Dim dtStartDate As Date = Nothing
                'If Date is blank then reset the modifier
                If Not strStartDate = "" Then
                    dtStartDate = CDate(strStartDate)
                Else
                    dtStartDate = Nothing
                    strStartDateModifier = ""
                End If
                If Not strTermId.ToString.Trim = "" Then
                    dtStartDate = Nothing
                    strStartDateModifier = ""
                End If



                intCheckDataExists = GetProgressDA.GetProgressReportRecordCount(strStuEnrollId, strCampGrpId, strPrgVerId, strStatusCodeId, strTermId, strStudentGrpId, dtStartDate, UserId.ToString, strStartDateModifier, strCampusId)
                If intCheckDataExists >= 1 Then
                    Dim strFormat As String = ""
                    Select Case Rpt.ExportFormat
                        Case ReportInfo.ExportType.PDF
                            strFormat = "Pdf"
                        Case ReportInfo.ExportType.Excel
                            strFormat = "Excel"
                        Case ReportInfo.ExportType.CSV
                            strFormat = "csv"
                        Case ReportInfo.ExportType.Word
                            strFormat = "word"
                    End Select

                    getReportOutput = CallRenderReport(strFormat, getSelectedValues, strSchoolName)
                    Return getReportOutput
                Else
                    Return Nothing
                End If
                Return Nothing
            Catch ex As Exception
                Throw New ArgumentException(ex.Message)
            End Try
        End Function
        Public Function CallRenderReport(ByVal format As String, ByVal getFilterValues As ReportSelectionValues, ByVal strSchoolName As String) As [Byte]()
            Dim getReportOutput As [Byte]()
            Dim generateReport As New ReportGenerator
            getReportOutput = generateReport.RenderReport(format, getFilterValues, strSchoolName)
            Return getReportOutput
        End Function
        Private Sub BuildParamtersForReport(ByVal strFilterFieldName As String, ByVal strFilterSelectedValue As String, ByVal Modifier As String)
            Select Case strFilterFieldName.ToLower.Trim
                Case "campgrpid"
                    strCampGrpId = strFilterSelectedValue.ToString
                Case "progverid"
                    strPrgVerId = strFilterSelectedValue.ToString
                Case "statuscodeid"
                    strStatusCodeId = strFilterSelectedValue.ToString
                Case "termid"
                    strTermId = strFilterSelectedValue.ToString
                Case "studentgrpid"
                Case "leadgrpid"
                    strStudentGrpId = strFilterSelectedValue.ToString
                Case "startdate"
                    strStartDate = strFilterSelectedValue.ToString
                    strStartDateModifier = Modifier.Trim
                Case "expgraddate"
                    strExpectedGradDate = strFilterSelectedValue.ToString
                    strExpectedGradDateModifier = Modifier.Trim
                Case "studentenrollmentid"
                    strStuEnrollId = strFilterSelectedValue.ToString
                Case "campusid"
                    strCampusId = strFilterSelectedValue.ToString
            End Select
        End Sub
        Private Function BuildGradeComponentTypeId(ByRef strGrdComponentTypeId As String, ByVal strGradeComponentDescription As String) As String
            If strGrdComponentTypeId.Trim.ToString.Length > 1 Then strGrdComponentTypeId &= ","
            Select Case strGradeComponentDescription.Trim.ToString.ToLower
                Case "homework"
                    strGrdComponentTypeId &= "499"
                Case "lab work"
                    strGrdComponentTypeId &= "500"
                Case "exam"
                    strGrdComponentTypeId &= "501"
                Case "final"
                    strGrdComponentTypeId &= "502"
                Case "lab hours"
                    strGrdComponentTypeId &= "503"
                Case "practical exams"
                    strGrdComponentTypeId &= "533"
                Case "externships"
                    strGrdComponentTypeId &= "544"
                Case "externship"
                    strGrdComponentTypeId &= "544"
            End Select
            Return strGrdComponentTypeId.ToString
        End Function
        Private Function ConvertModifierFromTextToSymbol(ByVal Modifier As String) As String
            Dim strReturnSymbol As String = ""
            Select Case Modifier.Trim.ToLower
                Case "isequalto"
                    strReturnSymbol = "="
                Case "greaterthanorequalto"
                    strReturnSymbol = ">="
                Case "greaterthan"
                    strReturnSymbol = ">"
                Case "lessthanorequalto"
                    strReturnSymbol = "<="
                Case "lessthan"
                    strReturnSymbol = "<"
            End Select
            Return strReturnSymbol
        End Function
    End Class
End Namespace

