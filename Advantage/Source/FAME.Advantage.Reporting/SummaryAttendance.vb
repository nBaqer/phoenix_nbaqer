﻿Imports System.Web
Imports FAME.Advantage.Common
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Reporting.Info
Imports FAME.Advantage.Reporting.Logic
Imports FAME.Parameters.Info

Namespace Logic
    Public Class SummaryAttendance
        Protected MyAdvAppSettings As AdvAppSettings
        Dim StudentenrollmentList As String = ""
        Dim ProgramVersionList As String = ""
        Dim StudentGroupList As String = ""
        Dim StartDate As Date
        Dim EndDate As  Date
        Public Function BuildReport(ByVal Rpt As ReportInfo, ByVal conn As String, ByVal UserId As String,
                                        ByVal strGradingMethod As String,
                                        ByVal strGPAMethod As String,
                                        ByVal strSchoolName As String,
                                        ByVal strTrackAttendanceBy As String,
                                        ByVal strGradeBookWeightingLevel As String,
                                        ByVal strStudentIdentifier As String,
                                        ByVal DisplayAttendanceUnitForProgressReportByClass As String,
                                        ByVal strReportPath As String) As [Byte]()
            Try
                Dim objReportInfo As ReportInfo = Rpt
                Dim getReportOutput As [Byte]()
                For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                    Select Case tabview.ParamSet.SetType
                        Case "Filter"
                        Case "Sort"
                        Case "Custom"
                            For Each item As ParamItemUserSettingsInfo In Rpt.CustomOptions
                                'Response.Write(item.ControlSettingsCollection.Item)
                                For Each drillItem As ControlSettingInfo In item.ControlSettingsCollection
                                    For Each controlValueItem As ControlValueInfo In drillItem.ControlValueCollection
                                        Dim strKeyName As String = ""
                                        Dim strKeyValue As String = ""
                                        Dim strDisplayText As String = ""
                                        Select Case drillItem.ControlName.Trim.ToString.ToLower
                                            Case Is = "radgridselectedstuenrollments"
                                                strKeyName = "studentenrollmentlist"
                                            Case Is = "radstartdatepicker"
                                                strKeyName = "radstartdatepicker"
                                            Case Is = "radenddatepicker"
                                                strKeyName = "radenddatepicker"
                                            Case Is = "radlistboxprogramversions2"
                                                strKeyName = "radlistboxprogramversions2"
                                            Case Is = "radlistboxstudentgroupselector2"
                                                strKeyName = "radlistboxstudentgroupselector2"

                                               

                                        End Select
                                        strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                        BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)

                                    Next
                                Next
                            Next
                        Case Else
                            Throw New Exception("Unknown Type")
                    End Select
                Next
                Dim getSelectedValues As New SummaryAttendanceSelectionValues
                With getSelectedValues
                    .ReportPath = objReportInfo.RDLName
                    .Environment = strReportPath
                    .Studentenrollmentlist = If(String.IsNullOrEmpty(StudentenrollmentList), Guid.Empty.ToString(), StudentenrollmentList)
                    .EndDate = EndDate
                    .StartDate = StartDate
                    .ProgramVersionlist = If(String.IsNullOrEmpty(ProgramVersionList), Guid.Empty.ToString(), ProgramVersionList)
                    .StudentGroupsList = If(String.IsNullOrEmpty(StudentGroupList), Guid.Empty.ToString(), StudentGroupList)
                End With
                Dim strFormat As String = ""
                Select Case Rpt.ExportFormat
                    Case ReportInfo.ExportType.PDF
                        strFormat = "Pdf"
                    Case ReportInfo.ExportType.Excel
                        strFormat = "Excel"
                    Case ReportInfo.ExportType.CSV
                        strFormat = "csv"
                End Select
                getReportOutput = CallRenderReport(strFormat, getSelectedValues, strSchoolName)
                Return getReportOutput
            Catch ex As Exception
                Throw ex
            End Try
            Return Nothing
        End Function

        Private Sub BuildParamtersForReport(ByVal strFilterFieldName As String,
                                            ByVal strFilterSelectedValue As String,
                                            ByVal strDisplayText As String)
            Select Case strFilterFieldName.ToLower.Trim
                Case "studentenrollmentlist"
                    StudentenrollmentList &= strFilterSelectedValue.ToString & ","
                Case "radstartdatepicker"
                    StartDate = CDate(strFilterSelectedValue)
                Case "radenddatepicker"
                    EndDate = CDate(strFilterSelectedValue)
                Case "radlistboxprogramversions2"
                    ProgramVersionList &= strFilterSelectedValue.ToString & ","
                Case "radlistboxstudentgroupselector2"
                    StudentGroupList &= strFilterSelectedValue.ToString & ","

            End Select
        End Sub
        Public Function CallRenderReport(ByVal format As String, ByVal getFilterValues As SummaryAttendanceSelectionValues, ByVal strSchoolName As String) As [Byte]()
            Dim getReportOutput As [Byte]()

            Dim generateReport As New SummaryAttendanceReportGenerator
            ''Update printed flag
            'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            'Else
            '    MyAdvAppSettings = New AdvAppSettings
            'End If

            'report generation
            getReportOutput = generateReport.RenderReport(format, getFilterValues, strSchoolName)

            '' if report was generated and mark for each enrollment
            'If getReportOutput.Length > 0 Then
            '    Dim DA As New TitleIVResultsDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
            '    DA.UpdatePrintedFlagForEnrollment(getFilterValues.Studentenrollmentlist)
            'End If

            Return getReportOutput
        End Function
    End Class

End Namespace