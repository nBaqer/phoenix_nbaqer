﻿Imports System.Web
Imports FAME.Advantage.Common
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.Reporting.ReportExecution2005
Imports FAME.Advantage.Reporting.ReportService2005

Public Class PreliminaryAndFallAnnualReportGenerator
#Region "Report Variables"
    Private _rptService As New ReportingService2005
    Private _rptExecutionService As New ReportExecution2005.ReportExecutionService

    Private _strReportName As String = Nothing
    Private _strHistoryId As String = Nothing
    Private _boolForRendering As Boolean = False
    Private _credentials As ReportService2005.DataSourceCredentials() = Nothing
    Private _rptParameterValues As ReportService2005.ParameterValue() = Nothing
    Private _rptParameters As ReportService2005.ReportParameter() = Nothing
    Private _rptExecutionServiceParameterValues As ReportExecution2005.ParameterValue() = Nothing
    Private _rptExecutionInfoObj As New ExecutionInfo

    'Variables needed to render report
    Private _historyId As String = Nothing
    Private _deviceInfo As String = Nothing
    Private _format As String = Nothing
    Private _generateReportAsBytes As [Byte]()
    Private _encoding As String = [String].Empty
    Private Shared _strExtension As String = String.Empty
    Private Shared _strMimeType As String = String.Empty
    Private _warnings As ReportExecution2005.Warning() = Nothing
    Private _streamIDs As String() = Nothing

    Dim strServerName As String = ""
    Dim strDBName As String = ""
    Dim strUserName As String = ""
    Dim strPassword As String = ""
#End Region

#Region "Initialize Web Services"
    Private Sub InitializeWebServices()

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString).ToLower
        _rptService.Credentials = System.Net.CredentialCache.DefaultCredentials
        _rptService.Url = MyAdvAppSettings.AppSettings("ReportServices").ToString
        'Set the credentials and url for the report execution services
        _rptExecutionService.Credentials = System.Net.CredentialCache.DefaultCredentials
        '_rptExecutionService.SetExecutionCredentials(dsCredentials)
        _rptExecutionService.Url = MyAdvAppSettings.AppSettings("ReportExecutionServices").ToString
    End Sub
#End Region

#Region "Initialize Export Formats"
    Private Shared Sub ConfigureExportFormat(ByRef strType As String)
        Select Case strType.ToUpper()
            Case "PDF"
                _strExtension = "pdf"
                _strMimeType = "application/pdf"
                Exit Select
            Case "EXCEL", "XLS"
                _strExtension = "xls"
                _strMimeType = "application/vnd.excel"
                Exit Select
            Case "WORD"
                _strExtension = "doc"
                _strMimeType = "application/vnd.ms-word"
            Case "CSV"
                _strExtension = "csv"
                _strMimeType = "text/csv"
            Case Else
                Throw New Exception("Unrecognized type: " & strType & ". Type must be PDF, Excel or Image, HTML.")
        End Select
    End Sub
#End Region


#Region "Build Report Parameters"
    Private Function BuildReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(),
                                                                       ByVal getReportParametersObj As PreliminaryAndFallAnnualSelectionValues,
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()

        Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}
        Dim strNullValue As String = "Null"

        Dim strProgramIds As String = ""
        Dim strReportType As String = ""
        Dim strResId As String = ""
        Dim strReportYear As String = ""
        Dim strCampusId As String = ""
        Dim strServerName As String = ""
        Dim strDBName As String = ""
        Dim strApiToken As String = ""
        Dim strApiUrl As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString).ToLower

        With getReportParametersObj
            strProgramIds = .ProgramList
            strReportType = .ReportType
            strResId = .ResId
            strReportYear = .Year
            strCampusId = .CampusId
            strApiToken = .ApiToken
            strApiUrl = .ApiUrl

            strServerName = GetServerName(strConnectionString)
            strDBName = GetDatabaseName(strConnectionString)
            strUserName = GetUserName(strConnectionString)
            strPassword = GetPassword(strConnectionString)
        End With

        'Pass the parameter values
        If rptServicesParameters.Length > 0 Then

            'is API Data Source
            If (strResId = "849") Then
                'ApiURL
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "ApiURL"
                rptExecutionParamValues(0).Name = "ApiURL"
                rptExecutionParamValues(0).Value = strApiUrl

                'CampusId
                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "CampusId"
                rptExecutionParamValues(1).Name = "CampusId"
                rptExecutionParamValues(1).Value = strCampusId

                'Year
                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "Year"
                rptExecutionParamValues(2).Name = "Year"
                rptExecutionParamValues(2).Value = strReportYear

                'Token
                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "Token"
                rptExecutionParamValues(3).Name = "Token"
                rptExecutionParamValues(3).Value = strApiToken
            Else
                'User name
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "uid"
                rptExecutionParamValues(0).Name = "uid"
                rptExecutionParamValues(0).Value = strUserName

                'password
                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "password"
                rptExecutionParamValues(1).Name = "password"
                rptExecutionParamValues(1).Value = strPassword

                'Server Name
                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "servername"
                rptExecutionParamValues(2).Name = "servername"
                rptExecutionParamValues(2).Value = strServerName

                'Database Name
                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "databasename"
                rptExecutionParamValues(3).Name = "databasename"
                rptExecutionParamValues(3).Value = strDBName

                'ReportType Parameter
                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "ReportType"
                rptExecutionParamValues(4).Name = "ReportType"
                If Not strReportType.Trim = "" Then
                    rptExecutionParamValues(4).Value = strReportType.ToString
                Else
                    rptExecutionParamValues(4).Value = Nothing
                End If

                'Year Parameter
                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "ReportYear"
                rptExecutionParamValues(5).Name = "ReportYear"
                If Not strReportYear.Trim = "" Then
                    rptExecutionParamValues(5).Value = strReportYear.ToString
                Else
                    rptExecutionParamValues(5).Value = Nothing
                End If

                'CampusId Parameter
                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "CampusId"
                rptExecutionParamValues(6).Name = "CampusId"
                If Not strCampusId.Trim = "" Then
                    rptExecutionParamValues(6).Value = strCampusId.ToString
                Else
                    rptExecutionParamValues(6).Value = Nothing
                End If

                'Program Parameter
                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "PrgIdList"
                rptExecutionParamValues(7).Name = "PrgIdList"
                If Not String.IsNullOrEmpty(strProgramIds) Then
                    rptExecutionParamValues(7).Value = strProgramIds.ToString
                Else
                    rptExecutionParamValues(7).Value = Nothing
                End If

                
            End If

        End If
        Return rptExecutionParamValues
    End Function

#End Region

#Region "Build Report"
    Public Function RenderReport(ByVal _format As String,
                                     ByVal getReportParametersObj As PreliminaryAndFallAnnualSelectionValues,
                                     ByVal strSchoolName As String) As [Byte]()

        'Initialize the report services and report execution services
        InitializeWebServices()

        'Set the path of report
        _strReportName = getReportParametersObj.Environment + getReportParametersObj.ReportPath
        'Use the Report Services: GetReportParameters method to get a list of parameters for a specified report
        'This is the only location where the reportingservices2005 is used
        _rptParameters = _rptService.GetReportParameters(_strReportName, _strHistoryId, _boolForRendering, _rptParameterValues, _credentials)

        'Need to load the specific report before passing the parameter values
        _rptExecutionInfoObj = _rptExecutionService.LoadReport(_strReportName, _strHistoryId)

        ' Prepare report parameter. 
        _rptExecutionServiceParameterValues = BuildReportParameters(_rptParameters, getReportParametersObj, strSchoolName)

        _rptExecutionService.Timeout = System.Threading.Timeout.Infinite

        'Set Report Parameters
        _rptExecutionService.SetExecutionParameters(_rptExecutionServiceParameterValues, "en-us")

        'Call ConfigureExportFormat to get the file extensions
        ConfigureExportFormat(_format)

        'Render the report
        _generateReportAsBytes = _rptExecutionService.Render(_format, _deviceInfo, _strExtension, _encoding, _strMimeType, _warnings, _streamIDs)

        Return _generateReportAsBytes
        'Display the report in the browser
        'DisplayReport(_generateReportAsBytes)
    End Function
#End Region

#Region "DisplayReport"
    Private Sub DisplayReport(ByVal byteReportOutput As [Byte]())
        'Pdf
        HttpContext.Current.Response.ClearContent()
        HttpContext.Current.Response.ContentType = _strMimeType
        HttpContext.Current.Response.BinaryWrite(byteReportOutput)
    End Sub
    Function GetDatabaseName(ByVal connString As String) As String
        Dim lcConnString = connString.ToLower()

        ' if this is a Jet database, find the index of the "data source" setting
        Dim startIndex As Integer
        startIndex = lcConnString.IndexOf("database=")
        If startIndex > -1 Then startIndex += 9
        ' if the "database", "data source" or "initial catalog" values are not 
        ' found, return an empty string
        If startIndex = -1 Then Return ""
        ' find where the database name/path ends
        Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
        If endIndex = -1 Then endIndex = lcConnString.Length
        ' return the substring with the database name/path
        Return connString.Substring(startIndex, endIndex - startIndex)
    End Function
    Function GetServerName(ByVal connString As String) As String
        Dim lcConnString = connString.ToLower()

        ' if this is a Jet database, find the index of the "data source" setting
        Dim startIndex As Integer
        startIndex = lcConnString.IndexOf("server=")
        If startIndex > -1 Then startIndex += 7

        ' if the "database", "data source" or "initial catalog" values are not 
        ' found, return an empty string
        If startIndex = -1 Then Return ""

        ' find where the database name/path ends
        Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
        If endIndex = -1 Then endIndex = lcConnString.Length

        ' return the substring with the database name/path
        Return connString.Substring(startIndex, endIndex - startIndex)
    End Function
    Function GetUserName(ByVal connString As String) As String
        Dim lcConnString = connString.ToLower()

        ' if this is a Jet database, find the index of the "data source" setting
        Dim startIndex As Integer
        startIndex = lcConnString.IndexOf("uid=")
        If startIndex > -1 Then startIndex += 4

        ' if the "uid" values are not 
        ' found, return an empty string
        If startIndex = -1 Then Return ""

        ' find where the database name/path ends
        Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
        If endIndex = -1 Then endIndex = lcConnString.Length

        ' return the substring with the database name/path
        Return connString.Substring(startIndex, endIndex - startIndex)
    End Function
    Function GetPassword(ByVal connString As String) As String
        Dim lcConnString = connString.ToLower()

        ' if this is a Jet database, find the index of the "data source" setting
        Dim startIndex As Integer
        startIndex = lcConnString.IndexOf("pwd=")
        If startIndex > -1 Then startIndex += 4

        ' if the "uid" values are not 
        ' found, return an empty string
        If startIndex = -1 Then Return ""

        ' find where the database name/path ends
        Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
        If endIndex = -1 Then endIndex = lcConnString.Length

        ' return the substring with the database name/path
        Return connString.Substring(startIndex, endIndex - startIndex)
    End Function
#End Region
End Class
