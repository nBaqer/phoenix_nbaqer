﻿
Imports FAME.Parameters.Collections
Imports FAME.Parameters.Info

Namespace Info
    <Serializable()>
    Public Class ReportInfo
        Public Enum AllowedExportType
            [PDFOnly] = 0
            [ExcelOnly] = 1
            [CSVOnly] = 2
            [PDFCSV] = 3
            [PDFExcel] = 4
            [CSVExcel] = 5
            [All] = 6
            [PDFExcelWord] = 7
        End Enum
        Public Enum ExportType
            [PDF] = 0
            [Excel] = 1
            [CSV] = 2
            [Word] = 3
        End Enum
        Public Enum PagingStyle
            [Standard] = 0
            [OneofN] = 1
            [None] = 2
        End Enum
        Public Enum HeadingStyle
            [Standard] = 0
            [Banner] = 1
            [None] = 2
        End Enum
        Public Enum ReportDateStyle
            [No] = 0
            [Yes] = 1
        End Enum
        Public Enum LayoutStyle
            [FullLayout] = 0
            [OptionsOnly] = 1
        End Enum
        Private _ReportId As Guid
        Private _ResourceId As Integer
        Private _ReportName As String
        Private _FriendlyName As String
        Private _ShowPerformanceMsg As Boolean
        Private _ShowFilterMode As Boolean
        Private _ReportDescription As String
        Private _PagingOption As PagingStyle
        Private _HeadingOption As HeadingStyle
        Private _ReportDateOption As ReportDateStyle
        Private _RDLName As String
        Private _AssemblyFilePath As String
        Private _WebServiceURL As String
        Private _RecordLimit As Long
        Private _ReportClass As String
        Private _CreationMethod As String
        Private _AllowedExportFormat As AllowedExportType
        Private _ReportTabLayout As LayoutStyle
        Private _StartingTabIndex As Integer
        Private _ExportFormat As ExportType
        Private _ReportTabCollection As List(Of ReportTabInfo)
        Private _Filters As MasterDictionary
        Private _Groupings As MasterDictionary
        Private _Sorts As List(Of SortItemInfo)
        Private _CustomOptions As List(Of ParamItemUserSettingsInfo)
        Private _Environment As String

        Public Property ReportId() As Guid
            Get
                Return _ReportId
            End Get
            Set(ByVal Value As Guid)
                _ReportId = Value
            End Set
        End Property
        Public Property ResourceId() As Integer
            Get
                Return _ResourceId
            End Get
            Set(ByVal Value As Integer)
                _ResourceId = Value
            End Set
        End Property
        Public Property ReportName() As String
            Get
                Return _ReportName
            End Get
            Set(ByVal Value As String)
                _ReportName = Value
            End Set
        End Property
        Public Property FriendlyName() As String
            Get
                Return _FriendlyName
            End Get
            Set(ByVal Value As String)
                _FriendlyName = Value
            End Set
        End Property
        Public Property ShowPerformanceMsg() As Boolean
            Get
                Return _ShowPerformanceMsg
            End Get
            Set(ByVal Value As Boolean)
                _ShowPerformanceMsg = Value
            End Set
        End Property
        Public Property ShowFilterMode() As Boolean
            Get
                Return _ShowFilterMode
            End Get
            Set(ByVal Value As Boolean)
                _ShowFilterMode = Value
            End Set
        End Property
        Public Property PagingOption() As PagingStyle
            Get
                Return _PagingOption
            End Get
            Set(ByVal Value As PagingStyle)
                _PagingOption = Value
            End Set
        End Property
        Public Property HeadingOption() As HeadingStyle
            Get
                Return _HeadingOption
            End Get
            Set(ByVal Value As HeadingStyle)
                _HeadingOption = Value
            End Set
        End Property
        Public Property ReportDateOption() As ReportDateStyle
            Get
                Return _ReportDateOption
            End Get
            Set(ByVal Value As ReportDateStyle)
                _ReportDateOption = Value
            End Set
        End Property
        Public Property ReportDescription() As String
            Get
                Return _ReportDescription
            End Get
            Set(ByVal Value As String)
                _ReportDescription = Value
            End Set
        End Property
        Public Property RDLName() As String
            Get
                Return _RDLName
            End Get
            Set(ByVal Value As String)
                _RDLName = Value
            End Set
        End Property
        Public Property WebServiceUrl() As String
            Get
                Return _WebServiceURL
            End Get
            Set(ByVal Value As String)
                _WebServiceURL = Value
            End Set
        End Property
        Public Property RecordLimit() As Long
            Get
                Return _RecordLimit
            End Get
            Set(ByVal value As Long)
                _RecordLimit = value
            End Set
        End Property
        Public Property AssemblyFilePath() As String
            Get
                Return _AssemblyFilePath
            End Get
            Set(ByVal Value As String)
                _AssemblyFilePath = Value
            End Set
        End Property
        Public Property ReportClass() As String
            Get
                Return _ReportClass
            End Get
            Set(ByVal Value As String)
                _ReportClass = Value
            End Set
        End Property
        Public Property CreationMethod() As String
            Get
                Return _CreationMethod
            End Get
            Set(ByVal Value As String)
                _CreationMethod = Value
            End Set
        End Property
        Public Property AllowedExportFormat() As ReportInfo.AllowedExportType
            Get
                Return _AllowedExportFormat
            End Get
            Set(ByVal Value As ReportInfo.AllowedExportType)
                _AllowedExportFormat = Value
            End Set
        End Property
        Public Property ReportTabLayout() As ReportInfo.LayoutStyle
            Get
                Return _ReportTabLayout
            End Get
            Set(ByVal Value As ReportInfo.LayoutStyle)
                _ReportTabLayout = Value
            End Set
        End Property
        Public Property StartingTabIndex() As Integer
            Get
                Return _StartingTabIndex
            End Get
            Set(ByVal Value As Integer)
                _StartingTabIndex = Value
            End Set
        End Property
        Public Property ExportFormat() As ReportInfo.ExportType
            Get
                Return _ExportFormat
            End Get
            Set(ByVal Value As ReportInfo.ExportType)
                _ExportFormat = Value
            End Set
        End Property
        Public Property ReportTabCollection() As List(Of ReportTabInfo)
            Get
                Return _ReportTabCollection
            End Get
            Set(ByVal Value As List(Of ReportTabInfo))
                _ReportTabCollection = Value
            End Set
        End Property
        Public Property Filters() As MasterDictionary
            Get
                Return _Filters
            End Get
            Set(ByVal Value As MasterDictionary)
                _Filters = Value
            End Set
        End Property
        Public Property Groupings() As MasterDictionary
            Get
                Return _Groupings
            End Get
            Set(ByVal Value As MasterDictionary)
                _Groupings = Value
            End Set
        End Property
        Public Property Sorts() As List(Of SortItemInfo)
            Get
                Return _Sorts
            End Get
            Set(ByVal Value As List(Of SortItemInfo))
                _Sorts = Value
            End Set
        End Property
        Public Property CustomOptions() As List(Of ParamItemUserSettingsInfo)
            Get
                Return _CustomOptions
            End Get
            Set(ByVal Value As List(Of ParamItemUserSettingsInfo))
                _CustomOptions = Value
            End Set
        End Property
        Public Property Environment() As String
            Get
                Return _Environment
            End Get
            Set(ByVal Value As String)
                _Environment = Value
            End Set
        End Property
    End Class
End Namespace

