﻿Imports System
Imports System.Xml.Serialization
Imports FAME.Parameters.Info

Namespace Info

    <Serializable()>
    Public Class ReportUserSettingsInfo
        Public Enum PagingStyle
            [Standard] = 0
            [OneofN] = 1
            [None] = 2
        End Enum
        Public Enum HeadingStyle
            [Standard] = 0
            [Banner] = 1
            [None] = 2
        End Enum
        Public Enum ReportDateStyle
            [No] = 0
            [Yes] = 1
        End Enum
        Private _PrefId As Guid
        Private _UserId As Guid
        Private _PrefName As String
        Private _PrefDescription As String
        Private _ResourceId As Integer
        Private _ReportId As Guid
        'Private _ModDate As Date
        Private _PagingOption As PagingStyle
        Private _HeadingOption As HeadingStyle
        Private _ReportDateOption As ReportDateStyle
        Private _FilterSettings As List(Of ParamItemUserSettingsInfo)
        Private _SortSettings As List(Of SortItemInfo)
        Private _CustomOptions As List(Of ParamItemUserSettingsInfo)
        <XmlElement(ElementName:="PrefId")> _
        Public Property PrefId() As Guid
            Get
                Return _PrefId
            End Get
            Set(ByVal Value As Guid)
                _PrefId = Value
            End Set
        End Property
        <XmlElement(ElementName:="UserId")> _
        Public Property UserId() As Guid
            Get
                Return _UserId
            End Get
            Set(ByVal Value As Guid)
                _UserId = Value
            End Set
        End Property
        <XmlElement(ElementName:="PrefName")> _
        Public Property PrefName() As String
            Get
                Return _PrefName
            End Get
            Set(ByVal Value As String)
                _PrefName = Value
            End Set
        End Property
        <XmlElement(ElementName:="PrefDescription")> _
        Public Property PrefDescription() As String
            Get
                Return _PrefDescription
            End Get
            Set(ByVal Value As String)
                _PrefDescription = Value
            End Set
        End Property
        <XmlElement(ElementName:="ResourceId")> _
        Public Property ResourceId() As Integer
            Get
                Return _ResourceId
            End Get
            Set(ByVal Value As Integer)
                _ResourceId = Value
            End Set
        End Property
        <XmlElement(ElementName:="ReportId")> _
        Public Property ReportId() As Guid
            Get
                Return _ReportId
            End Get
            Set(ByVal Value As Guid)
                _ReportId = Value
            End Set
        End Property
        <XmlElement(ElementName:="PagingOption")> _
        Public Property PagingOption() As PagingStyle
            Get
                Return _PagingOption
            End Get
            Set(ByVal Value As PagingStyle)
                _PagingOption = Value
            End Set
        End Property
        <XmlElement(ElementName:="HeadingOption")> _
        Public Property HeadingOption() As HeadingStyle
            Get
                Return _HeadingOption
            End Get
            Set(ByVal Value As HeadingStyle)
                _HeadingOption = Value
            End Set
        End Property
        <XmlElement(ElementName:="ReportDateOption")> _
        Public Property ReportDateOption() As ReportDateStyle
            Get
                Return _ReportDateOption
            End Get
            Set(ByVal Value As ReportDateStyle)
                _ReportDateOption = Value
            End Set
        End Property
        '<XmlElement(ElementName:="ModDate")> _
        'Public Property ModDate() As Date
        '    Get
        '        Return _ModDate
        '    End Get
        '    Set(ByVal Value As Date)
        '        _ModDate = Value
        '    End Set
        'End Property
        <XmlArray(ElementName:="FilterSettings")> _
        Public Property FilterSettings() As List(Of ParamItemUserSettingsInfo)
            Get
                Return _FilterSettings
            End Get
            Set(ByVal Value As List(Of ParamItemUserSettingsInfo))
                _FilterSettings = Value
            End Set
        End Property
        <XmlArray(ElementName:="SortSettings")> _
        Public Property SortSettings() As List(Of SortItemInfo)
            Get
                Return _SortSettings
            End Get
            Set(ByVal Value As List(Of SortItemInfo))
                _SortSettings = Value
            End Set
        End Property
        <XmlArray(ElementName:="CustomOptions")> _
        Public Property CustomOptions() As List(Of ParamItemUserSettingsInfo)
            Get
                Return _CustomOptions
            End Get
            Set(ByVal Value As List(Of ParamItemUserSettingsInfo))
                _CustomOptions = Value
            End Set
        End Property
    End Class
End Namespace
