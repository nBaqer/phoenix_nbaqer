﻿Imports FAME.Advantage.Reporting.Info
Imports FAME.Parameters.Info
Imports FAME.Advantage.Reporting
Namespace Logic
    Public Class ConsecutiveAbsencesReport
        Dim TermId As String = ""
        Dim CourseId As String = ""
        Dim NumberofDaysMissed As Integer = 0
        Dim CutOffDate As DateTime
        Dim PrgVerId As String = ""
        Dim CampusId As String = ""
        Dim ShowCalendarDays As Boolean = False
        Public Function BuildReport(ByVal Rpt As ReportInfo, ByVal conn As String, ByVal UserId As String,
                                                                ByVal strGradingMethod As String,
                                                                ByVal strGPAMethod As String,
                                                                ByVal strSchoolName As String,
                                                                ByVal strTrackAttendanceBy As String,
                                                                ByVal strGradeBookWeightingLevel As String,
                                                                ByVal strStudentIdentifier As String,
                                                                ByVal DisplayAttendanceUnitForProgressReportByClass As String,
                                                                ByVal strReportPath As String) As [Byte]()
            Dim TestText As String = String.Empty
            Try
                Dim objReportInfo As ReportInfo = Rpt
                Dim getReportOutput As [Byte]()
                For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                    Select Case tabview.ParamSet.SetType
                        Case "Filter"
                        Case "Sort"
                        Case "Custom"
                            For Each item As ParamItemUserSettingsInfo In Rpt.CustomOptions
                                'Response.Write(item.ControlSettingsCollection.Item)
                                For Each drillItem As ControlSettingInfo In item.ControlSettingsCollection
                                    For Each controlValueItem As ControlValueInfo In drillItem.ControlValueCollection
                                        Dim strKeyName As String = ""
                                        Dim strKeyValue As String = ""
                                        Dim strDisplayText As String = ""
                                        Select Case drillItem.ControlName.Trim.ToString.ToLower
                                            Case Is = "radlistboxterms2"
                                                strKeyName = "termid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radlistboxprogramversions2"
                                                strKeyName = "prgverid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radlistboxcourse2"
                                                strKeyName = "courseid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "raddatereportdate"
                                                strKeyName = "cutoffdate"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "txtnumberofdays"
                                                strKeyName = "numberofdays"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "radlistboxcampus2"
                                                strKeyName = "campusid"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)
                                            Case Is = "rblshowcalendardays"
                                                strKeyName = "rblshowcalendardays"
                                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                                BuildParamtersForReport(strKeyName, strKeyValue, strDisplayText)

                                        End Select
                                    Next
                                Next
                            Next
                        Case Else
                            Throw New Exception("Unknown Type")
                    End Select
                Next
                Dim getSelectedValues As New DailyConsecutiveAbsenceReportSelectionValues
                With getSelectedValues
                    .TermId = TermId.ToString
                    .ReqId = CourseId.ToString
                    .DaysMissed = NumberofDaysMissed
                    .CutOffDate = CutOffDate
                    .ReportPath = objReportInfo.RDLName
                    .Environment = strReportPath
                    .PrgVerId = PrgVerId
                    .CampusId = CampusId.ToString
                    .ShowCalendarDays = ShowCalendarDays
                End With
                'intCheckDataExists = GetProgressDA.GetProgressReportRecordCount(strStuEnrollId, strCampGrpId, strPrgVerId, strStatusCodeId, strTermId, strStudentGrpId, dtStartString, UserId.ToString, strStartStringModifier)
                'If intCheckDataExists >= 1 Then
                Dim strFormat As String = ""
                Select Case Rpt.ExportFormat
                    Case ReportInfo.ExportType.PDF
                        strFormat = "Pdf"
                    Case ReportInfo.ExportType.Excel
                        strFormat = "Excel"
                    Case ReportInfo.ExportType.CSV
                        strFormat = "csv"
                    Case ReportInfo.ExportType.Word
                        strFormat = "word"
                End Select
                getReportOutput = CallRenderReport(strFormat, getSelectedValues, strSchoolName)
                Return getReportOutput
                'Else
                '    Return Nothing
                'End If
                'Return Nothing
            Catch ex As Exception
                Throw ex
            End Try
            Return Nothing
        End Function
        Private Sub BuildParamtersForReport(ByVal strFilterFieldName As String,
                                            ByVal strFilterSelectedValue As String,
                                            ByVal strDisplayText As String)
            Select Case strFilterFieldName.ToLower.Trim
                Case "termid"
                    TermId &= strFilterSelectedValue.ToString & ","
                Case "campusid"
                    CampusId &= strFilterSelectedValue.ToString & ","
                Case "prgverid"
                    PrgVerId &= strFilterSelectedValue.ToString & ","
                Case "courseid"
                    CourseId &= strFilterSelectedValue.ToString & ","
                Case "numberofdays"
                    NumberofDaysMissed = Convert.ToInt32(strFilterSelectedValue)
                Case "cutoffdate"
                    CutOffDate = CDate(strFilterSelectedValue)
                Case "rblshowcalendardays"
                    ShowCalendarDays = Convert.ToBoolean(strFilterSelectedValue)

            End Select
        End Sub
        Public Function CallRenderReport(ByVal format As String, ByVal getFilterValues As DailyConsecutiveAbsenceReportSelectionValues, ByVal strSchoolName As String) As [Byte]()
            Dim getReportOutput As [Byte]()
            Dim generateReport As New cDailyAbsenceReportGenerator
            getReportOutput = generateReport.RenderReport(format, getFilterValues, strSchoolName)
            Return getReportOutput
        End Function
    End Class
End Namespace

