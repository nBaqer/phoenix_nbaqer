Imports FAME.Advantage.Reporting.ReportService2005
Imports FAME.Advantage.Reporting.ReportExecution2005
Imports System.Web
Imports FAME.Advantage.Reporting.Info
Imports FAME.Advantage.Common
Namespace Logic
    Public Class ReportGenerator
#Region "Report Variables"
        Private _rptService As New ReportingService2005
        Private _rptExecutionService As New ReportExecution2005.ReportExecutionService

        Private _strReportName As String = Nothing
        Private _strHistoryId As String = Nothing
        Private _boolForRendering As Boolean = False
        Private _credentials As ReportService2005.DataSourceCredentials() = Nothing
        Private _rptParameterValues As ReportService2005.ParameterValue() = Nothing
        Private _rptParameters As ReportService2005.ReportParameter() = Nothing
        Private _rptExecutionServiceParameterValues As ReportExecution2005.ParameterValue() = Nothing
        Private _rptExecutionInfoObj As New ExecutionInfo

        'Variables needed to render report
        Private _historyId As String = Nothing
        Private _deviceInfo As String = Nothing
        Private _format As String = Nothing
        Private _generateReportAsBytes As [Byte]()
        Private _encoding As String = [String].Empty
        Private Shared _strExtension As String = String.Empty
        Private Shared _strMimeType As String = String.Empty
        Private _warnings As ReportExecution2005.Warning() = Nothing
        Private _streamIDs As String() = Nothing

        Dim strServerName As String = ""
        Dim strDBName As String = ""
        Dim strUserName As String = ""
        Dim strPassword As String = ""
#End Region

#Region "Initialize Web Services"
        Private Sub InitializeWebServices()


            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString).ToLower

            'Set the credentials and url for the report services
            'The report service object is needed to get the parameter details associated with a specific report

            'Dim dsCredentials As ReportExecution2005.DataSourceCredentials = New ReportExecution2005.DataSourceCredentials()

            'With dsCredentials
            '    .DataSourceName = "AdvantageDataSource"
            '    .UserName = GetUserName(strConnectionString)
            '    .Password = GetPassword(strConnectionString)
            'End With

            _rptService.Credentials = System.Net.CredentialCache.DefaultCredentials
            _rptService.Url = MyAdvAppSettings.AppSettings("ReportServices").ToString
            'Set the credentials and url for the report execution services
            _rptExecutionService.Credentials = System.Net.CredentialCache.DefaultCredentials
            '_rptExecutionService.SetExecutionCredentials(dsCredentials)
            _rptExecutionService.Url = MyAdvAppSettings.AppSettings("ReportExecutionServices").ToString
        End Sub
#End Region

#Region "Initialize Export Formats"
        Private Shared Sub ConfigureExportFormat(ByRef strType As String)
            Select Case strType.ToUpper()
                Case "PDF"
                    _strExtension = "pdf"
                    _strMimeType = "application/pdf"
                    Exit Select
                Case "EXCEL", "XLS"
                    _strExtension = "xls"
                    _strMimeType = "application/vnd.excel"
                    Exit Select
                Case "WORD"
                    _strExtension = "doc"
                    _strMimeType = "application/vnd.ms-word"
                Case "CSV"
                    _strExtension = "csv"
                    _strMimeType = "text/csv"
                Case Else
                    Throw New Exception("Unrecognized type: " & strType & ". Type must be PDF, Excel or Image, HTML.")
            End Select
        End Sub
#End Region

#Region "Build Report Parameters"
        Private Function BuildReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(),
                                                                       ByVal getReportParametersObj As ReportSelectionValues,
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}
            Dim strNullValue As String = "Null"
            Dim strCampGrpId As String = ""
            Dim strPrgVerId As String = ""
            Dim strTermId As String = ""
            Dim strStatusCodeId As String = ""
            Dim strStuEnrollId As String = ""
            Dim strStudentGrpId As String = ""
            Dim dtTermDate As Date = Nothing
            Dim strGradesFormat As String = ""
            Dim strGPAMethod As String = ""
            Dim ShowWorkUnitGrouping As Boolean = False
            Dim SysComponentTypeId As String = ""
            Dim strOrderBy As String = ""
            Dim ShowFinanceCalculations As Boolean = False
            Dim ShowWeeklySchedule As Boolean = False
            Dim GradeRounding As Boolean = False
            Dim ShowStudentSignatureLIne As Boolean = False
            Dim ShowSchoolSignatureLine As Boolean = False
            Dim ShowPageNumber As Boolean = False
            Dim ShowHeading As Boolean = False
            Dim StartDateModifier As String = ""
            Dim StartDate As String = ""
            Dim ExpectedGradDateModifier As String = ""
            Dim ExpectedGradDate As String = ""
            Dim UserId As String = ""
            Dim strTrackAttendanceBy As String = ""
            Dim strGradeBookLevel As String = ""
            Dim strDisplayAttendanceUnitForProgressReportByClass As String = ""
            Dim boolShowTermModule As Boolean = False
            Dim boolShowDateInFooter As Boolean = False
            Dim strTerm_StartDate As String = ""
            Dim strStudentIdentifier As String = "ssn"
            Dim strServerName As String = ""
            Dim strDBName As String = ""
            Dim strUserName As String = ""
            Dim strPassword As String = ""
            Dim boolShowAllEnrollments As Boolean = False

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString).ToLower

            Dim strCampusId As String = ""

            '  GetConnectionStringValues()

            With getReportParametersObj
                strStuEnrollId = .StuEnrollId
                strCampGrpId = .CampGrpId
                strPrgVerId = .PrgVerId
                strStatusCodeId = .StatusCodeId
                strStuEnrollId = .StuEnrollId
                strTermId = .TermId
                strStudentGrpId = .StudentGrpId
                SysComponentTypeId = .GrdComponentTypeId
                strOrderBy = .SortFields
                ShowFinanceCalculations = .ShowFinanceCalculations
                ShowWeeklySchedule = .ShowWeeklySchedule
                GradeRounding = False
                ShowSchoolSignatureLine = .ShowSchoolSignatureLine
                ShowStudentSignatureLIne = .ShowStudentSignatureLine
                ShowPageNumber = .ShowPageNumber
                ShowHeading = .ShowHeading
                StartDateModifier = .StartDateModifier
                StartDate = .StartDate
                ExpectedGradDateModifier = .ExpectedGradDateModifier
                ExpectedGradDate = .ExpectedGradDate
                UserId = .UserId
                strGradesFormat = .GradingMethod
                strGPAMethod = .GPAMethod
                strTrackAttendanceBy = .TrackAttendanceBy
                ShowWorkUnitGrouping = .GroupByWorkUnits
                strGradeBookLevel = .SetGradeBookAt
                strDisplayAttendanceUnitForProgressReportByClass = .DisplayAttendanceUnitForProgressReportByClass
                boolShowTermModule = .ShowTermInReport
                boolShowDateInFooter = .ShowDateInFooter
                strTerm_StartDate = .Term_StartDate
                strStudentIdentifier = .StudentIdentifier
                strServerName = GetServerName(strConnectionString)
                strDBName = GetDatabaseName(strConnectionString)
                strUserName = GetUserName(strConnectionString)
                strPassword = GetPassword(strConnectionString)
                strCampusId = .CampusId
                boolShowAllEnrollments = .ShowAllEnrollments
            End With

            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                'user name
                'Server Name
                rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(0).Label = "servername"
                rptExecutionParamValues(0).Name = "servername"
                rptExecutionParamValues(0).Value = strServerName

                'Database Name
                rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(1).Label = "databasename"
                rptExecutionParamValues(1).Name = "databasename"
                rptExecutionParamValues(1).Value = strDBName


                rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(2).Label = "uid"
                rptExecutionParamValues(2).Name = "uid"
                rptExecutionParamValues(2).Value = strUserName

                'password
                rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(3).Label = "password"
                rptExecutionParamValues(3).Name = "password"
                rptExecutionParamValues(3).Value = strPassword

                'StuEnrollId
                rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(4).Label = "StuEnrollId"
                rptExecutionParamValues(4).Name = "StuEnrollId"
                If strStuEnrollId.Trim = "" Then
                    rptExecutionParamValues(4).Value = Nothing
                Else
                    rptExecutionParamValues(4).Value = strStuEnrollId
                End If

                'CampGrpId Parameter

                rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(5).Label = "CampGrpId"
                rptExecutionParamValues(5).Name = "CampGrpId"
                ' If the value is null, do not pass any value
                'the SSRS will assign a default value of Nothing
                If Not strCampGrpId.Trim = "" Then
                    rptExecutionParamValues(5).Value = strCampGrpId.ToString
                Else
                    rptExecutionParamValues(5).Value = Nothing
                End If

                rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(6).Label = "PrgVerId"
                rptExecutionParamValues(6).Name = "PrgVerId"
                If Not strPrgVerId.Trim = "" Then
                    rptExecutionParamValues(6).Value = strPrgVerId.ToString
                Else
                    rptExecutionParamValues(6).Value = Nothing
                End If

                rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(7).Label = "StatusCodeId"
                rptExecutionParamValues(7).Name = "StatusCodeId"
                If Not strStatusCodeId.Trim = "" Then
                    rptExecutionParamValues(7).Value = strStatusCodeId.ToString
                Else
                    rptExecutionParamValues(7).Value = Nothing
                End If

                rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(8).Label = "TermId"
                rptExecutionParamValues(8).Name = "TermId"
                If Not strTermId.Trim = "" Then
                    rptExecutionParamValues(8).Value = strTermId.ToString
                Else
                    rptExecutionParamValues(8).Value = Nothing
                End If

                rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(9).Label = "StudentGrpId"
                rptExecutionParamValues(9).Name = "StudentGrpId"
                If Not strStudentGrpId.Trim = "" Then
                    rptExecutionParamValues(9).Value = strStudentGrpId.ToString
                Else
                    rptExecutionParamValues(9).Value = Nothing
                End If

                rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(10).Label = "GradesFormat"
                rptExecutionParamValues(10).Name = "GradesFormat"
                If Not strGradesFormat.Trim = "" Then
                    rptExecutionParamValues(10).Value = strGradesFormat.Trim
                Else
                    rptExecutionParamValues(10).Value = Nothing
                End If

                rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(11).Label = "GPAMethod"
                rptExecutionParamValues(11).Name = "GPAMethod"
                If Not strGPAMethod.Trim = "" Then
                    rptExecutionParamValues(11).Value = strGPAMethod.Trim
                Else
                    rptExecutionParamValues(11).Value = Nothing
                End If

                rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(12).Label = "ShowWorkUnitGrouping"
                rptExecutionParamValues(12).Name = "ShowWorkUnitGrouping"
                If ShowWorkUnitGrouping = False Then
                    rptExecutionParamValues(12).Value = "false"
                Else
                    rptExecutionParamValues(12).Value = "true"
                End If

                rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(13).Label = "SysComponentTypeId"
                rptExecutionParamValues(13).Name = "SysComponentTypeId"
                If SysComponentTypeId.Trim = "" Then
                    rptExecutionParamValues(13).Value = Nothing
                Else
                    rptExecutionParamValues(13).Value = SysComponentTypeId
                End If

                rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(14).Label = "OrderBy"
                rptExecutionParamValues(14).Name = "OrderBy"
                If strOrderBy.Trim = "" Then
                    rptExecutionParamValues(14).Value = Nothing
                Else
                    rptExecutionParamValues(14).Value = strOrderBy
                End If

                rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(15).Label = "ShowFinanceCalculations"
                rptExecutionParamValues(15).Name = "ShowFinanceCalculations"
                If ShowFinanceCalculations = False Then
                    rptExecutionParamValues(15).Value = "false"
                Else
                    rptExecutionParamValues(15).Value = "true"
                End If

                rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(16).Label = "ShowWeeklySchedule"
                rptExecutionParamValues(16).Name = "ShowWeeklySchedule"
                If ShowWeeklySchedule = False Then
                    rptExecutionParamValues(16).Value = "false"
                Else
                    rptExecutionParamValues(16).Value = "true"
                End If

                rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(17).Label = "GradeRounding"
                rptExecutionParamValues(17).Name = "GradeRounding"
                If GradeRounding = False Then
                    rptExecutionParamValues(17).Value = "false"
                Else
                    rptExecutionParamValues(17).Value = "true"
                End If

                rptExecutionParamValues(18) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(18).Label = "ShowStudentSignatureLine"
                rptExecutionParamValues(18).Name = "ShowStudentSignatureLine"
                If ShowStudentSignatureLIne = False Then
                    rptExecutionParamValues(18).Value = "false"
                Else
                    rptExecutionParamValues(18).Value = "true"
                End If

                rptExecutionParamValues(19) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(19).Label = "ShowSchoolSignatureLine"
                rptExecutionParamValues(19).Name = "ShowSchoolSignatureLine"
                If ShowSchoolSignatureLine = False Then
                    rptExecutionParamValues(19).Value = "false"
                Else
                    rptExecutionParamValues(19).Value = "true"
                End If

                rptExecutionParamValues(20) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(20).Label = "ShowPageNumber"
                rptExecutionParamValues(20).Name = "ShowPageNumber"
                If ShowPageNumber = False Then
                    rptExecutionParamValues(20).Value = "false"
                Else
                    rptExecutionParamValues(20).Value = "true"
                End If

                rptExecutionParamValues(21) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(21).Label = "ShowHeading"
                rptExecutionParamValues(21).Name = "ShowHeading"
                If ShowHeading = False Then
                    rptExecutionParamValues(21).Value = "false"
                Else
                    rptExecutionParamValues(21).Value = "true"
                End If


                rptExecutionParamValues(22) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(22).Label = "StartDateModifier"
                rptExecutionParamValues(22).Name = "StartDateModifier"
                If StartDateModifier = "" Then
                    rptExecutionParamValues(22).Value = Nothing
                Else
                    rptExecutionParamValues(22).Value = StartDateModifier
                End If


                rptExecutionParamValues(23) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(23).Label = "StartDate"
                rptExecutionParamValues(23).Name = "StartDate"
                If StartDate = "" Then
                    rptExecutionParamValues(23).Value = Nothing
                Else
                    If Not strTermId.Trim = "" Then
                        rptExecutionParamValues(23).Value = strTerm_StartDate
                    Else
                        rptExecutionParamValues(23).Value = StartDate
                    End If
                End If


                rptExecutionParamValues(24) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(24).Label = "ExpectedGradDateModifier"
                rptExecutionParamValues(24).Name = "ExpectedGradDateModifier"
                If ExpectedGradDateModifier = "" Then
                    rptExecutionParamValues(24).Value = Nothing
                Else
                    rptExecutionParamValues(24).Value = ExpectedGradDateModifier
                End If

                rptExecutionParamValues(25) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(25).Label = "ExpectedGradDate"
                rptExecutionParamValues(25).Name = "ExpectedGradDate"
                If ExpectedGradDate = "" Then
                    rptExecutionParamValues(25).Value = Nothing
                Else
                    rptExecutionParamValues(25).Value = ExpectedGradDate
                End If

                rptExecutionParamValues(26) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(26).Label = "UserId"
                rptExecutionParamValues(26).Name = "UserId"
                If UserId = "" Then
                    rptExecutionParamValues(26).Value = Nothing
                Else
                    rptExecutionParamValues(26).Value = UserId.ToString.Trim
                End If

                rptExecutionParamValues(27) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(27).Label = "SchoolName"
                rptExecutionParamValues(27).Name = "SchoolName"
                rptExecutionParamValues(27).Value = strSchoolName.ToString.Trim

                rptExecutionParamValues(28) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(28).Label = "TrackAttendanceBy"
                rptExecutionParamValues(28).Name = "TrackAttendanceBy"
                rptExecutionParamValues(28).Value = strTrackAttendanceBy.ToString.Trim

                rptExecutionParamValues(29) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(29).Label = "SetGradeBookAt"
                rptExecutionParamValues(29).Name = "SetGradeBookAt"
                rptExecutionParamValues(29).Value = strGradeBookLevel.ToString.ToLower.Trim

                rptExecutionParamValues(30) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(30).Label = "DisplayHours"
                rptExecutionParamValues(30).Name = "DisplayHours"
                rptExecutionParamValues(30).Value = strDisplayAttendanceUnitForProgressReportByClass.ToString.ToLower.Trim

                rptExecutionParamValues(31) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(31).Label = "ShowTermModule"
                rptExecutionParamValues(31).Name = "ShowTermModule"
                If boolShowTermModule = True Then
                    rptExecutionParamValues(31).Value = "true"
                Else
                    rptExecutionParamValues(31).Value = "false"
                End If

                rptExecutionParamValues(32) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(32).Label = "ShowDateInFooter"
                rptExecutionParamValues(32).Name = "ShowDateInFooter"
                If boolShowDateInFooter = True Then
                    rptExecutionParamValues(32).Value = "true"
                Else
                    rptExecutionParamValues(32).Value = "false"
                End If

                rptExecutionParamValues(33) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(33).Label = "StudentIdentifier"
                rptExecutionParamValues(33).Name = "StudentIdentifier"
                rptExecutionParamValues(33).Value = strStudentIdentifier

                rptExecutionParamValues(34) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(34).Label = "CampusId"
                rptExecutionParamValues(34).Name = "CampusId"
                If Not strCampusId.Trim = "" Then
                    rptExecutionParamValues(34).Value = strCampusId.ToString
                Else
                    rptExecutionParamValues(34).Value = Nothing
                End If

                rptExecutionParamValues(35) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(35).Label = "ShowAllEnrollments"
                rptExecutionParamValues(35).Name = "ShowAllEnrollments"
                If boolShowAllEnrollments = True Then
                    rptExecutionParamValues(35).Value = "true"
                Else
                    rptExecutionParamValues(35).Value = "false"
                End If
            End If
            Return rptExecutionParamValues
        End Function
        'Private Function BuildReportParameters_Old(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
        '                                                               ByVal getReportParametersObj As ReportSelectionValues, _
        '                                                               ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()
        '    Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}
        '    Dim strNullValue As String = "Null"
        '    Dim strCampGrpId As String = ""
        '    Dim strPrgVerId As String = ""
        '    Dim strTermId As String = ""
        '    Dim strStatusCodeId As String = ""
        '    Dim strStuEnrollId As String = ""
        '    Dim strStudentGrpId As String = ""
        '    Dim dtTermDate As Date = Nothing
        '    Dim strGradesFormat As String = ""
        '    Dim strGPAMethod As String = ""
        '    Dim ShowWorkUnitGrouping As Boolean = False
        '    Dim SysComponentTypeId As String = ""
        '    Dim strOrderBy As String = ""
        '    Dim ShowFinanceCalculations As Boolean = False
        '    Dim ShowWeeklySchedule As Boolean = False
        '    Dim GradeRounding As Boolean = False
        '    Dim ShowStudentSignatureLIne As Boolean = False
        '    Dim ShowSchoolSignatureLine As Boolean = False
        '    Dim ShowPageNumber As Boolean = False
        '    Dim ShowHeading As Boolean = False
        '    Dim StartDateModifier As String = ""
        '    Dim StartDate As String = ""
        '    Dim ExpectedGradDateModifier As String = ""
        '    Dim ExpectedGradDate As String = ""
        '    Dim UserId As String = ""
        '    Dim strTrackAttendanceBy As String = ""
        '    Dim strGradeBookLevel As String = ""
        '    Dim strDisplayAttendanceUnitForProgressReportByClass As String = ""
        '    Dim boolShowTermModule As Boolean = False
        '    Dim boolShowDateInFooter As Boolean = False
        '    Dim strTerm_StartDate As String = ""
        '    Dim strStudentIdentifier As String = "ssn"


        '    With getReportParametersObj
        '        strStuEnrollId = .StuEnrollId
        '        strCampGrpId = .CampGrpId
        '        strPrgVerId = .PrgVerId
        '        strStatusCodeId = .StatusCodeId
        '        strStuEnrollId = .StuEnrollId
        '        strTermId = .TermId
        '        strStudentGrpId = .StudentGrpId
        '        SysComponentTypeId = .GrdComponentTypeId
        '        strOrderBy = .SortFields
        '        ShowFinanceCalculations = .ShowFinanceCalculations
        '        ShowWeeklySchedule = .ShowWeeklySchedule
        '        GradeRounding = False
        '        ShowSchoolSignatureLine = .ShowSchoolSignatureLine
        '        ShowStudentSignatureLIne = .ShowStudentSignatureLine
        '        ShowPageNumber = .ShowPageNumber
        '        ShowHeading = .ShowHeading
        '        StartDateModifier = .StartDateModifier
        '        StartDate = .StartDate
        '        ExpectedGradDateModifier = .ExpectedGradDateModifier
        '        ExpectedGradDate = .ExpectedGradDate
        '        UserId = .UserId
        '        strGradesFormat = .GradingMethod
        '        strGPAMethod = .GPAMethod
        '        strTrackAttendanceBy = .TrackAttendanceBy
        '        ShowWorkUnitGrouping = .GroupByWorkUnits
        '        strGradeBookLevel = .SetGradeBookAt
        '        strDisplayAttendanceUnitForProgressReportByClass = .DisplayAttendanceUnitForProgressReportByClass
        '        boolShowTermModule = .ShowTermInReport
        '        boolShowDateInFooter = .ShowDateInFooter
        '        strTerm_StartDate = .Term_StartDate
        '        strStudentIdentifier = .StudentIdentifier
        '    End With

        '    'Pass the parameter values
        '    If rptServicesParameters.Length > 0 Then

        '        'StuEnrollId
        '        rptExecutionParamValues(0) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(0).Label = "StuEnrollId"
        '        rptExecutionParamValues(0).Name = "StuEnrollId"
        '        If strStuEnrollId.Trim = "" Then
        '            rptExecutionParamValues(0).Value = Nothing
        '        Else
        '            rptExecutionParamValues(0).Value = strStuEnrollId
        '        End If

        '        'CampGrpId Parameter

        '        rptExecutionParamValues(1) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(1).Label = "CampGrpId"
        '        rptExecutionParamValues(1).Name = "CampGrpId"
        '        ' If the value is null, do not pass any value
        '        'the SSRS will assign a default value of Nothing
        '        If Not strCampGrpId.Trim = "" Then
        '            rptExecutionParamValues(1).Value = strCampGrpId.ToString
        '        Else
        '            rptExecutionParamValues(1).Value = Nothing
        '        End If

        '        rptExecutionParamValues(2) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(2).Label = "PrgVerId"
        '        rptExecutionParamValues(2).Name = "PrgVerId"
        '        If Not strPrgVerId.Trim = "" Then
        '            rptExecutionParamValues(2).Value = strPrgVerId.ToString
        '        Else
        '            rptExecutionParamValues(2).Value = Nothing
        '        End If

        '        rptExecutionParamValues(3) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(3).Label = "StatusCodeId"
        '        rptExecutionParamValues(3).Name = "StatusCodeId"
        '        If Not strStatusCodeId.Trim = "" Then
        '            rptExecutionParamValues(3).Value = strStatusCodeId.ToString
        '        Else
        '            rptExecutionParamValues(3).Value = Nothing
        '        End If

        '        rptExecutionParamValues(4) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(4).Label = "TermId"
        '        rptExecutionParamValues(4).Name = "TermId"
        '        If Not strTermId.Trim = "" Then
        '            rptExecutionParamValues(4).Value = strTermId.ToString
        '        Else
        '            rptExecutionParamValues(4).Value = Nothing
        '        End If

        '        rptExecutionParamValues(5) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(5).Label = "StudentGrpId"
        '        rptExecutionParamValues(5).Name = "StudentGrpId"
        '        If Not strStudentGrpId.Trim = "" Then
        '            rptExecutionParamValues(5).Value = strStudentGrpId.ToString
        '        Else
        '            rptExecutionParamValues(5).Value = Nothing
        '        End If

        '        rptExecutionParamValues(6) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(6).Label = "GradesFormat"
        '        rptExecutionParamValues(6).Name = "GradesFormat"
        '        If Not strGradesFormat.Trim = "" Then
        '            rptExecutionParamValues(6).Value = strGradesFormat.Trim
        '        Else
        '            rptExecutionParamValues(6).Value = Nothing
        '        End If

        '        rptExecutionParamValues(7) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(7).Label = "GPAMethod"
        '        rptExecutionParamValues(7).Name = "GPAMethod"
        '        If Not strGPAMethod.Trim = "" Then
        '            rptExecutionParamValues(7).Value = strGPAMethod.Trim
        '        Else
        '            rptExecutionParamValues(7).Value = Nothing
        '        End If

        '        rptExecutionParamValues(8) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(8).Label = "ShowWorkUnitGrouping"
        '        rptExecutionParamValues(8).Name = "ShowWorkUnitGrouping"
        '        If ShowWorkUnitGrouping = False Then
        '            rptExecutionParamValues(8).Value = "false"
        '        Else
        '            rptExecutionParamValues(8).Value = "true"
        '        End If

        '        rptExecutionParamValues(9) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(9).Label = "SysComponentTypeId"
        '        rptExecutionParamValues(9).Name = "SysComponentTypeId"
        '        If SysComponentTypeId.Trim = "" Then
        '            rptExecutionParamValues(9).Value = Nothing
        '        Else
        '            rptExecutionParamValues(9).Value = SysComponentTypeId
        '        End If

        '        rptExecutionParamValues(10) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(10).Label = "OrderBy"
        '        rptExecutionParamValues(10).Name = "OrderBy"
        '        If strOrderBy.Trim = "" Then
        '            rptExecutionParamValues(10).Value = Nothing
        '        Else
        '            rptExecutionParamValues(10).Value = strOrderBy
        '        End If

        '        rptExecutionParamValues(11) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(11).Label = "ShowFinanceCalculations"
        '        rptExecutionParamValues(11).Name = "ShowFinanceCalculations"
        '        If ShowFinanceCalculations = False Then
        '            rptExecutionParamValues(11).Value = "false"
        '        Else
        '            rptExecutionParamValues(11).Value = "true"
        '        End If

        '        rptExecutionParamValues(12) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(12).Label = "ShowWeeklySchedule"
        '        rptExecutionParamValues(12).Name = "ShowWeeklySchedule"
        '        If ShowWeeklySchedule = False Then
        '            rptExecutionParamValues(12).Value = "false"
        '        Else
        '            rptExecutionParamValues(12).Value = "true"
        '        End If

        '        rptExecutionParamValues(13) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(13).Label = "GradeRounding"
        '        rptExecutionParamValues(13).Name = "GradeRounding"
        '        If GradeRounding = False Then
        '            rptExecutionParamValues(13).Value = "false"
        '        Else
        '            rptExecutionParamValues(13).Value = "true"
        '        End If

        '        rptExecutionParamValues(14) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(14).Label = "ShowStudentSignatureLine"
        '        rptExecutionParamValues(14).Name = "ShowStudentSignatureLine"
        '        If ShowStudentSignatureLIne = False Then
        '            rptExecutionParamValues(14).Value = "false"
        '        Else
        '            rptExecutionParamValues(14).Value = "true"
        '        End If

        '        rptExecutionParamValues(15) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(15).Label = "ShowSchoolSignatureLine"
        '        rptExecutionParamValues(15).Name = "ShowSchoolSignatureLine"
        '        If ShowSchoolSignatureLine = False Then
        '            rptExecutionParamValues(15).Value = "false"
        '        Else
        '            rptExecutionParamValues(15).Value = "true"
        '        End If

        '        rptExecutionParamValues(16) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(16).Label = "ShowPageNumber"
        '        rptExecutionParamValues(16).Name = "ShowPageNumber"
        '        If ShowPageNumber = False Then
        '            rptExecutionParamValues(16).Value = "false"
        '        Else
        '            rptExecutionParamValues(16).Value = "true"
        '        End If

        '        rptExecutionParamValues(17) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(17).Label = "ShowHeading"
        '        rptExecutionParamValues(17).Name = "ShowHeading"
        '        If ShowHeading = False Then
        '            rptExecutionParamValues(17).Value = "false"
        '        Else
        '            rptExecutionParamValues(17).Value = "true"
        '        End If

        '        rptExecutionParamValues(18) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(18).Label = "StartDateModifier"
        '        rptExecutionParamValues(18).Name = "StartDateModifier"
        '        If StartDateModifier = "" Then
        '            rptExecutionParamValues(18).Value = Nothing
        '        Else
        '            rptExecutionParamValues(18).Value = StartDateModifier
        '        End If

        '        rptExecutionParamValues(19) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(19).Label = "StartDate"
        '        rptExecutionParamValues(19).Name = "StartDate"
        '        If StartDate = "" Then
        '            rptExecutionParamValues(19).Value = Nothing
        '        Else
        '            If Not strTermId.Trim = "" Then
        '                rptExecutionParamValues(19).Value = strTerm_StartDate
        '            Else
        '                rptExecutionParamValues(19).Value = StartDate
        '            End If
        '        End If

        '        rptExecutionParamValues(20) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(20).Label = "ExpectedGradDateModifier"
        '        rptExecutionParamValues(20).Name = "ExpectedGradDateModifier"
        '        If ExpectedGradDateModifier = "" Then
        '            rptExecutionParamValues(20).Value = Nothing
        '        Else
        '            rptExecutionParamValues(20).Value = ExpectedGradDateModifier
        '        End If

        '        rptExecutionParamValues(21) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(21).Label = "ExpectedGradDate"
        '        rptExecutionParamValues(21).Name = "ExpectedGradDate"
        '        If ExpectedGradDate = "" Then
        '            rptExecutionParamValues(21).Value = Nothing
        '        Else
        '            rptExecutionParamValues(21).Value = ExpectedGradDate
        '        End If

        '        rptExecutionParamValues(22) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(22).Label = "UserId"
        '        rptExecutionParamValues(22).Name = "UserId"
        '        If UserId = "" Then
        '            rptExecutionParamValues(22).Value = Nothing
        '        Else
        '            rptExecutionParamValues(22).Value = UserId.ToString.Trim
        '        End If

        '        rptExecutionParamValues(23) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(23).Label = "SchoolName"
        '        rptExecutionParamValues(23).Name = "SchoolName"
        '        rptExecutionParamValues(23).Value = strSchoolName.ToString.Trim

        '        rptExecutionParamValues(24) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(24).Label = "TrackAttendanceBy"
        '        rptExecutionParamValues(24).Name = "TrackAttendanceBy"
        '        rptExecutionParamValues(24).Value = strTrackAttendanceBy.ToString.Trim

        '        rptExecutionParamValues(25) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(25).Label = "SetGradeBookAt"
        '        rptExecutionParamValues(25).Name = "SetGradeBookAt"
        '        rptExecutionParamValues(25).Value = strGradeBookLevel.ToString.ToLower.Trim

        '        rptExecutionParamValues(26) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(26).Label = "DisplayHours"
        '        rptExecutionParamValues(26).Name = "DisplayHours"
        '        rptExecutionParamValues(26).Value = strDisplayAttendanceUnitForProgressReportByClass.ToString.ToLower.Trim

        '        rptExecutionParamValues(27) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(27).Label = "ShowTermModule"
        '        rptExecutionParamValues(27).Name = "ShowTermModule"
        '        If boolShowTermModule = True Then
        '            rptExecutionParamValues(27).Value = "true"
        '        Else
        '            rptExecutionParamValues(27).Value = "false"
        '        End If

        '        rptExecutionParamValues(28) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(28).Label = "ShowDateInFooter"
        '        rptExecutionParamValues(28).Name = "ShowDateInFooter"
        '        If boolShowDateInFooter = True Then
        '            rptExecutionParamValues(28).Value = "true"
        '        Else
        '            rptExecutionParamValues(28).Value = "false"
        '        End If

        '        rptExecutionParamValues(29) = New ReportExecution2005.ParameterValue()
        '        rptExecutionParamValues(29).Label = "StudentIdentifier"
        '        rptExecutionParamValues(29).Name = "StudentIdentifier"
        '        rptExecutionParamValues(29).Value = strStudentIdentifier
        '    End If
        '    Return rptExecutionParamValues
        'End Function
#End Region

#Region "Build Report"
        Public Function RenderReport(ByVal _format As String,
                                                                    ByVal getReportParametersObj As ReportSelectionValues,
                                                                    ByVal strSchoolName As String) As [Byte]()

            'Initialize the report services and report execution services
            InitializeWebServices()



            'Set the path of report
            If getReportParametersObj.GroupByWorkUnits = False Then 'Do not show work units option is selected (default option)
                '   _strReportName = getReportParametersObj.Environment & getReportParametersObj.ReportPath.ToString.Trim & "/PR_Main_DonotShowWorkUnits"
                _strReportName = getReportParametersObj.Environment & getReportParametersObj.ReportPath.ToString.Trim & "/PR_Main_Sub1"
            Else
                _strReportName = getReportParametersObj.Environment & getReportParametersObj.ReportPath.ToString.Trim & "/PR_Main_Sub1"
            End If

            'Use the Report Services: GetReportParameters method to get a list of parameters for a specified report
            'This is the only location where the reportingservices2005 is used
            _rptParameters = _rptService.GetReportParameters(_strReportName, _strHistoryId, _boolForRendering, _rptParameterValues, _credentials)

            'Need to load the specific report before passing the parameter values
            _rptExecutionInfoObj = _rptExecutionService.LoadReport(_strReportName, _strHistoryId)

            ' Prepare report parameter. 
            _rptExecutionServiceParameterValues = BuildReportParameters(_rptParameters, getReportParametersObj, strSchoolName)

            _rptExecutionService.Timeout = System.Threading.Timeout.Infinite

            'Set Report Parameters
            _rptExecutionService.SetExecutionParameters(_rptExecutionServiceParameterValues, "en-us")

            'Call ConfigureExportFormat to get the file extensions
            ConfigureExportFormat(_format)

            'Render the report
            _generateReportAsBytes = _rptExecutionService.Render(_format, _deviceInfo, _strExtension, _encoding, _strMimeType, _warnings, _streamIDs)

            Return _generateReportAsBytes
            'Display the report in the browser
            'DisplayReport(_generateReportAsBytes)
        End Function
#End Region

#Region "DisplayReport"
        'While designing the report in BIDS make sure the following rule of thumb is followed
        ' (BodyWidth+LeftMargin+RightMargin) <= (PageWidth).
        'The PageWidth, LeftMargin and RightMargin can be set by selecting report - properties.
        'If the rule is not followed, the table will be truncated or blank pages will be inserted
        Private Sub DisplayReport(ByVal byteReportOutput As [Byte]())
            'Pdf
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.ContentType = _strMimeType
            HttpContext.Current.Response.BinaryWrite(byteReportOutput)

            'CSV
            'Dim name As String = "AdvReport"
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.ContentType = _strMimeType
            'HttpContext.Current.Response.AddHeader("content-disposition", ("attachment; filename=" & name & ".") + _strExtension)
            'HttpContext.Current.Response.BinaryWrite(byteReportOutput)

        End Sub
        'Sub GetConnectionStringValues()
        '    Dim lcConnString = System.Configuration.ConfigurationManager.AppSettings("ConString").ToLower

        '    Dim getReportParametersObj As ReportSelectionValues = New ReportSelectionValues()

        '    Dim startIndex As Integer = 0
        '    Dim strServerName As String = ""
        '    Dim strDBName As String = ""
        '    Dim strUserName As String = ""
        '    Dim strPassword As String = ""

        '    'Get Server Name
        '    startIndex = lcConnString.IndexOf("server=")
        '    If startIndex > -1 Then startIndex += 7
        '    Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
        '    If endIndex = -1 Then endIndex = lcConnString.Length
        '    getReportParametersObj.ServerName = lcConnString.Substring(startIndex, endIndex - startIndex)

        '    'Get dbName
        '    startIndex = lcConnString.IndexOf("database=")
        '    If startIndex > -1 Then startIndex += 9
        '    endIndex = lcConnString.IndexOf(";", startIndex)
        '    If endIndex = -1 Then endIndex = lcConnString.Length
        '    getReportParametersObj.DatabaseName = lcConnString.Substring(startIndex, endIndex - startIndex)

        '    'Get User Name
        '    startIndex = lcConnString.IndexOf("uid=")
        '    If startIndex > -1 Then startIndex += 4
        '    endIndex = lcConnString.IndexOf(";", startIndex)
        '    If endIndex = -1 Then endIndex = lcConnString.Length
        '    getReportParametersObj.UserName = lcConnString.Substring(startIndex, endIndex - startIndex)

        '    'Get Password
        '    startIndex = lcConnString.IndexOf("pwd=")
        '    If startIndex > -1 Then startIndex += 4
        '    endIndex = lcConnString.IndexOf(";", startIndex)
        '    If endIndex = -1 Then endIndex = lcConnString.Length
        '    getReportParametersObj.Password = lcConnString.Substring(startIndex, endIndex - startIndex)
        'End Sub
        Function GetDatabaseName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("database=")
            If startIndex > -1 Then startIndex += 9
            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""
            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length
            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetServerName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("server=")
            If startIndex > -1 Then startIndex += 7

            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetUserName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("uid=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetPassword(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("pwd=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
#End Region
    End Class
End Namespace
