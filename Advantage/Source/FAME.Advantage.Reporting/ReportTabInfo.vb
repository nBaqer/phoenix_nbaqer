﻿
Imports FAME.Parameters.Info

Namespace Info
    <Serializable()>
    Public Class ReportTabInfo
        Private _ReportId As Guid
        Private _PageViewId As String
        Private _SetId As Integer
        Private _UserControlName As String
        Private _ControllerClass As String
        Private _DisplayName As String
        Private _ParameterSetLookUp As String
        Private _TabName As String
        Private _ParamSet As ParameterSetInfo
        Public Property ReportId() As Guid
            Get
                Return _ReportId
            End Get
            Set(ByVal Value As Guid)
                _ReportId = Value
            End Set
        End Property
        Public Property PageViewId() As String
            Get
                Return _PageViewId
            End Get
            Set(ByVal Value As String)
                _PageViewId = Value
            End Set
        End Property
        Public Property SetId() As Integer
            Get
                Return _SetId
            End Get
            Set(ByVal Value As Integer)
                _SetId = Value
            End Set
        End Property
        Public Property UserControlName() As String
            Get
                Return _UserControlName
            End Get
            Set(ByVal Value As String)
                _UserControlName = Value
            End Set
        End Property
        Public Property ControllerClass() As String
            Get
                Return _ControllerClass
            End Get
            Set(ByVal Value As String)
                _ControllerClass = Value
            End Set
        End Property
        Public Property DisplayName() As String
            Get
                Return _DisplayName
            End Get
            Set(ByVal Value As String)
                _DisplayName = Value
            End Set
        End Property
        Public Property ParameterSetLookUp() As String
            Get
                Return _ParameterSetLookUp
            End Get
            Set(ByVal Value As String)
                _ParameterSetLookUp = Value
            End Set
        End Property
        Public Property TabName() As String
            Get
                Return _TabName
            End Get
            Set(ByVal Value As String)
                _TabName = Value
            End Set
        End Property
        Public Property ParamSet() As ParameterSetInfo
            Get
                Return _ParamSet
            End Get
            Set(ByVal Value As ParameterSetInfo)
                _ParamSet = Value
            End Set
        End Property
    End Class
End Namespace

