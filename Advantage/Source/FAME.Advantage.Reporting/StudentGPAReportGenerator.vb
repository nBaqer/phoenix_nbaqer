﻿Imports FAME.Advantage.Reporting.ReportService2005
Imports FAME.Advantage.Reporting.ReportExecution2005
Imports System.Configuration
Imports System.Web
Imports FAME.Advantage.Reporting.Info
Imports FAME.Advantage.Reporting
Imports FAME.Advantage.Common
Namespace Logic
    Public Class StudentGpaReportGenerator
#Region "Report Variables"
        Private _rptService As New ReportingService2005
        Private _rptExecutionService As New ReportExecution2005.ReportExecutionService

        Private _strReportName As String = Nothing
        Private _strHistoryId As String = Nothing
        Private _boolForRendering As Boolean = False
        Private _credentials As ReportService2005.DataSourceCredentials() = Nothing
        Private _rptParameterValues As ReportService2005.ParameterValue() = Nothing
        Private _rptParameters As ReportService2005.ReportParameter() = Nothing
        Private _rptExecutionServiceParameterValues As ReportExecution2005.ParameterValue() = Nothing
        Private _rptExecutionInfoObj As New ExecutionInfo

        'Variables needed to render report
        Private _historyId As String = Nothing
        Private _deviceInfo As String = Nothing
        'Private _format As String = Nothing
        Private _generateReportAsBytes As [Byte]()
        Private _encoding As String = [String].Empty
        Private Shared _strExtension As String = String.Empty
        Private Shared _strMimeType As String = String.Empty
        Private _warnings As ReportExecution2005.Warning() = Nothing
        Private _streamIDs As String() = Nothing

        Dim strServerName As String = ""
        Dim strDBName As String = ""
        Dim strUserName As String = ""
        Dim strPassword As String = ""
        Dim StudentIdentifier As String = ""
#End Region

#Region "Initialize Web Services"
        Private Sub InitializeWebServices()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString).ToLower
            _rptService.Credentials = System.Net.CredentialCache.DefaultCredentials
            _rptService.Url = MyAdvAppSettings.AppSettings("ReportServices").ToString
            'Set the credentials and url for the report execution services
            _rptExecutionService.Credentials = System.Net.CredentialCache.DefaultCredentials
            '_rptExecutionService.SetExecutionCredentials(dsCredentials)
            _rptExecutionService.Url = MyAdvAppSettings.AppSettings("ReportExecutionServices").ToString
        End Sub
#End Region

#Region "Initialize Export Formats"
        Private Shared Sub ConfigureExportFormat(ByRef strType As String)
            Select Case strType.ToUpper()
                Case "PDF"
                    _strExtension = "pdf"
                    _strMimeType = "application/pdf"
                    Exit Select
                Case "EXCEL", "XLS"
                    _strExtension = "xls"
                    _strMimeType = "application/vnd.excel"
                    Exit Select
                Case "WORD"
                    _strExtension = "doc"
                    _strMimeType = "application/vnd.ms-word"
                Case "CSV"
                    _strExtension = "csv"
                    _strMimeType = "text/csv"
                Case Else
                    Throw New Exception("Unrecognized type: " & strType & ". Type must be PDF, Excel or Image, HTML.")
            End Select
        End Sub
#End Region

#Region "Build Report Parameters"
        Private Function BuildReportParameters(ByVal rptServicesParameters As ReportService2005.ReportParameter(), _
                                                                       ByVal getReportParametersObj As StudentGpaReportSelectionValues, _
                                                                       ByVal strSchoolName As String) As ReportExecution2005.ParameterValue()

            Dim rptExecutionParamValues As ReportExecution2005.ParameterValue() = New ReportExecution2005.ParameterValue(rptServicesParameters.Length - 1) {}
            Dim iParam As Integer = 0
            Dim strNullValue As String = "Null"

            Dim strCampGrpId As String = ""
            Dim strPrgVerId As String = ""
            Dim strEnrollmentStatus As String = ""
            Dim strAsOfDate As String = ""

            Dim strCampusId As String = ""
            Dim strReqId As String = ""
            Dim intNumberofDays As Integer = 0
            Dim dtCutOffDate As DateTime

            Dim strServerName As String = ""
            Dim strDBName As String = ""
            Dim strUserName As String = ""
            Dim strPassword As String = ""

            Dim strEnrollmentStatusId As String = ""
            Dim strStudentGrpId As String = ""
            Dim strStartDate As String = ""
            Dim cumGPAGT As Decimal
            Dim cumGPALT As Decimal
            Dim termGpagt As Decimal
            Dim termGPALT As Decimal
            Dim termCreditGT As Decimal
            Dim termCreditLT As Decimal
            Dim termid As String = ""
            'Dim boolShowGPAByTerm As Boolean = False

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            Dim strConnectionString As String = (MyAdvAppSettings.AppSettings("ConString").ToString).ToLower

            '  GetConnectionStringValues()

            With getReportParametersObj
                strPrgVerId = .PrgVerId
                strCampusId = .CampusId
                strEnrollmentStatusId = .StatusCodeId
                strStudentGrpId = .StudentGrpId
                strStartDate = DateTime.Now.ToString("MM/dd/yyyy")
                cumGPAGT = .CumulativeGpagt
                cumGPALT = .CumulativeGpalt
                termGpagt = .TermGpagt
                termGPALT = .TermGpalt
                termCreditGT = .TermCreditGt
                termCreditLT = .TermCreditLt
                termid = .TermId
                strServerName = GetServerName(strConnectionString)
                strDBName = GetDatabaseName(strConnectionString)
                strUserName = GetUserName(strConnectionString)
                strPassword = GetPassword(strConnectionString)
                'boolShowGPAByTerm = .ShowGpAbyTerm
                StudentIdentifier = .StudentIdentifier
            End With

            'If InStrRev(termid, ",") >= 1 Then
            '    termid = termid.Substring(0, termid.Length - 1)
            'End If

            'If InStrRev(strPrgVerId, ",") >= 1 Then
            '    strPrgVerId = strPrgVerId.Substring(0, strPrgVerId.Length - 1)
            'End If

            'If InStrRev(strEnrollmentStatusId, ",") >= 1 Then
            '    strEnrollmentStatusId = strEnrollmentStatusId.Substring(0, strEnrollmentStatusId.Length - 1)
            'End If

            'If InStrRev(strStudentGrpId, ",") >= 1 Then
            '    strStudentGrpId = strStudentGrpId.Substring(0, strStudentGrpId.Length - 1)
            'End If



            'Pass the parameter values
            If rptServicesParameters.Length > 0 Then

                'Server Name   0
                iParam = 0
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(iParam).Label = "ServerName"
                rptExecutionParamValues(iParam).Name = "servername"
                rptExecutionParamValues(iParam).Value = strServerName

                'Database Name   1
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(iParam).Label = "DBName"
                rptExecutionParamValues(iParam).Name = "databasename"
                rptExecutionParamValues(iParam).Value = strDBName
                '  rptExecutionParamValues(iParam).Value = "[" + strDBName + "]"

                'User id      2
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(iParam).Label = "UserName"
                rptExecutionParamValues(iParam).Name = "uid"
                rptExecutionParamValues(iParam).Value = strUserName

                'password    3
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(iParam).Label = "Password"
                rptExecutionParamValues(iParam).Name = "password"
                rptExecutionParamValues(iParam).Value = strPassword

                'CampusId Parameter     4
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(iParam).Label = "CampusId"
                rptExecutionParamValues(iParam).Name = "CampusId"
                rptExecutionParamValues(iParam).Value = strCampusId.ToString

                'PrgVerId  List Parameter    5
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(iParam).Label = "PrgVerId"
                rptExecutionParamValues(iParam).Name = "PrgVerId"
                rptExecutionParamValues(iParam).Value = strPrgVerId.ToString

                'termid  List Parameter   6
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(iParam).Label = "termid"
                rptExecutionParamValues(iParam).Name = "termid"
                rptExecutionParamValues(iParam).Value = termid.ToString

                'Enrollment Status List Parameter    7
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(iParam).Label = "EnrollmentStatusId"
                rptExecutionParamValues(iParam).Name = "EnrollmentStatusId"
                rptExecutionParamValues(iParam).Value = strEnrollmentStatusId.ToString


                'Student Group List Parameter  8
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(iParam).Label = "StudentGrpId"
                rptExecutionParamValues(iParam).Name = "StudentGrpId"
                rptExecutionParamValues(iParam).Value = strStudentGrpId.ToString


                'Start Date Parameter     9
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(iParam).Label = "StartDate"
                rptExecutionParamValues(iParam).Name = "StartDate"
                rptExecutionParamValues(iParam).Value = strStartDate.ToString

                'cumGPAGT Parameter      10
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(iParam).Label = "cumGPAGT"
                rptExecutionParamValues(iParam).Name = "cumGPAGT"
                If String.IsNullOrEmpty(cumGPAGT.ToString()) Then   '    Or cumGPAGT.ToString() = "0.00"
                    rptExecutionParamValues(iParam).Value = Nothing
                Else
                    rptExecutionParamValues(iParam).Value = cumGPAGT.ToString
                End If

                'cumGPALT Parameter    11
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(iParam).Label = "cumGPALT"
                rptExecutionParamValues(iParam).Name = "cumGPALT"
                If String.IsNullOrEmpty(cumGPALT.ToString()) Then ' Or cumGPALT.ToString() = "9999.00" Then
                    rptExecutionParamValues(iParam).Value = Nothing
                Else
                    rptExecutionParamValues(iParam).Value = cumGPALT.ToString
                End If

                'termGPAGT Parameter    12
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(iParam).Label = "termGPAGT"
                rptExecutionParamValues(iParam).Name = "termGPAGT"
                If String.IsNullOrEmpty(termGpagt.ToString()) Then ' Or termGpagt.ToString() = "9999.00" Then
                    rptExecutionParamValues(iParam).Value = Nothing
                Else
                    rptExecutionParamValues(iParam).Value = termGpagt.ToString
                End If


                'termGPALT Parameter     12
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(iParam).Label = "termGPALT"
                rptExecutionParamValues(iParam).Name = "termGPALT"
                If String.IsNullOrEmpty(termGPALT.ToString()) Then ' Or termGPALT.ToString() = "9999.00" Then
                    rptExecutionParamValues(iParam).Value = Nothing
                Else
                    rptExecutionParamValues(iParam).Value = termGPALT.ToString
                End If

                'termCreditGT Parameter      14
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(iParam).Label = "termCreditGT"
                rptExecutionParamValues(iParam).Name = "termCreditGT"
                If String.IsNullOrEmpty(termCreditGT.ToString()) Then ' Or termCreditGT.ToString() = "9999.00" Then
                    rptExecutionParamValues(iParam).Value = Nothing
                Else
                    rptExecutionParamValues(iParam).Value = termCreditGT.ToString
                End If

                'termCreditLT Parameter       15
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(iParam).Label = "termCreditLT"
                rptExecutionParamValues(iParam).Name = "termCreditLT"
                If String.IsNullOrEmpty(termCreditLT.ToString()) Then ' Or termCreditLT.ToString() = "9999.00" Then
                    rptExecutionParamValues(iParam).Value = Nothing
                Else
                    rptExecutionParamValues(iParam).Value = termCreditLT.ToString
                End If

                'Student Identifier          16
                iParam += 1
                rptExecutionParamValues(iParam) = New ReportExecution2005.ParameterValue()
                rptExecutionParamValues(iParam).Label = "StudentIdentifier"
                rptExecutionParamValues(iParam).Name = "StudentIdentifier"
                rptExecutionParamValues(iParam).Value = StudentIdentifier
            End If
            Return rptExecutionParamValues
        End Function
#End Region

#Region "Build Report"
        Public Function RenderReport(ByVal _format As String _
                                     , ByVal getReportParametersObj As StudentGpaReportSelectionValues _
                                     , ByVal strSchoolName As String _
                                     ) As [Byte]()

            'Initialize the report services and report execution services
            InitializeWebServices()



            'Set the path of report
            _strReportName = getReportParametersObj.Environment + getReportParametersObj.ReportPath
            'Use the Report Services: GetReportParameters method to get a list of parameters for a specified report
            'This is the only location where the reportingservices2005 is used
            _rptParameters = _rptService.GetReportParameters(_strReportName, _strHistoryId, _boolForRendering, _rptParameterValues, _credentials)

            'Need to load the specific report before passing the parameter values
            _rptExecutionInfoObj = _rptExecutionService.LoadReport(_strReportName, _strHistoryId)

            ' Prepare report parameter. 
            _rptExecutionServiceParameterValues = BuildReportParameters(_rptParameters, getReportParametersObj, strSchoolName)

            _rptExecutionService.Timeout = System.Threading.Timeout.Infinite

            'Set Report Parameters
            _rptExecutionService.SetExecutionParameters(_rptExecutionServiceParameterValues, "en-us")

            'Call ConfigureExportFormat to get the file extensions
            ConfigureExportFormat(_format)

            'Render the report
            _generateReportAsBytes = _rptExecutionService.Render(_format, _deviceInfo, _strExtension, _encoding, _strMimeType, _warnings, _streamIDs)

            Return _generateReportAsBytes
            'Display the report in the browser
            'DisplayReport(_generateReportAsBytes)
        End Function
#End Region

#Region "DisplayReport"
        Private Sub DisplayReport(ByVal byteReportOutput As [Byte]())
            'Pdf
            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.ContentType = _strMimeType
            HttpContext.Current.Response.BinaryWrite(byteReportOutput)
        End Sub
        Function GetDatabaseName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("database=")
            If startIndex > -1 Then startIndex += 9
            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""
            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length
            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetServerName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("server=")
            If startIndex > -1 Then startIndex += 7

            ' if the "database", "data source" or "initial catalog" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetUserName(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("uid=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
        Function GetPassword(ByVal connString As String) As String
            Dim lcConnString = connString.ToLower()

            ' if this is a Jet database, find the index of the "data source" setting
            Dim startIndex As Integer
            startIndex = lcConnString.IndexOf("pwd=")
            If startIndex > -1 Then startIndex += 4

            ' if the "uid" values are not 
            ' found, return an empty string
            If startIndex = -1 Then Return ""

            ' find where the database name/path ends
            Dim endIndex As Integer = lcConnString.IndexOf(";", startIndex)
            If endIndex = -1 Then endIndex = lcConnString.Length

            ' return the substring with the database name/path
            Return connString.Substring(startIndex, endIndex - startIndex)
        End Function
#End Region
    End Class
End Namespace



