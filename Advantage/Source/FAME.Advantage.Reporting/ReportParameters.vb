﻿''' <summary>
''' This class hold the parameters necesary to config a report services.
''' </summary>
''' <remarks></remarks>
Public Class ReportParameters
    
    Public Property Label As String
    Public Property Name As String
    Public Property Value As String

End Class
