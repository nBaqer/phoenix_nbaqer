﻿Namespace Info
    Public Class TitleIVSelectionValues
#Region "Variables"
        Private _fromdate As Date
        Private _toDate As Date
        Private _CampusID As String
        Private _ResourceId As Integer
        Private _ReportPath As String
        Private _boolShowDateInFooter As Boolean = False
        Private _boolShowPageNumber As Boolean = True
        Private _Environment As String
#End Region
#Region "Address Initialize"
        Public Sub New()
            _ResourceId = 0
            _ReportPath = ""
            _boolShowPageNumber = True
            _boolShowDateInFooter = False
        End Sub
#End Region
#Region "Properties"
        Public Property FromDate() As Date
            Get
                Return _fromdate
            End Get
            Set(ByVal Value As Date)
                _fromdate = Value
            End Set
        End Property
        Public Property ToDate() As Date
            Get
                Return _toDate
            End Get
            Set(ByVal Value As Date)
                _toDate = Value
            End Set
        End Property


        Public Property CampusID() As String
            Get
                Return _CampusID
            End Get
            Set(ByVal Value As String)
                _CampusID = Value
            End Set
        End Property
        Public Property ResourceId() As Integer
            Get
                Return _ResourceId
            End Get
            Set(ByVal Value As Integer)
                _ResourceId = Value
            End Set
        End Property
        Public Property ReportPath() As String
            Get
                Return _ReportPath
            End Get
            Set(ByVal Value As String)
                _ReportPath = Value
            End Set
        End Property
        Public Property ShowPageNumber() As Boolean
            Get
                Return _boolShowPageNumber
            End Get
            Set(ByVal Value As Boolean)
                _boolShowPageNumber = Value
            End Set
        End Property
        Public Property ShowDateInFooter() As Boolean
            Get
                Return _boolShowDateInFooter
            End Get
            Set(ByVal Value As Boolean)
                _boolShowDateInFooter = Value
            End Set
        End Property
        Public Property Environment() As String
            Get
                Return _Environment
            End Get
            Set(ByVal Value As String)
                _Environment = Value
            End Set
        End Property
#End Region

    End Class
End Namespace
