﻿Public Class WeeklyAttendanceMultipleSelectionValues
#Region "Variables"
    Private _campusId As String
    Private _selectedDate As Date
    Private _termId As String
    Private _courseId As String
    Private _instructorId As String
    Private _shiftId As String
    Private _sectionId As String
    Private _enrollmentId As String
    Private _ReportPath As String
    Private _environment As String
#End Region

#Region "Address Initialize"
    Public Sub New()

    End Sub
#End Region
#Region "Properties"

    Public Property CampusId() As String
        Get
            Return _campusId
        End Get
        Set(ByVal value As String)
            _campusId = value
        End Set
    End Property
    Public Property SelectedDate() As Date
        Get
            Return _selectedDate
        End Get
        Set(ByVal value As Date)
            _selectedDate = value
        End Set
    End Property
    Public Property TermId() As String
        Get
            Return _termId
        End Get
        Set(ByVal value As String)
            _termId = value
        End Set
    End Property
    Public Property CourseId() As String
        Get
            Return _courseId
        End Get
        Set(ByVal value As String)
            _courseId = value
        End Set
    End Property
    Public Property InstructorId() As String
        Get
            Return _instructorId
        End Get
        Set(ByVal value As String)
            _instructorId = value
        End Set
    End Property
    Public Property ShiftId() As String
        Get
            Return _shiftId
        End Get
        Set(ByVal value As String)
            _shiftId = value
        End Set
    End Property
    Public Property SectionId() As String
        Get
            Return _sectionId
        End Get
        Set(ByVal value As String)
            _sectionId = value
        End Set
    End Property
    Public Property EnrollmentId() As String
        Get
            Return _enrollmentId
        End Get
        Set(ByVal value As String)
            _enrollmentId = value
        End Set
    End Property
    Public Property ReportPath() As String
        Get

            Return _ReportPath
        End Get
        Set(ByVal value As String)
            _ReportPath = value
        End Set
    End Property
    Public Property Environment() As String
        Get

            Return _environment
        End Get
        Set(ByVal value As String)
            _environment = value
        End Set
    End Property
#End Region
End Class

