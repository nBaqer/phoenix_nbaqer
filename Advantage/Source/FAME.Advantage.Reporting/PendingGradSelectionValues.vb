﻿Namespace Info
    Public Class PendingGradSelectionValues
#Region "Variables"
        Private _PrgVerType As String
        Private _Amount As Decimal
        Private _CampGrpId As String
        Private _ProgVerId As String
        Private _ResourceId As Integer
        Private _ReportPath As String
        Private _boolShowDateInFooter As Boolean = False
        Private _boolShowPageNumber As Boolean = True
        Private _Environment As String
#End Region
#Region "Address Initialize"
        Public Sub New()
            _PrgVerType = "Credit"
            _Amount = 5
            _CampGrpId = ""
            _ProgVerId = ""
            _ResourceId = 0
            _ReportPath = ""
            _boolShowPageNumber = True
            _boolShowDateInFooter = False
        End Sub
#End Region
#Region "Properties"
        Public Property PrgVerType() As String
            Get
                Return _PrgVerType
            End Get
            Set(ByVal Value As String)
                _PrgVerType = Value
            End Set
        End Property
        Public Property Amount() As Decimal
            Get
                Return _Amount
            End Get
            Set(ByVal Value As Decimal)
                _Amount = Value
            End Set
        End Property
        Public Property ProgVerId() As String
            Get
                Return _ProgVerId
            End Get
            Set(ByVal Value As String)
                _ProgVerId = Value
            End Set
        End Property
        Public Property CampGrpId() As String
            Get
                Return _CampGrpId
            End Get
            Set(ByVal Value As String)
                _CampGrpId = Value
            End Set
        End Property
        Public Property ResourceId() As Integer
            Get
                Return _ResourceId
            End Get
            Set(ByVal Value As Integer)
                _ResourceId = Value
            End Set
        End Property
        Public Property ReportPath() As String
            Get
                Return _ReportPath
            End Get
            Set(ByVal Value As String)
                _ReportPath = Value
            End Set
        End Property
        Public Property ShowPageNumber() As Boolean
            Get
                Return _boolShowPageNumber
            End Get
            Set(ByVal Value As Boolean)
                _boolShowPageNumber = Value
            End Set
        End Property
        Public Property ShowDateInFooter() As Boolean
            Get
                Return _boolShowDateInFooter
            End Get
            Set(ByVal Value As Boolean)
                _boolShowDateInFooter = Value
            End Set
        End Property
        Public Property Environment() As String
            Get
                Return _Environment
            End Get
            Set(ByVal Value As String)
                _Environment = Value
            End Set
        End Property
#End Region

    End Class
End Namespace
