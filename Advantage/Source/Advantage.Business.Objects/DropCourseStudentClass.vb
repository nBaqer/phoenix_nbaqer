﻿Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Security.Policy

''' <summary>
''' Dropped Courses from all enrollments
''' </summary>
''' <remarks></remarks>
Public Class DropCourseStudentClass
#Region "Constructor"

    Public Sub New(listOfModifications As IList(Of PocoDropCourse))

        ModificationsList = listOfModifications

    End Sub
#End Region

#Region "Properties"
    Public Property ClassId() As String

    ''' <summary>
    ''' The list of Requirements class to be dropped
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    Property ModificationsList As List(Of PocoDropCourse)

    ''' <summary>
    ''' The list of enrollment where the operation is applied.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    Property EnrollmentIdToModifyList As List(Of String)
#End Region

#Region " Public Methods"

    Public Sub DroppingStudents()

        Dim listOperations As IList(Of PocoDropCourse) = New List(Of PocoDropCourse)

        For Each poco As PocoDropCourse In ModificationsList

            ' Create the list of enrollment active that the student has, that contain the classId 
            EnrollmentIdToModifyList = RegFacade.GetActiveEnrollmentWithClassAssociated(poco.StuEnrollId, poco.ClassId)

            ' For all enrollments active for the student
            For Each s As String In EnrollmentIdToModifyList

                ' PrepareInsertionRecords
                Dim op As PocoDropCourse = New PocoDropCourse()
                op.ClassId = poco.ClassId
                op.DateDetermined = poco.DateDetermined
                op.Grade = poco.Grade
                op.User = poco.User
                op.StuEnrollId = s
                listOperations.Add(op)
            Next
        Next

        'Update the records
        DropStudentFacade.UpdateFinalGrade(listOperations)



    End Sub


#End Region


End Class
