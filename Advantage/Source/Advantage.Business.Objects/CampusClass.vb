﻿Imports System
Imports System.Collections.Generic

Namespace Advantage.Business.Objects
    Public Class CampusClass

    End Class

    <Serializable()>
    Public Class Campus
        Private _zip As String
        Public Overridable Property Zip As String
            Get
                Return Me._zip
            End Get
            Set(ByVal value As String)
                Me._zip = value
            End Set
        End Property

        Private _website As String
        Public Overridable Property Website As String
            Get
                Return Me._website
            End Get
            Set(ByVal value As String)
                Me._website = value
            End Set
        End Property

        Private _useCampusAddress As Boolean?
        Public Overridable Property UseCampusAddress As Boolean?
            Get
                Return Me._useCampusAddress
            End Get
            Set(ByVal value As Boolean?)
                Me._useCampusAddress = value
            End Set
        End Property

        Private _transcriptAuthZnTitle As String
        Public Overridable Property TranscriptAuthZnTitle As String
            Get
                Return Me._transcriptAuthZnTitle
            End Get
            Set(ByVal value As String)
                Me._transcriptAuthZnTitle = value
            End Set
        End Property

        Private _transcriptAuthZnName As String
        Public Overridable Property TranscriptAuthZnName As String
            Get
                Return Me._transcriptAuthZnName
            End Get
            Set(ByVal value As String)
                Me._transcriptAuthZnName = value
            End Set
        End Property

        Private _tCTargetPath As String
        Public Overridable Property TCTargetPath As String
            Get
                Return Me._tCTargetPath
            End Get
            Set(ByVal value As String)
                Me._tCTargetPath = value
            End Set
        End Property

        Private _tCSourcePath As String
        Public Overridable Property TCSourcePath As String
            Get
                Return Me._tCSourcePath
            End Get
            Set(ByVal value As String)
                Me._tCSourcePath = value
            End Set
        End Property

        Private _targetFolderLocFL As String
        Public Overridable Property TargetFolderLocFL As String
            Get
                Return Me._targetFolderLocFL
            End Get
            Set(ByVal value As String)
                Me._targetFolderLocFL = value
            End Set
        End Property

        Private _targetFolderLoc As String
        Public Overridable Property TargetFolderLoc As String
            Get
                Return Me._targetFolderLoc
            End Get
            Set(ByVal value As String)
                Me._targetFolderLoc = value
            End Set
        End Property

        Private _statusId As Guid
        Public Overridable Property StatusId As Guid
            Get
                Return Me._statusId
            End Get
            Set(ByVal value As Guid)
                Me._statusId = value
            End Set
        End Property

        Private _stateId As System.Nullable(Of System.Guid)
        Public Overridable Property StateId As System.Nullable(Of System.Guid)
            Get
                Return Me._stateId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._stateId = value
            End Set
        End Property

        Private _sourceFolderLocFL As String
        Public Overridable Property SourceFolderLocFL As String
            Get
                Return Me._sourceFolderLocFL
            End Get
            Set(ByVal value As String)
                Me._sourceFolderLocFL = value
            End Set
        End Property

        Private _sourceFolderLoc As String
        Public Overridable Property SourceFolderLoc As String
            Get
                Return Me._sourceFolderLoc
            End Get
            Set(ByVal value As String)
                Me._sourceFolderLoc = value
            End Set
        End Property

        Private _remoteServerUsrNmFL As String
        Public Overridable Property RemoteServerUsrNmFL As String
            Get
                Return Me._remoteServerUsrNmFL
            End Get
            Set(ByVal value As String)
                Me._remoteServerUsrNmFL = value
            End Set
        End Property

        Private _remoteServerUsrNm As String
        Public Overridable Property RemoteServerUsrNm As String
            Get
                Return Me._remoteServerUsrNm
            End Get
            Set(ByVal value As String)
                Me._remoteServerUsrNm = value
            End Set
        End Property

        Private _remoteServerPwdFL As String
        Public Overridable Property RemoteServerPwdFL As String
            Get
                Return Me._remoteServerPwdFL
            End Get
            Set(ByVal value As String)
                Me._remoteServerPwdFL = value
            End Set
        End Property

        Private _remoteServerPwd As String
        Public Overridable Property RemoteServerPwd As String
            Get
                Return Me._remoteServerPwd
            End Get
            Set(ByVal value As String)
                Me._remoteServerPwd = value
            End Set
        End Property

        Private _portalContactEmail As String
        Public Overridable Property PortalContactEmail As String
            Get
                Return Me._portalContactEmail
            End Get
            Set(ByVal value As String)
                Me._portalContactEmail = value
            End Set
        End Property

        Private _phone3 As String
        Public Overridable Property Phone3 As String
            Get
                Return Me._phone3
            End Get
            Set(ByVal value As String)
                Me._phone3 = value
            End Set
        End Property

        Private _phone2 As String
        Public Overridable Property Phone2 As String
            Get
                Return Me._phone2
            End Get
            Set(ByVal value As String)
                Me._phone2 = value
            End Set
        End Property

        Private _phone1 As String
        Public Overridable Property Phone1 As String
            Get
                Return Me._phone1
            End Get
            Set(ByVal value As String)
                Me._phone1 = value
            End Set
        End Property

        Private _modUser As String
        Public Overridable Property ModUser As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property

        Private _modDate As Date?
        Public Overridable Property ModDate As Date?
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As Date?)
                Me._modDate = value
            End Set
        End Property

        Private _isRemoteServerFL As Boolean
        Public Overridable Property IsRemoteServerFL As Boolean
            Get
                Return Me._isRemoteServerFL
            End Get
            Set(ByVal value As Boolean)
                Me._isRemoteServerFL = value
            End Set
        End Property

        Private _isRemoteServer As Boolean?
        Public Overridable Property IsRemoteServer As Boolean?
            Get
                Return Me._isRemoteServer
            End Get
            Set(ByVal value As Boolean?)
                Me._isRemoteServer = value
            End Set
        End Property

        Private _isCorporate As Boolean?
        Public Overridable Property IsCorporate As Boolean?
            Get
                Return Me._isCorporate
            End Get
            Set(ByVal value As Boolean?)
                Me._isCorporate = value
            End Set
        End Property

        Private _invZip As String
        Public Overridable Property InvZip As String
            Get
                Return Me._invZip
            End Get
            Set(ByVal value As String)
                Me._invZip = value
            End Set
        End Property

        Private _invStateID As System.Nullable(Of System.Guid)
        Public Overridable Property InvStateID As System.Nullable(Of System.Guid)
            Get
                Return Me._invStateID
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._invStateID = value
            End Set
        End Property

        Private _invPhone1 As String
        Public Overridable Property InvPhone1 As String
            Get
                Return Me._invPhone1
            End Get
            Set(ByVal value As String)
                Me._invPhone1 = value
            End Set
        End Property

        Private _invFax As String
        Public Overridable Property InvFax As String
            Get
                Return Me._invFax
            End Get
            Set(ByVal value As String)
                Me._invFax = value
            End Set
        End Property

        Private _invCountryID As System.Nullable(Of System.Guid)
        Public Overridable Property InvCountryID As System.Nullable(Of System.Guid)
            Get
                Return Me._invCountryID
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._invCountryID = value
            End Set
        End Property

        Private _invCity As String
        Public Overridable Property InvCity As String
            Get
                Return Me._invCity
            End Get
            Set(ByVal value As String)
                Me._invCity = value
            End Set
        End Property

        Private _invAddress2 As String
        Public Overridable Property InvAddress2 As String
            Get
                Return Me._invAddress2
            End Get
            Set(ByVal value As String)
                Me._invAddress2 = value
            End Set
        End Property

        Private _invAddress1 As String
        Public Overridable Property InvAddress1 As String
            Get
                Return Me._invAddress1
            End Get
            Set(ByVal value As String)
                Me._invAddress1 = value
            End Set
        End Property

        Private _fLTargetPath As String
        Public Overridable Property FLTargetPath As String
            Get
                Return Me._fLTargetPath
            End Get
            Set(ByVal value As String)
                Me._fLTargetPath = value
            End Set
        End Property

        Private _fLSourcePath As String
        Public Overridable Property FLSourcePath As String
            Get
                Return Me._fLSourcePath
            End Get
            Set(ByVal value As String)
                Me._fLSourcePath = value
            End Set
        End Property

        Private _fLExceptionPath As String
        Public Overridable Property FLExceptionPath As String
            Get
                Return Me._fLExceptionPath
            End Get
            Set(ByVal value As String)
                Me._fLExceptionPath = value
            End Set
        End Property

        Private _fax As String
        Public Overridable Property Fax As String
            Get
                Return Me._fax
            End Get
            Set(ByVal value As String)
                Me._fax = value
            End Set
        End Property

        Private _email As String
        Public Overridable Property Email As String
            Get
                Return Me._email
            End Get
            Set(ByVal value As String)
                Me._email = value
            End Set
        End Property

        Private _countryId As System.Nullable(Of System.Guid)
        Public Overridable Property CountryId As System.Nullable(Of System.Guid)
            Get
                Return Me._countryId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._countryId = value
            End Set
        End Property

        Private _city As String
        Public Overridable Property City As String
            Get
                Return Me._city
            End Get
            Set(ByVal value As String)
                Me._city = value
            End Set
        End Property

        Private _campusId As Guid
        Public Overridable Property CampusId As Guid
            Get
                Return Me._campusId
            End Get
            Set(ByVal value As Guid)
                Me._campusId = value
            End Set
        End Property

        Private _campDescrip As String
        Public Overridable Property CampDescrip As String
            Get
                Return Me._campDescrip
            End Get
            Set(ByVal value As String)
                Me._campDescrip = value
            End Set
        End Property

        Private _campCode As String
        Public Overridable Property CampCode As String
            Get
                Return Me._campCode
            End Get
            Set(ByVal value As String)
                Me._campCode = value
            End Set
        End Property

        Private _address2 As String
        Public Overridable Property Address2 As String
            Get
                Return Me._address2
            End Get
            Set(ByVal value As String)
                Me._address2 = value
            End Set
        End Property

        Private _address1 As String
        Public Overridable Property Address1 As String
            Get
                Return Me._address1
            End Get
            Set(ByVal value As String)
                Me._address1 = value
            End Set
        End Property

        Private _Users As IList(Of User) = New List(Of User)
        Public Overridable ReadOnly Property Users As IList(Of User)
            Get
                Return Me._Users
            End Get
        End Property

        Private _CampusGroup As IList(Of CampusGroup) = New List(Of CampusGroup)
        Public Overridable ReadOnly Property CampusGroup As IList(Of CampusGroup)
            Get
                Return Me._CampusGroup
            End Get
        End Property

        Private _CmpGrpCmps As IList(Of CampusAndCampusGroup) = New List(Of CampusAndCampusGroup)
        Public Overridable ReadOnly Property CampusAndCampusGroups As IList(Of CampusAndCampusGroup)
            Get
                Return Me._CmpGrpCmps
            End Get
        End Property

        Private _StateDescription As String
        Public Overridable Property StateDescription As String
            Get
                Return Me._StateDescription
            End Get
            Set(ByVal value As String)
                Me._StateDescription = value
            End Set
        End Property
        Private _CountryDescription As String
        Public Overridable Property CountryDescription As String
            Get
                Return Me._CountryDescription
            End Get
            Set(ByVal value As String)
                Me._CountryDescription = value
            End Set
        End Property
        Private _StatusDescription As String
        Public Overridable Property StatusDescription As String
            Get
                Return Me._StatusDescription
            End Get
            Set(ByVal value As String)
                Me._StatusDescription = value
            End Set
        End Property
        Private _InvStateDescription As String
        Public Overridable Property InvStateDescription As String
            Get
                Return Me._InvStateDescription
            End Get
            Set(ByVal value As String)
                Me._InvStateDescription = value
            End Set
        End Property
    End Class

    <Serializable()>
    Public Class CampusAndCampusGroup

        Private _modUser As String
        Public Overridable Property ModUser As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property

        Private _modDate As Date
        Public Overridable Property ModDate As Date
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As Date)
                Me._modDate = value
            End Set
        End Property

        Private _cmpGrpCmpId As Guid
        Public Overridable Property CmpGrpCmpId As Guid
            Get
                Return Me._cmpGrpCmpId
            End Get
            Set(ByVal value As Guid)
                Me._cmpGrpCmpId = value
            End Set
        End Property

        Private _campusId As Guid
        Public Overridable Property CampusId As Guid
            Get
                Return Me._campusId
            End Get
            Set(ByVal value As Guid)
                Me._campusId = value
            End Set
        End Property

        Private _campGrpId As Guid
        Public Overridable Property CampGrpId As Guid
            Get
                Return Me._campGrpId
            End Get
            Set(ByVal value As Guid)
                Me._campGrpId = value
            End Set
        End Property

        Private _Campus As Campus
        Public Overridable Property Campus As Campus
            Get
                Return Me._Campus
            End Get
            Set(ByVal value As Campus)
                Me._Campus = value
            End Set
        End Property

        Private _CampusGroup As CampusGroup
        Public Overridable Property CampusGroup As CampusGroup
            Get
                Return Me._CampusGroup
            End Get
            Set(ByVal value As CampusGroup)
                Me._CampusGroup = value
            End Set
        End Property
    End Class

    'Partial Public Class Resource
    '    Private _usedIn As Integer?
    '    Public Overridable Property UsedIn As Integer?
    '        Get
    '            Return Me._usedIn
    '        End Get
    '        Set(ByVal value As Integer?)
    '            Me._usedIn = value
    '        End Set
    '    End Property

    '    Private _tblFldsId As Integer?
    '    Public Overridable Property TblFldsId As Integer?
    '        Get
    '            Return Me._tblFldsId
    '        End Get
    '        Set(ByVal value As Integer?)
    '            Me._tblFldsId = value
    '        End Set
    '    End Property

    '    Private _summListId As Short?
    '    Public Overridable Property SummListId As Short?
    '        Get
    '            Return Me._summListId
    '        End Get
    '        Set(ByVal value As Short?)
    '            Me._summListId = value
    '        End Set
    '    End Property

    '    Private _resourceURL As String
    '    Public Overridable Property ResourceURL As String
    '        Get
    '            Return Me._resourceURL
    '        End Get
    '        Set(ByVal value As String)
    '            Me._resourceURL = value
    '        End Set
    '    End Property

    '    Private _resourceTypeID As Byte
    '    Public Overridable Property ResourceTypeID As Byte
    '        Get
    '            Return Me._resourceTypeID
    '        End Get
    '        Set(ByVal value As Byte)
    '            Me._resourceTypeID = value
    '        End Set
    '    End Property

    '    Private _resourceID As Short
    '    Public Overridable Property ResourceID As Short
    '        Get
    '            Return Me._resourceID
    '        End Get
    '        Set(ByVal value As Short)
    '            Me._resourceID = value
    '        End Set
    '    End Property

    '    Private _resource As String
    '    Public Overridable Property Resource As String
    '        Get
    '            Return Me._resource
    '        End Get
    '        Set(ByVal value As String)
    '            Me._resource = value
    '        End Set
    '    End Property

    '    Private _mRUTypeId As Short?
    '    Public Overridable Property MRUTypeId As Short?
    '        Get
    '            Return Me._mRUTypeId
    '        End Get
    '        Set(ByVal value As Short?)
    '            Me._mRUTypeId = value
    '        End Set
    '    End Property

    '    Private _modUser As String
    '    Public Overridable Property ModUser As String
    '        Get
    '            Return Me._modUser
    '        End Get
    '        Set(ByVal value As String)
    '            Me._modUser = value
    '        End Set
    '    End Property

    '    Private _modDate As Date?
    '    Public Overridable Property ModDate As Date?
    '        Get
    '            Return Me._modDate
    '        End Get
    '        Set(ByVal value As Date?)
    '            Me._modDate = value
    '        End Set
    '    End Property

    '    Private _childTypeId As Byte?
    '    Public Overridable Property ChildTypeId As Byte?
    '        Get
    '            Return Me._childTypeId
    '        End Get
    '        Set(ByVal value As Byte?)
    '            Me._childTypeId = value
    '        End Set
    '    End Property

    '    Private _allowSchlReqFlds As Boolean?
    '    Public Overridable Property AllowSchlReqFlds As Boolean?
    '        Get
    '            Return Me._allowSchlReqFlds
    '        End Get
    '        Set(ByVal value As Boolean?)
    '            Me._allowSchlReqFlds = value
    '        End Set
    '    End Property

    '    Private _syUsers As IList(Of User) = New List(Of User)
    '    Public Overridable ReadOnly Property SyUsers As IList(Of User)
    '        Get
    '            Return Me._syUsers
    '        End Get
    '    End Property

    'End Class

    <Serializable()>
    Public Class Role
        Private _sysRoleId As Integer
        Public Overridable Property SysRoleId As Integer
            Get
                Return Me._sysRoleId
            End Get
            Set(ByVal value As Integer)
                Me._sysRoleId = value
            End Set
        End Property

        Private _statusId As Guid
        Public Overridable Property StatusId As Guid
            Get
                Return Me._statusId
            End Get
            Set(ByVal value As Guid)
                Me._statusId = value
            End Set
        End Property

        Private _roleId As Guid
        Public Overridable Property RoleId As Guid
            Get
                Return Me._roleId
            End Get
            Set(ByVal value As Guid)
                Me._roleId = value
            End Set
        End Property

        Private _role As String
        Public Overridable Property Role As String
            Get
                Return Me._role
            End Get
            Set(ByVal value As String)
                Me._role = value
            End Set
        End Property

        Private _modUser As String
        Public Overridable Property ModUser As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property

        Private _modDate As Date?
        Public Overridable Property ModDate As Date?
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As Date?)
                Me._modDate = value
            End Set
        End Property

        Private _code As String
        Public Overridable Property Code As String
            Get
                Return Me._code
            End Get
            Set(ByVal value As String)
                Me._code = value
            End Set
        End Property
        Private _syUsersRolesCampGrps As IList(Of UserRolesCampusGroup) = New List(Of UserRolesCampusGroup)
        Public Overridable ReadOnly Property SyUsersRolesCampGrps As IList(Of UserRolesCampusGroup)
            Get
                Return Me._syUsersRolesCampGrps
            End Get
        End Property
    End Class
End Namespace
