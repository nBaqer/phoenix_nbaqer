﻿Namespace Advantage.Business.Objects
    Public Class SetUpRequiredFieldResources
        Private _ModuleResourceId As Integer
        Private _ResourceId As Integer
        Private _Resource As String
        Private _ResourceTypeId As Integer
        Private _TabId As Integer
        Public Property ModuleResourceId() As Integer
            Set(value As Integer)
                _ModuleResourceId = value
            End Set
            Get
                Return _ModuleResourceId
            End Get
        End Property
        Public Property ResourceId() As Integer
            Set(value As Integer)
                _ResourceId = value
            End Set
            Get
                Return _ResourceId
            End Get
        End Property
        Public Property Resource() As String
            Set(value As String)
                _Resource = value
            End Set
            Get
                Return _Resource
            End Get
        End Property
        Public Property ResourceTypeId() As Integer
            Set(value As Integer)
                _ResourceTypeId = value
            End Set
            Get
                Return _Resource
            End Get
        End Property
        Public Property TabId() As Integer
            Set(value As Integer)
                _TabId = value
            End Set
            Get
                Return _TabId
            End Get
        End Property
    End Class
End Namespace
