﻿Imports System

Namespace Advantage.Business.Objects
    <Serializable()> Public Class StudentEnrollment
        Private _stuEnrollId As Guid
        Public Overridable Property StuEnrollId As Guid
            Get
                Return Me._stuEnrollId
            End Get
            Set(ByVal value As Guid)
                Me._stuEnrollId = value
            End Set
        End Property

        Private _studentId As Guid
        Public Overridable Property StudentId As Guid
            Get
                Return Me._studentId
            End Get
            Set(ByVal value As Guid)
                Me._studentId = value
            End Set
        End Property

        Private _enrollDate As Date
        Public Overridable Property EnrollDate As Date
            Get
                Return Me._enrollDate
            End Get
            Set(ByVal value As Date)
                Me._enrollDate = value
            End Set
        End Property

        Private _prgVerId As Guid
        Public Overridable Property PrgVerId As Guid
            Get
                Return Me._prgVerId
            End Get
            Set(ByVal value As Guid)
                Me._prgVerId = value
            End Set
        End Property

        Private _startDate As Nullable(Of DateTime)
        Public Overridable Property StartDate As Nullable(Of DateTime)
            Get
                Return Me._startDate
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._startDate = value
            End Set
        End Property

        Private _expStartDate As Nullable(Of DateTime)
        Public Overridable Property ExpStartDate As Nullable(Of DateTime)
            Get
                Return Me._expStartDate
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._expStartDate = value
            End Set
        End Property

        Private _midPtDate As Nullable(Of DateTime)
        Public Overridable Property MidPtDate As Nullable(Of DateTime)
            Get
                Return Me._midPtDate
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._midPtDate = value
            End Set
        End Property

        Private _expGradDate As Nullable(Of DateTime)
        Public Overridable Property ExpGradDate As Nullable(Of DateTime)
            Get
                Return Me._expGradDate
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._expGradDate = value
            End Set
        End Property

        Private _transferDate As Nullable(Of DateTime)
        Public Overridable Property TransferDate As Nullable(Of DateTime)
            Get
                Return Me._transferDate
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._transferDate = value
            End Set
        End Property

        Private _shiftId As System.Nullable(Of System.Guid)
        Public Overridable Property ShiftId As System.Nullable(Of System.Guid)
            Get
                Return Me._shiftId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._shiftId = value
            End Set
        End Property

        Private _billingMethodId As System.Nullable(Of System.Guid)
        Public Overridable Property BillingMethodId As System.Nullable(Of System.Guid)
            Get
                Return Me._billingMethodId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._billingMethodId = value
            End Set
        End Property

        Private _campusId As Guid
        Public Overridable Property CampusId As Guid
            Get
                Return Me._campusId
            End Get
            Set(ByVal value As Guid)
                Me._campusId = value
            End Set
        End Property

        Private _statusCodeId As System.Nullable(Of System.Guid)
        Public Overridable Property StatusCodeId As System.Nullable(Of System.Guid)
            Get
                Return Me._statusCodeId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._statusCodeId = value
            End Set
        End Property

        Private _lDA As Nullable(Of DateTime)
        Public Overridable Property LDA As Nullable(Of DateTime)
            Get
                Return Me._lDA
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._lDA = value
            End Set
        End Property

        Private _modDate As Date
        Public Overridable Property ModDate As Date
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As Date)
                Me._modDate = value
            End Set
        End Property

        Private _modUser As String
        Public Overridable Property ModUser As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property

        Private _enrollmentId As String
        Public Overridable Property EnrollmentId As String
            Get
                Return Me._enrollmentId
            End Get
            Set(ByVal value As String)
                Me._enrollmentId = value
            End Set
        End Property

        Private _admissionsRep As System.Nullable(Of System.Guid)
        Public Overridable Property AdmissionsRep As System.Nullable(Of System.Guid)
            Get
                Return Me._admissionsRep
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._admissionsRep = value
            End Set
        End Property

        Private _academicAdvisor As System.Nullable(Of System.Guid)
        Public Overridable Property AcademicAdvisor As System.Nullable(Of System.Guid)
            Get
                Return Me._academicAdvisor
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._academicAdvisor = value
            End Set
        End Property

        Private _scheduleId As System.Nullable(Of System.Guid)
        Public Overridable Property ScheduleId As System.Nullable(Of System.Guid)
            Get
                Return Me._scheduleId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._scheduleId = value
            End Set
        End Property

        Private _leadId As System.Nullable(Of System.Guid)
        Public Overridable Property LeadId As System.Nullable(Of System.Guid)
            Get
                Return Me._leadId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._leadId = value
            End Set
        End Property

        Private _tuitionCategoryId As System.Nullable(Of System.Guid)
        Public Overridable Property TuitionCategoryId As System.Nullable(Of System.Guid)
            Get
                Return Me._tuitionCategoryId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._tuitionCategoryId = value
            End Set
        End Property

        Private _dropReasonId As System.Nullable(Of System.Guid)
        Public Overridable Property DropReasonId As System.Nullable(Of System.Guid)
            Get
                Return Me._dropReasonId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._dropReasonId = value
            End Set
        End Property

        Private _dateDetermined As Nullable(Of DateTime)
        Public Overridable Property DateDetermined As Nullable(Of DateTime)
            Get
                Return Me._dateDetermined
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._dateDetermined = value
            End Set
        End Property

        Private _edLvlId As System.Nullable(Of System.Guid)
        Public Overridable Property EdLvlId As System.Nullable(Of System.Guid)
            Get
                Return Me._edLvlId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._edLvlId = value
            End Set
        End Property

        Private _fAAdvisorId As System.Nullable(Of System.Guid)
        Public Overridable Property FAAdvisorId As System.Nullable(Of System.Guid)
            Get
                Return Me._fAAdvisorId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._fAAdvisorId = value
            End Set
        End Property

        Private _attendtypeid As System.Nullable(Of System.Guid)
        Public Overridable Property Attendtypeid As System.Nullable(Of System.Guid)
            Get
                Return Me._attendtypeid
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._attendtypeid = value
            End Set
        End Property

        Private _degcertseekingid As System.Nullable(Of System.Guid)
        Public Overridable Property Degcertseekingid As System.Nullable(Of System.Guid)
            Get
                Return Me._degcertseekingid
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._degcertseekingid = value
            End Set
        End Property

        Private _reEnrollmentDate As Nullable(Of DateTime)
        Public Overridable Property ReEnrollmentDate As Nullable(Of DateTime)
            Get
                Return Me._reEnrollmentDate
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._reEnrollmentDate = value
            End Set
        End Property

        Private _badgeNumber As String
        Public Overridable Property BadgeNumber As String
            Get
                Return Me._badgeNumber
            End Get
            Set(ByVal value As String)
                Me._badgeNumber = value
            End Set
        End Property

        Private _cohortStartDate As Nullable(Of DateTime)
        Public Overridable Property CohortStartDate As Nullable(Of DateTime)
            Get
                Return Me._cohortStartDate
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._cohortStartDate = value
            End Set
        End Property

        Private _sAPId As System.Nullable(Of System.Guid)
        Public Overridable Property SAPId As System.Nullable(Of System.Guid)
            Get
                Return Me._sAPId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._sAPId = value
            End Set
        End Property

        Private _contractedGradDate As Nullable(Of DateTime)
        Public Overridable Property ContractedGradDate As Nullable(Of DateTime)
            Get
                Return Me._contractedGradDate
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._contractedGradDate = value
            End Set
        End Property

        Private _transferHours As Decimal
        Public Overridable Property TransferHours As Decimal
            Get
                Return Me._transferHours
            End Get
            Set(ByVal value As Decimal)
                Me._transferHours = value
            End Set
        End Property

        Private _graduatedOrReceivedDate As Nullable(Of DateTime)
        Public Overridable Property GraduatedOrReceivedDate As Nullable(Of DateTime)
            Get
                Return Me._graduatedOrReceivedDate
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._graduatedOrReceivedDate = value
            End Set
        End Property

        Private _Student As Student
        Public Overridable Property Student As Student
            Get
                Return Me._Student
            End Get
            Set(ByVal value As Student)
                Me._Student = value
            End Set
        End Property
    End Class
End Namespace


