﻿Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common

''' <summary>
''' Business Object to Score multiple enrollments that shared the same course.
''' </summary>
''' <remarks></remarks>
Public Class ArRegisterScoreInfo

    Protected Sub New()
    End Sub

#Region "Constructor"
    ''' <summary>
    ''' Constructor. Initialize class and get the necessary enrollments
    ''' </summary>
    ''' <param name="enrollId">This enroll is used to get the studentdId</param>
    ''' <param name="classId">The class that wants to be enrolled the student</param>
    ''' <remarks>Use always this constructor</remarks>
    Sub New(ByVal enrollId As String, ByVal classId As String, Optional ByVal isCourseLevel As Boolean = False)

        Me.ClassId = classId

        ' Create the list of enrollment active that the student has, that contain the classId 
        EnrollmentIdToModifyList = RegFacade.GetActiveEnrollmentWithClassAssociated(enrollId, classId)

        'Get the list of all VALUES already scored in arGrdBkResults
        RecordsInDbList = TermProgressFacade.GetAllScoredRecordsForEnrollmentAndClassId(EnrollmentIdToModifyList, classId, isCourseLevel)


    End Sub
#End Region

#Region "Properties"


    ''' <summary>
    ''' This is TestID in arResults and
    ''' ClsSectionId in arGrdBkResults AND
    ''' ClsSectionId in atClsSectAttendance
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property ClassId() As String

    ''' <summary>
    ''' Determine if the components and the curse is completed graded.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property IsInCompletedGraded As Boolean

    ''' <summary>
    ''' Student ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property StudentId() As String

    ''' <summary>
    ''' Application user
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property UserName As String

    ''' <summary>
    ''' The campus Id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property CampusId As String

    ''' <summary>
    ''' The list of enrollment where the operation is applied.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property EnrollmentIdToModifyList As List(Of String)

    ''' <summary>
    ''' The list of scores in database for the given ClassId
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property RecordsInDbList() As List(Of GrdRecordsPoco)

    ''' <summary>
    ''' Get the list of score in the screen for a given class
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property RecordsInScreenList() As List(Of GrdRecordsPoco)
#End Region

#Region "Internal Fields"

    Dim recordsToDeleteList As List(Of GrdRecordsPoco)
    Dim recordsToUpdateList As List(Of GrdRecordsPoco)
    Dim recordsToInsertList As List(Of GrdRecordsPoco)

#End Region

#Region "Methods Public"

    ''' <summary>
    ''' Enter the new score information in all components from a Course for all enrollments associated with the student
    ''' that shared the course. 
    ''' </summary>
    Public Sub ExecuteScoreUpdate()

        ' Execute for all Enrollment the prepared query in a single transaction.
        CreateListOfOperations()

        'If not operation was ordered terminate.
        If (recordsToDeleteList.Count = 0 And recordsToInsertList.Count = 0 And recordsToUpdateList.Count = 0) Then
            Return
        End If

        'Proceed to execute the ordered operation.
        Dim resultsValues As GrdRecordsPoco = New GrdRecordsPoco()
        resultsValues.ClsSectionId = ClassId
        resultsValues.IsIncomplete = IsInCompletedGraded
        resultsValues.UserName = UserName
        TermProgressFacade.ExecuteScoreUpdates(recordsToDeleteList, recordsToUpdateList, recordsToInsertList, EnrollmentIdToModifyList, resultsValues)
    End Sub

#End Region

#Region "Private Methods"

    ''' <summary>
    ''' Create three list of operations (DELETE UPDATE INSERT)
    ''' this list can be used to generate the queries to Scored the student 
    ''' in all enrollments that are common with the classId.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CreateListOfOperations()
        recordsToDeleteList = New List(Of GrdRecordsPoco)
        recordsToUpdateList = New List(Of GrdRecordsPoco)
        recordsToInsertList = New List(Of GrdRecordsPoco)

        'For each Component in screen see the relation with value in db
        For Each poco As GrdRecordsPoco In RecordsInScreenList

            If (String.IsNullOrEmpty(poco.Comments) And String.IsNullOrEmpty(poco.Score)) Then

                'In this case we need to eliminate the entries that can be exists in DB
                recordsToDeleteList.AddRange(RecordsInDbList.Where(Function(x) x.Descrip = poco.Descrip Or x.InstrGrdBkWgtDetailId = poco.InstrGrdBkWgtDetailId.ToLower() And x.ResNum = poco.ResNum).ToList())

            Else ' A value was enter or exists previously

                ' Ge the list of all records in GradeBook for this component (for the enrollments selected)
                Dim listOfexistsForComponent = RecordsInDbList.Where(Function(x) x.Descrip = poco.Descrip Or ((poco.InstrGrdBkWgtDetailId Is Nothing) OrElse x.InstrGrdBkWgtDetailId = poco.InstrGrdBkWgtDetailId.ToLower()) And x.ResNum = poco.ResNum).ToList()

                'See if the record exists for each enrollment if exists update if not create it for the given component
                For Each id As String In EnrollmentIdToModifyList
                    Dim grdBookRecord = listOfexistsForComponent.FirstOrDefault(Function(x) x.StuEnrollId = id)
                    If grdBookRecord Is Nothing OrElse grdBookRecord.ResNum <> poco.ResNum  Then
                        'Remove grade place holder if exists so duplicates are not created
                        Dim gradePlaceHolder = RecordsInDbList.Where(Function(x) x.InstrGrdBkWgtDetailId = poco.InstrGrdBkWgtDetailId.ToLower() Andalso x.ResNum = 0 AndAlso x.StuEnrollId = id).FirstOrDefault()

                        If(gradePlaceHolder isnot nothing) Then
                            recordsToDeleteList.Add(gradePlaceHolder)
                        End If

                        'Create poco to insert in the insert collection
                        Dim recpoco = New GrdRecordsPoco()
                        recpoco.Comments = poco.Comments
                        recpoco.ResNum = poco.ResNum ' unknown parameter always is 0
                        recpoco.Score = poco.Score
                        recpoco.DateCompleted = poco.DateCompleted
                        recpoco.GrdBkResultId = if ( string.IsNullOrEmpty(recpoco.GrdBkResultId),  Guid.NewGuid(), recpoco.GrdBkResultId.ToString()).ToString()
                        recpoco.StuEnrollId = id
                        recpoco.InstrGrdBkWgtDetailId = poco.InstrGrdBkWgtDetailId
                        recpoco.ClsSectionId = ClassId
                        recpoco.IsIncomplete = IsInCompletedGraded
                        recordsToInsertList.Add(recpoco)
                    Else 
                        grdBookRecord.Score = poco.Score
                        grdBookRecord.ResNum = poco.ResNum
                        grdBookRecord.DateCompleted = poco.DateCompleted
                        grdBookRecord.Comments = poco.Comments
                        grdBookRecord.IsIncomplete = poco.IsIncomplete
                        grdBookRecord.GrdBkResultId = poco.GrdBkResultId
                        recordsToUpdateList.Add(grdBookRecord)
                
                    End If
                Next
            End If
        Next
    End Sub


#End Region

End Class
