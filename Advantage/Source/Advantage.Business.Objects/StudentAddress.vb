﻿Imports System

Namespace Advantage.Business.Objects
    Public Class StudentAddress
        Private _stdAddressId As String
        Public Overridable Property StdAddressId As String
            Get
                Return Me._stdAddressId
            End Get
            Set(ByVal value As String)
                Me._stdAddressId = value
            End Set
        End Property
        Private _studentId As String
        Public Overridable Property StudentId As String
            Get
                Return Me._studentId
            End Get
            Set(ByVal value As String)
                Me._studentId = value
            End Set
        End Property

        Private _address1 As String
        Public Overridable Property Address1 As String
            Get
                Return Me._address1
            End Get
            Set(ByVal value As String)
                Me._address1 = value
            End Set
        End Property

        Private _address2 As String
        Public Overridable Property Address2 As String
            Get
                Return Me._address2
            End Get
            Set(ByVal value As String)
                Me._address2 = value
            End Set
        End Property

        Private _city As String
        Public Overridable Property City As String
            Get
                Return Me._city
            End Get
            Set(ByVal value As String)
                Me._city = value
            End Set
        End Property

        Private _stateId As System.Nullable(Of System.Guid)
        Public Overridable Property StateId As System.Nullable(Of System.Guid)
            Get
                Return Me._stateId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._stateId = value
            End Set
        End Property

        Private _zip As String
        Public Overridable Property Zip As String
            Get
                Return Me._zip
            End Get
            Set(ByVal value As String)
                Me._zip = value
            End Set
        End Property

        Private _countryId As System.Nullable(Of System.Guid)
        Public Overridable Property CountryId As System.Nullable(Of System.Guid)
            Get
                Return Me._countryId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._countryId = value
            End Set
        End Property

        Private _addressTypeId As System.Nullable(Of System.Guid)
        Public Overridable Property AddressTypeId As System.Nullable(Of System.Guid)
            Get
                Return Me._addressTypeId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._addressTypeId = value
            End Set
        End Property

        Private _modUser As String
        Public Overridable Property ModUser As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property

        Private _modDate As Nullable(Of DateTime)
        Public Overridable Property ModDate As Nullable(Of DateTime)
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._modDate = value
            End Set
        End Property

        Private _statusId As System.Nullable(Of System.Guid)
        Public Overridable Property StatusId As System.Nullable(Of System.Guid)
            Get
                Return Me._statusId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._statusId = value
            End Set
        End Property

        Private _default1 As Boolean
        Public Overridable Property Default1 As Boolean
            Get
                Return Me._default1
            End Get
            Set(ByVal value As Boolean)
                Me._default1 = value
            End Set
        End Property

        Private _foreignZip As Boolean
        Public Overridable Property ForeignZip As Boolean
            Get
                Return Me._foreignZip
            End Get
            Set(ByVal value As Boolean)
                Me._foreignZip = value
            End Set
        End Property

        Private _otherState As String
        Public Overridable Property OtherState As String
            Get
                Return Me._otherState
            End Get
            Set(ByVal value As String)
                Me._otherState = value
            End Set
        End Property
        Private _Student As Student
        Public Overridable Property Student As Student
            Get
                Return Me._Student
            End Get
            Set(ByVal value As Student)
                Me._Student = value
            End Set
        End Property
    End Class
End Namespace


