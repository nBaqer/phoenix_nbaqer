﻿Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common.Tables

Public Class TransferGradesBo

    ''' <summary>
    ''' Initialize object TransferGradesBO.
    ''' Here are classified for the situation the enrollment that the user can change.
    ''' </summary>
    ''' <param name="listToTransfer"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal listToTransfer As List(Of TransferRequirement))
        'Initialize Lists
        ListOfRareSituation = New List(Of TransferRequirement)()
        ListOfRegisterCopyOne = New List(Of ArResultsClass)()
        ListReadyToTransferGrade = New List(Of TransferRequirement)()
        ListTransferByCopyOne = New List(Of ArTransferGrades)()


        ' For each value en list get all enrollments that share the term
        For Each transfer As TransferRequirement In listToTransfer
            'Get the student id
            transfer.StudentId = RegFacade.GetStudentIdFromEnrollment(transfer.StuEnrollId)
            'Get the list of enrollments that have the requirement
            Dim listenrollmentsId As List(Of String) = RegFacade.GetAllStudentEnrollmentsWithRequerimentId(transfer.StudentId, transfer.ReqId)

   'Determine if they are register
            Dim listRegistered As List(Of ArResultsClass) = RegFacade.GetRegisterInformation(listenrollmentsId, transfer.ReqId)
            Dim listOfTransferred As List(Of ArTransferGrades) = TransferGradesFacade.GetListOfTransferredEnrollments(listenrollmentsId, transfer.ReqId)


            ' Determine those that has transfer information
            Dim enrollCount = listOfTransferred.Count + listRegistered.Count
            If (listOfTransferred.Count > 0 And listRegistered.Count > 0) Then
                'This should not occurred, but legacy data can be in this situation.
                If (enrollCount >= listenrollmentsId.Count) Then
                    transfer.MessageToWorld = "This course is transferred or graded in all enrollments. should not appear in the list. Please call FAME"
                    ListOfRareSituation.Add(transfer)
                Else
                    ' Take always the transfer values
                    ListTransferByCopyOne.Add(listOfTransferred(0))
                End If
                Continue For
            End If

            'Transfer ONe go to list TransferbyCopyOne (transfer is always graded)
            If listOfTransferred.Count > 0 Then

                If (listOfTransferred.Count = listenrollmentsId.Count) Then

                    transfer.MessageToWorld = "This course is transferred in all enrollments. should not appear in the list. Please call FAME"
                    ListOfRareSituation.Add(transfer)
                    Continue For
                End If

                'If (listOfTransferred.Count > 1) Then
                '    transfer.MessageToWorld = "Some enrollments are missing the transfer grade for this course.  Please review the student transcript pages, in each enrollment, to verify this course is graded"
                '    ListOfRareSituation.Add(transfer)
                '    Continue For
                'End If

                ListTransferByCopyOne.Add(listOfTransferred(0))
                Continue For
            End If

            If listRegistered.Count > 0 Then

                'Analysis rare situations.............
                If listRegistered.Count = listenrollmentsId.Count Then
                    transfer.MessageToWorld = "This course is scheduled in all enrollments. should not appear in the list. Please call FAME"
                    ListOfRareSituation.Add(transfer)
                    Continue For
                End If

                'If (listRegistered.Count > 1) Then
                '    transfer.MessageToWorld = "This course needs to be scheduled in all active enrollments containing this course. Please review the student schedule pages, in each enrollment, to verify this course is scheduled"
                '    ListOfRareSituation.Add(transfer)
                '    Continue For
                'End If

                Dim registered = listRegistered(0)
                If registered.IsCourseCompleted = 0 Then
                    If (registered.IsIncomplete = False) Then
                        'Register but no grade. Nothing to do put in list of rare situation
                        transfer.MessageToWorld = "The following course is currently in progress. Once the course is completed you can apply the transfer grade."
                        ListOfRareSituation.Add(transfer)
                        Continue For
                    End If

                End If

                'Register with grade go to list RegisterCopyOne
                ListOfRegisterCopyOne.Add(registered)
                Continue For
            End If

            'No register or transfer go to list ReadyToTransfer
            ListReadyToTransferGrade.Add(transfer)
        Next
    End Sub


#Region "Properties"

    Public Property ListReadyToTransferGrade() As IList(Of TransferRequirement)
    Public Property ListTransferByCopyOne() As IList(Of ArTransferGrades)
    Public Property ListOfRegisterCopyOne() As IList(Of ArResultsClass)
    Public Property ListOfRareSituation() As IList(Of TransferRequirement)

#End Region

#Region "Static Methods"

    Public Shared Function ValidateUserInput(listToTransfer As List(Of TransferRequirement), isNumeric As Boolean) As String
        Dim errStr As String = String.Empty
        Dim passingScore As Decimal

        For Each transfer As TransferRequirement In listToTransfer

            ' Test if term is selected
            If String.IsNullOrEmpty(transfer.TermId) Then
                errStr &= String.Format("Please select a term for Row {0}{1}", transfer.ReqCode, Environment.NewLine)
            End If

            'numeric 
            If isNumeric Then
                If transfer.Score = 0 And transfer.IsChecked = False Then
                    errStr &= String.Format("Please enter a score/check transfer grade check box for Row {0}{1}", transfer.ReqCode, Environment.NewLine)
                End If

                If transfer.Score <> 0 And transfer.IsChecked Then
                    passingScore = (New StuProgressReportObject).GetMinPassingScore_SP()
                    If transfer.Score < passingScore Then
                        errStr &= String.Format("For transfer grades,please enter a passing score for Row  {0}{1}", transfer.ReqCode, Environment.NewLine)
                    End If
                End If
            Else
                If String.IsNullOrEmpty(transfer.Grade) Then
                    errStr &= String.Format("Please enter a grade for Row  {0}{1}", transfer.ReqCode, Environment.NewLine)
                End If
            End If
        Next

        Return errStr
    End Function

#End Region


    Public Sub TransferRequirements(Optional ByVal overridePostAcademics As Boolean = False)
        If ListReadyToTransferGrade.Count > 0 Then
            Dim listToTransfer As IList(Of ArTransferGrades) = New List(Of ArTransferGrades)()
            For Each req As TransferRequirement In ListReadyToTransferGrade

                'Get all enrollments of the student related to this
                Dim listEnroll As List(Of String) = RegFacade.GetActiveEnrollmentsWithRequerimentId(req.StudentId, req.ReqId, overridePostAcademics)

                'Prepare the list to be inserted
                For Each s As String In listEnroll

                    'Go to insert the values
                    Dim tran As ArTransferGrades = New ArTransferGrades()
                    tran.EnrollmentId = s
                    tran.GradeOverridenBy = req.ModName
                    tran.GradeOverridenDate = DateTime.Now
                    tran.GrdSysDetailId = req.Grade
                    tran.IsClinicsSatisfied = 0
                    tran.IsCourseCompleted = Not (req.Grade Is Nothing And req.Score = 0)

                    tran.IsGradeOverriden = 0
                    tran.IsTransferred = req.IsChecked
                    tran.ModDate = DateTime.Now
                    tran.ModUser = req.ModName
                    tran.ReqId = req.ReqId
                    tran.Score = req.Score
                    tran.TermId = req.TermId
                    listToTransfer.Add(tran)

                Next
            Next

            TransferGradesFacade.SetTransferTable(listToTransfer)
        End If
    End Sub

    ''' <summary>
    ''' Transfer the requirement to the enrollment that haven't it.
    ''' </summary>
    ''' <param name="listTransfer"></param>
    ''' <remarks></remarks>
    Public Shared Sub TransferEnrollmentsByCopyOne(ByVal listTransfer As IList(Of ArTransferGrades))

        If listTransfer.Count > 0 Then
            Dim listToTransfer As IList(Of ArTransferGrades) = New List(Of ArTransferGrades)()
            For Each req As ArTransferGrades In listTransfer

                'Get all enrollments of the student related to this
                Dim studentId As String = RegFacade.GetStudentIdFromEnrollment(req.EnrollmentId)
                Dim listEnroll As List(Of String) = RegFacade.GetActiveEnrollmentsWithRequerimentId(studentId, req.ReqId)


                'Prepare the list to be inserted
                For Each s As String In listEnroll
                    'eliminate the enrollment that is transferred
                    If s = req.EnrollmentId Then
                        Continue For
                    End If

                    ' In the case of more than 2 enrollments, check if the enrollment is already register
                    If listTransfer.Count() > 2 Then
                        ' check if the enrollment is registered
                        Dim isRegister As Boolean = TransferGradesFacade.IsEnrollmentTransferredForTheReq(s, req.ReqId)
                        If isRegister Then
                            'It is already register
                            Continue For
                        End If
                    End If

                    'Go to insert the values
                    Dim tran As ArTransferGrades = New ArTransferGrades()
                    tran.EnrollmentId = s
                    tran.GradeOverridenBy = req.ModUser
                    tran.GradeOverridenDate = DateTime.Now
                    tran.GrdSysDetailId = req.GrdSysDetailId
                    tran.IsClinicsSatisfied = req.IsClinicsSatisfied
                    tran.IsCourseCompleted = (req.GrdSysDetailId Is Nothing And req.Score = 0)
                    tran.IsCourseCompleted = Not tran.IsCourseCompleted
                    tran.IsGradeOverriden = req.IsGradeOverriden
                    tran.IsTransferred = req.IsTransferred
                    tran.ModDate = DateTime.Now
                    tran.ModUser = req.ModUser
                    tran.ReqId = req.ReqId
                    tran.Score = req.Score
                    tran.TermId = req.TermId
                    listToTransfer.Add(tran)
                Next
            Next
            TransferGradesFacade.SetTransferTable(listToTransfer)
        End If
    End Sub

    ''' <summary>
    ''' Register the other enrollments that are not registered and graded
    ''' </summary>
    ''' <param name="listRegister"></param>
    ''' <remarks></remarks>
    Public Shared Sub RegisterEnrollmentsByCopyOne(ByVal listRegister As IList(Of ArResultsClass))
        If listRegister.Count > 0 Then
            Dim listToRegister As IList(Of CorrectionRegInfo) = New List(Of CorrectionRegInfo)()

            For Each req As ArResultsClass In listRegister
                'Get the registration information
                Dim inforegister As CorrectionRegInfo = CorrectionRegInfo.Factory()
                inforegister = RegFacade.GetAllAcademicInformationForOneClass(inforegister, req)
                
                'Get all enrollments of the student related to this
                Dim listEnroll As List(Of String) = RegFacade.GetActiveEnrollmentsWithClassAssociatedForRegisterClass(req.EnrollmentId, req.TestId)

                'Prepare the list to be inserted
                For Each s As String In listEnroll
                    'eliminate the enrollment that is transferred
                    If s = req.EnrollmentId Then
                        Continue For
                    End If

                    ' In the case of more than 2 enrollments, check if the enrollment is already register
                    If listEnroll.Count() > 2 Then
                        ' check if the enrollment is registered
                        Dim result As String = RegFacade.CheckIfEnrollmentIsAlreadyRegister(s, req.TestId)
                        If Not String.IsNullOrEmpty(result) Then
                            'It is already register
                            Continue For
                        End If
                    End If

                    'Create the CorrectionRegInfo to be used
                    Dim insertvalues As CorrectionRegInfo = CorrectionRegInfo.Factory()

                    'Go to insert the values
                    Dim res As ArResultsClass = ObjectCopier.Clone(req)
                    res.EnrollmentId = s
                    insertvalues.ArInfoList.Add(res)

                    'Clone attendance
                    For Each att As AtClsSectAttendance In inforegister.AttendanceList
                        Dim attendance As AtClsSectAttendance  = ObjectCopier.Clone(att)
                        attendance.StuEnrollId = s
                        insertvalues.AttendanceList.Add(attendance)
                    Next

                    'Clone Grade book
                    For Each info As GradeBookResultInfo In inforegister.GradeBookList
                        Dim gb As GradeBookResultInfo = ObjectCopier.Clone(info)
                        If (Not string.IsNullOrWhiteSpace(s))
                            gb.StuEnrollId = Guid.Parse(s)
                        End If
                        insertvalues.GradeBookList.Add(gb)
                    Next

                    listToRegister.Add(insertvalues)
                Next

            Next

            RegFacade.InsertAllcademicInformationInDataBase(listToRegister)

        End If
    End Sub
End Class
