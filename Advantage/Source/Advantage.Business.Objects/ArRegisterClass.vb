﻿
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade

Public Class ArRegisterClass

    ''' <summary>
    ''' Constructor. Initialize class and get the necessary enrollments
    ''' </summary>
    ''' <param name="enrollId">This enroll is used to get the studentdId</param>
    ''' <param name="classId">The class that wants to be enrolled the student</param>
    ''' <remarks>Use always this constructor</remarks>
    Sub New(ByVal enrollId As String, ByVal classId As String)

        Me.ClassId = classId

        'Initialize the list of co-requirements
        RegisterCoRequirementList = New List(Of RegisterCoRequirement)()

        'Initialize the list of warning and error messages
        InformationMessagesList = New List(Of String)()

        ' Create the list of enrollment active that the student has, that contain the classId 
        EnrollmentIdToModify = RegFacade.GetActiveEnrollmentsWithClassAssociatedForRegisterClass(enrollId, classId)

        'Initialize the Validate Enrollment class registration list
        ValidatedEnrollmentClassRegistrationList = New List(Of RegistrationClassInfo)()

    End Sub


#Region "Properties"
    ''' <summary>
    ''' This is TestID in arResults and
    ''' ClsSectionId in arGrdBkResults AND
    ''' ClsSectionId in atClsSectAttendance
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property ClassId() As String

    ''' <summary>
    ''' Student ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property StudentId() As String

    ''' <summary>
    ''' Application user
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property UserName As String

    Property CampusId As String

    ''' <summary>
    ''' The list of enrollment where the operation is applied.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property EnrollmentIdToModify As List(Of String)

    ''' <summary>
    ''' List of Enrollment validated to modify
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property ValidatedEnrollmentClassRegistrationList() As List(Of RegistrationClassInfo)

    ''' <summary>
    ''' List of messages with the warning or error 
    ''' of the last Validate Operation
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property InformationMessagesList() As List(Of String)

    ''' <summary>
    ''' Store the co-requirement if any
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property RegisterCoRequirementList() As List(Of RegisterCoRequirement)


    Property MajorMinor As Boolean

#End Region

#Region "Methods Public"

    Public Sub UnRegisteredClassWidthEnrollments()

        'Unregister Class for all student enrollments and also co-classes
        RegFacade.UnRegisterClassInEnrollmentsOfStudent(ValidatedEnrollmentClassRegistrationList)
    End Sub

    Public Sub RegisterClassWithEnrollments()

        ' Try To register the class in all enrollments 
        RegFacade.RegisterClassInEnrollmentsOfStudents(ValidatedEnrollmentClassRegistrationList)

    End Sub
   
    Public Function ValidateRegistrationDelete() As Boolean

        ' Clear list of error
        InformationMessagesList.Clear()
        ValidatedEnrollmentClassRegistrationList = New List(Of RegistrationClassInfo)()
        Dim facade As New RegFacade

        ' Check if the enrollment has register the operation
        For Each stuEnrollId As String In EnrollmentIdToModify

            Dim response As String = facade.DoesStdHaveFinalGrade(stuEnrollId, ClassId)
            If (response = "GradeExists") Then
                InformationMessagesList.Add(String.Format("{0} - {1}", response, stuEnrollId)) 'Message Grade Exists
            Else
                Dim registration As RegistrationClassInfo = New RegistrationClassInfo(String.Empty, ClassId, stuEnrollId)
                registration.CampusId = CampusId
                registration.UserName = UserName
                ValidatedEnrollmentClassRegistrationList.Add(registration) 'Verified that has not final grade
            End If
        Next

        'Verify Co requirement list.....
        Dim temp = New List(Of RegistrationClassInfo)()
        For Each co As RegisterCoRequirement In RegisterCoRequirementList

            For Each info As RegistrationClassInfo In ValidatedEnrollmentClassRegistrationList
                If (co.StuEnrollmentId = info.StuEnrollmentId) Then
                    ' Validate the relation
                    Dim response As String = facade.DoesStdHaveFinalGrade(co.StuEnrollmentId, co.ClassId)
                    If (response = "GradeExists") Then
                        InformationMessagesList.Add(String.Format("{0} - {1}", response, co.StuEnrollmentId)) 'Message Grade Exists
                    Else
                        Dim registration As RegistrationClassInfo = New RegistrationClassInfo(String.Empty, co.ClassId, co.StuEnrollmentId)
                        registration.CampusId = CampusId
                        registration.UserName = UserName
                        temp.Add(registration) 'Verified that has not final grade
                    End If
                End If
            Next
        Next

        ' Pass the possible co requirement to be added to the list 
        For Each info As RegistrationClassInfo In temp
            ValidatedEnrollmentClassRegistrationList.Add(info)
        Next

        Return (ValidatedEnrollmentClassRegistrationList.Count > 0)
    End Function

    ''' <summary>
    ''' Validate if all enrollment in the list are registered
    ''' </summary>
    ''' <returns>
    ''' True no all enrollments were registered
    ''' False if all enrollments were registered
    ''' </returns>
    Public Function ValidateRegistrationAdd() As Boolean

        ' Clear list of error
        InformationMessagesList.Clear()
        ValidatedEnrollmentClassRegistrationList = New List(Of RegistrationClassInfo)()

        ' Check if the enrollment has register the operation
        For Each stuEnrollId As String In EnrollmentIdToModify
            Dim response As String = RegFacade.CheckIfEnrollmentIsAlreadyRegister(stuEnrollId, ClassId)
            If Not (String.IsNullOrEmpty(response)) Then
                InformationMessagesList.Add(String.Format("{0} - {1}", response, stuEnrollId)) 'Message is already registered
            Else
                Dim registration As RegistrationClassInfo = New RegistrationClassInfo(String.Empty, ClassId, stuEnrollId)
                registration.CampusId = CampusId
                registration.UserName = UserName
                ValidatedEnrollmentClassRegistrationList.Add(registration) 'Verified that is not registered in the enrollment the class.
            End If
        Next

        'Verify Co requirement list.....
        Dim temp = New List(Of RegistrationClassInfo)()
        For Each co As RegisterCoRequirement In RegisterCoRequirementList

            For Each info As RegistrationClassInfo In ValidatedEnrollmentClassRegistrationList
                If (co.StuEnrollmentId = info.StuEnrollmentId) Then
                    ' Validate the relation
                    Dim response As String = RegFacade.CheckIfEnrollmentIsAlreadyRegister(co.StuEnrollmentId, co.ClassId)
                    If Not (String.IsNullOrEmpty(response)) Then
                        InformationMessagesList.Add(String.Format("{0} - {1}", response, co.StuEnrollmentId)) 'Message is already registered
                    Else
                        Dim registration As RegistrationClassInfo = New RegistrationClassInfo(String.Empty, co.ClassId, co.StuEnrollmentId)
                        registration.CampusId = CampusId
                        registration.UserName = UserName
                        temp.Add(registration) 'Verified that is not registered in the enrollment the class.
                    End If
                End If
            Next
        Next

        ' Pass the possible co requirement to be added to the list 
        For Each info As RegistrationClassInfo In temp
            ValidatedEnrollmentClassRegistrationList.Add(info)
        Next

        Return (ValidatedEnrollmentClassRegistrationList.Count > 0)
    End Function

    ''' <summary>
    ''' Validate if all enrollment in the list are registered
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValidateRegistrationAddSingleEnrollment(ByVal stuEnrollmentId As String) As Boolean

       ' Check if the enrollment has register the operation

        Dim response As String = RegFacade.CheckIfEnrollmentIsAlreadyRegister(stuEnrollmentId, ClassId)
        Return (String.IsNullOrEmpty(response))
    End Function


#End Region

End Class
