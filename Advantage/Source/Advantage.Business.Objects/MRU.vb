﻿Namespace Advantage.Business.Objects

    <Serializable()>
    <CLSCompliant(True)>
    Public MustInherit Class MRU

        Public Enum MRUType
            [Students] = 1
            [Employers] = 2
            [Employees] = 3
            [Leads] = 4
        End Enum

        'MRUType
        Public Property Id As String

        Public Property Name As String

        Public Property Counter As Integer

        Public Property ChildId As Guid

        Public Property Type As Integer

        Public Property UserId As Guid

        Public Property CampusId As Guid

        Public Property CampusName As String

        Public Property SortOrder As Integer

        Public Property IsSticky As Boolean

        Public Property ProgramVersionDesc As String

        Public Property EnrollmentStatus As String

        'Public Sub New(ByVal MRU_Id As Guid, ByVal MRU_Name As String, _
        '               ByVal MRUCounter As Integer, ByVal Child_Id As Guid, _
        '               ByVal MType As Integer, ByVal User_Id As Guid, _
        '               ByVal CampId As Guid, ByVal Sort_Order As Integer, _
        '               ByVal Is_Sticky As Boolean)
        '    Id = MRU_Id
        '    Name = MRU_Name
        '    Counter = MRUCounter
        '    ChildId = Child_Id
        '    Type = MType
        '    UserId = User_Id
        '    CampusId = CampId
        '    Sort_Order = Sort_Order
        '    IsSticky = Is_Sticky
        'End Sub
    End Class

    <Serializable()>
    <CLSCompliant(True)>
    Public Class StudentMRU
        Inherits MRU
        Public Property StudentId As Guid

        ''' <summary>
        ''' This lead is the associated lead with 
        ''' StudentId. The relation is in Lead Table
        ''' </summary>
        Public Property ShadowLead As Guid

        Public Property StudentNumber As String

        Public Property LeadId As Guid

        Public Property MRUCampusId As String

    End Class

    <Serializable()>
    <CLSCompliant(True)>
    Public Class LeadMRU
        Inherits MRU
        Public Property LeadId As Guid
        Public Property StudentId As Guid
    End Class

    <Serializable()>
    <CLSCompliant(True)>
    Public Class EmployerMRU
        Inherits MRU
        Public Property EmployerId As Guid
    End Class

    <Serializable()>
    <CLSCompliant(True)>
    Public Class EmployeeMRU
        Inherits MRU
        Public Property EmployeeId As Guid
    End Class
End Namespace

