﻿Namespace Advantage.Business.Objects

    <Serializable()>
    <CLSCompliant(True)>
    Public Class NavigationObjects
        Public Sub New
            DisplaySequence = 0
            ResourceTypeId = 0
            ModuleSortOrder = 0
        End Sub

        Public Property ModuleResourceId As Integer

        Public Property ChildResourceId As Integer

        Public Property DisplaySequence As Integer

        Public Property ChildResourceURL As String

        Public Property ChildResourceDescription As String

        Public Property ParentResourceId As Integer

        Public Property ResourceTypeId As Integer

        Public Property EnableMenu As Boolean

        Public Property ModuleSortOrder As Integer

        Public Property ParentResource As String
    End Class

    <Serializable()>
    <CLSCompliant(True)>
    Public Class CommonTaskMenuItem
        Inherits NavigationObjects
        Public Property TabId As Integer
    End Class
End Namespace
