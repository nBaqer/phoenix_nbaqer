﻿
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common

Public Class ArFinalGradeClass


#Region "Constructor"

    Sub New(studentInfoInScreenToProcess As List(Of GrdRecordsPoco), classId As String)

        PerStudentsInfoInScreen = studentInfoInScreenToProcess
        Me.ClassId = classId
    End Sub

#End Region


#Region "Properties"

    ''' <summary>
    ''' This is TestID in arResults and
    ''' ClsSectionId in arGrdBkResults AND
    ''' ClsSectionId in atClsSectAttendance
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property ClassId() As String


    ''' <summary>
    ''' The list of enrollment where the operation is applied.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property EnrollmentIdToModifyList As List(Of String)

    ''' <summary>
    ''' Get the list of score in the screen for a given class
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property PerStudentsInfoInScreen() As List(Of GrdRecordsPoco)

    Property ListOfUpdateOperationsFormatNumeric() As List(Of GrdRecordsPoco)

    Property ListOfUpdateOperationsFormatNotNumeric() As List(Of GrdRecordsPoco)




#End Region


#Region "Public Methods"


    Public Sub ExecuteOperations()

        'Create the list of operation
        CreateListOfOperationsPerStudents()
        'Execute The list of operation transactional
        PostFinalGrdsFacade.UpdateFinalGradePerEnrollments(ListOfUpdateOperationsFormatNumeric, ListOfUpdateOperationsFormatNotNumeric)


    End Sub


#End Region

    Private Sub CreateListOfOperationsPerStudents()

        'Create the output lists
        ListOfUpdateOperationsFormatNumeric = New List(Of GrdRecordsPoco)()
        ListOfUpdateOperationsFormatNotNumeric = New List(Of GrdRecordsPoco)()

        'For each record in PerStudentInfoInScreen
        For Each poco As GrdRecordsPoco In PerStudentsInfoInScreen

            'Get the list of enrollment to work with.
            ' Create the list of enrollment active that the student has, that contain the classId 
            EnrollmentIdToModifyList = RegFacade.GetActiveEnrollmentWithClassAssociated(poco.StuEnrollId, ClassId)
            For Each enrollId As String In EnrollmentIdToModifyList
                Dim operation As GrdRecordsPoco = New GrdRecordsPoco()
                operation.StuEnrollId = enrollId
                operation.IsIncomplete = False
                operation.UserName = poco.UserName
                operation.ClsSectionId = poco.ClsSectionId
                operation.DateCompleted = poco.DateCompleted
                If (poco.IsFormatNumeric) Then
                    operation.Score = poco.Score
                    ListOfUpdateOperationsFormatNumeric.Add(operation)
                Else
                    operation.Grade = poco.Grade
                    ListOfUpdateOperationsFormatNotNumeric.Add(operation)
                End If
            Next
        Next
    End Sub

End Class
