﻿Imports FAME.AdvantageV1.Common

Public Class ArGradeStudentsByComponent
   
    Sub New(studentInfoInScreenToProcess As List(Of GrdRecordsPoco), classId As String)

        PerStudentsInfoInScreen = studentInfoInScreenToProcess
        Me.ClassID = classId
    End Sub

#Region "Properties"
    ''' <summary>
    ''' This list has the score info for one component and the same classID for all student
    ''' each row is for a different student
    ''' the student id identify by his/her student enrollment Id.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property PerStudentsInfoInScreen() As List(Of GrdRecordsPoco)

    ''' <summary>
    ''' Class Id to be scored
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property ClassID() As String

#End Region


    Sub ProcessScoreInformation(Optional ByVal isCourseLevel As Boolean = False)

        For Each poco As GrdRecordsPoco In PerStudentsInfoInScreen
            Dim business = New ArRegisterScoreInfo(poco.StuEnrollId, poco.ClsSectionId, isCourseLevel)
            business.IsInCompletedGraded = poco.IsIncomplete
            business.UserName = poco.UserName
            business.RecordsInScreenList = New List(Of GrdRecordsPoco)()
            business.RecordsInScreenList.Add(poco)
            business.ExecuteScoreUpdate()
        Next
    End Sub

End Class
