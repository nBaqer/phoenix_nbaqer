﻿Imports System
Imports System.Data
Imports System.Collections.Generic

Namespace Advantage.Business.Objects
    <Serializable()> _
    Public Class User
        'Private _userstate As IUserState
        ''Public Sub New(ByVal baseState As IUserState)
        ''    _userstate = baseState
        ''End Sub
        'Public Function Status() As UserStatus
        '    Return _userstate.Status
        'End Function
        Public Overridable Property UserName As String

        Public Overridable Property UserId As Guid

        Private _showDefaultCampus As Boolean

        Public Overridable Property ShowDefaultCampus As Boolean
            Get
                Return Me._showDefaultCampus
            End Get
            Set(ByVal value As Boolean)
                Me._showDefaultCampus = value
            End Set
        End Property

        Private _salt As String

        Public Overridable Property Salt As String
            Get
                Return Me._salt
            End Get
            Set(ByVal value As String)
                Me._salt = value
            End Set
        End Property

        Private _password As String

        Public Overridable Property Password As String
            Get
                Return Me._password
            End Get
            Set(ByVal value As String)
                Me._password = value
            End Set
        End Property

        Private _modUser As String

        Public Overridable Property ModUser As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property

        Private _moduleId As Short

        Public Overridable Property ModuleId As Short
            Get
                Return Me._moduleId
            End Get
            Set(ByVal value As Short)
                Me._moduleId = value
            End Set
        End Property

        Private _modDate As Date?

        Public Overridable Property ModDate As Date?
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As Date?)
                Me._modDate = value
            End Set
        End Property

        Private _isLoggedIn As Boolean?

        Public Overridable Property IsLoggedIn As Boolean?
            Get
                Return Me._isLoggedIn
            End Get
            Set(ByVal value As Boolean?)
                Me._isLoggedIn = value
            End Set
        End Property

        Private _isDefaultAdminRep As Boolean?

        Public Overridable Property IsDefaultAdminRep As Boolean?
            Get
                Return Me._isDefaultAdminRep
            End Get
            Set(ByVal value As Boolean?)
                Me._isDefaultAdminRep = value
            End Set
        End Property

        Private _isAdvantageSuperUser As Boolean

        Public Overridable Property IsAdvantageSuperUser As Boolean
            Get
                Return Me._isAdvantageSuperUser
            End Get
            Set(ByVal value As Boolean)
                Me._isAdvantageSuperUser = value
            End Set
        End Property

        Private _fullName As String

        Public Overridable Property FullName As String
            Get
                Return Me._fullName
            End Get
            Set(ByVal value As String)
                Me._fullName = value
            End Set
        End Property

        Private _email As String

        Public Overridable Property Email As String
            Get
                Return Me._email
            End Get
            Set(ByVal value As String)
                Me._email = value
            End Set
        End Property

        Private _confirmPassword As String

        Public Overridable Property ConfirmPassword As String
            Get
                Return Me._confirmPassword
            End Get
            Set(ByVal value As String)
                Me._confirmPassword = value
            End Set
        End Property

        Private _campusId As Guid

        Public Overridable Property CampusId As Guid
            Get
                Return Me._campusId
            End Get
            Set(ByVal value As Guid)
                Me._campusId = value
            End Set
        End Property

        Private _accountActive As Boolean?

        Public Overridable Property AccountActive As Boolean?
            Get
                Return Me._accountActive
            End Get
            Set(ByVal value As Boolean?)
                Me._accountActive = value
            End Set
        End Property

        Private _Campus As Campus

        Public Overridable Property Campus As Campus
            Get
                Return Me._Campus
            End Get
            Set(ByVal value As Campus)
                Me._Campus = value
            End Set
        End Property

        Private _UsersRolesCampGrps As IList(Of UserRolesCampusGroup) = New List(Of UserRolesCampusGroup)

        ''' <summary>
        ''' This Property gives access to user's roles and campus group the user belongs to
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overridable Property UserRolesCampusGroups As IList(Of UserRolesCampusGroup)
            Get
                Return Me._UsersRolesCampGrps
            End Get
            Set(ByVal value As IList(Of UserRolesCampusGroup))
                Me._UsersRolesCampGrps = value
            End Set
        End Property

        Private _RedirectURL As String

        Public Overridable Property RedirectURL As String
            Get
                Return Me._RedirectURL
            End Get
            Set(ByVal value As String)
                Me._RedirectURL = value
            End Set
        End Property

        Private _ReturnMessage As String

        Public Overridable Property ReturnMessage As String
            Get
                Return Me._ReturnMessage
            End Get
            Set(ByVal value As String)
                Me._ReturnMessage = value
            End Set
        End Property

        Private _UserRoles As IList(Of PageLevelPermission) = New List(Of PageLevelPermission)

        Public Overridable Property UserRoles As IList(Of PageLevelPermission)
            Get
                Return Me._UserRoles
            End Get
            Set(ByVal value As IList(Of PageLevelPermission))
                Me._UserRoles = value
            End Set
        End Property

        Private _UserCampuses As IList(Of Campus) = New List(Of Campus)

        Public Overridable Property UserCampuses As IList(Of Campus)
            Get
                Return Me._UserCampuses
            End Get
            Set(ByVal value As IList(Of Campus))
                Me._UserCampuses = value
            End Set
        End Property

        Private _Resource As Short

        Public Overridable Property ResourceId As Short
            Get
                Return Me._Resource
            End Get
            Set(ByVal value As Short)
                Me._Resource = value
            End Set
        End Property

        Private _ModuleCode As String

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overridable Property ModuleCode As String
            Get
                Return Me._ModuleCode
            End Get
            Set(ByVal value As String)
                Me._ModuleCode = value
            End Set
        End Property

        Private _IsLoginSuccessful As Boolean

        ''' <summary>
        ''' This boolean property can be used to determine if the user was able to login in to Advantage
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overridable Property IsLoginSuccessful As Boolean
            Get
                Return Me._IsLoginSuccessful
            End Get
            Set(ByVal value As Boolean)
                Me._IsLoginSuccessful = value
            End Set
        End Property

        Private _IsUserSA As Boolean = False
        Private _IsUserSupport As Boolean = False
        Private _IsUserSuper As Boolean = False

        Public Overridable Property IsUserSA As Boolean
            Get
                Return Me._IsUserSA
            End Get
            Set(ByVal value As Boolean)
                Me._IsUserSA = value
            End Set
        End Property

        Public Overridable Property IsUserSupport As Boolean
            Get
                Return Me._IsUserSupport
            End Get
            Set(ByVal value As Boolean)
                Me._IsUserSupport = value
            End Set
        End Property

        Public Overridable Property IsUserSuper As Boolean
            Get
                Return Me._IsUserSuper
            End Get
            Set(ByVal value As Boolean)
                Me._IsUserSuper = value
            End Set
        End Property

        Private _FullPermission As Boolean = False
        Private _EditPersmission As Boolean = False
        Private _CreatePermission As Boolean = False
        Private _DeletePermission As Boolean = False
        Private _InsufficientPermission As Boolean = False

        ''' <summary>
        ''' All Advantage Special Users -  sa, support and superuser have full access to the application
        ''' Developers can call this property to check if user has full access to the application
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overridable Property FullPermission() As Boolean
            Get
                Return Me._FullPermission
            End Get
            Set(ByVal value As Boolean)
                Me._FullPermission = value
            End Set
        End Property

        Public Overridable Property EditPersmission() As Boolean
            Get
                Return Me._EditPersmission
            End Get
            Set(ByVal value As Boolean)
                Me._EditPersmission = value
            End Set
        End Property

        Public Overridable Property CreatePersmission() As Boolean
            Get
                Return Me._CreatePermission
            End Get
            Set(ByVal value As Boolean)
                Me._CreatePermission = value
            End Set
        End Property

        Public Overridable Property DeletePersmission() As Boolean
            Get
                Return Me._DeletePermission
            End Get
            Set(ByVal value As Boolean)
                Me._DeletePermission = value
            End Set
        End Property

        Public Overridable Property InsufficientPermission() As Boolean
            Get
                Return Me._InsufficientPermission
            End Get
            Set(ByVal value As Boolean)
                Me._InsufficientPermission = value
            End Set
        End Property
    End Class

    Public Enum UserStatus
        Success = 0
        Fail_InvalidCredentials = 1
        Fail_InactiveUser = 2
        Fail_InsufficientRoles = 3
        ''Added to verify if the logged in user has rights to any active campus
        ''The user should have rights to any one of the active campuses
        ''DE 7669
        Fail_InactiveCampus = 4
    End Enum

    Public Enum Specialuser
        sa
        support
    End Enum

    <Serializable()>
    Public Class CampusGroup
        Private _statusId As Guid
        Public Overridable Property StatusId As Guid
            Get
                Return Me._statusId
            End Get
            Set(ByVal value As Guid)
                Me._statusId = value
            End Set
        End Property

        Private _modUser As String
        Public Overridable Property ModUser As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property

        Private _modDate As Date
        Public Overridable Property ModDate As Date
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As Date)
                Me._modDate = value
            End Set
        End Property

        Private _isAllCampusGrp As Boolean?
        Public Overridable Property IsAllCampusGrp As Boolean?
            Get
                Return Me._isAllCampusGrp
            End Get
            Set(ByVal value As Boolean?)
                Me._isAllCampusGrp = value
            End Set
        End Property

        Private _campusId As System.Nullable(Of System.Guid)
        Public Overridable Property CampusId As System.Nullable(Of System.Guid)
            Get
                Return Me._campusId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._campusId = value
            End Set
        End Property

        Private _campGrpId As Guid
        Public Overridable Property CampGrpId As Guid
            Get
                Return Me._campGrpId
            End Get
            Set(ByVal value As Guid)
                Me._campGrpId = value
            End Set
        End Property

        Private _campGrpDescrip As String
        Public Overridable Property CampGrpDescrip As String
            Get
                Return Me._campGrpDescrip
            End Get
            Set(ByVal value As String)
                Me._campGrpDescrip = value
            End Set
        End Property

        Private _campGrpCode As String
        Public Overridable Property CampGrpCode As String
            Get
                Return Me._campGrpCode
            End Get
            Set(ByVal value As String)
                Me._campGrpCode = value
            End Set
        End Property

        Private _campus As Campus
        Public Overridable Property Campus As Campus
            Get
                Return Me._campus
            End Get
            Set(ByVal value As Campus)
                Me._campus = value
            End Set
        End Property
        Private _UserRolesCampusGroups As IList(Of UserRolesCampusGroup) = New List(Of UserRolesCampusGroup)
        Public Overridable ReadOnly Property UserRolesCampusGroups As IList(Of UserRolesCampusGroup)
            Get
                Return Me._UserRolesCampusGroups
            End Get
        End Property
        Private _CampusAndCampusGroups As IList(Of CampusAndCampusGroup) = New List(Of CampusAndCampusGroup)
        Public Overridable ReadOnly Property CampusAndCampusGroups As IList(Of CampusAndCampusGroup)
            Get
                Return Me._CampusAndCampusGroups
            End Get
        End Property

        Function InsertCampGroup(ByVal strGuid As String, ByVal Code As String, ByVal sStatusId As String, ByVal Description As String, ByVal p5 As Object) As String
            Throw New NotImplementedException
        End Function

        Function InsertCampusCampGrp(ByVal strGuid As String, ByVal ID As String, ByVal p3 As Object) As Integer
            Throw New NotImplementedException
        End Function

        Function UpdateCampGroup(ByVal Code As String, ByVal sStatusId As String, ByVal Description As String, ByVal storedGuid As String, ByVal p5 As Object) As String
            Throw New NotImplementedException
        End Function

        Sub UpdateRowAdded(ByVal storedGuid As String, ByVal iCampusId As String, ByVal p3 As Object)
            Throw New NotImplementedException
        End Sub

        Sub UpdateRowDeleted(ByVal storedGuid As String, ByVal iCampusId As String)
            Throw New NotImplementedException
        End Sub

        Function DeleteCampGroup(ByVal sCampgrpId As String) As String
            Throw New NotImplementedException
        End Function

        Function GetCampusesOnItemCmd(ByVal strGUID As String) As DataSet
            Throw New NotImplementedException
        End Function

        Function GetCampuses() As DataSet
            Throw New NotImplementedException
        End Function

    End Class

    <Serializable()>
    Public Class UserRolesCampusGroup

        Private _userRoleCampGrpId As Guid
        Public Overridable Property UserRoleCampGrpId As Guid
            Get
                Return Me._userRoleCampGrpId
            End Get
            Set(ByVal value As Guid)
                Me._userRoleCampGrpId = value
            End Set
        End Property

        Private _userId As Guid
        Public Overridable Property UserId As Guid
            Get
                Return Me._userId
            End Get
            Set(ByVal value As Guid)
                Me._userId = value
            End Set
        End Property

        Private _roleId As System.Nullable(Of System.Guid)
        Public Overridable Property RoleId As System.Nullable(Of System.Guid)
            Get
                Return Me._roleId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._roleId = value
            End Set
        End Property

        Private _modUser As String
        Private _CampgrpDescrip As String
        Public Overridable Property ModUser As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property
        Public Overridable Property CampGrpDescrip As String
            Get
                Return Me._CampgrpDescrip
            End Get
            Set(ByVal value As String)
                Me._CampgrpDescrip = value
            End Set
        End Property
        Private _modDate As Date?
        Public Overridable Property ModDate As Date?
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As Date?)
                Me._modDate = value
            End Set
        End Property

        Private _campGrpId As System.Nullable(Of System.Guid)
        Public Overridable Property CampGrpId As System.Nullable(Of System.Guid)
            Get
                Return Me._campGrpId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._campGrpId = value
            End Set
        End Property

        Private _CampusGroup As CampusGroup
        Public Overridable Property CampusGroup As CampusGroup
            Get
                Return Me._CampusGroup
            End Get
            Set(ByVal value As CampusGroup)
                Me._CampusGroup = value
            End Set
        End Property

        Private _syUser As User
        Public Overridable Property SyUser As User
            Get
                Return Me._syUser
            End Get
            Set(ByVal value As User)
                Me._syUser = value
            End Set
        End Property

        Private _AccessLevel As Integer
        Public Overridable Property AccessLevel() As Integer
            Get
                Return Me._AccessLevel
            End Get
            Set(ByVal value As Integer)
                Me._AccessLevel = value
            End Set
        End Property
        Private _RolesResourcesAndAccessLevel As IList(Of PageLevelPermission) = New List(Of PageLevelPermission)
        ''' <summary>
        ''' This property holds the details regarding the level of access (permission) a user has on a page
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overridable Property UserRoles As IList(Of PageLevelPermission)
            Get
                Return Me._RolesResourcesAndAccessLevel
            End Get
            Set(ByVal value As IList(Of PageLevelPermission))
                Me._RolesResourcesAndAccessLevel = value
            End Set
        End Property

        Private _UserCampuses As IList(Of CampusAndCampusGroup) = New List(Of CampusAndCampusGroup)
        Public Overridable Property UserCampuses As IList(Of CampusAndCampusGroup)
            Get
                Return Me._UserCampuses
            End Get
            Set(ByVal value As IList(Of CampusAndCampusGroup))
                Me._UserCampuses = value
            End Set
        End Property
    End Class

    <Serializable()>
    Public Class Modules

        Private _resourceModuleId As Short?
        Public Overridable Property Resource_ModuleId As Short?
            Get
                Return Me._resourceModuleId
            End Get
            Set(ByVal value As Short?)
                Me._resourceModuleId = value
            End Set
        End Property

        Private _modUser As String
        Public Overridable Property ModUser As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property

        Private _moduleName As String
        Public Overridable Property ModuleName As String
            Get
                Return Me._moduleName
            End Get
            Set(ByVal value As String)
                Me._moduleName = value
            End Set
        End Property

        Private _moduleID As Byte
        Public Overridable Property ModuleID As Byte
            Get
                Return Me._moduleID
            End Get
            Set(ByVal value As Byte)
                Me._moduleID = value
            End Set
        End Property

        Private _moduleCode As String
        Public Overridable Property ModuleCode As String
            Get
                Return Me._moduleCode
            End Get
            Set(ByVal value As String)
                Me._moduleCode = value
            End Set
        End Property

        Private _modDate As Date?
        Public Overridable Property ModDate As Date?
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As Date?)
                Me._modDate = value
            End Set
        End Property

        Private _allowSDF As Boolean
        Public Overridable Property AllowSDF As Boolean
            Get
                Return Me._allowSDF
            End Get
            Set(ByVal value As Boolean)
                Me._allowSDF = value
            End Set
        End Property
        Private _syResource As Short
        Public Overridable Property SyResource As Short
            Get
                Return Me._syResource
            End Get
            Set(ByVal value As Short)
                Me._syResource = value
            End Set
        End Property
        Public Function getModuleCode(ByVal ModuleId As Integer) As String
            Select Case ModuleId
                Case Is = 26
                    Return "AR"
                Case Is = 189
                    Return "AD"
                Case Is = 300
                    Return "FC"
                Case Is = 191
                    Return "FA"
                Case Is = 192
                    Return "HR"
                Case Is = 193
                    Return "PL"
                Case Is = 194
                    Return "SA"
                Case Is = 195
                    Return "SY"
                Case Else
                    Return ""
            End Select
        End Function
    End Class

    <Serializable()>
    Partial Public Class Campuses
        Private _CampusId As Guid
        Public Overridable Property CampusId As Guid
            Get
                Return Me._CampusId
            End Get
            Set(ByVal value As Guid)
                Me._CampusId = value
            End Set
        End Property

        Private _CampusDescription As String
        Public Overridable Property CampusDescription As String
            Get
                Return Me._CampusDescription
            End Get
            Set(ByVal value As String)
                Me._CampusDescription = value
            End Set
        End Property
    End Class

    <Serializable()>
    Partial Public Class PageLevelPermission
        Private _rRLId As Guid
        Public Overridable Property RRLId As Guid
            Get
                Return Me._rRLId
            End Get
            Set(ByVal value As Guid)
                Me._rRLId = value
            End Set
        End Property

        Private _roleId As Guid
        Public Overridable Property RoleId As Guid
            Get
                Return Me._roleId
            End Get
            Set(ByVal value As Guid)
                Me._roleId = value
            End Set
        End Property

        Private _roleName As String
        Public Overridable Property RoleName As String
            Get
                Return Me._roleName
            End Get
            Set(ByVal value As String)
                Me._roleName = value
            End Set
        End Property

        Private _resourceID As Short
        Public Overridable Property ResourceID As Short
            Get
                Return Me._resourceID
            End Get
            Set(ByVal value As Short)
                Me._resourceID = value
            End Set
        End Property

        Private _parentId As Integer?
        Public Overridable Property ParentId As Integer?
            Get
                Return Me._parentId
            End Get
            Set(ByVal value As Integer?)
                Me._parentId = value
            End Set
        End Property

        Private _modUser As String
        Public Overridable Property ModUser As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property

        Private _modDate As Date?
        Public Overridable Property ModDate As Date?
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As Date?)
                Me._modDate = value
            End Set
        End Property

        Private _accessLevel As Short
        Public Overridable Property AccessLevel As Short
            Get
                Return Me._accessLevel
            End Get
            Set(ByVal value As Short)
                Me._accessLevel = value
            End Set
        End Property

        Private _syRole As Role
        Public Overridable Property SyRole As Role
            Get
                Return Me._syRole
            End Get
            Set(ByVal value As Role)
                Me._syRole = value
            End Set
        End Property
        Private _PageLevelFullAccess As Boolean
        Public Overridable Property PageLevelFullAccess As Boolean
            Get
                Return Me._PageLevelFullAccess
            End Get
            Set(ByVal value As Boolean)
                Me._PageLevelFullAccess = value
            End Set
        End Property
        Private _PageLevelEditAccess As Boolean
        Public Overridable Property PageLevelEditAccess As Boolean
            Get
                Return Me._PageLevelEditAccess
            End Get
            Set(ByVal value As Boolean)
                Me._PageLevelEditAccess = value
            End Set
        End Property
        Private _PageLevelCreateAccess As Boolean
        Public Overridable Property PageLevelCreateAccess As Boolean
            Get
                Return Me._PageLevelCreateAccess
            End Get
            Set(ByVal value As Boolean)
                Me._PageLevelCreateAccess = value
            End Set
        End Property
        Private _PageLevelDeleteAccess As Boolean
        Public Overridable Property PageLevelDeleteAccess As Boolean
            Get
                Return Me._PageLevelDeleteAccess
            End Get
            Set(ByVal value As Boolean)
                Me._PageLevelDeleteAccess = value
            End Set
        End Property
        Private _PageLevelReadOnlyAccess As Boolean
        Public Overridable Property PageLevelReadOnlyAccess As Boolean
            Get
                Return Me._PageLevelReadOnlyAccess
            End Get
            Set(ByVal value As Boolean)
                Me._PageLevelReadOnlyAccess = value
            End Set
        End Property
        Private _CampusId As Guid
        Public Overridable Property CampusId As Guid
            Get
                Return Me._CampusId
            End Get
            Set(ByVal value As Guid)
                Me._CampusId = value
            End Set
        End Property
        
    End Class

    <Serializable()>
    Partial Public Class AssignedRoles
        Private _UserId As Guid
        Public Overridable Property UserId As Guid
            Get
                Return Me._UserId
            End Get
            Set(ByVal value As Guid)
                Me._UserId = value
            End Set
        End Property

        Private _UserName As String
        Public Overridable Property UserName As String
            Get
                Return Me._UserName
            End Get
            Set(ByVal value As String)
                Me._UserName = value
            End Set
        End Property

        Private _RoleId As Guid
        Public Overridable Property RoleId As Guid
            Get
                Return Me._RoleId
            End Get
            Set(ByVal value As Guid)
                Me._RoleId = value
            End Set
        End Property

        Private _Role As String
        Public Overridable Property Role As String
            Get
                Return Me._Role
            End Get
            Set(ByVal value As String)
                Me._Role = value
            End Set
        End Property
    End Class

    <Serializable()>
    Partial Public Class Resources

        Private _usedIn As Integer?
        Public Overridable Property UsedIn As Integer?
            Get
                Return Me._usedIn
            End Get
            Set(ByVal value As Integer?)
                Me._usedIn = value
            End Set
        End Property

        Private _tblFldsId As Integer?
        Public Overridable Property TblFldsId As Integer?
            Get
                Return Me._tblFldsId
            End Get
            Set(ByVal value As Integer?)
                Me._tblFldsId = value
            End Set
        End Property

        Private _summListId As Short?
        Public Overridable Property SummListId As Short?
            Get
                Return Me._summListId
            End Get
            Set(ByVal value As Short?)
                Me._summListId = value
            End Set
        End Property

        Private _resourceURL As String
        Public Overridable Property ResourceURL As String
            Get
                Return Me._resourceURL
            End Get
            Set(ByVal value As String)
                Me._resourceURL = value
            End Set
        End Property

        Private _resourceTypeID As Byte
        Public Overridable Property ResourceTypeID As Byte
            Get
                Return Me._resourceTypeID
            End Get
            Set(ByVal value As Byte)
                Me._resourceTypeID = value
            End Set
        End Property

        Private _resourceID As Short
        Public Overridable Property ResourceID As Short
            Get
                Return Me._resourceID
            End Get
            Set(ByVal value As Short)
                Me._resourceID = value
            End Set
        End Property

        Private _resource As String
        Public Overridable Property Resource As String
            Get
                Return Me._resource
            End Get
            Set(ByVal value As String)
                Me._resource = value
            End Set
        End Property

        Private _mRUTypeId As Short?
        Public Overridable Property MRUTypeId As Short?
            Get
                Return Me._mRUTypeId
            End Get
            Set(ByVal value As Short?)
                Me._mRUTypeId = value
            End Set
        End Property

        Private _modUser As String
        Public Overridable Property ModUser As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property

        Private _modDate As Date?
        Public Overridable Property ModDate As Date?
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As Date?)
                Me._modDate = value
            End Set
        End Property

        Private _childTypeId As Byte?
        Public Overridable Property ChildTypeId As Byte?
            Get
                Return Me._childTypeId
            End Get
            Set(ByVal value As Byte?)
                Me._childTypeId = value
            End Set
        End Property

        Private _allowSchlReqFlds As Boolean?
        Public Overridable Property AllowSchlReqFlds As Boolean?
            Get
                Return Me._allowSchlReqFlds
            End Get
            Set(ByVal value As Boolean?)
                Me._allowSchlReqFlds = value
            End Set
        End Property

        Private _syUsers As IList(Of User) = New List(Of User)
        Public Overridable ReadOnly Property SyUsers As IList(Of User)
            Get
                Return Me._syUsers
            End Get
        End Property
        Private _syModules As IList(Of Modules) = New List(Of Modules)
        Public Overridable ReadOnly Property SyModules As IList(Of Modules)
            Get
                Return Me._syModules
            End Get
        End Property
        Private _syNavigationNodes1 As IList(Of NavigationNodes) = New List(Of NavigationNodes)
        Public Overridable Property NavigationNodes As IList(Of NavigationNodes)

            Get
                Return Me._syNavigationNodes1
            End Get
            Set(ByVal value As IList(Of NavigationNodes))
                Me._syNavigationNodes1 = value
            End Set
        End Property
    End Class

    <Serializable()>
    Partial Public Class NavigationNodes

        Private _resourceId As Short?
        Public Overridable Property ResourceId As Short?
            Get
                Return Me._resourceId
            End Get
            Set(ByVal value As Short?)
                Me._resourceId = value
            End Set
        End Property

        Private _parentId As Guid
        Public Overridable Property ParentId As Guid
            Get
                Return Me._parentId
            End Get
            Set(ByVal value As Guid)
                Me._parentId = value
            End Set
        End Property

        Private _modUser As String
        Public Overridable Property ModUser As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property

        Private _modDate As Date?
        Public Overridable Property ModDate As Date?
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As Date?)
                Me._modDate = value
            End Set
        End Property

        Private _isPopupWindow As Boolean?
        Public Overridable Property IsPopupWindow As Boolean?
            Get
                Return Me._isPopupWindow
            End Get
            Set(ByVal value As Boolean?)
                Me._isPopupWindow = value
            End Set
        End Property

        Private _hierarchyIndex As Short
        Public Overridable Property HierarchyIndex As Short
            Get
                Return Me._hierarchyIndex
            End Get
            Set(ByVal value As Short)
                Me._hierarchyIndex = value
            End Set
        End Property

        Private _hierarchyId As Guid
        Public Overridable Property HierarchyId As Guid
            Get
                Return Me._hierarchyId
            End Get
            Set(ByVal value As Guid)
                Me._hierarchyId = value
            End Set
        End Property

        Private _parent As NavigationNodes
        Public Overridable Property Parent As NavigationNodes
            Get
                Return Me._parent
            End Get
            Set(ByVal value As NavigationNodes)
                Me._parent = value
            End Set
        End Property

        Private _syResource1 As Resources
        Public Overridable Property SyResource As Resources
            Get
                Return Me._syResource1
            End Get
            Set(ByVal value As Resources)
                Me._syResource1 = value
            End Set
        End Property

        Private _syNavigationNodes As IList(Of NavigationNodes) = New List(Of NavigationNodes)
        Public Overridable Property ChildNavigationNodes As IList(Of NavigationNodes)
            Set(ByVal value As IList(Of NavigationNodes))
                Me._syNavigationNodes = value
            End Set
            Get
                Return Me._syNavigationNodes
            End Get
        End Property

    End Class
End Namespace
