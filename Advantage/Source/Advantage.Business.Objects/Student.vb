﻿Imports System
Imports System.Collections.Generic

Namespace Advantage.Business.Objects
    Public Class Student
        Private _studentId As String
        Public Overridable Property StudentId As String
            Get
                Return Me._studentId
            End Get
            Set(ByVal value As String)
                Me._studentId = value
            End Set
        End Property

        Private _firstName As String
        Public Overridable Property FirstName As String
            Get
                Return Me._firstName
            End Get
            Set(ByVal value As String)
                Me._firstName = value
            End Set
        End Property

        Private _middleName As String
        Public Overridable Property MiddleName As String
            Get
                Return Me._middleName
            End Get
            Set(ByVal value As String)
                Me._middleName = value
            End Set
        End Property

        Private _lastName As String
        Public Overridable Property LastName As String
            Get
                Return Me._lastName
            End Get
            Set(ByVal value As String)
                Me._lastName = value
            End Set
        End Property

        Private _sSN As String
        Public Overridable Property SSN As String
            Get
                Return Me._sSN
            End Get
            Set(ByVal value As String)
                Me._sSN = value
            End Set
        End Property

        Private _studentStatus As Guid
        Public Overridable Property StudentStatus As Guid
            Get
                Return Me._studentStatus
            End Get
            Set(ByVal value As Guid)
                Me._studentStatus = value
            End Set
        End Property

        Private _workEmail As String
        Public Overridable Property WorkEmail As String
            Get
                Return Me._workEmail
            End Get
            Set(ByVal value As String)
                Me._workEmail = value
            End Set
        End Property

        Private _homeEmail As String
        Public Overridable Property HomeEmail As String
            Get
                Return Me._homeEmail
            End Get
            Set(ByVal value As String)
                Me._homeEmail = value
            End Set
        End Property

        Private _dOB As Nullable(Of DateTime)
        Public Overridable Property DOB As Nullable(Of DateTime)
            Get
                Return Me._dOB
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._dOB = value
            End Set
        End Property

        Private _modUser As String
        Public Overridable Property ModUser As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property

        Private _modDate As Nullable(Of DateTime)
        Public Overridable Property ModDate As Nullable(Of DateTime)
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._modDate = value
            End Set
        End Property

        Private _prefix As System.Nullable(Of System.Guid)
        Public Overridable Property Prefix As System.Nullable(Of System.Guid)
            Get
                Return Me._prefix
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._prefix = value
            End Set
        End Property

        Private _suffix As System.Nullable(Of System.Guid)
        Public Overridable Property Suffix As System.Nullable(Of System.Guid)
            Get
                Return Me._suffix
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._suffix = value
            End Set
        End Property

        Private _sponsor As System.Nullable(Of System.Guid)
        Public Overridable Property Sponsor As System.Nullable(Of System.Guid)
            Get
                Return Me._sponsor
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._sponsor = value
            End Set
        End Property

        Private _assignedDate As Nullable(Of DateTime)
        Public Overridable Property AssignedDate As Nullable(Of DateTime)
            Get
                Return Me._assignedDate
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._assignedDate = value
            End Set
        End Property

        Private _gender As System.Nullable(Of System.Guid)
        Public Overridable Property Gender As System.Nullable(Of System.Guid)
            Get
                Return Me._gender
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._gender = value
            End Set
        End Property

        Private _race As System.Nullable(Of System.Guid)
        Public Overridable Property Race As System.Nullable(Of System.Guid)
            Get
                Return Me._race
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._race = value
            End Set
        End Property

        Private _maritalStatus As System.Nullable(Of System.Guid)
        Public Overridable Property MaritalStatus As System.Nullable(Of System.Guid)
            Get
                Return Me._maritalStatus
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._maritalStatus = value
            End Set
        End Property

        Private _familyIncome As System.Nullable(Of System.Guid)
        Public Overridable Property FamilyIncome As System.Nullable(Of System.Guid)
            Get
                Return Me._familyIncome
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._familyIncome = value
            End Set
        End Property

        Private _children As Nullable(Of Integer)
        Public Overridable Property Children As Nullable(Of Integer)
            Get
                Return Me._children
            End Get
            Set(ByVal value As Nullable(Of Integer))
                Me._children = value
            End Set
        End Property

        Private _expectedStart As Nullable(Of DateTime)
        Public Overridable Property ExpectedStart As Nullable(Of DateTime)
            Get
                Return Me._expectedStart
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._expectedStart = value
            End Set
        End Property

        Private _shiftId As System.Nullable(Of System.Guid)
        Public Overridable Property ShiftId As System.Nullable(Of System.Guid)
            Get
                Return Me._shiftId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._shiftId = value
            End Set
        End Property

        Private _nationality As System.Nullable(Of System.Guid)
        Public Overridable Property Nationality As System.Nullable(Of System.Guid)
            Get
                Return Me._nationality
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._nationality = value
            End Set
        End Property

        Private _citizen As System.Nullable(Of System.Guid)
        Public Overridable Property Citizen As System.Nullable(Of System.Guid)
            Get
                Return Me._citizen
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._citizen = value
            End Set
        End Property

        Private _drivLicStateId As System.Nullable(Of System.Guid)
        Public Overridable Property DrivLicStateId As System.Nullable(Of System.Guid)
            Get
                Return Me._drivLicStateId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._drivLicStateId = value
            End Set
        End Property

        Private _drivLicNumber As String
        Public Overridable Property DrivLicNumber As String
            Get
                Return Me._drivLicNumber
            End Get
            Set(ByVal value As String)
                Me._drivLicNumber = value
            End Set
        End Property

        Private _alienNumber As String
        Public Overridable Property AlienNumber As String
            Get
                Return Me._alienNumber
            End Get
            Set(ByVal value As String)
                Me._alienNumber = value
            End Set
        End Property

        Private _comments As String
        Public Overridable Property Comments As String
            Get
                Return Me._comments
            End Get
            Set(ByVal value As String)
                Me._comments = value
            End Set
        End Property

        Private _county As System.Nullable(Of System.Guid)
        Public Overridable Property County As System.Nullable(Of System.Guid)
            Get
                Return Me._county
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._county = value
            End Set
        End Property

        Private _sourceDate As Nullable(Of DateTime)
        Public Overridable Property SourceDate As Nullable(Of DateTime)
            Get
                Return Me._sourceDate
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._sourceDate = value
            End Set
        End Property

        Private _edLvlId As System.Nullable(Of System.Guid)
        Public Overridable Property EdLvlId As System.Nullable(Of System.Guid)
            Get
                Return Me._edLvlId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._edLvlId = value
            End Set
        End Property

        Private _studentNumber As String
        Public Overridable Property StudentNumber As String
            Get
                Return Me._studentNumber
            End Get
            Set(ByVal value As String)
                Me._studentNumber = value
            End Set
        End Property

        Private _objective As String
        Public Overridable Property Objective As String
            Get
                Return Me._objective
            End Get
            Set(ByVal value As String)
                Me._objective = value
            End Set
        End Property

        Private _dependencyTypeId As System.Nullable(Of System.Guid)
        Public Overridable Property DependencyTypeId As System.Nullable(Of System.Guid)
            Get
                Return Me._dependencyTypeId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._dependencyTypeId = value
            End Set
        End Property

        Private _degCertSeekingId As System.Nullable(Of System.Guid)
        Public Overridable Property DegCertSeekingId As System.Nullable(Of System.Guid)
            Get
                Return Me._degCertSeekingId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._degCertSeekingId = value
            End Set
        End Property

        Private _geographicTypeId As System.Nullable(Of System.Guid)
        Public Overridable Property GeographicTypeId As System.Nullable(Of System.Guid)
            Get
                Return Me._geographicTypeId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._geographicTypeId = value
            End Set
        End Property

        Private _housingId As System.Nullable(Of System.Guid)
        Public Overridable Property HousingId As System.Nullable(Of System.Guid)
            Get
                Return Me._housingId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._housingId = value
            End Set
        End Property

        Private _admincriteriaid As System.Nullable(Of System.Guid)
        Public Overridable Property Admincriteriaid As System.Nullable(Of System.Guid)
            Get
                Return Me._admincriteriaid
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._admincriteriaid = value
            End Set
        End Property

        Private _attendTypeId As System.Nullable(Of System.Guid)
        Public Overridable Property AttendTypeId As System.Nullable(Of System.Guid)
            Get
                Return Me._attendTypeId
            End Get
            Set(ByVal value As System.Nullable(Of System.Guid))
                Me._attendTypeId = value
            End Set
        End Property

        Private _StudentEnrollments As IList(Of StudentEnrollment) = New List(Of StudentEnrollment)
        Public Property StudentEnrollments As IList(Of StudentEnrollment)
            Get
                Return Me._StudentEnrollments
            End Get
            Set(ByVal value As IList(Of StudentEnrollment))
                Me._StudentEnrollments = value
            End Set
        End Property
        Private _StudentAddresses As IList(Of StudentAddress) = New List(Of StudentAddress)
        Public Property StudentAddresses As IList(Of StudentAddress)
            Get
                Return Me._StudentAddresses
            End Get
            Set(ByVal value As IList(Of StudentAddress))
                Me._StudentAddresses = value
            End Set
        End Property
    End Class
End Namespace


