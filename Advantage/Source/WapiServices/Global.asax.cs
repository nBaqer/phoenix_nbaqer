﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Global.asax.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the WebApiApplication type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServices
{
    using System.Diagnostics;
    using System.Web.Http;
    using System.Web.Mvc;

    using WapiServices.App_Start;

    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    /// <summary>
    /// The web API application.
    /// </summary>
    public class WebApiApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// The application_ start.
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            // RouteConfig.RegisterRoutes(RouteTable.Routes);
            // BundleConfig.RegisterBundles(BundleTable.Bundles);
//#if (DEBUG)
//            Debugger.Launch(); // <-- Simple form to debug a web services 
//#endif
            // Initialize WAPI Service
            InitService.GetSettingFromWebConfig();

            InitService.GetProxySettings();
            
            // InitService.GetInternalSettingsAndStoreIt();
            InitService.InitCryptographie();
            
            // InitService.SetInitialStateForWindowsService();
        }
    }
}