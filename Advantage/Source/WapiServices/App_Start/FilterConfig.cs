﻿using System.Web;
using System.Web.Mvc;
using WapiServices.Proxy;

namespace WapiServices
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
          
        }
    }
}