﻿using System.Net.Http;
using System.Web.Http;
using WapiServices.Proxy;
using RouteParameter = System.Web.Http.RouteParameter;

namespace WapiServices
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "WapiProxy", 
                routeTemplate: "{*WapiServices}", 
                handler: HttpClientFactory.CreatePipeline(
                                innerHandler: new HttpClientHandler(), // will never get here if proxy is doing its job
                                handlers: new DelegatingHandler[]
                                { new ProxyHandler() }
                                                        ),
               defaults: new { path = RouteParameter.Optional },
               constraints: null
            );
            
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            //config.EnableSystemDiagnosticsTracing();
        }
    }
}
