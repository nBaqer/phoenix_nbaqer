﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccessWapiProxy.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the AccessWapiProxy type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServices.DataDefinitions
{
    using System;
    using System.Collections.Generic;

    using WapiServices.DataDefinitions.Contracts;

    /// <summary>
    /// The access WAPI proxy.
    /// </summary>
    public class AccessWapiProxy : IAccessWapiProxy
    {
        #region Implementation of IAccessWapiProxy

        /// <summary>
        /// Gets or sets Store the key used in communication.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets Store the company code (company secret) not encrypted.
        /// </summary>
        public string CompanySecret { get; set; }

        /// <summary>
        /// Gets or sets Expiration Date Time: the moment that the key expire and is no longer valid.
        /// </summary>
        public DateTime ExpirationDateTime { get; set; }

        /// <summary>
        /// Gets or sets The code of the tenant associated with the Company Secret. A company secret can
        /// be associated only with one Tenant, but a Tenant can have many associated
        /// company secret
        /// </summary>
        public string TenantCode { get; set; }

        /// <summary>
        /// Gets or sets The Tenant Id 0 is not tenant
        /// </summary>
        public int TenantId { get; set; }

        /// <summary>
        /// Gets or sets The API Key for access the Service Layer
        /// </summary>
        public string ApiKey { get; set; }

        /// <summary>
        /// Gets or sets The connection string for the specific Advantage Database, related to tenant.
        /// This is used in direct access to DB without services.
        /// </summary>
        public string AdvantageConnectionString { get; set; }

        /// <summary>
        /// Gets or sets Hold the name of service authorized to the company code and its URL in the
        /// service layer.
        /// </summary>
        public IDictionary<string, string> AvailableServicesDictionary { get; set; }

        /// <summary>
        /// Gets or sets Ask if the given key is valid 
        /// </summary>
        /// <param name="key">the key to validate</param>
        /// <returns>true if the key is valid</returns>
        public bool IsKeyValid(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return false;
            }

            var isvalid = key == this.Key && this.ExpirationDateTime > DateTime.Now;

            return isvalid;
        }

        /// <summary>
        /// Get the operation URL if exists
        /// </summary>
        /// <param name="operation">
        /// The operation requested
        /// </param>
        /// <returns>If the operation does not exists or is not allowed return empty string</returns>
        public string GetOperationUrlIfAllowed(string operation)
        {
            // See if the operation is in dictionary
            string url = string.Empty;
            this.AvailableServicesDictionary?.TryGetValue(operation, out url);

            return url;
        }

        /// <summary>
        /// Ask the object for a valid key
        /// </summary>
        /// <returns>A valid key</returns>
        public string GetValidKey()
        {
         if (string.IsNullOrEmpty(this.Key) == false && this.ExpirationDateTime > DateTime.Now)
            {
                this.ExpirationDateTime = DateTime.Now.AddMinutes(20);
                return this.Key;
            }

            this.Key = Guid.NewGuid().ToString();
            this.ExpirationDateTime = DateTime.Now.AddMinutes(20);
            return this.Key;
        }

        #endregion

        /// <summary>
        /// The factory.
        /// </summary>
        /// <returns>
        /// The <see cref="IAccessWapiProxy"/>.
        /// </returns>
        // ReSharper disable once StyleCop.SA1204
        public static IAccessWapiProxy Factory()
        {
            IAccessWapiProxy access = new AccessWapiProxy();
            access.AvailableServicesDictionary = new Dictionary<string, string>();
            return access;
        }
    }
}