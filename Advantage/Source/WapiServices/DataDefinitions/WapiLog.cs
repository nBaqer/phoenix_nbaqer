﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiLog.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the WapiLog type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServices.DataDefinitions
{
    using System;

    using WapiServices.DataDefinitions.Contracts;

    /// <summary>
    /// The WAPI log.
    /// </summary>
    public class WapiLog : IWapiLog
    {
        #region Implementation of IWapiLog

        /// <summary>
        /// Gets or sets Internal ID.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets User who invoked the service
        /// </summary>
        public string TenantName { get; set; }

        /// <summary>
        /// Gets or sets Date of write the log
        /// </summary>
        public DateTime DateMod { get; set; }

        /// <summary>
        /// Gets or sets Number of modified records
        /// </summary>
        public int NumberOfRecords { get; set; }

        /// <summary>
        /// Gets or sets The Company that invoke the service
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// Gets or sets The name of the invoked service
        /// </summary>
        public string ServiceInvoqued { get; set; }

        /// <summary>
        /// Gets or sets The type of operation. Accepted values are LOGIN PUSH, PULL, WRITE, READ
        /// </summary>
        public string OperationDescription { get; set; }

        /// <summary>
        /// Gets or sets is OK 1: OK true 0: error false.
        /// </summary>
        public bool IsOk { get; set; }

        /// <summary>
        /// Gets or sets A optional comment about the log.
        /// </summary>
        public string Comment { get; set; }

        #endregion

        /// <summary>
        /// The factory.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <param name="operationCode">
        /// The operation code.
        /// </param>
        /// <returns>
        /// The <see cref="IWapiLog"/>.
        /// </returns>
        public static IWapiLog Factory(IFilterMessage filter, string operationCode)
        {
            IWapiLog log = new WapiLog();
            log.DateMod = DateTime.Now;
            log.NumberOfRecords = filter.NumberRecords;
            log.ServiceInvoqued = filter.ServiceCode;
            log.OperationDescription = operationCode;
            if (string.IsNullOrEmpty(filter.CompanySecret))
            {
                if (string.IsNullOrEmpty(filter.AccessKey) == false)
                {
                   log.Company = StaticSettingStore.GetRelatedCompanyName(filter.AccessKey);
                }
            }
            else
            {
               log.Company = filter.CompanySecret;
            }

            log.TenantName = filter.TenantName;
            log.IsOk = filter.IsOk;
            log.Comment = filter.Comment; 
            return log;
        }
    }
}