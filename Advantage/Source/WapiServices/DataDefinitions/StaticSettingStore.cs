// --------------------------------------------------------------------------------------------------------------------
// <copyright company="FAME" file="StaticSettingStore.cs">
//   2014
// </copyright>
// <summary>
//   Static Setting Store
// </summary>
// 
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServices.DataDefinitions
{
    using System.Collections.Generic;
    using System.Linq;

    using WapiServices.DataDefinitions.Contracts;

    /// <summary>
    /// Store the settings or the Advantage WAPI layer
    /// </summary>
    public static class StaticSettingStore
    {
        /// <summary>
        /// Initializes static members of the <see cref="StaticSettingStore"/> class. 
        /// Initialize the list of access to proxy
        /// </summary>
        static StaticSettingStore()
        {
            AccessWapiProxyList = new List<IAccessWapiProxy>();
        }

        /// <summary>
        /// Gets or sets The connection string to tenant DB.
        /// </summary>
        public static string ConnectionStringTenantDb { get; set; }

        /// <summary>
        /// Gets or sets Contain a list with the information to work
        /// with different tenant.
        /// </summary>
        public static IList<IAccessWapiProxy> AccessWapiProxyList { get; set; }

        /// <summary>
        /// Gets or sets Test if the access key is valid.
        /// </summary>
        /// <param name="accesskey">
        /// The access token 
        /// </param>
        /// <returns>
        /// true if the token is valid
        /// </returns>
        public static bool ExistsValidAccessKey(string accesskey)
        {
            if (string.IsNullOrEmpty(accesskey))
            {
                return false;
            }

            return AccessWapiProxyList.Any(proxy => proxy.IsKeyValid(accesskey));
        }

        /// <summary>
        /// Return the tenant related to access key if exists.
        /// </summary>
        /// <param name="accesskey">
        /// The access token
        /// </param>
        /// <returns>
        /// The company name
        /// </returns>
        public static string GetRelatedCompanyName(string accesskey)
        {
            var accessWapiProxy = AccessWapiProxyList.FirstOrDefault(proxy => proxy.Key == accesskey);
            if (accessWapiProxy != null)
            {
                var company = accessWapiProxy.CompanySecret;
                return company;
            }

            return string.Empty;
        }
    } // end StaticSettingStore
} // end name space Initialization