﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SettingWapiService.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Settings for WAPI Services.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServices.DataDefinitions
{
    using System.Collections.Generic;

    using WapiServices.DataDefinitions.Contracts;

    /// <summary>
    /// Settings for WAPI Services.
    /// </summary>
    public class SettingWapiService : ISettingWapiService
    {
        #region Implementation of ISettingWapiService

        /// <summary>
        ///  Gets or sets The code of the company authorized to use this.
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// Gets or sets the available services dictionary.
        /// </summary>
        public IDictionary<string, string> AvailableServicesDictionary { get; set; } 

        #endregion
    }
}