using System.Collections;
using System.Collections.Generic;

namespace WapiServices.DataDefinitions.Contracts
{
    /// <summary>
    /// ISettingWapiService.
    /// </summary>
    public interface ISettingWapiService
    {
        ///// <summary>
        ///// Code of service
        ///// </summary>
        //string CodeService { get; set; }

        ///// <summary>
        ///// Url of the service in Advantage Service Layer (route).
        ///// </summary>
        //string UrlService { get; set; }

        /// <summary>
        /// The code of the company authorizated to use this.
        /// </summary>
        string CompanyCode { get; set; }

        /// <summary>
        /// Availables Services for this company
        /// Give the name of services and the url to going on.
        /// </summary>
        IDictionary<string, string> AvailableServicesDictionary { get; set; } 

        ///// <summary>
        ///// Username and password for the specific company.
        ///// </summary>
        //IDictionary<string, string> AuthentificationDictionary {get;set;}
    }
}


