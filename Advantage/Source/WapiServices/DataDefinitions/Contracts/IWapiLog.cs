﻿using System;

namespace WapiServices.DataDefinitions.Contracts
{
    /// <summary>
    /// Data to be logged in the syWAPILog table.
    /// </summary>
    public interface IWapiLog
    {
        /// <summary>
        /// Internal id.
        /// </summary>
        long Id { get; set; }

        /// <summary>
        /// User who invoked the service
        /// </summary>
        string TenantName { get; set; }

        /// <summary>
        /// Date of write the log
        /// </summary>
        DateTime DateMod { get; set; }

        /// <summary>
        /// Number of modified records
        /// </summary>
        int NumberOfRecords { get; set; }

        /// <summary>
        /// The Company that invoke the service
        /// </summary>
        string Company { get; set; }

        /// <summary>
        /// The name of the invoked service
        /// </summary>
        string ServiceInvoqued { get; set; }

        /// <summary>
        /// The type of operation. Accepted values are LOGIN PUSH, PULL, WRITE, READ
        /// </summary>
        string OperationDescription { get; set; }

        /// <summary>
        /// 1: ok true 0: error false.
        /// </summary>
        bool IsOk { get; set; }

        /// <summary>
        /// A optional comment about the log.
        /// </summary>
        string Comment { get; set; }
    }
}
