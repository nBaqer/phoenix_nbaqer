﻿using System.Collections.Generic;
using System.Net.Http;

namespace WapiServices.DataDefinitions.Contracts
{
    /// <summary>
    /// Describe a input message.
    /// </summary>
    public interface IFilterMessage
    {
        // Proxy Headers ........................................................................................
        /// <summary>
        /// The company code
        /// </summary>
        string CompanySecret { get; set; }

        /// <summary>
        /// The proxy access keys
        /// </summary>
        string AccessKey { get; set; }
        
        /// <summary>
        /// The service requested code.
        /// </summary>
        string ServiceCode { get; set; }

       //--------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Optional parameters from the request.
        /// </summary>
        /// <remarks> obsolete</remarks>
        IDictionary<string, string> OptionalFiltersDict { get; set; }

        /// <summary>
        /// Query string of the request.
        /// </summary>
        string QueryString { get; set; }

        ///// <summary>
        ///// Internal user, This parameter is fill inside the service
        ///// In fact is a request header.
        ///// </summary>
        //string UserName { get; set; }

        ///// <summary>
        ///// Internal use. It is send by the request as a header
        ///// </summary>
        //string Password { get; set; }

        /// <summary>
        /// The name of the tenant.
        /// </summary>
        string TenantName { get; set; }

        /// <summary>
        /// Internal use. Store the result of the operation to be passed to log table.
        /// </summary>
        bool IsOk { get; set; }

        /// <summary>
        /// Internal Use. Store the number of register read or write for the operation.
        /// </summary>
        int NumberRecords { get; set; }

        /// <summary>
        /// Internal use Log file operation comment.
        /// </summary>
        string Comment { get; set; }

        /// <summary>
        /// Get the method of the request. GET, POST, UPDATE, DELETE
        /// </summary>
        HttpMethod Method { get; set; }

        /// <summary>
        /// Store the content of a post request.
        /// </summary>
        HttpContent PostContent { get; set; }

    }
}
