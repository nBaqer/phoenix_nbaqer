﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FilterMessage.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Describe a input message.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServices.DataDefinitions
{
    using System.Collections.Generic;
    using System.Net.Http;

    using WapiServices.DataDefinitions.Contracts;

    /// <summary>
    /// Describe a input message.
    /// </summary>
    public class FilterMessage : IFilterMessage
    {
        #region Implementation of IFilterMessage

        /// <summary>
        /// Gets or sets The company code
        /// </summary>
        public string CompanySecret { get; set; }

        /// <summary>
        ///  Gets or sets The proxy access keys
        /// </summary>
        public string AccessKey { get; set; }

        /// <summary>
        ///  Gets or sets The operation requested code.
        /// </summary>
        public string ServiceCode { get; set; }

        /// <summary>
        ///  Gets or sets Optional parameters from the request.
        /// </summary>
        public IDictionary<string, string> OptionalFiltersDict { get; set; }

        /// <summary>
        /// Gets or sets Query string of the request.
        /// </summary>
        public string QueryString { get; set; }

        /// <summary>
        /// Gets or sets The name of the tenant.
        /// </summary>
        public string TenantName { get; set; }

        /// <summary>
        /// Gets or sets Internal user, This parameter is fill inside the service
        /// In fact is a request header.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets Internal use. It is send by the request as a header
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the result of the operation to be passed to log table  (Internal Use).
        /// </summary>
        public bool IsOk { get; set; }

        /// <summary>
        /// Gets or sets Internal Use. Store the number of register read or write for the operation.
        /// </summary>
        public int NumberRecords { get; set; }

        /// <summary>
        /// Gets or sets Internal use Log file operation comment.
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets Get the method of the request. GET, POST, UPDATE, DELETE
        /// </summary>
        public HttpMethod Method { get; set; }

        /// <summary>
        /// Gets or sets  Store the content of a post request.
        /// </summary>
        public HttpContent PostContent { get; set; }

        #endregion

        /// <summary>
        /// Create from here the object.
        /// </summary>
        /// <returns>IFilterMessage Object</returns>
        public static IFilterMessage Factory()
        {
            IFilterMessage fm = new FilterMessage();
            fm.OptionalFiltersDict = new Dictionary<string, string>();
            fm.ServiceCode = string.Empty;
            fm.CompanySecret = string.Empty;
            
            return fm;
        }
    }
}