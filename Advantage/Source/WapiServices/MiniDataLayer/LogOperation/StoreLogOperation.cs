﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StoreLogOperation.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the StoreLogOperation type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServices.MiniDataLayer.LogOperation
{
    using System.Threading.Tasks;

    using WapiServices.DataDefinitions.Contracts;

    /// <summary>
    /// The store log operation.
    /// </summary>
    internal class StoreLogOperation
    {
        /// <summary>
        /// The write log entry.
        /// </summary>
        /// <param name="entrylog">
        /// The entry log.
        /// </param>
        internal static async void WriteLogEntry(IWapiLog entrylog)
        {
           // Run the facade to save the log
           await Task.Factory.StartNew(() => Facade.Facade.WriteEntry(entrylog));
        }
    }
}