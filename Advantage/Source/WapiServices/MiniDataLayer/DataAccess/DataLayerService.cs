// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataLayerService.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the DataLayerService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServices.MiniDataLayer.DataAccess
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using WapiServices.DataDefinitions;
    using WapiServices.DataDefinitions.Contracts;

    /// <summary>
    /// The data layer service.
    /// </summary>
    public class DataLayerService
    {
        #region Log manager

        /// <summary>
        /// The write log in database.
        /// </summary>
        /// <param name="connStr">
        /// The connection string.
        /// </param>
        /// <param name="log">
        /// The log.
        /// </param>
        public void WriteLogInDatabase(string connStr, IWapiLog log)
        {
           // Control of null.....
            log.Company = log.Company ?? string.Empty;
            log.ServiceInvoqued = log.ServiceInvoqued ?? string.Empty;
            log.Comment = log.Comment ?? string.Empty;
            log.TenantName = log.TenantName ?? string.Empty;

            // Control of limit size .........
            log.Company = (log.Company.Length > 50) ? log.Company.Remove(50) : log.Company;
            log.ServiceInvoqued = (log.ServiceInvoqued.Length > 30) ? log.ServiceInvoqued.Remove(30) : log.ServiceInvoqued;
            log.TenantName = (log.TenantName.Length > 50) ? log.TenantName.Remove(50) : log.TenantName;
             
            // Create the connection object
            var conn = new SqlConnection(connStr);
            
            // Create the command
            var cmd = new SqlCommand
            {
                Connection = conn,
                CommandType = CommandType.Text,
                CommandText = "INSERT INTO WapiLog (TenantName, DateMod, NumberOfRecords, Company, ServiceInvoqued, OperationDescription, IsOk, Comment) VALUES (@1,@2,@3,@4,@5,@6,@7,@8)"  
             };
            cmd.Parameters.AddWithValue("@1", log.TenantName);
            cmd.Parameters.AddWithValue("@2", log.DateMod);
            cmd.Parameters.AddWithValue("@3", log.NumberOfRecords);
            cmd.Parameters.AddWithValue("@4", log.Company);
            cmd.Parameters.AddWithValue("@5", log.ServiceInvoqued);
            cmd.Parameters.AddWithValue("@6", log.OperationDescription);
            cmd.Parameters.AddWithValue("@7", log.IsOk ? 1 : 0);
            cmd.Parameters.AddWithValue("@8", log.Comment);
            conn.Open();
            try
            {
               cmd.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The get log from database.
        /// </summary>
        /// <param name="connStr">
        /// The connection string.
        /// </param>
        /// <param name="operationCode">
        /// The operation code.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;IWapiLog&gt; "/>.
        /// </returns>
        public IList<IWapiLog> GetLogFromDatabase(string connStr, string operationCode)
        {
            var list = new List<IWapiLog>();
            
            // Get The connection String
            // Create the connection object
            var conn = new SqlConnection(connStr);
            
            // Create the command
            var cmd = new SqlCommand
            {
                Connection = conn,
                CommandType = CommandType.Text,
                CommandText = "SELECT id, TenantName, DateMod, NumberOfRecords, Company, ServiceInvoqued, OperationDescription, isOk, Comment FROM WapiLog WHERE OperationDescription = @1"
            };
            cmd.Parameters.AddWithValue("@1", operationCode);
            conn.Open();
            try
            {
                var sqldr = cmd.ExecuteReader();
                
                // return values
                while (sqldr.Read())
                {
                    var log = WapiLog.Factory(new FilterMessage(), operationCode);
                    log.Id = (long)sqldr.GetValue(0);
                    log.TenantName = sqldr.GetString(1);
                    log.DateMod = sqldr.GetDateTime(2);
                    log.NumberOfRecords = sqldr.GetInt32(3);
                    log.Company = sqldr.GetString(4);
                    log.ServiceInvoqued = sqldr.GetString(5);
                    log.OperationDescription = sqldr.GetString(6);
                    log.IsOk = sqldr.GetBoolean(7);
                    log.Comment = sqldr.GetString(8);
                    list.Add(log);
                }

                return list;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The delete log in database by operation code.
        /// </summary>
        /// <param name="connStr">
        /// The connection string.
        /// </param>
        /// <param name="operationCode">
        /// The operation code.
        /// </param>
        public void DeleteLogInDatabaseByOperationCode(string connStr, string operationCode)
        {
            // Get The connection String
            // Create the connection object
            var conn = new SqlConnection(connStr);
            
            // Create the command
            var cmd = new SqlCommand
            {
                Connection = conn,
                CommandType = CommandType.Text,
                CommandText = "DELETE FROM WapiLog WHERE [OperationDescription] = @1"
            };
            cmd.Parameters.AddWithValue("@1", operationCode);
           
            conn.Open();
            try
            {
                cmd.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Get initial settings for Proxy service.

        /// <summary>
        /// The get tenant and company data.
        /// </summary>
        /// <param name="connectionStringTenantDb">
        /// The connection string tenant DB.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;IAccessWapiProxy&gt;"/>.
        /// </returns>
        public IList<IAccessWapiProxy> GetTenantAndCompanyData(string connectionStringTenantDb)
        {
             IList<IAccessWapiProxy> resultList = new List<IAccessWapiProxy>();
            
            // Get The connection String
            // Create the connection object
            var conn = new SqlConnection(connectionStringTenantDb);
            
            // Create the command
            var cmd = new SqlCommand
            {
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "[WapiTenantGetAdvantageOperationSettings]"
            };
            conn.Open();
            try
            {
                var sqldr = cmd.ExecuteReader();
                
                // return values
                while (sqldr.Read())
                {
                    var setting = AccessWapiProxy.Factory();
                    setting.AdvantageConnectionString = sqldr.GetString(0);
                    setting.ApiKey = sqldr.GetString(1);
                    setting.CompanySecret = sqldr.GetString(2);
                    setting.TenantId = sqldr.GetInt32(3);
                    setting.TenantCode = sqldr.GetString(4);
                    resultList.Add(setting);
                }

                sqldr.Close();
                return resultList;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Get the configuration settings for WAPI Services.
        /// Return a list of Allowed Services for each Company Code.
        /// </summary>
        /// <param name="connStr">
        /// The connection string.
        /// </param>
        /// <returns>
        /// List of <code>ISettingWapiService</code>
        /// </returns>
        internal IList<ISettingWapiService> GetSettingsFromDatabase(string connStr)
        {
            IList<ISettingWapiService> resultList = new List<ISettingWapiService>();
            
            // Get The connection String
            // Create the connection object
            var conn = new SqlConnection(connStr);
            
            // Create the command
            var cmd = new SqlCommand
            {
                Connection = conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "WapiGetSettingsService"
            };
            conn.Open();
            try
            {
                var sqldr = cmd.ExecuteReader();
                
                // return values
                while (sqldr.Read())
                {
                    ////dat.CodeService = sqldr.GetString(0);
                    var companyCode = sqldr.GetString(1);
                    var com = resultList.FirstOrDefault(x => x.CompanyCode == companyCode);
                    if (com == null)
                    {
                        ISettingWapiService dat = new SettingWapiService();
                        ////dat.AuthentificationDictionary = new Dictionary<string, string>();
                        dat.AvailableServicesDictionary = new Dictionary<string, string>();
                        dat.CompanyCode = companyCode;
                        dat.AvailableServicesDictionary.Add(sqldr.GetString(0), sqldr.GetString(2));
                        resultList.Add(dat);
                    }
                    else
                    {
                        com.AvailableServicesDictionary.Add(sqldr.GetString(0), sqldr.GetString(2));
                    }
                }

                sqldr.Close();

                return resultList;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion
    } // end DataLayerService
} // end name-space Initialization