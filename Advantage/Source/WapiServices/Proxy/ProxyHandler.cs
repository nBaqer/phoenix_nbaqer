﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProxyHandler.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the ProxyHandler type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServices.Proxy
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;

    using WapiServices.DataDefinitions;
    using WapiServices.DataDefinitions.Contracts;
    using WapiServices.MiniDataLayer.LogOperation;
    using WapiServices.Security;

    /// <summary>
    /// The proxy handler.
    /// </summary>
    public class ProxyHandler : DelegatingHandler
    {
        /// <summary>
        /// Send the public Key
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Reviewed. Suppression is OK here.")]
        // ReSharper disable once InconsistentNaming
        private const string SEND_PUBLIC_KEY = "SEND_PUBLIC_KEY";
        
        /// <summary>
        /// The response problem delegate function.
        /// </summary>
        private readonly Func<object, HttpResponseMessage> responseProblemDelegateFunc = delegate(object request)
        {
            // var protocol = ((HttpRequestMessage) request).RequestUri.Scheme;
            // Create the response message
            var response = new HttpResponseMessage();

            // Get the SERVICE_CODE, ACCESS_KEY, COMPANY_SECRET
            IFilterMessage filter = ProxyBusinessRules.ProcessMessage(request); // ProcessOperationParameters(request);
            filter.Comment = "No process found..";
            
            // See if the operation is a request of public key. if it send it.
            if (filter.ServiceCode.ToUpper() == SEND_PUBLIC_KEY)
            {
                response.StatusCode = HttpStatusCode.OK;
                response.RequestMessage = request as HttpRequestMessage;
                var publicKey = Encryption.GetPublicKey();
                response.Content = new StringContent(publicKey);
                filter.IsOk = true;
                filter.Comment = "PUBLIC KEY";
                StoreLogOperation.WriteLogEntry(WapiLog.Factory(filter, "READ"));
                return response;
            }

            ////// Authenticate the user name and password
            ////var isAuthenticate = ProxyBusinessRules.LoginRequest(filter);
            ////filter.IsOk = isAuthenticate;
            ////filter.Comment = string.Empty;
            ////if (isAuthenticate == false)
            ////{
            ////    response.StatusCode = HttpStatusCode.Unauthorized;
            ////    response.Content = new StringContent("Bad username or password");
            ////    filter.Comment = "Bad username or password";
            ////    StoreLogOperation.WriteLogEntry(WapiLog.Factory(filter, "LOGIN"));
            ////    return response;
            ////}

            // Look if it is a ECHO operations and ECHO is does.
            if (filter.ServiceCode.ToUpper() == "ECHO")
            {
                response.StatusCode = HttpStatusCode.OK;
                response.RequestMessage = request as HttpRequestMessage;
                response.Content = new StringContent("ECHO received OK");
                filter.IsOk = true;
                filter.Comment = "ECHO received OK";
                StoreLogOperation.WriteLogEntry(WapiLog.Factory(filter, "READ"));
                return response;
            }

            // Look if a ACCESS KEY Was received. to execute the actions
            if (string.IsNullOrWhiteSpace(filter.AccessKey) == false)
            {
                // test if the access key is valid and active
                if (StaticSettingStore.ExistsValidAccessKey(filter.AccessKey))
                {
                    // If the operation is present test if the operation is authorized
                    var operationkeyValue = ProxyBusinessRules.AuthorizedRequest(filter);
                    if (operationkeyValue == null || operationkeyValue.Value.Key == null)
                    {
                        response.StatusCode = HttpStatusCode.Forbidden;
                        response.Content = new StringContent("Your are not right to do the operation");
                        filter.IsOk = false;
                        filter.Comment = "Your are not right to do the operation";
                        StoreLogOperation.WriteLogEntry(WapiLog.Factory(filter, "READ"));
                        return response;
                    }

                    // Operation is authorized... access service layer and return the result
                    // Get the value of the operation code as your path to service.
                    var servicepath = operationkeyValue.Value.Value;
                    
                    // Access the service in the Service Layer.....
                    var asl = new AccessServiceLayer();
                    
                    HttpResponseMessage sres = asl.AccessingServiceLayer(servicepath, filter);
                    filter.Comment = "Executed..";
                    filter.IsOk = true;
                    StoreLogOperation.WriteLogEntry(WapiLog.Factory(filter, filter.Method.ToString()));
                    return sres;
                }

                filter.Comment = "Access Key void or not Valid";    
                response.ReasonPhrase = "WRONG_ACCESS_KEY";
            }

            // See if it is a request for login
            if (string.IsNullOrWhiteSpace(filter.CompanySecret) == false)
            {
                // The client is requesting for login validate the request
                string accesskey = ProxyBusinessRules.LoginRequest(filter);
                if (accesskey != null)
                {
                    // Login correct, return the access key
                    response.StatusCode = HttpStatusCode.OK;
                    response.RequestMessage = request as HttpRequestMessage;
                    response.Content = new StringContent(accesskey);
                    filter.IsOk = true;
                    filter.Comment = "LOGIN OK";
                    StoreLogOperation.WriteLogEntry(WapiLog.Factory(filter, "LOGIN"));
                    return response;
                }

                filter.Comment = "Company Secret not valid, fail login!";
            }

            // Bad format, fail request, log it and return 401
            response.StatusCode = HttpStatusCode.ProxyAuthenticationRequired;
            response.Content = new StringContent("Bad Format or Fail authorization");
            filter.IsOk = false;
            StoreLogOperation.WriteLogEntry(WapiLog.Factory(filter, "LOGIN"));
            return response;
        };

        /// <summary>
        /// Create the task to manage the request in the proxy.
        /// </summary>
        /// <param name="request">
        /// The request to proxy
        /// </param>
        /// <param name="cancellationToken">
        /// The cancellation token
        /// </param>
        /// <returns>
        /// A a-sync response message.
        /// </returns>
        protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request,
            System.Threading.CancellationToken cancellationToken)
        {
            // Call Task a sync and process the authentication and authorization. Also process the data access to service layer.... 
            HttpResponseMessage respTask = await Task<HttpResponseMessage>.Factory.StartNew(this.responseProblemDelegateFunc, request, cancellationToken);
            return respTask;
        }
    }
}