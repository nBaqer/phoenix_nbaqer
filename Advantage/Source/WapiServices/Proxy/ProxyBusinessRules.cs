﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProxyBusinessRules.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the ProxyBusinessRules type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServices.Proxy
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;

    using WapiServices.DataDefinitions;
    using WapiServices.DataDefinitions.Contracts;
    using WapiServices.Security;

    /// <summary>
    /// The proxy business rules.
    /// </summary>
    public static class ProxyBusinessRules
    {
        /// <summary>
        /// The process operation parameters.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="IFilterMessage"/>.
        /// </returns>
        internal static IFilterMessage ProcessOperationParameters(object request)
        {
            var filter = FilterMessage.Factory();
            var req = request as HttpRequestMessage;
            var dict = req.GetQueryNameValuePairs();
            foreach (KeyValuePair<string, string> pair in dict)
            {
                if (pair.Key.ToLower() == "company")
                {
                    filter.CompanySecret = pair.Value;
                    continue;
                }

                if (pair.Key.ToLower() == "operation")
                {
                    filter.ServiceCode = pair.Value;
                    continue;
                }

                filter.OptionalFiltersDict.Add(pair);
            }

            return filter;
        }

        /// <summary>
        /// If the key is valid and exists and the operation is valid, return the available service dictionary
        /// with the service information.
        /// </summary>
        /// <param name="filter">
        /// The filter
        /// </param>
        /// <returns>
        /// Key value part with the path.
        /// </returns>
        internal static KeyValuePair<string, string>? AuthorizedRequest(IFilterMessage filter)
        {
             var internalSettings = StaticSettingStore.AccessWapiProxyList.FirstOrDefault(x => x.Key == filter.AccessKey);
            if (internalSettings != null)
            {
                var servicedata = internalSettings.AvailableServicesDictionary.FirstOrDefault(x => x.Key == filter.ServiceCode);
                filter.TenantName = internalSettings.TenantCode;
                return servicedata;
            }

            return null;
        }

        /// <summary>
        /// The login request.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        internal static string LoginRequest(IFilterMessage filter)
        {
            var internalSettings = StaticSettingStore.AccessWapiProxyList.FirstOrDefault(x => x.CompanySecret == filter.CompanySecret);
            
            // The Company secret is valid. get a valid key
            var accessKey = internalSettings?.GetValidKey();
            return accessKey;
        }

        /// <summary>
        /// Version 2 PRoxy 
        /// It is prepare to receive 4 header with
        /// user-name, password, company and operation.
        /// </summary>
        /// <param name="request">
        /// The request
        /// </param>
        /// <returns>
        /// The message
        /// </returns>
        public static IFilterMessage ProcessMessage(object request)
        {
            var filter = FilterMessage.Factory();
            
            var req = request as HttpRequestMessage;
            if (req != null)
            {
                var headers = req.Headers;
                foreach (KeyValuePair<string, IEnumerable<string>> header in headers)
                {
                    if (header.Key.ToUpper() == "ACCESS_KEY")
                    {
                        filter.AccessKey = header.Value.FirstOrDefault();
                        continue;
                    }

                    if (header.Key.ToUpper() == "COMPANY_SECRET")
                    {
                       // Get header value
                        var encryp = header.Value.FirstOrDefault();

                        // Decrypt the company-secret
                        filter.CompanySecret = Encryption.DecryptCompanyCode(encryp);
                        continue;
                    }

                    if (header.Key.ToUpper() == "SERVICE_CODE")
                    {
                        filter.ServiceCode = header.Value.FirstOrDefault();
                    }
                }

                // Get the query string
                filter.QueryString = req.RequestUri.Query;
                filter.Method = req.Method;
                if (req.Method == HttpMethod.Post)
                {
                    filter.PostContent = req.Content;
                }
            }

            return filter;
        }
    }
}