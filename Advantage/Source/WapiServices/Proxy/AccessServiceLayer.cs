﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccessServiceLayer.cs" company="Fame">
//   2014
// </copyright>
// <summary>
//   Defines the AccessServiceLayer type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServices.Proxy
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Net;
    using System.Net.Http;

    using WapiServices.DataDefinitions;
    using WapiServices.DataDefinitions.Contracts;

    /// <summary>
    /// The access service layer.
    /// </summary>
    internal class AccessServiceLayer
    {
        /// <summary>
        /// The accessing service layer.
        /// </summary>
        /// <param name="servicepath">
        /// The service-path.
        /// </param>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="HttpResponseMessage"/>.
        /// </returns>
        internal HttpResponseMessage AccessingServiceLayer(string servicepath, IFilterMessage filter)
        {
            // Get the proper API key to access Service Layer
            var proxySetting = StaticSettingStore.AccessWapiProxyList.SingleOrDefault(x => x.Key == filter.AccessKey);
            if (proxySetting != null)
            {
                servicepath += filter.QueryString; // both POST and GET can query string
                var apikey = proxySetting.ApiKey;
                switch (filter.Method.Method)
                {
                    case "POST":
                        {
                            var result = this.PostOperation(servicepath, filter, apikey);
                            return result;
                        }

                    case "GET":
                        {
                            var result = this.GetOperation(servicepath, filter, apikey);
                            return result;
                        }
                }
            }

            return new HttpResponseMessage(HttpStatusCode.NotImplemented);
        }

        /// <summary>
        /// The get operation.
        /// </summary>
        /// <param name="servicepath">
        /// The service-path.
        /// </param>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <param name="apiKey">
        /// The API key.
        /// </param>
        /// <returns>
        /// The <see cref="HttpResponseMessage"/>.
        /// </returns>
        private HttpResponseMessage GetOperation(string servicepath, IFilterMessage filter, string apiKey)
        {
            var client = new HttpClient();
            client.Timeout = TimeSpan.FromMinutes(15);
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicepath),
                Method = filter.Method
            };

            request.Headers.Add("AuthKey", apiKey);
            HttpResponseMessage response = null;
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                if (taskwithmsg.IsCanceled)
                {
                    response = new HttpResponseMessage(HttpStatusCode.GatewayTimeout);
                    response.ReasonPhrase = "Task was canceled. Possible timeout";
                    Debug.WriteLine(response.ReasonPhrase);
                }
                else
                {
                    response = taskwithmsg.Result;
                    Debug.WriteLine(response.ReasonPhrase);
                    Debug.WriteLine(response.Content.ReadAsStringAsync().Result);
                }
            });

            task.Wait();

            return response;
        }

        /// <summary>
        /// The post operation.
        /// </summary>
        /// <param name="servicepath">
        /// The service-path.
        /// </param>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <param name="apiKey">
        /// The API key.
        /// </param>
        /// <returns>
        /// The <see cref="HttpResponseMessage"/>.
        /// </returns>
        private HttpResponseMessage PostOperation(string servicepath, IFilterMessage filter, string apiKey)
        {
            var client = new HttpClient();
            client.Timeout = TimeSpan.FromMinutes(15);
            client.DefaultRequestHeaders.Add("AuthKey", apiKey);
            HttpResponseMessage response = null;
            var task = client.PostAsync(new Uri(servicepath), filter.PostContent).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
                Debug.WriteLine(response.ToString());
                Debug.WriteLine(response.Content.ReadAsStringAsync().Result);
            });
            task.Wait();

            return response;
        }

        // public void SetStateForWindowsService(IFilterMessage filter, string serviceCommand)
        // {
        // //send the login request as headers...
        // var apikey = StaticSettingStore.AdvantageOperationSetting.ApiKey.ToString();
        // var settingWapiService = StaticSettingStore.InternalSettings.SingleOrDefault(x => x.CompanyCode == "FAME");
        // if (settingWapiService != null)
        // {
        // string servicesUrl = settingWapiService.AvailableServicesDictionary.SingleOrDefault(v => v.Key == "INITWSERV").Value;
        // var client = new HttpClient();
        // client.DefaultRequestHeaders.Add("AuthKey", apikey);
        // var postData = new List<KeyValuePair<string, string>>
        // {
        // new KeyValuePair<string, string>("ServiceName", "WapiWindowsService"),
        // new KeyValuePair<string, string>("Command", serviceCommand)
        // };

        // var task = client.PostAsJsonAsync(servicesUrl, postData).ContinueWith(taskwithmsg =>
        // {
        // var response = taskwithmsg.Result;
        // var strResponse = response.ToString();
        // Debug.WriteLine(strResponse);
        // var strContentResponse = response.Content.ReadAsStringAsync().Result;
        // Debug.WriteLine(strContentResponse);
        // var res = strContentResponse;
        // filter.Comment = res;
        // filter.IsOk = !(res.Contains("Error"));
        // IWapiLog log = WapiLog.Factory(filter, "INIT");
        // Facade.WriteEntry(log);
        // });
        // task.Wait();
        // // return res;
        // }

        // }
    }
}