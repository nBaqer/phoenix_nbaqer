﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Encryption.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the Encryption type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServices.Security
{
    using System;
    using System.Security.Cryptography;
    using System.Text;

    using WapiServices.DataDefinitions;
    using WapiServices.DataDefinitions.Contracts;
    using WapiServices.MiniDataLayer.LogOperation;

    /// <summary>
    /// The encryption.
    /// </summary>
    public static class Encryption
    {
        /// <summary>
        /// The encryption key container name.
        /// </summary>
        private const string XContainerName = "AdvantageKeyContainer";

        /// <summary>
        /// The private key.
        /// </summary>
        private static string keyPrivat;

        /// <summary>
        /// The key public.
        /// </summary>
        private static string keyPublic;

        #region Initializations process, call this when begin the application

        /// <summary>
        /// The initialization encryption.
        /// </summary>
        public static void InitEncryption()
        {
            //// Create a empty list of access keys
            //// listOfKeys = new List<AccessKey>();

            var cp = new CspParameters
            {
                KeyContainerName = XContainerName,
                Flags = CspProviderFlags.UseExistingKey | CspProviderFlags.UseMachineKeyStore // If key does not exists, does not create it!!!
            };

            // Create a new instance of RSACryptoServiceProvider that accesses
            // the key container MyKeyContainerName.
            try
            {
                var rsa = new RSACryptoServiceProvider(cp);
                keyPrivat = rsa.ToXmlString(true);
                keyPublic = rsa.ToXmlString(false);
            }
            catch (Exception)
            {
                // Log Error
                IWapiLog log = new WapiLog();
                log.Comment = "Advantage Encryption Key was not found";
                log.Company = "WAPIPROXY";
                log.DateMod = DateTime.Now;
                log.IsOk = false;
                log.OperationDescription = "INIT";
                log.ServiceInvoqued = "INITWSERV";
                StoreLogOperation.WriteLogEntry(log);
                keyPrivat = string.Empty;
                keyPublic = string.Empty;
            }
        }

        #endregion

        #region Encryption and decryption

        /// <summary>
        /// Return decrypted the Company Code.
        /// </summary>
        /// <param name="encryptedCompanyCode">
        /// The encrypted company code
        /// </param>
        /// <returns>
        /// The decrypted string
        /// </returns>
        /// <remarks> Use only in computer with XP or later, 
        /// if not use Decrypt second parameter to false
        /// </remarks>
        public static string DecryptCompanyCode(string encryptedCompanyCode)
        {
            var text = Convert.FromBase64String(encryptedCompanyCode);
            var rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(keyPrivat);
            var decrypted = rsa.Decrypt(text, true);
            return Encoding.UTF8.GetString(decrypted);
        }

        /// <summary>
        /// This routine is used basically in internal unit test
        /// the proxy only need to decrypt.
        /// </summary>
        /// <param name="data">the data to be encrypted</param>
        /// <returns>
        /// The encrypted Script
        /// </returns>
        public static string Encrypt(string data)
        {
            // Test if exists the key
            if (keyPublic == string.Empty)
            {
                return string.Empty;
            }
            
            // Exists then Encrypt
            var rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(keyPublic);
            var dataToEncrypt = Encoding.UTF8.GetBytes(data);
            var encrypted = rsa.Encrypt(dataToEncrypt, true);
            return Convert.ToBase64String(encrypted);
        }

        #endregion

        /// <summary>
        /// The get public key.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetPublicKey()
        {
            return keyPublic;
        }
    }
}