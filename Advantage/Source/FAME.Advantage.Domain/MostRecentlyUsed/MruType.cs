﻿using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.MostRecentlyUsed
{
    /// <summary>
    /// Type of MRU
    /// </summary>
    public class MruType : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// default constructor
        /// </summary>
        protected MruType()
        {
        }

        /// <summary>
        /// Constructor Public.
        /// </summary>
        /// <param name="mruTypeDescription"></param>
        public MruType(string mruTypeDescription)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            MruTypeDescription = mruTypeDescription;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Property
        /// </summary>
        public virtual string MruTypeDescription { get; protected set; }

    }
}
