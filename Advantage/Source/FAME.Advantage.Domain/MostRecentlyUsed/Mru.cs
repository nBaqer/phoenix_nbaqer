﻿using System;
using FAME.Advantage.Domain.Campuses;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Users;

namespace FAME.Advantage.Domain.MostRecentlyUsed
{
    public class Mru : DomainEntity
    {
        /// <summary>
        /// Protected Constructor
        /// </summary>
        protected Mru(){}


        /// <summary>
        /// default constructor
        /// </summary>
        public Mru(int counter
            , Guid entityId
            , MruType mruType
            , User user
            , Campus campus
            , int? sortOrder
            , string modUser
            , DateTime modDate)
        {
// ReSharper disable DoNotCallOverridableMethodsInConstructor
            Counter = counter;
            EntityId = entityId;
            MruTypeObj = mruType;
            UserObj = user;
            CampusObj = campus;
            SortOrder = sortOrder;
            ModUser = modUser;
            ModDate = modDate;
 // ReSharper restore DoNotCallOverridableMethodsInConstructor
       }


        //Class Properties
        /// <summary>
        /// Counter ?
        /// </summary>
        public virtual int Counter { get; protected set; }

        /// <summary>
        /// Entity ID 
        /// </summary>
        public virtual Guid EntityId { get; protected set; }

        /// <summary>
        /// Type of MRU 4 is Lead
        /// </summary>
        public virtual MruType MruTypeObj { get; protected set; }

        /// <summary>
        /// User associated wit this MRU
        /// </summary>
        public virtual User UserObj { get; set; }

        /// <summary>
        /// Campus
        /// </summary>
        public virtual Campus CampusObj { get; set; }

        /// <summary>
        /// Sort Order?
        /// </summary>
        public virtual int? SortOrder { get; protected set; }

        /// <summary>
        /// Mod User
        /// </summary>
        public virtual string ModUser { get; set; }

        /// <summary>
        /// Mod Date
        /// </summary>
        public virtual DateTime? ModDate { get; set; }

    }
}
