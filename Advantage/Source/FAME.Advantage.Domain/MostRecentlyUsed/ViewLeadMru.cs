﻿using System;
using FAME.Advantage.Domain.Campuses;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Users;

namespace FAME.Advantage.Domain.MostRecentlyUsed
{
    /// <summary>
    /// Domain for the View_LeadMru
    /// </summary>
    public class ViewLeadMru : DomainEntity
    {

        #region Constructors
        /// <summary>
        /// Protected Constructor
        /// </summary>
        protected ViewLeadMru()
        {

        }

        /// <summary>
        /// constructor: Use this to create a instance of the class
        /// </summary>
        public ViewLeadMru( MruType mruType
            , Lead.Lead leadObj
            , User user
            , Campus campus
            , string modUser
            , DateTime modDate)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
           // Counter = counter;
            MruTypeObj = mruType;
            LeadObj = leadObj;
            UserObj = user;
            CampusObj = campus;
            ModUser = modUser;
            ModDate = modDate;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Properties
        /// <summary>
        /// Inverse relation. The entity (lead in this view)
        /// </summary>
        public virtual Lead.Lead LeadObj { get; protected set; }

        /// <summary>
        /// Counter.
        /// The biggest is the last 
        /// </summary>
        public virtual int Counter { get; protected set; }

        /// <summary>
        /// The type of Entity in this must be only 4
        /// </summary>
        public virtual MruType MruTypeObj { get; protected set; }

        /// <summary>
        /// The user that it is associated the MRU
        /// </summary>
        public virtual User UserObj { get; protected set; }

        /// <summary>
        /// The campus that it is associated the MRU
        /// </summary>
        public virtual Campus CampusObj { get; protected set; }

        /// <summary>
        /// The user that modify the record
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// The data of modification.
        /// </summary>
        public virtual DateTime? ModDate { get; protected set; }

        #endregion

        #region Methods

        /// <summary>
        /// Change the counter value. Normally used to 
        /// put the Lead in the top of selection
        /// </summary>
        /// <param name="counter"></param>
        public virtual void ChangeCounter(int counter)
        {
            Counter = counter;
        }

        #endregion
    }
}

