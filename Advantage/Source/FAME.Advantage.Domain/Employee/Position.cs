﻿using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Employee
{
    public class Position : DomainEntity
    {
        protected Position() { }


        public virtual string Code { get; protected set; }
        public virtual string Description { get; protected set; }


    }
}

