﻿using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Employee
{
    public class EmployeeContact : DomainEntity
    {
        protected EmployeeContact() { }

        public EmployeeContact(string wPhone)
        {
            WorkPhone = wPhone;
        }

        public virtual string WorkPhone { get; protected set; }

        public virtual Employee Employee { get; protected set; }


    }
}

