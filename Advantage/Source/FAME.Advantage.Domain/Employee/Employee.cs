﻿using System.Net.Mime;
using FAME.Advantage.Domain.Campuses;
using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FAME.Advantage.Domain.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.StatusesOptions;
using FAME.Advantage.Domain.SystemStuff;


namespace FAME.Advantage.Domain.Employee
{
    using FAME.Advantage.Domain.Campuses.CampusGroups;

    public class Employee : DomainEntity
    {
        protected Employee() { }

        public Employee(string fName, string lName)
        {
            FirstName = fName;
            LastName = lName;
        }

        public virtual string FirstName { get; protected set; }
        public virtual string LastName { get; protected set; }
        public virtual string FullName
        {
            get { return FirstName + " " + LastName; }
            set { value = FirstName +  " " + LastName; }
        }
        public virtual string Address { get; protected set; }
        public virtual string City { get; protected set; }

        public virtual SystemStates State { get; set; }

        public virtual string Zip { get; protected set; }

        public virtual IList<EmployeeContact> EmpContacts { get; protected set; }
        public virtual IList<EmployeeInfo> EmployeeInfo { get; protected set; }
        public virtual StatusOption Status { get; protected set; }

        public virtual Campus Campus { get; protected set; }

        public virtual Guid? SearchCampusId { get; protected set; }
        public virtual string ImageUrl { get; set; }
        public virtual string NotFoundImageUrl { get; set; }

        public virtual string ModUser { get; set; }
        public virtual DateTime? ModDate { get; set; }

         /// <summary>
        /// Fill the campus search string list
        /// </summary>
        public virtual void GetCampusStringList()
        {
            this.SearchCampusId = this.Campus.ID;
        }


    }
}

