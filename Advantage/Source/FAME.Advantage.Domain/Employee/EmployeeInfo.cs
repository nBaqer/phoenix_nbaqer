﻿
using System;
using FAME.Advantage.Domain.Infrastructure.Entities;



namespace FAME.Advantage.Domain.Employee
{
    public class EmployeeInfo : DomainEntity
    {
        protected EmployeeInfo() { }

        public virtual Department Department { get; protected set; }
        public virtual Position Position { get; protected set; }
        public virtual DateTime? HireDate { get; protected set; }

    }
}

