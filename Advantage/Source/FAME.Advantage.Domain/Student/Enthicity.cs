﻿using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Student
{
    /// <summary>
    /// Domain for table Ethnicity
    /// </summary>
    public class Ethnicity : DomainEntity
    {
        /// <summary>
        /// Constructor protected
        /// </summary>
        protected Ethnicity()
        {
        }

        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="code"></param>
        /// <param name="description"></param>
        public Ethnicity(string code, string description)
        {
            // ReSharper disable VirtualMemberCallInContructor
            Code = code;
            Description = description;
             // ReSharper restore VirtualMemberCallInContructor
       }

        /// <summary>
        /// Code
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Description
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// Campus Groups
        /// </summary>
        public virtual CampusGroup CampusGroupObj { get; protected set; }

        /// <summary>
        /// Status Active inactive of the phone type.
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }
    }
}
