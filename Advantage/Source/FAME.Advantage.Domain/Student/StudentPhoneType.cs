﻿using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student
{
    public class StudentPhoneType : DomainEntity
    {
        public StudentPhoneType()
        {
        }

        public virtual string Code { get; protected set; }
        public virtual string Description { get; protected set; }
    }
}
