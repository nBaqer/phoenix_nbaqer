﻿using System.Linq;
using FAME.Advantage.Domain.Catalogs;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Requirements.Documents;
using FAME.Advantage.Domain.StatusesOptions;
using FAME.Advantage.Domain.Student.Enrollments;
using System;
using System.Collections.Generic;


namespace FAME.Advantage.Domain.Student
{
    public class Student : DomainEntity
    {
        protected Student() { }

        public Student(string firstName, string middleName, string lastName, StatusOption status, Gender gender, Ethnicity ethnicity, IEnumerable<Enrollment> enrollments)
        {
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            Status = Status;
            Gender = gender;
            Ethnicity = ethnicity;
            Enrollments = enrollments;
        }

        public virtual string FirstName { get; protected set; }
        public virtual string MiddleName { get; protected set; }
        public virtual string LastName { get; protected set; }
        public virtual string FullName
        {
            get { return FirstName + (MiddleName != null ? (" " + MiddleName) : "") + " " + LastName; }
            set { value = FirstName + (MiddleName != null ? (" " + MiddleName) : "") + " " + LastName; }
        }

        public virtual string FullNameLastFirst
        {
            get { return LastName + ", " + FirstName + " " + (MiddleName != null ? (" " + MiddleName) : ""); }
            set { value = LastName + ", " + FirstName + " " + (MiddleName != null ? (" " + MiddleName) : ""); }
        }

        public virtual string SSN { get; protected set; }
        public virtual string StudentNumber { get; protected set; }
        public virtual string WorkEmail { get; protected set; }
        public virtual string HomeEmail { get; protected set; }
        public virtual DateTime? DOB { get; protected set; }
        public virtual Gender Gender { get; protected set; }
        public virtual Ethnicity Ethnicity { get; protected set; }
        public virtual StatusOption Status { get; protected set; }
        //public virtual IEnumerable<DocumentHistory> DocumentHistory { get; protected set; }
        public virtual string ImageUrl{ get; protected set; }
        public virtual string NotFoundImageUrl { get; protected set; }
        public virtual Guid? SearchCampusId { get; protected set; }
        

        public virtual string MaritalStatus { get; protected set; }
        //public virtual string CitizenGuid { get; protected set; }
        public virtual string FamilyIncoming { get; protected set; }
        public virtual string StudentId { get; protected set; }
        public virtual string ModUser { get; protected set; }
        public virtual DateTime? ModDate { get; protected set; }

        /// <summary>
        /// Added for VOYANT.
        /// </summary>
        public virtual Citizenships CitizenshipsObj { get; protected set; }
       
        public virtual IEnumerable<Enrollment> Enrollments { get; protected set; }

        public virtual IEnumerable<StudentPhone> StudentPhones { get; protected set; }


        /// <summary>
        /// Get the image path for the student photo
        /// </summary>
        /// <param name="studentImagePath"></param>
        /// <param name="siteUrl"></param>
        public virtual void GetImageUrl(string studentImagePath, string siteUrl)
        {
            ImageUrl = siteUrl + "/images/face75.png";
            //var docHistoryRecord = DocumentHistory.FirstOrDefault(n => n.DocumentType.ToUpper() == "PHOTOID");
            //if (docHistoryRecord != null)
            //    ImageUrl = studentImagePath + "/" + docHistoryRecord.DocumentType + "/" + docHistoryRecord.ID.ToString() + docHistoryRecord.FileExtension;

            NotFoundImageUrl = siteUrl + "/images/face75_notfound.png";


            var enr = this.Enrollments.OrderByDescending(n => n.EnrollmentDate).FirstOrDefault();
            if (enr != null) this.SearchCampusId = enr.CampusId;
        }

        /// <summary>
        /// Update the Student Status
        /// </summary>
        /// <param name="status">The new status</param>
        public virtual void UpdateStatus(StatusOption status)
        {
            this.Status = status;
        }
    }
}

