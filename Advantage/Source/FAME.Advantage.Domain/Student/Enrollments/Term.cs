﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.StatusesOptions;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    /// <summary>
    /// Domain for Table arTerm
    /// </summary>
    public class Term : DomainEntity
    {
        /// <summary>
        /// Protected Constructor
        /// </summary>
        protected Term()
        {
        }

        /// <summary>
        /// Test Constructor
        /// </summary>
        /// <param name="termCode"></param>
        /// <param name="termDesc"></param>
        /// <param name="isMod"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        protected Term(string termCode, string termDesc, bool isMod, DateTime start, DateTime end)
        {
// ReSharper disable DoNotCallOverridableMethodsInConstructor
            TermCode = termCode;
            TermDescription = termDesc;
            IsModule = isMod;
            StartDate = start;
            EndDate = end;
 // ReSharper restore DoNotCallOverridableMethodsInConstructor
       }

        /// <summary>
        /// The code
        /// </summary>
        public virtual string TermCode { get; protected set; }

        /// <summary>
        /// The description
        /// </summary>
        public virtual string TermDescription { get; protected set; }   
     
        /// <summary>
        /// Is module
        /// </summary>
        public virtual bool IsModule { get; set; }

        /// <summary>
        /// The campus group Id that belong
        /// </summary>
        public virtual Guid CampGrpId { get; set; }

        /// <summary>
        /// The Term start date
        /// </summary>
        public virtual DateTime StartDate { get; protected set; }

        /// <summary>
        /// The Term end date
        /// </summary>
        public virtual DateTime EndDate { get; protected set; }

        /// <summary>
        /// The status Object
        /// </summary>
        public virtual StatusOption Status { get; protected set; }

        /// <summary>
        /// The campus group object that belong
        /// </summary>
        public virtual CampusGroup CampusGroup { get; protected set; }

        /// <summary>
        /// The program that the Term belong
        /// </summary>
        public virtual ArPrograms ProgramObj { get; protected set; }

        /// <summary>
        /// The list of class that the Term has.
        /// </summary>
        public virtual IList<ArClassSections> ClassSectionsList { get; protected set; }
    }
}



