﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    /// <summary>
    ///  Table faFundSources
    /// </summary>
    public class FundSources: DomainEntity
    {
        protected FundSources() { }

        /// <summary>
        /// Table faFundSources
        /// </summary>
        /// <param name="fundSourceCode"></param>
        /// <param name="studentAwardList"></param>
        public FundSources(string fundSourceCode, IList<StudentAwards> studentAwardList)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            FundSourceCode = fundSourceCode;
            StudentAwardList = studentAwardList;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public virtual string FundSourceCode { get; protected set; }
        public virtual IList<StudentAwards> StudentAwardList { get; protected set; }

    }
}
