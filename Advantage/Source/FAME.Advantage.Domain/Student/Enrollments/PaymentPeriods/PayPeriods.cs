﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;


namespace FAME.Advantage.Domain.Student.Enrollments.PaymentPeriods
{
    public class PayPeriods : DomainEntity
    {
        protected PayPeriods()
        {
        }

        public virtual Increments Increment { get; protected set; }
        public virtual Int32 PeriodNumber { get; protected set; }
        public virtual decimal IncrementValue { get; protected set; }
        public virtual decimal CumulativeValue { get; protected set; }
        public virtual decimal ChargeAmount { get; protected set; }
        public virtual bool IsCharged{ get; protected set; }
        public virtual string ModUser { get; protected set; }
        public virtual DateTime ModDate { get; protected set; }




    }
}



