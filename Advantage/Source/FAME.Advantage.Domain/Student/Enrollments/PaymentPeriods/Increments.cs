﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;


namespace FAME.Advantage.Domain.Student.Enrollments.PaymentPeriods
{
    public class Increments : DomainEntity
    {
        protected Increments()
        {
        }

        public virtual Int32 IncrementType { get; protected set; }
        public virtual Int32 IncrementName { get; protected set; }
        public virtual DateTime EffectiveDate { get; protected set; }
        public virtual decimal ExcessiveAdbsencePercentage { get; protected set; }
        public virtual string ModUser { get; protected set; }
        public virtual DateTime ModDate { get; protected set; }

    }
}



