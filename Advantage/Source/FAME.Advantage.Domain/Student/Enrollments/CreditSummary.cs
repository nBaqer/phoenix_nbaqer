﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreditSummary.cs" company="FAME Inc.">
//  FAME.Advantage.Domain.Student.Enrollments 
// </copyright>
// <summary>
//   Defines the CreditSummary type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Student.Enrollments
{
    using System;

    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The credit summary.
    /// </summary>
    public class CreditSummary : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreditSummary"/> class.
        /// </summary>
        public CreditSummary()
        {
        }

        /// <summary>
        /// Gets or sets the Student Enrollment id.
        /// </summary>
        public virtual Guid StuEnrollId { get; protected set; }

        /// <summary>
        /// Gets or sets the term id.
        /// </summary>
        public virtual Guid TermId { get; protected set; }

        /// <summary>
        /// Gets or sets the term description.
        /// </summary>
        public virtual string TermDescription { get; protected set; }

        /// <summary>
        /// Gets or sets the requirement id.
        /// </summary>
        public virtual Guid RequirementId { get; protected set; }

        /// <summary>
        /// Gets or sets the requirement description.
        /// </summary>
        public virtual string RequirementDescription { get; protected set; }

        /// <summary>
        /// Gets or sets the class section id.
        /// </summary>
        public virtual string ClassSectionId { get; protected set; }

        /// <summary>
        /// Gets or sets the credits earned.
        /// </summary>
        public virtual decimal CreditsEarned { get; protected set; }

        /// <summary>
        /// Gets or sets the credits attempted.
        /// </summary>
        public virtual decimal CreditsAttempted { get; protected set; }

        /// <summary>
        /// Gets or sets the current score.
        /// </summary>
        public virtual decimal CurrentScore { get; protected set; }

        /// <summary>
        /// Gets or sets the current grade.
        /// </summary>
        public virtual string CurrentGrade { get; protected set; }

        /// <summary>
        /// Gets or sets the final score.
        /// </summary>
        public virtual decimal FinalScore { get; protected set; }

        /// <summary>
        /// Gets or sets the final grade.
        /// </summary>
        public virtual string FinalGrade { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether completed.
        /// </summary>
        public virtual bool Completed { get; protected set; }

        /// <summary>
        /// Gets or sets the final GPA.
        /// </summary>
        public virtual decimal FinalGpa { get; protected set; }

        /// <summary>
        /// Gets or sets the product weighted average credits GPA.
        /// </summary>
        public virtual decimal ProductWeightedAverageCreditsGpa { get; protected set; }

        /// <summary>
        /// Gets or sets the count weighted average credits.
        /// </summary>
        public virtual decimal CountWeightedAverageCredits { get; protected set; }

        /// <summary>
        /// Gets or sets the product simple average credits GPA.
        /// </summary>
        public virtual decimal ProductSimpleAverageCreditsGpa { get; protected set; }

        /// <summary>
        /// Gets or sets the count simple average credits.
        /// </summary>
        public virtual decimal CountSimpleAverageCredits { get; protected set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public virtual DateTime? ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the term GPA simple.
        /// </summary>
        public virtual decimal TermGpaSimple { get; protected set; }

        /// <summary>
        /// Gets or sets the term GPA weighted.
        /// </summary>
        public virtual decimal TermGpaWeighted { get; protected set; }

        /// <summary>
        /// Gets or sets the Course Credits.
        /// </summary>
        public virtual decimal CourseCredits { get; protected set; }

        /// <summary>
        /// Gets or sets the cumulative GPA.
        /// </summary>
        public virtual decimal CumulativeGpa { get; protected set; }

        /// <summary>
        /// Gets or sets the cumulative GPA simple.
        /// </summary>
        public virtual decimal CumulativeGpaSimple { get; protected set; }

        /// <summary>
        /// Gets or sets the financial aid credits earned.
        /// </summary>
        public virtual decimal FinancialAidCreditsEarned { get; protected set; }

        /// <summary>
        /// Gets or sets the average.
        /// </summary>
        public virtual decimal Average { get; protected set; }

        /// <summary>
        /// Gets or sets the cum average.
        /// </summary>
        public virtual decimal CumAverage { get; protected set; }

        /// <summary>
        /// Gets or sets the term start date.
        /// </summary>
        public virtual DateTime? TermStartDate { get; protected set; }
    }
}
