﻿#region header
// // --------------------------------------------------------------------------------------------
// // Advantage - FAME.Advantage.Domain - ArPrograms.cs
// // Author: JAGG
// // © Copyright FAME 2014, 2015
// //---------------------------------------------------------------------------------------------
#endregion

using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    using FAME.Advantage.Domain.Catalogs;

    /// <summary>
    /// Domain for Table ArPrograms
    /// </summary>
    public class ArPrograms: DomainEntity
    {
        /// <summary>
        /// Protected constructor
        /// </summary>
        protected ArPrograms() { }

        /// <summary>
        /// Constructor for test
        /// </summary>
        /// <param name="progCode">Program Code</param>
        /// <param name="progDescrip">Description</param>
        /// <param name="statusesObj">Status active inactive</param>
        public ArPrograms(string progCode, string progDescrip, SyStatuses statusesObj)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            ProgCode = progCode;
            ProgDescrip = progDescrip;
            StatusesObj = statusesObj;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes the instance for ArPrograms
        /// </summary>
        /// <param name="progCode">Program Code</param>
        /// <param name="progDescrip">Description</param>
        /// <param name="statusesObj">Status active inactive</param>
        /// <param name="calendartype">clock hour or not</param>
        public ArPrograms(string progCode, string progDescrip, SyStatuses statusesObj,SyAcademicCalendars calendartype)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            ProgCode = progCode;
            ProgDescrip = progDescrip;
            StatusesObj = statusesObj;
            this.Calendartype = calendartype;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }
        /// <summary>
        /// Program Code
        /// </summary>
        public virtual string ProgCode { get; protected set; }

        /// <summary>
        /// Program Description
        /// </summary>
        public virtual string ProgDescrip { get; protected set; }

        /// <summary>
        /// If program is active or not
        /// </summary>
        public virtual SyStatuses StatusesObj { get; protected set; }

        /// <summary>
        /// Campus Group that belong.
        /// </summary>
        public virtual CampusGroup CampusGroupObj { get; protected set; }

        /// <summary>
        /// Leads associated with this program
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }

        /// <summary>
        /// List of program version that the program own
        /// </summary>
        public virtual IList<ProgramVersion> ProgramVersionList { get; protected set; }

        /// <summary>
        /// Gets or sets the calendar type. (It tells if the program belongs to clock hour or not)
        /// </summary>
        public virtual SyAcademicCalendars Calendartype { get; protected set; }
    }
}
