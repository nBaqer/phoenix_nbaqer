﻿using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    public class LoaReasons: DomainEntity
    {
        protected LoaReasons() { }

        public LoaReasons(string code, string description)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Code = code;
            Description = description;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public virtual string Code { get; protected set; }
        public virtual string Description  { get; protected set; }
    }
}
