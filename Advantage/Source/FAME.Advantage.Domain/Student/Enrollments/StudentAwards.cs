﻿using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    public class StudentAwards: DomainEntity
    {
        protected StudentAwards() { }

        public StudentAwards(decimal grossAmount, FundSources awardTypeObj, Enrollment enrollmentObj)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            GrossAmount = grossAmount;
            AwardTypeObj = awardTypeObj;
            EnrollmentObj = enrollmentObj;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public virtual decimal GrossAmount { get; protected set; }
        public virtual FundSources AwardTypeObj { get; protected set; }
        public virtual Enrollment EnrollmentObj { get; protected set; }



    }
}
