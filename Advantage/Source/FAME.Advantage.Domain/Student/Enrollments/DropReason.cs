﻿using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    public class DropReason: DomainEntity
    {
        protected DropReason() { }

        public DropReason(string description)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Description = description;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public virtual string Description { get; protected set; }
    }
}

