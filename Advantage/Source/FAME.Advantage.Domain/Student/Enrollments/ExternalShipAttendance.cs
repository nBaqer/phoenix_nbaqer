﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExternalShipAttendance.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Domain for Table arExternshipAttendance.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Student.Enrollments
{
    using System;

    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// Domain for Table arExternshipAttendance.
    /// </summary>
    public class ExternalShipAttendance : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExternalShipAttendance"/> class.
        /// </summary>
        /// <param name="attendedDate">
        /// The attended date.
        /// </param>
        /// <param name="stuEnrollmentObj">
        /// The stu enrollment obj.
        /// </param>
        public ExternalShipAttendance(DateTime attendedDate, Enrollment stuEnrollmentObj)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.AttendedDate = attendedDate;
            this.StuEnrollmentObj = stuEnrollmentObj;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExternalShipAttendance"/> class.
        /// </summary>
        protected ExternalShipAttendance()
        {
        }

        // point to ClassSections

        /// <summary>
        /// Gets or sets the attended date.
        /// </summary>
        public virtual DateTime AttendedDate { get; protected set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the stu enrollment obj.
        /// </summary>
        public virtual Enrollment StuEnrollmentObj { get; protected set; }
    }
}
