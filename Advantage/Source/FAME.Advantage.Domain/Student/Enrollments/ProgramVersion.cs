﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramVersion.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Student.Enrollments.ProgramVersion
// </copyright>
// <summary>
//   Domain for table arPrgVersion
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Student.Enrollments
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.Domain.Campuses.CampusGroups;
    using FAME.Advantage.Domain.Catalogs;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Requirements;
    using FAME.Advantage.Domain.StatusesOptions;

    /// <summary>
    /// Domain for table ARPRGVersion
    /// </summary>
    public class ProgramVersion : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProgramVersion"/> class.
        /// </summary>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        public ProgramVersion(string code, string description)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.Code = code;
            this.Description = description;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="ProgramVersion"/> class.
        /// </summary>
        protected ProgramVersion()
        {
        }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// Gets or sets the track tardies.
        /// </summary>
        public virtual Int16 TrackTardies { get; protected set; }

        /// <summary>
        /// Gets or sets the tardies making absence.
        /// </summary>
        public virtual int? TardiesMakingAbsence { get; protected set; }

        /// <summary>
        /// Gets or sets the campus group id.
        /// </summary>
        public virtual Guid CampGrpId { get; protected set; }

        /// <summary>
        /// Gets or sets the status of the program.
        /// </summary>
        public virtual StatusOption Status { get; protected set; }

        /// <summary>
        /// Gets or sets the campus group.
        /// </summary>
        public virtual CampusGroup CampusGroup { get; protected set; }

        /// <summary>
        /// Gets or sets the program object.
        /// </summary>
        public virtual ArPrograms ProgramObj { get; protected set; }

        /// <summary>
        /// Gets or sets the program group object.
        /// </summary>
        public virtual ArProgramGroup ProgramGroupObj { get; protected set; }

        /// <summary>
        /// Gets or sets the attendance type.
        /// </summary>
        public virtual AttendanceUnitType AttendanceType { get; protected set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the requirement program version bridge.
        /// </summary>
        public virtual IList<RequirementProgramVersionBridge> RequirementProgramVersionBridge { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether use time clock.
        /// </summary>
        public virtual bool UseTimeClock { get; protected set; }
    }
}
