﻿using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    public class InstructionType: DomainEntity
    {
        protected InstructionType() { }

        public InstructionType(string instructionTypeDescrip)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            InstructionTypeDescrip = instructionTypeDescrip;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public virtual string InstructionTypeDescrip { get; protected set; }
    }
}