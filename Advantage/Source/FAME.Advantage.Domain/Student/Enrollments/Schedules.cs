﻿namespace FAME.Advantage.Domain.Student.Enrollments
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// Domain Entity for Student Schedules
    /// </summary>
    public class Schedules : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Schedules"/> class. 
        /// </summary>
        protected Schedules()
        {
        }

        /// <summary>
        /// Gets or sets the stu enroll id.
        /// </summary>
        public virtual Guid StuEnrollId { get; protected set; }

        /// <summary>
        /// Gets or sets the schedule id.
        /// </summary>
        public virtual Guid ScheduleId { get; protected set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public virtual DateTime? StartDate { get; protected set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public virtual DateTime? EndDate { get; protected set; }

        /// <summary>
        /// Gets or sets the active.
        /// </summary>
        public virtual bool? Active { get; protected set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public virtual DateTime? ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        public virtual string Source { get; protected set; }
    }
}
