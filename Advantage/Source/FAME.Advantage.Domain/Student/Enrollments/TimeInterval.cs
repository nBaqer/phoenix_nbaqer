﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    /// <summary>
    /// Time interval domain for table cmTimeInterval
    /// </summary>
    public class TimeInterval: DomainEntity
    {
        /// <summary>
        /// Constructor protected
        /// </summary>
        protected TimeInterval() { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="timeIntervalDescrip"></param>
        public TimeInterval(DateTime timeIntervalDescrip)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            TimeIntervalDescrip = timeIntervalDescrip;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// This is a date time with the time interval description. only the time part has sense.
        /// </summary>
        public virtual DateTime? TimeIntervalDescrip { get; protected set; }

        /// <summary>
        /// Status Active inactive
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// CampusGrp object
        /// </summary>
        public virtual CampusGroup CampusGroupObj { get; protected set; }

        /// <summary>
        /// List of Leads connected with a specific TimeInterval Description
        /// </summary>
        public virtual IList<Lead.Lead> LeadsList { get; protected set; }

       
    }
}

