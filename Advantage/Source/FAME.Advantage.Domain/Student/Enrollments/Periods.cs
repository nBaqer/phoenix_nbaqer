﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    public class Periods: DomainEntity
    {
        protected Periods() { }

        public Periods(string periodDescrip, TimeInterval startTimeIntervalObj, TimeInterval endTimeIntervalObj)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            PeriodDescrip = periodDescrip;
            StartTimeIntervalObj = startTimeIntervalObj;
            EndTimeIntervalObj = endTimeIntervalObj;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public virtual string PeriodDescrip { get; protected set; }
        public virtual TimeInterval StartTimeIntervalObj { get; protected set; }
        public virtual TimeInterval EndTimeIntervalObj { get; protected set; }
        public virtual IList<WorkDays> WorkDaysList { get; protected set; }

    }
}
