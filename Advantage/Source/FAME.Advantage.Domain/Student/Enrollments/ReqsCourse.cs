﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    /// <summary>
    /// Domain Table arReqs
    /// </summary>
    public class ReqsCourse : DomainEntity
    {
        protected ReqsCourse() { }

        public ReqsCourse(string description, AttendanceUnitType unitType)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Description = description;
            UnitType = unitType;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public virtual string Description { get; protected set; }
        public virtual string Code { get; protected set; }
        public virtual bool IsTrackTardies { get; protected set; }
        public virtual int? TardiesMakingAbsence { get; protected set; }
        public virtual AttendanceUnitType UnitType { get; protected set; }

    }
}
