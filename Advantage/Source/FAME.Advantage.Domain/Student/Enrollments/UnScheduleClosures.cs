﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    public class UnScheduleClosures: DomainEntity
    {
        protected UnScheduleClosures() { }

        public UnScheduleClosures(ArClassSections classSection, DateTime starDateTime, DateTime endDateTime)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            StartDate = starDateTime;
            EndDate = endDateTime;
            ClassSection = classSection;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public virtual DateTime StartDate { get; protected set; }
        public virtual DateTime EndDate { get; protected set; }
        public virtual ArClassSections ClassSection { get; protected set; }
    }
}