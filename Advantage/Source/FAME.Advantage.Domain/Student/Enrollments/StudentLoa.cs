﻿using System;

using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    public class StudentLoa : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StudentLoa"/> class. 
        /// </summary>
        protected StudentLoa()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentLoa"/> class. 
        /// </summary>
        /// <param name="stuEnrollment">
        /// The stu Enrollment.
        /// </param>
        /// <param name="starDateTime">
        /// The star Date Time.
        /// </param>
        /// <param name="endDateTime">
        /// The end Date Time.
        /// </param>
        public StudentLoa(Enrollment stuEnrollment, DateTime starDateTime, DateTime endDateTime)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.StartDate = starDateTime;
            this.EndDate = endDateTime;
            this.StuEnrollment = stuEnrollment;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentLoa"/> class. 
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="starDateTime">
        /// The star Date Time.
        /// </param>
        /// <param name="endDateTime">
        /// The end Date Time.
        /// </param>
        /// <param name="studentStatusChangeId">
        /// The student Status Change Id.
        /// </param>
        /// <param name="statusCodeId">
        /// The status Code Id.
        /// </param>
        /// <param name="modUser">
        /// The mod User.
        /// </param>
        public StudentLoa(Enrollment enrollment, DateTime starDateTime, DateTime endDateTime, Guid studentStatusChangeId, Guid statusCodeId, string modUser, LoaReasons reason)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.StartDate = starDateTime;
            this.EndDate = endDateTime;
            this.StudentStatusChangeId = studentStatusChangeId;
            this.StatusCodeId = statusCodeId;
            this.ModUser = modUser;
            this.LOAReturnDate = endDateTime;
            this.LoaRequestDate = DateTime.Now;
            this.ModDate = DateTime.Now;
            this.StuEnrollment = enrollment;
            this.LoaReasonsObj = reason;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public virtual DateTime StartDate { get; protected set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public virtual DateTime EndDate { get; protected set; }

        /// <summary>
        /// Gets or sets the loa return date.
        /// </summary>
        public virtual DateTime? LOAReturnDate { get; protected set; }

        /// <summary>
        /// Gets or sets the stu enrollment.
        /// </summary>
        public virtual Enrollment StuEnrollment { get; protected set; }

        /// <summary>
        /// Gets or sets the loa reasons object.
        /// </summary>
        public virtual LoaReasons LoaReasonsObj { get; protected set; }

        /// <summary>
        /// Gets or sets the student status change id.
        /// </summary>
        public virtual Guid? StudentStatusChangeId { get; protected set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public virtual DateTime? ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets the status code id.
        /// </summary>
        public virtual Guid? StatusCodeId { get; protected set; }

        /// <summary>
        /// Gets or sets the loa request date.
        /// </summary>
        public virtual DateTime? LoaRequestDate { get; protected set; }

        /// <summary>
        /// Update Student Suspension Entity based on parameters 
        /// </summary>
        /// <param name="endDate">
        /// The end Date.
        /// </param>
        /// <param name="loaReturnDate">
        /// The loa Return Date.
        /// </param>
        public virtual void Update(DateTime endDate, DateTime loaReturnDate)
        {
            this.EndDate = endDate;
            this.LOAReturnDate = loaReturnDate;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="studentStatusChangeId">
        /// The student status change id.
        /// </param>
        /// <param name="statusCodeId">
        /// The Status Code Id.
        /// </param>
        /// <param name="modUser">
        /// The modified user
        /// </param>
        public virtual void Update(Guid studentStatusChangeId, Guid statusCodeId, string modUser)
        {
            this.StudentStatusChangeId = studentStatusChangeId;
            this.StatusCodeId = statusCodeId;
            this.ModUser = modUser;
            this.ModDate = DateTime.Now;
        }
    }
}
