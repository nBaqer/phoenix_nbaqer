﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    public class ArClassSections : DomainEntity
    {
        protected ArClassSections() { }

        public ArClassSections(IList<ArResults> results, Term term)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            ResultList = results;
            TermEntity = term;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public virtual Term TermEntity { get; protected set; }
        public virtual ReqsCourse RequerimentCourse  { get; protected set; }
        public virtual string ClassSection { get; protected set; }
        public virtual IList<ArResults> ResultList { get; protected set; }
        public virtual DateTime StartDate { get; protected set; }
        public virtual DateTime EndDate { get; protected set; }
        // This field has a relation many to one with UserID in syUsers
        // The current se in VOYANTdoes not require to mapping the relation.
        public virtual string InstructorId { get; protected set; }
        public virtual IList<UnScheduleClosures> UnScheduleClosuresList { get; protected set; }
        public virtual IList<ClassSectionMeeting> ClassSectionMeetingList { get; protected set; }
        public virtual IList<ClassSectionAttendance> ClassSectionAttendanceList { get; set; }
    }
}
