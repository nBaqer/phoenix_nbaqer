﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentSuspensions.cs" company="FAME Inc.">
//   FAME.Advantage.Domain.Student.Enrollments
// </copyright>
// <summary>
//   The student suspensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Student.Enrollments
{
    using System;

    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The student suspensions.
    /// </summary>
    public class StudentSuspensions : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StudentSuspensions"/> class.
        /// </summary>
        protected StudentSuspensions()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentSuspensions"/> class.
        /// </summary>
        /// <param name="stuEnrollment">
        /// The stu enrollment.
        /// </param>
        /// <param name="starDateTime">
        /// The star date time.
        /// </param>
        /// <param name="endDateTime">
        /// The end date time.
        /// </param>
        /// <param name="modUser">
        /// The mod user.
        /// </param>
        /// <param name="reason">
        /// The reason.
        /// </param>
        /// <param name="studentStatusChangeId">
        /// The student status change id.
        /// </param>
        public StudentSuspensions(Enrollment stuEnrollment, DateTime starDateTime, DateTime endDateTime, string modUser, string reason, Guid? studentStatusChangeId)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.StartDate = starDateTime;
            this.EndDate = endDateTime;
            this.Reason = reason;
            this.StuEnrollment = stuEnrollment;
            this.ModUser = modUser;
            this.ModDate = DateTime.Now;
            this.StudentStatusChangeId = studentStatusChangeId;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public virtual DateTime StartDate { get; protected set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public virtual DateTime EndDate { get; protected set; }

        /// <summary>
        /// Gets or sets the reason.
        /// </summary>
        public virtual string Reason { get; protected set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public virtual DateTime? ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets the student status change id.
        /// </summary>
        public virtual Guid? StudentStatusChangeId { get; protected set; }

        /// <summary>
        /// Gets or sets the stu enrollment.
        /// </summary>
        public virtual Enrollment StuEnrollment { get; protected set; }

        /// <summary>
        /// Update Student Suspension Entity based on parameters 
        /// </summary>
        /// <param name="endDate">
        /// The end Date.
        /// </param>
        public virtual void Update(DateTime endDate)
        {
            this.EndDate = endDate;
        }
    }
}
