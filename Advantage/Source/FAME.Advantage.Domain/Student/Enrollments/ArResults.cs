﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArResults.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Domain for ArResults information.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Student.Enrollments
{
    using System;

    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// Domain for <see cref="ArResults"/> information.
    /// </summary>
    public class ArResults : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArResults"/> class.
        /// </summary>
        /// <param name="classSections">
        /// The class sections.
        /// </param>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        public ArResults(ArClassSections classSections, Enrollment enrollment)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.ClassSections = classSections;
            this.StuEnrollment = enrollment;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArResults"/> class.
        /// </summary>
        protected ArResults()
        {
        }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        public virtual decimal? Score { get; protected set; }

        /// <summary>
        /// Gets or sets the modification date.
        /// </summary>
        public virtual DateTime? ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the class sections.
        /// </summary>
        public virtual ArClassSections ClassSections { get; protected set; }

        /// <summary>
        /// Gets or sets the student enrollment.
        /// </summary>
        public virtual Enrollment StuEnrollment { get; protected set; }

        /// <summary>
        /// Gets or sets the grade system details object.
        /// </summary>
        public virtual GradeSystemDetails GradeSystemDetailsObj { get; protected set; }
    }
}
