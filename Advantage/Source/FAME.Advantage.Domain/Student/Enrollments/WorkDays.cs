﻿using System.Collections;
using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    public class WorkDays: DomainEntity
    {
        protected WorkDays() { }

        public WorkDays(string workDaysDescrip, int vieworder)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            WorkDaysDescrip = workDaysDescrip;
            ViewOrder = vieworder;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public virtual string WorkDaysDescrip { get; protected set; }
        public virtual int ViewOrder { get; protected set; }

        public virtual IList<Periods> PeriodList { get; protected set; }
    }
}
