﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentAttendanceSummary.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the StudentAttendanceSummary type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Student.Enrollments
{
    using System;

    using Infrastructure.Entities;

    /// <summary>
    /// The student attendance summary.
    /// </summary>
    public class StudentAttendanceSummary : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StudentAttendanceSummary"/> class.
        /// </summary>
        protected StudentAttendanceSummary()
        {
        }

        /// <summary>
        /// Gets or sets the enrollment object.
        /// </summary>
        public virtual Enrollment EnrollmentObj { get; protected set; }

        /// <summary>
        /// Gets or sets the class sections object.
        /// </summary>
        public virtual ArClassSections ClassSectionsObj { get; protected set; }

        /// <summary>
        /// Gets or sets the term object.
        /// </summary>
        public virtual Term TermObj { get; protected set; }

        /// <summary>
        /// Gets or sets the actual.
        /// </summary>
        public virtual int? Actual { get; protected set; }

        /// <summary>
        /// Gets or sets the attendance date.
        /// </summary>
        public virtual DateTime? AttendanceDate { get; protected set; }

        /// <summary>
        /// Gets or sets the student attended date.
        /// </summary>
        public virtual DateTime? StudentAttendedDate { get; protected set; }

        /// <summary>
        /// Gets or sets the scheduled days.
        /// </summary>
        public virtual decimal? ScheduledDays { get; protected set; }

        /// <summary>
        /// Gets or sets the actual days.
        /// </summary>
        public virtual decimal? ActualDays { get; protected set; }

        /// <summary>
        /// Gets or sets the actual running scheduled days.
        /// </summary>
        public virtual decimal? ActualRunningScheduledDays { get; protected set; }

        /// <summary>
        /// Gets or sets the actual running present days.
        /// </summary>
        public virtual decimal? ActualRunningPresentDays { get; protected set; }

        /// <summary>
        /// Gets or sets the actual running absent days.
        /// </summary>
        public virtual decimal? ActualRunningAbsentDays { get; protected set; }

        /// <summary>
        /// Gets or sets the actual running makeup days.
        /// </summary>
        public virtual decimal? ActualRunningMakeupDays { get; protected set; }

        /// <summary>
        /// Gets or sets the actual running tardy days.
        /// </summary>
        public virtual decimal? ActualRunningTardyDays { get; protected set; }

        /// <summary>
        /// Gets or sets the adjusted present days.
        /// </summary>
        public virtual decimal? AdjustedPresentDays { get; protected set; }

        /// <summary>
        /// Gets or sets the adjusted absent days.
        /// </summary>
        public virtual decimal? AdjustedAbsentDays { get; protected set; }

        /// <summary>
        /// Gets or sets the attendance track type.
        /// </summary>
        public virtual string AttendanceTrackType { get; protected set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public virtual string ModifiedUser { get; protected set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public virtual DateTime? ModifiedDate { get; protected set; }

        /// <summary>
        /// Gets or sets the actual present days convert to hours calculated.
        /// </summary>
        public virtual decimal? ActualPresentDaysConvertToHoursCalc { get; protected set; }

        /// <summary>
        /// Gets or sets the scheduled hours.
        /// </summary>
        public virtual decimal? ScheduledHours { get; protected set; }

        /// <summary>
        /// Gets or sets the actual absent days convert to hours calculated.
        /// </summary>
        public virtual decimal? ActualAbsentDaysConvertToHoursCalc { get; protected set; }

        /// <summary>
        /// Gets or sets the term start date.
        /// </summary>
        public virtual decimal? TermStartDate { get; protected set; }

        /// <summary>
        /// Gets or sets the actual present days convert to hours.
        /// </summary>
        public virtual decimal? ActualPresentDaysConvertToHours { get; protected set; }

        /// <summary>
        /// Gets or sets the actual absent days convert to hours.
        /// </summary>
        public virtual decimal? ActualAbsentDaysConvertToHours { get; protected set; }

        /// <summary>
        /// Gets or sets the actual tardy days convert to hours.
        /// </summary>
        public virtual decimal? ActualTardyDaysConvertToHours { get; protected set; }

        /// <summary>
        /// Gets or sets the term description.
        /// </summary>
        public virtual decimal? TermDescrip { get; protected set; }

        /// <summary>
        /// Gets or sets the tardies making absence.
        /// </summary>
        public virtual int? TardiesMakingAbsence { get; protected set; }

        /// <summary>
        /// Gets or sets the is excused.
        /// </summary>
        public virtual int? IsExcused { get; protected set; }

        /// <summary>
        /// Gets or sets the actual running excused days.
        /// </summary>
        public virtual int? ActualRunningExcusedDays { get; protected set; }

        /// <summary>
        /// Gets or sets the is tardy.
        /// </summary>
        public virtual int? IsTardy { get; protected set; }
    }
}
