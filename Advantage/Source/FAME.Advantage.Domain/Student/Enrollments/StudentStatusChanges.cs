﻿using System;
using FAME.Advantage.Domain.Campuses;
using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    public class StudentStatusChanges: DomainEntity
    {
        protected StudentStatusChanges() { }

        public StudentStatusChanges(
            DateTime changeOfDate, 
            Enrollment enrollment,
            UserDefStatusCode originalEnrollmentStatusObj,
            UserDefStatusCode newEnrollmentStatusObj,
            Campus campusObj,
            DropReason dropReasonObj)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            DateOfChange = changeOfDate;
            EnrollmentObj = enrollment;
            OriginalEnrollmentStatusObj = originalEnrollmentStatusObj;
            NewEnrollmentStatusObj = newEnrollmentStatusObj;
            CampusObj = campusObj;
            DropReasonObj = dropReasonObj;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }
        public virtual DateTime DateOfChange { get; protected set; }
        public virtual Enrollment EnrollmentObj { get; protected set; }
        public virtual UserDefStatusCode OriginalEnrollmentStatusObj { get; protected set; }
        public virtual UserDefStatusCode NewEnrollmentStatusObj { get; protected set; }
        public virtual Campus   CampusObj { get; protected set; }
        
        public virtual DropReason DropReasonObj { get; protected set; }
    }
}



