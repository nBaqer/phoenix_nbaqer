﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    /// <summary>
    /// Domain Table arGradeSystemDetails
    /// </summary>
    public class GradeSystemDetails: DomainEntity
    {
        protected GradeSystemDetails() { }

        public GradeSystemDetails(string grade, decimal gpa, IList<ArResults> arResultsList)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Grade = grade;
            Gpa = gpa;
            ArResultsList = arResultsList;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        // point to ClassSections
        public virtual  string Grade { get; protected set; }
        public virtual decimal Gpa { get; protected set; }
        public virtual IList<ArResults> ArResultsList { get; protected set; } 
    }
}

