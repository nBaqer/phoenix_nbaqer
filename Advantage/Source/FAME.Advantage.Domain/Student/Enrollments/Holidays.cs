﻿using System;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.StatusesOptions;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    public class Holidays: DomainEntity
    {
        protected Holidays() { }

        public Holidays(string description, DateTime startDate, DateTime endDate, bool allday, CampusGroup campusgroupObj)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Description = description;
            StartDate = startDate;
            EndDate = endDate;
            AllDay = allday;
            CampusGroupObj = campusgroupObj;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public virtual string Description { get; protected set; }
        public virtual DateTime  StartDate { get; protected set; }
        public virtual DateTime? EndDate { get; protected set; }
        public virtual bool AllDay { get; protected set; }
        public virtual CampusGroup CampusGroupObj { get; protected set; }
        public virtual StatusOption StatusOptionObj { get; protected set; }
        public virtual TimeInterval StartTimeIntervalObj { get; protected set; }
        public virtual TimeInterval EndTimeIntervalObj { get; protected set; }
    }
}