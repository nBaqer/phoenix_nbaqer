﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.Infrastructure.Entities;
using NHibernate.Event;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    public class GradeBookResult : DomainEntity
    {
        public virtual ArClassSections ClassSection { get; protected set; }
        public virtual Guid InstrGrdBkWgtDetailId { get; protected set; }
        public virtual decimal? Score { get; protected set; }
        public virtual string Comments { get; protected set; }
        public virtual Enrollment Enrollment { get; protected set; }
        public virtual string ModifiedUser { get; protected set; }
        public virtual DateTime ModifiedDate { get; protected set; }
        public virtual int ResNum { get; protected set; }
        public virtual DateTime? PostDate { get; protected set; }
        public virtual bool IsCompGraded { get; protected set; }
        public virtual bool IsCourseCredited { get; protected set; }
    }
}
