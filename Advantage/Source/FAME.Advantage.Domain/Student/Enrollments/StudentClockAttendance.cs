﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentClockAttendance.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Domain for Table arStudentClockAttendance
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Student.Enrollments
{
    using System;

    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// Domain for Table arStudentClockAttendance
    /// </summary>
    public class StudentClockAttendance: DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StudentClockAttendance"/> class.
        /// </summary>
        /// <param name="recordDate">
        /// The record date.
        /// </param>
        /// <param name="actualHours">
        /// The actual hours.
        /// </param>
        /// <param name="stuEnrollment">
        /// The stu enrollment.
        /// </param>
        public StudentClockAttendance(DateTime recordDate, decimal actualHours, Enrollment stuEnrollment)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.RecordDate = recordDate;
            this.ActualHours = actualHours;
            this.StuEnrollmentObj = stuEnrollment;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentClockAttendance"/> class.
        /// </summary>
        protected StudentClockAttendance()
        {
        }

        /// <summary>
        /// Gets or sets the record date.
        /// </summary>
        public virtual DateTime RecordDate { get; protected set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the actual hours.
        /// </summary>
        public virtual decimal? ActualHours { get; protected set; }

        /// <summary>
        /// Gets or sets the stu enrollment obj.
        /// </summary>
        public virtual Enrollment StuEnrollmentObj { get; protected set; }
    }
}
