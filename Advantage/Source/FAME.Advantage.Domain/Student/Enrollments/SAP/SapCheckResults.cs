﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SapCheckResults.cs" company="FAME Inc.">
//   FAME.Advantage.Domain.Student.Enrollments.SAP
// </copyright>
// <summary>
//   Defines the SapCheckResults type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Student.Enrollments.SAP
{
    using System;

    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The sap check results.
    /// </summary>
    public class SapCheckResults : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SapCheckResults"/> class.
        /// </summary>
        public SapCheckResults()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SapCheckResults"/> class.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <param name="period">
        /// The period.
        /// </param>
        /// <param name="datePerformed">
        /// The date performed.
        /// </param>
        /// <param name="modUser">
        /// The mod user.
        /// </param>
        public SapCheckResults(Guid studentEnrollmentId, int period, DateTime datePerformed, string modUser)
        {
            this.DatePerformed = datePerformed;
            this.StuEnrollId = studentEnrollmentId;
            this.Period = period;
            this.ModUser = modUser;
            this.ModDate = DateTime.Now;
        }

        /// <summary>
        /// Gets or sets the stu enroll id.
        /// </summary>
        public virtual Guid StuEnrollId { get; protected set; }

        /// <summary>
        /// Gets or sets the period.
        /// </summary>
        public virtual int Period { get; protected set; }

        /// <summary>
        /// Gets or sets the date performed.
        /// </summary>
        public virtual DateTime DatePerformed { get; protected set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public virtual DateTime? ModDate { get; protected set; }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="period">
        /// The period.
        /// </param>
        /// <param name="datePerformed">
        /// The date Performed.
        /// </param>
        /// <param name="modUser">
        /// The mod user.
        /// </param>
        public virtual void Update(int period, DateTime datePerformed, string modUser)
        {
            this.Period = period;
            this.DatePerformed = datePerformed;
            this.ModUser = modUser;
            this.ModDate = DateTime.Now;
        }
    }
}
