﻿using System.Collections;
using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    /// <summary>
    /// Hold the domain for table arAttendTypes
    /// </summary>
    public class ArAttendTypes: DomainEntity
    {
        /// <summary>
        /// Protected Constructor
        /// </summary>
        protected ArAttendTypes() { }

        /// <summary>
        /// Types of Attention of the student
        /// </summary>
        /// <param name="code"></param>
        /// <param name="descrip"></param>
        /// <param name="statusObj"></param>
        /// <param name="campusGrpObj"></param>
        public ArAttendTypes(string code, string descrip, SyStatuses statusObj, CampusGroup campusGrpObj)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Code = code;
            Descrip = descrip;
            StatusObj = statusObj;
            CampusGrpObj = campusGrpObj;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Code
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Description
        /// </summary>
        public virtual string Descrip  { get; protected set; }

        /// <summary>
        ///  Status Active / Inactive
        /// </summary>
        public virtual SyStatuses StatusObj { get; protected set; }

        /// <summary>
        /// Campus Group
        /// </summary>
        public virtual CampusGroup CampusGrpObj { get; protected set; }

        /// <summary>
        /// List of leads with this type of attendance 
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }
       
    }
}
