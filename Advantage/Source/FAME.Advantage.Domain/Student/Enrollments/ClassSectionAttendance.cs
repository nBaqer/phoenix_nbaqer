﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassSectionAttendance.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Domain for atClsSectAttendance
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Student.Enrollments
{
    using System;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Messages.Enumerations;

    /// <summary>
    /// Domain for atClsSectAttendance
    /// </summary>
    /// <remarks>
    /// StuEnrollment relations is not mapped you can if you need of course
    /// StudentId is empty in the table, it is not used/
    /// </remarks>
    public class ClassSectionAttendance : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClassSectionAttendance"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="classSectionMeetingObj">
        /// The class section meeting obj.
        /// </param>
        /// <param name="enrollmentObj">
        /// The enrollment obj.
        /// </param>
        /// <param name="classSections">
        /// The class sections.
        /// </param>
        /// <param name="meetDate">
        /// The meet date.
        /// </param>
        /// <param name="actual">
        /// The actual.
        /// </param>
        /// <param name="tardy">
        /// The tardy.
        /// </param>
        /// <param name="excused">
        /// The excused.
        /// </param>
        /// <param name="comments">
        /// The comments.
        /// </param>
        /// <param name="scheduled">
        /// The scheduled.
        /// </param>
        /// <param name="modUser">the modification user</param>
        public ClassSectionAttendance(
            Guid id,
            ClassSectionMeeting classSectionMeetingObj,
            Enrollment enrollmentObj,
            ArClassSections classSections,
            DateTime meetDate,
            decimal actual,
            bool tardy,
            bool excused,
            string comments,
            decimal scheduled,
            string modUser)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.ID = id;
            this.ClassSectionMeetingObj = classSectionMeetingObj;
            this.EnrollmentObj = enrollmentObj;
            this.ClassSectionObj = classSections;
            this.MeetDate = meetDate;
            this.Actual = actual;
            this.Tardy = tardy;
            this.Excused = excused;
            this.Comments = comments;
            this.Scheduled = scheduled;
            this.ModUser = modUser;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClassSectionAttendance"/> class.
        /// </summary>
        protected ClassSectionAttendance()
        {
        }

        /// <summary>
        /// Gets or sets the meet date.
        /// </summary>
        public virtual DateTime MeetDate { get; protected set; }

        /// <summary>
        /// Gets or sets the actual.
        /// </summary>
        public virtual decimal Actual { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether tardy.
        /// </summary>
        public virtual bool Tardy { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether excused.
        /// </summary>
        public virtual bool Excused { get; protected set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        public virtual string Comments { get; protected set; }

        /// <summary>
        /// Gets or sets the scheduled.
        /// </summary>
        public virtual decimal? Scheduled { get; protected set; }

        /// <summary>
        /// Gets or sets the modification user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets the modification date.
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the class section meeting object.
        /// </summary>
        public virtual ClassSectionMeeting ClassSectionMeetingObj { get; protected set; }

        /// <summary>
        /// Gets or sets the class section object.
        /// </summary>
        public virtual ArClassSections ClassSectionObj { get; protected set; }

        /// <summary>
        /// Gets or sets the enrollment object.
        /// </summary>
        public virtual Enrollment EnrollmentObj { get; protected set; }

        /// <summary>
        /// The update make up.
        /// </summary>
        /// <param name="actual">
        /// The actual.
        /// </param>
        /// <param name="tardy">
        /// The tardy.
        /// </param>
        /// <param name="excused">
        /// The excused.
        /// </param>
        /// <param name="scheduled">
        /// The scheduled.
        /// </param>
        /// <param name="comment">
        /// The comment.
        /// </param>
        public virtual void UpdateMakeUp(int actual, bool tardy, bool excused, int scheduled, string comment)
        {
            this.Actual = actual;
            this.Tardy = tardy;
            this.Excused = excused;
            this.Scheduled = scheduled;
            this.Comments = comment;
        }

        /// <summary>
        /// Update to Attendance Status.
        /// </summary>
        /// <param name="attendanceStatus">The attendance status</param>
        public virtual void UpdateAttendanceStatus(Enumerations.AttendanceStatusEnum attendanceStatus)
        {
            switch (attendanceStatus)
            {
                case Enumerations.AttendanceStatusEnum.Unselected:
                    {
                        throw new ArgumentException("Update to unselected is prohibited: Please Call Fame");
                    }

                case Enumerations.AttendanceStatusEnum.Absent:
                    {   // 0001
                        this.Actual = 0;
                        this.Tardy = false;
                        this.Excused = false;
                        this.Scheduled = 1;
                        break;
                    }

                case Enumerations.AttendanceStatusEnum.Excused:
                    {   // 0011
                        this.Actual = 0;
                        this.Tardy = false;
                        this.Excused = true;
                        this.Scheduled = 1;
                        break;
                    }

                case Enumerations.AttendanceStatusEnum.Present:
                    {   // 1001
                        this.Actual = 1;
                        this.Tardy = false;
                        this.Excused = false;
                        this.Scheduled = 1;
                        break;
                    }

                case Enumerations.AttendanceStatusEnum.Tardy:
                    {   // 1101
                        this.Actual = 1;
                        this.Tardy = true;
                        this.Excused = false;
                        this.Scheduled = 1;
                        break;
                    }

                case Enumerations.AttendanceStatusEnum.MakeUp:
                    {   // 1000
                        this.Actual = 1;
                        this.Tardy = false;
                        this.Excused = false;
                        this.Scheduled = 0;
                        break;
                    }

                default:
                    {
                        throw new ArgumentException("Attendance not specific: Please Call Fame");
                    }
            }
        }

        /// <summary>
        /// The update clock minutes.
        /// </summary>
        /// <param name="actual">
        /// The actual.
        /// </param>
        /// <param name="isTardy">
        /// The is tardy.
        /// </param>
        /// <param name="isExcused">
        /// The is excused.
        /// </param>
        public virtual void UpdateClockMinutes(int actual, bool isTardy, bool isExcused)
        {
            this.Actual = actual;
            this.Tardy = isTardy;
            this.Excused = isExcused;
        }
    }
}
