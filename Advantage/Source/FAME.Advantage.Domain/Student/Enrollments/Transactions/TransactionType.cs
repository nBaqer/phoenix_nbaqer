﻿    
    using FAME.Advantage.Domain.Infrastructure.Entities;
    

    namespace FAME.Advantage.Domain.Student.Enrollments.Transactions
    {
        public class TransactionType : DomainEntityWithTypedID<int>
        {
            protected TransactionType()
            {
            }


            public virtual string Description { get; protected set; }


        }
    }



