﻿    using FAME.Advantage.Domain.Campuses.CampusGroups;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.StatusesOptions;

    namespace FAME.Advantage.Domain.Student.Enrollments.Transactions
    {
        public class TransactionCode : DomainEntity
        {
            protected TransactionCode()
            {
            }


            public virtual string Description { get; protected set; }
            public virtual StatusOption Status { get; protected set; }
            public virtual int SysTransCodeId { get; protected set; }

            public virtual CampusGroup CampusGroup { get; protected set; }


        }
    }



