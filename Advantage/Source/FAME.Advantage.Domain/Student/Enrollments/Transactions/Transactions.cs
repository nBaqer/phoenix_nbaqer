﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student.Enrollments.PaymentPeriods;

namespace FAME.Advantage.Domain.Student.Enrollments.Transactions
{
    public class Transactions : DomainEntity
    {
        protected Transactions()
        {
        }


        public virtual Enrollment StudentEnrollment { get; protected set; }
        public virtual Term Term { get; protected set; }

        public virtual DateTime TransDate { get; protected set; }

        public virtual TransactionCode TransactionCode { get; protected set; }

        public virtual string Reference { get; protected set; }
        public virtual string Description { get; protected set; }
        public virtual decimal Amount { get; protected set; }

       

        public virtual bool IsPosted { get; protected set; }
        public virtual DateTime CreatedDate { get; protected set; }

        public virtual PaymentPeriods.PayPeriods PaymentPeriod { get; protected set; }




    }
}



