﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    /// <summary>
    /// Domain for arClsSectMeeting Table.
    /// </summary>
    public class ClassSectionMeeting : DomainEntity
    {
        protected ClassSectionMeeting() { }

        public ClassSectionMeeting(DateTime startDate, DateTime endDate, int breakDuration, ArClassSections classSections,
            InstructionType instructionTypeObj, Periods period)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            StartDate = startDate;
            EndDate = endDate;
            BreakDuration = breakDuration;
            ClassSectionsObj = classSections;
            InstructionTypeObj = instructionTypeObj;
            PeriodObj = period;
            //WorkDayObj = workDay;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public virtual DateTime? StartDate { get; protected set; }
        public virtual DateTime? EndDate { get; protected set; }
        public virtual int? BreakDuration { get; protected set; }
        public virtual ArClassSections ClassSectionsObj { get; protected set; }
        public virtual InstructionType InstructionTypeObj { get; protected set; }
        public virtual Periods PeriodObj { get; protected set; }

        /// <summary>
        /// Can be a list because a Section meeting can have  
        /// </summary>
        public virtual IList<ClassSectionAttendance> ClassSectionAttendanceList { get; protected set; }
        //public virtual WorkDays WorkDayObj { get; protected set; }

    }
}
