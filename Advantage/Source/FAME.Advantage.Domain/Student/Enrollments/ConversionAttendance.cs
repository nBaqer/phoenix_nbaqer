﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConversionAttendance.cs" company="FAME Inc.">
//   FAME.Advantage.Domain.Student.Enrollments
// </copyright>
// <summary>
//   Defines the ConversionAttendance type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
// ReSharper disable VirtualMemberCallInConstructor
namespace FAME.Advantage.Domain.Student.Enrollments
{
    using System;

    using Infrastructure.Entities;

    /// <summary>
    /// The conversion attendance.
    /// </summary>
    public class ConversionAttendance : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConversionAttendance"/> class.
        /// </summary>
        public ConversionAttendance()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConversionAttendance"/> class.
        /// </summary>
        /// <param name="studentEnrollmentId">
        ///     The student enrollment id.
        /// </param>
        /// <param name="actual">
        ///     The actual.
        /// </param>
        /// <param name="meetDate">
        ///     The meet date.
        /// </param>
        /// <param name="modUser">The user that made the modification</param>
        public ConversionAttendance(
            Guid studentEnrollmentId,
            decimal actual,
            DateTime meetDate,
            string modUser)
        {
            this.MeetDate = meetDate;
            this.StuEnrollId = studentEnrollmentId;
            this.Actual = actual;
            this.ModUser = modUser;
        }

        /// <summary>
        /// Gets or sets the student enroll id.
        /// </summary>
        public virtual Guid StuEnrollId { get; protected set; }

        /// <summary>
        /// Gets or sets the period.
        /// </summary>
        public virtual decimal Actual { get; protected set; }

        /// <summary>
        /// Gets or sets the date performed.
        /// </summary>
        public virtual DateTime MeetDate { get; protected set; }

        /// <summary>
        /// Gets or sets the modification user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets the modification date.
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the enrollment object.
        /// </summary>
        public virtual Enrollment EnrollmentObj { get; protected set; }
    }
}
