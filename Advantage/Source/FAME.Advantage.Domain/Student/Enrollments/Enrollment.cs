﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Enrollment.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the Enrollment type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Student.Enrollments
{
    using System;
    using System.Collections.Generic;
    using SystemStuff;
    using Campuses;
    using Campuses.CampusGroups.Subresources;
    using Catalogs;
    using Infrastructure.Entities;
    using Lead;
    using Requirements;
    using Users;

    /// <summary>
    /// The enrollment.
    /// </summary>
    public class Enrollment : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Enrollment"/> class.
        /// </summary>
        /// <param name="studentId">
        ///  The student id.
        /// </param>
        /// <param name="enrollDate">
        ///  The enroll date.
        /// </param>
        /// <param name="campusId">
        ///  The campus id.
        /// </param>
        /// <param name="enrollmentStatusId">
        ///  The enrollment status id.
        /// </param>
        /// <param name="enrollmentId">
        ///  The enrollment id.
        /// </param>
        /// <param name="resultList">
        ///  The result list.
        /// </param>
        public Enrollment(Guid studentId, DateTime enrollDate, Guid campusId, Guid enrollmentStatusId, string enrollmentId, IList<ArResults> resultList)
        {
// ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.StudentId = studentId;
            this.EnrollmentDate = enrollDate;
            this.CampusId = campusId;
            this.EnrollmentStatusId = enrollmentStatusId;
            this.EnrollmentId = enrollmentId;
            this.ResultList = resultList;

// ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Enrollment"/> class.
        /// </summary>
        protected Enrollment()
        {
        }

        /// <summary>
        /// Gets or sets the student id.
        /// </summary>
        public virtual Guid StudentId { get; protected set; }

        /// <summary>
        /// Gets or sets the enrollment date.
        /// </summary>
        public virtual DateTime EnrollmentDate { get; protected set; }        
        
        public virtual Guid CampusId { get; protected set; }

        public virtual Guid EnrollmentStatusId { get; protected set; }

        /// <summary>
        /// Gets or sets the enrollment id.
        /// </summary>
        public virtual string EnrollmentId { get; protected set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public virtual DateTime? StartDate { get; protected set; }

        /// <summary>
        /// Gets or sets the date determined.
        /// </summary>
        public virtual DateTime? DateDetermined { get; protected set; }

        /// <summary>
        /// Gets or sets the lda.
        /// </summary>
        public virtual DateTime? Lda { get; protected set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the student.
        /// </summary>
        public virtual Lead Student { get; protected set; }

        /// <summary>
        /// Gets or sets the lead object.
        /// </summary>
        public virtual Lead LeadObj { get; protected set; }

        /// <summary>
        /// Gets or sets the program version.
        /// </summary>
        public virtual ProgramVersion ProgramVersion { get; protected set; }

        /// <summary>
        /// Gets or sets the enrollment status.
        /// </summary>
        public virtual UserDefStatusCode EnrollmentStatus { get; protected set; }

        /// <summary>
        /// Gets or sets the campus object.
        /// </summary>
        public virtual Campus CampusObj { get; protected set; }

        /// <summary>
        /// Gets or sets the student group bridges.
        /// </summary>
        public virtual IList<LeadGroupBridge> StudentGroupBridges { get; protected set; }

        /// <summary>
        /// Gets or sets the result list.
        /// </summary>
        public virtual IList<ArResults> ResultList { get; protected set; }

        /// <summary>
        /// Gets or sets the grade book results.
        /// </summary>
        public virtual IList<GradeBookResult> GradeBookResults { get; protected set; }

        /// <summary>
        /// Gets or sets the student loa list.
        /// </summary>
        public virtual IList<StudentLoa> StudentLoaList { get; protected set; }

        /// <summary>
        /// Gets or sets the student suspensions list.
        /// </summary>
        public virtual IList<StudentSuspensions> StudentSuspensionsList { get; protected set; }

        /// <summary>
        /// Gets or sets the external ship attendance list.
        /// </summary>
        public virtual IList<ExternalShipAttendance> ExternalShipAttendanceList { get; protected set; }

        /// <summary>
        /// Gets or sets the attendance list.
        /// </summary>
        public virtual IList<Attendance> AttendanceList { get; protected set; }

        /// <summary>
        /// Gets or sets the student attendance summary list.
        /// </summary>
        public virtual IList<StudentAttendanceSummary> StudentAttendanceSummaryList { get; protected set; }

        /// <summary>
        /// Gets or sets the class section attendance list.
        /// </summary>
        public virtual IList<ClassSectionAttendance> ClassSectionAttendanceList { get; protected set; }

        /// <summary>
        /// Gets or sets the student clock attendance list.
        /// </summary>
        public virtual IList<StudentClockAttendance> StudentClockAttendanceList { get; protected set; }

        /// <summary>
        /// Gets or sets the conversion attendance list.
        /// </summary>
        public virtual IList<ConversionAttendance> ConversionAttendanceList { get; protected set; }

        /// <summary>
        /// Gets or sets the student award list.
        /// </summary>
        public virtual IList<StudentAwards> StudentAwardList { get; protected set; }

        /// <summary>
        /// Gets or sets the expected graduation date.
        /// </summary>
        public virtual DateTime? ExpectedGraduationDate { get; protected set; }

        /// <summary>
        /// Gets or sets the drop reason id.
        /// </summary>
        public virtual Guid? DropReasonId { get; protected set; }

        /// <summary>
        /// Gets or sets the re enrollment date.
        /// </summary>
        public virtual DateTime? ReEnrollmentDate { get; protected set; }

        /// <summary>
        /// Gets or sets the is first time in school.
        /// </summary>
        public virtual bool IsFirstTimeInSchool { get; protected set; }

        /// <summary>
        /// Gets or sets the is first time post school.
        /// </summary>
        public virtual bool IsFirstTimePostSecSchool { get; protected set; }

        /// <summary>
        /// Gets or sets the shift.
        /// </summary>
        public virtual Shifts Shift { get; protected set; }

        /// <summary>
        /// Gets or sets the charging method.
        /// </summary>
        public virtual ChargingMethod ChargingMethod { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether to disable auto charge.
        /// </summary>
        public virtual bool DisableAutoCharge { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether third party contract.
        /// </summary>
        public virtual bool ThirdPartyContract { get; set; }
        /// <summary>
        /// Gets or sets the program version type.
        /// </summary>
        public virtual ProgramVersionType ProgramVersionType { get; protected set; }

        /// <summary>
        /// Gets or sets the entrance interview date.
        /// </summary>
        public virtual DateTime? EntranceInterviewDate { get; protected set; }

        /// <summary>
        /// Gets or sets the expected start date.
        /// </summary>
        public virtual DateTime? ExpectedStartDate { get; protected set; }

        /// <summary>
        /// Gets or sets the mid point date.
        /// </summary>
        public virtual DateTime? MidPointDate { get; protected set; }

        /// <summary>
        /// Gets or sets the contracted grad date.
        /// </summary>
        public virtual DateTime? ContractedGradDate { get; protected set; }

        /// <summary>
        /// Gets or sets the transfer date.
        /// </summary>
        public virtual DateTime? TransferDate { get; protected set; }

        /// <summary>
        /// Gets or sets the tuition category.
        /// </summary>
        public virtual TuitionCategories TuitionCategory { get; protected set; }

        /// <summary>
        /// Gets or sets the fin aid advisor.
        /// </summary>
        public virtual User FinAidAdvisor { get; set; }

        /// <summary>
        /// Gets or sets the academic advisor.
        /// </summary>
        public virtual User AcademicAdvisor { get; set; }

        /// <summary>
        /// Gets or sets the badge number.
        /// </summary>
        public virtual string BadgeNumber { get; protected set; }

        /// <summary>
        /// Gets or sets the education level.
        /// </summary>
        public virtual EducationLevel EducationLevel { get; protected set; }

        /// <summary>
        /// Gets or sets the transfer hours.
        /// </summary>
        public virtual decimal TransferHours { get; protected set; }

        /// <summary>
        /// Gets or sets the Total Transfer Hours From This School
        /// </summary>
        public virtual decimal TotalTransferHoursFromThisSchool { get; protected set; }

        /// <summary>
        /// Update Enrollment Entity based on parameters
        /// </summary>
        /// <param name="dateDetermined">
        /// Student Enrollment Date Determined
        /// </param>
        /// <param name="lda">
        /// Student Enrollment Last Date of Attendance
        /// </param>
        /// <param name="enrollmentStatus">
        /// Student Enrollment Status Code (System Status Code) 
        /// </param>
        /// <param name="modifiedDate">
        /// Student Enrollment Modified Date
        /// </param>
        /// <param name="modifiedUser">
        /// Student Enrollment Modified user
        /// </param>
        public virtual void Update(DateTime? dateDetermined, DateTime? lda, UserDefStatusCode enrollmentStatus, DateTime modifiedDate, string modifiedUser)
        {
            this.DateDetermined = dateDetermined;
            this.Lda = lda;
            this.EnrollmentStatus = enrollmentStatus;
            this.ModDate = modifiedDate;
            this.ModUser = modifiedUser;
        }

        /// <summary>
        /// Update Enrollment Entity based on parameters
        /// </summary>
        /// <param name="dateDetermined">Student Enrollment Date Determined</param>
        /// <param name="enrollmentStatus">
        /// Student Enrollment Status Code (System Status Code) 
        /// </param>
        /// <param name="modifiedDate">
        /// Student Enrollment Modified Date
        /// </param>
        /// <param name="modifiedUser">
        /// Student Enrollment Modified user
        /// </param>
        public virtual void Update(DateTime dateDetermined, UserDefStatusCode enrollmentStatus, DateTime modifiedDate, string modifiedUser)
        {
            this.DateDetermined = dateDetermined;
            this.EnrollmentStatus = enrollmentStatus;
            this.ModDate = modifiedDate;
            this.ModUser = modifiedUser;
        }

        /// <summary>
        /// Update Enrollment Entity based on parameters (usually use to set drop status)
        /// </summary>
        /// <param name="dateDetermined">
        /// Student Enrollment Date Determined
        /// </param>
        /// <param name="lda">
        /// Student Enrollment Last Date of Attendance
        /// </param>
        /// <param name="dropReasonId">
        /// The drop Reason Id.
        /// </param>
        /// <param name="enrollmentStatus">
        /// Student Enrollment Status Code (System Status Code) 
        /// </param>
        /// <param name="modifiedDate">
        /// Student Enrollment Modified Date
        /// </param>
        /// <param name="modifiedUser">
        /// Student Enrollment Modified user
        /// </param>
        /// <param name="dateOfReenrolment">
        /// Student Date of Re Enrollment
        /// </param>
        public virtual void Update(DateTime? dateDetermined, DateTime? lda, Guid? dropReasonId, UserDefStatusCode enrollmentStatus, DateTime modifiedDate, string modifiedUser, DateTime? dateOfReenrolment)
        {
            this.DateDetermined = dateDetermined;
            this.Lda = lda;
            this.EnrollmentStatus = enrollmentStatus;
            this.ModDate = modifiedDate;
            this.ModUser = modifiedUser;
            this.DropReasonId = dropReasonId;
            this.ReEnrollmentDate = dateOfReenrolment;
        }


        /// <summary>
        /// Update Enrollment Entity based on parameters
        /// </summary>
        /// <param name="dateDetermined">
        /// Student Enrollment Date Determined
        /// </param>
        /// <param name="lda">
        /// Student Enrollment Last Date of Attendance
        /// </param>
        /// <param name="dropReasonId">
        /// The drop Reason Id.
        /// </param>
        /// <param name="enrollmentStatus">
        /// Student Enrollment Status Code (System Status Code) 
        /// </param>
        /// <param name="modifiedDate">
        /// Student Enrollment Modified Date
        /// </param>
        /// <param name="modifiedUser">
        /// Student Enrollment Modified user
        /// </param>
        /// <param name="dateOfReenrolment">
        /// Student Date of Re Enrollment
        /// </param>
        /// <param name="startDate">
        /// The start Date.
        /// </param>
        public virtual void Update(DateTime? dateDetermined, DateTime? lda, Guid? dropReasonId, UserDefStatusCode enrollmentStatus, DateTime modifiedDate, string modifiedUser, DateTime? dateOfReenrolment, DateTime? startDate)
        {
            this.DateDetermined = dateDetermined;
            this.Lda = lda;
            this.EnrollmentStatus = enrollmentStatus;
            this.ModDate = modifiedDate;
            this.ModUser = modifiedUser;
            this.DropReasonId = dropReasonId;
            this.ReEnrollmentDate = dateOfReenrolment;
            this.StartDate = startDate;
        }

        /// <summary>
        /// Update Enrollment Entity based on parameters
        /// </summary>
        /// <param name="dateDetermined">Student Enrollment Date Determined</param>
        /// <param name="lda">Student Enrollment Last Date of Attendance</param>
        /// <param name="enrollmentStatus">Student Enrollment Status Code (System Status Code) </param>
        /// <param name="modifiedDate">Student Enrollment Modified Date</param>
        /// <param name="modifiedUser">Student Enrollment Modified user</param>
        /// <param name="startDate">Student Enrollment Start Date</param>
        public virtual void Update(DateTime? dateDetermined, DateTime? lda,  UserDefStatusCode enrollmentStatus, DateTime modifiedDate, string modifiedUser, DateTime startDate)
        {
            this.DateDetermined = dateDetermined;
            this.Lda = lda;
            this.EnrollmentStatus = enrollmentStatus;
            this.ModDate = modifiedDate;
            this.ModUser = modifiedUser;
            this.StartDate = startDate;
        }

        /// <summary>
        /// Update Enrollment Entity based on parameters.
        /// LDA and Drop Reason Id are set to null when this method is called.
        /// </summary>
        /// <param name="enrollmentStatus">Student Enrollment Status Code (System Status Code) </param>
        /// <param name="modifiedDate">Student Enrollment Modified Date</param>
        /// <param name="modifiedUser">Student Enrollment Modified user</param>
        public virtual void Update(UserDefStatusCode enrollmentStatus, DateTime modifiedDate, string modifiedUser)
        {
            this.EnrollmentStatus = enrollmentStatus;
            this.ModDate = modifiedDate;
            this.ModUser = modifiedUser;
            this.Lda = null;
            this.DropReasonId = null;
        }


        /// <summary>
        /// Update Enrollment Entity based on parameters
        /// </summary>
        /// <param name="dateDetermined">
        /// Student Enrollment Date Determined
        /// </param>
        /// <param name="lda">
        /// Student Enrollment Last Date of Attendance
        /// </param>
        /// <param name="dropReasonId">
        /// The Drop Reason Id.
        /// </param>
        /// <param name="enrollmentStatus">
        /// Student Enrollment Status Code (System Status Code) 
        /// </param>
        /// <param name="modifiedDate">
        /// Student Enrollment Modified Date
        /// </param>
        /// <param name="modifiedUser">
        /// Student Enrollment Modified user
        /// </param>
        public virtual void Update(DateTime? dateDetermined, DateTime? lda, Guid? dropReasonId, UserDefStatusCode enrollmentStatus, DateTime modifiedDate, string modifiedUser)
        {
            this.DateDetermined = dateDetermined;
            this.Lda = lda;
            this.EnrollmentStatus = enrollmentStatus;
            this.ModDate = modifiedDate;
            this.ModUser = modifiedUser;
            this.DropReasonId = dropReasonId;
        }

        /// <summary>
        /// The initialize leave of absence list.
        /// </summary>
        public virtual void InitializeLeaveOfAbsenceList()
        {
            this.StudentLoaList = new List<StudentLoa>();
        }

        /// <summary>
        /// The initialize suspension list.
        /// </summary>
        public virtual void InitializeSuspensionList()
        {
            this.StudentSuspensionsList = new List<StudentSuspensions>();
        }
    }
}



