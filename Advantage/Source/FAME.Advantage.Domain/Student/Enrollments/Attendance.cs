﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    /// <summary>
    /// Domain for atAttendance Table.
    /// </summary>
    public class Attendance: DomainEntity
    {
        protected Attendance() { }

        public Attendance(Enrollment enrollmentObj, Int16? actual, DateTime? attendanceDate)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            EnrollmentObj = enrollmentObj;
            Actual = actual;
            AttendanceDate = attendanceDate;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public virtual Enrollment EnrollmentObj { get; protected set; }
        public virtual Int16? Actual  { get; protected set; }
        public virtual DateTime? AttendanceDate { get; protected set; }
    }
}
