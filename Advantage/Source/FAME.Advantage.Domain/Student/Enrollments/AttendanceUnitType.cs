﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Enrollments
{
    
    /// <summary>
    ///  Unit Type Description hold values as Clock Hours, minutes, present Absent, None.
    /// </summary>
    public class AttendanceUnitType : DomainEntity
    {
        /// <summary>
        /// Protected Constructor
        /// </summary>
        protected AttendanceUnitType(){}

        /// <summary>
        ///  Unit Type Description hold values as Clock Hours, minutes, present Absent, None.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="unitTypeDescription"></param>
        public AttendanceUnitType(Guid id, string unitTypeDescription)
        {
// ReSharper disable DoNotCallOverridableMethodsInConstructor
            ID = id;
            Description = unitTypeDescription;
// ReSharper restore DoNotCallOverridableMethodsInConstructor
        }
        
        /// <summary>
        /// Description
        /// </summary>
        public virtual string Description { get; protected set; }
    }
}
