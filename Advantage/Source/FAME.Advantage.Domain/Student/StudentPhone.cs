﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student
{
    public class StudentPhone : DomainEntity
    {

        public StudentPhone()
        {
        }

        public virtual string Phone { get; protected set; }
        public virtual string BestTime { get; protected set; }
        public virtual bool Default1 { get; protected set; }

        public virtual StudentPhoneType PhoneType { get; protected set; }




    }
}   
