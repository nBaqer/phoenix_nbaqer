﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student
{
    /// <summary>
    /// Lead or Student Citizen ship. It is different than nationality.
    /// </summary>
    public class Citizenships : DomainEntity
    {
        /// <summary>
        /// Default constructor protected to avoid the use of object.
        /// </summary>
        protected Citizenships()
        {
        }

        /// <summary>
        /// Constructor to be use in test routines if any.
        /// </summary>
        public Citizenships(string citizenshipCode, string citizenshipsDescrip)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            CitizenshipsDescrip = citizenshipCode;
            CitizenshipsDescrip = citizenshipsDescrip;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor  
        }



        public virtual string CitizenshipCode { get; protected set; }
        public virtual string CitizenshipsDescrip { get; protected set; }
        public virtual IList<Student> StudentList { get; protected set; }
        
        /// <summary>
        /// List of Lead associated with each citizenship.
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }

    }
}
