﻿using System;
using System.Collections.Generic;
using System.Linq;
using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student.Enrollments;

namespace FAME.Advantage.Domain.Student.Requirements
{
    public class RequirementProgramVersionBridge : DomainEntity
    {
        protected RequirementProgramVersionBridge()
        {
        }

        public RequirementProgramVersionBridge(ProgramVersion programVersion, RequirementGroup requirementGroup, DocumentRequirement documentRequirement)
        {
            if (requirementGroup == null && documentRequirement == null) throw new ApplicationException("Both requirementGroup and documentRequirement cannot be null");

            ProgramVersion = programVersion;
            RequirementGroup = requirementGroup;
            Requirement = documentRequirement;
        }

        public virtual ProgramVersion ProgramVersion { get; protected set; }

        protected internal virtual RequirementGroup RequirementGroup { get; set; }
        protected internal virtual DocumentRequirement Requirement { get; set; }

        public virtual IEnumerable<DocumentRequirement> Requirements
        {
            get
            {
                var requirements = RequirementGroup != null ?
                    RequirementGroup.RequirementGroupDefinitions.Select(x => x.Requirement) :
                    Enumerable.Empty<DocumentRequirement>();

                return (Requirement != null) ? requirements.Concat(new[] { Requirement }) : requirements;
            }
        }
    }
}
