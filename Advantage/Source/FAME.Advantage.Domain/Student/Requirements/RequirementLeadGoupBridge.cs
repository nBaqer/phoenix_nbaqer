﻿using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Requirements
{
    public class RequirementLeadGoupBridge : DomainEntity
    {
        protected RequirementLeadGoupBridge()
        {
        }
        
        public virtual bool? IsRequired { get; protected set; }

        public virtual StudentGroup StudentGroup { get; protected set; }
        public virtual RequirementEffectiveDates EffectiveDates { get; protected set; }
    }
}