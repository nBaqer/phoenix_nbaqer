﻿using System;
using System.Linq;
using System.Linq.Expressions;
using FAME.Advantage.Domain.Student.Requirements.Documents;

namespace FAME.Advantage.Domain.Student.Requirements
{
    public class DocumentRequirement : BaseRequirement
    {
        private const int DocumentRequirementTypeId = 3;

        protected DocumentRequirement()
        {
        }
        
        public DocumentRequirement(string code, string description) : base(code, description, DocumentRequirementTypeId)
        {
        }

        public virtual Document Document { get; protected set; }

        public virtual void SetDocument(Document document)
        {
            Document = document;
        }

        public static readonly Expression<Func<DocumentRequirement, bool>> IsSchoolLevel = r => r.EffectiveDates.Any(d => d.IsMandatory && d.StartDate <= DateTime.Now.Date && d.EndDate == null)
            || (r.EffectiveDates.Any(d => d.IsMandatory && d.StartDate <= DateTime.Now.Date && d.EndDate != null && d.EndDate >= DateTime.Now.Date));
    }
}
 