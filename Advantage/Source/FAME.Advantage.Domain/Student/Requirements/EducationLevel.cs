﻿using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.StatusesOptions;

namespace FAME.Advantage.Domain.Student.Requirements
{
    public class EducationLevel : DomainEntity
    {

        protected EducationLevel()
        {            
        }

        protected EducationLevel( string cd, string desc)
        {
            Code = cd;
            Description = desc;
        }

        public virtual string Code { get; set; }
        public virtual string Description { get; set; }
        public virtual StatusOption Status { get; set; }
        public virtual CampusGroup CampusGroup { get; protected set; }
    }
}
