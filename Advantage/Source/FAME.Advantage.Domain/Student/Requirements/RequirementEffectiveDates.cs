﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Requirements
{
    public class RequirementEffectiveDates : DomainEntity
    {
        public RequirementEffectiveDates()
        {
        }

        public virtual DateTime? StartDate { get; protected set; }
        public virtual DateTime? EndDate { get; protected set; }
        public virtual decimal? MinimumScore { get; protected set; }
        public virtual bool IsMandatory { get; protected set; }
        public virtual int? ValidDays { get; protected set; }

        public virtual DocumentRequirement Requirement { get; protected set; }
        
        public virtual IList<RequirementLeadGoupBridge> RequirementLeadGroupBridge { get; protected set; }
    }
}   



