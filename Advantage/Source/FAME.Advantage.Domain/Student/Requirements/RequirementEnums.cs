﻿namespace FAME.Advantage.Domain.Student.Requirements
{
    public class RequirementEnums
    {
        public enum RequirementTypeLevel
        {
            School,
            RequirementGroup,
            StudentGroup,
            Program
        }

        public enum RequiredFor
        {
            Enrollment,
            FinancialAid,
            Graduation,
            None
        }
    }
}
