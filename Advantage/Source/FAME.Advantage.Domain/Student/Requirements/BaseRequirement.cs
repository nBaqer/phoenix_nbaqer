﻿    using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.StatusesOptions;
    using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Student.Requirements
{
    public abstract class BaseRequirement : DomainEntity
    {
        protected BaseRequirement()
        {
        }

        protected BaseRequirement(string code, string description, int type)
        {
            Code = code;
            Description = description;
            Type = type;
        }

        public virtual string Code { get; protected set; }
        public virtual string Description { get; protected set; }
        public virtual int Type { get; protected set; }        
        public virtual bool? RequiredForEnrollment { get; set; }
        public virtual bool? RequiredForFinancialAid { get; set; }
        public virtual bool? RequiredForGraduation { get; set; }
        public virtual Guid CampGrpId { get; set; }
        public virtual bool showUpdate { get; set; }


        public virtual RequirementEnums.RequiredFor  RequiredFor 
        {
            get
            {
                if (RequiredForEnrollment.HasValue) return RequirementEnums.RequiredFor.Enrollment;
                else if (RequiredForFinancialAid.HasValue) return RequirementEnums.RequiredFor.FinancialAid;
                else if (RequiredForFinancialAid.HasValue) return RequirementEnums.RequiredFor.Graduation;  
                else return RequirementEnums.RequiredFor.None;
            }
        }

        public virtual string RequirementType { get; set; }
        public virtual StatusOption Status { get; set; }
        public virtual CampusGroup CampusGroup { get; protected set; }
        public virtual SystemModule SystemModule { get; protected set; }        
        public virtual EducationLevel Level { get; protected set; }

        public virtual IList<RequirementEffectiveDates> EffectiveDates { get; protected set; }
        public virtual IList<RequirementProgramVersionBridge> RequirementProgramVersionBridge { get; protected set; }
        

        public virtual  bool SetRequirementType( RequirementEnums.RequirementTypeLevel eReqType)
        {
            RequirementType = eReqType.ToString();
            return true;
        }

        public virtual void ShowUpdateForFinancialAid( bool show )
        {
            if (!show && this.SystemModule.Code == "FA") this.showUpdate = false;
            else this.showUpdate = true;
        }
    }
} 