﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Requirements.Documents
{
    public class Document : DomainEntity
    {
        protected Document()
        {
        }

        public Document(Domain.Student.Student student, DocumentStatus documentStatus, DocumentRequirement documentRequirement, DateTime? dateRequested, DateTime? dateReceived, DateTime? dateModified, string user)
        {
            Student = student;
            DocumentStatus = documentStatus;
            DocumentRequirement = documentRequirement;
            DateRequested = dateRequested;
            DateReceived = dateReceived;
            DateModified = dateModified;
            ModUser = user;
        }
            
        public static bool IsEmpty(Document document)
        {
            return document.ID.Equals(Guid.Empty);
        }
            
        public static Document Empty()
        {
            return new Document(null, DocumentStatus.Empty(), null, null, null, null,null);
        }

        public virtual Domain.Student.Student Student { get; protected set; }        
        public virtual DocumentRequirement DocumentRequirement { get; protected set; }
        public virtual DateTime? DateModified { get; protected set; }
        public virtual DateTime? DateRequested { get; protected set; }
        public virtual DateTime? DateReceived { get; protected set; }
        public virtual string ModUser { get; set; }
        public virtual bool showUpdate { get; set; }

        public virtual DocumentStatus DocumentStatus { get; set; }

        public virtual IEnumerable<File.File> Files { get; protected set; }

        public virtual void SaveDocumentRecord(DateTime? dtRequested, DateTime? dtReceived, bool approved )
        {
            DateRequested = dtRequested;
            DateReceived = dtReceived;
            DocumentStatus.IsApproved = approved;
        }
            
        public virtual void SetStatus(DocumentStatus status)
        {
            DocumentStatus = status;
            DateModified = DateTime.Now;
        }

        public virtual void SetDateRequested(DateTime? dateRequested)
        {
            DateRequested = dateRequested;
            DateModified = DateTime.Now;
        }

        public virtual void SetDateReceived(DateTime? dateReceived)
        {
            DateReceived = dateReceived;
            DateModified = DateTime.Now;
        }

        public virtual void SetModUser(string user)
        {
            ModUser = user;
            DateModified = DateTime.Now;
        }

        public virtual void ShowUpdateForFinancialAid(bool show)
        {
            if (!show && this.DocumentRequirement.SystemModule.Code == "FA") this.showUpdate = false;
            else this.showUpdate = true;
        }
    }
}
