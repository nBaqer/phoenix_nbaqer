﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Requirements.Documents.File
{
    public class File : DomainEntity
    {
        protected File()
        {
        }

        public File(Document document, string fileName, string modUser, byte[] content)
        {
            Document = document;
            ID = null;
            FileName = fileName;
            ModUser = modUser;
            Content = content;
        }

        new public virtual Guid? ID { get; protected set; }
        public virtual string FileName { get; protected set; }
        public virtual string ModUser { get; protected set; }
        public virtual byte[] Content { get; protected set; }
        public virtual DateTime DateCreated { get; protected set; }

        public virtual Document Document { get; protected set; }
    }
}
