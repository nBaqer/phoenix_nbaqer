﻿using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Requirements.Documents
{
    public class DocumentStatus : DomainEntity
    {
        protected DocumentStatus() { }

        public DocumentStatus(string code, string description, bool isApproved)
        {
            Code = code;
            Description = description;
            IsApproved = isApproved;
        }

        public static DocumentStatus Empty()
        {
            return new DocumentStatus(string.Empty, string.Empty, false);
        }

        public virtual string Code { get; protected set; }
        public virtual string Description { get; protected set; }
        public virtual int SystemDocStatusId { get; protected set; }

        public virtual bool IsApproved { get; set; }
    }
}
