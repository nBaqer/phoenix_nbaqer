﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;



namespace FAME.Advantage.Domain.Student.Requirements.Documents
{
    public class DocumentHistory : DomainEntity
    {
        protected DocumentHistory() { }

        public DocumentHistory(string fName, string fExt, string dType)
        {
            FileName = fName;
            FileExtension = fExt;
            DocumentType = dType;
        }

        public virtual string FileName { get; protected set; }
        public virtual string FileExtension { get; protected set; }
        public virtual string DocumentType { get; protected set; }
        
        
    }
}
