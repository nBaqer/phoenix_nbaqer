﻿using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Requirements
{
    public class StudentGroupRequirementGroupBridge : DomainEntity
    {
        protected StudentGroupRequirementGroupBridge()
        {
        }

        public StudentGroupRequirementGroupBridge(StudentGroup studentGroup,
            RequirementGroup requirementGroup)
        {
            StudentGroup = studentGroup;
            RequirementGroup = requirementGroup;
        }

        public virtual StudentGroup StudentGroup { get; protected set; }
        public virtual RequirementGroup RequirementGroup { get; protected set; }
    }
}
