﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Infrastructure.Extensions;
using FAME.Advantage.Domain.StatusesOptions;

namespace FAME.Advantage.Domain.Student.Requirements
{
    public class RequirementGroup : DomainEntity
    {
        protected RequirementGroup()
        {
        }

        public RequirementGroup(string code, string description, bool? mandatory, IList<RequirementGroupDefinition> requirementGroupDefinitions)
        {
            if (requirementGroupDefinitions.IsNullOrEmpty()) throw new ArgumentException("requirementGroupDefinitions cannot be null or empty");

            Code = code;
            Description = description;
            IsMandatory = mandatory;

            RequirementGroupDefinitions = requirementGroupDefinitions;
        }

        public virtual string  Code { get; set; }
        public virtual string Description { get; set; }
        public virtual bool? IsMandatory { get; set; }

        public virtual StatusOption Status { get; set; }
        public virtual CampusGroup CampusGroup { get; set; }

        public virtual IList<StudentGroupRequirementGroupBridge> StudentGroupRequirementGroupBridge { get; protected set; }
        public virtual IList<RequirementGroupDefinition> RequirementGroupDefinitions { get; protected set; }
        public virtual IList<RequirementProgramVersionBridge> RequirementProgramVersionBridge { get; protected set; }
    }
}