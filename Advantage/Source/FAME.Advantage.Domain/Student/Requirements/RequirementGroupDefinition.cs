﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Student.Requirements
{
    public class RequirementGroupDefinition : DomainEntity
    {
        public RequirementGroupDefinition() 
        { }

        public RequirementGroupDefinition(int? sequence, bool? isRequired, DocumentRequirement documentRequirement)
        {
            if (documentRequirement == null) throw new ArgumentNullException("documentRequirement");

            Sequence = sequence;
            IsRequired = isRequired;
            Requirement = documentRequirement;
        }

        public virtual int? Sequence { get; set; }
        public virtual bool? IsRequired { get; set; }

        public virtual RequirementGroup RequirementGroup { get; set; }
        public virtual DocumentRequirement Requirement { get; set; }        
    }
}


