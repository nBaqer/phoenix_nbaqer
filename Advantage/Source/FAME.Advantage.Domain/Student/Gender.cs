﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.StatusesOptions;

namespace FAME.Advantage.Domain.Student
{
    public class Gender : DomainEntity
    {
        protected Gender() { }

        public Gender(string code, string descrip, StatusOption status)
        {
            Code = code;
            Description = descrip;
            Status = status;
        }


        public virtual string Code { get; set; }
        public virtual string Description { get; set; }
        public virtual StatusOption Status { get; set; }

    }
}

