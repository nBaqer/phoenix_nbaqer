﻿using System;
using System.Runtime.Serialization;

namespace FAME.Advantage.Domain
{
    public class DomainException : ApplicationException
    {
        public DomainException(string message) : base(message) {}
        public DomainException(string message, Exception innerException) : base(message, innerException) {}
        protected DomainException(SerializationInfo info, StreamingContext context) : base(info, context) {}

        public DomainException(string message, params object[] parameters) : base(String.Format(message, parameters)) { }

    }
}
