﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student.Enrollments;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Catalogs
{
    /// <summary>
    /// Domain for Program Schedule Details (arProgScheduleDetails Table)
    /// </summary>
    public class ProgramScheduleDetails : DomainEntity
    {
        /// <summary>
        /// Dw
        /// </summary>
        public virtual int? Dw { get; protected set; }

        /// <summary>
        /// Dw
        /// </summary>
        public virtual decimal? Total { get; protected set; }

        /// <summary>
        /// Time In
        /// </summary>
        public virtual DateTime? TimeIn { get; protected set; }


        /// <summary>
        /// Lunch In
        /// </summary>
        public virtual DateTime? LunchIn { get; protected set; }

        /// <summary>
        /// Lunch Out
        /// </summary>
        public virtual DateTime? LunchOut { get; protected set; }

        /// <summary>
        /// Time Out
        /// </summary>
        public virtual DateTime? TimeOut { get; protected set; }

        /// <summary>
        /// Max No Lunch
        /// </summary>
        public virtual decimal? MaxNoLunch { get; protected set; }

        /// <summary>
        /// Allow Early In
        /// </summary>
        public virtual bool? AllowEarlyIn { get; protected set; }

        /// <summary>
        /// Allow Late Out
        /// </summary>
        public virtual bool? AllowLateOut { get; protected set; }

        /// <summary>
        /// Allow Extra Hours
        /// </summary>
        public virtual bool? AllowExtraHours { get; protected set; }

        /// <summary>
        /// Check Tardy In
        /// </summary>
        public virtual bool? CheckTardyIn { get; protected set; }

        /// <summary>
        /// Max Before Tardy
        /// </summary>
        public virtual DateTime? MaxBeforeTardy { get; protected set; }

        /// <summary>
        /// Tardy In Time
        /// </summary>
        public virtual DateTime? TardyInTime { get; protected set; }


        /// <summary>
        /// Minimum Hours To Be Present
        /// </summary>
        public virtual decimal? MinimumHoursToBePresent { get; protected set; }

        /// <summary>
        /// Program schedule object that belong the schedule details
        /// </summary>
        public virtual ProgramSchedule ProgramScheduleObj { get; protected set; }

    }
}
   

