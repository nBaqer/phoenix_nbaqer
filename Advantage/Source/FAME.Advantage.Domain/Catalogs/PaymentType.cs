﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PaymentType.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Catalogs.PaymentType
// </copyright>
// <summary>
//   The payment type.
//   Domain Entity for Table <code>saPaymentTypes</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Catalogs
{
    using Infrastructure.Entities;

    /// <summary>
    /// The payment type.
    /// Domain Entity for Table <code>saPaymentTypes</code>
    /// </summary>
    public class PaymentType : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentType"/> class.
        /// </summary>
        public PaymentType()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentType"/> class.
        /// </summary>
        /// <param name="description">
        /// The description.
        /// </param>
        public PaymentType(string description)
        {
            // ReSharper disable once VirtualMemberCallInConstructor
            this.Description = description;
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Description { get; protected set; }
    }
}
