﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GeographicTypes.cs" company="FAME INc">
//   2016
// </copyright>
// <summary>
//   Defines the GeographicTypes type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Catalogs
{
    using System.Collections.Generic;

    using FAME.Advantage.Domain.Campuses.CampusGroups;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.SystemStuff;

    /// <summary>
    /// The geographic types.
    /// </summary>
    public class GeographicTypes : DomainEntity
    {
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        //public virtual string Code { get; protected set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Descrip { get; protected set; }

        /// <summary>
        /// Gets or sets the system statuses object.
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// Gets or sets the campus group object.
        /// </summary>
        public virtual CampusGroup CampusGroupObj { get; protected set; }

        /// <summary>
        /// Gets or sets the lead list.
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }
    }
}