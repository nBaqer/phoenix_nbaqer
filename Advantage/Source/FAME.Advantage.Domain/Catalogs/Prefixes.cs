﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Prefixes.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Domain for Name Prefixes
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Catalogs
{
    using System.Collections.Generic;
    using SystemStuff;
    using Campuses.CampusGroups;
    using Infrastructure.Entities;

    /// <summary>
    /// Domain for Name Prefixes
    /// </summary>
    public class Prefixes : DomainEntity
    {
        /// <summary>
        /// Gets or sets Code of the groups
        /// </summary>
        public virtual string PrefixCode { get; protected set; }

        /// <summary>
        ///  Gets or sets Description of the program group
        /// </summary>
        public virtual string PrefixDescrip { get; protected set; }

        /// <summary>
        ///  Gets or sets Status Active inactive.
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// Gets or sets the campus group object.
        /// </summary>
        public virtual CampusGroup CampusGroupObject { get; protected set; }

        /// <summary>
        ///  Gets or sets List of lead associated with the prefix
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }
    }
}