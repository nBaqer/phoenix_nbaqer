﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Catalogs
{
    /// <summary>
    /// The type of dependence of the lead with other person (dependent independent usually)
    /// This field is customizable by the school
    /// </summary>
    public class DependencyType : DomainEntity
    {
        /// <summary>
        /// Code
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Description 
        /// </summary>
        public virtual string Descrip { get; protected set; }

        /// <summary>
        /// Status Active Inactive 
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// List of lead associated with the dependency Type
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }
    }
}