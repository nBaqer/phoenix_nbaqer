﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramVersionType.cs" company="FAME INC">
//   2016
// </copyright>
// <summary>
//   Defines the ProgramVersionType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Catalogs
{
    using System.Collections.Generic;

    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The program version type.
    /// </summary>
    public class ProgramVersionType : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Gets or sets the program version type id.
        /// </summary>
        //public virtual int ProgramVersionTypeId { get; protected set; }

        /// <summary>
        /// Gets or sets the description program version type.
        /// </summary>
        public virtual string Descrip { get; protected set; }

        /// <summary>
        /// Gets or sets the enroll list.
        /// </summary>
        public virtual IList<Student.Enrollments.Enrollment> EnrollList { get; protected set; }
    }
}
