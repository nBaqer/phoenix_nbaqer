﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Catalogs
{
    /// <summary>
    /// Transportation (example public, owner auto)
    /// </summary>
    public class Transportation : DomainEntity
    {
        /// <summary>
        /// Code
        /// </summary>
        public virtual string TransportationCode { get; protected set; }

        /// <summary>
        /// Description 
        /// </summary>
        public virtual string TransportationDescrip { get; protected set; }

        /// <summary>
        /// Status Active inactive.
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// Campus group object for the Category
        /// </summary>
        public virtual CampusGroup CampusGroupObj { get; protected set; }

        /// <summary>
        /// List of lead associated
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }

    }
}