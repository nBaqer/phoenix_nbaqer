﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NotesPageFields.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Notes Page Field domain, for table syNotesPageFields
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Catalogs
{
    using System;
    using System.Collections.Generic;
    using Infrastructure.Entities;
    using Lead;

    /// <summary>
    /// Notes Page Field domain, for table <code> syNotesPageFields</code>
    /// </summary>
    public class NotesPageFields : DomainEntityWithTypedID<int>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="NotesPageFields"/> class.
        /// </summary>
        /// <param name="id">
        ///  The id.
        /// </param>
        /// <param name="pageName">
        ///  The page name.
        /// </param>
        /// <param name="caption">
        ///  The caption.
        /// </param>
        /// <param name="moduser">
        ///  The modification user.
        /// </param>
        public NotesPageFields(int id, string pageName, string caption, string moduser)
        {
            // ReSharper disable VirtualMemberCallInContructor
            this.ID = id;
            this.PageName = pageName;
            this.FieldCaption = caption;
            this.ModUser = moduser;
            this.ModDate = DateTime.Now;

            // ReSharper restore VirtualMemberCallInContructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotesPageFields"/> class. 
        /// No used
        /// </summary>
        protected NotesPageFields()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets Code of the groups
        /// </summary>
        public virtual string PageName { get; protected set; }

        /// <summary>
        /// Gets or sets Description of the program group
        /// </summary>
        public virtual string FieldCaption { get; protected set; }

        /// <summary>
        /// Gets or sets Status Active inactive of the phone type.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets <code>CampusGrp</code> object
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets List of notes with the same page and field
        /// </summary>
        public virtual IList<AllNotes> AllNotesList { get; protected set; }
        #endregion
    }
}