﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Catalogs
{
    /// <summary>
    /// County Catalog. It is associated with campus.
    /// </summary>
    public class County : DomainEntity
    {
        /// <summary>
        /// Code of the groups
        /// </summary>
        public virtual string CountyCode { get; protected set; }

        /// <summary>
        /// Description of the program group
        /// </summary>
        public virtual string CountyDescrip { get; protected set; }

        /// <summary>
        /// Status Active inactive of the phone type.
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// CampusGrp object
        /// </summary>
        public virtual CampusGroup CampusGroupObj { get; protected set; }

        /// <summary>
        /// Inverse relation. list of Leads in the program group
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }
    }
}