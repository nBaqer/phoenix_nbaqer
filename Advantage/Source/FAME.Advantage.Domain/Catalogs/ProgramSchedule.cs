﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student.Enrollments;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Catalogs
{
    /// <summary>
    /// Domain for Program Schedule (arProgSchedules Table)
    /// </summary>
    public class ProgramSchedule: DomainEntity
    {
        /// <summary>
        /// Code of the groups
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Description of the program schedule
        /// </summary>
        public virtual string Descrip { get; protected set; }

        /// <summary>
        /// Status Active inactive of the Schedule
        /// </summary>
        public virtual bool Active { get; protected set; }

        /// <summary>
        /// Program version object that belong the schedule
        /// </summary>
        public virtual ProgramVersion ProgramVersionObj { get; protected set; }

        /// <summary>
        /// Inverse relation. list of Leads in the schedule
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }

      
    }
}
   

