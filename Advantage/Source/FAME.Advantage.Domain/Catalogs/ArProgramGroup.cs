﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student.Enrollments;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Catalogs
{
    /// <summary>
    /// Domain for ArPrgGrp Table
    /// </summary>
    public class ArProgramGroup : DomainEntity
    {
        /// <summary>
        /// Code of the groups
        /// </summary>
        public virtual string PrgGrpCode { get; protected set; }

        /// <summary>
        /// Description of the program group
        /// </summary>
        public virtual string PrgGrpDescrip { get; protected set; }

        /// <summary>
        /// Campus group object for the Category
        /// </summary>
        public virtual CampusGroup CampusGroupObj { get; protected set; }

        /// <summary>
        /// Status Active inactive of the phone type.
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// Inverse relation. list of Leads in the program group
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }

        /// <summary>
        /// List of Program Version associated with the program group.
        /// </summary>
        public virtual IList<ProgramVersion> ProgramVersionList { get; protected set; }
    }
}