﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Nationalities.cs" company="FAME INC">
//   2016
// </copyright>
// <summary>
//   Defines the Nationalities type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Catalogs
{
    using System.Collections.Generic;

    using FAME.Advantage.Domain.Campuses.CampusGroups;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.SystemStuff;

    /// <summary>
    /// The nationalities.
    /// </summary>
    public class Nationalities : DomainEntity
    {
        /// <summary>
        /// Gets or sets the nationality code.
        /// </summary>
        //public virtual string NationalityCode { get; protected set; }

        /// <summary>
        /// Gets or sets the nationality description.
        /// </summary>
        public virtual string Descrip { get; protected set; }

        /// <summary>
        /// Gets or sets the system statuses object.
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// Gets or sets the campus group object.
        /// </summary>
        public virtual CampusGroup CampusGroupObj { get; protected set; }

        /// <summary>
        /// Gets or sets the lead list.
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }
    }
}
