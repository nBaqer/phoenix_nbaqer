﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Catalogs
{
    /// <summary>
    /// Domain for plAddressTypes Table
    /// </summary>
    public class AddressType : DomainEntity
    {
        /// <summary>
        /// Code of the groups
        /// </summary>
        public virtual string AddressCode { get; protected set; }

        /// <summary>
        /// Description of the program group
        /// </summary>
        public virtual string AddessDescrip { get; protected set; }

        /// <summary>
        /// Status Active inactive of the phone type.
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// CampusGrp object
        /// </summary>
        public virtual CampusGroup CampusGroupObj { get; protected set; }

        /// <summary>
        /// Inverse relation. list of Leads in the address type (AddressType in Lead)
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }
    }
}