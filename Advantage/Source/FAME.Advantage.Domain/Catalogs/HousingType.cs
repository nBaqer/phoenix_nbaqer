﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Catalogs
{
    /// <summary>
    /// Housing type domain for table arHousing
    /// </summary>
    public class HousingType : DomainEntity
    {
        /// <summary>
        /// Hidden Default constructor
        /// </summary>
        protected HousingType() { }

        /// <summary>
        /// Constructor to test domain
        /// </summary>
        /// <param name="code"></param>
        /// <param name="descrip"></param>
        /// <param name="status"></param>
        public HousingType(string code, string descrip, SyStatuses status)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Code = code;
            Descrip = descrip;
            SyStatusesObj = status;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Code
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public virtual string Descrip { get; set; }

        //public virtual StatusOption Status { get; set; }

        /// <summary>
        /// Status Active inactive.
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// List of lead associated with the prefix
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }

    }
}

