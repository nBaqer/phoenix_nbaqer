﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Catalogs
{
    /// <summary>
    /// Family incoming Domain. Use the table syFamilyIncome.
    /// </summary>
    public class FamilyIncoming : DomainEntity
    {
        /// <summary>
        /// Code 
        /// </summary>
        public virtual string FamilyIncomeCode { get; protected set; }

        /// <summary>
        /// Description 
        /// </summary>
        public virtual string FamilyIncomeDescrip { get; protected set; }

        /// <summary>
        /// View Order. Use this to order the list of rows.
        /// </summary>
        public virtual int ViewOrder { get; protected set; }

        /// <summary>
        /// Status Active inactive
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// List of lead associated 
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }
        /// <summary>
        /// Campus group Id that is a foreign key
        /// </summary>
        public virtual CampusGroup CampusGrp { get; protected set; }
    }
}