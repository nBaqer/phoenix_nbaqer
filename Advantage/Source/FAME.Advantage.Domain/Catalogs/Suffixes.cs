﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Catalogs
{
    public class Suffixes : DomainEntity
    {
        /// <summary>
        /// Code of the groups
        /// </summary>
        public virtual string SuffixCode { get; protected set; }

        /// <summary>
        /// Description of the program group
        /// </summary>
        public virtual string SuffixDescrip { get; protected set; }

        /// <summary>
        /// Status Active inactive.
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// List of lead associated with the prefix
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }
    }
}