﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Lead;

namespace FAME.Advantage.Domain.Catalogs
{
    ///// <summary>
    ///// Domain for table syNotesType
    ///// </summary>
    //public class NotesType: DomainEntityWithTypedID<int>
    //{
        //#region Constructor
        ///// <summary>
        ///// Disable default constructor
        ///// </summary>
        //protected NotesType() { }

        ///// <summary>
        ///// Notes Type constructor use this.
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="code"></param>
        ///// <param name="description"></param>
        ///// <param name="moduser"></param>
        //public NotesType(int id, string code, string description, string moduser)
        //{
        //    // ReSharper disable VirtualMemberCallInContructor
        //    ID = id;
        //    Code = code;
        //    Description = description;
        //    ModUser = moduser;
        //    ModDate = DateTime.Now;
        //  // ReSharper restore VirtualMemberCallInContructor
  
        //}
        //#endregion

        //#region Constructor Properties

        ///// <summary>
        ///// Two letter code for the notes type
        ///// </summary>
        //public virtual string Code { get; protected set; }
        
        ///// <summary>
        ///// Description of the type
        ///// </summary>
        //public virtual string Description { get; protected set; }
        
        ///// <summary>
        ///// User that modify the field
        ///// </summary>
        //public virtual string ModUser { get; protected set; }

        ///// <summary>
        ///// Modification Date
        ///// </summary>
        //public virtual DateTime ModDate { get; protected set; }

        ///// <summary>
        ///// List of notes associate with the same type
        ///// </summary>
        //public virtual IList<AllNotes> AllNotesList { get; protected set; } 
        //#endregion
  //  }
}
