﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Catalogs
{
    /// <summary>
    /// Gender Domain
    /// </summary>
    public class Gender : DomainEntity
    {
        /// <summary>
        /// Hidden Default constructor
        /// </summary>
        protected Gender() { }

        /// <summary>
        /// Constructor to test domain
        /// </summary>
        /// <param name="code"></param>
        /// <param name="descrip"></param>
        /// <param name="status"></param>
        public Gender(string code, string descrip, SyStatuses status)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Code = code;
            Description = descrip;
            SyStatusesObj = status;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Code
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public virtual string Description { get; set; }

        //public virtual StatusOption Status { get; set; }

        /// <summary>
        /// Status Active inactive.
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// List of lead associated with the prefix
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }
        /// <summary>
        /// Campus group Id that is a foreign key
        /// </summary>
        public virtual CampusGroup CampusGrp { get; protected set; }
    }
}

