﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="syAcademicCalendars.cs" company="FAME Inc">
//   2016
// </copyright>
// <summary>
//   Defines the syAcademicCalendars type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Catalogs
{
    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The academic calendar.
    /// </summary>
    public class SyAcademicCalendars : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Descrip { get; protected set; }
    }
}
