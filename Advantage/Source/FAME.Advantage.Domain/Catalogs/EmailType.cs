﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Lead;

namespace FAME.Advantage.Domain.Catalogs
{
    /// <summary>
    /// Email type catalog Domain for Table syEmailType
    /// </summary>
    public class EmailType : DomainEntity
    {
        /// <summary>
        /// Code of the type
        /// </summary>
        public virtual string EmailTypeCode { get; protected set; }

        /// <summary>
        /// Description of the type
        /// </summary>
        public virtual string EmailTypeDescrip { get; protected set; }


        /// <summary>
        /// List of leadEmail associated with the type
        /// </summary>
        public virtual IList<LeadEmail> LeadMailsList { get; protected set; }

    }
}