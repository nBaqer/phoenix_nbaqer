﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Catalogs
{
    /// <summary>
    /// Domain for Table adMaritalStatus
    /// </summary>
    public class MaritalStatus : DomainEntity
    {
        /// <summary>
        /// Hidden Default constructor
        /// </summary>
        protected MaritalStatus() { }

        /// <summary>
        /// Constructor to test domain
        /// </summary>
        /// <param name="code"></param>
        /// <param name="descrip"></param>
        /// <param name="status"></param>
        public MaritalStatus(string code, string descrip, SyStatuses status)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            MaritalStatCode = code;
            MaritalStatDescrip = descrip;
            SyStatusesObj = status;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Code
        /// </summary>
        public virtual string MaritalStatCode { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public virtual string MaritalStatDescrip { get; set; }

        /// <summary>
        /// Status Active inactive.
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// List of lead associated with the prefix
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }

    }
}

