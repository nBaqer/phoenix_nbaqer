﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Catalogs
{
    /// <summary>
    /// Source of the advertisement
    /// </summary>
    public class SourceAdvertisement : DomainEntity
    {
        /// <summary>
        ///Category description
        /// </summary>
        public virtual string SourceAdvDescrip { get; protected set; }

        /// <summary>
        /// Category Code
        /// </summary>
        public virtual string SourceAdvCode { get; protected set; }

        /// <summary>
        /// Status Active inactive of the phone type.
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// Campus group object for the Category
        /// </summary>
        public virtual CampusGroup CampusGroupObj { get; protected set; }

        /// <summary>
        /// Source Type for the Advertisement
        /// </summary>
        public virtual SourceType SourceTypeObj { get; protected set; }

        /// <summary>
        /// Inverse relation. list of Leads in the program group
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }
    }
}