﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// AdSkill Table Domain 
     /// </summary>
    public class AdSkills : DomainEntityWithTypedID<int>
    {
              #region constructors

        /// <summary>
        /// Protected constructor
        /// </summary>
        protected AdSkills()
        {

        }

        /// <summary>
        /// constructor
        /// </summary>
        public AdSkills(string extraCurricularDesCrip, string comments, Lead leadObj, SkillGroups group, AdLevels level, string modUser, DateTime modDate)
        {
            // ReSharper disable VirtualMemberCallInContructor
            Description = extraCurricularDesCrip;
            Comment = comments;
            LeadObj = leadObj;
            SkillGroupObj = group;
            LevelObj = level;
            ModUser = modUser;
            ModDate = modDate;
            // ReSharper restore VirtualMemberCallInContructor
        }
        #endregion

        
        /// <summary>
        ///Skill description
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// Skill Code
        /// </summary>
        public virtual string Comment { get; protected set; }

        /// <summary>
        /// Level object for the Skill
        /// </summary>
        public virtual AdLevels LevelObj { get; protected set; }

        /// <summary>
        /// Inverse relation. list of Leads in the Skill
        /// </summary>
        public virtual Lead LeadObj { get; protected set; }

        /// <summary>
        /// Group that owned the skill.
        /// </summary>
        public virtual SkillGroups SkillGroupObj { get; protected set; }

        /// <summary>
        /// The user that modified the record
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Date time of modifications
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }
    }
}