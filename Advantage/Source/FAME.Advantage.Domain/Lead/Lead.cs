﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Lead.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Lead Domain entity. Represent the table AdLeads
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Lead
{
    // Insert Update Lead need to change the SLead Statuses Changes Table!!!!
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Domain.Campuses;
    using Domain.Catalogs;
    using Domain.Infrastructure.Entities;
    using Domain.MostRecentlyUsed;
    using Domain.Requirements;
    using Domain.Requirements.Documents;
    using Domain.StatusesOptions;
    using Domain.Student;
    using Domain.Student.Enrollments;
    using Domain.SystemStuff;
    using Domain.SystemStuff.Intitution;
    using Domain.SystemStuff.SchoolDefinedFields;
    using Domain.Users;

    using FAME.Advantage.Domain.Utils;

    using Messages.Lead;
    
    /// <summary>
    /// Lead Domain entity. Represent the table AdLeads
    /// </summary>
    public class Lead : DomainEntity
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Lead"/> class.
        /// </summary>
        /// <param name="firstName">
        ///  The first name.
        /// </param>
        /// <param name="middleName">
        ///  The middle name.
        /// </param>
        /// <param name="lastName">
        ///  The last name.
        /// </param>
        /// <param name="status">
        ///  The status.
        /// </param>
        /// <param name="expStart">
        ///  The expected start.
        /// </param>
        /// <param name="cmp">
        ///  The campus.
        /// </param>
        public Lead(string firstName, string middleName, string lastName, UserDefStatusCode status, DateTime? expStart, Campus cmp)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.FirstName = firstName.ToPascalCase();
            this.MiddleName = middleName.ToPascalCase();
            this.LastName = lastName.ToPascalCase();
            this.LeadStatus = status;
            this.ExpectedStart = expStart;
            this.Campus = cmp;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Lead"/> class.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        /// <param name="campus">
        /// The campus.
        /// </param>
        /// <param name="grp">
        /// The program group.
        /// </param>
        /// <param name="program">
        /// The program.
        /// </param>
        /// <param name="programV">
        /// The program version.
        /// </param>
        /// <param name="admidrepObj">
        /// The admission represents object.
        /// </param>
        /// <param name="attendTypes">
        /// The attendance types.
        /// </param>
        /// <param name="programSchedule">
        /// The program schedule.
        /// </param>
        /// <param name="prefixes">
        /// The prefixes.
        /// </param>
        /// <param name="suffixes">
        /// The suffixes.
        /// </param>
        /// <param name="gender">
        /// The gender.
        ///  </param>
        /// <param name="race">
        /// The ethnicity
        /// </param>
        /// <param name="citizen">
        /// The citizen.
        /// </param>
        /// <param name="dependencyType">
        /// The dependency type.
        /// </param>
        /// <param name="maritalStatus">
        /// The marital status.
        /// </param>
        /// <param name="familyIncoming">
        /// The family incoming.
        /// </param>
        /// <param name="housingType">
        /// The housing type.
        /// </param>
        /// <param name="driverLicState">
        /// The driver license state.
        /// </param>
        /// <param name="transportation">
        /// The transportation.
        /// </param>
        /// <param name="sourceCategory">
        /// The source category.
        /// </param>
        /// <param name="sourceType">
        /// The source type.
        /// </param>
        /// <param name="sourceAdvertisement">
        /// The source advertisement.
        /// </param>
        /// <param name="agencySponsors">
        /// The agency sponsors.
        /// </param>
        /// <param name="reasonNotEnrolled">
        /// The reason Not Enrolled.
        /// </param>
        /// <param name="educationLevel">
        /// The education level.
        /// </param>
        /// <param name="highSchools">
        /// The high schools.
        /// </param>
        /// <param name="adminCriteria">
        /// The administrative criteria.
        /// </param>
        /// <param name="phoneTypeList">
        /// The phone type list.
        /// </param>
        /// <param name="addressTypeList">
        /// The list of type of address
        /// </param>
        /// <param name="emailTypeList">
        /// The email type list.
        /// </param>
        /// <param name="usaStatesList">
        /// The USA States List.
        /// </param>
        /// <param name="countyList">
        /// The county List.
        /// </param>
        /// <param name="syStatusesList">
        /// The SY Statuses List.
        /// </param>
        /// <param name="countryList">
        /// The country List.
        /// </param>
        /// <param name="leadGroups">
        /// The lead groups.
        /// </param>
        /// <param name="sdfModuleList">
        /// The SDF module list.
        /// </param>
        /// <param name="phoneStatus">
        /// The phone Status.
        /// </param>
        /// <param name="emailStatus">
        /// The email Status.
        /// </param>
        /// <param name="studentStatusId">
        /// Student Status ID
        /// </param>
        /// <param name="studentNumber">
        /// The Student unique generate number
        /// </param>
        /// <param name="studentId">
        /// The Student Id generated when the Lead is enrolled
        /// </param>
        /// <param name="isFirstTimeSchool">
        /// The is First Time School.
        /// </param>
        /// <param name="isFirstTimePostSecSchool">
        /// The is First Time Post Sec School.
        /// </param>
        public Lead(
            LeadInfoPageOutputModel input,
            UserDefStatusCode status,
            Campus campus,
            ArProgramGroup grp,
            ArPrograms program,
            ProgramVersion programV,
            User admidrepObj,
            ArAttendTypes attendTypes,
            ProgramSchedule programSchedule,
            Prefixes prefixes,
            Suffixes suffixes,
            Gender gender,
            Ethnicity race,
            Citizenships citizen,
            DependencyType dependencyType,
            MaritalStatus maritalStatus,
            FamilyIncoming familyIncoming,
            HousingType housingType,
            SystemStates driverLicState,
            Transportation transportation,
            SourceCategory sourceCategory,
            SourceType sourceType,
            SourceAdvertisement sourceAdvertisement,
            AgencySponsors agencySponsors,
            SyReasonNotEnrolled reasonNotEnrolled,
            EducationLevel educationLevel,
            HighSchools highSchools,
            AdminCriteria adminCriteria,
            IQueryable<SyPhoneType> phoneTypeList,
            IQueryable<AddressType> addressTypeList,
            IQueryable<EmailType> emailTypeList,
            IQueryable<SystemStates> usaStatesList,
            IQueryable<County> countyList,
            // ReSharper disable StyleCop.SA1305
            IQueryable<SyStatuses> syStatusesList,
            // ReSharper restore StyleCop.SA1305
            IQueryable<Country> countryList,
            ICollection<LeadGroups> leadGroups,
            List<SdfModuleValueLead> sdfModuleList,
            SyStatuses phoneStatus,
            SyStatuses emailStatus,
            StatusOption studentStatusId,
            string studentNumber,
            Guid studentId,
            bool isFirstTimeSchool,
            bool isFirstTimePostSecSchool)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor

            // Create list empties
            this.StatusesChangesList = new List<LeadStatusesChanges>();
            this.LeadPhoneList = new List<LeadPhone>();
            this.LeadEmailList = new List<LeadEmail>();
            this.MruList = new List<ViewLeadMru>();
            this.VehiclesList = new List<AdVehicles>();
            this.AddressList = new List<LeadAddress>();

            // Update Simple values
            this.UpdateLeadInfoPageSingleValues(input);
            this.LeadStatus = status;
            this.Campus = campus;
            this.ProgramGroupObj = grp;
            this.ProgramObj = program;
            this.ProgramVersion = programV;
            this.AdmissionRepUserObj = admidrepObj;
            this.AttendTypeObj = attendTypes;
            this.ProgramScheduleObj = programSchedule;
            this.PrefixObj = prefixes;
            this.SuffixObj = suffixes;
            this.GenderObj = gender;
            this.EthnicityObj = race;
            this.CitizenshipsObj = citizen;
            this.DependencyTypeObj = dependencyType;
            this.MaritalStatusObj = maritalStatus;
            this.FamilyIncomingObj = familyIncoming;
            this.HousingTypeObj = housingType;
            this.TransportationObj = transportation;
            this.DriverLicenseStateCodeObj = driverLicState;
            this.ModDate = DateTime.Now;
            this.StudentStatus = studentStatusId;
            this.StudentNumber = studentNumber;
            this.StudentId = studentId;
            this.IsFirstTimeInSchool = isFirstTimeSchool;
            this.IsFirstTimePostSecSchool = isFirstTimePostSecSchool;

            // Source
            this.SourceCategoryObj = sourceCategory;
            this.SourceTypeObj = sourceType;
            this.SourceAdvertisementObj = sourceAdvertisement;
            this.AgencySponsorsObj = agencySponsors;
            this.ReasonNotEnrolledObj = reasonNotEnrolled;
            this.PreviousEducationObj = educationLevel;
            this.HighSchoolsObj = highSchools;
            this.AdminCriteriaObj = adminCriteria;

            // Vehicles of the entity
            this.UpdateVehiclesList(input.Vehicles);

            input.LeadAddress.Moduser = input.ModUser;

            // Address list of the entity
            this.UpdateAddresslist(input.LeadAddress, addressTypeList, countyList, usaStatesList, countryList, syStatusesList);

            // Phone load of entities
            this.UpdatePhoneList(input.PhonesList, phoneTypeList, phoneStatus);

            // Email update of entities
            this.UpdateEmailList(input.NoneEmail, input.EmailList, emailTypeList, emailStatus);

            // Lead Groups
            this.LeadGroupsList = leadGroups;

            // Store the SdfList.......

            // Loop through all fields that comes from Client
            foreach (SdfModuleValueLead valueLead in sdfModuleList)
            {
                valueLead.UpdateLead(this);
                valueLead.UpdateModUser(this.ModUser);
            }

            this.SdfModuleValueList = sdfModuleList;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Lead"/> class. 
        /// Default constructor protected to avoid the use of object.
        /// </summary>
        protected Lead()
        {
        }

        #endregion

        #region Demographic information

        /// <summary>
        /// Gets or sets Lead Age 
        /// The database hold this value as a string not null of 3 characters
        /// </summary>
        public virtual string Age { get; protected set; }

        /// <summary>
        /// Gets or sets Assigned Alien Number for international people if any.
        /// </summary>
        public virtual string AlienNumber { get; protected set; }

        /// <summary>
        /// Gets or sets Birth Date
        /// </summary>
        public virtual DateTime? BirthDate { get; protected set; }

        /// <summary>
        /// Gets or sets Lead  citizenship object. point to table adCitizenships
        /// </summary>
        public virtual Citizenships CitizenshipsObj { get; protected set; }

        /// <summary>
        /// Gets or sets Peoples that depend from the Lead.
        /// This field was previously Children
        /// </summary>
        public virtual string Dependants { get; protected set; }

        /// <summary>
        /// Gets or sets If the lead leave independent or depend for other people.
        /// This table can  have user custom values.
        /// The object point to Table adDependencyTypes
        /// </summary>
        public virtual DependencyType DependencyTypeObj { get; protected set; }

        /// <summary>
        /// Gets or sets Driver License Number. can be Null
        /// </summary>
        public virtual string DriverLicenseNumber { get; protected set; }

        /// <summary>
        /// Gets or sets State Object of the Lead. Can be any state of the united states
        /// </summary>
        public virtual SystemStates DriverLicenseStateCodeObj { get; protected set; }

        /// <summary>
        /// Gets or sets Lead Family Incoming
        /// </summary>
        public virtual FamilyIncoming FamilyIncomingObj { get; protected set; }

        /// <summary>
        /// Gets or sets First Name
        /// </summary>
        public virtual string FirstName { get; protected set; }

        /// <summary>
        /// Gets or sets Lead Gender
        /// </summary>
        public virtual Gender GenderObj { get; protected set; }

        /// <summary>
        /// Gets or sets Lead Housing Type
        /// </summary>
        public virtual HousingType HousingTypeObj { get; protected set; }

        /// <summary>
        /// Gets or sets  null no selected/ 0 NO /1 YES
        /// </summary>
        public virtual bool? IsDisabled { get; protected set; }

        /// <summary>
        ///  Gets or sets  Last Name
        /// </summary>
        public virtual string LastName { get; protected set; }

        /// <summary>
        ///  Gets or sets  Lead Marital Status
        /// </summary>
        public virtual MaritalStatus MaritalStatusObj { get; protected set; }

        /// <summary>
        /// Gets or sets Middle Name
        /// </summary>
        public virtual string MiddleName { get; protected set; }

        /// <summary>
        /// Gets or sets Time in minutes or hours that the student is from school
        /// change to distance to school 
        /// </summary>
        public virtual int? DistanceToSchool { get; protected set; }

        /// <summary>
        /// Gets or sets Lead Nickname
        /// </summary>
        public virtual string NickName { get; protected set; }

        /// <summary>
        /// Gets or sets Lead Social Security Number
        /// </summary>
        public virtual string SSN { get; protected set; }

        /// <summary>
        /// Gets or sets Lead Transportation type
        /// </summary>
        public virtual Transportation TransportationObj { get; protected set; }

        /// <summary>
        /// Gets or sets Name prefix object
        /// </summary>
        public virtual Prefixes PrefixObj { get; protected set; }

        /// <summary>
        /// Gets or sets Name suffix object
        /// </summary>
        public virtual Suffixes SuffixObj { get; protected set; }

        /// <summary>
        /// Gets or sets Ethnicity or Race.
        /// </summary>
        public virtual Ethnicity EthnicityObj { get; protected set; }

        /// <summary>
        /// Gets or sets List of all changes of last name of the lead
        /// </summary>
        public virtual IList<LeadLastNameHistory> LeadLastNameList { get; protected set; }

        /// <summary>
        /// Gets or sets List Of Vehicles per lead
        /// </summary>
        public virtual IList<AdVehicles> VehiclesList { get; protected set; }

        #endregion Demographic

        #region Academic Information

        /// <summary>
        /// Gets or sets the expected start.
        /// </summary>
        public virtual DateTime? ExpectedStart { get; set; }

        /// <summary>
        /// Gets or sets Campus for the lead object
        /// </summary>
        public virtual Campus Campus { get; set; }

        /// <summary>
        /// Gets or sets Lead Status Object
        /// </summary>
        public virtual UserDefStatusCode LeadStatus { get; protected set; }

        /// <summary>
        /// Gets or sets Program group object (AreaId) or Interesting Area
        /// </summary>
        public virtual ArProgramGroup ProgramGroupObj { get; protected set; }

        /// <summary>
        /// Gets or sets Lead proposed program
        /// </summary>
        public virtual ArPrograms ProgramObj { get; protected set; }

        /// <summary>
        /// Gets or sets Object get program version from <code> arProgramVersion</code> table.
        /// </summary>
        public virtual ProgramVersion ProgramVersion { get; protected set; }

        /// <summary>
        /// Gets or sets Type of student attention to the school (partial full time)
        /// </summary>
        public virtual ArAttendTypes AttendTypeObj { get; protected set; }

        /// <summary>
        /// Gets or sets Admission Rep user.
        /// </summary>
        public virtual User AdmissionRepUserObj { get; set; }

        /// <summary>
        /// Gets or sets the vendor.
        /// </summary>
        public virtual User Vendor { get; set; }

        /// <summary>
        /// Gets or sets Program Schedule associated with the Lead.
        /// </summary>
        public virtual ProgramSchedule ProgramScheduleObj { get; protected set; }

        /// <summary>
        /// Gets or sets the student id.
        /// </summary>
        public virtual Guid StudentId { get; protected set; }

        /// <summary>
        /// Gets or sets the enrollment list.
        /// </summary>
        public virtual IList<Enrollment> EnrollmentList { get; protected set; }

        #endregion

        #region Sources

        /// <summary>
        /// Gets or sets Category of the Source where the lead was referred. 
        /// </summary>
        public virtual SourceCategory SourceCategoryObj { get; protected set; }

        /// <summary>
        /// Gets or sets Type of Source Category. it is related to Source Category.
        /// </summary>
        public virtual SourceType SourceTypeObj { get; protected set; }

        /// <summary>
        /// Gets or sets Source Advertisement. The table AdSourceAdvertisement is empty now
        /// </summary>
        public virtual SourceAdvertisement SourceAdvertisementObj { get; protected set; }

        /// <summary>
        /// Gets or sets Source Date from source.
        /// </summary>
        public virtual DateTime? SourceDate { get; protected set; }

        #endregion

        #region Vendor Fields

        /// <summary>
        /// Gets or sets Prospect ID given by the Vendor
        /// </summary>
        public virtual string ProspectId { get; protected set; }

        /// <summary>
        /// Gets or sets Vendor Campaign object associated with this Lead
        /// </summary>
        public virtual AdVendorCampaign CampaignObj { get; protected set; }

        #endregion

        #region Contact Info

        /// <summary>
        /// Gets or sets the address list.
        /// </summary>
        public virtual IList<LeadAddress> AddressList { get; protected set; }

        ///// <summary>
        ///// Gets or sets the address 1.
        ///// </summary>
        // public virtual string Address1 { get; protected set; }

        ///// <summary>
        ///// Gets or sets the address apt.
        ///// </summary>
        // public virtual string AddressApt { get; protected set; }

        ///// <summary>
        ///// Gets or sets the address 2.
        ///// </summary>
        // public virtual string Address2 { get; protected set; }

        ///// <summary>
        ///// Gets or sets the city (free text).
        ///// </summary>
        // public virtual string City { get; protected set; }

        ///// <summary>
        ///// Gets or sets the zip (max 10 characters).
        ///// </summary>
        // public virtual string Zip { get; protected set; }

        ///// <summary>
        ///// Gets or sets the other states.
        ///// Other states, use this for international address.
        ///// if here exists information check the international check box
        ///// in the interface lead.
        ///// </summary>
        // public virtual string OtherStates { get; protected set; }

        /// <summary>
        /// Gets or sets the state object.
        /// Can be any state of the united states
        /// </summary>
        public virtual SystemStates StateObj { get; protected set; }

        /// <summary>
        /// Gets or sets the enroll state.
        /// </summary>
        public virtual SystemStates EnrollState { get; protected set; }

        ///// <summary>
        ///// Gets or sets the county object.
        ///// </summary>
        // public virtual County CountyObj { get; protected set; }

        ///// <summary>
        ///// Gets or sets the country object.
        ///// </summary>
        // public virtual Country CountryObj { get; protected set; }

        ///// <summary>
        ///// Gets or sets the address type object.
        ///// Example: Home
        ///// </summary>
        // public virtual AddressType AddressTypeObj { get; protected set; }

        /// <summary>
        /// Gets or sets the home email.
        /// </summary>
        public virtual string HomeEmail { get; protected set; }

        /// <summary>
        /// Gets or sets Preferred contact id. This field comes from one enumeration
        /// then we used the integer part of the enumeration.
        /// 1:phone 2:email 3:text
        /// </summary>
        public virtual int PreferredContactId { get; protected set; }

        /// <summary>
        /// Gets or sets The list of phone associated with the lead.
        /// </summary>
        public virtual ICollection<LeadPhone> LeadPhoneList { get; protected set; }

        /// <summary>
        /// Gets or sets The list of email associated with the lead.
        /// </summary>
        public virtual ICollection<LeadEmail> LeadEmailList { get; protected set; }

        /// <summary>
        /// Gets or sets the best time to communicate with the lead (time in format only used the time portion)
        /// </summary>
        public virtual string BestTime { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether none email.
        /// true if the leads declare no email.
        /// </summary>
        public virtual bool NoneEmail { get; protected set; }

        #endregion

        #region Others
        /// <summary>
        /// Gets or sets the AFA Student Id
        /// </summary>
        public virtual string AfaStudentId { get; protected set; }
        /// <summary>
        /// Gets or sets Assigned Lead Date to Admission Rep.
        /// </summary>
        public virtual DateTime? AssignedDate { get; protected set; }

        /// <summary>
        ///  Gets or sets Assigned Date that applied the lead in school
        /// </summary>
        public virtual DateTime? DateApplied { get; protected set; }

        ///// <summary>
        /////  50 characters null able comment about the advertisement
        /////  Note: obsolete field after created notes page 
        ///// </summary>
        //// public virtual string AdvertisementNote { get; protected set; }

        /// <summary>
        ///  Gets or sets Assigned Agency sponsor that bring the lead. (this is not the vendor)
        /// </summary>
        public virtual AgencySponsors AgencySponsorsObj { get; protected set; }

        /// <summary>
        /// Gets or sets Reason for not enrolled the lead
        /// </summary>
        public virtual SyReasonNotEnrolled ReasonNotEnrolledObj { get; protected set; }

        ///// <summary>
        ///// Free comment about the lead 100 characters nullable
        ///// Note: obsolete field after created notes page
        ///// </summary>
        //// public virtual string Comments { get; protected set; }

        /// <summary>
        ///  Gets or sets Assigned Previous education of the Lead
        /// </summary>
        public virtual EducationLevel PreviousEducationObj { get; protected set; }

        /// <summary>
        ///  Gets or sets Assigned High school information
        /// </summary>
        public virtual HighSchools HighSchoolsObj { get; protected set; }

        /// <summary>
        ///  Gets or sets Assigned Lead graduation day from high school
        /// </summary>
        public virtual DateTime? HighSchoolGradDate { get; protected set; }

        /// <summary>
        ///  Gets or sets Assigned If the lead is currently attending high school
        /// no answer (null) yes (1) no (0)
        /// </summary>
        public virtual int? AttendingHs { get; protected set; }

        /// <summary>
        ///  Gets or sets Assigned Administrative criteria to accept the lead
        /// </summary>
        public virtual AdminCriteria AdminCriteriaObj { get; protected set; }

        #endregion

        #region special compositions for lead name

        /// <summary>
        /// Gets or sets Lead FullName FirstName + MiddleName + LastName
        /// </summary>
        public virtual string FullName
        {
            get { return this.FirstName + (this.MiddleName != null ? (" " + this.MiddleName) : string.Empty) + " " + this.LastName; }
            set { value = this.FirstName + (this.MiddleName != null ? (" " + this.MiddleName) : string.Empty) + " " + this.LastName; }
        }

        /// <summary>
        /// Gets LeadInfoBarName
        /// Read only property
        /// </summary>
        public virtual string LeadInfoBarName
        {
            get
            {
                return this.FirstName
                       + ((string.IsNullOrWhiteSpace(this.NickName) == false) ? (" (" + this.NickName + ") ") : " ")
                       + this.LastName;
            }
        }

        /// <summary>
        /// Gets or sets the full name last first.
        /// </summary>
        public virtual string FullNameLastFirst
        {
            get { return this.LastName + ", " + this.FirstName + " " + (this.MiddleName != null ? (" " + this.MiddleName) : string.Empty); }
            set { value = this.LastName + ", " + this.FirstName + " " + (this.MiddleName != null ? (" " + this.MiddleName) : string.Empty); }
        }

        #endregion

        #region Image related fields

        /// <summary>
        /// Gets a partial URL of the image, to use this 
        /// you should add before the site URL
        /// If return empty string you should supply the URL of the fake image.
        /// </summary>
        public virtual string ImagePartialUrl
        {
            get
            {
                var docHistoryRecord = this.DocumentHistory.FirstOrDefault(n => n.DocumentType.ToUpper() == "PHOTOID");
                var imageName = (docHistoryRecord != null) ? string.Format("/{0}/{1}{2}", docHistoryRecord.DocumentType, docHistoryRecord.ID, docHistoryRecord.FileExtension) : string.Empty;
                return imageName;
            }
        }

        /// <summary>
        /// Gets or sets the image list 64.
        /// </summary>
        public virtual IList<LeadImage> ImageList64 { get; protected set; }

        /// <summary>
        /// Gets or sets URL Image
        /// </summary>
        public virtual string ImageUrl { get; protected set; }

        /// <summary>
        /// Gets or sets Image Encoded 64
        /// </summary>
        public virtual string ImageEncode64 { get; protected set; }

        /// <summary>
        /// Gets or sets Image encoded
        /// </summary>
        public virtual string EncodeImage { get; protected set; }

        /// <summary>
        /// Gets or sets URL for default image for no image.
        /// </summary>
        public virtual string NotFoundImageUrl { get; protected set; }

        #endregion

        #region enroll
        /// <summary>
        /// Gets or sets entrance interview date
        /// </summary>
        public virtual DateTime? EntranceInterviewDate { get; set; }

        /// <summary>
        /// Gets or sets the is first time in school.
        /// </summary>
        public virtual bool? IsFirstTimeInSchool { get; set; }

        /// <summary>
        /// Gets or sets the is first time post sec school.
        /// </summary>
        public virtual bool? IsFirstTimePostSecSchool { get; set; }

        /// <summary>
        /// Gets or sets the shift id.
        /// </summary>
        public virtual Shifts ShiftId { get; set; }

        /// <summary>
        /// Gets or sets the international lead.
        /// </summary>
        public virtual bool? InternationalLead { get; set; }

        /// <summary>
        /// Gets or sets the nationality.
        /// </summary>
        public virtual Nationalities Nationality { get; set; }

        /// <summary>
        /// Gets or sets the geographic type id.
        /// </summary>
        public virtual GeographicTypes GeographicTypeId { get; set; }

        /// <summary>
        /// Gets or sets the degree certificate seeking id.
        /// </summary>
        public virtual DegCertSeeking DegCertSeekingId { get; set; }

        /// <summary>
        /// Gets or sets the billing method id.
        /// </summary>
        public virtual StatusOption StudentStatus { get; protected set; }

        /// <summary>
        /// Gets or sets the student number.
        /// </summary>
        public virtual string StudentNumber { get; protected set; }
        #endregion
        /// <summary>
        /// Gets or sets the List of transaction associated with the lead
        /// </summary>
        public virtual IList<LeadTransactions> TransactionsList { get; protected set; }

        /// <summary>
        /// Gets or sets the List of extracurricular in this lead
        /// </summary>
        public virtual IList<ExtraCurriculars> ExtraCurricularList { get; protected set; }

        /// <summary>
        ///  Gets or sets List of employment of the lead.
        /// </summary>
        public virtual IList<AdLeadEmployment> LeadEmploymentsList { get; protected set; }

        /// <summary>
        ///  Gets or sets List of the Lead Skills
        /// </summary>
        public virtual IList<AdSkills> SkillList { get; protected set; }

        /// <summary>
        ///  Gets or sets List of notes associates with the Lead
        /// </summary>
        public virtual IList<AllNotes> NotesList { get; protected set; }

        /// <summary>
        ///  Gets or sets List of Historical records associates with the Lead
        /// </summary>
        public virtual IList<AllHistory> HistoryList { get; protected set; }

        /// <summary>
        ///  Gets or sets List of possible info in MRU for lead
        /// </summary>
        public virtual IList<ViewLeadMru> MruList { get; protected set; }

        /// <summary>
        ///  Gets or sets Education list for Lead
        /// </summary>
        public virtual IList<LeadEducation> EducationList { get; protected set; }

        /// <summary>
        ///  Gets or sets Statuses Change List
        /// </summary>
        public virtual IList<LeadStatusesChanges> StatusesChangesList { get; protected set; }

        /// <summary>
        ///  Gets or sets Other Contacts
        /// </summary>
        public virtual IList<LeadOtherContact> OtherContacts { get; protected set; }

        /// <summary>
        ///  Gets or sets Search campus ID
        /// </summary>
        public virtual Guid? SearchCampusId { get; protected set; }

        /// <summary>
        ///  Gets or sets  the list of task associated with this lead.
        /// </summary>
        public virtual IList<LeadTaskView> TasksList { get; protected set; }

        /// <summary>
        ///  Gets or sets Date of modification
        /// </summary>
        public virtual DateTime? ModDate { get; set; }

        /// <summary>
        ///  Gets or sets User that made the modification
        /// </summary>
        public virtual string ModUser { get; set; }

        #region Documents  manager

        /// <summary>
        ///  Gets or sets Document history
        /// </summary>
        public virtual IEnumerable<DocumentHistory> DocumentHistory { get; protected set; }

        /// <summary>
        ///  Gets or sets  the list of documents required to lead. the document can be fulfilled or bypassed.
        /// in either case the entry's done.
        /// </summary>
        public virtual IEnumerable<LeadDocument> LeadDocumentsList { get; protected set; }

        #endregion

        /// <summary>
        ///  Gets or sets Program of interest
        /// </summary>
        public virtual string ProgramOfInterest { get; protected set; }

        /// <summary>
        ///  Gets or sets Campus of interest
        /// </summary>
        public virtual string CampusOfInterest { get; protected set; }

        #region Groups and SDF

        /// <summary>
        /// Gets or sets many to many relation with Group list. The bridge table 
        /// will not be mapped. <code>(AdLEadByLeadGroups)</code>
        /// </summary>
        public virtual ICollection<LeadGroups> LeadGroupsList { get; protected set; }

        /// <summary>
        /// Gets or sets School Defined Fields list
        /// </summary>
        public virtual IList<SdfModuleValueLead> SdfModuleValueList { get; set; }
        #endregion

        #region Duplicate lead management

        /// <summary>
        /// Gets or sets Many to many with itself through table AdDuplicateMap
        /// </summary>
        public virtual IList<Lead> NewLeadsWithDuplicatesList { get; protected set; }

        /// <summary>
        /// Gets or sets List of Lead possible duplicates
        /// </summary>
        public virtual IList<Lead> DuplicateLeadsList { get; protected set; }

        #endregion

        /// <summary>
        /// Get the image path for the student photo
        /// </summary>
        /// <param name="studentImagePath">The image path</param>
        /// <param name="siteUrl">The URL of the site</param>
        public virtual void GetImageUrl(string studentImagePath, string siteUrl)
        {
            ////this.ImageUrl = siteUrl + "/images/face75.png";

            //// var docHistoryRecord = DocumentHistory.FirstOrDefault(n => n.DocumentType.ToUpper() == "PHOTOID");
            //// if (docHistoryRecord != null)
            //// ImageUrl = studentImagePath + "/" + docHistoryRecord.DocumentType + "/" + docHistoryRecord.ID.ToString() + docHistoryRecord.FileExtension;
            ////this.NotFoundImageUrl = siteUrl + "/images/face75_notfound.png";

            if (this.StudentId != Guid.Empty)
            {
                var enr = this.EnrollmentList.OrderByDescending(n => n.EnrollmentDate).FirstOrDefault();
                if (enr != null)
                {
                    this.SearchCampusId = enr.CampusId;
                }
            }
            else
            {
                if (this.Campus != null)
                {
                    this.SearchCampusId = this.Campus.ID;
                }
            }
        }

        /// <summary>
        /// Change the lead Status
        /// </summary>
        /// <param name="leadStatus">New lead Status object</param>
        public virtual void ChangeLeadStatus(UserDefStatusCode leadStatus)
        {
            this.LeadStatus = leadStatus;
            this.ModDate = DateTime.Now;
        }

        /// <summary>
        /// Command 1 Duplicates
        /// Copy fields from one lead to this
        /// </summary>
        /// <param name="leadA">Lead duplicated used to get fields to update this</param>
        /// <param name="username">The user name</param>
        public virtual void UpdateFieldsDuplicates(Lead leadA, string username)
        {
            var address = leadA.AddressList.SingleOrDefault(x => x.IsShowOnLeadPage);
            var myAddress = this.AddressList.SingleOrDefault(x => x.IsShowOnLeadPage);
            if (address != null)
            {
                ////this.AddressList.Remove(myaddress);
                ////this.AddressList.Add(address);
                if (myAddress != null)
                {
                    myAddress.UpdateAddress(address, this);
                }
                else
                {
                    LeadAddress newAddress = new LeadAddress(address, this);
                    this.AddressList.Add(newAddress);
                }
            }

            this.LastName = leadA.LastName.ToPascalCase();
            this.FirstName = leadA.FirstName.ToPascalCase();
            this.SourceDate = leadA.SourceDate;
            this.CampusOfInterest = leadA.CampusOfInterest;

            ////this.HomeEmail = leadA.HomeEmail;
            // Clean EmailList and copy the phone for Lead A if any
            this.LeadEmailList.Clear();
            foreach (LeadEmail email in leadA.LeadEmailList)
            {
                LeadEmail newEmail = new LeadEmail(email, this);
                newEmail.UpdateModDateAndUser(DateTime.Now, username);
                this.LeadEmailList.Add(newEmail);
            }

            // Clean phone list and copy the phone for Lead A if any
            this.LeadPhoneList.Clear();
            foreach (LeadPhone phone in leadA.LeadPhoneList)
            {
                LeadPhone newPhone = new LeadPhone(phone, this);
                newPhone.UpdateModDateAndUser(DateTime.Now, username);
                this.LeadPhoneList.Add(newPhone);
            }

            this.StateObj = leadA.StateObj;
            this.ProgramOfInterest = leadA.ProgramOfInterest;
            this.CampaignObj = leadA.CampaignObj;
            this.ProspectId = leadA.ProspectId;
        }

        /// <summary>
        /// Update Assign lead to Campus
        /// </summary>
        /// <param name="campus">Campus object</param>
        /// <param name="userName">The user name </param>
        public virtual void UpdateAssignLeadToCampus(Campus campus, string userName)
        {
            this.Campus = campus;
            this.AssignedDate = DateTime.Now;
            this.ModDate = DateTime.Now;
            this.ModUser = userName;
        }

        /// <summary>
        /// Update Lead Assigned
        /// </summary>
        /// <param name="adminRep">Admission Represent Object</param>
        /// <param name="leadStatus">Lead Status Object</param>
        /// <param name="userName">The User Name</param>
        public virtual void UpdateAssignLeadToRep(User adminRep, UserDefStatusCode leadStatus, string userName)
        {
            this.AdmissionRepUserObj = adminRep;
            this.LeadStatus = leadStatus;
            this.AssignedDate = DateTime.Now;
            this.ModDate = DateTime.Now;
            this.ModUser = userName;
        }

        /// <summary>
        /// Get the image-URL, if does not exists get a dummy image path.
        /// Also get the image encoded 64
        /// </summary>
        /// <param name="studentImagePath">The real path to image</param>
        /// <param name="siteUrl">the URL where the image it</param>
        public virtual void GetLeadImageUrl(string studentImagePath, string siteUrl)
        {
            this.ImageEncode64 = string.Empty;
            var docHistoryRecord = this.DocumentHistory.FirstOrDefault(n => n.DocumentType.ToUpper() == "PHOTOID");
            this.ImageUrl = (docHistoryRecord != null) ? string.Format("{0}/{1}/{2}{3}", studentImagePath, docHistoryRecord.DocumentType, docHistoryRecord.ID, docHistoryRecord.FileExtension) : siteUrl + "/images/face75.png";
        }

        /// <summary>
        /// Update single values (no entities) present in the input
        /// </summary>
        /// <param name="input">Info page input information</param>
        public virtual void UpdateLeadInfoPageSingleValues(LeadInfoPageOutputModel input)
        {
            // Demographic
            this.FirstName = input.FirstName.ToPascalCase();
            this.LastName = input.LastName.ToPascalCase();
            this.MiddleName = input.MiddleName.ToPascalCase();
            this.NickName = input.NickName;
            this.SSN = input.SSN;
            this.BirthDate = input.Dob;
            this.Age = input.Age;
            this.AlienNumber = input.AlienNumber;
            this.Dependants = input.Dependants;
            this.DriverLicenseNumber = input.DriverLicenseNumber;
            this.DistanceToSchool = input.DistanceToSchool;
            this.BestTime = (input.BestTime == null) ? null : input.BestTime.ToUpperInvariant();

            // Contact
            this.NoneEmail = input.NoneEmail;
            this.PreferredContactId = input.PreferredContactId;

            // Source
            this.SourceDate = input.SourceDateTime;

            // Other
            this.DateApplied = input.DateApplied;
            this.AssignedDate = input.AssignedDate;
            this.HighSchoolGradDate = input.HighSchoolGradDate;
            this.AttendingHs = input.AttendingHs;

            // Comments = input.Comments;
            this.ExpectedStart = input.ExpectedStart;

            // General
            this.ModUser = input.ModUser;
            this.ModDate = DateTime.Now;
        }

        /// <summary>
        /// This is a universal method to update entities in Lead
        /// Use the name in lead of the entity property
        /// </summary>
        /// <param name="key">
        /// Supported Keys:
        /// <code>LeadStatus
        /// Campus
        /// AdmissionRepUserObj
        /// ProgramGroupObj
        /// ProgramObj
        /// PrgVersionObj
        /// AttendTypeObj
        /// ProgramScheduleObj</code>
        /// </param>
        /// <param name="entity">entity to be entered</param>
        public virtual void UpdateObject(string key, dynamic entity)
        {
            switch (key)
            {
                case "LeadStatus":
                    {
                        this.LeadStatus = entity;
                        break;
                    }

                case "Campus":
                    {
                        this.Campus = entity;
                        break;
                    }

                case "ProgramGroupObj":
                    {
                        this.ProgramGroupObj = entity;
                        break;
                    }

                case "ProgramObj":
                    {
                        this.ProgramObj = entity;
                        break;
                    }

                case "PrgVersionObj":
                    {
                        this.ProgramVersion = entity;
                        break;
                    }

                case "AttendTypeObj":
                    {
                        this.AttendTypeObj = entity;
                        break;
                    }

                case "ProgramScheduleObj":
                    {
                        this.ProgramScheduleObj = entity;
                        break;
                    }

                case "PrefixObj":
                    {
                        this.PrefixObj = entity;
                        break;
                    }

                case "SuffixObj":
                    {
                        this.SuffixObj = entity;
                        break;
                    }

                case "GenderObj":
                    {
                        this.GenderObj = entity;
                        break;
                    }

                case "EthnicityObj":
                    {
                        this.EthnicityObj = entity;
                        break;
                    }

                case "CitizenshipsObj":
                    {
                        this.CitizenshipsObj = entity;
                        break;
                    }

                case "DependencyTypeObj":
                    {
                        this.DependencyTypeObj = entity;
                        break;
                    }

                case "MaritalStatusObj":
                    {
                        this.MaritalStatusObj = entity;
                        break;
                    }

                case "FamilyIncomingObj":
                    {
                        this.FamilyIncomingObj = entity;
                        break;
                    }

                case "HousingTypeObj":
                    {
                        this.HousingTypeObj = entity;
                        break;
                    }

                case "DriverLicenseStateCodeObj":
                    {
                        this.DriverLicenseStateCodeObj = entity;
                        break;
                    }

                case "TransportationObj":
                    {
                        this.TransportationObj = entity;
                        break;
                    }

                // case "AddressType":
                // {
                // AddressTypeObj = entity;
                // break;
                // }

                // case "CountyObj":
                // {
                // CountyObj = entity;
                // break;
                // }

                // case "CountryObj":
                // {
                // CountryObj = entity;
                // break;
                // }
                case "StateObj":
                    {
                        this.StateObj = entity;
                        break;
                    }

                case "SourceCategoryObj":
                    {
                        this.SourceCategoryObj = entity;
                        break;
                    }

                case "SourceTypeObj":
                    {
                        this.SourceTypeObj = entity;
                        break;
                    }

                case "SourceAdvertisementObj":
                    {
                        this.SourceAdvertisementObj = entity;
                        break;
                    }

                case "AdmissionRepUserObj":
                    {
                        this.AdmissionRepUserObj = entity;
                        break;
                    }

                case "AgencySponsorsObj":
                    {
                        this.AgencySponsorsObj = entity;
                        break;
                    }

                case "ReasonNotEnrolledObj":
                    {
                        this.ReasonNotEnrolledObj = entity;
                        break;
                    }

                case "PreviousEducationObj":
                    {
                        this.PreviousEducationObj = entity;
                        break;
                    }

                case "HighSchoolsObj":
                    {
                        this.HighSchoolsObj = entity;
                        break;
                    }

                case "AdminCriteriaObj":
                    {
                        this.AdminCriteriaObj = entity;
                        break;
                    }

                default:
                    {
                        throw new NotSupportedException(string.Format("This key: {0} is not supported", key));
                    }
            }
        }

        /// <summary>
        /// Update the vehicle list
        /// </summary>
        /// <param name="vehicles">
        /// The vehicle list
        /// </param>
        public virtual void UpdateVehiclesList(IList<VehicleOutputModel> vehicles)
        {
            if (this.VehiclesList == null || this.VehiclesList.Count == 0)
            {
                // no vehicles, first time
                if (this.VehiclesList == null)
                {
                    this.VehiclesList = new List<AdVehicles>();
                }

                foreach (var m in vehicles)
                {
                    var vehicle = new AdVehicles(
                        0,
                        m.Position,
                        this,
                        m.Permit,
                        m.Make,
                        m.Model,
                        m.Color,
                        m.Plate,
                        m.ModUser);
                    this.VehiclesList.Add(vehicle);
                }
            }
            else
            {
                // Vehicles exists
                foreach (VehicleOutputModel m in vehicles)
                {
                    var m1 = m;
                    var v = this.VehiclesList.SingleOrDefault(x => x.Position == m1.Position);
                    if (v == null)
                    {
                        var vehicle = new AdVehicles(
                            0,
                            m.Position,
                            this,
                            m.Permit,
                            m.Make,
                            m.Model,
                            m.Color,
                            m.Plate,
                            m.ModUser);
                        this.VehiclesList.Add(vehicle);
                    }
                    else
                    {
                        v.Update(m);
                    }
                }
            }
        }

        /// <summary>
        /// The update of three phone list.
        /// </summary>
        /// <param name="phonesList">
        /// The phones list.
        /// </param>
        /// <param name="phoneTypes">
        /// The phone types.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        public virtual void UpdatePhoneList(IList<LeadPhonesOutputModel> phonesList, IQueryable<SyPhoneType> phoneTypes, SyStatuses status)
        {
            foreach (LeadPhonesOutputModel model in phonesList)
            {
                // case 1 no previous phone, not new phone
                if (model.ID == "0" && string.IsNullOrWhiteSpace(model.Phone))
                {
                    continue;
                }

                if (model.ID == "0")
                {
                    // check again if there is no phone, especially in the position it is being inserted
                    var currentPhone = this.LeadPhoneList.FirstOrDefault(x => x.Position == model.Position);
                    if (currentPhone != null)
                    {
                        this.LeadPhoneList.Remove(currentPhone);
                    }

                    // New phone. create a new phone and insert in the collection.
                    LeadPhonesOutputModel model1 = model;
                    var phonetype = phoneTypes.SingleOrDefault(x => x.ID.ToString() == model1.PhoneTypeId);
                    var phone = new LeadPhone(new Guid(), model.Phone, model.Extension, model.IsForeignPhone, model.Position, this.ModUser, DateTime.Now, phonetype, this, status);
                    this.LeadPhoneList.Add(phone);
                    continue;
                }

                Guid guid;
                if (Guid.TryParse(model.ID, out guid))
                {
                    if (string.IsNullOrWhiteSpace(model.Phone))
                    {
                        // Remove phone
                        var phone = this.LeadPhoneList.SingleOrDefault(x => x.ID == guid);
                        this.LeadPhoneList.Remove(phone);
                    }
                    else
                    {
                        // update phone
                        var phone = this.LeadPhoneList.SingleOrDefault(x => x.ID == guid);
                        LeadPhonesOutputModel model1 = model;
                        var phonetype = phoneTypes.SingleOrDefault(x => x.ID.ToString() == model1.PhoneTypeId);
                        if (phone != null)
                        {
                            phone.UpdatePhone(model, this.ModUser, phonetype);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The output model that return from client
        /// </summary>
        /// <param name="model">
        /// The lead address.
        /// </param>
        /// <param name="addressTypeList">
        /// The address type list.
        /// </param>
        /// <param name="countyList">
        /// The county list
        /// </param>
        /// <param name="usaStateList">
        /// States of United States list
        /// </param>
        /// <param name="countryList">
        /// Country list
        /// </param>
        /// <param name="statusesList">
        /// The Statuses List.
        /// </param>
        public virtual void UpdateAddresslist(
            LeadAddressOutputModel model,
            IQueryable<AddressType> addressTypeList,
            IQueryable<County> countyList,
            IQueryable<SystemStates> usaStateList,
            IQueryable<Country> countryList,
            IQueryable<SyStatuses> statusesList)
        {
            // Get the auxiliary object
            var addresstypeObj = addressTypeList.SingleOrDefault(x => x.ID.ToString() == model.AddressTypeId);
            var stateObj = usaStateList.SingleOrDefault(x => x.ID.ToString() == model.StateId);
            var countryObj = countryList.SingleOrDefault(x => x.ID.ToString() == model.CountryId);
            var countyObj = countyList.SingleOrDefault(x => x.ID.ToString() == model.CountyId);
            SyStatuses statusObj;
            if (model.StatusId == null)
            {
                statusObj = statusesList.SingleOrDefault(x => x.StatusCode == "A");
            }
            else
            {
                statusObj = statusesList.SingleOrDefault(x => x.ID.ToString() == model.StatusId);
            }

            var addressHasAnyValue = !string.IsNullOrEmpty(model.Address2) || !string.IsNullOrEmpty(model.AddressApto)
                                                                           || !string.IsNullOrEmpty(model.City)
                                                                           || stateObj != null
                                                                           || !string.IsNullOrEmpty(model.ZipCode)
                                                                           || countryObj != null || countyObj != null;

            if (addressHasAnyValue && (addresstypeObj == null || model.AddressTypeId == null))
            {
                addresstypeObj = addressTypeList.SingleOrDefault(x => x.AddessDescrip.ToLower() == "other");
            }

            // Get the list of address associated with this lead
            // If the list is empty....
            if (this.AddressList == null || this.AddressList.Count == 0)
            {
                if ((!string.IsNullOrWhiteSpace(model.Address1) & model.AddressTypeId != null) || addressHasAnyValue)
                {
                    // Model has address1 valid. Insert the address
                    var address = new LeadAddress(
                        addresstypeObj,
                         model.Address1,
                        model.Address2,
                        model.AddressApto,
                        model.City,
                        stateObj,
                        model.ZipCode,
                        countryObj,
                        statusObj,
                        true,
                        true,
                        model.Moduser,
                        this,
                        model.IsInternational,
                        model.StateInternational,
                        countyObj,
                        string.Empty,
                        model.CountryId);

                    if (this.AddressList == null)
                    {
                        this.AddressList = new List<LeadAddress>();
                    }

                    this.AddressList.Add(address);
                }
                else
                {
                    // Model has not address1. Finish
                    return;
                }
            }

            // Select the address in the adLeadAddress to be show in page
            var addressShow = this.AddressList.SingleOrDefault(x => x.IsShowOnLeadPage);

            // Exists one address that is showed in the lead page
            if (addressShow != null)
            {
                // if the address exists
                if ((!string.IsNullOrWhiteSpace(model.Address1) & model.AddressTypeId != null) || addressHasAnyValue)
                {
                    // AND Model has a valid address1. Update the list
                    addressShow.UpdateAddress(
                        addresstypeObj,
                        model.Address1,
                        model.Address2,
                        model.City,
                        stateObj,
                        model.ZipCode,
                        countryObj,
                        statusObj,
                        addressShow.IsMailingAddress,
                        true,
                        model.Moduser,
                        this,
                        model.IsInternational,
                        model.StateInternational,
                        countyObj,
                        addressShow.CountyInternational,
                        (countryObj == null) ? string.Empty : countryObj.CountryDescrip);
                }
                else
                {
                    // AND Model is empty. Delete the address. Because the address show is the address with the 
                    // flag is show in the lead page.
                    this.AddressList.Remove(addressShow);
                }

                return;
            }

            // If exists another address in the adLeadAddress wit the same address1.
            var otherAddress = this.AddressList.SingleOrDefault(x => x.Address1 == model.Address1 && string.IsNullOrWhiteSpace(x.Address1) == false);

            if (otherAddress != null)
            {
                // update the other address to be show is Show IN Lead Info Page Flag
                // AND Model has a valid address1. Update the list
                otherAddress.UpdateAddress(
                    addresstypeObj,
                    model.Address1,
                    model.Address2,
                    model.City,
                    stateObj,
                    model.ZipCode,
                    countryObj,
                    statusObj,
                    otherAddress.IsMailingAddress,
                    true,
                    model.Moduser,
                    this,
                    model.IsInternational,
                    model.StateInternational,
                    countyObj,
                    otherAddress.CountyInternational,
                    (countryObj == null) ? string.Empty : countryObj.CountryDescrip);
                return;
            }

            // If the address does not exists
            if ((!string.IsNullOrWhiteSpace(model.Address1) & model.AddressTypeId != null) || addressHasAnyValue)
            {
                // Model is valid. Insert in the list
                // Model has address1 valid. Insert the address
                var address = new LeadAddress(
                    addresstypeObj,
                    model.Address1,
                    model.Address2,
                    model.AddressApto,
                    model.City,
                    stateObj,
                    model.ZipCode,
                    countryObj,
                    statusObj,
                    true,
                    true,
                    model.Moduser,
                    this,
                    model.IsInternational,
                    model.StateInternational,
                    countyObj,
                    string.Empty,
                    model.CountryId);

                if (this.AddressList == null)
                {
                    this.AddressList = new List<LeadAddress>();
                }

                this.AddressList.Add(address);
            }
        }

        /// <summary>
        /// Update the email list for the lead.
        /// </summary>
        /// <param name="noneEmail">
        /// None Email true the lead has not email
        /// </param>
        /// <param name="emailList">
        /// the list of email address
        /// </param>
        /// <param name="emailtypes">
        /// If true all email are deleted from the lead.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        public virtual void UpdateEmailList(bool noneEmail, IList<LeadEmailOutputModel> emailList, IQueryable<EmailType> emailtypes, SyStatuses status)
        {
            this.NoneEmail = noneEmail;

            if (noneEmail)
            {
                this.LeadEmailList.Clear();
                return;
            }

            foreach (LeadEmailOutputModel model in emailList)
            {
                // case 1 no previous email, not new email
                if (model.ID == "0" && string.IsNullOrWhiteSpace(model.Email))
                {
                    continue;
                }

                if (model.ID == "0")
                {
                    // New email. create a new email and insert in the collection.
                    LeadEmailOutputModel model1 = model;
                    var emailtype = emailtypes.SingleOrDefault(x => x.ID.ToString() == model1.EmailTypeId);
                    var isshow = model.IsShowOnLeadPage == "Yes";
                    var email = new LeadEmail(new Guid(), model.Email, model.IsPreferred, isshow, false, this.ModUser, DateTime.Now, this, emailtype, status);
                    this.LeadEmailList.Add(email);
                    continue;
                }

                Guid guid;
                if (Guid.TryParse(model.ID, out guid))
                {
                    if (string.IsNullOrWhiteSpace(model.Email))
                    {
                        var email = this.LeadEmailList.SingleOrDefault(x => x.ID == guid);
                        this.LeadEmailList.Remove(email);
                    }
                    else
                    {
                        // update email
                        var email = this.LeadEmailList.SingleOrDefault(x => x.ID == guid);
                        LeadEmailOutputModel model1 = model;
                        var emailtype = emailtypes.SingleOrDefault(x => x.ID.ToString() == model1.EmailTypeId);
                        if (email != null)
                        {
                            email.UpdateEmail(model, this.ModUser, emailtype);
                        }
                    }
                }
            }
        }

        ///// <summary>
        ///// This is used in contact page 
        ///// </summary>
        ///// <param name="comments"></param>
        ///// <param name="modUser"></param>
        // public virtual void UpdateComments(string comments, string modUser)
        // {
        // // Comments = comments;
        // ModUser = modUser;
        // ModDate = DateTime.Now;
        // }

        /// <summary>
        ///  The set notes list.
        /// </summary>
        /// <param name="notesList">
        ///  The notes list.
        /// </param>
        public virtual void SetNotesList(IList<AllNotes> notesList)
        {
            if (this.NotesList == null)
            {
                this.NotesList = new List<AllNotes>();
            }

            foreach (AllNotes notes in notesList)
            {
                this.NotesList.Add(notes);
            }
        }
        public virtual void UpdateStatus(StatusOption status)
        {
            this.StudentStatus = status;
        }
    }
}