﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadQuick.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Domain for Table VIEW_GetQuickLeadFields
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Lead
{
    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// Domain for Table VIEW_GetQuickLeadFields
    /// </summary>
    public class LeadQuick : DomainEntityWithTypedID<int>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadQuick"/> class. 
        /// Protected Constructor.
        /// </summary>
        public LeadQuick()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadQuick"/> class. 
        /// Constructor use this to create the object.
        /// </summary>
        /// <param name="tblFldsId">
        /// The table Field Id.
        /// </param>
        /// <param name="fldId">
        /// The field Id.
        /// </param>
        /// <param name="sectionId">
        /// The section Id.
        /// </param>
        /// <param name="fldName">
        /// The field Name.
        /// </param>
        /// <param name="required">
        /// The required.
        /// </param>
        /// <param name="ddlId">
        /// The drop down list Id.
        /// </param>
        /// <param name="fldLen">
        /// The field Length.
        /// </param>
        /// <param name="fldType">
        /// The field Type.
        /// </param>
        /// <param name="caption">
        /// The caption.
        /// </param>
        /// <param name="controlName">
        /// The control Name.
        /// </param>
        /// <param name="fldTypeId">
        /// The field Type Id.
        /// </param>
        /// <param name="ctrlIdName">
        /// The ctrl Id Name.
        /// </param>
        /// <param name="propName">
        /// The prop Name.
        /// </param>
        /// <param name="parentCtrlId">
        /// The parent Ctrl Id.
        /// </param>
        public LeadQuick(
 int tblFldsId, int fldId, int sectionId, string fldName, int required, int? ddlId, int fldLen, string fldType, string caption, string controlName, int fldTypeId, string ctrlIdName, string propName, string parentCtrlId)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.TblFldsId = tblFldsId;
            this.FldId = fldId;
            this.SectionId = sectionId;
            this.FldName = fldName;
            this.Required = required;
            this.DdlId = ddlId;
            this.FldLen = fldLen;
            this.FldType = fldType;
            this.Caption = caption;
            this.ControlName = controlName;
            this.FldTypeId = fldTypeId;
            this.CtrlIdName = ctrlIdName;
            this.PropName = propName;
            this.ParentCtrlId = parentCtrlId;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the table field id.
        /// </summary>
        public virtual int TblFldsId { get; set; }

        /// <summary>
        /// Gets or sets the field id.
        /// </summary>
        public virtual int FldId { get; set; }

        /// <summary>
        /// Gets or sets the section id.
        /// </summary>
        public virtual int SectionId { get; set; }

        /// <summary>
        /// Gets or sets the field name
        /// </summary>
        public virtual string FldName { get; protected set; }

        /// <summary>
        /// Gets or sets if the field is required or not
        /// </summary>
        public virtual int Required { get; protected set; }

        /// <summary>
        /// Gets or sets the length of the field
        /// </summary>
        public virtual int FldLen { get; protected set; }

        /// <summary>
        /// Gets or sets the data type of the field
        /// </summary>
        public virtual string FldType { get; protected set; }

        /// <summary>
        /// Gets or sets the name given to the label in UI
        /// </summary>
        public virtual string Caption { get; protected set; }

        /// <summary>
        /// Gets or sets the value if it has dropdown mostly it is null
        /// </summary>
        public virtual string ControlName { get; protected set; }

        /// <summary>
        /// Gets or sets the value if the field is a dropdown
        /// </summary>
        public virtual int? DdlId { get; protected set; }

        /// <summary>
        /// Gets or sets the field type id.
        /// </summary>
        public virtual int FldTypeId { get; protected set; }

        /// <summary>
        /// Gets or sets the sequence.
        /// </summary>
        public virtual int Sequence { get; protected set; }

        /// <summary>
        /// Gets or sets the control id name. this is the name of the static control in Lead Info page
        /// </summary>
        public virtual string CtrlIdName { get; protected set; }

        /// <summary>
        /// Gets or sets the property name. this is the name in the mapper
        /// </summary>
        public virtual string PropName { get; protected set; }

        /// <summary>
        /// Gets or sets the Parent control Id. this is the name in the mapper
        /// </summary>
        public virtual string ParentCtrlId { get; protected set; }
        #endregion
    }
}