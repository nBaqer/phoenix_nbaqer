﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.Catalogs;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// LeadOtherContactAddress is the Domain Object for the adLeadOtherContactsAddreses table
    /// </summary>
    public class LeadOtherContactAddress : DomainEntity
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public LeadOtherContactAddress()
        {

        }
        /// <summary>
        /// Create New Lead Other Contact Address
        /// </summary>
        /// <param name="OtherContact"></param>
        /// <param name="Lead"></param>
        /// <param name="AddressType"></param>
        /// <param name="Address1"></param>
        /// <param name="Address2"></param>
        /// <param name="City"></param>
        /// <param name="State"></param>
        /// <param name="County"></param>
        /// <param name="Country"></param>
        /// <param name="ZipCode"></param>
        /// <param name="IsInternational"></param>
        /// <param name="IsMailingAddress"></param>
        /// <param name="Status"></param>
        /// <param name="ModUser"></param>
        /// <param name="StateInternational"></param>
        /// <param name="CountyInternational"></param>
        /// <param name="CountryInternational"></param>
        public LeadOtherContactAddress( LeadOtherContact OtherContact, Lead Lead, AddressType AddressType, string Address1, string Address2, string City, SystemStates State, County County, Country Country,
            string ZipCode, bool IsInternational, bool IsMailingAddress, SyStatuses Status, string ModUser, string StateInternational, string CountyInternational, string CountryInternational)
        {

            this.LeadOtherContact = OtherContact;

            this.Lead = Lead;

            this.AddressType = AddressType;
            this.Address1 = Address1;
            this.Address2 = Address2;
            this.City = City;

            this.ZipCode = ZipCode;
            this.IsInternational = IsInternational;
            this.IsMailingAddress = IsMailingAddress;
            this.Status = Status;
            this.ModUser = ModUser;
            this.ModDate = DateTime.Now;

            if (!this.IsInternational)
            {
                this.State = State;
                this.Country = Country;
                this.County = County;
                this.StateInternational = null;
                this.CountyInternational = null;
                this.CountryInternational = null;
            }
            else
            {
                this.State = null;
                this.Country = null;
                this.County = null;
                this.StateInternational = StateInternational;
                this.CountyInternational = CountyInternational;
                this.CountryInternational = CountryInternational;
            }

        }
        /// <summary>
        /// ID
        /// </summary>
        public virtual Guid OtherContactsAddresesId
        {
            get { return ID; }
            protected set { ID = value; }
        }
        /// <summary>
        /// Lead Other Contact 
        /// </summary>
        public virtual LeadOtherContact LeadOtherContact { get; protected set; }
        /// <summary>
        /// Lead
        /// </summary>
        public virtual Lead Lead { get; protected set; }
        /// <summary>
        /// Address Type
        /// </summary>
        public virtual AddressType AddressType { get; protected set; }
        /// <summary>
        /// Address 1
        /// </summary>
        public virtual string Address1 { get; protected set; }
        /// <summary>
        /// Address 2
        /// </summary>
        public virtual string Address2 { get; protected set; }
        /// <summary>
        /// City
        /// </summary>
        public virtual string City { get; protected set; }
        /// <summary>
        /// State
        /// </summary>
        public virtual SystemStates State { get; protected set; }
        /// <summary>
        /// The States Name if the address is an international address
        /// </summary>
        public virtual string StateInternational { get; set; }
        /// <summary>
        /// The Country Name if the address is an international adddress
        /// </summary>
        public virtual string CountryInternational { get; set; }
        /// <summary>
        /// /// The County Name if the address is an international adddress
        /// </summary>
        public virtual string CountyInternational { get; set; }
        /// <summary>
        /// Zip Code
        /// </summary>
        public virtual string ZipCode { get; protected set; }
        /// <summary>
        /// Country
        /// </summary>
        public virtual Country Country { get; protected set; }
        /// <summary>
        /// County
        /// </summary>
        public virtual County County { get; protected set; }
        /// <summary>
        /// Status
        /// </summary>
        public virtual SyStatuses Status { get; protected set; }
        /// <summary>
        /// Flag Indicating if this is the mailing address
        /// </summary>
        public virtual bool IsMailingAddress { get; protected set; }
        /// <summary>
        /// Flag indicating if this is an international address
        /// </summary>
        public virtual bool IsInternational { get; protected set; }
        /// <summary>
        /// User last modified this object
        /// </summary>
        public virtual string ModUser { get; protected set; }
        /// <summary>
        /// Last Modified Date
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Update Lead Other Contact
        /// </summary>
        /// <param name="AddressType"></param>
        /// <param name="Status"></param>
        /// <param name="Address1"></param>
        /// <param name="Address2"></param>
        /// <param name="City"></param>
        /// <param name="State"></param>
        /// <param name="County"></param>
        /// <param name="Country"></param>
        /// <param name="ZipCode"></param>
        /// <param name="IsInternational"></param>
        /// <param name="IsMailingAddress"></param>
        /// <param name="ModUser"></param>
        /// <param name="StateInternational"></param>
        /// <param name="CountyInternational"></param>
        /// <param name="CountryInternational"></param>
        public virtual void Update(AddressType AddressType, SyStatuses Status, string Address1, string Address2, string City, SystemStates State, County County, Country Country,
            string ZipCode, bool IsInternational, bool IsMailingAddress,  string ModUser, string StateInternational, string CountyInternational, string CountryInternational)
        {

            this.Address1 = Address1;
            this.Address2 = Address2;
            this.City = City;
            this.ZipCode = ZipCode;
            this.IsInternational = IsInternational;
            this.IsMailingAddress = IsMailingAddress;
            this.ModUser = ModUser;
            this.ModDate = DateTime.Now;

            if (!this.IsInternational)
            {
                this.State = State;
                this.Country = Country;
                this.County = County;
                this.StateInternational = null;
                this.CountyInternational = null;
                this.CountryInternational = null;
            }
            else
            {
                this.State = null;
                this.Country = null;
                this.County = null;
                this.StateInternational = StateInternational;
                this.CountyInternational = CountyInternational;
                this.CountryInternational = CountryInternational;
            }

        }
    }
}
