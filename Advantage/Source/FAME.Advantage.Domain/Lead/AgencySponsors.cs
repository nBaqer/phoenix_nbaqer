﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AgencySponsors.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Agency that bring the lead (not the vendor)
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Lead
{
    using System;
    using System.Collections.Generic;
    using SystemStuff;
    using Campuses.CampusGroups;
    using Infrastructure.Entities;

    /// <summary>
    /// Agency that bring the lead (not the vendor)
    /// </summary>
    public class AgencySponsors : DomainEntity
    {
        /// <summary>
        /// Gets or sets Agency Name. More explicative name for Agency.
        /// </summary>
        public virtual string AgencySpDescrip { get; protected set; }

        /// <summary>
        /// Gets or sets Agency Code. This should be unique for each Agency
        /// This code must be only a word.
        /// </summary>
        public virtual string AgencySpCode { get; protected set; }

        /// <summary>
        /// Gets or sets Status Active Inactive 
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// Gets or sets Campus group object for the Category
        /// </summary>
        public virtual CampusGroup CampusGroupObj { get; protected set; }

        /// <summary>
        /// Gets or sets Who change the record
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets Date of change
        /// </summary>
        public virtual DateTime? ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets List of lead that comes from the particular Agency
        /// </summary>
        public virtual IList<Lead> LeadList { get; protected set; }
    }
}