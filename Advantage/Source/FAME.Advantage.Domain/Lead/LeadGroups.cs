﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadGroups.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Lead.LeadGroups
// </copyright>
// <summary>
//   Domain Entity for table adLeadGroups
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Requirements;
    using FAME.Advantage.Domain.SystemStuff;
    using System.Collections;
    using System.Security.Cryptography.X509Certificates;

    using FAME.Advantage.Domain.SystemStuff.Resources;

    /// <summary>
    /// Domain Entity for table adLeadGroups
    /// </summary>
    public class LeadGroups : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadGroups"/> class.
        /// </summary>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="modifiedDate">
        /// The modified date.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        public LeadGroups(string code, string description, DateTime? modifiedDate, string modifiedUser)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.ModifiedDate = modifiedDate;
            this.ModifiedUser = modifiedUser;
            this.Code = code;
            this.Description = description;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadGroups"/> class. 
        /// </summary>
        protected LeadGroups()
        {
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Gets or sets the use for scheduling.
        /// </summary>
        public virtual bool? UseForScheduling { get; set; }

        /// <summary>
        /// Gets or sets the use for Student Group Tracking.
        /// </summary>
        public virtual bool? UseForStudentGroupTracking { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public virtual DateTime? ModifiedDate { get; protected set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public virtual string ModifiedUser { get; protected set; }

        /// <summary>
        /// Gets or sets the campus group.
        /// </summary>
        public virtual Campuses.CampusGroups.CampusGroup CampusGroup { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public virtual SyStatuses Status { get; protected set; }

        /// <summary>
        /// Gets or sets the leads list.
        /// </summary>
        public virtual IList<Lead> LeadsList { get; protected set; }

        /// <summary>
        /// Gets or sets a List of the Requirements Groups that have been set this instance of Lead Group. A Lead Group can belong to many Requirement Groups.
        /// </summary>
        public virtual IList<RequirementGroup> RequirementGroups { get; protected set; }

        /// <summary>
        /// Gets the bridge that joins the Lead Groups, Requirements Groups and Requirements, based on the List of Requirements Groups that are associated to this instance of Lead Group, Get the Requirements Group Definitions.
        /// </summary>
        public virtual IList<RequirementGroupDefinition> RequirementGroupDefinitions
        {
            get
            {
                return this.RequirementGroups.Where(x => x.Status.StatusCode == "A").SelectMany(x => x.RequirementGroupDefinitions).Where(x => x.LeadGroup.ID == this.ID).ToList();
            }
        }

        /// <summary>
        /// Gets or sets the requirement lead group.
        /// </summary>
        public virtual IList<RequirementLeadGroupBridge> RequirementLeadGroup { get; protected set; }

        /// <summary>
        /// Gets the requirements associated to this instance of Lead Group.
        /// </summary>
        /// <param name="isMandatory">
        /// The is Mandatory.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public virtual IList<Requirement> Requirements(bool isMandatory)
        {
            return this.RequirementEffectiveDates(isMandatory).Where(x => x != null).Select(x => x.Requirement).Where(x => x.Status.StatusCode == "A").ToList();
        }

        /// <summary>
        /// Gets the requirements that are part of requirement groups that belongs to this Lead Group.
        /// </summary>
        /// <param name="isMandatory">
        /// The is Mandatory.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public virtual IList<Requirement> RequirementsPartOfRequirementGroups(bool isMandatory)
        {
            return this.RequirementGroupDefinitions.Where(x => x.RequirementGroup.Status.StatusCode == "A").Select(x => x.Requirement).Where(x => x.Status.StatusCode == "A").ToList();
        }

        /// <summary>
        /// Gets the requirement effective dates (bridge entity for the requirement and it's details effective dates, if is Mandatory for all the lead groups).
        /// </summary>
        /// <param name="isMandatory">
        /// The is Mandatory.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public virtual IList<RequirementEffectiveDates> RequirementEffectiveDates(bool isMandatory)
        {
            IList<RequirementEffectiveDates> values = null;

            if (isMandatory)
            {
                values =
                    this.RequirementLeadGroup.Where(
                        x =>
                        x.LeadGroup.Status.StatusCode == "A"
                        && (x.IsRequired == null || x.IsRequired.Value))
                        .Select(x => x.RequirementEffectiveDates)
                        .Where(x => x != null)
                        .ToList();
            }
            else
            {
                values = this.RequirementLeadGroup.Where(x => x.LeadGroup.Status.StatusCode == "A" && (x.IsRequired == null || x.IsRequired.Value == false)).Select(x => x.RequirementEffectiveDates).Where(x => x != null && x.IsMandatory == false).ToList();
            }

            return values;
        }

        /// <summary>
        /// The get requirements 
        /// Where they are required or optional for the lead group based 
        /// and effective dates match the current date 
        /// and where the status is active 
        /// and not part of Requirement Group
        /// and Required for Enrollment
        /// </summary>
        /// <param name="isMandatory">
        /// The is mandatory (if true, will return required requirements; if false, will return optional requirements).
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public virtual IList<Requirement> GetRequirementsForEnrollment(bool isMandatory)
        {
            List<Requirement> requirements = this.Requirements(isMandatory).Where(n => n.GetIsRequirementForAdmissionEnrollment()).ToList();

            List<Requirement> requirementsPartOfRequirementGroups =
                this.RequirementsPartOfRequirementGroups(isMandatory)
                    .Where(n => n.GetIsRequirementForAdmissionEnrollment())
                    .Where(x => x.Status.StatusCode == "A")
                    .ToList();

            List<Guid> reqsInRequirementGroupsIds = requirementsPartOfRequirementGroups.Select(x => x.ID).ToList();

            return requirements.Where(x => !reqsInRequirementGroupsIds.Contains(x.ID)).ToList();
        }

        /// <summary>
        /// The get optional requirements for enrollment.
        /// Where they are optional for the lead group  
        /// and effective dates match the current date 
        /// and where the status is active 
        /// and Required for Enrollment
        /// </summary>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public virtual IList<Requirement> GetOptionalRequirementsForEnrollment()
        {
            List<Requirement> requirements = this.Requirements(false).Where(n => n.GetIsRequirementForAdmissionEnrollment()).ToList();

            return requirements.ToList();
        }

        /// <summary>
        /// The get requirement groups for enrollment.
        /// </summary>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public virtual IList<RequirementGroup> GetRequirementGroupsForEnrollment()
        {
            var groups = this.RequirementGroups.Where(x => x.Status.StatusCode == "A" && x.LeadGroupRequirementGroupBridge.Where(m => m.LeadGroup.ID == this.ID).Select(n => n.Status).Any(n => n.StatusCode == "A")).Where(x => x.RequirementGroupDefinitions.Any(n => n.Requirement.GetIsRequirementForAdmissionEnrollment()) && x.SetLeadGroupId(this.ID)).ToList();
            return groups;
        }
    }
}