﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// Hold the description and code of the form of the vendor is paid.
    /// Normally should be pre-load the following values
    /// Pay by Lead Enrolled     PERLENROLLED
    /// Pay by Lead Send         PERLSEND
    /// Pay monthly fixed value  PFIXMONTHLY
    /// Other                    OTHER
    /// </summary>
    public class AdVendorPayFor : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// The code of the pay form to VENDOR
        /// </summary>
        public virtual string PayForCode { get; protected set; }

        /// <summary>
        /// The description of the pay form.
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// List of Vendor Campaigns associated with a type of pay for.
        /// </summary>
        public virtual IList<AdVendorCampaign> VendorCampaignsList { get; protected set; } 
    }
}