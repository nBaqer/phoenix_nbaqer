﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdTitles.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Domain for Table AdTitles
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Lead
{
    using System;

    using FAME.Advantage.Domain.Campuses.CampusGroups;
    using FAME.Advantage.Domain.SystemStuff;

    using Infrastructure.Entities;

    /// <summary>
    /// Domain for Table AdTitles
    /// </summary>
    public class AdTitles : DomainEntity
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AdTitles"/> class.
        /// Constructor use this to create the object.
        /// </summary>
        /// <param name="titleCode">
        /// The title Code.
        /// </param>
        /// <param name="statusObj">
        /// The status Object.
        /// </param>
        /// <param name="titleDescrip">
        /// The title Description.
        /// </param>
        /// <param name="jobCatObj">
        /// The job Category Object.
        /// </param>
        /// <param name="campGrpObj">
        /// The camp Group Object.
        /// </param>
        /// <param name="modUser">
        /// The modification User.
        /// </param>
        /// <param name="modDate">
        /// The modification Date.
        /// </param>
        public AdTitles(
            string titleCode, 
            SyStatuses statusObj, 
            string titleDescrip, 
            PlJobStatus jobCatObj, 
            CampusGroup campGrpObj, 
            string modUser, 
            DateTime modDate)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.TitleCode = titleCode;
            this.StatusObj = statusObj;
            this.TitleDescrip = titleDescrip;
            this.JobCatObj = jobCatObj;
            this.CampGrpObj = campGrpObj;
            this.ModUser = modUser;
            this.ModDate = modDate;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdTitles"/> class.
        /// Protected Constructor.
        /// </summary>
        protected AdTitles()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets Title Code
        /// </summary>
        public virtual string TitleCode { get; protected set; }

        /// <summary>
        /// Gets or sets Status Object
        /// </summary>
        public virtual SyStatuses StatusObj { get; protected set; }

        /// <summary>
        /// Gets or sets Title Description
        /// </summary>
        public virtual string TitleDescrip { get; protected set; }

        /// <summary>
        /// Gets or sets Job Category Object
        /// </summary>
        public virtual PlJobStatus JobCatObj { get; protected set; }

        /// <summary>
        /// Gets or sets Campus Group Object
        /// </summary>
        public virtual CampusGroup CampGrpObj { get; protected set; }

        /// <summary>
        /// Gets or sets Modification User
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets Modification Date
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        #endregion
    }
}