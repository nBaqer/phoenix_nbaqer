﻿using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;
using FAME.Advantage.Domain.Users;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// Domain for the VIEW Voew_LeadStatusChangesPerUser
    /// </summary>
    public class ViewLeadStatusChangesPermission : DomainEntity
    {
        /// <summary>
        /// Protect the default constructor
        /// </summary>
        protected ViewLeadStatusChangesPermission() { }
 
        /// <summary>
        /// Original status
        /// </summary>
        public virtual UserDefStatusCode OrigStatusObj { get; protected set; }

        /// <summary>
        /// New Status of the lead
        /// </summary>
        public virtual UserDefStatusCode NewStatusObj { get; protected set; }

        /// <summary>
        /// Campus 
        /// </summary>
        public virtual Campuses.Campus CampusObj { get; protected set; }
        
        /// <summary>
        /// User wit the permission
        /// </summary>
        public virtual User UserObj { get; protected set; }

    }
}