
using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Lead
{

    /// <summary>
    /// Domain for Table View_Messages
    /// </summary>
    public class ViewMessages : DomainEntity
    {
        #region Constructors
        /// <summary>
        /// Protected Constructor.
        /// </summary>
        protected ViewMessages() { }
        /// <summary>
        /// Constructor use this to create the object.
        /// </summary>
        public ViewMessages(MsgTemplates templateObj, 
            string deliveryType, 
            DateTime dateDelivered, 
            Users.User fromObj, 
            Lead recipientObj)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            TemplateObj = templateObj;
            DeliveryType = deliveryType;
            DateDelivered = dateDelivered;
            FromObj = fromObj;
            RecipientObj = recipientObj;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }
        #endregion

        #region Properties
 
        ///<summary>
        ///
        ///</summary>
        public virtual MsgTemplates TemplateObj { get; protected set; }

        ///<summary>
        ///Email / Printer
        ///</summary>
        public virtual string DeliveryType { get; protected set; }

        ///<summary>
        ///Date of delivered
        ///</summary>
        public virtual DateTime DateDelivered { get; protected set; }

        ///<summary>
        /// The User who send the message
        ///</summary>
        public virtual Users.User FromObj { get; protected set; }

        ///<summary>
        ///The Lead that received the message
        ///</summary>
        public virtual Lead RecipientObj { get; protected set; }

        #endregion
    }
}

