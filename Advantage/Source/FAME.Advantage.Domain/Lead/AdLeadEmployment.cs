﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdLeadEmployment.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Domain for Table adLeadEmployment
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Lead
{
    using System;
    using System.Collections.Generic;
    using Infrastructure.Entities;

    /// <summary>
    /// Domain for Table adLeadEmployment
    /// </summary>
    public class AdLeadEmployment : DomainEntity
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AdLeadEmployment"/> class. 
        /// Constructor use this to create the object.
        /// </summary>
        /// <param name="leadObj">
        /// The lead Object.
        /// </param>
        /// <param name="jobTitleObj">
        /// The job Title Object.
        /// </param>
        /// <param name="jobStatusObj">
        /// The job Status Object.
        /// </param>
        /// <param name="startDate">
        /// The start Date.
        /// </param>
        /// <param name="endDate">
        /// The end Date.
        /// </param>
        /// <param name="comments">
        /// The comments.
        /// </param>
        /// <param name="modUser">
        /// The modification User.
        /// </param>
        /// <param name="modDate">
        /// The modification Date.
        /// </param>
        /// <param name="jobResponsibilities">
        /// The job Responsibilities.
        /// </param>
        /// <param name="employerName">
        /// The employer Name.
        /// </param>
        /// <param name="employerJobTitle">
        /// The employer Job Title.
        /// </param>
        public AdLeadEmployment(
            Lead leadObj,
            AdTitles jobTitleObj,
            PlJobStatus jobStatusObj,
            DateTime? startDate,
            DateTime? endDate, 
            string comments, 
            string modUser, 
            DateTime modDate, 
            string jobResponsibilities, 
            string employerName, 
            string employerJobTitle)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.PriorWorkAddressesList = new List<PriorWorkAddress>();
            this.LeadObj = leadObj;
            this.JobTitleObj = jobTitleObj;
            this.JobStatusObj = jobStatusObj;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.Comments = comments;
            this.ModUser = modUser;
            this.ModDate = modDate;
            this.JobResponsibilities = jobResponsibilities;
            this.EmployerName = employerName;
            this.EmployerJobTitle = employerJobTitle;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }
       
        /// <summary>
        /// Initializes a new instance of the <see cref="AdLeadEmployment"/> class. 
        /// Protected Constructor.
        /// </summary>
        protected AdLeadEmployment()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets The lead associated with this previous works
        /// </summary>
        public virtual Lead LeadObj { get; protected set; }

        /// <summary>
        /// Gets or sets The job Title
        /// </summary>
        public virtual AdTitles JobTitleObj { get; protected set; }

        /// <summary>
        /// Gets or sets The Status of the job
        /// </summary>
        public virtual PlJobStatus JobStatusObj { get; protected set; }

        /// <summary>
        ///  Gets or sets 
        /// </summary>
        public virtual DateTime? StartDate { get; protected set; }

        /// <summary>
        ///  Gets or sets 
        /// </summary>
        public virtual DateTime? EndDate { get; protected set; }

        /// <summary>
        ///  Gets or sets 
        /// </summary>
        public virtual string Comments { get; protected set; }

        /// <summary>
        ///  Gets or sets 
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        ///  Gets or sets 
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        ///  Gets or sets 
        /// </summary>
        public virtual string JobResponsibilities { get; protected set; }

        /// <summary>
        ///  Gets or sets 
        /// </summary>
        public virtual string EmployerName { get; protected set; }

        /// <summary>
        ///  Gets or sets 
        /// </summary>
        public virtual string EmployerJobTitle { get; protected set; }

        /// <summary>
        /// Gets or sets the prior work addresses list.
        /// </summary>
        public virtual IList<PriorWorkAddress> PriorWorkAddressesList { get; protected set; }

        /// <summary>
        /// Gets or sets the prior work contacts list.
        /// </summary>
        public virtual IList<PriorWorkContact> PriorWorkContactsList { get; protected set; }

        #endregion

        /// <summary>
        ///  The update ad lead employment.
        /// </summary>
        /// <param name="leadObj">
        ///  The lead object.
        /// </param>
        /// <param name="jobTitleObj">
        ///  The job title object.
        /// </param>
        /// <param name="jobStatusObj">
        ///  The job status object.
        /// </param>
        /// <param name="startDate">
        ///  The start date.
        /// </param>
        /// <param name="endDate">
        ///  The end date.
        /// </param>
        /// <param name="comments">
        ///  The comments.
        /// </param>
        /// <param name="modUser">
        ///  The mod user.
        /// </param>
        /// <param name="modDate">
        ///  The mod date.
        /// </param>
        /// <param name="jobResponsibilities">
        ///  The job responsibilities.
        /// </param>
        /// <param name="employerName">
        ///  The employer name.
        /// </param>
        /// <param name="employerJobTitle">
        ///  The employer job title.
        /// </param>
        /// <param name="addresslist"></param>
        public virtual void UpdateAdLeadEmployment(
                Lead leadObj,
                AdTitles jobTitleObj,
                PlJobStatus jobStatusObj,
                DateTime? startDate,
                DateTime? endDate,
                string comments,
                string modUser,
                DateTime modDate,
                string jobResponsibilities,
                string employerName,
                string employerJobTitle,
                IList<PriorWorkAddress> addresslist
            )
        {
            this.LeadObj = leadObj;
            this.JobTitleObj = jobTitleObj;
            this.JobStatusObj = jobStatusObj;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.Comments = comments;
            this.ModUser = modUser;
            this.ModDate = modDate;
            this.JobResponsibilities = jobResponsibilities;
            this.EmployerName = employerName;
            this.EmployerJobTitle = employerJobTitle;
            this.PriorWorkAddressesList = addresslist;
        }
    }
}