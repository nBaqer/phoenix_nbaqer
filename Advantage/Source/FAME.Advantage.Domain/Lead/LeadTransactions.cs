﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadTransactions.cs" company="FAME Inc.">
//   2016
//   FAME.Advantage.Domain.Lead.LeadTransactions
// </copyright>
// <summary>
//   Describe the Transaction of the Leads
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Lead
{
    using System;
    using System.Collections.Generic;

    using Campuses;
    using Infrastructure.Entities;
    using Requirements;
    using Student.Enrollments.Transactions;

    /// <summary>
    /// Describe the Transaction of the Leads
    /// </summary>
    public class LeadTransactions : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadTransactions"/> class. 
        /// </summary>
        /// <param name="lead">
        /// The lead associated with the transaction
        /// </param>
        /// <param name="transCode">
        /// The transaction code
        /// </param>
        /// <param name="transType">
        /// The transaction type
        /// </param>
        /// <param name="reference">
        /// The transaction reference
        /// </param>
        /// <param name="description">
        /// The transaction description
        /// </param>
        /// <param name="transAmount">
        /// The transaction amount
        /// </param>
        /// <param name="transDate">
        /// The transaction date
        /// </param>
        /// <param name="modUser">
        /// The transaction modified user
        /// </param>
        /// <param name="campus">
        /// The transaction campus
        /// </param>
        public LeadTransactions(
            Domain.Lead.Lead lead,
            TransactionCode transCode,
            TransactionType transType,
            string reference,
            string description,
            decimal transAmount,
            DateTime transDate,
            string modUser,
            Campus campus)
        {
            this.ID = new Guid();
            this.LeadObj = lead;
            this.TransactionCode = transCode;
            this.TransactionType = transType;
            this.TransReference = reference;
            this.TransDescription = description;
            this.TransAmount = transAmount;
            this.TransDate = transDate;
            this.CreatedDate = DateTime.Now;
            this.Voided = false;
            this.ModUser = modUser;
            this.ModDate = DateTime.Now;
            this.Campus = campus;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadTransactions"/> class. 
        /// Protected Constructor
        /// </summary>
        protected LeadTransactions()
        {
        }

        // ReSharper disable CSharpWarnings::CS1591

        /// <summary>
        /// Gets or sets the lead object.
        /// </summary>
        public virtual Lead LeadObj { get; protected set; }

        /// <summary>
        /// Gets or sets the transaction code.
        /// </summary>
        public virtual TransactionCode TransactionCode { get; protected set; }

        /// <summary>
        /// Gets or sets the transaction type.
        /// </summary>
        public virtual TransactionType TransactionType { get; protected set; }

        /// <summary>
        /// Gets or sets the transaction reference.
        /// </summary>
        public virtual string TransReference { get; protected set; }

        /// <summary>
        /// Gets or sets the transaction description.
        /// </summary>
        public virtual string TransDescription { get; protected set; }

        /// <summary>
        /// Gets or sets the transaction amount.
        /// </summary>
        public virtual decimal TransAmount { get; protected set; }

        /// <summary>
        /// Gets or sets the transaction date.
        /// </summary>
        public virtual DateTime TransDate { get; protected set; }

        /// <summary>
        /// Gets or sets the is enrolled. Value can be null.
        /// </summary>
        public virtual bool? IsEnrolled { get; protected set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        public virtual DateTime CreatedDate { get; protected set; }

        /// <summary>
        /// Gets or sets the voided. Value can be null.
        /// </summary>
        public virtual bool? Voided { get; protected set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public virtual DateTime? ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the campus.
        /// </summary>
        public virtual Campus Campus { get; protected set; }

        /// <summary>
        /// Gets or sets the reversal reason.
        /// </summary>
        public virtual string ReversalReason { get; protected set; }

        /// <summary>
        /// Gets or sets the display sequence. Value can be null.
        /// </summary>
        public virtual int? DisplaySequence { get; protected set; }

        /// <summary>
        /// Gets or sets the second display sequence. Value can be null.
        /// </summary>
        public virtual int? SecondDisplaySequence { get; protected set; }

        /// <summary>
        /// Gets or sets the map transaction id. Value can be null.
        /// </summary>
        public virtual Guid? MapTransactionId { get; protected set; }

        /// <summary>
        /// Gets or sets the Requirement associated with this Lead Transaction.
        /// </summary>
        public virtual Requirement Requirement { get; protected set; }

        /// <summary>
        /// Gets or sets the payments.
        /// </summary>
        public virtual LeadPayments Payment { get; protected set; }

        /// <summary>
        /// The set transaction as void.
        /// </summary>
        /// <param name="modifiedUser">
        /// The modified User.
        /// </param>
        public virtual void SetTransactionAsVoid(string modifiedUser)
        {
            this.Voided = true;
            this.ModDate = DateTime.Now;
            this.ModUser = modifiedUser;
        }
    }
}