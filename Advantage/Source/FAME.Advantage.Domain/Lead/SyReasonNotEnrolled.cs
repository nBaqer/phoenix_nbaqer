﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyReasonNotEnrolled.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Domain for Table syReasonNotEnrolled
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Lead
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.Domain.Campuses.CampusGroups;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.SystemStuff;

    /// <summary>
    /// Domain for Table <code>syReasonNotEnrolled</code>
    /// </summary>
    public class SyReasonNotEnrolled : DomainEntity
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SyReasonNotEnrolled"/> class.
        /// Constructor use this to create the object.
        /// </summary>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="statusObj">
        /// The status Object.
        /// </param>
        /// <param name="campGrpObj">
        /// The campus Group Object.
        /// </param>
        /// <param name="modUser">
        /// The mod User.
        /// </param>
        /// <param name="modDate">
        /// The mod Date.
        /// </param>
        public SyReasonNotEnrolled(
                   string code, 
               string description, 
               SyStatuses statusObj, 
               CampusGroup campGrpObj, 
               string modUser, 
               DateTime modDate)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.Code = code;
            this.Description = description;
            this.StatusObj = statusObj;
            this.CampGrpObj = campGrpObj;
            this.ModUser = modUser;
            this.ModDate = modDate;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SyReasonNotEnrolled"/> class.
        /// Protected Constructor.
        /// </summary>
        protected SyReasonNotEnrolled()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string Description { get; protected set; }
        
        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }
       
        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual SyStatuses StatusObj { get; protected set; }

        /// <summary>
        /// Gets or sets Campus Group
        /// </summary>
        public virtual CampusGroup CampGrpObj { get; protected set; }

        /// <summary>
        /// Gets or sets the lead object list associated with this reason.
        /// </summary>
        public virtual IList<Lead> LeadList { get; protected set; }

        #endregion
    }
}