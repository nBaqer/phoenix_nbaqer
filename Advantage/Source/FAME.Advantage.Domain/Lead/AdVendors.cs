﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.SystemStuff.Maintenance;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// Describe the vendors specifications
    /// </summary>
    public class AdVendors : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Vendor Name. More explicative name for Vendor.
        /// </summary>
        public virtual string VendorName { get; protected set; }
        
        /// <summary>
        /// Vendor Code. This should be unique for each vendor
        /// This code must be only a word.
        /// </summary>
        public virtual string VendorCode { get; protected set; }

        /// <summary>
        /// True: The Vendor lead service is active on the system
        /// False: The vendor lead service is inactive. 
        /// They can not send lead info to Advantage
        /// </summary>
        public virtual bool IsActive { get; protected set; }

        /// <summary>
        /// 1 the record appears as deleted and not be considered when you query the table.
        /// </summary>
        public virtual bool IsDeleted { get; protected set; }

        /// <summary>
        /// Description of the Vendor
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// Date and time that begin the relation with the Vendor
        /// </summary>
        public virtual DateTime DateOperationBegin { get; protected set; }

        /// <summary>
        /// Propose time to finish the Vendor Relations
        /// </summary>
        public virtual DateTime? DateOperationEnd { get; protected set; }

        /// <summary>
        /// List of campaigns associated with this vendor.
        /// </summary>
        //public virtual IList<AdVendorCampaign> VendorCampaignsList { get; protected set; }

        public virtual void UpdateRecord(WapiVendorsLeadOutputModel data)
        {
            VendorName = data.VendorName;
            DateOperationBegin = data.DateOperationBegin;
            DateOperationEnd = data.DateOperationEnd;
            Description = data.Description;
            IsActive = data.IsActive;

        }
    }
}