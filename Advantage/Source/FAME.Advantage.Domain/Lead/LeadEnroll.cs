﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadEnroll.cs" company="FAME INC">
//   2016
// </copyright>
// <summary>
//   Defines the LeadEnroll type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Lead
{
    using System;

    using FAME.Advantage.Domain.Catalogs;
    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The lead enroll.
    /// </summary>
    public class LeadEnroll : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadEnroll"/> class.
        /// </summary>
        /// <param name="enteranceInterviewDt">
        /// The enterance interview date.
        /// </param>
        /// <param name="isFirstTimeSchool">
        /// The is first time in school.
        /// </param>
        /// <param name="isFirstTimePostSecSchool">
        /// The is first time in post secondary school.
        /// </param>
        /// <param name="shiftId">
        /// The shift id.
        /// </param>
        /// <param name="internationalLead">
        /// The international lead.
        /// </param>
        /// <param name="nationality">
        /// The nationality.
        /// </param>
        /// <param name="geographicType">
        /// The geographic type.
        /// </param>
        /// <param name="DegCertSeeking">
        /// The degree certificate seeking.
        /// </param>
        /// <param name="billingMethod">
        /// The billing method.
        /// </param>
        /// <param name="leadObj">
        /// The lead object.
        /// </param>
        public LeadEnroll(DateTime enteranceInterviewDt,bool isFirstTimeSchool, bool isFirstTimePostSecSchool,string shiftId,bool internationalLead,string nationality,string geographicType,string DegCertSeeking,string billingMethod, Lead leadObj)
        {
            this.EntranceInterviewDate = enteranceInterviewDt;
            this.IsFirstTimeInSchool = isFirstTimeSchool;
            this.IsFirstTimePostSecSchool = isFirstTimePostSecSchool;
            this.ShiftId = shiftId;
            this.InternationalLead = internationalLead;
            this.Nationality = nationality;
            this.GeographicTypeId = geographicType;
            this.DegCertSeekingId = DegCertSeeking;
            this.BillingMethodId = billingMethod;
            this.LeadObj = leadObj;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadEnroll"/> class.
        /// </summary>
        protected LeadEnroll()
        {
        }

        /// <summary>
        /// Gets or sets entrance interview date
        /// </summary>
        public virtual DateTime? EntranceInterviewDate { get; protected set; }

        /// <summary>
        /// Gets or sets the is first time in school.
        /// </summary>
        public virtual bool? IsFirstTimeInSchool { get; protected set; }

        /// <summary>
        /// Gets or sets the is first time post sec school.
        /// </summary>
        public virtual bool? IsFirstTimePostSecSchool { get; protected set; }

        /// <summary>
        /// Gets or sets the shift id.
        /// </summary>
        public virtual string ShiftId { get; protected set; }

        /// <summary>
        /// Gets or sets the international lead.
        /// </summary>
        public virtual bool? InternationalLead { get; protected set; }

        /// <summary>
        /// Gets or sets the nationality.
        /// </summary>
        public virtual string Nationality { get; protected set; }

        /// <summary>
        /// Gets or sets the geographic type id.
        /// </summary>
        public virtual string GeographicTypeId { get; protected set; }

        /// <summary>
        /// Gets or sets the degree certificate seeking id.
        /// </summary>
        public virtual string DegCertSeekingId { get; protected set; }

        /// <summary>
        /// Gets or sets the billing method id.
        /// </summary>
        public virtual string BillingMethodId { get; protected set; }

        /// <summary>
        /// Gets or sets the lead info.
        /// </summary>
        public virtual Lead LeadObj { get; protected set; }

        /// <summary>
        /// Gets or sets the program version type.
        /// </summary>
        public virtual ProgramVersionType ProgramVersionType { get; protected set; }
    }
}
