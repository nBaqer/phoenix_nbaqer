using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Lead
{

    /// <summary>
    /// Domain for Table msgTemplates
    /// </summary>
    public class MsgTemplates : DomainEntity
    {
        #region Constructors
        /// <summary>
        /// Protected Constructor.
        /// </summary>
        protected MsgTemplates() { }
        /// <summary>
        /// Constructor use this to create the object.
        /// </summary>
        //public MsgTemplates(string groupId, int moduleEntityId, string campGroupId, string code, string descrip, string data, DateTime modDate, string modUser, int active)
        public MsgTemplates(string code, string descrip, DateTime modDate, string modUser, int active)
 
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            //GroupId = groupId;
            //ModuleEntityId = moduleEntityId;
            //CampGroupId = campGroupId;
            Code = code;
            Descrip = descrip;
            //Data = data;
            ModDate = modDate;
            ModUser = modUser;
            Active = active;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }
        #endregion

        #region Properties

        ///<summary>
        /// Not used for now
        ///</summary>
        //public virtual string GroupId { get; protected set; }

        ///<summary>
        /// Not Used for now
        ///</summary>
        //public virtual int ModuleEntityId { get; protected set; }

        ///<summary>
        ///Not used for now
        ///</summary>
        //public virtual string CampGroupId { get; protected set; }

        ///<summary>
        /// 
        ///</summary>
        public virtual string Code { get; protected set; }

        ///<summary>
        ///A description of message
        ///</summary>
        public virtual string Descrip { get; protected set; }

        ///<summary>
        ///Not used for now The text of the message
        ///</summary>
        //public virtual string Data { get; protected set; }

        ///<summary>
        ///Modification Date
        ///</summary>
        public virtual DateTime ModDate { get; protected set; }

        ///<summary>
        ///Modification User name
        ///</summary>
        public virtual string ModUser { get; protected set; }

        ///<summary>
        ///If the template is active or not
        ///</summary>
        public virtual int Active { get; protected set; }

        /// <summary>
        /// The list of messages from this Template
        /// </summary>
        public virtual IList<ViewMessages> MessagesList { get; protected set; }

        #endregion
    }
}


