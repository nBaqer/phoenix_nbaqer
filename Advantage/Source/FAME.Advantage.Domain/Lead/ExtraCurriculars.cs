﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExtraCurriculars.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Domain for Table adExtraCurr
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Lead
{
    using System;

    using FAME.Advantage.Domain.Campuses.CampusGroups;
    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// Domain for Table <code>adExtraCurr</code>
    /// </summary>
    /// <remarks>
    /// The relation of this table with Leads is through adLeadExtraCurricular bridge table
    /// </remarks>
    public class ExtraCurriculars : DomainEntityWithTypedID<int>
    {
        #region constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtraCurriculars"/> class. 
        /// constructor
        /// </summary>
        /// <param name="extraCurricularDesCrip">
        /// The extra Curricular Description.
        /// </param>
        /// <param name="comments">
        /// The comments.
        /// </param>
        /// <param name="leadObj">
        /// The lead Object.
        /// </param>
        /// <param name="group">
        /// The group.
        /// </param>
        /// <param name="level">
        /// The level.
        /// </param>
        /// <param name="modUser">
        /// The modification User.
        /// </param>
        /// <param name="modDate">
        /// The modification Date.
        /// </param>
        public ExtraCurriculars(string extraCurricularDesCrip, string comments, Lead leadObj, ExtraCurricularGroup group, AdLevels level, string modUser, DateTime modDate)
        {
            // ReSharper disable VirtualMemberCallInContructor
            this.ExtraCurrDescription = extraCurricularDesCrip;
            this.ExtraCurrComments = comments;
            this.LeadObj = leadObj;
            this.ExtraCurricularGroupObj = group;
            this.LevelObj = level;
            this.ModUser = modUser;
            this.ModDate = modDate;

            // ReSharper restore VirtualMemberCallInContructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtraCurriculars"/> class. 
        /// Protected constructor. Not Used
        /// </summary>
        protected ExtraCurriculars()
        {
        }

        #endregion

        /// <summary>
        /// Gets or sets Description of the extra curricular
        /// </summary>
        public virtual string ExtraCurrDescription { get; protected set; }

        /// <summary>
        /// Gets or sets A comment to the extra curricular.
        /// </summary>
        public virtual string ExtraCurrComments { get; protected set; }

        /// <summary>
        /// Gets or sets Lead associated with this extracurricular entry
        /// </summary>
        public virtual Lead LeadObj { get; protected set; }

        /// <summary>
        /// Gets or sets Group that owned this extracurricular.
        /// </summary>
        public virtual ExtraCurricularGroup ExtraCurricularGroupObj { get; protected set; }

        /// <summary>
        ///  Gets or sets The extra curricular level usually school level (college high school or personal curricular)
        /// </summary>
        public virtual AdLevels LevelObj { get; protected set; }

        /// <summary>
        ///  Gets or sets The campus group that owner the group
        /// </summary>
        public virtual CampusGroup CampusGroupObj { get; protected set; }

        /// <summary>
        ///  Gets or sets Audit Modification Date
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        ///  Gets or sets Audit Modification User
        /// </summary>
        public virtual string ModUser { get; protected set; }
    }
}