﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// Domain for LeadStatuses Changes
    /// </summary>
    public class LeadStatusesChanges : DomainEntity
    {
        /// <summary>
        /// Protect the default constructor
        /// </summary>
        protected LeadStatusesChanges() { }

        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="origStatusObj"></param>
        /// <param name="newStatusObj"></param>
        /// <param name="dateOfChange"></param>
        /// <param name="modDate"></param>
        /// <param name="modUser"></param>
        /// <param name="leadObj"></param>
        public LeadStatusesChanges(
            UserDefStatusCode origStatusObj,
            UserDefStatusCode newStatusObj,
            DateTime dateOfChange,
            DateTime modDate,
            string modUser,
            Lead leadObj)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            OrigStatusObj = origStatusObj;
            NewStatusObj = newStatusObj;
            DateOfChange = dateOfChange;
            ModDate = modDate;
            ModUser = modUser;
            LeadObj = leadObj;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Original status
        /// </summary>
        public virtual UserDefStatusCode OrigStatusObj { get; protected set; }

        /// <summary>
        /// New Status of the lead
        /// </summary>
        public virtual UserDefStatusCode NewStatusObj { get; protected set; }

        /// <summary>
        /// Date of change status
        /// </summary>
        public virtual DateTime DateOfChange { get; protected set; }

        /// <summary>
        /// Day that record was input
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// User that input the record
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Inverse relation. list of Leads in the program group
        /// </summary>
        public virtual Lead LeadObj { get; protected set; }
    }
}