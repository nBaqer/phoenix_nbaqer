﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Routing;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// LeadOtherContact is the Domain Object for the adLeadOtherContacts table
    /// </summary>
    public class LeadOtherContact : DomainEntity
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public LeadOtherContact()
        {

        }
        /// <summary>
        /// Create New Lead Other Contact
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="LeadId"></param>
        /// <param name="StatusId"></param>
        /// <param name="RelationshipId"></param>
        /// <param name="ContactTypeId"></param>
        /// <param name="PrefixId"></param>
        /// <param name="SufixId"></param>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <param name="MiddleName"></param>
        /// <param name="Comments"></param>
        /// <param name="ModUser"></param>
        public LeadOtherContact( Lead Lead, SyStatuses Status, Relationship Relationship, ContactType ContactType,
            string FirstName, string LastName, string ModUser)
        {
            // ReSharper disable VirtualMemberCallInConstructor
            this.Lead = Lead;

           /* this.LeadId = Lead.ID;
            this.StatusId = Status.ID;
            this.RelationshipId = Relationship.ID;
            this.ContactTypeId = ContactType.ID;*/
            this.Status = Status;
            this.Relationship = Relationship;
            this.ContactType = ContactType;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.ModUser = ModUser;
            this.ModDate = DateTime.Now;
              // ReSharper restore VirtualMemberCallInConstructor   
        }


        /// <summary>
        /// ID
        /// </summary>
        public virtual Guid OtherContactId
        {
            get { return ID; }
            protected set { ID = value; }
        }


        /// <summary>
        /// Lead
        /// </summary>
        public virtual Lead Lead { get; protected set; }

        /// <summary>
        /// Status
        /// </summary>
        public virtual SyStatuses Status { get; protected set; }

        /// <summary>
        /// Relationship 
        /// </summary>
        public virtual Relationship Relationship { get; protected set; }

  
        /// <summary>
        /// Contact Type
        /// </summary>
        public virtual ContactType ContactType { get; protected set; }


        /// <summary>
        /// Prefix (nullable object)
        /// </summary>
        public virtual Prefix Prefix { get; protected set; }

    
        /// <summary>
        /// Sufix (nullable object)
        /// </summary>
        public virtual Suffix Sufix { get; protected set; }

        /// <summary>
        /// First Name
        /// </summary>
        public virtual string FirstName { get; protected set; }

        /// <summary>
        /// Last Name
        /// </summary>
        public virtual string LastName { get; protected set; }

        /// <summary>
        /// Middle Name (nullable object)
        /// </summary>
        public virtual string MiddleName { get; protected set; }

        /// <summary>
        /// Comments (nullable object)
        /// </summary>
        public virtual string Comments { get; protected set; }

        /// <summary>
        /// User last modified this object
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Last Modified Date
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Lead Other Contact Phones 
        /// </summary>
        public virtual IList<LeadOtherContactPhone> Phones { get; protected set; }

        /// <summary>
        /// Lead Other Contact Emails 
        /// </summary>
        public virtual IList<LeadOtherContactEmail> Emails { get; protected set; }

        /// <summary>
        /// Lead Other Contact Addresses
        /// </summary>
        public virtual IList<LeadOtherContactAddress> Addresses { get; protected set; }

        public virtual void Update(SyStatuses Status, Relationship Relationship, ContactType ContactType,
            string FirstName, string LastName, string ModUser)
        {
            this.Status = Status;
            this.Relationship = Relationship;
            this.ContactType = ContactType;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.ModUser = ModUser;
            this.ModDate = DateTime.Now;
        }

        /// <summary>
        /// Update Prefix Value
        /// </summary>
        /// <param name="PrefixId">Accept a GUID as string or null</param>
        public virtual void UpdatePrefix(Prefix Prefix)
        {
            this.Prefix = Prefix;
        }
        /// <summary>
        /// Update Sufix Value
        /// </summary>
        /// <param name="SufixId">Accept a GUID as string or null</param>
        public virtual void UpdateSufix(Suffix Sufix)
        {
            this.Sufix = Sufix;
        }
        /// <summary>
        /// Update Middle Value
        /// </summary>
        /// <param name="MiddleName">Accept a string or null</param>
        public virtual void UpdateMiddleName(string MiddleName)
        {
            this.MiddleName = MiddleName;
        }
        /// <summary>
        /// Update Comments Value
        /// </summary>
        /// <param name="Comments">Accept a string or null</param>
        public virtual void UpdateComments(string Comments)
        {
            this.Comments = Comments;
        }

        /// <summary>
        /// Set Prefix Value
        /// </summary>
        /// <param name="PrefixId">Accept a GUID as string, if empty or null nothing is set</param>
        public virtual void setPrefix(string PrefixId)
        {
            if (!String.IsNullOrEmpty(PrefixId))
            {
               // this.PrefixId = Guid.Parse(PrefixId);
            }
        }
        /// <summary>
        /// Set Sufix Value
        /// </summary>
        /// <param name="SufixId">Accept a GUID as string, if empty or null nothing is set</param>
        public virtual void setSufix(string SufixId)
        {
            if (!String.IsNullOrEmpty(SufixId))
            {
               // this.SufixId = Guid.Parse(SufixId);
            }
        }
        /// <summary>
        /// Set Middle Value
        /// </summary>
        /// <param name="MiddleName">Accept a string, if empty or null nothing is set</param>
        public virtual void setMiddleName(string MiddleName)
        {
            if (!String.IsNullOrEmpty(MiddleName))
            {
                this.MiddleName = MiddleName;
            }
        }
        /// <summary>
        /// Set Comments Value
        /// </summary>
        /// <param name="Comments">Accept a string, if empty or null nothing is set</param>
        public virtual void setComments(string Comments)
        {
            if (!String.IsNullOrEmpty(Comments))
            {
                this.Comments = Comments;
            }
        }

        public virtual void AddPhone(LeadOtherContactPhone model)
        {
            if (Phones == null)
                Phones = new List<LeadOtherContactPhone>();

            Phones.Add(model);
        }
        public virtual void AddEmail(LeadOtherContactEmail model)
        {
            if (Emails == null)
                Emails = new List<LeadOtherContactEmail>();

            Emails.Add(model);
        }
        public virtual void AddAddress(LeadOtherContactAddress model)
        {
            if (Addresses == null)
                Addresses = new List<LeadOtherContactAddress>();

            Addresses.Add(model);
        }
    }

}
