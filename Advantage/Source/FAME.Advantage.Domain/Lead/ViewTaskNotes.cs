﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ViewTaskNotes.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Domain for view View_TaskNotes
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Lead
{
    using System;
    using Infrastructure.Entities;
    using Users;

    /// <summary>
    /// Domain for view View_TaskNotes
    /// </summary>
    public class ViewTaskNotes : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewTaskNotes"/> class. 
        /// Protect the default constructor
        /// </summary>
        protected ViewTaskNotes()
        {
        }
 
        /// <summary>
        /// Gets or sets Lead Id 
        /// </summary>
        public virtual Lead LeadObj { get; protected set; }

        /// <summary>
        /// Gets or sets The task message
        /// </summary>
        public virtual string Message { get; protected set; }

        /// <summary>
        /// Gets or sets The task description, normally Appointment, call Lead 
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        ///  Gets or sets The owner of the task
        /// </summary>
        public virtual User UserObj { get; protected set; }

        /// <summary>
        /// Gets or sets Modification day of task
        /// </summary>
        public virtual DateTime? ModDate { get; protected set; }
    }
}