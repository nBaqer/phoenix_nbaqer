﻿using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Users;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// This Domain if for the view VIEW_AdmissionRepByCampus
    /// </summary>
    public class AdmissionRepByCampusView : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Full Admission Rep name
        /// </summary>
        public virtual string FullName { get; protected set; }

        /// <summary>
        /// User that is Admission Represent 
        /// </summary>
        public virtual User UserObj { get; protected set; }

        /// <summary>
        /// Campus where the user can be admission represent
        /// </summary>
        public virtual Campuses.Campus CampusObj { get; protected set; }
    }
}
