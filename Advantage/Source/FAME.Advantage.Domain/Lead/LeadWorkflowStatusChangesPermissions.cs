﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Users.UserRoles;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// Domain for Table syLeadStatusChangePermissions
    /// </summary>
    public class LeadWorkflowStatusChangesPermissions: DomainEntity
    {
        /// <summary>
        /// Protect default constructor
        /// </summary>
        protected LeadWorkflowStatusChangesPermissions()
        {
        }

        /// <summary>
        /// Lead Work flow status change joined to this permission
        /// </summary>
        public virtual LeadWorkflowStatusChanges LeadWorkflowStatusChangesObj { get; protected set; }

        /// <summary>
        /// Role that have the permission
        /// </summary>
        public virtual Roles RoleObj { get; protected set; }

        /// <summary>
        /// If this permission is active or not
        /// </summary>
        public virtual SystemStuff.SyStatuses StatusObj { get; protected set; }

        /// <summary>
        /// Day that record was input
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// User that input the record
        /// </summary>
        public virtual string ModUser { get; protected set; }

        
    }
}