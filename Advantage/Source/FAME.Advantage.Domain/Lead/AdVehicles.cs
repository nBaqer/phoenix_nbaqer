﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.Lead;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// Table AdVehicles.................
    /// </summary>
    public class AdVehicles : DomainEntityWithTypedID<int>
    {
        #region constructors

        /// <summary>
        /// Protected constructor
        /// </summary>
        protected AdVehicles(){}

        /// <summary>
        /// constructor
        /// </summary>
        public AdVehicles(int id, int position , Lead leadObj, string permit, string make , string model, string color, string plate, string modUser)
        {
            // ReSharper disable VirtualMemberCallInContructor
            ID = id;
            Position = position;
            LeadObj = leadObj;
            Permit = permit;
            Make = make;
            Model = model;
            Color = color;
            Plate = plate;
            ModUser = modUser;
            ModDate = DateTime.Now;
            // ReSharper restore VirtualMemberCallInContructor
        }
        #endregion
        
        /// <summary>
        /// 1,2 are show in Vehicle1 Vehicle 2 in the interface
        /// Other values are not show
        /// LeadId and Position together are uniques.
        /// </summary>
        public virtual int Position { get; protected set; }

        /// <summary>
        /// Parking Permit
        /// </summary>
        public virtual string Permit { get; protected set; }
        
        /// <summary>
        /// Vehicle Maker
        /// </summary>
        public virtual string Make { get; protected set; }

        /// <summary>
        /// Vehicle Model
        /// </summary>
        public virtual string Model { get; protected set; }

        /// <summary>
        /// Vehicle Color
        /// </summary>
        public virtual string Color { get; protected set; }

        /// <summary>
        /// Vehicle Plate
        /// </summary>
        public virtual string Plate { get; protected set; }
      
        /// <summary>
        /// Modification date
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Modification user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Lead associated with the vehicle.
        /// </summary>
        public virtual Lead LeadObj { get; protected set; }

        /// <summary>
        /// Update the vehicle
        /// </summary>
        /// <param name="vehicle"></param>
        public virtual void Update(VehicleOutputModel vehicle)
        {
            Color = vehicle.Color;
            Make = vehicle.Make;
            ModDate = DateTime.Now;
            ModUser = vehicle.ModUser;
            Model = vehicle.Model;
            Permit = vehicle.Permit;
            Plate = vehicle.Plate;
            Position = vehicle.Position;
        }
    }
}
