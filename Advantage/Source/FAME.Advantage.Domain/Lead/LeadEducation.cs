﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadEducation.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Lead.LeadEducation
// </copyright>
// <summary>
//   The lead education.
//   The Domain Entity for table <code>adLeadEducation</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Lead
{
    using System;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Domain.SystemStuff.Intitution;

    /// <summary>
    /// The lead education.
    /// The Domain Entity for table <code>adLeadEducation</code>
    /// </summary>
    public class LeadEducation : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadEducation"/> class.
        /// </summary>
        public LeadEducation()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadEducation"/> class.
        /// </summary>
        /// <param name="educationInstitutionType">
        /// The education institution type.
        /// </param>
        /// <param name="lead">
        /// The lead.
        /// </param>
        /// <param name="institution">
        /// The institution.
        /// </param>
        /// <param name="graduatedDate">
        /// The graduated date.
        /// </param>
        /// <param name="finalGrade">
        /// The final grade.
        /// </param>
        /// <param name="comments">
        /// The comments.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="modifiedDate">
        /// The modified date.
        /// </param>
        /// <param name="major">
        /// The major.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        /// <param name="graduate">
        /// The graduate.
        /// </param>
        /// <param name="gpa">
        /// The GPA.
        /// </param>
        /// <param name="rank">
        /// The rank.
        /// </param>
        /// <param name="percentile">
        /// The percentile.
        /// </param>
        /// <param name="certificate">
        /// The certificate.
        /// </param>
        public LeadEducation(
            string educationInstitutionType,
            Lead lead,
            HighSchools institution,
            DateTime? graduatedDate,
            string finalGrade,
            string comments,
            string modifiedUser,
            DateTime? modifiedDate,
            string major,
            SyStatuses status,
            bool graduate,
            decimal? gpa,
            int? rank,
            int? percentile,
            string certificate)
        {
            this.EducationInstitutionType = educationInstitutionType;
            this.Lead = lead;
            this.Institution = institution;
            this.GraduatedDate = graduatedDate;
            this.FinalGrade = finalGrade;
            this.Comments = comments;
            this.ModifiedUser = modifiedUser;
            this.ModifiedDate = modifiedDate;
            this.Major = major;
            this.Status = status;
            this.Graduate = graduate;
            this.Gpa = gpa;
            this.Rank = rank;
            this.Percentile = percentile;
            this.Certificate = certificate;
        }

        /// <summary>
        /// Gets or sets the education institution type.
        /// </summary>
        public virtual string EducationInstitutionType { get; protected set; }

        /// <summary>
        /// Gets or sets the lead.
        /// </summary>
        public virtual Lead Lead { get; protected set; }

        /// <summary>
        /// Gets or sets the institution.
        /// </summary>
        public virtual HighSchools Institution { get; protected set; }

        /// <summary>
        /// Gets or sets the graduated date.
        /// </summary>
        public virtual DateTime? GraduatedDate { get; protected set; }

        /// <summary>
        /// Gets or sets the final grade.
        /// </summary>
        public virtual string FinalGrade { get; protected set; }

        /// <summary>
        /// Gets or sets the certificate id.
        /// </summary>
        public virtual Guid? CertificateId { get; protected set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        public virtual string Comments { get; protected set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public virtual string ModifiedUser { get; protected set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public virtual DateTime? ModifiedDate { get; protected set; }

        /// <summary>
        /// Gets or sets the major.
        /// </summary>
        public virtual string Major { get; protected set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public virtual SyStatuses Status { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether graduate.
        /// </summary>
        public virtual bool Graduate { get; protected set; }

        /// <summary>
        /// Gets or sets the GPA.
        /// </summary>
        public virtual decimal? Gpa { get; protected set; }

        /// <summary>
        /// Gets or sets the rank.
        /// </summary>
        public virtual int? Rank { get; protected set; }

        /// <summary>
        /// Gets or sets the percentile.
        /// </summary>
        public virtual int? Percentile { get; protected set; }

        /// <summary>
        /// Gets or sets the certificate.
        /// </summary>
        public virtual string Certificate { get; protected set; }
       

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="educationInstitutionType">
        /// The education institution type.
        /// </param>
        /// <param name="lead">
        /// The lead.
        /// </param>
        /// <param name="institution">
        /// The institution.
        /// </param>
        /// <param name="graduatedDate">
        /// The graduated date.
        /// </param>
        /// <param name="finalGrade">
        /// The final grade.
        /// </param>
        /// <param name="comments">
        /// The comments.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="modifiedDate">
        /// The modified date.
        /// </param>
        /// <param name="major">
        /// The major.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        /// <param name="graduate">
        /// The graduate.
        /// </param>
        /// <param name="gpa">
        /// The GPA.
        /// </param>    
        /// <param name="rank">
        /// The rank.
        /// </param>
        /// <param name="percentile">
        /// The percentile.
        /// </param>
        /// <param name="certificate">
        /// The certificate.
        /// </param>
        public virtual void Update(
            string educationInstitutionType,
            Lead lead,
            HighSchools institution,
            DateTime? graduatedDate,
            string finalGrade,
            string comments,
            string modifiedUser,
            DateTime? modifiedDate,
            string major,
            SyStatuses status,
            bool graduate,
            decimal? gpa,
            int? rank,
            int? percentile,
            string certificate)
        {
            this.EducationInstitutionType = educationInstitutionType;
            this.Lead = lead;
            this.Institution = institution;
            this.GraduatedDate = graduatedDate;
            this.FinalGrade = finalGrade;
            this.Comments = comments;
            this.ModifiedUser = modifiedUser;
            this.ModifiedDate = modifiedDate;
            this.Major = major;
            this.Status = status;
            this.Graduate = graduate;
            this.Gpa = gpa;
            this.Rank = rank;
            this.Percentile = percentile;
            this.Certificate = certificate;
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        public virtual void Delete(string modifiedUser, SyStatuses status)
        {
            this.Status = status;
            this.ModifiedUser = modifiedUser;
        }
    }
}