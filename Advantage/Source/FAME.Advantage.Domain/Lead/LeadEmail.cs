﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadEmail.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Store the email of the system
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Lead
{
    using System;
    using Catalogs;

    using FAME.Advantage.Domain.SystemStuff;

    using Infrastructure.Entities;
    using Messages.Lead;

    /// <summary>
    /// Store the email of the system
    /// </summary>
    public class LeadEmail : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadEmail"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="isPreferred">
        /// The is preferred.
        /// </param>
        /// <param name="isShowOnLeadPage">
        /// The is Show In Lead Page.
        /// </param>
        /// <param name="isPortalUserName">
        /// The is portal user name.
        /// </param>
        /// <param name="modUser">
        /// The modification user.
        /// </param>
        /// <param name="modDate">
        /// The modification date.
        /// </param>
        /// <param name="leadObj">
        /// The lead object.
        /// </param>
        /// <param name="emailtypeObj">
        /// The email type object.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        public LeadEmail(Guid id, string email, bool isPreferred, bool isShowOnLeadPage, bool isPortalUserName, string modUser, DateTime modDate, Lead leadObj, EmailType emailtypeObj, SyStatuses status)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.ID = id;
            this.Email = email;
            this.IsPreferred = isPreferred;
            this.IsShowOnLeadPage = isShowOnLeadPage;
            this.IsPortalUserName = isPortalUserName;
            this.ModUser = modUser;
            this.ModDate = modDate;
            this.LeadObj = leadObj;
            this.EmailTypeObj = emailtypeObj;
            this.Status = status;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadEmail"/> class.
        /// </summary>
        /// <param name="id">
        ///  The id.
        /// </param>
        /// <param name="email">
        ///  The email.
        /// </param>
        /// <param name="isPreferred">
        ///  The is preferred.
        /// </param>
        /// <param name="showOnLeadPage">
        ///  The show on lead page.
        /// </param>
        /// <param name="modUser">
        ///  The mod user.
        /// </param>
        /// <param name="modDate">
        ///  The mod date.
        /// </param>
        /// <param name="leadObj">
        ///  The lead object.
        /// </param>
        /// <param name="emailtypeObj">
        ///  The email type object.
        /// </param>
        /// <param name="status">
        ///  The status.
        /// </param>
        public LeadEmail(Guid id, string email, bool isPreferred, bool showOnLeadPage, string modUser, DateTime modDate, Lead leadObj, EmailType emailtypeObj, SyStatuses status)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.ID = id;
            this.Email = email;
            this.IsPreferred = isPreferred;
            this.IsShowOnLeadPage = showOnLeadPage;
            this.ModUser = modUser;
            this.ModDate = modDate;
            this.LeadObj = leadObj;
            this.EmailTypeObj = emailtypeObj;
            this.Status = status;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadEmail"/> class.
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="lead">
        /// The lead object.
        /// </param>
        public LeadEmail(LeadEmail email, Lead lead)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.ID = new Guid();
            this.Email = email.Email;
            this.IsPreferred = email.IsPreferred;
            this.IsShowOnLeadPage = email.IsShowOnLeadPage;
            this.ModUser = email.ModUser;
            this.ModDate = email.ModDate;
            this.LeadObj = lead;
            this.EmailTypeObj = email.EmailTypeObj;
            this.Status = email.Status;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadEmail"/> class. 
        /// Protected Constructor.
        /// </summary>
        protected LeadEmail()
        {
        }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public virtual string Email { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is preferred.
        /// </summary>
        public virtual bool IsPreferred { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is show on lead page.
        /// </summary>
        public virtual bool IsShowOnLeadPage { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is portal user name.
        /// true if the email is used in the portal as user name
        /// This field is reserved for future use, when student migrating to the new schema.
        /// </summary>
        public virtual bool IsPortalUserName { get; protected set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the lead object that owe the email.
        /// </summary>
        public virtual Lead LeadObj { get; protected set; }

        /// <summary>
        ///  Gets or sets Email Type Object
        /// </summary>
        public virtual EmailType EmailTypeObj { get; protected set; }

        /// <summary>
        ///  Gets or sets The Status of the Lead Email
        /// </summary>
        public virtual SyStatuses Status { get; protected set; }

        /// <summary>
        ///  Update email.
        /// </summary>
        /// <param name="record">
        ///  The record.
        /// </param>
        /// <param name="username">
        ///  The user name.
        /// </param>
        /// <param name="emailtype">
        ///  The email type.
        /// </param>
        public virtual void UpdateEmail(LeadEmailOutputModel record, string username, EmailType emailtype)
        {
            this.Email = record.Email;
            this.EmailTypeObj = emailtype;
            this.IsPreferred = record.IsPreferred;
            this.ModDate = DateTime.Now;
            this.ModUser = username;
        }

        /// <summary>
        /// update email.
        /// </summary>
        /// <param name="record">
        ///  The record.
        /// </param>
        /// <param name="username">
        ///  The user name.
        /// </param>
        /// <param name="emailtype">
        ///  The email type.
        /// </param>
        /// <param name="status">
        ///  The status.
        /// </param>
        public virtual void UpdateEmail(LeadContactInputModel record, string username, EmailType emailtype, SyStatuses status)
        {
            this.Email = record.Email.Trim();
            this.EmailTypeObj = emailtype;
            this.IsPreferred = record.IsBest;
            this.ModDate = DateTime.Now;
            this.ModUser = username;
            this.IsShowOnLeadPage = record.ShowOnLeadPage;
            this.Status = status;
        }

        /// <summary>
        ///  Update modification date and user in
        /// this LeadEmail
        /// </summary>
        /// <param name="modDate">
        ///  The modification date.
        /// </param>
        /// <param name="username">
        /// The user-name.
        /// </param>
        public virtual void UpdateModDateAndUser(DateTime modDate, string username)
        {
            this.ModDate = modDate;
            this.ModUser = username;
        }
    }
}
