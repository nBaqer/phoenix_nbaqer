﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadImage.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Domain for Table LeadImage
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Lead
{
    using System;
    using Infrastructure.Entities;

    /// <summary>
    /// Domain for Table LeadImage
    /// </summary>
    public class LeadImage : DomainEntityWithTypedID<int>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadImage"/> class.
        /// Constructor use this to create the object.
        /// </summary>
        /// <param name="leadObj">
        /// The lead Object.
        /// </param>
        /// <param name="image">
        /// The image.
        /// </param>
        /// <param name="imgFile">
        /// The image File.
        /// </param>
        /// <param name="imgSize">
        /// The image Size.
        /// </param>
        /// <param name="extension">
        /// The extension.
        /// </param>
        /// <param name="type">
        /// The type Object.
        /// </param>
        /// <param name="modDate">
        /// The mod Date.
        /// </param>
        /// <param name="modUser">
        /// The mod User.
        /// </param>
        public LeadImage(
               Lead leadObj,
               byte[] image,
               string imgFile,
               int imgSize,
               string extension,
               int type,
               DateTime modDate,
               string modUser)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.LeadObj = leadObj;
            this.Image = image;
            this.ImgFile = imgFile;
            this.ImgSize = imgSize;
            this.MediaType = extension;
            this.Type = type;
            this.ModDate = modDate;
            this.ModUser = modUser;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadImage"/> class.
        /// Protected Constructor.
        /// </summary>
        protected LeadImage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual Lead LeadObj { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual byte[] Image { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string ImgFile { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual int ImgSize { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string MediaType { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual int Type { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string ModUser { get; protected set; }

        #endregion

        /// <summary>
        /// Update the image
        /// </summary>
        /// <param name="leadObj">The lead Object</param>
        /// <param name="image"> The image body</param>
        /// <param name="imgFile">The name of the file</param>
        /// <param name="imgSize">The size of the image</param>
        /// <param name="mediatype">The media type of the image</param>
        /// <param name="type">The Type. For now type 1</param>
        /// <param name="modDate">The modification Date</param>
        /// <param name="modUser">The modification user</param>
        public virtual void UpdateImage(
             Lead leadObj,
             byte[] image,
             string imgFile,
             int imgSize,
             string mediatype,
             int type,
             DateTime modDate,
             string modUser)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.LeadObj = leadObj;
            this.Image = image;
            this.ImgFile = imgFile;
            this.ImgSize = imgSize;
            this.MediaType = mediatype;
            this.Type = type;
            this.ModDate = modDate;
            this.ModUser = modUser;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }
    }
}