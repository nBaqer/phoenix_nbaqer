﻿using System;
using System.Collections;
using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// Domain for Table syLeadStatusChanges
    /// This table hold the allowed changes 
    /// from one status to another for lead
    /// </summary>
    public class LeadWorkflowStatusChanges : DomainEntity
    {
        /// <summary>
        /// Protect the default constructor
        /// </summary>
        protected LeadWorkflowStatusChanges() { }

        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="origStatusObj"></param>
        /// <param name="newStatusObj"></param>
        /// <param name="campusGroupObj"></param>
        /// <param name="modDate"></param>
        /// <param name="modUser"></param>
        /// <param name="leadObj"></param>
        public LeadWorkflowStatusChanges(
            UserDefStatusCode origStatusObj,
            UserDefStatusCode newStatusObj,
            CampusGroup campusGroupObj,
            DateTime modDate,
            string modUser,
            Lead leadObj)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            OrigStatusObj = origStatusObj;
            NewStatusObj = newStatusObj;
            CampusGroupObj = campusGroupObj;
            ModDate = modDate;
            ModUser = modUser;
            LeadObj = leadObj;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Original status
        /// </summary>
        public virtual UserDefStatusCode OrigStatusObj { get; protected set; }

        /// <summary>
        /// New Status of the lead
        /// </summary>
        public virtual UserDefStatusCode NewStatusObj { get; protected set; }

        /// <summary>
        /// Date of change status
        /// </summary>
        public virtual CampusGroup CampusGroupObj { get; protected set; }

        /// <summary>
        /// Day that record was input
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// User that input the record
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Inverse relation. list of Leads in the program group
        /// </summary>
        public virtual Lead LeadObj { get; protected set; }

        /// <summary>
        /// List of permission given to roles to the specific work flow status change
        /// </summary>
        public virtual IList<LeadWorkflowStatusChangesPermissions> StatusChangesPermissionsList { get; protected set; }
    }
}