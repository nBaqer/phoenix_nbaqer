﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// LeadOtherContactPhone is the Domain Object for the adLeadOtherContactsPhone table
    /// </summary>
    public class LeadOtherContactPhone : DomainEntity
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public LeadOtherContactPhone()
        {
        }
        /// <summary>
        /// Default Constructor
        /// </summary>
        public LeadOtherContactPhone(LeadOtherContact OtherContact, Lead Lead, SyPhoneType PhoneType, SyStatuses Status, string Phone, string Extension, bool IsForeignPhone, string ModUser)
        {

            this.OtherContactId = OtherContact.ID;
            this.LeadId = Lead.ID;
            this.PhoneTypeId = PhoneType.ID;
            this.StatusId = Status.ID;

            this.LeadOtherContact = OtherContact;
            this.Lead = Lead;
            this.PhoneType = PhoneType;
            this.Status = Status;

            this.Phone = Phone;
            this.Extension = Extension;
            this.IsForeignPhone = IsForeignPhone;
            this.ModUser = ModUser;
            ModDate = DateTime.Now;
        }

        /// <summary>
        /// ID
        /// </summary>
        public virtual Guid OtherContactsPhoneId
        {
            get { return ID; }
            protected set { ID = value; }
        }
        /// <summary>
        /// ID Of the Lead Other Other Contact Instance
        /// </summary>
        public virtual Guid OtherContactId { get; protected set; }
        /// <summary>
        /// Lead Other Contact
        /// </summary>
        public virtual LeadOtherContact LeadOtherContact { get; protected set; }
        /// <summary>
        /// Lead ID
        /// </summary>
        public virtual Guid LeadId { get; protected set; }
        /// <summary>
        /// Lead
        /// </summary>
        public virtual Lead Lead { get; protected set; }
        /// <summary>
        /// Phone Type Id
        /// </summary>
        public virtual Guid PhoneTypeId { get; protected set; }
        /// <summary>
        /// Phone Type
        /// </summary>
        public virtual SyPhoneType PhoneType { get; protected set; }
        /// <summary>
        /// Phone
        /// </summary>
        public virtual string Phone { get; protected set; }
        /// <summary>
        /// Phone Extension
        /// </summary>
        public virtual string Extension { get; protected set; }
        /// <summary>
        /// Flag Indicating if Phone Number is Foreign
        /// </summary>
        public virtual bool IsForeignPhone { get; protected set; }
        /// <summary>
        /// Status ID
        /// </summary>
        public virtual Guid StatusId { get; protected set; }
        /// <summary>
        /// Status
        /// </summary>
        public virtual SyStatuses Status { get; protected set; } 
        /// <summary>
        /// User last modified this object
        /// </summary>
        public virtual string ModUser { get; protected set; }
        /// <summary>
        /// Last Modified Date
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        public virtual void Update(SyPhoneType PhoneType, SyStatuses Status, string Phone, string Extension, bool IsForeignPhone, string ModUser)
        {
            this.PhoneType = PhoneType;
            this.Status = Status;
            this.Phone = Phone;
            this.Extension = Extension;
            this.IsForeignPhone = IsForeignPhone;
            this.ModUser = ModUser;
            ModDate = DateTime.Now;
        }

    }
}
