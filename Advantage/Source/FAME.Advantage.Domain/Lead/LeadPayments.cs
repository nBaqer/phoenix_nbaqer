﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadPayments.cs" company="FAME Inc.">
//   FAME 2016
//    FAME.Advantage.Domain.Lead.LeadPayments
// </copyright>
// <summary>
//   Describe the vendors specifications
// </summary>
// --------------------------------------------------------------------------------------------------------------------

// ReSharper disable VirtualMemberCallInConstructor
namespace FAME.Advantage.Domain.Lead
{
    using System;

    using FAME.Advantage.Domain.Catalogs;

    using Infrastructure.Entities;

    /// <summary>
    /// Describe the vendors specifications
    /// </summary>
    public class LeadPayments : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadPayments"/> class. 
        /// </summary>
        public LeadPayments()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadPayments"/> class. 
        /// </summary>
        /// <param name="paymentGuidId">
        /// The Payment Id (Transaction Id)
        /// </param>
        /// <param name="checkNumber">
        /// The Check Number
        /// </param>
        /// <param name="payTypeId">
        /// The Payment Type Id (Associate to SAPaymentTypes table)
        /// </param>
        /// <param name="modUser">
        /// The modified user
        /// </param>
        /// <param name="isDeposited">
        /// The is deposited
        /// </param>
        public LeadPayments(Guid paymentGuidId, string checkNumber, int payTypeId, string modUser, bool? isDeposited)
        {
            this.ID = paymentGuidId;
            this.CheckNumber = checkNumber;
            this.ModUser = modUser;
            this.ModDate = DateTime.Now;
            this.IsDeposited = isDeposited != null ? isDeposited.Value : false;
        }

        /// <summary>
        /// Gets or sets the check number.
        /// </summary>
        public virtual string CheckNumber { get; protected set; }

        /// <summary>
        /// Gets or sets the payment type.
        /// </summary>
        public virtual PaymentType PaymentType { get; protected set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public virtual DateTime? ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the is deposited.
        /// </summary>
        public virtual bool? IsDeposited { get; protected set; }
    }
}