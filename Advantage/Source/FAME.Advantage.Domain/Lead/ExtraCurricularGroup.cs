﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// Domain for Extra curricular Group table
    /// </summary>
    public class ExtraCurricularGroup : DomainEntity
    {
        /// <summary>
        /// Extra curricular Code
        /// </summary>
        public virtual string ExtraCurrGrpCode { get; set; }
        
        /// <summary>
        /// Description
        /// </summary>
        public virtual string ExtraCurrGrpDescrip { get; set; }
        
        /// <summary>
        /// Status (Active Inactive) of the group
        /// </summary>
        public virtual SyStatuses StatusObj { get; set; }
        
        /// <summary>
        /// The campus group that owner the group
        /// </summary>
        public virtual CampusGroup CampusGroupObj { get; set; }
        
        /// <summary>
        /// Modification date
        /// </summary>
        public virtual DateTime ModDate { get; set; }
        
        /// <summary>
        /// Modification user.
        /// </summary>
        public virtual string ModUser { get; set; }

        /// <summary>
        /// List of extracurricular where apply this group.
        /// </summary>
        public virtual IList<ExtraCurriculars> ExtraCurricularList { get; set; } 

    }
}
