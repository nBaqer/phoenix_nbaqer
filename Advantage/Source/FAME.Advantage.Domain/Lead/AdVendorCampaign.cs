﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.SystemStuff.Maintenance;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// Vendor Campaign Domain
    /// </summary>
    public class AdVendorCampaign : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Vendor proprietary of the campaign.
        /// </summary>
        public virtual AdVendors VendorObj { get; protected set; }

        /// <summary>
        /// The code of this campaign
        /// The campaign code and VendorIbj combination must be unique
        /// </summary>
        public virtual string CampaignCode { get; protected set; }

        /// <summary>
        /// Account Id with the vendor for this campaign
        /// </summary>
        public virtual string AccountId { get; protected set; }

        /// <summary>
        /// True: The Vendor lead service is active on the system
        /// False: The vendor lead service is inactive. 
        /// They can not send lead info to Advantage
        /// </summary>
        public virtual int IsActive { get; protected set; }

        /// <summary>
        /// This is 1 the record should not be take in considerations
        /// to get the info. that is all select operation must not get records
        /// with this field in 1.
        /// </summary>
        public virtual int IsDeleted { get; protected set; }

        /// <summary>
        /// Campaign date and time of Init
        /// </summary>
        public virtual DateTime DateCampaignBegin { get; protected set; }

        /// <summary>
        /// Campaign Date an Time to finish.
        /// </summary>
        public virtual DateTime DateCampaignEnd { get; protected set; }

        /// <summary>
        /// Cost of the campaign
        /// </summary>
        public virtual float Cost { get; protected set; }

        /// <summary>
        /// 1: reject leads that are not  from certain counties defined by the school
        /// </summary>
        public virtual int FilterRejectByCounty { get; protected set; }

        /// <summary>
        /// 1: reject the leads that are considered as duplicated.
        /// </summary>
        public virtual int FilterRejectDuplicates { get; protected set; }

        /// <summary>
        /// Unit of Cost of the campaign
        /// </summary>
        public virtual AdVendorPayFor PayForObj { get; protected set; }

        /// <summary>
        /// List of Leads associated with this Campaign.
        /// </summary>
        public virtual IList<Lead> LeadsList { get; protected set; }

        /// <summary>
        /// Update Record
        /// </summary>
        /// <param name="data"></param>
        /// <param name="objPayFor"></param>
        /// <param name="objVendor"></param>
        public virtual void UpdateRecord(WapiVendorsCampaignLeadOutputModel data, AdVendorPayFor objPayFor, AdVendors objVendor)
        {
            VendorObj = objVendor;
            PayForObj = objPayFor;
            CampaignCode = data.CampaignCode;
            AccountId = data.AccountId;
            IsActive = data.IsActive;
            DateCampaignBegin = data.DateCampaignBegin;
            DateCampaignEnd = data.DateCampaignEnd;
            Cost = data.Cost;
            FilterRejectByCounty = data.FilterRejectByCounty;
            FilterRejectDuplicates = data.FilterRejectDuplicates;
        }

        /// <summary>
        /// This mark as deleted the record
        /// </summary>
        public virtual void Delete()
        {
            IsDeleted = 1;
        }
    }
}