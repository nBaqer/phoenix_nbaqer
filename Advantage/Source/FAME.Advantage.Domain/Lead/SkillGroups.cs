﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// Skills groups table.
    /// </summary>
    public class SkillGroups : DomainEntity
    {
        /// <summary>
        /// Skill Code
        /// </summary>
        public virtual string SkillGrpCode { get; set; }

        /// <summary>
        /// Skill Name
        /// </summary>
        public virtual string SkillGrpName { get; set; }

        /// <summary>
        /// Status (Active Inactive) of the group
        /// </summary>
        public virtual SyStatuses StatusObj { get; set; }

        /// <summary>
        /// The campus group that owner the group
        /// </summary>
        public virtual CampusGroup CampusGroupObj { get; set; }

        /// <summary>
        /// Modification date
        /// </summary>
        public virtual DateTime ModDate { get; set; }

        /// <summary>
        /// Modification user.
        /// </summary>
        public virtual string ModUser { get; set; }

        /// <summary>
        /// List of Skill where apply this group.
        /// </summary>
        public virtual IList<AdSkills> SkillsList { get; set; }

    }
}
