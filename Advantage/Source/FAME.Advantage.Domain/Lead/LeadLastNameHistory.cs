﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// Store consecutive the changes of the last name for lead
    /// Domain for Table adLastNamehistory
    /// </summary>
    public class LeadLastNameHistory : DomainEntityWithTypedID<int>
    {
         /// <summary>
        /// Default constructor protected to avoid the use of object.
        /// </summary>
        protected LeadLastNameHistory() { }
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="lastName"></param>
        /// <param name="leadObj"></param>
        /// <param name="modUser"></param>
        /// <param name="modDate"></param>
        public LeadLastNameHistory(string lastName, Lead leadObj, string modUser, DateTime modDate)
        {
            // ReSharper disable VirtualMemberCallInContructor
            LastName = lastName;
            LeadObj = leadObj;
            ModUser = modUser;
            ModDate = modDate;
            // ReSharper restore VirtualMemberCallInContructor
        }

        /// <summary>
        /// Lead Previous Last Name
        /// </summary>
        public virtual string LastName { get; protected set; }

        /// <summary>
        /// Lead associated with the employment
        /// </summary>
        public virtual Lead LeadObj { get; protected set; }

        /// <summary>
        /// Modification user
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Date of modification.
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }
    }
}