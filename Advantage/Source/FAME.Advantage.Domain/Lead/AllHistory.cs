﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Catalogs;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff.Resources;
using FAME.Advantage.Domain.Users;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// Domain where all history records are stored
    /// </summary>
    public class AllHistory: DomainEntity
    {
        #region Constructor
        /// <summary>
        /// Protected to avoid external use
        /// </summary>
        public AllHistory() { }

        /// <summary>
        /// All History Public constructor. Use this to instantiate the class.
        /// </summary>
        /// <param name="moduleCode"></param>
        /// <param name="modDate"></param>
        /// <param name="historyModule"></param>
        /// <param name="type">type of record that was changed
        /// <param name="description">description of the historical change
        /// <param name="modUser"></param>
        ///  <param name="AdditionalContent"></param>
        protected AllHistory(string moduleCode,DateTime modDate, string historyModule, string type,  string description,
            string modUser, Guid leadId, string additionalContent)
        {
            // ReSharper disable VirtualMemberCallInContructor
            ModuleCode = moduleCode;
            ModDate = modDate;
            HistoryModule = historyModule;
            HistoryType = type;
            Description = description;
            ModUserFullName = modUser;
            LeadId = leadId; ;
            AdditionalContent = additionalContent;

            // ReSharper restore VirtualMemberCallInContructor
        }
        #endregion

        #region Properties
        /// <summary>
        /// Module code 2 characters
        /// </summary>
        public virtual string ModuleCode { get; protected set; }

        /// <summary>
        /// Modification date
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Module description
        /// </summary>
        public virtual string HistoryModule { get; protected set; }

        /// <summary>
        /// Type of history
        /// </summary>
        public virtual string HistoryType { get; protected set; }

        /// <summary>
        /// Description of historical change
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// User that made the historical change 
        /// </summary>
        public virtual string ModUserFullName { get; protected set; }
        

        /// <summary>
        /// List of Notes entered in the lead Notes page.
        /// </summary>
        public virtual Guid LeadId { get; protected set; }


        /// <summary>
        /// Additional content assocaited with the historical event e.g. email content
        /// </summary>
        public virtual string AdditionalContent { get; protected set; }
        #endregion

    }
}
