﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.Catalogs;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Lead
{
    public class LeadOtherContactEmail : DomainEntity
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public LeadOtherContactEmail()
        {

        }
        /// <summary>
        /// Create Lead Other Contact Email
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="OtherContactId"></param>
        /// <param name="LeadId"></param>
        /// <param name="Email"></param>
        /// <param name="EmatilTypeId"></param>
        /// <param name="ModUser"></param>
        /// <param name="StatusId"></param>
        public LeadOtherContactEmail(LeadOtherContact OtherContact, Lead Lead, EmailType EmailType,SyStatuses Status, string Email, string ModUser )
        {
            this.OtherContact= OtherContact;
            //this.LeadId = Lead.ID;
            this.Email = Email;
            this.EmailType = EmailType;
            this.ModUser = ModUser;
           // this.StatusId = Status.ID;
            this.Status = Status;
            ModDate = DateTime.Now;
            this.Lead = Lead;
            this.EmailType = EmailType;
        }
        /// <summary>
        /// ID
        /// </summary>
        public virtual Guid OtherContactsEmailId
        {
            get { return ID; }
            protected set { ID = value; }
        }
        /// <summary>
        /// Lead Other Contact
        /// </summary>
        public virtual LeadOtherContact OtherContact { get; protected set; }
        /// <summary>
        /// Lead
        /// </summary>
        public virtual Lead Lead { get; protected set; }

        /// <summary>
        /// Email Address
        /// </summary>
        public virtual string Email { get; protected set; }

        /// <summary>
        /// Email Type
        /// </summary>
        public virtual EmailType EmailType { get; protected set; }

        /// <summary>
        /// User last modified this object
        /// </summary>
        public virtual string ModUser { get; protected set; }
        /// <summary>
        /// Last Modified Date
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }
        /// <summary>
        /// Status
        /// </summary>
        public virtual SyStatuses Status { get; protected set; }

        /// <summary>
        /// Update Lead Other Contact Email
        /// </summary>
        /// <param name="EmailType"></param>
        /// <param name="Status"></param>
        /// <param name="Email"></param>
        /// <param name="ModUser"></param>
        public virtual void Update(EmailType EmailType, SyStatuses Status, string Email, string ModUser)
        {
            this.Email = Email;
            this.ModUser = ModUser;
            ModDate = DateTime.Now;
            this.EmailType = EmailType;
            this.Status = Status;
        }
    }
}