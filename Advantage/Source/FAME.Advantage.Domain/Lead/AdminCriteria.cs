﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// Domain for table adAdminCriteria
    /// </summary>
    public class AdminCriteria : DomainEntity
    {
        /// <summary>
        ///AdminCriteria description
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// AdminCriteria Code
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Status Active inactive of the phone type.
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// Campus group object for the AdminCriteria
        /// </summary>
        public virtual CampusGroup CampusGroupObj { get; protected set; }

        /// <summary>
        /// Inverse relation. list of Leads in the program group
        /// </summary>
        public virtual IList<Lead> LeadList { get; protected set; }
    }
}