﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// Extracurricular Levels
    /// </summary>
    public class AdLevels : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Extra curricular Code
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Description
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// Status (Active Inactive) of the group
        /// </summary>
        public virtual SyStatuses StatusObj { get; protected set; }

        /// <summary>
        /// Modification date
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Modification user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// List of extracurricular where apply this group.
        /// </summary>
        public virtual IList<ExtraCurriculars> ExtraCurricularList { get; protected set; }

    }
}
