﻿using System;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Lead
{
    /// <summary>
    /// Domain for View View_LeadUserTask
    /// </summary>
    public class LeadTaskView: DomainEntity
    {
        /// <summary>
        /// Protected Constructor
        /// </summary>
        protected LeadTaskView()
        {
        }

        /// <summary>
        /// Use this to create instance
        /// </summary>
        /// <param name="code"></param>
        /// <param name="activity"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public LeadTaskView(string code, string activity, DateTime? startDate, DateTime? endDate)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            StartDate = startDate;
            EndDate = endDate;
            Code = code;
            Activity = activity;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Activity Description
        /// </summary>
        public virtual string Activity { get; set; }
        
       /// <summary>
        /// Activity  Code
        /// </summary>
        public virtual string Code { get; protected set; }
        
        /// <summary>
        /// Start Date
        /// </summary>
        public virtual DateTime? StartDate { get; protected set; }

        /// <summary>
        /// End Date
        /// </summary>
        public virtual DateTime? EndDate { get; protected set; }
  
        /// <summary>
        /// Status Task
        /// </summary>
        public virtual int? Status { get; protected set; }
       
        /// <summary>
        /// Lead associate with this Lead Task
        /// </summary>
        public virtual Lead LeadsObj { get; protected set; }

        /// <summary>
        /// Campus Group Obj.
        /// </summary>
        public virtual CampusGroup CampusGrpObj { get; protected set; }

    }
}