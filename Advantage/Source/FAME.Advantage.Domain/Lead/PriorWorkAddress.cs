﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PriorWorkAddress.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Domain for Table PriorWorkAddress
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Lead
{
    using System;

    using FAME.Advantage.Domain.Catalogs;
    using FAME.Advantage.Domain.SystemStuff;

    using Infrastructure.Entities;

    /// <summary>
    /// Domain for Table PriorWorkAddress
    /// </summary>
    public class PriorWorkAddress : DomainEntityWithTypedID<int>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PriorWorkAddress"/> class.
        /// Constructor use this to create the object.
        /// </summary>
        /// <param name="employmentObj">
        /// The st Employment Object.
        /// </param>
        /// <param name="address1">
        /// The address 1.
        /// </param>
        /// <param name="apartment">
        /// The apartment.
        /// </param>
        /// <param name="address2">
        /// The address 2.
        /// </param>
        /// <param name="city">
        /// The city.
        /// </param>
        /// <param name="stateObj">
        /// The state Code.
        /// </param>
        /// <param name="isInternational">
        ///  True is international
        /// </param>
        /// <param name="stateInternational">
        /// the state if the address is international
        /// </param>
        /// <param name="zipCode">
        /// The zip Code.
        /// </param>
        /// <param name="countryObj">
        /// The country.
        /// </param>
        /// <param name="modUser">
        /// The mod User.
        /// </param>
        /// <param name="modDate">
        /// The mod Date.
        /// </param>
        public PriorWorkAddress(
                 AdLeadEmployment employmentObj,
                 string address1,
                 string apartment,
                 string address2,
                 string city,
                 SystemStates stateObj,
                 bool isInternational,
                 string stateInternational,
                 string zipCode,
                 Country countryObj,
                 string modUser,
                 DateTime modDate)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.StEmploymentObj = employmentObj;
            this.Address1 = address1;
            this.Apartment = apartment ?? string.Empty;
            this.Address2 = address2 ?? string.Empty;
            this.City = city ?? string.Empty;
            this.StateObj = stateObj;
            this.IsInternational = isInternational;
            this.StateInternational = stateInternational ?? string.Empty;
            this.ZipCode = zipCode ?? string.Empty;
            this.CountryObj = countryObj;
            this.ModUser = modUser;
            this.ModDate = modDate;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PriorWorkAddress"/> class.
        /// Protected Constructor.
        /// </summary>
        protected PriorWorkAddress()
        {
        }

        #endregion
        
        #region Properties

        /// <summary>
        /// Gets or sets Employment
        /// </summary>
        public virtual AdLeadEmployment StEmploymentObj { get; protected set; }

        /// <summary>
        /// Gets or sets Address1
        /// </summary>
        public virtual string Address1 { get; protected set; }

        /// <summary>
        /// Gets or sets Apartment, unit
        /// </summary>
        public virtual string Apartment { get; protected set; }

        /// <summary>
        /// Gets or sets Address 2
        /// </summary>
        public virtual string Address2 { get; protected set; }

        /// <summary>
        /// Gets or sets City
        /// </summary>
        public virtual string City { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is international.
        /// </summary>
        public virtual bool IsInternational { get; protected set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public virtual SystemStates StateObj { get; protected set; }

        /// <summary>
        /// Gets or sets the state international.
        /// </summary>
        public virtual string StateInternational { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public virtual Country CountryObj { get; protected set; }
        
        /// <summary>
        /// Gets or sets Zip Code
        /// </summary>
        public virtual string ZipCode { get; protected set; }

        /// <summary>
        /// Gets or sets Modification User
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets Modification Date
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        #endregion

        /// <summary>
        ///  The update prior work address.
        /// </summary>
        /// <param name="employmentObj">
        ///  The employment object.
        /// </param>
        /// <param name="address1">
        ///  The address 1.
        /// </param>
        /// <param name="apartment">
        ///  The apartment.
        /// </param>
        /// <param name="address2">
        ///  The address 2.
        /// </param>
        /// <param name="city">
        ///  The city.
        /// </param>
        /// <param name="stateObj">
        ///  The state object.
        /// </param>
        /// <param name="isInternational">
        ///  The is international.
        /// </param>
        /// <param name="stateInternational">
        ///  The state international.
        /// </param>
        /// <param name="zipCode">
        ///  The zip code.
        /// </param>
        /// <param name="countryObj">
        ///  The country object.
        /// </param>
        /// <param name="modUser">
        ///  The mod user.
        /// </param>
        /// <param name="modDate">
        ///  The modification date.
        /// </param>
        public virtual void UpdatePriorWorkAddress(
           AdLeadEmployment employmentObj,
           string address1,
           string apartment,
           string address2,
           string city,
           SystemStates stateObj,
           bool isInternational,
           string stateInternational,
           string zipCode,
           Country countryObj,
           string modUser,
           DateTime modDate)
        {
            this.StEmploymentObj = employmentObj;
            this.Address1 = address1;
            this.Apartment = apartment ?? string.Empty;
            this.Address2 = address2 ?? string.Empty;
            this.City = city ?? string.Empty;
            this.StateObj = stateObj;
            this.IsInternational = isInternational;
            this.StateInternational = stateInternational;
            this.ZipCode = zipCode ?? string.Empty;
            this.CountryObj = countryObj;
            this.ModUser = modUser;
            this.ModDate = modDate;
       }
    }
}