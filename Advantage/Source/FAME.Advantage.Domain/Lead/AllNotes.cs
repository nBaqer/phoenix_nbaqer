﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AllNotes.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Domain where all notes are stored
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Lead
{
    using System;
    using System.Collections.Generic;
    using SystemStuff.Resources;
    using Catalogs;
    using Infrastructure.Entities;
    using Users;

    /// <summary>
    /// Domain where all notes are stored
    /// </summary>
    public class AllNotes : DomainEntityWithTypedID<int>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AllNotes"/> class. 
        /// All Notes Public constructor. Use this to instantiate the class.
        /// </summary>
        /// <param name="module">
        /// The system module
        /// </param>
        /// <param name="type">
        /// free text with a type description
        /// </param>
        /// <param name="pageFields">
        /// The page field
        /// </param>
        /// <param name="user">
        /// The user associated
        /// </param>
        /// <param name="noteText">
        /// the text of the notes itself
        /// </param>
        /// <param name="modUser">
        /// the modification user
        /// </param>
        public AllNotes(
            SystemModule module,
            string type,
            NotesPageFields pageFields,
            User user,
            string noteText,
            string modUser)
        {
            // ReSharper disable VirtualMemberCallInContructor
            this.ModuleObj = module;
            this.NoteType = type;

            // IsConfidential = isConfidential;
            this.PageFieldsObj = pageFields;
            this.UserObj = user;
            this.NoteText = noteText;
            this.ModDate = DateTime.Now;
            this.ModUser = modUser;

            // ReSharper restore VirtualMemberCallInContructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AllNotes"/> class. 
        /// Protected to avoid external use
        /// </summary>
        protected AllNotes()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets Module code 2 characters
        /// </summary>
        public virtual SystemModule ModuleObj { get; protected set; }

        /// <summary>
        ///  Gets or sets Type of notes
        /// </summary>
        public virtual string NoteType { get; protected set; }

        /// <summary>
        ///  Gets or sets Define is the notes is confidential or not
        /// 0 no confidential 1 confidential.
        /// </summary>
        public virtual int IsConfidential { get; protected set; }

        /// <summary>
        ///  Gets or sets Page and field caption where the notes was wrote.
        /// </summary>
        public virtual NotesPageFields PageFieldsObj { get; protected set; }

        /// <summary>
        ///  Gets or sets User that make or change the notes
        /// </summary>
        public virtual User UserObj { get; protected set; }

        /// <summary>
        ///  Gets or sets Notes Text
        /// </summary>
        public virtual string NoteText { get; protected set; }

        /// <summary>
        ///  Gets or sets Modification date
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        ///  Gets or sets Modification user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        ///  Gets or sets List of Notes entered in the lead Notes page.
        /// </summary>
        public virtual IList<Lead> LeadList { get; protected set; }
        #endregion

        #region Methods

        /// <summary>
        /// Update the notes type, the notes text and who modify
        /// </summary>
        /// <param name="noteText">The text of the notes</param>
        /// <param name="modUser">The user that modification or create the notes</param>
        /// <param name="noteType">The type of notes</param>
        /// <param name="userObj"> The user that modify the notes</param>
        public virtual void Update(
            string noteText, 
            string modUser, 
            string noteType,
            User userObj)
        {
            this.NoteText = noteText;
            
            this.ModUser = modUser;
            this.NoteType = noteType;
            this.ModDate = DateTime.Now;
            this.UserObj = userObj;
        }

        #endregion
    }
}