﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadAddress.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Domain for AdLeadPhone Table
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Lead
{
    using System;

    using FAME.Advantage.Domain.Catalogs;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Messages.Lead;

    /// <summary>
    /// Domain for AdLeadAddress Table
    /// </summary>
    public class LeadAddress : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadAddress"/> class.
        /// </summary>
        /// <param name="addressType">
        ///     The address type.
        /// </param>
        /// <param name="address1">
        ///     The address 1.
        /// </param>
        /// <param name="address2">
        ///     The address 2.
        /// </param>
        /// <param name="addressApto">
        /// Apartment, Unit or other identification
        /// </param>
        /// <param name="city">
        ///     The city.
        /// </param>
        /// <param name="state">
        ///     The state.
        /// </param>
        /// <param name="zipcode">
        ///     The zip-code.
        /// </param>
        /// <param name="country">
        ///     The country.
        /// </param>
        /// <param name="status">
        ///     The status.
        /// </param>
        /// <param name="isMailingAddress">
        ///     The is mailing address.
        /// </param>
        /// <param name="isShowOnLeadPage">
        ///     The is show on lead page.
        /// </param>
        /// <param name="modUser">
        ///     The mod user.
        /// </param>
        /// <param name="lead">
        ///     The lead.
        /// </param>
        /// <param name="isInternational">
        ///     The is international.
        /// </param>
        /// <param name="stateInternational">
        ///     The state international.
        /// </param>
        /// <param name="county">
        ///     The county.
        /// </param>
        /// <param name="countyInternational">
        ///     The county international.
        /// </param>
        /// <param name="countryInternational">
        ///     The country international.
        /// </param>
        public LeadAddress(
            AddressType addressType,
            string address1,
            string address2,
            string addressApto,
            string city,
            SystemStates state,
            string zipcode,
            Country country,
            SyStatuses status,
            bool isMailingAddress,
            bool isShowOnLeadPage,
            string modUser,
            Lead lead,
            bool isInternational,
            string stateInternational,
            County county,
            string countyInternational,
            string countryInternational)
        {
            // ReSharper disable VirtualMemberCallInConstructor
            this.AddressType = addressType;
            this.Address1 = address1;
            this.Address2 = address2;
            this.City = city;
            this.StateObj = state;
            this.ZipCode = zipcode;
            this.CountryObj = country;
            this.StatusObj = status;
            this.IsMailingAddress = isMailingAddress;
            this.IsShowOnLeadPage = isShowOnLeadPage;
            this.ModUser = modUser;
            this.LeadObj = lead;
            this.StateInternational = stateInternational;
            this.IsInternational = isInternational;
            this.CountyObj = county;
            this.CountyInternational = countyInternational;
            this.CountryInternational = countryInternational;
            this.AddressApto = addressApto ?? string.Empty;
            this.ModDate = DateTime.Now;
            // ReSharper restore VirtualMemberCallInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadAddress"/> class.
        /// </summary>
        /// <param name="newAddress">
        ///  The new address.
        /// </param>
        /// <param name="lead">
        ///  The lead.
        /// </param>
        public LeadAddress(LeadAddress newAddress, Lead lead)
        {
            // ReSharper disable VirtualMemberCallInConstructor
            this.AddressType = newAddress.AddressType;

            this.Address1 = newAddress.Address1;
            this.Address2 = newAddress.Address2;
            this.City = newAddress.City;
            this.StateObj = newAddress.StateObj;
            this.ZipCode = newAddress.ZipCode;
            this.CountryObj = newAddress.CountryObj;
            this.StatusObj = newAddress.StatusObj;
            this.IsShowOnLeadPage = newAddress.IsShowOnLeadPage;
            this.ModUser = newAddress.ModUser;
            this.LeadObj = lead;
            this.StateInternational = newAddress.StateInternational;
            this.IsInternational = newAddress.IsInternational;
            this.CountyObj = newAddress.CountyObj;
            this.CountyInternational = newAddress.CountyInternational;
            this.CountryInternational = newAddress.CountryInternational;
            this.ModDate = DateTime.Now;
            // ReSharper restore VirtualMemberCallInConstructor 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadAddress"/> class. 
        /// Protected Constructor
        /// </summary>
        protected LeadAddress()
        {
        }

        /// <summary>
        /// Gets or sets the address type.
        /// </summary>
        public virtual AddressType AddressType { get; protected set; }

        /// <summary>
        /// Gets or sets the address 1.
        /// </summary>
        public virtual string Address1 { get; protected set; }

        /// <summary>
        /// Gets or sets the address apartment.
        /// </summary>
        public virtual string AddressApto { get; protected set; }

        /// <summary>
        /// Gets or sets the address 2.
        /// </summary>
        public virtual string Address2 { get; protected set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public virtual string City { get; protected set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public virtual SystemStates StateObj { get; protected set; }

        /// <summary>
        /// Gets or sets the state international.
        /// </summary>
        public virtual string StateInternational { get; set; }

        /// <summary>
        /// Gets or sets the Country Name if the address is an international address
        /// </summary>
        public virtual string CountryInternational { get; set; }
        
        /// <summary>
        ///  Gets or sets the  County Name if the address is an international address
        /// </summary>
        public virtual string CountyInternational { get; set; }

        /// <summary>
        /// Gets or sets the zip code.
        /// </summary>
        public virtual string ZipCode { get; protected set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public virtual Country CountryObj { get; protected set; }

        /// <summary>
        /// Gets or sets the county.
        /// </summary>
        public virtual County CountyObj { get; protected set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public virtual SyStatuses StatusObj { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is mailing address.
        /// </summary>
        public virtual bool IsMailingAddress { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is show on lead page.
        /// </summary>
        public virtual bool IsShowOnLeadPage { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is international.
        /// </summary>
        public virtual bool IsInternational { get; protected set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets the lead object.
        /// </summary>
        public virtual Lead LeadObj { get; protected set; }

        /// <summary>
        /// Update Address
        /// </summary>
        /// <param name="input">See <see cref="LeadAddressInputModel"/></param>
        /// <param name="type">See <see cref="AddressType"/></param>
        /// <param name="status">See <see cref="SyStatuses"/></param>
        /// <param name="state">See <see cref="SystemStates"/></param>
        /// <param name="country">See <see cref="CountryObj"/></param>
        /// <param name="county">See <see cref="CountyObj"/></param>
        public virtual void Update(LeadAddressInputModel input, AddressType type, SyStatuses status, SystemStates state, Country country, County county)
        {
            this.AddressType = type;
            this.StatusObj = status;
            this.Address1 = input.Address1;
            this.Address2 = input.Address2;
            this.City = input.City;
            this.IsMailingAddress = input.IsMaillingAddress;
            this.IsShowOnLeadPage = input.ShowOnLeadPage;
            this.ModDate = DateTime.Now;
            this.ModUser = input.UserName;
            this.ZipCode = input.ZipCode;
            this.IsInternational = input.IsInternational;
            if (this.IsInternational)
            {
                this.CountyInternational = input.County;
                this.CountryInternational = input.Country;
                this.StateInternational = input.State;
                this.StateObj = null;
                this.CountryObj = null;
                this.CountyObj = null;
            }
            else
            {
                this.CountryInternational = null;
                this.CountyInternational = null;
                this.StateInternational = null;
                this.CountyObj = county;
                this.StateObj = state;
                this.CountryObj = country;
            }
        }

        /// <summary>
        ///  The update address.
        /// </summary>
        /// <param name="addressTypeObj">
        ///  The address type.
        /// </param>
        /// <param name="address1">
        ///  The address 1.
        /// </param>
        /// <param name="address2">
        ///  The address 2.
        /// </param>
        /// <param name="city">
        ///  The city.
        /// </param>
        /// <param name="stateObj">
        ///  The state.
        /// </param>
        /// <param name="zipcode">
        ///  The zip code.
        /// </param>
        /// <param name="countryObj">
        ///  The country.
        /// </param>
        /// <param name="statusObj">
        ///  The status.
        /// </param>
        /// <param name="isMailingAddress">
        ///  The is mailing address.
        /// </param>
        /// <param name="isShowOnLeadPage">
        ///  The is show on lead page.
        /// </param>
        /// <param name="modUser">
        ///  The modification user.
        /// </param>
        /// <param name="leadObj">
        ///  The lead.
        /// </param>
        /// <param name="isInternational">
        ///  The is international.
        /// </param>
        /// <param name="stateInternational">
        ///  The state international.
        /// </param>
        /// <param name="countyObj">
        ///  The county.
        /// </param>
        /// <param name="countyInternational">
        ///  The county international.
        /// </param>
        /// <param name="countryInternational">
        ///  The country international.
        /// </param>
        public virtual void UpdateAddress(
            AddressType addressTypeObj,
            string address1,
            string address2,
            string city,
            SystemStates stateObj,
            string zipcode,
            Country countryObj,
            SyStatuses statusObj,
            bool isMailingAddress,
            bool isShowOnLeadPage,
            string modUser,
            Lead leadObj,
            bool isInternational,
            string stateInternational,
            County countyObj,
            string countyInternational,
            string countryInternational)
        {
            this.AddressType = addressTypeObj;
            this.Address1 = address1;
            this.Address2 = address2;
            this.City = city;
            this.StateObj = stateObj;
            this.ZipCode = zipcode;
            this.CountryObj = countryObj;
            this.StatusObj = statusObj;
            this.IsMailingAddress = isMailingAddress;
            this.IsShowOnLeadPage = isShowOnLeadPage;
            this.ModUser = modUser;
            this.LeadObj = leadObj;
            this.StateInternational = stateInternational;
            this.IsInternational = isInternational;
            this.CountyObj = countyObj;
            this.CountyInternational = countyInternational;
            this.CountryInternational = countryInternational;
            this.ModDate = DateTime.Now;
        }
        public virtual void UpdateAddress(LeadAddress newAddress, Lead lead)
        {
            this.AddressType = newAddress.AddressType;
            this.Address1 = newAddress.Address1;
            this.Address2 = newAddress.Address2;
            this.City = newAddress.City;
            this.StateObj = newAddress.StateObj;
            this.ZipCode = newAddress.ZipCode;
            this.CountryObj = newAddress.CountryObj;
            this.StatusObj = newAddress.StatusObj;
            ////this.IsMailingAddress = newAddress.IsMailingAddress;
            this.IsShowOnLeadPage = newAddress.IsShowOnLeadPage;
            this.ModUser = newAddress.ModUser;
            this.LeadObj = lead;
            this.StateInternational = newAddress.StateInternational;
            this.IsInternational = newAddress.IsInternational;
            this.CountyObj = newAddress.CountyObj;
            this.CountyInternational = newAddress.CountyInternational;
            this.CountryInternational = newAddress.CountryInternational;
            this.ModDate = DateTime.Now;
        }

        public virtual void UpdateIsShowInLeadPage(bool isshow)
        {
            this.IsShowOnLeadPage = isshow;
        }
    }
}
