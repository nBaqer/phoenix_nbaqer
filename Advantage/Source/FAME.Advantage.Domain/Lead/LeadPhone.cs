﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadPhone.cs" company="FAME Inc.">
//   FAME 2016
//   FAME.Advantage.Domain.Lead.LeadPhone    
// </copyright>
// <summary>
//   Domain for AdLeadPhone Table
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Lead
{
    using System;
    using SystemStuff;
    using Infrastructure.Entities;
    using Messages.Lead;

    /// <summary>
    /// Domain for AdLeadPhone Table
    /// </summary>
    public class LeadPhone : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadPhone"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="phone">
        /// The phone.
        /// </param>
        /// <param name="ext">
        /// The ext.
        /// </param>
        /// <param name="isForeignPhone">
        /// The is foreign phone.
        /// </param>
        /// <param name="position">
        /// The position.
        /// </param>
        /// <param name="modUser">
        /// The mod user.
        /// </param>
        /// <param name="modDate">
        /// The mod date.
        /// </param>
        /// <param name="phonetype">
        /// The phone type.
        /// </param>
        /// <param name="lead">
        /// The lead.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        public LeadPhone(Guid id, string phone, string ext, bool isForeignPhone, int position, string modUser, DateTime modDate, SyPhoneType phonetype, Lead lead, SyStatuses status)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.ID = id;
            this.Phone = phone;
            this.Extension = ext;
            this.IsForeignPhone = isForeignPhone;
            this.Position = position;
            this.ModUser = modUser;
            this.ModDate = modDate;
            this.NewLeadObj = lead;
            this.SyPhoneTypeObj = phonetype;
            this.Status = status;

            if (this.Position == 1)
            {
                this.IsBest = true;
            }
            else if (this.Position == 2 || this.Position == 3)
            {
                this.IsShowOnLeadPage = true;
            }

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadPhone"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="phone">
        /// The phone.
        /// </param>
        /// <param name="ext">
        /// The ext.
        /// </param>
        /// <param name="isForeignPhone">
        /// The is foreign phone.
        /// </param>
        /// <param name="position">
        /// The position.
        /// </param>
        /// <param name="modUser">
        /// The mod user.
        /// </param>
        /// <param name="modDate">
        /// The mod date.
        /// </param>
        /// <param name="phoneType">
        /// The phone type.
        /// </param>
        /// <param name="lead">
        /// The lead.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        /// <param name="isBest">
        /// The is best.
        /// </param>
        /// <param name="isShowOnLeadPage">
        /// The is show on lead page.
        /// </param>
        public LeadPhone(Guid id, string phone, string ext, bool isForeignPhone, int position, string modUser, DateTime modDate, SyPhoneType phoneType, Lead lead, SyStatuses status, bool isBest, bool isShowOnLeadPage)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.ID = id;
            this.Phone = phone;
            this.Extension = ext;
            this.IsForeignPhone = isForeignPhone;
            this.Position = position;
            this.ModUser = modUser;
            this.ModDate = modDate;
            this.NewLeadObj = lead;
            this.SyPhoneTypeObj = phoneType;
            this.IsBest = isBest;
            this.IsShowOnLeadPage = isShowOnLeadPage;
            this.Status = status;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadPhone"/> class.
        /// </summary>
        /// <param name="phone">
        /// The phone.
        /// </param>
        /// <param name="lead">
        /// The lead.
        /// </param>
        public LeadPhone(LeadPhone phone, Lead lead)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.ID = new Guid();
            this.Phone = phone.Phone;
            this.Extension = phone.Extension;
            this.IsForeignPhone = phone.IsForeignPhone;
            this.Position = phone.Position;
            this.ModUser = phone.ModUser;
            this.ModDate = phone.ModDate;
            this.NewLeadObj = lead;
            this.SyPhoneTypeObj = phone.SyPhoneTypeObj;
            this.IsBest = phone.IsBest;

            // this.IsShowOnLeadPage = phone.IsShowOnLeadPage;
            this.Status = phone.Status;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadPhone"/> class. 
        /// </summary>
        protected LeadPhone()
        {
        }

        /// <summary>
        /// The eLeadPhonePosition.
        /// </summary>
        public enum ELeadPhonePosition
        {
            /// <summary>
            /// The is best.
            /// </summary>
            IsBest = 1,

            /// <summary>
            /// The show on lead page.
            /// </summary>
            ShowOnLeadPage = 2,

            /// <summary>
            /// The show on lead page third.
            /// </summary>
            ShowOnLeadPageThird = 3
        }

        /// <summary>
        /// Gets or sets The phone number without format
        /// </summary>
        public virtual string Phone { get; protected set; }

        /// <summary>
        /// Gets or sets The extension part of the phone number
        /// </summary>
        public virtual string Extension { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is foreign phone.
        /// </summary>
        public virtual bool IsForeignPhone { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is best one to contact the lead
        /// </summary>
        public virtual bool IsBest { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is show on lead page.
        /// </summary>
        public virtual bool IsShowOnLeadPage { get; protected set; }

        //// public virtual bool IsBest { get; protected set; }

        //// public virtual bool IsShowOnLeadPage { get; protected set; }

        // public virtual StatusesOptions.StatusOption Status { get; protected set; }

        /// <summary>
        /// Gets or sets the position.
        /// The position of the phone in the form. This field is 
        /// for internal use. it classified the phones as 
        /// phone 1 phone 2 phone 3 etc
        /// </summary>
        public virtual int Position { get; protected set; }

        /// <summary>
        /// Gets or sets User that modified something here
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets Date of modification.
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets Lead that owned the phone record
        /// </summary>
        public virtual Lead NewLeadObj { get; protected set; }

        /// <summary>
        /// Gets or sets Phone Type Object
        /// </summary>
        public virtual SyPhoneType SyPhoneTypeObj { get; protected set; }

        /// <summary>
        /// Gets or sets The Status of the Lead Email
        /// </summary>
        public virtual SyStatuses Status { get; protected set; }

        /// <summary>
        /// The update phone.
        /// </summary>
        /// <param name="record">
        /// The record.
        /// </param>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="phoneType">
        /// The phone type.
        /// </param>
        public virtual void UpdatePhone(LeadPhone record, string username, SyPhoneType phoneType)
        {
            this.Phone = record.Phone;
            this.Extension = record.Extension;
            this.Position = record.Position;
            this.IsForeignPhone = record.IsForeignPhone;
            this.SyPhoneTypeObj = phoneType;
            this.ModDate = DateTime.Now;
            this.ModUser = username;
        }

        /// <summary>
        /// The update phone.
        /// </summary>
        /// <param name="record">
        /// The record.
        /// </param>
        /// <param name="position">
        /// The position.
        /// </param>
        /// <param name="lead">
        /// The lead.
        /// </param>
        /// <param name="phoneType">
        /// The phone type.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        public virtual void UpdatePhone(LeadContactInputModel record, int position, Lead lead, SyPhoneType phoneType, SyStatuses status)
        {
            this.Phone = record.Phone.Trim();
            this.Extension = record.Extension;
            this.Position = position;
            this.IsForeignPhone = record.IsForeignPhone;
            this.SyPhoneTypeObj = phoneType;
            this.ModDate = DateTime.Now;
            this.ModUser = record.UserName.Trim();
            this.IsBest = record.IsBest;
            this.IsShowOnLeadPage = record.ShowOnLeadPage;
            this.NewLeadObj = lead;
            this.Status = status;
        }

        /// <summary>
        /// The update phone.
        /// </summary>
        /// <param name="record">
        /// The record.
        /// </param>
        /// <param name="modUser">
        /// The mod user.
        /// </param>
        /// <param name="phoneType">
        /// The phone type.
        /// </param>
        public virtual void UpdatePhone(LeadPhonesOutputModel record, string modUser, SyPhoneType phoneType)
        {
            this.Phone = record.Phone.Trim();
            this.Extension = record.Extension.Trim();
            this.IsForeignPhone = record.IsForeignPhone;
            this.SyPhoneTypeObj = phoneType;
            this.ModDate = DateTime.Now;
            this.ModUser = modUser;
        }

        /// <summary>
        /// The update phone.
        /// </summary>
        /// <param name="phone">
        /// The phone.
        /// </param>
        /// <param name="ext">
        /// The ext.
        /// </param>
        /// <param name="modUser">
        /// The mod user.
        /// </param>
        public virtual void UpdatePhone(string phone, string ext, string modUser)
        {
            this.Phone = phone;
            this.Extension = ext;
            this.ModUser = modUser;
            this.ModDate = DateTime.Now;
        }

        /// <summary>
        /// The update position.
        /// </summary>
        /// <param name="position">
        /// The position.
        /// </param>
        public virtual void UpdatePosition(int position)
        {
            this.Position = position;
        }

        /// <summary>
        ///  Update modification date and user in
        /// this LeadEmail
        /// </summary>
        /// <param name="modDate">
        ///  The modification date.
        /// </param>
        /// <param name="username">
        /// The user-name.
        /// </param>
        public virtual void UpdateModDateAndUser(DateTime modDate, string username)
        {
            this.ModDate = modDate;
            this.ModUser = username;
        }
    }
}
