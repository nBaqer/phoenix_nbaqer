﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlJobStatus.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Domain for Table PlJobStatus
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Lead
{
    using System;
    using SystemStuff;
    using Campuses.CampusGroups;
    using Infrastructure.Entities;

    /// <summary>
    /// Domain for Table PlJobStatus
    /// </summary>
    public class PlJobStatus : DomainEntity
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PlJobStatus"/> class.
        /// Constructor use this to create the object.
        /// </summary>
        /// <param name="jobStatusDescrip">
        /// The job Status Description.
        /// </param>
        /// <param name="statusObj">
        /// The status Object.
        /// </param>
        /// <param name="jobCode">
        /// The job Code.
        /// </param>
        /// <param name="modUser">
        /// The modification User.
        /// </param>
        /// <param name="modDate">
        /// The modification Date.
        /// </param>
        /// <param name="campGrpObj">
        /// The campus Group Object.
        /// </param>
        public PlJobStatus(
               string jobStatusDescrip,
               SyStatuses statusObj, 
               string jobCode, 
               string modUser, 
               DateTime modDate, 
               CampusGroup campGrpObj)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.JobStatusDescrip = jobStatusDescrip;
            this.StatusObj = statusObj;
            this.JobCode = jobCode;
            this.ModUser = modUser;
            this.ModDate = modDate;
            this.CampGrpObj = campGrpObj;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlJobStatus"/> class.
        /// Protected Constructor.
        /// </summary>
        protected PlJobStatus()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the job Status Description
        /// </summary>
        public virtual string JobStatusDescrip { get; protected set; }

        /// <summary>
        /// Gets or sets Status object
        /// </summary>
        public virtual SyStatuses StatusObj { get; protected set; }

        /// <summary>
        /// Gets or sets Job Code
        /// </summary>
        public virtual string JobCode { get; protected set; }

        /// <summary>
        /// Gets or sets Modification User
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets Modification Date
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets Campus Group Object
        /// </summary>
        public virtual CampusGroup CampGrpObj { get; protected set; }

        #endregion
    }
}