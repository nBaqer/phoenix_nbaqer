﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PriorWorkContact.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Domain for Table PriorWorkContact
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Lead
{
    using System;
    using Catalogs;
    using Infrastructure.Entities;

    /// <summary>
    /// Domain for Table PriorWorkContact
    /// </summary>
    public class PriorWorkContact : DomainEntityWithTypedID<int>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PriorWorkContact"/> class.
        /// Constructor use this to create the object.
        /// </summary>
        /// <param name="employmentObj">
        /// The employment Object.
        /// </param>
        /// <param name="jobTitle">
        /// The job Title.
        /// </param>
        /// <param name="firstName">
        /// The first Name.
        /// </param>
        /// <param name="middleName">
        /// The middle Name.
        /// </param>
        /// <param name="lastName">
        /// The last Name.
        /// </param>
        /// <param name="prefixObj">
        /// The prefix Object.
        /// </param>
        /// <param name="isPhoneInternational">
        /// True if the PHone is international
        /// </param>
        /// <param name="phone">
        /// The phone.
        /// </param>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="isActive">
        /// The is Active.
        /// </param>
        /// <param name="modUser">
        /// The modification User.
        /// </param>
        /// <param name="modDate">
        /// The modification Date.
        /// </param>
        /// <param name="isDefault">
        /// The is Default.
        /// </param>
        public PriorWorkContact(
             AdLeadEmployment employmentObj,
             string jobTitle,
             string firstName,
             string middleName,
             string lastName,
             Prefixes prefixObj,
             bool isPhoneInternational,
             string phone,
             string email,
             int isActive,
             string modUser,
             DateTime modDate,
             int isDefault)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.StEmploymentObj = employmentObj;
            this.JobTitle = jobTitle;
            this.FirstName = firstName;
            this.MiddleName = middleName ?? string.Empty;
            this.LastName = lastName;
            this.PrefixObj = prefixObj;
            this.IsPhoneInternational = isPhoneInternational;
            this.Phone = phone ?? string.Empty;
            this.Email = email ?? string.Empty;
            this.IsActive = isActive;
            this.ModUser = modUser;
            this.ModDate = modDate;
            this.IsDefault = isDefault;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PriorWorkContact"/> class.
        /// Protected Constructor.
        /// </summary>
        protected PriorWorkContact()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual AdLeadEmployment StEmploymentObj { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string JobTitle { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string FirstName { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string MiddleName { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string LastName { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual Prefixes PrefixObj { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is phone international.
        /// </summary>
        public virtual bool IsPhoneInternational { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string Phone { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string Email { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual int IsActive { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual int IsDefault { get; protected set; }

        #endregion

        /// <summary>
        /// The update contact.
        /// </summary>
        /// <param name="employmentObj">
        /// The employment object.
        /// </param>
        /// <param name="jobTitle">
        /// The job title.
        /// </param>
        /// <param name="firstName">
        /// The first name.
        /// </param>
        /// <param name="middleName">
        /// The middle name.
        /// </param>
        /// <param name="lastName">
        /// The last name.
        /// </param>
        /// <param name="prefixObj">
        /// The prefix object.
        /// </param>
        /// <param name="isPhoneInternational">
        /// The is Phone International.
        /// </param>
        /// <param name="phone">
        /// The phone.
        /// </param>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="isActive">
        /// The is active.
        /// </param>
        /// <param name="modUser">
        /// The mod user.
        /// </param>
        /// <param name="modDate">
        /// The mod date.
        /// </param>
        /// <param name="isDefault">
        /// The is default.
        /// </param>
        public virtual void UpdateContact(
              AdLeadEmployment employmentObj,
             string jobTitle,
             string firstName,
             string middleName,
             string lastName,
             Prefixes prefixObj,
             bool isPhoneInternational,
             string phone,
             string email,
             int isActive,
             string modUser,
             DateTime modDate,
             int isDefault)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.StEmploymentObj = employmentObj;
            this.JobTitle = jobTitle;
            this.FirstName = firstName;
            this.MiddleName = middleName ?? string.Empty;
            this.LastName = lastName;
            this.PrefixObj = prefixObj;
            this.IsPhoneInternational = isPhoneInternational;
            this.Phone = phone ?? string.Empty;
            this.Email = email ?? string.Empty;
            this.IsActive = isActive;
            this.ModUser = modUser;
            this.ModDate = modDate;
            this.IsDefault = isDefault;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }
    }
}
