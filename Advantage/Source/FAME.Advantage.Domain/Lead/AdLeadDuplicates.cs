﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdLeadDuplicates.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Domain for adLeadDuplicates
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Lead
{
    using Infrastructure.Entities;

    /// <summary>
    /// Domain for adLeadDuplicates
    /// </summary>
    public class AdLeadDuplicates : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Gets or sets New Lead from Vendor
        /// </summary>
        public virtual Lead NewLeadObj { get; protected set; }

        /// <summary>
        ///  Gets or sets Lead possible duplicate, from vendor or from advantage
        /// </summary>
        public virtual Lead PosibleLeadDuplicateObj { get; protected set; }
    }
}
