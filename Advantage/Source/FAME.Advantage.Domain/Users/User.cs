﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="User.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The Advantage User
// </summary>
// --------------------------------------------------------------------------------------------------------------------
using FAME.Advantage.Domain.Lead;

namespace FAME.Advantage.Domain.Users
{
    using System.Collections.Generic;

    using FAME.Advantage.Domain.Campuses.CampusGroups;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    
    using FAME.Advantage.Domain.MostRecentlyUsed;

    /// <summary>
    /// The Advantage User
    /// </summary>
    public class User : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        public User()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="accountActive">
        /// The account active.
        /// </param>
        /// <param name="fullName">
        /// The full name.
        /// </param>
        public User(string userName, string email, bool? accountActive, string fullName)
        {
            this.UserName = userName;
            this.Email = email;
            this.AccountActive = accountActive;
            this.FullName = fullName;
        }


        /// <summary>
        /// Gets or sets User name
        /// </summary>
        public virtual string UserName { get; protected set; }

        /// <summary>
        /// Gets or sets Email
        /// </summary>
        public virtual string Email { get; protected set; }

        /// <summary>
        /// Gets or sets List of campus group that the user belong.
        /// </summary>
        public virtual IList<CampusGroup> CampusGroup { get; protected set; } // Many to many with CampGrps

        /// <summary>
        /// Gets or sets If the account is active or not
        /// </summary>
        public virtual bool? AccountActive { get; set; }

        /// <summary>
        /// Gets or sets The full name of the user
        /// </summary>
        public virtual string FullName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether True is is one of the following users sa, Super, Support users
        /// </summary>
        public virtual bool IsAdvantageSuperUser { get; protected set; }

        /// <summary>
        /// Gets or sets the active lead count.
        /// </summary>
        public virtual int? ActiveLeadCount { get; set; }

        /// <summary>
        /// Gets or setsThe list of lead for this user is admission rep
        /// </summary>
        public virtual IList<Lead.Lead> AdmissionRepLeadList { get; set; }

        /// <summary>
        /// Gets or setsList of Admission rep associated with the campus ID.
        /// </summary>
        public virtual IList<AdmissionRepByCampusView> AdmissionRepList { get; protected set; }

        /// <summary>
        /// Gets or sets the list of roles associates with campus groups.
        /// </summary>
        public virtual IList<UserRoles.UserRolesCampusGroupBridge> RolesByCampusList { get; protected set; }

        /// <summary>
        /// Gets or setsList of MRU typo Lead for this User
        /// </summary>
        public virtual IList<ViewLeadMru> LeadMruList { get; protected set; }

        /// <summary>
        /// Gets the user and name.
        /// </summary>
        public virtual string UserAndName
        {
            get
            {
                return this.FullName + " (" + this.UserName + ")";
            }
        }

        /// <summary>
        /// Gets or sets the user type.
        /// </summary>
        public virtual UserType UserType { get; protected set; }
    }
}
