﻿using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FAME.Advantage.Domain.StatusesOptions;

namespace FAME.Advantage.Domain.Users.UserRoles
{
    public class SystemRoles : DomainEntityWithTypedID<int>
    {
        public SystemRoles()
        {
        }

        public SystemRoles(string description, StatusOption status, DateTime modDate, string modUser, Roles roles)
        {
            this.Description = description;
            this.Status = status;
            this.ModUser = modUser;
            this.ModDate = modDate;
           

        }

        public virtual string Description { get; protected set; }
        public virtual StatusOption Status { get; set; }
        public virtual DateTime? ModDate { get; set; }
        public virtual string ModUser { get; set; }

        public virtual IList<Roles> Roles { get; set; }

        public virtual bool IsFinancialAidEnabled()
        {
            switch (this.ID)
            {
                case (int)UserRolEnums.SystemRoles.SystemAdministrator:
                    return true;
                case (int)UserRolEnums.SystemRoles.DirectorOfFinancialAid:
                case (int)UserRolEnums.SystemRoles.FinancialAidAdvisor:
                    return true;
                default:
                    return false;
            }
        }
    }
}
