﻿using System;

using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff.SchoolDefinedFields;

namespace FAME.Advantage.Domain.Users.UserRoles
{
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Domain.SystemStuff.Resources;

    /// <summary>
    /// Resources domain for table syRespources
    /// </summary>
    public class Resources : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// PRotected basic constructor
        /// </summary>
        protected Resources()
        {
        }

        /// <summary>
        /// Resource constructor. Use this
        /// </summary>
        /// <param name="resource"></param>
        /// <param name="resourceUrl"></param>
        /// <param name="displayName"></param>
        /// <param name="modUser"></param>
        /// <param name="modDate"></param>
        public Resources(string resource, string resourceUrl, string displayName, string modUser, DateTime? modDate)
        {
            this.Resource = resource;
            this.ResourceUrl = resourceUrl;
            this.DisplayName = displayName;
            this.ModUser = modUser;
            this.ModDate = modDate;

        }

        /// <summary>
        /// Resource name
        /// </summary>
        public virtual string Resource { get; protected set; }

        /// <summary>
        /// Resource URL
        /// </summary>
        public virtual string ResourceUrl { get; protected set; }

        /// <summary>
        /// Resource display name
        /// </summary>
        public virtual string DisplayName { get; protected set; }

        /// <summary>
        /// Access Level
        /// </summary>
        public virtual long AccessLevel { get; protected set; }

        /// <summary>
        /// ??
        /// </summary>
        public virtual bool? AllowSchlReqFields { get; protected set; }
        
        /// <summary>
        /// Date of modification
        /// </summary>
        public virtual DateTime? ModDate { get; set; }

        /// <summary>
        /// Modification user
        /// </summary>
        public virtual string ModUser { get; set; }

        /// <summary>
        /// The type of resource
        /// </summary>
        public virtual ResourceTypes ResourceTypeObj { get; protected set; }

        /// <summary>
        /// Level of resources object
        /// </summary>
        public virtual IList<ResourceLevels> ResourceLevels { get; set; }

        /// <summary>
        /// List of resources Tables related to resource.
        /// </summary>
        public virtual IList<ResourceTableFields> ResourceTableFieldsList { get; protected set; }

        /// <summary>
        /// List of Resources associated with UDF
        /// </summary>
        public virtual IList<SyAdvantageResourceRelations> ResourceUdfList { get; protected set; }

        /// <summary>
        /// List of resources associated with the UDF
        /// </summary>
        public virtual IList<SyAdvantageResourceRelations> ResourceRelatedUdfList { get; protected set; }
        
    }
}
