﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FAME.Advantage.Domain.Users.UserRoles
{
    public class UserRolEnums
    {
        public enum SystemRoles
        {
            SystemAdministrator = 1,
            Instructors = 2,
            AdmissionReps = 3,
            AcademicAdvisors = 4,
            Other = 5,
            PlacementReps = 6,
            FinancialAidAdvisor = 7,
            DirectorOfAdmissions = 8,
            DirectorOfFinancialAid = 9,
            DirectorOfBusinessOffice=10,
            INSTRUCTORSSupervisor=11,
            FrontDesk=12,
            DirectorOfAcademics=13,
            ReportAdministrator=14
        }
    }
}




