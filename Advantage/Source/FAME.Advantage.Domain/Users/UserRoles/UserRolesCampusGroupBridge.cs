﻿using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using System;

namespace FAME.Advantage.Domain.Users.UserRoles
{
    public class UserRolesCampusGroupBridge : DomainEntity
    {
        public UserRolesCampusGroupBridge()
        {
        }

        public UserRolesCampusGroupBridge(User user, Roles role, CampusGroup campusGroup, DateTime modDate, string modUser)
        {
// ReSharper disable DoNotCallOverridableMethodsInConstructor
            User = user;
            Role = role;
            CampusGroup = campusGroup;
            ModUser = modUser;
            ModDate = modDate;
// ReSharper restore DoNotCallOverridableMethodsInConstructor

        }


            public virtual User User { get; set; }
            public virtual Roles Role { get; set; }
            public virtual CampusGroup CampusGroup { get; set; }
            public virtual DateTime? ModDate { get; set; }
            public virtual string ModUser { get; set; }

    }
}
