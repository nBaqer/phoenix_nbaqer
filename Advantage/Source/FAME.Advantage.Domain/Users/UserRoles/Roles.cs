﻿using FAME.Advantage.Domain.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Lead;
using FAME.Advantage.Domain.StatusesOptions;

namespace FAME.Advantage.Domain.Users.UserRoles
{
    using System.Linq;

    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Domain.SystemStuff.Resources;

    /// <summary>
    /// referred to table syRoles
    /// </summary>
    public class Roles : DomainEntity
    {
        /// <summary>
        /// Protected Constructor
        /// </summary>
        public Roles()
        {
        }

        /// <summary>
        /// Constructor to be use to instantiate the class
        /// </summary>
        /// <param name="role"></param>
        /// <param name="code"></param>
        /// <param name="status"></param>
        /// <param name="modDate"></param>
        /// <param name="modUser"></param>
        public Roles(string role, string code, StatusOption status, DateTime modDate, string modUser)
        {
// ReSharper disable DoNotCallOverridableMethodsInConstructor
            Role = role;
            Code = code;
            Status = status;
            ModUser = modUser;
            ModDate = modDate;
// ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Role name
        /// </summary>
        public virtual string Role { get; protected set; }
        
        /// <summary>
        /// Role Code
        /// </summary>
        public virtual string Code { get; protected set; }
        
        /// <summary>
        /// Status of the role
        /// </summary>
        public virtual StatusOption Status { get; set; }
        
        /// <summary>
        /// Date of modification record
        /// </summary>
        public virtual DateTime? ModDate { get; set; }
        
        /// <summary>
        /// User that modify the record
        /// </summary>
        public virtual string ModUser { get; set; }
        
        /// <summary>
        /// System role that is linked this user role
        /// </summary>
        public virtual SystemRoles SystemRole { get; set; }

        /// <summary>
        /// Resource Level
        /// </summary>
        public virtual IList<ResourceLevels> ResourceLevels { get; protected set; }

        /// <summary>
        /// Get the list of user and campus groups associated with this role.
        /// </summary>
        public virtual IList<UserRolesCampusGroupBridge> UserByCampusGroupList { get; protected set; }

        /// <summary>
        /// List of Lead status change permission allowed for the Role.
        /// </summary>
        public virtual IList<LeadWorkflowStatusChangesPermissions> LeadStatusChangePermissionList { get; protected set; }

        public virtual IEnumerable<User> Users
        {
            get
            {
                return this.UserByCampusGroupList.Select(x => x.User).Distinct();
            }
        }

        /// <summary>
        /// Gets or sets the role modules bridge.
        /// </summary>
        public virtual IEnumerable<RolesModulesBridge> RoleModulesBridge { get; protected set; }

        /// <summary>
        /// Gets the modules.
        /// </summary>
        public virtual IEnumerable<Resources> Resources
        {
            get
            {
                return this.RoleModulesBridge.Select(x => x.Resource).Distinct();
            }
        }

    }
}