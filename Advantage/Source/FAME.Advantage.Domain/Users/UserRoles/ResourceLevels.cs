﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Users.UserRoles
{
    public class ResourceLevels : DomainEntity
    {
        public ResourceLevels()
        {
        }

        public ResourceLevels(Guid roleId, long? accessLevel, Resources resource, DateTime modDate, string modUser, int? parentId)
        {
            this.RoleId = roleId;
            this.AccessLevel = accessLevel;
            
            this.ModUser = modUser;
            this.ModDate = modDate;
            this.ParentId = parentId;

        }

        public virtual Guid RoleId { get; protected set; }
        public virtual long? AccessLevel { get; protected set; }
        public virtual long ResourceId { get; set; }
        public virtual DateTime? ModDate { get; set; }
        public virtual string ModUser { get; set; }
        public virtual int? ParentId { get; set; }

       public virtual Resources Resource { get; set; }
       

             

    }   
}
