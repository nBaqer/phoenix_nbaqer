﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserType.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the UserType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Users
{
    using System;

    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The user type.
    /// </summary>
    public class UserType : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// The vendor user type code.
        /// </summary>
        public const string VendorUserTypeCode = "Vendor";

        /// <summary>
        /// The advantage user type code.
        /// </summary>
        public const string AdvantageUserTypeCode = "Advantage";

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public virtual int Id { get; protected set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public virtual DateTime ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public virtual string ModifiedUser { get; set; }
    }
}
