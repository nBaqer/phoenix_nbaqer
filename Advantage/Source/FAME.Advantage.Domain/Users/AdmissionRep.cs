﻿using System;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using System.Collections.Generic;
using FAME.Advantage.Domain.StatusesOptions;

namespace FAME.Advantage.Domain.Users
{
    public class AdmissionRep : DomainEntity
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public AdmissionRep()
        {
        }

        


        //Class Properties
        public virtual string Description { get; protected set; }
        public virtual StatusOption Status { get; protected set; }
        public virtual Guid AdmissionsRepId { get; set; }
       

    }
}
