﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Domain.Enumerations
{
    public class Enumerations
    {
        public enum eSystemStatus
        {
            NewLead = 1,

            ApplicationReceived = 2,

            ApplicationNotAccepted = 3,

            InterviewScheduled = 4,

            Interviewed = 5,

            Enrolled = 6,

            FutureStart = 7,

            NoStart = 8,

            CurrentlyAttending = 9,

            LeaveOfAbsence = 10,

            Suspension = 11,

            Dropped = 12,

            Graduated = 14,

            Active = 15,

            InActive = 16,

            DeadLead = 17,

            WillEnrollIntheFuture = 18,

            TransferOut = 19,

            AcademicProbationSap = 20,

            Externship = 22,

            DisciplinaryProbation = 23,

            WarningProbation = 24,
        };

        public enum eLeadSystemStatus
        {
            NewLead = 1,

            ApplicationReceived = 2,

            ApplicationNotAccepted = 3,

            InterviewScheduled = 4,

            Interviewed = 5,

            Enrolled = 6,

            Active = 15,

            InActive = 16,

            DeadLead = 17,

            WillEnrollIntheFuture = 18
        };

        public enum eEnrollmentSystemStatus
        {
            FutureStart = 7,

            NoStart = 8,

            CurrentlyAttending = 9,

            LeaveOfAbsence = 10,

            Suspension = 11,

            Dropped = 12,

            Graduated = 14,

            TransferOut = 19,

            AcademicProbationSap = 20,

            Externship = 22,

            DisciplinaryProbation = 23,

            WarningProbation = 24
        };

        /// <summary>
        /// The enum for institution import type.
        /// </summary>
        public enum EInstitutionImportType
        {
            /// <summary>
            /// The school defined.
            /// </summary>
            SchoolDefined = 1,

            /// <summary>
            /// The imported.
            /// </summary>
            Imported = 2,

            /// <summary>
            /// The new.
            /// </summary>
            New = 3
        }

        /// <summary>
        /// The enum for AD level.
        /// </summary>
        public enum ELevel
        {
            /// <summary>
            /// The college.
            /// </summary>
            College = 1,

            /// <summary>
            /// The h school.
            /// </summary>
            HSchool = 2,

            /// <summary>
            /// The life skill.
            /// </summary>
            LifeSkill = 3,

            /// <summary>
            /// The vo tech.
            /// </summary>
            VoTech = 4
        }

        /// <summary>
        /// The e institution type.
        /// </summary>
        public enum EInstitutionType
        {
            /// <summary>
            /// The private.
            /// </summary>
            Private = 1,

            /// <summary>
            /// The public.
            /// </summary>
            Public = 2
        }

        /// <summary>
        /// The System Status (Active, Inactive) enumeration
        /// </summary>
        public enum EStatus
        {
            /// <summary>
            /// Active = 1
            /// </summary>
            Active = 1,

            /// <summary>
            /// Inactive = 2
            /// </summary>
            Inactive = 2
        }

        /// <summary>
        /// Gets the String Constant from the Status
        /// </summary>
        /// <param name="status"> The current status to return the string. </param>
        /// <returns>A String based on the Status</returns>
        public static string GetStatus(EStatus status)
        {
            return status == EStatus.Active ? "Active" : "Inactive";
        }
    }

}
