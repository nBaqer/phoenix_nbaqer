﻿using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.StatusesOptions;

namespace FAME.Advantage.Domain.SystemStuff
{
    public class SystemStatus : DomainEntityWithTypedID<int>
    {
        protected SystemStatus()
        {
        }

        public SystemStatus( string descr, int? levelId, bool inSchool, string geProgramStatus)
        {
            Description = descr;
            StatusLevelId = levelId;
            IsInSchool = inSchool;
            GEProgramStatus = geProgramStatus;
        }

        
        public virtual string Description { get; protected set; }        
        public virtual int? StatusLevelId { get; protected set; }
        public virtual bool? IsInSchool { get; protected set; }
        public virtual string GEProgramStatus { get; protected set; }
        

        public virtual StatusOption Status { get; protected set; }
        
    }
}



