﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.System.Menu;

namespace FAME.Advantage.Domain.System.ConfigurationAppSettings
{
    public class ConfigurationAppSettingValues : DomainEntity
    {
        protected ConfigurationAppSettingValues()
        {
        }

        public ConfigurationAppSettingValues( int? settingId, Campus camp, bool active, IList<MenuItem> mItem)
        {
            SettingId = settingId;
            Campus = camp;
            Active = active;
           // MenuItemsToExclude = mItem;
        }

        public virtual int? SettingId { get; set; }
        public virtual Campus Campus { get; set; }
        public virtual string Value { get; set; }
        public virtual bool Active { get; set; }
        public virtual IList<MenuItem> MenuItemsToExclude { get; set; }

    }
}
