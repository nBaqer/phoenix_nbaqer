﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatusChangeDeleteReasons.cs" company="FAME Inc.">
//   FAME.Advantage.Domain.SystemStuff
// </copyright>
// <summary>
//   The status change delete reasons.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff
{
    using System;

    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The status change delete reasons.
    /// </summary>
    public class StatusChangeDeleteReasons : DomainEntity
    {
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public virtual DateTime? ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public virtual string ModUser { get; protected set; }
    }
}
