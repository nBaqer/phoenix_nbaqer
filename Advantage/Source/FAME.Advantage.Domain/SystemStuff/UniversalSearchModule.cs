﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UniversalSearchModule.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.SystemStuff.UniversalSearchModule
// </copyright>
// <summary>
//   The universal search modules.
//   Domain Entity Class for table <code>syUniversalSearchModules</code> which hold the list of items that are searchable from the Universal Search drop down.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.SystemStuff.Resources;

    /// <summary>
    /// The universal search modules.
    /// Domain Entity Class for table <code>syUniversalSearchModules</code> which hold the list of items that are searchable from the Universal Search drop down.
    /// </summary>
    public class UniversalSearchModule : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UniversalSearchModule"/> class.
        /// </summary>
        public UniversalSearchModule()
        {
        }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public virtual DateTime ModifiedDate { get; protected set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public virtual string ModifiedUser { get; protected set; }

        /// <summary>
        /// Gets or sets the module.
        /// </summary>
        public virtual Users.UserRoles.Resources Resource { get; protected set; }
    }
}
