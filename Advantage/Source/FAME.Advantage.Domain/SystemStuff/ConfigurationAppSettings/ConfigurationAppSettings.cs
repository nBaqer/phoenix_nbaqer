﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurationAppSettings.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the ConfigurationAppSetting type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff.ConfigurationAppSettings
{
    using System.Collections.Generic;

    using Infrastructure.Entities;

    /// <summary>
    /// The configuration application setting.
    /// </summary>
    public class ConfigurationAppSetting : DomainEntityWithTypedID<int>
    {
         /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationAppSetting"/> class.
        /// </summary>
        /// <param name="keyName">
        /// The key name.
        /// </param>
        /// <param name="descr">
        /// The description.
        /// </param>
        /// <param name="campusSpec">
        /// The campus spec.
        /// </param>
        /// <param name="extraConf">
        /// The extra configuration.
        /// </param>
        /// <param name="appSettingValues">
        /// The application setting values.
        /// </param>
        public ConfigurationAppSetting(string keyName, string descr, bool campusSpec, bool extraConf, IList<ConfigurationAppSettingValues> appSettingValues)
        {
            // ReSharper disable VirtualMemberCallInConstructor
            this.KeyName = keyName;
            this.Description = descr;
            this.CampusSpecific = campusSpec;
            this.ExtraConfirmation = extraConf;
            this.ConfigurationValues = appSettingValues;   
            // ReSharper restore VirtualMemberCallInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationAppSetting"/> class.
        /// </summary>
        protected ConfigurationAppSetting()
        {
        }

        /// <summary>
        /// Gets or sets the key name.
        /// </summary>
        public virtual string KeyName { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether campus specific.
        /// </summary>
        public virtual bool CampusSpecific { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether extra confirmation.
        /// </summary>
        public virtual bool ExtraConfirmation { get; set; }

        /// <summary>
        /// Gets or sets the configuration values.
        /// </summary>
        public virtual IList<ConfigurationAppSettingValues> ConfigurationValues { get; set; }

    }
}
