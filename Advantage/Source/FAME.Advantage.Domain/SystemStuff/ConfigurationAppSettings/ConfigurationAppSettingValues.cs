﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurationAppSettingValues.cs" company="FAME Inc.">
//   FAME Inc. 2014
//   FAME.Advantage.Domain.SystemStuff.ConfigurationAppSettings.ConfigurationAppSettingValues
// </copyright>
// <summary>
//   The configuration app setting values.
//   Domain Entity for table <code>syConfigAppSetValues</code>.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff.ConfigurationAppSettings
{
    using System.Collections.Generic;

    using FAME.Advantage.Domain.Campuses;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.SystemStuff.Menu;

    /// <summary>
    /// The configuration app setting values.
    /// Domain Entity for table <code>syConfigAppSetValues</code>.
    /// </summary>
    public class ConfigurationAppSettingValues : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationAppSettingValues"/> class.
        /// </summary>
        /// <param name="configurationAppSetting">
        /// The configuration app setting.
        /// </param>
        /// <param name="campus">
        /// The campus.
        /// </param>
        /// <param name="active">
        /// The active.
        /// </param>
        /// <param name="menuItems">
        /// The menu items.
        /// </param>
        public ConfigurationAppSettingValues(ConfigurationAppSetting configurationAppSetting, Campus campus, bool active, IList<MenuItem> menuItems)
        {
            this.ConfigurationAppSetting = configurationAppSetting;
            this.Campus = campus;
            this.Active = active;
           // MenuItemsToExclude = mItem;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationAppSettingValues"/> class.
        /// </summary>
        protected ConfigurationAppSettingValues()
        {
        }

        /// <summary>
        /// Gets the setting id.
        /// </summary>
        public virtual int? SettingId
        {
            get
            {
                return this.ConfigurationAppSetting != null ? this.ConfigurationAppSetting.ID : 0;
            }
        }

        /// <summary>
        /// Gets or sets the campus.
        /// </summary>
        public virtual Campus Campus { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public virtual string Value { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether active.
        /// </summary>
        public virtual bool Active { get; set; }

        /// <summary>
        /// Gets or sets the menu items to exclude.
        /// </summary>
        public virtual IList<MenuItem> MenuItemsToExclude { get; set; }

        /// <summary>
        /// Gets or sets the configuration app setting.
        /// </summary>
        public virtual ConfigurationAppSetting ConfigurationAppSetting { get; set; }
    }
}
