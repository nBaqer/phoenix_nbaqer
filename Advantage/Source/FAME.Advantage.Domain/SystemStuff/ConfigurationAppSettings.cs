﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.System.ConfigurationAppSettings
{
    public class ConfigurationAppSetting : DomainEntity
    {
        protected ConfigurationAppSetting()
        {
        }

        public ConfigurationAppSetting(string keyName, string descr, bool campusSpec, bool extraConf, IList<ConfigurationAppSettingValues> appSettingValues )
        {
            KeyName = keyName;
            Description = descr;
            CampusSpecific = campusSpec;
            ExtraConfirmation = extraConf;
            ConfigurationValues = appSettingValues;
        }

        public virtual string KeyName { get; set; }
        public virtual string Description { get; set; }
        public virtual bool CampusSpecific { get; set; }
        public virtual bool ExtraConfirmation { get; set; }
        public virtual IList<ConfigurationAppSettingValues> ConfigurationValues { get; set; }

    }
}
