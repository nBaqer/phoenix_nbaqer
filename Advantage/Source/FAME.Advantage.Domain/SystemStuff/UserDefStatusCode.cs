﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserDefStatusCode.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Domain for Table syStatusCode
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.SystemStuff
{
    using System;
    using System.Collections.Generic;
    using Campuses.CampusGroups;
    using Infrastructure.Entities;
    using Lead;
    using StatusesOptions;

    /// <summary>
    /// Domain for Table <code>syStatusCode</code>
    /// </summary>
    public class UserDefStatusCode : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserDefStatusCode"/> class.
        /// </summary>
        /// <param name="statusCode">
        ///  The status code.
        /// </param>
        /// <param name="descr">
        ///  The description.
        /// </param>
        /// <param name="campGrpId">
        ///  The camp group id.
        /// </param>
        /// <param name="syStatusId">
        ///  The system status id.
        /// </param>
        /// <param name="isAcacProb">
        ///  The is academic probation.
        /// </param>
        /// <param name="disProb">
        ///  The disciplinary probation.
        /// </param>
        /// <param name="isDefaultStatus">
        /// The is default status.
        /// </param>
        public UserDefStatusCode(
            string statusCode, 
            string descr, 
            Guid campGrpId, 
            int? syStatusId, 
            bool isAcacProb, 
            bool disProb, 
            bool isDefaultStatus)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.StatusCode = statusCode;

            this.Description = descr;
            this.CampGrpId = campGrpId;
            this.SystemStatusId = syStatusId;
            this.IsAcademicProbation = isAcacProb;
            this.DiscloseProbation = disProb;
            this.IsDefaultLeadStatus = isDefaultStatus;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor       
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDefStatusCode"/> class. 
        /// protect the default constructor
        /// </summary>
        protected UserDefStatusCode()
        {
        }

        /// <summary>
        /// Gets or sets User defined status code
        /// </summary>
        public virtual string StatusCode { get; protected set; }

        /// <summary>
        ///  Gets or sets User Description of the created status
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        ///  Gets or sets User Campus Group where is defined the status
        /// </summary>
        public virtual Guid CampGrpId { get; protected set; }

        /// <summary>
        ///  Gets or sets User System status where is referred this status
        /// All user status should be referred to one system status 
        /// </summary>
        public virtual int? SystemStatusId { get; protected set; }

        /// <summary>
        ///  Gets or sets User Academic Probation
        /// </summary>
        public virtual bool? IsAcademicProbation { get; protected set; }

        /// <summary>
        ///  Gets or sets User Disclosure Probation
        /// </summary>
        public virtual bool? DiscloseProbation { get; protected set; }

        /// <summary>
        ///  Gets or sets User It is default lead status
        /// </summary>
        public virtual bool? IsDefaultLeadStatus { get; protected set; }

        /// <summary>
        ///  Gets or sets User It is active or inactive
        /// </summary>
        public virtual StatusOption Status { get; protected set; }

        /// <summary>
        ///  Gets or sets User Campus Group where is defined the status
        /// </summary>
        public virtual CampusGroup CampusGroup { get; protected set; }

        /// <summary>
        ///  Gets or sets User System status where is referred this status
        /// All user status should be referred to one system status
        /// </summary>
        public virtual SystemStatus SystemStatus { get; protected set; }

        /// <summary>
        ///  Gets or sets User List of Original changes column in <code>syLeadStatusesChanges</code> 
        /// </summary>
        public virtual IList<LeadStatusesChanges> LeadStatusesOriginalChangesList { get; protected set; }

        /// <summary>
        ///  Gets or sets User List of new changes column in <code>syLeadStatusesChanges</code> 
        /// </summary>
        public virtual IList<LeadStatusesChanges> LeadStatusesNewChangesList { get; protected set; }

        /// <summary>
        ///  Gets or sets User List of Original changes column in <code>syLeadStatusChanges</code> 
        /// </summary>
        public virtual IList<LeadWorkflowStatusChanges> LeadWorkflowOriginalChangeList { get; protected set; }

        /// <summary>
        ///  Gets or sets User List of new changes column in <code>syLeadStatusChanges</code> 
        /// </summary>
        public virtual IList<LeadWorkflowStatusChanges> LeadWorkflowNewChangeList { get; protected set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public virtual DateTime? ModifiedDate { get; protected set; }
    }
}
