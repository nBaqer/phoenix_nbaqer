﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyWapiOperationLogger.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the SyWapiOperationLogger type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff
{
    using System;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Messages.SystemStuff.Maintenance;

    /// <summary>
    /// The system WAPI operation logger.
    /// </summary>
    public class SyWapiOperationLogger : DomainEntityWithTypedID<int>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SyWapiOperationLogger"/> class. 
        /// Internal constructor
        /// </summary>
        public SyWapiOperationLogger()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SyWapiOperationLogger"/> class. 
        /// Use this for test domain.
        /// </summary>
        /// <param name="serviceCode">
        /// The service Code.
        /// </param>
        /// <param name="companyCode">
        /// The company Code.
        /// </param>
        /// <param name="dateExecution">
        /// The date Execution.
        /// </param>
        /// <param name="nextPlanningDate">
        /// The next Planning Date.
        /// </param>
        /// <param name="isError">
        /// The is Error.
        /// </param>
        /// <param name="comment">
        /// The comment.
        /// </param>
        protected SyWapiOperationLogger(
          string serviceCode,
          string companyCode,
          DateTime dateExecution,
          DateTime nextPlanningDate,
          int isError,
          string comment)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.ServiceCode = serviceCode;
            this.CompanyCode = companyCode;
            this.DateExecution = dateExecution;
            this.NextPlanningDate = nextPlanningDate;
            this.IsError = isError;
            this.Comment = comment;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor 
        }

        #endregion
        
        #region Properties

        /// <summary>
        /// Gets or sets the service code.
        /// </summary>
        public virtual string ServiceCode { get; protected set; }

        /// <summary>
        /// Gets or sets the company code.
        /// </summary>
        public virtual string CompanyCode { get; protected set; }

        /// <summary>
        /// Gets or sets the date execution.
        /// </summary>
        public virtual DateTime DateExecution { get; protected set; }

        /// <summary>
        /// Gets or sets the next planning date.
        /// </summary>
        public virtual DateTime NextPlanningDate { get; protected set; }

        /// <summary>
        /// Gets or sets the is error.
        /// </summary>
        public virtual int IsError { get; protected set; }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        public virtual string Comment { get; protected set; }

        #endregion

        /// <summary>
        /// Fill the object with a <code>WapiLoggerOutputModel</code> entity.
        /// </summary>
        /// <param name="logRecord">Record to be logged</param>
        public virtual void ImportObject(WapiLoggerOutputModel logRecord)
        {
            this.Comment = logRecord.Comment;
            this.CompanyCode = logRecord.CompanyCode;
            this.DateExecution = logRecord.DateExecution;
            this.IsError = logRecord.IsError;
            this.NextPlanningDate = logRecord.NextPlanningDate;
            this.ServiceCode = logRecord.ServiceCode;
        }
    }
}
