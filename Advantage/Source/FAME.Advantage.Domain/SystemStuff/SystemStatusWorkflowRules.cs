﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.SystemStuff
{
    public class SystemStatusWorkflowRules: DomainEntityWithTypedID<int>
    {
        protected SystemStatusWorkflowRules()
        {
           
        }
        public virtual int Id { get; protected set; }
        public virtual SystemStatus StatusFrom{ get; protected set; }
        public virtual SystemStatus StatusTo { get; protected set; }
        public virtual System.Guid UniqueId { get; protected set; }
        public virtual bool AllowInsert { get; protected set; }
        public virtual bool AllowInsertSameStatusType { get; protected set; }
        public virtual bool AllowEdit { get; protected set; }
        public virtual bool AllowEditSameStatusType { get; protected set; }
        public virtual bool AllowDelete { get; protected set; }
        public virtual bool AllowedInBatch { get; protected set; }
        public virtual string ModUser { get; protected set; }
        public virtual DateTime ModDate { get; protected set; }
    }
}
