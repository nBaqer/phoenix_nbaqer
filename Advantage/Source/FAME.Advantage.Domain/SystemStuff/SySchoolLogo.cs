﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;
using NHibernate.Type;

namespace FAME.Advantage.Domain.SystemStuff
{
    /// <summary>
    /// Domain for table SySchoolLogo
    /// Create a logo entity, this table is independent.
    /// </summary>
    public class SySchoolLogo : DomainEntityWithTypedID<int>
    {
        #region Constructors

        /// <summary>
        /// Internal constructor
        /// </summary>
        protected SySchoolLogo()
        {
        }

        /// <summary>
        /// Use this for test domain.
        /// </summary>
        public SySchoolLogo(
            byte[] imagenBytes,
            int imgLenth,
            string contentType,
            bool officialUse,
            string imageCode,
            string description,
            string imageFile)
        {

            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            ImagenBytes = imagenBytes;
            ImgLenth = imgLenth;
            ContentType = contentType;
            OfficialUse = officialUse;
            ImageCode = imageCode;
            Description = description;
            ImageFile = imageFile;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }
        #endregion

        #region Properties
        /// <summary>
        /// Imagen in byte array
        /// </summary>
        public virtual byte[] ImagenBytes { get; set; }
        /// <summary>
        /// Tamano de la imagen
        /// </summary>
        public virtual int ImgLenth { get; set; }
        /// <summary>
        /// Typo de Imagen jpg, png, bmp
        /// </summary>
        public virtual string ContentType { get; set; }
        /// <summary>
        /// True is it official logo
        /// </summary>
        public virtual bool OfficialUse { get; set; }
        /// <summary>
        /// Code used to identified the imagen in the application
        /// Use this to reference the imagen in the application
        /// and do not use the id. 
        /// </summary>
        public virtual string ImageCode { get; set; }
        /// <summary>
        /// Explain in what context you use the image.
        /// </summary>
        public virtual string Description { get; set; }
        /// <summary>
        /// The name of the file image.
        /// </summary>
        public virtual string ImageFile { get; set; }

      
        #endregion

        public virtual void UpdateLogo(string logoDescription, string logoCode, bool isOfficialLogo)
        {
            Description = logoDescription;
            ImageCode = logoCode;
            OfficialUse = isOfficialLogo;
           
        }
    }
}
