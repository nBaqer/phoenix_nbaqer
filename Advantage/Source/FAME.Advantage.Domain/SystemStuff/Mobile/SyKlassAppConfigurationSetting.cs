﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyKlassAppConfigurationSetting.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Domain for Table syKlassAppConfigurationSetting
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff.Mobile
{
    using System;

    using FAME.Advantage.Messages.SystemStuff.Mobile;

    using Infrastructure.Entities;

    /// <summary>
    /// Domain for Table <code>syKlassAppConfigurationSetting</code>
    /// </summary>
    public class SyKlassAppConfigurationSetting : DomainEntityWithTypedID<int>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SyKlassAppConfigurationSetting"/> class.
        /// Constructor use this to create the object.
        /// </summary>
        /// <param name="advantageIdentification" > 
        /// The <code>advantageIdentification</code> 
        /// </param> 
        /// <param name="klassAppIdentification" > 
        /// The <code>klassAppIdentification</code> 
        /// </param> 
        /// <param name="klassEntityObject" > 
        /// The <code>klassEntityObject</code> 
        /// </param> 
        /// <param name="klassOperationTypeObject" > 
        /// The <code>klassOperationTypeObject</code> 
        /// </param> 
        /// <param name="isCustomField" > 
        /// The <code>isCustomField</code> 
        /// </param> 
        /// <param name="isActive" > 
        /// The <code>isActive</code> 
        /// </param> 
        /// <param name="itemStatus" > 
        /// The <code>itemStatus</code> 
        /// </param> 
        /// <param name="itemValue" > 
        /// The <code>itemValue</code> 
        /// </param> 
        /// <param name="itemLabel" > 
        /// The <code>itemLabel</code> 
        /// </param> 
        /// <param name="itemValueType" > 
        /// The <code>itemValueType</code> 
        /// </param> 
        /// <param name="creationDate" > 
        /// The <code>creationDate</code> 
        /// </param> 
        /// <param name="modDate" > 
        /// The <code>modDate</code> 
        /// </param> 
        /// <param name="modUser" > 
        /// The <code>modUser</code> 
        /// </param> 
        public SyKlassAppConfigurationSetting(
                string advantageIdentification,
                int klassAppIdentification,
                SyKlassEntity klassEntityObject,
                SyKlassOperationType klassOperationTypeObject,
                int isCustomField,
                int isActive,
                char itemStatus,
                string itemValue,
                string itemLabel,
                string itemValueType,
                DateTime creationDate,
                DateTime modDate,
                string modUser)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.AdvantageIdentification = advantageIdentification;
            this.KlassAppIdentification = klassAppIdentification;
            this.KlassEntityObject = klassEntityObject;
            this.KlassOperationTypeObject = klassOperationTypeObject;
            this.IsCustomField = isCustomField;
            this.IsActive = isActive;
            this.ItemStatus = itemStatus;
            this.ItemValue = itemValue;
            this.ItemLabel = itemLabel;
            this.ItemValueType = itemValueType;
            this.CreationDate = creationDate;
            this.ModDate = modDate;
            this.ModUser = modUser;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SyKlassAppConfigurationSetting"/> class.
        /// Protected Constructor.
        /// </summary>
        protected SyKlassAppConfigurationSetting()
        {
        }

        #endregion

        #region Properties     

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string AdvantageIdentification { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual int KlassAppIdentification { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual SyKlassEntity KlassEntityObject { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual SyKlassOperationType KlassOperationTypeObject { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual int IsCustomField { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual int IsActive { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual char ItemStatus { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string ItemValue { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string ItemLabel { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string ItemValueType { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual DateTime CreationDate { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets the last operation log.
        /// Contains the result of the last operation over <code>Klass_App</code>
        /// configuration system.
        /// </summary>
        public virtual string LastOperationLog { get; protected set; }

        #endregion

        ///// <summary>
        ///// The update status.
        ///// Accepted Values are N, I, D, U (No action, Insert, Delete, Update)
        ///// </summary>
        ///// <param name="status">
        ///// The status.
        ///// </param>
        ///// <param name="user">
        ///// The user.
        ///// </param>
        ////public virtual void UpdateStatus(char status, string user)
        ////{
        ////    if (status == 'N' && status == 'I' && status == 'D' && status == 'U')
        ////    {
        ////        this.ItemStatus = status;
        ////        this.ModDate = DateTime.Now;
        ////        this.ModUser = user;
        ////    }
        ////}

        /// <summary>
        /// The update <code>klass app</code> identification.
        /// </summary>
        /// <param name="klassAppIdentification">
        /// The <code>klass app</code> identification.
        /// </param>
        /// <param name="user">
        /// The user.
        /// </param>
        public virtual void UpdateKlassAppIdentification(int klassAppIdentification, string user)
        {
            this.KlassAppIdentification = klassAppIdentification;
            this.ModDate = DateTime.Now;
            this.ModUser = user;
        }

        /// <summary>
        /// The update setting configuration.
        /// </summary>
        /// <param name="klassAppIdentification">
        /// The <code>klass_app</code> identification.
        /// </param>
        /// <param name="lastOperationLog">
        /// The last operation log.
        /// </param>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="isActive">
        /// The is active.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        public virtual void UpdateSettingConfiguration(
            int klassAppIdentification,
            string lastOperationLog,
            string user,
            int isActive,
            char status)
        {
            if (status == 'N' || status == 'I' || status == 'D' || status == 'U')
            {
                this.ItemStatus = status;
            }

            this.KlassAppIdentification = klassAppIdentification;
            this.LastOperationLog = string.IsNullOrWhiteSpace(lastOperationLog) ? "OK" : lastOperationLog;
            this.IsActive = isActive;
            this.ModDate = DateTime.Now;
            this.ModUser = user;
        }

        /// <summary>
        /// The update table from UI.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        public virtual void UpdateTableFromUi(ClassAppConfigurationOutputModel model)
        {
            if (model.ItemStatus == 'N' || model.ItemStatus == 'I' || model.ItemStatus == 'D' || model.ItemStatus == 'U')
            {
                this.ItemStatus = model.ItemStatus;
                this.IsActive = model.IsActive ? 1 : 0;
                this.ItemLabel = model.ItemLabel;
                this.ModDate = DateTime.Now;
                this.ModUser = model.ModUser;
            }
        }
    }
}