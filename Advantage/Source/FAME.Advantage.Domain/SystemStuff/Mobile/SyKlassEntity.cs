﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyKlassEntity.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Domain for Table syKlassEntity
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff.Mobile
{
    using System.Collections.Generic;

    using Infrastructure.Entities;

    /// <summary>
    /// Domain for Table <code>syKlassEntity</code>
    /// </summary>
    public class SyKlassEntity : DomainEntityWithTypedID<int>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SyKlassEntity"/> class.
        /// Constructor use this to create the object.
        /// </summary>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        public SyKlassEntity(
                   string code,
               string description)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.Code = code;
            this.Description = description;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SyKlassEntity"/> class.
        /// Protected Constructor.
        /// </summary>
        protected SyKlassEntity()
        {
        }

        #endregion

        #region Properties
        
        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// Gets or sets the configuration setting List.
        /// </summary>
        public virtual IList<SyKlassAppConfigurationSetting> ConfigurationSettingList { get; protected set; }

        #endregion
    }
}