﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyKlassOperationType.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Domain for Table syKlassOperationType
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff.Mobile
{
    using System.Collections;
    using System.Collections.Generic;

    using Infrastructure.Entities;

    /// <summary>
    /// Domain for Table <code>syKlassOperationType</code>
    /// </summary>
    public class SyKlassOperationType : DomainEntityWithTypedID<int>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SyKlassOperationType"/> class.
        /// Constructor use this to create the object.
        /// </summary>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        public SyKlassOperationType(
                   string code,
               string description)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.Code = code;
            this.Description = description;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SyKlassOperationType"/> class.
        /// Protected Constructor.
        /// </summary>
        protected SyKlassOperationType()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// Gets or sets the configuration setting list.
        /// </summary>
        public virtual IList<SyKlassAppConfigurationSetting> ConfigurationSettingList { get; protected set; }

        #endregion
    }
}