﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Lead;

namespace FAME.Advantage.Domain.SystemStuff
{
    
    /// <summary>
    /// This is a table system Domain (syPhoneType)
    /// TODO: this table should be all codes equal in all school it  is not!
    /// </summary>
    public class SyPhoneType : DomainEntity
    {
        /// <summary>
        /// TODO: This should be unique in all school but not
        /// </summary>
        public virtual string PhoneTypeCode { get; protected set; }

        /// <summary>
        /// Description of the phone type
        /// </summary>
        public virtual string PhoneTypeDescrip { get; protected set; }

        /// <summary>
        /// Status Active inactive of the phone type.
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// List of phone of lead associated with this type
        /// </summary>
        public virtual IList<LeadPhone> LeadPhoneList { get; protected set; }  

    }
}