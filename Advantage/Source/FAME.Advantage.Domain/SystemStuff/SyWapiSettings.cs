﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyWapiSettings.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Domain for Table syWapiSettings
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff
{
    using System;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Messages.SystemStuff.Maintenance;

    /// <summary>
    /// Domain for Table <code>syWapiSettings</code>
    /// </summary>
    public class SyWapiSettings : DomainEntityWithTypedID<int>
    {
        #region Constructors

        /// <summary>
        /// Internal constructor
        /// </summary>       
        public SyWapiSettings()
        {
        }

        /// <summary>
        /// Use this for test domain.
        /// </summary>
        protected SyWapiSettings(
           string codeOperation,
           string externalUrl,
           SyWapiExternalCompanies codeExternalCompany,
           SyWapiExternalOperationMode codeExternalOperationMode,
           SyWapiAllowedServices codeWapiService,
           string consumerKey,
           string privateKey,
           int operationSecondTimeInterval,
           int pollSecondForOnDemandOperation,
           int flagOnDemandOperation,
           int flagRefreshConfiguration,
           DateTime dateLastExecution,
           bool isActive,
           string userName,
           string firstAllowedServiceQueryString
            )
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            CodeOperation = codeOperation;
            ExternalUrl = externalUrl;
            ExternalCompanyObj = codeExternalCompany;
            ExternalOperationModeObj = codeExternalOperationMode;
            AllowedServiceObj = codeWapiService;
            ConsumerKey = consumerKey;
            PrivateKey = privateKey;
            OperationSecondTimeInterval = operationSecondTimeInterval;
            PollSecondForOnDemandOperation = pollSecondForOnDemandOperation;
            FlagOnDemandOperation = flagOnDemandOperation;
            FlagRefreshConfiguration = flagRefreshConfiguration;
            DateLastExecution = dateLastExecution;
            IsActive = isActive;
            UserName = userName;
            FirstAllowedServiceQueryString = firstAllowedServiceQueryString;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor 
        }

        #endregion

        #region Properties

        public virtual string CodeOperation { get; protected set; }

        /// <summary>
        /// Code of the company that owner the web service
        /// in the case that the operation access a external
        /// resource.
        /// </summary>
        public virtual SyWapiExternalCompanies ExternalCompanyObj { get; protected set; }

        /// <summary>
        /// The type of interaction with the external resource if exists
        /// can be push or pull.
        /// </summary>
        public virtual SyWapiExternalOperationMode ExternalOperationModeObj { get; protected set; }

         /// <summary>
        /// The code of operation in the Wapi
        /// </summary>
        public virtual SyWapiAllowedServices AllowedServiceObj { get; protected set; }

        /// <summary>
        /// Some operations need a second WAPI operation to be complete.
        /// In this case this field hold the code.
        /// </summary>
        public virtual SyWapiAllowedServices SecondAllowedServiceObj { get; protected set; }

        /// <summary>
        /// Security key to interact with external service if exists
        /// </summary>
        public virtual string ConsumerKey { get; protected set; }

        /// <summary>
        /// Secirity key to interact with external service
        /// if exists.
        /// </summary>
        public virtual string PrivateKey { get; protected set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public virtual string UserName { get; protected set; }

        /// <summary>
        /// Time that a windows service should repeat the operation
        /// if it is needed. -1 if inactive.0 one time operation.
        /// </summary>
        public virtual int OperationSecondTimeInterval { get; protected set; }

        /// <summary>
        /// Poll Wapi from windows service to look for a configuration change.
        /// </summary>
        public virtual int PollSecondForOnDemandOperation { get; protected set; }

        /// <summary>
        /// 0: inactive, 1: active
        /// </summary>
        public virtual int FlagOnDemandOperation { get; protected set; }

        /// <summary>
        /// 0: inactive, 1: active
        /// </summary>
        public virtual int FlagRefreshConfiguration { get; protected set; }

        /// <summary>
        /// External URL to looking for, if exists.
        /// </summary>
        public virtual string ExternalUrl { get; protected set; }

        /// <summary>
        /// Date and time of the last execution.
        /// </summary>
        public virtual DateTime DateLastExecution { get; protected set; }

        /// <summary>
        /// True the operation is active and executable.
        /// </summary>
        public virtual bool IsActive { get; protected set; }

        /// <summary>
        /// Gets or sets the first allowed service query string.
        /// </summary>
        public virtual string FirstAllowedServiceQueryString { get; protected set; }
        #endregion

        #region Updates
        /// <summary>
        /// Allowed changes
        /// Only the table information is change.
        /// </summary>
        /// <param name="data">The Operation settings values</param>
        /// <param name="extCompany"></param>
        /// <param name="extMode"></param>
        /// <param name="allowServ"></param>
        /// <param name="sAllowServ"></param>
        public virtual void UpdateRecord(WapiOperationSettingOutputModel data, 
            SyWapiExternalCompanies extCompany, SyWapiExternalOperationMode extMode,
            SyWapiAllowedServices allowServ, SyWapiAllowedServices sAllowServ)
        {
            ExternalCompanyObj = extCompany;
            ExternalOperationModeObj = extMode;
            AllowedServiceObj = allowServ;
            SecondAllowedServiceObj = sAllowServ;
            ConsumerKey = data.ConsumerKey;
            PrivateKey = data.PrivateKey;
            OperationSecondTimeInterval = data.OperationSecInterval;
            PollSecondForOnDemandOperation = data.PollSecOnDemandOperation;
            ExternalUrl = data.ExternalUrl;
            DateLastExecution = data.DateLastExecution;
            IsActive = data.IsActiveOperation;
            CodeOperation = data.CodeOperation;
            UserName = data.UserName;
            FirstAllowedServiceQueryString = data.FirstAllowedServiceQueryString;
        }

        /// <summary>
        /// Update the ondemand flag
        /// </summary>
        /// <param name="ondemandValue"></param>
        public virtual void UpdateOnDemandFlag(int ondemandValue)
        {
            FlagOnDemandOperation = ondemandValue;
        }

        /// <summary>
        /// Update last Execution Date
        /// </summary>
        /// <param name="value"></param>
        public virtual void UpdateLastExecutedDate(DateTime value)
        {
            DateLastExecution = value;
        }
        #endregion
    }


}
