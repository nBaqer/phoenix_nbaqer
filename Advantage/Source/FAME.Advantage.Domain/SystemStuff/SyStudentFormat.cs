﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentFormat.cs" company="FAME INC ">
//   2018
// </copyright>
// <summary>
//   The student format.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.Domain.SystemStuff
{


    using System;
    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The student format.
    /// </summary>
    public class SyStudentFormat : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Gets or sets the Student format id.
        /// </summary>
        public virtual int StudentFormatId { get; protected set; }
        /// <summary>
        /// Gets or sets the format type.
        /// </summary>
        public virtual string FormatType { get; protected set; }

        /// <summary>
        /// Gets or sets the year number.
        /// </summary>
        public virtual int? YearNumber { get; protected set; }

        /// <summary>
        /// Gets or sets the month number.
        /// </summary>
        public virtual int? MonthNumber { get; protected set; }

        /// <summary>
        /// Gets or sets the date number.
        /// </summary>
        public virtual int? DateNumber { get; protected set; }

        /// <summary>
        /// Gets or sets the l name number.
        /// </summary>
        public virtual int? LNameNumber { get; protected set; }

        /// <summary>
        /// Gets or sets the f name number.
        /// </summary>
        public virtual int? FNameNumber { get; protected set; }

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        public virtual int? SeqNumber { get; protected set; }

        /// <summary>
        /// Gets or sets the sequence starting number.
        /// </summary>
        public virtual int? SeqStartingNumber { get; protected set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// should be made to map to user object but since it is just a string in database, no foreign key, kept it as string.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }
    }
}


