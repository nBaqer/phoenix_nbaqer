﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.SystemStuff
{
    public class SyEmailType : DomainEntity
    {
        /// <summary>
        /// The Email Type Unique Id
        /// </summary>
        public virtual Guid EMailTypeId { get; protected set; }

        /// <summary>
        /// The Email Type Code
        /// </summary>
        public virtual string EMailTypeCode { get; protected set; }

        /// <summary>
        /// The Email Type description
        /// </summary>
        public virtual string EMailTypeDescription { get; protected set; }

        /// <summary>
        /// Mofification Date Time
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// User last modified this object
        /// </summary>
        public virtual string ModUser { get; protected set; }
    }
}
