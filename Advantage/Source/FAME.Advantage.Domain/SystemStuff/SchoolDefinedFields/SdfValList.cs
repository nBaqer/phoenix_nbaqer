﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.SystemStuff.SchoolDefinedFields
{
    public class SdfValList: DomainEntity
    {
        #region Constructors

        protected SdfValList() { }

        public SdfValList(Sdf sdfobj, string vallist, string moduser, DateTime? moddate)
        {
// ReSharper disable DoNotCallOverridableMethodsInConstructor
            SdfObj = sdfobj;
            ValList = vallist;
            ModDate = moddate;
            ModUser = moduser;
// ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Properties

        public virtual Sdf SdfObj { get; protected set; }

        public virtual string ValList { get; protected set; }

        public virtual string ModUser { get; protected set; }

        public virtual DateTime? ModDate { get; protected set; }

        #endregion

    }
}
