﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.SystemStuff.SchoolDefinedFields
{
    /// <summary>
    /// Hold table for SDF controls values when the control need a min and max value
    /// Control type 1
    /// </summary>
    public class SdfRange : DomainEntity
    {
        #region Constructors

        protected SdfRange() { }

        public SdfRange(Sdf sdfobj, string minval, string maxval, string moduser, DateTime? moddate)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            SdfObj = sdfobj;
            MinVal = minval;
            MaxVal = maxval;
            ModDate = moddate;
            ModUser = moduser;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Properties

        public virtual Sdf SdfObj { get; protected set; }

        public virtual string MinVal { get; protected set; }

        public virtual string MaxVal { get; protected set; }

        public virtual string ModUser { get; protected set; }

        public virtual DateTime? ModDate { get; protected set; }

        #endregion

    }
}
