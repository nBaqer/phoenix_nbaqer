﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Sdf.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Defines the SDF type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.SystemStuff.SchoolDefinedFields
{
    using System;
    using System.Collections.Generic;
    using Infrastructure.Entities;

    /// <summary>
    ///  The SDF
    /// </summary>
    public class Sdf : DomainEntity
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Sdf"/> class.
        /// </summary>
        /// <param name="sdfDescrip">
        ///  The SDF description.
        /// </param>
        /// <param name="dtypeId">
        ///  The d-type id.
        /// </param>
        /// <param name="len">
        ///  The length.
        /// </param>
        /// <param name="moduser">
        ///  The modification user.
        /// </param>
        /// <param name="moddate">
        ///  The modification date.
        /// </param>
        public Sdf(string sdfDescrip, int dtypeId, int len, string moduser, DateTime? moddate)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.SdfDescrip = sdfDescrip;
            this.DtypeId = dtypeId;
            this.Len = len;
            this.ModDate = moddate;
            this.ModUser = moduser;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Sdf"/> class.
        /// don't use it.
        /// </summary>
        protected Sdf()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets School Defined Field description. Usually used as label
        /// </summary>
        public virtual string SdfDescrip { get; protected set; }

        /// <summary>
        ///  Gets or sets Use unknown
        /// </summary>
        public virtual int DtypeId { get; protected set; }

        /// <summary>
        ///  Gets or sets Use Unknown. Possible length
        /// </summary>
        public virtual int Len { get; protected set; }

        /// <summary>
        ///  Gets or sets If the value is a number the number of decimals
        /// </summary>
        public virtual int Decimals { get; protected set; }

        /// <summary>
        ///  Gets or sets Maybe to be use as tooltip. Not use actually
        /// </summary>
        public virtual string HelpText { get; protected set; }

        // ModuleID es not mapping because it is not used.

        /// <summary>
        ///  Gets or sets Type of control. For now exists the following values:
        /// 0 Single value
        /// 1 Control with min and max range
        /// 2 Control with list values or items (combo box type)
        /// </summary>
        public virtual int ValTypeId { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is required.
        /// 0 Not required
        /// 1 Required
        /// </summary>
        public virtual bool IsRequired { get; protected set; }

        /// <summary>
        ///  Gets or sets Status of the school defined Field
        /// Active A Inactive I
        /// </summary>
        public virtual SyStatuses StatusObj { get; protected set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public virtual DateTime? ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the values list.
        /// </summary>
        public virtual IList<SdfValList> ValuesList { get; protected set; }

        /// <summary>
        /// Gets or sets the range list.
        /// </summary>
        public virtual IList<SdfRange> RangeList { get; protected set; }

        /// <summary>
        /// Gets or sets the resources list.
        /// </summary>
        public virtual IList<SdfResources> ResourcesList { get; protected set; }

        /// <summary>
        /// Gets or sets the SDF module values list.
        /// </summary>
        public virtual IList<SdfModuleValueLead> SdfModuleValuesList { get; protected set; }

        #endregion
    }
}