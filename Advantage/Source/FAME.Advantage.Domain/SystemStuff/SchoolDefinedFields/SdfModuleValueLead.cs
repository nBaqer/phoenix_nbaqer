﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.Lead;

namespace FAME.Advantage.Domain.SystemStuff.SchoolDefinedFields
{
    /// <summary>
    /// This domain class can be tricky in the future
    /// because should also hold a student and employee obj.
    /// </summary>
    public class SdfModuleValueLead : DomainEntity
    {
        #region Constructors

        /// <summary>
        /// Protected Constructor
        /// </summary>
        protected SdfModuleValueLead() { }

        /// <summary>
        /// Diagnostic constructor
        /// </summary>
        /// <param name="sdfobj"></param>
        /// <param name="sdfvalue"></param>
        /// <param name="leadObj"></param>
        /// <param name="moduser"></param>
        /// <param name="moddate"></param>
        public SdfModuleValueLead(Sdf sdfobj, string sdfvalue, Lead.Lead leadObj, string moduser, DateTime? moddate)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            ID = new Guid();
            SdfObj = sdfobj;
            LeadObj = leadObj;
            SdfValue = sdfvalue;
            ModDate = moddate;
            ModUser = moduser;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Properties

        /// <summary>
        /// SDf Object .it is the school defined field object
        /// </summary>
        public virtual Sdf SdfObj { get; protected set; }

        /// <summary>
        /// the value of the field
        /// </summary>
        public virtual string SdfValue { get; protected set; }

        /// <summary>
        /// User to do the modification
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Modification date
        /// </summary>
        public virtual DateTime? ModDate { get; protected set; }

        /// <summary>
        /// The lead associated with this field
        /// </summary>
        public virtual Lead.Lead LeadObj { get; protected set; }

        #endregion

        /// <summary>
        /// Update operation
        /// </summary>
        /// <param name="model"></param>
        public virtual void UpdateSdf(LeadSdfOutputModel model)
        {
            SdfValue = model.SdfValue;
            ModDate = DateTime.Now;
        }

        /// <summary>
        /// The update custom module value lead.
        /// </summary>
        /// <param name="sdfobj">
        /// The custom object.
        /// </param>
        /// <param name="sdfvalue">
        /// The custom value.
        /// </param>
        /// <param name="leadObj">
        /// The lead object.
        /// </param>
        /// <param name="moduser">
        /// The user that modified.
        /// </param>
        /// <param name="moddate">
        /// The modification date.
        /// </param>
        public virtual void UpdateSdfModuleValueLead(string sdfvalue, Lead.Lead leadObj, string moduser, DateTime? moddate)
        {
            //this.SdfObj = sdfobj;
            this.LeadObj = leadObj;
            this.SdfValue = sdfvalue;
            this.ModDate = moddate;
            this.ModUser = moduser;
        }

        /// <summary>
        /// Update Lead Object
        /// </summary>
        /// <param name="lead"></param>
        public virtual void UpdateLead(Lead.Lead lead)
        {
            LeadObj = lead;
        }

        /// <summary>
        /// Update Mod User
        /// </summary>
        /// <param name="moduser"></param>
        public virtual void UpdateModUser(string moduser)
        {
            ModUser = moduser;
        }
    }
}
