﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SdfResources.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the SdfResources type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff.SchoolDefinedFields
{
    using System;

    using FAME.Advantage.Domain.Users.UserRoles;
    using FAME.Advantage.Messages.SystemStuff.Udf;

    using Infrastructure.Entities;

    /// <summary>
    ///  The SDF resources (<code>sySdfResources</code> Table).
    /// </summary>
    public class SdfResources : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SdfResources"/> class.
        /// </summary>
        /// <param name="resourcesObj">
        ///  The resources object.
        /// </param>
        /// <param name="sdfObj">
        ///  The SDF object.
        /// </param>
        /// <param name="sdfVisibility">
        ///  The SDF visibility.
        /// </param>
        /// <param name="modUser">
        ///  The mod user.
        /// </param>
        /// <param name="entityObj">
        ///  The entity object.
        /// </param>
        /// <param name="position">
        ///  The position.
        /// </param>
        public SdfResources(
            Resources resourcesObj,
            Sdf sdfObj,
            string sdfVisibility,
            string modUser,
            Resources entityObj,
            int position)
        {
            // ReSharper disable VirtualMemberCallInConstructor
            this.ResourceObj = resourcesObj;
            this.ID = Guid.NewGuid();
            this.Position = position;
            this.EntityObj = entityObj;
            this.ModDate = DateTime.Now;
            this.ModUser = modUser;
            this.SdfObj = sdfObj;
            this.SdfVisibility = sdfVisibility;
            // ReSharper restore VirtualMemberCallInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SdfResources"/> class. 
        /// Protected to avoid usage in code.
        /// </summary>
        protected SdfResources()
        {
        }

        /// <summary>
        /// Gets or sets Resource of the page to be show
        /// </summary>
        public virtual Resources ResourceObj { get; protected set; }

        /// <summary>
        ///  Gets or sets SDF Object related selectable with this resource combination
        /// </summary>
        public virtual Sdf SdfObj { get; protected set; }

        /// <summary>
        ///  Gets or sets Object visibility
        /// </summary>
        public virtual string SdfVisibility { get; protected set; }

        /// <summary>
        ///  Gets or sets User Roles this is a number associated with a entity 
        /// ( Resource type 8 entity). They are hard-coded
        /// in table <code>syResources</code>
        /// Lead entity = 395
        /// </summary>
        public virtual Resources EntityObj { get; protected set; }

        /// <summary>
        ///   Gets or sets  User that make the modification
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        ///   Gets or sets Date Time of the modification
        /// </summary>
        public virtual DateTime? ModDate { get; protected set; }

        /// <summary>
        ///   Gets or sets Position of the resource in page (1-12) 0 no position
        /// </summary>
        public virtual int Position { get; protected set; }

        /// <summary>
        ///  The update position and description.
        ///  This method does not save you need to 
        /// call after it the repository save!!!
        /// </summary>
        /// <param name="item">
        ///  The item.
        /// </param>
        public virtual void UpdatePositionAndVisibility(UdfDto item)
        {
            this.SdfVisibility = "Edit";//item.SdfVisibility ? "Edit" : "View";
            this.Position = item.Position;
        }
    }
}
