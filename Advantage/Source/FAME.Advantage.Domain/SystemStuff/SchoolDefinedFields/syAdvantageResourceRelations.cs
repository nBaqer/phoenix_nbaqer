﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyAdvantageResourceRelations.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Domain for Table syAdvantageResourceRelations
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff.SchoolDefinedFields
{
    using Infrastructure.Entities;

    /// <summary>
    /// Domain for Table syAdvantageResourceRelations
    /// </summary>
    public class SyAdvantageResourceRelations : DomainEntityWithTypedID<int>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SyAdvantageResourceRelations"/> class.
        /// Constructor use this to create the object.
        /// </summary>
        public SyAdvantageResourceRelations(
                   Users.UserRoles.Resources resourceObj,
               Users.UserRoles.Resources relatedResourceObj
)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            ResourceObj = resourceObj;
            RelatedResourceObj = relatedResourceObj;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SyAdvantageResourceRelations"/> class.
        /// Protected Constructor.
        /// </summary>
        protected SyAdvantageResourceRelations()
        {
        }

        #endregion

        #region Properties


        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual Users.UserRoles.Resources ResourceObj { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public virtual Users.UserRoles.Resources RelatedResourceObj { get; protected set; }

        #endregion
    }
}


