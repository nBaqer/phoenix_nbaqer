﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatusChangesDeleted.cs" company="FAME Inc.">
//   FAME.Advantage.Domain.SystemStuff
// </copyright>
// <summary>
//   Defines the StatusChangesDeleted type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff
{
    using System;

    using FAME.Advantage.Domain.Campuses;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Student.Enrollments;

    /// <summary>
    /// The status changes deleted.
    /// </summary>
    public class StatusChangesDeleted : DomainEntity
    {
        protected StatusChangesDeleted()
        {

        }

        /// <summary>
        /// Initialize a new instance of Status Changes Deleted
        /// </summary>
        /// <param name="enrollment">The Student Enrollment</param>
        /// <param name="originalStatus">The Original Status</param>
        /// <param name="newStatus">The New Status</param>
        /// <param name="campus">The Campus</param>
        /// <param name="modifiedDate">The Modified Date</param>
        /// <param name="modifiedUser">The Modified User</param>
        /// <param name="isReversal">The Is Reversal</param>
        /// <param name="dropReasonId">The Drop Reason Id</param>
        /// <param name="dateOfChange">The Date of Change</param>
        /// <param name="lda">The Last day of Attendance</param>
        /// <param name="caseNumber">The Case Number</param>
        /// <param name="requestedBy">The user who requested the change</param>
        /// <param name="haveBackup">The Confirmation if a backup was created</param>
        /// <param name="haveClientConfirmation">The confirmation if the client agree with deleting the record</param>
        /// <param name="deleteDate">The Delete Date</param>
        /// <param name="userDeleted">The User who Deleted</param>
        /// <param name="deleteReason">The reason for deleting the record</param>
        /// <param name="studentStatusChangeId">The status change that is been deleted</param>
        public StatusChangesDeleted(Enrollment enrollment, UserDefStatusCode originalStatus, UserDefStatusCode newStatus, Campus campus, DateTime? modifiedDate, string modifiedUser, bool isReversal, Guid? dropReasonId, DateTime? dateOfChange, DateTime? lda, string caseNumber, string requestedBy, bool? haveBackup, bool? haveClientConfirmation, Guid studentStatusChangeId)
        {
            this.Enrollment = enrollment;
            this.OriginalStatus = originalStatus;
            this.NewStatus = newStatus;
            this.Campus = campus;
            this.ModifiedDate = modifiedDate;
            this.ModifiedUser = modifiedUser;
            this.IsReversal = isReversal;
            this.DropReasonId = dropReasonId;
            this.DateOfChange = dateOfChange;
            this.Lda = lda;
            this.CaseNumber = caseNumber;
            this.RequestedBy = requestedBy;
            this.HaveBackup = haveBackup;
            this.HaveClientConfirmation = haveClientConfirmation;
            this.StudentStatusChangeId = studentStatusChangeId;
        }

        /// <summary>
        /// Gets or sets the enrollment.
        /// </summary>
        public virtual Enrollment Enrollment { get; protected set; }

        /// <summary>
        /// Gets or sets the original status.
        /// </summary>
        public virtual UserDefStatusCode OriginalStatus { get; protected set; }

        /// <summary>
        /// Gets or sets the new status.
        /// </summary>
        public virtual UserDefStatusCode NewStatus { get; protected set; }

        /// <summary>
        /// Gets or sets the campus.
        /// </summary>
        public virtual Campus Campus { get; protected set; }

        /// <summary>
        /// Gets or sets the Modified Date. Also known of the date of change
        /// </summary>
        public virtual DateTime? ModifiedDate { get; protected set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public virtual string ModifiedUser { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is reversal.
        /// </summary>
        public virtual bool IsReversal { get; protected set; }

        /// <summary>
        /// Gets or sets the drop reason id.
        /// </summary>
        public virtual Guid? DropReasonId { get; protected set; }

        /// <summary>
        /// Gets or sets the Date of Change. Also Known as Effective Date
        /// </summary>
        public virtual DateTime? DateOfChange { get; protected set; }

        /// <summary>
        /// Gets or sets the LDA.
        /// </summary>
        public virtual DateTime? Lda { get; protected set; }

        /// <summary>
        /// Gets or sets the case number.
        /// </summary>
        public virtual string CaseNumber { get; protected set; }

        /// <summary>
        /// Gets or sets the requested by.
        /// </summary>
        public virtual string RequestedBy { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether have backup of Advantage Database before of deleting attendance and grades posted.
        /// </summary>
        public virtual bool? HaveBackup { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether have client confirmation to delete attendance and grades posted.
        /// </summary>
        public virtual bool? HaveClientConfirmation { get; protected set; }

        /// <summary>
        /// Gets or sets the delete date.
        /// </summary>
        public virtual DateTime DeleteDate { get; protected set; }

        /// <summary>
        /// Gets or sets the user deleted.
        /// </summary>
        public virtual string UserDeleted { get; protected set; }

        /// <summary>
        /// Gets or sets the delete reason.
        /// </summary>
        public virtual StatusChangeDeleteReasons DeleteReason { get; protected set; }

        /// <summary>
        /// Gets or sets the student status change id.
        /// </summary>
        public virtual Guid StudentStatusChangeId { get; protected set; }

        /// <summary>
        /// The update domain based on parameter.
        /// </summary>
        /// <param name="userDeleted">
        /// The user deleted.
        /// </param>
        /// <param name="deleteReason">
        /// The delete reason.
        /// </param>
        public virtual void Update(string userDeleted, StatusChangeDeleteReasons deleteReason)
        {
            this.UserDeleted = userDeleted;
            this.DeleteReason = deleteReason;
            this.DeleteDate = DateTime.Now;
        }

    }
}
