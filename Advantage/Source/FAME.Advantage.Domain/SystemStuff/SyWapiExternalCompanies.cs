﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.SystemStuff.Wapi;

namespace FAME.Advantage.Domain.SystemStuff
{
    public class SyWapiExternalCompanies : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Internal constructor
        /// </summary>       
        public SyWapiExternalCompanies()
        {
        }

        public SyWapiExternalCompanies(int id, string code, string description, bool isActive, IList<SyWapiSettings> wapiSettingsList)
        {
            base.ID = id;
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Code = code;
            Description = description;
            WapiSettingsList = wapiSettingsList;
            IsActive = isActive;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor 
        }

        public virtual string Code { get; protected set; }
        public virtual string Description { get; protected set; }
        public virtual bool IsActive { get; protected set; }

        public virtual IList<SyWapiSettings> WapiSettingsList { get; protected set; }

        /// <summary>
        /// Many to many throught syWapiExternalCompanyAllowedService bridge.
        /// </summary>
        public virtual IList<SyWapiAllowedServices> WapiAllowedServicesList { get; protected set; }

        /// <summary>
        /// Update the Company fields...
        /// </summary>
        /// <param name="model"></param>
        public virtual void Update(WapiCatalogsOutputModel model)
        {
            Code = model.Code;
            Description = model.Description;
            IsActive = model.IsActive;
        }
    }
}
