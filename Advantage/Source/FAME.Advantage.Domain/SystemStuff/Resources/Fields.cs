﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.SystemStuff.Resources
{
    /// <summary>
    /// Domain for Table syFields
    /// </summary>
    public class Fields : DomainEntityWithTypedID<int>
    {
        public virtual int FieldLen { get; protected set; }

        /// <summary>
        /// The resource name of the field. normally the name of the field in the table.
        /// </summary>
        public virtual string FldName { get; protected set; }

        public virtual IList<TableFields> TableFieldsList { get; protected set; }
        public virtual IList<FieldsCaptions> FieldsCaptionsList { get; protected set; }
    }
}
