﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RolesModulesBridge.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.SystemStuff.Resources.RolesModulesBridge
// </copyright>
// <summary>
//   The roles modules bridge.
//   Domain Entity for table <code>syRolesModules</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff.Resources
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Users.UserRoles;

    /// <summary>
    /// The roles modules bridge.
    /// Domain Entity for table <code>syRolesModules</code>
    /// </summary>
    public class RolesModulesBridge : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RolesModulesBridge"/> class.
        /// </summary>
        public RolesModulesBridge()
        {
        }

        /// <summary>
        /// Gets or sets the role.
        /// </summary>
        public virtual Roles Role { get; protected set; }

        /// <summary>
        /// Gets or sets the module.
        /// </summary>
        public virtual Resources Resource { get; protected set; }
    }
}
