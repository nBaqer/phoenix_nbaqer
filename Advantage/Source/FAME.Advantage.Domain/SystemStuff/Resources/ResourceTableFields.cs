﻿using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.SystemStuff.Resources
{
    /// <summary>
    /// Domain for Table syResTblFlds
    /// </summary>
    public class ResourceTableFields : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// If the resource is required or not
        /// </summary>
        public virtual bool IsRequired { get; protected set; }

        /// <summary>
        /// ?
        /// </summary>
        public virtual bool SchlReq { get; protected set; }

        /// <summary>
        /// The resources (for example page) associated with this resource (field)
        /// </summary>
        public virtual Users.UserRoles.Resources ResourceObj { get; protected set; }

        /// <summary>
        /// The table that the resource (field ) belong.
        /// </summary>
        public virtual TableFields TableFieldsObj { get; protected set; }
    }
}
