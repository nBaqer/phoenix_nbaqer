﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.SystemStuff.Resources
{
    /// <summary>
    /// Domain Table syLangs
    /// </summary>
    public class Languages : DomainEntityWithTypedID<int>
    {
        public virtual string LangName { get; protected set; }
        public virtual IList<FieldsCaptions> FieldsCaptionsList { get; protected set; }
    }
}
