﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RptFields.cs" company="FAME INC">
//   2016
// </copyright>
// <summary>
//   Defines the RptFields type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff.Resources
{
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.SystemStuff.Resources;

    /// <summary>
    /// The required fields for IPEDS fields.
    /// </summary>
    public class RptFields : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RptFields"/> class. 
        /// </summary>
        protected RptFields()
        {
        }

        /// <summary>
        /// Gets or sets the description of field.
        /// </summary>
        public virtual string Descrip { get; set; }

        /// <summary>
        /// Gets or sets the field id.
        /// </summary>
        public virtual Fields Field { get; set; }
    }
}
