﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemModule.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.SystemStuff.Resources.SystemModule
// </copyright>
// <summary>
//   Domain for syModules
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff.Resources
{
    using System.Collections.Generic;

    using Infrastructure.Entities;
    using Lead;

    /// <summary>
    /// The system module.
    /// Domain entity for SYModules
    /// </summary>
    public class SystemModule : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemModule"/> class.
        /// </summary>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        public SystemModule(string code, string name)
        {
            // ReSharper disable VirtualMemberCallInContructor
            this.Code = code;
            this.Name = name;
            // ReSharper restore VirtualMemberCallInContructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemModule"/> class.
        /// </summary>
        protected SystemModule()
        {
        }

        /// <summary>
        /// The enumeration system module.
        /// </summary>
        public enum ESystemModule
        {
            /// <summary>
            /// The academic records (ModuleCode = AR).
            /// </summary>
            AcademicRecords = 1,

            /// <summary>
            /// The admissions (ModuleCode = AD).
            /// </summary>
            Admissions = 2,

            /// <summary>
            /// The faculty (ModuleCode = FC).
            /// </summary>
            Faculty = 3,

            /// <summary>
            /// The financial aid  (ModuleCode = FA).
            /// </summary>
            FinancialAid = 4,

            /// <summary>
            /// The human resources (ModuleCode = HR).
            /// </summary>
            HumanResources = 5,

            /// <summary>
            /// The placement (ModuleCode = PL).
            /// </summary>
            Placement = 6,

            /// <summary>
            /// The student accounts (ModuleCode = SA).
            /// </summary>
            StudentAccounts = 7,

            /// <summary>
            /// The system (ModuleCode = SY).
            /// </summary>
            System = 8,
        }

        /// <summary>
        /// Gets or sets the Module 2 letters code.
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public virtual string Name { get; protected set; }

        /// <summary>
        /// Gets or sets the all notes list.
        /// </summary>
        public virtual IList<AllNotes> AllNotesList { get; protected set; }

        //// <summary>
        ///// Gets or sets the universal search modules.
        ///// </summary>
        // public virtual IList<UniversalSearchModule> UniversalSearchModules { get; protected set; }
    }
}
