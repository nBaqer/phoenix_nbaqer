﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.SystemStuff.Resources
{
    /// <summary>
    /// Domain syTblFlds
    /// </summary>
    public class TableFields : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Domain of the object that represent syFiels Table
        /// </summary>
        public virtual Fields FieldObj { get; protected set; }

        /// <summary>
        /// The list of resources tables connected with this resource.
        /// </summary>
        public virtual IList<ResourceTableFields> ResourceTableFieldsList { get; protected set; }
    }
}
