﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Domain.SystemStuff.Resources
{
    using FAME.Advantage.Domain.Infrastructure.Entities;

    public class RptAgencyFields : DomainEntityWithTypedID<int>
    {
        protected RptAgencyFields()
        {
        }

        public virtual RptAgencies RptAgencyId { get; set; }
        public virtual RptFields Fields { get; set; }
    }
}
