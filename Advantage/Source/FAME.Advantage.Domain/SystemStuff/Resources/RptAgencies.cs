﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RptAgencies.cs" company="FAME INC">
//   2016
// </copyright>
// <summary>
//   Defines the RptAgencies type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff.Resources
{
    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The 1 IPEDS
    /// 2 NACCAS
    /// 3 ACCSCT
    /// 4 ISIR
    /// 5 Gainful Employment.
    /// </summary>
    public class RptAgencies : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RptAgencies"/> class.
        /// </summary>
        public RptAgencies()
        {
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Descrip { get; set; }
    }
}
