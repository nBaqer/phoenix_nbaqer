﻿using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.SystemStuff.Resources
{
    /// <summary>
    /// Domain Table syFldCaptions
    /// </summary>
    public class FieldsCaptions : DomainEntityWithTypedID<int>
    {
        public virtual string FieldDescription { get; protected set; }
        public virtual string Caption { get; protected set; }

        public virtual Fields FieldsObj { get; protected set; }
        public virtual Languages LenguagesObj { get; protected set; }
    }
}
