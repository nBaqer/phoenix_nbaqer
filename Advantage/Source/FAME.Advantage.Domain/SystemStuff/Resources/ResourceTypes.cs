﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;


namespace FAME.Advantage.Domain.SystemStuff.Resources
{
    /// <summary>
    /// Hold the resources types of Advantage
    /// IT is a Catalog
    /// </summary>
    public  class ResourceTypes: DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Type of resource
        /// </summary>
        public virtual string ResourceType { get; protected set; }

        public virtual IList<Users.UserRoles.Resources> ResourcesList { get; protected set; }

    }
}
