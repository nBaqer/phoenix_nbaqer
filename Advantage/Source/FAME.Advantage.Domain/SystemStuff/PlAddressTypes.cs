﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.SystemStuff
{
    public class PlAddressTypes : DomainEntity
    {
        /// <summary>
        /// The Address Type Unique Id
        /// </summary>
        public virtual Guid AddressTypeId { get; protected set; }
        /// <summary>
        /// The Address Type Description
        /// </summary>
        public virtual string AddressDescrip { get; protected set; }
        /// <summary>
        /// Status Active inactive of the address type.
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// The Campus Group associated with this Address Type
        /// </summary>
        public virtual Guid CampGrpId { get; protected set; }

        /// <summary>
        /// The Address Type Code
        /// </summary>
        public virtual string AddressCode { get; protected set; }

        /// <summary>
        /// Mofification Date Time
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// User last modified this object
        /// </summary>
        public virtual string ModUser { get; protected set; }
    }
}
