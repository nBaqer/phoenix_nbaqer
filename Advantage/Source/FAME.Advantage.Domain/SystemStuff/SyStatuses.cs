// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyStatuses.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Describe active and inactive status.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff
{
    using System.Collections.Generic;

    using Campuses;
    using Catalogs;
    using Infrastructure.Entities;
    using Lead;
    using Student.Enrollments;

    /// <summary>
    /// Describe active and inactive status.
    /// </summary>
    public class SyStatuses : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyStatuses"/> class. 
        /// Protected Constructor
        /// </summary>
       public SyStatuses()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SyStatuses"/> class.
        /// </summary>
        /// <param name="status">
        ///  The status.
        /// </param>
        /// <param name="statusCode">
        ///  The status code.
        /// </param>
        /// <param name="campuses">
        ///  The campuses.
        /// </param>
        public SyStatuses(string status, string statusCode, IEnumerable<Campus> campuses = null)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.Status = status;
            this.StatusCode = statusCode;
            this.Campuses = new List<Campus>();
            if (campuses != null)
            {
                foreach (Campus c in campuses)
                {
                    this.Campuses.Add(c);
                }
            }

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Gets or sets Status Can be Active/Inactive
        /// </summary>
        public virtual string Status { get; protected set; }

        /// <summary>
        /// Gets or sets Status Code Can be A/I
        /// </summary>
        public virtual string StatusCode { get; protected set; }

        /// <summary>
        /// Gets or sets the campuses.
        /// </summary>
        public virtual IList<Campus> Campuses { get; protected set; } // Related to one to many with syCampuses

        /// <summary>
        /// Gets or sets the holidays list.
        /// </summary>
        public virtual IList<Holidays> HolidaysList { get; protected set; }

        /// <summary>
        /// Gets or sets the program group list.
        /// </summary>
        public virtual IList<ArProgramGroup> ProgramGroupList { get; protected set; }

        /// <summary>
        /// Gets or sets the suffixes list.
        /// </summary>
        public virtual IList<Suffixes> SuffixesList { get; protected set; }

        /// <summary>
        /// Gets or sets the prefixes list.
        /// </summary>
        public virtual IList<Prefixes> PrefixesList { get; protected set; }

        /// <summary>
        /// Gets or sets the program list.
        /// </summary>
        public virtual IList<ArPrograms> ProgramList { get; protected set; }

        /// <summary>
        /// Gets or sets the attend types list.
        /// </summary>
        public virtual IList<ArAttendTypes> AttendTypesList { get; protected set; }

        /// <summary>
        /// Gets or sets the reason not enrolled list.
        /// </summary>
        public virtual IList<SyReasonNotEnrolled> ReasonNotEnrolledList { get; protected set; }
    }
}