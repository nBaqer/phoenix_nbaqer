﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemDocumentStatus.cs" company="FAME Inc">
//   2016
//   FAME.Advantage.Domain.SystemStuff.SystemDocumentStatus
// </copyright>
// <summary>
//   Defines the System Document Status type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff
{
    using System;

    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The system document status.
    /// </summary>
    public class SystemDocumentStatus : DomainEntityWithTypedID<short>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemDocumentStatus"/> class.
        /// </summary>
        /// <param name="description">
        /// The description.
        /// </param>
        public SystemDocumentStatus(string description)
        {
            this.Description = description;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemDocumentStatus"/> class.
        /// </summary>
        protected SystemDocumentStatus()
        {
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Description { get; set; }
    }
}
