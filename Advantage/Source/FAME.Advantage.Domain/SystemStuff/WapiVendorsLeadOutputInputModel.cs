﻿namespace FAME.Advantage.Domain.SystemStuff
{
    /// <summary>
    /// Filter for Vendors. If filter null return all vendors.
    /// </summary>
    public class WapiVendorsLeadOutputInputModel
    {
        /// <summary>
        /// Vendor Id 
        /// </summary>
        public int VendorId { get; set; }

        /// <summary>
        /// Internal Campaign Id
        /// </summary>
        public int CampaignId { get; set; }
    }
}
