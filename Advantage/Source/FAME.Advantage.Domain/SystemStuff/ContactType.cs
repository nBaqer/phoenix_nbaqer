﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.SystemStuff
{
    /// <summary>
    /// ContactType is the Domain Object for the syContactTypes table
    /// </summary>
    public class ContactType: DomainEntity
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ContactType()
        {
        }

        /// <summary>
        /// ID
        /// </summary>
        public virtual Guid ContactTypeId { get; protected set; }
        /// <summary>
        /// Contact Type Code
        /// </summary>
        public virtual string Code { get; protected set; }
        /// <summary>
        /// Contact Type Description
        /// </summary>
        public virtual string Description { get; protected set; }
        /// <summary>
        /// Status Id
        /// </summary>
        public virtual Guid StatusId { get; protected set; }
        /// <summary>
        /// Status
        /// </summary>
        public virtual SyStatuses Status { get; protected set; }
        /// <summary>
        /// Campus Group Id
        /// </summary>
        public virtual Guid CampusGroupId { get; protected set; }
        /// <summary>
        /// User last modified this object
        /// </summary>
        public virtual string ModUser { get; protected set; }
        /// <summary>
        /// Last Modified Date
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }
    }
}
