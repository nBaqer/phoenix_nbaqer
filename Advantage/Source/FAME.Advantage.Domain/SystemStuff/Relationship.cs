﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.SystemStuff
{
    /// <summary>
    /// Relationship is the Domain Object for the syRelations table
    /// </summary>
    public class Relationship : DomainEntity
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public Relationship()
        {

        }
        /// <summary>
        /// ID
        /// </summary>
        public virtual Guid RelationId { get; protected set; }
        /// <summary>
        /// Relationship Code
        /// </summary>
        public virtual string Code { get; protected set; }
        /// <summary>
        /// Id of the Status
        /// </summary>
        public virtual Guid StatusId { get; protected set; }
        /// <summary>
        /// Description
        /// </summary>
        public virtual string Description { get; protected set; }
        /// <summary>
        /// Campus Group Id
        /// </summary>
        public virtual Guid CampusGroupId { get; protected set; }
        /// <summary>
        /// Status 
        /// </summary>
        public virtual SyStatuses Status { get; protected set; }
        /// <summary>
        /// User last modified this object
        /// </summary>
        public virtual string ModUser { get; protected set; }
        /// <summary>
        /// Last Modified Date
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }
    }
}
