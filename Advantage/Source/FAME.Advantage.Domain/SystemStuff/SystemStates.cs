﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemStates.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the SystemStates type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff
{
    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The system states.
    /// </summary>
    public class SystemStates : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemStates"/> class.
        /// </summary>
        /// <param name="stateCode">
        ///  The state code.
        /// </param>
        /// <param name="description">
        ///  The description.
        /// </param>
        public SystemStates(string stateCode, string description)
        {
            // ReSharper disable VirtualMemberCallInConstructor
            this.Code = stateCode;
            this.Description = description;
            // ReSharper restore VirtualMemberCallInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemStates"/> class.
        /// </summary>
        protected SystemStates()
        {
        }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// Gets or sets the status object.
        /// </summary>
        public virtual SyStatuses StatusObj { get; protected set; }
    }
}
