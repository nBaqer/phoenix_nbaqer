﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.SystemStuff
{
    public class SyWapiExternalOperationMode : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Internal constructor
        /// </summary>       
        public SyWapiExternalOperationMode()
        {
        }

        public SyWapiExternalOperationMode(int id, string code, string description, IList<SyWapiSettings> wapiSettingsList)
        {
            base.ID = id;
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Code = code;
            Description = description;
            WapiSettingsList = wapiSettingsList;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor 
        }

        public virtual string Code { get; protected set; }
        public virtual string Description { get; protected set; }
        public virtual IList<SyWapiSettings> WapiSettingsList { get; protected set; }
    }
}
