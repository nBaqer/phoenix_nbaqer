﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionContact.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.SystemStuff.Intitution.InstitutionContact
// </copyright>
// <summary>
//   The institution contact.
//   Domain Entity for table  <code>syInstitutionContacts</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff.Intitution
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The institution contact.
    /// Domain Entity for table  <code>syInstitutionContacts</code>
    /// </summary>
    public class InstitutionContact : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionContact"/> class.
        /// </summary>
        public InstitutionContact()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionContact"/> class.
        /// </summary>
        /// <param name="institution">
        /// The institution.
        /// </param>
        /// <param name="firstName">
        /// The first name.
        /// </param>
        /// <param name="lastName">
        /// The last name.
        /// </param>
        /// <param name="middleName">
        /// The middle name.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        /// <param name="suffix">
        /// The suffix.
        /// </param>
        /// <param name="prefix">
        /// The prefix.
        /// </param>
        /// <param name="title">
        /// The title.
        /// </param>
        /// <param name="phone">
        /// The phone.
        /// </param>
        /// <param name="phoneExt">
        /// The phone ext.
        /// </param>
        /// <param name="isForeignPhone">
        /// The is foreign phone.
        /// </param>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="modifiedDate">
        /// The modified date.
        /// </param>
        /// <param name="isDefault">
        /// The is default.
        /// </param>
        public InstitutionContact(HighSchools institution, string firstName, string lastName, string middleName, SyStatuses status, Suffix suffix, Prefix prefix, string title, string phone, string phoneExt, bool isForeignPhone, string email, string modifiedUser, DateTime? modifiedDate, bool isDefault)
        {
            this.Institution = institution;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.MiddleName = middleName;
            this.Status = status;
            this.Suffix = suffix;
            this.Prefix = prefix;
            this.Title = title;
            this.Phone = phone;
            this.PhoneExt = phoneExt;
            this.IsForeignPhone = isForeignPhone;
            this.Email = email;
            this.ModifiedUser = modifiedUser;
            this.ModifiedDate = modifiedDate;
            this.IsDefault = isDefault;
        }

        /// <summary>
        /// Gets or sets the institution.
        /// </summary>
        public virtual HighSchools Institution { get; protected set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public virtual string FirstName { get; protected set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public virtual string LastName { get; protected set; }

        /// <summary>
        /// Gets or sets the middle name.
        /// </summary>
        public virtual string MiddleName { get; protected set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public virtual SyStatuses Status { get; protected set; }

        /// <summary>
        /// Gets or sets the suffix.
        /// </summary>
        public virtual Suffix Suffix { get; protected set; }

        /// <summary>
        /// Gets or sets the prefix.
        /// </summary>
        public virtual Prefix Prefix { get; protected set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public virtual string Title { get; protected set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public virtual string Phone { get; protected set; }

        /// <summary>
        /// Gets or sets the phone ext.
        /// </summary>
        public virtual string PhoneExt { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is foreign phone.
        /// </summary>
        public virtual bool IsForeignPhone { get; protected set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public virtual string Email { get; protected set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public virtual string ModifiedUser { get; protected set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public virtual DateTime? ModifiedDate { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is default.
        /// </summary>
        public virtual bool IsDefault { get; protected set; }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="institution">
        /// The institution.
        /// </param>
        /// <param name="firstName">
        /// The first name.
        /// </param>
        /// <param name="lastName">
        /// The last name.
        /// </param>
        /// <param name="middleName">
        /// The middle name.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        /// <param name="suffix">
        /// The suffix.
        /// </param>
        /// <param name="prefix">
        /// The prefix.
        /// </param>
        /// <param name="title">
        /// The title.
        /// </param>
        /// <param name="phone">
        /// The phone.
        /// </param>
        /// <param name="phoneExt">
        /// The phone ext.
        /// </param>
        /// <param name="isForeignPhone">
        /// The is foreign phone.
        /// </param>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="modifiedDate">
        /// The modified date.
        /// </param>
        /// <param name="isDefault">
        /// The is default.
        /// </param>
        public virtual void Update(HighSchools institution, string firstName, string lastName, string middleName, SyStatuses status, Suffix suffix, Prefix prefix, string title, string phone, string phoneExt, bool isForeignPhone, string email, string modifiedUser, DateTime? modifiedDate, bool isDefault)
        {
            this.Institution = institution;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.MiddleName = middleName;
            this.Status = status;
            this.Suffix = suffix;
            this.Prefix = prefix;
            this.Title = title;
            this.Phone = phone;
            this.PhoneExt = phoneExt;
            this.IsForeignPhone = isForeignPhone;
            this.Email = email;
            this.ModifiedUser = modifiedUser;
            this.ModifiedDate = modifiedDate;
            this.IsDefault = isDefault;
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        public virtual void Delete(string modifiedUser, SyStatuses status)
        {
            this.Status = status;
            this.ModifiedUser = modifiedUser;
        }
    }
}
