﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionType.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.SystemStuff.Intitution.InstitutionType
// </copyright>
// <summary>
//   The institution type.
//   Domain Entity for table <code>syInstitutionTypes</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff.Intitution
{
    using System.Collections.Generic;

    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The institution type.
    /// Domain Entity for table <code>syInstitutionTypes</code>
    /// </summary>
    public class InstitutionType : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// Gets or sets the high school list.
        /// </summary>
        public virtual IList<HighSchools> HighSchoolList { get; protected set; }
    }
}
