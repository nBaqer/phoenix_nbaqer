﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionAddress.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.SystemStuff.Intitution.InstitutionAddress
// </copyright>
// <summary>
//   The institution address.
// Domain Entity for table <code>syInstitutionAddresses</code> 
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff.Intitution
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.Domain.Catalogs;
    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The institution address.
    /// Domain Entity for table <code>syInstitutionAddresses</code>
    /// </summary>
    public class InstitutionAddress : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionAddress"/> class.
        /// </summary>
        public InstitutionAddress()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionAddress"/> class.
        /// </summary>
        /// <param name="institution">
        /// The institution.
        /// </param>
        /// <param name="addressType">
        /// The address type.
        /// </param>
        /// <param name="address1">
        /// The address 1.
        /// </param>
        /// <param name="address2">
        /// The address 2.
        /// </param>
        /// <param name="city">
        /// The city.
        /// </param>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="zipCode">
        /// The zip code.
        /// </param>
        /// <param name="isMailingAddress">
        /// The is mailing address.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="modifiedDate">
        /// The modified date.
        /// </param>
        /// <param name="country">
        /// The country.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        /// <param name="isInternational">
        /// The is international.
        /// </param>
        /// <param name="county">
        /// The county.
        /// </param>
        /// <param name="foreignCountry">
        /// The foreign country.
        /// </param>
        /// <param name="addressApto">
        /// The address APTO.
        /// </param>
        /// <param name="otherState">
        /// The other state.
        /// </param>
        /// <param name="isDefault">
        /// The is default.
        /// </param>
        /// <param name="foreignCounty">
        /// The foreign County.
        /// </param>
        public InstitutionAddress(HighSchools institution, PlAddressTypes addressType, string address1, string address2, string city, SystemStates state, string zipCode, bool isMailingAddress, string modifiedUser, DateTime? modifiedDate, Country country, SyStatuses status, bool isInternational, County county, string foreignCountry, string addressApto, string otherState, bool isDefault, string foreignCounty)
        {
            this.Institution = institution;
            this.AddressType = addressType;
            this.Address1 = address1;
            this.Address2 = address2;
            this.City = city;
            this.State = state;
            this.ZipCode = zipCode;
            this.IsMailingAddress = isMailingAddress;
            this.ModifiedUser = modifiedUser;
            this.ModifiedDate = modifiedDate;
            this.Country = country;
            this.Status = status;
            this.IsInternational = isInternational;
            this.County = county;
            this.ForeignCountry = foreignCountry;
            this.AddressApto = addressApto;
            this.OtherState = otherState;
            this.IsDefault = isDefault;
            this.ForeignCounty = foreignCounty;
        }

        /// <summary>
        /// Gets or sets the institution.
        /// </summary>
        public virtual HighSchools Institution { get; protected set; }

        /// <summary>
        /// Gets or sets the address type.
        /// </summary>
        public virtual PlAddressTypes AddressType { get; protected set; }

        /// <summary>
        /// Gets or sets the address 1.
        /// </summary>
        public virtual string Address1 { get; protected set; }

        /// <summary>
        /// Gets or sets the address 2.
        /// </summary>
        public virtual string Address2 { get; protected set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public virtual string City { get; protected set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public virtual SystemStates State { get; protected set; }

        /// <summary>
        /// Gets or sets the zip code.
        /// </summary>
        public virtual string ZipCode { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is mailing address.
        /// </summary>
        public virtual bool IsMailingAddress { get; protected set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public virtual string ModifiedUser { get; protected set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public virtual DateTime? ModifiedDate { get; protected set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public virtual Country Country { get; protected set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public virtual SyStatuses Status { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is international.
        /// </summary>
        public virtual bool IsInternational { get; protected set; }

        /// <summary>
        /// Gets or sets the county.
        /// </summary>
        public virtual County County { get; protected set; }

        /// <summary>
        /// Gets or sets the foreign country.
        /// </summary>
        public virtual string ForeignCountry { get; protected set; }

        /// <summary>
        /// Gets or sets the foreign county.
        /// </summary>
        public virtual string ForeignCounty { get; protected set; }

        /// <summary>
        /// Gets or sets the address APTO.
        /// </summary>
        public virtual string AddressApto { get; protected set; }

        /// <summary>
        /// Gets or sets the other state.
        /// </summary>
        public virtual string OtherState { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is default.
        /// </summary>
        public virtual bool IsDefault { get; protected set; }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="institution">
        /// The institution.
        /// </param>
        /// <param name="addressType">
        /// The address type.
        /// </param>
        /// <param name="address1">
        /// The address 1.
        /// </param>
        /// <param name="address2">
        /// The address 2.
        /// </param>
        /// <param name="city">
        /// The city.
        /// </param>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="zipCode">
        /// The zip code.
        /// </param>
        /// <param name="isMailingAddress">
        /// The is mailing address.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="modifiedDate">
        /// The modified date.
        /// </param>
        /// <param name="country">
        /// The country.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        /// <param name="isInternational">
        /// The is international.
        /// </param>
        /// <param name="county">
        /// The county.
        /// </param>
        /// <param name="foreignCountry">
        /// The foreign country.
        /// </param>
        /// <param name="addressApto">
        /// The address APTO.
        /// </param>
        /// <param name="otherState">
        /// The other state.
        /// </param>
        /// <param name="isDefault">
        /// The is default.
        /// </param>
        /// <param name="foreignCounty">
        /// The foreign County.
        /// </param>
        public virtual void Update(HighSchools institution, PlAddressTypes addressType, string address1, string address2, string city, SystemStates state, string zipCode, bool isMailingAddress, string modifiedUser, DateTime? modifiedDate, Country country, SyStatuses status, bool isInternational, County county, string foreignCountry, string addressApto, string otherState, bool isDefault, string foreignCounty)
        {
            this.Institution = institution;
            this.AddressType = addressType;
            this.Address1 = address1;
            this.Address2 = address2;
            this.City = city;
            this.State = state;
            this.ZipCode = zipCode;
            this.IsMailingAddress = isMailingAddress;
            this.ModifiedUser = modifiedUser;
            this.ModifiedDate = modifiedDate;
            this.Country = country;
            this.Status = status;
            this.IsInternational = isInternational;
            this.County = county;
            this.ForeignCountry = foreignCountry;
            this.AddressApto = addressApto;
            this.OtherState = otherState;
            this.IsDefault = isDefault;
            this.ForeignCounty = foreignCounty;
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        public virtual void Delete(string modifiedUser, SyStatuses status)
        {
            this.Status = status;
            this.ModifiedUser = modifiedUser;
        }
    }
}
