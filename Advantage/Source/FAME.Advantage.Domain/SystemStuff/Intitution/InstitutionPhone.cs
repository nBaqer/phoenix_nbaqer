﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionPhone.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.SystemStuff.Intitution.InstitutionPhone
// </copyright>
// <summary>
//   The institution phone.
//   Domain Entity for table <code>syInstitutionPhone</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff.Intitution
{
    using System;
    using Infrastructure.Entities;

    /// <summary>
    /// The institution phone.
    /// Domain Entity for table <code>syInstitutionPhone</code>
    /// </summary>
    public class InstitutionPhone : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionPhone"/> class.
        /// </summary>
        public InstitutionPhone()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionPhone"/> class.
        /// </summary>
        /// <param name="institution">
        /// The institution.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="phone">
        /// The phone.
        /// </param>
        /// <param name="modifiedDate">
        /// The modified date.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="isForeignPhone">
        /// The is foreign phone.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        /// <param name="isDefault">
        /// The is default.
        /// </param>
        public InstitutionPhone(HighSchools institution, SyPhoneType type, string phone, DateTime modifiedDate, string modifiedUser, bool isForeignPhone, SyStatuses status, bool? isDefault)
        {
            this.Institution = institution;
            this.Type = type;
            this.Phone = phone;
            this.ModifiedDate = modifiedDate;
            this.ModifiedUser = modifiedUser;
            this.IsForeignPhone = isForeignPhone;
            this.Status = status;
            this.IsDefault = isDefault;
        }

        /// <summary>
        /// Gets or sets the institution.
        /// </summary>
        public virtual HighSchools Institution { get; protected set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public virtual SyPhoneType Type { get; protected set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public virtual string Phone { get; protected set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public virtual DateTime ModifiedDate { get; protected set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public virtual string ModifiedUser { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is foreign phone.
        /// </summary>
        public virtual bool IsForeignPhone { get; protected set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public virtual SyStatuses Status { get; protected set; }

        /// <summary>
        /// Gets or sets the is default.
        /// </summary>
        public virtual bool? IsDefault { get; protected set; }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="institution">
        /// The institution.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="phone">
        /// The phone.
        /// </param>
        /// <param name="modifiedDate">
        /// The modified date.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="isForeignPhone">
        /// The is foreign phone.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        /// <param name="isDefault">
        /// The is default.
        /// </param>
        public virtual void Update(HighSchools institution, SyPhoneType type, string phone, DateTime modifiedDate, string modifiedUser, bool isForeignPhone, SyStatuses status, bool? isDefault)
        {
            this.Institution = institution;
            this.Type = type;
            this.Phone = phone;
            this.ModifiedDate = modifiedDate;
            this.ModifiedUser = modifiedUser;
            this.IsForeignPhone = isForeignPhone;
            this.Status = status;
            this.IsDefault = isDefault;
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        public virtual void Delete(string modifiedUser, SyStatuses status)
        {
            this.Status = status;
            this.ModifiedUser = modifiedUser;
        }
    }
}
