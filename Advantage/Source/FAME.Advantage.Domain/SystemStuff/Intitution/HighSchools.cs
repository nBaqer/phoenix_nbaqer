﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HighSchools.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Catalogs.HighSchools
// </copyright>
// <summary>
//   The high schools also known as Institution.
//   Domain Entity for table <code>syInstitutions</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff.Intitution
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.Domain.Campuses.CampusGroups;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Domain.SystemStuff;
    
    /// <summary>
    /// The high schools also known as Institution.
    /// Domain Entity for table <code>syInstitutions</code>
    /// </summary>
    public class HighSchools : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HighSchools"/> class.
        /// </summary>
        public HighSchools()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HighSchools"/> class.
        /// </summary>
        /// <param name="code">
        /// The HighSchools code.
        /// </param>
        /// <param name="description">
        /// The HighSchools description.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="modifiedDate">
        /// The modified date.
        /// </param>
        /// <param name="status">
        /// The system status.
        /// </param>
        /// <param name="campusGroup">
        /// The campus group.
        /// </param>
        /// <param name="level">
        /// The level.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="institutionImportType">
        /// The institution Import Type.
        /// </param>
        public HighSchools(string code, string description,  string modifiedUser, DateTime modifiedDate, SyStatuses status, CampusGroup campusGroup, AdLevels level, InstitutionType type, InstitutionImportType institutionImportType)
        {
            // ReSharper disable VirtualMemberCallInConstructor
            this.HsCode = code;
            this.HsDescrip = description;
            this.ModifiedUser = modifiedUser;
            this.ModifiedDate = modifiedDate;
            this.SyStatusesObj = status;
            this.CampusGroupObj = campusGroup;
            this.Level = level;
            this.Type = type;
            this.ImportType = institutionImportType;
           // ReSharper restore VirtualMemberCallInConstructor
         }

        /// <summary>
        /// Gets or sets the high school code.
        /// </summary>
        public virtual string HsCode { get; protected set; }

        /// <summary>
        /// Gets or sets the high school description (name).
        /// </summary>
        public virtual string HsDescrip { get; protected set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public virtual string ModifiedUser { get; protected set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public virtual DateTime ModifiedDate { get; protected set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public virtual SyStatuses SyStatusesObj { get; protected set; }

        /// <summary>
        /// Gets or sets the campus group.
        /// </summary>
        public virtual CampusGroup CampusGroupObj { get; protected set; }

        /// <summary>
        /// Gets or sets the lead list.
        /// </summary>
        public virtual IList<Lead> LeadList { get; protected set; }

        /// <summary>
        /// Gets or sets the level.
        /// </summary>
        public virtual AdLevels Level { get; protected set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public virtual InstitutionType Type { get; protected set; }

        /// <summary>
        /// Gets or sets the import type.
        /// </summary>
        public virtual InstitutionImportType ImportType { get; protected set; }

        /// <summary>
        /// Gets or sets the phones.
        /// </summary>
        public virtual IList<InstitutionPhone> Phones { get; protected set; }

        /// <summary>
        /// Gets or sets the addresses.
        /// </summary>
        public virtual IList<InstitutionAddress> Addresses { get; protected set; }

        /// <summary>
        /// Gets or sets the Contacts.
        /// </summary>
        public virtual IList<InstitutionContact> Contacts { get; protected set; }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="modifiedDate">
        /// The modified date.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        /// <param name="campusGroup">
        /// The campus group.
        /// </param>
        /// <param name="level">
        /// The level.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="institutionImportType">
        /// The institution Import Type.
        /// </param>
        public virtual void Update(string code, string description, string modifiedUser, DateTime modifiedDate, SyStatuses status, CampusGroup campusGroup, AdLevels level, InstitutionType type, InstitutionImportType institutionImportType)
        {
            this.HsCode = code;
            this.HsDescrip = description;
            this.ModifiedUser = modifiedUser;
            this.ModifiedDate = modifiedDate;
            this.SyStatusesObj = status;
            this.CampusGroupObj = campusGroup;
            this.Level = level;
            this.Type = type;
            this.ImportType = institutionImportType;
        }

        /// <summary>
        /// The update Institution Import Type.
        /// </summary>
        /// <param name="institutionImportType">
        /// The institution import type.
        /// </param>
        public virtual void Update(InstitutionImportType institutionImportType)
        {
            this.ImportType = institutionImportType;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="description">
        /// The description.
        /// </param>
        public virtual void Update(string description)
        {
            this.HsDescrip = description;
        }
    }
}