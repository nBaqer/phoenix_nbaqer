﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionImportType.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.SystemStuff.Intitution.InstitutionImportType
// </copyright>
// <summary>
//   The institution import type.
//   Domain Entity for table <code>syInstitutionImportTypes</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff.Intitution
{
    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The institution import type.
    /// Domain Entity for table <code>syInstitutionImportTypes</code>
    /// </summary>
    public class InstitutionImportType : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Description { get; protected set; }
    }
}
