﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.SystemStuff.Wapi;

namespace FAME.Advantage.Domain.SystemStuff
{
    public class SyWapiAllowedServices : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Internal constructor
        /// </summary>       
        public SyWapiAllowedServices()
        {
        }

        public SyWapiAllowedServices(int id, string code, string description, string url, bool isActive, IList<SyWapiSettings> wapiSettingsList)
        {
            base.ID = id;
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Code = code;
            Description = description;
            Url  = url;
            IsActive = isActive;
            WapiSettingsList = wapiSettingsList;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor 
        }

        public virtual string Code { get; protected set; }
        public virtual string Description { get; protected set; }
        public virtual string Url { get; protected set; }
        public virtual bool IsActive { get; protected set; }

        public virtual IList<SyWapiSettings> WapiSettingsList { get; protected set; }
        public virtual IList<SyWapiSettings> SecondWapingSettingList { get; protected set; }

        public virtual IList<SyWapiExternalCompanies> WapiExternalCompaniesList { get; protected set; }

        /// <summary>
        /// This only change the values with the new values
        /// but donot execute the update in db.
        /// </summary>
        /// <param name="model"></param>
        public virtual void Update(WapiCatalogsOutputModel model)
        {
            Code = model.Code;
            Description = model.Description;
            Url = model.Url;
            IsActive = model.IsActive;
        }
    }
}
