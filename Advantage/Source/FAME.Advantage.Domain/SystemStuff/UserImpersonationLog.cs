﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.StatusesOptions;
using NHibernate.Hql.Ast.ANTLR;

namespace FAME.Advantage.Domain.SystemStuff
{
    public class UserImpersonationLog : DomainEntityWithTypedID<int>
    {
        protected UserImpersonationLog()
        {
        }

        public UserImpersonationLog(string impUser, DateTime? logStart, DateTime? logEnd)
        {
            ImpersonatedUser = impUser;
            LogStart = logStart;
            LogEnd = logEnd;
        }


        public virtual string ImpersonatedUser { get; protected set; }
        public virtual DateTime? LogStart { get; protected set; }
        public virtual DateTime? LogEnd { get; protected set; }
        public virtual string ModUser { get; protected set; }

        public virtual string LStart
        {
            get { return LogStart == null ? "" : LogStart.Value.ToString("g"); }
            set { value = LogStart == null ? "" : LogStart.Value.ToString("g"); }
        }

        public virtual string LEnd
        {
            get { return LogEnd == null ? "" : LogEnd.Value.ToString("g"); }
            set { value = LogEnd == null ? "" : LogEnd.Value.ToString("g"); }
        }


        public virtual void SetEndDate(DateTime endDate)
        {
            LogEnd = endDate;
        }
    }
}



