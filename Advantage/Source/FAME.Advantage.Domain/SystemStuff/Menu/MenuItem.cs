﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff.ConfigurationAppSettings;
using FAME.Advantage.Domain.Users;

namespace FAME.Advantage.Domain.SystemStuff.Menu
{
    
    public class MenuItem : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// default constructor
        /// </summary>
        protected MenuItem()
        {
           
        }

        /// <summary>
        /// constructor - initialize members
        /// </summary>
        /// <param name="menuName"></param>
        /// <param name="displayName"></param>
        /// <param name="url"></param>
        /// <param name="displayOrder"></param>
        /// <param name="isPopup"></param>
        /// <param name="modDate"></param>
        /// <param name="modUser"></param>
        /// <param name="isActive"></param>
        /// <param name="resourceId"></param>
        /// <param name="hierarchyId"></param>
        /// <param name="itemType"></param>
        /// <param name="parent"></param>
        /// <param name="children"></param>
        public MenuItem(string menuName, string displayName, string url, int? displayOrder, bool isPopup, DateTime? modDate, User modUser, bool isActive, short? resourceId, 
            Guid hierarchyId, MenuItemType itemType, MenuItem parent, IList<MenuItem> children   )
        {
            MenuName = menuName;
            DisplayName = displayName;
            ResourceUrl = url;
            DisplayOrder = displayOrder;
            IsPopup = isPopup;
            ModDate = modDate;
            IsActive = isActive;
            ResourceId = resourceId;
            HierarchyId = hierarchyId;
            ItemType = itemType;
            ChildMenuItems = children;
        }

        //public domain properties
        public virtual string MenuName { get; protected set; }
        public virtual string DisplayName { get; protected set; }
        public virtual string ResourceUrl { get; protected set; }
        public virtual int? DisplayOrder { get; protected set; }
        public virtual bool IsPopup { get; protected set; }
        public virtual DateTime? ModDate { get; protected set; }
        public virtual string ModuleCode { get; set; }
        public virtual bool IsActive { get; protected set; }
        public virtual short? ResourceId { get; protected set; }
        public virtual Guid HierarchyId { get; protected set; }
        public virtual int? ParentId { get; protected set; }
        public virtual MenuItemType ItemType { get; set; }
        public virtual bool IsEnabled { get; protected set; }
        public virtual IList<MenuItem> ChildMenuItems { get; set; }
        
        

        public virtual string FullUrl { get; set; }
        public virtual IList<MenuItem> ChildrenForOutput { get; set; }

        /// <summary>
        /// Only add child items if not excluded
        /// </summary>
        /// <param name="campusId"></param>
        /// <returns></returns>
        public virtual bool FilterOutExclusions(List<int> exclusions )
        {

            //final list of children
            this.ChildrenForOutput = new List<MenuItem>();
            
            //iterate the children, and check whether this item is in the 
            //list of exclusions. If not, include it in the child output values
            foreach (var item in this.ChildMenuItems)
            {
                if (item.ResourceId != null)
                {
                    if(!exclusions.Contains((int)item.ResourceId))
                        this.ChildrenForOutput.Add(item);
                }
            }

            this.ChildrenForOutput = this.ChildrenForOutput.OrderBy(n => n.DisplayOrder).ToList();
            

            return true;
        }


        /// <summary>
        /// Build the url for item child item
        /// </summary>
        /// <param name="siteUri"></param>
        /// <param name="moduleCode"></param>
        /// <param name="campusId"></param>
        /// <param name="enabledResources"></param>
        /// /// <param name="includeAll"></param>
        public virtual void BuildUrl(string siteUri, string moduleCode, Guid campusId, IList<short> enabledResources, bool includeAll, bool? isImpersonating)
        {
            //set the page group item isenabled to true
            this.IsEnabled = true;

            bool isImp = isImpersonating != null && isImpersonating == true;

            //iterate through each child item.  If this is a page item, has a url and the full url 
            //has not yet been filled, build the url and query string
            foreach (var item in this.ChildMenuItems)
            {
                if (!string.IsNullOrEmpty(item.ResourceUrl) && (item.ItemType.ItemType.ToUpper() == "PAGE" || item.ItemType.ItemType.ToUpper() == "SUBMENUPAGE") && item.FullUrl == null)
                {
                    item.FullUrl = siteUri + item.ResourceUrl;
                    item.FullUrl += "?resid=" + item.ResourceId.ToString() + "&cmpid=" + campusId.ToString() + "&desc=" + item.DisplayName;
                    
                    //Avoid that this page open as new.
                    if (item.DisplayName == "Leads")
                    {
                        item.FullUrl += "&new=0";
                    }

                    if (item.MenuName.ToUpper() == "ADHOC")
                        item.FullUrl += "&mod=" + item.ModuleCode;
                    else
                        item.FullUrl += "&mod=" + moduleCode;
                    

                    if (item.IsPopup)
                        item.FullUrl += "&ispopup=1";

                    if (includeAll)
                        item.IsEnabled = true;
                    else
                        item.IsEnabled = enabledResources.Any(n => n.Equals(item.ResourceId));


                    if (item.MenuName.ToUpper() == "USER IMPERSONATION")
                        item.IsEnabled = includeAll || isImp;

                    // DE13304 to do nothing when the url is clicked so that it doenst display the page if not given access to
                    if (!item.IsEnabled)
                    {
                        item.FullUrl = "javascript:void(0);";
                    }
                }
            }
        }

       
        

    }
}
