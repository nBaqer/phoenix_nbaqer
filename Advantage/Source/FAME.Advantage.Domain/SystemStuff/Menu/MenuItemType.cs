﻿using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.SystemStuff.Menu
{
    public class MenuItemType : DomainEntityWithTypedID<int>
    {
        protected MenuItemType()
        {
        }

        public MenuItemType(string itemType)
        {
            ItemType = itemType;
        }

        
        public virtual string ItemType { get; protected set; }
    }
}
