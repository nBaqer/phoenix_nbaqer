﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemStatusChangeHistory.cs" company="FAME Inc.">
//   FAME.Advantage.Domain.SystemStuff
// </copyright>
// <summary>
//   Defines the SystemStatusChangeHistory type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.SystemStuff
{
    using System;
    using FAME.Advantage.Domain.Campuses;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Student.Enrollments;

    /// <summary>
    /// The system status change history.
    /// </summary>
    public class SystemStatusChangeHistory : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemStatusChangeHistory"/> class. 
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="campus">
        /// The campus.
        /// </param>
        /// <param name="originalStatus">
        /// The original status.
        /// </param>
        /// <param name="newStatus">
        /// The new status.
        /// </param>
        /// <param name="dateOfChange">
        /// The date of change.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="effectiveDate">
        /// The effective date.
        /// </param>
        /// <param name="caseNumber">
        /// The case number.
        /// </param>
        /// <param name="requestedBy">
        /// The requested by.
        /// </param>
        public SystemStatusChangeHistory(Enrollment enrollment, Campus campus, UserDefStatusCode originalStatus, UserDefStatusCode newStatus, DateTime dateOfChange, string modifiedUser, DateTime effectiveDate, string caseNumber, string requestedBy)
        {
            this.Enrollment = enrollment;
            this.OriginalStatus = originalStatus;
            this.NewStatus = newStatus;
            this.ModifiedDate = dateOfChange;
            this.ModifiedUser = modifiedUser;
            this.DateOfChange = effectiveDate;
            this.CaseNumber = caseNumber;
            this.RequestedBy = requestedBy;
            this.Campus = campus;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemStatusChangeHistory"/> class. 
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="campus">
        /// The campus.
        /// </param>
        /// <param name="originalStatus">
        /// The original status.
        /// </param>
        /// <param name="newStatus">
        /// The new status.
        /// </param>
        /// <param name="dateOfChange">
        /// The date of change.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="effectiveDate">
        /// The effective date.
        /// </param>
        /// <param name="caseNumber">
        /// The case number.
        /// </param>
        /// <param name="requestedBy">
        /// The requested by.
        /// </param>
        /// <param name="dropReasonId">
        /// The drop reason id.
        /// </param>
        /// <param name="lda">
        /// The LDA.
        /// </param>
        public SystemStatusChangeHistory(Guid id, Enrollment enrollment, Campus campus, UserDefStatusCode originalStatus, UserDefStatusCode newStatus, DateTime dateOfChange, string modifiedUser, DateTime effectiveDate, string caseNumber, string requestedBy, Guid? dropReasonId, DateTime? lda)
        {
            this.ID = id;
            this.Enrollment = enrollment;
            this.OriginalStatus = originalStatus;
            this.NewStatus = newStatus;
            this.ModifiedDate = dateOfChange;
            this.ModifiedUser = modifiedUser;
            this.DateOfChange = effectiveDate;
            this.CaseNumber = caseNumber;
            this.RequestedBy = requestedBy;
            this.DropReasonId = dropReasonId;
            this.Lda = lda;
            this.Campus = campus;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemStatusChangeHistory"/> class. 
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="campus">
        /// The campus.
        /// </param>
        /// <param name="originalStatus">
        /// The original status.
        /// </param>
        /// <param name="newStatus">
        /// The new status.
        /// </param>
        /// <param name="dateOfChange">
        /// The date of change.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="effectiveDate">
        /// The effective date.
        /// </param>
        /// <param name="caseNumber">
        /// The case number.
        /// </param>
        /// <param name="requestedBy">
        /// The requested by.
        /// </param>
        /// <param name="dropReasonId">
        /// The drop reason id.
        /// </param>
        /// <param name="lda">
        /// The LDA.
        /// </param>
        public SystemStatusChangeHistory(Enrollment enrollment, Campus campus, UserDefStatusCode originalStatus, UserDefStatusCode newStatus, DateTime dateOfChange, string modifiedUser, DateTime effectiveDate, string caseNumber, string requestedBy, Guid? dropReasonId, DateTime? lda)
        {
            this.Enrollment = enrollment;
            this.OriginalStatus = originalStatus;
            this.NewStatus = newStatus;
            this.ModifiedDate = dateOfChange;
            this.ModifiedUser = modifiedUser;
            this.DateOfChange = effectiveDate;
            this.CaseNumber = caseNumber;
            this.RequestedBy = requestedBy;
            this.DropReasonId = dropReasonId;
            this.Lda = lda;
            this.Campus = campus;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemStatusChangeHistory"/> class.
        /// </summary>
        protected SystemStatusChangeHistory()
        {
        }

        /// <summary>
        /// Gets or sets the enrollment.
        /// </summary>
        public virtual Enrollment Enrollment { get; protected set; }

        /// <summary>
        /// Gets or sets the original status.
        /// </summary>
        public virtual UserDefStatusCode OriginalStatus { get; protected set; }

        /// <summary>
        /// Gets or sets the new status.
        /// </summary>
        public virtual UserDefStatusCode NewStatus { get; protected set; }

        /// <summary>
        /// Gets or sets the campus.
        /// </summary>
        public virtual Campus Campus { get; protected set; }

        /// <summary>
        /// Gets or sets the Modified Date. Also known of the date of change
        /// </summary>
        public virtual DateTime? ModifiedDate { get; protected set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public virtual string ModifiedUser { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is reversal.
        /// </summary>
        public virtual bool IsReversal { get; protected set; }

        /// <summary>
        /// Gets or sets the drop reason id.
        /// </summary>
        public virtual Guid? DropReasonId { get; protected set; }

        /// <summary>
        /// Gets or sets the Date of Change. Also Known as Effective Date
        /// </summary>
        public virtual DateTime? DateOfChange { get; protected set; }

        /// <summary>
        /// Gets or sets the LDA.
        /// </summary>
        public virtual DateTime? Lda { get; protected set; }

        /// <summary>
        /// Gets or sets the case number.
        /// </summary>
        public virtual string CaseNumber { get; protected set; }

        /// <summary>
        /// Gets or sets the requested by.
        /// </summary>
        public virtual string RequestedBy { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether have backup of Advantage Database before of deleting attendance and grades posted.
        /// </summary>
        public virtual bool? HaveBackup { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether have client confirmation to delete attendance and grades posted.
        /// </summary>
        public virtual bool? HaveClientConfirmation { get; protected set; }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="originalStatus">
        /// The original status.
        /// </param>
        /// <param name="modifiedDate">
        /// The modified date.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        public virtual void Update(UserDefStatusCode originalStatus, DateTime modifiedDate, string modifiedUser)
        {
            this.OriginalStatus = originalStatus;
            this.ModifiedUser = modifiedUser;
            this.ModifiedDate = modifiedDate;
        }

        /// <summary>
        /// The add millisecond to date of change.
        /// </summary>
        public virtual void AddMillisecondToDateOfChange()
        {
            if (this.DateOfChange != null) 
            { 
                this.DateOfChange = this.DateOfChange.Value.AddMilliseconds(1000);
            }
        }


        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="haveBackup">
        /// The have backup.
        /// </param>
        /// <param name="haveClientConfirmation">
        /// The have client confirmation.
        /// </param>
        public virtual void Update(bool haveBackup, bool haveClientConfirmation)
        {
            this.HaveBackup = haveBackup;
            this.HaveClientConfirmation = haveClientConfirmation;
        }
    }
}
