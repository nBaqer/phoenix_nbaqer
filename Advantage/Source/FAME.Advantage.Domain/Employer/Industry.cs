﻿using FAME.Advantage.Domain.Campuses;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FAME.Advantage.Domain.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.SystemStuff;


namespace FAME.Advantage.Domain.Employer
{
    public class Industry : DomainEntity
    {
        protected Industry() { }

       
        public virtual string Code { get; protected set; }
        public virtual string Description { get; protected set; }
        public virtual CampusGroup CampusGroup { get; protected set; }


    }
}

