﻿using FAME.Advantage.Domain.Campuses;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FAME.Advantage.Domain.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.SystemStuff;


namespace FAME.Advantage.Domain.Employer
{
    public class Employer : DomainEntity
    {
        protected Employer() { }

        public Employer(string employerName, string address, string city, SystemStates state, string zip, string email)
        {
            EmployerName = employerName;
            Address = address;
            City = city;
            State = state;
            Zip = zip;
            Email = email;
        }

        public virtual string EmployerName { get; protected set; }
        public virtual string Address { get; protected set; }
        public virtual string City { get; protected set; }
        
        public virtual string Zip { get; protected set; }
        public virtual string Email { get; protected set; }

        public virtual SystemStates State { get; protected set; }
        public virtual string Phone { get; set; }

        public virtual string ImageUrl { get; set; }
        public virtual string NotFoundImageUrl { get; set; }

        public virtual CampusGroup CampusGroup { get; protected set; }

        public virtual string SearchCampusStringList { get; protected set; }

        public virtual Industry Industry { get; protected set; }

        public virtual DateTime? ModDate { get; protected set; }
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Fill the campus search string list
        /// </summary>
        /// <param name="siteUrl"></param>
        public virtual void GetImageUrl(string siteUrl)
        {
            this.ImageUrl = siteUrl + "/images/face75.png";
            this.NotFoundImageUrl = this.ImageUrl;

            foreach (var c in CampusGroup.Campuses)
            {
                this.SearchCampusStringList += c.ID.ToString() + ";";
            }
        }


        /// <summary>
        /// Fill the campus search string list
        /// </summary>
        public virtual void GetCampusStringList()
        {
            foreach (var c in this.CampusGroup.Campuses)
            {
                this.SearchCampusStringList = c.ID + ";";
            }
        }
    }
}