using System;
using System.Linq.Expressions;

namespace FAME.Advantage.Domain.Infrastructure.Entities
{
    public interface IDomainQueryWithTypedID<TEntity, TId> 
        where TEntity : DomainEntityWithTypedID<TId>
    {
        Expression<Func<TEntity, bool>> Expression { get; }
    }
}