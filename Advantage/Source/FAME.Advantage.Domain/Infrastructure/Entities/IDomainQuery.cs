using System;

namespace FAME.Advantage.Domain.Infrastructure.Entities
{
    public interface IDomainQuery<TEntity> : IDomainQueryWithTypedID<TEntity, Guid> 
        where TEntity : DomainEntityWithTypedID<Guid> { }
}