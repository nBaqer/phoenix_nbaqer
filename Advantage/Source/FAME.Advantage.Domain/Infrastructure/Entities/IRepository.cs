using System;

namespace FAME.Advantage.Domain.Infrastructure.Entities
{
	public interface IRepository : IRepositoryWithTypedID<Guid> { }
}