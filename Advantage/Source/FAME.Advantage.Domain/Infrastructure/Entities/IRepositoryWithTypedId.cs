﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRepositoryWithTypedId.cs" company="FAME">
//   2013
// </copyright>
// <summary>
//   Defines the IRepositoryWithTypedID type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Infrastructure.Entities
{
    using System.Collections.Generic;
    using System.Linq;

    using NHibernate;

    /// <summary>
    /// The <code>RepositoryWithTypedID</code> interface.
    /// </summary>
    /// <typeparam name="TId">
    /// The type of parameter
    /// </typeparam>
    public interface IRepositoryWithTypedID<TId>
    {
        /// <summary>
        /// Gets the session.
        /// </summary>
        ISession Session { get; }

        void SaveAll<TEntity>(IEnumerable<DomainEntityWithTypedID<TId>> entities)
            where TEntity : DomainEntityWithTypedID<TId>;

        /// <summary>
        ///  The update all without flush.
        /// </summary>
        /// <param name="entities">
        /// The entities.
        /// </param>
        void UpdateAll(IEnumerable<DomainEntityWithTypedID<TId>> entities);

        void SaveOnly<TEntity>(TEntity entity)
            where TEntity : DomainEntityWithTypedID<TId>;

        void Save<TEntity>(TEntity entity)
            where TEntity : DomainEntityWithTypedID<TId>;

        void SaveAndFlush<TEntity>(TEntity entity)
            where TEntity : DomainEntityWithTypedID<TId>;

        void Merge<TEntity>(TEntity entity)
            where TEntity : DomainEntityWithTypedID<TId>;

        void MergeAndFlush<TEntity>(TEntity entity)
            where TEntity : DomainEntityWithTypedID<TId>;

        void Evict<TEntity>(TEntity entity)
            where TEntity : DomainEntityWithTypedID<TId>;

        TEntity Get<TEntity>(TId id)
            where TEntity : DomainEntityWithTypedID<TId>;

        TEntity Load<TEntity>(TId id)
            where TEntity : DomainEntityWithTypedID<TId>;

        void Delete<TEntity>(TEntity entity)
            where TEntity : DomainEntityWithTypedID<TId>;

        IQueryable<TEntity> Query<TEntity>()
            where TEntity : DomainEntityWithTypedID<TId>;

        IQueryable<TEntity> Query<TEntity>(IDomainQueryWithTypedID<TEntity, TId> whereQuery)
            where TEntity : DomainEntityWithTypedID<TId>;

        IQueryable<TEntity> Evict<TEntity>(IQueryable<TEntity> queryable)
            where TEntity : DomainEntityWithTypedID<TId>;

        void Update<TEntity>(TEntity entity)
            where TEntity : DomainEntityWithTypedID<TId>;

        void UpdateAndFlush<TEntity>(TEntity entity)
        where TEntity : DomainEntityWithTypedID<TId>;
    }
}
