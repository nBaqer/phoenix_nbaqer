﻿using System;

namespace FAME.Advantage.Domain.Infrastructure.Entities
{
	/// <summary>
	/// Class basic for Domain.
	/// </summary>
    [Serializable]
	public class DomainEntity : DomainEntityWithTypedID<Guid>
	{
	    /// <summary>
	    /// Constructor
	    /// </summary>
        public DomainEntity()
	    {
	        ID = Guid.Empty;
	    }
	}
}
