﻿namespace FAME.Advantage.Domain.Infrastructure.KeyGen
{
    public class SecretKeyGenerator
    {
        private readonly PasswordGeneratorOptions _options;

        public SecretKeyGenerator()
        {
            _options = new PasswordGeneratorOptions {
                CharacterSets = new[] {
                    PasswordCharacterSet.UPPERCASE,
                    PasswordCharacterSet.LOWERCASE,
                    PasswordCharacterSet.NUMERIC,
                    PasswordCharacterSet.XML_SAFE_SYMBOLS,
                },
                RequiredSets = 4,
                AllowConsecutiveCharacters = false,
                AllowRepeatingCharacters = false,
                MaximumLength = 80,
                MinimumLength = 80,
            };
        }

        public string NewKey()
        {
            return new PasswordGenerator(_options).Generate();
        }
    }
}
