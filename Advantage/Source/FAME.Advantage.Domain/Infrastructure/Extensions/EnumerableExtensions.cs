﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace FAME.Advantage.Domain.Infrastructure.Extensions
{
    public static class EnumerableExtensions
    {
        [DebuggerStepThrough]
        public static void Each<T>(this IEnumerable<T> values, Action<T, int> eachAction)
        {
            int index = 0;
            foreach (T item in values) {
                eachAction(item, index++);
            }
        }

        [DebuggerStepThrough]
        public static void Each<T>(this IEnumerable<T> values, Action<T> eachAction)
        {
            foreach (T item in values) {
                eachAction(item);
            }
        }

        [DebuggerStepThrough]
        public static void Each(this IEnumerable values, Action<object> eachAction)
        {
            foreach (object item in values) {
                eachAction(item);
            }
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> item)
        {
            return item == null || !item.Any();
        }
    }
}
