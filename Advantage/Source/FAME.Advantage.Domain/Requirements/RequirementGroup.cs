﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequirementGroup.cs" company="FAME Inc. ">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Requirements.RequirementGroup
// </copyright>
// <summary>
//   The requirement group.
//   Domain Entity for table ADREQSGroups.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Requirements
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using FAME.Advantage.Domain.Campuses.CampusGroups;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Infrastructure.Extensions;
    using FAME.Advantage.Domain.StatusesOptions;
    using FAME.Advantage.Domain.SystemStuff.Resources;

    using NHibernate.Linq;

    /// <summary>
    /// The requirement group.
    /// Domain Entity for table ADREQGroups.
    /// </summary>
    public class RequirementGroup : DomainEntity
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RequirementGroup"/> class.
        /// </summary>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="mandatory">
        /// The mandatory.
        /// </param>
        /// <param name="requirementGroupDefinitions">
        /// The requirement group definitions.
        /// </param>
        /// <exception cref="ArgumentException">
        /// ArgumentException if requirementGroupDefinitions is null or empty
        /// </exception>
        public RequirementGroup(string code, string description, bool? mandatory, IList<RequirementGroupDefinition> requirementGroupDefinitions)
        {
            if (requirementGroupDefinitions.IsNullOrEmpty())
            {
                throw new ArgumentException("requirementGroupDefinitions cannot be null or empty");
            }

            this.Code = code;
            this.Description = description;
            this.IsMandatory = mandatory;
            this.RequirementGroupDefinitions = requirementGroupDefinitions;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequirementGroup"/> class.
        /// </summary>
        protected RequirementGroup()
        {
        }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Gets or sets the is mandatory.
        /// </summary>
        public virtual bool? IsMandatory { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public virtual StatusOption Status { get; set; }

        /// <summary>
        /// Gets or sets the campus group.
        /// </summary>
        public virtual CampusGroup CampusGroup { get; set; }

        /// <summary>
        /// Gets or sets the lead group requirement group bridge.
        /// </summary>
        public virtual IList<LeadGroupRequirementGroupBridge> LeadGroupRequirementGroupBridge { get; protected set; }

        /// <summary>
        /// Gets or sets the requirement group definitions.
        /// </summary>
        public virtual IList<RequirementGroupDefinition> RequirementGroupDefinitions { get; protected set; }

        /// <summary>
        /// Gets or sets the requirement program version bridge.
        /// </summary>
        public virtual IList<RequirementProgramVersionBridge> RequirementProgramVersionBridge { get; protected set; }

        /// <summary>
        /// Gets or sets the lead group id.
        /// This value is set when Method SetLeadGroupId is called to pass the one to one association with the Lead Group.
        /// Call the SetLeadGroupId before calling GetMinimumRequirementRequired when trying to query the Requirement Group based on a Lead Group 
        /// Example: this.repository.Query RequirementGroup ().Where(x=> x.Expression).ToList().Where(x=> x.SetLeadGroupId(LeadGroupId));
        /// </summary>
        public virtual Guid LeadGroupId { get; protected set; }

        /// <summary>
        /// Gets or sets the list of lead groups id.
        /// This value is set when Method SetLeadGroupsId is called to pass the one to many association with the Lead Groups.
        /// Call the SetLeadGroupsId(List o Guid) before calling GetTotalRequirementsForEnrollment when trying to query the Requirement Group based on a Lead Group 
        /// Example: this.repository.Query RequirementGroup ().Where(x=> x.Expression).ToList().Where(x=> x.SetListOfLeadGroupsId(LeadGroupId));
        /// </summary>
        public virtual List<Guid> LeadGroupsId { get; protected set; }


        /// <summary>
        /// Gets or sets the lead guid.
        /// </summary>
        public virtual Guid LeadGuid { get; protected set; }


        /// <summary>
        /// The set lead group id.
        /// </summary>
        /// <param name="leadGroupId">
        /// The lead group id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool SetLeadGroupId(Guid leadGroupId)
        {
            this.LeadGroupId = leadGroupId;
            return true;
        }

        /// <summary>
        /// The set lead groups id.
        /// </summary>
        /// <param name="leadGroupsId">
        /// The lead groups id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool SetListOfLeadGroupsId(List<Guid> leadGroupsId)
        {
            this.LeadGroupsId = leadGroupsId;
            return true;
        }

        /// <summary>
        /// The set lead guid.
        /// </summary>
        /// <param name="leadGuidId">
        /// The lead guid id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool SetLeadGuid(Guid leadGuidId)
        {
            this.LeadGuid = leadGuidId;
            return true;
        }

        /// <summary>
        /// The get requirements for enrollments.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public virtual List<Requirement> GetRequirementsForEnrollments()
        {
            var requirementDefinition =
                this.RequirementGroupDefinitions.Where(
                    n =>
                    this.LeadGroupsId.Contains(n.LeadGroup.ID) &&
                    n.RequirementGroup.Status.StatusCode == "A" && n.Requirement.RequiredForEnrollment == true
                    && n.Requirement.Status.StatusCode == "A"
                    && n.Requirement.SystemModule.ID == (int)SystemModule.ESystemModule.Admissions).ToList();

            var requirements = requirementDefinition.Where(x => x.SetRequirementIsRequired()).Select(x => x.Requirement)
                 .ToList();

            requirements.ForEach(x => x.SetIsSchoolLevel());

            var inSchoolRequirements = requirements.Where(x => x.IsSchoolLevel).Distinct().ToList();

            return inSchoolRequirements;
        }

        /// <summary>
        /// The get minimum requirement required.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public virtual int GetMinimumRequirementRequired()
        {
            var leadGroup = this.LeadGroupRequirementGroupBridge.FirstOrDefault(x => x.LeadGroup.ID == this.LeadGroupId);
            if (leadGroup != null && leadGroup.MinimumNumberRequirements != null)
            {
                return leadGroup.MinimumNumberRequirements.Value;
            }

            return 0;
        }

        /// <summary>
        /// The have requirements for enrollment.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool HaveRequirementsForEnrollment()
        {
            return this.GetRequirementsForEnrollments().Any();
        }
    }
}