﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequirementEffectiveDates.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Requirements
// </copyright>
// <summary>
//   Defines the RequirementEffectiveDates type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Requirements
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The requirement effective dates (bridge table for the requirement and it's details effective dates, if is Mandatory for all the lead groups).
    /// Domain entity for table adREQSEffectiveDates
    /// </summary>
    public class RequirementEffectiveDates : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequirementEffectiveDates"/> class.
        /// </summary>
        public RequirementEffectiveDates()
        {
        }

        /// <summary>
        /// Gets or sets the start date of the Requirement. Used for filtering purpose to show Requirements where the current date is higher or equal to this date. 
        /// If null not validation is needed.
        /// </summary>
        public virtual DateTime? StartDate { get; protected set; }

        /// <summary>
        /// Gets or sets the end date of the Requirements.  Used for filtering purpose to show Requirements where the current date is prior or equal to this date. 
        /// If null not validation is needed.
        /// </summary>
        public virtual DateTime? EndDate { get; protected set; }

        /// <summary>
        /// Gets or sets the minimum score.
        /// This is higher than 0 when the type of requirement is a test.
        /// </summary>
        public virtual decimal? MinimumScore { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is mandatory for all the lead groups.
        /// </summary>
        public virtual bool IsMandatory { get; protected set; }

        /// <summary>
        /// Gets or sets the valid days.
        /// </summary>
        public virtual int? ValidDays { get; protected set; }

        /// <summary>
        /// Gets or sets the requirement.
        /// </summary>
        public virtual Requirement Requirement { get; protected set; }

        /// <summary>
        /// Gets or sets the requirement lead group bridge (based on the relationship with Requirement Effective Dates).
        /// Domain Entity for bridge table adREQLeadGroups.
        /// Table adREQLeadGroups holds the association between a Lead Group and a Requirement Effective Date (table adREQSEffectiveDates).
        /// Requirement Effective Date bridge table holds the association with requirement.
        /// </summary>
        public virtual IList<RequirementLeadGroupBridge> RequirementLeadGroupBridge { get; protected set; }
    }
}   