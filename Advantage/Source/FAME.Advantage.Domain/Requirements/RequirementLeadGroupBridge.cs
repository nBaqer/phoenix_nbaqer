﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequirementLeadGroupBridge.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Requirements.RequirementLeadGroupBridge
// </copyright>
// <summary>
//   The requirement lead group bridge (based on the relationship with Requirement Effective Dates).
//   Domain Entity for bridge table adREQLeadGroups.
//   Table adREQLeadGroups holds the association between a Lead Group and a Requirement Effective Date (table adREQSEffectiveDates).
//   Requirement Effective Date bridge table holds the association with requirement.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Requirements
{
    using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Lead;

    /// <summary>
    /// The requirement lead group bridge (based on the relationship with Requirement Effective Dates).
    /// Domain Entity for bridge table <code>adREQLeadGroups</code> .
    /// Table adREQLeadGroups holds the association between a Lead Group and a Requirement Effective Date (table adREQSEffectiveDates).
    /// Requirement Effective Date bridge table holds the association with requirement.
    /// </summary>
    public class RequirementLeadGroupBridge : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequirementLeadGroupBridge"/> class.
        /// </summary>
        protected RequirementLeadGroupBridge()
        {
        }

        /// <summary>
        /// Gets or sets the is required.
        /// This boolean defines whether the Requirement is Required for the Lead group or Not.
        /// </summary>
        public virtual bool? IsRequired { get; protected set; }

        /// <summary>
        /// Gets or sets the lead group.
        /// </summary>
        public virtual LeadGroups LeadGroup { get; protected set; }

        /// <summary>
        /// Gets or sets the requirement effective dates (bridge for getting the Requirement and the Effective Dates of the Requirement).
        /// </summary>
        public virtual RequirementEffectiveDates RequirementEffectiveDates { get; protected set; }
    }
}