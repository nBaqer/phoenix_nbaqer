﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.StatusesOptions;

namespace FAME.Advantage.Domain.Requirements
{
    /// <summary>
    /// Domain for table adEdLvls
    /// </summary>
    public class EducationLevel : DomainEntity
    {
        /// <summary>
        /// Private constructor
        /// </summary>
        protected EducationLevel()
        {            
        }

        /// <summary>
        /// Constructor 
        /// </summary>
        /// <param name="cd"></param>
        /// <param name="desc"></param>
        protected EducationLevel( string cd, string desc)
        {
// ReSharper disable DoNotCallOverridableMethodsInConstructor
            Code = cd;
            Description = desc;
 // ReSharper restore DoNotCallOverridableMethodsInConstructor
       }

        /// <summary>
        /// Education level Code
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Education level description
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// If this row is active or not
        /// </summary>
        public virtual StatusOption Status { get; set; }

        /// <summary>
        /// Campus group associated
        /// </summary>
        public virtual CampusGroup CampusGroup { get; protected set; }
        
        /// <summary>
        /// List of lead with the education level
        /// </summary>
        public virtual IList<Lead.Lead> LeadList { get; protected set; }
    }
}
