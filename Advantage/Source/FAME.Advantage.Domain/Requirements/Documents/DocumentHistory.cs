﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Requirements.Documents
{
    public class DocumentHistory : DomainEntity
    {
        protected DocumentHistory() { }

        public DocumentHistory( string fileName, string ext, string docType, Guid docReqId, Guid leadId, string modUser, int? moduleId, string displayName)
        {
            
            FileName = fileName;
            FileExtension = ext;
            DocumentType = docType;
            DisplayName = displayName;
            DocumentId = docReqId;
            LeadId = leadId;
            ModUser = modUser;
            ModuleId = moduleId;
            UploadDate = DateTime.Now;
        }

        public virtual string FileName { get; protected set; }
        public virtual string FileExtension { get; protected set; }
        public virtual string DocumentType { get; protected set; }
        public virtual string DisplayName { get; protected set; }
        public virtual DateTime? UploadDate { get; protected set; }
        public virtual Guid DocumentId { get; protected set; }
        public virtual Guid LeadId { get; protected set; }

        public virtual string ModUser { get; protected set; }
        public virtual int? ModuleId { get; protected set; }
        



        /// <summary>
        /// update document object
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="ext"></param>
        /// <param name="docType"></param>
        /// <param name="displayName"></param>
        /// <param name="docReqId"></param>
        /// <param name="leadId"></param>
        /// <param name="modUser"></param>
        /// <param name="moduleId"></param>
        /// <param name="displayName"></param>
        public virtual void UpdateExistingDocument(string fileName, string ext, string docType, Guid docReqId, Guid leadId, string modUser, int? moduleId, string displayName)
        {
            FileName = fileName;
            FileExtension = ext;
            DocumentType = docType;
            DisplayName = displayName;
            DocumentId = docReqId;
            LeadId = leadId;
            ModUser = modUser;
            ModuleId = moduleId;

        }


    }
}
