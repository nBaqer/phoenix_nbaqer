﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdLeadTranReceived.cs" company="FAME Inc.">
//   FAME 2016
//   FAME.Advantage.Domain.Requirements.Documents.AdLeadTranReceived
// </copyright>
// <summary>
//   Defines the AdLeadTranReceived type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Requirements.Documents
{
    using System;

    using Infrastructure.Entities;

    /// <summary>
    /// The admissions lead tran received.
    /// </summary>
    public class AdLeadTranReceived : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AdLeadTranReceived"/> class.
        /// </summary>
        public AdLeadTranReceived()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdLeadTranReceived"/> class.
        /// </summary>
        /// <param name="lead">
        /// The lead.
        /// </param>
        /// <param name="requirement">
        /// The requirement.
        /// </param>
        /// <param name="receivedDate">
        /// The received date.
        /// </param>
        /// <param name="modUser">
        /// The mod user.
        /// </param>
        /// <param name="overridden">
        /// The overridden.
        /// </param>
        /// <param name="overReason">
        /// The over reason.
        /// </param>
        public AdLeadTranReceived(Lead.Lead lead, Requirement requirement, DateTime receivedDate, string modUser, bool overridden, string overReason)
        {
            this.Lead = lead;
            this.Requirement = requirement;
            this.ReceivedDate = receivedDate;
            this.IsApproved = true;
            this.ApprovalDate = receivedDate;
            this.ModDate = DateTime.Now;
            this.Override = overridden;
            this.OverrideReason = overReason;
            this.ModUser = modUser;
        }

        /// <summary>
        /// Gets or sets the lead.
        /// </summary>
        public virtual Lead.Lead Lead { get; protected set; }

        /// <summary>
        /// Gets or sets the received date.
        /// </summary>
        public virtual DateTime ReceivedDate { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is approved.
        /// </summary>
        public virtual bool IsApproved { get; protected set; }

        /// <summary>
        /// Gets or sets the approval date.
        /// </summary>
        public virtual DateTime? ApprovalDate { get; protected set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public virtual DateTime? ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets the user full name.
        /// </summary>
        public virtual string UserFullName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether override.
        /// </summary>
        public virtual bool Override { get; protected set; }

        /// <summary>
        /// Gets or sets the override reason.
        /// </summary>
        public virtual string OverrideReason { get; protected set; }

        /// <summary>
        /// Gets or sets the requirement.
        /// </summary>
        public virtual Requirement Requirement { get; protected set; }

        /// <summary>
        /// The save trans record.
        /// </summary>
        /// <param name="lead">
        /// The lead.
        /// </param>
        /// <param name="requirement">
        /// The requirement.
        /// </param>
        /// <param name="receivedDate">
        /// The received date.
        /// </param>
        /// <param name="modUser">
        /// The mod user.
        /// </param>
        /// <param name="overridden">
        /// The overridden.
        /// </param>
        /// <param name="overReason">
        /// The over reason.
        /// </param>
        public virtual void SaveTransRecord(Lead.Lead lead, Requirement requirement, DateTime receivedDate, string modUser, bool overridden, string overReason)
        {
            this.Lead = lead;
            this.Requirement = requirement;
            this.ReceivedDate = receivedDate;
            this.ModUser = modUser;
            this.ModDate = DateTime.Now;
            this.Override = overridden;
            this.OverrideReason = overReason;
            this.IsApproved = true;
            this.ApprovalDate = DateTime.Now;
        }

        /// <summary>
        /// The set is approved.
        /// </summary>
        /// <param name="isApproved">
        /// The is approved.
        /// </param>
        public virtual void SetIsApproved(bool isApproved)
        {
            this.IsApproved = isApproved;
        }

        /// <summary>
        /// The set modified date.
        /// </summary>
        /// <param name="date">
        /// The date.
        /// </param>
        public virtual void SetModifiedDate(DateTime date)
        {
            this.ModDate = date;
        }
    }
}
