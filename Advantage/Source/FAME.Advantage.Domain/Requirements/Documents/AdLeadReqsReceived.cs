﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Requirements.Documents
{
    public class AdLeadReqsReceived : DomainEntity
    {
        public AdLeadReqsReceived(Domain.Lead.Lead lead, Requirement requirement, DateTime? receivedDate, bool overridden, string overReason, string modUser)
        {
            Lead = lead;
            Requirement = requirement;
            ReceivedDate = receivedDate;
            ApprovalDate = receivedDate;
            Override = overridden;
            OverrideReason = overReason;
            ModUser = modUser;
            ModDate = DateTime.Now;
            IsApproved = true;
        }


        public AdLeadReqsReceived()
        {
        }

        public virtual Lead.Lead Lead { get; protected set; }
        public virtual DateTime? ReceivedDate { get; protected set; }
        public virtual bool? IsApproved { get; protected set; }
        public virtual DateTime? ApprovalDate { get; protected set; }
        public virtual DateTime? ModDate { get; protected set; }
        
        /// <summary>
        /// Can be null
        /// </summary>
        public virtual string ModUser { get; protected set; }

        public virtual string UserFullName { get; set; }

        public virtual bool Override { get; protected set; }
        public virtual string  OverrideReason { get; protected set; }


        public virtual Requirement Requirement { get; protected set; }



        public virtual void UpdateExistingRecord(Domain.Lead.Lead lead, Requirement requirement, DateTime? receivedDate,bool overridden, string overReason, string modUser)
        {
            Lead = lead;
            Requirement = requirement;
            ReceivedDate = receivedDate;
            Override = overridden;
            OverrideReason = overReason;
            ModUser = modUser;

        }
    }


}
