﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadDocument.cs" company="FAME Inc">
//   2016
//   FAME.Advantage.Domain.Requirements.Documents.LeadDocument    
// </copyright>
// <summary>
//   Domain entity for table adLeadDocReceived
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Requirements.Documents
{
    using System;

    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// Domain entity for table adLeadDocsReceived
    /// </summary>
    public class LeadDocument : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadDocument"/> class. 
        /// </summary>
        /// <param name="dateReceived">
        /// The Date Received
        /// </param>
        /// <param name="docStatus">
        /// the The Document Status
        /// </param>
        /// <param name="requirement">
        /// The requirement
        /// </param>
        /// <param name="lead">
        /// The lead
        /// </param>
        /// <param name="overridden">
        /// The is Overridden
        /// </param>
        /// <param name="overrideReason">
        /// The override reason
        /// </param>
        /// <param name="approvalDate">
        /// The Approval date
        /// </param>
        /// <param name="modUser">
        /// The modified user
        /// </param>
        public LeadDocument(
            DateTime? dateReceived,
            DocumentStatus docStatus,
            Requirement requirement,
            Lead.Lead lead,
            bool overridden,
            string overrideReason,
            DateTime? approvalDate,
            string modUser)
        {
            // ReSharper disable VirtualMemberCallInContructor
            this.DateReceived = dateReceived;
            this.ApprovalDate = approvalDate;
            this.Requirement = requirement;
            this.Lead = lead;
            this.Override = overridden;
            this.OverrideReason = overrideReason;
            this.DocumentStatus = docStatus;
            this.ModDate = DateTime.Now;
            this.ModUser = modUser;
            // ReSharper restore VirtualMemberCallInContructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadDocument"/> class. 
        /// </summary>
        /// <param name="dateReceived">
        /// The Date Received
        /// </param>
        /// <param name="requirement">
        /// The requirement
        /// </param>
        /// <param name="lead">
        /// The lead
        /// </param>
        /// <param name="overridden">
        /// The is Overridden
        /// </param>
        /// <param name="overrideReason">
        /// The override reason
        /// </param>
        /// <param name="approvalDate">
        /// The Approval date
        /// </param>
        /// <param name="modUser">
        /// The modified user
        /// </param>
        public LeadDocument(
            DateTime? dateReceived,
            Requirement requirement,
            Lead.Lead lead,
            bool overridden,
            string overrideReason,
            DateTime? approvalDate,
            string modUser)
        {
            // ReSharper disable VirtualMemberCallInContructor
            this.DateReceived = dateReceived;
            this.ApprovalDate = approvalDate;
            this.Requirement = requirement;
            this.Lead = lead;
            this.Override = overridden;
            this.OverrideReason = overrideReason;
            this.ModDate = DateTime.Now;
            this.ModUser = modUser;
            // ReSharper restore VirtualMemberCallInContructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadDocument"/> class.
        /// </summary>
        protected LeadDocument()
        {
        }

        /// <summary>
        /// Gets or sets the lead.
        /// </summary>
        public virtual Domain.Lead.Lead Lead { get; protected set; }

        /// <summary>
        /// Gets or sets the requirement.
        /// </summary>
        public virtual Requirement Requirement { get; protected set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the date received.
        /// </summary>
        public virtual DateTime? DateReceived { get; protected set; }

        /// <summary>
        /// Gets or sets the approval date.
        /// </summary>
        public virtual DateTime? ApprovalDate { get; protected set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public virtual string ModUser { get; set; }

        /// <summary>
        /// Gets or sets the user full name.
        /// </summary>
        public virtual string UserFullName { get; set; }

        /// <summary>
        /// Gets or sets the document status.
        /// </summary>
        public virtual DocumentStatus DocumentStatus { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether override.
        /// </summary>
        public virtual bool Override { get; protected set; }

        /// <summary>
        /// Gets or sets the override reason.
        /// </summary>
        public virtual string OverrideReason { get; protected set; }


        /// <summary>
        /// The save document record.
        /// </summary>
        /// <param name="dateReceived">
        /// The date received.
        /// </param>
        /// <param name="docStatus">
        /// The doc status.
        /// </param>
        /// <param name="overridden">
        /// The overridden.
        /// </param>
        /// <param name="overrideReason">
        /// The override reason.
        /// </param>
        /// <param name="approvalDate">
        /// The approval date.
        /// </param>
        /// <param name="modUser">
        /// The mod user.
        /// </param>
        public virtual void SaveDocumentRecord(DateTime? dateReceived, DocumentStatus docStatus, bool overridden, string overrideReason, DateTime? approvalDate, string modUser )
        {
            this.DateReceived = dateReceived;
            this.ApprovalDate = approvalDate;
            this.DocumentStatus = docStatus;
            this.Override = overridden;
            this.OverrideReason = overrideReason;
            this.ModDate = DateTime.Now;
            this.ModUser = modUser;
        }

        /// <summary>
        /// The save document record.
        /// </summary>
        /// <param name="dateReceived">
        /// The date received.
        /// </param>
        /// <param name="overridden">
        /// The overridden.
        /// </param>
        /// <param name="overrideReason">
        /// The override reason.
        /// </param>
        /// <param name="approvalDate">
        /// The approval date.
        /// </param>
        /// <param name="modUser">
        /// The mod user.
        /// </param>
        public virtual void SaveDocumentRecord(DateTime? dateReceived,  bool overridden, string overrideReason, DateTime? approvalDate, string modUser)
        {
            this.DateReceived = dateReceived;
            this.ApprovalDate = approvalDate;
            this.Override = overridden;
            this.OverrideReason = overrideReason;
            this.ModDate = DateTime.Now;
            this.ModUser = modUser;
        }
    }
}
