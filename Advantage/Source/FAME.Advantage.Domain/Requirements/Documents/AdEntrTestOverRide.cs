﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Requirements.Documents
{
    /// <summary>
    /// Domain for AdEntrTestOverRide
    /// </summary>
    public class AdEntrTestOverRide : DomainEntity
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public AdEntrTestOverRide()
        {
            
        }

        /// <summary>
        /// constructor for leads and unenrolled students that are now leads
        /// </summary>
        /// <param name="lead"></param>
        /// <param name="requirement"></param>
        /// <param name="modUser"></param>
        /// <param name="overRide"></param>
        public AdEntrTestOverRide(Lead.Lead lead, Requirement requirement, string modUser, string overRide)
        {
            // ReSharper disable VirtualMemberCallInContructor
            LeadObj = lead;
            Requirement = requirement;
            ModDate = DateTime.Now;
            ModUser = modUser;
            OverRide = overRide;
            // ReSharper restore VirtualMemberCallInContructor
        }
        /// <summary>
        /// constructor for students
        /// </summary>
        /// <param name="lead"></param>
        /// <param name="requirement"></param>
        /// <param name="modUser"></param>
        /// <param name="overRide"></param>
        /// <param name="student"></param>
        public AdEntrTestOverRide(Lead.Lead lead, Requirement requirement, string modUser, string overRide,Student.Student student)
        {
            // ReSharper disable VirtualMemberCallInContructor
            LeadObj = lead;
            Requirement = requirement;
            ModDate = DateTime.Now;
            ModUser = modUser;
            OverRide = overRide;
            Student = student;
            // ReSharper restore VirtualMemberCallInContructor
        }
        /// <summary>
        /// For LeadId
        /// </summary>
        public virtual Lead.Lead LeadObj { get; protected set; }
        /// <summary>
        /// Test requirement 
        /// </summary>
        public virtual Requirement Requirement { get; protected set; }
        /// <summary>
        /// Modified by
        /// </summary>
        public virtual string ModUser { get; protected set; }
        /// <summary>
        /// Modification Date
        /// </summary>
        public virtual DateTime? ModDate { get; protected set; }
        /// <summary>
        /// 1 if overriden, 0 if not but still it is varchar in the database
        /// </summary>
        public virtual string OverRide { get; protected set; }
        /// <summary>
        /// Student Id
        /// </summary>
        public virtual Student.Student Student { get; protected set; }

        /// <summary>
        /// to update the document and fee requirement from lead requirement page.
        /// </summary>
        /// <param name="modUser">the user that modifies</param>
        /// <param name="overRide"> "1" if override and "0" is not</param>
        public virtual void SaveEntrTestOverRideRecord(string modUser, string overRide)
        {
            ModDate = DateTime.Now;
            ModUser = modUser;
            OverRide = overRide;
        }
    }
}
