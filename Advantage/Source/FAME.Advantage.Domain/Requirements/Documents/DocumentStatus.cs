﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentStatus.cs" company="FAME Inc">
//   2016
//   FAME.Advantage.Domain.Requirements.Documents.DocumentStatus
// </copyright>
// <summary>
//   Defines the DocumentStatus type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Requirements.Documents
{
    using System;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.SystemStuff;

    /// <summary>
    /// The document status.
    /// </summary>
    public class DocumentStatus : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentStatus"/> class.
        /// </summary>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="isApproved">
        /// The is approved.
        /// </param>
        public DocumentStatus(string code, string description, bool isApproved)
        {
            this.Code = code;
            this.Description = description;
            this.IsApproved = isApproved;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentStatus"/> class.
        /// </summary>
        protected DocumentStatus()
        {
        }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// Gets or sets the system document status.
        /// </summary>
        public virtual SystemDocumentStatus SystemDocumentStatus { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is approved.
        /// </summary>
        public virtual bool IsApproved { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public virtual DateTime? ModDate { get; set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public virtual string ModUser { get; set; }

        /// <summary>
        /// The empty.
        /// </summary>
        /// <returns>
        /// The <see cref="DocumentStatus"/>.
        /// </returns>
        public static DocumentStatus Empty()
        {
            return new DocumentStatus(string.Empty, string.Empty, false);
        }
    }
}
