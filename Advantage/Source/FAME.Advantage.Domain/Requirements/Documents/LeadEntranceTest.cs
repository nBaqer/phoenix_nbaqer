﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Requirements.Documents
{
    /// <summary>
    /// Domain for table adLeadEntranceTest
    /// </summary>
    public class LeadEntranceTest : DomainEntity
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public LeadEntranceTest()
        {
        }

        /// <summary>
        /// Constructor use this to instantiate a new object
        /// </summary>
        /// <param name="lead"></param>
        /// <param name="requirement"></param>
        /// <param name="completedDate"></param>
        /// <param name="score"></param>
        /// <param name="minScore"></param>
        /// <param name="overridden"></param>
        /// <param name="overReason"></param>
        /// <param name="modUser"></param>
        public LeadEntranceTest(Lead.Lead lead, Requirement requirement, DateTime? completedDate, string score, string minScore, bool overridden, string overReason, string modUser)
        {
            // ReSharper disable VirtualMemberCallInContructor
            LeadObj = lead;
            Requirement = requirement;

            decimal sc;
            Decimal.TryParse(score, out sc);
            ActualScore = sc;

            decimal ms;
            Decimal.TryParse(minScore, out ms);

            Pass = false;
            if (sc > 0 && sc > 0)
            {
                if (sc >= ms)
                    Pass = true;
            }

            Override = overridden;
            OverrideReason = overReason;
            ModUser = modUser;
            ModDate = DateTime.Now;
            CompletedDate = completedDate;
           // ReSharper restore VirtualMemberCallInContructor
 
        }
        /// <summary>
        /// Lead object that own the requirement
        /// </summary>
        public virtual Lead.Lead LeadObj { get; protected set; }
        /// <summary>
        /// Requirement object associate
        /// </summary>
        public virtual Requirement Requirement { get; protected set; }
        public virtual string TestTaken { get; protected set; }
        public virtual decimal? ActualScore { get; protected set; }
        public virtual string Comments { get; protected set; }
        public virtual int? ViewOrder { get; set; }
        public virtual bool? Pass { get; set; }

        /// <summary>
        /// This table has override nullable
        /// </summary>
        public virtual bool? Override { get; set; }
        
        /// <summary>
        /// Override reason nullable
        /// </summary>
        public virtual string OverrideReason { get; set; }
        public virtual string ModUser { get; protected set; }
        public virtual DateTime? ModDate { get; protected set; }
        public virtual DateTime? CompletedDate { get; protected set; }
        public virtual string UserFullName { get;  set; }


        public virtual Student.Student Student { get; protected set; }
     
    }


}
