﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Requirement.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Requirements.Requirement
// </copyright>
// <summary>
//   Defines the Requirement type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Requirements
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    using FAME.Advantage.Domain.Campuses.CampusGroups;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Domain.Requirements.Documents;
    using FAME.Advantage.Domain.StatusesOptions;
    using FAME.Advantage.Domain.SystemStuff.Resources;
    using FAME.Advantage.Messages.Users;

    using Enumerations = FAME.Advantage.Messages.Enumerations.Enumerations;

    /// <summary>
    /// The requirement domain entity for table adREQS.
    /// </summary>
    public class Requirement : DomainEntity
    {
        /// <summary>
        /// The is effective date active.
        /// </summary>
        public static readonly Expression<Func<Requirement, bool>> IsEffectiveDateActive = r => r.EffectiveDates.Any(d => d.StartDate <= DateTime.Now.Date && d.EndDate == null)
            || r.EffectiveDates.Any(d => d.StartDate <= DateTime.Now.Date && d.EndDate != null && d.EndDate >= DateTime.Now.Date);

        /// <summary>
        /// Initializes a new instance of the <see cref="Requirement"/> class.
        /// </summary>
        protected Requirement()
        {
        }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public virtual RequirementType Type { get; protected set; }

        /// <summary>
        /// Gets or sets the required for enrollment.
        /// </summary>
        public virtual bool? RequiredForEnrollment { get; set; }

        /// <summary>
        /// Gets or sets the required for financial aid.
        /// </summary>
        public virtual bool? RequiredForFinancialAid { get; set; }

        /// <summary>
        /// Gets or sets the required for graduation.
        /// </summary>
        public virtual bool? RequiredForGraduation { get; set; }

        /// <summary>
        /// Gets or sets the campus group id.
        /// </summary>
        public virtual Guid CampusGroupId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether show update.
        /// </summary>
        public virtual bool ShowUpdate { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public virtual StatusOption Status { get; set; }

        /// <summary>
        /// Gets or sets the campus group.
        /// </summary>
        public virtual CampusGroup CampusGroup { get; protected set; }

        /// <summary>
        /// Gets or sets the system module.
        /// </summary>
        public virtual SystemModule SystemModule { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether is school level.
        /// </summary>
        public virtual bool IsSchoolLevel { get; set; }

        /// <summary>
        /// Gets or sets the effective dates.
        /// </summary>
        public virtual IList<RequirementEffectiveDates> EffectiveDates { get; protected set; }

        /// <summary>
        /// Gets or sets the requirement program version bridge.
        /// </summary>
        public virtual IList<RequirementProgramVersionBridge> RequirementProgramVersionBridge { get; protected set; }

        /// <summary>
        /// Gets or sets the lead documents.
        /// </summary>
        public virtual IList<LeadDocument> LeadDocuments { get; protected set; }

        /// <summary>
        /// Gets or sets the lead entrance tests.
        /// </summary>
        public virtual IList<LeadEntranceTest> LeadEntranceTests { get; protected set; }
        /// <summary>
        /// Gets or sets the lead entrance override tests.
        /// </summary>
        public virtual IList<AdEntrTestOverRide> EntranceOverRideTests { get; protected set; }

        /// <summary>
        /// Gets or sets the lead recs received.
        /// </summary>
        public virtual IList<AdLeadReqsReceived> LeadRecsReceived { get; protected set; }

        /// <summary>
        /// Gets or sets the lead tran received.
        /// </summary>
        public virtual IList<AdLeadTranReceived> LeadTranReceived { get; protected set; }

        /// <summary>
        /// Gets or sets the user list.
        /// </summary>
        public virtual IList<UserOutputModel> UserList { get; set; }

        /// <summary>
        /// Gets or sets the lead transactions.
        /// </summary>
        public virtual IList<LeadTransactions> LeadTransactions { get; protected set; }

        /// <summary>
        /// Gets or sets the lead guid.
        /// </summary>
        public virtual Guid LeadGuid { get; protected set; }

        public virtual bool IsRequired { get; protected set; }

        /// <summary>
        /// Gets the required for.
        /// </summary>
        public virtual Enumerations.RequiredFor RequiredFor
        {
            get
            {
                if (this.RequiredForEnrollment.HasValue)
                {
                    return Enumerations.RequiredFor.Enrollment;
                }
                else if (this.RequiredForFinancialAid.HasValue)
                {
                    return Enumerations.RequiredFor.FinancialAid;
                }
                else if (this.RequiredForFinancialAid.HasValue)
                {
                    return Enumerations.RequiredFor.Graduation;
                }
                else
                {
                    return Enumerations.RequiredFor.None;
                }
            }
        }

        /// <summary>
        /// The set lead guid.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool SetLeadGuid(Guid leadId)
        {
            this.LeadGuid = leadId;
            return true;
        }

        /// <summary>
        /// The set is required.
        /// </summary>
        /// <param name="isRequired">
        /// The is required.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool SetIsRequired(bool isRequired)
        {
            this.IsRequired = isRequired;
            return true;
        }

        /// <summary>
        /// The set is school level.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool SetIsSchoolLevel()
        {
            this.IsSchoolLevel = this.EffectiveDates.Any(d => d.StartDate <= DateTime.Now.Date && d.EndDate == null)
                || this.EffectiveDates.Any(d => d.StartDate <= DateTime.Now.Date && d.EndDate != null && d.EndDate >= DateTime.Now.Date);

            return true;
        }
        
        /// <summary>
        /// The is school or program level.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool IsSchoolOrProgramLevel()
        {
            var value = false;
            var effectiveDates = this.EffectiveDates.Where(n => n.StartDate <= DateTime.Now.Date);

            if (effectiveDates.Any())
            {
                if (effectiveDates.Any(e => e.IsMandatory))
                {
                    value = true;
                }
            }

            if (this.RequirementProgramVersionBridge.Any())
            {
                value = true;
            }

            return value;
        }

        /// <summary>
        /// The is student group mandatory.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool IsStudentGroupMandatory()
        {
            bool retIsMandatory = false;

            var effectiveDates = this.EffectiveDates.Where(n => n.StartDate <= DateTime.Now.Date);
            if (!effectiveDates.Any())
            {
                return false;
            }

            if (effectiveDates.Any(e => e.RequirementLeadGroupBridge.Any(n => n.IsRequired == true)))
            {
                retIsMandatory = true;
            }

            return retIsMandatory;
        }

        /// <summary>
        /// The is requirement for admission enrollment.
        /// Validates if Requirements is Active and Required For Enrollment and Module is Admissions and Effective Dates matches the current date
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool GetIsRequirementForAdmissionEnrollment()
        {
            this.SetIsSchoolLevel();

            return this.Status.StatusCode == "A" && (this.RequiredForEnrollment == true)
                   && this.SystemModule.ID == (int)SystemModule.ESystemModule.Admissions && this.IsSchoolLevel;
        }
    }
}