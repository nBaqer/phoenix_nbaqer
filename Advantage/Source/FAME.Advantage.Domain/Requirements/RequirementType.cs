﻿using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.Requirements
{
    public class RequirementType : DomainEntityWithTypedID<int>
    {
        protected RequirementType()
        {
        }
       
        public virtual string Description { get; protected set; }
       

        
    }
} 