﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadGroupRequirementGroupBridge.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Requirements.LeadGroupRequirementGroupBridge
// </copyright>
// <summary>
//   The lead group requirement group bridge.
//   Domain Entity for table adLeadGRPREQGroups
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Requirements
{
    using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Domain.SystemStuff;

    /// <summary>
    /// The lead group requirement group bridge.
    /// Domain Entity for table adLeadGRPREQGroups
    /// </summary>
    public class LeadGroupRequirementGroupBridge : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadGroupRequirementGroupBridge"/> class.
        /// </summary>
        /// <param name="leadGroup">
        /// The lead group.
        /// </param>
        /// <param name="requirementGroup">
        /// The requirement group.
        /// </param>
        public LeadGroupRequirementGroupBridge(LeadGroups leadGroup, RequirementGroup requirementGroup)
        {
            this.LeadGroup = leadGroup;
            this.RequirementGroup = requirementGroup;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadGroupRequirementGroupBridge"/> class.
        /// </summary>
        protected LeadGroupRequirementGroupBridge()
        {
        }

        /// <summary>
        /// Gets or sets the minimum number requirements.
        /// </summary>
        public virtual int? MinimumNumberRequirements { get; protected set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public virtual SyStatuses Status { get; protected set; }

        /// <summary>
        /// Gets or sets the lead group.
        /// </summary>
        public virtual LeadGroups LeadGroup { get; protected set; }

        /// <summary>
        /// Gets or sets the requirement group.
        /// </summary>
        public virtual RequirementGroup RequirementGroup { get; protected set; }
    }
}
