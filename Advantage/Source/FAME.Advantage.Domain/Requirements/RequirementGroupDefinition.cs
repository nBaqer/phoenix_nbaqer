﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequirementGroupDefinition.cs" company="FAME Inc.">
//   FAME.Advantage.Domain.Requirements.RequirementGroupDefinition
// </copyright>
// <summary>
//   The requirement group definition.
//   Domain Entity for Table adREQGRPDEF
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Requirements
{
    using System;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Lead;

    /// <summary>
    /// The requirement group definition.
    /// Domain Entity for Table adREQGRPDEF
    /// </summary>
    public class RequirementGroupDefinition : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequirementGroupDefinition"/> class.
        /// </summary>
        public RequirementGroupDefinition()
        {
        }

        /// <summary>
        /// Gets or sets the sequence.
        /// </summary>
        public virtual int? Sequence { get; set; }

        /// <summary>
        /// Gets or sets the is required.
        /// </summary>
        public virtual bool? IsRequired { get; set; }

        /// <summary>
        /// Gets or sets the requirement group.
        /// </summary>
        public virtual RequirementGroup RequirementGroup { get; set; }

        /// <summary>
        /// Gets or sets the requirement.
        /// </summary>
        public virtual Requirement Requirement { get; set; }

        /// <summary>
        /// Gets or sets the lead group.
        /// </summary>
        public virtual LeadGroups LeadGroup { get; set; }

        /// <summary>
        /// The set requirement is required.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool SetRequirementIsRequired()
        {
            if (this.Requirement != null)
            {
                if (this.IsRequired != null)
                {
                    this.Requirement.SetIsRequired(this.IsRequired.Value);
                }
            }

            return true;
        }
    }
}