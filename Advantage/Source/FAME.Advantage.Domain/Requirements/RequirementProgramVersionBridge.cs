﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequirementProgramVersionBridge.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Requirements.RequirementProgramVersionBridge
// </copyright>
// <summary>
//   Defines the RequirementProgramVersionBridge type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Requirements
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Requirements.Documents;
    using FAME.Advantage.Domain.Student.Enrollments;

    /// <summary>
    /// The requirement program version bridge. Domain entity for table adPRGVERTestDetails.
    /// This Bridge Domain associate a program version with a requirement or with requirement group. 
    /// </summary>
    public class RequirementProgramVersionBridge : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequirementProgramVersionBridge"/> class.
        /// </summary>
        protected RequirementProgramVersionBridge()
        {
        }

        /// <summary>
        /// Gets or sets the program version.
        /// </summary>
        public virtual ProgramVersion ProgramVersion { get; protected set; }

        /// <summary>
        /// Gets or sets the requirement. If null it means a relationship with a Requirement Group exist.
        /// </summary>
        public virtual Requirement Requirement { get; set; }

        /// <summary>
        /// Gets or sets the minimum score.
        /// </summary>
        public virtual decimal MinScore { get; set; }

        /// <summary>
        /// Gets the list of requirements that belongs to the associated Requirement Group if exist.
        /// </summary>
        public virtual IEnumerable<Requirement> Requirements
        {
            get
            {
               var requirements = this.RequirementGroup != null ?
                    this.RequirementGroup.RequirementGroupDefinitions.Select(x => x.Requirement) :
                    Enumerable.Empty<Requirement>();

               return (this.Requirement != null) ? requirements.Concat(new[] { this.Requirement }) : requirements;
            }
        }

        /// <summary>
        /// Gets or sets the requirement group. If null it means a relationship with a Requirement exist.
        /// </summary>
        public virtual RequirementGroup RequirementGroup { get; protected set; }
    }
}
