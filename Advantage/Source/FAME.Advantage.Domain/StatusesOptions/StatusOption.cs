﻿using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student.Enrollments;

namespace FAME.Advantage.Domain.StatusesOptions
{
    public class StatusOption : DomainEntity
    {
        public StatusOption()
        {
        }

        public StatusOption(string status, string statusCode, IEnumerable<Campus> campuses = null)
        {

            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Status = status;
            StatusCode = statusCode;
            Campuses = new List<Campus>();
            if (campuses != null)
            {
                foreach (Campus c in campuses)
                {
                    Campuses.Add(c);
                }
            }
            // ReSharper restore DoNotCallOverridableMethodsInConstructor

        }

        public virtual string Status { get; protected set; }
        public virtual string StatusCode { get; protected set; }
        public virtual IList<Campus> Campuses { get; protected set; }// Related to one to many with syCampuses
        public virtual IList<Holidays> HolidaysList { get; protected set; }
    }
}
