﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadGroupBridge.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Campuses.CampusGroups.Subresources.LeadGroupBridge
// </copyright>
// <summary>
//   The lead group bridge.
//   Domain Entity for Table adLeadByLeadGroups
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Campuses.CampusGroups.Subresources
{
    using System;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Domain.Student.Enrollments;

    /// <summary>
    /// The lead group bridge.
    /// Domain Entity for Table adLeadByLeadGroups
    /// </summary>
    public class LeadGroupBridge : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadGroupBridge"/> class.
        /// </summary>
        public LeadGroupBridge()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadGroupBridge"/> class.
        /// </summary>
        /// <param name="studentGroupId">
        /// The student group id.
        /// </param>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        public LeadGroupBridge(Guid studentGroupId, Guid leadId)
        {
            this.StudentGroupId = studentGroupId;
            this.LeadId = leadId;
        }

        /// <summary>
        /// Gets or sets the student group id.
        /// </summary>
        public virtual Guid StudentGroupId { get; protected set; }

        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        public virtual Guid LeadId { get; protected set; }

        /// <summary>
        /// Gets or sets the lead.
        /// </summary>
        public virtual Lead Lead { get; set; }

        /// <summary>
        /// Gets or sets the lead groups.
        /// </summary>
        public virtual LeadGroups LeadGroups { get; set; }

        /// <summary>
        /// Gets or sets the student enrollment id.
        /// </summary>
        public virtual Guid StudentEnrollmentId { get; protected set; }

        /// <summary>
        /// Gets or sets the enrollments.
        /// </summary>
        public virtual Enrollment Enrollments { get; set; }
    }
}
