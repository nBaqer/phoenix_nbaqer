﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Requirements;
using FAME.Advantage.Domain.StatusesOptions;

namespace FAME.Advantage.Domain.Campuses.CampusGroups.Subresources
{
    /// <summary>
    /// The student group.
    /// Domain Entity for Table adLeadGroups
    /// This Entity is Duplicated.
    /// </summary>
    public class StudentGroup : DomainEntity
    {
        protected StudentGroup()
        {
        }

        protected StudentGroup(string descr, string code, bool useForSch, Guid campGrpId)
        {
            Description = descr;
            Code = code;
            UseForScheduling = useForSch;
            CampGrpId = campGrpId;

            StudentGroupBridges = new List<LeadGroupBridge>();
        }
        public virtual string Description { get; set; }
        public virtual string Code { get; set; }
        public virtual bool UseForScheduling { get; set; }
        public virtual Guid CampGrpId { get; set; }
        
        public virtual StatusOption Status { get; protected set; }
        public virtual CampusGroup CampusGroup { get; protected set; }

        public virtual IList<LeadGroupBridge> StudentGroupBridges { get; protected set; }
        public virtual IList<LeadGroupRequirementGroupBridge> StudentGroupRequirementGroupBridge { get; protected set; }
        public virtual IList<RequirementLeadGroupBridge> RequirementLeadGroupBridge { get; protected set; }
    }
}
