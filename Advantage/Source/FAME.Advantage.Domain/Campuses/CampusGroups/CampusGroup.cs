﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusGroup.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   The campus group.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Campuses.CampusGroups
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Domain.Student.Enrollments;
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Domain.Users;

    /// <summary>
    ///  The campus group.
    /// </summary>
    public class CampusGroup : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CampusGroup"/> class.
        /// </summary>
        public CampusGroup()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CampusGroup"/> class.
        /// </summary>
        /// <param name="id">
        ///  The id.
        /// </param>
        /// <param name="campusGrpCode">
        ///  The campus grp code.
        /// </param>
        /// <param name="campusGrpDescrip">
        ///  The campus grp descrip.
        /// </param>
        /// <param name="statusId">
        ///  The status id.
        /// </param>
        /// <param name="isAllCampus">
        ///  The is all campus.
        /// </param>
        public CampusGroup(Guid id, string campusGrpCode, string campusGrpDescrip, Guid statusId, bool isAllCampus)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.ID = id;
            this.CampusGrpCode = campusGrpCode;
            this.CampusGrpDescription = campusGrpDescrip;
            this.StatusId = statusId;
            this.IsAllCampusGrp = isAllCampus;
            this.Campuses = new List<Campus>();

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Gets or sets the campus grp code.
        /// </summary>
        public virtual string CampusGrpCode { get; protected set; }

        /// <summary>
        /// Gets or sets the campus grp description.
        /// </summary>
        public virtual string CampusGrpDescription { get; protected set; }

        /// <summary>
        /// Gets or sets the status id.
        /// </summary>
        public virtual Guid StatusId { get; protected set; }

        /// <summary>
        /// Gets or sets the is all campus grp.
        /// </summary>
        public virtual bool? IsAllCampusGrp { get; protected set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public virtual Guid? CampusId { get; protected set; }

        /// <summary>
        /// Gets or sets the campuses.
        /// </summary>
        public virtual IList<Campus> Campuses { get; protected set; }

        /// <summary>
        /// Gets or sets the program versions.
        /// </summary>
        public virtual IList<ProgramVersion> ProgramVersions { get; protected set; }

        /// <summary>
        /// Gets or sets the enrollment statuses.
        /// </summary>
        public virtual IList<UserDefStatusCode> EnrollmentStatuses { get; protected set; }

        /// <summary>
        /// Gets or sets the student groups.
        /// </summary>
        public virtual IList<StudentGroup> StudentGroups { get; protected set; }

        /// <summary>
        /// Gets or sets the terms.
        /// </summary>
        public virtual IList<Term> Terms { get; protected set; }

        /// <summary>
        /// Gets or sets the list users.
        /// </summary>
        public virtual IList<User> ListUsers { get; protected set; }

        /// <summary>
        /// Gets or sets the holidays list.
        /// </summary>
        public virtual IList<Holidays> HolidaysList { get; protected set; }

        /// <summary>
        /// Gets or sets the lead groups list.
        /// </summary>
        public virtual IList<LeadGroups> LeadGroupsList { get; protected set; }

        /// <summary>
        /// Gets or sets the attend types list.
        /// </summary>
        public virtual IList<ArAttendTypes> AttendTypesList { get; protected set; }

        /// <summary>
        /// Gets or sets the res not enrolled list.
        /// </summary>
        public virtual IList<SyReasonNotEnrolled> ReasonNotEnrolledList { get; protected set; }
    }
}

