﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Campus.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Campuses.Campus
// </copyright>
// <summary>
//   Domain entity for table <code>syCampuses</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Campuses
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.Domain.Campuses.CampusGroups;
    using FAME.Advantage.Domain.Catalogs;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.MostRecentlyUsed;
    using FAME.Advantage.Domain.StatusesOptions;
    using FAME.Advantage.Domain.Student.Enrollments;
    using FAME.Advantage.Domain.SystemStuff;

    /// <summary>
    /// Domain entity for table <code>syCampuses</code>
    /// </summary>
    public class Campus : DomainEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Campus"/> class.
        /// </summary>
        public Campus()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Campus"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="address1">
        /// The address 1.
        /// </param>
        /// <param name="address2">
        /// The address 2.
        /// </param>
        /// <param name="city">
        /// The city.
        /// </param>
        /// <param name="stateId">
        /// The state id.
        /// </param>
        /// <param name="campusGroup">
        /// The campus group.
        /// </param>
        /// <param name="zip">
        /// The zip.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        public Campus(
            Guid id,
            string code,
            string description,
            string address1,
            string address2,
            string city,
            Guid stateId,
            List<CampusGroup> campusGroup,
            string zip,
            StatusOption status)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.ID = id;

            this.Code = code;
            this.Description = description;
            this.Address1 = address1;
            this.Address2 = address2;
            this.City = city;
            this.StateId = stateId;
            this.Zip = zip;
            this.CampusGroup = new List<CampusGroup>();
            if (campusGroup != null)
            {
                foreach (CampusGroup cg in campusGroup)
                {
                    this.CampusGroup.Add(cg);
                }
            }

            this.Status = status;
            this.MruList = new List<ViewLeadMru>();
            // ReSharper restore DoNotCallOverridableMethodsInConstructor      
        }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public virtual string Code { get; protected set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual string Description { get; protected set; }

        /// <summary>
        /// Gets or sets the address 1.
        /// </summary>
        public virtual string Address1 { get; protected set; }

        /// <summary>
        /// Gets or sets the address 2.
        /// </summary>
        public virtual string Address2 { get; protected set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public virtual string City { get; protected set; }

        /// <summary>
        /// Gets or sets the state id.
        /// </summary>
        public virtual Guid StateId { get; protected set; }

        /// <summary>
        /// Gets or sets the status id.
        /// </summary>
        public virtual Guid StatusId { get; protected set; }

        /// <summary>
        /// Gets or sets the zip.
        /// </summary>
        public virtual string Zip { get; protected set; }

        /// <summary>
        /// Gets or sets the system state.
        /// </summary>
        public virtual SystemStates State { get; protected set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public virtual Country Country { get; protected set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public virtual StatusOption Status { get; protected set; }

        /// <summary>
        /// Gets or sets the campus group.
        /// </summary>
        public virtual IList<CampusGroup> CampusGroup { get; protected set; }

        /// <summary>
        /// Gets or sets the enrollments list.
        /// </summary>
        public virtual IList<Enrollment> EnrollmentsList { get; protected set; }

        /// <summary>
        ///  Gets or sets the List of campus in MRU
        /// </summary>
        public virtual IList<ViewLeadMru> MruList { get; protected set; }

        /// <summary>
        /// Gets or sets the token 1098 t service.
        /// </summary>
        public virtual string Token1098TService { get; protected set; }

        /// <summary>
        /// Gets or sets the school code kiss school id.
        /// </summary>
        public virtual string SchoolCodeKissSchoolId { get; protected set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public virtual string Email { get; protected set; }

        /// <summary>
        /// Gets or sets the phone 1.
        /// </summary>
        public virtual string Phone1 { get; protected set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public virtual DateTime ModDate { get; protected set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public virtual string ModUser { get; protected set; }

        /// <summary>
        /// Gets or sets the cms id.
        /// </summary>
        public virtual string CmsId { get; protected set; }
    }
}
