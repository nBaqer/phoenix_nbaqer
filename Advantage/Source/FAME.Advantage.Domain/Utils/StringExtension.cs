﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringExtension.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the StringExtension type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The string extension.
    /// </summary>
    public static class StringExtension
    {
        /// <summary>
        /// The to pascal case.
        /// This function will return a string text in pascal case. If there are spaces at the start or end they will be trimmed in the process.
        /// </summary>
        /// <param name="text">
        /// The text.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string ToPascalCase(this string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                if (text.Trim().Length > 0)
                {
                    if (text.Trim().Contains(" "))
                    {
                        string[] content = text.Trim().Split(' ');
                        StringBuilder toPascalCase = new StringBuilder();
                        foreach (var value in content)
                        {
                            toPascalCase.Append(value.ToPascalCase() + " ");
                        }

                        return toPascalCase.ToString().Trim();
                    }
                    else
                    {
                        return text.Trim().ToLower().Substring(0, 1).ToUpper() + text.Trim().ToLower().Substring(1, text.Trim().Length - 1);
                    }
                }

            }

            return text;
        }
    }
}
