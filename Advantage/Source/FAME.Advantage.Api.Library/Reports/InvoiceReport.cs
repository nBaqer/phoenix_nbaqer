﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Api.Library.Reports
{
    using System.Net.Http;

    using Newtonsoft.Json;
    using FAME.Advantage.Api.Library.Models.Reports;
    using System.IO;

    public class InvoiceReport : Request
    {
        /// The api url.
        /// </summary>
        private readonly string apiUrl;

        /// <summary>
        /// The token.
        /// </summary>
        private readonly string token;
        public InvoiceReport(string apiUrl, string token)
            : base(apiUrl, token)
        {
            this.token = token;
            this.apiUrl = apiUrl;
        }

        public Stream getInvoiceReport(Models.Reports.InvoiceReport model)
        {
            var route = this.apiUrl + "/v1/Reports/InvoiceReport/GetInvoiceReport";
            var response = Task.Run(() => this.Client.PostAsJsonAsync(route, model)).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStreamAsync().Result;

                if (content.Length > 0)
                {
                    return content;
                }
            }

            return null;
        }
    }
}
