﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppStudentSummaryRequest.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the KlassAppStudentSummaryRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.KlassApp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.Api.Library.FinancialAid;
    using FAME.Advantage.Api.Library.Models.Common;
    using FAME.Advantage.Api.Library.Models.KlassApp;

    using Newtonsoft.Json;

    /// <summary>
    /// The klass app student summary request.
    /// </summary>
    public class KlassAppStudentSummaryRequest : Request
    {
        /// <summary>
        /// The Advantage API url.
        /// </summary>
        private string apiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="KlassAppStudentSummaryRequest"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public KlassAppStudentSummaryRequest(string apiUrl, string token)
            : base(apiUrl, token)
        {
            this.apiUrl = apiUrl;
        }

        /// <summary>
        /// The get students to sync.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// The action result status message if the request is not successful
        /// </exception>
        public List<KlassAppSync> GetStudentsToSync()
        {
            var route = this.apiUrl + "/v1/AcademicRecords/KlassApp/GetStudentsToSync";

            this.Client.Timeout = new TimeSpan(1, 0, 0);
            var response = this.Client.GetAsync(route).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;

                if (!string.IsNullOrEmpty(content))
                {
                    var actionResult = JsonConvert.DeserializeObject<ListActionResult<KlassAppSync>>(content);
                    if (actionResult.ResultStatus == Enums.ResultStatus.Success)
                    {
                        return actionResult;
                    }

                    throw new Exception(actionResult.ResultStatusMessage);
                }
            }

            return null;
        }
    }
}
