﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TitleIVSAPRequest.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the TitleIVSAPRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using FAME.Advantage.Api.Library.Models.AcademicRecords;
    using FAME.Advantage.Api.Library.Models.Common;
    using Newtonsoft.Json;

    /// <summary>
    /// The program version.
    /// </summary>
    public class TitleIVSAPRequest : Request
    {
        /// <summary>
        /// The api url.
        /// </summary>
        private readonly string apiUrl;

        /// <summary>
        /// The token.
        /// </summary>
        private readonly string token;

        /// <summary>
        /// Initializes a new instance of the <see cref="TitleIVSAPRequest"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public TitleIVSAPRequest(string apiUrl, string token)
            : base(apiUrl, token)
        {
            this.apiUrl = apiUrl;
            this.token = token;
        }

        /// <summary>
        /// The title iv execution.
        /// </summary>
        public class TitleIVExecution
        {
            /// <summary>
            /// The student enrollments
            /// </summary>
            public List<Guid> StudentEnrollments { get; set; }
            /// <summary>
            /// The run all increments.
            /// </summary>
            public bool RunAllIncrements { get; set; }

        }

        /// <summary>
        /// The title iv sap check.
        /// </summary>
        /// <param name="studentEnrollmentsList">
        /// The student enrollments list.
        /// </param>
        /// <param name="runAll">
        /// The run all.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool TitleIVSapCheck(List<Guid> studentEnrollmentsList)
        {

            var titleIVExecutionData = new TitleIVExecution();
            titleIVExecutionData.StudentEnrollments = studentEnrollmentsList;

            var route = this.apiUrl + "/v1/AcademicRecords/TitleIVSAP/ExecuteTitleIVSap?execution=" + titleIVExecutionData;

            var a = JsonConvert.SerializeObject(titleIVExecutionData);
            var content = new StringContent(a, Encoding.UTF8, "application/json");
            var response = this.Client.PostAsync(route, content).Result;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(responseContent))
                {
                    if (responseContent == "[]")
                    {
                        return true;
                    }

                    return false;
                }
            }

            return false;
        }
        /// <summary>
        /// The title iv sap check asynchronous.
        /// </summary>
        /// <param name="studentEnrollmentsList">
        /// The student enrollments list.
        /// </param>
        /// <param name="runAll">
        /// The run all.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public async Task<bool> TitleIVSapCheckAsync(List<Guid> studentEnrollmentsList)
        {

            var titleIVExecutionData = new TitleIVExecution();
            titleIVExecutionData.StudentEnrollments = studentEnrollmentsList;

            var route = this.apiUrl + "/v1/AcademicRecords/TitleIVSAP/ExecuteTitleIVSap?execution=" + titleIVExecutionData;

            var a = JsonConvert.SerializeObject(titleIVExecutionData);
            var content = new StringContent(a, Encoding.UTF8, "application/json");
            var response = await  this.Client.PostAsync(route, content);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(responseContent))
                {
                    if (responseContent == "[]")
                    {
                        return true;
                    }

                    return false;
                }
            }

            return false;
        }
        /// <summary>
        /// The run iv sap check for students.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool RunIVSapCheckForStudents()
        {
          
              var route = this.apiUrl + "/v1/AcademicRecords/TitleIVSAP/RunIVSapCheckForStudents";
            var response = this.Client.GetAsync(route).Result;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(responseContent))
                {
                    var result = responseContent.ToUpper();
                    if (result == "TRUE")
                    {
                        return true;
                    }

                    return false;
                }
            }
            return false;
        }

    }
}
