﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramVersion.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ProgramVersion type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.Api.Library.Models.AcademicRecords;
    using FAME.Advantage.Api.Library.Models.Common;
    using Newtonsoft.Json;

    /// <summary>
    /// The program version.
    /// </summary>
    public class ProgramVersionRequest : Request
    {
        /// <summary>
        /// The api url.
        /// </summary>
        private readonly string apiUrl;

        /// <summary>
        /// The token.
        /// </summary>
        private readonly string token;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgramVersionRequest"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public ProgramVersionRequest(string apiUrl, string token)
            : base(apiUrl, token)
        {
            this.apiUrl = apiUrl;
            this.token = token;
        }

        /// <summary>
        /// The set program registration type.
        /// When set to By Class, the user have to manually configure terms and class section, additionally it needs to register the students manually.
        /// When set to By Program, the user does not have to configure term or class section, since those are configured during the set up of the program version and program version definition.
        /// Upon enrollment the students are automatically registered into the term and class section.
        /// </summary>
        /// <param name="programRegistrationType">
        /// The program registration type.
        /// </param>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <param name="gradeScaleId">
        /// The grade scale id
        /// </param>
        /// <returns>
        /// The <see cref="Action"/>.
        /// </returns>
        public ActionResult<ProgramVersion> SetProgramRegistrationType(
            Enums.ProgramRegistrationType programRegistrationType,
            Guid programVersionId,
            Guid? gradeScaleId)
        {
            var route = this.apiUrl + "/v1/AcademicRecords/ProgramVersions/SetProgramRegistrationType";

            Dictionary<string, string> pairs = new Dictionary<string, string>();
            pairs.Add("programRegistrationType", programRegistrationType.ToString());
            pairs.Add("programVersionId", programVersionId.ToString());
            pairs.Add("gradeScaleId", gradeScaleId.ToString());
            FormUrlEncodedContent content = new FormUrlEncodedContent(pairs);

            var response = this.Client.PostAsync(route, content).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(responseContent))
                {
                    var actionResult = JsonConvert.DeserializeObject<ActionResult<ProgramVersion>>(responseContent);

                    return actionResult;
                }
            }
            else
            {
                return new ActionResult<ProgramVersion>()
                           {
                               ResultStatus = Enums.ResultStatus.Error,
                               ResultStatusMessage =
                                   response.Content.ReadAsStringAsync().Result
                           };
            }

            return null;
        }

        /// <summary>
        /// The have enrollments.
        /// </summary>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool HaveEnrollments(Guid programVersionId)
        {
            var route = this.apiUrl + "/v1/AcademicRecords/ProgramVersions/HaveEnrollments?programVersionId=" + programVersionId.ToString();

            var response = this.Client.GetAsync(route).Result;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(responseContent))
                {
                    var actionResult = JsonConvert.DeserializeObject<bool>(responseContent);

                    return actionResult;
                }
            }

            return false;
        }


        /// <summary>
        /// The have enrollments.
        /// </summary>
        /// <param name="enrollmentId">
        /// The program version id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public Enums.ProgramRegistrationType? GetRegistrationType(Guid enrollmentId)
        {
            var route = this.apiUrl + "/v1/AcademicRecords/ProgramVersions/GetRegistrationType?enrollmentId=" + enrollmentId.ToString();

            var response = this.Client.GetAsync(route).Result;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(responseContent))
                {
                    var actionResult = JsonConvert.DeserializeObject<Enums.ProgramRegistrationType?>(responseContent);

                    return actionResult;
                }
            }

            return null;
        }
    }
}
