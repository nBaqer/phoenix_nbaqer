﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Api.Library.AcademicRecords
{
    using System;
    using System.Net.Http;
    using FAME.Advantage.Api.Library.Models.AFA.Enrollments;
    using FAME.Advantage.Api.Library.Models.Common;

    using Newtonsoft.Json;
    public class EnrollmentRequest : Request
    {

        /// <summary>
        /// The api url.
        /// </summary>
        private readonly string apiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnrollmentRequest"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public EnrollmentRequest(string apiUrl, string token)
            : base(apiUrl, token)
        {
            this.apiUrl = apiUrl;
        }


        public string GetNewBadgeNumber(Guid campusId)
        {
            var route = this.apiUrl + "/v1/AcademicRecords/StudentSummary/GetNewBadgeNumber";
            route += "?campusId=" + campusId;

            var response = this.Client.GetAsync(route).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(content))
                {
                    var actionResult = JsonConvert.DeserializeObject<ActionResult<string>>(content);
                    return actionResult.Result;
                }
            }

            return null;
        }
        public void UpdateAfaEnrollmentStatus(string cmsId)
        {
            var route = this.apiUrl + "/v1/AFA/Afa/UpdateEnrollmentsStatus";

            var response = Task.Run(() => this.Client.PostAsJsonAsync(route, cmsId)).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
            }

        }

        public AfaEnrollmentDetails GetAfaEnrollmentDetails(Guid enrollmentId)
        {
            var route = this.apiUrl + "/v1/AFA/Afa/GetAfaEnrollmentDetails?enrollmentId=" + enrollmentId;

            var response = Task.Run(() => this.Client.GetAsync(route)).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                var actionResult = JsonConvert.DeserializeObject<AfaEnrollmentDetails>(responseContent);
                return actionResult;
            }
            return null;
        }
        public IEnumerable<AfaEnrollmentDetails> GetAfaEnrollmentDetails(IEnumerable<Guid> enrollmentIds)
        {
            var route = this.apiUrl + "/v1/AFA/Afa/GetAfaEnrollmentDetails";

            var response = Task.Run(() => this.Client.PostAsJsonAsync(route, enrollmentIds)).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                var actionResult = JsonConvert.DeserializeObject<IEnumerable<AfaEnrollmentDetails>>(responseContent);
                return actionResult;
            }
            return null;
        }
        public string EnrollAfa()
        {
            return "enrolled";
        }
    }
}
