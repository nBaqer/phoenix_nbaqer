﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PaymentPeriodRequest.cs" company="Fame Inc.">
//   2018
// </copyright>
// <summary>
//   Defines the PaymentPeriodRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.Api.Library.AcademicRecords
{
    using FAME.Advantage.Api.Library.Models.AcademicRecords;
    using FAME.Advantage.Api.Library.Models.Admissions;
    using FAME.Advantage.Api.Library.Models.AFA.Demographics;
    using FAME.Advantage.Api.Library.Models.Common;
    using FAME.Advantage.Api.Library.Models.SystemCatalog;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The post zero request.
    /// </summary>
    public class PostZeroRequest : Request
    {
        /// <summary>
        /// The Advantage API url.
        /// </summary>
        private string apiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentPeriodRequest"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The Advantage API url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public PostZeroRequest(string apiUrl, string token)
            : base(apiUrl, token)
        {
            this.apiUrl = apiUrl;
        }

        /// <summary>
        /// The update AFA payment period staging table in Advantage.
        /// </summary>
        /// <param name="paymentPeriods">
        /// The collection of payment periods.
        /// </param>
        /// <returns>
        /// The list of records that could not be updated.
        /// </returns>
        public bool PostZerosForSingleDay(IEnumerable<string> StuEnrollIdList, DateTime date)
        {
            var route = this.apiUrl + "/v1/AcademicRecords/Attendance/PostZerosForSingleDay";
            var response = this.Client.PostAsJsonAsync(route, new PostZeroParams() { stuEnrollIdList = StuEnrollIdList, date = date }).Result;
            return response.IsSuccessStatusCode;
        }
    }

    public class PostZeroParams
    {
        /// <summary>
        /// The list of student enrollment ids
        /// </summary>
        public IEnumerable<string> stuEnrollIdList { get; set; }

        /// <summary>
        /// The date to post to zeros for
        /// </summary>
        public DateTime date { get; set; }
    }

}
