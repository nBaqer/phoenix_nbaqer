﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PaymentPeriodRequest.cs" company="Fame Inc.">
//   2018
// </copyright>
// <summary>
//   Defines the PaymentPeriodRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.Api.Library.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using FAME.Advantage.Api.Library.Models.AcademicRecords;
    using FAME.Advantage.Api.Library.Models.Common;
    using FAME.Advantage.Api.Library.Models.Admissions;
    using FAME.Advantage.Api.Library.Models.AFA.Demographics;
    using FAME.Advantage.Api.Library.Models.SystemCatalog;
    using Newtonsoft.Json;

    /// <summary>
    /// The payment period request.
    /// </summary>
    public class PaymentPeriodRequest : Request
    {
        /// <summary>
        /// The Advantage API url.
        /// </summary>
        private string apiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentPeriodRequest"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The Advantage API url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public PaymentPeriodRequest(string apiUrl, string token)
            : base(apiUrl, token)
        {
            this.apiUrl = apiUrl;
        }

        /// <summary>
        /// The update AFA payment period staging table in Advantage.
        /// </summary>
        /// <param name="paymentPeriods">
        /// The collection of payment periods.
        /// </param>
        /// <returns>
        /// The list of records that could not be updated.
        /// </returns>
        public async Task<List<ListItem<Guid, List<PaymentPeriod>>>> UpdateAfaPaymentPeriodsStaging(IEnumerable<PaymentPeriod> paymentPeriods)
        {
            var route = this.apiUrl + "/v1/AcademicRecords/PaymentPeriod/UpdateAfaPaymentPeriodsStaging";

            var response = await this.Client.PostAsJsonAsync(route, paymentPeriods);
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(responseContent))
                {
                    var actionResult = JsonConvert.DeserializeObject<List<ListItem<Guid, List<PaymentPeriod>>>>(responseContent);

                    return actionResult;
                }
            }

            return new List<ListItem<Guid, List<PaymentPeriod>>>();
        }

        /// <summary>
        /// The calculate payment period attendance for a student enrollment.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment identifier.
        /// </param>
        /// <returns>
        /// The payment period record with attendance data.
        /// </returns>
        public Attendance CalculatePaymentPeriodAttendance(Guid studentEnrollmentId)
        {
            var route = this.apiUrl + "/v1/AcademicRecords/PaymentPeriod/CalculateCumulativePaymentPeriodAttendance";

            var response = Task.Run(() =>  this.Client.PostAsJsonAsync(route, studentEnrollmentId)).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                if (!string.IsNullOrEmpty(responseContent))
                {
                    var actionResult = JsonConvert.DeserializeObject<Attendance>(responseContent);

                    return actionResult;
                }
            }

            return null;
        }

        /// <summary>
        /// The get attendance summary.
        /// </summary>
        /// <param name="stuEnrollmentId">
        /// The stu enrollment id.
        /// </param>
        public void GetAttendanceSummary(Guid stuEnrollmentId)
        {
            var uri = new Uri(
                this.apiUrl + "/v1/AcademicRecords/StudentSummary/GetAttendanceSummary?enrollmentId="
                + stuEnrollmentId);

            HttpResponseMessage response;

            // Do a get call to AFA Service............
            var task = Client.GetAsync(uri).ContinueWith(taskwithmsg =>
                {
                    response = taskwithmsg.Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var leadjson = response.Content.ReadAsStringAsync().Result;
                    }
                });
            task.Wait();
        }
    }
}
