﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GraduationCalculator.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The graduation calculator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.Common
{
    using System;

    using FAME.Advantage.Api.Library.Models.Common;

    using Newtonsoft.Json;

    /// <summary>
    /// The graduation calculator.
    /// </summary>
    public class GraduationCalculator : Request
    {
        /// <summary>
        /// The api url.
        /// </summary>
        private readonly string apiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="GraduationCalculator"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public GraduationCalculator(string apiUrl, string token)
            : base(apiUrl, token)
        {
            this.apiUrl = apiUrl;
        }

        /// <summary>
        /// The calculate contracted graduation date.
        /// </summary>
        /// <param name="scheduleId">
        /// The schedule id.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        public ActionResult<DateTime> CalculateContractedGraduationDate(Guid scheduleId, DateTime startDate, Guid campusId)
        {
            var route = this.apiUrl + "/v1/Common/GraduationCalculator/GetCalculatedContractedGraduationDate";
            route += "?scheduleId=" + scheduleId + "&startDate=" + startDate.ToString("MM/dd/yyyy") + "&campusId=" + campusId;

            var response = this.Client.GetAsync(route).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(content))
                {
                    var actionResult = JsonConvert.DeserializeObject<ActionResult<DateTime>>(content);

                    return actionResult;
                }
            }

            return null;
        }
    }
}
