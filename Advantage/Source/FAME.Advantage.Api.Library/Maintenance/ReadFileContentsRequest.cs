﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReadFileContentsRequest.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the ReadFileContentsRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.Storage
{
    using FAME.Advantage.Api.Library.Models.Common;
    using FAME.Advantage.Api.Library.Models.TimeClock;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;

    /// <summary>
    /// The ReadFileContentsRequest request.
    /// </summary>
    public class ReadFileContentsRequest : Request
    {
        /// <summary>
        /// The Advantage API url.
        /// </summary>
        private string apiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReadFileContentsRequest"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public ReadFileContentsRequest(string apiUrl, string token)
            : base(apiUrl, token)
        {
            this.apiUrl = apiUrl;
        }

        /// <summary>
        ///The get automated time clock locations
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// The action result status message if the request is not successful
        /// </exception>
        public ActionResult<string> ReadFileContents(ReadFileContentsParams @params)
        {
            var route = this.apiUrl + "/v1/Maintenance/Storage/ReadFileContents";
            this.Client.Timeout = new TimeSpan(1, 0, 0);
            var response = this.Client.PostAsync(route, new StringContent(JsonConvert.SerializeObject(@params), Encoding.UTF8, "application/json")).Result;
            try
            {
                var content = response.Content.ReadAsStringAsync().Result;
                {
                    var actionResult = JsonConvert.DeserializeObject<ActionResult<string>>(content);
                    return actionResult;
                }
            }
            catch (Exception e)
            {
                return new ActionResult<string>()
                {
                    Result = e.Message,
                    ResultStatus = Enums.ResultStatus.Error,
                    ResultStatusMessage = e.Message
                };
            }
        }
    }
}

