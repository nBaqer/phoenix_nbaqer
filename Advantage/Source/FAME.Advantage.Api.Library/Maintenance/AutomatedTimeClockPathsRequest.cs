﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AutomatedTimeClockPathsRequest.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the AutomatedTimeClockPathsRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.TimeClock
{
    using FAME.Advantage.Api.Library.Models.Common;
    using FAME.Advantage.Api.Library.Models.TimeClock;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The AutomatedTimeClockPathsRequest request.
    /// </summary>
    public class AutomatedTimeClockPathsRequest : Request
    {
        /// <summary>
        /// The Advantage API url.
        /// </summary>
        private string apiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileStoragePathsRequest"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public AutomatedTimeClockPathsRequest(string apiUrl, string token)
            : base(apiUrl, token)
        {
            this.apiUrl = apiUrl;
        }

        /// <summary>
        ///The get automated time clock locations
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// The action result status message if the request is not successful
        /// </exception>
        public ActionResult<List<AutomatedTimeClockLocations>> GetAutomatedTimeClockLocations()
        {
            var route = this.apiUrl + "/v1/Maintenance/Storage/GetAutomatedTimeClockLocations";

            this.Client.Timeout = new TimeSpan(1, 0, 0);
            var response = this.Client.GetAsync(route).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;

                if (!string.IsNullOrEmpty(content))
                {
                    var actionResult = JsonConvert.DeserializeObject<ActionResult<List<AutomatedTimeClockLocations>>>(content);
                    if (actionResult.ResultStatus == Enums.ResultStatus.Success)
                    {
                        return actionResult;
                    }
                    else
                    {
                        throw new Exception(actionResult.ResultStatusMessage);
                    }
                }
            }

            throw new Exception(response.ReasonPhrase);
        }
    }
}
