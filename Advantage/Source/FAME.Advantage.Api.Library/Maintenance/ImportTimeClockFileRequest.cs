﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImportTimeClockFileRequest.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the ImportTimeClockFileRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.TimeClock
{
    using FAME.Advantage.Api.Library.Models.Common;
    using FAME.Advantage.Api.Library.Models.TimeClock;
    using Newtonsoft.Json;
    using System;
    using System.Net.Http;
    using System.Text;

    /// <summary>
    /// The ImportTimeClockFileRequest request.
    /// </summary>
    public class ImportTimeClockFileRequest : Request
    {
        /// <summary>
        /// The Advantage method url.
        /// </summary>
        private string apiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="ImportTimeClockFileRequest"/> class.
        /// </summary>
        /// <param name="methodUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public ImportTimeClockFileRequest(string apiUrl, string token)
            : base(apiUrl, token)
        {
            this.apiUrl = apiUrl;
        }

        /// <summary>
        ///The import time clock file request
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="TimeClockImportResult">
        /// The result of the response
        /// </exception>
        public ActionResult<string> ImportTimeClockFile(TimeClockImportParams @params)
        {
            try
            {
                var route = this.apiUrl + "/v1/AcademicRecords/Attendance/ImportTimeClockFile";
                this.Client.Timeout = new TimeSpan(1, 0, 0);
                var response = this.Client.PostAsync(route, new StringContent(JsonConvert.SerializeObject(@params), Encoding.UTF8, "application/json")).Result;

                var content = response.Content.ReadAsStringAsync().Result;
                {
                    var actionResult = JsonConvert.DeserializeObject<ActionResult<string>>(content);
                    return actionResult;
                }
            }
            catch (Exception e)
            {
                return new ActionResult<string>()
                {
                    Result = null,
                    ResultStatus = Enums.ResultStatus.Error,
                    ResultStatusMessage = e.Message
                };
            }
        }
    }
}
