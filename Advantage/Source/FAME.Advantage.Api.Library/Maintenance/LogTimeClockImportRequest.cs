﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogTimeClockImportRequest.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the LogTimeClockImportRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.TimeClock
{
    using FAME.Advantage.Api.Library.Models.Common;
    using FAME.Advantage.Api.Library.Models.TimeClock;
    using Newtonsoft.Json;
    using System;
    using System.Net.Http;
    using System.Text;

    /// <summary>
    /// The LogTimeClockImportRequest request.
    /// </summary>
    public class LogTimeClockImportRequest : Request
    {
        /// <summary>
        /// The Advantage api url.
        /// </summary>
        private string apiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogTimeClockImportRequest"/> class.
        /// </summary>
        /// <param name="methodUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public LogTimeClockImportRequest(string methodUrl, string token)
            : base(methodUrl, token)
        {
            this.apiUrl = methodUrl;
        }

        /// <summary>
        /// Request to upsert time clock into database
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="string">
        /// The result of the response
        /// </exception>
        public ActionResult<string> UpsertTimeClockLog(TimeClockLogParams @params)
        {
            var route = this.apiUrl + "/v1/AcademicRecords/Attendance/UpsertTimeClockLog";

            this.Client.Timeout = new TimeSpan(1, 0, 0);
            var response = this.Client.PostAsync(route, new StringContent(JsonConvert.SerializeObject(@params), Encoding.UTF8, "application/json")).Result;

            try
            {
                var content = response.Content.ReadAsStringAsync().Result;
                {
                    var actionResult = JsonConvert.DeserializeObject<ActionResult<string>>(content);
                    return actionResult;
                }
            }
            catch (Exception e)
            {
                return new ActionResult<string>()
                {
                    Result = null,
                    ResultStatus = Enums.ResultStatus.Error,
                    ResultStatusMessage = e.Message
                };
            }
        }
    }
}
