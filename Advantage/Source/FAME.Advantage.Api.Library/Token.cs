// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TokenRequest.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the TokenRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.Api.Library.Models;

    using Newtonsoft.Json;

    /// <summary>
    /// The token request.
    /// </summary>
    public class Token
    {
        /// <summary>
        /// The default path.
        /// </summary>
        private const string Path = "/v1/Token";

        private const string PathImpersonate = "/v1/Token/ImpersonatedToken";

        /// <summary>
        /// The api.
        /// </summary>
        private readonly string apiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="Token"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        public Token(string apiUrl)
        {
            this.apiUrl = apiUrl;
        }


        /// <summary>
        /// The get token method that will call the API and get the response.
        /// </summary>
        /// <param name="tenantName">
        /// The tenant name.
        /// </param>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <returns>
        /// The <see cref="TokenResponse"/>.
        /// </returns>
        public TokenResponse GetToken(string tenantName, string userName, string password)
        {
            var url = apiUrl;

            var tokenRequestAsJson = JsonConvert.SerializeObject(new TokenRequest()
                                                                     {
                                                                         TenantName = tenantName,
                                                                         Username = userName,
                                                                         Password = password
                                                                     });
            var tokenRequestAsStringContent = new StringContent(tokenRequestAsJson, Encoding.UTF8, "application/json");
            var responseContent = string.Empty;
            TokenResponse tokenResponse = new TokenResponse();

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            try
            {
                var task = client.PostAsync(url + Path, tokenRequestAsStringContent).ContinueWith(
                    taskwithmsg =>
                        {
                            tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(
                                taskwithmsg.Result.Content.ReadAsStringAsync().Result.ToString());
                            if ((int)taskwithmsg.Result.StatusCode == 200)
                            {
                                tokenResponse.ApiUrl = url;
                                tokenResponse.ResponseStatusCode = (int)taskwithmsg.Result.StatusCode;
                                tokenResponse.ResponseText = taskwithmsg.Result.Content.ReadAsStringAsync().Result.ToString();
                                taskwithmsg.Result.Headers.TransferEncodingChunked = false;
                                responseContent = taskwithmsg.Result.ToString();
                                return tokenResponse;
                            }
                            else
                            {
                                tokenResponse = new TokenResponse();
                                tokenResponse.ApiUrl = url;
                                tokenResponse.ResponseStatusCode = (int)taskwithmsg.Result.StatusCode;
                                tokenResponse.ResponseText = taskwithmsg.Result.Content.ReadAsStringAsync().Result.ToString();
                                tokenResponse.Token = null;
                                responseContent = taskwithmsg.Result.ToString();
                                return tokenResponse;
                            }
                        });
                task.Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return tokenResponse;
        }

        /// <summary>
        /// The get token method that will call the API and get the response for support impersonation.
        /// </summary>
        /// <param name="tenantName">
        /// The tenant name.
        /// </param>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <returns>
        /// The <see cref="TokenResponse"/>.
        /// </returns>
        public TokenResponse GetTokenForImpersonation(string tenantName, Guid userId, string SupportToken)
        {
            var url = apiUrl;

            var tokenRequestAsJson = JsonConvert.SerializeObject(new TokenRequestImpersonate()
            {
                TenantName = tenantName,
                UserId = userId,
            });
            var tokenRequestAsStringContent = new StringContent(tokenRequestAsJson, Encoding.UTF8, "application/json");
            var responseContent = string.Empty;
            TokenResponse tokenResponse = new TokenResponse();

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + SupportToken);
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            try
            {
                var task = client.PostAsync(url + PathImpersonate, tokenRequestAsStringContent).ContinueWith(
                    taskwithmsg =>
                    {
                        tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(
                            taskwithmsg.Result.Content.ReadAsStringAsync().Result.ToString());
                        if ((int)taskwithmsg.Result.StatusCode == 200)
                        {
                            tokenResponse.ApiUrl = url;
                            tokenResponse.ResponseStatusCode = (int)taskwithmsg.Result.StatusCode;
                            tokenResponse.ResponseText = taskwithmsg.Result.Content.ReadAsStringAsync().Result.ToString();
                            taskwithmsg.Result.Headers.TransferEncodingChunked = false;
                            responseContent = taskwithmsg.Result.ToString();
                            return tokenResponse;
                        }
                        else
                        {
                            tokenResponse = new TokenResponse();
                            tokenResponse.ApiUrl = url;
                            tokenResponse.ResponseStatusCode = (int)taskwithmsg.Result.StatusCode;
                            tokenResponse.ResponseText = taskwithmsg.Result.Content.ReadAsStringAsync().Result.ToString();
                            tokenResponse.Token = null;
                            responseContent = taskwithmsg.Result.ToString();
                            return tokenResponse;
                        }
                    });
                task.Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return tokenResponse;
        }
    }
}
