﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TransactionCodeRequest.cs" company="Fame Inc.">
//   2018
// </copyright>
// <summary>
//   Defines the TransactionCodeRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
using FAME.Advantage.Api.Library.Models.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Api.Library.FinancialAid
{
    /// <summary>
    /// Defines the TransactionCodeRequest type.
    /// </summary>
    public class TransactionCodeRequest : Request
    {
        /// <summary>
        /// The Advantage API url.
        /// </summary>
        private string apiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentAwardRequest"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The Advantage API url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public TransactionCodeRequest(string apiUrl, string token)
            : base(apiUrl, token)
        {
            this.apiUrl = apiUrl;
        }

        /// <summary>
        /// The get the transaction codes for the charging method.
        /// </summary>
        /// <returns>
        /// The list of transactionCode description and ID
        /// </returns>
        public IEnumerable<IListItem<string, Guid>> GetTransactionCodeForChargingMethod(string campusId)
        {
            // CONTENT-TYPE header
            var uri = new Uri(this.apiUrl + "/v1/FinancialAid/TransactionCode/GetTransactionCodeForChargingMethod?campusId=" + campusId);

            var response = this.Client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(responseContent))
                {
                    var actionResult = JsonConvert.DeserializeObject<IEnumerable<ListItem<string, Guid>>>(responseContent);

                    return actionResult;
                }
            }

            return new List<ListItem<string, Guid>>();
        }
    }
}
