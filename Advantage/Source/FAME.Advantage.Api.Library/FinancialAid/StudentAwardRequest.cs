﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentAwardRequest.cs" company="Fame Inc.">
//   2018
// </copyright>
// <summary>
//   Defines the StudentAwardRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using FAME.Advantage.Api.Library.Models.StudentAccounts.AFA;

namespace FAME.Advantage.Api.Library.FinancialAid
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using FAME.Advantage.Api.Library.Models.AcademicRecords;
    using FAME.Advantage.Api.Library.Models.Common;
    using FAME.Advantage.Api.Library.Models.Admissions;
    using FAME.Advantage.Api.Library.Models.AFA.Demographics;
    using FAME.Advantage.Api.Library.Models.SystemCatalog;
    using Newtonsoft.Json;

    /// <summary>
    /// The student award request.
    /// </summary>
    public class StudentAwardRequest : Request
    {
        /// <summary>
        /// The Advantage API url.
        /// </summary>
        private string apiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentAwardRequest"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The Advantage API url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public StudentAwardRequest(string apiUrl, string token)
            : base(apiUrl, token)
        {
            this.apiUrl = apiUrl;
        }

        /// <summary>
        /// The create or update student awards in Advantage.
        /// </summary>
        /// <param name="studentAwards">
        /// The collection of student awards.
        /// </param>
        /// <returns>
        /// The list of records that could not be updated.
        /// </returns>
        public List<ListItem<Guid, List<StudentAward>>> CreateUpdateAdvantageStudentAwards(IEnumerable<StudentAward> studentAwards)
        {
            var route = this.apiUrl + "/v1/FinancialAid/StudentAward/CreateUpdateStudentAwards";

            var response = Task.Run(() => this.Client.PostAsJsonAsync(route, studentAwards)).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(responseContent))
                {
                    var actionResult = JsonConvert.DeserializeObject<List<ListItem<Guid, List<StudentAward>>>>(responseContent);

                    return actionResult;
                }
            }

            return null; // new List<ListItem<Guid, List<StudentAward>>>();
        }

        /// <summary>
        /// The create or update student awards in Advantage.
        /// </summary>
        /// <param name="studentAwards">
        /// The collection of student awards.
        /// </param>
        /// <returns>
        /// The list of records that could not be updated.
        /// </returns>
        public List<ListItem<Guid, List<Disbursement>>> CreateUpdateAdvantageDisbursements(IEnumerable<Disbursement> studentAwards)
        {
            var route = this.apiUrl + "/v1/FinancialAid/StudentAward/CreateUpdateStudentDisbursements";
            this.Client.Timeout = TimeSpan.FromMinutes(20);
            var response = Task.Run(() => this.Client.PostAsJsonAsync(route, studentAwards)).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(responseContent))
                {
                    var actionResult = JsonConvert.DeserializeObject<List<ListItem<Guid, List<Disbursement>>>>(responseContent);

                    return actionResult;
                }
            }

            return null; //new List<ListItem<Guid, List<Disbursement>>>();
        }

    }
}
