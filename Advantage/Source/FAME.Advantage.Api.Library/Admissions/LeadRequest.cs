﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadRequest.cs" company="Fame Inc.">
//   2018
// </copyright>
// <summary>
//   Defines the LeadRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.Api.Library.Models.AcademicRecords;

namespace FAME.Advantage.Api.Library.Admissions
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using FAME.Advantage.Api.Library.Models.Admissions;
    using FAME.Advantage.Api.Library.Models.AFA.Demographics;
    using FAME.Advantage.Api.Library.Models.SystemCatalog;
    using Newtonsoft.Json;

    /// <summary>
    /// The lead request.
    /// </summary>
    public class LeadRequest : Request
    {
        /// <summary>
        /// The Advantage API url.
        /// </summary>
        private string apiUrl;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadRequest"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The Advantage API url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public LeadRequest(string apiUrl, string token)
            : base(apiUrl, token)
        {
            this.apiUrl = apiUrl;
        }

        /// <summary>
        /// The check if AFA student exist.
        /// </summary>
        /// <param name="firstName">
        /// The first name.
        /// </param>
        /// <param name="lastName">
        /// The last name.
        /// </param>
        /// <param name="ssn">
        /// The SSN.
        /// </param>
        /// <param name="cmsId">
        /// The CMS id.
        /// </param>
        /// <param name="afaStudentId">
        /// The AFA student id.
        /// </param>
        /// <returns>
        /// The <see cref="AfaStudentExist"/>.
        /// </returns>
        public AfaStudentExist CheckIfAfaStudentExist(string firstName, string lastName, string ssn, string cmsId, long afaStudentId)
        {
            var route = this.apiUrl + "/v1/Leads/CheckIfAfaStudentIdExists?FirstName=" + firstName + "&LastName="
                        + lastName + "&SSN=" + ssn + "&CmsId=" + cmsId + "&AfaStudentId="
                        + afaStudentId;
            var response = this.Client.GetAsync(route).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(content))
                {
                    var actionResult = JsonConvert.DeserializeObject<AfaStudentExist>(content);

                    return actionResult;
                }
            }

            return null;
        }

        /// <summary>
        /// The update AFA student id.
        /// </summary>
        /// <param name="lead">
        /// The lead.
        /// </param>
        /// <returns>
        /// The <see cref="LeadBase"/>.
        /// </returns>
        public LeadBase UpdateAfaStudentId(LeadBase lead)
        {
            var route = this.apiUrl + "/v1/Leads/PutAfaStudentId";
            var leadUpdate = JsonConvert.SerializeObject(lead);
            var content = new StringContent(leadUpdate, Encoding.UTF8, "application/json");
            var response = this.Client.PutAsync(route, content).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(responseContent))
                {
                    var actionResult = JsonConvert.DeserializeObject<LeadBase>(responseContent);

                    return actionResult;
                }
            }

            return null;
        }
        /// <summary>
        ///  Get all the enrollment details for afa sync given the leadId
        /// </summary>
        /// <param name="leadId"></param>
        /// <returns></returns>
        public IEnumerable<AdvStagingEnrollmentV2> GetAllLeadEnrollmentsForAfaSync(Guid leadId)
        {
            var route = this.apiUrl + "/v1/AcademicRecords/Enrollment/GetAllLeadEnrollmentsForAfaSync?leadId="+leadId;
            var response = this.Client.GetAsync(route).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(content))
                {
                    var actionResult = JsonConvert.DeserializeObject<IEnumerable<AdvStagingEnrollmentV2>>(content);

                    return actionResult;
                }
            }

            return null;
        }
        /// <summary>
        /// Post enrollment to afa for the existing lead that recieved ISR in AFA
        /// </summary>
        /// <param name="enrollments"></param>
        /// <returns></returns>
        public string PostAllEnrollmentsToAfa(IEnumerable<AdvStagingEnrollmentV2> enrollments)
        {
            var route = this.apiUrl + "/v1/AFA/Afa/PostAllEnrollmentsToAfa";
            var updateEnrollment = JsonConvert.SerializeObject(enrollments);
            var content = new StringContent(updateEnrollment, Encoding.UTF8, "application/json");
            var response = this.Client.PostAsync(route, content).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(responseContent))
                {
                    return responseContent.ToString();
                }
            }

            return null;
        }

        public Attendance getAttendanceForAfa(Guid enrollmentId)
        {
            var route = this.apiUrl + "/v1/AFA/Afa/CalculateAttendanceUpdatesForEnrollment";
            var updateEnrollment = JsonConvert.SerializeObject(enrollmentId);
            var content = new StringContent(updateEnrollment, Encoding.UTF8, "application/json");
            var response = this.Client.PostAsync(route, content).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(responseContent))
                {
                    var actionResult = JsonConvert.DeserializeObject<Attendance>(responseContent);

                    return actionResult;
                }
            }

            return null;
        }

        public string PostAttendanceToAfa(IEnumerable<Attendance> attendanceUpdates)
        {
            var route = this.apiUrl + "/v1/AFA/Afa/PostAttendanceUpdatesToAfa";
            var updateAttendance = JsonConvert.SerializeObject(attendanceUpdates);
            var content = new StringContent(updateAttendance, Encoding.UTF8, "application/json");
            var response = this.Client.PostAsync(route, content).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(responseContent))
                {
                    return responseContent.ToString();
                }
            }

            return null;
        }

        /// <summary>
        /// The create lead.
        /// </summary>
        /// <param name="lead">
        /// The lead.
        /// </param>
        /// <returns>
        /// The <see cref="Lead"/>.
        /// </returns>
        public Lead CreateLead(Lead lead)
        {
            var route = this.apiUrl + "/v1/Leads/PostLeadAfa";
            var leadCreate = JsonConvert.SerializeObject(lead);
            var content = new StringContent(leadCreate, Encoding.UTF8, "application/json");
            var response = this.Client.PostAsync(route, content).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(responseContent))
                {
                    var actionResult = JsonConvert.DeserializeObject<Lead>(responseContent);

                    return actionResult;
                }
            }

            return null;
        }

        /// <summary>
        /// The get catalog.
        /// </summary>
        /// <returns>
        /// The <see>
        /// <cref>"IList"</cref>
        /// </see> .
        /// </returns>
        public IList<Catalog> GetCatalog()
        {
            var route = this.apiUrl + "/v1/SystemCatalog/Catalog/GetCatalog";
            var response = this.Client.GetAsync(route).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(content))
                {
                    var actionResult = JsonConvert.DeserializeObject<IList<Catalog>>(content);

                    return actionResult;
                }
            }

            return null;
        }

        /// <summary>
        /// The get campus id for Client Management Software id.
        /// </summary>
        /// <param name="cmsId">
        /// The Client Management Software id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public Guid GetCampusIdForCmsId(string cmsId)
        {
            var route = this.apiUrl + "/v1/SystemCatalog/Campus/GetCampusIdForCmsId?cmsId=" + cmsId;
            var response = this.Client.GetAsync(route).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
                return Guid.Parse(content.ToString());
            }

            return Guid.Empty;
        }
    }
}
