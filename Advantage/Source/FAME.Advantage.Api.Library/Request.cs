﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Request.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The request.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;

    /// <summary>
    /// The request.
    /// </summary>
    public class Request
    {
        /// <summary>
        /// The api url.
        /// </summary>
        private string apiUrl;

        /// <summary>
        /// The token.
        /// </summary>
        private string token;

        /// <summary>
        /// Initializes a new instance of the <see cref="Request"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public Request(string apiUrl, string token)
        {
            this.apiUrl = apiUrl;
            this.token = token;
            this.Client = new HttpClient();
            this.Client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            this.Client.DefaultRequestHeaders.Add("Accept", "application/json");
        }

        /// <summary>
        /// Gets the client.
        /// </summary>
        public HttpClient Client { get; }
    }

}
