﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SchoolLogo.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   School Logo
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.SystemCatalog
{
    using System;
    using System.Text;
    using FAME.Advantage.Api.Library.Models.Common; 

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// School Logo
    /// </summary>
    public class SchoolLogo : Request
    {
        /// <summary>
        /// The api url.
        /// </summary>
        private readonly string apiUrl;

        /// <summary>
        /// The token.
        /// </summary>
        private readonly string token;

        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolLogo"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public SchoolLogo(string apiUrl, string token)
            : base(apiUrl, token)
        {
            this.token = token;
            this.apiUrl = apiUrl;
        }

        /// <summary>
        /// Finds the logo.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public byte[] RequestSchoolLogo(string campusId)
        {
            var route = this.apiUrl + "/v1/SystemCatalog/School/GetDetailJson";
            route += "?campusId=" + campusId + "&token=" + this.token;
           
            var response = this.Client.GetAsync(route).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;

                if (!string.IsNullOrEmpty(content))
                {
                    var actionResult = JsonConvert.DeserializeObject<SchoolDetail>(content);
                    return actionResult.SchoolLogo;
                }
            }

            return null;
        }

        /// <summary>
        /// The clear school cash.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ClearSchoolCash(string campusId)
        {
            var route = this.apiUrl + "/v1/SystemCatalog/School/ClearSchoolCash";
            route += "?token=" + this.token + "&campusId=" + campusId;

            var response = this.Client.GetAsync(route).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;

                if (!string.IsNullOrEmpty(content))
                {
                    var pass = content.ToUpper();
                    if (pass == "TRUE")
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
