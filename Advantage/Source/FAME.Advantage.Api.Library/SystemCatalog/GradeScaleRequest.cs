﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GradeScaleRequest.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the GradeScaleRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.Api.Library.Models.Common;

    using Newtonsoft.Json;

    /// <summary>
    /// The grade scale request.
    /// </summary>
    public class GradeScaleRequest : Request
    {
        /// <summary>
        /// The api url.
        /// </summary>
        private readonly string apiUrl;

        /// <summary>
        /// The token.
        /// </summary>
        private readonly string token;

        /// <summary>
        /// Initializes a new instance of the <see cref="GradeScaleRequest"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public GradeScaleRequest(string apiUrl, string token)
            : base(apiUrl, token)
        {
            this.apiUrl = apiUrl;
            this.token = token;
        }

        /// <summary>
        /// The request school logo.
        /// </summary>
        /// <param name="gradeSystemId">
        /// The grade system id.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public List<ListItem<string, Guid>> GetGradeScales(string gradeSystemId)
        {
            var route = this.apiUrl + "/v1/SystemCatalog/GradeScale/GetGradeScales";
            route += "?gradeSystemId=" + gradeSystemId;

            var response = this.Client.GetAsync(route).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;

                if (!string.IsNullOrEmpty(content))
                {
                    var actionResult = JsonConvert.DeserializeObject<ActionResult<List<ListItem<string, Guid>>>>(content);
                    if (actionResult.ResultStatus == Enums.ResultStatus.Success)
                    {
                        return actionResult.Result;
                    }
                    
                    throw new Exception(actionResult.ResultStatusMessage);
                }
            }

            return null;
        }

    }
}
