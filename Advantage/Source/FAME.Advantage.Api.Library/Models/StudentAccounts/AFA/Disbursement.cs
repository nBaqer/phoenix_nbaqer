﻿
namespace FAME.Advantage.Api.Library.Models.StudentAccounts.AFA
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    /// <summary>
    /// The AFA disbursement representation
    /// </summary>
    public class Disbursement : StudentAccounts.Disbursement
    {
        /// <summary>
        /// The location cms id
        /// </summary>
        public string CmsId { get; set; }
        /// <summary>
        /// The financial aid id
        /// </summary>
        public string FinancialAidId { get; set; }
        /// <summary>
        /// The afa student id
        /// </summary>
        public string AfaStudentId { get; set; }
        /// <summary>
        /// The anticipated disbursement date
        /// </summary>
        public DateTime AnticipatedDisbursementDate { get; set; }
        /// <summary>
        /// The anticipated gross amount
        /// </summary>
        public decimal AnticipatedGrossAmount { get; set; }
        /// <summary>
        /// The anticipated net amount
        /// </summary>
        public decimal AnticipatedNetDisbursementAmt { get; set; }
        /// <summary>
        /// The anticipated fee amount
        /// </summary>
        public decimal FeeAmount { get; set; }
        /// <summary>
        /// The actual disbursement date
        /// </summary>
        public DateTime ActualDisbursementDate { get; set; }
        /// <summary>
        /// The actual net disbursement amount
        /// </summary>
        public decimal ActualNetDisbursementAmt { get; set; }
        /// <summary>
        /// The actual gross amount
        /// </summary>
        public decimal ActualGrossAmount { get; set; }
        /// <summary>
        /// The actual fee amount
        /// </summary>
        public decimal ActualFeeAmount { get; set; }
        /// <summary>
        /// The payment period name
        /// </summary>
        public string PaymentPeriodName { get; set; }
        /// <summary>
        /// The payment period start date
        /// </summary>
        public DateTime PaymentPeriodStartDate { get; set; }
        /// <summary>
        /// The payment period end date
        /// </summary>
        public DateTime PaymentPeriodEndDate { get; set; }
        /// <summary>
        /// The origination fee used
        /// </summary>
        public decimal OriginationFeePercentageUsed { get; set; }
        ///<summary>
        /// The get and set for DisbursementStatus
        /// </summary>
        public string DisbursementStatus { get; set; }
        /// <summary>
        /// The deleted flag
        /// </summary>
        public bool Deleted { get; set; }
        /// <summary>
        /// The date created
        /// </summary>
        public DateTime DateCreated { get; set; }
        /// <summary>
        /// The user created
        /// </summary>
        public string UserCreated { get; set; }
    }
}
