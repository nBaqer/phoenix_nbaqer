﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TokenResponse.cs" company="Fame Inc">
//   2017
// </copyright>
// <summary>
//   Defines the TokenResponse type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.Models
{
    using System;

    /// <summary>
    /// The token response that will be given by API.
    /// </summary>
    [Serializable]
    public class TokenResponse
    {
        /// <summary>
        /// Gets or sets the API url.
        /// </summary>
        public string ApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the response text.
        /// </summary>
        public string ResponseText { get; set; }

        /// <summary>
        /// Gets or sets the token.
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Gets or sets the response status code.
        /// </summary>
        public int ResponseStatusCode { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }
    }
}
