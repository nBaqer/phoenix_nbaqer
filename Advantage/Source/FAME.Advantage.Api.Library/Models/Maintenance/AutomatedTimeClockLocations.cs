﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AutomatedTimeClockLocations.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   AutomatedTimeClockLocations file.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.Models.TimeClock
{
    using System;

    /// <summary>
    /// The AutomatedTimeClockLocations class definition.
    /// </summary>
    public class AutomatedTimeClockLocations
    {
        /// <summary>
        /// Gets or sets Path. 
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets the CampusId.
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets the Userame.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the Password.
        /// </summary>
        public string Password { get; set; }
    }
}
