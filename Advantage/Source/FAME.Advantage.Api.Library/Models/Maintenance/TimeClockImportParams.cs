﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TimeClockImportParams.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   TimeClockImportParams file.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.Models.TimeClock
{
    using System;

    /// <summary>
    /// The TimeClockImportParams class definition.
    /// </summary>
    public class TimeClockImportParams
    {
        /// <summary>
        /// Gets or sets FilePath. 
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// Gets or sets the CampusId.
        /// </summary>
        public Guid CampusId { get; set; }
    }
}
