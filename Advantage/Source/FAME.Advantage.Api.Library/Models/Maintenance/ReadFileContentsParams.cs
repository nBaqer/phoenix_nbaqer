﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReadFileContentsParams.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   ReadFileContentsParams file.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.Models.TimeClock
{
    using System;
    using static FAME.Advantage.Api.Library.Models.Common.Enums;

    /// <summary>
    /// The ReadFileContentsParams implementation
    /// </summary>
    public class ReadFileContentsParams
    {
        ///The get and set for CampusId///
        public Guid CampusId { get; set; }

        ///The get and set for FileName///
        public string FileName { get; set; }

        ///The get and set for FeatureType///
        public FileConfigurationFeature FeatureType { get; set; }
    }
}
