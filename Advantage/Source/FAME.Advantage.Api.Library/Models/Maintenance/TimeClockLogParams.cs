﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TimeClockLogParams.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   The TimeClockLogParams params object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.Models.TimeClock
{
    using System;

    /// <summary>
    /// The TimeClockLogParams encapsulation.
    /// </summary>
    public class TimeClockLogParams
    {
        /// <summary>
        /// FileName
        /// </summary>
        public Guid? TimeClockImportLogId { get; set; }

        /// <summary>
        /// CampusId
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// FileName
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Message Property
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Status Property
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// IsAutoImport Property
        /// </summary>
        public bool? IsAutoImport { get; set; }
    }
}
