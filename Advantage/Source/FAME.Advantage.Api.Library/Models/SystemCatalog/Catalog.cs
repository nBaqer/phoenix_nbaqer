﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Catalog.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the catalog for the API.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.Models.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using FAME.Advantage.Api.Library.Models.Admissions;
    using FAME.Advantage.Api.Library.Models.Common;

    /// <summary>
    /// The catalog.
    /// </summary>
    public class Catalog
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the phone type list.
        /// </summary>
        public List<ListItem<string, Guid>> PhoneTypes { get; set; }

        /// <summary>
        /// Gets or sets the address type list.
        /// </summary>
        public List<ListItem<string, Guid>> AddressTypes { get; set; }

        /// <summary>
        /// Gets or sets the countries type list.
        /// </summary>
        public List<ListItem<string, Guid>> CountriesTypes { get; set; }

        /// <summary>
        /// Gets or sets the states list.
        /// </summary>
        public List<ListItem<string, Guid>> States { get; set; }

        /// <summary>
        /// Gets or sets the interest area list.
        /// </summary>
        public List<ListItem<string, Guid>> InterestAreas { get; set; }

        /// <summary>
        /// Gets or sets the source category list.
        /// </summary>
        public List<SourceCategory> SourceCategories { get; set; }

        /// <summary>
        /// Gets or sets the email type list.
        /// </summary>
        public List<ListItem<string, Guid>> EmailTypes { get; set; }

        /// <summary>
        /// Gets or sets the admissions reps.
        /// </summary>
        public List<ListItem<string, Guid>> AdmissionsReps { get; set; }

        /// <summary>
        /// Gets or sets the lead statuses.
        /// </summary>
        public List<ListItem<string, Guid>> LeadStatuses { get; set; }

        /// <summary>
        /// Gets or sets the genders.
        /// </summary>
        public List<Gender> Genders { get; set; }

        /// <summary>
        /// Gets or sets the marital status.
        /// </summary>
        public List<MaritalStatus> MaritalStatus { get; set; }

        /// <summary>
        /// Gets or sets the citizen ship.
        /// </summary>
        public List<CitizenShip> CitizenShip { get; set; }
    }
}
