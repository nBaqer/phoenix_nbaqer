﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CitizenShip.cs" company="Fame Inc">
//   2018
// </copyright>
// <summary>
//   Defines the CitizenShip type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.Models.SystemCatalog
{
    using System;
    using FAME.Advantage.Api.Library.Models.Common;

    /// <summary>
    /// The citizen ship.
    /// </summary>
    public class CitizenShip : IListItem<string, Guid>
    {
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public Guid Value { get; set; }

        /// <summary>
        /// Gets or sets the AFA value (The number that they get from ISIR).
        /// </summary>
        public string AfaValue { get; set; }
    }
}