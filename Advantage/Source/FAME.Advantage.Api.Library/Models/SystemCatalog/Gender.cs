﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Gender.cs" company="FameInc.">
//   2018
// </copyright>
// <summary>
//   Defines the Gender type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.Models.SystemCatalog
{
    using System;
    using FAME.Advantage.Api.Library.Models.Common;

    /// <summary>
    /// The gender.
    /// </summary>
    public class Gender : IListItem<string, Guid>
    {
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public Guid Value { get; set; }

        /// <summary>
        /// Gets or sets the AFA value (The number that they get from ISIR).
        /// </summary>
        public string AfaValue { get; set; }
    }
}
