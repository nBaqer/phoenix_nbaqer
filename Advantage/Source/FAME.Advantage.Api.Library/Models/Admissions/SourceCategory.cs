﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SourceCategory.cs" company="FAME Inc.">
//   2018
// </copyright>
// <summary>
//   The source category.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.Models.Admissions
{
    using System;
    using System.Collections.Generic;
    using FAME.Advantage.Api.Library.Models.Common;

    /// <summary>
    /// The source category.
    /// </summary>
    public class SourceCategory : IListItem<string, Guid>
    {
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public Guid Value { get; set; }

        /// <summary>
        /// Gets or sets the lists of source type description and source type Id 
        /// </summary>
        public List<ListItem<string, Guid>> SourceType { get; set; }
    }
}
