﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadBase.cs" company="FAME Inc.">
//   Fame Inc.2018
// </copyright>
// <summary>
//   Defines the LeadBase type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.Models.Admissions
{
    using System;

    /// <summary>
    /// The lead base.
    /// </summary>
    public class LeadBase
    {
        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        public Guid? LeadId { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the lead status id.
        /// </summary>
        public Guid? LeadStatusId { get; set; }

        /// <summary>
        /// Gets or sets the create date.
        /// </summary>
        public DateTime? CreateDate { get; set; }

        /// <summary>
        /// Gets or sets the Social Security Number.
        /// </summary>
        public string SSN { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid? CampusId { get; set; }

        /// <summary>
        /// Gets or sets the AFA student id.
        /// </summary>
        public long? AfaStudentId { get; set; }

        /// <summary>
        /// Gets or sets the Client Management Software id.
        /// </summary>
        public string CmsId { get; set; }

        /// <summary>
        /// Gets or sets the middle name.
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the birth date.
        /// </summary>
        public DateTime? BirthDate { get; set; }
    }
}
