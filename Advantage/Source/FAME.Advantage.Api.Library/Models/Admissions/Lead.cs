﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Lead.cs" company="FAME Inc.">
//   2018
// </copyright>
// <summary>
//   Defines the Lead type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.Models.Admissions
{
    using System;

    /// <summary>
    /// The lead.
    /// </summary>
    public class Lead : LeadBase
    {
        /// <summary>
        /// Gets or sets the phone type id.
        /// </summary>
        public Guid? PhoneTypeId { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the phone id.
        /// </summary>
        public Guid? PhoneId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is US phone number.
        /// </summary>
        public bool IsUsaPhoneNumber { get; set; } = true;

        /// <summary>
        /// Gets or sets the email type id.
        /// </summary>
        public Guid? EmailTypeId { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the email id.
        /// </summary>
        public Guid? EmailId { get; set; }

        /// <summary>
        /// Gets or sets the address type id.
        /// </summary>
        public Guid? AddressTypeId { get; set; }

        /// <summary>
        /// Gets or sets the address id.
        /// </summary>
        public Guid? AddressId { get; set; }

        /// <summary>
        /// Gets or sets the address 1.
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state id.
        /// </summary>
        public Guid? StateId { get; set; }

        /// <summary>
        /// Gets or sets the zip.
        /// </summary>
        public string Zip { get; set; }
        
        /// <summary>
        /// Gets or sets the driver license number.
        /// </summary>
        public string DriverLicenseNumber { get; set; }

        /// <summary>
        /// Gets or sets the state in which license was issued.
        /// </summary>
        public Guid? LicenseState { get; set; }

        /// <summary>
        /// Gets or sets the citizen ship status.
        /// </summary>
        public Guid? CitizenShipStatus { get; set; }

        /// <summary>
        /// Gets or sets the marital status.
        /// </summary>
        public Guid? MaritalStatus { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        public Guid? Gender { get; set; }
    }
}
