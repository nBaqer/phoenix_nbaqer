﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Api.Library.Models.AcademicRecords
{
    public class Attendance
    {
        public long AdvStagingAttendanceID { get; set; }
        public string LocationCMSID { get; set; }
        public long IDStudent { get; set; }
        public Guid SISEnrollmentID { get; set; }
        public string SSN { get; set; }
        public string PaymentPeriodName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal HoursCreditEarned { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string SAP { get; set; }
        public bool Processed { get; set; }
        public string SessionKey { get; set; }
        public DateTime DateCreated { get; set; }
        public string UserCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public string UserUpdated { get; set; }
    }
}
