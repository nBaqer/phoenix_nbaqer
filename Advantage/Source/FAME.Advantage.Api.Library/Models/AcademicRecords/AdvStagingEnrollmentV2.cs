﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Api.Library.Models.AcademicRecords
{
    public class AdvStagingEnrollmentV2
    {
        public long AdvStagingAttendanceID { get; set; }
        public string LocationCMSID { get; set; }
        public Guid SISEnrollmentID { get; set; }
        public string programName { get; set; }
        public string programVersionID { get; set; }
        public long IDStudent { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime gradDate { get; set; }
        public bool Processed { get; set; }
        public DateTime DateCreated { get; set; }
        public string UserCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public string UserUpdated { get; set; }
        public DateTime? LeaveOfAbsenceDate { get; set; }
        public DateTime? DroppedDate { get; set; }
        public DateTime? LastDateAttendance { get; set; }
        public DateTime? StatusEffectiveDate { get; set; }
        public string AdmissionsCriteria { get; set; }
        public string EnrollmentStatus { get; set; }
        public string SSN { get; set; }
        public DateTime? ReturnFromLOADate { get; set; }
    }
}