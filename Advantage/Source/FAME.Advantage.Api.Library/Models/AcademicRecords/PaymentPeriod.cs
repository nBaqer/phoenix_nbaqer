﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Api.Library.Models.AcademicRecords
{
    public class PaymentPeriod
    {
        /// <summary>
        /// The Afa student id
        /// </summary>
        public long AfaStudentId { get; set; }
        /// <summary>
        /// The student enrollment id
        /// </summary>
        public Guid StudentEnrollmentID { get; set; }
        /// <summary>
        /// The academic year sequence number
        /// </summary>
        public int AcademicYearSeqNo { get; set; }
        /// <summary>
        /// The payment period name
        /// </summary>
        public string PaymentPeriodName { get; set; }
        /// <summary>
        ///  The start date
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// The end date
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// The weeks of instructional time
        /// </summary>
        public decimal WeeksOfInstructionalTime { get; set; }
        /// <summary>
        /// The hours or credits enrolled
        /// </summary>
        public decimal HoursCreditEnrolled { get; set; }
        /// <summary>
        /// The enrollment status description
        /// </summary>
        public string EnrollmentStatusDescription { get; set; }
        /// <summary>
        /// the hours or credits earned
        /// </summary>
        public decimal? HoursCreditEarned { get; set; }
        /// <summary>
        /// The effective date
        /// </summary>
        public DateTime? EffectiveDate { get; set; }
        /// <summary>
        /// The title iv sap result
        /// </summary>
        public string TitleIVSapResult { get; set; }
        /// <summary>
        /// The delete record bit
        /// </summary>
        public bool DeleteRecord { get; set; }
        /// <summary>
        /// The date created
        /// </summary>
        public DateTime DateCreated { get; set; }
        /// <summary>
        /// The user that created the record
        /// </summary>
        public string UserCreated { get; set; }
        /// <summary>
        /// The date updated
        /// </summary>
        public DateTime? DateUpdated { get; set; }
        /// <summary>
        /// The user that updated the record
        /// </summary>
        public string UserUpdated{ get; set; }
    }
}
