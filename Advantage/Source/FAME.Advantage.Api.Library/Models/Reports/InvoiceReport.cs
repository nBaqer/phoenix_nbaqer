﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Api.Library.Models.Reports
{
    public class InvoiceReport
    {
        public List<Guid> StuEnrollId { get; set; }

        /// <summary>
        /// Gets or sets the program version id.
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets the date of change.
        /// </summary>
        public DateTime RefDate { get; set; }
    }
}
