﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ListActionResult.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ListActionResult type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.Models.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The list action result.
    /// </summary>
    /// <typeparam name="T">
    /// The type entity
    /// </typeparam>
    public class ListActionResult<T> : List<T>, IActionResultStatus
    {
        /// <summary>
        /// Gets or sets the result status message.
        /// </summary>
        public string ResultStatusMessage { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public Enums.ResultStatus ResultStatus { get; set; }
    }
}
