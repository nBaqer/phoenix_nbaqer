﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ActionResult.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The action result.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.Models.Common
{
    /// <summary>
    /// The action result.
    /// </summary>
    /// <typeparam name="T">
    /// The type of object to return as the the result.
    /// </typeparam>
    public class ActionResult<T> : IActionResultStatus
    {
        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        public T Result { get; set; }

        /// <summary>
        /// Gets or sets the result status message.
        /// </summary>
        public string ResultStatusMessage { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public Enums.ResultStatus ResultStatus { get; set; }
    }
}
