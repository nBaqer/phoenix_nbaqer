﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AfaStudentExist.cs" company="FAME Inc.">
//   Fame Inc 2018
// </copyright>
// <summary>
//   The AFA student exist.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Api.Library.Models.AFA.Demographics
{
    using System;

    using FAME.Advantage.Api.Library.Models.Common;

    /// <summary>
    /// The AFA student exist.
    /// </summary>
    public class AfaStudentExist : IActionResultStatus
    {
        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        public Guid? LeadId { get; set; }

        /// <summary>
        /// Gets or sets the AFA student id.
        /// </summary>
        public long? AfaStudentId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether AFA student id exists.
        /// </summary>
        public bool AfaStudentIdExists { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public Enums.ResultStatus ResultStatus { get; set; }

        /// <summary>
        /// Gets or sets the result status message.
        /// </summary>
        public string ResultStatusMessage { get; set; }
    }
}
