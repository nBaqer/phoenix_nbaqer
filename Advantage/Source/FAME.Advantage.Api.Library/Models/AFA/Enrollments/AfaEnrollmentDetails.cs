﻿

namespace FAME.Advantage.Api.Library.Models.AFA.Enrollments
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    public class AfaEnrollmentDetails
    {
        /// <summary>
        /// The student enrollment id
        /// </summary>
        public Guid StudentEnrollmentId { get; set; }
        /// <summary>
        /// The start date
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// The program version name
        /// </summary>
        public string ProgramVersionName { get; set; }
        /// <summary>
        /// The program version id
        /// </summary>
        public Guid ProgramVersionId { get; set; }
        /// <summary>
        /// The afa student id
        /// </summary>
        public long? AfaStudentId { get; set; }
        /// <summary>
        /// The cms id
        /// </summary>
        public string CMSId { get; set; }
        /// <summary>
        /// The leave of absence effective date
        /// </summary>
        public DateTime? LeaveOfAbsenceDate { get; set; }
        /// <summary>
        /// The date dropped
        /// </summary>
        public DateTime? DroppedDate { get; set; }
        /// <summary>
        /// The last day attended
        /// </summary>
        public DateTime? LastDateAttendance { get; set; }
        /// <summary>
        /// The status effective date
        /// </summary>
        public DateTime? StatusEffectiveDate { get; set; }
        /// <summary>
        /// The admissions criteria description
        /// </summary>
        public string AdmissionsCriteria { get; set; }
        /// <summary>
        /// The enrollment status description
        /// </summary>
        public string EnrollmentStatus { get; set; }
        /// <summary>
        /// The social security number
        /// </summary>
        public string SSN { get; set; }
        /// <summary>
        /// The revised graduation date
        /// </summary>
        public DateTime? GradDate { get; set; }
        /// <summary>
        /// The return from leave of absence date
        /// </summary>
        public DateTime? ReturnFromLOADate { get; set; }

        public DateTime ModDate { get; set; }

        public string ModUser { get; set; }
        /// <summary>
        /// Advantage Student Number
        /// </summary>
        public string AdvStudentNumber { get; set; }
        /// <summary>
        /// Scheduled Hours Per Week
        /// </summary>
        public Decimal? HoursPerWeek { get; set; }
        /// <summary>
        /// Total Transfer Hours
        /// </summary>
        public Decimal? PriorSchoolHrs { get; set; }
        /// <summary>
        /// Transfer Hours From This School
        /// </summary>
        public Decimal? InSchoolTransferHrs { get; set; }
    }
}
