﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Api.Library.Afa
{
    using Newtonsoft.Json;

    public class AfaIntegrationRequest : Request
    {
        private string apiUrl1;
        public AfaIntegrationRequest(string apiUrl, string token)
            : base(apiUrl, token)
        {
            this.apiUrl1 = apiUrl;
        }

        public List<string> GetCmsIdsToSync()
        {
            var route = this.apiUrl1 + "/v1/SystemCatalog/Campus/GetCmsIdsToSync";
            var response = this.Client.GetAsync(route).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(content))
                {
                    var actionResult = JsonConvert.DeserializeObject<List<string>>(content);

                    return actionResult;
                }
            }
            return null;
        }
    }
}
