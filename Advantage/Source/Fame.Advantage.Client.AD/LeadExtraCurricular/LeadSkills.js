var AD;
(function (AD) {
    var LeadSkills = (function () {
        function LeadSkills(leadid) {
            this.leadId = leadid;
            this.bo = new AD.LeadSkillsBo(leadid);
            $("#extraGrid").kendoGrid({
                dataSource: this.bo.dataSource,
                pageable: true,
                columns: [
                    {
                        field: "Group",
                        title: "Group",
                        width: "150px",
                        editor: this.bo.getGroupValuesEditor,
                        template: "#=Group.Description#"
                    },
                    { field: "Description", title: "Description" },
                    {
                        field: "Level",
                        title: "Level",
                        width: "150px",
                        editor: this.bo.getLevelValuesEditor,
                        template: "#=Level.Description#"
                    },
                    { field: "Comment", title: "Comment" },
                    { command: [{ name: "edit", text: " " }], title: "Edit", width: "50px" },
                    { command: [{ name: "destroy", text: " " }], title: "", width: "50px" }],
                editable: "popup",
                edit: function (e) {
                    if (e.model.isNew()) {
                        e.container.data("kendoWindow").title("Enter a New Skill");
                    }
                    else {
                        e.container.data("kendoWindow").title("Edit the Skill");
                    }
                }
            });
            var newRow = document.getElementById("extraPlusImage");
            newRow.addEventListener('click', this.bo.addNewRow, false);
        }
        return LeadSkills;
    }());
    AD.LeadSkills = LeadSkills;
})(AD || (AD = {}));
//# sourceMappingURL=LeadSkills.js.map