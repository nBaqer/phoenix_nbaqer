﻿module AD {
// ReSharper disable InconsistentNaming    
    export class DropDownItem implements IDropDownItem {
        Id: string;
        Description: string;
    }

    export class ExtraInfo implements IExtraInfo {
        Comment: string;
        Description: string;
        Group: IDropDownItem;
        Level: IDropDownItem;
        Id: number;
        //LeadId: string;
    }

    export class LeadExtraInputInfo implements ILeadExtraInputInfo {
        LeadId: string;
        CampusId: string;
        UserId: any;
        Command: number;
    }


    export class LeadExtraInfo implements ILeadExtraInfo {
        ExtraInfoList: IExtraInfo[];
        LevelsItemsList: IDropDownItem[];
        GroupItemsList: IDropDownItem[];
        Filter: ILeadExtraInputInfo;
    }

// ReSharper restore InconsistentNaming

} 