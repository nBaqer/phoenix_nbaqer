﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />

module AD {

    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;
    declare var XSTUDENT_GET_SHADOW_LEAD: string;

    export class LeadExtraCurricularBo {
        dataSource: kendo.data.DataSource;
        levelDataSource: kendo.data.DataSource;
        groupDataSource: kendo.data.DataSource;
        public leadValidator: kendo.ui.Validator;

        constructor(leadid: string) {
            let body = $("body");
            body.prop("id", "extracurricularBody");

            this.leadValidator = MasterPage.ADVANTAGE_VALIDATOR("extracurricularBody");
            const campusId: string = XMASTER_GET_CURRENT_CAMPUS_ID;
            var userId: string = $("#hdnUserId").val();
            var command: number = 1;
            this.dataSource = new kendo.data.DataSource({
                transport: {
                    read: options => {
                        $.ajax({
                            url: XGET_SERVICE_LAYER_EXTRACURRICULAR,
                            type: "GET",
                            dataType: "json",
                            data: { CampusId: campusId, LeadId: leadid, Command: command },
                            success: result => {
                                options.success(result);
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    update: options => {
                        var info: ILeadExtraInfo = new LeadExtraInfo();
                        info.Filter = new LeadExtraInputInfo();
                        info.Filter.Command = 1;
                        info.GroupItemsList = new Array<DropDownItem>();//options.models[0].Group;
                        info.LevelsItemsList = new Array<DropDownItem>(); //options.models[0].Level;
                        info.ExtraInfoList = new Array<ExtraInfo>();
                        var extra: IExtraInfo = new ExtraInfo();
                        extra.Level = options.data.models[0].Level;
                        extra.Group = options.data.models[0].Group;
                        extra.Comment = options.data.models[0].Comment;
                        extra.Description = options.data.models[0].Description;
                        extra.Id = options.data.models[0].Id;
                        info.Filter.LeadId = leadid;
                        info.Filter.UserId = userId;
                        info.ExtraInfoList.push(extra);
                        $.ajax({
                            type: "POST",
                            dataType: "text",
                            url: XPOST_SERVICE_LAYER_EXTRACURRICULAR,
                            data: JSON.stringify(info),
                            success: result => {
                                options.success(result);
                                let grid = $("#extraGrid").data("kendoGrid") as kendo.ui.Grid;
                                $.when(grid.dataSource.read())
                                    .then(promise => {
                                        grid.dataSource.fetch();
                                        grid.resize(true);
                                        grid.refresh();
                                    });
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    destroy: options => {
                        var info: ILeadExtraInfo = new LeadExtraInfo();
                        info.Filter = new LeadExtraInputInfo();
                        info.Filter.Command = 2;
                        info.GroupItemsList = new Array<DropDownItem>();
                        info.LevelsItemsList = new Array<DropDownItem>();
                        info.ExtraInfoList = new Array<ExtraInfo>();
                        var extra: IExtraInfo = new ExtraInfo();
                        extra.Id = options.data.models[0].Id;
                        info.Filter.LeadId = leadid;
                        info.Filter.UserId = userId;
                        info.ExtraInfoList.push(extra);
                        $.ajax({
                            type: "POST",
                            dataType: "text",
                            url: XPOST_SERVICE_LAYER_EXTRACURRICULAR,
                            data: JSON.stringify(info),
                            success: result => {
                                options.success(result);
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    create: options => {
                        var info: ILeadExtraInfo = new LeadExtraInfo();
                        info.Filter = new LeadExtraInputInfo();
                        info.Filter.Command = 3;
                        info.GroupItemsList = new Array<DropDownItem>();//options.models[0].Group;
                        info.LevelsItemsList = new Array<DropDownItem>(); //options.models[0].Level;
                        info.ExtraInfoList = new Array<ExtraInfo>();
                        var extra: IExtraInfo = new ExtraInfo();
                        extra.Level = options.data.models[0].Level;
                        extra.Group = options.data.models[0].Group;
                        extra.Comment = options.data.models[0].Comment;
                        extra.Description = options.data.models[0].Description;
                        extra.Id = 0;
                        info.Filter.LeadId = leadid;
                        info.Filter.UserId = userId;
                        info.ExtraInfoList.push(extra);
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: XPOST_SERVICE_LAYER_EXTRACURRICULAR,
                            data: JSON.stringify(info),
                            success: result => {
                                options.success(result);
                                let grid = $("#extraGrid").data("kendoGrid") as kendo.ui.Grid;
                                $.when(grid.dataSource.read())
                                    .then(promise => {
                                        grid.resize(true);
                                        grid.refresh();
                                    });

                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    parameterMap: (options, operation) => {
                        if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                        return null;
                    }
                },
                batch: true,
                pageSize: 12,
                schema: {
                    data: "ExtraInfoList",
                    total: response => response.ExtraInfoList.length,
                    model: {
                        id: "Id",
                        fields: {
                            Id: { editable: false, nullable: false },
                            Group: {
                                validation: { required: true },
                                defaultValue: { ID: "0", Description: "" }
                            },
                            Description: {
                                type: "string",
                                validation: {
                                    required: true
                                }
                            },
                            Level: {
                                validation: { required: true },
                                defaultValue: { ID: "0", Description: "" }
                            },
                            Comment: {
                                type: "string"
                            }
                        }
                    }
                }
            });
        }

        addNewRow(): (ev: UIEvent) => any {
            var grid = $("#extraGrid").data("kendoGrid") as any;
            grid.addRow();
            return null;
        }


        getGroupValuesEditor(container, options) {
            $('<input required data-text-field="Description" data-value-field="ID"  data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    dataSource: {
                        transport: {
                            read: options => {
                                $.ajax({
                                    url: XGET_SERVICE_LAYER_EXTRACURRICULAR,
                                    type: "GET",
                                    dataType: "json",
                                    data: {
                                        CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                        LeadId: "00000000-0000-0000-0000-000000000000",
                                        Command: 3
                                    },
                                    success: result => {
                                        options.success(result.GroupItemsList);
                                    },
                                    error: result => {
                                        options.error(result);
                                        MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                                    }
                                });
                            }
                        },
                        dataValueField: "ID",
                        dataTextField: "Description",
                        autoBind: false
                    },
                    optionLabel: "Select"
                });
        }

        getLevelValuesEditor(container, options) {
            $('<input required data-text-field="Description" data-value-field="ID"  data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    dataSource: {
                        transport: {
                            read: options => {
                                $.ajax({
                                    url: XGET_SERVICE_LAYER_EXTRACURRICULAR,
                                    type: "GET",
                                    dataType: "json",
                                    data: {
                                        CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                        LeadId: "00000000-0000-0000-0000-000000000000",
                                        Command: 2
                                    },
                                    success: result => {
                                        options.success(result.LevelsItemsList);
                                    },
                                    error: result => {
                                        options.error(result);
                                        MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                                    }
                                });
                            }
                        },
                        dataValueField: "ID",
                        dataTextField: "Description",
                        autoBind: false
                    },
                    optionLabel: "Select"
                });
        };
    }
}