/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
declare module AD {
    class LeadSkillsBo {
        dataSource: kendo.data.DataSource;
        levelDataSource: kendo.data.DataSource;
        groupDataSource: kendo.data.DataSource;
        constructor(leadid: string);
        addNewRow(): (ev: UIEvent) => any;
        getGroupValuesEditor(container: any, options: any): void;
        getLevelValuesEditor(container: any, options: any): void;
    }
}
