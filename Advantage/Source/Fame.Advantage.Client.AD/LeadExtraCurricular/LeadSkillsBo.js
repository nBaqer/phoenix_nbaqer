var AD;
(function (AD) {
    var LeadSkillsBo = (function () {
        function LeadSkillsBo(leadid) {
            var campusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            var userId = $("#hdnUserId").val();
            var command = 1;
            this.dataSource = new kendo.data.DataSource({
                transport: {
                    read: function (options) {
                        $.ajax({
                            url: AD.XGET_SERVICE_LAYER_SKILLS,
                            type: "GET",
                            dataType: "json",
                            data: { CampusId: campusId, LeadId: leadid, Command: command },
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    update: function (options) {
                        var info = new AD.LeadExtraInfo();
                        info.Filter = new AD.LeadExtraInputInfo();
                        info.Filter.Command = 1;
                        info.GroupItemsList = new Array();
                        info.LevelsItemsList = new Array();
                        info.ExtraInfoList = new Array();
                        var extra = new AD.ExtraInfo();
                        extra.Level = options.data.models[0].Level;
                        extra.Group = options.data.models[0].Group;
                        extra.Comment = options.data.models[0].Comment;
                        extra.Description = options.data.models[0].Description;
                        extra.Id = options.data.models[0].Id;
                        info.Filter.LeadId = leadid;
                        info.Filter.UserId = userId;
                        info.ExtraInfoList.push(extra);
                        $.ajax({
                            type: "POST",
                            dataType: "text",
                            url: AD.XPOST_SERVICE_LAYER_SKILLS,
                            data: JSON.stringify(info),
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    destroy: function (options) {
                        var info = new AD.LeadExtraInfo();
                        info.Filter = new AD.LeadExtraInputInfo();
                        info.Filter.Command = 2;
                        info.GroupItemsList = new Array();
                        info.LevelsItemsList = new Array();
                        info.ExtraInfoList = new Array();
                        var extra = new AD.ExtraInfo();
                        extra.Id = options.data.models[0].Id;
                        info.Filter.LeadId = leadid;
                        info.Filter.UserId = userId;
                        info.ExtraInfoList.push(extra);
                        $.ajax({
                            type: "POST",
                            dataType: "text",
                            url: AD.XPOST_SERVICE_LAYER_SKILLS,
                            data: JSON.stringify(info),
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    create: function (options) {
                        var info = new AD.LeadExtraInfo();
                        info.Filter = new AD.LeadExtraInputInfo();
                        info.Filter.Command = 3;
                        info.GroupItemsList = new Array();
                        info.LevelsItemsList = new Array();
                        info.ExtraInfoList = new Array();
                        var extra = new AD.ExtraInfo();
                        extra.Level = options.data.models[0].Level;
                        extra.Group = options.data.models[0].Group;
                        extra.Comment = options.data.models[0].Comment;
                        extra.Description = options.data.models[0].Description;
                        extra.Id = 0;
                        info.Filter.LeadId = leadid;
                        info.Filter.UserId = userId;
                        info.ExtraInfoList.push(extra);
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: AD.XPOST_SERVICE_LAYER_SKILLS,
                            data: JSON.stringify(info),
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                        return null;
                    }
                },
                batch: true,
                pageSize: 20,
                schema: {
                    data: "ExtraInfoList",
                    total: function (response) { return response.ExtraInfoList.length; },
                    model: {
                        id: "Id",
                        fields: {
                            Id: { editable: false, nullable: false },
                            Group: { validation: { required: true }, defaultValue: { ID: "0", Description: "" } },
                            Description: {
                                type: "string",
                                validation: {
                                    required: { message: "Skill Description is required!" },
                                    maxCharacteres: function (input) {
                                        input.attr("data-maxCharacteres-msg", "Description Cannot Be Longer Than 50 Characters!");
                                        if (input[0].name === "Description") {
                                            return input.val().length <= 50;
                                        }
                                        return true;
                                    }
                                }
                            },
                            Level: {
                                validation: { required: true },
                                defaultValue: { ID: "0", Description: "" }
                            },
                            Comment: {
                                type: "string",
                                validation: {
                                    commentLength: function (input) {
                                        input.attr("data-commentLength-msg", "Comment Cannot Be Longer Than 100 Characters!");
                                        if (input[0].name === "Comment") {
                                            return input.val().length <= 100;
                                        }
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
        LeadSkillsBo.prototype.addNewRow = function () {
            var grid = $("#extraGrid").data("kendoGrid");
            grid.addRow();
            return null;
        };
        LeadSkillsBo.prototype.getGroupValuesEditor = function (container, options) {
            $('<input required name="Skill Group" data-text-field="Description" data-value-field="ID"  data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                dataSource: {
                    transport: {
                        read: function (options) {
                            $.ajax({
                                url: AD.XGET_SERVICE_LAYER_SKILLS,
                                type: "GET",
                                dataType: "json",
                                data: {
                                    CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                    LeadId: "00000000-0000-0000-0000-000000000000",
                                    Command: 3
                                },
                                success: function (result) {
                                    options.success(result.GroupItemsList);
                                },
                                error: function (result) {
                                    options.error(result);
                                    ad.SHOW_DATA_SOURCE_ERROR(result);
                                }
                            });
                        }
                    },
                    dataValueField: "ID",
                    dataTextField: "Description",
                    autoBind: false
                },
                optionLabel: "Select"
            });
        };
        LeadSkillsBo.prototype.getLevelValuesEditor = function (container, options) {
            $('<input required name="Skill Level" data-text-field="Description" data-value-field="ID"  data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                dataSource: {
                    transport: {
                        read: function (options) {
                            $.ajax({
                                url: AD.XGET_SERVICE_LAYER_SKILLS,
                                type: "GET",
                                dataType: "json",
                                data: {
                                    CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                    LeadId: "00000000-0000-0000-0000-000000000000",
                                    Command: 2
                                },
                                success: function (result) {
                                    options.success(result.LevelsItemsList);
                                },
                                error: function (result) {
                                    options.error(result);
                                    ad.SHOW_DATA_SOURCE_ERROR(result);
                                }
                            });
                        }
                    },
                    dataValueField: "ID",
                    dataTextField: "Description",
                    autoBind: false
                },
                optionLabel: "Select"
            });
        };
        ;
        return LeadSkillsBo;
    }());
    AD.LeadSkillsBo = LeadSkillsBo;
})(AD || (AD = {}));
//# sourceMappingURL=LeadSkillsBo.js.map