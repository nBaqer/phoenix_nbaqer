var AD;
(function (AD) {
    var LeadExtraCurricular = (function () {
        function LeadExtraCurricular(leadid) {
            this.leadId = leadid;
            this.bo = new AD.LeadExtraCurricularBo(leadid);
            $("#extraGrid").kendoGrid({
                dataSource: this.bo.dataSource,
                pageable: true,
                columns: [
                    {
                        field: "Group",
                        title: "Group",
                        width: "150px",
                        editor: this.bo.getGroupValuesEditor,
                        template: "#=Group.Description#"
                    },
                    { field: "Description", title: "Description" },
                    {
                        field: "Level",
                        title: "Level",
                        width: "150px",
                        editor: this.bo.getLevelValuesEditor,
                        template: "#=Level.Description#"
                    },
                    { field: "Comment", title: "Comment" },
                    { command: [{ name: "edit", text: " " }], title: "Edit", width: "50px" },
                    { command: [{ name: "destroy", text: " " }], title: "", width: "50px" }],
                editable: "popup",
                edit: function (e) {
                    if (e.model.isNew()) {
                        e.container.data("kendoWindow").title(AD.X_MESSAGE_NEW_EXTRACURRICULAR);
                    }
                    else {
                        e.container.data("kendoWindow").title(AD.X_MESSAGE_EDIT_EXTRACURRICULAR);
                    }
                }
            });
            var newRow = document.getElementById("extraPlusImage");
            newRow.addEventListener('click', this.bo.addNewRow, false);
        }
        return LeadExtraCurricular;
    }());
    AD.LeadExtraCurricular = LeadExtraCurricular;
})(AD || (AD = {}));
//# sourceMappingURL=LeadExtraCurricular.js.map