declare module AD {
    interface IDropDownItem {
        Id: string;
        Description: string;
    }
    interface IExtraInfo {
        Comment: string;
        Description: string;
        Group: IDropDownItem;
        Level: IDropDownItem;
        Id: number;
    }
    interface ILeadExtraInputInfo {
        LeadId: string;
        CampusId: string;
        UserId: any;
        Command: number;
    }
    interface ILeadExtraInfo {
        ExtraInfoList: IExtraInfo[];
        LevelsItemsList: IDropDownItem[];
        GroupItemsList: IDropDownItem[];
        Filter: ILeadExtraInputInfo;
    }
}
