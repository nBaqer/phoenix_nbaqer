﻿module AD
{
    // ReSharper disable InconsistentNaming

    /* IDrop Down item*/
    export interface IDropDownItem {

        Id: string;

        Description:string;
    }


    /* Extracurricular Info*/
    export interface IExtraInfo {
        Comment: string;
        Description: string;
        Group: IDropDownItem;
        Level: IDropDownItem;
        Id: number;
       
    }

    /// <summary>
    /// Input filter class to command the service
    /// </summary>
    export interface ILeadExtraInputInfo {
         /// <summary>
        /// The Lead Id
        /// </summary>
        LeadId: string;

        /// <summary>
        /// The CampusId
        /// </summary>
        CampusId: string;

        /// <summary>
        /// User id
        /// </summary>
       UserId ; 

        /// <summary>
        ///  GET Command =
        ///  1: Get the Extra values
        ///  2: Get the Levels
        ///  3: Get the Groups
        ///  4: Get Levels and Groups
        ///  POST Commands
        /// 1: Update ExtraCurricular
        /// 2: Delete Extracurricular
        /// 3: Insert ExtraCurricular
        /// </summary>
        Command:number;
    }

 
    export interface ILeadExtraInfo
    {
         //List of Extracurricular Info
        ExtraInfoList: IExtraInfo[] ;

        /// <summary>
        /// List of Levels items 
        /// </summary>
        LevelsItemsList: IDropDownItem[];

        /// <summary>
        /// List of Group items
        /// </summary>
        GroupItemsList: IDropDownItem[];

        /// <summary>
        /// Use in Post Operation
        /// </summary>
        Filter: ILeadExtraInputInfo;

        ///// <summary>
        ///// This is used only when the class is used as input in Post
        ///// 1: Update ExtraCurricular
        ///// 2: Delete Extracurricular
        ///// 3: Insert ExtraCurricular
        ///// </summary>
        //Command:number;

        ///// <summary>
        ///// UserId responsible for the operation.
        ///// Used only when output model is used as filter.
        ///// </summary>
        //UserId: string;

        //// Lead Id used to send to server the extracurriculars
        //LeadId: string;
    }

   

    // ReSharper restore InconsistentNaming
}