declare module AD {
    class DropDownItem implements IDropDownItem {
        Id: string;
        Description: string;
    }
    class ExtraInfo implements IExtraInfo {
        Comment: string;
        Description: string;
        Group: IDropDownItem;
        Level: IDropDownItem;
        Id: number;
    }
    class LeadExtraInputInfo implements ILeadExtraInputInfo {
        LeadId: string;
        CampusId: string;
        UserId: any;
        Command: number;
    }
    class LeadExtraInfo implements ILeadExtraInfo {
        ExtraInfoList: IExtraInfo[];
        LevelsItemsList: IDropDownItem[];
        GroupItemsList: IDropDownItem[];
        Filter: ILeadExtraInputInfo;
    }
}
