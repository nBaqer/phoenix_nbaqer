﻿module AD {
    export class LeadSkills {

        public bo: LeadSkillsBo;
        public leadId: string;
        public skillValidator: kendo.ui.Validator;

        constructor(leadid: string){
            this.leadId = leadid;
            let body = $("body");
            body.prop("id", "skillBody");
            this.skillValidator = MasterPage.ADVANTAGE_VALIDATOR("skillBody");
            this.bo = new LeadSkillsBo(leadid);
            var grid = $("#extraGrid").kendoGrid({
                dataSource: this.bo.dataSource,
                pageable: true,
                columns: [
                    // { field: "Id", title: "", width: "1px" },
                    {
                        field: "Group",
                        title: "Group",
                        width: "150px",
                        editor: this.bo.getGroupValuesEditor,
                        template: "#=Group.Description#"
                    },
                    { field: "Description", title: "Description" },
                    {
                        field: "Level",
                        title: "Level",
                        width: "150px",
                        editor: this.bo.getLevelValuesEditor,
                        template: "#=Level.Description#"
                    },
                    { field: "Comment", title: "Comments" },
                    {
                         command: [{ name: "edit", text: " " }], title: "Edit", width: "50px"
                    },
                    {
                        command: [{
                            name: "delet",
                            click: function (e) {
                                e.preventDefault(); //prevent page scroll reset
                                var tr = $(e.target).closest("tr"); //get the row for deletion
                                var data = this.dataItem(tr); //get the row data so it can be referred later
                                $.when(MasterPage
                                    .SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure you want to delete this record?"))
                                    .then(confirmed => {
                                        if (confirmed) {
                                            grid.dataSource.remove(data); //prepare a "destroy" request
                                            grid.dataSource.sync(); //actually send the request 
                                        }
                                    });
                            },
                            text: "X"
                        }], title: "", width: "50px"

                    }],
                editable: "popup",
                edit: e => {
                    // Required labels
                    let label: any = e.container.find("label[for='Group']");
                    label[0].innerHTML = label[0].innerHTML + "<span style='color:red'> *</span>";
                    let label1: any = e.container.find("label[for='Description']");
                    label1[0].innerHTML = label1[0].innerHTML + "<span style='color:red'> *</span>";
                    let label2: any = e.container.find("label[for='Level']");
                    label2[0].innerHTML = label2[0].innerHTML + "<span style='color:red'> *</span>";

                    // Field length limitations
                    e.container.find("input[name='Description']").attr('maxlength', "50");
                    e.container.find("input[name='Comment']").attr('maxlength', "100");

                    if (e.model.isNew()) {
                        (e.container.data("kendoWindow") as any).title("Enter a New Skill");
                    } else {
                        (e.container.data("kendoWindow") as any).title("Edit the Skill");
                    }

                    // Validator
                    e.container.find("a.k-grid-update").bind("click",
                        () => {
                            this.skillValidator.validate();
                        });
                }
            } as any).data("kendoGrid") as kendo.ui.Grid;

            //// Set the data-source for control
            //grid.setDataSource(this.bo.getExtraCurricularGridInfo());

            // Add AddRow handler
            var newRow = document.getElementById("extraPlusImage");
            newRow.addEventListener('click', this.bo.addNewRow, false);
            MasterPage.Common.enableDisableBtns(false);        
        }
    }
} 