﻿module AD {


    export class MyLeadManageVm extends kendo.Observable {

        MyData: any;
        LeadData: any;
        GridDataSource: kendo.data.DataSource;
        db: MyLeadManageDb;

        //#region Constructor
        constructor() {
            super();
            this.db = new MyLeadManageDb();
            this.MyData = this.db.GetMyLeads();
            this.LeadData = this.db.GetLeadData();
            this.GridDataSource = this.getGridDataSource("none");
        }
        //#endregion

        //#region Create Charts
        CreateChartUnAssignedLeads() {
            var fthis = this;
            $("#UnAssignedWidget").kendoChart({
                chartArea: {
                    width: 300,
                    height: 200,
                    // border: { color: "cyan", width: 1 },
                    margin: 20
                },
                //title: {
                //    text: "No Assigned Leads",
                //    font: "12px"
                //},

                legend: {
                    visible: false
                    //position: "top"
                },
                seriesDefaults: {
                    labels: {
                        // template: "#= category # - #= kendo.format('{0:P}', percentage)#",
                        template: "#= category #",
                        //position: "outsideEnd",
                        visible: true,
                        background: "transparent",
                        font: "9px sans-serif"
                    }
                },
                dataSource: {
                    data: fthis.LeadData
                },
                series: [{
                    type: "donut",
                    field: "value",
                    visibleInLegend: false,
                    categoryField: "category",
                    explodeField: "explode"

                }],
                tooltip: {
                    visible: true,
                    template: "#= value#"
                },
                seriesClick: function (e) {
                    // Close the other chart exploding
                    var chart = $('#MyLeadWidget').data('kendoChart') as any;
                    $(chart.dataSource.options.data).each(function (i, item: any) {
                        item.explode = false;
                    });
                    fthis.CreateChartMyLead();

                    var selectedItem: string;
                    $(e.sender.dataSource.options.data).each(function (i, item: any) {
                        if (item.category != e.category) {
                            item.explode = false;
                        }
                        else {
                            item.explode = true;
                            selectedItem = item.category;
                        }

                    });
                    fthis.CreateChartUnAssignedLeads();
                    fthis.GridDataSource = fthis.getGridDataSource(selectedItem);
                    var grid: any = $('#leadGrid').data('kendoGrid');
                    grid.setDataSource(fthis.getGridDataSource(selectedItem));

                }
            });
        }

        CreateChartMyLead() {
            var fthis = this;

            $("#MyLeadWidget").kendoChart({
                chartArea: {
                    width: 300,
                    height: 200,
                    //border: { color: "cyan", width: 1 },
                    margin: 20

                },

                //title: {
                //    text: "My Assigned Leads",
                //    font: "12px"
                //},
                legend: {
                    visible: false
                },
                seriesDefaults: {
                    labels: {
                        template: "#= category #",
                        //position: "outsideEnd",
                        visible: true,
                        background: "transparent",
                        font: "9px sans-serif"

                    }
                },

                dataSource: {
                    data: fthis.MyData
                },
                series: [{
                    type: "donut",
                    field: "value",
                    visibleInLegend: false,
                    categoryField: "category",
                    explodeField: "explode"
                }],
                tooltip: {
                    visible: true,
                    template: "#= value#"
                },
                seriesClick: function (e) {
                    //var chart = $('#UnAssignedWidget').data('kendoChart');
                    //$(chart.dataSource.options.data).each(function (i, item: any) {
                    //    item.explode = false;
                    //});
                    //fthis.CreateChartUnAssignedLeads();

                    $(e.sender.dataSource.options.data).each(function (i, item: any) {
                        if (item.category != e.category) {
                            item.explode = false;
                        }
                        else {
                            item.explode = true;
                        }
                    });
                    fthis.CreateChartMyLead();
                    var selectedItem: string;
                    $(e.sender.dataSource.options.data).each(function (i, item: any) {
                        if (item.category != e.category) {
                            item.explode = false;
                        }
                        else {
                            item.explode = true;
                            selectedItem = item.category;
                        }

                    });
                   // fthis.CreateChartUnAssignedLeads();
                    fthis.GridDataSource = fthis.getGridDataSource(selectedItem);
                    var grid:any = $('#leadGrid').data('kendoGrid');
                    grid.setDataSource(fthis.getGridDataSource(selectedItem));
                }
            });


        }

        //#endregion

        //#region leadGrid Code and definitions

        CreateLeadGrid() {
            var fthis = this;
            $("#leadGrid").kendoGrid({
                toolbar: ["excel", "pdf"],
                excel: {
                    allPages: true
                },
                pdf: {
                    fileName: "AdvantageMyLeadsManagement",
                    landscape: true
                },
                dataSource: fthis.GridDataSource,
                height: 550,
                sortable: true,
                filterable: true,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },

                columns: [

                    { field: "Id", title: "", width: 1 },
                    { field: "FirstName", title: "First Name" },
                    { field: "MiddleName", title: "Middle Name" },
                    { field: "LastName" },
                    { field: "Campus", title: "Campus", width: 150, editor: function (c, o) { fthis.editorCampus(c, o); }, template: "#=Campus#" },
                    { field: "LeadStatus", title: "LeadStatus", editor: function (c, o) { fthis.editorStatus(c, o); }, template: "#=LeadStatus#" },
                   // { field: "Phone" },
                    { field: "LeadAssignedTo", title: "Lead Assigned To", editor: function (c, o) { fthis.editordd(c, o); }, template: "#=LeadAssignedTo#" },
                    { field: "Vendor", title: "Vendor" },
                    { field: "CampaignCode" },
                    { command: ["edit", { text: "Details", click: fthis.showLeadDetails }, { text: "Duplicates", click: fthis.showLeadDuplicates }], title: " ", width: 240 }
                ],
                editable: "inline"
            });

            $("#WindowsDetails").kendoWindow(
                {
                    actions: ["Close"],
                    title: "Lead Details",
                   // width: "700px",
                  //  height: "350px",
                    visible: false,
                    modal: false,
                    resizable: true,
                });

            $("#WindowsDuplicate").kendoWindow(
                {
                    actions: ["Close"],
                    title: "Lead List of possibles duplicates",
                    width: "745px",
                    height: "410px",
                    visible: false,
                    modal: false,
                    resizable: true,
                });
        }

        getGridDataSource(verb: string): kendo.data.DataSource {

            var ethis = this;
            var dat = ethis.db.GetDataByVerb(verb);
            var dsource = new kendo.data.DataSource(
                {
                    data: dat,
                    pageSize: 20,
                    schema: {
                        model: {
                            id: "ProspectID",
                            fields: {
                                ProspectID: { editable: false, visible: false },
                                FirstName: { editable: false, visible: true },
                                LastName: { editable: false, validation: { required: true } },
                                MiddleName: { editable: false },
                                Campus: { editable: true },
                                LeadStatus: { editable: true },
                                Phone: { editable: true },
                                LeadAssignedTo: { editable: true },
                                Vendor: { editable: false },
                                CampaignCode: { editable: false },

                            }
                        }
                    }
                });

            return dsource;

        }

        editordd(container, options) {
            $('<input required data-text-field="Description" data-value-field="ID" data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: false,
                    dataSource: [
                        { Description: "Juana de Arco", ID: 1 },
                        { Description: "Bilbo Bolson", ID: 2 },
                        { Description: "Hulk Rest", ID: 3 },
                        { Description: "Jeremias Smith", ID: 4 }
                    ]
                });
        }

        editorCampus(container, options) {
            $('<input required data-text-field="Description" data-value-field="ID" data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: false,
                    dataSource: [
                        { Description: "Campus Del Rio", ID: 1 },
                        { Description: "Hudson River", ID: 2 },
                        { Description: "Island Club", ID: 3 },
                        { Description: "Askaban", ID: 4 }
                    ]
                });
        }

        editorStatus(container, options) {
            $('<input required data-text-field="Description" data-value-field="ID" data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: false,
                    dataSource: [
                        { Description: "Possible Duplicate", ID: 1 },
                        { Description: "New Lead", ID: 2 },
                        { Description: "Duplicate", ID: 3 },
                        { Description: "Nooooo", ID: 4 }
                    ]
                });
        }

        showLeadDuplicates(e) {
            e.preventDefault();
            var wnd:any = $("#WindowsDuplicate").data("kendoWindow");
            //var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
            //wnd.content(detailsTemplate(dataItem));
            wnd.center().open();

        }

        //#endregion

        //#region Windows Associate with Specific Leads

        showLeadDetails(e) {
            e.preventDefault();
           
            var wnd:kendo.ui.Window = $("#WindowsDetails").data("kendoWindow") as any;
            var dataItem:any = ($(e.currentTarget).closest("tr"));
            if (dataItem != null && dataItem.length > 0) {
                var items = dataItem[0].all;
                var value = items[0].textContent; //Get Id
                var db = new MyLeadManageDb();
                var data = db.GetDetailLead(value);

                var r = new Array(), j = -1;
                r[++j] = '<tr><td class="detailC1">';
                r[++j] = "Lead Name:";
                r[++j] = '</td><td>';
                r[++j] = data[0].FirstName + " " + data[0].MiddleName +" " + data[0].LastName;
                r[++j] = '</td></tr>';

                r[++j] = '<tr class="detailRAlt"><td class="detailC1">';
                r[++j] = "Address:";
                r[++j] = '</td><td >';
                r[++j] = data[0].Address1 + " " + data[0].Address2 + ", " + data[0].City + ", " + data[0].County + ", " + data[0].City + ", " + data[0].Zip;
                r[++j] = '</td></tr>';

                r[++j] = '<tr><td class="detailC1">';
                r[++j] = "Status:";
                r[++j] = '</td><td>';
                r[++j] = data[0].LeadStatus;
                r[++j] = '</td></tr>';

                r[++j] = '<tr class="detailRAlt"><td class="detailC1">';
                r[++j] = "Phone:";
                r[++j] = '</td><td>';
                r[++j] = data[0].Phone;
                r[++j] = '</td></tr>';

                r[++j] = '<tr><td class="detailC1">';
                r[++j] = 'Emails:';
                r[++j] = '</td><td>';
                r[++j] = data[0].HomeEmail + ", " + data[0].WorkEmail;
                r[++j] = '</td></tr>';

                r[++j] = '<tr class="detailRAlt"><td class="detailC1">';
                r[++j] = "Vendor Info:";
                r[++j] = '</td><td>';
                r[++j] = data[0].Vendor + ", " + data[0].CampaignCode;
                r[++j] = '</td></tr>';

                r[++j] = '<tr><td class="detailC1">';
                r[++j] = "Assigned to:";
                r[++j] = '</td><td>';
                r[++j] = data[0].LeadAssignedTo;
                r[++j] = '</td></tr>';

                r[++j] = '<tr class="detailRAlt"><td class="detailC1">';
                r[++j] = "Campus:";
                r[++j] = '</td><td>';
                r[++j] = data[0].Campus;
                r[++j] = '</td></tr>';

                r[++j] = '<tr><td class="detailC1">';
                r[++j] = "Program of Interest:";
                r[++j] = '</td><td>';
                r[++j] = data[0].ProgramOfInterest;
                r[++j] = '</td></tr>';

                r[++j] = '<trclass="detailRAlt"><td class="detailC1">';
                r[++j] = "Comments:";
                r[++j] = '</td><td >';
                r[++j] = data[0].Comment;
                r[++j] = '</td></tr>';

                $('#datailDataTable').html(r.join(''));
            }


            //wnd.content(detailsTemplate(dataItem));
            wnd.center().open();

        }


        //#endregion

        //#region Initialization

        setInitialCategory() {

            var ethis = this;

            var category: string = $.fn.QueryString["Category"];
            if (category != null && category != "") {
                var chart:any = $('#UnAssignedWidget').data('kendoChart');
                $(chart.dataSource.options.data).each(function (i, item: any) {
                    if (category == item.category) {
                        item.explode = true;
                        ethis.CreateChartUnAssignedLeads();
                        var grid = $('#leadGrid').data('kendoGrid') as any;
                        grid.setDataSource(ethis.getGridDataSource(category));
                        return;
                    }
                });

                chart = $('#MyLeadWidget').data('kendoChart');
                $(chart.dataSource.options.data).each(function (i, item: any) {
                    if (category === item.category) {
                        item.explode = true;
                        ethis.CreateChartMyLead();
                        var grid = $('#leadGrid').data('kendoGrid') as any;
                        grid.setDataSource(ethis.getGridDataSource(category));
                        return;
                    }
                });


            }

        }

        //#endregion
    }
}