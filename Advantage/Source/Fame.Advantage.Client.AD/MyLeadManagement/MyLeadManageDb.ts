﻿module AD {
    export class MyLeadManageDb {

        DataSourceError(e) {
            alert(e.responseText);
        }

        // Fake data-sources

        GetMyLeads() {

            return [{
                "category": "New Lead",
                "value": 8,
                "color": '#F84A4A',//'#FA6A6A',


            }, {
                    "category": "Document Pending",
                    "value": 4,
                    "color": '#7FD4FF'
                }, {
                    "category": "Call pending",
                    "value": 1,
                    "color": '#007FFF'
                }, {
                    "category": "Ready to Enroll",
                    "value": 6,
                    "color": '#7FD4FF'

                }];
        } //End GetMyLeads

        GetLeadData() {
            return [{
                category: "Today",
                value: 5,
                color: '#F84A4A'


            }, {
                    category: "Yesterday",
                    value: 2,
                    color: '#7FD4FF'
                }, {
                    category: "10 days ago",
                    value: 1,
                    color: '#007FFF'

                }, {
                    category: "Old",
                    value: 3,
                    color: '#7FD4FF'
                }];
        } // End GetLeadData

        GetDataByVerb(verb: string) {
            var verbl = verb.toLowerCase();
            var dat: any;
            switch (verbl) {
                case "today": {
                    dat = [
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Juan', LastName: 'Smith', MiddleName: 'D', Campus: '', LeadStatus: 'New Lead', Phone: '3334334567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Luis', LastName: 'Anderson', MiddleName: 'A', Campus: '', LeadStatus: 'New Lead', Phone: '3334394567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'John', LastName: 'Snow', MiddleName: 'B', Campus: '', LeadStatus: 'New Lead', Phone: '3384334567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Sean', LastName: 'Anderson', MiddleName: 'A', Campus: '', LeadStatus: 'New Lead', Phone: '3334394567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Peter', LastName: 'Snow', MiddleName: 'B', Campus: '', LeadStatus: 'New Lead', Phone: '3384334567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' }

                    ];
                    return dat;
                }
                case "yesterday": {
                    dat = [
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Andrei', LastName: 'Smith', MiddleName: 'D', Campus: '', LeadStatus: 'New Lead', Phone: '3334334567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Julian', LastName: 'Anderson', MiddleName: 'A', Campus: '', LeadStatus: 'New Lead', Phone: '3334394567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },

                    ];
                    return dat;
                }
                case "10 days ago": {
                    dat = [
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Pavel', LastName: 'Smith', MiddleName: 'D', Campus: '', LeadStatus: 'New Lead', Phone: '3334334567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },

                    ];
                    return dat;
                }
                case "old": {
                    dat = [
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Luisa', LastName: 'Smith', MiddleName: 'D', Campus: '', LeadStatus: 'New Lead', Phone: '3334334567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Rose', LastName: 'Snow', MiddleName: 'A', Campus: '', LeadStatus: 'New Lead', Phone: '3334394567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Marian', LastName: 'Snow', MiddleName: 'B', Campus: '', LeadStatus: 'New Lead', Phone: '3384334567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' }

                    ];
                    return dat;
                }

                case "new lead": {
                    dat = [
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Juan', LastName: 'Smith', MiddleName: 'D', Campus: '', LeadStatus: 'New Lead', Phone: '3334334567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Luis', LastName: 'Anderson', MiddleName: 'A', Campus: '', LeadStatus: 'New Lead', Phone: '3334394567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'John', LastName: 'Snow', MiddleName: 'B', Campus: '', LeadStatus: 'New Lead', Phone: '3384334567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Sean', LastName: 'Anderson', MiddleName: 'A', Campus: '', LeadStatus: 'New Lead', Phone: '3334394567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Peter', LastName: 'Snow', MiddleName: 'B', Campus: '', LeadStatus: 'New Lead', Phone: '3384334567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'John', LastName: 'Watson', MiddleName: 'B', Campus: '', LeadStatus: 'New Lead', Phone: '3384334567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Sherlock', LastName: 'Holmes', MiddleName: 'A', Campus: '', LeadStatus: 'New Lead', Phone: '3334394567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Erasmus', LastName: 'Petrarch', MiddleName: 'B', Campus: '', LeadStatus: 'New Lead', Phone: '3384334567', LeadAssignedTo: '', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' }

                    ];
                    return dat;
                }
                case "document pending": {
                    dat = [
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Andrei', LastName: 'Smith', MiddleName: 'D', Campus: '', LeadStatus: 'New Lead', Phone: '3334334567', LeadAssignedTo: 'John Doe', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Julian', LastName: 'Anderson', MiddleName: 'A', Campus: '', LeadStatus: 'New Lead', Phone: '3334394567', LeadAssignedTo: 'John Doe', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Peter', LastName: 'Pan', MiddleName: 'D', Campus: '', LeadStatus: 'New Lead', Phone: '3334334567', LeadAssignedTo: 'John Doe', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'John', LastName: 'Huck', MiddleName: 'A', Campus: '', LeadStatus: 'New Lead', Phone: '3334394567', LeadAssignedTo: 'John Doe', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },

                    ];
                    return dat;
                }
                case "call pending": {
                    dat = [
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Sergei', LastName: 'Abramov', MiddleName: 'D', Campus: '', LeadStatus: 'New Lead', Phone: '3334334567', LeadAssignedTo: 'John Doe', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },

                    ];
                    return dat;
                }
                case "ready to enroll": {
                    dat = [
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Luisa', LastName: 'Smith', MiddleName: 'D', Campus: '', LeadStatus: 'New Lead', Phone: '3334334567', LeadAssignedTo: 'John Doe', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Rose', LastName: 'Snow', MiddleName: 'A', Campus: '', LeadStatus: 'New Lead', Phone: '3334394567', LeadAssignedTo: 'John Doe', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Marian', LastName: 'Snow', MiddleName: 'B', Campus: '', LeadStatus: 'New Lead', Phone: '3384334567', LeadAssignedTo: 'John Doe', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Julian', LastName: 'Casal', MiddleName: 'D', Campus: '', LeadStatus: 'New Lead', Phone: '3334334567', LeadAssignedTo: 'John Doe', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Enrique', LastName: 'Armentero', MiddleName: 'A', Campus: '', LeadStatus: 'New Lead', Phone: '3334394567', LeadAssignedTo: 'John Doe', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' },
                        { Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Tulio', LastName: 'Sanders', MiddleName: 'B', Campus: '', LeadStatus: 'New Lead', Phone: '3384334567', LeadAssignedTo: 'John Doe', Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55' }

                    ];
                    return dat;
                }



                default: {
                    dat = [];
                    return dat;

                }
            }

        }

        GetDetailLead(value: string) {
            var dat = [
                {
                    Id: '000000-0000000-00000', ProspectID: 'xdrs3456', FirstName: 'Pavel', LastName: 'Smith', MiddleName: 'D'
                    , Campus: '', LeadStatus: 'New Lead', Phone: '3334334567', LeadAssignedTo: 'John Doe'
                    , Vendor: 'LeadConduit', CampaignCode: 'XDCFGG55'
                    , ProgramOfInterest: 'Cosmetology Operator', HomeEmail: 'Pavel.hotmail.com', Address1: '000 1st PL'
                    , Address2: 'Unit 2310', City: 'Colorado Spring', State: 'Colorado', Zip: '33000', County: 'Carlsbad'
                    , WorkEmail: 'zacatecas.ibm.com', Comment: 'Very interesting in barber...'
                     
                }

            ];
            return dat;


        }

    }
}