/// <reference path="../AdvWeb/Kendo/typescript/jquery.d.ts" />
/// <reference path="../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
declare module ad {
    function GET_QUERY_STRING_PARAMETER_BY_NAME(name: string): string;
    function GET_AGE(dateString: any): number;
    function OPEN_TASK_RAD_EMAIL_WINDOW_WITH_ENTITY(entityId: any, mod: any): void;
    function OPEN_TASK_RAD_TASK_WINDOW_WITH_ENTITY(entityId: any, mod: any): void;
    function OPEN_TASK_RAD_SCHEDULE_WINDOW_WITH_ENTITY(entityId: any, mod: any): void;
    function SHOW_DATA_SOURCE_ERROR(e: any): string;
    function FORMAT_DATE_TO_MM_DD_YYYY(d: Date): string;
}
declare module AD {
    class LeadInfoBar {
        viewModel: LeadInfoBarVm;
        constructor();
    }
}
declare module AD {
    var XGET_SERVICE_LAYER_LEAD_DUPLICATES_GET: string;
    var XGET_SERVICE_LAYER_LEAD_DUPLICATES_PAIR_GET: string;
    var XGET_SERVICE_LAYER_LEAD_DUPLICATES_COMMAND_GET: string;
    var XGET_SERVICE_LAYER_LEAD_INFOBAR_COMMAND_GET: string;
    var XIMAGE_INFO: string;
    var XGET_SERVICE_LAYER_LEAD_REQUIREMENT_STATUS_GET: string;
    var XGET_SERVICE_LAYER_LEAD_GROUPS_COMMAND_GET: string;
    var XGET_SERVICE_LAYER_SCHOOL_DEFINED_FIELDS_COMMAND_GET: string;
    var XGET_SERVICE_LAYER_INFOPAGE_RESOURCES_GET: string;
    var XGET_SERVICE_LAYER_CATALOGS_COMMAND_GET: string;
    var XGET_SERVICE_LAYER_LEAD_DEMOGRAPHIC_COMMAND_GET: string;
    var XGET_SERVICE_LAYER_CONFIGURATION_SETTING_GET: string;
    var XPOST_SERVICE_LAYER_LEAD_INFORMATION: string;
    var XGET_SERVICE_LAYER_LEAD_IS_ERASABLE_COMMAND: string;
    var XPOST_SERVICE_LAYER_LEAD_DELETE_COMMAND: string;
    var XGET_SERVICE_LAYER_QUEUE_COMMAND_GET: string;
    var XPOST_SERVICE_LAYER_SET_MRU: string;
    var XPOST_PHONE_SERVICE_LAYER_SET_MRU: string;
    var XGET_SERVICE_LAYER_TOOLBAR_FLAGS: string;
    var XGET_SERVICE_LAYER_EXTRACURRICULAR: string;
    var XPOST_SERVICE_LAYER_EXTRACURRICULAR: string;
    var XGET_SERVICE_LAYER_SKILLS: string;
    var XPOST_SERVICE_LAYER_SKILLS: string;
    var XGET_SERVICE_LAYER_NOTES: string;
    var XPOST_SERVICE_LAYER_NOTES: string;
    var XLEAD_INFO_DLLS_REQUIRED_ITEMS: string;
    var XLEAD_INFO_SDF_FIELDS: string;
    var XLEAD_INFO_GROUPS: string;
    var XLEAD_NEW: string;
    var X_FLAG_LEAD: string;
    var X_FLAG_TASK: string;
    var X_FLAG_APPO: string;
}
declare module AD {
    class LeadInfoBarDb {
        showNotification(message: string): void;
        getInfoLeadFromServer(leadguid: string, mcontext: any): JQueryXHR;
        getInfoLeadReqStatusFromServer(leadguid: string): kendo.data.DataSource;
    }
}
declare module AD {
    class LeadInfoBarVm extends kendo.Observable {
        infolead: kendo.data.DataSource;
        db: LeadInfoBarDb;
        constructor();
        getLeadInfoFromServer(leadGuid: string): void;
        getLeadReqStatusFromServer(leadGuid: string): kendo.data.DataSource;
        static formatDate(d: any): string;
        goToLeadInfoScreen(): void;
        static refreshLeadFields(leadFullName: string, programVersionName: string, expectedStart: string, admissionRep: string, dateAssigned: string, currentStatus: string): void;
    }
}
declare module AD {
    class LeadDuplicate {
        viewModel: LeadDuplicateVm;
        constructor();
    }
}
declare module AD {
    class LeadDuplicateDb {
        showNotification(message: string): void;
        showNotificationCentered(message: string): void;
        onShow(e: any): void;
        GetDuplicateLeadsList(): kendo.data.DataSource;
        commandAndGetDuplicateLeadsList(command: number, newleadguid: string, duplicateguid: string, userid: string): kendo.data.DataSource;
        getDuplicatePairForServer(newleadguid: string, duplicateguid: string): kendo.data.DataSource;
    }
}
declare module AD {
    class LeadDuplicateVm extends kendo.Observable {
        duplicateLeadlistView: kendo.data.DataSource;
        dummyDataSource: kendo.data.DataSource;
        db: LeadDuplicateDb;
        constructor();
        GetDuplicateInformationFromServer(): void;
        UpdateDuplicate: (e: any) => void;
        DeleteDuplicate: (e: any) => void;
        CreateLeadFromA: (e: any) => void;
        CreateLeadFromB: (e: any) => void;
        DecoreItems: () => void;
        SendCommand(command: number): void;
    }
}
declare module AD {
    interface IDropDownItem {
        Id: string;
        Description: string;
    }
    interface IExtraInfo {
        Comment: string;
        Description: string;
        Group: IDropDownItem;
        Level: IDropDownItem;
        Id: number;
    }
    interface ILeadExtraInputInfo {
        LeadId: string;
        CampusId: string;
        UserId: any;
        Command: number;
    }
    interface ILeadExtraInfo {
        ExtraInfoList: IExtraInfo[];
        LevelsItemsList: IDropDownItem[];
        GroupItemsList: IDropDownItem[];
        Filter: ILeadExtraInputInfo;
    }
}
declare module AD {
    class LeadExtraCurricular {
        bo: LeadExtraCurricularBo;
        constructor();
    }
}
declare module AD {
    class LeadExtraCurricularBo {
        dataSource: kendo.data.DataSource;
        levelDataSource: kendo.data.DataSource;
        groupDataSource: kendo.data.DataSource;
        constructor();
        addNewRow(): (ev: UIEvent) => any;
        getGroupValuesEditor(container: any, options: any): void;
        getLevelValuesEditor(container: any, options: any): void;
    }
}
declare module AD {
    class DropDownItem implements IDropDownItem {
        Id: string;
        Description: string;
    }
    class ExtraInfo implements IExtraInfo {
        Comment: string;
        Description: string;
        Group: IDropDownItem;
        Level: IDropDownItem;
        Id: number;
    }
    class LeadExtraInputInfo implements ILeadExtraInputInfo {
        LeadId: string;
        CampusId: string;
        UserId: any;
        Command: number;
    }
    class LeadExtraInfo implements ILeadExtraInfo {
        ExtraInfoList: IExtraInfo[];
        LevelsItemsList: IDropDownItem[];
        GroupItemsList: IDropDownItem[];
        Filter: ILeadExtraInputInfo;
    }
}
declare module AD {
    class LeadSkills {
        bo: LeadSkillsBo;
        constructor();
    }
}
declare module AD {
    class LeadSkillsBo {
        dataSource: kendo.data.DataSource;
        levelDataSource: kendo.data.DataSource;
        groupDataSource: kendo.data.DataSource;
        constructor();
        addNewRow(): (ev: UIEvent) => any;
        getGroupValuesEditor(container: any, options: any): void;
        getLevelValuesEditor(container: any, options: any): void;
    }
}
declare module AD {
    interface ILastNameHistory {
        Id: number;
        LastName: string;
        LeadId: string;
    }
    interface IPhone {
        ID: string;
        Position: number;
        IsForeignPhone: boolean;
        Phone: string;
        Extension: string;
        PhoneTypeId: string;
        UserId: string;
        LeadId: string;
    }
    interface IeMail {
        ID: string;
        IsPreferred: number;
        Email: string;
        EmailType: string;
    }
    interface ISdf {
        SdfId: string;
        SdfValue: string;
    }
    interface IGroup {
        Description: string;
        CampusId: string;
        GroupId: string;
    }
    interface IVehicle {
        Id: number;
        Position: number;
        Permit: string;
        Make: string;
        Model: string;
        Color: string;
        Plate: string;
        ModUser: string;
        ModDate: Date;
        LeadId: string;
    }
    interface ILeadInputModel {
        LeadId: string;
        CampusId: string;
        LeadStatusId: string;
        UserId: string;
        PageResourceId: number;
        CommandString: string;
    }
    interface ILeadInfoPage {
        LeadId: string;
        ModUser: string;
        AvoidDuplicateAnalysis: boolean;
        InputModel: ILeadInputModel;
        FirstName: string;
        MiddleName: string;
        LastName: string;
        NickName: string;
        SSN: string;
        DriverLicenseNumber: string;
        AlienNumber: string;
        Age: number;
        Dependants: number;
        DistanceToSchool: number;
        Suffix: string;
        Gender: string;
        Prefix: string;
        Dob: Date;
        RaceId: string;
        Citizenship: string;
        Dependency: string;
        MaritalStatus: string;
        FamilyIncoming: string;
        HousingType: string;
        DrvLicStateCode: string;
        Transportation: string;
        IsDisabled: boolean;
        Vehicles: IVehicle[];
        PreferredContactId: number;
        CountyId: string;
        CountryId: string;
        AddressTypeId: string;
        City: string;
        OtherState: string;
        AddressStateId: string;
        Zip: string;
        Address1: string;
        AddressApt: string;
        Address2: string;
        PhonesList: IPhone[];
        EmailList: IeMail[];
        LeadLastNameHistoryList: ILastNameHistory[];
        BestTime: Date;
        NoneEmail: boolean;
        SourceCategoryId: string;
        SourceTypeIdI: string;
        AdvertisementId: string;
        Note: string;
        SourceDateTime: Date;
        DateApplied: Date;
        AssignedDate: Date;
        AdmissionRepId: string;
        Comments: string;
        PreviousEducationId: string;
        HighSchoolId: string;
        HighSchoolGradDate: Date;
        AttendingHs: boolean;
        AdminCriteriaId: string;
        AgencySponsorId: string;
        CampusId: string;
        AreaId: string;
        ProgramId: string;
        PrgVerId: string;
        ScheduleId: string;
        ExpectedStart: Date;
        AttendTypeId: string;
        LeadStatusId: string;
        SdfList: ISdf[];
        GroupIdList: string[];
    }
}
declare module AD {
    class LastNameHistory implements ILastNameHistory {
        Id: number;
        LastName: string;
        LeadId: string;
    }
    class Phone implements IPhone {
        ID: string;
        Position: number;
        IsForeignPhone: boolean;
        Phone: string;
        Extension: string;
        PhoneTypeId: string;
        UserId: string;
        LeadId: string;
    }
    class Email implements IeMail {
        ID: string;
        IsPreferred: number;
        Email: string;
        EmailType: string;
    }
    class Sdf implements ISdf {
        SdfId: string;
        SdfValue: string;
    }
    class Group implements IGroup {
        Description: string;
        CampusId: string;
        GroupId: string;
    }
    class Vehicle implements IVehicle {
        Id: number;
        Position: number;
        Permit: string;
        Make: string;
        Model: string;
        Color: string;
        Plate: string;
        ModUser: string;
        ModDate: Date;
        LeadId: string;
    }
    class LeadInfoOutput implements ILeadInfoPage {
        constructor();
        InputModel: ILeadInputModel;
        LeadId: string;
        AvoidDuplicateAnalysis: boolean;
        ModUser: string;
        FirstName: string;
        MiddleName: string;
        LastName: string;
        NickName: string;
        LeadLastNameHistoryList: ILastNameHistory[];
        SSN: string;
        DriverLicenseNumber: string;
        AlienNumber: string;
        Age: number;
        Dependants: number;
        DistanceToSchool: number;
        Suffix: string;
        Gender: string;
        Prefix: string;
        RaceId: string;
        Dob: Date;
        Citizenship: string;
        Dependency: string;
        MaritalStatus: string;
        FamilyIncoming: string;
        HousingType: string;
        DrvLicStateCode: string;
        Transportation: string;
        IsDisabled: boolean;
        Vehicles: IVehicle[];
        PreferredContactId: number;
        CountyId: string;
        CountryId: string;
        AddressTypeId: string;
        City: string;
        OtherState: string;
        AddressStateId: string;
        Zip: string;
        Address1: string;
        AddressApt: string;
        Address2: string;
        PhonesList: IPhone[];
        EmailList: IeMail[];
        BestTime: Date;
        NoneEmail: boolean;
        SourceCategoryId: string;
        SourceTypeIdI: string;
        AdvertisementId: string;
        Note: string;
        SourceDateTime: Date;
        DateApplied: Date;
        AssignedDate: Date;
        AdmissionRepId: string;
        Comments: string;
        PreviousEducationId: string;
        HighSchoolId: string;
        HighSchoolGradDate: Date;
        AttendingHs: boolean;
        AdminCriteriaId: string;
        AgencySponsorId: string;
        CampusId: string;
        AreaId: string;
        ProgramId: string;
        PrgVerId: string;
        ScheduleId: string;
        ExpectedStart: Date;
        AttendTypeId: string;
        LeadStatusId: string;
        SdfList: ISdf[];
        GroupIdList: string[];
    }
}
declare module AD {
    class LeadInfoPage {
        viewModel: LeadInfoPageVm;
        constructor();
        initializeKendoComponents(): void;
    }
}
declare module AD {
    class LeadInfoPageDb {
        getResourcesForPage(pageResourceId: number, campusId: string, mcontext: any): JQueryXHR;
        getCatalogsInfoFromServer(fldName: string, mcontext: any): JQueryXHR;
        getLeadDemographic(leadGuid: Object, userId: string, mcontext: any): JQueryXHR;
        getLeadItemInformation(codeItemToGet: string, value: any, campusId: string, mcontext: any): JQueryXHR;
        getVehiclesFromServer(leadId: string, userId: string, mcontext: LeadInfoPageVm): JQueryXHR;
        postLeadInformation(lead: ILeadInfoPage, context: any): JQueryXHR;
        postLeadDelete(leadGuid: string, mcontext: any): JQueryXHR;
        getIsLeadErasable(leadGuid: string, mcontext: any): JQueryXHR;
        getAllowedStatus(campusId: string, lStatus: any, userId: string, mcontext: any): JQueryXHR;
        staticInformationForCaptionRequirement(): AD.ICaptionRequirement[];
    }
}
declare module AD {
    interface ICaptionRequirement {
        fldName: string;
        labelName: string;
        controlName?: string;
        isDll: boolean;
        caption?: string;
        isrequired?: boolean;
        dll?: any;
    }
    class LeadInfoPageVm extends kendo.Observable {
        preferredMethod: string;
        lastNameInitial: any;
        string: any;
        db: LeadInfoPageDb;
        captionRequirementdb: ICaptionRequirement[];
        filteredDatadb: ICaptionRequirement[];
        gruppenDb: IGroup[];
        storeVehicles: Array<string>;
        constructor();
        getResourcesForPage(islead: boolean): void;
        processSdfFields(sdfList: any): void;
        processLeadGroups(grps: any): void;
        initializePage(islead: boolean): void;
        configureRequired(db: ICaptionRequirement[]): void;
        setDllValue(controlName: string, value: any): void;
        getLeadDemographic(leadGuid: any): void;
        selectLeadAllowedStates(allowedStatusChange: any, leadStatusId: string): kendo.data.DataSource;
        calculateAge(dob: string, age: string): void;
        checkradiobutton(old: string, sel: string): void;
        getDropDownListItemObject(controlName: string): any;
        getFilteredDropDownListItemObject(controlName: string): any;
        initializeControls(campus: string, id?: string): void;
        fillFilteredDatadb(campusId: string): void;
        resizeDropDown: (e: any) => void;
        onClickContactInformation(): void;
        getLeadProgramInformation(msgOr: any, fldName: string, targetDropDown: string, value: Object, setting?: string): void;
        getLeadProgramTypeInformation(msgOr: any, fldName: string, targetDropDown: string, value: Object, setting?: string): void;
        getExpectedLeadItemInformation(campusId: string, fldName: string, targetDropDown: string, value: Object, setting?: string): void;
        getItemInformation(campusId: string, fldName: string, targetDropDown: string, value: Object, setting?: string): void;
        postLeadInformationToServer(leadGuid: string, avoidDuplicate: boolean): void;
        deleteLead(leadGuid: string): void;
        refreshCampusInControl(campusId: string): void;
        processExpectedStart(): Date;
        fillPrintInfo(p: IPrintOutputModel): IPrintOutputModel;
        executeCustomValidators(): boolean;
        storeVehicleInformation(): void;
        cancelVehicleEdition(): void;
    }
}
declare module AD {
    class LeadPrint {
        constructor();
        customTemplate(index: string, label: string, value: string): any;
    }
}
declare module AD {
    interface IUserdf {
        label: string;
        value: string;
    }
    class Userdf implements IUserdf {
        label: string;
        value: string;
    }
    interface IPrintOutputModel {
        photopath: string;
        preferredContact: string;
        bestTime: string;
        phoneBest: string;
        phone1: string;
        phone2: string;
        emailBest: string;
        email: string;
        address: string;
        county: string;
        cityStateZip: string;
        country: string;
        ssn: string;
        dobAge: string;
        gender: string;
        race: string;
        citizenship: string;
        dependency: string;
        dependents: string;
        alienNumber: string;
        maritalStatus: string;
        familyIncoming: string;
        housing: string;
        driverLicState: string;
        driverLicNumber: string;
        transportation: string;
        distanceToSchool: string;
        disabled: string;
        area: string;
        program: string;
        attend: string;
        schedule: string;
        expectedGrad: string;
        leadFullName: string;
        campusName: string;
        status: string;
        programVersion: string;
        expectedStart: string;
        admissionRep: string;
        dateAssigned: string;
        requirementMet: string;
        category: string;
        advertisement: string;
        typeAdvertisement: string;
        createdDate: string;
        note: string;
        dateApplied: string;
        previousEducation: string;
        sponsor: string;
        highSchool: string;
        adminCriteria: string;
        hsGradeDate: string;
        attendingHs: string;
        comments: string;
        reasonNotEnrolled: string;
        leadGroups: string;
        userDf: IUserdf[];
    }
    class PrintOutputModel implements IPrintOutputModel {
        photopath: string;
        preferredContact: string;
        bestTime: string;
        phoneBest: string;
        phone1: string;
        phone2: string;
        emailBest: string;
        email: string;
        address: string;
        county: string;
        cityStateZip: string;
        country: string;
        area: string;
        program: string;
        attend: string;
        schedule: string;
        expectedGrad: string;
        ssn: string;
        dobAge: string;
        gender: string;
        race: string;
        citizenship: string;
        dependency: string;
        dependents: string;
        alienNumber: string;
        maritalStatus: string;
        familyIncoming: string;
        housing: string;
        driverLicState: string;
        driverLicNumber: string;
        transportation: string;
        distanceToSchool: string;
        disabled: string;
        category: string;
        advertisement: string;
        typeAdvertisement: string;
        createdDate: string;
        note: string;
        dateApplied: string;
        previousEducation: string;
        sponsor: string;
        highSchool: string;
        adminCriteria: string;
        hsGradeDate: string;
        attendingHs: string;
        comments: string;
        reasonNotEnrolled: string;
        leadGroups: string;
        userDf: IUserdf[];
        leadFullName: string;
        campusName: string;
        status: string;
        programVersion: string;
        expectedStart: string;
        admissionRep: string;
        dateAssigned: string;
        requirementMet: string;
    }
}
declare module AD {
    interface INotes {
        IdNote: number;
        NoteDate: Date;
        ModuleName: string;
        Source: string;
        Type: string;
        Field: string;
        PageFieldId: number;
        NoteText: string;
        UserName: string;
        UserId: string;
    }
    interface ILeadNotesInputInfo {
        Command: number;
        LeadId: string;
        ModuleCode: string;
        UserId: string;
        CampusId: string;
    }
    interface ILeadNotesInfo {
        NotesList: INotes[];
        ModulesList: IDropDownItem[];
        Filter: ILeadNotesInputInfo;
    }
}
declare module AD {
    class LeadNotes {
        bo: LeadNotesBo;
        constructor();
        setPopupDimensions(ev: any): void;
    }
}
declare module AD {
    class LeadNotesBo {
        dataSource: kendo.data.DataSource;
        addNewRow(): (ev: UIEvent) => any;
        getLeadNotesInfo(moduleCode: string, command: number): kendo.data.DataSource;
        getTypeNotesEditor(container: any, options: any): void;
        textareaEditor(container: any, options: any): void;
        controlCommandEditButton(e: any): void;
    }
}
declare module AD {
    class Notes implements INotes {
        IdNote: number;
        NoteDate: Date;
        ModuleName: string;
        Source: string;
        Type: string;
        Field: string;
        NoteText: string;
        UserName: string;
        UserId: string;
        PageFieldId: number;
    }
    class LeadNotesInputInfo implements ILeadNotesInputInfo {
        Command: number;
        LeadId: string;
        ModuleCode: string;
        UserId: string;
        CampusId: string;
    }
    class LeadNotesInfo implements ILeadNotesInfo {
        NotesList: INotes[];
        ModulesList: IDropDownItem[];
        Filter: ILeadNotesInputInfo;
    }
}
declare module AD {
    class LeadQueue {
        vm: LeadQueueVm;
        selectedRow: any;
        constructor();
        myparseDate(data: any): string;
    }
}
declare module AD {
    class LeadQueueDb {
        getInfoFromServer(assingRepId: string, campusId: string, userId: string, command: number, mcontext: LeadQueueVm): JQueryXHR;
        setLeadInMru(campusId: string, userId: string, leadId: any, mcontext: LeadQueueVm): JQueryXHR;
        postLeadNewPhone(phone: IPhone, mcontext: LeadQueueVm): JQueryXHR;
    }
}
declare module AD {
    class LeadQueueVm {
        db: LeadQueueDb;
        constructor();
        getInfoFromServer(command?: number, assignedId?: string): void;
        commandGotoLeadPage(grid: kendo.ui.Grid, e: any): void;
        savePhone(selectedRow: any, grid: kendo.ui.Grid): void;
        refreshInfoFromServer(): void;
    }
}
declare module AD {
    class LeadToolbar {
        viewModel: LeadToolbarVm;
        constructor();
    }
}
declare module AD {
    class LeadToolBarDb {
        getFromServerFlagInformation(campusId: string, userId: string, command: number, mcontext: any): JQueryXHR;
        setFromServerQueueLead(campusId: string, userId: string, leadId: string, command: number, mcontext: LeadToolbarVm): JQueryXHR;
    }
}
declare module AD {
    class LeadToolbarVm extends kendo.Observable {
        infolead: kendo.data.DataSource;
        db: LeadInfoBarDb;
        constructor();
        goToAlarmPage(): void;
        goToQueuePage(): void;
        goToLeadInfoScreen(): void;
        goToLeadQuickScreen(): void;
        goToLeadNotesScreen(): void;
        goToLeadRequirementScreen(): void;
        goToLeadHistoryScreen(): void;
        goToLeadEducationScreen(): void;
        goToSPriorWorkScreen(): void;
        goToLeadExtracurricularScreen(): void;
        goToSkillScreen(): void;
        goToLeadEnrollmentScreen(): void;
        goToEmailPage(): void;
        goToLeadTaskScreen(): void;
        goToLeadScheduleScreen(): void;
        getFromServerFlagInformation(): void;
        goToSetQueueLead(e: any): void;
    }
}
declare module AD {
    var X_SAVE_PLEASE_FILL_REQUIRED_FIELDS: string;
    var X_SAVE_REVIEW_INFO_IN_FORM: string;
    var X_DELETE_SUCESS: string;
    var X_DELETE_NOT_POSSIBLE: string;
    var X_DELETE_ARE_YOU_SURE: string;
    var X_SELECT_ITEM: string;
}
