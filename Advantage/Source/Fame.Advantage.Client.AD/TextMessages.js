var AD;
(function (AD) {
    AD.X_SAVE_PLEASE_FILL_REQUIRED_FIELDS = "Please fill in all required fields to continue.";
    AD.X_SAVE_REVIEW_INFO_IN_FORM = "Please revise if all information was correctly input in the form!";
    AD.X_DELETE_SUCESS = "The lead was successfully deleted";
    AD.X_DELETE_NOT_POSSIBLE = "This lead record contains uploaded documents or processed fees and cannot be deleted.";
    AD.X_DELETE_ARE_YOU_SURE = "Are you sure to delete the Lead ";
    AD.X_SELECT_ITEM = "Select";
    AD.X_MESSAGE_NEW_EXTRACURRICULAR = "Enter a New Extracurricular";
    AD.X_MESSAGE_EDIT_EXTRACURRICULAR = "Edit the Extracurricular";
    AD.X_MESSAGE_EXTRACURRICULAR_DESCRIPTION_REQUIRED = "Extracurricular Description is required!";
    AD.X_MESSAGE_EXTRACURRICULAR_DESCRIPTION_CANNOT_BE_LONGER_THAN = "Description Cannot Be Longer Than 50 Characters!";
    AD.X_MESSAGE_EXTRACURRICULAR_COMMENT_CANNOT_BE_LONGER_THAN = "Comment Cannot Be Longer Than 100 Characters!";
})(AD || (AD = {}));
//# sourceMappingURL=TextMessages.js.map