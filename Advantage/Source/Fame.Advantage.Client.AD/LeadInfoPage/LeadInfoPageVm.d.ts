/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
declare module AD {
    interface ICaptionRequirement {
        fldName: string;
        labelName: string;
        controlName?: string;
        isDll: boolean;
        caption?: string;
        isrequired?: boolean;
        dll?: any;
    }
    class LeadInfoPageVm extends kendo.Observable {
        preferredMethod: string;
        lastNameInitial: any;
        string: any;
        db: LeadInfoPageDb;
        captionRequirementdb: ICaptionRequirement[];
        filteredDatadb: ICaptionRequirement[];
        gruppenDb: IGroup[];
        storeVehicles: Array<string>;
        assignedDateTime: Date;
        adminRepOld: string;
        constructor();
        getResourcesForPage(islead: boolean): void;
        processSdfFields(sdfList: any): void;
        processLeadGroups(grps: any): void;
        initializePage(islead: boolean): void;
        configureRequired(db: ICaptionRequirement[]): void;
        setDllValue(controlName: string, value: any): void;
        getLeadDemographic(leadGuid: any): void;
        selectLeadAllowedStates(allowedStatusChange: any, leadStatusId: string): kendo.data.DataSource;
        calculateAge(dob: string, age: string): void;
        checkradiobutton(old: string, sel: string): void;
        getDropDownListItemObject(controlName: string): any;
        getFilteredDropDownListItemObject(controlName: string): any;
        initializeControls(campus: string, id?: string): void;
        fillFilteredDatadb(campusId: string): void;
        resizeDropDown: (e: any) => void;
        onClickContactInformation(): void;
        getLeadProgramInformation(msgOr: any, fldName: string, targetDropDown: string, value: Object, setting?: string): void;
        getLeadProgramTypeInformation(msgOr: any, fldName: string, targetDropDown: string, value: Object, setting?: string): void;
        getExpectedLeadItemInformation(campusId: string, fldName: string, targetDropDown: string, value: Object, setting?: string): void;
        getItemInformation(campusId: string, fldName: string, targetDropDown: string, value: Object, setting?: string): void;
        postLeadInformationToServer(leadGuid: string, avoidDuplicate: boolean, redirectpage?: string): void;
        getFinalDate(dt: Date): string;
        deleteLead(leadGuid: string): void;
        refreshCampusInControl(campusId: string): void;
        processExpectedStart(): Date;
        fillPrintInfo(p: IPrintOutputModel): IPrintOutputModel;
        executeCustomValidators(): boolean;
        storeVehicleInformation(): void;
        cancelVehicleEdition(): void;
    }
}
