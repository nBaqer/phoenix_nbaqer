/// <reference path="../ServiceUrls.d.ts" />
declare module AD {
    class LeadInfoPageDb {
        getResourcesForPage(pageResourceId: number, campusId: string, mcontext: any): JQueryXHR;
        getCatalogsInfoFromServer(fldName: string, mcontext: any): JQueryXHR;
        getLeadDemographic(leadGuid: Object, userId: string, mcontext: any): JQueryXHR;
        getLeadItemInformation(codeItemToGet: string, value: any, campusId: string, mcontext: any): JQueryXHR;
        getVehiclesFromServer(leadId: string, userId: string, mcontext: LeadInfoPageVm): JQueryXHR;
        postLeadInformation(lead: ILeadInfoPageOutputModel, context: any): JQueryXHR;
        postLeadDelete(leadGuid: string, mcontext: any): JQueryXHR;
        getIsLeadErasable(leadGuid: string, mcontext: any): JQueryXHR;
        getAllowedStatus(campusId: string, lStatus: any, userId: string, mcontext: any): JQueryXHR;
        staticInformationForCaptionRequirement(): ICaptionRequirement[];
    }
}
