var AD;
(function (AD) {
    var LastNameHistory = (function () {
        function LastNameHistory() {
        }
        return LastNameHistory;
    }());
    AD.LastNameHistory = LastNameHistory;
    var Phone = (function () {
        function Phone() {
        }
        return Phone;
    }());
    AD.Phone = Phone;
    var Email = (function () {
        function Email() {
        }
        return Email;
    }());
    AD.Email = Email;
    var Sdf = (function () {
        function Sdf() {
        }
        return Sdf;
    }());
    AD.Sdf = Sdf;
    var Group = (function () {
        function Group() {
        }
        return Group;
    }());
    AD.Group = Group;
    var Vehicle = (function () {
        function Vehicle() {
        }
        return Vehicle;
    }());
    AD.Vehicle = Vehicle;
    var LeadAddressOutputModel = (function () {
        function LeadAddressOutputModel() {
        }
        return LeadAddressOutputModel;
    }());
    AD.LeadAddressOutputModel = LeadAddressOutputModel;
    var LeadInputModel = (function () {
        function LeadInputModel() {
        }
        return LeadInputModel;
    }());
    var LeadInfoPageOutputModel = (function () {
        function LeadInfoPageOutputModel() {
            this.Vehicles = [];
            this.PhonesList = [];
            this.EmailList = [];
            this.LeadLastNameHistoryList = [];
            this.InputModel = new LeadInputModel();
            this.LeadAddress = new LeadAddressOutputModel();
        }
        return LeadInfoPageOutputModel;
    }());
    AD.LeadInfoPageOutputModel = LeadInfoPageOutputModel;
})(AD || (AD = {}));
//# sourceMappingURL=LeadInfoOuput.js.map