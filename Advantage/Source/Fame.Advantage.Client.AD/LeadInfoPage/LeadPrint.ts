﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
module AD {

    export class LeadPrint {

        constructor() {
            //var userId: string = sessionStorage.getItem("USER_ID");
         
            // Hook the unload windows event, to remove information from session storage
           // window.addEventListener("unload", () => { sessionStorage.removeItem("PRINT_INFO"); });
            
            // Get Demographic Information
            var info: IPrintOutputModel = JSON.parse(sessionStorage.getItem("PRINT_INFO"));
            const unselected: string = "Select";

            // Header Info
            document.getElementById("printCampus").innerHTML = info.campusName.toUpperCase();
            var d = new Date();
            document.getElementById("printDateTime").innerHTML = ad.FORMAT_DATE_TO_MM_DD_YYYY(d);
                        
            // Get image from encode64...
            $("#printImage").attr("src", info.photopath);
            // enter second header information
            $("#hr1text2").val(info.leadFullName);
            $("#hr1text4").val(info.admissionRep === unselected ? "" : info.admissionRep);
            $("#hr2text2").val(info.status);
            $("#hr2text4").val(info.dateAssigned);
            $("#hr3text2").val(info.programVersion === unselected ? "" : info.programVersion);
            $("#hr3text4").val("");
            $("#hr4text2").val(info.expectedStart);

            $("#valPreferContact").val(info.preferredContact);
            $("#valPhoneBest").val(info.bestTime);
            $("#valPhone").val(info.phoneBest);
            $("#valPhone2").val(info.phone1);
            $("#valPhone3").val(info.phone2);
            $("#valEmailBest").val(info.emailBest);
            $("#valEmail").val(info.email);
            $("#valAddress").val(info.address);
            $("#valCounty").val(info.county === unselected ? "" : info.county);
            $("#valCityStateZip").val(info.cityStateZip);
            $("#valCountry").val(info.country === unselected ? "" : info.country);

            $("#valArea").val(info.area === unselected ? "" : info.area);
            $("#valProgram").val(info.program === unselected ? "" : info.program);
            $("#valAttend").val(info.attend === unselected ? "" : info.attend);
            $("#valSchedule").val(info.schedule === unselected ? "" : info.schedule);
            //$("#valExpectedGrad").val(info.expectedGrad);

            $("#valSsn").val(info.ssn);
            $("#valDobAge").val(info.dobAge);
            $("#valGender").val(info.gender === unselected ? "" : info.gender);
            $("#valRace").val(info.race === unselected ? "" : info.race);
            $("#valCitizenship").val(info.citizenship === unselected ? "" : info.citizenship);
            $("#valDependency").val(info.dependency === unselected ? "" : info.dependency);
            $("#valDependents").val(info.dependents);
            $("#valAlienNumber").val(info.alienNumber);
            $("#valMaritalStatus").val(info.maritalStatus === unselected ? "" : info.maritalStatus);
            $("#valFamilyIncoming").val(info.familyIncoming === unselected ? "" : info.familyIncoming);
            $("#valHousing").val(info.housing === unselected ? "" : info.housing);
            $("#valDriverLic").val(info.driverLicState === unselected ? "" : info.driverLicState);
            $("#valDriverNo").val(info.driverLicNumber);
            $("#valTransportation").val(info.transportation === unselected ? "" : info.transportation);
            $("#valDistToSchool").val(info.distanceToSchool);
            $("#valDisabled").val(info.disabled);

            $("#valCategory").val(info.category === unselected ? "" : info.category);
            $("#valAdvertisement").val(info.advertisement === unselected ? "" : info.advertisement);
            $("#valType").val(info.typeAdvertisement === unselected ? "" : info.typeAdvertisement);
            $("#valCreatedDate").val(info.createdDate);
            $("#valNote").val(info.note);

            $("#valDateApplied").val(info.dateApplied);
            $("#valPreviousEducation").val(info.previousEducation === unselected ? "" : info.previousEducation);
            $("#valSponsor").val(info.sponsor === unselected ? "" : info.sponsor);
            $("#valHs").val(info.highSchool === unselected ? "" : info.highSchool);
            $("#valAdminCriteria").val(info.adminCriteria === unselected ? "" : info.adminCriteria);
            $("#valHsGradeDate").val(info.hsGradeDate);
            $("#valHsAttending").val(info.attendingHs === unselected ? "" : info.attendingHs);
            $("#valComments").val(info.comments);
            $("#valReasonNotEnrolled").val(info.reasonNotEnrolled);

            $("#valGroups").val(info.leadGroups);
           
            // Dynamically construct the custom field
            for (var i = 0; i < info.userDf.length; i++) {
                var template: any = this.customTemplate(i.toString(), info.userDf[i].label, info.userDf[i].value);
                $("#customPlaceHolder").append(template);
            }

            // Call to print or preview
            window.print();
            window.onfocus = () => {
                window.close(); // Close the window
            }

        }

        customTemplate(index: string, label: string, value: string): any {

            var template = `<div class='corpusRow' ><label id='customL${index}' class='corpus1x' >${label}</label><input id= 'customI${index
                }' class='corpuslargeM' value= '${value}' />  </div><div class='clearfix'></div>`;

            return template;
        }

    }

} 