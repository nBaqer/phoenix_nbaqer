﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />

module AD {

    // ReSharper disable InconsistentNaming

    export interface ILastNameHistory {
        Id: number;
        LastName: string;
        LeadId: string;
    }

    export interface IPhone {
        ID: string;
        Position: number;
        IsForeignPhone: boolean;
        Phone: string;
        Extension: string;
        PhoneTypeId: string;
        UserId: string;
        LeadId: string;
    }
    // ReSharper enable InconsistentNaming

    export interface IeMail {
        ID: string;
        IsPreferred: boolean;
        Email: string;
        EmailType: string;
        EmailTypeId: string;
        IsShowOnLeadPage: string;
    }

    export interface ISdf {
        SdfId: string;
        SdfValue: string;
        DtypeId: number;
    }

    export interface IGroup {
        Description: string;
        CampusId: string;
        GroupId: string;
    }

    export interface IVehicle {
        /// <summary>
        /// vehicle unique ID
        /// </summary>
        Id: number;

        /// <summary>
        /// 1,2 are show in Vehicle1 Vehicle 2 in the interface
        /// Other values are not show
        /// LeadId and Position together are uniques.
        /// </summary>
        Position: number;

        /// <summary>
        /// Parking Permit
        /// </summary>
        Permit: string;

        /// <summary>
        /// Vehicle Maker
        /// </summary>
        Make: string;

        /// <summary>
        /// Vehicle Model
        /// </summary>
        Model: string;

        /// <summary>
        /// Vehicle Color
        /// </summary>
        Color: string;

        /// <summary>
        /// Vehicle Plate
        /// </summary>
        Plate: string;

        /// <summary>
        /// Modification user.
        /// </summary>
        ModUser: string;

        /// <summary>
        /// Modification Date
        /// </summary>
        ModDate: Date;

        /// <summary>
        /// Lead associated with the vehicle.
        /// </summary>
        LeadId: string;
    }

    /*
     * The Phone output for Leads
     *  Each of this field are associated with a lead in a list.
     */
    export interface ILeadAddressOutputModel {
        /*
         * Gets or sets the id.
         */
        ID: string;

        /*
         * Gets or sets The Address Type Description
         */
        AddressType: string;

        /*
         * Gets or sets The Address Type Id
         */
        AddressTypeId: string;

        /*
         * Gets or sets First Part of the Address (max 250 characters)
         */
        Address1: string;

        /*
         * Gets or sets Second Part of the Address (max 250 characters)
         */
        Address2: string;

        /*
         * Gets or sets Apartment number or Unit (max 20 characters)
         */
        AddressApto: string;

        /*
         * Gets or sets City of the Address
         */
        City: string;

        /*
         * Gets or sets State Name of the Address
         */
        State: string;

        /*
         * Gets or sets The State Id
         */
        StateId: string;

        /*
         * Gets or sets the state international.
         */
        StateInternational: string;

        /*
         * Gets or sets Zip Code of the Address
         */
        ZipCode: string;

        /*
         * Gets or sets Country name of the Address
         */
        Country: string;

        /*
         * Gets or sets The Country Id
         */
        CountryId: string;

        /*
         * Gets or sets Status description
         */
        Status: string;

        /*
         * Gets or sets The Status Id
         */
        StatusId: string;

        /*
         * Gets or sets Flag indicating if this address is the mailing one
         */
        IsMailingAddress: string;

        /*
         * Gets or sets 
         *  Flag indication if this address show ID be shown on the Lead page
         */
        IsShowOnLeadPage: string;

        /*
         * Gets or sets Last Modification Date
         */
        ModDate: string;

        /*
         * Gets or sets Last User Modified this record.
         */
        Moduser: string;

        /*
         * Gets or sets The County name
         */
        County: string;

        /*
         * Gets or sets The County Unique Id
         */
        CountyId: string;

        /*
         * Gets or sets a value indicating whether is international.
         */
        IsInternational: Boolean;
    }


    export interface ILeadInputModel {
        /// <summary>
        /// Lead Id
        /// </summary>
        LeadId: string;

        /// <summary>
        /// Campus ID
        /// </summary>
        CampusId: string;

        /// <summary>
        /// Actual Lead Status Id
        /// </summary>
        LeadStatusId: string;

        /// <summary>
        /// Actual User ID
        /// </summary>
        UserId: string;

        /// <summary>
        /// Page Resource ID. Each page in advantage has a resource.
        /// </summary>
        PageResourceId: number;

        /// <summary>
        /// Used to define a specific operation in the controller service
        /// </summary>
        CommandString: string;
    }

    /*
  * Output Model for Lead Demographic Information
  */
    export interface ILeadInfoPageOutputModel {
        /*
         * Gets or sets the lead id that the information is referred.
         */
        LeadId: string;

        /*
         * Gets or sets The user that does the modification.
         */
        ModUser: string;

        /*
         * Gets or sets Use to send parameters in post operations
         */
        InputModel: ILeadInputModel;

        /*
         * Gets or sets a value indicating whether avoid duplicate analysis.
         *  true is not executed.
         *  This is to permit enter a possible duplicate value for the user
         */
        AvoidDuplicateAnalysis: Boolean;

        /*
         * Gets or sets List of Id State Changes that can be possible
         *  with the actual lead status.
         */
        StateChangeIdsList: string[];

        /*
         * Gets or sets First Name
         */
        FirstName: string;

        /*
         * Gets or sets Middle Name
         */
        MiddleName: string;

        /*
         * Gets or sets Last Name
         */
        LastName: string;

        /*
         * Gets or sets Name Prefix
         */
        Prefix: string;

        /*
         * Gets or sets Name Suffix I, II, Junior
         */
        Suffix: string;

        /*
         * Gets or sets Nick Name
         */
        NickName: string;

        /*
         * Gets or sets Social Security Number
         */
        SSN: string;

        /*
         * Gets or sets Date of Birth
         */
        Dob: Date;

        /*
         * Gets or sets Citizen ship (ID)
         */
        Citizenship: string;

        /*
         * Gets or sets The USA Alien Number if any
         */
        AlienNumber: string;

        /*
         * Gets or sets If live independent or depend of other person
         */
        Dependency: string;

        /*
         * Gets or sets Marital Status Description
         */
        MaritalStatus: string;

        /*
         * Gets or sets other family that depend from the Lead.
         */
        Dependants: number;

        /*
         * Gets or sets Incoming Description
         */
        FamilyIncoming: string;

        /*
         * Gets or sets Example OffCampus, On Campus, Incarcerated ,with parent
         *  This is defined by school
         */
        HousingType: string;

        /*
         * Gets or sets State Code of driver license.
         */
        DrvLicStateCode: string;

        /*
         * Gets or sets Driver License Number
         */
        DriverLicenseNumber: string;

        /*
         * Gets or sets The transportation that the lead owner or use.
         */
        Transportation: string;

        /*
         * Gets or sets Gender
         */
        Gender: string;

        /*
         * Gets or sets Age
         */
        Age: string;

        /*
         * Gets or sets Miles from school to student house (-1 is not set)
         */
        DistanceToSchool: number;

        /*
         * Gets or sets If the lead is disabled or not.
         */
        IsDisabled: boolean;

        /*
         * Gets or sets Race GUID
         */
        RaceId: string;

        /*
         * Gets or sets History of the leads official last name changes
         */
        LeadLastNameHistoryList: ILastNameHistory[];

        /*
         * Gets or sets List of vehicles associated with the lead
         */
        Vehicles: IVehicle[];

        /*
         * Gets or sets Preferred Contact Id
         *  1:phone 2:email 3:text
         */
        PreferredContactId: number;

        /*
         * Gets or sets the lead address.
         */
        LeadAddress: ILeadAddressOutputModel;

        /*
         * Gets or sets A list of phones associated with the lead.
         */
        PhonesList: IPhone[];

        /*
         * Gets or sets List of lead emails.
         */
        EmailList: IeMail[];

        /*
         * Gets or sets Best time to contact the lead
         */
        BestTime: string;

        /*
         * Gets or sets a value indicating whether none email.
         */
        NoneEmail: boolean;

        /*
         * Gets or sets Id of the category of the source
         */
        SourceCategoryId: string;

        /*
         * Gets or sets The type of the lead source
         */
        SourceTypeId: string;

        /*
         * Gets or sets if was a advertisement, the type of it.
         */
        AdvertisementId: string;

        /**
         * Gets or sets the vendor source if the Lead was inserted by a third party vendor.
         */
        VendorSource: string;

        /*
         * Gets or sets A text referred to advertisement
         */
        Note: string;

        /*
         * Gets or sets Source Date Time. It is the moment that the lead
         *  is enter in the system.
         */
        SourceDateTime: string;

        /*
         * Gets or sets Date lead apply
         */
        DateApplied: string;

        /*
         * Gets or sets Date that is assigned to rep ad-min.
         */
        AssignedDate: string;

        /*
         * Gets or sets the admission rep id assigned
         */
        AdmissionRepId: string;

        /*
         * Gets or sets Agency Sponsor Id
         */
        AgencySponsorId: string;

        /*
         * Gets or sets Free comments about lead 100 characters null
         */
        Comments: string;

        /*
         * Gets or sets Lead previous education ID
         */
        PreviousEducationId: string;

        /*
         * Gets or sets HighSchool Id
         */
        HighSchoolId: string;

        /*
        * Gets or sets High School Name
        */
        HighSchoolName: string;

        /*
         * Gets or sets Lead Graduation day from high school
         */
        HighSchoolGradDate: Date;

        /*
         * Gets or sets Is the lead attending high school?
         */
        AttendingHs: number;

        /*
         * Gets or sets Administrative criteria to accept the lead.
         */
        AdminCriteriaId: string;

        /*
         * Gets or sets Reason id because was not enrolled the student
         */
        ReasonNotEnrolled: string;

        /*
         * Gets or sets Campus for the lead.
         */
        CampusId: string;

        /*
         * Gets or sets Area of interest PrgGroupdEscription in arPrgGrp
         */
        AreaId: string;

        /*
         * Gets or sets proDescrip in ArPrograms
         */
        ProgramId: string;

        /*
         * Gets or sets PrgVerDescrip in ArPrgVersions
         */
        PrgVerId: string;

        /*
         * Gets or sets Description from ArAttendTypes
         */
        AttendTypeId: string;

        /*
         * Gets or sets Lead Status
         */
        LeadStatusId: string;

        /*
         * Gets or sets Expected start Date, 
         *  This should be transformed in a string to compare
         *  with the begin of a string in the client.
         */
        ExpectedStart: Date;

        /*
         * Gets or sets Description from ArProgSchedules
         *  The value is droved by program version
         */
        ScheduleId: string;

        /*
         * Gets or sets Information about the SDF specific to the lead.
         */
        SdfList: ISdf[];

        /*
         * Gets or sets List of identifier of group that belong to lead.
         */
        GroupIdList: string[];

        IsEnrolled: boolean;
    }


    //export interface ILeadInfoPage {
    //    // ReSharper disable InconsistentNaming
    //    // Lead Id  who owned the information send here.
    //    LeadId: string;

    //    // The user that modified the field
    //    ModUser: string;

    //    // True indicate server does not analysis duplicate
    //    AvoidDuplicateAnalysis: boolean;

    //    // This is use in post operations.
    //    InputModel: ILeadInputModel;

    //    //#region Demographic
    //    FirstName: string;
    //    MiddleName: string;
    //    LastName: string;
    //    NickName: string;
    //    SSN: string;
    //    DriverLicenseNumber: string;
    //    AlienNumber: string;
    //    Age: number;
    //    Dependants: number;
    //    DistanceToSchool: number;
    //    Suffix: string;
    //    Gender: string;
    //    Prefix: string;
    //    Dob: Date;
    //    RaceId: string;
    //    Citizenship: string;
    //    Dependency: string;
    //    MaritalStatus: string;
    //    FamilyIncoming: string;
    //    HousingType: string;
    //    DrvLicStateCode: string;
    //    Transportation: string;
    //    IsDisabled: boolean;
    //    Vehicles: IVehicle[];
    //    //#endregion

    //    //#region Contact Info
    //    PreferredContactId: number;
    //    CountyId: string;
    //    CountryId: string;
    //    AddressTypeId: string;
    //    City: string;
    //    OtherState: string;
    //    AddressStateId: string;
    //    Zip: string;
    //    Address1: string;
    //    AddressApt: string;
    //    Address2: string;
    //    PhonesList: IPhone[];
    //    EmailList: IeMail[];
    //    LeadLastNameHistoryList: ILastNameHistory[];
    //    BestTime: Date;
    //    NoneEmail: boolean;
    //    //#endregion

    //    //#region Source fields
    //    SourceCategoryId: string;
    //    SourceTypeIdI: string;
    //    AdvertisementId: string;
    //    Note: string;
    //    SourceDateTime: Date;
    //    //#endregion

    //    //#region Others Fields
    //    DateApplied: Date;
    //    AssignedDate: Date;
    //    AdmissionRepId: string;
    //    Comments: string;
    //    PreviousEducationId: string;
    //    HighSchoolId: string;
    //    HighSchoolGradDate: Date;
    //    AttendingHs: boolean;
    //    AdminCriteriaId: string;
    //    AgencySponsorId: string;
    //    //#endregion

    //    //#region Academics.................................................................................
    //    CampusId: string;
    //    AreaId: string;
    //    ProgramId: string;
    //    PrgVerId: string;
    //    ScheduleId: string;
    //    ExpectedStart: Date;
    //    AttendTypeId: string;
    //    LeadStatusId: string;
    //    //#endregion

    //    //#region Custom Fields (School Defined Fields SDF)..............

    //    SdfList: ISdf[];

    //    //#endregion

    //    GroupIdList: string[];

    //    //#region Lead Groups


    //    // ReSharper restore InconsistentNaming
    //}
} 