﻿module AD {

    export interface IUserdf {
        label: string;
        value: string;
    }

    export class Userdf implements IUserdf {
        label: string;
        value: string;
    }

    export interface IPrintOutputModel {

        photopath: string;
        preferredContact: string;
        bestTime: string;
        phoneBest: string;
        phone1: string;
        phone2: string;
        emailBest: string;
        email: string;
        address: string;
        county: string;
        cityStateZip: string;
        country: string;
        ssn: string;
        dobAge: string;
        gender: string;
        race: string;
        citizenship: string;
        dependency: string;
        dependents: string;
        alienNumber: string;
        maritalStatus: string;
        familyIncoming: string;
        housing: string;
        driverLicState: string;
        driverLicNumber: string;
        transportation: string;
        distanceToSchool: string;
        disabled: string;

        area: string;
        program: string;
        attend: string;
        schedule: string;
        expectedGrad: string;

        leadFullName: string;
        campusName: string;
        status: string;
        programVersion: string;
        expectedStart: string;
        admissionRep: string;
        dateAssigned: string;
        requirementMet: string;

        category: string;
        advertisement: string;
        typeAdvertisement: string;
        createdDate: string;
        note: string;

        dateApplied: string;
        previousEducation: string;
        sponsor: string;
        highSchool: string;
        adminCriteria: string;
        hsGradeDate: string;
        attendingHs: string;
        comments: string;
        reasonNotEnrolled: string;

        leadGroups: string;
        userDf: IUserdf[];
    }

    export class PrintOutputModel implements IPrintOutputModel {

        photopath: string;
        preferredContact: string;
        bestTime: string;
        phoneBest: string;
        phone1: string;
        phone2: string;
        emailBest: string;
        email: string;
        address: string;
        county: string;
        cityStateZip: string;
        country: string;
        area: string;
        program: string;
        attend: string;
        schedule: string;
        expectedGrad: string;
        ssn: string;
        dobAge: string;
        gender: string;
        race: string;
        citizenship: string;
        dependency: string;
        dependents: string;
        alienNumber: string;
        maritalStatus: string;
        familyIncoming: string;
        housing: string;
        driverLicState: string;
        driverLicNumber: string;
        transportation: string;
        distanceToSchool: string;
        disabled: string;
        category: string;
        advertisement: string;
        typeAdvertisement: string;
        createdDate: string;
        note: string;
        dateApplied: string;
        previousEducation: string;
        sponsor: string;
        highSchool: string;
        adminCriteria: string;
        hsGradeDate: string;
        attendingHs: string;
        comments: string;
        reasonNotEnrolled: string;
        leadGroups: string;
        userDf: IUserdf[];
        leadFullName: string;
        campusName: string;
        status: string;
        programVersion: string;
        expectedStart: string;
        admissionRep: string;
        dateAssigned: string;
        requirementMet: string;
    }
} 