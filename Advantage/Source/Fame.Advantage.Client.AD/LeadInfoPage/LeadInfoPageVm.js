var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AD;
(function (AD) {
    var LeadInfoPageVm = (function (_super) {
        __extends(LeadInfoPageVm, _super);
        function LeadInfoPageVm() {
            _super.call(this);
            this.assignedDateTime = new Date();
            this.adminRepOld = "select";
            this.resizeDropDown = function (e) {
                e.list.width("auto");
            };
            this.preferredMethod = "Email";
            this.lastNameInitial = "";
            this.db = new AD.LeadInfoPageDb();
            this.filteredDatadb = this.db.staticInformationForCaptionRequirement();
        }
        LeadInfoPageVm.prototype.getResourcesForPage = function (islead) {
            var _this = this;
            $("body").css("cursor", "progress");
            var temp = sessionStorage.getItem(AD.XLEAD_INFO_DLLS_REQUIRED_ITEMS);
            var that = this;
            if (temp == null || temp === "{}") {
                this.captionRequirementdb = this.db.staticInformationForCaptionRequirement();
                this.db.getResourcesForPage(170, XMASTER_GET_CURRENT_CAMPUS_ID, this)
                    .done(function (msg) {
                    if (msg != undefined) {
                        var res1 = msg.RequiredList;
                        if (res1 != null) {
                            for (var i = 0; i < res1.length; i++) {
                                var fldName = res1[i].FldName;
                                for (var x = 0; x < _this.captionRequirementdb.length; x++) {
                                    if (_this.captionRequirementdb[x].fldName === fldName) {
                                        _this.captionRequirementdb[x].isrequired = res1[i].Required;
                                        _this.captionRequirementdb[x].dll = res1[i].Dll;
                                    }
                                }
                            }
                            sessionStorage.setItem(AD.XLEAD_INFO_DLLS_REQUIRED_ITEMS, JSON.stringify(_this.captionRequirementdb));
                        }
                        var res2 = msg.SdfList;
                        if (res2 != null) {
                            _this.processSdfFields(res2);
                            sessionStorage.setItem(AD.XLEAD_INFO_SDF_FIELDS, JSON.stringify(res2));
                        }
                        var res3 = msg.LeadGroupList;
                        if (res3 != null) {
                            _this.processLeadGroups(res3);
                            _this.gruppenDb = res3;
                            sessionStorage.setItem(AD.XLEAD_INFO_GROUPS, JSON.stringify(res3));
                        }
                        _this.initializePage(islead);
                    }
                })
                    .fail(function (msg) {
                    $("body").css("cursor", "default");
                    ad.SHOW_DATA_SOURCE_ERROR(msg);
                    return;
                });
            }
            else {
                that.captionRequirementdb = Object.create($.parseJSON(temp));
                var sdf = Object.create($.parseJSON(sessionStorage.getItem(AD.XLEAD_INFO_SDF_FIELDS)));
                this.processSdfFields(sdf);
                var grps = Object.create($.parseJSON(sessionStorage.getItem(AD.XLEAD_INFO_GROUPS)));
                this.gruppenDb = grps;
                this.processLeadGroups(grps);
                this.initializePage(islead);
            }
        };
        LeadInfoPageVm.prototype.processSdfFields = function (sdfList) {
            var html = '';
            var last = "none";
            for (var i = 0; i < sdfList.length; i++) {
                var requiredtext = "";
                if (sdfList[i].Required === "True") {
                    requiredtext = "<span style=color:red>*</span>";
                }
                html += "<div style='position:relative; width:50%; float:left'>";
                if (i % 2 === 0) {
                    var idcontrol = sdfList[i].SdfId;
                    html += "<label class='textLabelcustomleft' >" + sdfList[i].Description + requiredtext + "</label><input id='" + idcontrol + "' name='" + sdfList[i].Description + "' /> ";
                    last = "even";
                }
                else {
                    var idcontrol2 = sdfList[i].SdfId;
                    html += "<label class='customrightlabel'>" + sdfList[i].Description + requiredtext + "</label><input id='" + idcontrol2 + "'  name='" + sdfList[i].Description + "' /> ";
                    last = "odd";
                }
                html += "</div>";
                if (last === "odd") {
                    html += "<div class='clearfix'></div>";
                }
            }
            if (last === "even") {
                html += "<div style='position:relative; width:50%; float:left' ></div> <div class='clearfix'></div>";
            }
            $("#custom").append(html);
            for (var x = 0; x < sdfList.length; x++) {
                var idCtrl = sdfList[x].SdfId;
                if (sdfList[x].ControlType === "0" || sdfList[x].ControlType === "1") {
                    var textbox = "k-textbox fieldlabel1";
                    $("#" + idCtrl).attr("class", textbox);
                }
                if (sdfList[x].ControlType === "2") {
                    $("#" + idCtrl).kendoDropDownList({
                        dataSource: sdfList[x].ListOfValues,
                        optionLabel: 'Select option...'
                    });
                }
                if (sdfList[x].Required === "True") {
                    $("#" + idCtrl).attr('required', 'required');
                }
            }
        };
        LeadInfoPageVm.prototype.processLeadGroups = function (grps) {
            if (grps != undefined) {
                var filteredGroup = new Array();
                for (var j = 0; j < grps.length; j++) {
                    if (grps[j].CampusId === XMASTER_GET_CURRENT_CAMPUS_ID) {
                        filteredGroup.push(grps[j]);
                    }
                }
                for (var i = 0; i < filteredGroup.length; i++) {
                    var html = '<input type="checkbox" class="k-checkbox" id="' + filteredGroup[i].GroupId + '" /> <label class="k-checkbox-label f-checkbox-label" style="padding-left: 18px !important;" for="' + filteredGroup[i].GroupId + '">' + filteredGroup[i].Description + '</label>';
                    html = "<div class='leadGroupCheckboxStyle'>" + html + "</div>";
                    $("#leadGroups").append(html);
                }
            }
        };
        LeadInfoPageVm.prototype.initializePage = function (islead) {
            var campusId = ad.GET_QUERY_STRING_PARAMETER_BY_NAME("cmpId");
            this.fillFilteredDatadb(campusId);
            this.initializeControls(XMASTER_GET_CURRENT_CAMPUS_ID);
            this.configureRequired(this.captionRequirementdb);
            if (!islead) {
                assignedDateTime = new Date();
                adminRepOld = "select";
            }
            if (islead) {
                var leadElement = document.getElementById('leadId');
                var leadGuid = leadElement.value;
                this.getLeadDemographic(leadGuid);
            }
        };
        LeadInfoPageVm.prototype.configureRequired = function (db) {
            var caption = "";
            for (var i = 0; i < db.length; i++) {
                var label = $("#" + db[i].labelName);
                if (label != undefined) {
                    caption = label.text();
                    if (db[i].isrequired) {
                        caption = caption + "<span style=color:red>*</span>";
                        label.html(caption);
                    }
                }
                var control = $("#" + db[i].controlName);
                if (control != undefined) {
                    if (db[i].isrequired) {
                        control.attr('required', 'required');
                        control.attr('name', caption);
                        $("<span class='k-invalid-msg' data-for='" + control.attr('name') + "'></span>").insertAfter(control);
                    }
                }
            }
        };
        LeadInfoPageVm.prototype.setDllValue = function (controlName, value) {
            for (var i = 0; i < this.captionRequirementdb.length; i++) {
                if (this.captionRequirementdb[i].controlName === controlName) {
                    this.captionRequirementdb[i].dll = value;
                    break;
                }
            }
        };
        LeadInfoPageVm.prototype.getLeadDemographic = function (leadGuid) {
            var _this = this;
            var userId = $("#hdnUserId").val();
            this.db.getLeadDemographic(leadGuid, userId, this)
                .done(function (msg1) {
                if (msg1 != undefined) {
                    try {
                        var msg = msg1;
                        $("#txtFirstName").val(msg.FirstName);
                        $("#txtMiddleName").val(msg.MiddleName);
                        $("#txtLastName").val(msg.LastName);
                        _this.lastNameInitial = msg.LastName;
                        var pln = $("#cbPrevlast").data("kendoDropDownList");
                        var dshistory = new kendo.data.DataSource();
                        for (var n = 0; n < msg.LeadLastNameHistoryList.length; n++) {
                            dshistory.add(msg.LeadLastNameHistoryList[n]);
                        }
                        pln.setDataSource(dshistory);
                        if (pln.dataSource.data.length > 0) {
                            pln.select(0);
                        }
                        $("#tbNickname").val(msg.NickName);
                        $("#maskedsocialSecurity").data("kendoMaskedTextBox").value(msg.SSN);
                        $("#tdriverlicensenumber").val(msg.DriverLicenseNumber);
                        $("#tbaliennumber").val(msg.AlienNumber);
                        $("#textage").val(msg.Age);
                        $("#itextdependants").data("kendoNumericTextBox").value(msg.Dependants);
                        $("#cbsuffix").data("kendoDropDownList").value(msg.Suffix);
                        $("#cbgender").data("kendoDropDownList").value(msg.Gender);
                        $("#cbprefix").data("kendoDropDownList").value(msg.Prefix);
                        $("#cbRace").data("kendoDropDownList").value(msg.RaceId);
                        if (msg.Dob != null) {
                            _this.calculateAge(msg.Dob.toString(), msg.Age);
                        }
                        else {
                            _this.calculateAge(null, msg.Age);
                        }
                        $("#cbcitizenship").data("kendoDropDownList").value(msg.Citizenship);
                        $("#cbdependency").data("kendoDropDownList").value(msg.Dependency);
                        $("#cbmaritalstatus").data("kendoDropDownList").value(msg.MaritalStatus);
                        $("#cbfamilyincome").data("kendoDropDownList").value(msg.FamilyIncoming);
                        $("#cbhousingtype").data("kendoDropDownList").value(msg.HousingType);
                        $("#cbdriverlicensestate").data("kendoDropDownList").value(msg.DrvLicStateCode);
                        $("#cbtransportation").data("kendoDropDownList").value(msg.Transportation);
                        if (msg.DistanceToSchool != null) {
                            $("#tpTimeTo").data("kendoNumericTextBox").value(msg.DistanceToSchool);
                        }
                        for (var x = 0; x < msg.Vehicles.length; x++) {
                            if (msg.Vehicles[x].Position === 1) {
                                $("#vehicleParkingPermit1").val(msg.Vehicles[x].Permit);
                                $("#vehicleMaker1").val(msg.Vehicles[x].Make);
                                $("#vehicleModel1").val(msg.Vehicles[x].Model);
                                $("#vehicleColor1").val(msg.Vehicles[x].Color);
                                $("#vehiclePlate1").val(msg.Vehicles[x].Plate);
                            }
                            if (msg.Vehicles[x].Position === 2) {
                                $("#vehicleParkingPermit2").val(msg.Vehicles[x].Permit);
                                $("#vehicleMaker2").val(msg.Vehicles[x].Make);
                                $("#vehicleModel2").val(msg.Vehicles[x].Model);
                                $("#vehicleColor2").val(msg.Vehicles[x].Color);
                                $("#vehiclePlate2").val(msg.Vehicles[x].Plate);
                            }
                        }
                        switch (msg.PreferredContactId) {
                            case 1:
                                $("#preferredPhone").click();
                                break;
                            case 2:
                                $("#preferredEmail").click();
                                break;
                            case 3:
                                $("#preferredText").click();
                                break;
                            default:
                                {
                                    $("#preferredPhone").click();
                                }
                        }
                        if (msg.LeadAddress != null) {
                            $("#cbCounty").data("kendoDropDownList").value(msg.LeadAddress.CountyId);
                            $("#cbcountry").data("kendoDropDownList").value(msg.LeadAddress.CountryId);
                            $("#cbAddress").data("kendoDropDownList").value(msg.LeadAddress.AddressTypeId);
                            $("#txtCity").val(msg.LeadAddress.City);
                            var isInt = (msg.LeadAddress.StateInternational != null && msg.LeadAddress.StateInternational !== "");
                            var cbState = $("#cbState").data("kendoDropDownList");
                            var otherState = $("#otherStates");
                            var zipcode = $("#txtZipCode").data("kendoMaskedTextBox");
                            if (isInt) {
                                $("#checkIntAddress").prop('checked', true);
                                cbState.wrapper.hide();
                                otherState.css("display", "block");
                                otherState.val(msg.LeadAddress.StateInternational);
                                zipcode.setOptions({ mask: '9999999999' });
                            }
                            else {
                                $("#checkIntAddress").prop('checked', false);
                                cbState.wrapper.show();
                                otherState.css("display", "none");
                                zipcode.setOptions({ mask: '00000' });
                                cbState.value(msg.LeadAddress.StateId);
                            }
                            zipcode.value(msg.LeadAddress.ZipCode);
                            $("#txtAddress1").val(msg.LeadAddress.Address1);
                            $("#txtApart").val(msg.LeadAddress.AddressApto);
                            $("#txtAddress2").val(msg.LeadAddress.Address2);
                        }
                        if (msg.PhonesList != undefined && msg.PhonesList != null && msg.PhonesList.length > 0) {
                            var list = msg.PhonesList;
                            for (var i = 0; i < list.length; i++) {
                                if (list[i].Position === 1) {
                                    var isf = list[i].IsForeignPhone;
                                    $("#checkphone").prop("checked", isf);
                                    if (isf) {
                                        $("#txtPhone").data("kendoMaskedTextBox").setOptions({ mask: '999999999999999' });
                                    }
                                    $("#txtPhone").data("kendoMaskedTextBox").value(list[i].Phone);
                                    $("#hiddenPhoneId").val(list[i].ID);
                                    $("#tbphoneext").val(list[i].Extension);
                                    $("#cbphone").data("kendoDropDownList").value(list[i].PhoneTypeId);
                                }
                                if (list[i].Position === 2) {
                                    var isf1 = list[i].IsForeignPhone;
                                    $("#checkphone1").prop("checked", isf1);
                                    if (isf1) {
                                        $("#txtPhone1").data("kendoMaskedTextBox").setOptions({ mask: '999999999999999' });
                                    }
                                    $("#txtPhone1").data("kendoMaskedTextBox").value(list[i].Phone);
                                    $("#hiddenPhone1Id").val(list[i].ID);
                                    $("#tbphone1ext").val(list[i].Extension);
                                    $("#cbphone1").data("kendoDropDownList").value(list[i].PhoneTypeId);
                                }
                                if (list[i].Position === 3) {
                                    var isf2 = list[i].IsForeignPhone;
                                    $("#checkphone2").prop("checked", isf2);
                                    if (isf2) {
                                        $("#txtPhone2").data("kendoMaskedTextBox").setOptions({ mask: '999999999999999' });
                                    }
                                    $("#txtPhone2").data("kendoMaskedTextBox").value(list[i].Phone);
                                    $("#hiddenPhone2Id").val(list[i].ID);
                                    $("#tbphone2ext").val(list[i].Extension);
                                    $("#cbphone2").data("kendoDropDownList").value(list[i].PhoneTypeId);
                                }
                            }
                        }
                        if (msg.EmailList != undefined && msg.EmailList != null && msg.EmailList.length > 0) {
                            var list1 = msg.EmailList;
                            for (var j = 0; j < list1.length; j++) {
                                var preferred = list1[j].IsPreferred;
                                if (preferred === 1 || preferred === true || preferred === "True") {
                                    $("#txtemailprimary").val(list1[j].Email);
                                    $("#hiddenEmailPrimaryId").val(list1[j].ID);
                                    $("#cbemailprimary").data("kendoDropDownList").value(list1[j].EmailTypeId);
                                }
                                else {
                                    $("#txtemail").val(list1[j].Email);
                                    $("#hiddenEmailId").val(list1[j].ID);
                                    $("#cbemail").data("kendoDropDownList").value(list1[j].EmailTypeId);
                                }
                            }
                        }
                        $("#tpBestTime").data("kendoTimePicker").value(msg.BestTime);
                        $("#checkNoEmail").prop("checked", msg.NoneEmail);
                        $("#txtemailprimary").prop("disabled", msg.NoneEmail);
                        $("#txtemail").prop("disabled", msg.NoneEmail);
                        if (msg.NoneEmail) {
                            $("#txtemail").css("color", "white");
                            $("#txtemailprimary").css("color", "white");
                        }
                        $("#cbcategorySource").data("kendoDropDownList").value(msg.SourceCategoryId);
                        $("#cbtypeSource").data("kendoDropDownList").value(msg.SourceTypeId);
                        $("#cbAdvertisementSource").data("kendoDropDownList").value(msg.AdvertisementId);
                        $("#txtNoteSource").val(msg.Note);
                        if (msg.SourceDateTime != null) {
                            $("#dtDateSource").data("kendoDateTimePicker").value(msg.SourceDateTime);
                        }
                        if (msg.DateApplied != null) {
                            $("#dtotherDateApplied").data("kendoDatePicker").value(msg.DateApplied);
                        }
                        var assignData = $("#dtotherAdmRepAssignDate").data("kendoDatePicker");
                        if (msg.AssignedDate != null) {
                            assignData.value(msg.AssignedDate);
                            assignedDateTime = msg.AssignedDate;
                        }
                        else {
                            assignData.value(new Date());
                            assignedDateTime = new Date();
                        }
                        $("#cbOtherHighSchool").data("kendoDropDownList").value(msg.HighSchoolId);
                        if (msg.AdmissionRepId != null) {
                            $("#cbotherAdmRep").data("kendoDropDownList").value(msg.AdmissionRepId);
                            adminRepOld = msg.AdmissionRepId;
                        }
                        else {
                            adminRepOld = "select";
                        }
                        $("#cbotherSponsor").data("kendoDropDownList").value(msg.AgencySponsorId);
                        $("#tbOtherComment").val(msg.Comments);
                        $("#cbotherPreviousEducation").data("kendoDropDownList").value(msg.PreviousEducationId);
                        if (msg.HighSchoolGradDate != null) {
                            $("#dtHsGradDate").data("kendoDatePicker").value(msg.HighSchoolGradDate);
                        }
                        if (msg.AttendingHs == null) {
                            msg.AttendingHs = -1;
                        }
                        $("#ddAttendingHs").data("kendoDropDownList").value(msg.AttendingHs);
                        $("#cbotherAdmCriteria").data("kendoDropDownList").value(msg.AdminCriteriaId);
                        if (msg.CampusId == null) {
                            msg.CampusId = XMASTER_GET_CURRENT_CAMPUS_ID;
                        }
                        $("#cbLeadAssign").data("kendoDropDownList").value(msg.CampusId);
                        $("#cbInterestArea").data("kendoDropDownList").value(msg.AreaId);
                        _this.getLeadProgramInformation(msg, "ProgramID", "cbprogramLabel", msg.AreaId, msg.ProgramId);
                        var cbattend = $("#cbattend").data("kendoDropDownList");
                        cbattend.value(msg.AttendTypeId);
                        sessionStorage.setItem("allowedLeadStatus", JSON.stringify(msg.StateChangeIdsList));
                        var dllnew = _this.selectLeadAllowedStates(msg.StateChangeIdsList, msg.LeadStatusId);
                        var statusControl = $("#cbLeadStatus").data("kendoDropDownList");
                        statusControl.setDataSource(dllnew);
                        statusControl.value(msg.LeadStatusId);
                        if (msg.SdfList != null) {
                            for (var k = 0; k < msg.SdfList.length; k++) {
                                var id = "#" + msg.SdfList[k].SdfId;
                                if ($(id).attr("data-role") === 'dropdownlist') {
                                    $(id).data("kendoDropDownList").value(msg.SdfList[k].SdfValue);
                                }
                                else {
                                    $(id).val(msg.SdfList[k].SdfValue);
                                }
                            }
                        }
                        var grps = Object.create($.parseJSON(sessionStorage.getItem(AD.XLEAD_INFO_GROUPS)));
                        if (grps != null) {
                            for (var l = 0; l < grps.length; l++) {
                                $("#" + grps[l].GroupId).prop("checked", false);
                            }
                            if (msg.GroupIdList != null) {
                                for (var m = 0; m < msg.GroupIdList.length; m++) {
                                    $("#" + msg.GroupIdList[m]).prop("checked", true);
                                }
                            }
                        }
                        $("body").css("cursor", "default");
                    }
                    catch (e) {
                        sessionStorage.removeItem(AD.XLEAD_INFO_DLLS_REQUIRED_ITEMS);
                        sessionStorage.removeItem(AD.XLEAD_INFO_GROUPS);
                        sessionStorage.removeItem(AD.XLEAD_INFO_SDF_FIELDS);
                        $("body").css("cursor", "default");
                        alert(e.message);
                    }
                }
            })
                .fail(function (msg) {
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadInfoPageVm.prototype.selectLeadAllowedStates = function (allowedStatusChange, leadStatusId) {
            var dllnew = new kendo.data.DataSource();
            var dll = this.getDropDownListItemObject("cbLeadStatus");
            if (allowedStatusChange !== null && allowedStatusChange !== undefined && allowedStatusChange.length > 0) {
                for (var n = 0; n < dll.length; n++) {
                    for (var o = 0; o < allowedStatusChange.length; o++) {
                        if (dll[n].ID.toLocaleString().toUpperCase() === allowedStatusChange[o].toLocaleString().toUpperCase()) {
                            dllnew.add(dll[n]);
                        }
                    }
                }
            }
            var result = $.grep(dll, function (t) { return t.ID === leadStatusId; });
            dllnew.add(result[0]);
            return dllnew;
        };
        LeadInfoPageVm.prototype.calculateAge = function (dob, age) {
            if (dob != null && dob !== "") {
                $("#dtDob").data("kendoDatePicker").value(new Date(dob));
                var calAge = ad.GET_AGE(dob);
                $("#textage").val(calAge.toString());
            }
            else {
                $("#textage").val(age);
            }
        };
        LeadInfoPageVm.prototype.checkradiobutton = function (old, sel) {
            var rfd = $("input[name='contact'][value= '" + old + "'").attr("_rfddecoratedID");
            $("#" + rfd).attr("class", "rfdRadioUnchecked");
            var rfddecoratedId = $("input[name='contact'][value= '" + sel + "'").attr("_rfddecoratedID");
            $("#" + rfddecoratedId).attr("class", "rfdRadioChecked");
        };
        LeadInfoPageVm.prototype.getDropDownListItemObject = function (controlName) {
            for (var i = 0; i < this.captionRequirementdb.length; i++) {
                if (this.captionRequirementdb[i].controlName === controlName) {
                    return this.captionRequirementdb[i].dll;
                }
            }
            return null;
        };
        LeadInfoPageVm.prototype.getFilteredDropDownListItemObject = function (controlName) {
            for (var i = 0; i < this.captionRequirementdb.length; i++) {
                if (this.filteredDatadb[i].controlName === controlName) {
                    return this.filteredDatadb[i].dll;
                }
            }
            return null;
        };
        LeadInfoPageVm.prototype.initializeControls = function (campus, id) {
            if (id === void 0) { id = ""; }
            var cbLeadStatus = $("#cbLeadStatus").data("kendoDropDownList");
            var dll = this.getDropDownListItemObject("cbLeadStatus");
            if (id === "") {
                for (var i = 0; i < dll.length; i++) {
                    if (dll[i].Description.indexOf("**") === 0) {
                        id = dll[i].ID;
                        dll[i].Description = dll[i].Description.replace("**", "");
                        break;
                    }
                }
            }
            var cad = sessionStorage.getItem("allowedLeadStatus");
            var allowedStates;
            if (cad !== undefined && cad !== null && cad.length > 10) {
                allowedStates = cad.split(",");
            }
            else {
                allowedStates = [];
            }
            dll = this.selectLeadAllowedStates(allowedStates, id);
            cbLeadStatus.setDataSource(dll);
            cbLeadStatus.value(id);
            cbLeadStatus.list.css("min-width", "150px");
            cbLeadStatus.list.width("auto");
            var cbLeadAssign = $("#cbLeadAssign").data("kendoDropDownList");
            cbLeadAssign.setDataSource(this.getDropDownListItemObject("cbLeadAssign"));
            cbLeadAssign.value(campus);
            var cbInterestArea = $("#cbInterestArea").data("kendoDropDownList");
            cbInterestArea.setDataSource(this.getFilteredDropDownListItemObject("cbInterestArea"));
            var cbattend = $("#cbattend").data("kendoDropDownList");
            cbattend.setDataSource(this.getFilteredDropDownListItemObject("cbattend"));
            var cbprefix = $("#cbprefix").data("kendoDropDownList");
            cbprefix.setDataSource(this.getDropDownListItemObject("cbprefix"));
            cbprefix.list.css("min-width", "90px");
            cbprefix.list.width("auto");
            var cbsuffix = $("#cbsuffix").data("kendoDropDownList");
            cbsuffix.setDataSource(this.getDropDownListItemObject("cbsuffix"));
            cbsuffix.list.css("min-width", "90px");
            cbsuffix.list.width("auto");
            var cbgender = $("#cbgender").data("kendoDropDownList");
            cbgender.setDataSource(this.getDropDownListItemObject("cbgender"));
            cbgender.list.css("min-width", "90px");
            cbgender.list.width("auto");
            var cbmaritalstatus = $("#cbmaritalstatus").data("kendoDropDownList");
            cbmaritalstatus.setDataSource(this.getDropDownListItemObject("cbmaritalstatus"));
            var cbdriverlicensestate = $("#cbdriverlicensestate").data("kendoDropDownList");
            cbdriverlicensestate.setDataSource(this.getDropDownListItemObject("cbdriverlicensestate"));
            var cbcitizenship = $("#cbcitizenship").data("kendoDropDownList");
            cbcitizenship.setDataSource(this.getDropDownListItemObject("cbcitizenship"));
            var cbdependency = $("#cbdependency").data("kendoDropDownList");
            cbdependency.setDataSource(this.getDropDownListItemObject("cbdependency"));
            var cbfamilyincome = $("#cbfamilyincome").data("kendoDropDownList");
            cbfamilyincome.setDataSource(this.getDropDownListItemObject("cbfamilyincome"));
            var cbhousingtype = $("#cbhousingtype").data("kendoDropDownList");
            cbhousingtype.setDataSource(this.getDropDownListItemObject("cbhousingtype"));
            var cbtransportation = $("#cbtransportation").data("kendoDropDownList");
            cbtransportation.setDataSource(this.getDropDownListItemObject("cbtransportation"));
            var cbRace = $("#cbRace").data("kendoDropDownList");
            cbRace.setDataSource(this.getFilteredDropDownListItemObject("cbRace"));
            cbRace.list.css("min-width", "90px");
            cbRace.list.width("auto");
            var cbcategorySource = $("#cbcategorySource").data("kendoDropDownList");
            cbcategorySource.setDataSource(this.getDropDownListItemObject("cbcategorySource"));
            var cbtypeSource = $("#cbtypeSource").data("kendoDropDownList");
            cbtypeSource.setDataSource(this.getDropDownListItemObject("cbtypeSource"));
            var cbAdvertisementSource = $("#cbAdvertisementSource").data("kendoDropDownList");
            cbAdvertisementSource.setDataSource(this.getDropDownListItemObject("cbAdvertisementSource"));
            var cbphone = $("#cbphone").data("kendoDropDownList");
            cbphone.setDataSource(this.getDropDownListItemObject("cbphone"));
            var cbphone1 = $("#cbphone1").data("kendoDropDownList");
            cbphone1.setDataSource(this.getDropDownListItemObject("cbphone1"));
            var cbphone2 = $("#cbphone2").data("kendoDropDownList");
            cbphone2.setDataSource(this.getDropDownListItemObject("cbphone2"));
            var cbAddress = $("#cbAddress").data("kendoDropDownList");
            cbAddress.setDataSource(this.getFilteredDropDownListItemObject("cbAddress"));
            var cbemailprimary = $("#cbemailprimary").data("kendoDropDownList");
            cbemailprimary.setDataSource(this.getDropDownListItemObject("cbemailprimary"));
            var cbemail = $("#cbemail").data("kendoDropDownList");
            cbemail.setDataSource(this.getDropDownListItemObject("cbemail"));
            var cbcountry = $("#cbcountry").data("kendoDropDownList");
            cbcountry.setDataSource(this.getFilteredDropDownListItemObject("cbcountry"));
            var cbCounty = $("#cbCounty").data("kendoDropDownList");
            cbCounty.setDataSource(this.getFilteredDropDownListItemObject("cbCounty"));
            var cbState = $("#cbState").data("kendoDropDownList");
            cbState.setDataSource(this.getDropDownListItemObject("cbState"));
            var cbotherAdmRep = $("#cbotherAdmRep").data("kendoDropDownList");
            cbotherAdmRep.setDataSource(this.getFilteredDropDownListItemObject("cbotherAdmRep"));
            var cbotherPreviousEducation = $("#cbotherPreviousEducation").data("kendoDropDownList");
            cbotherPreviousEducation.setDataSource(this.getFilteredDropDownListItemObject("cbotherPreviousEducation"));
            var cbOtherHighSchool = $("#cbOtherHighSchool").data("kendoDropDownList");
            cbOtherHighSchool.setDataSource(this.getFilteredDropDownListItemObject("cbOtherHighSchool"));
            var cbotherAdmCriteria = $("#cbotherAdmCriteria").data("kendoDropDownList");
            cbotherAdmCriteria.setDataSource(this.getFilteredDropDownListItemObject("cbotherAdmCriteria"));
            var cbotherReasonNot = $("#cbotherReasonNot").data("kendoDropDownList");
            cbotherReasonNot.list.width("auto");
            var cbotherSponsor = $("#cbotherSponsor").data("kendoDropDownList");
            cbotherSponsor.list.width("auto");
            this.resizeDropDown(cbprefix);
            this.resizeDropDown(cbsuffix);
            this.resizeDropDown(cbgender);
            this.resizeDropDown(cbdependency);
            this.resizeDropDown(cbmaritalstatus);
            this.resizeDropDown(cbfamilyincome);
            this.resizeDropDown(cbhousingtype);
            this.resizeDropDown(cbdriverlicensestate);
            this.resizeDropDown(cbtransportation);
            this.resizeDropDown(cbcitizenship);
            this.resizeDropDown(cbattend);
            this.resizeDropDown(cbLeadStatus);
            this.resizeDropDown(cbphone);
            this.resizeDropDown(cbphone1);
            this.resizeDropDown(cbphone2);
            this.resizeDropDown(cbState);
            this.resizeDropDown(cbAddress);
            this.resizeDropDown(cbemailprimary);
            this.resizeDropDown(cbemail);
            this.resizeDropDown(cbotherSponsor);
            this.resizeDropDown(cbotherPreviousEducation);
            this.resizeDropDown(cbotherReasonNot);
        };
        LeadInfoPageVm.prototype.fillFilteredDatadb = function (campusId) {
            for (var i = 0; i < this.captionRequirementdb.length; i++) {
                if (this.captionRequirementdb[i].dll == undefined || this.captionRequirementdb[i].dll.length === 0) {
                    continue;
                }
                this.filteredDatadb[i].dll = [];
                for (var x = 0; x < this.captionRequirementdb[i].dll.length; x++) {
                    if (this.captionRequirementdb[i].dll[x].CampusesIdList != undefined
                        && this.captionRequirementdb[i].dll[x].CampusesIdList.length > 0) {
                        var list = this.captionRequirementdb[i].dll[x].CampusesIdList;
                        for (var j = 0; j < list.length; j++) {
                            if (list[j].CampusId === campusId) {
                                this.filteredDatadb[i].dll.push(this.captionRequirementdb[i].dll[x]);
                                break;
                            }
                        }
                    }
                }
            }
        };
        LeadInfoPageVm.prototype.onClickContactInformation = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/leadContacts.aspx?RESID=270&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Quick&mod=AD");
        };
        LeadInfoPageVm.prototype.getLeadProgramInformation = function (msgOr, fldName, targetDropDown, value, setting) {
            var _this = this;
            if (setting === void 0) { setting = null; }
            this.db.getLeadItemInformation(fldName, value, XMASTER_GET_CURRENT_CAMPUS_ID, this)
                .done(function (msg) {
                if (msg != undefined) {
                    var cbprogramLabel = $("#" + targetDropDown).data("kendoDropDownList");
                    cbprogramLabel.setDataSource(msg);
                    if (setting != null) {
                        cbprogramLabel.value(setting);
                    }
                    _this.getLeadProgramTypeInformation(msgOr, "PrgVerId", "cbprogramVersion", msgOr.ProgramId, msgOr.PrgVerId);
                    if (XMASTER_LEAD_IS_CLOCK_HOUR === 'Yes') {
                        $("#dpExpectedStart").data("kendoDatePicker").value(kendo.parseDate(msgOr.ExpectedStart));
                    }
                    else {
                        if (msgOr.ExpectedStart == null) {
                            _this.getExpectedLeadItemInformation(XMASTER_GET_CURRENT_CAMPUS_ID, "ExpectedStart", "cbExpectedStart", msgOr.ProgramId);
                        }
                        else {
                            var dat1 = kendo.parseDate(msgOr.ExpectedStart);
                            var lookdate = dat1.toLocaleString("en-US").split(" ")[0].toString();
                            lookdate = lookdate.replace(",", "");
                            _this.getExpectedLeadItemInformation(XMASTER_GET_CURRENT_CAMPUS_ID, "ExpectedStart", "cbExpectedStart", msgOr.ProgramId, lookdate);
                        }
                    }
                }
            })
                .fail(function (msg) {
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadInfoPageVm.prototype.getLeadProgramTypeInformation = function (msgOr, fldName, targetDropDown, value, setting) {
            var _this = this;
            if (setting === void 0) { setting = null; }
            this.db.getLeadItemInformation(fldName, value, XMASTER_GET_CURRENT_CAMPUS_ID, this)
                .done(function (msg) {
                if (msg != undefined) {
                    var cbprogramVersion = $("#" + targetDropDown).data("kendoDropDownList");
                    cbprogramVersion.setDataSource(msg);
                    if (setting != null) {
                        cbprogramVersion.value(setting);
                    }
                    _this.getItemInformation(XMASTER_GET_CURRENT_CAMPUS_ID, "ProgramScheduleId", "cbschedule", msgOr.PrgVerId, msgOr.ScheduleId);
                }
            })
                .fail(function (msg) {
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadInfoPageVm.prototype.getExpectedLeadItemInformation = function (campusId, fldName, targetDropDown, value, setting) {
            if (setting === void 0) { setting = null; }
            this.db.getLeadItemInformation(fldName, value, campusId, this)
                .done(function (msg) {
                if (msg != undefined) {
                    var cbExpected = $("#" + targetDropDown).data("kendoDropDownList");
                    cbExpected.setDataSource(msg);
                    if (setting != null) {
                        var list = cbExpected.dataSource.data();
                        if (list != null) {
                            for (var i = 0; i < list.length; i++) {
                                var descriptdate = list[i].Description.split("]")[0].slice(1).toString().trim();
                                if (descriptdate.localeCompare(setting) === 0) {
                                    cbExpected.value(list[i].ID);
                                    break;
                                }
                            }
                        }
                    }
                }
            })
                .fail(function (msg) {
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadInfoPageVm.prototype.getItemInformation = function (campusId, fldName, targetDropDown, value, setting) {
            if (setting === void 0) { setting = null; }
            this.db.getLeadItemInformation(fldName, value, campusId, this)
                .done(function (msg) {
                if (msg != undefined) {
                    var itemdd = $("#" + targetDropDown).data("kendoDropDownList");
                    itemdd.setDataSource(msg);
                    if (setting != null) {
                        itemdd.value(setting);
                    }
                }
            })
                .fail(function (msg) {
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadInfoPageVm.prototype.postLeadInformationToServer = function (leadGuid, avoidDuplicate, redirectpage) {
            var _this = this;
            $("body").css("cursor", "progress");
            var lead = new AD.LeadInfoPageOutputModel();
            var newLead = sessionStorage.getItem(AD.XLEAD_NEW);
            if (newLead != null && newLead === "true") {
                lead.LeadId = '00000000-0000-0000-0000-000000000000';
                lead.AvoidDuplicateAnalysis = avoidDuplicate;
                assignedDateTime = new Date();
                adminRepOld = "select";
            }
            else {
                lead.LeadId = leadGuid;
            }
            var userId = XMASTER_PAGE_USER_OPTIONS_USERID;
            lead.InputModel.UserId = userId;
            lead.InputModel.CampusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            lead.InputModel.LeadId = lead.LeadId;
            lead.ModUser = userId;
            lead.FirstName = $("#txtFirstName").val();
            lead.MiddleName = $("#txtMiddleName").val();
            lead.LastName = $("#txtLastName").val();
            if (this.lastNameInitial !== "") {
                if (this.lastNameInitial !== lead.LastName) {
                    var isOfficial = window.confirm("Is this an official name change?");
                    if (isOfficial) {
                        var hist = new AD.LastNameHistory();
                        hist.LastName = this.lastNameInitial;
                        hist.LeadId = lead.LeadId;
                        lead.LeadLastNameHistoryList = [];
                        lead.LeadLastNameHistoryList.push(hist);
                        this.lastNameInitial = lead.LastName;
                    }
                }
            }
            lead.NickName = $("#tbNickname").val();
            lead.SSN = $("#maskedsocialSecurity").data("kendoMaskedTextBox").value();
            lead.DriverLicenseNumber = $("#tdriverlicensenumber").val();
            lead.AlienNumber = $("#tbaliennumber").val();
            lead.Age = $("#textage").val();
            lead.Dependants = $("#itextdependants").data("kendoNumericTextBox").value();
            lead.DistanceToSchool = $("#tpTimeTo").data("kendoNumericTextBox").value();
            lead.Suffix = $("#cbsuffix").data("kendoDropDownList").value();
            lead.Gender = $("#cbgender").data("kendoDropDownList").value();
            lead.Prefix = $("#cbprefix").data("kendoDropDownList").value();
            lead.RaceId = $("#cbRace").data("kendoDropDownList").value();
            lead.Dob = $("#dtDob").data("kendoDatePicker").value();
            lead.Citizenship = $("#cbcitizenship").data("kendoDropDownList").value();
            lead.Dependency = $("#cbdependency").data("kendoDropDownList").value();
            lead.MaritalStatus = $("#cbmaritalstatus").data("kendoDropDownList").value();
            lead.FamilyIncoming = $("#cbfamilyincome").data("kendoDropDownList").value();
            lead.HousingType = $("#cbhousingtype").data("kendoDropDownList").value();
            lead.DrvLicStateCode = $("#cbdriverlicensestate").data("kendoDropDownList").value();
            lead.Transportation = $("#cbtransportation").data("kendoDropDownList").value();
            var veh = new AD.Vehicle();
            veh.Permit = $("#vehicleParkingPermit1").val();
            veh.Make = $("#vehicleMaker1").val();
            veh.Model = $("#vehicleModel1").val();
            veh.Color = $("#vehicleColor1").val();
            veh.Plate = $("#vehiclePlate1").val();
            veh.Position = 1;
            lead.Vehicles.push(veh);
            var veh2 = new AD.Vehicle();
            veh2.Permit = $("#vehicleParkingPermit2").val();
            veh2.Make = $("#vehicleMaker2").val();
            veh2.Model = $("#vehicleModel2").val();
            veh2.Color = $("#vehicleColor2").val();
            veh2.Plate = $("#vehiclePlate2").val();
            veh2.Position = 2;
            lead.Vehicles.push(veh2);
            switch (this.preferredMethod) {
                case "Phone":
                    lead.PreferredContactId = 1;
                    break;
                case "Email":
                    lead.PreferredContactId = 2;
                    break;
                case "Text":
                    lead.PreferredContactId = 3;
                    break;
                default:
                    lead.PreferredContactId = 1;
            }
            lead.LeadAddress.CountyId = $("#cbCounty").data("kendoDropDownList").value();
            lead.LeadAddress.CountryId = $("#cbcountry").data("kendoDropDownList").value();
            lead.LeadAddress.AddressTypeId = $("#cbAddress").data("kendoDropDownList").value();
            lead.LeadAddress.City = $("#txtCity").val();
            lead.LeadAddress.IsInternational = ($("#checkIntAddress").prop("checked"));
            lead.LeadAddress.StateInternational = $("#otherStates").val();
            lead.LeadAddress.StateId = $("#cbState").data("kendoDropDownList").value();
            lead.LeadAddress.ZipCode = $("#txtZipCode").data("kendoMaskedTextBox").value();
            lead.LeadAddress.Address1 = $("#txtAddress1").val();
            lead.LeadAddress.AddressApto = $("#txtApart").val();
            lead.LeadAddress.Address2 = $("#txtAddress2").val();
            lead.LeadAddress.IsShowOnLeadPage = 'yes';
            lead.LeadAddress.IsMailingAddress = 'yes';
            lead.PhonesList = [];
            var phone = new AD.Phone();
            phone.Extension = $("#tbphoneext").val();
            phone.ID = $("#hiddenPhoneId").val();
            phone.IsForeignPhone = ($("#checkphone").prop("checked"));
            phone.Phone = $("#txtPhone").data("kendoMaskedTextBox").raw();
            phone.PhoneTypeId = $("#cbphone").data("kendoDropDownList").value();
            phone.Position = 1;
            lead.PhonesList.push(phone);
            var phone1 = new AD.Phone();
            phone1.Extension = $("#tbphone1ext").val();
            phone1.ID = $("#hiddenPhone1Id").val();
            phone1.IsForeignPhone = ($("#checkphone1").prop("checked"));
            phone1.Phone = $("#txtPhone1").data("kendoMaskedTextBox").raw();
            phone1.PhoneTypeId = $("#cbphone1").data("kendoDropDownList").value();
            phone1.Position = 2;
            lead.PhonesList.push(phone1);
            var phone2 = new AD.Phone();
            phone2.Extension = $("#tbphone2ext").val();
            phone2.ID = $("#hiddenPhone2Id").val();
            phone2.IsForeignPhone = ($("#checkphone2").prop("checked"));
            phone2.Phone = $("#txtPhone2").data("kendoMaskedTextBox").raw();
            phone2.PhoneTypeId = $("#cbphone2").data("kendoDropDownList").value();
            phone2.Position = 3;
            lead.PhonesList.push(phone2);
            lead.NoneEmail = ($("#checkNoEmail").prop("checked"));
            lead.EmailList = [];
            var email = new AD.Email();
            email.Email = $("#txtemailprimary").val();
            email.EmailType = $("#cbemailprimary").data("kendoDropDownList").value();
            email.ID = $("#hiddenEmailPrimaryId").val();
            email.IsPreferred = 1;
            email.IsShowOnLeadPage = "Yes";
            lead.EmailList.push(email);
            var email1 = new AD.Email();
            email1.Email = $("#txtemail").val();
            email1.ID = $("#hiddenEmailId").val();
            email1.EmailType = $("#cbemail").data("kendoDropDownList").value();
            email1.IsPreferred = 0;
            email1.IsShowOnLeadPage = "Yes";
            lead.EmailList.push(email1);
            var besttime = $("#tpBestTime").data("kendoTimePicker");
            var datetime = besttime.value();
            if (datetime !== null && datetime !== undefined) {
                var output = datetime.getHours() + ":" + datetime.getMinutes();
                lead.BestTime = output;
            }
            lead.SourceCategoryId = $("#cbcategorySource").data("kendoDropDownList").value();
            lead.SourceTypeId = $("#cbtypeSource").data("kendoDropDownList").value();
            lead.AdvertisementId = $("#cbAdvertisementSource").data("kendoDropDownList").value();
            lead.SourceDateTime = $("#dtDateSource").data("kendoDateTimePicker").value();
            lead.Note = $("#txtNoteSource").val();
            lead.DateApplied = $("#dtotherDateApplied").data("kendoDatePicker").value();
            var assignedDatetimeChange = $("#dtotherAdmRepAssignDate").data("kendoDatePicker").value();
            var temDateDb = new Date(assignedDateTime.toString());
            var tempChange = assignedDatetimeChange.getMonth() + "/" + assignedDatetimeChange.getDate() + "/" + assignedDatetimeChange.getFullYear();
            var tempDate = temDateDb.getMonth() + "/" + temDateDb.getDate() + "/" + temDateDb.getFullYear();
            if (tempChange === tempDate) {
                if (adminRepOld === $("#cbotherAdmRep").data("kendoDropDownList").value()) {
                    lead.AssignedDate = assignedDateTime;
                }
                else {
                    var dtPass = new Date(assignedDateTime.toString());
                    var finDate = this.getFinalDate(dtPass);
                    lead.AssignedDate = new Date(finDate);
                }
            }
            else {
                var finDate = this.getFinalDate(assignedDatetimeChange);
                lead.AssignedDate = new Date(finDate);
            }
            lead.AdmissionRepId = $("#cbotherAdmRep").data("kendoDropDownList").value();
            lead.PreviousEducationId = $("#cbotherPreviousEducation").data("kendoDropDownList").value();
            lead.HighSchoolGradDate = $("#dtHsGradDate").data("kendoDatePicker").value();
            lead.HighSchoolId = $("#cbOtherHighSchool").data("kendoDropDownList").value();
            lead.Comments = $("#tbOtherComment").val();
            lead.AttendingHs = +$("#ddAttendingHs").data("kendoDropDownList").value();
            lead.AgencySponsorId = $("#cbotherSponsor").data("kendoDropDownList").value();
            lead.AdminCriteriaId = $("#cbotherAdmCriteria").data("kendoDropDownList").value();
            lead.CampusId = $("#cbLeadAssign").data("kendoDropDownList").value();
            lead.AreaId = $("#cbInterestArea").data("kendoDropDownList").value();
            lead.ProgramId = $("#cbprogramLabel").data("kendoDropDownList").value();
            lead.PrgVerId = $("#cbprogramVersion").data("kendoDropDownList").value();
            lead.ScheduleId = $("#cbschedule").data("kendoDropDownList").value();
            lead.ExpectedStart = this.processExpectedStart();
            lead.AttendTypeId = $("#cbattend").data("kendoDropDownList").value();
            lead.LeadStatusId = $("#cbLeadStatus").data("kendoDropDownList").value();
            var $inputs = $("#custom :input");
            lead.SdfList = [];
            $inputs.each(function () {
                var sdf = new AD.Sdf();
                sdf.SdfId = $(this).prop("id");
                sdf.SdfValue = $(this).val();
                lead.SdfList.push(sdf);
            });
            lead.GroupIdList = new Array();
            if (this.gruppenDb != null) {
                var filteredGroup = new Array();
                for (var j = 0; j < this.gruppenDb.length; j++) {
                    if (this.gruppenDb[j].CampusId === XMASTER_GET_CURRENT_CAMPUS_ID) {
                        filteredGroup.push(this.gruppenDb[j]);
                    }
                }
                for (var l = 0; l < filteredGroup.length; l++) {
                    if ($("#" + filteredGroup[l].GroupId).prop("checked") === true) {
                        lead.GroupIdList.push(filteredGroup[l].GroupId);
                    }
                }
            }
            this.db.postLeadInformation(lead, this)
                .done(function (msg) {
                if (msg != undefined) {
                    $("body").css("cursor", "default");
                    if (lead.LeadId === '00000000-0000-0000-0000-000000000000') {
                        if (msg.ValidationErrorMessages != null && msg.ValidationErrorMessages !== "") {
                            alert(msg.ValidationErrorMessages);
                            return;
                        }
                        if (msg.DuplicatedMessages != null && msg.ValidationErrorMessages !== "") {
                            var dupgrid = $("#infoPageDupGrid").data("kendoGrid");
                            var dsk = new kendo.data.DataSource({
                                data: msg.PossiblesDuplicatesList
                            });
                            dupgrid.setDataSource(dsk);
                            var dupWin = $("#WindowsDuplicate").data("kendoWindow");
                            dupWin.center().open();
                            return;
                        }
                        alert("Lead saved successful");
                        sessionStorage.setItem("NewLead", "false");
                        var url = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&NewEnt=1&cmpId=" + lead.CampusId;
                        window.location.assign(url);
                        return;
                    }
                    alert("Lead saved successful");
                    if (redirectpage !== null && redirectpage !== undefined && redirectpage !== "") {
                        sessionStorage.setItem(AD.XLEAD_NEW, "true");
                        window.location.assign(redirectpage);
                    }
                    if (msg.ValidStatusChangeList != null) {
                        sessionStorage.setItem("allowedLeadStatus", msg.ValidStatusChangeList);
                        var dllnew = _this.selectLeadAllowedStates(msg.StateChangeIdsList, lead.LeadStatusId);
                        var statusControl = $("#cbLeadStatus").data("kendoDropDownList");
                        statusControl.setDataSource(dllnew);
                        statusControl.value(msg.LeadStatusId);
                    }
                    if (lead.CampusId !== XMASTER_GET_CURRENT_CAMPUS_ID) {
                        var url1 = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&NewEnt=1&cmpId=" + lead.CampusId;
                        window.location.assign(url1);
                    }
                    var fullname = lead.FirstName;
                    fullname += (lead.NickName == null || lead.NickName === "") ? " " : " (" + lead.NickName + ") ";
                    fullname += lead.LastName;
                    var expectedStart;
                    if (lead.ExpectedStart == null) {
                        expectedStart = "";
                    }
                    else {
                        expectedStart = lead.ExpectedStart.toString();
                    }
                    AD.LeadInfoBarVm.refreshLeadFields(fullname, $("#cbprogramVersion").data("kendoDropDownList").text(), expectedStart, $("#cbotherAdmRep").data("kendoDropDownList").text(), lead.AssignedDate.toString(), $("#cbLeadStatus").data("kendoDropDownList").text());
                }
            })
                .fail((function (msg) {
                $("body").css("cursor", "default");
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            }));
        };
        LeadInfoPageVm.prototype.getFinalDate = function (dt) {
            var sysDate = new Date();
            var monthC = (dt.getMonth() + 1).toString();
            var dateC = dt.getDate().toString();
            if (monthC.length === 1) {
                monthC = "0" + monthC;
            }
            if (dateC.length === 1) {
                dateC = "0" + dateC;
            }
            var finalDate = dt.getFullYear() + "-" + monthC + "-" + dateC + "T" + sysDate.getHours() + ":" + sysDate.getMinutes() + ":" + sysDate.getSeconds() + "." + sysDate.getMilliseconds();
            return finalDate;
        };
        LeadInfoPageVm.prototype.deleteLead = function (leadGuid) {
            var _this = this;
            var firstName = $("#txtFirstName").val();
            var lastname = $("#txtLastName").val();
            var r = confirm(AD.X_DELETE_ARE_YOU_SURE + firstName + " " + lastname);
            if (r) {
                var db = new AD.LeadInfoPageDb();
                db.getIsLeadErasable(leadGuid, this)
                    .done(function (msg) {
                    if (msg === "false") {
                        alert(AD.X_DELETE_NOT_POSSIBLE);
                    }
                    else {
                        db.postLeadDelete(leadGuid, _this)
                            .done(function () {
                            alert(AD.X_DELETE_SUCESS);
                            sessionStorage.setItem(AD.XLEAD_NEW, "true");
                            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&NewEnt=1&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageLead&mod=AD");
                        })
                            .fail(function (msg1) {
                            ad.SHOW_DATA_SOURCE_ERROR(msg1);
                        });
                    }
                })
                    .fail(function (msg) {
                    ad.SHOW_DATA_SOURCE_ERROR(msg);
                });
            }
        };
        LeadInfoPageVm.prototype.refreshCampusInControl = function (campusId) {
            var _this = this;
            var lStatus = $("#cbLeadStatus").data("kendoDropDownList").value();
            var userId = $("#hdnUserId").val();
            this.db.getAllowedStatus(campusId, lStatus, userId, this)
                .done(function (msg) {
                sessionStorage.setItem("allowedLeadStatus", msg.StateChangeIdsList);
                _this.initializeControls(campusId, lStatus);
                var cbprogramVersion = $("#cbprogramVersion").data("kendoDropDownList");
                var cbschedule = $("#cbschedule").data("kendoDropDownList");
                var cbexpectedStart = $("#cbExpectedStart").data("kendoDropDownList");
                var cbprogramLabel = $("#cbprogramLabel").data("kendoDropDownList");
                cbprogramLabel.setDataSource(new kendo.data.DataSource());
                cbschedule.setDataSource(new kendo.data.DataSource());
                cbprogramVersion.setDataSource(new kendo.data.DataSource());
                cbexpectedStart.setDataSource(new kendo.data.DataSource());
            })
                .fail(function (msg) {
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadInfoPageVm.prototype.processExpectedStart = function () {
            var expectedStart;
            if (XMASTER_LEAD_IS_CLOCK_HOUR === 'Yes') {
                expectedStart = $("#dpExpectedStart").data("kendoDatePicker").value();
            }
            else {
                var description = $("#cbExpectedStart").data("kendoDropDownList").text();
                if (description == null || description === "" || description === AD.X_SELECT_ITEM) {
                    expectedStart = null;
                }
                else {
                    var descriptdate = description.split("]")[0].slice(1).toString().trim();
                    expectedStart = new Date(descriptdate);
                }
            }
            return expectedStart;
        };
        LeadInfoPageVm.prototype.fillPrintInfo = function (p) {
            p.preferredContact = this.preferredMethod;
            var bestTime = $("#tpBestTime").data("kendoTimePicker").value();
            p.bestTime = (bestTime !== null && bestTime !== undefined) ? ad.FORMAT_AMPM(bestTime) : "";
            p.address = $("#txtAddress1").val() + " " + $("#txtApart").val();
            p.phoneBest = $("#txtPhone").data("kendoMaskedTextBox").value();
            p.phone1 = $("#txtPhone1").data("kendoMaskedTextBox").value();
            p.phone2 = $("#txtPhone2").data("kendoMaskedTextBox").value();
            p.emailBest = $("#txtemailprimary").val();
            p.email = $("#txtemail").val();
            p.county = $("#cbCounty").data("kendoDropDownList").text();
            p.country = $("#cbcountry").data("kendoDropDownList").text();
            var state = "";
            if (($("#checkIntAddress").prop("checked"))) {
                state = $("#txtCity").val();
            }
            else {
                state = $("#cbState").data("kendoDropDownList").text();
                state = (state === "Select") ? "" : state;
            }
            p.cityStateZip = $("#txtCity").val() + ", " + state + ", " + $("#txtZipCode").val();
            p.area = $("#cbInterestArea").data("kendoDropDownList").text();
            p.program = $("#cbprogramLabel").data("kendoDropDownList").text();
            p.attend = $("#cbattend").data("kendoDropDownList").text();
            p.schedule = $("#cbschedule").data("kendoDropDownList").text();
            var expectedGrad = $("#dtGrad").data("kendoDatePicker").value();
            p.expectedGrad = (expectedGrad !== null && expectedGrad !== undefined)
                ? ad.FORMAT_DATE_TO_MM_DD_YYYY(expectedGrad)
                : "";
            p.campusName = $("#cbLeadAssign").data("kendoDropDownList").text();
            p.programVersion = $("#cbprogramVersion").data("kendoDropDownList").text();
            p.status = $("#cbLeadStatus").data("kendoDropDownList").text();
            var expectedStart = this.processExpectedStart();
            p.expectedStart = (expectedStart !== null && expectedStart !== undefined)
                ? ad.FORMAT_DATE_TO_MM_DD_YYYY(expectedStart)
                : "";
            p.photopath = $("#infobarImage").attr("src");
            var prefix = $("#cbprefix").data("kendoDropDownList").text();
            p.leadFullName = prefix === "" || prefix === "Select" ? "" : prefix + " ";
            p.leadFullName += $("#txtFirstName").val();
            p.leadFullName += $("#txtMiddleName").val() === "" ? "" : " " + $("#txtMiddleName").val();
            p.leadFullName += " " + $("#txtLastName").val();
            var suffix = $("#cbsuffix").data("kendoDropDownList").text();
            p.leadFullName += suffix === "" || suffix === "Select" ? "" : " " + suffix;
            var nickname = $("#tbNickname").val();
            p.leadFullName += nickname !== "" ? " (" + nickname + ")" : "";
            p.ssn = $("#maskedsocialSecurity").data("kendoMaskedTextBox").raw().replace(/^\d{5}/, "*****");
            p.dobAge = "";
            var dob = $("#dtDob").data("kendoDatePicker").value();
            var age = $("#textage").val();
            if (dob !== null || age !== "") {
                if (dob == null) {
                    p.dobAge = " ? - " + $("#textage").val();
                }
                else {
                    p.dobAge = ad.FORMAT_DATE_TO_MM_DD_YYYY(dob) + " - " + $("#textage").val();
                }
            }
            p.gender = $("#cbgender").data("kendoDropDownList").text();
            p.race = $("#cbRace").data("kendoDropDownList").text();
            p.citizenship = $("#cbcitizenship").data("kendoDropDownList").text();
            p.dependency = $("#cbdependency").data("kendoDropDownList").text();
            var dependents = $("#itextdependants").data("kendoNumericTextBox").value();
            p.dependents = dependents !== null && dependents !== undefined ? dependents.toString() : "";
            p.alienNumber = $("#tbaliennumber").val();
            p.maritalStatus = $("#cbmaritalstatus").data("kendoDropDownList").text();
            p.familyIncoming = $("#cbfamilyincome").data("kendoDropDownList").text();
            p.housing = $("#cbhousingtype").data("kendoDropDownList").text();
            p.driverLicState = $("#cbdriverlicensestate").data("kendoDropDownList").text();
            p.driverLicNumber = $("#tdriverlicensenumber").val();
            p.transportation = $("#cbtransportation").data("kendoDropDownList").text();
            var distanceToSchool = $("#tpTimeTo").data("kendoNumericTextBox").value();
            p.distanceToSchool = distanceToSchool !== null && distanceToSchool !== undefined ? distanceToSchool.toString() + " Miles" : "";
            p.category = $("#cbcategorySource").data("kendoDropDownList").text();
            p.advertisement = $("#cbAdvertisementSource").data("kendoDropDownList").text();
            p.typeAdvertisement = $("#cbtypeSource").data("kendoDropDownList").text();
            p.note = $("#txtNoteSource").val();
            var createdate = $("#dtDateSource").data("kendoDateTimePicker").value();
            p.createdDate = (createdate !== null && createdate !== undefined)
                ? " " + ad.FORMAT_DATE_TO_MM_DD_YYYY(createdate) + " " + ad.FORMAT_AMPM(createdate) : "";
            var dateApplied = $("#dtotherDateApplied").data("kendoDatePicker").value();
            p.dateApplied = (dateApplied !== null && dateApplied !== undefined) ? ad.FORMAT_DATE_TO_MM_DD_YYYY(dateApplied) : "";
            p.previousEducation = $("#cbotherPreviousEducation").data("kendoDropDownList").text();
            p.sponsor = $("#cbotherSponsor").data("kendoDropDownList").text();
            var dateAssigned = $("#dtotherAdmRepAssignDate").data("kendoDatePicker").value();
            p.dateAssigned = (dateAssigned !== null && dateAssigned !== undefined) ? ad.FORMAT_DATE_TO_MM_DD_YYYY(dateAssigned) : "";
            p.admissionRep = $("#cbotherAdmRep").data("kendoDropDownList").text();
            p.highSchool = $("#cbOtherHighSchool").data("kendoDropDownList").text();
            p.adminCriteria = $("#cbotherAdmCriteria").data("kendoDropDownList").text();
            var hsGradeDate = $("#dtHsGradDate").data("kendoDatePicker").value();
            p.hsGradeDate = (hsGradeDate !== null && hsGradeDate !== undefined) ? ad.FORMAT_DATE_TO_MM_DD_YYYY(hsGradeDate) : "";
            p.attendingHs = $("#ddAttendingHs").data("kendoDropDownList").text();
            p.reasonNotEnrolled = "";
            var comment = $("#tbOtherComment").val();
            p.comments = comment.replace(/\n/g, " ");
            p.leadGroups = " ";
            var filteredGroup = new Array();
            for (var j = 0; j < this.gruppenDb.length; j++) {
                if (this.gruppenDb[j].CampusId === XMASTER_GET_CURRENT_CAMPUS_ID) {
                    filteredGroup.push(this.gruppenDb[j]);
                }
            }
            for (var l = 0; l < filteredGroup.length; l++) {
                if ($("#" + filteredGroup[l].GroupId).prop("checked") === true) {
                    if (p.leadGroups.length > 3) {
                        p.leadGroups += ", ";
                    }
                    p.leadGroups += filteredGroup[l].Description;
                }
            }
            var $inputs = $("#custom :input");
            p.userDf = [];
            $inputs.each(function () {
                var sdf = new AD.Userdf();
                sdf.label = $(this).prop("name");
                sdf.value = $(this).val();
                p.userDf.push(sdf);
            });
            return p;
        };
        LeadInfoPageVm.prototype.executeCustomValidators = function () {
            var error = "";
            var ssn = $("#maskedsocialSecurity").data("kendoMaskedTextBox");
            if (ssn.value() !== "") {
                if (ssn.value().indexOf(ssn.options.promptChar) !== -1) {
                    error = "SNN Number is incomplete! \n";
                }
            }
            var dob = $("#dtDob").val();
            if (dob !== null && dob !== undefined && dob !== "") {
                if (kendo.parseDate(dob) == null) {
                    error += "Invalid DOB!\n";
                }
            }
            if (error.length > 1) {
                error = "The following Fields have errors:\n" + error;
                alert(error);
                return false;
            }
            return true;
        };
        LeadInfoPageVm.prototype.storeVehicleInformation = function () {
            this.storeVehicles = [];
            this.storeVehicles.push($("#vehicleParkingPermit1").val());
            this.storeVehicles.push($("#vehicleMaker1").val());
            this.storeVehicles.push($("#vehicleModel1").val());
            this.storeVehicles.push($("#vehicleColor1").val());
            this.storeVehicles.push($("#vehiclePlate1").val());
            this.storeVehicles.push($("#vehicleParkingPermit2").val());
            this.storeVehicles.push($("#vehicleMaker2").val());
            this.storeVehicles.push($("#vehicleModel2").val());
            this.storeVehicles.push($("#vehicleColor2").val());
            this.storeVehicles.push($("#vehiclePlate2").val());
        };
        LeadInfoPageVm.prototype.cancelVehicleEdition = function () {
            $("#vehiclePlate2").val(this.storeVehicles.pop());
            $("#vehicleColor2").val(this.storeVehicles.pop());
            $("#vehicleModel2").val(this.storeVehicles.pop());
            $("#vehicleMaker2").val(this.storeVehicles.pop());
            $("#vehicleParkingPermit2").val(this.storeVehicles.pop());
            $("#vehiclePlate1").val(this.storeVehicles.pop());
            $("#vehicleColor1").val(this.storeVehicles.pop());
            $("#vehicleModel1").val(this.storeVehicles.pop());
            $("#vehicleMaker1").val(this.storeVehicles.pop());
            $("#vehicleParkingPermit1").val(this.storeVehicles.pop());
        };
        return LeadInfoPageVm;
    }(kendo.Observable));
    AD.LeadInfoPageVm = LeadInfoPageVm;
})(AD || (AD = {}));
//# sourceMappingURL=LeadInfoPageVm.js.map