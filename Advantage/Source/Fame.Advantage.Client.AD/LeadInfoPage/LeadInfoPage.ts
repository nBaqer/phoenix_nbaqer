﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />


module AD {
    // This is created in Lead Master Page.
    declare var XMASTER_LEAD_IS_CLOCK_HOUR: string;
    declare var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL: string;
    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;


    export class LeadInfoPage {

        public viewModel: LeadInfoPageVm;
        public vehicleValidator: kendo.ui.Validator;
        public leadValidator: kendo.ui.Validator;
        public xmasterLeadNotificationCenter: kendo.ui.Notification;

        constructor() {
            try {
                this.viewModel = new LeadInfoPageVm();
                this.viewModel.showLoadingLeadPage(true);
                //this.xmasterLeadNotificationCenter = MasterPage.CREATE_NOTIFICATION_CENTER("leadPopupNotification", "leadNotificationCenterWrapper",50);
                //MasterPage.SHOW_NOTIFICATION_INFO(this.xmasterLeadNotificationCenter, "Notification info test");
                //MasterPage.SHOW_NOTIFICATION_ERROR(this.xmasterLeadNotificationCenter,"error Test rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr rrrrrrrrrrrrrrrrrrrrrrrrrrr rrrrrrrrrrrrrrrrrrr rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
                var ethis = this;
                var isLead: boolean = true;
                var leadId: string;

                // Prevent is new if the click comes from the Menu
                var isnew: string = MasterPage.GET_QUERY_STRING_PARAMETER_BY_NAME("new");
                if (isnew === "0") {
                    // clean is new. that is because comes from the main menu. never should be new
                    sessionStorage.setItem(AD.XLEAD_NEW, "false");
                }
                // Create the page validator
                this.leadValidator = MasterPage.ADVANTAGE_VALIDATOR("containerInfoPage");

                // var test:number = 1;
                // Get if it is a new page or use a specific lead
                var newLead = sessionStorage.getItem(AD.XLEAD_NEW);
                if (newLead != null && newLead === "true") {
                    // If it is present the TAG XLEAD_NEW in the query string
                    // We have a request of new lead setgocontactInformation flag isLead to false
                    isLead = false;
                    this.viewModel.showLoadingLeadPage(false);
                } else {
                    // If it is a normal page of lead, Analise the special case
                    // we have a campus without leads
                    let leadSelected: any = document.getElementById('leadId');
                    leadId = leadSelected.value;
                    if ((leadId == undefined || leadId == null || leadId === "")) {
                        // We are in a Campus without Leads.... Open as NEW
                        sessionStorage.setItem(XLEAD_NEW, "true");
                        window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                            "AD/AleadInfoPage.aspx?resid=170&cmpid=" +
                            XMASTER_GET_CURRENT_CAMPUS_ID +
                            "&desc=InfoPageQuick&mod=AD");
                        return;
                    } else {
                        this.viewModel.showLoadingLeadPage(false);
                    }
                }
                $(document).keypress(function (event) {

                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if (keycode === 13) {
                        var btnId = $(document.activeElement).attr("id");
                        if (btnId === "goMoreTransportation" || btnId === "gocontactInformation" || btnId === "insertLead" || btnId === "cancelLead" || btnId === "vehicleSave" || btnId === "vehicleCancel") {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }

                });
                // Duplicate windows
                $("#WindowsDuplicate").kendoWindow(
                    {
                        actions: ["Close"],
                        title: "Duplicates Window",
                        width: "900px",
                        height: "650px",
                        visible: false,
                        modal: false,
                        resizable: true
                    });

                // Duplicate Windows : Grid for duplicates
                $("#infoPageDupGrid").kendoGrid({
                    toolbar: ["excel", "pdf"],
                    excel: {
                        allPages: true,
                        fileName: "DuplicateLeads.xlsx"
                    },
                    pdf: {
                        fileName: "DuplicateLeads.pdf",
                        landscape: true
                    },
                    dataSource: [{}],
                    //height: 600,
                    sortable: true,
                    filterable: false,
                    resizable: true,

                    columns: [

                        { field: "Id", title: "", width: 1 },
                        { field: "FirstName", title: "First Name", attributes: { style: "font-size:11px;" } },
                        { field: "LastName", title: "Last Name", attributes: { style: "font-size:11px;" } },
                        { field: "ssn", title: "SSN", attributes: { style: "font-size:11px;" } },
                        { field: "Dob", title: "DOB", attributes: { style: "font-size:11px;" } },
                        { field: "Address1", title: "Address", attributes: { style: "font-size:11px;" } },
                        { field: "Phone", title: "Phones" },
                        { field: "Email", title: "Email" },
                        { field: "Campus", title: "Campus", attributes: { style: "font-size:11px;" } },
                        { field: "Status", title: "Status", attributes: { style: "font-size:11px;" } },
                        //{ field: "LeadAssignedTo", title: "Lead Assigned To", editor: function (c, o) { fthis.editordd(c, o); }, template: "#=LeadAssignedTo#" },
                        { field: "AdmissionRep", title: "Admission Rep", attributes: { style: "font-size:11px;" } }
                    ]

                });

                // Buttons events for Duplicate windows
                $('#cancelLead').click(() => {
                    var dupWin = $("#WindowsDuplicate").data("kendoWindow") as any;
                    dupWin.close();
                });

                //#region Windows Vehicles
                // Create the validation for vehicles
                this.vehicleValidator = MasterPage.ADVANTAGE_VALIDATOR("windowsVehicle");

                // Vehicles windows
                $("#windowsVehicle").kendoWindow(
                    {
                        actions: ["Close"],
                        title: "Vehicle Information",
                        width: "520px",
                        height: "290px",
                        visible: false,
                        modal: true,
                        resizable: false,
                        close: (e) => {
                            if (e.userTriggered) {
                                this.viewModel.cancelVehicleEdition();
                                MasterPage.ADVANTAGE_VALIDATOR_HIDDEN_VALIDATION("windowsVehicle");
                            }
                        }
                    });

                // Button event to open the vehicle windows
                $('#goMoreTransportation').click(() => {
                    var vWin = $("#windowsVehicle").data("kendoWindow") as any;
                    this.viewModel.storeVehicleInformation();
                    vWin.center().open();
                });

                // Button events for Vehicle Windows
                $('#vehicleCancel').click(() => {
                    this.viewModel.cancelVehicleEdition();
                    MasterPage.ADVANTAGE_VALIDATOR_HIDDEN_VALIDATION("windowsVehicle");
                    var win = $("#windowsVehicle").data("kendoWindow") as kendo.ui.Window;
                    win.close();
                });


                //var context = this;
                // CLOSE Now - Button events for Vehicle Windows
                $('#vehicleSave').click((e) => {
                    e.preventDefault();
                    // Check if the fields are valid
                    if (this.vehicleValidator.validate()) {
                        var win = $("#windowsVehicle").data("kendoWindow") as any;
                        win.close();
                    }
                });

                //#endregion

                //Create the kendo control layout
                this.initializeKendoComponents();

                // Get all the information to create the page................
                this.viewModel.getResourcesForPage(isLead);

                //#region Handler for Click in tool bar
                $('#insertLead').click(() => {
                    var dupWin = $("#WindowsDuplicate").data("kendoWindow") as any;
                    dupWin.close();
                    //var customValidation: boolean = this.viewModel.executeCustomValidators();
                    //if (customValidation === false) {
                    //    return;
                    //}
                    // var validator = $("#containerInfoPage").kendoValidator().data("kendoValidator") as any;
                    if (this.leadValidator.validate()) {
                        // If the form is valid, the Valida-tor will return true
                        this.viewModel.postLeadInformation(leadId, true);
                    } else {
                        MasterPage.SHOW_WARNING_WINDOW(X_SAVE_REVIEW_INFO_IN_FORM);
                    }
                });

                /* Hooker for Print Button */
                $("#leadPrintButton").click(() => {
                    //Get the user id and store it in the session
                    var userId: string = $("#hdnUserId").val();
                    var printInfo: IPrintOutputModel = new PrintOutputModel();
                    printInfo = this.viewModel.fillPrintInfo(printInfo);
                    sessionStorage.removeItem("PRINT_INFO");
                    sessionStorage.setItem("PRINT_INFO", JSON.stringify(printInfo));
                    sessionStorage.setItem("USER_ID", userId);
                    var url: string = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL
                        + "AD/ALeadPrint.aspx?resid=206&cmpid="
                        + XMASTER_GET_CURRENT_CAMPUS_ID
                        + "&desc=PrintProfile&mod=AD"
                        + "&leadId=" + leadId;
                    window.open(url, "_newtab");
                });

                $('#btn13,#btn14,#btn04,#btn11,#btn12,#btn15,#btn16,#btn17,#btn18').click((e:any) => {
                    var item = e.target.id;
                    let redirectPage = "";
                    switch (item) {
                        case "btn13":
                            redirectPage = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadHistory.aspx?resid=838&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=History&mod=AD";
                            break;
                        case "btn04":
                            sessionStorage.setItem(XLEAD_QUICK_NEW, "true");
                            redirectPage = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +"AD/AleadQuick.aspx?resid=268&cmpid=" +XMASTER_GET_CURRENT_CAMPUS_ID +"&desc=Quick&mod=AD";
                            break;
                        case "btn11":
                            redirectPage = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadNotes.aspx?resid=456&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Notes&mod=AD";
                            break;
                        case "btn12":
                            redirectPage = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/leadRequirements.aspx?resid=826&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Requirements&mod=AD";
                            break;
                        case "btn14":
                            redirectPage = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadEducation.aspx?resid=145&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Education&mod=AD";
                            break;
                        case "btn15":
                            redirectPage = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadPriorWork.aspx?resid=146&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=PriorWorks&mod=AD";
                            break;
                        case "btn16":
                            redirectPage = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadExtracurricular.aspx?resid=148&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Extracurricular&mod=AD";
                            break;
                        case "btn17":
                            redirectPage = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadSkills.aspx?resid=147&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Skills&mod=AD";
                            break;
                        case "btn18":
                            redirectPage = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadEnrollment.aspx?resid=174&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Enrollment&mod=AD";
                            break;
                    default:
                            redirectPage = "";
                            break;
                }
                    (item)
                    
                    if ($("#leadToolBarSave").hasClass("k-state-disabled")) {
                        window.location.assign(redirectPage);
                    } else {
                        $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(X_LEAD_SAVE_CHANGES))
                            .then(confirmed => {
                                if (confirmed) {
                                    if (this.leadValidator.validate()) {
                                        this.viewModel.postLeadInformation(leadId, false,redirectPage);
                                        return;
                                    } else {

                                        MasterPage.SHOW_WARNING_WINDOW(X_SAVE_PLEASE_FILL_REQUIRED_FIELDS);
                                        return;
                                    }
                                } else {
                                    window.location.assign(redirectPage);
                                }
                            });
                    }
                });
                /*
                 * Hook Handler for New button in tool bar.....................................................................
                 */
                $('#leadToolBarNew').click(() => {
                    let redirectPage = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?resid=170&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&new=true&mod=AD";
                    sessionStorage.setItem(XLEAD_NEW, "false");
                    if (sessionStorage.getItem("IsEnrolledLead") === "true") {
                        sessionStorage.setItem(XLEAD_NEW, "true");
                        window.location.assign(redirectPage);
                    } else {
                        $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(X_LEAD_SAVE_CHANGES))
                            .then(confirmed => {
                                if (confirmed) {
                                    if (this.leadValidator.validate()) {
                                        this.viewModel.postLeadInformation(leadId, false, redirectPage);
                                        return;
                                    } else {

                                        MasterPage.SHOW_WARNING_WINDOW(X_SAVE_PLEASE_FILL_REQUIRED_FIELDS);
                                        return;
                                    }
                                }

                                sessionStorage.setItem(XLEAD_NEW, "true");
                                window.location.assign(redirectPage);
                            });
                    }
                });

                /*
                 * Hook Handler for Save button in the tool bar
                 */
                $('#leadToolBarSave').click(() => {
                    if ($("#leadToolBarSave").hasClass("k-state-disabled")) {
                        return;
                    }
                    if (this.leadValidator.validate()) {
                        this.viewModel.postLeadInformation(leadId, false);
                    } else {
                        MasterPage.SHOW_WARNING_WINDOW(X_SAVE_PLEASE_FILL_REQUIRED_FIELDS);
                    }
                });

                /*
                 * Hook Handler for DELETE button in the tool bar
                 */
                $('#leadToolBarDelete').click(() => {
                    this.viewModel.deleteLead(leadId);
                });

                $("#preferredEmail")
                    .click(() => {
                        //if ($(this).is(":checked")) {
                        this.viewModel.preferredMethod = "Email";
                        //}

                    });

                $("#preferredPhone")
                    .click(() => {
                        //if ($(this).is(":checked")) {
                        this.viewModel.preferredMethod = "Phone";
                        //}
                    });

                $("#preferredText")
                    .click(() => {
                        //if ($(this).is(":checked")) {
                        this.viewModel.preferredMethod = "Text";
                        //}
                    });

                //#endregion

                //#Region Handle windows resize....

                // Resize the windows to maintain a correct tab / scroll relation
                $(window).on('resize', () => {
                    this.controlRezising(isnew);
                });

                // Initial tab / scroll relation
                this.controlRezising(isnew);

                //#endregion
            }
            catch (e) {
                $("body").css("cursor", "default");
                MasterPage.SHOW_ERROR_WINDOW(e.message + "/n" + e.stack);
                this.viewModel.showLoadingLeadPage(false);
                //If errors clear the cache
                sessionStorage.removeItem(XLEAD_INFO_DLLS_REQUIRED_ITEMS);
                sessionStorage.removeItem(XLEAD_INFO_GROUPS);
                sessionStorage.removeItem(XLEAD_INFO_SDF_FIELDS);
            }
        }

        controlRezising(isnew: string) {
            let pageheight: number;
            let pageTop: number;
            //var win = $(this); this = window
            if (isnew === "true") {
                pageheight = window.innerHeight - 155;
                pageTop = 155;
            } else {
                pageheight = window.innerHeight - 215;
                pageTop = 215;
            }
            if (pageheight < 0) {
                pageheight = 15;
            }

            // Assign to wrapper the height
            $("#wrapperLead").css("max-height", pageheight);
            $("#wrapperLead").css("top", pageTop);
        }


        initializeKendoComponents() {

            //#region Demographic

            $("#cbPrevlast").kendoDropDownList({

                optionLabel: "",
                dataTextField: "LastName",
                dataValueField: "Id",
                change: (e) => {
                    e.preventDefault();
                    e.sender.select(0);
                }
            });

            $("#cbRace").kendoDropDownList({
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#dtDob").kendoDatePicker({
                change: () => {
                    var dob = ($("#dtDob").data("kendoDatePicker") as any).value();
                    var age = $("#textage").val();
                    this.viewModel.calculateAge(dob, age);
                }
            });


            // Masked text box
            $("#maskedsocialSecurity").kendoMaskedTextBox({
                mask: "000-00-0000",
                clearPromptChar: false
            });


            $("#itextdependants").kendoNumericTextBox({
                decimals: 0,
                format: "n0",
                step: 1,
                min: 0,
                max: 99
            });

            $("#tpTimeTo").kendoNumericTextBox({
                decimals: 0,
                format: "n0",
                step: 1,
                min: 0,
                max: 999
            });

            $("#cbprefix").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbsuffix").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbgender").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbmaritalstatus").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbdriverlicensestate").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbcitizenship").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbdependency").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbfamilyincome").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });


            $("#cbhousingtype").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbtransportation").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbState").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });


            //#endregion

            //#region Source

            $("#cbcategorySource").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbtypeSource").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbAdvertisementSource").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });


            $("#dtDateSource").kendoDateTimePicker({
                //value: null
            });

            //#endregion

            //#region Others

            $("#dtotherDateApplied").kendoDatePicker();

            $("#cbotherPreviousEducation").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });


            $("#dtotherAdmRepAssignDate").kendoDatePicker(
                //{
                //format: "MM/dd/yyyy",
                //parseFormats: ["MM/dd/yyyy hh:mm tt"]
                //}
            );

            $("#cbOtherHighSchool").kendoAutoComplete({
                dataTextField: "Name",
                placeholder: "Search high school",
                filter: "contains",
                minLength: 3
            });

            $("#cbotherAdmRep").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#dtHsGradDate").kendoDatePicker();


            $("#cbotherSponsor").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#ddAttendingHs").kendoDropDownList({
                dataSource: [
                    { text: "Select", value: "-1" },
                    { text: "Yes", value: "1" },
                    { text: "No", value: "0" }
                ],
                dataTextField: "text",
                dataValueField: "value"
            });


            $("#cbotherReasonNot").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbotherAdmCriteria").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            //#endregion

            //#region Contact

            $("#tpBestTime").kendoTimePicker({
                interval: 15,
                min: new Date(2000, 0, 1, 6, 0, 0),
                format: "hh:mm tt",
                parseFormats: ["H:mm"]
            });

            $("#tpBestTime").closest("span.k-datepicker").width(300);

            // Masked text box
            $("#txtPhone").kendoMaskedTextBox({
                mask: "(000)-000-0000",
                clearPromptChar: false
            });

            // Masked text box
            $("#txtPhone1").kendoMaskedTextBox({
                mask: "(000)-000-0000",
                clearPromptChar: false
            });

            // Masked text box
            $("#txtPhone2").kendoMaskedTextBox({
                mask: "(000)-000-0000",
                clearPromptChar: false
            });

            // International Check-boxes change events
            // Do not use lambda notation here!! the this internal this is lost.
            $('#checkphone').change(function () {
                // Get check- box checked value
                var maskedtextbox = $("#txtPhone").data("kendoMaskedTextBox") as any;
                var raw = maskedtextbox.raw();
                if ($(this).is(":checked")) {
                    maskedtextbox.setOptions({ mask: '+9999999999999999999999999' });
                } else {
                    maskedtextbox.setOptions({ mask: '(000)-000-0000' });
                }
                maskedtextbox.value(raw);
            });

            // Do not use lambda notation here!! the this internal this is lost.
            $('#checkphone1').change(function () {
                // Get check- box checked value
                var maskedtextbox = $("#txtPhone1").data("kendoMaskedTextBox") as any;
                var raw = maskedtextbox.raw();
                if ($(this).is(":checked")) {
                    maskedtextbox.setOptions({ mask: '+9999999999999999999999999' });
                } else {
                    maskedtextbox.setOptions({ mask: '(000)-000-0000' });
                }
                maskedtextbox.value(raw);
            });

            // Do not use lambda notation here!! the this internal this is lost.
            $('#checkphone2').change(function () {
                // Get check- box checked value
                var maskedtextbox = $("#txtPhone2").data("kendoMaskedTextBox") as any;
                var raw = maskedtextbox.raw();
                if ($(this).is(":checked")) {
                    maskedtextbox.setOptions({ mask: '+9999999999999999999999999' });
                } else {
                    maskedtextbox.setOptions({ mask: '(000)-000-0000' });
                }
                maskedtextbox.value(raw);
            });


            $("#cbphone").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbphone1").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbphone2").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbcountry").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbCounty").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbAddress").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            // ZIP CODE Masked text box
            $("#txtZipCode").kendoMaskedTextBox({
                mask: "00000",
                clearPromptChar: false
            });

            // Do not use lambda notation here!! the this internal this is lost.
            $('#checkIntAddress').change(function () {
                var cbState = $("#cbState").data("kendoDropDownList") as any;
                var otherState = $("#otherStates");
                var zipcode = $("#txtZipCode").data("kendoMaskedTextBox") as any;
                zipcode.value("");
                if ($(this).is(":checked")) {
                    $("#checkIntAddress").prop('checked', true);
                    cbState.wrapper.hide();
                    otherState.css("display", "block");
                    zipcode.setOptions({ mask: 'aaaaaaaaaa' });
                } else {
                    $("#checkIntAddress").prop('checked', false);
                    cbState.wrapper.show();
                    otherState.css("display", "none");
                    zipcode.setOptions({ mask: '00000' });
                }
            });

            $("#cbemailprimary").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbemail").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $('#checkNoEmail').change(function () {
                var disabled: boolean = ($(this).is(":checked"));
                $("#txtemailprimary").prop("disabled", disabled);
                $("#txtemail").prop("disabled", disabled);
                if (disabled) {
                    $("#txtemail").css("color", "white");
                    $("#txtemailprimary").css("color", "white");
                } else {
                    $("#txtemail").css("color", "inherit");
                    $("#txtemailprimary").css("color", "inherit");
                }
            });

            //$("#cbBestTime").kendoDropDownList({
            //    dataSource: [],
            //    optionLabel: "Select",
            //    dataTextField: "Description",
            //    dataValueField: "ID"
            //});

            let button = $("#gocontactInformation").kendoButton({
                click: this.viewModel.onClickContactInformation
            }).data("kendoButton") as kendo.ui.Button;

            let isnew: string = MasterPage.GET_QUERY_STRING_PARAMETER_BY_NAME("new");
            button.enable(isnew !== "true");

            //#endregion

            //#region Academic
            // Cascade group ........................................................

            // Show the campus to assign the lead
            var campus = $("#cbLeadAssign").kendoDropDownList({
                dataSource: [],
                dataTextField: "Description",
                dataValueField: "ID",
                change: () => {
                    var campusId = ($("#cbLeadAssign").data("kendoDropDownList") as any).value();
                    var programgrp = $("#cbInterestArea").data("kendoDropDownList") as any;

                    programgrp.setDataSource(new kendo.data.DataSource());
                    this.viewModel.refreshLeadGroups(campusId);
                }
            }).data("kendoDropDownList") as any;

            campus.list.width("auto");
            campus.list.css("white-space", "nowrap");


            // Interest area (program group) Depend of the selected campus....
            // Master of the cascade group.
            $("#cbInterestArea").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID",
                change: () => {

                    // Clean Program, Program Version and Expected Start and schedule
                    var campusId = ($("#cbLeadAssign").data("kendoDropDownList") as any).value();
                    var cbprogramVersion = $("#cbprogramVersion").data("kendoDropDownList") as any;
                    var cbschedule = $("#cbschedule").data("kendoDropDownList") as any;
                    var cbexpectedStart = $("#cbExpectedStart").data("kendoDropDownList") as any;


                    cbschedule.setDataSource(new kendo.data.DataSource());
                    cbprogramVersion.setDataSource(new kendo.data.DataSource());
                    cbexpectedStart.setDataSource(new kendo.data.DataSource());

                    //Reload Program
                    var selectedValue = ($("#cbInterestArea").data("kendoDropDownList") as any).value();
                    if (selectedValue !== "") {
                        this.viewModel.getItemInformation(campusId, "Program", "cbprogramLabel", selectedValue);
                    } else {
                        var cbprogramLabel = $("#cbprogramLabel").data("kendoDropDownList") as any;
                        cbprogramLabel.setDataSource(new kendo.data.DataSource());
                    }
                }
            });

            // Set item list....
            var dia = $("#cbInterestArea").data("kendoDropDownList") as any;
            dia.list.width("auto");
            dia.list.css("white-space", "nowrap");

            // Program Depend of interest Area (program group)
            $("#cbprogramLabel").kendoDropDownList({
                optionLabel: "Select",
                dataTextField: "Description",
                //autoBind: false,
                dataValueField: "ID",
                dataSource: [],
                change: () => {

                    // Clean Program Version and Expected Start and schedule
                    var campusId = ($("#cbLeadAssign").data("kendoDropDownList") as any).value();
                    var cbprogramVersion = $("#cbprogramVersion").data("kendoDropDownList") as any;
                    var cbschedule = $("#cbschedule").data("kendoDropDownList") as any;
                    var cbexpectedStart = $("#cbExpectedStart").data("kendoDropDownList") as any;

                    cbschedule.setDataSource(new kendo.data.DataSource());
                    cbprogramVersion.setDataSource(new kendo.data.DataSource());
                    cbexpectedStart.setDataSource(new kendo.data.DataSource());

                    //Reload Program Version and Expected Start
                    var selectedValue = ($("#cbprogramLabel").data("kendoDropDownList") as any).value();
                    var pddl = $("#cbprogramLabel").data("kendoDropDownList") as any;
                    var dataTable = pddl.dataSource.data();
                    var calendartype = dataTable[pddl.selectedIndex - 1]["CalendarType"];
                    if (selectedValue !== "") {
                        this.viewModel.getItemInformation(campusId, "PrgVerId", "cbprogramVersion", selectedValue);

                        //Reload Expected Start
                        if (calendartype.toLowerCase() === "clock hour") {
                            //display it as date time
                            $("#dpExpectedStart").css("display", "inline");
                            $("#dpExpectedStart").css("width", "auto");
                            $("#dpExpectedStart").kendoDatePicker();
                            cbexpectedStart.wrapper.hide();
                        } else {
                            cbexpectedStart.wrapper.show();
                            this.viewModel.getExpectedLeadItemInformation(campusId, "ExpectedStart", "cbExpectedStart", selectedValue);
                            ($("#dpExpectedStart").data("kendoDatePicker") as kendo.ui.DatePicker).value("");
                            $("#dpExpectedStart").parent().parent().hide();
                        }
                    }
                }
            });

            let dpl = $("#cbprogramLabel").data("kendoDropDownList") as any;
            dpl.list.width("auto");
            dpl.list.css("white-space", "nowrap");

            // Program version ... depend of program
            $("#cbprogramVersion").kendoDropDownList({

                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID",
                dataSource: [],
                change: () => {

                    var campusId = ($("#cbLeadAssign").data("kendoDropDownList") as any).value();
                    //Reload program version
                    var cbprogramVersion = $("#cbprogramVersion").data("kendoDropDownList") as any;
                    var selectedValue = cbprogramVersion.value();
                    if (selectedValue !== "") {
                        this.viewModel.getItemInformation(campusId, "ProgramScheduleId", "cbschedule", selectedValue);
                    }

                }
            });

            var ddl = $("#cbprogramVersion").data("kendoDropDownList") as any;
            ddl.list.width("auto");
            ddl.list.css("white-space", "nowrap");

            // Schedule (arProgSchedule) .. depend of program version
            $("#cbschedule").kendoDropDownList({
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID",
                dataSource: []
            });

            //if (XMASTER_LEAD_IS_CLOCK_HOUR === 'Yes') {
            $("#dpExpectedStart").css("display", "inline");
            $("#dpExpectedStart").kendoDatePicker();
            //} else {

            $("#cbExpectedStart").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            var dea = $("#cbExpectedStart").data("kendoDropDownList") as any;
            dea.list.width("auto");
            dea.list.css("white-space", "nowrap");
            // }

            // end of cascade group ..............................................

            $("#cbattend").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });

            $("#cbLeadStatus").kendoDropDownList({
                dataSource: [],
                dataTextField: "Description",
                dataValueField: "ID"
            });

            //$("#dtGrad").kendoDatePicker();

            //#endregion
        }

        //validateVehicleFields(value: string): boolean {
        //    let result: boolean = MasterPage.VALIDATE_ALPHABETIC_STRING(value);
        //    return result;
        //}
    }
} 