﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../Fame.Advantage.Client.SY/Institution/Models/InstitutionFilterInputModel.ts" />
/// <reference path="../../Fame.Advantage.Client.SY/Institution/Institution.ts" />
/// <reference path="../../fame.advantage.client.masterpage/commonfunctions.ts" />

module AD {
    import InstitutionFilterInputModel = SystemInstitution.IInstitutionFilterInputModel;

    // This must be in the html code of control. ...............................
    declare var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL: string;
    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;
    declare var XMASTER_LEAD_IS_CLOCK_HOUR: string;
    declare var XMASTER_PAGE_USER_OPTIONS_USERID: string;

    //added as a work around to preserve the time of AssignedDate to calculate Age in the Queue Page (US7657)
    declare var assignedDateTime: Date;
    declare var adminRepOld: string;
    export interface ICaptionRequirement {
        fldName: string;
        labelName: string;
        controlName?: string;
        isDll: boolean;
        caption?: string;
        isrequired?: boolean;
        dll?: any;
    }

    export class LeadInfoPageVm extends kendo.Observable {

        public preferredMethod: string;
        lastNameInitial; string;
        db: LeadInfoPageDb;
        captionRequirementdb: ICaptionRequirement[];
        filteredDatadb: ICaptionRequirement[];
        gruppenDb: IGroup[];
        storeVehicles: Array<string>;
        assignedDateTime = new Date();
        adminRepOld = "select";
        highSchool: string;
        leadValidator: kendo.ui.Validator;
        shouldValidate : boolean;
        public isClockHourProgram: boolean;
        constructor() {
            super();
            this.preferredMethod = "Email";
            this.lastNameInitial = "";
            this.db = new LeadInfoPageDb();
            this.filteredDatadb = this.db.staticInformationForCaptionRequirement();
            this.isClockHourProgram = false;
            this.highSchool = undefined;
            this.leadValidator = MasterPage.ADVANTAGE_VALIDATOR("containerInfoPage");
            this.shouldValidate = (MasterPage.GET_QUERY_STRING_PARAMETER_BY_NAME("Validate") == "true");


        }

        showLoadingLeadPage(show) {
            var pageLoading = $(document).find(".page-loading");
            if (pageLoading.length === 0) {
                $(document).find("body").append('<div class="page-loading"></div>');
            }

            if (show) {
                $("#wrapperLead").addClass("hidden");
            } else {
                $("#wrapperLead").removeClass("hidden");
            }

            kendo.ui.progress($(".page-loading"), show);
        }
        /*
         * This get all information relative to this page from database
         * also call the rest of the necessary information when is returned the
         * information of database.
         */
        getResourcesForPage(islead: boolean) {
            $("body").css("cursor", "progress");
            var temp = sessionStorage.getItem(XLEAD_INFO_DLLS_REQUIRED_ITEMS);
            var that = this;
            if (temp == null || temp === "{}") {

                // Load the base information
                this.captionRequirementdb = this.db.staticInformationForCaptionRequirement();
                // Get from Server the information
                this.db.getResourcesForPage(170, XMASTER_GET_CURRENT_CAMPUS_ID,XMASTER_PAGE_USER_OPTIONS_USERID, this)
                    .done(msg => {
                        if (msg != undefined) {
                            // Process the Required information resource and DLL.........................
                            var res1 = msg.RequiredList;
                            if (res1 != null) {
                                //Get the check-box container
                                for (var i = 0; i < res1.length; i++) {
                                    var fldName: string = res1[i].FldName;

                                    for (var x = 0; x < this.captionRequirementdb.length; x++) {

                                        if (this.captionRequirementdb[x].fldName === fldName) {
                                            //this.captionRequirementdb[x].caption = res1[i].Caption;
                                            this.captionRequirementdb[x].isrequired = res1[i].Required;
                                            this.captionRequirementdb[x].dll = res1[i].Dll;
                                        }
                                    }
                                }
                                let cache = JSON.stringify(this.captionRequirementdb);
                                sessionStorage.setItem(XLEAD_INFO_DLLS_REQUIRED_ITEMS, cache);
                            }

                            // Process the SDF Fields if any
                            var res2 = msg.SdfList;
                            if (res2 != null) {
                                // this.processSdfFields(res2);
                                MasterPage.AdvantageUserDefinedFields.processSdfFields(res2, "custom");
                                sessionStorage.setItem(XLEAD_INFO_SDF_FIELDS, JSON.stringify(res2));
                            }

                            // Process the Lead Groups if any
                            var res3 = msg.LeadGroupList;
                            if (res3 != null) {
                                this.processLeadGroups(res3);
                                this.gruppenDb = res3;
                                sessionStorage.setItem(XLEAD_INFO_GROUPS, JSON.stringify(res3));
                            }

                            this.initializePage(islead);
                        }
                    })
                    .always(() => {
                        $("body").css("cursor", "default");
                    })
                    .fail(msg => {

                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    return;
                });
            } else {

                // Process required
                that.captionRequirementdb = Object.create($.parseJSON(temp)) as ICaptionRequirement[];

                // Process SDF
                var sdf: any = Object.create($.parseJSON(sessionStorage.getItem(XLEAD_INFO_SDF_FIELDS)));
                MasterPage.AdvantageUserDefinedFields.processSdfFields(sdf, "custom");
                // Process Lead Groups
                var grps: any = Object.create($.parseJSON(sessionStorage.getItem(XLEAD_INFO_GROUPS)));
                this.gruppenDb = grps;
                this.processLeadGroups(grps);

                //Initialize page
                this.initializePage(islead);
            }

            // Initializations
        }

        processLeadGroups(grps) {
            if (grps != undefined) {

                // Filter groups by CampusID
                var filteredGroup = new Array<Group>();
                for (var j = 0; j < grps.length; j++) {
                    if (grps[j].CampusId === XMASTER_GET_CURRENT_CAMPUS_ID) {
                        filteredGroup.push(grps[j]);
                    }
                }
                filteredGroup.sort((a, b) => {
                    var nA = a.Description.toLocaleLowerCase();
                    var nB = b.Description.toLocaleLowerCase();
                    if (nA < nB) {
                        return -1;
                    } else if (nA > nB) {
                        return 1;
                    }
                    return 0;
                });
                var html: string;
                //Create the check-box containers for the filtered groups.
                $("#leadGroups").html("");
                for (var i = 0; i < filteredGroup.length; i++) {
                    html = `<input type="checkbox" class="k-checkbox" id="${filteredGroup[i].GroupId}" />`;
                    html = html + '<label class="k-checkbox-label f-checkbox-label" style= "padding-left: 18px !important;" for="' + filteredGroup[i].GroupId + '" > ' + filteredGroup[i].Description + ' </label>';
                    html = `<div class='leadGroupCheckboxStyle'>${html}</div>`;
                    $("#leadGroups").append(html);
                }
            }
        }

        processLeadGroupsByCampus(grps, campusId: string) {
            if (grps != undefined) {

                // Filter groups by CampusID
                var filteredGroup = new Array<Group>();
                for (var j = 0; j < grps.length; j++) {
                    if (grps[j].CampusId === campusId) {
                        filteredGroup.push(grps[j]);
                    }
                }
                filteredGroup.sort((a, b) => {
                    var nA = a.Description.toLocaleLowerCase();
                    var nB = b.Description.toLocaleLowerCase();
                    if (nA < nB) {
                        return -1;
                    } else if (nA > nB) {
                        return 1;
                    }
                    return 0;
                });
                var html: string;
                //Create the check-box containers for the filtered groups.
                $("#leadGroups").html("");
                for (var i = 0; i < filteredGroup.length; i++) {
                    html = `<input type="checkbox" class="k-checkbox" id="${filteredGroup[i].GroupId}" />`;
                    html = html + '<label class="k-checkbox-label f-checkbox-label" style= "padding-left: 18px !important;" for="' + filteredGroup[i].GroupId + '" > ' + filteredGroup[i].Description + ' </label>';
                    html = `<div class='leadGroupCheckboxStyle'>${html}</div>`;
                    $("#leadGroups").append(html);
                }
            }
        }

        initializePage(islead: boolean) {
            var campusId: string = MasterPage.GET_QUERY_STRING_PARAMETER_BY_NAME("cmpid");

            this.fillFilteredDatadb(campusId);
            this.initializeControls(XMASTER_GET_CURRENT_CAMPUS_ID);
            this.configureRequired(this.captionRequirementdb);
            if (!islead) {
                //first time new lead 
                assignedDateTime = new Date();
                adminRepOld = "select";
                $("#dpExpectedStart").parent().parent().hide();
                sessionStorage.setItem("IsEnrolledLead", "false");
            }
            if (islead) {
                var leadElement: any = document.getElementById('leadId');
                var leadGuid = leadElement.value;

                // Load the specific lead information
                this.getLeadDemographic(leadGuid);
            }

        }

        /* Create Caption and requirement for all register 
         * fields as resources in the database
         */
        configureRequired(db: ICaptionRequirement[]) {
            var caption: string;
            for (var i = 0; i < db.length; i++) {
                var label = $("#" + db[i].labelName);
                if (label != undefined) {

                    caption = label.text();
                    if (db[i].isrequired) {
                        caption = caption + "<span style=color:red>*</span>";
                        label.html(caption);
                    }
                }
                var control = $("#" + db[i].controlName);
                if (control != undefined) {


                    if (db[i].isrequired) {
                        control.attr("required", "required");
                    }
                }
            }
        }

        /*
         * Service routine set the values in a item DLL
         */
        setDllValue(controlName: string, value: any) {
            for (var i = 0; i < this.captionRequirementdb.length; i++) {

                if (this.captionRequirementdb[i].controlName === controlName) {
                    this.captionRequirementdb[i].dll = value;
                    break;
                }
            }
        }

        /*
         * Get All data of the lead.
         */
        getLeadDemographic(leadGuid) {
            // Request the info from server
            let that = this;
            var userId: string = $("#hdnUserId").val();
            this.showLoadingLeadPage(true);
            this.db.getLeadDemographic(leadGuid, userId, this)
                .done(msg1 => {
                    this.showLoadingLeadPage(false);
                    if (msg1 != undefined) {

                        try {
                            var msg: ILeadInfoPageOutputModel = msg1;
                            if (msg.IsEnrolled !== null) {
                                sessionStorage.setItem("IsEnrolledLead", msg.IsEnrolled.toString().toLowerCase());
                                if (msg.IsEnrolled) {
                                    let toolbar = $("#LeadToolbar").data("kendoToolBar") as kendo.ui.ToolBar;
                                    toolbar.enable("#leadToolBarSave", false);
                                } else {
                                    let toolbar = $("#LeadToolbar").data("kendoToolBar") as kendo.ui.ToolBar;
                                    toolbar.enable("#leadToolBarSave", true);
                                }
                            }
                            //#region Demographic  Enter the information in the controls
                            $("#txtFirstName").val(msg.FirstName);
                            $("#txtMiddleName").val(msg.MiddleName);
                            $("#txtLastName").val(msg.LastName);
                            this.lastNameInitial = msg.LastName;

                            var pln: kendo.ui.DropDownList = $("#cbPrevlast").data("kendoDropDownList") as any;
                            var dshistory: kendo.data.DataSource = new kendo.data.DataSource();
                            for (var n = 0; n < msg.LeadLastNameHistoryList.length; n++) {
                                dshistory.add(msg.LeadLastNameHistoryList[n]);
                            }

                            pln.setDataSource(dshistory);
                            if (pln.dataSource.data.length > 0) {
                                pln.select(0);
                            }
                            pln.list.width("auto");

                            $("#tbNickname").val(msg.NickName);
                            ($("#maskedsocialSecurity").data("kendoMaskedTextBox") as any).value(msg.SSN);

                            $("#tdriverlicensenumber").val(msg.DriverLicenseNumber);
                            $("#tbaliennumber").val(msg.AlienNumber);
                            $("#textage").val(msg.Age);
                            ($("#itextdependants").data("kendoNumericTextBox") as any).value(msg.Dependants);

                            ($("#cbsuffix").data("kendoDropDownList") as any).value(msg.Suffix);
                            ($("#cbgender").data("kendoDropDownList") as any).value(msg.Gender);
                            ($("#cbprefix").data("kendoDropDownList") as any).value(msg.Prefix);
                            ($("#cbRace").data("kendoDropDownList") as any).value(msg.RaceId);

                            // DOB and AGE Calculation.......................................
                            if (msg.Dob != null) {
                                this.calculateAge(msg.Dob.toString(), msg.Age);
                            } else {
                                this.calculateAge(null, msg.Age);
                            }

                            ($("#cbcitizenship").data("kendoDropDownList") as any).value(msg.Citizenship);
                            ($("#cbdependency").data("kendoDropDownList") as any).value(msg.Dependency);
                            ($("#cbmaritalstatus").data("kendoDropDownList") as any).value(msg.MaritalStatus);
                            ($("#cbfamilyincome").data("kendoDropDownList") as any).value(msg.FamilyIncoming);
                            ($("#cbhousingtype").data("kendoDropDownList") as any).value(msg.HousingType);
                            ($("#cbdriverlicensestate").data("kendoDropDownList") as any).value(msg.DrvLicStateCode);
                            ($("#cbtransportation").data("kendoDropDownList") as any).value(msg.Transportation);

                            // Distance to School
                            if (msg.DistanceToSchool != null) {
                                ($("#tpTimeTo").data("kendoNumericTextBox") as any).value(msg.DistanceToSchool);
                            }

                            // Vehicles
                            for (let x = 0; x < msg.Vehicles.length; x++) {
                                if (msg.Vehicles[x].Position === 1) {
                                    $("#vehicleParkingPermit1").val(msg.Vehicles[x].Permit);
                                    $("#vehicleMaker1").val(msg.Vehicles[x].Make);
                                    $("#vehicleModel1").val(msg.Vehicles[x].Model);
                                    $("#vehicleColor1").val(msg.Vehicles[x].Color);
                                    $("#vehiclePlate1").val(msg.Vehicles[x].Plate);
                                }
                                if (msg.Vehicles[x].Position === 2) {
                                    $("#vehicleParkingPermit2").val(msg.Vehicles[x].Permit);
                                    $("#vehicleMaker2").val(msg.Vehicles[x].Make);
                                    $("#vehicleModel2").val(msg.Vehicles[x].Model);
                                    $("#vehicleColor2").val(msg.Vehicles[x].Color);
                                    $("#vehiclePlate2").val(msg.Vehicles[x].Plate);
                                }
                            }

                            //#endregion

                            //#region Contact Info
                            switch (msg.PreferredContactId) {
                                case 1:
                                    $("#preferredPhone").click();
                                    break;
                                case 2:
                                    $("#preferredEmail").click();
                                    break;
                                case 3:
                                    $("#preferredText").click();
                                    break;
                                default:
                                    {
                                        $("#preferredPhone").click();
                                    }
                            }

                            if (msg.LeadAddress != null) {
                                if (msg.LeadAddress.IsShowOnLeadPage === "Yes") {
                                    // County Country, State
                                    ($("#cbCounty").data("kendoDropDownList") as any).value(msg.LeadAddress.CountyId);
                                    ($("#cbcountry").data("kendoDropDownList") as any).value(msg.LeadAddress.CountryId);
                                    ($("#cbAddress").data("kendoDropDownList") as any).value(msg.LeadAddress.AddressTypeId);
                                    $("#txtCity").val(msg.LeadAddress.City);

                                    // International Address & USA Address
                                    var isInt = "false";
                                    if (msg.LeadAddress.IsInternational != null) {
                                        isInt = msg.LeadAddress.IsInternational.toString();
                                    }
                                    var cbState = $("#cbState").data("kendoDropDownList") as any;
                                    var otherState = $("#otherStates");
                                    var zipcode = $("#txtZipCode").data("kendoMaskedTextBox") as any;
                                    if (isInt === "true") {
                                        $("#checkIntAddress").prop('checked', true);
                                        cbState.wrapper.hide();
                                        otherState.css("display", "block");
                                        otherState.val(msg.LeadAddress.StateInternational);
                                        zipcode.setOptions({ mask: 'aaaaaaaaaa' });
                                    } else {
                                        $("#checkIntAddress").prop('checked', false);
                                        cbState.wrapper.show();
                                        otherState.css("display", "none");
                                        zipcode.setOptions({ mask: '00000' });
                                        cbState.value(msg.LeadAddress.StateId);
                                    }

                                    // Address common information.
                                    zipcode.value(msg.LeadAddress.ZipCode);
                                    $("#txtAddress1").val(msg.LeadAddress.Address1);
                                    $("#txtApart").val(msg.LeadAddress.AddressApto);
                                    $("#txtAddress2").val(msg.LeadAddress.Address2);
                                }
                            }

                            // Get phones values
                            if (msg.PhonesList != undefined && msg.PhonesList != null && msg.PhonesList.length > 0) {

                                // Enter phone in controls..
                                var list = msg.PhonesList;

                                for (var i = 0; i < list.length; i++) {

                                    if (list[i].Position === 1) {
                                        let isf = list[i].IsForeignPhone;
                                        $("#checkphone").prop("checked", isf);
                                        if (isf) {
                                            ($("#txtPhone").data("kendoMaskedTextBox") as any).setOptions({ mask: '999999999999999' });
                                        }

                                        ($("#txtPhone").data("kendoMaskedTextBox") as any).value(list[i].Phone);
                                        $("#hiddenPhoneId").val(list[i].ID);
                                        $("#tbphoneext").val(list[i].Extension);
                                        ($("#cbphone").data("kendoDropDownList") as any).value(list[i].PhoneTypeId);
                                    }

                                    if (list[i].Position === 2) {
                                        var isf1 = list[i].IsForeignPhone;
                                        $("#checkphone1").prop("checked", isf1);
                                        if (isf1) {
                                            ($("#txtPhone1").data("kendoMaskedTextBox") as any).setOptions({ mask: '999999999999999' });
                                        }

                                        ($("#txtPhone1").data("kendoMaskedTextBox") as any).value(list[i].Phone);
                                        $("#hiddenPhone1Id").val(list[i].ID);
                                        $("#tbphone1ext").val(list[i].Extension);
                                        ($("#cbphone1").data("kendoDropDownList") as any).value(list[i].PhoneTypeId);
                                    }

                                    if (list[i].Position === 3) {
                                        var isf2 = list[i].IsForeignPhone;
                                        $("#checkphone2").prop("checked", isf2);
                                        if (isf2) {
                                            ($("#txtPhone2").data("kendoMaskedTextBox") as any).setOptions({ mask: '999999999999999' });
                                        }

                                        ($("#txtPhone2").data("kendoMaskedTextBox") as any).value(list[i].Phone);
                                        $("#hiddenPhone2Id").val(list[i].ID);
                                        $("#tbphone2ext").val(list[i].Extension);
                                        ($("#cbphone2").data("kendoDropDownList") as any).value(list[i].PhoneTypeId);
                                    }
                                }
                            }

                            // Get email values
                            if (msg.EmailList != undefined && msg.EmailList != null && msg.EmailList.length > 0) {

                                // Enter emails in controls..
                                var list1 = msg.EmailList;

                                if (list1.length === 1) {
                                    if (list1[0].IsPreferred === true) {
                                        $("#txtemailprimary").val(list1[0].Email);
                                        $("#hiddenEmailPrimaryId").val(list1[0].ID);
                                        ($("#cbemailprimary").data("kendoDropDownList") as any).value(list1[0].EmailTypeId);
                                    }
                                    else if (list1[0].IsShowOnLeadPage === "Yes") {
                                        $("#txtemail").val(list1[0].Email);
                                        $("#hiddenEmailId").val(list1[0].ID);
                                        ($("#cbemail").data("kendoDropDownList") as any).value(list1[0].EmailTypeId);
                                    }
                                }
                                else if (list1.length > 1) {
                                    if (list1[0].IsPreferred === true) {
                                        $("#txtemailprimary").val(list1[0].Email);
                                        $("#hiddenEmailPrimaryId").val(list1[0].ID);
                                        ($("#cbemailprimary").data("kendoDropDownList") as any).value(list1[0].EmailTypeId);
                                    }
                                    if (list1[1].IsShowOnLeadPage === "Yes") {
                                        $("#txtemail").val(list1[1].Email);
                                        $("#hiddenEmailId").val(list1[1].ID);
                                        ($("#cbemail").data("kendoDropDownList") as any).value(list1[1].EmailTypeId);
                                    }
                                    else if (list1[0].IsPreferred === false && list1[0].IsShowOnLeadPage === "Yes") {
                                        $("#txtemail").val(list1[0].Email);
                                        $("#hiddenEmailId").val(list1[0].ID);
                                        ($("#cbemail").data("kendoDropDownList") as any).value(list1[0].EmailTypeId);
                                    }
                                }
                            }

                            ($("#tpBestTime").data("kendoTimePicker") as any).value(msg.BestTime);

                            //None Email
                            $("#checkNoEmail").prop("checked", msg.NoneEmail);
                            $("#txtemailprimary").prop("disabled", msg.NoneEmail);
                            $("#txtemail").prop("disabled", msg.NoneEmail);
                            if (msg.NoneEmail) {
                                $("#txtemail").css("color", "white");
                                $("#txtemailprimary").css("color", "white");
                            }
                            //#endregion

                            //#region Source fields
                            ($("#cbcategorySource").data("kendoDropDownList") as any).value(msg.SourceCategoryId);
                            ($("#cbtypeSource").data("kendoDropDownList") as any).value(msg.SourceTypeId);
                            ($("#cbAdvertisementSource").data("kendoDropDownList") as any).value(msg.AdvertisementId);
                            $("#txtVendorSource").val(msg.VendorSource);
                            $("#txtNoteSource").val(msg.Note);
                            if (msg.SourceDateTime != null) {
                                ($("#dtDateSource").data("kendoDateTimePicker") as any).value(msg.SourceDateTime);
                            }

                            //#endregion

                            //#region Others Fields
                            if (msg.DateApplied != null) {
                                ($("#dtotherDateApplied").data("kendoDatePicker") as any).value(msg.DateApplied);
                            }

                            var assignData: any = $("#dtotherAdmRepAssignDate").data("kendoDatePicker");
                            if (msg.AssignedDate != null) {
                                assignData.value(msg.AssignedDate);
                                assignedDateTime = new Date(msg.AssignedDate);
                            } else {
                                assignData.value(new Date());
                                assignedDateTime = new Date();
                            }
                            that.highSchool = msg.HighSchoolId;
                            ($("#cbOtherHighSchool").data("kendoAutoComplete") as any).value(msg.HighSchoolName);

                            if (msg.AdmissionRepId != null) {
                                ($("#cbotherAdmRep").data("kendoDropDownList") as any).value(msg.AdmissionRepId);
                                adminRepOld = msg.AdmissionRepId;
                            } else {
                                adminRepOld = "select";
                            }
                            ($("#cbotherSponsor").data("kendoDropDownList") as any).value(msg.AgencySponsorId);
                            $("#tbOtherComment").val(msg.Comments);
                            ($("#cbotherPreviousEducation").data("kendoDropDownList") as any).value(msg.PreviousEducationId);
                            if (msg.HighSchoolGradDate != null) {
                                ($("#dtHsGradDate").data("kendoDatePicker") as any).value(msg.HighSchoolGradDate);
                            }

                            if (msg.AttendingHs == null) {
                                msg.AttendingHs = -1;
                            }

                            ($("#ddAttendingHs").data("kendoDropDownList") as any).value(msg.AttendingHs);
                            ($("#cbotherAdmCriteria").data("kendoDropDownList") as any).value(msg.AdminCriteriaId);
                            ($("#cbotherReasonNot").data("kendoDropDownList") as kendo.ui.DropDownList).value(msg.ReasonNotEnrolled);

                            //#endregion

                            //#region Academics.................................................................................
                            if (msg.CampusId == null) {
                                msg.CampusId = XMASTER_GET_CURRENT_CAMPUS_ID;
                            }
                            ($("#cbLeadAssign").data("kendoDropDownList") as any).value(msg.CampusId);

                            ($("#cbInterestArea").data("kendoDropDownList") as any).value(msg.AreaId);

                            // Save campusName 
                            var leadCampusName = $("#cbLeadAssign").data("kendoDropDownList").text();
                            sessionStorage.removeItem("CAMPUS_NAME");
                            sessionStorage.setItem("CAMPUS_NAME", leadCampusName);

                            // Cascade................. from AreaId (Program Group).....................................
                            // Program...
                            //this.getLeadProgramInformation(msg, "ProgramID", "cbprogramLabel", msg.AreaId, msg.ProgramId);
                            this.getLeadProgramInformation(msg, "Program", "cbprogramLabel", msg.AreaId, msg.ProgramId);
                            // ............. end of cascade items

                            let cbattend: kendo.ui.DropDownList = $("#cbattend").data("kendoDropDownList") as any;
                            cbattend.value(msg.AttendTypeId);

                            // Lead Status Analysis -------------------------------------------------------------
                            sessionStorage.setItem("allowedLeadStatus", JSON.stringify(msg.StateChangeIdsList));
                            var dllnew = this.selectLeadAllowedStates(msg.StateChangeIdsList, msg.LeadStatusId);
                            var statusControl: kendo.ui.DropDownList = $("#cbLeadStatus").data("kendoDropDownList") as any;
                            statusControl.setDataSource(dllnew);
                            statusControl.value(msg.LeadStatusId);
                            // ----------------------------------------------------------------------------------
                            //#endregion

                            //#region Custom Fields
                            if (msg.SdfList != null) {

                                for (var k = 0; k < msg.SdfList.length; k++) {
                                    var id: string = "#" + msg.SdfList[k].SdfId;
                                    var value = msg.SdfList[k].SdfValue;
                                    if ($(id).attr("data-role") === 'dropdownlist') {
                                        if ($(id).data("kendoDropDownList") != null) {
                                            $(id).data("kendoDropDownList").value(value);
                                            $(id).data("kendoDropDownList").list.width("auto");
                                        }
                                    } else {
                                        if (value) {
                                            let type = msg.SdfList[k].DtypeId;
                                            switch (type) {
                                                case 1:
                                                case 2:
                                                    {
                                                        $(id).val(value);
                                                        break;
                                                    }
                                                case 3: // Date
                                                    {
                                                        if ($(id).data("kendoDatePicker") != null) {
                                                            ($(id).data("kendoDatePicker") as any)
                                                                .value(value);
                                                            break;
                                                        }
                                                    }
                                                case 4: //Boolean checkbox
                                                    {
                                                        let isf = !(value === "False");
                                                        if (isf != null) {
                                                            if (isf) {
                                                                if ($(id) != null) {
                                                                    $(id).prop("checked", isf);
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                default:
                                                    {
                                                        $(id).val(value);
                                                    }
                                            }
                                        }
                                    }
                                }
                            }
                            //#endregion

                            //#region Lead Groups

                            // Check the necessary groups
                            var grps: any = Object.create($.parseJSON(sessionStorage.getItem(XLEAD_INFO_GROUPS)));
                            if (grps != null) {

                                // Clear all
                                for (var l = 0; l < grps.length; l++) {
                                    $("#" + grps[l].GroupId).prop("checked", false);
                                }

                                // Check only the used by the lead
                                if (msg.GroupIdList != null) {
                                    for (var m = 0; m < msg.GroupIdList.length; m++) {
                                        $("#" + msg.GroupIdList[m]).prop("checked", true);
                                    }
                                }
                            }

                            //#endregion

                            $("body").css("cursor", "default");

                        } catch (e) {
                            //If errors clear the cache
                            sessionStorage.removeItem(XLEAD_INFO_DLLS_REQUIRED_ITEMS);
                            sessionStorage.removeItem(XLEAD_INFO_GROUPS);
                            sessionStorage.removeItem(XLEAD_INFO_SDF_FIELDS);
                            $("body").css("cursor", "default");
                            MasterPage.SHOW_ERROR_WINDOW(e.message);
                            //alert(e.message);
                        }

                    }

                    // If should validate flag is true, run validator after data load
                    if (this.shouldValidate) {
                        this.leadValidator.validate();
                    }
                })
                .fail(msg => {
                    this.showLoadingLeadPage(false);
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }

        /*
         * From server comes the allowed lead status for the actual lead status
         * the status possibles are not filtered by campus, but by the configured
         * possible status. Always the actual status is added to list. 
         */
        selectLeadAllowedStates(allowedStatusChange: any, leadStatusId: string): kendo.data.DataSource {
            var dllnew = new kendo.data.DataSource();
            var dll = this.getDropDownListItemObject("cbLeadStatus");
            if (allowedStatusChange !== null && allowedStatusChange !== undefined && allowedStatusChange.length > 0) {
                for (var n = 0; n < dll.length; n++) {
                    for (var o = 0; o < allowedStatusChange.length; o++) {
                        if (dll[n].ID.toLocaleString().toUpperCase() === allowedStatusChange[o].toLocaleString().toUpperCase()) {
                            dllnew.add(dll[n]);
                        }
                    }
                }
            }
            var result = $.grep(dll, (t: any) => t.ID === leadStatusId);
            if (result.length > 0) {
                var dllData = dllnew.data();
                var isExist = false;
                for (var i = 0; i < dllData.length; i++) {
                    if (dllData[i].ID === result[0].ID) {
                        isExist = true;
                        break;
                    }
                }
                if (!isExist) {
                    dllnew.add(result[0]);
                }
            }
            return dllnew;
        }

        calculateAge(dob: string, age: string) {
            if (dob != null && dob !== "") {
                ($("#dtDob").data("kendoDatePicker") as any).value(dob);
                // Calculate the Age from DOB
                var calAge = ad.GET_AGE(dob);
                $("#textage").val(calAge.toString());
            } else {
                $("#textage").val(age);
            }
        }

        checkradiobutton(old: string, sel: string) {
            var rfd = $("input[name='contact'][value= '" + old + "'").attr("_rfddecoratedID");
            $("#" + rfd).attr("class", "rfdRadioUnchecked");

            var rfddecoratedId = $("input[name='contact'][value= '" + sel + "'").attr("_rfddecoratedID");
            $("#" + rfddecoratedId).attr("class", "rfdRadioChecked");
        }

        getDropDownListItemObject(controlName: string): any {
            for (var i = 0; i < this.captionRequirementdb.length; i++) {
                if (this.captionRequirementdb[i].controlName === controlName) {
                    return this.captionRequirementdb[i].dll;
                }
            }
            return null;
        }

        getFilteredDropDownListItemObject(controlName: string): any {
            for (var i = 0; i < this.captionRequirementdb.length; i++) {
                if (this.filteredDatadb[i].controlName === controlName) {
                    return this.filteredDatadb[i].dll;
                }
            }

            return null;
        }

        /*
         * Initialize DropDown Controls
         */
        initializeControls(campus: string, id: string = "") {

            //Academic ....................................................

            // Lead status logic..........................
            var cbLeadStatus: kendo.ui.DropDownList = $("#cbLeadStatus").data("kendoDropDownList") as any;
            var dll = this.getDropDownListItemObject("cbLeadStatus");
            if (id === "") {
                for (var i = 0; i < dll.length; i++) {
                    if (dll[i].Description.indexOf("**") === 0) {
                        id = dll[i].ID;
                        dll[i].Description = dll[i].Description.replace("**", "");
                        //break;
                    }
                }
            }

            var cad = sessionStorage.getItem("allowedLeadStatus");
            var allowedStates: string[];
            if (cad !== undefined && cad !== null && cad.length > 10) {

                allowedStates = cad.split(",");
            }
            else {
                allowedStates = [];
            }

            dll = this.selectLeadAllowedStates(allowedStates, id);
            cbLeadStatus.setDataSource(dll);

            cbLeadStatus.value(id);
            cbLeadStatus.list.css("min-width", "150px");
            cbLeadStatus.list.width("auto");


            var cbLeadAssign: kendo.ui.DropDownList = $("#cbLeadAssign").data("kendoDropDownList") as any;
            cbLeadAssign.setDataSource(this.getDropDownListItemObject("cbLeadAssign"));
            cbLeadAssign.value(campus); // 

            var cbInterestArea: kendo.ui.DropDownList = $("#cbInterestArea").data("kendoDropDownList") as any;
            cbInterestArea.setDataSource(this.getFilteredDropDownListItemObject("cbInterestArea"));

            var cbattend: kendo.ui.DropDownList = $("#cbattend").data("kendoDropDownList") as any;
            cbattend.setDataSource(this.getFilteredDropDownListItemObject("cbattend"));

            // Demographic..........................................................
            var cbprefix: kendo.ui.DropDownList = $("#cbprefix").data("kendoDropDownList") as any;
            cbprefix.setDataSource(this.getDropDownListItemObject("cbprefix"));
            cbprefix.list.css("min-width", "90px");
            cbprefix.list.width("auto");


            var cbsuffix: kendo.ui.DropDownList = $("#cbsuffix").data("kendoDropDownList") as any;
            cbsuffix.setDataSource(this.getDropDownListItemObject("cbsuffix"));
            cbsuffix.list.css("min-width", "90px");
            cbsuffix.list.width("auto");


            var cbgender: kendo.ui.DropDownList = $("#cbgender").data("kendoDropDownList") as any;
            cbgender.setDataSource(this.getDropDownListItemObject("cbgender"));
            cbgender.list.css("min-width", "90px");
            cbgender.list.width("auto");

            var cbmaritalstatus: kendo.ui.DropDownList = $("#cbmaritalstatus").data("kendoDropDownList") as any;
            cbmaritalstatus.setDataSource(this.getDropDownListItemObject("cbmaritalstatus"));

            var cbdriverlicensestate: kendo.ui.DropDownList = $("#cbdriverlicensestate").data("kendoDropDownList") as any;
            cbdriverlicensestate.setDataSource(this.getDropDownListItemObject("cbdriverlicensestate"));

            var cbcitizenship: kendo.ui.DropDownList = $("#cbcitizenship").data("kendoDropDownList") as any;
            cbcitizenship.setDataSource(this.getDropDownListItemObject("cbcitizenship"));

            var cbdependency: kendo.ui.DropDownList = $("#cbdependency").data("kendoDropDownList") as any;
            cbdependency.setDataSource(this.getDropDownListItemObject("cbdependency"));

            var cbfamilyincome: kendo.ui.DropDownList = $("#cbfamilyincome").data("kendoDropDownList") as any;
            cbfamilyincome.setDataSource(this.getDropDownListItemObject("cbfamilyincome"));

            var cbhousingtype: kendo.ui.DropDownList = $("#cbhousingtype").data("kendoDropDownList") as any;
            cbhousingtype.setDataSource(this.getDropDownListItemObject("cbhousingtype"));

            var cbtransportation: kendo.ui.DropDownList = $("#cbtransportation").data("kendoDropDownList") as any;
            cbtransportation.setDataSource(this.getDropDownListItemObject("cbtransportation"));

            var cbRace: kendo.ui.DropDownList = $("#cbRace").data("kendoDropDownList") as any;
            cbRace.setDataSource(this.getFilteredDropDownListItemObject("cbRace"));
            cbRace.list.css("min-width", "90px");
            cbRace.list.width("auto");

            // Source
            var cbcategorySource: kendo.ui.DropDownList = $("#cbcategorySource").data("kendoDropDownList") as any;
            cbcategorySource.setDataSource(this.getFilteredDropDownListItemObject("cbcategorySource"));

            var cbtypeSource: kendo.ui.DropDownList = $("#cbtypeSource").data("kendoDropDownList") as any;
            cbtypeSource.setDataSource(this.getDropDownListItemObject("cbtypeSource"));

            var cbAdvertisementSource: kendo.ui.DropDownList = $("#cbAdvertisementSource").data("kendoDropDownList") as any;
            cbAdvertisementSource.setDataSource(this.getDropDownListItemObject("cbAdvertisementSource"));

            // Contact Info
            var cbphone: kendo.ui.DropDownList = $("#cbphone").data("kendoDropDownList") as any;
            cbphone.setDataSource(this.getDropDownListItemObject("cbphone"));

            var cbphone1: kendo.ui.DropDownList = $("#cbphone1").data("kendoDropDownList") as any;
            cbphone1.setDataSource(this.getDropDownListItemObject("cbphone1"));

            var cbphone2: kendo.ui.DropDownList = $("#cbphone2").data("kendoDropDownList") as any;
            cbphone2.setDataSource(this.getDropDownListItemObject("cbphone2"));

            var cbAddress: kendo.ui.DropDownList = $("#cbAddress").data("kendoDropDownList") as any;
            cbAddress.setDataSource(this.getFilteredDropDownListItemObject("cbAddress"));

            var cbemailprimary: kendo.ui.DropDownList = $("#cbemailprimary").data("kendoDropDownList") as any;
            cbemailprimary.setDataSource(this.getDropDownListItemObject("cbemailprimary"));

            var cbemail: kendo.ui.DropDownList = $("#cbemail").data("kendoDropDownList") as any;
            cbemail.setDataSource(this.getDropDownListItemObject("cbemail"));

            var cbcountry: kendo.ui.DropDownList = $("#cbcountry").data("kendoDropDownList") as any;
            cbcountry.setDataSource(this.getFilteredDropDownListItemObject("cbcountry"));

            var cbCounty: kendo.ui.DropDownList = $("#cbCounty").data("kendoDropDownList") as any;
            cbCounty.setDataSource(this.getFilteredDropDownListItemObject("cbCounty"));

            var cbState: kendo.ui.DropDownList = $("#cbState").data("kendoDropDownList") as any;
            cbState.setDataSource(this.getDropDownListItemObject("cbState"));

            // Others
            var cbotherAdmRep: kendo.ui.DropDownList = $("#cbotherAdmRep").data("kendoDropDownList") as any;
            cbotherAdmRep.setDataSource(this.getFilteredDropDownListItemObject("cbotherAdmRep"));

            var cbotherPreviousEducation: kendo.ui.DropDownList = $("#cbotherPreviousEducation").data("kendoDropDownList") as any;
            cbotherPreviousEducation.setDataSource(this.getFilteredDropDownListItemObject("cbotherPreviousEducation"));

            var cbOtherHighSchool: kendo.ui.AutoComplete = $("#cbOtherHighSchool").data("kendoAutoComplete") as any;
            let institutionDb: SystemInstitution.InstitutionDb = new SystemInstitution.InstitutionDb();

            cbOtherHighSchool.setDataSource(institutionDb.findData({
                CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                UserId: $("#hdnUserId").val(),
                Level: "0",
                InstitutionType: "2"
            } as InstitutionFilterInputModel));
            let that = this;
            cbOtherHighSchool.bind("select", (e: any) => {
                that.highSchool = ($("#cbOtherHighSchool").data("kendoAutoComplete") as kendo.ui.AutoComplete).dataItem(e.item.index()).Id;
            });
            $("#cbOtherHighSchool").removeAttr("style");
            var cbotherAdmCriteria: kendo.ui.DropDownList = $("#cbotherAdmCriteria").data("kendoDropDownList") as any;
            cbotherAdmCriteria.setDataSource(this.getFilteredDropDownListItemObject("cbotherAdmCriteria"));

            var cbotherReasonNot: kendo.ui.DropDownList = $("#cbotherReasonNot").data("kendoDropDownList") as any;
            cbotherReasonNot.setDataSource(this.getFilteredDropDownListItemObject("cbotherReasonNot"));
            cbotherReasonNot.list.width("auto");

            var cbotherSponsor: kendo.ui.DropDownList = $("#cbotherSponsor").data("kendoDropDownList") as kendo.ui.DropDownList;
            cbotherSponsor.setDataSource(this.getFilteredDropDownListItemObject("cbotherSponsor"));
            cbotherSponsor.list.width("auto");


            // Resize Drop Downs
            this.resizeDropDown(cbprefix);
            this.resizeDropDown(cbsuffix);
            this.resizeDropDown(cbgender);
            this.resizeDropDown(cbdependency);
            this.resizeDropDown(cbmaritalstatus);
            this.resizeDropDown(cbfamilyincome);
            this.resizeDropDown(cbhousingtype);
            this.resizeDropDown(cbdriverlicensestate);
            this.resizeDropDown(cbtransportation);
            this.resizeDropDown(cbcitizenship);
            this.resizeDropDown(cbattend);
            this.resizeDropDown(cbLeadStatus);

            this.resizeDropDown(cbphone);
            this.resizeDropDown(cbphone1);
            this.resizeDropDown(cbphone2);
            this.resizeDropDown(cbState);
            this.resizeDropDown(cbAddress);
            this.resizeDropDown(cbemailprimary);
            this.resizeDropDown(cbemail);
            this.resizeDropDown(cbotherSponsor);
            this.resizeDropDown(cbotherPreviousEducation);
            this.resizeDropDown(cbotherReasonNot);

        }

        /*
         * Fill the filtered data DLL information. This array has the items for combo-box
         * filtered by the campusId.
         */
        fillFilteredDatadb(campusId: string) {

            for (var i = 0; i < this.captionRequirementdb.length; i++) {

                if (this.captionRequirementdb[i].dll == undefined || this.captionRequirementdb[i].dll.length === 0) {
                    continue;
                }
                this.filteredDatadb[i].dll = []; // Clean the filtered data
                for (var x = 0; x < this.captionRequirementdb[i].dll.length; x++) {

                    if (this.captionRequirementdb[i].dll[x].CampusesIdList != undefined
                        && this.captionRequirementdb[i].dll[x].CampusesIdList.length > 0) {

                        var list = this.captionRequirementdb[i].dll[x].CampusesIdList;
                        for (let j = 0; j < list.length; j++) {

                            if (list[j].CampusId === campusId) {
                                this.filteredDatadb[i].dll.push(this.captionRequirementdb[i].dll[x]);
                                break;
                            }
                        }
                    }
                }
            }
        }

        resizeDropDown = e => {
            e.list.width("auto");
        }

        /* Open the Contact Information page */
        onClickContactInformation() {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/leadContacts.aspx?resid=270&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Quick&mod=AD");
        }

        /* Get the Program (program combo box) Information. Also should trigger the Expected Start and Program Version  */
        getLeadProgramInformation(msgOr: any, fldName: string, targetDropDown: string, value: Object, setting: string = null) {
            // Request the info from server
            this.db.getLeadItemInformation(fldName, value, XMASTER_GET_CURRENT_CAMPUS_ID, this)
                .done(msg => {
                    if (msg != undefined) {
                        // Enter the information in the controls
                        var cbprogramLabel: any = $("#" + targetDropDown).data("kendoDropDownList");

                        // Set the data source
                        cbprogramLabel.setDataSource(msg);
                        // Optional set the value of the filled drop down
                        if (setting != null) {
                            cbprogramLabel.value(setting);
                            for (var i = 0; i < msg.length; i++) {
                                if (setting.toLowerCase() === msg[i].ID.toLowerCase()) {
                                    if (msg[i].CalendarType.toLowerCase() === "clock hour") {
                                        this.isClockHourProgram = true;

                                    } else {
                                        this.isClockHourProgram = false;
                                    }
                                    break;
                                }
                            }
                        }


                        // Call next cascade (Program Version)
                        this.getLeadProgramTypeInformation(msgOr, "PrgVerId", "cbprogramVersion", msgOr.ProgramId, msgOr.PrgVerId);

                        // Call Next Cascade ExpectedStart Date
                        //if (XMASTER_LEAD_IS_CLOCK_HOUR === 'Yes') {
                        var cbexpectedStart = $("#cbExpectedStart").data("kendoDropDownList") as any;
                        if (this.isClockHourProgram) {
                            $("#dpExpectedStart").css("display", "inline");
                            $("#dpExpectedStart").css("width", "auto");
                            $("#dpExpectedStart").kendoDatePicker();
                            cbexpectedStart.wrapper.hide();
                            if (msgOr.ExpectedStart != null && msgOr.ExpectedStart != undefined) {
                                ($("#dpExpectedStart").data("kendoDatePicker") as kendo.ui.DatePicker).value(kendo.parseDate(msgOr.ExpectedStart));
                            }
                        } else {
                            cbexpectedStart.wrapper.show();
                            $("#dpExpectedStart").parent().parent().hide();
                            // ExpectedStart with Term...
                            if (msgOr.ExpectedStart == null) {
                                this.getExpectedLeadItemInformation(XMASTER_GET_CURRENT_CAMPUS_ID, "ExpectedStart", "cbExpectedStart", msgOr.ProgramId);
                            } else {
                                var dat1: Date = kendo.parseDate(msgOr.ExpectedStart);
                                var lookdate: string = dat1.toLocaleString("en-US").split(" ")[0].toString();
                                lookdate = lookdate.replace(",", "");
                                this.getExpectedLeadItemInformation(XMASTER_GET_CURRENT_CAMPUS_ID, "ExpectedStart", "cbExpectedStart", msgOr.ProgramId, lookdate);
                            }
                        }
                    }
                })
                .fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }
        //checkIsClockHr(): boolean {
        //    var cbprogramLabel: any = $("#cbprogramLabel").data("kendoDropDownList") as any;
        //    var selectedProg = cbprogramLabel.value();
        //    var dataTable = cbprogramLabel.dataSource.data();
        //    if (selectedProg != null && selectedProg !== "") {
        //        var calendartype = dataTable[cbprogramLabel.selectedIndex - 1]["CalendarType"];
        //        if (calendartype.toLowerCase() === "clock hour") {
        //            this.isClockHourProgram = true;
        //        } else {
        //            this.isClockHourProgram = false;
        //        }
        //    } else {
        //        this.isClockHourProgram = false;
        //    }
        //    return this.isClockHourProgram;
        //}
        /* Get the program Version. This trigger also the Schedule */
        getLeadProgramTypeInformation(msgOr: any, fldName: string, targetDropDown: string, value: Object, setting: string = null) {
            // Request the info from server
            this.db.getLeadItemInformation(fldName, value, XMASTER_GET_CURRENT_CAMPUS_ID, this)
                .done(msg => {
                    if (msg != undefined) {
                        // Enter the information in the controls
                        var cbprogramVersion: kendo.ui.DropDownList = $("#" + targetDropDown).data("kendoDropDownList") as any;

                        // Set the data source
                        cbprogramVersion.setDataSource(msg);

                        // Optional set the value of the filled drop down
                        if (setting != null) {
                            cbprogramVersion.value(setting);
                        }

                        // Get the schedule and the expected start date
                        this.getItemInformation(XMASTER_GET_CURRENT_CAMPUS_ID, "ProgramScheduleId", "cbschedule", msgOr.PrgVerId, msgOr.ScheduleId);
                    }
                })
                .fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }

        /* Get the expected Start of the Lead */
        getExpectedLeadItemInformation(campusId: string, fldName: string, targetDropDown: string, value: Object, setting: string = null) {

            // Request the info from server

            this.db.getLeadItemInformation(fldName, value, campusId, this)
                .done(msg => {
                    if (msg != undefined) {
                        // Enter the information in the controls
                        var cbExpected: kendo.ui.DropDownList = $("#" + targetDropDown).data("kendoDropDownList") as any;
                        // Set the data source
                        cbExpected.setDataSource(msg);

                        // Optional set the value of the filled drop down
                        if (setting != null) {

                            // Get the list of drop down
                            var list: any = cbExpected.dataSource.data();
                            if (list != null) {

                                // Look up through the list to get the item that contain the date
                                for (var i = 0; i < list.length; i++) {
                                    var descriptdate: string = list[i].Description.split("]")[0].slice(1).toString().trim();
                                    if (descriptdate.localeCompare(setting) === 0) {

                                        // Enter the value
                                        cbExpected.value(list[i].ID);
                                        break;
                                    }
                                }
                            }
                        }

                        // Resize the drop down to avoid wrap
                        //    this.resizeDropDown(cbExpected);
                    }
                    //else {
                    //    cbExpected.setDataSource(new kendo.data.DataSource());
                    //}
                })
                .fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }

        /* Manage changes of a specific drop down it is used to manage the user change in cascade drop down */
        getItemInformation(campusId: string, fldName: string, targetDropDown: string, value: Object, setting: string = null) {
            // Request the info from server
            this.db.getLeadItemInformation(fldName, value, campusId, this)
                .done(msg => {
                    if (msg != undefined) {
                        // Enter the information in the controls
                        var itemdd: kendo.ui.DropDownList = $("#" + targetDropDown).data("kendoDropDownList") as any;

                        // Set the data source
                        itemdd.setDataSource(msg);

                        // Optional set the value of the filled drop down
                        if (setting != null) {
                            itemdd.value(setting);
                        }
                    }
                })
                .fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }



        /*
         * Send Information about the lead to server
         */

        postLeadInformation(leadGuid: string, avoidDuplicate: boolean, redirectpage?: string) {
            var lead: ILeadInfoPageOutputModel = new LeadInfoPageOutputModel();

            // Demographic
            lead.FirstName = $("#txtFirstName").val();
            lead.MiddleName = $("#txtMiddleName").val();
            lead.LastName = $("#txtLastName").val();
            var context = this;

            if (this.lastNameInitial !== "") {
                if (this.lastNameInitial !== lead.LastName) {
                    let message: string =
                        "<b>Is this an official name change</b>? <br/><br/> Selecting Yes will track Prior Last Name.<br/> Selecting No will still update the Last Name field but not update the Prior Last name field.";
                    $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(message))
                        .then(function (confirmed) {
                            if (confirmed) {
                                var hist: ILastNameHistory = new LastNameHistory();
                                hist.LastName = context.lastNameInitial;
                                hist.LeadId = lead.LeadId;
                                lead.LeadLastNameHistoryList = [];
                                lead.LeadLastNameHistoryList.push(hist);
                                context.postLeadInformationToServer(lead, leadGuid, avoidDuplicate, redirectpage);
                                let cbPrior = $("#cbPrevlast").data("kendoDropDownList");
                                cbPrior.dataSource.insert(0,hist);
                                cbPrior.text(hist.LastName);

                            } else {
                                context.postLeadInformationToServer(lead, leadGuid, avoidDuplicate, redirectpage);
                            }
                        });

                } else {
                    context.postLeadInformationToServer(lead, leadGuid, avoidDuplicate, redirectpage);
                }
            } else {
                context.postLeadInformationToServer(lead, leadGuid, avoidDuplicate, redirectpage);
            }
        }


        postLeadInformationToServer(lead: ILeadInfoPageOutputModel, leadGuid: string, avoidDuplicate: boolean, redirectpage?: string) {
            $("body").css("cursor", "progress");
            //  var lead: ILeadInfoPageOutputModel = new LeadInfoPageOutputModel();

            // Determine if it is a insertion of a new lead or a update
            var newLead = sessionStorage.getItem(XLEAD_NEW);
            if (newLead != null && newLead === "true") {
                // it is a insertion
                lead.LeadId = '00000000-0000-0000-0000-000000000000';
                lead.AvoidDuplicateAnalysis = avoidDuplicate;
                assignedDateTime = new Date();
                adminRepOld = "select";
            } else {
                // it is a lead update
                lead.LeadId = leadGuid;
            }

            // Continue to fill the entity
            var userId = XMASTER_PAGE_USER_OPTIONS_USERID;

            // Fill the necessary filter information
            lead.InputModel.UserId = userId;
            lead.InputModel.CampusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            lead.InputModel.LeadId = lead.LeadId;


            lead.ModUser = userId;

            lead.NickName = $("#tbNickname").val();
            var ssn = ($("#maskedsocialSecurity").data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox).value();
            ssn = ssn.replace(/[^0-9]/g, "");
            lead.SSN = ssn;
            lead.DriverLicenseNumber = $("#tdriverlicensenumber").val();
            lead.AlienNumber = $("#tbaliennumber").val();
            lead.Age = $("#textage").val();
            lead.Dependants = ($("#itextdependants").data("kendoNumericTextBox") as kendo.ui.NumericTextBox).value();
            lead.DistanceToSchool = ($("#tpTimeTo").data("kendoNumericTextBox") as kendo.ui.NumericTextBox).value();
            lead.Suffix = ($("#cbsuffix").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.Gender = ($("#cbgender").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.Prefix = ($("#cbprefix").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.RaceId = ($("#cbRace").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.Dob = ($("#dtDob").data("kendoDatePicker") as kendo.ui.DatePicker).value();
            lead.Citizenship = ($("#cbcitizenship").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.Dependency = ($("#cbdependency").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.MaritalStatus = ($("#cbmaritalstatus").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.FamilyIncoming = ($("#cbfamilyincome").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.HousingType = ($("#cbhousingtype").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.DrvLicStateCode = ($("#cbdriverlicensestate").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.Transportation = ($("#cbtransportation").data("kendoDropDownList") as kendo.ui.DropDownList).value();

            //Vehicles
            let veh: IVehicle = new Vehicle();
            veh.Permit = $("#vehicleParkingPermit1").val();
            veh.Make = $("#vehicleMaker1").val();
            veh.Model = $("#vehicleModel1").val();
            veh.Color = $("#vehicleColor1").val();
            veh.Plate = $("#vehiclePlate1").val();
            veh.Position = 1;
            //veh.ModUser = userId;
            lead.Vehicles.push(veh);

            let veh2: IVehicle = new Vehicle();
            veh2.Permit = $("#vehicleParkingPermit2").val();
            veh2.Make = $("#vehicleMaker2").val();
            veh2.Model = $("#vehicleModel2").val();
            veh2.Color = $("#vehicleColor2").val();
            veh2.Plate = $("#vehiclePlate2").val();
            veh2.Position = 2;
            lead.Vehicles.push(veh2);

            // Contacts
            //preferred contact information
            switch (this.preferredMethod) {
                case "Phone":
                    lead.PreferredContactId = 1;
                    break;
                case "Email":
                    lead.PreferredContactId = 2;
                    break;
                case "Text":
                    lead.PreferredContactId = 3;
                    break;
                default:
                    lead.PreferredContactId = 1;
            }

            lead.LeadAddress.CountyId = ($("#cbCounty").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.LeadAddress.CountryId = ($("#cbcountry").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.LeadAddress.AddressTypeId = ($("#cbAddress").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.LeadAddress.City = $("#txtCity").val();
            lead.LeadAddress.IsInternational = ($("#checkIntAddress").prop("checked"));
            lead.LeadAddress.StateInternational = $("#otherStates").val();
            lead.LeadAddress.StateId = ($("#cbState").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.LeadAddress.ZipCode = ($("#txtZipCode").data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox).value();
            lead.LeadAddress.Address1 = $("#txtAddress1").val();
            lead.LeadAddress.AddressApto = $("#txtApart").val();
            lead.LeadAddress.Address2 = $("#txtAddress2").val();
            lead.LeadAddress.IsShowOnLeadPage = 'yes';
            lead.LeadAddress.IsMailingAddress = 'yes';
            lead.PhonesList = [];

            var phone: IPhone = new Phone();
            phone.Extension = $("#tbphoneext").val();
            phone.ID = $("#hiddenPhoneId").val();
            phone.IsForeignPhone = ($("#checkphone").prop("checked"));
            phone.Phone = ($("#txtPhone").data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox).raw();
            phone.PhoneTypeId = ($("#cbphone").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            phone.Position = 1;
            lead.PhonesList.push(phone);

            var phone1: IPhone = new Phone();
            phone1.Extension = $("#tbphone1ext").val();
            phone1.ID = $("#hiddenPhone1Id").val();
            phone1.IsForeignPhone = ($("#checkphone1").prop("checked"));
            phone1.Phone = ($("#txtPhone1").data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox).raw();
            phone1.PhoneTypeId = ($("#cbphone1").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            phone1.Position = 2;
            lead.PhonesList.push(phone1);

            var phone2: IPhone = new Phone();
            phone2.Extension = $("#tbphone2ext").val();
            phone2.ID = $("#hiddenPhone2Id").val();
            phone2.IsForeignPhone = ($("#checkphone2").prop("checked"));
            phone2.Phone = ($("#txtPhone2").data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox).raw();
            phone2.PhoneTypeId = ($("#cbphone2").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            phone2.Position = 3;
            lead.PhonesList.push(phone2);

            lead.NoneEmail = ($("#checkNoEmail").prop("checked"));
            lead.EmailList = [];
            var email: IeMail = new Email();
            email.Email = $("#txtemailprimary").val();
            email.EmailTypeId = ($("#cbemailprimary").data("kendoDropDownList") as any).value();
            email.ID = $("#hiddenEmailPrimaryId").val();
            email.IsPreferred = true;
            email.IsShowOnLeadPage = "Yes";
            lead.EmailList.push(email);

            var email1: IeMail = new Email();
            email1.Email = $("#txtemail").val();
            email1.ID = $("#hiddenEmailId").val();
            email1.EmailTypeId = ($("#cbemail").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            email1.IsPreferred = false;
            email1.IsShowOnLeadPage = "Yes";
            lead.EmailList.push(email1);
            let besttime: kendo.ui.DateTimePicker = $("#tpBestTime").data("kendoTimePicker") as any;
            let datetime: Date = besttime.value();
            if (datetime !== null && datetime !== undefined) {
                let output: string = datetime.getHours() + ":" + datetime.getMinutes();
                lead.BestTime = output;
            }

            //#region Source fields
            lead.SourceCategoryId = ($("#cbcategorySource").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.SourceTypeId = ($("#cbtypeSource").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.AdvertisementId = ($("#cbAdvertisementSource").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.SourceDateTime = kendo.toString(($("#dtDateSource").data("kendoDateTimePicker") as kendo.ui.DateTimePicker).value(), "MM/dd/yyyy HH:mm:ss");
            lead.Note = $("#txtNoteSource").val();
            //#endregion

            //#region Others Fields

            lead.DateApplied = kendo.toString(($("#dtotherDateApplied").data("kendoDatePicker") as kendo.ui.DatePicker).value(), "MM/dd/yyyy HH:mm:ss");
            let assignedDatetimeChange: Date = ($("#dtotherAdmRepAssignDate").data("kendoDatePicker") as kendo.ui.DatePicker).value();
            let temDateDb: Date = new Date(assignedDateTime.toString());
            let tempChange: string = assignedDatetimeChange.getMonth() + "/" + assignedDatetimeChange.getDate() + "/" + assignedDatetimeChange.getFullYear();
            let tempDate: string = temDateDb.getMonth() + "/" + temDateDb.getDate() + "/" + temDateDb.getFullYear();
            //if the date is same as that of the database... means no change in time else take the system time and store in DB
            if (tempChange === tempDate) {
                if (adminRepOld === ($("#cbotherAdmRep").data("kendoDropDownList") as kendo.ui.DropDownList).value()) //the admin rep are same
                {
                    lead.AssignedDate = kendo.toString(assignedDateTime, "MM/dd/yyyy HH:mm:ss");//as this has the original system time when the lead was assigned to the rep.
                } else {
                    //take the same date and current system time.
                    let dtPass: Date = new Date(assignedDateTime.toString());
                    let finDate: string = this.getFinalDate(dtPass);
                    lead.AssignedDate = kendo.toString(new Date(finDate), "MM/dd/yyyy HH:mm:ss");
                }
            } else {
                let finDate: string = this.getFinalDate(assignedDatetimeChange);
                lead.AssignedDate = kendo.toString(new Date(finDate), "MM/dd/yyyy HH:mm:ss");
            }

            lead.AdmissionRepId = ($("#cbotherAdmRep").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.PreviousEducationId = ($("#cbotherPreviousEducation").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.HighSchoolGradDate = ($("#dtHsGradDate").data("kendoDatePicker") as kendo.ui.DatePicker).value();
            //lead.HighSchoolId = ($("#cbOtherHighSchool").data("kendoAutoComplete") as kendo.ui.AutoComplete).value();
            lead.HighSchoolId = this.highSchool;
            lead.Comments = $("#tbOtherComment").val();
            lead.AttendingHs = (($("#ddAttendingHs").data("kendoDropDownList") as kendo.ui.DropDownList).value()) === "-1" ? null : parseInt(($("#ddAttendingHs").data("kendoDropDownList") as kendo.ui.DropDownList).value());
            lead.AgencySponsorId = ($("#cbotherSponsor").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.AdminCriteriaId = ($("#cbotherAdmCriteria").data("kendoDropDownList") as kendo.ui.DropDownList).value();

            lead.ReasonNotEnrolled = ($("#cbotherReasonNot").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            //#endregion

            // Academics.................................................................................
            lead.CampusId = ($("#cbLeadAssign").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.AreaId = ($("#cbInterestArea").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.ProgramId = ($("#cbprogramLabel").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.PrgVerId = ($("#cbprogramVersion").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.ScheduleId = ($("#cbschedule").data("kendoDropDownList") as kendo.ui.DropDownList).value();

            // ExpectedStart Date
            lead.ExpectedStart = ad.processExpectedStart("cbprogramLabel", "dpExpectedStart", "cbExpectedStart");//this.processExpectedStart();

            lead.AttendTypeId = ($("#cbattend").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.LeadStatusId = ($("#cbLeadStatus").data("kendoDropDownList") as kendo.ui.DropDownList).value();

            // Custom Fields
            // Custom Fields
            var $inputs = $("#custom :input");
            lead.SdfList = [];
            if ($inputs != null) {
                let dType: any;
                $inputs.each(function () {
                    var sdf = new Sdf();
                    // ReSharper disable SuspiciousThisUsage
                    sdf.SdfId = $(this).prop("id");
                    dType = $(this).attr("data-dtypeid");
                    if (dType === "4") {
                        sdf.SdfValue = $(this).prop("checked");
                    } else {
                        sdf.SdfValue = $(this).val();
                    }
                    lead.SdfList.push(sdf);
                });
            }



            //var $inputs = $("#custom :input");
            //lead.SdfList = [];

            //$inputs.each(function () {
            //    var sdf = new Sdf();
            //    // ReSharper disable SuspiciousThisUsage
            //    sdf.SdfId = $(this).prop("id");
            //    sdf.SdfValue = $(this).val();
            //    // ReSharper restore SuspiciousThisUsage
            //    lead.SdfList.push(sdf);
            //});

            // Lead Groups
            lead.GroupIdList = new Array<string>();
            if (this.gruppenDb != null) {

                // Get filtered group
                // Filter groups by CampusID
                var filteredGroup = new Array<Group>();
                for (var j = 0; j < this.gruppenDb.length; j++) {
                    if (this.gruppenDb[j].CampusId === lead.CampusId) {
                        filteredGroup.push(this.gruppenDb[j]);
                    }
                }

                // Get checked group
                for (var l = 0; l < filteredGroup.length; l++) {
                    if ($("#" + filteredGroup[l].GroupId).prop("checked") === true) {

                        lead.GroupIdList.push(filteredGroup[l].GroupId);
                    }
                }
            }
            // Open progress panel
            MasterPage.Common.displayLoading(document.body, true);
            // Request the info from server
           this.db.postLeadInformation(lead, this)
                .done(msg => {
                    if (msg != undefined) {
                        $("body").css("cursor", "default");
                        // Update the last initial name only when it is saved
                        this.lastNameInitial = lead.LastName;
                        // in case of insert analyses the return info
                        if (lead.LeadId === '00000000-0000-0000-0000-000000000000') {
                            // Insertion return
                            if (msg.ValidationErrorMessages != null && msg.ValidationErrorMessages !== "") {
                                // A validation error happen
                                MasterPage.SHOW_WARNING_WINDOW(msg.ValidationErrorMessages);
                                return;
                            } else {
                                // See if a duplicate warning occurred
                                if (msg.DuplicatedMessages != null && msg.ValidationErrorMessages !== "") {
                                    // A warning duplicate error happen
                                    // Load the data-grid
                                    var dupgrid = $("#infoPageDupGrid").data("kendoGrid") as kendo.ui.Grid;
                                    var dsk = new kendo.data.DataSource({
                                        data: msg.PossiblesDuplicatesList
                                    });
                                    dupgrid.setDataSource(dsk);
                                    var dupWin = $("#WindowsDuplicate").data("kendoWindow") as kendo.ui.Window;
                                    dupWin.center().open();
                                    return;
                                }
                                $.when(MasterPage.SHOW_INFO_WINDOW_PROMISE(X_LEAD_SAVED))
                                    .then(confirmed => {
                                        sessionStorage.setItem("NewLead", "false");
                                        if (lead.CampusId !== XMASTER_GET_CURRENT_CAMPUS_ID) {

                                            // Reload the page to go to the other campus
                                            //var url1 = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                                            //    "AD/AleadInfoPage.aspx?resid=170&NewEnt=1&cmpid=" +
                                            //    lead.CampusId +
                                            //    "&desc=InfoPageQuick&mod=AD&entityType=1&entity=" + lead.LeadId;
                                            //window.location.assign(url1);

                                            let masterCampusDropDown =
                                                $("#MasterCampusDropDown")
                                                    .data("kendoDropDownList") as kendo.ui.DropDownList;
                                            masterCampusDropDown.value(lead.CampusId);
                                            masterCampusDropDown.trigger("change");
                                        } else {
                                            var url = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                                                "AD/AleadInfoPage.aspx?resid=170&NewEnt=1&cmpid=" +
                                                lead.CampusId +
                                                "&desc=InfoPageQuick&mod=AD";
                                            window.location.assign(url);
                                        }
                                    });

                                // window.location.reload(true);
                                return;
                            }
                        }

                        // Was update..................................................................................................................
                        $.when(MasterPage.SHOW_INFO_WINDOW_PROMISE(X_LEAD_SAVED))
                            .then(confirmed => {
                                // Redirect to other page (normally used in save changes in new page
                                if (redirectpage !== null && redirectpage !== undefined && redirectpage !== "") {
                                    sessionStorage.setItem(XLEAD_NEW, "true");
                                    window.location.assign(redirectpage);
                                } 

                                if (msg.ValidStatusChangeList != null) {

                                    // A new list of change of status was send
                                    // Lead Status Analysis
                                    sessionStorage.setItem("allowedLeadStatus", msg.ValidStatusChangeList);
                                    var dllnew = this
                                        .selectLeadAllowedStates(msg.StateChangeIdsList, lead.LeadStatusId);
                                    var statusControl = $("#cbLeadStatus").data("kendoDropDownList") as kendo.ui.
                                        DropDownList;
                                    statusControl.setDataSource(dllnew);
                                    statusControl.value(msg.LeadStatusId);
                                }

                                // If the campus was changed reload the page 
                                if (lead.CampusId !== XMASTER_GET_CURRENT_CAMPUS_ID) {

                                    // Reload the page to go to the other campus
                                    //var url1 = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                                    //    "AD/AleadInfoPage.aspx?resid=170&NewEnt=1&cmpid=" +
                                    //    lead.CampusId +
                                    //    "&desc=InfoPageQuick&mod=AD&entityType=1&entity=" + lead.LeadId;
                                    //window.location.assign(url1);

                                    let masterCampusDropDown =
                                        $("#MasterCampusDropDown").data("kendoDropDownList") as kendo.ui.DropDownList;
                                    masterCampusDropDown.value(lead.CampusId);
                                    masterCampusDropDown.trigger("change");
                                }


                                // Update the Lead info Bar
                                var fullname: string = lead.FirstName;
                                fullname += (lead.NickName == null || lead.NickName === "")
                                    ? " "
                                    : " (" + lead.NickName + ") ";
                                fullname += lead.LastName;
                                var expectedStart: string;
                                if (lead.ExpectedStart == null) {
                                    expectedStart = "";
                                } else {
                                    expectedStart = lead.ExpectedStart.toString();
                                }

                                LeadInfoBarVm.refreshLeadFields(
                                    fullname,
                                    $("#cbprogramVersion").data("kendoDropDownList").text(),
                                    expectedStart,
                                    $("#cbotherAdmRep").data("kendoDropDownList").text(),
                                    lead.AssignedDate.toString(),
                                    $("#cbLeadStatus").data("kendoDropDownList").text()
                                );
                            });
                    }
                })
                .always(() => {

                    // Close progress panel
                    MasterPage.Common.displayLoading(document.body, false);
                })
                .fail((msg => {
                    $("body").css("cursor", "default");
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                }));
        }
        getFinalDate(dt: Date): string {
            let sysDate = new Date();
            let monthC: string = (dt.getMonth() + 1).toString();
            let dateC: string = dt.getDate().toString();
            if (monthC.length === 1) {
                monthC = "0" + monthC;
            }
            if (dateC.length === 1) {
                dateC = "0" + dateC;
            }
            let finalDate: string = dt.getFullYear() + "-" + monthC + "-" + dateC + "T" + sysDate.getHours() + ":" + sysDate.getMinutes() + ":" + sysDate.getSeconds() + "." + sysDate.getMilliseconds();
            return finalDate;
        }
        deleteLead(leadGuid: string) {
            var firstName = $("#txtFirstName").val();
            var lastname = $("#txtLastName").val();
            if (leadGuid !== undefined && leadGuid !== null) {
                $.when(MasterPage
                    .SHOW_CONFIRMATION_WINDOW_PROMISE(X_DELETE_ARE_YOU_SURE + firstName + " " + lastname + "?"))
                    .then(confirmed => {
                        if (confirmed) {
                            // Selected delete, check if lead has fees or documents posted
                            var db = new LeadInfoPageDb();

                            db.getIsLeadErasable(leadGuid, this)
                                .done(msg => {
                                    if (msg === "false") {

                                        // You can not delete a lead with fees or documents
                                        MasterPage.SHOW_WARNING_WINDOW(X_DELETE_NOT_POSSIBLE);
                                        //alert(X_DELETE_NOT_POSSIBLE);

                                    } else {
                                        // It is erasable, delete it
                                        db.postLeadDelete(leadGuid, this)
                                            .done(() => {

                                                $.when(MasterPage.SHOW_INFO_WINDOW_PROMISE(X_DELETE_SUCESS))
                                                    .then(confirmed => {
                                                        // Go to the New lead Page...
                                                        let
                                                            redirectPage =
                                                                XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                                                                "AD/AleadInfoPage.aspx?resid=170&cmpid=" +
                                                                XMASTER_GET_CURRENT_CAMPUS_ID +
                                                                "&desc=InfoPageQuick&new=true&mod=AD";
                                                        sessionStorage.setItem(XLEAD_NEW, "true");
                                                        window.location.assign(redirectPage);
                                                    });
                                                // alert(X_DELETE_SUCESS);
                                            })
                                            .fail(msg1 => {
                                                MasterPage.SHOW_DATA_SOURCE_ERROR(msg1);
                                            });
                                    }
                                })
                                .fail(msg => {
                                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                                });
                        }
                    });
            }
        }

        refreshCampusInControl(campusId: string) {
            var lStatus = ($("#cbLeadStatus").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            var userId: string = $("#hdnUserId").val();

            this.db.getAllowedStatus(campusId, lStatus, userId, this)
                .done(msg => {
                    sessionStorage.setItem("allowedLeadStatus", msg.StateChangeIdsList);
                    this.initializeControls(campusId, lStatus);
                    // Change the cascade.... 
                    // Clean Program, Program Version and Expected Start and schedule
                    var cbprogramVersion: kendo.ui.DropDownList = $("#cbprogramVersion").data("kendoDropDownList") as any;
                    var cbschedule: kendo.ui.DropDownList = $("#cbschedule").data("kendoDropDownList") as any;
                    var cbexpectedStart: kendo.ui.DropDownList = $("#cbExpectedStart").data("kendoDropDownList" as any);
                    var cbprogramLabel: kendo.ui.DropDownList = $("#cbprogramLabel").data("kendoDropDownList" as any);

                    cbprogramLabel.setDataSource(new kendo.data.DataSource());
                    cbschedule.setDataSource(new kendo.data.DataSource());
                    cbprogramVersion.setDataSource(new kendo.data.DataSource());
                    cbexpectedStart.setDataSource(new kendo.data.DataSource());

                })
                .fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }

        refreshLeadGroups(campusId: string) {
            $("body").css("cursor", "progress");

            // Get from Server the information
            this.db.getResourcesForPage(170, campusId,XMASTER_PAGE_USER_OPTIONS_USERID, this)
                .done(msg => {
                    if (msg != undefined) {
                        // Process the Required information resource and DLL.........................
                        var res1 = msg.RequiredList;
                        if (res1 != null) {
                            //Get the check-box container
                            for (var i = 0; i < res1.length; i++) {
                                var fldName: string = res1[i].FldName;

                                for (var x = 0; x < this.captionRequirementdb.length; x++) {

                                    if (this.captionRequirementdb[x].fldName === fldName) {
                                        //this.captionRequirementdb[x].caption = res1[i].Caption;
                                        this.captionRequirementdb[x].isrequired = res1[i].Required;
                                        this.captionRequirementdb[x].dll = res1[i].Dll;
                                    }
                                }
                            }
                            let cache = JSON.stringify(this.captionRequirementdb);
                            sessionStorage.setItem(XLEAD_INFO_DLLS_REQUIRED_ITEMS, cache);
                            this.fillFilteredDatadb(campusId);
                            this.refreshCampusInControl(campusId);
                        }
                        // Process the Lead Groups if any
                        var res3 = msg.LeadGroupList;
                        if (res3 != null) {
                            this.processLeadGroupsByCampus(res3, campusId);
                            this.gruppenDb = res3;
                            sessionStorage.setItem(XLEAD_INFO_GROUPS, JSON.stringify(res3));
                        }

                        //this.initializePage(islead);
                    }
                    $("body").css("cursor", "default");
                })
                .fail(msg => {
                    $("body").css("cursor", "default");
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    return;
                });
        }

        //processExpectedStart(): Date {
        //    var expectedStart: Date;
        //    //if (XMASTER_LEAD_IS_CLOCK_HOUR === 'Yes') {
        //    //if (this.checkIsClockHr()) {
        //    if (ad.checkIsClockHr("cbprogramLabel")) {
        //        expectedStart = ($("#dpExpectedStart").data("kendoDatePicker") as kendo.ui.DatePicker).value();
        //    } else {
        //        // ExpectedStart with Term...
        //        var description = $("#cbExpectedStart").data("kendoDropDownList").text();
        //        if (description == null || description === "" || description === X_SELECT_ITEM) {
        //            expectedStart = null;
        //        } else {
        //            var descriptdate: string = description.split("]")[0].slice(1).toString().trim();
        //            expectedStart = new Date(descriptdate);
        //        }
        //    }
        //    return expectedStart;
        //}

        /* 
         * Fill the object to be used for Print Info Lead Page
         * This object is stored in session cache
         */
        fillPrintInfo(p: IPrintOutputModel): IPrintOutputModel {
            // Contacts
            p.preferredContact = this.preferredMethod;
            var bestTime = ($("#tpBestTime").data("kendoTimePicker") as kendo.ui.TimePicker).value();
            p.bestTime = (bestTime !== null && bestTime !== undefined) ? ad.FORMAT_AMPM(bestTime) : "";
            p.address = $("#txtAddress1").val() + " " + $("#txtApart").val();
            p.phoneBest = ($("#txtPhone").data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox).value();
            p.phone1 = ($("#txtPhone1").data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox).value();
            p.phone2 = ($("#txtPhone2").data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox).value();
            p.emailBest = $("#txtemailprimary").val();
            p.email = $("#txtemail").val();
            p.county = $("#cbCounty").data("kendoDropDownList").text();
            p.country = $("#cbcountry").data("kendoDropDownList").text();
            var state: string = "";
            if (($("#checkIntAddress").prop("checked"))) {
                state = $("#txtCity").val();
            } else {
                state = $("#cbState").data("kendoDropDownList").text();
                state = (state === "Select") ? "" : state;
            }
            p.cityStateZip = $("#txtCity").val() + ", " + state + ", " + $("#txtZipCode").val();

            // Academics
            p.area = $("#cbInterestArea").data("kendoDropDownList").text();
            p.program = $("#cbprogramLabel").data("kendoDropDownList").text();
            p.attend = $("#cbattend").data("kendoDropDownList").text();
            p.schedule = $("#cbschedule").data("kendoDropDownList").text();
            //var expectedGrad: Date = ($("#dtGrad").data("kendoDatePicker") as kendo.ui.DatePicker).value();
            //p.expectedGrad = (expectedGrad !== null && expectedGrad !== undefined)
            //    ? ad.FORMAT_DATE_TO_MM_DD_YYYY(expectedGrad)
            //    : "";
            p.campusName = $("#cbLeadAssign").data("kendoDropDownList").text();
            p.programVersion = $("#cbprogramVersion").data("kendoDropDownList").text();
            p.status = $("#cbLeadStatus").data("kendoDropDownList").text();
            var expectedStart = ad.processExpectedStart("cbprogramLabel", "dpExpectedStart", "cbExpectedStart"); //this.processExpectedStart();
            p.expectedStart = (expectedStart !== null && expectedStart !== undefined)
                ? ad.FORMAT_DATE_TO_MM_DD_YYYY(expectedStart)
                : "";
            //Demographic
            p.photopath = $("#infobarImage").attr("src");

            let prefix = $("#cbprefix").data("kendoDropDownList").text();
            p.leadFullName = prefix === "" || prefix === "Select" ? "" : prefix + " ";
            p.leadFullName += $("#txtFirstName").val();
            p.leadFullName += $("#txtMiddleName").val() === "" ? "" : ` ${$("#txtMiddleName").val()}`;
            p.leadFullName += " " + $("#txtLastName").val();
            let suffix: string = $("#cbsuffix").data("kendoDropDownList").text();
            p.leadFullName += suffix === "" || suffix === "Select" ? "" : ` ${suffix}`;
            let nickname = $("#tbNickname").val();
            p.leadFullName += nickname !== "" ? ` (${nickname})` : "";

            p.ssn = ($("#maskedsocialSecurity").data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox).raw().replace(/^\d{5}/, "*****");
            p.dobAge = "";
            var dob: Date = ($("#dtDob").data("kendoDatePicker") as kendo.ui.DatePicker).value();
            var age: string = $("#textage").val();
            if (dob !== null || age !== "") {
                if (dob == null) {
                    p.dobAge = " ? - " + $("#textage").val();
                } else {
                    p.dobAge = ad.FORMAT_DATE_TO_MM_DD_YYYY(dob) + " - " + $("#textage").val();
                }
            }
            p.gender = $("#cbgender").data("kendoDropDownList").text();
            p.race = $("#cbRace").data("kendoDropDownList").text();
            p.citizenship = $("#cbcitizenship").data("kendoDropDownList").text();
            p.dependency = $("#cbdependency").data("kendoDropDownList").text();
            var dependents: number = ($("#itextdependants").data("kendoNumericTextBox") as kendo.ui.NumericTextBox).value();
            p.dependents = dependents !== null && dependents !== undefined ? dependents.toString() : "";
            p.alienNumber = $("#tbaliennumber").val();
            p.maritalStatus = $("#cbmaritalstatus").data("kendoDropDownList").text();
            p.familyIncoming = $("#cbfamilyincome").data("kendoDropDownList").text();
            p.housing = $("#cbhousingtype").data("kendoDropDownList").text();
            p.driverLicState = $("#cbdriverlicensestate").data("kendoDropDownList").text();
            p.driverLicNumber = $("#tdriverlicensenumber").val();
            p.transportation = $("#cbtransportation").data("kendoDropDownList").text();
            var distanceToSchool: number = ($("#tpTimeTo").data("kendoNumericTextBox") as kendo.ui.NumericTextBox).value();
            p.distanceToSchool = distanceToSchool !== null && distanceToSchool !== undefined ? distanceToSchool.toString() + " Miles" : "";

            p.category = $("#cbcategorySource").data("kendoDropDownList").text();
            p.advertisement = $("#cbAdvertisementSource").data("kendoDropDownList").text();
            p.typeAdvertisement = $("#cbtypeSource").data("kendoDropDownList").text();
            p.note = $("#txtNoteSource").val();

            let createdate: Date = ($("#dtDateSource").data("kendoDateTimePicker") as kendo.ui.DateTimePicker).value();
            p.createdDate = (createdate !== null && createdate !== undefined)
                ? ` ${ad.FORMAT_DATE_TO_MM_DD_YYYY(createdate)} ${ad.FORMAT_AMPM(createdate)}` : "";

            // Others
            var dateApplied = ($("#dtotherDateApplied").data("kendoDatePicker") as kendo.ui.DatePicker).value();
            p.dateApplied = (dateApplied !== null && dateApplied !== undefined) ? ad.FORMAT_DATE_TO_MM_DD_YYYY(dateApplied) : "";
            p.previousEducation = $("#cbotherPreviousEducation").data("kendoDropDownList").text();
            p.sponsor = $("#cbotherSponsor").data("kendoDropDownList").text();

            var dateAssigned = ($("#dtotherAdmRepAssignDate").data("kendoDatePicker") as kendo.ui.DatePicker).value();
            p.dateAssigned = (dateAssigned !== null && dateAssigned !== undefined) ? ad.FORMAT_DATE_TO_MM_DD_YYYY(dateAssigned) : "";
            p.admissionRep = $("#cbotherAdmRep").data("kendoDropDownList").text();

            p.highSchool = $("#cbOtherHighSchool").data("kendoAutoComplete").value();
            p.adminCriteria = $("#cbotherAdmCriteria").data("kendoDropDownList").text();
            var hsGradeDate = ($("#dtHsGradDate").data("kendoDatePicker") as kendo.ui.DatePicker).value();
            p.hsGradeDate = (hsGradeDate !== null && hsGradeDate !== undefined) ? ad.FORMAT_DATE_TO_MM_DD_YYYY(hsGradeDate) : "";
            p.attendingHs = $("#ddAttendingHs").data("kendoDropDownList").text();
            p.reasonNotEnrolled = ($("#cbotherReasonNot").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            let comment: string = $("#tbOtherComment").val();
            p.comments = comment.replace(/\n/g, " ");
            p.leadGroups = " ";
            // Get filtered group
            // Filter groups by CampusID
            var filteredGroup = new Array<Group>();
            for (var j = 0; j < this.gruppenDb.length; j++) {
                if (this.gruppenDb[j].CampusId === XMASTER_GET_CURRENT_CAMPUS_ID) {
                    filteredGroup.push(this.gruppenDb[j]);
                }
            }

            // Get checked group
            for (var l = 0; l < filteredGroup.length; l++) {
                if ($("#" + filteredGroup[l].GroupId).prop("checked") === true) {
                    if (p.leadGroups.length > 3) {
                        p.leadGroups += ", ";
                    }
                    p.leadGroups += filteredGroup[l].Description;
                }
            }

            // Get user defined campus...............
            var $inputs = $("#custom .udfFields");
            p.userDf = [];

            $inputs.each(function () {
                // ReSharper disable SuspiciousThisUsage
                let label = $(this).find(".udfLabelControl");
                if (label.length > 0) {
                    var sdf = new Userdf();
                    sdf.label = label.text();
                    sdf.value = $(this).find("input").val();
                    p.userDf.push(sdf);
                }
                // ReSharper restore SuspiciousThisUsage
            });

            return p;


            //var $inputs = $("#custom :input");
            //p.userDf = [];

            //$inputs.each(function () {
            //    var sdf = new Userdf();
            //    // ReSharper disable SuspiciousThisUsage
            //    sdf.label = $(this).prop("name");
            //    sdf.value = $(this).val();
            //    // ReSharper restore SuspiciousThisUsage
            //    p.userDf.push(sdf);
            //});

            //return p;
        }

        /* Execute validation in not required fields */
        //executeCustomValidators(): boolean {

        //    // Validate special rules
        //    var error: string = "";
        //    // SSN incomplete       
        //    var ssn = $("#maskedsocialSecurity").data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox;
        //    if (ssn.value() !== "") {
        //        if (ssn.value().indexOf(ssn.options.promptChar) !== -1) {
        //            error = "SNN Number is incomplete! \n";
        //        }
        //    }

        //    var dob = $("#dtDob").val();
        //    if (dob !== null && dob !== undefined && dob !== "") {
        //        if (kendo.parseDate(dob) == null) {
        //            error += "Invalid DOB!\n";
        //        }
        //    }

        //    if (error.length > 1) {
        //        error = "The following Fields have errors:\n" + error;

        //        $.when(MasterPage.SHOW_WARNING_WINDOW_PROMISE(error))
        //            .then(confirmed => {
        //                // alert(error);
        //                return false;
        //            });
        //        return false;
        //    } else {
        //        return true;
        //    }
        //}

        storeVehicleInformation() {
            this.storeVehicles = [];
            this.storeVehicles.push($("#vehicleParkingPermit1").val());
            this.storeVehicles.push($("#vehicleMaker1").val());
            this.storeVehicles.push($("#vehicleModel1").val());
            this.storeVehicles.push($("#vehicleColor1").val());
            this.storeVehicles.push($("#vehiclePlate1").val());
            this.storeVehicles.push($("#vehicleParkingPermit2").val());
            this.storeVehicles.push($("#vehicleMaker2").val());
            this.storeVehicles.push($("#vehicleModel2").val());
            this.storeVehicles.push($("#vehicleColor2").val());
            this.storeVehicles.push($("#vehiclePlate2").val());
        }

        cancelVehicleEdition() {
            $("#vehiclePlate2").val(this.storeVehicles.pop());
            $("#vehicleColor2").val(this.storeVehicles.pop());
            $("#vehicleModel2").val(this.storeVehicles.pop());
            $("#vehicleMaker2").val(this.storeVehicles.pop());
            $("#vehicleParkingPermit2").val(this.storeVehicles.pop());
            $("#vehiclePlate1").val(this.storeVehicles.pop());
            $("#vehicleColor1").val(this.storeVehicles.pop());
            $("#vehicleModel1").val(this.storeVehicles.pop());
            $("#vehicleMaker1").val(this.storeVehicles.pop());
            $("#vehicleParkingPermit1").val(this.storeVehicles.pop());
        }
    }
}