﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />

module AD {

    // ReSharper disable InconsistentNaming

    export class LastNameHistory implements ILastNameHistory {
        Id: number;
        LastName: string;
        LeadId: string;
    }

    export class Phone implements IPhone {
        ID: string;
        Position: number;
        IsForeignPhone: boolean;
        Phone: string;
        Extension: string;
        PhoneTypeId: string;
        UserId: string;
        LeadId: string;
    }

    export class Email implements IeMail {
        ID: string;
        IsPreferred: boolean;
        Email: string;
        EmailType: string;
        EmailTypeId: string;
        IsShowOnLeadPage: string;
    }

    export class Sdf implements ISdf {
        SdfId: string;
        SdfValue: string;
        DtypeId: number;
    }

    export class Group implements IGroup {
        Description: string;
        CampusId: string;
        GroupId: string;
    }

    export class Vehicle implements IVehicle {
        Id: number;
        Position: number;
        Permit: string;
        Make: string;
        Model: string;
        Color: string;
        Plate: string;
        ModUser: string;
        ModDate: Date;
        LeadId: string;
    }

    export class LeadAddressOutputModel {
        /*
         * Gets or sets the id.
         */
        ID: string;

        /*
         * Gets or sets The Address Type Description
         */
        AddressType: string;

        /*
         * Gets or sets The Address Type Id
         */
        AddressTypeId: string;

        /*
         * Gets or sets First Part of the Address (max 250 characters)
         */
        Address1: string;

        /*
         * Gets or sets Second Part of the Address (max 250 characters)
         */
        Address2: string;

        /*
         * Gets or sets Apartment number or Unit (max 20 characters)
         */
        AddressApto: string;

        /*
         * Gets or sets City of the Address
         */
        City: string;

        /*
         * Gets or sets State Name of the Address
         */
        State: string;

        /*
         * Gets or sets The State Id
         */
        StateId: string;

        /*
         * Gets or sets the state international.
         */
        StateInternational: string;

        /*
         * Gets or sets Zip Code of the Address
         */
        ZipCode: string;

        /*
         * Gets or sets Country name of the Address
         */
        Country: string;

        /*
         * Gets or sets The Country Id
         */
        CountryId: string;

        /*
         * Gets or sets Status description
         */
        Status: string;

        /*
         * Gets or sets The Status Id
         */
        StatusId: string;

        /*
         * Gets or sets Flag indicating if this address is the mailing one
         */
        IsMailingAddress: string;

        /*
         * Gets or sets 
         *  Flag indication if this address show ID be shown on the Lead page
         */
        IsShowOnLeadPage: string;

        /*
         * Gets or sets Last Modification Date
         */
        ModDate: string;

        /*
         * Gets or sets Last User Modified this record.
         */
        Moduser: string;

        /*
         * Gets or sets The County name
         */
        County: string;

        /*
         * Gets or sets The County Unique Id
         */
        CountyId: string;

        /*
         * Gets or sets a value indicating whether is international.
         */
        IsInternational: Boolean;
    }

    class LeadInputModel implements ILeadInputModel {
        LeadId: string;
        CampusId: string;
        LeadStatusId: string;
        UserId: string;
        PageResourceId: number;
        CommandString: string;
    }

    /*
  * Output Model for Lead Demographic Information
  */
    export class LeadInfoPageOutputModel implements ILeadInfoPageOutputModel {

        constructor() {
            this.Vehicles = [];
            this.PhonesList = [];
            this.EmailList = [];
            this.LeadLastNameHistoryList = [];
            this.InputModel = new LeadInputModel();
            this.LeadAddress = new LeadAddressOutputModel();
        }

        /*
         * Gets or sets the lead id that the information is referred.
         */
        LeadId: string;

        /*
         * Gets or sets The user that does the modification.
         */
        ModUser: string;

        /*
         * Gets or sets Use to send parameters in post operations
         */
        InputModel: ILeadInputModel;

        /*
         * Gets or sets a value indicating whether avoid duplicate analysis.
         *  true is not executed.
         *  This is to permit enter a possible duplicate value for the user
         */
        AvoidDuplicateAnalysis: Boolean;

        /*
         * Gets or sets List of Id State Changes that can be possible
         *  with the actual lead status.
         */
        StateChangeIdsList: string[];

        /*
         * Gets or sets First Name
         */
        FirstName: string;

        /*
         * Gets or sets Middle Name
         */
        MiddleName: string;

        /*
         * Gets or sets Last Name
         */
        LastName: string;

        /*
         * Gets or sets Name Prefix
         */
        Prefix: string;

        /*
         * Gets or sets Name Suffix I, II, Junior
         */
        Suffix: string;

        /*
         * Gets or sets Nick Name
         */
        NickName: string;

        /*
         * Gets or sets Social Security Number
         */
        SSN: string;

        /*
         * Gets or sets Date of Birth
         */
        Dob: Date;

        /*
         * Gets or sets Citizen ship (ID)
         */
        Citizenship: string;

        /*
         * Gets or sets The USA Alien Number if any
         */
        AlienNumber: string;

        /*
         * Gets or sets If live independent or depend of other person
         */
        Dependency: string;

        /*
         * Gets or sets Marital Status Description
         */
        MaritalStatus: string;

        /*
         * Gets or sets other family that depend from the Lead.
         */
        Dependants: number;

        /*
         * Gets or sets Incoming Description
         */
        FamilyIncoming: string;

        /*
         * Gets or sets Example OffCampus, On Campus, Incarcerated ,with parent
         *  This is defined by school
         */
        HousingType: string;

        /*
         * Gets or sets State Code of driver license.
         */
        DrvLicStateCode: string;

        /*
         * Gets or sets Driver License Number
         */
        DriverLicenseNumber: string;

        /*
         * Gets or sets The transportation that the lead owner or use.
         */
        Transportation: string;

        /*
         * Gets or sets Gender
         */
        Gender: string;

        /*
         * Gets or sets Age
         */
        Age: string;

        /*
         * Gets or sets Miles from school to student house (-1 is not set)
         */
        DistanceToSchool: number;

        /*
         * Gets or sets If the lead is disabled or not.
         */
        IsDisabled: boolean;

        /*
         * Gets or sets Race GUID
         */
        RaceId: string;

        /*
         * Gets or sets History of the leads official last name changes
         */
        LeadLastNameHistoryList: ILastNameHistory[];

        /*
         * Gets or sets List of vehicles associated with the lead
         */
        Vehicles: IVehicle[];

        /*
         * Gets or sets Preferred Contact Id
         *  1:phone 2:email 3:text
         */
        PreferredContactId: number;

        /*
         * Gets or sets the lead address.
         */
        LeadAddress: ILeadAddressOutputModel;

        /*
         * Gets or sets A list of phones associated with the lead.
         */
        PhonesList: IPhone[];

        /*
         * Gets or sets List of lead emails.
         */
        EmailList: IeMail[];

        /*
         * Gets or sets Best time to contact the lead
         */
        BestTime: string;

        /*
         * Gets or sets a value indicating whether none email.
         */
        NoneEmail: boolean;

        /*
         * Gets or sets Id of the category of the source
         */
        SourceCategoryId: string;

        /*
         * Gets or sets The type of the lead source
         */
        SourceTypeId: string;

        /*
         * Gets or sets if was a advertisement, the type of it.
         */
        AdvertisementId: string;

        /**
         * Gets or sets the vendor source if the Lead was inserted by a third party vendor.
         */
        VendorSource: string;

        /*
         * Gets or sets A text referred to advertisement
         */
        Note: string;

        /*
         * Gets or sets Source Date Time. It is the moment that the lead
         *  is enter in the system.
         */
        SourceDateTime: string;

        /*
         * Gets or sets Date lead apply
         */
        DateApplied: string;

        /*
         * Gets or sets Date that is assigned to rep ad-min.
         */
        AssignedDate: string;

        /*
         * Gets or sets the admission rep id assigned
         */
        AdmissionRepId: string;

        /*
         * Gets or sets Agency Sponsor Id
         */
        AgencySponsorId: string;

        /*
         * Gets or sets Free comments about lead 100 characters null
         */
        Comments: string;

        /*
         * Gets or sets Lead previous education ID
         */
        PreviousEducationId: string;

        /*
         * Gets or sets HighSchool Id
         */
        HighSchoolId: string;

        /*
         * Gets or sets HighSchool Name
         */
        HighSchoolName: string;

        /*
         * Gets or sets Lead Graduation day from high school
         */
        HighSchoolGradDate: Date;

        /*
         * Gets or sets Is the lead attending high school?
         */
        AttendingHs: number;

        /*
         * Gets or sets Administrative criteria to accept the lead.
         */
        AdminCriteriaId: string;

        /*
         * Gets or sets Reason id because was not enrolled the student
         */
        ReasonNotEnrolled: string;

        /*
         * Gets or sets Campus for the lead.
         */
        CampusId: string;

        /*
         * Gets or sets Area of interest PrgGroupdEscription in arPrgGrp
         */
        AreaId: string;

        /*
         * Gets or sets proDescrip in ArPrograms
         */
        ProgramId: string;

        /*
         * Gets or sets PrgVerDescrip in ArPrgVersions
         */
        PrgVerId: string;

        /*
         * Gets or sets Description from ArAttendTypes
         */
        AttendTypeId: string;

        /*
         * Gets or sets Lead Status
         */
        LeadStatusId: string;

        /*
         * Gets or sets Expected start Date, 
         *  This should be transformed in a string to compare
         *  with the begin of a string in the client.
         */
        ExpectedStart: Date;

        /*
         * Gets or sets Description from ArProgSchedules
         *  The value is droved by program version
         */
        ScheduleId: string;

        /*
         * Gets or sets Information about the SDF specific to the lead.
         */
        SdfList: ISdf[];

        /*
         * Gets or sets List of identifier of group that belong to lead.
         */
        GroupIdList: string[];

        IsEnrolled: boolean;
    }
}  