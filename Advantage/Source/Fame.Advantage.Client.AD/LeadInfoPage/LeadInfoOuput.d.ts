/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
declare module AD {
    class LastNameHistory implements ILastNameHistory {
        Id: number;
        LastName: string;
        LeadId: string;
    }
    class Phone implements IPhone {
        ID: string;
        Position: number;
        IsForeignPhone: boolean;
        Phone: string;
        Extension: string;
        PhoneTypeId: string;
        UserId: string;
        LeadId: string;
    }
    class Email implements IeMail {
        ID: string;
        IsPreferred: number;
        Email: string;
        EmailType: string;
        EmailTypeId: string;
        IsShowOnLeadPage: string;
    }
    class Sdf implements ISdf {
        SdfId: string;
        SdfValue: string;
    }
    class Group implements IGroup {
        Description: string;
        CampusId: string;
        GroupId: string;
    }
    class Vehicle implements IVehicle {
        Id: number;
        Position: number;
        Permit: string;
        Make: string;
        Model: string;
        Color: string;
        Plate: string;
        ModUser: string;
        ModDate: Date;
        LeadId: string;
    }
    class LeadAddressOutputModel {
        ID: string;
        AddressType: string;
        AddressTypeId: string;
        Address1: string;
        Address2: string;
        AddressApto: string;
        City: string;
        State: string;
        StateId: string;
        StateInternational: string;
        ZipCode: string;
        Country: string;
        CountryId: string;
        Status: string;
        StatusId: string;
        IsMailingAddress: string;
        IsShowOnLeadPage: string;
        ModDate: string;
        Moduser: string;
        County: string;
        CountyId: string;
        IsInternational: Boolean;
    }
    class LeadInfoPageOutputModel implements ILeadInfoPageOutputModel {
        constructor();
        LeadId: string;
        ModUser: string;
        InputModel: ILeadInputModel;
        AvoidDuplicateAnalysis: Boolean;
        StateChangeIdsList: string[];
        FirstName: string;
        MiddleName: string;
        LastName: string;
        Prefix: string;
        Suffix: string;
        NickName: string;
        SSN: string;
        Dob: Date;
        Citizenship: string;
        AlienNumber: string;
        Dependency: string;
        MaritalStatus: string;
        Dependants: number;
        FamilyIncoming: string;
        HousingType: string;
        DrvLicStateCode: string;
        DriverLicenseNumber: string;
        Transportation: string;
        Gender: string;
        Age: string;
        DistanceToSchool: number;
        IsDisabled: boolean;
        RaceId: string;
        LeadLastNameHistoryList: ILastNameHistory[];
        Vehicles: IVehicle[];
        PreferredContactId: number;
        LeadAddress: ILeadAddressOutputModel;
        PhonesList: IPhone[];
        EmailList: IeMail[];
        BestTime: string;
        NoneEmail: boolean;
        SourceCategoryId: string;
        SourceTypeId: string;
        AdvertisementId: string;
        Note: string;
        SourceDateTime: Date;
        DateApplied: Date;
        AssignedDate: Date;
        AdmissionRepId: string;
        AgencySponsorId: string;
        Comments: string;
        PreviousEducationId: string;
        HighSchoolId: string;
        HighSchoolGradDate: Date;
        AttendingHs: number;
        AdminCriteriaId: string;
        ReasonNotEnrolled: string;
        CampusId: string;
        AreaId: string;
        ProgramId: string;
        PrgVerId: string;
        AttendTypeId: string;
        LeadStatusId: string;
        ExpectedStart: Date;
        ScheduleId: string;
        SdfList: ISdf[];
        GroupIdList: string[];
    }
}
