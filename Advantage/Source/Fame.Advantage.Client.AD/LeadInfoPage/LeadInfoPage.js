var AD;
(function (AD) {
    var LeadInfoPage = (function () {
        function LeadInfoPage() {
            var _this = this;
            try {
                this.viewModel = new AD.LeadInfoPageVm();
                var ethis = this;
                var isLead = true;
                var leadId;
                var isnew = ad.GET_QUERY_STRING_PARAMETER_BY_NAME("new");
                if (isnew === "0") {
                    sessionStorage.setItem(AD.XLEAD_NEW, "false");
                }
                var newLead = sessionStorage.getItem(AD.XLEAD_NEW);
                if (newLead != null && newLead === "true") {
                    isLead = false;
                }
                else {
                    var leadSelected = document.getElementById('leadId');
                    leadId = leadSelected.value;
                    if ((leadId == undefined || leadId == null || leadId === "")) {
                        sessionStorage.setItem(AD.XLEAD_NEW, "true");
                        window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&mod=AD");
                        return;
                    }
                }
                $("#WindowsDuplicate").kendoWindow({
                    actions: ["Close"],
                    title: "Duplicates Window",
                    width: "900px",
                    height: "650px",
                    visible: false,
                    modal: false,
                    resizable: true
                });
                $("#infoPageDupGrid").kendoGrid({
                    toolbar: ["excel", "pdf"],
                    excel: {
                        allPages: true
                    },
                    pdf: {
                        fileName: "AdvantageLeadsManagement.pdf",
                        landscape: true
                    },
                    dataSource: [{}],
                    sortable: true,
                    filterable: false,
                    resizable: true,
                    columns: [
                        { field: "Id", title: "", width: 1 },
                        { field: "FirstName", title: "First Name", attributes: { style: "font-size:11px;" } },
                        { field: "LastName", title: "Last Name", attributes: { style: "font-size:11px;" } },
                        { field: "ssn", title: "SSN", attributes: { style: "font-size:11px;" } },
                        { field: "Dob", title: "DOB", attributes: { style: "font-size:11px;" } },
                        { field: "Address1", title: "Address", attributes: { style: "font-size:11px;" } },
                        { field: "Phone", title: "Phones" },
                        { field: "Email", title: "Email" },
                        { field: "Campus", title: "Campus", attributes: { style: "font-size:11px;" } },
                        { field: "Status", title: "Status", attributes: { style: "font-size:11px;" } },
                        { field: "AdmissionRep", title: "Admission Rep", attributes: { style: "font-size:11px;" } }
                    ]
                });
                $('#cancelLead').click(function () {
                    var dupWin = $("#WindowsDuplicate").data("kendoWindow");
                    dupWin.close();
                });
                $("#windowsVehicle").kendoWindow({
                    actions: ["Close"],
                    title: "Vehicles Information",
                    width: "520px",
                    height: "290px",
                    visible: false,
                    modal: true,
                    resizable: false
                });
                $('#goMoreTransportation').click(function () {
                    var vWin = $("#windowsVehicle").data("kendoWindow");
                    _this.viewModel.storeVehicleInformation();
                    vWin.center().open();
                });
                $('#vehicleCancel').click(function () {
                    _this.viewModel.cancelVehicleEdition();
                    var win = $("#windowsVehicle").data("kendoWindow");
                    win.close();
                });
                $('#vehicleSave').click(function () {
                    var win = $("#windowsVehicle").data("kendoWindow");
                    win.close();
                });
                this.initializeKendoComponents();
                this.viewModel.getResourcesForPage(isLead);
                $('#insertLead').click(function () {
                    var dupWin = $("#WindowsDuplicate").data("kendoWindow");
                    dupWin.close();
                    var customValidation = _this.viewModel.executeCustomValidators();
                    if (customValidation === false) {
                        return;
                    }
                    var validator = $("#containerInfoPage").kendoValidator().data("kendoValidator");
                    if (validator.validate()) {
                        _this.viewModel.postLeadInformationToServer(leadId, true);
                    }
                    else {
                        alert(AD.X_SAVE_REVIEW_INFO_IN_FORM);
                    }
                });
                $("#leadPrintButton").click(function () {
                    var userId = $("#hdnUserId").val();
                    var printInfo = new AD.PrintOutputModel();
                    printInfo = _this.viewModel.fillPrintInfo(printInfo);
                    sessionStorage.removeItem("PRINT_INFO");
                    sessionStorage.setItem("PRINT_INFO", JSON.stringify(printInfo));
                    sessionStorage.setItem("USER_ID", userId);
                    var url = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL
                        + "AD/ALeadPrint.aspx?RESID=206&cmpId="
                        + XMASTER_GET_CURRENT_CAMPUS_ID
                        + "&desc=PrintProfile&mod=AD"
                        + "&leadId=" + leadId;
                    window.open(url, "_newtab");
                });
                $('#leadToolBarNew').click(function () {
                    var redirectPage = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&new=true&mod=AD";
                    sessionStorage.setItem(AD.XLEAD_NEW, "false");
                    var save = confirm("Save Changes?");
                    if (save) {
                        var customValidation = _this.viewModel.executeCustomValidators();
                        if (customValidation === false) {
                            return;
                        }
                        var validator = $("#containerInfoPage").kendoValidator().data("kendoValidator");
                        if (validator.validate()) {
                            _this.viewModel.postLeadInformationToServer(leadId, false, redirectPage);
                            return;
                        }
                        else {
                            alert(AD.X_SAVE_PLEASE_FILL_REQUIRED_FIELDS);
                        }
                    }
                    sessionStorage.setItem(AD.XLEAD_NEW, "true");
                    window.location.assign(redirectPage);
                });
                $('#leadToolBarSave').click(function () {
                    var customValidation = _this.viewModel.executeCustomValidators();
                    if (customValidation === false) {
                        return;
                    }
                    var validator = $("#containerInfoPage").kendoValidator().data("kendoValidator");
                    if (validator.validate()) {
                        _this.viewModel.postLeadInformationToServer(leadId, false);
                    }
                    else {
                        alert(AD.X_SAVE_PLEASE_FILL_REQUIRED_FIELDS);
                    }
                });
                $('#leadToolBarDelete').click(function () {
                    _this.viewModel.deleteLead(leadId);
                });
                $("#preferredEmail")
                    .click(function () {
                    ethis.viewModel.preferredMethod = "Email";
                });
                $("#preferredPhone")
                    .click(function () {
                    ethis.viewModel.preferredMethod = "Phone";
                });
                $("#preferredText")
                    .click(function () {
                    ethis.viewModel.preferredMethod = "Text";
                });
                var container = $("#containerInfoPage");
                kendo.init(container);
                container.kendoValidator({
                    validateOnBlur: true,
                    rules: {
                        validSsn: function (input) {
                            if (input.is("[name=maskedsocialSecurity]") && input.val() !== "") {
                                var ssnTxt = input.data("kendoMaskedTextBox");
                                return ssnTxt.value().indexOf(ssnTxt.options.promptChar) === -1;
                            }
                            else {
                                return true;
                            }
                        },
                        validDob: function (input) {
                            if (input.is("[name=dtDob]") && input.val() !== "") {
                                var currentDate = kendo.parseDate($(input).val());
                                if (!currentDate) {
                                    return false;
                                }
                            }
                            return true;
                        }
                    },
                    messages: {
                        validDob: "Invalid Date!"
                    }
                });
                kendo.bind($("#containerInfoPage"), ethis.viewModel);
            }
            catch (e) {
                $("body").css("cursor", "default");
                alert(e.message + "/n" + e.stack);
                sessionStorage.removeItem(AD.XLEAD_INFO_DLLS_REQUIRED_ITEMS);
                sessionStorage.removeItem(AD.XLEAD_INFO_GROUPS);
                sessionStorage.removeItem(AD.XLEAD_INFO_SDF_FIELDS);
            }
        }
        LeadInfoPage.prototype.initializeKendoComponents = function () {
            var _this = this;
            $("#cbPrevlast").kendoDropDownList({
                optionLabel: "",
                dataTextField: "LastName",
                dataValueField: "Id",
                change: function (e) {
                    e.preventDefault();
                    e.sender.select(0);
                }
            });
            $("#cbRace").kendoDropDownList({
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#dtDob").kendoDatePicker({
                change: function () {
                    var dob = $("#dtDob").data("kendoDatePicker").value();
                    var age = $("#textage").val();
                    _this.viewModel.calculateAge(dob, age);
                }
            });
            $("#itextdependants").kendoNumericTextBox({
                decimals: 0,
                format: "n0",
                step: 1,
                min: 0,
                max: 99
            });
            $("#tpTimeTo").kendoNumericTextBox({
                decimals: 0,
                format: "n0",
                step: 1,
                min: 0,
                max: 999
            });
            $("#cbprefix").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbsuffix").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbgender").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbmaritalstatus").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbdriverlicensestate").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbcitizenship").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbdependency").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbfamilyincome").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbhousingtype").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbtransportation").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbState").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbcategorySource").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbtypeSource").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbAdvertisementSource").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#dtDateSource").kendoDateTimePicker({});
            $("#dtotherDateApplied").kendoDatePicker();
            $("#cbotherPreviousEducation").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#dtotherAdmRepAssignDate").kendoDatePicker();
            $("#cbOtherHighSchool").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbotherAdmRep").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#dtHsGradDate").kendoDatePicker();
            $("#cbotherSponsor").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#ddAttendingHs").kendoDropDownList({
                dataSource: [
                    { text: "Select", value: "-1" },
                    { text: "Yes", value: "1" },
                    { text: "No", value: "0" }
                ],
                dataTextField: "text",
                dataValueField: "value"
            });
            $("#cbotherReasonNot").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbotherAdmCriteria").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#tpBestTime").kendoTimePicker({
                interval: 15,
                min: new Date(2000, 0, 1, 6, 0, 0),
                format: "hh:mm tt",
                parseFormats: ["H:mm"]
            });
            $("#tpBestTime").closest("span.k-datepicker").width(300);
            $("#txtPhone").kendoMaskedTextBox({
                mask: "(000)-000-0000",
                clearPromptChar: true
            });
            $("#txtPhone1").kendoMaskedTextBox({
                mask: "(000)-000-0000",
                clearPromptChar: true
            });
            $("#txtPhone2").kendoMaskedTextBox({
                mask: "(000)-000-0000",
                clearPromptChar: true
            });
            $('#checkphone').change(function () {
                var maskedtextbox = $("#txtPhone").data("kendoMaskedTextBox");
                var raw = maskedtextbox.raw();
                if ($(this).is(":checked")) {
                    maskedtextbox.setOptions({ mask: '+9999999999999999999999999' });
                }
                else {
                    maskedtextbox.setOptions({ mask: '(000)-000-0000' });
                }
                maskedtextbox.value(raw);
            });
            $('#checkphone1').change(function () {
                var maskedtextbox = $("#txtPhone1").data("kendoMaskedTextBox");
                var raw = maskedtextbox.raw();
                if ($(this).is(":checked")) {
                    maskedtextbox.setOptions({ mask: '+9999999999999999999999999' });
                }
                else {
                    maskedtextbox.setOptions({ mask: '(000)-000-0000' });
                }
                maskedtextbox.value(raw);
            });
            $('#checkphone2').change(function () {
                var maskedtextbox = $("#txtPhone2").data("kendoMaskedTextBox");
                var raw = maskedtextbox.raw();
                if ($(this).is(":checked")) {
                    maskedtextbox.setOptions({ mask: '+9999999999999999999999999' });
                }
                else {
                    maskedtextbox.setOptions({ mask: '(000)-000-0000' });
                }
                maskedtextbox.value(raw);
            });
            $("#cbphone").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbphone1").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbphone2").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbcountry").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbCounty").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbAddress").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#txtZipCode").kendoMaskedTextBox({
                mask: "00000",
                clearPromptChar: true
            });
            $('#checkIntAddress').change(function () {
                var cbState = $("#cbState").data("kendoDropDownList");
                var otherState = $("#otherStates");
                var zipcode = $("#txtZipCode").data("kendoMaskedTextBox");
                if ($(this).is(":checked")) {
                    $("#checkIntAddress").prop('checked', true);
                    cbState.wrapper.hide();
                    otherState.css("display", "block");
                    zipcode.setOptions({ mask: '9999999999' });
                }
                else {
                    $("#checkIntAddress").prop('checked', false);
                    cbState.wrapper.show();
                    otherState.css("display", "none");
                    zipcode.setOptions({ mask: '00000' });
                }
            });
            $("#cbemailprimary").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbemail").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $('#checkNoEmail').change(function () {
                var disabled = ($(this).is(":checked"));
                $("#txtemailprimary").prop("disabled", disabled);
                $("#txtemail").prop("disabled", disabled);
                if (disabled) {
                    $("#txtemail").css("color", "white");
                    $("#txtemailprimary").css("color", "white");
                }
                else {
                    $("#txtemail").css("color", "inherit");
                    $("#txtemailprimary").css("color", "inherit");
                }
            });
            $("#gocontactInformation").kendoButton({
                click: this.viewModel.onClickContactInformation
            });
            var campus = $("#cbLeadAssign").kendoDropDownList({
                dataSource: [],
                dataTextField: "Description",
                dataValueField: "ID",
                change: function () {
                    var campusId = $("#cbLeadAssign").data("kendoDropDownList").value();
                    var programgrp = $("#cbInterestArea").data("kendoDropDownList");
                    programgrp.setDataSource(new kendo.data.DataSource());
                    _this.viewModel.fillFilteredDatadb(campusId);
                    _this.viewModel.refreshCampusInControl(campusId);
                }
            }).data("kendoDropDownList");
            campus.list.width("auto");
            campus.list.css("white-space", "nowrap");
            $("#cbInterestArea").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID",
                change: function () {
                    var campusId = $("#cbLeadAssign").data("kendoDropDownList").value();
                    var cbprogramVersion = $("#cbprogramVersion").data("kendoDropDownList");
                    var cbschedule = $("#cbschedule").data("kendoDropDownList");
                    var cbexpectedStart = $("#cbExpectedStart").data("kendoDropDownList");
                    cbschedule.setDataSource(new kendo.data.DataSource());
                    cbprogramVersion.setDataSource(new kendo.data.DataSource());
                    cbexpectedStart.setDataSource(new kendo.data.DataSource());
                    var selectedValue = $("#cbInterestArea").data("kendoDropDownList").value();
                    if (selectedValue !== "") {
                        _this.viewModel.getItemInformation(campusId, "ProgramID", "cbprogramLabel", selectedValue);
                    }
                    else {
                        var cbprogramLabel = $("#cbprogramLabel").data("kendoDropDownList");
                        cbprogramLabel.setDataSource(new kendo.data.DataSource());
                    }
                }
            });
            var dia = $("#cbInterestArea").data("kendoDropDownList");
            dia.list.width("auto");
            dia.list.css("white-space", "nowrap");
            $("#cbprogramLabel").kendoDropDownList({
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID",
                dataSource: [],
                change: function () {
                    var campusId = $("#cbLeadAssign").data("kendoDropDownList").value();
                    var cbprogramVersion = $("#cbprogramVersion").data("kendoDropDownList");
                    var cbschedule = $("#cbschedule").data("kendoDropDownList");
                    var cbexpectedStart = $("#cbExpectedStart").data("kendoDropDownList");
                    cbschedule.setDataSource(new kendo.data.DataSource());
                    cbprogramVersion.setDataSource(new kendo.data.DataSource());
                    cbexpectedStart.setDataSource(new kendo.data.DataSource());
                    var selectedValue = $("#cbprogramLabel").data("kendoDropDownList").value();
                    if (selectedValue !== "") {
                        _this.viewModel.getItemInformation(campusId, "PrgVerId", "cbprogramVersion", selectedValue);
                        _this.viewModel.getExpectedLeadItemInformation(campusId, "ExpectedStart", "cbExpectedStart", selectedValue);
                    }
                }
            });
            var dpl = $("#cbprogramLabel").data("kendoDropDownList");
            dpl.list.width("auto");
            dpl.list.css("white-space", "nowrap");
            $("#cbprogramVersion").kendoDropDownList({
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID",
                dataSource: [],
                change: function () {
                    var campusId = $("#cbLeadAssign").data("kendoDropDownList").value();
                    var cbprogramVersion = $("#cbprogramVersion").data("kendoDropDownList");
                    var selectedValue = cbprogramVersion.value();
                    if (selectedValue !== "") {
                        _this.viewModel.getItemInformation(campusId, "ProgramScheduleId", "cbschedule", selectedValue);
                    }
                }
            });
            var ddl = $("#cbprogramVersion").data("kendoDropDownList");
            ddl.list.width("auto");
            ddl.list.css("white-space", "nowrap");
            $("#cbschedule").kendoDropDownList({
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID",
                dataSource: []
            });
            if (XMASTER_LEAD_IS_CLOCK_HOUR === 'Yes') {
                $("#dpExpectedStart").css("display", "inline");
                $("#dpExpectedStart").kendoDatePicker();
            }
            else {
                $("#cbExpectedStart").kendoDropDownList({
                    dataSource: [],
                    optionLabel: "Select",
                    dataTextField: "Description",
                    dataValueField: "ID"
                });
                var dea = $("#cbExpectedStart").data("kendoDropDownList");
                dea.list.width("auto");
                dea.list.css("white-space", "nowrap");
            }
            $("#cbattend").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbLeadStatus").kendoDropDownList({
                dataSource: [],
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#dtGrad").kendoDatePicker();
        };
        return LeadInfoPage;
    }());
    AD.LeadInfoPage = LeadInfoPage;
})(AD || (AD = {}));
//# sourceMappingURL=LeadInfoPage.js.map