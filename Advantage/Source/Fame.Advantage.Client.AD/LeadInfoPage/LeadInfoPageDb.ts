﻿/// <reference path="../ServiceUrls.ts" />
module AD {

    export class LeadInfoPageDb {
        
        public getResourcesForPage(pageResourceId: number, campusId: string, userId: string, mcontext: any) {

            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_INFOPAGE_RESOURCES_GET + "?PageResourceId=" + pageResourceId + "&CampusId=" + campusId + "&UserId=" + userId,
                type: 'GET',
                timeout: 25000,
                dataType: 'json'
            });
        }

        getCatalogsInfoFromServer(fldName: string, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_CATALOGS_COMMAND_GET + "?fldName=" + fldName,
                type: 'GET',
                timeout: 25000,
                dataType: 'json'
            });
        }

        getLeadDemographic(leadGuid: Object, userId: string, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_LEAD_DEMOGRAPHIC_COMMAND_GET + "?LeadId=" + leadGuid + "&UserId=" + userId,
                type: 'GET',
                timeout: 25000,
                dataType: 'json'
            });

        }

        getLeadItemInformation(codeItemToGet: string, value: any, campusId: string, mcontext: any) {

            if (value != null) {
                value = value.toLocaleString();
            }

            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_CATALOGS_COMMAND_GET + "?FldName=" + codeItemToGet + "&CampusID=" + campusId + "&AdditionalFilter=" + value,
                type: 'GET',
                timeout: 25000,
                dataType: 'json'
            });
        }

        getVehiclesFromServer(leadId: string, userId: string, mcontext: LeadInfoPageVm) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_LEAD_DEMOGRAPHIC_COMMAND_GET + "?LeadId=" + leadId + "&UserId=" + userId,
                type: 'GET',
                timeout: 25000,
                dataType: 'json'
            });
        }

        /* Post the lead information to server*/
        postLeadInformation(lead: ILeadInfoPageOutputModel, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XPOST_SERVICE_LAYER_LEAD_INFORMATION,
                type: 'POST',
                dataType: 'json',
                timeout: 25000,
                data: JSON.stringify(lead)
            });
        }

        postLeadDelete(leadGuid: string, mcontext: any) {

            return $.ajax({
                context: mcontext,
                url: XPOST_SERVICE_LAYER_LEAD_DELETE_COMMAND + "?LeadId=" + leadGuid,
                type: 'POST',
                timeout: 25000,
                dataType: 'text'
            });
        }

        getIsLeadErasable(leadGuid: string, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_LEAD_IS_ERASABLE_COMMAND + "?LeadId=" + leadGuid,
                type: 'GET',
                timeout: 25000,
                dataType: 'text'
            });
        }


        getAllowedStatus(campusId: string, lStatus, userId:string, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_LEAD_DEMOGRAPHIC_COMMAND_GET + "?CampusId=" + campusId +
                                                                        "&LeadStatusId=" + lStatus +
                                                                        "&UserId=" + userId +
                                                                        "&CommandString=" + "ALLOWED_STATUS",
                type: "GET",
                timeout: 25000,
                dataType: "json"
            });
        }

        public getQuickLeadDropDownValues(ctrlName: string, fldName: string, campusId: string, mcontext: any) {
            var model = {
                CtrlName: ctrlName,
                Input: {
                    FldName: fldName,
                    CampusId: campusId
                },
            };
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_DDL_SOURCE_GET + "?filter=" + JSON.stringify(model),
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }
 
        // Fixed table with the translation information from fldName to name control
        public staticInformationForCaptionRequirement(): ICaptionRequirement[] {

            return [
                //   { fldName: "ShiftId", labelName: "", controlName: "", isDll: false }
                { fldName: "Age", labelName: "age", controlName: "textage", isDll: false },
                { fldName: "AlienNumber", labelName: "aliennumber", controlName: "tbaliennumber", isDll: false },
                { fldName: "BirthDate", labelName: "dob", controlName: "dtdob", isDll: false },
                { fldName: "Citizen", labelName: "citizenship", controlName: "cbcitizenship", isDll: false },
                { fldName: "FirstName", labelName: "firstName", controlName: "txtFirstName", isDll: false },
                { fldName: "Children", labelName: "dependants", controlName: "itextdependants", isDll: false },
                { fldName: "DependencyTypeId", labelName: "dependency", controlName: "cbdependency", isDll: true, dll: null },
                { fldName: "DrivLicNumber", labelName: "driverlicensenumber", controlName: "tdriverlicensenumber", isDll: false },
                { fldName: "DrivLicStateID", labelName: "driverlicensestate", controlName: "", isDll: false },
                { fldName: "StateId", labelName: "", controlName: "cbdriverlicensestate", isDll: true, dll: [] },
                { fldName: "Race", labelName: "lblRace", controlName: "cbRace", isDll: true, dll: [] },


                { fldName: "FamilyIncome", labelName: "familyincome", controlName: "cbfamilyincome", isDll: true, dll: [] },
                { fldName: "Gender", labelName: "gender", controlName: "cbgender", isDll: true, dll: [] },
                { fldName: "HousingId", labelName: "housingtype", controlName: "cbhousingtype", isDll: true, dll: null },
                { fldName: "LastName", labelName: "lastName", controlName: "txtLastName", isDll: false },
                { fldName: "MaritalStatus", labelName: "maritalstatus", controlName: "cbmaritalstatus", isDll: true, dll: null },
                { fldName: "MiddleName", labelName: "middleName", controlName: "txtMiddleName", isDll: false },
                { fldName: "NickName", labelName: "nickname", controlName: "tbNickName", isDll: false },
                { fldName: "Prefix", labelName: "prefix", controlName: "cbprefix", isDll: true, dll: null },
                { fldName: "SSN", labelName: "socialSecurity", controlName: "txtsocialSecurity", isDll: false },
                { fldName: "Suffix", labelName: "suffix", controlName: "cbsuffix", isDll: true, dll: null },
                { fldName: "TransportationId", labelName: "transportation", controlName: "cbtransportation", isDll: true, dll: [] },
                { fldName: "TimeToSchool", labelName: "timeTo", controlName: "tpTimeTo", isDll: false },
 
                // .................................................................................................................   

                //Academics ........................................................................................................                        
                { fldName: "AreaId", labelName: "interestArea", controlName: "cbInterestArea", isDll: true, dll: [] },
                { fldName: "AttendTypeId", labelName: "attend", controlName: "cbattend", isDll: true, dll: [] },
                { fldName: "LeadAssignedToId", labelName: "leadAssign", controlName: "cbLeadAssign", isDll: true, dll: [] },
                { fldName: "ExpectedStart", labelName: "expectedStart", controlName: "cbexpectedStart", isDll: false, dll: [] },
                { fldName: "grad", labelName: "grad", controlName: "dpgrad", isDll: false, dll: null },
                { fldName: "LeadStatus", labelName: "leadStatus", controlName: "cbLeadStatus", isDll: true, dll: [] },
                { fldName: "PrgVerId", labelName: "programVersion", controlName: "cbprogramVersion", isDll: false, dll: [] },
                { fldName: "ProgramID", labelName: "programLabel", controlName: "cbprogramLabel", isDll: false, dll: [] },
                { fldName: "ProgramScheduleId", labelName: "schedule", controlName: "cbschedule", isDll: true, dll: [] },
                // .................................................................................................................   
                // Source  .........................................................................................................

                { fldName: "CreatedDate", labelName: "DateSource", controlName: "dtDateSource", isDll: false, dll: null },
                { fldName: "AdvertisementNote", labelName: "noteSource", controlName: "txtNoteSource", isDll: false },
                { fldName: "SourceCategoryID", labelName: "categorySource", controlName: "cbcategorySource", isDll: true, dll: [] },
                { fldName: "SourceAdvertisement", labelName: "advertisementSource", controlName: "cbAdvertisementSource", isDll: true, dll: [] },
                { fldName: "VendorSource", labelName: "vendorSource", controlName: "txtVendorSource", isDll: false },
                { fldName: "SourceTypeID", labelName: "typeSource", controlName: "cbtypeSource", isDll: true, dll: [] },
                // .................................................................................................................   
                // Others  .........................................................................................................
                { fldName: "DateApplied", labelName: "otherDateApplied", controlName: "dtotherDateApplied", isDll: false },
                { fldName: "AssignedDate", labelName: "otherAdmRepAssignDate", controlName: "dtotherAdmRepAssignDate", isDll: false },
                { fldName: "AdmissionsRep", labelName: "otherAdmRep", controlName: "cbotherAdmRep", isDll: true, dll: [] },
                { fldName: "Sponsor", labelName: "otherSponsor", controlName: "cbotherSponsor", isDll: true, dll: [] },
                { fldName: "Comments", labelName: "otherComment", controlName: "tbOtherComment", isDll: false },
                { fldName: "PreviousEducation", labelName: "otherPreviousEducation", controlName: "cbotherPreviousEducation", isDll: true, dll: [] },
                { fldName: "HighSchool", labelName: "otherHighSchool", controlName: "cbOtherHighSchool", isDll: true, dll: [] },
                { fldName: "HighSchoolGradDate", labelName: "HsGradDate", controlName: "dtHsGradDate", isDll: false },
                { fldName: "AttendingHs", labelName: "AttendingHs", controlName: "ddAttendingHs", isDll: false },
                { fldName: "admincriteriaid", labelName: "otherAdmCriteria", controlName: "cbotherAdmCriteria", isDll: true, dll: [] },

                { fldName: "Description", labelName: "otherReasonNot", controlName: "cbotherReasonNot", isDll: true, dll: [] },
                //{ fldName: "IsDisabled", labelName: "lblDisabled", controlName: "cbDisabled", isDll: false },


                // ..................................................................................................................   
                // Contact Information ..............................................................................................
                { fldName: "PreferredContactId", labelName: "preferredContact", controlName: "", isDll: false },
                { fldName: "PhoneType", labelName: "", controlName: "cbphone", isDll: true, dll: null },
                { fldName: "PhoneType", labelName: "", controlName: "cbphone1", isDll: true, dll: null },
                { fldName: "PhoneType", labelName: "", controlName: "cbphone2", isDll: true, dll: null },

                { fldName: "Address1", labelName: "address1", controlName: "txtAddress1", isDll: false },
                { fldName: "Address2", labelName: "address2", controlName: "txtAddress2", isDll: false },
                { fldName: "City", labelName: "city", controlName: "txtCity", isDll: false },
                { fldName: "Country", labelName: "country", controlName: "cbcountry", isDll: true, dll: [] },
                { fldName: "County", labelName: "county", controlName: "cbCounty", isDll: true, dll: [] },
                { fldName: "StateId", labelName: "", controlName: "cbState", isDll: true, dll: null },
                { fldName: "Zip", labelName: "", controlName: "txtZipCode", isDll: false },
                { fldName: "AddressType", labelName: "", controlName: "cbAddress", isDll: true, dll: [] },
                { fldName: "IsForeignPhone", labelName: "txtcheckphone", controlName: "checkphone", isDll: false },
                { fldName: "IsForeignPhone", labelName: "txtcheckphone1", controlName: "checkphone1", isDll: false },
                { fldName: "IsForeignPhone", labelName: "txtcheckphone2", controlName: "checkphone2", isDll: false },
                { fldName: "Phone", labelName: "phone", controlName: "txtphone", isDll: false },
                { fldName: "Phone2", labelName: "phone1", controlName: "txtphone1", isDll: false },
                { fldName: "Phone2", labelName: "phone2", controlName: "txtphone2", isDll: false },
                { fldName: "EmailTypeId", labelName: "", controlName: "cbemailprimary", isDll: true, dll: [] },
                { fldName: "EmailTypeId", labelName: "", controlName: "cbemail", isDll: true, dll: [] },
                { fldName: "HomeEmail", labelName: "emailprimary", controlName: "txtemailprimary", isDll: false },
                { fldName: "WorkEmail", labelName: "email", controlName: "txtemail", isDll: false },
                { fldName: "BestTimeId", labelName: "besttime", controlName: "cbBestTime", isDll: false, dll: [] },
                { fldName: "NoneEmail", labelName: "noEmail", controlName: "checkNoEmail", isDll: false },

                { fldName: "ShiftId", labelName: "", controlName: "", isDll: false }
            ];
            // ..................................................................................................................   

        }

 
    }
} 