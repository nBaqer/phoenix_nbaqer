var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ad;
(function (ad) {
    function GET_QUERY_STRING_PARAMETER_BY_NAME(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    ad.GET_QUERY_STRING_PARAMETER_BY_NAME = GET_QUERY_STRING_PARAMETER_BY_NAME;
    function GET_AGE(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
    ad.GET_AGE = GET_AGE;
    function OPEN_TASK_RAD_EMAIL_WINDOW_WITH_ENTITY(entityId, mod) {
        var oWnd = $find('radPop');
        oWnd.setSize(800, 780);
        oWnd.setUrl(XMASTER_GET_BASE_URL + '/defaultTMAddNew.html?addnew=true&entityId=' + entityId + "&mod=" + mod.toString());
        oWnd.show();
    }
    ad.OPEN_TASK_RAD_EMAIL_WINDOW_WITH_ENTITY = OPEN_TASK_RAD_EMAIL_WINDOW_WITH_ENTITY;
    function OPEN_TASK_RAD_TASK_WINDOW_WITH_ENTITY(entityId, mod) {
        var oWnd = $find('radPop');
        oWnd.setSize(800, 780);
        oWnd.setUrl(XMASTER_GET_BASE_URL + '/defaultTM.html?addnew=true&entityId=' + entityId + "&mod=" + mod.toString());
        oWnd.show();
    }
    ad.OPEN_TASK_RAD_TASK_WINDOW_WITH_ENTITY = OPEN_TASK_RAD_TASK_WINDOW_WITH_ENTITY;
    function OPEN_TASK_RAD_SCHEDULE_WINDOW_WITH_ENTITY(entityId, mod) {
        var oWnd = $find('radPop');
        oWnd.setSize(800, 780);
        oWnd.setUrl(XMASTER_GET_BASE_URL + '/defaultTMSchedule.html?addnew=true&entityId=' + entityId + "&mod=" + mod.toString());
        oWnd.show();
    }
    ad.OPEN_TASK_RAD_SCHEDULE_WINDOW_WITH_ENTITY = OPEN_TASK_RAD_SCHEDULE_WINDOW_WITH_ENTITY;
    function SHOW_DATA_SOURCE_ERROR(e) {
        try {
            if (e.xhr != undefined) {
                if (showSessionFinished(e.xhr.statusText)) {
                    return "";
                }
                var display = "";
                if (e.xhr.statusText !== "OK") {
                    display = "Error: " + e.xhr.statusText + ", ";
                }
                display += (e.xhr.responseText == undefined) ? e.xhr.responseXML : e.xhr.responseText;
                alert(display);
            }
            else {
                var display = "";
                if (e.statusText !== "OK") {
                    display = "Error: " + e.statusText + ", ";
                }
                display += (e.responseText == undefined) ? e.responseXML : e.responseText;
                alert(display);
            }
        }
        catch (ex) {
            alert("Server returned an undefined error");
        }
    }
    ad.SHOW_DATA_SOURCE_ERROR = SHOW_DATA_SOURCE_ERROR;
    function showSessionFinished(statusText) {
        if (statusText === "OK") {
            alert("Session expired");
            return true;
        }
        return false;
    }
    function FORMAT_DATE_TO_MM_DD_YYYY(d) {
        var currDate = d.getDate().toString();
        if (currDate.length < 2) {
            currDate = "0" + currDate;
        }
        var currMonth = (d.getMonth() + 1).toString();
        if (currMonth.length < 2) {
            currMonth = "0" + currMonth;
        }
        var currYear = d.getFullYear();
        return (currMonth + "/" + currDate + "/" + currYear);
    }
    ad.FORMAT_DATE_TO_MM_DD_YYYY = FORMAT_DATE_TO_MM_DD_YYYY;
})(ad || (ad = {}));
$.fn.bindUp = function (type, fn) {
    type = type.split(/\s+/);
    this.each(function () {
        var len = type.length;
        while (len--) {
            $(this).bind(type[len], fn);
            var evt = $.data(this, 'events')[type[len]];
            evt.splice(0, 0, evt.pop());
        }
    });
};
var AD;
(function (AD) {
    var LeadInfoBar = (function () {
        function LeadInfoBar() {
            try {
                this.viewModel = new AD.LeadInfoBarVm();
                var ethis = this;
                var leadGuid = document.getElementById('leadId');
                var temp = sessionStorage.getItem("NewLead");
                if (temp != null && temp === "true") {
                    $("#kendoleadBarControl").hide();
                }
                else {
                    if (leadGuid.value !== "") {
                        $("#kendoleadBarControl").show();
                        this.viewModel.getLeadInfoFromServer(leadGuid.value);
                    }
                }
                document.getElementById("infobarToLeadInfoPage").addEventListener("click", this.viewModel.goToLeadInfoScreen);
                kendo.init($("#kendoleadBarControl"));
                kendo.bind($("#kendoleadBarControl"), ethis.viewModel);
            }
            catch (e) {
                alert(e.message + "/n" + e.stack);
            }
        }
        return LeadInfoBar;
    })();
    AD.LeadInfoBar = LeadInfoBar;
})(AD || (AD = {}));
var AD;
(function (AD) {
    AD.XGET_SERVICE_LAYER_LEAD_DUPLICATES_GET = "../proxy/api/Lead/LeadDuplicates/Get";
    AD.XGET_SERVICE_LAYER_LEAD_DUPLICATES_PAIR_GET = "../proxy/api/Lead/LeadDuplicates/GetDuplicate";
    AD.XGET_SERVICE_LAYER_LEAD_DUPLICATES_COMMAND_GET = "../proxy/api/Lead/LeadDuplicates/ButtomCommandDuplicate";
    AD.XGET_SERVICE_LAYER_LEAD_INFOBAR_COMMAND_GET = "../proxy/api/Lead/Leads/GetLeadInfoBar";
    AD.XIMAGE_INFO = "../../images/info.jpg";
    AD.XGET_SERVICE_LAYER_LEAD_REQUIREMENT_STATUS_GET = "../proxy/api/Lead/Leads/GetLeadRequirementStatus";
    AD.XGET_SERVICE_LAYER_LEAD_GROUPS_COMMAND_GET = "../proxy/api/Lead/Leads/GetLeadGroupInformation";
    AD.XGET_SERVICE_LAYER_SCHOOL_DEFINED_FIELDS_COMMAND_GET = "../proxy/api/Lead/Leads/GetLeadSchoolCustomFields";
    AD.XGET_SERVICE_LAYER_INFOPAGE_RESOURCES_GET = "../proxy/api/Lead/Leads/GetInfoPageResources";
    AD.XGET_SERVICE_LAYER_CATALOGS_COMMAND_GET = "../proxy/api/Catalogs/Catalogs/Get";
    AD.XGET_SERVICE_LAYER_LEAD_DEMOGRAPHIC_COMMAND_GET = "../proxy/api/Lead/Leads/GetLeadInfoDemographicFields";
    AD.XGET_SERVICE_LAYER_CONFIGURATION_SETTING_GET = "../proxy/api/SystemStuff/ConfigurationAppSettings/Get";
    AD.XPOST_SERVICE_LAYER_LEAD_INFORMATION = "../proxy/api/Lead/Leads/PostInfoPageLeadFields";
    AD.XGET_SERVICE_LAYER_LEAD_IS_ERASABLE_COMMAND = "../proxy/api/Lead/Leads/IsLeadErasable";
    AD.XPOST_SERVICE_LAYER_LEAD_DELETE_COMMAND = "../proxy/api/Lead/Leads/PostLeadDelete";
    AD.XGET_SERVICE_LAYER_QUEUE_COMMAND_GET = "../proxy/api/Lead/LeadQueue/Get";
    AD.XPOST_SERVICE_LAYER_SET_MRU = "../proxy/api/SystemStuff/LeadStuffRouter/Mru/Post";
    AD.XPOST_PHONE_SERVICE_LAYER_SET_MRU = "../proxy/api/Lead/LeadQueue/Post";
    AD.XGET_SERVICE_LAYER_TOOLBAR_FLAGS = "../proxy/api/Lead/LeadToolBar/Get";
    AD.XGET_SERVICE_LAYER_EXTRACURRICULAR = "../proxy/api/Lead/LeadExtraCurricular/Get";
    AD.XPOST_SERVICE_LAYER_EXTRACURRICULAR = "../proxy/api/Lead/LeadExtraCurricular/Post";
    AD.XGET_SERVICE_LAYER_SKILLS = "../proxy/api/Lead/LeadSkills/Get";
    AD.XPOST_SERVICE_LAYER_SKILLS = "../proxy/api/Lead/LeadSkills/Post";
    AD.XGET_SERVICE_LAYER_NOTES = "../proxy/api/Lead/LeadNotes/Get";
    AD.XPOST_SERVICE_LAYER_NOTES = "../proxy/api/Lead/LeadNotes/Post";
    AD.XLEAD_INFO_DLLS_REQUIRED_ITEMS = "captionRequirementdb";
    AD.XLEAD_INFO_SDF_FIELDS = "SdfFields";
    AD.XLEAD_INFO_GROUPS = "LeadGroups";
    AD.XLEAD_NEW = "NewLead";
    AD.X_FLAG_LEAD = "X_FLAG_LEAD";
    AD.X_FLAG_TASK = "X_FLAG_TASK";
    AD.X_FLAG_APPO = "X_FLAG_APPO";
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadInfoBarDb = (function () {
        function LeadInfoBarDb() {
        }
        LeadInfoBarDb.prototype.showNotification = function (message) {
            var notif = $("#kNotification").kendoNotification({
                stacking: "down",
                position: { bottom: 30, right: 50 },
                autoHideAfter: 3000,
                templates: [{ type: "info", template: $("#infoTemplate").html() }]
            });
            notif.data("kendoNotification").show({ title: "Advantage", message: message, src: AD.XIMAGE_INFO }, "info");
        };
        LeadInfoBarDb.prototype.getInfoLeadFromServer = function (leadguid, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XGET_SERVICE_LAYER_LEAD_INFOBAR_COMMAND_GET + "?LeadGuid=" + leadguid,
                type: 'GET',
                timeout: 15000,
                dataType: 'json'
            });
        };
        LeadInfoBarDb.prototype.getInfoLeadReqStatusFromServer = function (leadguid) {
            var url = AD.XGET_SERVICE_LAYER_LEAD_REQUIREMENT_STATUS_GET + "?LeadGuid=" + leadguid;
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }
                },
                error: ad.SHOW_DATA_SOURCE_ERROR,
                schema: {
                    model: {
                        RequirementName: "RequirementName",
                        TypeOfRequirement: "TypeOfRequirement",
                        RequirementStatus: "RequirementStatus",
                        IsMandatory: "IsMandatory",
                        ImagePath: "ImagePath"
                    }
                }
            });
            return ds;
        };
        return LeadInfoBarDb;
    })();
    AD.LeadInfoBarDb = LeadInfoBarDb;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadInfoBarVm = (function (_super) {
        __extends(LeadInfoBarVm, _super);
        function LeadInfoBarVm() {
            _super.call(this);
            this.db = new AD.LeadInfoBarDb();
        }
        LeadInfoBarVm.prototype.getLeadInfoFromServer = function (leadGuid) {
            try {
                $("body").css("cursor", "progress");
                this.db.getInfoLeadFromServer(leadGuid, this)
                    .done(function (msg) {
                    if (msg != undefined) {
                        LeadInfoBarVm.refreshLeadFields(msg.LeadFullName, msg.ProgramVersionName, msg.ExpectedStart, msg.AdmissionRep, msg.DateAssigned, msg.CurrentStatus);
                        $("#infobarTableImage").attr("src", msg.LeadImagePath);
                    }
                })
                    .fail(function (msg) {
                    ad.SHOW_DATA_SOURCE_ERROR(msg);
                });
            }
            finally {
                $("body").css("cursor", "default");
            }
        };
        LeadInfoBarVm.prototype.getLeadReqStatusFromServer = function (leadGuid) {
            return this.db.getInfoLeadReqStatusFromServer(leadGuid);
        };
        LeadInfoBarVm.formatDate = function (d) {
            function addZero(n) {
                return n < 10 ? '0' + n : '' + n;
            }
            return addZero(d.getMonth() + 1) + "/" + addZero(d.getDate()) + "/" + d.getFullYear();
        };
        LeadInfoBarVm.prototype.goToLeadInfoScreen = function () {
            sessionStorage.setItem(AD.XLEAD_NEW, "false");
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&mod=AD");
        };
        LeadInfoBarVm.refreshLeadFields = function (leadFullName, programVersionName, expectedStart, admissionRep, dateAssigned, currentStatus) {
            $("#infoBarTableFullname").text(leadFullName);
            $("#infoBarTableProgramVersion").text(programVersionName);
            var date = "";
            if (expectedStart === "Invalid Date") {
                date = "";
            }
            else {
                if (expectedStart != null && expectedStart.length > 1) {
                    if (expectedStart.substring(0, 4) === "0001") {
                        date = "";
                    }
                    else {
                        date = this.formatDate(new Date(expectedStart));
                    }
                }
            }
            $("#infoBarTableExpectedStart").text(date);
            date = "";
            $("#infoBarTableAdmissionRep").text(admissionRep);
            if (dateAssigned.length > 1) {
                date = this.formatDate(new Date(dateAssigned));
            }
            $("#infoBarTableDateAssigned").text(date);
            $("#infoBarTableStatus").text(currentStatus);
        };
        return LeadInfoBarVm;
    })(kendo.Observable);
    AD.LeadInfoBarVm = LeadInfoBarVm;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadDuplicate = (function () {
        function LeadDuplicate() {
            try {
                this.viewModel = new AD.LeadDuplicateVm();
                var ethis = this;
                $("#duplicateLeadlistView").kendoListView({
                    dataSource: ethis.viewModel.duplicateLeadlistView,
                    template: kendo.template($("#templateDuplicateLeadListView").html()),
                    selectable: "single",
                    change: function () {
                        ethis.viewModel.GetDuplicateInformationFromServer();
                    },
                    dataBinding: function () {
                        var list = $("#duplicateLeadlistView").data("kendoListView").dataItems();
                        var count = list.length;
                        if (count == 0) {
                            $("#duplicateLeadTab").children(".k-link").html("Duplicates (" + count + ")");
                        }
                        else {
                            $("#duplicateLeadTab").children(".k-link").html("Duplicates (<span style='color:red'>" + count + "</span>)");
                        }
                        $("#duplicateLeadTab").children(".k-link").css("font-weight", "bold");
                    },
                });
                $("#duplicateHeaderLeadlistView").kendoListView({
                    template: kendo.template($("#templateduplicateHeaderLeadlistView").html()),
                    dataBound: ethis.viewModel.DecoreItems
                });
                $("#buttonDupUpdateLeadB").kendoButton({
                    icon: "folder-up",
                    enable: false,
                    click: ethis.viewModel.UpdateDuplicate
                });
                $("#buttonDupDeleteLeadA").kendoButton({
                    icon: "close",
                    enable: false,
                    click: ethis.viewModel.DeleteDuplicate
                });
                $("#buttonDupCreateLeadA").kendoButton({
                    icon: "plus",
                    enable: false,
                    click: ethis.viewModel.CreateLeadFromA
                });
                $("#buttonDupCreateLeadB").kendoButton({
                    icon: "plus",
                    enable: false,
                    click: ethis.viewModel.CreateLeadFromB
                });
                $("#buttonDupCreateLeadB").css("display", "none");
                kendo.init($("#leadDuplicatePagebind"));
                kendo.bind($("#leadDuplicatePagebind"), ethis.viewModel);
            }
            catch (e) {
                alert(e.message + "/n" + e.stack);
            }
        }
        return LeadDuplicate;
    })();
    AD.LeadDuplicate = LeadDuplicate;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadDuplicateDb = (function () {
        function LeadDuplicateDb() {
        }
        LeadDuplicateDb.prototype.showNotification = function (message) {
            var notif = $("#kNotification").kendoNotification({
                stacking: "down",
                position: { bottom: 30, right: 50 },
                autoHideAfter: 3000,
                templates: [{ type: "info", template: $("#infoTemplate").html() }]
            });
            notif.data("kendoNotification").show({ title: "Advantage", message: message, src: AD.XIMAGE_INFO }, "info");
        };
        LeadDuplicateDb.prototype.showNotificationCentered = function (message) {
            var notif = $("#kNotification").kendoNotification({
                stacking: "down",
                show: this.onShow,
                autoHideAfter: 3000,
                templates: [{ type: "info", template: $("#infoTemplate").html() }]
            });
            notif.data("kendoNotification").show({ title: "Advantage", message: message, src: AD.XIMAGE_INFO }, "info");
        };
        LeadDuplicateDb.prototype.onShow = function (e) {
            if (!$("." + e.sender._guid)[1]) {
                var element = e.element.parent(), eWidth = element.width(), eHeight = element.height(), wWidth = $(window).width(), wHeight = $(window).height();
                var newLeft = Math.floor(wWidth / 2 - eWidth / 2);
                var newTop = Math.floor(wHeight / 2 - eHeight / 2);
                e.element.parent().css({ top: newTop, left: newLeft });
            }
        };
        LeadDuplicateDb.prototype.GetDuplicateLeadsList = function () {
            var url = AD.XGET_SERVICE_LAYER_LEAD_DUPLICATES_GET;
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }
                },
                error: ad.SHOW_DATA_SOURCE_ERROR,
                schema: {
                    model: {
                        NewLeadGuid: "NewLeadGuid",
                        DuplicateGuid: "DuplicateGuid",
                        PosibleDuplicateFullName: "PosibleDuplicateFullName"
                    }
                }
            });
            return ds;
        };
        LeadDuplicateDb.prototype.commandAndGetDuplicateLeadsList = function (command, newleadguid, duplicateguid, userid) {
            var url = AD.XGET_SERVICE_LAYER_LEAD_DUPLICATES_COMMAND_GET + "?Command=" + command + "&LeadNewGuid=" + newleadguid + "&LeadDuplicateGuid=" + duplicateguid + "&UserId=" + userid;
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }
                },
                error: ad.SHOW_DATA_SOURCE_ERROR,
                schema: {
                    model: {
                        NewLeadGuid: "NewLeadGuid",
                        DuplicateGuid: "DuplicateGuid",
                        PosibleDuplicateFullName: "PosibleDuplicateFullName"
                    }
                }
            });
            return ds;
        };
        LeadDuplicateDb.prototype.getDuplicatePairForServer = function (newleadguid, duplicateguid) {
            var url = AD.XGET_SERVICE_LAYER_LEAD_DUPLICATES_PAIR_GET + "?LeadNewGuid=" + newleadguid + "&LeadDuplicateGuid=" + duplicateguid;
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }
                },
                error: ad.SHOW_DATA_SOURCE_ERROR,
                schema: {
                    model: {
                        Header: "Header",
                        AcquiredDate: "AcquiredDate",
                        Id: "Id",
                        From: "From",
                        FullName: "FullName",
                        Address1: "Address1",
                        Address2: "Address2",
                        City: "City",
                        State: "State",
                        Zip: "Zip",
                        Status: "Status",
                        Phone: "Phone",
                        Email: "Email",
                        SourceInfo: "SourceInfo",
                        AdmissionsRep: "AdmissionsRep",
                        CampusOfInterest: "CampusOfInterest",
                        Campus: "Campus",
                        ProgramOfInterest: "ProgramOfInterest",
                        Comments: "CampusOfInterest"
                    }
                }
            });
            return ds;
        };
        return LeadDuplicateDb;
    })();
    AD.LeadDuplicateDb = LeadDuplicateDb;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadDuplicateVm = (function (_super) {
        __extends(LeadDuplicateVm, _super);
        function LeadDuplicateVm() {
            var _this = this;
            _super.call(this);
            this.UpdateDuplicate = function (e) {
                _this.SendCommand(1);
            };
            this.DeleteDuplicate = function (e) {
                _this.SendCommand(2);
            };
            this.CreateLeadFromA = function (e) {
                _this.SendCommand(3);
            };
            this.CreateLeadFromB = function (e) {
                _this.SendCommand(4);
            };
            this.DecoreItems = function () {
                var datas = $("#duplicateHeaderLeadlistView").data("kendoListView");
                if (typeof datas !== "undefined") {
                    var ds = datas.dataSource.data();
                    if (ds == null || ds.length < 3) {
                        return;
                    }
                    if (ds[1].From == ds[2].From) {
                        $(".item2").css("background", "transparent");
                        $("#buttonDupUpdateLeadB").css("display", "none");
                        $("#buttonDupDeleteLeadA").css("display", "none");
                        $("#buttonDupCreateLeadA").css("display", "inline");
                        $("#buttonDupCreateLeadB").css("display", "inline");
                    }
                    else {
                        $(".item2:not(:first)").css("background", "yellow");
                        $("#buttonDupUpdateLeadB").css("display", "inline");
                        $("#buttonDupDeleteLeadA").css("display", "inline");
                        $("#buttonDupCreateLeadA").css("display", "inline");
                        $("#buttonDupCreateLeadB").css("display", "none");
                    }
                    ;
                    if (ds[1].AcquiredDate == ds[2].AcquiredDate) {
                        $(".item1").css("background", "transparent");
                    }
                    else {
                        $(".item1:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].FullName == ds[2].FullName) {
                        $(".item3").css("background", "transparent");
                    }
                    else {
                        $(".item3:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].Address1 == ds[2].Address1) {
                        $(".item4").css("background", "transparent");
                    }
                    else {
                        $(".item4:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].Address2 == ds[2].Address2) {
                        $(".item5").css("background", "transparent");
                    }
                    else {
                        $(".item5:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].City == ds[2].City) {
                        $(".item6").css("background", "transparent");
                    }
                    else {
                        $(".item6:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].State == ds[2].State) {
                        $(".item7").css("background", "transparent");
                    }
                    else {
                        $(".item7:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].Zip == ds[2].Zip) {
                        $(".item8").css("background", "transparent");
                    }
                    else {
                        $(".item8:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].Status == ds[2].Status) {
                        $(".item9").css("background", "transparent");
                    }
                    else {
                        $(".item9:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].Phone == ds[2].Phone) {
                        $(".item10").css("background", "transparent");
                    }
                    else {
                        $(".item10:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].Email == ds[2].Email) {
                        $(".item11").css("background", "transparent");
                    }
                    else {
                        $(".item11:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].SourceInfo == ds[2].SourceInfo) {
                        $(".item12").css("background", "transparent");
                    }
                    else {
                        $(".item12:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].AdmissionsRep == ds[2].AdmissionsRep) {
                        $(".item13").css("background", "transparent");
                    }
                    else {
                        $(".item13:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].Campus == ds[2].Campus) {
                        $(".item14").css("background", "transparent");
                    }
                    else {
                        $(".item14:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].ProgramOfInterest == ds[2].ProgramOfInterest) {
                        $(".item15").css("background", "transparent");
                    }
                    else {
                        $(".item15:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].Comments == ds[2].Comments) {
                        $(".item16").css("background", "transparent");
                    }
                    else {
                        $(".item16:not(:first)").css("background", "yellow");
                    }
                    ;
                }
            };
            this.db = new AD.LeadDuplicateDb();
            this.duplicateLeadlistView = this.db.GetDuplicateLeadsList();
            this.dummyDataSource = kendo.data.DataSource.create({
                data: [
                    {}
                ]
            });
        }
        LeadDuplicateVm.prototype.GetDuplicateInformationFromServer = function () {
            try {
                $("body").css("cursor", "progress");
                var listView = $("#duplicateLeadlistView").data("kendoListView");
                var newLead = listView.select();
                var newLeadGuid = $.map(newLead, function (item) {
                    return listView.dataSource.view()[$(item).index()].NewLeadGuid;
                });
                var dupLeadGuid = $.map(newLead, function (item) {
                    return listView.dataSource.view()[$(item).index()].DuplicateGuid;
                });
                var datasource = this.db.getDuplicatePairForServer(newLeadGuid, dupLeadGuid);
                var visor = $("#duplicateHeaderLeadlistView").data("kendoListView");
                visor.setDataSource(datasource);
                var btn1 = $("#buttonDupUpdateLeadB").data("kendoButton");
                var btn2 = $("#buttonDupDeleteLeadA").data("kendoButton");
                var btn3 = $("#buttonDupCreateLeadA").data("kendoButton");
                var btn4 = $("#buttonDupCreateLeadB").data("kendoButton");
                btn1.enable(true);
                btn2.enable(true);
                btn3.enable(true);
                btn4.enable(true);
            }
            finally {
                $("body").css("cursor", "default");
            }
        };
        LeadDuplicateVm.prototype.SendCommand = function (command) {
            switch (command) {
                case 1:
                    if (confirm("If you continue the information of Lead A will be transferred to Lead B. Are you sure?") == false)
                        return;
                    break;
                case 2:
                    if (confirm("Are you sure to delete Lead A?") == false)
                        return;
                    break;
                case 3:
                    if (confirm("Are you sure to create a new Lead with Lead A?") == false)
                        return;
                    break;
                case 4:
                    if (confirm("Are you sure to create a new Lead with Lead B?") == false)
                        return;
                    break;
                default:
                    alert("Command Not recognize. Operation is aborted!");
                    return;
            }
            try {
                $("body").css("cursor", "progress");
                var list = $("#duplicateLeadlistView").data("kendoListView");
                var newLead = list.select();
                var newLeadGuid = $.map(newLead, function (item) {
                    return list.dataSource.view()[$(item).index()].NewLeadGuid;
                });
                var dupLeadGuid = $.map(newLead, function (item) {
                    return list.dataSource.view()[$(item).index()].DuplicateGuid;
                });
                var userId = $("#hdnUserId").val();
                this.duplicateLeadlistView = this.db.commandAndGetDuplicateLeadsList(command, newLeadGuid, dupLeadGuid, userId);
                list.setDataSource(this.duplicateLeadlistView);
                var datasource = $("#duplicateHeaderLeadlistView").data("kendoListView").dataSource;
                datasource.remove(datasource.at(2));
                datasource.remove(datasource.at(1));
                datasource.remove(datasource.at(0));
                this.db.showNotificationCentered("Operation confirmed!");
            }
            finally {
                $("body").css("cursor", "default");
            }
        };
        return LeadDuplicateVm;
    })(kendo.Observable);
    AD.LeadDuplicateVm = LeadDuplicateVm;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadExtraCurricular = (function () {
        function LeadExtraCurricular() {
            this.bo = new AD.LeadExtraCurricularBo();
            $("#extraGrid").kendoGrid({
                dataSource: this.bo.dataSource,
                pageable: true,
                columns: [
                    {
                        field: "Group",
                        title: "Group",
                        width: "150px",
                        editor: this.bo.getGroupValuesEditor,
                        template: "#=Group.Description#"
                    },
                    { field: "Description", title: "Description" },
                    {
                        field: "Level",
                        title: "Level",
                        width: "150px",
                        editor: this.bo.getLevelValuesEditor,
                        template: "#=Level.Description#"
                    },
                    { field: "Comment", title: "Comment" },
                    { command: [{ name: "edit", text: " " }], title: "Edit", width: "50px" },
                    { command: [{ name: "destroy", text: " " }], title: "", width: "50px" }],
                editable: "popup",
                edit: function (e) {
                    if (e.model.isNew()) {
                        e.container.data("kendoWindow").title("Enter a New Extra Curricular");
                    }
                    else {
                        e.container.data("kendoWindow").title("Edit the Extra Curricular");
                    }
                }
            });
            var newRow = document.getElementById("extraPlusImage");
            newRow.addEventListener('click', this.bo.addNewRow, false);
        }
        return LeadExtraCurricular;
    })();
    AD.LeadExtraCurricular = LeadExtraCurricular;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadExtraCurricularBo = (function () {
        function LeadExtraCurricularBo() {
            var campusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            var leadElement = document.getElementById('leadId');
            var userId = $("#hdnUserId").val();
            var leadId = leadElement.value;
            var command = 1;
            this.dataSource = new kendo.data.DataSource({
                transport: {
                    read: function (options) {
                        $.ajax({
                            url: AD.XGET_SERVICE_LAYER_EXTRACURRICULAR,
                            type: "GET",
                            dataType: "json",
                            data: { CampusId: campusId, LeadId: leadId, Command: command },
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    update: function (options) {
                        var info = new AD.LeadExtraInfo();
                        info.Filter = new AD.LeadExtraInputInfo();
                        info.Filter.Command = 1;
                        info.GroupItemsList = new Array();
                        info.LevelsItemsList = new Array();
                        info.ExtraInfoList = new Array();
                        var extra = new AD.ExtraInfo();
                        extra.Level = options.data.models[0].Level;
                        extra.Group = options.data.models[0].Group;
                        extra.Comment = options.data.models[0].Comment;
                        extra.Description = options.data.models[0].Description;
                        extra.Id = options.data.models[0].Id;
                        info.Filter.LeadId = leadId;
                        info.Filter.UserId = userId;
                        info.ExtraInfoList.push(extra);
                        $.ajax({
                            type: "POST",
                            dataType: "text",
                            url: AD.XPOST_SERVICE_LAYER_EXTRACURRICULAR,
                            data: JSON.stringify(info),
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    destroy: function (options) {
                        var info = new AD.LeadExtraInfo();
                        info.Filter = new AD.LeadExtraInputInfo();
                        info.Filter.Command = 2;
                        info.GroupItemsList = new Array();
                        info.LevelsItemsList = new Array();
                        info.ExtraInfoList = new Array();
                        var extra = new AD.ExtraInfo();
                        extra.Id = options.data.models[0].Id;
                        info.Filter.LeadId = leadId;
                        info.Filter.UserId = userId;
                        info.ExtraInfoList.push(extra);
                        $.ajax({
                            type: "POST",
                            dataType: "text",
                            url: AD.XPOST_SERVICE_LAYER_EXTRACURRICULAR,
                            data: JSON.stringify(info),
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    create: function (options) {
                        var info = new AD.LeadExtraInfo();
                        info.Filter = new AD.LeadExtraInputInfo();
                        info.Filter.Command = 3;
                        info.GroupItemsList = new Array();
                        info.LevelsItemsList = new Array();
                        info.ExtraInfoList = new Array();
                        var extra = new AD.ExtraInfo();
                        extra.Level = options.data.models[0].Level;
                        extra.Group = options.data.models[0].Group;
                        extra.Comment = options.data.models[0].Comment;
                        extra.Description = options.data.models[0].Description;
                        extra.Id = 0;
                        info.Filter.LeadId = leadId;
                        info.Filter.UserId = userId;
                        info.ExtraInfoList.push(extra);
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: AD.XPOST_SERVICE_LAYER_EXTRACURRICULAR,
                            data: JSON.stringify(info),
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                        return null;
                    }
                },
                batch: true,
                pageSize: 20,
                schema: {
                    data: "ExtraInfoList",
                    total: function (response) { return response.ExtraInfoList.length; },
                    model: {
                        id: "Id",
                        fields: {
                            Id: { editable: false, nullable: false },
                            Group: {
                                validation: { required: true },
                                defaultValue: { ID: "0", Description: "" }
                            },
                            Description: {
                                type: "string",
                                validation: {
                                    required: { message: "Extra Curricular Description is required!" },
                                    maxCharacteres: function (input) {
                                        input.attr("data-maxCharacteres-msg", "Description Cannot Be Longer Than 50 Characters!");
                                        if (input[0].name === "Description") {
                                            return input.val().length <= 50;
                                        }
                                        return true;
                                    }
                                }
                            },
                            Level: {
                                validation: { required: true },
                                defaultValue: { ID: "0", Description: "" }
                            },
                            Comment: {
                                type: "string",
                                validation: {
                                    commentLength: function (input) {
                                        input.attr("data-commentLength-msg", "Comment Cannot Be Longer Than 100 Characters!");
                                        if (input[0].name === "Comment") {
                                            return input.val().length <= 100;
                                        }
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
        LeadExtraCurricularBo.prototype.addNewRow = function () {
            var grid = $("#extraGrid").data("kendoGrid");
            grid.addRow();
            return null;
        };
        LeadExtraCurricularBo.prototype.getGroupValuesEditor = function (container, options) {
            $('<input required data-text-field="Description" data-value-field="ID"  data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                dataSource: {
                    transport: {
                        read: function (options) {
                            $.ajax({
                                url: AD.XGET_SERVICE_LAYER_EXTRACURRICULAR,
                                type: "GET",
                                dataType: "json",
                                data: {
                                    CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                    LeadId: "00000000-0000-0000-0000-000000000000",
                                    Command: 3
                                },
                                success: function (result) {
                                    options.success(result.GroupItemsList);
                                },
                                error: function (result) {
                                    options.error(result);
                                    ad.SHOW_DATA_SOURCE_ERROR(result);
                                }
                            });
                        }
                    },
                    dataValueField: "ID",
                    dataTextField: "Description",
                    autoBind: false
                },
                optionLabel: "Select"
            });
        };
        LeadExtraCurricularBo.prototype.getLevelValuesEditor = function (container, options) {
            $('<input required data-text-field="Description" data-value-field="ID"  data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                dataSource: {
                    transport: {
                        read: function (options) {
                            $.ajax({
                                url: AD.XGET_SERVICE_LAYER_EXTRACURRICULAR,
                                type: "GET",
                                dataType: "json",
                                data: {
                                    CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                    LeadId: "00000000-0000-0000-0000-000000000000",
                                    Command: 2
                                },
                                success: function (result) {
                                    options.success(result.LevelsItemsList);
                                },
                                error: function (result) {
                                    options.error(result);
                                    ad.SHOW_DATA_SOURCE_ERROR(result);
                                }
                            });
                        }
                    },
                    dataValueField: "ID",
                    dataTextField: "Description",
                    autoBind: false
                },
                optionLabel: "Select"
            });
        };
        ;
        return LeadExtraCurricularBo;
    })();
    AD.LeadExtraCurricularBo = LeadExtraCurricularBo;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var DropDownItem = (function () {
        function DropDownItem() {
        }
        return DropDownItem;
    })();
    AD.DropDownItem = DropDownItem;
    var ExtraInfo = (function () {
        function ExtraInfo() {
        }
        return ExtraInfo;
    })();
    AD.ExtraInfo = ExtraInfo;
    var LeadExtraInputInfo = (function () {
        function LeadExtraInputInfo() {
        }
        return LeadExtraInputInfo;
    })();
    AD.LeadExtraInputInfo = LeadExtraInputInfo;
    var LeadExtraInfo = (function () {
        function LeadExtraInfo() {
        }
        return LeadExtraInfo;
    })();
    AD.LeadExtraInfo = LeadExtraInfo;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadSkills = (function () {
        function LeadSkills() {
            this.bo = new AD.LeadSkillsBo();
            $("#extraGrid").kendoGrid({
                dataSource: this.bo.dataSource,
                pageable: true,
                columns: [
                    {
                        field: "Group",
                        title: "Group",
                        width: "150px",
                        editor: this.bo.getGroupValuesEditor,
                        template: "#=Group.Description#"
                    },
                    { field: "Description", title: "Description" },
                    {
                        field: "Level",
                        title: "Level",
                        width: "150px",
                        editor: this.bo.getLevelValuesEditor,
                        template: "#=Level.Description#"
                    },
                    { field: "Comment", title: "Comment" },
                    { command: [{ name: "edit", text: " " }], title: "Edit", width: "50px" },
                    { command: [{ name: "destroy", text: " " }], title: "", width: "50px" }],
                editable: "popup",
                edit: function (e) {
                    if (e.model.isNew()) {
                        e.container.data("kendoWindow").title("Enter a New Extra Curricular");
                    }
                    else {
                        e.container.data("kendoWindow").title("Edit the Extra Curricular");
                    }
                }
            });
            var newRow = document.getElementById("extraPlusImage");
            newRow.addEventListener('click', this.bo.addNewRow, false);
        }
        return LeadSkills;
    })();
    AD.LeadSkills = LeadSkills;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadSkillsBo = (function () {
        function LeadSkillsBo() {
            var campusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            var leadElement = document.getElementById('leadId');
            var userId = $("#hdnUserId").val();
            var leadId = leadElement.value;
            var command = 1;
            this.dataSource = new kendo.data.DataSource({
                transport: {
                    read: function (options) {
                        $.ajax({
                            url: AD.XGET_SERVICE_LAYER_SKILLS,
                            type: "GET",
                            dataType: "json",
                            data: { CampusId: campusId, LeadId: leadId, Command: command },
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    update: function (options) {
                        var info = new AD.LeadExtraInfo();
                        info.Filter = new AD.LeadExtraInputInfo();
                        info.Filter.Command = 1;
                        info.GroupItemsList = new Array();
                        info.LevelsItemsList = new Array();
                        info.ExtraInfoList = new Array();
                        var extra = new AD.ExtraInfo();
                        extra.Level = options.data.models[0].Level;
                        extra.Group = options.data.models[0].Group;
                        extra.Comment = options.data.models[0].Comment;
                        extra.Description = options.data.models[0].Description;
                        extra.Id = options.data.models[0].Id;
                        info.Filter.LeadId = leadId;
                        info.Filter.UserId = userId;
                        info.ExtraInfoList.push(extra);
                        $.ajax({
                            type: "POST",
                            dataType: "text",
                            url: AD.XPOST_SERVICE_LAYER_SKILLS,
                            data: JSON.stringify(info),
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    destroy: function (options) {
                        var info = new AD.LeadExtraInfo();
                        info.Filter = new AD.LeadExtraInputInfo();
                        info.Filter.Command = 2;
                        info.GroupItemsList = new Array();
                        info.LevelsItemsList = new Array();
                        info.ExtraInfoList = new Array();
                        var extra = new AD.ExtraInfo();
                        extra.Id = options.data.models[0].Id;
                        info.Filter.LeadId = leadId;
                        info.Filter.UserId = userId;
                        info.ExtraInfoList.push(extra);
                        $.ajax({
                            type: "POST",
                            dataType: "text",
                            url: AD.XPOST_SERVICE_LAYER_SKILLS,
                            data: JSON.stringify(info),
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    create: function (options) {
                        var info = new AD.LeadExtraInfo();
                        info.Filter = new AD.LeadExtraInputInfo();
                        info.Filter.Command = 3;
                        info.GroupItemsList = new Array();
                        info.LevelsItemsList = new Array();
                        info.ExtraInfoList = new Array();
                        var extra = new AD.ExtraInfo();
                        extra.Level = options.data.models[0].Level;
                        extra.Group = options.data.models[0].Group;
                        extra.Comment = options.data.models[0].Comment;
                        extra.Description = options.data.models[0].Description;
                        extra.Id = 0;
                        info.Filter.LeadId = leadId;
                        info.Filter.UserId = userId;
                        info.ExtraInfoList.push(extra);
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: AD.XPOST_SERVICE_LAYER_SKILLS,
                            data: JSON.stringify(info),
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                        return null;
                    }
                },
                batch: true,
                pageSize: 20,
                schema: {
                    data: "ExtraInfoList",
                    total: function (response) { return response.ExtraInfoList.length; },
                    model: {
                        id: "Id",
                        fields: {
                            Id: { editable: false, nullable: false },
                            Group: { validation: { required: true }, defaultValue: { ID: "0", Description: "" } },
                            Description: {
                                type: "string",
                                validation: {
                                    required: { message: "Skill Description is required!" },
                                    maxCharacteres: function (input) {
                                        input.attr("data-maxCharacteres-msg", "Description Cannot Be Longer Than 50 Characters!");
                                        if (input[0].name === "Description") {
                                            return input.val().length <= 50;
                                        }
                                        return true;
                                    }
                                }
                            },
                            Level: {
                                validation: { required: true },
                                defaultValue: { ID: "0", Description: "" }
                            },
                            Comment: {
                                type: "string",
                                validation: {
                                    commentLength: function (input) {
                                        input.attr("data-commentLength-msg", "Comment Cannot Be Longer Than 100 Characters!");
                                        if (input[0].name === "Comment") {
                                            return input.val().length <= 100;
                                        }
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
        LeadSkillsBo.prototype.addNewRow = function () {
            var grid = $("#extraGrid").data("kendoGrid");
            grid.addRow();
            return null;
        };
        LeadSkillsBo.prototype.getGroupValuesEditor = function (container, options) {
            $('<input required name="Skill Group" data-text-field="Description" data-value-field="ID"  data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                dataSource: {
                    transport: {
                        read: function (options) {
                            $.ajax({
                                url: AD.XGET_SERVICE_LAYER_SKILLS,
                                type: "GET",
                                dataType: "json",
                                data: {
                                    CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                    LeadId: "00000000-0000-0000-0000-000000000000",
                                    Command: 3
                                },
                                success: function (result) {
                                    options.success(result.GroupItemsList);
                                },
                                error: function (result) {
                                    options.error(result);
                                    ad.SHOW_DATA_SOURCE_ERROR(result);
                                }
                            });
                        }
                    },
                    dataValueField: "ID",
                    dataTextField: "Description",
                    autoBind: false
                },
                optionLabel: "Select"
            });
        };
        LeadSkillsBo.prototype.getLevelValuesEditor = function (container, options) {
            $('<input required name="Skill Level" data-text-field="Description" data-value-field="ID"  data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                dataSource: {
                    transport: {
                        read: function (options) {
                            $.ajax({
                                url: AD.XGET_SERVICE_LAYER_SKILLS,
                                type: "GET",
                                dataType: "json",
                                data: {
                                    CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                    LeadId: "00000000-0000-0000-0000-000000000000",
                                    Command: 2
                                },
                                success: function (result) {
                                    options.success(result.LevelsItemsList);
                                },
                                error: function (result) {
                                    options.error(result);
                                    ad.SHOW_DATA_SOURCE_ERROR(result);
                                }
                            });
                        }
                    },
                    dataValueField: "ID",
                    dataTextField: "Description",
                    autoBind: false
                },
                optionLabel: "Select"
            });
        };
        ;
        return LeadSkillsBo;
    })();
    AD.LeadSkillsBo = LeadSkillsBo;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LastNameHistory = (function () {
        function LastNameHistory() {
        }
        return LastNameHistory;
    })();
    AD.LastNameHistory = LastNameHistory;
    var Phone = (function () {
        function Phone() {
        }
        return Phone;
    })();
    AD.Phone = Phone;
    var Email = (function () {
        function Email() {
        }
        return Email;
    })();
    AD.Email = Email;
    var Sdf = (function () {
        function Sdf() {
        }
        return Sdf;
    })();
    AD.Sdf = Sdf;
    var Group = (function () {
        function Group() {
        }
        return Group;
    })();
    AD.Group = Group;
    var Vehicle = (function () {
        function Vehicle() {
        }
        return Vehicle;
    })();
    AD.Vehicle = Vehicle;
    var LeadInputModel = (function () {
        function LeadInputModel() {
        }
        return LeadInputModel;
    })();
    var LeadInfoOutput = (function () {
        function LeadInfoOutput() {
            this.Vehicles = [];
            this.PhonesList = [];
            this.EmailList = [];
            this.LeadLastNameHistoryList = [];
            this.InputModel = new LeadInputModel();
        }
        return LeadInfoOutput;
    })();
    AD.LeadInfoOutput = LeadInfoOutput;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadInfoPage = (function () {
        function LeadInfoPage() {
            var _this = this;
            try {
                this.viewModel = new AD.LeadInfoPageVm();
                var ethis = this;
                var isLead = true;
                var leadId;
                var isnew = ad.GET_QUERY_STRING_PARAMETER_BY_NAME("new");
                if (isnew === "0") {
                    sessionStorage.setItem(AD.XLEAD_NEW, "false");
                }
                var newLead = sessionStorage.getItem(AD.XLEAD_NEW);
                if (newLead != null && newLead === "true") {
                    isLead = false;
                }
                else {
                    var leadSelected = document.getElementById('leadId');
                    leadId = leadSelected.value;
                    if ((leadId == undefined || leadId == null || leadId === "")) {
                        sessionStorage.setItem(AD.XLEAD_NEW, "true");
                        window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&mod=AD");
                        return;
                    }
                }
                $("#WindowsDuplicate").kendoWindow({
                    actions: ["Close"],
                    title: "Duplicates Window",
                    width: "900px",
                    height: "650px",
                    visible: false,
                    modal: false,
                    resizable: true
                });
                $("#infoPageDupGrid").kendoGrid({
                    toolbar: ["excel", "pdf"],
                    excel: {
                        allPages: true
                    },
                    pdf: {
                        fileName: "AdvantageLeadsManagement.pdf",
                        landscape: true
                    },
                    dataSource: [{}],
                    sortable: true,
                    filterable: false,
                    resizable: true,
                    columns: [
                        { field: "Id", title: "", width: 1 },
                        { field: "FirstName", title: "First Name", attributes: { style: "font-size:11px;" } },
                        { field: "LastName", title: "Last Name", attributes: { style: "font-size:11px;" } },
                        { field: "ssn", title: "SSN", attributes: { style: "font-size:11px;" } },
                        { field: "Dob", title: "DOB", attributes: { style: "font-size:11px;" } },
                        { field: "Address1", title: "Address", attributes: { style: "font-size:11px;" } },
                        { field: "Phone", title: "Phones" },
                        { field: "Email", title: "E-Mails" },
                        { field: "Campus", title: "Campus", attributes: { style: "font-size:11px;" } },
                        { field: "Status", title: "Status", attributes: { style: "font-size:11px;" } },
                        { field: "AdmissionRep", title: "Admission Rep", attributes: { style: "font-size:11px;" } }
                    ]
                });
                $('#cancelLead').click(function () {
                    var dupWin = $("#WindowsDuplicate").data("kendoWindow");
                    dupWin.close();
                });
                $("#windowsVehicle").kendoWindow({
                    actions: ["Close"],
                    title: "Vehicles Information",
                    width: "520px",
                    height: "290px",
                    visible: false,
                    modal: true,
                    resizable: false
                });
                $('#goMoreTransportation').click(function () {
                    var vWin = $("#windowsVehicle").data("kendoWindow");
                    _this.viewModel.storeVehicleInformation();
                    vWin.center().open();
                });
                $('#vehicleCancel').click(function () {
                    _this.viewModel.cancelVehicleEdition();
                    var win = $("#windowsVehicle").data("kendoWindow");
                    win.close();
                });
                $('#vehicleSave').click(function () {
                    var win = $("#windowsVehicle").data("kendoWindow");
                    win.close();
                });
                this.initializeKendoComponents();
                this.viewModel.getResourcesForPage(isLead);
                $('#insertLead').click(function () {
                    var dupWin = $("#WindowsDuplicate").data("kendoWindow");
                    dupWin.close();
                    var customValidation = _this.viewModel.executeCustomValidators();
                    if (customValidation === false) {
                        return;
                    }
                    var validator = $("#containerInfoPage").kendoValidator().data("kendoValidator");
                    if (validator.validate()) {
                        _this.viewModel.postLeadInformationToServer(leadId, true);
                    }
                    else {
                        alert(AD.X_SAVE_REVIEW_INFO_IN_FORM);
                    }
                });
                $("#leadPrintButton").click(function () {
                    var userId = $("#hdnUserId").val();
                    var printInfo = new AD.PrintOutputModel();
                    printInfo = _this.viewModel.fillPrintInfo(printInfo);
                    sessionStorage.removeItem("PRINT_INFO");
                    sessionStorage.setItem("PRINT_INFO", JSON.stringify(printInfo));
                    sessionStorage.setItem("USER_ID", userId);
                    var url = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL
                        + "AD/ALeadPrint.aspx?RESID=206&cmpId="
                        + XMASTER_GET_CURRENT_CAMPUS_ID
                        + "&desc=PrintProfile&mod=AD"
                        + "&leadId=" + leadId;
                    window.open(url, "_newtab");
                });
                $('#leadToolBarNew').click(function () {
                    var save = confirm("Save Changes?");
                    if (save) {
                        $("#leadToolBarSave").click();
                    }
                    sessionStorage.setItem(AD.XLEAD_NEW, "true");
                    window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&mod=AD");
                });
                $('#leadToolBarSave').click(function () {
                    var customValidation = _this.viewModel.executeCustomValidators();
                    if (customValidation === false) {
                        return;
                    }
                    var validator = $("#containerInfoPage").kendoValidator().data("kendoValidator");
                    if (validator.validate()) {
                        _this.viewModel.postLeadInformationToServer(leadId, false);
                    }
                    else {
                        alert(AD.X_SAVE_PLEASE_FILL_REQUIRED_FIELDS);
                    }
                });
                $('#leadToolBarDelete').click(function () {
                    _this.viewModel.deleteLead(leadId);
                });
                var container = $("#containerInfoPage");
                kendo.init(container);
                container.kendoValidator({
                    validateOnBlur: true,
                    rules: {
                        validSsn: function (input) {
                            if (input.is("[name=maskedsocialSecurity]") && input.val() !== "") {
                                var ssnTxt = input.data("kendoMaskedTextBox");
                                return ssnTxt.value().indexOf(ssnTxt.options.promptChar) === -1;
                            }
                            else {
                                return true;
                            }
                        },
                        validDob: function (input) {
                            if (input.is("[name=dtDob]") && input.val() !== "") {
                                var currentDate = kendo.parseDate($(input).val());
                                if (!currentDate) {
                                    return false;
                                }
                            }
                            return true;
                        }
                    },
                    messages: {
                        validDob: "Invalid Date!"
                    }
                });
                kendo.bind($("#containerInfoPage"), ethis.viewModel);
            }
            catch (e) {
                $("body").css("cursor", "default");
                alert(e.message + "/n" + e.stack);
                sessionStorage.removeItem(AD.XLEAD_INFO_DLLS_REQUIRED_ITEMS);
                sessionStorage.removeItem(AD.XLEAD_INFO_GROUPS);
                sessionStorage.removeItem(AD.XLEAD_INFO_SDF_FIELDS);
            }
        }
        LeadInfoPage.prototype.initializeKendoComponents = function () {
            var _this = this;
            $("#cbPrevlast").kendoDropDownList({
                optionLabel: "",
                dataTextField: "LastName",
                dataValueField: "Id",
                change: function (e) {
                    e.preventDefault();
                    e.sender.select(0);
                }
            });
            $("#cbRace").kendoDropDownList({
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#dtDob").kendoDatePicker({
                change: function () {
                    var dob = $("#dtDob").data("kendoDatePicker").value();
                    var age = $("#textage").val();
                    _this.viewModel.calculateAge(dob, age);
                }
            });
            $("#itextdependants").kendoNumericTextBox({
                decimals: 0,
                format: "n0",
                step: 1,
                min: 0,
                max: 99
            });
            $("#tpTimeTo").kendoNumericTextBox({
                decimals: 0,
                format: "n0",
                step: 1,
                min: 0,
                max: 999
            });
            $("#cbprefix").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbsuffix").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbgender").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbmaritalstatus").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbdriverlicensestate").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbcitizenship").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbdependency").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbfamilyincome").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbhousingtype").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbtransportation").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbState").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbcategorySource").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbtypeSource").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbAdvertisementSource").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#dtDateSource").kendoDateTimePicker({
                value: new Date()
            });
            $("#dtotherDateApplied").kendoDatePicker();
            $("#cbotherPreviousEducation").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#dtotherAdmRepAssignDate").kendoDatePicker({
                format: "MM/dd/yyyy",
                parseFormats: ["MM/dd/yyyy hh:mm tt"]
            });
            $("#cbOtherHighSchool").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbotherAdmRep").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#dtHsGradDate").kendoDatePicker();
            $("#cbotherSponsor").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#ddAttendingHs").kendoDropDownList({
                dataSource: [
                    { text: "Select", value: "-1" },
                    { text: "Yes", value: "1" },
                    { text: "No", value: "0" }
                ],
                dataTextField: "text",
                dataValueField: "value"
            });
            $("#cbotherReasonNot").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbotherAdmCriteria").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#tpBestTime").kendoTimePicker({
                interval: 15,
                min: new Date(2000, 0, 1, 6, 0, 0)
            });
            $("#txtPhone").kendoMaskedTextBox({
                mask: "(000)-000-0000",
                clearPromptChar: true
            });
            $("#txtPhone1").kendoMaskedTextBox({
                mask: "(000)-000-0000",
                clearPromptChar: true
            });
            $("#txtPhone2").kendoMaskedTextBox({
                mask: "(000)-000-0000",
                clearPromptChar: true
            });
            $('#checkphone').change(function () {
                var maskedtextbox = $("#txtPhone").data("kendoMaskedTextBox");
                var raw = maskedtextbox.raw();
                if ($(this).is(":checked")) {
                    maskedtextbox.setOptions({ mask: '999999999999999' });
                }
                else {
                    maskedtextbox.setOptions({ mask: '(000)-000-0000' });
                }
                maskedtextbox.value(raw);
            });
            $('#checkphone1').change(function () {
                var maskedtextbox = $("#txtPhone1").data("kendoMaskedTextBox");
                var raw = maskedtextbox.raw();
                if ($(this).is(":checked")) {
                    maskedtextbox.setOptions({ mask: '999999999999999' });
                }
                else {
                    maskedtextbox.setOptions({ mask: '(000)-000-0000' });
                }
                maskedtextbox.value(raw);
            });
            $('#checkphone2').change(function () {
                var maskedtextbox = $("#txtPhone2").data("kendoMaskedTextBox");
                var raw = maskedtextbox.raw();
                if ($(this).is(":checked")) {
                    maskedtextbox.setOptions({ mask: '999999999999999' });
                }
                else {
                    maskedtextbox.setOptions({ mask: '(000)-000-0000' });
                }
                maskedtextbox.value(raw);
            });
            $("#cbphone").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbphone1").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbphone2").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbcountry").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbCounty").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbAddress").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#txtZipCode").kendoMaskedTextBox({
                mask: "00000",
                clearPromptChar: true
            });
            $('#checkIntAddress').change(function () {
                var cbState = $("#cbState").data("kendoDropDownList");
                var otherState = $("#otherStates");
                var zipcode = $("#txtZipCode").data("kendoMaskedTextBox");
                if ($(this).is(":checked")) {
                    $("#checkIntAddress").prop('checked', true);
                    cbState.wrapper.hide();
                    otherState.css("display", "block");
                    zipcode.setOptions({ mask: '9999999999' });
                }
                else {
                    $("#checkIntAddress").prop('checked', false);
                    cbState.wrapper.show();
                    otherState.css("display", "none");
                    zipcode.setOptions({ mask: '00000' });
                }
            });
            $("#cbemailprimary").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbemail").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Type",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $('#checkNoEmail').change(function () {
                var disabled = ($(this).is(":checked"));
                $("#txtemailprimary").prop("disabled", disabled);
                $("#txtemail").prop("disabled", disabled);
                if (disabled) {
                    $("#txtemail").css("color", "white");
                    $("#txtemailprimary").css("color", "white");
                }
                else {
                    $("#txtemail").css("color", "inherit");
                    $("#txtemailprimary").css("color", "inherit");
                }
            });
            $("#cbBestTime").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#gocontactInformation").kendoButton({
                click: this.viewModel.onClickContactInformation
            });
            var campus = $("#cbLeadAssign").kendoDropDownList({
                dataSource: [],
                dataTextField: "Description",
                dataValueField: "ID",
                change: function () {
                    var campusId = $("#cbLeadAssign").data("kendoDropDownList").value();
                    var programgrp = $("#cbInterestArea").data("kendoDropDownList");
                    programgrp.setDataSource(new kendo.data.DataSource());
                    _this.viewModel.fillFilteredDatadb(campusId);
                    _this.viewModel.refreshCampusInControl(campusId);
                }
            }).data("kendoDropDownList");
            campus.list.width("auto");
            campus.list.css("white-space", "nowrap");
            $("#cbInterestArea").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID",
                change: function () {
                    var campusId = $("#cbLeadAssign").data("kendoDropDownList").value();
                    var cbprogramVersion = $("#cbprogramVersion").data("kendoDropDownList");
                    var cbschedule = $("#cbschedule").data("kendoDropDownList");
                    var cbexpectedStart = $("#cbExpectedStart").data("kendoDropDownList");
                    cbschedule.setDataSource(new kendo.data.DataSource());
                    cbprogramVersion.setDataSource(new kendo.data.DataSource());
                    cbexpectedStart.setDataSource(new kendo.data.DataSource());
                    var selectedValue = $("#cbInterestArea").data("kendoDropDownList").value();
                    if (selectedValue !== "") {
                        _this.viewModel.getItemInformation(campusId, "ProgramID", "cbprogramLabel", selectedValue);
                    }
                    else {
                        var cbprogramLabel = $("#cbprogramLabel").data("kendoDropDownList");
                        cbprogramLabel.setDataSource(new kendo.data.DataSource());
                    }
                }
            });
            var dia = $("#cbInterestArea").data("kendoDropDownList");
            dia.list.width("auto");
            dia.list.css("white-space", "nowrap");
            $("#cbprogramLabel").kendoDropDownList({
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID",
                dataSource: [],
                change: function () {
                    var campusId = $("#cbLeadAssign").data("kendoDropDownList").value();
                    var cbprogramVersion = $("#cbprogramVersion").data("kendoDropDownList");
                    var cbschedule = $("#cbschedule").data("kendoDropDownList");
                    var cbexpectedStart = $("#cbExpectedStart").data("kendoDropDownList");
                    cbschedule.setDataSource(new kendo.data.DataSource());
                    cbprogramVersion.setDataSource(new kendo.data.DataSource());
                    cbexpectedStart.setDataSource(new kendo.data.DataSource());
                    var selectedValue = $("#cbprogramLabel").data("kendoDropDownList").value();
                    if (selectedValue !== "") {
                        _this.viewModel.getItemInformation(campusId, "PrgVerId", "cbprogramVersion", selectedValue);
                        _this.viewModel.getExpectedLeadItemInformation(campusId, "ExpectedStart", "cbExpectedStart", selectedValue);
                    }
                }
            });
            var dpl = $("#cbprogramLabel").data("kendoDropDownList");
            dpl.list.width("auto");
            dpl.list.css("white-space", "nowrap");
            $("#cbprogramVersion").kendoDropDownList({
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID",
                dataSource: [],
                change: function () {
                    var campusId = $("#cbLeadAssign").data("kendoDropDownList").value();
                    var cbprogramVersion = $("#cbprogramVersion").data("kendoDropDownList");
                    var selectedValue = cbprogramVersion.value();
                    if (selectedValue !== "") {
                        _this.viewModel.getItemInformation(campusId, "ProgramScheduleId", "cbschedule", selectedValue);
                    }
                }
            });
            var ddl = $("#cbprogramVersion").data("kendoDropDownList");
            ddl.list.width("auto");
            ddl.list.css("white-space", "nowrap");
            $("#cbschedule").kendoDropDownList({
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID",
                dataSource: []
            });
            if (XMASTER_LEAD_IS_CLOCK_HOUR === 'Yes') {
                $("#dpExpectedStart").css("display", "inline");
                $("#dpExpectedStart").kendoDatePicker();
            }
            else {
                $("#cbExpectedStart").kendoDropDownList({
                    dataSource: [],
                    optionLabel: "Select",
                    dataTextField: "Description",
                    dataValueField: "ID"
                });
                var dea = $("#cbExpectedStart").data("kendoDropDownList");
                dea.list.width("auto");
                dea.list.css("white-space", "nowrap");
            }
            $("#cbattend").kendoDropDownList({
                dataSource: [],
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#cbLeadStatus").kendoDropDownList({
                dataSource: [],
                dataTextField: "Description",
                dataValueField: "ID"
            });
            $("#dtGrad").kendoDatePicker();
        };
        return LeadInfoPage;
    })();
    AD.LeadInfoPage = LeadInfoPage;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadInfoPageDb = (function () {
        function LeadInfoPageDb() {
        }
        LeadInfoPageDb.prototype.getResourcesForPage = function (pageResourceId, campusId, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XGET_SERVICE_LAYER_INFOPAGE_RESOURCES_GET + "?PageResourceId=" + pageResourceId + "&CampusId=" + campusId,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        };
        LeadInfoPageDb.prototype.getCatalogsInfoFromServer = function (fldName, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XGET_SERVICE_LAYER_CATALOGS_COMMAND_GET + "?fldName=" + fldName,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        };
        LeadInfoPageDb.prototype.getLeadDemographic = function (leadGuid, userId, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XGET_SERVICE_LAYER_LEAD_DEMOGRAPHIC_COMMAND_GET + "?LeadId=" + leadGuid + "&UserId=" + userId,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        };
        LeadInfoPageDb.prototype.getLeadItemInformation = function (codeItemToGet, value, campusId, mcontext) {
            if (value != null) {
                value = value.toLocaleString();
            }
            return $.ajax({
                context: mcontext,
                url: AD.XGET_SERVICE_LAYER_CATALOGS_COMMAND_GET + "?FldName=" + codeItemToGet + "&CampusID=" + campusId + "&AdditionalFilter=" + value,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        };
        LeadInfoPageDb.prototype.getVehiclesFromServer = function (leadId, userId, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XGET_SERVICE_LAYER_LEAD_DEMOGRAPHIC_COMMAND_GET + "?LeadId=" + leadId + "&UserId=" + userId,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        };
        LeadInfoPageDb.prototype.postLeadInformation = function (lead, context) {
            return $.ajax({
                url: AD.XPOST_SERVICE_LAYER_LEAD_INFORMATION,
                type: 'POST',
                dataType: 'json',
                timeout: 20000,
                data: JSON.stringify(lead)
            });
        };
        LeadInfoPageDb.prototype.postLeadDelete = function (leadGuid, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XPOST_SERVICE_LAYER_LEAD_DELETE_COMMAND + "?LeadId=" + leadGuid,
                type: 'POST',
                timeout: 20000,
                dataType: 'text'
            });
        };
        LeadInfoPageDb.prototype.getIsLeadErasable = function (leadGuid, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XGET_SERVICE_LAYER_LEAD_IS_ERASABLE_COMMAND + "?LeadId=" + leadGuid,
                type: 'GET',
                timeout: 20000,
                dataType: 'text'
            });
        };
        LeadInfoPageDb.prototype.getAllowedStatus = function (campusId, lStatus, userId, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XGET_SERVICE_LAYER_LEAD_DEMOGRAPHIC_COMMAND_GET + "?CampusId=" + campusId +
                    "&LeadStatusId=" + lStatus +
                    "&UserId=" + userId +
                    "&CommandString=" + "ALLOWED_STATUS",
                type: "GET",
                timeout: 20000,
                dataType: "json"
            });
        };
        LeadInfoPageDb.prototype.staticInformationForCaptionRequirement = function () {
            return [
                { fldName: "Age", labelName: "age", controlName: "textage", isDll: false },
                { fldName: "AlienNumber", labelName: "aliennumber", controlName: "tbaliennumber", isDll: false },
                { fldName: "BirthDate", labelName: "dob", controlName: "dtdob", isDll: false },
                { fldName: "Citizen", labelName: "citizenship", controlName: "cbcitizenship", isDll: false },
                { fldName: "FirstName", labelName: "firstName", controlName: "txtFirstName", isDll: false },
                { fldName: "Children", labelName: "dependants", controlName: "itextdependants", isDll: false },
                { fldName: "DependencyTypeId", labelName: "dependency", controlName: "cbdependency", isDll: true, dll: null },
                { fldName: "DrivLicNumber", labelName: "driverlicensenumber", controlName: "tdriverlicensenumber", isDll: false },
                { fldName: "DrivLicStateID", labelName: "driverlicensestate", controlName: "", isDll: false },
                { fldName: "StateId", labelName: "", controlName: "cbdriverlicensestate", isDll: true, dll: [] },
                { fldName: "Race", labelName: "lblRace", controlName: "cbRace", isDll: true, dll: [] },
                { fldName: "FamilyIncome", labelName: "familyincome", controlName: "cbfamilyincome", isDll: true, dll: null },
                { fldName: "Gender", labelName: "gender", controlName: "cbgender", isDll: true, dll: [] },
                { fldName: "HousingId", labelName: "housingtype", controlName: "cbhousingtype", isDll: true, dll: null },
                { fldName: "LastName", labelName: "lastName", controlName: "txtLastName", isDll: false },
                { fldName: "MaritalStatus", labelName: "maritalstatus", controlName: "cbmaritalstatus", isDll: true, dll: null },
                { fldName: "MiddleName", labelName: "middleName", controlName: "txtMiddleName", isDll: false },
                { fldName: "NickName", labelName: "nickname", controlName: "tbNickName", isDll: false },
                { fldName: "Prefix", labelName: "prefix", controlName: "cbprefix", isDll: true, dll: null },
                { fldName: "SSN", labelName: "socialSecurity", controlName: "txtsocialSecurity", isDll: false },
                { fldName: "Suffix", labelName: "suffix", controlName: "cbsuffix", isDll: true, dll: null },
                { fldName: "TransportationId", labelName: "transportation", controlName: "cbtransportation", isDll: true, dll: [] },
                { fldName: "TimeToSchool", labelName: "timeTo", controlName: "tpTimeTo", isDll: false },
                { fldName: "AreaId", labelName: "interestArea", controlName: "cbInterestArea", isDll: true, dll: [] },
                { fldName: "AttendTypeId", labelName: "attend", controlName: "cbattend", isDll: true, dll: [] },
                { fldName: "CampusId", labelName: "leadAssign", controlName: "cbLeadAssign", isDll: true, dll: [] },
                { fldName: "ExpectedStart", labelName: "expectedStart", controlName: "cbexpectedStart", isDll: false, dll: [] },
                { fldName: "grad", labelName: "grad", controlName: "dpgrad", isDll: false, dll: null },
                { fldName: "LeadStatus", labelName: "leadStatus", controlName: "cbLeadStatus", isDll: true, dll: [] },
                { fldName: "PrgVerId", labelName: "programVersion", controlName: "cbprogramVersion", isDll: false, dll: [] },
                { fldName: "ProgramID", labelName: "programLabel", controlName: "cbprogramLabel", isDll: false, dll: [] },
                { fldName: "ProgramScheduleId", labelName: "schedule", controlName: "cbschedule", isDll: true, dll: [] },
                { fldName: "CreatedDate", labelName: "DateSource", controlName: "dtDateSource", isDll: false, dll: null },
                { fldName: "AdvertisementNote", labelName: "noteSource", controlName: "txtNoteSource", isDll: false },
                { fldName: "SourceCategoryID", labelName: "categorySource", controlName: "cbcategorySource", isDll: true, dll: [] },
                { fldName: "SourceAdvertisement", labelName: "advertisementSource", controlName: "cbAdvertisementSource", isDll: true, dll: [] },
                { fldName: "SourceTypeID", labelName: "typeSource", controlName: "cbtypeSource", isDll: true, dll: [] },
                { fldName: "DateApplied", labelName: "otherDateApplied", controlName: "dtotherDateApplied", isDll: false },
                { fldName: "AssignedDate", labelName: "otherAdmRepAssignDate", controlName: "dtotherAdmRepAssignDate", isDll: false },
                { fldName: "AdmissionsRep", labelName: "otherAdmRep", controlName: "cbotherAdmRep", isDll: true, dll: [] },
                { fldName: "Sponsor", labelName: "otherSponsor", controlName: "cbotherSponsor", isDll: true, dll: [] },
                { fldName: "Comments", labelName: "otherComment", controlName: "tbOtherComment", isDll: false },
                { fldName: "PreviousEducation", labelName: "otherPreviousEducation", controlName: "cbotherPreviousEducation", isDll: true, dll: [] },
                { fldName: "HighSchool", labelName: "otherHighSchool", controlName: "cbOtherHighSchool", isDll: true, dll: [] },
                { fldName: "HighSchoolGradDate", labelName: "HsGradDate", controlName: "dtHsGradDate", isDll: false },
                { fldName: "AttendingHs", labelName: "AttendingHs", controlName: "ddAttendingHs", isDll: false },
                { fldName: "admincriteriaid", labelName: "otherAdmCriteria", controlName: "cbotherAdmCriteria", isDll: true, dll: [] },
                { fldName: "PreferredContactId", labelName: "preferredContact", controlName: "", isDll: false },
                { fldName: "PhoneType", labelName: "", controlName: "cbphone", isDll: true, dll: null },
                { fldName: "PhoneType", labelName: "", controlName: "cbphone1", isDll: true, dll: null },
                { fldName: "PhoneType", labelName: "", controlName: "cbphone2", isDll: true, dll: null },
                { fldName: "Address1", labelName: "address1", controlName: "txtAddress1", isDll: false },
                { fldName: "Address2", labelName: "address2", controlName: "txtAddress2", isDll: false },
                { fldName: "City", labelName: "city", controlName: "txtCity", isDll: false },
                { fldName: "Country", labelName: "country", controlName: "cbcountry", isDll: true, dll: [] },
                { fldName: "County", labelName: "county", controlName: "cbCounty", isDll: true, dll: [] },
                { fldName: "StateId", labelName: "", controlName: "cbState", isDll: true, dll: null },
                { fldName: "Zip", labelName: "", controlName: "txtZipCode", isDll: false },
                { fldName: "AddressType", labelName: "", controlName: "cbAddress", isDll: true, dll: [] },
                { fldName: "IsForeignPhone", labelName: "txtcheckphone", controlName: "checkphone", isDll: false },
                { fldName: "IsForeignPhone", labelName: "txtcheckphone1", controlName: "checkphone1", isDll: false },
                { fldName: "IsForeignPhone", labelName: "txtcheckphone2", controlName: "checkphone2", isDll: false },
                { fldName: "Phone", labelName: "phone", controlName: "txtphone", isDll: false },
                { fldName: "Phone2", labelName: "phone1", controlName: "txtphone1", isDll: false },
                { fldName: "Phone2", labelName: "phone2", controlName: "txtphone2", isDll: false },
                { fldName: "EmailTypeId", labelName: "", controlName: "cbemailprimary", isDll: true, dll: [] },
                { fldName: "EmailTypeId", labelName: "", controlName: "cbemail", isDll: true, dll: [] },
                { fldName: "HomeEmail", labelName: "emailprimary", controlName: "txtemailprimary", isDll: false },
                { fldName: "WorkEmail", labelName: "email", controlName: "txtemail", isDll: false },
                { fldName: "BestTimeId", labelName: "besttime", controlName: "cbBestTime", isDll: false, dll: [] },
                { fldName: "NoneEmail", labelName: "noEmail", controlName: "checkNoEmail", isDll: false },
                { fldName: "ShiftId", labelName: "", controlName: "", isDll: false }
            ];
        };
        return LeadInfoPageDb;
    })();
    AD.LeadInfoPageDb = LeadInfoPageDb;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadInfoPageVm = (function (_super) {
        __extends(LeadInfoPageVm, _super);
        function LeadInfoPageVm() {
            _super.call(this);
            this.resizeDropDown = function (e) {
                e.list.width("auto");
            };
            this.preferredMethod = "email";
            this.lastNameInitial = "";
            this.db = new AD.LeadInfoPageDb();
            this.filteredDatadb = this.db.staticInformationForCaptionRequirement();
        }
        LeadInfoPageVm.prototype.getResourcesForPage = function (islead) {
            var _this = this;
            $("body").css("cursor", "progress");
            var temp = sessionStorage.getItem(AD.XLEAD_INFO_DLLS_REQUIRED_ITEMS);
            var that = this;
            if (temp == null || temp === "{}") {
                this.captionRequirementdb = this.db.staticInformationForCaptionRequirement();
                this.db.getResourcesForPage(170, XMASTER_GET_CURRENT_CAMPUS_ID, this)
                    .done(function (msg) {
                    if (msg != undefined) {
                        var res1 = msg.RequiredList;
                        if (res1 != null) {
                            for (var i = 0; i < res1.length; i++) {
                                var fldName = res1[i].FldName;
                                for (var x = 0; x < _this.captionRequirementdb.length; x++) {
                                    if (_this.captionRequirementdb[x].fldName === fldName) {
                                        _this.captionRequirementdb[x].isrequired = res1[i].Required;
                                        _this.captionRequirementdb[x].dll = res1[i].Dll;
                                    }
                                }
                            }
                            sessionStorage.setItem(AD.XLEAD_INFO_DLLS_REQUIRED_ITEMS, JSON.stringify(_this.captionRequirementdb));
                        }
                        var res2 = msg.SdfList;
                        if (res2 != null) {
                            _this.processSdfFields(res2);
                            sessionStorage.setItem(AD.XLEAD_INFO_SDF_FIELDS, JSON.stringify(res2));
                        }
                        var res3 = msg.LeadGroupList;
                        if (res3 != null) {
                            _this.processLeadGroups(res3);
                            _this.gruppenDb = res3;
                            sessionStorage.setItem(AD.XLEAD_INFO_GROUPS, JSON.stringify(res3));
                        }
                        _this.initializePage(islead);
                    }
                })
                    .fail(function (msg) {
                    $("body").css("cursor", "default");
                    ad.SHOW_DATA_SOURCE_ERROR(msg);
                    return;
                });
            }
            else {
                that.captionRequirementdb = Object.create($.parseJSON(temp));
                var sdf = Object.create($.parseJSON(sessionStorage.getItem(AD.XLEAD_INFO_SDF_FIELDS)));
                this.processSdfFields(sdf);
                var grps = Object.create($.parseJSON(sessionStorage.getItem(AD.XLEAD_INFO_GROUPS)));
                this.gruppenDb = grps;
                this.processLeadGroups(grps);
                this.initializePage(islead);
            }
        };
        LeadInfoPageVm.prototype.processSdfFields = function (sdfList) {
            var html = '';
            var last = "none";
            for (var i = 0; i < sdfList.length; i++) {
                var requiredtext = "";
                if (sdfList[i].Required === "True") {
                    requiredtext = "<span style=color:red>*</span>";
                }
                html += "<div style='position:relative; width:50%; float:left'>";
                if (i % 2 === 0) {
                    var idcontrol = sdfList[i].SdfId;
                    html += "<label class='textLabelcustomleft' >" + sdfList[i].Description + requiredtext + "</label><input id='" + idcontrol + "' name='" + sdfList[i].Description + "' /> ";
                    last = "even";
                }
                else {
                    var idcontrol2 = sdfList[i].SdfId;
                    html += "<label class='customrightlabel'>" + sdfList[i].Description + requiredtext + "</label><input id='" + idcontrol2 + "'  name='" + sdfList[i].Description + "' /> ";
                    last = "odd";
                }
                html += "</div>";
                if (last === "odd") {
                    html += "<div class='clearfix'></div>";
                }
            }
            if (last === "even") {
                html += "<div style='position:relative; width:50%; float:left' ></div> <div class='clearfix'></div>";
            }
            $("#custom").append(html);
            for (var x = 0; x < sdfList.length; x++) {
                var idCtrl = sdfList[x].SdfId;
                if (sdfList[x].ControlType === "0" || sdfList[x].ControlType === "1") {
                    var textbox = "k-textbox fieldlabel1";
                    $("#" + idCtrl).attr("class", textbox);
                }
                if (sdfList[x].ControlType === "2") {
                    $("#" + idCtrl).kendoDropDownList({
                        dataSource: sdfList[x].ListOfValues,
                        optionLabel: 'Select option...'
                    });
                }
                if (sdfList[x].Required === "True") {
                    $("#" + idCtrl).attr('required', 'required');
                }
            }
        };
        LeadInfoPageVm.prototype.processLeadGroups = function (grps) {
            if (grps != undefined) {
                var filteredGroup = new Array();
                for (var j = 0; j < grps.length; j++) {
                    if (grps[j].CampusId === XMASTER_GET_CURRENT_CAMPUS_ID) {
                        filteredGroup.push(grps[j]);
                    }
                }
                for (var i = 0; i < filteredGroup.length; i++) {
                    var html = '<input type="checkbox" class="k-checkbox" id="' + filteredGroup[i].GroupId + '" /> <label class="k-checkbox-label f-checkbox-label" style="padding-left: 18px !important;" for="' + filteredGroup[i].GroupId + '">' + filteredGroup[i].Description + '</label>';
                    html = "<div class='leadGroupCheckboxStyle'>" + html + "</div>";
                    $("#leadGroups").append(html);
                }
            }
        };
        LeadInfoPageVm.prototype.initializePage = function (islead) {
            var campusId = ad.GET_QUERY_STRING_PARAMETER_BY_NAME("cmpId");
            this.fillFilteredDatadb(campusId);
            this.initializeControls(XMASTER_GET_CURRENT_CAMPUS_ID);
            this.configureRequired(this.captionRequirementdb);
            if (islead) {
                var leadElement = document.getElementById('leadId');
                var leadGuid = leadElement.value;
                this.getLeadDemographic(leadGuid);
            }
        };
        LeadInfoPageVm.prototype.configureRequired = function (db) {
            var caption = "";
            for (var i = 0; i < db.length; i++) {
                var label = $("#" + db[i].labelName);
                if (label != undefined) {
                    caption = label.text();
                    if (db[i].isrequired) {
                        caption = caption + "<span style=color:red>*</span>";
                        label.html(caption);
                    }
                }
                var control = $("#" + db[i].controlName);
                if (control != undefined) {
                    if (db[i].isrequired) {
                        control.attr('required', 'required');
                        control.attr('name', caption);
                        $("<span class='k-invalid-msg' data-for='" + control.attr('name') + "'></span>").insertAfter(control);
                    }
                }
            }
        };
        LeadInfoPageVm.prototype.setDllValue = function (controlName, value) {
            for (var i = 0; i < this.captionRequirementdb.length; i++) {
                if (this.captionRequirementdb[i].controlName === controlName) {
                    this.captionRequirementdb[i].dll = value;
                    break;
                }
            }
        };
        LeadInfoPageVm.prototype.getLeadDemographic = function (leadGuid) {
            var _this = this;
            var userId = $("#hdnUserId").val();
            this.db.getLeadDemographic(leadGuid, userId, this)
                .done(function (msg) {
                if (msg != undefined) {
                    try {
                        $("#txtFirstName").val(msg.FirstName);
                        $("#txtMiddleName").val(msg.MiddleName);
                        $("#txtLastName").val(msg.LastName);
                        _this.lastNameInitial = msg.LastName;
                        var pln = $("#cbPrevlast").data("kendoDropDownList");
                        pln.setDataSource(msg.LeadLastNameHistoryList);
                        if (pln.dataSource.data.length > 0) {
                            pln.select(0);
                        }
                        $("#tbNickname").val(msg.NickName);
                        $("#maskedsocialSecurity").data("kendoMaskedTextBox").value(msg.SSN);
                        $("#tdriverlicensenumber").val(msg.DriverLicenseNumber);
                        $("#tbaliennumber").val(msg.AlienNumber);
                        $("#textage").val(msg.Age);
                        $("#itextdependants").data("kendoNumericTextBox").value(msg.Dependants);
                        $("#cbsuffix").data("kendoDropDownList").value(msg.Suffix);
                        $("#cbgender").data("kendoDropDownList").value(msg.Gender);
                        $("#cbprefix").data("kendoDropDownList").value(msg.Prefix);
                        $("#cbRace").data("kendoDropDownList").value(msg.RaceId);
                        _this.calculateAge(msg.Dob, msg.Age);
                        $("#cbcitizenship").data("kendoDropDownList").value(msg.Citizenship);
                        $("#cbdependency").data("kendoDropDownList").value(msg.Dependency);
                        $("#cbmaritalstatus").data("kendoDropDownList").value(msg.MaritalStatus);
                        $("#cbfamilyincome").data("kendoDropDownList").value(msg.FamilyIncoming);
                        $("#cbhousingtype").data("kendoDropDownList").value(msg.HousingType);
                        $("#cbdriverlicensestate").data("kendoDropDownList").value(msg.DrvLicStateCode);
                        $("#cbtransportation").data("kendoDropDownList").value(msg.Transportation);
                        if (msg.DistanceToSchool != null) {
                            $("#tpTimeTo").data("kendoNumericTextBox").value(msg.DistanceToSchool);
                        }
                        for (var x = 0; x < msg.Vehicles.length; x++) {
                            if (msg.Vehicles[x].Position === 1) {
                                $("#vehicleParkingPermit1").val(msg.Vehicles[x].Permit);
                                $("#vehicleMaker1").val(msg.Vehicles[x].Make);
                                $("#vehicleModel1").val(msg.Vehicles[x].Model);
                                $("#vehicleColor1").val(msg.Vehicles[x].Color);
                                $("#vehiclePlate1").val(msg.Vehicles[x].Plate);
                            }
                            if (msg.Vehicles[x].Position === 2) {
                                $("#vehicleParkingPermit2").val(msg.Vehicles[x].Permit);
                                $("#vehicleMaker2").val(msg.Vehicles[x].Make);
                                $("#vehicleModel2").val(msg.Vehicles[x].Model);
                                $("#vehicleColor2").val(msg.Vehicles[x].Color);
                                $("#vehiclePlate2").val(msg.Vehicles[x].Plate);
                            }
                        }
                        switch (msg.PreferredContactId) {
                            case 1:
                                _this.checkradiobutton(_this.preferredMethod, "phone");
                                _this.preferredMethod = "phone";
                                break;
                            case 2:
                                _this.checkradiobutton(_this.preferredMethod, "email");
                                _this.preferredMethod = "email";
                                break;
                            case 3:
                                _this.checkradiobutton(_this.preferredMethod, "text");
                                _this.preferredMethod = "text";
                                break;
                            default:
                        }
                        $("#cbCounty").data("kendoDropDownList").value(msg.CountyId);
                        $("#cbcountry").data("kendoDropDownList").value(msg.CountryId);
                        $("#cbAddress").data("kendoDropDownList").value(msg.AddressTypeId);
                        $("#txtCity").val(msg.City);
                        var isInt = (msg.OtherState != null && msg.OtherState !== "");
                        var cbState = $("#cbState").data("kendoDropDownList");
                        var otherState = $("#otherStates");
                        var zipcode = $("#txtZipCode").data("kendoMaskedTextBox");
                        if (isInt) {
                            $("#checkIntAddress").prop('checked', true);
                            cbState.wrapper.hide();
                            otherState.css("display", "block");
                            otherState.val(msg.OtherState);
                            zipcode.setOptions({ mask: '9999999999' });
                        }
                        else {
                            $("#checkIntAddress").prop('checked', false);
                            cbState.wrapper.show();
                            otherState.css("display", "none");
                            zipcode.setOptions({ mask: '00000' });
                            cbState.value(msg.AddressStateId);
                        }
                        zipcode.value(msg.Zip);
                        $("#txtAddress1").val(msg.Address1);
                        $("#txtApart").val(msg.AddressApt);
                        $("#txtAddress2").val(msg.Address2);
                        if (msg.PhonesList != undefined && msg.PhonesList != null && msg.PhonesList.length > 0) {
                            var list = msg.PhonesList;
                            for (var i = 0; i < list.length; i++) {
                                if (list[i].Position === 1) {
                                    var isf = list[i].IsForeignPhone;
                                    $("#checkphone").prop("checked", isf);
                                    if (isf) {
                                        $("#txtPhone").data("kendoMaskedTextBox").setOptions({ mask: '999999999999999' });
                                    }
                                    $("#txtPhone").data("kendoMaskedTextBox").value(list[i].Phone);
                                    $("#hiddenPhoneId").val(list[i].ID);
                                    $("#tbphoneext").val(list[i].Extension);
                                    $("#cbphone").data("kendoDropDownList").value(list[i].PhoneTypeId);
                                }
                                if (list[i].Position === 2) {
                                    var isf1 = list[i].IsForeignPhone;
                                    $("#checkphone1").prop("checked", isf1);
                                    if (isf1) {
                                        $("#txtPhone1").data("kendoMaskedTextBox").setOptions({ mask: '999999999999999' });
                                    }
                                    $("#txtPhone1").data("kendoMaskedTextBox").value(list[i].Phone);
                                    $("#hiddenPhone1Id").val(list[i].ID);
                                    $("#tbphone1ext").val(list[i].Extension);
                                    $("#cbphone1").data("kendoDropDownList").value(list[i].PhoneTypeId);
                                }
                                if (list[i].Position === 3) {
                                    var isf2 = list[i].IsForeignPhone;
                                    $("#checkphone2").prop("checked", isf2);
                                    if (isf2) {
                                        $("#txtPhone2").data("kendoMaskedTextBox").setOptions({ mask: '999999999999999' });
                                    }
                                    $("#txtPhone2").data("kendoMaskedTextBox").value(list[i].Phone);
                                    $("#hiddenPhone2Id").val(list[i].ID);
                                    $("#tbphone2ext").val(list[i].Extension);
                                    $("#cbphone2").data("kendoDropDownList").value(list[i].PhoneTypeId);
                                }
                            }
                        }
                        if (msg.EmailList != undefined && msg.EmailList != null && msg.EmailList.length > 0) {
                            var list1 = msg.EmailList;
                            for (var j = 0; j < list1.length; j++) {
                                if (list1[j].IsPreferred === 1) {
                                    $("#txtemailprimary").val(list1[j].Email);
                                    $("#hiddenEmailPrimaryId").val(list1[j].ID);
                                    $("#cbemailprimary").data("kendoDropDownList").value(list1[j].EmailTypeId);
                                }
                                else {
                                    $("#txtemail").val(list1[j].Email);
                                    $("#hiddenEmailId").val(list1[j].ID);
                                    $("#cbemail").data("kendoDropDownList").value(list1[j].EmailTypeId);
                                }
                            }
                        }
                        if (msg.BestTime != null) {
                            $("#tpBestTime").data("kendoTimePicker").value(kendo.parseDate(msg.BestTime));
                        }
                        else {
                            $("#tpBestTime").data("kendoTimePicker").value(new Date());
                        }
                        $("#checkNoEmail").attr("checked", msg.NoneEmail);
                        $("#txtemailprimary").prop("disabled", msg.NoneEmail);
                        $("#txtemail").prop("disabled", msg.NoneEmail);
                        if (msg.NoneEmail) {
                            $("#txtemail").css("color", "white");
                            $("#txtemailprimary").css("color", "white");
                        }
                        $("#cbcategorySource").data("kendoDropDownList").value(msg.SourceCategoryId);
                        $("#cbtypeSource").data("kendoDropDownList").value(msg.SourceTypeId);
                        $("#cbAdvertisementSource").data("kendoDropDownList").value(msg.AdvertisementId);
                        $("#txtNoteSource").val(msg.Note);
                        if (msg.SourceDateTime != null) {
                            $("#dtDateSource").data("kendoDateTimePicker").value(kendo.parseDate(msg.SourceDateTime));
                        }
                        else {
                            $("#dtDateSource").data("kendoDateTimePicker").value(new Date());
                        }
                        if (msg.DateApplied != null) {
                            $("#dtotherDateApplied").data("kendoDatePicker").value(kendo.parseDate(msg.DateApplied));
                        }
                        else {
                            $("#dtotherDateApplied").data("kendoDatePicker").value(new Date());
                        }
                        if (msg.AssignedDate != null) {
                            $("#dtotherAdmRepAssignDate").data("kendoDatePicker").value(kendo.parseDate(msg.AssignedDate));
                        }
                        else {
                            $("#dtotherAdmRepAssignDate").data("kendoDatePicker").value(new Date());
                        }
                        $("#cbOtherHighSchool").data("kendoDropDownList").value(msg.HighSchoolId);
                        $("#cbotherAdmRep").data("kendoDropDownList").value(msg.AdmissionRepId);
                        $("#cbotherSponsor").data("kendoDropDownList").value(msg.AgencySponsorId);
                        $("#tbOtherComment").val(msg.Comments);
                        $("#cbotherPreviousEducation").data("kendoDropDownList").value(msg.PreviousEducationId);
                        if (msg.HighSchoolGradDate != null) {
                            $("#dtHsGradDate").data("kendoDatePicker").value(kendo.parseDate(msg.HighSchoolGradDate));
                        }
                        if (msg.AttendingHs == null) {
                            msg.AttendingHs = -1;
                        }
                        $("#ddAttendingHs").data("kendoDropDownList").value(msg.AttendingHs);
                        $("#cbotherAdmCriteria").data("kendoDropDownList").value(msg.AdminCriteriaId);
                        if (msg.CampusId == null) {
                            msg.CampusId = XMASTER_GET_CURRENT_CAMPUS_ID;
                        }
                        $("#cbLeadAssign").data("kendoDropDownList").value(msg.CampusId);
                        $("#cbInterestArea").data("kendoDropDownList").value(msg.AreaId);
                        _this.getLeadProgramInformation(msg, "ProgramID", "cbprogramLabel", msg.AreaId, msg.ProgramId);
                        $("#cbattend").data("kendoDropDownList").value(msg.AttendTypeId);
                        sessionStorage.setItem("allowedLeadStatus", msg.StateChangeIdsList);
                        var dllnew = _this.selectLeadAllowedStates(msg.StateChangeIdsList, msg.LeadStatusId);
                        var statusControl = $("#cbLeadStatus").data("kendoDropDownList");
                        statusControl.setDataSource(dllnew);
                        statusControl.value(msg.LeadStatusId);
                        if (msg.SdfList != null) {
                            for (var k = 0; k < msg.SdfList.length; k++) {
                                var id = "#" + msg.SdfList[k].SdfId;
                                if ($(id).attr("data-role") === 'dropdownlist') {
                                    $(id).data("kendoDropDownList").value(msg.SdfList[k].SdfValue);
                                }
                                else {
                                    $(id).val(msg.SdfList[k].SdfValue);
                                }
                            }
                        }
                        var grps = Object.create($.parseJSON(sessionStorage.getItem(AD.XLEAD_INFO_GROUPS)));
                        if (grps != null) {
                            for (var l = 0; l < grps.length; l++) {
                                $("#" + grps[l].GroupId).prop("checked", false);
                            }
                            if (msg.GroupIdList != null) {
                                for (var m = 0; m < msg.GroupIdList.length; m++) {
                                    $("#" + msg.GroupIdList[m]).prop("checked", true);
                                }
                            }
                        }
                        $("body").css("cursor", "default");
                    }
                    catch (e) {
                        sessionStorage.removeItem(AD.XLEAD_INFO_DLLS_REQUIRED_ITEMS);
                        sessionStorage.removeItem(AD.XLEAD_INFO_GROUPS);
                        sessionStorage.removeItem(AD.XLEAD_INFO_SDF_FIELDS);
                        $("body").css("cursor", "default");
                        alert(e.message);
                    }
                }
            })
                .fail(function (msg) {
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadInfoPageVm.prototype.selectLeadAllowedStates = function (allowedStatusChange, leadStatusId) {
            var dllnew = new kendo.data.DataSource();
            var dll = this.getDropDownListItemObject("cbLeadStatus");
            if (allowedStatusChange !== null && allowedStatusChange !== undefined && allowedStatusChange.length > 0) {
                for (var n = 0; n < dll.length; n++) {
                    for (var o = 0; o < allowedStatusChange.length; o++) {
                        if (dll[n].ID.toLocaleString().toUpperCase() === allowedStatusChange[o].toLocaleString().toUpperCase()) {
                            dllnew.add(dll[n]);
                        }
                    }
                }
            }
            var result = $.grep(dll, function (t) { return t.ID === leadStatusId; });
            dllnew.add(result[0]);
            return dllnew;
        };
        LeadInfoPageVm.prototype.calculateAge = function (dob, age) {
            if (dob != null && dob !== "") {
                $("#dtDob").data("kendoDatePicker").value(new Date(dob));
                var calAge = ad.GET_AGE(dob);
                $("#textage").val(calAge.toString());
            }
            else {
                $("#textage").val(age);
            }
        };
        LeadInfoPageVm.prototype.checkradiobutton = function (old, sel) {
            var rfd = $("input[name='contact'][value= '" + old + "'").attr("_rfddecoratedID");
            $("#" + rfd).attr("class", "rfdRadioUnchecked");
            var rfddecoratedId = $("input[name='contact'][value= '" + sel + "'").attr("_rfddecoratedID");
            $("#" + rfddecoratedId).attr("class", "rfdRadioChecked");
        };
        LeadInfoPageVm.prototype.getDropDownListItemObject = function (controlName) {
            for (var i = 0; i < this.captionRequirementdb.length; i++) {
                if (this.captionRequirementdb[i].controlName === controlName) {
                    return this.captionRequirementdb[i].dll;
                }
            }
            return null;
        };
        LeadInfoPageVm.prototype.getFilteredDropDownListItemObject = function (controlName) {
            for (var i = 0; i < this.captionRequirementdb.length; i++) {
                if (this.filteredDatadb[i].controlName === controlName) {
                    return this.filteredDatadb[i].dll;
                }
            }
            return null;
        };
        LeadInfoPageVm.prototype.initializeControls = function (campus, id) {
            if (id === void 0) { id = ""; }
            var cbLeadStatus = $("#cbLeadStatus").data("kendoDropDownList");
            var dll = this.getDropDownListItemObject("cbLeadStatus");
            if (id === "") {
                for (var i = 0; i < dll.length; i++) {
                    if (dll[i].Description.indexOf("**") === 0) {
                        id = dll[i].ID;
                        dll[i].Description = dll[i].Description.replace("**", "");
                        break;
                    }
                }
            }
            var cad = sessionStorage.getItem("allowedLeadStatus");
            var allowedStates;
            if (cad !== undefined && cad !== null && cad.length > 10) {
                allowedStates = cad.split(",");
            }
            else {
                allowedStates = [];
            }
            dll = this.selectLeadAllowedStates(allowedStates, id);
            cbLeadStatus.setDataSource(dll);
            cbLeadStatus.value(id);
            cbLeadStatus.list.css("min-width", "150px");
            cbLeadStatus.list.width("auto");
            var cbLeadAssign = $("#cbLeadAssign").data("kendoDropDownList");
            cbLeadAssign.setDataSource(this.getDropDownListItemObject("cbLeadAssign"));
            cbLeadAssign.value(campus);
            var cbInterestArea = $("#cbInterestArea").data("kendoDropDownList");
            cbInterestArea.setDataSource(this.getFilteredDropDownListItemObject("cbInterestArea"));
            var cbattend = $("#cbattend").data("kendoDropDownList");
            cbattend.setDataSource(this.getFilteredDropDownListItemObject("cbattend"));
            var cbprefix = $("#cbprefix").data("kendoDropDownList");
            cbprefix.setDataSource(this.getDropDownListItemObject("cbprefix"));
            cbprefix.list.css("min-width", "90px");
            cbprefix.list.width("auto");
            var cbsuffix = $("#cbsuffix").data("kendoDropDownList");
            cbsuffix.setDataSource(this.getDropDownListItemObject("cbsuffix"));
            cbsuffix.list.css("min-width", "90px");
            cbsuffix.list.width("auto");
            var cbgender = $("#cbgender").data("kendoDropDownList");
            cbgender.setDataSource(this.getDropDownListItemObject("cbgender"));
            cbgender.list.css("min-width", "90px");
            cbgender.list.width("auto");
            var cbmaritalstatus = $("#cbmaritalstatus").data("kendoDropDownList");
            cbmaritalstatus.setDataSource(this.getDropDownListItemObject("cbmaritalstatus"));
            var cbdriverlicensestate = $("#cbdriverlicensestate").data("kendoDropDownList");
            cbdriverlicensestate.setDataSource(this.getDropDownListItemObject("cbdriverlicensestate"));
            var cbcitizenship = $("#cbcitizenship").data("kendoDropDownList");
            cbcitizenship.setDataSource(this.getDropDownListItemObject("cbcitizenship"));
            var cbdependency = $("#cbdependency").data("kendoDropDownList");
            cbdependency.setDataSource(this.getDropDownListItemObject("cbdependency"));
            var cbfamilyincome = $("#cbfamilyincome").data("kendoDropDownList");
            cbfamilyincome.setDataSource(this.getDropDownListItemObject("cbfamilyincome"));
            var cbhousingtype = $("#cbhousingtype").data("kendoDropDownList");
            cbhousingtype.setDataSource(this.getDropDownListItemObject("cbhousingtype"));
            var cbtransportation = $("#cbtransportation").data("kendoDropDownList");
            cbtransportation.setDataSource(this.getDropDownListItemObject("cbtransportation"));
            var cbRace = $("#cbRace").data("kendoDropDownList");
            cbRace.setDataSource(this.getFilteredDropDownListItemObject("cbRace"));
            cbRace.list.css("min-width", "90px");
            cbRace.list.width("auto");
            var cbcategorySource = $("#cbcategorySource").data("kendoDropDownList");
            cbcategorySource.setDataSource(this.getDropDownListItemObject("cbcategorySource"));
            var cbtypeSource = $("#cbtypeSource").data("kendoDropDownList");
            cbtypeSource.setDataSource(this.getDropDownListItemObject("cbtypeSource"));
            var cbAdvertisementSource = $("#cbAdvertisementSource").data("kendoDropDownList");
            cbAdvertisementSource.setDataSource(this.getDropDownListItemObject("cbAdvertisementSource"));
            var cbphone = $("#cbphone").data("kendoDropDownList");
            cbphone.setDataSource(this.getDropDownListItemObject("cbphone"));
            var cbphone1 = $("#cbphone1").data("kendoDropDownList");
            cbphone1.setDataSource(this.getDropDownListItemObject("cbphone1"));
            var cbphone2 = $("#cbphone2").data("kendoDropDownList");
            cbphone2.setDataSource(this.getDropDownListItemObject("cbphone2"));
            var cbAddress = $("#cbAddress").data("kendoDropDownList");
            cbAddress.setDataSource(this.getFilteredDropDownListItemObject("cbAddress"));
            var cbemailprimary = $("#cbemailprimary").data("kendoDropDownList");
            cbemailprimary.setDataSource(this.getDropDownListItemObject("cbemailprimary"));
            var cbemail = $("#cbemail").data("kendoDropDownList");
            cbemail.setDataSource(this.getDropDownListItemObject("cbemail"));
            var cbcountry = $("#cbcountry").data("kendoDropDownList");
            cbcountry.setDataSource(this.getFilteredDropDownListItemObject("cbcountry"));
            var cbCounty = $("#cbCounty").data("kendoDropDownList");
            cbCounty.setDataSource(this.getFilteredDropDownListItemObject("cbCounty"));
            var cbState = $("#cbState").data("kendoDropDownList");
            cbState.setDataSource(this.getDropDownListItemObject("cbState"));
            var cbotherAdmRep = $("#cbotherAdmRep").data("kendoDropDownList");
            cbotherAdmRep.setDataSource(this.getFilteredDropDownListItemObject("cbotherAdmRep"));
            var cbotherPreviousEducation = $("#cbotherPreviousEducation").data("kendoDropDownList");
            cbotherPreviousEducation.setDataSource(this.getFilteredDropDownListItemObject("cbotherPreviousEducation"));
            var cbOtherHighSchool = $("#cbOtherHighSchool").data("kendoDropDownList");
            cbOtherHighSchool.setDataSource(this.getFilteredDropDownListItemObject("cbOtherHighSchool"));
            var cbotherAdmCriteria = $("#cbotherAdmCriteria").data("kendoDropDownList");
            cbotherAdmCriteria.setDataSource(this.getFilteredDropDownListItemObject("cbotherAdmCriteria"));
            var cbotherReasonNot = $("#cbotherReasonNot").data("kendoDropDownList");
            cbotherReasonNot.list.width("auto");
            var cbotherSponsor = $("#cbotherSponsor").data("kendoDropDownList");
            cbotherSponsor.list.width("auto");
            this.resizeDropDown(cbprefix);
            this.resizeDropDown(cbsuffix);
            this.resizeDropDown(cbgender);
            this.resizeDropDown(cbdependency);
            this.resizeDropDown(cbmaritalstatus);
            this.resizeDropDown(cbfamilyincome);
            this.resizeDropDown(cbhousingtype);
            this.resizeDropDown(cbdriverlicensestate);
            this.resizeDropDown(cbtransportation);
            this.resizeDropDown(cbcitizenship);
            this.resizeDropDown(cbattend);
            this.resizeDropDown(cbLeadStatus);
            this.resizeDropDown(cbphone);
            this.resizeDropDown(cbphone1);
            this.resizeDropDown(cbphone2);
            this.resizeDropDown(cbState);
            this.resizeDropDown(cbAddress);
            this.resizeDropDown(cbemailprimary);
            this.resizeDropDown(cbemail);
            this.resizeDropDown(cbotherSponsor);
            this.resizeDropDown(cbotherPreviousEducation);
            this.resizeDropDown(cbotherReasonNot);
        };
        LeadInfoPageVm.prototype.fillFilteredDatadb = function (campusId) {
            for (var i = 0; i < this.captionRequirementdb.length; i++) {
                if (this.captionRequirementdb[i].dll == undefined || this.captionRequirementdb[i].dll.length === 0) {
                    continue;
                }
                this.filteredDatadb[i].dll = [];
                for (var x = 0; x < this.captionRequirementdb[i].dll.length; x++) {
                    if (this.captionRequirementdb[i].dll[x].CampusesIdList != undefined
                        && this.captionRequirementdb[i].dll[x].CampusesIdList.length > 0) {
                        var list = this.captionRequirementdb[i].dll[x].CampusesIdList;
                        for (var j = 0; j < list.length; j++) {
                            if (list[j].CampusId === campusId) {
                                this.filteredDatadb[i].dll.push(this.captionRequirementdb[i].dll[x]);
                                break;
                            }
                        }
                    }
                }
            }
        };
        LeadInfoPageVm.prototype.onClickContactInformation = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/leadContacts.aspx?RESID=270&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Quick&mod=AD");
        };
        LeadInfoPageVm.prototype.getLeadProgramInformation = function (msgOr, fldName, targetDropDown, value, setting) {
            var _this = this;
            if (setting === void 0) { setting = null; }
            this.db.getLeadItemInformation(fldName, value, XMASTER_GET_CURRENT_CAMPUS_ID, this)
                .done(function (msg) {
                if (msg != undefined) {
                    var cbprogramLabel = $("#" + targetDropDown).data("kendoDropDownList");
                    cbprogramLabel.setDataSource(msg);
                    if (setting != null) {
                        cbprogramLabel.value(setting);
                    }
                    _this.getLeadProgramTypeInformation(msgOr, "PrgVerId", "cbprogramVersion", msgOr.ProgramId, msgOr.PrgVerId);
                    if (XMASTER_LEAD_IS_CLOCK_HOUR === 'Yes') {
                        $("#dpExpectedStart").data("kendoDatePicker").value(kendo.parseDate(msgOr.ExpectedStart));
                    }
                    else {
                        if (msgOr.ExpectedStart == null) {
                            _this.getExpectedLeadItemInformation(XMASTER_GET_CURRENT_CAMPUS_ID, "ExpectedStart", "cbExpectedStart", msgOr.ProgramId);
                        }
                        else {
                            var dat1 = kendo.parseDate(msgOr.ExpectedStart);
                            var lookdate = dat1.toLocaleString("en-US").split(" ")[0].toString();
                            lookdate = lookdate.replace(",", "");
                            _this.getExpectedLeadItemInformation(XMASTER_GET_CURRENT_CAMPUS_ID, "ExpectedStart", "cbExpectedStart", msgOr.ProgramId, lookdate);
                        }
                    }
                }
            })
                .fail(function (msg) {
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadInfoPageVm.prototype.getLeadProgramTypeInformation = function (msgOr, fldName, targetDropDown, value, setting) {
            var _this = this;
            if (setting === void 0) { setting = null; }
            this.db.getLeadItemInformation(fldName, value, XMASTER_GET_CURRENT_CAMPUS_ID, this)
                .done(function (msg) {
                if (msg != undefined) {
                    var cbprogramVersion = $("#" + targetDropDown).data("kendoDropDownList");
                    cbprogramVersion.setDataSource(msg);
                    if (setting != null) {
                        cbprogramVersion.value(setting);
                    }
                    _this.getItemInformation(XMASTER_GET_CURRENT_CAMPUS_ID, "ProgramScheduleId", "cbschedule", msgOr.PrgVerId, msgOr.ScheduleId);
                }
            })
                .fail(function (msg) {
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadInfoPageVm.prototype.getExpectedLeadItemInformation = function (campusId, fldName, targetDropDown, value, setting) {
            if (setting === void 0) { setting = null; }
            this.db.getLeadItemInformation(fldName, value, campusId, this)
                .done(function (msg) {
                if (msg != undefined) {
                    var cbExpected = $("#" + targetDropDown).data("kendoDropDownList");
                    cbExpected.setDataSource(msg);
                    if (setting != null) {
                        var list = cbExpected.dataSource.data();
                        if (list != null) {
                            for (var i = 0; i < list.length; i++) {
                                var descriptdate = list[i].Description.split("]")[0].slice(1).toString().trim();
                                if (descriptdate.localeCompare(setting) == 0) {
                                    cbExpected.value(list[i].ID);
                                    break;
                                }
                            }
                        }
                    }
                }
            })
                .fail(function (msg) {
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadInfoPageVm.prototype.getItemInformation = function (campusId, fldName, targetDropDown, value, setting) {
            if (setting === void 0) { setting = null; }
            this.db.getLeadItemInformation(fldName, value, campusId, this)
                .done(function (msg) {
                if (msg != undefined) {
                    var itemdd = $("#" + targetDropDown).data("kendoDropDownList");
                    itemdd.setDataSource(msg);
                    if (setting != null) {
                        itemdd.value(setting);
                    }
                }
            })
                .fail(function (msg) {
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadInfoPageVm.prototype.postLeadInformationToServer = function (leadGuid, avoidDuplicate) {
            var _this = this;
            $("body").css("cursor", "progress");
            var lead = new AD.LeadInfoOutput();
            var newLead = sessionStorage.getItem(AD.XLEAD_NEW);
            if (newLead != null && newLead === "true") {
                lead.LeadId = '00000000-0000-0000-0000-000000000000';
                lead.AvoidDuplicateAnalysis = avoidDuplicate;
            }
            else {
                lead.LeadId = leadGuid;
            }
            var userId = XMASTER_PAGE_USER_OPTIONS_USERID;
            lead.InputModel.UserId = userId;
            lead.InputModel.CampusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            lead.InputModel.LeadId = lead.LeadId;
            lead.ModUser = userId;
            var preferedContactFunction = function () {
                switch (this.preferredMethod) {
                    case "phone":
                        return 1;
                    case "email":
                        return 2;
                    case "text":
                        return 3;
                    default:
                        return 1;
                }
            };
            lead.FirstName = $("#txtFirstName").val();
            lead.MiddleName = $("#txtMiddleName").val();
            lead.LastName = $("#txtLastName").val();
            if (this.lastNameInitial !== "") {
                if (this.lastNameInitial !== lead.LastName) {
                    var isOfficial = window.confirm("Is this an official name change?");
                    if (isOfficial) {
                        var hist = new AD.LastNameHistory();
                        hist.LastName = this.lastNameInitial;
                        hist.LeadId = lead.LeadId;
                        lead.LeadLastNameHistoryList = [];
                        lead.LeadLastNameHistoryList.push(hist);
                        this.lastNameInitial = lead.LastName;
                    }
                }
            }
            lead.NickName = $("#tbNickname").val();
            lead.SSN = $("#maskedsocialSecurity").data("kendoMaskedTextBox").value();
            lead.DriverLicenseNumber = $("#tdriverlicensenumber").val();
            lead.AlienNumber = $("#tbaliennumber").val();
            lead.Age = $("#textage").val();
            lead.Dependants = $("#itextdependants").data("kendoNumericTextBox").value();
            lead.DistanceToSchool = $("#tpTimeTo").data("kendoNumericTextBox").value();
            lead.Suffix = $("#cbsuffix").data("kendoDropDownList").value();
            lead.Gender = $("#cbgender").data("kendoDropDownList").value();
            lead.Prefix = $("#cbprefix").data("kendoDropDownList").value();
            lead.RaceId = $("#cbRace").data("kendoDropDownList").value();
            lead.Dob = $("#dtDob").data("kendoDatePicker").value();
            lead.Citizenship = $("#cbcitizenship").data("kendoDropDownList").value();
            lead.Dependency = $("#cbdependency").data("kendoDropDownList").value();
            lead.MaritalStatus = $("#cbmaritalstatus").data("kendoDropDownList").value();
            lead.FamilyIncoming = $("#cbfamilyincome").data("kendoDropDownList").value();
            lead.HousingType = $("#cbhousingtype").data("kendoDropDownList").value();
            lead.DrvLicStateCode = $("#cbdriverlicensestate").data("kendoDropDownList").value();
            lead.Transportation = $("#cbtransportation").data("kendoDropDownList").value();
            var veh = new AD.Vehicle();
            veh.Permit = $("#vehicleParkingPermit1").val();
            veh.Make = $("#vehicleMaker1").val();
            veh.Model = $("#vehicleModel1").val();
            veh.Color = $("#vehicleColor1").val();
            veh.Plate = $("#vehiclePlate1").val();
            veh.Position = 1;
            lead.Vehicles.push(veh);
            var veh2 = new AD.Vehicle();
            veh2.Permit = $("#vehicleParkingPermit2").val();
            veh2.Make = $("#vehicleMaker2").val();
            veh2.Model = $("#vehicleModel2").val();
            veh2.Color = $("#vehicleColor2").val();
            veh2.Plate = $("#vehiclePlate2").val();
            veh2.Position = 2;
            lead.Vehicles.push(veh2);
            lead.PreferredContactId = preferedContactFunction();
            lead.CountyId = $("#cbCounty").data("kendoDropDownList").value();
            lead.CountryId = $("#cbcountry").data("kendoDropDownList").value();
            lead.AddressTypeId = $("#cbAddress").data("kendoDropDownList").value();
            lead.City = $("#txtCity").val();
            lead.OtherState = $("#otherStates").val();
            lead.AddressStateId = $("#cbState").data("kendoDropDownList").value();
            lead.Zip = $("#txtZipCode").data("kendoMaskedTextBox").value();
            lead.Address1 = $("#txtAddress1").val();
            lead.AddressApt = $("#txtApart").val();
            lead.Address2 = $("#txtAddress2").val();
            lead.PhonesList = [];
            var phone = new AD.Phone();
            phone.Extension = $("#tbphoneext").val();
            phone.ID = $("#hiddenPhoneId").val();
            phone.IsForeignPhone = ($("#checkphone").prop("checked"));
            phone.Phone = $("#txtPhone").data("kendoMaskedTextBox").raw();
            phone.PhoneTypeId = $("#cbphone").data("kendoDropDownList").value();
            phone.Position = 1;
            lead.PhonesList.push(phone);
            var phone1 = new AD.Phone();
            phone1.Extension = $("#tbphone1ext").val();
            phone1.ID = $("#hiddenPhone1Id").val();
            phone1.IsForeignPhone = ($("#checkphone1").prop("checked"));
            phone1.Phone = $("#txtPhone1").data("kendoMaskedTextBox").raw();
            phone1.PhoneTypeId = $("#cbphone1").data("kendoDropDownList").value();
            phone1.Position = 2;
            lead.PhonesList.push(phone1);
            var phone2 = new AD.Phone();
            phone2.Extension = $("#tbphone2ext").val();
            phone2.ID = $("#hiddenPhone2Id").val();
            phone2.IsForeignPhone = ($("#checkphone2").prop("checked"));
            phone2.Phone = $("#txtPhone2").data("kendoMaskedTextBox").raw();
            phone2.PhoneTypeId = $("#cbphone2").data("kendoDropDownList").value();
            phone2.Position = 3;
            lead.PhonesList.push(phone2);
            lead.NoneEmail = ($("#checkNoEmail").prop("checked"));
            lead.EmailList = [];
            var email = new AD.Email();
            email.Email = $("#txtemailprimary").val();
            email.EmailType = $("#cbemailprimary").data("kendoDropDownList").value();
            email.ID = $("#hiddenEmailPrimaryId").val();
            email.IsPreferred = 1;
            lead.EmailList.push(email);
            var email1 = new AD.Email();
            email1.Email = $("#txtemail").val();
            email1.ID = $("#hiddenEmailId").val();
            email1.EmailType = $("#cbemail").data("kendoDropDownList").value();
            email1.IsPreferred = 0;
            lead.EmailList.push(email1);
            lead.BestTime = $("#tpBestTime").data("kendoTimePicker").value();
            lead.SourceCategoryId = $("#cbcategorySource").data("kendoDropDownList").value();
            lead.SourceTypeIdI = $("#cbtypeSource").data("kendoDropDownList").value();
            lead.AdvertisementId = $("#cbAdvertisementSource").data("kendoDropDownList").value();
            lead.SourceDateTime = $("#dtDateSource").data("kendoDateTimePicker").value();
            lead.Note = $("#txtNoteSource").val();
            lead.DateApplied = $("#dtotherDateApplied").data("kendoDatePicker").value();
            lead.AssignedDate = $("#dtotherAdmRepAssignDate").data("kendoDatePicker").value();
            lead.AdmissionRepId = $("#cbotherAdmRep").data("kendoDropDownList").value();
            lead.PreviousEducationId = $("#cbotherPreviousEducation").data("kendoDropDownList").value();
            lead.HighSchoolGradDate = $("#dtHsGradDate").data("kendoDatePicker").value();
            lead.HighSchoolId = $("#cbOtherHighSchool").data("kendoDropDownList").value();
            lead.Comments = $("#tbOtherComment").val();
            lead.AttendingHs = ($("#ddAttendingHs").data("kendoDropDownList").value() === "1");
            lead.AgencySponsorId = $("#cbotherSponsor").data("kendoDropDownList").value();
            lead.AdminCriteriaId = $("#cbotherAdmCriteria").data("kendoDropDownList").value();
            lead.CampusId = $("#cbLeadAssign").data("kendoDropDownList").value();
            lead.AreaId = $("#cbInterestArea").data("kendoDropDownList").value();
            lead.ProgramId = $("#cbprogramLabel").data("kendoDropDownList").value();
            lead.PrgVerId = $("#cbprogramVersion").data("kendoDropDownList").value();
            lead.ScheduleId = $("#cbschedule").data("kendoDropDownList").value();
            lead.ExpectedStart = this.processExpectedStart();
            lead.AttendTypeId = $("#cbattend").data("kendoDropDownList").value();
            lead.LeadStatusId = $("#cbLeadStatus").data("kendoDropDownList").value();
            var $inputs = $("#custom :input");
            lead.SdfList = [];
            $inputs.each(function () {
                var sdf = new AD.Sdf();
                sdf.SdfId = $(this).prop("id");
                sdf.SdfValue = $(this).val();
                lead.SdfList.push(sdf);
            });
            lead.GroupIdList = new Array();
            if (this.gruppenDb != null) {
                var filteredGroup = new Array();
                for (var j = 0; j < this.gruppenDb.length; j++) {
                    if (this.gruppenDb[j].CampusId === XMASTER_GET_CURRENT_CAMPUS_ID) {
                        filteredGroup.push(this.gruppenDb[j]);
                    }
                }
                for (var l = 0; l < filteredGroup.length; l++) {
                    if ($("#" + filteredGroup[l].GroupId).prop("checked") === true) {
                        lead.GroupIdList.push(filteredGroup[l].GroupId);
                    }
                }
            }
            this.db.postLeadInformation(lead, this)
                .done(function (msg) {
                if (msg != undefined) {
                    $("body").css("cursor", "default");
                    if (lead.LeadId === '00000000-0000-0000-0000-000000000000') {
                        if (msg.ValidationErrorMessages != null && msg.ValidationErrorMessages !== "") {
                            alert(msg.ValidationErrorMessages);
                            return;
                        }
                        if (msg.DuplicatedMessages != null && msg.ValidationErrorMessages !== "") {
                            var dupgrid = $("#infoPageDupGrid").data("kendoGrid");
                            var dsk = new kendo.data.DataSource({
                                data: msg.PossiblesDuplicatesList
                            });
                            dupgrid.setDataSource(dsk);
                            var dupWin = $("#WindowsDuplicate").data("kendoWindow");
                            dupWin.center().open();
                            return;
                        }
                        alert("Lead saved successful");
                        sessionStorage.setItem("NewLead", "false");
                        var url = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&NewEnt=1&cmpId=" + lead.CampusId;
                        window.location.assign(url);
                        return;
                    }
                    alert("Lead saved successful");
                    if (msg.ValidStatusChangeList != null) {
                        sessionStorage.setItem("allowedLeadStatus", msg.ValidStatusChangeList);
                        var dllnew = _this.selectLeadAllowedStates(msg.StateChangeIdsList, lead.LeadStatusId);
                        var statusControl = $("#cbLeadStatus").data("kendoDropDownList");
                        statusControl.setDataSource(dllnew);
                        statusControl.value(msg.LeadStatusId);
                    }
                    if (lead.CampusId !== XMASTER_GET_CURRENT_CAMPUS_ID) {
                        var url1 = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&NewEnt=1&cmpId=" + lead.CampusId;
                        window.location.assign(url1);
                    }
                    var fullname = lead.FirstName;
                    fullname += (lead.NickName == null || lead.NickName === "") ? " " : " (" + lead.NickName + ") ";
                    fullname += lead.LastName;
                    var expectedStart;
                    if (lead.ExpectedStart == null) {
                        expectedStart = "";
                    }
                    else {
                        expectedStart = lead.ExpectedStart.toString();
                    }
                    AD.LeadInfoBarVm.refreshLeadFields(fullname, $("#cbprogramVersion").data("kendoDropDownList").text(), expectedStart, $("#cbotherAdmRep").data("kendoDropDownList").text(), lead.AssignedDate.toString(), $("#cbLeadStatus").data("kendoDropDownList").text());
                }
            })
                .fail((function (msg) {
                $("body").css("cursor", "default");
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            }));
        };
        LeadInfoPageVm.prototype.deleteLead = function (leadGuid) {
            var _this = this;
            var firstName = $("#txtFirstName").val();
            var lastname = $("#txtLastName").val();
            var r = confirm(AD.X_DELETE_ARE_YOU_SURE + firstName + " " + lastname);
            if (r) {
                var db = new AD.LeadInfoPageDb();
                db.getIsLeadErasable(leadGuid, this)
                    .done(function (msg) {
                    if (msg === "false") {
                        alert(AD.X_DELETE_NOT_POSSIBLE);
                    }
                    else {
                        db.postLeadDelete(leadGuid, _this)
                            .done(function () {
                            alert(AD.X_DELETE_SUCESS);
                            sessionStorage.setItem(AD.XLEAD_NEW, "true");
                            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&NewEnt=1&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageLead&mod=AD");
                        })
                            .fail(function (msg1) {
                            ad.SHOW_DATA_SOURCE_ERROR(msg1);
                        });
                    }
                })
                    .fail(function (msg) {
                    ad.SHOW_DATA_SOURCE_ERROR(msg);
                });
            }
        };
        LeadInfoPageVm.prototype.refreshCampusInControl = function (campusId) {
            var _this = this;
            var lStatus = $("#cbLeadStatus").data("kendoDropDownList").value();
            var userId = $("#hdnUserId").val();
            this.db.getAllowedStatus(campusId, lStatus, userId, this)
                .done(function (msg) {
                sessionStorage.setItem("allowedLeadStatus", msg.StateChangeIdsList);
                _this.initializeControls(campusId, lStatus);
                var cbprogramVersion = $("#cbprogramVersion").data("kendoDropDownList");
                var cbschedule = $("#cbschedule").data("kendoDropDownList");
                var cbexpectedStart = $("#cbExpectedStart").data("kendoDropDownList");
                var cbprogramLabel = $("#cbprogramLabel").data("kendoDropDownList");
                cbprogramLabel.setDataSource(new kendo.data.DataSource());
                cbschedule.setDataSource(new kendo.data.DataSource());
                cbprogramVersion.setDataSource(new kendo.data.DataSource());
                cbexpectedStart.setDataSource(new kendo.data.DataSource());
            })
                .fail(function (msg) {
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadInfoPageVm.prototype.processExpectedStart = function () {
            var expectedStart;
            if (XMASTER_LEAD_IS_CLOCK_HOUR === 'Yes') {
                expectedStart = $("#dpExpectedStart").data("kendoDatePicker").value();
            }
            else {
                var description = $("#cbExpectedStart").data("kendoDropDownList").text();
                if (description == null || description === "" || description === AD.X_SELECT_ITEM) {
                    expectedStart = null;
                }
                else {
                    var descriptdate = description.split("]")[0].slice(1).toString().trim();
                    expectedStart = new Date(descriptdate);
                }
            }
            return expectedStart;
        };
        LeadInfoPageVm.prototype.fillPrintInfo = function (p) {
            p.preferredContact = this.preferredMethod;
            var bestTime = $("#tpBestTime").data("kendoTimePicker").value();
            p.bestTime = (bestTime !== null) ? ad.FORMAT_DATE_TO_MM_DD_YYYY(bestTime) : "";
            p.address = $("#txtAddress1").val() + " " + $("#txtApart").val();
            p.phoneBest = $("#txtPhone").data("kendoMaskedTextBox").value();
            p.phone1 = $("#txtPhone1").data("kendoMaskedTextBox").value();
            p.phone2 = $("#txtPhone2").data("kendoMaskedTextBox").value();
            p.emailBest = $("#txtemailprimary").val();
            p.email = $("#txtemail").val();
            p.county = $("#cbCounty").data("kendoDropDownList").text();
            p.country = $("#cbcountry").data("kendoDropDownList").text();
            var state = "";
            if (($("#checkIntAddress").prop("checked"))) {
                state = $("#txtCity").val();
            }
            else {
                state = $("#cbState").data("kendoDropDownList").text();
                state = (state === "Select") ? "" : state;
            }
            p.cityStateZip = $("#txtCity").val() + ", " + state + ", " + $("#txtZipCode").val();
            p.area = $("#cbInterestArea").data("kendoDropDownList").text();
            p.program = $("#cbprogramLabel").data("kendoDropDownList").text();
            p.attend = $("#cbattend").data("kendoDropDownList").text();
            p.schedule = $("#cbschedule").data("kendoDropDownList").text();
            var expectedGrad = $("#dtGrad").data("kendoDatePicker").value();
            p.expectedGrad = (expectedGrad !== null && expectedGrad !== undefined) ? ad.FORMAT_DATE_TO_MM_DD_YYYY(expectedGrad) : "";
            p.campusName = $("#cbLeadAssign").data("kendoDropDownList").text();
            p.programVersion = $("#cbprogramVersion").data("kendoDropDownList").text();
            p.status = $("#cbLeadStatus").data("kendoDropDownList").text();
            var expectedStart = this.processExpectedStart();
            p.expectedStart = (expectedStart !== null && expectedStart !== undefined) ? ad.FORMAT_DATE_TO_MM_DD_YYYY(expectedStart) : "";
            p.photopath = $("#infobarImage").attr("src");
            p.leadFullName = $("#txtFirstName").val();
            p.leadFullName += $("#txtMiddleName").val() === "" ? "" : " " + $("#txtMiddleName").val();
            p.leadFullName += " " + $("#txtLastName").val();
            p.ssn = $("#maskedsocialSecurity").data("kendoMaskedTextBox").raw().replace(/^\d{5}/, "*****");
            p.dobAge = "";
            var dob = $("#dtDob").data("kendoDatePicker").value();
            var age = $("#textage").val();
            if (dob !== null || age !== "") {
                if (dob == null) {
                    p.dobAge = " ? - " + $("#textage").val();
                }
                else {
                    p.dobAge = ad.FORMAT_DATE_TO_MM_DD_YYYY(dob) + " - " + $("#textage").val();
                }
            }
            p.gender = $("#cbgender").data("kendoDropDownList").text();
            p.race = $("#cbRace").data("kendoDropDownList").text();
            p.citizenship = $("#cbcitizenship").data("kendoDropDownList").text();
            p.dependency = $("#cbdependency").data("kendoDropDownList").text();
            var dependents = $("#itextdependants").data("kendoNumericTextBox").value();
            p.dependents = dependents !== null && dependents !== undefined ? dependents.toString() : "";
            p.alienNumber = $("#tbaliennumber").val();
            p.maritalStatus = $("#cbmaritalstatus").data("kendoDropDownList").text();
            p.familyIncoming = $("#cbfamilyincome").data("kendoDropDownList").text();
            p.housing = $("#cbhousingtype").data("kendoDropDownList").text();
            p.driverLicState = $("#cbdriverlicensestate").data("kendoDropDownList").text();
            p.driverLicNumber = $("#tdriverlicensenumber").val();
            p.transportation = $("#cbtransportation").data("kendoDropDownList").text();
            var distanceToSchool = $("#tpTimeTo").data("kendoNumericTextBox").value();
            p.distanceToSchool = distanceToSchool !== null && distanceToSchool !== undefined ? distanceToSchool.toString() + " Miles" : "";
            p.category = $("#cbcategorySource").data("kendoDropDownList").text();
            p.advertisement = $("#cbAdvertisementSource").data("kendoDropDownList").text();
            p.typeAdvertisement = $("#cbtypeSource").data("kendoDropDownList").text();
            p.note = $("#txtNoteSource").val();
            var createdate = $("#dtDateSource").data("kendoDateTimePicker").value();
            p.createdDate = (createdate !== null && createdate !== undefined) ? ad.FORMAT_DATE_TO_MM_DD_YYYY(createdate) : "";
            var dateApplied = $("#dtotherDateApplied").data("kendoDatePicker").value();
            p.dateApplied = (dateApplied !== null && dateApplied !== undefined) ? ad.FORMAT_DATE_TO_MM_DD_YYYY(dateApplied) : "";
            p.previousEducation = $("#cbotherPreviousEducation").data("kendoDropDownList").text();
            p.sponsor = $("#cbotherSponsor").data("kendoDropDownList").text();
            var dateAssigned = $("#dtotherAdmRepAssignDate").data("kendoDatePicker").value();
            p.dateAssigned = (dateAssigned !== null && dateAssigned !== undefined) ? ad.FORMAT_DATE_TO_MM_DD_YYYY(dateAssigned) : "";
            p.admissionRep = $("#cbotherAdmRep").data("kendoDropDownList").text();
            p.highSchool = $("#cbOtherHighSchool").data("kendoDropDownList").text();
            p.adminCriteria = $("#cbotherAdmCriteria").data("kendoDropDownList").text();
            var hsGradeDate = $("#dtHsGradDate").data("kendoDatePicker").value();
            p.hsGradeDate = (hsGradeDate !== null && hsGradeDate !== undefined) ? ad.FORMAT_DATE_TO_MM_DD_YYYY(hsGradeDate) : "";
            p.attendingHs = $("#ddAttendingHs").data("kendoDropDownList").text();
            p.reasonNotEnrolled = "";
            p.comments = $("#tbOtherComment").val();
            p.leadGroups = " ";
            var filteredGroup = new Array();
            for (var j = 0; j < this.gruppenDb.length; j++) {
                if (this.gruppenDb[j].CampusId === XMASTER_GET_CURRENT_CAMPUS_ID) {
                    filteredGroup.push(this.gruppenDb[j]);
                }
            }
            for (var l = 0; l < filteredGroup.length; l++) {
                if ($("#" + filteredGroup[l].GroupId).prop("checked") === true) {
                    if (p.leadGroups.length > 3) {
                        p.leadGroups += ", ";
                    }
                    p.leadGroups += filteredGroup[l].Description;
                }
            }
            var $inputs = $("#custom :input");
            p.userDf = [];
            $inputs.each(function () {
                var sdf = new AD.Userdf();
                sdf.label = $(this).prop("name");
                sdf.value = $(this).val();
                p.userDf.push(sdf);
            });
            return p;
        };
        LeadInfoPageVm.prototype.executeCustomValidators = function () {
            var error = "";
            var ssn = $("#maskedsocialSecurity").data("kendoMaskedTextBox");
            if (ssn.value() !== "") {
                if (ssn.value().indexOf(ssn.options.promptChar) !== -1) {
                    error = "SNN Number is incomplete! \n";
                }
            }
            var dob = $("#dtDob").val();
            if (dob !== null && dob !== undefined && dob !== "") {
                if (kendo.parseDate(dob) == null) {
                    error += "Invalid DOB!\n";
                }
            }
            if (error.length > 1) {
                error = "The following Fields have errors:\n" + error;
                alert(error);
                return false;
            }
            return true;
        };
        LeadInfoPageVm.prototype.storeVehicleInformation = function () {
            this.storeVehicles = [];
            this.storeVehicles.push($("#vehicleParkingPermit1").val());
            this.storeVehicles.push($("#vehicleMaker1").val());
            this.storeVehicles.push($("#vehicleModel1").val());
            this.storeVehicles.push($("#vehicleColor1").val());
            this.storeVehicles.push($("#vehiclePlate1").val());
            this.storeVehicles.push($("#vehicleParkingPermit2").val());
            this.storeVehicles.push($("#vehicleMaker2").val());
            this.storeVehicles.push($("#vehicleModel2").val());
            this.storeVehicles.push($("#vehicleColor2").val());
            this.storeVehicles.push($("#vehiclePlate2").val());
        };
        LeadInfoPageVm.prototype.cancelVehicleEdition = function () {
            $("#vehiclePlate2").val(this.storeVehicles.pop());
            $("#vehicleColor2").val(this.storeVehicles.pop());
            $("#vehicleModel2").val(this.storeVehicles.pop());
            $("#vehicleMaker2").val(this.storeVehicles.pop());
            $("#vehicleParkingPermit2").val(this.storeVehicles.pop());
            $("#vehiclePlate1").val(this.storeVehicles.pop());
            $("#vehicleColor1").val(this.storeVehicles.pop());
            $("#vehicleModel1").val(this.storeVehicles.pop());
            $("#vehicleMaker1").val(this.storeVehicles.pop());
            $("#vehicleParkingPermit1").val(this.storeVehicles.pop());
        };
        return LeadInfoPageVm;
    })(kendo.Observable);
    AD.LeadInfoPageVm = LeadInfoPageVm;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadPrint = (function () {
        function LeadPrint() {
            var info = JSON.parse(sessionStorage.getItem("PRINT_INFO"));
            var unselected = "Select";
            document.getElementById("printCampus").innerHTML = info.campusName;
            var d = new Date();
            document.getElementById("printDateTime").innerHTML = ad.FORMAT_DATE_TO_MM_DD_YYYY(d);
            $("#printImage").attr("src", info.photopath);
            $("#hr1text2").val(info.leadFullName);
            $("#hr1text4").val(info.admissionRep === unselected ? "" : info.admissionRep);
            $("#hr2text2").val(info.status);
            $("#hr2text4").val(info.dateAssigned);
            $("#hr3text2").val(info.programVersion === unselected ? "" : info.programVersion);
            $("#hr3text4").val("");
            $("#hr4text2").val(info.expectedStart);
            $("#valPreferContact").val(info.preferredContact);
            $("#valPhoneBest").val(info.bestTime);
            $("#valPhone").val(info.phoneBest);
            $("#valPhone2").val(info.phone1);
            $("#valPhone3").val(info.phone2);
            $("#valEmailBest").val(info.emailBest);
            $("#valEmail").val(info.email);
            $("#valAddress").val(info.address);
            $("#valCounty").val(info.county === unselected ? "" : info.county);
            $("#valCityStateZip").val(info.cityStateZip);
            $("#valCountry").val(info.country === unselected ? "" : info.country);
            $("#valArea").val(info.area === unselected ? "" : info.area);
            $("#valProgram").val(info.program === unselected ? "" : info.program);
            $("#valAttend").val(info.attend === unselected ? "" : info.attend);
            $("#valSchedule").val(info.schedule === unselected ? "" : info.schedule);
            $("#valExpectedGrad").val(info.expectedGrad);
            $("#valSsn").val(info.ssn);
            $("#valDobAge").val(info.dobAge);
            $("#valGender").val(info.gender === unselected ? "" : info.gender);
            $("#valRace").val(info.race === unselected ? "" : info.race);
            $("#valCitizenship").val(info.citizenship === unselected ? "" : info.citizenship);
            $("#valDependency").val(info.dependency === unselected ? "" : info.dependency);
            $("#valDependents").val(info.dependents);
            $("#valAlienNumber").val(info.alienNumber);
            $("#valMaritalStatus").val(info.maritalStatus === unselected ? "" : info.maritalStatus);
            $("#valFamilyIncoming").val(info.familyIncoming === unselected ? "" : info.familyIncoming);
            $("#valHousing").val(info.housing === unselected ? "" : info.housing);
            $("#valDriverLic").val(info.driverLicState === unselected ? "" : info.driverLicState);
            $("#valDriverNo").val(info.driverLicNumber);
            $("#valTransportation").val(info.transportation === unselected ? "" : info.transportation);
            $("#valDistToSchool").val(info.distanceToSchool);
            $("#valDisabled").val(info.disabled);
            $("#valCategory").val(info.category === unselected ? "" : info.category);
            $("#valAdvertisement").val(info.advertisement === unselected ? "" : info.advertisement);
            $("#valType").val(info.typeAdvertisement === unselected ? "" : info.typeAdvertisement);
            $("#valCreatedDate").val(info.createdDate);
            $("#valNote").val(info.note);
            $("#valDateApplied").val(info.dateApplied);
            $("#valPreviousEducation").val(info.previousEducation === unselected ? "" : info.previousEducation);
            $("#valSponsor").val(info.sponsor === unselected ? "" : info.sponsor);
            $("#valHs").val(info.highSchool === unselected ? "" : info.highSchool);
            $("#valAdminCriteria").val(info.adminCriteria === unselected ? "" : info.adminCriteria);
            $("#valHsGradeDate").val(info.hsGradeDate);
            $("#valHsAttending").val(info.attendingHs === unselected ? "" : info.attendingHs);
            $("#valComments").val(info.comments);
            $("#valReasonNotEnrolled").val(info.reasonNotEnrolled);
            $("#valGroups").val(info.leadGroups);
            for (var i = 0; i < info.userDf.length; i++) {
                var template = this.customTemplate(i.toString(), info.userDf[i].label, info.userDf[i].value);
                $("#customPlaceHolder").append(template);
            }
            window.print();
            window.onfocus = function () {
                window.close();
            };
        }
        LeadPrint.prototype.customTemplate = function (index, label, value) {
            var template = "<div class='corpusRow' ><label id='customL" + index + "1'" +
                " class='corpus1x' >" + label + "</label>" +
                "<input id= 'customI" + index + "' class='corpuslargeM' value= '" + value +
                "' /> <div class='clearfix' > </div> </div>";
            return template;
        };
        return LeadPrint;
    })();
    AD.LeadPrint = LeadPrint;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var Userdf = (function () {
        function Userdf() {
        }
        return Userdf;
    })();
    AD.Userdf = Userdf;
    var PrintOutputModel = (function () {
        function PrintOutputModel() {
        }
        return PrintOutputModel;
    })();
    AD.PrintOutputModel = PrintOutputModel;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadNotes = (function () {
        function LeadNotes() {
            var _this = this;
            try {
                this.bo = new AD.LeadNotesBo();
                $("#notesGrid").kendoGrid({
                    pdf: {
                        allPages: true
                    },
                    dataSource: [],
                    pageable: true,
                    sortable: true,
                    columns: [
                        {
                            field: "NoteDate",
                            title: "Date",
                            width: "100px",
                            template: "#=  (NoteDate == null)? '' : kendo.toString(kendo.parseDate(NoteDate, 'yyyy-MM-dd'), 'MM/dd/yy hh:mm:ss tt') #"
                        },
                        {
                            field: "ModuleName",
                            title: "Module",
                            width: "100px"
                        },
                        {
                            field: "Source",
                            title: "Source",
                            width: "110px"
                        },
                        {
                            field: "Type",
                            title: "Type",
                            width: "100px",
                            editor: this.bo.getTypeNotesEditor,
                            template: kendo.template($("#notesTypeGridStyleTemplate").html())
                        },
                        {
                            field: "Field",
                            title: "Field",
                            width: "100px"
                        },
                        {
                            field: "NoteText",
                            title: "Note",
                            editor: this.bo.textareaEditor,
                            template: "#=NoteText#"
                        },
                        {
                            field: "UserName",
                            title: "User",
                            width: "140px"
                        },
                        { command: [{ name: "edit", text: " " }], title: "Edit", width: "50px" }],
                    editable: "popup",
                    edit: function (e) {
                        if (e.model.isNew()) {
                            e.container.data("kendoWindow").title("Enter a New Note");
                        }
                        else {
                            e.container.data("kendoWindow").title("Edit the Note");
                        }
                        _this.setPopupDimensions(e);
                    },
                    dataBound: function (e) {
                        _this.bo.controlCommandEditButton(e);
                    }
                });
                $("#notesModulesDdl").kendoDropDownList({
                    dataSource: [{ text: "Admissions", value: "AD" }],
                    dataTextField: "text",
                    dataValueField: "value"
                });
                var grid = $("#notesGrid").data("kendoGrid");
                var dllModule = $("#notesModulesDdl").data("kendoDropDownList");
                grid.setDataSource(this.bo.getLeadNotesInfo(dllModule.value(), 1));
                var newRow = document.getElementById("notesPlusImage");
                newRow.addEventListener('click', this.bo.addNewRow, false);
                $("#notesPrintbutton").click(function (e) {
                    var grid = $("#notesGrid").data("kendoGrid");
                    grid.saveAsPDF();
                });
            }
            catch (e) {
                $("body").css("cursor", "default");
                alert(e.message + "/n" + e.stack);
            }
        }
        LeadNotes.prototype.setPopupDimensions = function (ev) {
            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(670).height(400).data("kendoWindow").center();
            }, 100);
        };
        return LeadNotes;
    })();
    AD.LeadNotes = LeadNotes;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadNotesBo = (function () {
        function LeadNotesBo() {
        }
        LeadNotesBo.prototype.addNewRow = function () {
            var grid = $("#notesGrid").data("kendoGrid");
            grid.addRow();
            return null;
        };
        LeadNotesBo.prototype.getLeadNotesInfo = function (moduleCode, command) {
            var leadElement = document.getElementById('leadId');
            var userId = $("#hdnUserId").val();
            var leadId = leadElement.value;
            this.dataSource = new kendo.data.DataSource({
                transport: {
                    read: function (options) {
                        $.ajax({
                            url: AD.XGET_SERVICE_LAYER_NOTES,
                            type: "GET",
                            dataType: "json",
                            data: {
                                ModuleCode: moduleCode,
                                LeadId: leadId,
                                Command: command,
                                CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                UserId: XMASTER_PAGE_USER_OPTIONS_USERID
                            },
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    update: function (options) {
                        var info = new AD.LeadNotesInfo();
                        info.NotesList = new Array();
                        var notes = new AD.Notes();
                        notes.IdNote = options.data.models[0].IdNote;
                        notes.ModuleName = options.data.models[0].ModuleName;
                        notes.NoteDate = options.data.models[0].NoteDate;
                        notes.NoteText = options.data.models[0].NoteText;
                        notes.Source = options.data.models[0].Source;
                        notes.Field = options.data.models[0].Source;
                        notes.UserId = options.data.models[0].UserId;
                        notes.UserName = options.data.models[0].UserName;
                        notes.Type = options.data.models[0].Type;
                        var inputInfo = new AD.LeadNotesInputInfo();
                        inputInfo.Command = 1;
                        inputInfo.LeadId = leadId;
                        inputInfo.UserId = userId;
                        info.Filter = inputInfo;
                        info.NotesList.push(notes);
                        $.ajax({
                            type: "POST",
                            dataType: "text",
                            url: AD.XPOST_SERVICE_LAYER_NOTES,
                            data: JSON.stringify(info),
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    create: function (options) {
                        var info = new AD.LeadNotesInfo();
                        info.NotesList = new Array();
                        var notes = new AD.Notes();
                        notes.IdNote = options.data.models[0].IdNote;
                        notes.ModuleName = options.data.models[0].ModuleName;
                        notes.NoteDate = options.data.models[0].NoteDate;
                        notes.NoteText = options.data.models[0].NoteText;
                        notes.Source = options.data.models[0].Source;
                        notes.Field = options.data.models[0].Field;
                        notes.PageFieldId = options.data.models[0].PageFieldId;
                        notes.UserId = XMASTER_PAGE_USER_OPTIONS_USERID;
                        notes.UserName = options.data.models[0].UserName;
                        notes.Type = options.data.models[0].Type;
                        var inputInfo = new AD.LeadNotesInputInfo();
                        inputInfo.Command = 2;
                        inputInfo.LeadId = leadId;
                        inputInfo.UserId = XMASTER_PAGE_USER_OPTIONS_USERID;
                        info.Filter = inputInfo;
                        info.NotesList.push(notes);
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: AD.XPOST_SERVICE_LAYER_NOTES,
                            data: JSON.stringify(info),
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                        return null;
                    }
                },
                batch: true,
                pageSize: 12,
                schema: {
                    data: "NotesList",
                    total: function (response) { return response.NotesList.length; },
                    model: {
                        id: "IdNote",
                        fields: {
                            IdNote: { editable: false, nullable: false },
                            ModuleName: {
                                editable: false,
                                validation: { required: true },
                                defaultValue: $("#notesModulesDdl").data("kendoDropDownList").text()
                            },
                            NoteText: {
                                type: "string",
                                validation: { required: true }
                            },
                            Type: { type: "string"
                            },
                            Source: {
                                editable: false,
                                type: "string",
                                defaultValue: "Notes"
                            },
                            UserName: {
                                editable: false,
                                type: "string",
                                defaultValue: XMASTER_PAGE_USER_OPTIONS_USERNAME
                            },
                            UserId: { editable: false },
                            Field: {
                                editable: false,
                                type: "string",
                                defaultValue: "Note"
                            },
                            PageFieldId: {
                                editable: false,
                                defaultValue: 1
                            },
                            NoteDate: {
                                editable: false,
                                type: "Date",
                                defaultValue: new Date()
                            }
                        }
                    }
                }
            });
            return this.dataSource;
        };
        LeadNotesBo.prototype.getTypeNotesEditor = function (container, options) {
            $('<input required name="Note Type" data-text-field="text" data-value-field="text"  data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                dataSource: [{ text: "Notes" }, { text: "Confidential" }],
                optionLabel: "Select"
            });
        };
        LeadNotesBo.prototype.textareaEditor = function (container, options) {
            $('<textarea name="Note Text" required maxlength="2000" data-bind="value: ' + options.field + '" cols="60" rows="5" ></textarea>')
                .appendTo(container);
        };
        LeadNotesBo.prototype.controlCommandEditButton = function (e) {
            var dataItems = e.sender.dataSource.view();
            for (var j = 0; j < dataItems.length; j++) {
                var userId = dataItems[j].get("UserId");
                var sourceFieldId = dataItems[j].get("PageFieldId");
                var row = e.sender.tbody.find("[data-uid='" + dataItems[j].uid + "']");
                if (userId.toUpperCase() !== XMASTER_PAGE_USER_OPTIONS_USERID.toUpperCase() || sourceFieldId !== 1) {
                    var editButton = row.find(".k-grid-edit");
                    editButton.hide();
                }
            }
        };
        return LeadNotesBo;
    })();
    AD.LeadNotesBo = LeadNotesBo;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var Notes = (function () {
        function Notes() {
        }
        return Notes;
    })();
    AD.Notes = Notes;
    var LeadNotesInputInfo = (function () {
        function LeadNotesInputInfo() {
        }
        return LeadNotesInputInfo;
    })();
    AD.LeadNotesInputInfo = LeadNotesInputInfo;
    var LeadNotesInfo = (function () {
        function LeadNotesInfo() {
        }
        return LeadNotesInfo;
    })();
    AD.LeadNotesInfo = LeadNotesInfo;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadQueue = (function () {
        function LeadQueue() {
            var _this = this;
            try {
                this.vm = new AD.LeadQueueVm();
                var ethis = this;
                var phonew = $("#queueWindowsPhone").kendoWindow({
                    actions: ["Close"],
                    title: "Edit Phone Number",
                    width: "400px",
                    height: "150px",
                    visible: false,
                    modal: false,
                    resizable: true
                }).data("kendoWindow");
                var grid = $("#queueGrid").kendoGrid({
                    dataSource: {
                        data: [],
                        pageSize: 15
                    },
                    sortable: true,
                    filterable: false,
                    resizable: true,
                    pageable: true,
                    columns: [
                        { field: "Id", title: "", width: 1 },
                        {
                            field: "FirstName",
                            title: "First Name",
                            attributes: { style: "font-size:11px;" },
                            template: '<a href="\\#" class = "link">#=FirstName# </a>',
                            width: 100
                        },
                        {
                            field: "LastName",
                            title: "Last Name",
                            attributes: { style: "font-size:11px;" }
                        },
                        {
                            field: "Status",
                            title: "Status",
                            attributes: { style: "font-size:11px;" },
                            template: kendo.template($("#queueGridStatusTemplate").html())
                        },
                        {
                            field: "ProgramInterest",
                            title: "Program Interest",
                            attributes: {
                                title: "tooltip",
                                style: "font-size:11px;"
                            }
                        },
                        {
                            field: "Received",
                            title: "Received",
                            attributes: { style: "font-size:11px; text-align:center" },
                            width: 75,
                            sortable: false
                        },
                        {
                            title: "Activity",
                            attributes: { style: "text-align:center;" },
                            columns: [
                                {
                                    field: "LastActivity",
                                    title: "Last Activity"
                                },
                                {
                                    field: "DateLast",
                                    title: "Date",
                                    template: "#=  (DateLast == null)? '' : kendo.toString(kendo.parseDate(DateLast, 'yyyy-MM-dd'), 'MM/dd/yy') #",
                                    attributes: { style: "font-size:11px; text-align:center" },
                                    width: 75,
                                    sortable: false
                                },
                                {
                                    field: "Age",
                                    title: "Age",
                                    attributes: { style: "font-size:11px; text-align:center" },
                                    width: 65,
                                    sortable: false
                                },
                                {
                                    field: "NextActivity",
                                    title: "Next Activity",
                                    attributes: { style: "font-size:11px;" }
                                },
                                {
                                    field: "DateNext",
                                    title: "Date",
                                    template: "#=  (DateNext == null)? '' : kendo.toString(kendo.parseDate(DateNext, 'yyyy-MM-dd'), 'MM/dd/yy') #",
                                    attributes: { style: "font-size:11px; text-align:center" },
                                    width: 75,
                                    sortable: false
                                }
                            ]
                        },
                        {
                            field: "Phone",
                            title: "Phone",
                            attributes: {
                                style: "font-size:11px;"
                            },
                            template: "<span class='cellphone'> #=Phone# <span> ",
                            width: 95
                        },
                        {
                            field: "",
                            title: "Edit Phone",
                            template: "<div class='clickpencil'><img src='../images/Edit_bluePencil.gif' /></div>",
                            width: 50
                        },
                        {
                            field: "",
                            title: "Email",
                            template: "<div class='clickemail'><img src='../images/Email2.gif' /></div>",
                            width: 50
                        },
                        { field: "IsInternational", title: "", width: 1 }
                    ]
                }).data("kendoGrid");
                grid.table.kendoTooltip({
                    filter: "td[title]",
                    content: function (x) {
                        var target = x.target;
                        return $(target).text();
                    }
                });
                $("#queuesavePhone").kendoButton({
                    click: function () {
                        _this.vm.savePhone(_this.selectedRow, grid);
                    }
                });
                $("#queuecancelPhone").kendoButton({
                    click: function () {
                        phonew.close();
                    }
                });
                var maskphone = $("#queuePhoneEdit").kendoMaskedTextBox({}).data("kendoMaskedTextBox");
                $('#queueInternationalPhone').change(function () {
                    var maskedtextbox = $("#queuePhoneEdit").data("kendoMaskedTextBox");
                    var raw = maskedtextbox.raw();
                    if ($(this).is(":checked")) {
                        maskedtextbox.setOptions({ mask: '999999999999999' });
                    }
                    else {
                        maskedtextbox.setOptions({ mask: '(000)-000-0000' });
                    }
                    maskedtextbox.value(raw);
                });
                grid.table.on('click', '.link', function (e) {
                    _this.vm.commandGotoLeadPage(grid, e);
                });
                grid.table.on('click', '.clickpencil', function (e) {
                    var w = $("#queueWindowsPhone").data("kendoWindow");
                    _this.selectedRow = grid.dataItem($(e.currentTarget).closest("tr"));
                    var inter = _this.selectedRow.IsInternational;
                    ($("#queueInternationalPhone").prop("checked", inter));
                    if (inter) {
                        maskphone.setOptions({ mask: "999999999999999" });
                    }
                    else {
                        maskphone.setOptions({ mask: "(999) 000-0000" });
                    }
                    maskphone.value(_this.selectedRow.Phone);
                    w.open();
                    w.center();
                });
                grid.table.on('click', '.clickemail', function (e) {
                    _this.selectedRow = grid.dataItem($(e.currentTarget).closest("tr"));
                    var leadId = _this.selectedRow.Id;
                    ad.OPEN_TASK_RAD_EMAIL_WINDOW_WITH_ENTITY(leadId, 4);
                });
                $("#ddlqueueAdmm").kendoDropDownList({
                    dataSource: [{}],
                    optionLabel: "Select",
                    dataTextField: "Description",
                    dataValueField: "ID",
                    change: function (e) {
                        e.preventDefault();
                        var value = this.value();
                        if (value === "") {
                            var grid = $("#queueGrid").data("kendoGrid");
                            var ds = new kendo.data.DataSource();
                            grid.setDataSource(ds);
                            alert('Please Select a valid value');
                            return;
                        }
                        var vm = new AD.LeadQueueVm();
                        vm.getInfoFromServer(2, value);
                    }
                });
                this.vm.getInfoFromServer();
                kendo.init($("#containerInfoPage"));
                kendo.bind($("#containerInfoPage"), ethis.vm);
            }
            catch (e) {
                $("body").css("cursor", "default");
                alert(e.message + "/n" + e.stack);
            }
        }
        LeadQueue.prototype.myparseDate = function (data) {
            if (data == null) {
                return "";
            }
            else {
                return kendo.toString(kendo.parseDate(data, "yyyy-MM-dd"), "MM/dd/yy");
            }
        };
        return LeadQueue;
    })();
    AD.LeadQueue = LeadQueue;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadQueueDb = (function () {
        function LeadQueueDb() {
        }
        LeadQueueDb.prototype.getInfoFromServer = function (assingRepId, campusId, userId, command, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XGET_SERVICE_LAYER_QUEUE_COMMAND_GET +
                    "?AssingRepId=" + assingRepId +
                    "&UserId=" + userId +
                    "&CampusID=" + campusId +
                    "&Command=" + command,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        };
        LeadQueueDb.prototype.setLeadInMru = function (campusId, userId, leadId, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XPOST_SERVICE_LAYER_SET_MRU + "?UserId=" + userId + "&CampusId=" + campusId + "&EntityId=" + leadId + "&TypeEntity=4",
                type: 'POST',
                timeout: 20000,
                dataType: 'text'
            });
        };
        LeadQueueDb.prototype.postLeadNewPhone = function (phone, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XPOST_PHONE_SERVICE_LAYER_SET_MRU,
                data: JSON.stringify(phone),
                type: 'POST',
                timeout: 20000,
                dataType: 'json'
            });
        };
        return LeadQueueDb;
    })();
    AD.LeadQueueDb = LeadQueueDb;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadQueueVm = (function () {
        function LeadQueueVm() {
            this.db = new AD.LeadQueueDb();
        }
        LeadQueueVm.prototype.getInfoFromServer = function (command, assignedId) {
            var _this = this;
            if (command === void 0) { command = 0; }
            if (assignedId === void 0) { assignedId = $("#hdnUserId").val(); }
            var campusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            var userId = $("#hdnUserId").val();
            this.db.getInfoFromServer(assignedId, campusId, userId, command, this)
                .done(function (msg) {
                if (msg != undefined) {
                    try {
                        var dda = $("#ddlqueueAdmm").data("kendoDropDownList");
                        if (command !== 2) {
                            dda.setDataSource(msg.AdmissionRepList);
                            for (var i = 0; i < msg.AdmissionRepList.length; i++) {
                                if (msg.AdmissionRepList[i].ID === userId) {
                                    dda.select(i + 1);
                                }
                            }
                        }
                        var isshow = msg.QueueInfoObj.ShowAdmissionRep;
                        if (isshow) {
                            dda.enable(true);
                        }
                        else {
                            dda.enable(false);
                        }
                        var grid = $("#queueGrid").data("kendoGrid");
                        var dataSource;
                        var infoitems = msg.LeadQueueInfoList;
                        if (infoitems !== undefined && infoitems !== null && infoitems.length > 0) {
                            dataSource = new kendo.data.DataSource({
                                data: msg.LeadQueueInfoList,
                                pageSize: 15
                            });
                        }
                        else {
                            dataSource = new kendo.data.DataSource();
                        }
                        grid.setDataSource(dataSource);
                        var t = "[" + dataSource.total().toString() + "]";
                        $("#queueQuantity").text(t);
                        var item = dataSource.at(0);
                        if (item != undefined) {
                            $("#queueRefresh").text(item.Received);
                        }
                        var refresh = msg.QueueInfoObj.RefreshQueueInterval * 1000;
                        if (refresh < 30000) {
                            refresh = 30000;
                        }
                        window.setInterval(_this.refreshInfoFromServer, refresh);
                        $("body").css("cursor", "default");
                    }
                    catch (e) {
                        $("body").css("cursor", "default");
                        alert(e.message);
                    }
                }
            })
                .fail(function (msg) {
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadQueueVm.prototype.commandGotoLeadPage = function (grid, e) {
            var campusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            var userId = $("#hdnUserId").val();
            var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
            this.db.setLeadInMru(campusId, userId, dataItem.Id, this)
                .done(function (msg) {
                if (msg != undefined) {
                    try {
                        sessionStorage.setItem("NewLead", "false");
                        var url = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&NewEnt=1&cmpId=" + campusId;
                        window.location.assign(url);
                        $("body").css("cursor", "default");
                    }
                    catch (e) {
                        $("body").css("cursor", "default");
                        alert(e.message);
                    }
                }
            })
                .fail(function (msg) {
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadQueueVm.prototype.savePhone = function (selectedRow, grid) {
            var maskphone = $("#queuePhoneEdit").data("kendoMaskedTextBox");
            if (selectedRow.Phone === maskphone.raw()) {
                return;
            }
            var phone = new AD.Phone();
            phone.Position = 1;
            phone.IsForeignPhone = ($("#queueInternationalPhone").prop("checked"));
            phone.Phone = maskphone.raw();
            phone.UserId = $("#hdnUserId").val();
            phone.LeadId = selectedRow.Id;
            this.db.postLeadNewPhone(phone, this)
                .done(function (msg) {
                if (msg != undefined) {
                    try {
                        selectedRow.set("Phone", msg.Phone);
                        selectedRow.set("IsInternational", msg.IsForeignPhone);
                        var phonew = $("#queueWindowsPhone").data("kendoWindow");
                        phonew.close();
                        $("body").css("cursor", "default");
                    }
                    catch (e) {
                        $("body").css("cursor", "default");
                        alert(e.message);
                    }
                }
            })
                .fail(function (msg) {
                var phonew = $("#queueWindowsPhone").data("kendoWindow");
                phonew.close();
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadQueueVm.prototype.refreshInfoFromServer = function () {
            var dd = $("#ddlqueueAdmm").data("kendoDropDownList");
            var selected = dd.value();
            if (selected !== "") {
                var command = 2;
                try {
                    $("body").css("cursor", "wait");
                    var db1 = new AD.LeadQueueDb();
                    var userId = $("#hdnUserId").val();
                    db1.getInfoFromServer(selected, XMASTER_GET_CURRENT_CAMPUS_ID, userId, command, this)
                        .done(function (msg) {
                        if (msg != undefined) {
                            try {
                                var grid = $("#queueGrid").data("kendoGrid");
                                var dataSource;
                                var infoitems = msg.LeadQueueInfoList;
                                if (infoitems !== undefined && infoitems !== null && infoitems.length > 0) {
                                    dataSource = new kendo.data.DataSource({
                                        data: msg.LeadQueueInfoList,
                                        pageSize: 15
                                    });
                                }
                                else {
                                    dataSource = new kendo.data.DataSource();
                                }
                                grid.setDataSource(dataSource);
                                $("#queueQuantity").text(dataSource.total().toString());
                                var item = dataSource.at(0);
                                if (item != undefined) {
                                    $("#queueRefresh").text(item.Received);
                                }
                                $("body").css("cursor", "default");
                            }
                            catch (e) {
                                $("body").css("cursor", "default");
                                alert(e.message);
                            }
                        }
                    });
                }
                catch (e) {
                    $("body").css("cursor", "default");
                    alert(e.message);
                }
            }
        };
        return LeadQueueVm;
    })();
    AD.LeadQueueVm = LeadQueueVm;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadToolbar = (function () {
        function LeadToolbar() {
            try {
                this.viewModel = new AD.LeadToolbarVm();
                var ethis = this;
                var toolbar = $("#LeadToolbar").kendoToolBar({
                    resizable: true,
                    items: [
                        { id: "leadToolBarNew", type: "button", spriteCssClass: "tb-new", text: "New", showIcon: "toolbar", overflow: "never" },
                        { id: "leadToolBarSave", type: "button", text: "Save", spriteCssClass: "tb-save", showIcon: "toolbar", overflow: "never" },
                        { id: "leadToolBarAlert", type: "button", text: "Alert", spriteCssClass: "tb-alert", showIcon: "toolbar", click: ethis.viewModel.goToAlarmPage, overflow: "never" },
                        { id: "btn04", type: "button", text: "Quick Lead", spriteCssClass: "tb-plus", showIcon: "toolbar", click: ethis.viewModel.goToLeadQuickScreen, overflow: "never" },
                        { id: "btnPriorLead", type: "button", text: "Prior Lead", spriteCssClass: "tb-arrow-left", showIcon: "toolbar", showText: "overflow", click: ethis.viewModel.goToSetQueueLead, overflow: "never" },
                        { id: "btn06", type: "button", text: "Lead", click: ethis.viewModel.goToLeadInfoScreen, overflow: "never" },
                        { id: "btnNextLead", type: "button", text: "Next Lead", spriteCssClass: "tb-arrow-right", showIcon: "toolbar", showText: "overflow", click: ethis.viewModel.goToSetQueueLead, overflow: "never" },
                        { id: "btn08", type: "button", text: "Task", spriteCssClass: "tb-redcheckmark", showIcon: "toolbar", click: ethis.viewModel.goToLeadTaskScreen, overflow: "never" },
                        { id: "btn09", type: "button", text: "Appt", spriteCssClass: "tb-event", showIcon: "toolbar", click: ethis.viewModel.goToLeadScheduleScreen, overflow: "never" },
                        { id: "btn10", type: "button", text: "Email", spriteCssClass: "tb-email", showIcon: "toolbar", click: ethis.viewModel.goToEmailPage, overflow: "never" },
                        { id: "btn11", type: "button", text: "Notes", spriteCssClass: "tb-notepad", showIcon: "toolbar", click: ethis.viewModel.goToLeadNotesScreen, overflow: "never" },
                        { id: "btn12", type: "button", text: "Requirements", spriteCssClass: "tb-requirement", showIcon: "toolbar", click: ethis.viewModel.goToLeadRequirementScreen, overflow: "never" },
                        { id: "btn13", type: "button", text: "History", spriteCssClass: "tb-books", showIcon: "toolbar", click: ethis.viewModel.goToLeadHistoryScreen, overflow: "never" },
                        { id: "btn14", type: "button", text: "Prior Ed", spriteCssClass: "tb-prior-ed", showIcon: "toolbar", click: ethis.viewModel.goToLeadHistoryScreen, overflow: "never" },
                        { id: "btn15", type: "button", text: "Prior Work", spriteCssClass: "tb-priorwork", showIcon: "toolbar", click: ethis.viewModel.goToSPriorWorkScreen, overflow: "never" },
                        { id: "btn16", type: "button", text: "Extra Curricular", spriteCssClass: "tb-extracurricular", showIcon: "toolbar", enable: true, click: ethis.viewModel.goToLeadExtracurricularScreen, overflow: "never" },
                        { id: "btn17", type: "button", text: "Skills", spriteCssClass: "tb-skill", showIcon: "toolbar", click: ethis.viewModel.goToSkillScreen, overflow: "never" },
                        { id: "btn18", type: "button", text: "Enroll", spriteCssClass: "tb-greenCheckMark", showIcon: "toolbar", overflow: "never", click: ethis.viewModel.goToLeadEnrollmentScreen },
                        { id: "leadPrintButton", type: "button", text: "Print", spriteCssClass: "tb-printer-Icon-16x16", showIcon: "toolbar", overflow: "never" },
                        { id: "leadToolBarDelete", type: "button", text: "Delete", spriteCssClass: "tb-delete", showIcon: "toolbar", overflow: "never" }
                    ]
                });
                $("#leadToolBarWindow").kendoWindow({
                    actions: ["Close"],
                    visible: false,
                    width: 320,
                    height: 100,
                    title: "Select a page"
                });
                $("#alertToolBar").kendoToolBar({
                    resizable: false,
                    items: [
                        { id: "btnQueue", type: "button", text: "Lead Queue", spriteCssClass: "tb-alert", showIcon: "toolbar", click: ethis.viewModel.goToQueuePage },
                        { id: "btnTask", type: "button", text: "Task", spriteCssClass: "tb-alert", showIcon: "toolbar", click: ethis.viewModel.goToLeadTaskScreen },
                        { id: "btnAppt", type: "button", text: "Appointments", spriteCssClass: "tb-alert", showIcon: "toolbar", click: ethis.viewModel.goToLeadScheduleScreen },
                    ]
                });
                ethis.viewModel.getFromServerFlagInformation();
                setInterval(ethis.viewModel.getFromServerFlagInformation, 180000);
            }
            catch (e) {
                alert(e.message + "/n" + e.stack);
            }
        }
        return LeadToolbar;
    })();
    AD.LeadToolbar = LeadToolbar;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadToolBarDb = (function () {
        function LeadToolBarDb() {
        }
        LeadToolBarDb.prototype.getFromServerFlagInformation = function (campusId, userId, command, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XGET_SERVICE_LAYER_TOOLBAR_FLAGS + "?Command=" + command + "&UserId=" + userId,
                type: "GET",
                timeout: 20000,
                dataType: "json"
            });
        };
        LeadToolBarDb.prototype.setFromServerQueueLead = function (campusId, userId, leadId, command, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XGET_SERVICE_LAYER_TOOLBAR_FLAGS + "?Command=" + command
                    + "&UserId=" + userId
                    + "&CampusID=" + campusId
                    + "&LeadId=" + leadId,
                type: "GET",
                timeout: 20000,
                dataType: "json"
            });
        };
        return LeadToolBarDb;
    })();
    AD.LeadToolBarDb = LeadToolBarDb;
})(AD || (AD = {}));
var AD;
(function (AD) {
    var LeadToolbarVm = (function (_super) {
        __extends(LeadToolbarVm, _super);
        function LeadToolbarVm() {
            _super.call(this);
            this.db = new AD.LeadInfoBarDb();
            sessionStorage.setItem(AD.X_FLAG_LEAD, "0");
            sessionStorage.setItem(AD.X_FLAG_TASK, "0");
            sessionStorage.setItem(AD.X_FLAG_APPO, "0");
        }
        LeadToolbarVm.prototype.goToAlarmPage = function () {
            var wnd = $("#leadToolBarWindow").data("kendoWindow");
            var sume = 0;
            sume += Number(sessionStorage.getItem(AD.X_FLAG_LEAD));
            sume += Number(sessionStorage.getItem(AD.X_FLAG_APPO));
            sume += Number(sessionStorage.getItem(AD.X_FLAG_TASK));
            if (sume > 1) {
                var toolbar_1 = $("#alertToolBar").data("kendoToolBar");
                toolbar_1.enable("#btnQueue", (sessionStorage.getItem(AD.X_FLAG_LEAD) === "1"));
                toolbar_1.enable("#btnTask", (sessionStorage.getItem(AD.X_FLAG_TASK) === "1"));
                toolbar_1.enable("#btnAppt", (sessionStorage.getItem(AD.X_FLAG_APPO) === "1"));
                wnd.open();
                wnd.center();
            }
            if (sume === 1) {
                if (sessionStorage.getItem(AD.X_FLAG_TASK) === "1") {
                    this.goToLeadTaskScreen();
                    wnd.close();
                    return;
                }
                if (sessionStorage.getItem(AD.X_FLAG_APPO) === "1") {
                    this.goToLeadScheduleScreen();
                    wnd.close();
                    return;
                }
                window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/ALeadQueue.aspx?RESID=825&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=LeadQueue&mod=AD");
            }
        };
        LeadToolbarVm.prototype.goToQueuePage = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/ALeadQueue.aspx?RESID=825&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=LeadQueue&mod=AD");
        };
        LeadToolbarVm.prototype.goToLeadInfoScreen = function () {
            sessionStorage.setItem(AD.XLEAD_NEW, "false");
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&mod=AD");
        };
        LeadToolbarVm.prototype.goToLeadQuickScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadQuick.aspx?RESID=268&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Quick&mod=AD");
        };
        LeadToolbarVm.prototype.goToLeadNotesScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadNotes.aspx?RESID=456&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Notes&mod=AD");
        };
        LeadToolbarVm.prototype.goToLeadRequirementScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/leadRequirements.aspx?RESID=826&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Requirements&mod=AD");
        };
        LeadToolbarVm.prototype.goToLeadHistoryScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadHistory.aspx?RESID=000&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=History&mod=AD");
        };
        LeadToolbarVm.prototype.goToLeadEducationScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadEducation.aspx?RESID=145&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Education&mod=AD");
        };
        LeadToolbarVm.prototype.goToSPriorWorkScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadPriorWork.aspx?RESID=146&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=PriorWorks&mod=AD");
        };
        LeadToolbarVm.prototype.goToLeadExtracurricularScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadExtracurricular.aspx?RESID=148&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Extracurricular&mod=AD");
        };
        LeadToolbarVm.prototype.goToSkillScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadSkills.aspx?RESID=147&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Skills&mod=AD");
        };
        LeadToolbarVm.prototype.goToLeadEnrollmentScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/leadEnrollments.aspx?RESID=174&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Enrollment&mod=AD");
        };
        LeadToolbarVm.prototype.goToEmailPage = function () {
            var leadElement = document.getElementById('leadId');
            var leadGuid = leadElement.value;
            ad.OPEN_TASK_RAD_EMAIL_WINDOW_WITH_ENTITY(leadGuid, 4);
        };
        LeadToolbarVm.prototype.goToLeadTaskScreen = function () {
            var leadElement = document.getElementById('leadId');
            var leadGuid = leadElement.value;
            ad.OPEN_TASK_RAD_TASK_WINDOW_WITH_ENTITY(leadGuid, 4);
            var wnd = $("#leadToolBarWindow").data("kendoWindow");
            wnd.close();
        };
        LeadToolbarVm.prototype.goToLeadScheduleScreen = function () {
            var leadElement = document.getElementById('leadId');
            var leadGuid = leadElement.value;
            ad.OPEN_TASK_RAD_SCHEDULE_WINDOW_WITH_ENTITY(leadGuid, 4);
            var wnd = $("#leadToolBarWindow").data("kendoWindow");
            wnd.close();
        };
        LeadToolbarVm.prototype.getFromServerFlagInformation = function () {
            var command = 0;
            try {
                $("body").css("cursor", "wait");
                var db1 = new AD.LeadToolBarDb();
                var userId = $("#hdnUserId").val();
                db1.getFromServerFlagInformation(XMASTER_GET_CURRENT_CAMPUS_ID, userId, command, this)
                    .done(function (msg) {
                    if (msg != undefined) {
                        try {
                            if ((msg.FlagLead || msg.FlagAppointment || msg.FlagTask) === false) {
                                var toolbar_2 = $("#LeadToolbar").data("kendoToolBar");
                                toolbar_2.enable("#leadToolBarAlert", false);
                            }
                            else {
                                var toolbar_3 = $("#LeadToolbar").data("kendoToolBar");
                                toolbar_3.enable("#leadToolBarAlert", true);
                            }
                            sessionStorage.setItem(AD.X_FLAG_LEAD, (msg.FlagLead) ? "1" : "0");
                            sessionStorage.setItem(AD.X_FLAG_TASK, (msg.FlagTask) ? "1" : "0");
                            sessionStorage.setItem(AD.X_FLAG_APPO, (msg.FlagAppointment) ? "1" : "0");
                            $("body").css("cursor", "default");
                        }
                        catch (e) {
                            $("body").css("cursor", "default");
                            alert(e.message);
                        }
                    }
                });
            }
            catch (e) {
                $("body").css("cursor", "default");
                alert(e.message);
            }
        };
        LeadToolbarVm.prototype.goToSetQueueLead = function (e) {
            try {
                var command = (e.id === "btnPriorLead") ? 1 : 2;
                var userId = $("#hdnUserId").val();
                var campusId = XMASTER_GET_CURRENT_CAMPUS_ID;
                var control = document.getElementById('leadId');
                var leadId = control.value;
                $("body").css("cursor", "wait");
                var db1 = new AD.LeadToolBarDb();
                db1.setFromServerQueueLead(campusId, userId, leadId, command, this)
                    .done(function (msg) {
                    if (msg != undefined) {
                        try {
                            sessionStorage.setItem("NewLead", "false");
                            var url = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&NewEnt=1&cmpId=" + campusId;
                            window.location.assign(url);
                            $("body").css("cursor", "default");
                        }
                        catch (e) {
                            $("body").css("cursor", "default");
                            alert(e.message);
                        }
                    }
                })
                    .fail(function (msg) {
                    $("body").css("cursor", "default");
                    ad.SHOW_DATA_SOURCE_ERROR(msg);
                });
            }
            catch (e) {
                $("body").css("cursor", "default");
                alert(e.message);
            }
        };
        ;
        return LeadToolbarVm;
    })(kendo.Observable);
    AD.LeadToolbarVm = LeadToolbarVm;
})(AD || (AD = {}));
var AD;
(function (AD) {
    AD.X_SAVE_PLEASE_FILL_REQUIRED_FIELDS = "Please fill in all required fields to continue.";
    AD.X_SAVE_REVIEW_INFO_IN_FORM = "Please revise if all information was correctly input in the form!";
    AD.X_DELETE_SUCESS = "The lead was successfully deleted";
    AD.X_DELETE_NOT_POSSIBLE = "This lead record contains uploaded documents or processed fees and cannot be deleted.";
    AD.X_DELETE_ARE_YOU_SURE = "Are you sure to delete the Lead ";
    AD.X_SELECT_ITEM = "Select";
})(AD || (AD = {}));
//# sourceMappingURL=AdvantageClientAD.js.map