﻿module AD {
    /*  General */
    export var X_SAVE_PLEASE_FILL_REQUIRED_FIELDS = "Please complete required fields or correct the highlighted field values.";
    export var X_PLEASE_FILL_REQUIRED_FIELDS = "Please enter the required values.";
    export var X_SAVE_REVIEW_INFO_IN_FORM = "Please review if all information was correctly input in the form!";
    export var X_DELETE_SUCESS = "The lead was successfully deleted";
    export var X_DELETE_NOT_POSSIBLE = "This lead record contains uploaded documents or processed fees and cannot be deleted.";
    export var X_DELETE_ARE_YOU_SURE = "Are you sure to delete the Lead ";
    export var X_SELECT_ITEM = "Select";
    export var X_LEAD_SAVED = "Lead saved successfully";
    export var X_LEAD_SAVE_CHANGES = "Do you want to save changes?";
    export var X_LEAD_CONFIGURE_UDF =
        "Custom fields are not visible until you customize they position at [Maintenance -> Add Custom Field to Page]";
    export var X_CALLED_FROM_INCORRECT_MODULE = "Page called form incorrect module";

    /* Extracurricular messages*/
    export var X_MESSAGE_NEW_EXTRACURRICULAR = "Enter a New Extracurricular";
    export var X_MESSAGE_EDIT_EXTRACURRICULAR = "Edit the Extracurricular";
    export var X_MESSAGE_EXTRACURRICULAR_DESCRIPTION_REQUIRED = "Extracurricular Description is required!";
    export var X_MESSAGE_EXTRACURRICULAR_DESCRIPTION_CANNOT_BE_LONGER_THAN = "Description Cannot Be Longer Than 50 Characters!";
    export var X_MESSAGE_EXTRACURRICULAR_COMMENT_CANNOT_BE_LONGER_THAN = "Comment Cannot Be Longer Than 100 Characters!";

    /* Lead Duplicates Messages */
    export var X_MESSAGE_DUPLICATED_LEAD_A_TO_B = "If you continue the information of Lead A will be transferred to Lead B. Are you sure?";
    export var X_MESSAGE_DUPLICATED_DELETE_A = "Are you sure to delete Lead A?";
    export var X_MESSAGE_DUPLICATED_NEW_LEAD_A = "Are you sure to create a new Lead with Lead A?";
    export var X_MESSAGE_DUPLICATED_NEW_LEAD_B = "Are you sure to create a new Lead with Lead B?";

    /* Student Termination Messages*/
    export var X_MESSAGE_LDA_GREATER_THAN_ENROLLMENTDATE = "Last attended date should be greater than or equal to enrollment start date ";
    export var X_MESSAGE_LDA_LESS_THAN_DOD = "Last attended date should be less than or equal to date of determination ";
    export var X_MESSAGE_DOD_LESS_THAN_LDA = "Date of determination should be greater than or equal to Last attended date ";
    export var X_MESSAGE_DOD_GREATER_THAN_ENROLLMENTDATE = "Date of determination should be more than enrollment date ";
    export var X_MESAGE_TERMINATION_DELETE_SUCCESS = "Record deletion was successful.";
    export var X_MESSAGE_ENDDATE_LESS_THAN_STARTDATE = "End date must be greater than start date [date] or enrollment start date ";
    export var X_MESSAGE_STARTDATE_LESS_THAN_ENDDATE = "Start date must be less than end date ";
    export var X_MESSAGE_WD_GREATER_THAN_ENROLLMENTDATE = "Withdrawal date should be greater than or equal to enrollment start date ";
    export var X_MESSAGE_WD_EQUAL_TO_LDA = "Withdrawal date should be equal to last date attended.";
    export var X_MESSAGE_STARTDATE_GREATER_THAN_ENROLLMENTSTARTDATE = "Start date should be greater than or equal to enrollment start date ";
    export var X_MESSAGE_WD_GREATER_THAN_SCHEDULEDENDDATE = "Withdrawal date should be less than scheduled end date ";
    export var X_MESSAGE_WD_GREATER_THAN_STARTDATE = "Withdrawal date should be greater than Start date ";
    export var X_MESSAGE_WD_LESS_THAN_OR_EQUAL_TO_DOD = "Withdrawal date should be less than or equal to date of determination ";
    export var X_MESAGE_R2T4Input_Prefix = "Please enter the required values under ";
    export var X_MESAGE_R2T4Input_AID_FILL = "'Title IV aid'";
    export var X_MESAGE_R2T4Input_InstituitionFess_FILL = "'Institutional charges posted'";
    export var X_MESAGE_R2T4Input_Percentage_FILL = "'Percentage of period completed'";
    export var X_MESAGE_R2T4Input_ZERO_VAL_SCHEDULED = "'Hours scheduled to complete' should be greater than zero";
    export var X_MESAGE_R2T4Input_ZERO_VAL_TOTAL = "'Total hours in period' should be greater than zero";
    export var X_MESAGE_R2T4Input_ZERO_VAL_COMPLETED_DAYS = "'Completed days' should be greater than zero";
    export var X_MESAGE_R2T4Input_ZERO_VAL_TOTAL_DAYS = "'Total days' should be greater than zero";
    export var X_MESAGE_R2T4Input_TOTALHRS_LESSTHAN_COMPLETEDHRS = "'Total hours in period' should be greater than or equal to'Hours scheduled to complete'";
    export var X_MESAGE_R2T4Input_TOTALDAYS_LESSTHAN_COMPLETEDDAYS = "'Total days' should be greater than or equal to 'Completed days'";

    export var X_MESSAGE_R2T4Input_LDA_CHANGE = "R2T4 Inputs would have to be recalculated and existing inputs would be lost. Are you sure you want to make changes?";
    export var X_MESSAGE_R2T4Result_LDA_CHANGE = "R2T4 Inputs and R2T4 Results would have to be recalculated and existing inputs and results or overridden results would be lost. Are you sure you want to make changes?";
    export var X_MESSAGE_R2T4ApproveTermination_LDA_CHANGE = "R2T4 Inputs and R2T4 Results would have to be recalculated and existing inputs, results or overridden results and details on 'Approve Termination' would be lost. Are you sure you want to make changes?";

    export var X_MESSAGE_R2T4Result_DOD_BIGGER_THAN_ENROLLMENTDATE = "Determination date on 'R2T4 Results' tab would be changed. Are you sure you want to make changes?";
    export var X_MESSAGE_R2T4ApproveTermination_DOD_BIGGER_THAN_ENROLLMENTDATE = "Determination date on 'R2T4 Results' and 'Approve Termination' tabs would be changed. Are you sure you want to make changes?";
    export var X_MESSAGE_R2T4Input_ENROLLMENTDATE_BIGGER_THAN_DOD = "As the 'Date of determination' specified is less than enrollment start date and the status code associated with the enrollment is 'FutureStart', R2T4 calculation is not required and thus existing details on 'R2T4 Input' would be deleted. Are you sure you want to make changes?";
    export var X_MESSAGE_R2T4InputCALCULATIONPERIOD_CHANGE = "As R2T4 calculation is not required existing details on 'R2T4 Input' would be deleted. Are you sure you want to make changes?";
    export var X_MESSAGE_R2T4InputDOD_CHANGE = "As the 'Date of determination' specified is less than enrollment start date, R2T4 calculation is not required and thus existing details on 'R2T4 Input' would be deleted. Are you sure you want to make changes?";
    export var X_MESSAGE_R2T4Input_BLANK_DOD = "As the 'Date of determination' is not specified, 'R2T4 Input' would be deleted. Are you sure you want to make changes?";
    export var X_MESSAGE_R2T4ResultCALCULATIONPERIOD_CHANGE = "By un-checking the 'Perform R2T4' check box existing details on 'R2T4 Input' and 'R2T4 Results' will be deleted. Are you sure you want to make change?";
    export var X_MESSAGE_R2T4ResultDOD_CHANGE = "As the 'Date of determination' specified is less than enrollment start date, R2T4 calculation is not required and thus existing details on 'R2T4 Input' and 'R2T4 Results' would be deleted. Are you sure you want to make changes?";
    export var X_MESSAGE_R2T4Result_BLANK_DOD = "As the 'Date of determination' is not specified, 'R2T4 Input' and 'R2T4 Results' would be deleted. Are you sure you want to make changes?";
    export var X_MESSAGE_R2T4ApproveTerminationCALCULATIONPERIOD_CHANGE = "As R2T4 calculation is not required existing details on 'R2T4 Input', 'R2T4 Results' and 'Approve Termination' would be deleted. Are you sure you want to make changes?";
    export var X_MESSAGE_R2T4ApproveTerminationDOD_CHANGE = "As the 'Date of determination' specified is less than enrollment start date, R2T4 calculation is not required and thus existing details on 'R2T4 Input', 'R2T4 Results' and 'Approve Termination' would be deleted. Are you sure you want to make changes?";
    export var X_MESSAGE_R2T4ApproveTermination_BLANK_DOD = "As the 'Date of determination' is not specified, 'R2T4 Input', 'R2T4 Results' and 'Approve Termination' details would be deleted. Are you sure you want to make changes?";
    export var X_MESSAGE_R2T4Result_TICKET_NO_REQUIRED = "Please enter the ticket number";
    export var X_MESSAGE_R2T4_RESULT_OVERRIDE_DISCARD = "All the changes made would be discarded. Are you sure?";
    export var X_MESSAGE_R2T4Result_Ticket_Mandatory = "Ticket number is mandatory.";
    export var X_MESSAGE_APPROVE_TERMINATION_CONFIRM = "Are you sure you want to terminate the $StundentName from $EnrollmentName?";
    export var X_MESSAGE_APPROVE_TERMINATION_CONFIRM_R2T4_MULTIPLE = "$StundentName is terminated from $EnrollmentName successfully. The student has one or more active enrollments remaining. Do you wish to terminate another enrollment for this student?";
    export var X_MESSAGE_APPROVE_TERMINATION_CONFIRM_NONR2T4_MULTIPLE = "$StundentName is terminated from $EnrollmentName successfully. The student has one or more active enrollments remaining. Do you wish to terminate another enrollment for this student?";
    export var X_MESSAGE_APPROVE_TERMINATION_CONFIRM_R2T4_SINGLE = "$StundentName is terminated from $EnrollmentName successfully.";
    export var X_MESSAGE_APPROVE_TERMINATION_CONFIRM_NONR2T4_SINGLE = "$StundentName is terminated from $EnrollmentName successfully.";
    // export var X_MESAGE_R2T4Input_COMPLETEDHRS_GREATERTHAN_TOTALHRS = "'Hours scheduled to complete' should be less than 'Total hours in period' ";
    // export var X_MESAGE_R2T4Input_COMPLETEDDAYS_GREATERTHAN_TOTALDAYS = "'Completed days' should be less than 'Total days' ";
    export var X_MESAGE_RESULT_DELETE_FAIL =
        "Record deletion failed due to some internal error. Please try again.";

    export var X_MESAGE_TERMINATION_DELETE_FAIL_ERROR = "An error occurred while cancelling the student termination record for $StundentName and $EnrollmentName. Please try again.";
    export var X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL = "An error occurred while saving the termination record for $StundentName and $EnrollmentName. Please save the termination details for the same before changing the Student or Enrollment.";
    export var X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS = "All the student termination details specified is saved";

    export var X_MESAGE_SAVE_INPUT_NEXT_FAIL = "An error occurred while saving the $CurrentTabName related to the current student termination record. Please try saving the $CurrentTabName details again.";

    export var X_MESAGE_APPROVE_STUDENT_TERMINATION_SUCCESS = "Student is terminated from the specified enrollment";
    export var X_MESAGE_SAVE_STUDENT_STATUS_CHANGES_UNSUCCESS = "Failed saving student status changes.";
    export var X_MESAGE_SAVE_APPROVE_TERMINATION = "An error occurred while approving the student termination record for $StundentName and $EnrollmentName. Please try again.";
    export var X_MESAGE_SAVE_STUDENT_DOCUMENT_DETAILS_UNSUCCESS = "Failed saving student document details.";
    export var X_MESAGE_NO_RECORD = "There are no inactive enrollments for this student.";
    export var X_MESSAGE_R2T4Input_CALCULATION_PERIOD_CHANGE = "Details entered under 'Institutional charges posted' section on the 'R2T4 Input' would be updated. Are you sure you want to make changes?";
    export var X_MESSAGE_R2T4Result_CALCULATION_PERIOD_CHANGE = "Details entered under 'Institutional charges posted' section on the 'R2T4 Input' tab would be updated and R2T4 results on ‘R2T4 Results’ tab would be lost. Are you sure you want to make changes?";
    export var X_MESSAGE_AppTermination_CALCULATION_PERIOD_CHANGE = "Details entered under 'Institutional charges posted' section on the 'R2T4 Input' tab would be updated, R2T4 results on ‘R2T4 Results’ tab and details on 'Approve Termination' tab would be lost. Are you sure you want to make changes?";
    export var X_MESSAGE_UNDO_TERMINATION = "Are you sure you want to undo the termination for $StundentName from $EnrollmentName.";
    export var X_DATE_TYPE = "Withdrawal date";
    export var X_MESSAGE_WITHDRAWL_LESS_THAN_DOD = "Withdrawal date should be less than or equal to date of determination ";
    export var X_MESSAGE_WITHDRAWL_GREATER_THAN_ENROLLMENTDATE = "Withdrawal date should be greater than or equal to enrollment start date ";
    export var X_MESSAGE_NO_AWARDS = "There are no title iv awards for this student.";
    export var X_MESSAGE_END_DATE = "Please specify an end date when the student is expected to complete the period";
    export var X_MESSAGE_WITHDRAWAL_END_DATE = "Please enter the withdrawal period end date and try again";
    export var X_MESSAGE_NO_FAILED_COURSES = "No failed course found for the given enrollment.";
    export var X_MESSAGE_COURSES_NOT_FOUND = "Courses not found.";
    export var X_MESSAGE_NON_TERM_NOT_SELFPACED_TOTAL_DAYS_ISSUE = "There is some issue in calculating total days for non term not self paced program. Please try again.";
    export var X_MESSAGE_NONTERM_NOT_SELFPACED_COMPLETED_AND_TOTAL_DAYS_ISSUE = "There is some issue in calculating completed and total days for non term not self paced program. Please try again.";
    export var X_MESSAGE_NON_TERM_NOT_SELFPACED_COMPLETED_DAYS_ISSUE = "There is some issue in calculating completed days for non term not self paced program. Please try again.";
    export var X_MESSAGE_NONSTANDARD_SUB_EQUAL_COMPLETED_AND_TOTAL_DAYS_ISSUE = "There is some issue in calculating completed and total days for non standard substantially equal in length program. Please try again.";
    export var X_MESSAGE_NONSTANDARD_SUB_NOT_EQUAL_COMPLETED_AND_TOTAL_DAYS_ISSUE = "There is some issue in calculating completed and total days for non standard not substantially equal in length program. Please try again.";
    export var X_MESSAGE_CLOCK_HOUR_COMPLETED_AND_TOTAL_HOURS_ISSUE = "There is some issue in calculating total hours and hours scheduled to complete for clock hour program. Please try again.";
    export var X_MESSAGE_DOD_LESS_THAN_WD = "Date of determination should be greater than or equal to withdrawal date ";
    export var X_MESSAGE_ENDDATE_LESSTHAN_STARTDATE = "Entered End Date is should be greater than the start date $startDate or withdrawal date $withdrawal. Please enter the valid end date and try again.";
    export var X_MESSAGE_NO_AWARDS_FOR_STUDENT = "Student $studentName was not awarded any title 4 aid for the enrollment $enrollmentName. Thus R2T4 is not required.";
    export var X_FAILED_CALCULATING_FRAGMENT = "Student's schedule is not set to";

}