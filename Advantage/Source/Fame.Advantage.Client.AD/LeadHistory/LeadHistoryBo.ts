﻿module AD {

    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;
    declare var XMASTER_PAGE_USER_OPTIONS_USERNAME: string;
    declare var XMASTER_PAGE_USER_OPTIONS_USERID: string;
    

    export class LeadHistoryBo {

        dataSource: kendo.data.DataSource;
        dddataSource: kendo.data.DataSource;

        constructor() {
            this.dddataSource = new kendo.data.DataSource({
                transport: {
                    read: options => {
                        $.ajax({
                            url: XGET_SERVICE_LAYER_HISTORY,
                            type: "GET",
                            dataType: "json",
                            data: {
                                LeadId: "00000000-0000-0000-0000-000000000000",
                                Command: 2
                            },
                            success: result => {
                                result.ModulesList.unshift({ Description: "All", ID: "ALL" });
                                options.success(result.ModulesList);
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    }
                }
            });
        }


        getLeadHistoryInfo(moduleCode: string, command: number, leadid:string): kendo.data.DataSource {

            var userId: string = $("#hdnUserId").val();
            this.dataSource = new kendo.data.DataSource({
                transport: {
                    read: options => {
                        $.ajax({
                            url: XGET_SERVICE_LAYER_HISTORY,
                            type: "GET",
                            dataType: "json",
                            data: {
                                ModuleCode: moduleCode,
                                LeadId: leadid,
                                Command: command,
                                CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                UserId: XMASTER_PAGE_USER_OPTIONS_USERID
                            },
                            success: result => {
                                options.success(result);
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });

                    },
                    parameterMap: (options, operation) => {
                        if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                        return null;
                    }
                },
                batch: true,
                pageSize: 12,
                schema: {
                    data: "HistoryList",
                    total: response => response.HistoryList.length,
                    model: {
                        id: "HistoryId",
                        fields: {
                            History: { editable: false, nullable: false },
                            ModuleName: {
                                editable: false,
                                validation: { required: true },
                                defaultValue: $("#historyModulesDdl").data("kendoDropDownList").text()

                            },
                            Description: {
                                editable: false,
                                type: "string",
                                validation: {required: true}
                            },
                            Type: { type: "string"
                            },
                            ModUser: {
                                editable: false,
                                type: "string",
                                defaultValue: XMASTER_PAGE_USER_OPTIONS_USERNAME
                            },
                            UserId: { editable: false },

                             HistoryDate: {
                                editable: false,
                                type: "Date",
                                defaultValue: new Date()
                            }

                        }
                    }
                }
            }
            );
            return this.dataSource;
        }
    }
} 