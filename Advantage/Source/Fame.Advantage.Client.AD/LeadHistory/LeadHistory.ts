﻿module AD {
    export class LeadHistory {

        public bo: LeadHistoryBo;
        public moduleInit: string;
        public leadId: string;

        constructor(leadid: string, whatModule: string) {
            this.leadId = leadid;
            this.moduleInit = whatModule;
            MasterPage.Common.enableDisableBtns(false);
        }

        /*
         * Use this after constructor
         */
        initOperation() {
            try {

                //var that = this;
                this.bo = new LeadHistoryBo();
                $("#historyGrid").kendoGrid({
                    pdf: {
                        allPages: true
                    },
                    dataSource: [],
                    pageable: true,
                    sortable: true,
                    columns: [ 
                        {
                            field: "HistoryDate"
                            , title: "Date"
                            , width: "100px"
                            , template: "#=  (HistoryDate == null)? '' : kendo.toString(kendo.parseDate(HistoryDate, 'yyyy-MM-dd'), 'MM/dd/yy hh:mm:ss tt') #"
                        },

                        {
                            field: "HistoryModule",
                            title: "Module",
                            width: "100px" 
                        },
                        {
                            field: "Type",
                            title: "Type",
                            width: "100px"
                        },
                        {
                            field: "Description",
                            title: "Description",
                            width: "200px"
                             , template: "# if (Type === 'Email') { # <a href=javascript:void(0);'  class='email-link'>#= Description # </a> # } else { # #=Description# #}#"
 
                        },

                        {
                            field: "ModUser",
                            title: "User",
                            width: "140px"

                        },

                        {
                            field: "AdditionalContent",
                            title: "EmailContent",
                            hidden: true
                        } 


                    ]
                });

               var grid = $('#historyGrid').data('kendoGrid');
                //bind click event to the checkbox
               grid.table.on("click", ".email-link", this.updateMandGridSelect);

           
                //Drop down list for modules
                $("#historyModulesDdl").kendoDropDownList(({
                  //   dataSource: this.bo.dddataSource,                DO NOT Delete - This provides all Modules as selections for Module ddl 
                    dataSource: [{ Description: 'Admissions', ID:'AD'}], // but for now we only have History for Admissions
                    dataTextField: "Description",
                    dataValueField: "ID",
                    dataBound: eb => {
                        eb.sender.value(this.moduleInit);
                        this.getHistoryFromServer(eb, this);
                    },
                    change: (ex) => {
                        this.getHistoryFromServer(ex, this);
                    }
                }) as any);


                // Add Print Event Button
                $("#historyPrintbutton").click(() => {
                    var grid: any = $("#historyGrid").data("kendoGrid");
                    grid.saveAsPDF();
                });

            } catch (e) {
                $("body").css("cursor", "default");
                alert(e.message + "/n" + e.stack);

            }


        }

        updateMandGridSelect(e: any) {
            var row = $(this).closest("tr");
            var dataItem = $("#historyGrid").data("kendoGrid").dataItem(row);
            //     var myWindow: kendo.ui.Window = e.container.data("kendoWindow") as any;
            if (dataItem != null || dataItem != undefined) {
                if ($('#popupEmailWindow').length === 0) {
                    $("<div id='popupEmailWindow'><div class='popup-content'></div></div>").appendTo("body");

                    $("#popupEmailWindow").kendoWindow({
                        modal: true,
                        width: "700",
                        title: "Email: " + dataItem.get('Description'),
                        actions: ["close"],
                        resizable: false,
                        draggable: true
                    });
                }
            }

            $(".popup-content").html(dataItem.get('AdditionalContent'));
            $("#popupEmailWindow").data("kendoWindow").center().open();
 
        }
                
        /*
         * Get the History information from Server 
         */
        getHistoryFromServer(e: any, that: any): void {

            // Set the data-source for control
            var grid: kendo.ui.Grid = $("#historyGrid").data("kendoGrid") as any;
            var dllModule: kendo.ui.DropDownList = e.sender;
            var infoFromServer = that.bo.getLeadHistoryInfo(dllModule.value(), 1, that.leadId);
            grid.setDataSource(infoFromServer);
        }
    }
} 