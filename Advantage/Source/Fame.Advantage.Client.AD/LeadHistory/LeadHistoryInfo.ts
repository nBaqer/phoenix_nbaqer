﻿module AD {
    // ReSharper disable InconsistentNaming 
    export class History implements History {
        HistoryId : string;
        ModuleCode: string;
        HistoryDate: Date;
        ModuleName: string;
        Type: string;
        Description: string;
        UserName: string;
        UserId: string;
    }


    /*
     * Filter to input information in service
     */
    export class LeadHistoryInputInfo  {
        Command: number;
        LeadId: string;
        ModuleCode: string;
        UserId: string;
        CampusId: string;
    }
     export class LeadHistoryInfo {
        HistoryList: History[];
        ModulesList: IDropDownItem[];
        Filter: LeadHistoryInputInfo;
    }
// ReSharper restore InconsistentNaming

} 