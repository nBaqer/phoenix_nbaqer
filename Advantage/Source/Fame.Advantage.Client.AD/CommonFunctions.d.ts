declare module ad {
    function GET_QUERY_STRING_PARAMETER_BY_NAME(name: string): string;
    function GET_AGE(dateString: any): number;
    function OPEN_TASK_RAD_EMAIL_WINDOW_WITH_ENTITY(entityId: any, mod: any): void;
    function OPEN_TASK_RAD_TASK_WINDOW_WITH_ENTITY(entityId: any, mod: any): void;
    function OPEN_TASK_RAD_SCHEDULE_WINDOW_WITH_ENTITY(entityId: any, mod: any): void;
    function SHOW_DATA_SOURCE_ERROR(e: any): string;
    function FORMAT_DATE_TO_MM_DD_YYYY(d: Date): string;
    function FORMAT_AMPM(date: Date): string;
}
