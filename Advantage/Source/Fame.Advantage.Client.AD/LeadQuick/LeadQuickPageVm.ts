﻿/// <reference path="../../AdvWeb/Kendo/typescript/jquery.d.ts" />
/// <reference path="../../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../fame.advantage.client.masterpage/userdefinedcontrols/advantageuserdefinedfields.ts" />
/// <reference path="../../fame.advantage.client.masterpage/commonfunctions.ts" />

module AD {

    declare var XGET_SERVICE_LAYER_FIELDS_GET: string;
    // This must be in the html code of control.
    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;
    declare var XMASTER_PAGE_USER_OPTIONS_USERID: string;
    declare var XMASTER_LEAD_IS_CLOCK_HOUR: string;
    declare var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL: string;
    export declare var dropdownlistArray: Array<any>;
    export declare var previousBtnClicked: string;
    export interface ILeadQuickPage {
        tblFldsId: number;
        fldId: number;
        sectionId: number;
        ddlId?: number;
        fldName: string;
        required: number;
        fldLen: number;
        fldType: string;
        caption: string;
        controlName?: string;
    }
    export class LeadQuickPageVm extends kendo.Observable {
        db: LeadQuickPageDb;
        leadDb: LeadInfoPageDb;
        filterDataDb: ILeadQuickFields[];
        gruppenDb: IGroup[];
        stateId: any;
        leadStatusId: string;
        driverLiscStateId: any;
        filteredLeadStatusDll: any = [];
        finalLeadStatusdll: kendo.data.DataSource;
        filteredAdminRepDll: any = [];
        filteredInterestAreaDll: any = [];
        //static string constant
        public static TRUE: string = "true";
        public static REQUIRED: string = "True";
        public static PHONE: string = "Phone";
        public static BEST_TIME: string = "BestTime";
        public static SOURCE_DATE_TIME: string = "SourceDateTime";

        constructor() {
            super();
            this.db = new LeadQuickPageDb();
            this.leadDb = new LeadInfoPageDb();
            dropdownlistArray = new Array();
            previousBtnClicked = "";
        }



        cancelLead() {
            $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure to cancel?"))
                .then(confirmed => {
                    if (confirmed) {
                        //var r = confirm("are u sure to cancel");
                        //if (r) {
                        //selected cancel so do nothing and go to to new quick lead page
                        sessionStorage.setItem(XLEAD_QUICK_NEW, LeadQuickPageVm.TRUE);
                        window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                            "AD/AleadQuick.aspx?resid=268&cmpid=" +
                            XMASTER_GET_CURRENT_CAMPUS_ID +
                            "&desc=Quick&mod=AD");
                    }
                }); //else stay on the same page with the entered values
        }

        postLeadInformationToServer(leadGuid: string, avoidDuplicate: boolean, btnName: string) {
            //var that = this;
            var lead: ILeadInfoPageOutputModel = new LeadInfoPageOutputModel();
            // Determine if it is a insertion of a new lead or a update
            var newLead = sessionStorage.getItem(XLEAD_QUICK_NEW);
            if (newLead != null && newLead === LeadQuickPageVm.TRUE) {
                // it is a insertion
                lead.LeadId = '00000000-0000-0000-0000-000000000000';
                lead.AvoidDuplicateAnalysis = avoidDuplicate;
            } else {
                // it is a lead update
                lead.LeadId = leadGuid;
            }

            // Continue to fill the entity
            var userId = XMASTER_PAGE_USER_OPTIONS_USERID;

            // Fill the necessary filter information
            lead.InputModel.UserId = userId;
            lead.InputModel.CampusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            lead.InputModel.LeadId = lead.LeadId;


            lead.ModUser = userId;
            //take the controls that are displayed from the session.. to get the controlId
            var temp = sessionStorage.getItem(XLEAD_QUICK_FIELDS);
            if (temp != null || !(temp === "{}")) {
                var filterDataDb = Object.create(JSON.parse(temp));
                if (filterDataDb != null) {
                    var fixedResponse = filterDataDb.responseText.replace(/\\'/g, "'");
                    var result = JSON.parse(fixedResponse);
                    if (result != null) {
                        for (let i = 0; i < result.length; i++) {
                            var idControl = result[i].PropName;
                            let ctrGrp = result[i].CtrlIdName;
                            //textbox for SmallInt,Int,Float,Money,TinyInt,Char,Decimal,Varchar
                            if ((result[i].FldTypeId === FieldType.SmallInt) ||
                                (result[i].FldTypeId === FieldType.Int) ||
                                (result[i].FldTypeId === FieldType.Float) ||
                                (result[i].FldTypeId === FieldType.Money) ||
                                (result[i].FldTypeId === FieldType.TinyInt) ||
                                (result[i].FldTypeId === FieldType.Char) ||
                                (result[i].FldTypeId === FieldType.Decimal) ||
                                (result[i].FldTypeId === FieldType.Varchar)) {

                                if (ctrGrp != null) {
                                    //phone
                                    if (ctrGrp === LeadQuickPageVm.PHONE) {
                                        lead.PhonesList = [];
                                        var phone: IPhone = new Phone();
                                        phone.Extension = $("#Extension").val();
                                        phone.ID = $("#hiddenPhoneId").val();
                                        phone.IsForeignPhone = ($("#IsForeignPhone").prop("checked"));
                                        phone.Phone = $("#Phone").data("kendoMaskedTextBox").raw();
                                        //phone.Phone = $("#Phone").val();
                                        phone.PhoneTypeId = $("#PhoneTypeId").data("kendoDropDownList").value();
                                        phone.Position = 1;
                                        lead.PhonesList.push(phone);
                                    }
                                    //address
                                    else if (ctrGrp === "Address") {
                                        lead.LeadAddress.CountyId = $("#CountyId").data("kendoDropDownList").value();
                                        lead.LeadAddress.CountryId = $("#CountryId").data("kendoDropDownList").value();
                                        lead.LeadAddress.AddressTypeId = $("#AddressTypeId").data("kendoDropDownList").value();
                                        lead.LeadAddress.IsInternational = ($("#IsInternational").prop("checked"));
                                        if ($("#IsInternational").prop("checked")) {
                                            lead.LeadAddress.StateInternational = $("#StateInternational").val();
                                        } else {
                                            //if (this.stateId != null) {
                                            lead.LeadAddress.StateId = this.stateId;
                                            //}
                                        }
                                        //lead.LeadAddress.StateId = $("#StateId").data("kendoDropDownList").value();
                                        lead.LeadAddress.ZipCode = $("#ZipCode").data("kendoMaskedTextBox").value();
                                        lead.LeadAddress.City = $("#City").val();
                                        lead.LeadAddress.Address1 = $("#Address1").val();
                                        lead.LeadAddress.AddressApto = $("#AddressApto").val();
                                        lead.LeadAddress.Address2 = $("#Address2").val();
                                        lead.LeadAddress.IsShowOnLeadPage = 'yes';
                                        lead.LeadAddress.IsMailingAddress = 'yes';
                                    }
                                    //email
                                    else if (ctrGrp === "Email") {
                                        lead.EmailList = [];
                                        var email: IeMail = new Email();
                                        email.Email = $("#Email").val();
                                        email.EmailType = $("#EmailType").data("kendoDropDownList").value();
                                        email.ID = $("#hiddenEmailPrimaryId").val();
                                        email.IsPreferred = true;
                                        lead.EmailList.push(email);
                                    }
                                    else if (idControl === "DistanceToSchool" || idControl === "Dependants") {
                                        if ($("#" + idControl).val() != null) {
                                            lead[idControl] = ($("#" + idControl).data("kendoNumericTextBox") as kendo.ui.NumericTextBox).value();
                                        }
                                    }
                                    else {
                                        //all the rest of the common controls of this type
                                        if ($("#" + idControl).val() != null) {
                                            lead[idControl] = $("#" + idControl).val();
                                        }
                                    }
                                }
                            }
                            //all the rest of the common controls of this type
                            if (result[i].FldTypeId === FieldType.DateTime) {
                                if (idControl === "ExpectedStart") {
                                    lead.ExpectedStart = ad.processExpectedStart("ProgramId", "dpExpectedStart", "ddlExpectedStart"); //this.processExpectedStart();
                                } else if (idControl === LeadQuickPageVm.BEST_TIME) {
                                    if ($("#" + LeadQuickPageVm.BEST_TIME).data("kendoTimePicker") != null) {
                                        let besttime: kendo.ui.DateTimePicker = $("#" + LeadQuickPageVm.BEST_TIME).data("kendoTimePicker") as any;
                                        let datetime: Date = besttime.value();
                                        if (datetime !== null && datetime !== undefined) {
                                            let output: string = datetime.getHours() + ":" + datetime.getMinutes();
                                            lead.BestTime = output;
                                        }
                                        //lead.BestTime = $("#" + LeadQuickPageVm.BEST_TIME).data("kendoTimePicker").value().toString();
                                    }
                                } else if (idControl === LeadQuickPageVm.SOURCE_DATE_TIME) {
                                    if ($("#" + idControl).data("kendoDateTimePicker") != null) {
                                        if ($("#" + idControl).data("kendoDateTimePicker").value() != null) {
                                            lead[idControl] = kendo.toString(($("#" + idControl).data("kendoDateTimePicker") as kendo.ui.DateTimePicker).value(), "MM/dd/yyyy HH:mm:ss");
                                        }
                                    }
                                }
                                else {
                                    if ($("#" + idControl).data("kendoDatePicker") != null) {
                                        if ($("#" + idControl).data("kendoDatePicker").value() != null) {
                                            lead[idControl] = $("#" + idControl).data("kendoDatePicker").value();
                                        }
                                    }
                                }
                            }
                            //dropdown
                            //all the rest of the common controls of this type
                            if (result[i].FldTypeId === FieldType.UniqueIdentifier) {
                                if ((ctrGrp === LeadQuickPageVm.PHONE) || (ctrGrp === "Address") || (ctrGrp === "Email")) {
                                    //do nothing
                                } else if (idControl === "DrvLicStateCode") {
                                    lead[idControl] = this.driverLiscStateId;
                                }
                                else {
                                    if ($("#" + idControl).data("kendoDropDownList").value() != null) {
                                        lead[idControl] = $("#" + idControl).data("kendoDropDownList").value();

                                        if (idControl === "LeadAssignedToId") {
                                            lead.CampusId = lead[idControl];
                                        }
                                    }

                                }
                            }
                            //checkbox
                            //all the rest of the common controls of this type
                            if (result[i].FldTypeId === FieldType.Bit) {
                                lead[idControl] = ($("#" + idControl).prop("checked"));
                            }
                        }
                    }
                }
            }

            // Custom Fields
            var $inputs = $("#dvCustom :input");
            if ($inputs != null) {
                lead.SdfList = [];
                let dType: any;
                $inputs.each(function () {
                    var sdf = new Sdf();
                    sdf.SdfId = $(this).prop("id");
                    dType = $(this).attr("data-dtypeid");
                    if (dType === "4") {
                        sdf.SdfValue = $(this).prop("checked");
                    } else {
                        sdf.SdfValue = $(this).val();
                    }
                    lead.SdfList.push(sdf);
                });
            }

            // Lead Groups
            lead.GroupIdList = new Array<string>();
            if (this.gruppenDb != null) {

                // Get filtered group
                // Filter groups by CampusID
                var filteredGroup = new Array<Group>();
                for (var j = 0; j < this.gruppenDb.length; j++) {
                    if (this.gruppenDb[j].CampusId === lead.CampusId) {
                        filteredGroup.push(this.gruppenDb[j]);
                    }
                }

                // Get checked group
                for (var l = 0; l < filteredGroup.length; l++) {
                    if ($("#" + filteredGroup[l].GroupId).prop("checked") === true) {

                        lead.GroupIdList.push(filteredGroup[l].GroupId);
                    }
                }
            }
            //to set default values for the controls tht are not null in database and not selected/entered in this page
            if (lead.PreferredContactId == null) {
                lead.PreferredContactId = 1; //phone
            }
            if (lead.Note == null) {
                lead.Note = '';
            }
            if (lead.Comments == null) {
                lead.Comments = '';
            }
            if (lead.NickName == null) {
                lead.NickName = '';
            }
            //database call to save lead info
            // Request the info from server
            if (lead.SSN !== "" && lead.SSN !== undefined) {
                lead.SSN = lead.SSN.replace(/[^0-9]/g, "");
            }
            // Open progress panel
            MasterPage.Common.displayLoading(document.body, true);
            this.leadDb.postLeadInformation(lead, this)
                .done(msg => {
                    if (msg != undefined) {
                        $("body").css("cursor", "default");

                        //// in case of insert analyses the return info
                        if (lead.LeadId === '00000000-0000-0000-0000-000000000000') {
                            // Insertion return
                            if (msg.ValidationErrorMessages != null && msg.ValidationErrorMessages !== "") {
                                // A validation error happen
                                MasterPage.SHOW_WARNING_WINDOW(msg.ValidationErrorMessages);
                                return;
                            }

                            // See if a duplicate warning occurred
                            if (msg.DuplicatedMessages != null && msg.ValidationErrorMessages !== "") {
                                previousBtnClicked = btnName;
                                // A warning duplicate error happen
                                // Load the data-grid
                                var dupgrid = $("#infoPageDupGrid").data("kendoGrid") as kendo.ui.Grid;
                                var dsk = new kendo.data.DataSource({
                                    data: msg.PossiblesDuplicatesList
                                });
                                dupgrid.setDataSource(dsk);
                                var dupWin = $("#WindowsDuplicate").data("kendoWindow") as kendo.ui.Window;
                                dupWin.center().open();
                                return;
                            }
                            $.when(MasterPage.SHOW_INFO_WINDOW_PROMISE(X_LEAD_SAVED))
                                .then(confirmed => {
                                    //update the info bar
                                    this.updateLeadInfoBar(lead.FirstName,
                                        lead.NickName,
                                        lead.LastName,
                                        lead.ExpectedStart,
                                        lead.AssignedDate);

                                    if (btnName === "save" || (btnName === "insertLead" && previousBtnClicked === "save")) {
                                        //Region lead status new list
                                        if (msg.ValidStatusChangeList != null) {

                                            // A new list of change of status was send
                                            // Lead Status Analysis
                                            sessionStorage.setItem(QUICK_ALLOWEDLEADSTATUS, msg.ValidStatusChangeList);
                                            let statusControl = $("#LeadStatusId").data("kendoDropDownList") as any;
                                            let dllnew = this
                                                .selectLeadAllowedStates(statusControl,
                                                    msg.StateChangeIdsList,
                                                    lead.LeadStatusId);
                                            statusControl.setDataSource(dllnew);
                                            statusControl.value(msg.LeadStatusId);
                                        }
                                        //endRegion lead status new list
                                        sessionStorage.setItem(XLEAD_QUICK_NEW, "false");

                                        window.location
                                            .assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                                                "AD/AleadQuick.aspx?resid=268&NewEnt=1&cmpid=" +
                                                lead.CampusId +
                                                "&desc=Quick&mod=AD");
                                    } else if (btnName === "new") {
                                        sessionStorage.setItem(XLEAD_QUICK_NEW, LeadQuickPageVm.TRUE);
                                        window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                                            "AD/AleadQuick.aspx?resid=268&NewEnt=1&setNew=1&cmpid=" +
                                            XMASTER_GET_CURRENT_CAMPUS_ID +
                                            "&desc=Quick&mod=AD");
                                    } else if (btnName === "saveRedirect" || (btnName === "insertLead" && previousBtnClicked === "saveRedirect")) {
                                        sessionStorage.setItem(XLEAD_NEW, "false");
                                        if (lead.CampusId !== XMASTER_GET_CURRENT_CAMPUS_ID) {
                                            sessionStorage.setItem("MasterName", lead.FirstName + " " + lead.LastName);
                                            window.location
                                                .assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                                                    "AD/AleadInfoPage.aspx?resid=170&NewEnt=1&cmpid=" +
                                                    lead.CampusId +
                                                    //XMASTER_GET_CURRENT_CAMPUS_ID +
                                                    "&desc=InfoPageQuick&mod=AD&switchCampus=true");
                                        }
                                        else {
                                            window.location
                                                .assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                                                    "AD/AleadInfoPage.aspx?resid=170&NewEnt=1&cmpid=" +
                                                    lead.CampusId +
                                                    //XMASTER_GET_CURRENT_CAMPUS_ID +
                                                    "&desc=InfoPageQuick&mod=AD");
                                        }
                                    }
                                });
                            // window.location.reload(true);
                            return;
                        }

                        // Was update..................................................................................................................
                        $.when(MasterPage.SHOW_INFO_WINDOW_PROMISE(X_LEAD_SAVED))
                            .then(confirmed => {
                                //update info bar
                                this.updateLeadInfoBar(lead.FirstName,
                                    lead.NickName,
                                    lead.LastName,
                                    lead.ExpectedStart,
                                    lead.AssignedDate);
                                //Region lead status new list
                                if (msg.ValidStatusChangeList != null) {

                                    // A new list of change of status was send
                                    // Lead Status Analysis
                                    sessionStorage.setItem(QUICK_ALLOWEDLEADSTATUS, msg.ValidStatusChangeList);
                                    let statusControl = $("#LeadStatusId").data("kendoDropDownList") as any;
                                    let dllnew = this
                                        .selectLeadAllowedStates(statusControl,
                                            msg.StateChangeIdsList,
                                            lead.LeadStatusId);
                                    statusControl.setDataSource(dllnew);
                                    statusControl.value(msg.LeadStatusId);
                                }
                                //endRegion lead status new list
                                if (btnName === "new") {
                                    sessionStorage.setItem(XLEAD_QUICK_NEW, LeadQuickPageVm.TRUE);
                                    window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                                        "AD/AleadQuick.aspx?resid=268&NewEnt=1&setNew=1&cmpid=" +
                                        XMASTER_GET_CURRENT_CAMPUS_ID +
                                        "&desc=Quick&mod=AD");
                                } else if (btnName === "saveRedirect") {
                                    sessionStorage.setItem(XLEAD_NEW, "false");
                                    window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                                        "AD/AleadInfoPage.aspx?resid=170&NewEnt=1&cmpid=" +
                                        XMASTER_GET_CURRENT_CAMPUS_ID +
                                        "&desc=InfoPageQuick&mod=AD");
                                    //window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?resid=170&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&mod=AD");
                                }
                            });
                    }

                })
                .always(() => {

                    // Close progress panel
                    MasterPage.Common.displayLoading(document.body, false);
                })
                .fail((msg => {
                    $("body").css("cursor", "default");
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                }));
        }

        updateLeadInfoBar(fName: string, nName: string, lName: string, expectedStart: any, assignedDate: any) {
            let fullname: string = fName;
            fullname += (nName == null || nName === "") ? " " : " (" + nName + ") ";
            fullname += lName;
            let expectedStartP: string;
            if (expectedStart == null) {
                expectedStartP = "";
            } else {
                expectedStartP = expectedStart.toString();
            }
            let assignDate: string;
            if (assignedDate == null) {
                assignDate = "";
            } else {
                assignDate = assignedDate.toString();
            }
            let progVer = "";
            if ($("#PrgVerId").data("kendoDropDownList") != null) {
                progVer = $("#PrgVerId").data("kendoDropDownList").text();
            }
            LeadInfoBarVm.refreshLeadFields(
                fullname,
                progVer,
                expectedStartP,
                $("#AdmissionRepId").data("kendoDropDownList").text(),
                assignDate,
                $("#LeadStatusId").data("kendoDropDownList").text()
            );
        }

        getQuickLeadFields(isLead: boolean, mcontext: any) {
            var temp = sessionStorage.getItem(XLEAD_QUICK_FIELDS);
            var that = this;
            //if (temp == null || temp === "{}") {
            //load the info from database
            var res = this.db.getResourcesForQuickPage(this)
                .done(msg1 => {
                    if (msg1 != undefined) {
                        try {
                            sessionStorage.setItem(XLEAD_QUICK_FIELDS, JSON.stringify(res));

                            this.processFields(isLead, msg1);
                            //--------------------------------------------------------------------------------------------------
                            //var elements = $(":input");
                            //for (let x = 0; x < elements.length; x++) {
                            //    //var element = document.getElementById(elements[x].id);
                            //    elements[x].addEventListener("focus", that.processFocus);
                            //}
                            //--------------------------------------------------------------------------------------------------

                            // Process SDF //todo take sdf values independently from db and not from session
                            //var sdf = Object.create($.parseJSON(sessionStorage.getItem(XLEAD_QUICK_INFO_SDF_FIELDS))); // all sdf processing in other page
                            this.processSdfFields(isLead);
                            this.processLeadGrpFields(isLead);
                            if (!isLead) {
                                //assigning the default value to date assigned as current system date
                                if ($("#AssignedDate") != null) {
                                    ($("#AssignedDate").data("kendoDatePicker") as any).value(new Date());
                                }
                                //hiding the stateInternational by default for 1st time
                                let maskedtextbox;
                                if ($("#ZipCode") != null) {
                                    maskedtextbox = $("#ZipCode").data("kendoMaskedTextBox") as any;
                                }
                                let ddlState;
                                if ($("#StateId") != null) {
                                    ddlState = $("#StateId").data("kendoDropDownList") as any;
                                }
                                if (maskedtextbox != null) {
                                    maskedtextbox.setOptions({ mask: "00000" });
                                }
                                if ($("#StateInternational") != null) {
                                    $("#StateInternational").attr("placeholder", "State");
                                    $("#StateInternational").attr("style", "display: none");
                                }
                                if (ddlState != null) {
                                    ddlState.wrapper.show();
                                }


                            }
                            // this.initializePage(true, msg1); // called inside process fields

                        } catch (e) {
                            $("body").css("cursor", "default");
                            MasterPage.SHOW_DATA_SOURCE_ERROR(e.toString());
                            sessionStorage.removeItem(XLEAD_QUICK_FIELDS);
                        }
                    }
                }).fail((msg => {
                    $("body").css("cursor", "default");
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                }));
            //}
            //take from session
            //else {
            //that.filterDataDb = Object.create($.parseJSON(temp));
            //this.processFields(that.filterDataDb);//todo call to get the data from seeion for leadQuick field,sdf,leadGrp

            //}
        }

        //------------------------------------------------------------------------------------------------------------------------
        //processFocus() {
        //    var center = $(window).height() / 2;
        //    var top = $(this).offset().top;
        //    if (top > center) {
        //        $(window).scrollTop(top - center);
        //    }
        //};
        //------------------------------------------------------------------------------------------------------------------------

        processLeadGrpFields(isLead: boolean) {
            //if (!isLead) {
            var grps = Object.create($.parseJSON(sessionStorage.getItem(XLEAD_INFO_GROUPS)));
            if (grps != null) {
                this.gruppenDb = grps as IGroup[];
            } else {
                var grpsD = this.db.getLeadGrpList(170, XMASTER_GET_CURRENT_CAMPUS_ID, this)
                    .done(msg => {
                        if (msg != undefined) {
                            this.gruppenDb = msg;
                            sessionStorage.setItem(XLEAD_INFO_GROUPS, JSON.stringify(msg));
                        }
                    }).fail((msg => {
                        $("body").css("cursor", "default");
                        MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    }));
            }
            // Filter groups by CampusID
            var filteredGroup = new Array<Group>();
            for (var j = 0; j < this.gruppenDb.length; j++) {
                if (this.gruppenDb[j].CampusId === XMASTER_GET_CURRENT_CAMPUS_ID) {
                    filteredGroup.push(this.gruppenDb[j]);
                }
            }

            //Create the check-box containers for the filtered groups.
            filteredGroup.sort(function (a, b) {
                var nA = a.Description.toLocaleLowerCase();
                var nB = b.Description.toLocaleLowerCase();
                if (nA < nB) {
                    return -1;
                } else if (nA > nB) {
                    return 1;
                }
                return 0;
            });
            //for (var i = 0; i < filteredGroup.length; i++) {
            //    var html: string = '<input type="checkbox" class="k-checkbox" id="' + filteredGroup[i].GroupId + '" /> <label class="k-checkbox-label "  for="' + filteredGroup[i].GroupId + '">' + filteredGroup[i].Description + '</label>';
            //    html = "<div>" + html + "</div>";
            //    $("#dvLeadGrp").append(html);
            //}
            var html: string;
            for (var i = 0; i < filteredGroup.length; i++) {
                html = `<input type="checkbox" class="k-checkbox" id="${filteredGroup[i].GroupId}" />`;
                html = html + '<label class="k-checkbox-label" for="' + filteredGroup[i].GroupId + '" > ' + filteredGroup[i].Description + ' </label>';
                html = `<div >${html}</div>`;
                $("#dvLeadGrp").append(html);
            }
            $(document).find("html").removeClass('rfdLabel');
        }

        processSdfFields(isLead: boolean) {
            //Create the school defined control
            //new lead so get data from DB
            if (!isLead) {
                var sdfList = this.db.getSdfList(268, XMASTER_GET_CURRENT_CAMPUS_ID, this)
                    .done(msg => {
                        if (msg != undefined) {
                            try {
                                sessionStorage.setItem(XLEAD_QUICK_INFO_SDF_FIELDS, JSON.stringify(sdfList));
                                if (msg !== null) {
                                    //call
                                    this.displaysdf(msg);
                                }
                            } catch (e) {
                                $("body").css("cursor", "default");
                                MasterPage.SHOW_ERROR_WINDOW(e.message);
                            }
                        }
                    }).fail((msg => {
                        $("body").css("cursor", "default");
                        MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    }));
            }
            else {
                var temp: any = Object.create($.parseJSON(sessionStorage.getItem(XLEAD_QUICK_INFO_SDF_FIELDS)));
                var fixedResponse = temp.responseText.replace(/\\'/g, "'");
                var result = JSON.parse(fixedResponse);
                if (result != null) {
                    this.displaysdf(result);
                }
            }
        }
        displaysdf(msg: any) {

            let result = MasterPage.AdvantageUserDefinedFields.processSdfFields(msg, "dvCustom");
            if (result === 1) {
                $("#dvCustomBar").attr("style", "display:block");
                $("#dvCustom").attr("style", "display:block");
            }

        }
        initializePage(isLead: boolean, msg1) {
            if (isLead) {
                var leadElement: any = document.getElementById('leadId');
                var leadGuid = leadElement.value;

                // Load the specific lead information
                this.getLeadDemographic(leadGuid, msg1);
            }
        }

        getLeadDemographic(leadGuid, msg1) {
            // Request the info from server
            var userId: string = $("#hdnUserId").val();
            MasterPage.SHOW_LOADING(true);
            this.leadDb.getLeadDemographic(leadGuid, userId, this)
                .done(msg => {
                    MasterPage.SHOW_LOADING(false);
                    if (msg != undefined) {

                        try {
                            // var result = JSON.stringify(msg);
                            var res: ILeadInfoPageOutputModel = msg;
                            var isPhoneAssigned = false, isAddressAssigned = false, isemailAssigned = false, isAddDdlAssigned = false;

                            for (let k = 0; k < msg1.length; k++) {
                                //var idControl = msg1[k].PropName;
                                //textbox for SmallInt,Int,Float,Money,TinyInt,Char,Decimal,Varchar
                                if ((msg1[k].FldTypeId === FieldType.SmallInt) ||
                                    (msg1[k].FldTypeId === FieldType.Int) ||
                                    (msg1[k].FldTypeId === FieldType.Float) ||
                                    (msg1[k].FldTypeId === FieldType.Money) ||
                                    (msg1[k].FldTypeId === FieldType.TinyInt) ||
                                    (msg1[k].FldTypeId === FieldType.Char) ||
                                    (msg1[k].FldTypeId === FieldType.Decimal) ||
                                    (msg1[k].FldTypeId === FieldType.Varchar)) {
                                    let ctrGrp = msg1[k].CtrlIdName;
                                    let propertyNameK = msg1[k].PropName;
                                    if (ctrGrp != null) {
                                        if (ctrGrp === LeadQuickPageVm.PHONE) {
                                            if (!isPhoneAssigned) {
                                                let list = msg.PhonesList;
                                                if (list != null) {
                                                    for (let i = 0; i < list.length; i++) {
                                                        if (list[i].Position === 1) {
                                                            let isf = list[i].IsForeignPhone;
                                                            if (isf != null) {
                                                                if ($("#Phone") != null) {
                                                                    if (isf) {
                                                                        ($("#Phone").data("kendoMaskedTextBox") as any).setOptions({ mask: '999999999999999' });
                                                                    }
                                                                    ($("#Phone").data("kendoMaskedTextBox") as any).value(list[i].Phone);
                                                                    $("#hiddenPhoneId").val(list[i].ID);
                                                                    $("#Extension").val(list[i].Extension);
                                                                    isPhoneAssigned = true;
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        // #region Address
                                        else if (ctrGrp === "Address") {
                                            let state = $("#StateId").data("kendoDropDownList") as any;
                                            let stateInt = $("#StateInternational");
                                            if (!isAddressAssigned) {
                                                let addList = msg.LeadAddress;
                                                if (addList != null) {
                                                    if (addList.Address1 != null) {
                                                        $("#Address1").val(addList.Address1);
                                                    }
                                                    if (addList.AddressApto != null) {
                                                        $("#AddressApto").val(addList.AddressApto);
                                                    }
                                                    let isf = addList.IsInternational;
                                                    let zipCode = $("#ZipCode").data("kendoMaskedTextBox") as any;

                                                    if (isf != null) {
                                                        $("#IsInternational").prop("checked", isf);
                                                        if (isf) {
                                                            if (state != null) {
                                                                state.wrapper.hide();
                                                            }
                                                            stateInt.css("display", "inline-block");
                                                            stateInt.val(addList.StateInternational);
                                                            zipCode.setOptions({ mask: "aaaaaaaaaa" });

                                                        } else {
                                                            if (state != null) {
                                                                state.wrapper.show();
                                                            }
                                                            stateInt.css("display", "none");
                                                            zipCode.setOptions({ mask: "00000" });
                                                            if (state != null) {
                                                                if (addList.stateId === null) {
                                                                    state.value("00000000-0000-0000-0000-000000000000");
                                                                } else {
                                                                    state.value(addList.StateId);
                                                                }
                                                            }
                                                        }
                                                    } else { //isf is null tht means nothing is in address
                                                        if (state != null) {
                                                            state.wrapper.show();
                                                        }
                                                        stateInt.css("display", "none");
                                                        zipCode.setOptions({ mask: "00000" });
                                                        if (state != null) {
                                                            if (addList.stateId === null) {
                                                                state.value("00000000-0000-0000-0000-000000000000");// as no state is slected as it is first time always
                                                            }
                                                        }
                                                    }
                                                    if (addList.City != null) {
                                                        $("#City").val(addList.City);
                                                    }

                                                    if (addList.ZipCode != null) {
                                                        // $("#ZipCode").val(addList.ZipCode);
                                                        zipCode.value(addList.ZipCode);
                                                    }

                                                    isAddressAssigned = true;
                                                } else {//no adress at all in db
                                                    if (state != null) {
                                                        state.wrapper.show();
                                                    }
                                                    stateInt.css("display", "none");
                                                }

                                            }
                                        }
                                        //#endregion

                                        else if (ctrGrp === "Email") {
                                            if (!isemailAssigned) {
                                                let emailList = msg.EmailList;
                                                if (emailList != null) {
                                                    for (let em = 0; em < emailList.length; em++) {
                                                        if (emailList[em].IsPreferred) {
                                                            $("#hiddenEmailPrimaryId").val(emailList[em].ID);
                                                            $("#Email").val(emailList[em].Email);
                                                            ($("#EmailType").data("kendoDropDownList") as any)
                                                                .value(emailList[em].EmailTypeId);
                                                        }
                                                        isemailAssigned = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else if (res[msg1[k].PropName] != null) {
                                            if (msg1[k].PropName === "Dependants" ||
                                                msg1[k].PropName === "DistanceToSchool") {
                                                ($("#" + msg1[k].PropName).data("kendoNumericTextBox") as any).value(res[msg1[k].PropName]);
                                            } else {
                                                if (msg1[k].PropName === "SSN") {
                                                    ($("#" + msg1[k].PropName).data("kendoMaskedTextBox") as any).value(res[msg1[k].PropName]);
                                                } else {

                                                    $("#" + msg1[k].PropName).val(res[msg1[k].PropName]);
                                                }
                                            }
                                        }
                                    }
                                }
                                //dateal
                                if (msg1[k].FldTypeId === FieldType.DateTime) {
                                    let dtPropName = msg1[k].PropName;
                                    if (res[dtPropName] != null) {
                                        if (dtPropName === "ExpectedStart") {
                                            //if (XMASTER_LEAD_IS_CLOCK_HOUR === "YES") {
                                            //    ($("#" + dtPropName).data("kendoDatePicker") as any)
                                            //        .value(res[dtPropName]);
                                            //} else {
                                            //    ($("#" + dtPropName).data("kendoDropDownList") as any).value(res[dtPropName]);
                                            //}
                                        } else if (dtPropName === LeadQuickPageVm.BEST_TIME) {
                                            if (res[dtPropName] != null) {
                                                ($("#BestTime").data("kendoTimePicker") as any).value(res[dtPropName]);
                                            }
                                        } else if (dtPropName === LeadQuickPageVm.SOURCE_DATE_TIME) {
                                            if (res[dtPropName] != null) {
                                                kendo.toString(($("#" + LeadQuickPageVm.SOURCE_DATE_TIME).data("kendoDateTimePicker") as any).value(res[dtPropName]), "MM/dd/yyyy HH:mm:ss");
                                            }
                                        } else if (msg1[k].PropName === "Dob") {
                                            let age = "";
                                            if ($("#Age") != null) {
                                                age = $("#Age").val();
                                            }

                                            if (res[dtPropName] != null) {
                                                this.calculateAge((res[dtPropName]).toString(), age);
                                            } else {
                                                this.calculateAge(null, age);
                                            }
                                        } else {
                                            ($("#" + dtPropName).data("kendoDatePicker") as any)
                                                .value(res[dtPropName]);
                                        }

                                    }
                                }
                                //drop down
                                if (msg1[k].FldTypeId === FieldType.UniqueIdentifier) {
                                    let ctrGrp = msg1[k].CtrlIdName;
                                    let uiPropName = msg1[k].PropName;
                                    if (ctrGrp != null) {
                                        if (ctrGrp === LeadQuickPageVm.PHONE) {
                                            let list = msg.PhonesList;
                                            for (let i = 0; i < list.length; i++) {
                                                if (list[i].Position === 1) {
                                                    let idControl = uiPropName;
                                                    if ($("#" + idControl).data("kendoDropDownList") != null) {
                                                        ($("#" + idControl).data("kendoDropDownList") as any)
                                                            .value(list[i].PhoneTypeId);
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                        else if (ctrGrp === "Email") {//do nothing
                                        }
                                        else if (ctrGrp === "Address") {
                                            if (!isAddDdlAssigned) {
                                                let addList = msg.LeadAddress;
                                                if (addList != null) {
                                                    isAddDdlAssigned = true;
                                                    if ($("#AddressTypeId").data("kendoDropDownList") != null) {
                                                        if (addList.AddressTypeId != null) {
                                                            ($("#AddressTypeId").data("kendoDropDownList") as any)
                                                                .value(addList.AddressTypeId);
                                                        } else {
                                                            isAddDdlAssigned = false;
                                                        }
                                                    }
                                                    if ($("#CountyId").data("kendoDropDownList") != null) {
                                                        if (addList.CountyId != null) {
                                                            ($("#CountyId").data("kendoDropDownList") as any)
                                                                .value(addList.CountyId);
                                                        } else {
                                                            isAddDdlAssigned = false;
                                                        }
                                                    }
                                                    if ($("#CountryId").data("kendoDropDownList") != null) {
                                                        if (addList.CountryId != null) {
                                                            ($("#CountryId").data("kendoDropDownList") as any)
                                                                .value(addList.CountryId);
                                                        } else {
                                                            isAddDdlAssigned = false;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else if (uiPropName === "DrvLicStateCode") {
                                            let dState = $("#DrvLicStateCode").data("kendoDropDownList") as any;
                                            if (dState != null) {
                                                if (res[uiPropName] === null) {
                                                    dState.value("00000000-0000-0000-0000-000000000000");
                                                } else {
                                                    dState.value(res[uiPropName]);
                                                }
                                            }
                                        } else if (uiPropName === "ProgramId") { //program dropdown
                                            this
                                                .getLeadProgramInfo(res,
                                                    "Program",
                                                    "ProgramId",
                                                    res.AreaId,
                                                    res.ProgramId);
                                        } else if (uiPropName === "LeadStatusId") {
                                            //sessionStorage
                                            //    .setItem(QUICK_ALLOWEDLEADSTATUS, JSON.stringify(msg.StateChangeIdsList));
                                            let statusControl = $("#LeadStatusId").data("kendoDropDownList") as any;
                                            //let dllnewR = this.selectLeadAllowedStates(statusControl,msg.StateChangeIdsList,msg.LeadStatusId); 
                                            //statusControl.setDataSource(dllnewR);
                                            statusControl.value(msg.LeadStatusId);
                                        } else if (msg1[k].PropName === "AttendingHs") {
                                            ($("#AttendingHs").data("kendoDropDownList") as any).value(res[uiPropName]);
                                        }
                                        else if (res[uiPropName] != null || uiPropName === "LeadAssignedToId") {
                                            let idControl = msg1[k].PropName;
                                            if (idControl === "ProgramId" ||
                                                idControl === "PrgVerId" ||
                                                idControl === "ProgramScheduleId" ||
                                                idControl === "DrvLicStateCode" ||
                                                idControl === "AttendingHs") {
                                                //do nothing
                                            }
                                            if ($("#" + idControl).data("kendoDropDownList") != null) {
                                                if (idControl === "LeadAssignedToId") {
                                                    if (res.CampusId == null) {
                                                        res.CampusId = XMASTER_GET_CURRENT_CAMPUS_ID;
                                                    }
                                                }
                                                ($("#" + idControl).data("kendoDropDownList") as any).value(res[idControl !== "LeadAssignedToId" ? idControl : "CampusId"]);
                                            }
                                        }
                                    }

                                }
                                //checkbox
                                if (msg1[k].FldTypeId === FieldType.Bit) {
                                    let ctrGrp = msg1[k].CtrlIdName;
                                    let bPropName = msg1[k].PropName;
                                    if (ctrGrp != null) {
                                        if (ctrGrp === LeadQuickPageVm.PHONE) {
                                            let list = msg.PhonesList;
                                            for (let i = 0; i < list.length; i++) {
                                                if (list[i].Position === 1) {
                                                    let isf = list[i].IsForeignPhone;
                                                    if (isf != null) {
                                                        $("#" + bPropName).prop("checked", isf);
                                                    }
                                                    break;
                                                }
                                            }
                                        } else if (ctrGrp === "Address") {
                                            //do nothing
                                        }
                                    }
                                }
                            }

                            //#region Custom Fields
                            if (msg.SdfList != null) {

                                for (var k = 0; k < msg.SdfList.length; k++) {
                                    var id: string = "#" + msg.SdfList[k].SdfId;
                                    if ($(id).attr("data-role") === 'dropdownlist') {
                                        if ($(id).data("kendoDropDownList") != null) {
                                            $(id).data("kendoDropDownList").value(msg.SdfList[k].SdfValue);
                                            $(id).data("kendoDropDownList").list.width("auto");
                                        }
                                    } else {
                                        if (msg.SdfList[k].SdfValue != null) {
                                            //get as per the datatype
                                            //character or number
                                            if (msg.SdfList[k].DtypeId === 1 || msg.SdfList[k].DtypeId === 2) {
                                                $(id).val(msg.SdfList[k].SdfValue);
                                            }
                                            //date
                                            if (msg.SdfList[k].DtypeId === 3) {
                                                if ($(id).data("kendoDatePicker") != null) {
                                                    ($(id).data("kendoDatePicker") as any)
                                                        .value(msg.SdfList[k].SdfValue);
                                                }
                                            }
                                            //checkbox
                                            if (msg.SdfList[k].DtypeId === 4) {
                                                let isf = msg.SdfList[k].SdfValue;
                                                if (isf != null) {
                                                    if (isf === false || isf === "False") {
                                                        //do nothing
                                                    } else {
                                                        if ($(id) != null) {
                                                            $(id).prop("checked", isf);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //#endregion

                            //#region Lead Groups

                            // Check the necessary groups
                            var grps: any = Object.create($.parseJSON(sessionStorage.getItem(XLEAD_INFO_GROUPS)));
                            if (grps != null) {

                                // Clear all
                                for (var l = 0; l < grps.length; l++) {
                                    $("#" + grps[l].GroupId).prop("checked", false);
                                }

                                // Check only the used by the lead
                                if (msg.GroupIdList != null) {
                                    for (var m = 0; m < msg.GroupIdList.length; m++) {
                                        $("#" + msg.GroupIdList[m]).prop("checked", true);
                                    }
                                }
                            }

                            //#endregion

                        } catch (e) {
                            //If errors clear the cache
                            //sessionStorage.removeItem(XLEAD_INFO_DLLS_REQUIRED_ITEMS);
                            //sessionStorage.removeItem(XLEAD_INFO_GROUPS);
                            //sessionStorage.removeItem(XLEAD_QUICK_INFO_SDF_FIELDS);
                            //$("body").css("cursor", "default");
                            $("body").css("cursor", "default");
                            MasterPage.SHOW_ERROR_WINDOW(e.message);
                        }

                    }
                })
                .fail(msg => {
                    $("body").css("cursor", "default");
                    MasterPage.SHOW_LOADING(false);
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }

        /*
         * From server comes the allowed lead status for the actual lead status
         * the status possibles are not filtered by campus, but by the configured
         * possible status. Always the actual status is added to list. 
         */
        selectLeadAllowedStates(sourceDll: any, allowedStatusChange: any, leadStatusId: string): kendo.data.DataSource {
            var dllnew = new kendo.data.DataSource();
            var dll = sourceDll;
            if (allowedStatusChange !== null && allowedStatusChange !== undefined && allowedStatusChange.length > 0) {
                for (var n = 0; n < dll.length; n++) {
                    for (var o = 0; o < allowedStatusChange.length; o++) {
                        if (dll[n].ID.toLocaleString().toUpperCase() === allowedStatusChange[o].toLocaleString().toUpperCase()) {
                            dllnew.add(dll[n]);
                        }
                    }
                }
            }
            var result = $.grep(dll, (t: any) => t.ID === leadStatusId);
            dllnew.add(result[0]);
            return dllnew;
        }

        /* Get the Program (program combo box) Information. Also should trigger the Expected Start and Program Version  */
        getLeadProgramInfo(msgOr: any, fldName: string, targetDropDown: string, value: Object, setting: string = null) {
            // Request the info from server
            this.leadDb.getLeadItemInformation(fldName, value, XMASTER_GET_CURRENT_CAMPUS_ID, this)
                .done(msg => {
                    if (msg != undefined) {
                        // Enter the information in the controls
                        var cbprogramLabel: any = $("#" + targetDropDown).data("kendoDropDownList");
                        var isClockHourProgram: boolean = false;
                        // Set the data source
                        if (cbprogramLabel != null) {
                            cbprogramLabel.setDataSource(msg);
                            // Optional set the value of the filled drop down
                            if (setting != null) {
                                cbprogramLabel.value(setting);
                                for (var i = 0; i < msg.length; i++) {
                                    if (setting.toLowerCase() === msg[i].ID.toLowerCase()) {
                                        if (msg[i].CalendarType.toLowerCase() === "clock hour") {
                                            isClockHourProgram = true;

                                        } else {
                                            isClockHourProgram = false;
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                        // Call next cascade (Program Version)
                        this.getLeadProgramTypeInfo(msgOr, "PrgVerId", "PrgVerId", msgOr.ProgramId, msgOr.PrgVerId);

                        // Call Next Cascade ExpectedStart Date
                        //if (XMASTER_LEAD_IS_CLOCK_HOUR === 'Yes') {
                        var cbexpectedStart = $("#ddlExpectedStart").data("kendoDropDownList") as any;
                        if (isClockHourProgram) {
                            $("#dpExpectedStart").css("display", "inline");
                            $("#dpExpectedStart").css("width", "auto");
                            $("#dpExpectedStart").kendoDatePicker();
                            cbexpectedStart.wrapper.hide();
                            if (msgOr.ExpectedStart != null && msgOr.ExpectedStart != undefined) {
                                ($("#dpExpectedStart").data("kendoDatePicker") as kendo.ui.DatePicker).value(kendo.parseDate(msgOr.ExpectedStart));
                            }
                        } else {
                            cbexpectedStart.wrapper.show();
                            $("#dpExpectedStart").parent().parent().hide();
                            // ExpectedStart with Term...
                            if (msgOr.ExpectedStart == null) {
                                this.getExpectedLeadItemInfo(XMASTER_GET_CURRENT_CAMPUS_ID, "ExpectedStart", "ddlExpectedStart", msgOr.ProgramId);
                            } else {
                                var dat1: Date = kendo.parseDate(msgOr.ExpectedStart);
                                var lookdate: string = dat1.toLocaleString("en-US").split(" ")[0].toString();
                                lookdate = lookdate.replace(",", "");
                                this.getExpectedLeadItemInfo(XMASTER_GET_CURRENT_CAMPUS_ID, "ExpectedStart", "ddlExpectedStart", msgOr.ProgramId, lookdate);
                            }
                        }
                        //$("#ExpectedStart").data("kendoDatePicker").value(kendo.parseDate(msgOr.ExpectedStart));
                        //} else {
                        //    // ExpectedStart with Term...
                        //    if (msgOr.ExpectedStart == null) {
                        //        this.getExpectedLeadItemInfo(XMASTER_GET_CURRENT_CAMPUS_ID, "ExpectedStart", "ExpectedStart", msgOr.ProgramId);
                        //    } else {
                        //        var dat1: Date = kendo.parseDate(msgOr.ExpectedStart);
                        //        var lookdate: string = dat1.toLocaleString("en-US").split(" ")[0].toString();
                        //        lookdate = lookdate.replace(",", "");
                        //        this.getExpectedLeadItemInfo(XMASTER_GET_CURRENT_CAMPUS_ID, "ExpectedStart", "ExpectedStart", msgOr.ProgramId, lookdate);
                        //    }
                        //}
                    }
                })
                .fail(msg => {
                    $("body").css("cursor", "default");
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }
        getLeadProgramTypeInfo(msgOr: any, fldName: string, targetDropDown: string, value: Object, setting: string = null) {
            // Request the info from server
            this.leadDb.getLeadItemInformation(fldName, value, XMASTER_GET_CURRENT_CAMPUS_ID, this)
                .done(msg => {
                    if (msg != undefined) {
                        // Enter the information in the controls
                        var cbprogramVersion = $("#" + targetDropDown).data("kendoDropDownList");

                        // Set the data source
                        if (cbprogramVersion != null) {
                            cbprogramVersion.setDataSource(msg);
                            // Optional set the value of the filled drop down
                            if (setting != null) {
                                cbprogramVersion.value(setting);
                            }
                        }

                        // Get the schedule and the expected start date
                        this.getItemInfo(XMASTER_GET_CURRENT_CAMPUS_ID, "ProgramScheduleId", "ScheduleId", msgOr.PrgVerId, msgOr.ScheduleId);
                    }
                })
                .fail(msg => {
                    $("body").css("cursor", "default");
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }
        /* Get the expected Start of the Lead */
        getExpectedLeadItemInfo(campusId: string, fldName: string, targetDropDown: string, value: Object, setting: string = null) {
            // Request the info from server
            this.leadDb.getLeadItemInformation(fldName, value, campusId, this)
                .done(msg => {
                    if (msg != undefined) {
                        // Enter the information in the controls
                        var cbExpected = $("#ddlExpectedStart").data("kendoDropDownList");

                        // Set the data source
                        if (cbExpected != null) {
                            cbExpected.setDataSource(msg);
                            // Optional set the value of the filled drop down
                            if (setting != null) {
                                // Get the list of drop down
                                var list = cbExpected.dataSource.data();
                                if (list != null) {

                                    // Look up through the list to get the item that contain the date
                                    for (var i = 0; i < list.length; i++) {
                                        var descriptdate: string = list[i].Description.split("]")[0].slice(1).toString().trim();
                                        if (descriptdate.localeCompare(setting) === 0) {

                                            // Enter the value
                                            cbExpected.value(list[i].ID);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        // Resize the drop down to avoid wrap
                        //    this.resizeDropDown(cbExpected);
                    }
                })
                .fail(msg => {
                    $("body").css("cursor", "default");
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }
        /* Manage changes of a specific drop down it is used to manage the user change in cascade drop down */
        getItemInfo(campusId: string, fldName: string, targetDropDown: string, value: Object, setting: string = null) {
            // Request the info from server
            this.leadDb.getLeadItemInformation(fldName, value, campusId, this)
                .done(msg => {
                    if (msg != undefined) {
                        // Enter the information in the controls
                        var itemdd = $("#" + targetDropDown).data("kendoDropDownList");

                        // Set the data source
                        if (itemdd != null) {
                            itemdd.setDataSource(msg);
                            // Optional set the value of the filled drop down
                            if (setting != null) {
                                itemdd.value(setting);
                            }
                        }
                    }
                })
                .fail(msg => {
                    $("body").css("cursor", "default");
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }
        processFields(isLead: boolean, msg1) {
            //var msg: ILeadQuickFields = msg1;
            //create controls dynamically
            var that = this;
            var isPhoneDisplayed = false;
            var isEmailDisplayed = false;
            var isAddressDisplayed = false;
            for (var i = 0; i < msg1.length; i++) {
                var html = "";
                var requiredText = "";
                if (msg1[i].Required === 1) {
                    requiredText = "<span style=color:red>*</span>";
                }

                html += "<div>";
                var idControl = msg1[i].PropName;

                let ctrGrp = msg1[i].CtrlIdName;
                if (ctrGrp != null) {
                    //displaying all the phone controls on one line
                    if (ctrGrp === LeadQuickPageVm.PHONE) {
                        if (!isPhoneDisplayed) {
                            //Phone - Best Phone type
                            html +=
                                "<div class = 'quickLabel'><label>Phone - Best</label></div>" +
                                "<div class = 'quickControl'>" +
                                " <div class = 'col-quick-lead-20' ><input id = 'PhoneTypeId' data-validation='dependRequired' data-dependable='Phone' data-empty='Select'  name = 'Type' data-ControlName = 'Phone' data-controlType = '72' data-FieldName = 'PhoneType' /> </div>" +
                                //Phone Number
                                "<div class = 'col-quick-lead-20' ><input id = 'Phone' type='tel'  name = 'PhoneBest' data-ControlName = 'Phone' data-controlType = '200' data-FieldName = 'Phone' /></div>" +
                                //Phone Ext
                                "<div class = 'col-quick-lead-22' ><input id = 'Extension'  name = 'Extension' data-ControlName = 'Phone' data-controlType = '200' data-FieldName = 'Extension' /> &nbsp;" +
                                //International checkbox
                                "<input type='checkBox' id = 'IsForeignPhone' style='vertical-align:top;margin-top:5px;'  name = 'International' data-ControlName = 'Phone' data-controlType = '11' data-FieldName = 'IsForeignPhone' /><span>Int'l </span></div>" +
                                "</div>";
                            isPhoneDisplayed = true;
                        }

                    } else if (ctrGrp === "Address") {
                        if (!isAddressDisplayed) {
                            //address 1,address type
                            html +=
                                "<div class= 'quickLabel'><label>Address 1</label></div>" +
                                "<div class = 'quickControl'>" +
                                "<div class = 'col-quick-lead-20' ><input id = 'AddressTypeId' data-validation='dependRequired' data-dependable='Address1' data-empty='Select' name = 'Type' data-ParentId = 'LeadAssignedToId' data-ControlName = 'Address' data-controlType = '72' data-FieldName = 'AddressType' /></div>" +
                                //address 1 text box
                                "<div class = 'col-quick-lead-20' ><input id = 'Address1'  name = 'Address' data-ControlName = 'Address' data-controlType = '200' data-FieldName = 'Address1' /> </div>" +
                                // apt
                                "<div class = 'col-quick-lead-22' ><input id = 'AddressApto'   name = 'Apt' data-ControlName = 'Address' data-controlType = '200' data-FieldName = 'AddressApto' />  &nbsp;" +
                                //checkbox international
                                "<input type='checkBox' id = 'IsInternational' style='vertical-align:top;margin-top:5px;'  name = 'International' data-ControlName = 'Address' data-controlType = '11' data-FieldName = 'ForeignZip' /><span>Int'l </span></div>" +
                                "</div>" +
                                //next line
                                "</div> <div class='clearfix bottomSpace'></div>" +
                                //city
                                "<div class= 'quickLabel'><label>City, State, Zip</label></div>" +
                                "<div class = 'quickControl'>" +
                                "<div class = 'col-quick-lead-20' ><input id = 'City'  name = 'Type' data-ControlName = 'Address' data-controlType = '200' data-FieldName = 'City' /> </div>" +
                                "<div class = 'col-quick-lead-20' ><input id = 'StateId'  name = 'StateId' data-ControlName = 'Address' data-controlType = '72' data-FieldName = 'StateId' />  " +
                                "<input id = 'StateInternational' style = 'display:none' name = 'StateInternational' data-ControlName = 'Address' data-controlType = '200' data-FieldName = 'OtherState' /> </div>" +
                                "<div class = 'col-quick-lead-22' ><input id = 'ZipCode'  name = 'Apt' data-ControlName = 'ZipCode' data-controlType = '200' data-FieldName = 'Zip' /></div>" +
                                //next line
                                "</div> <div class='clearfix bottomSpace'></div>" +
                                //county,country
                                "<div class= 'quickLabel'><label>County</label></div>" +
                                "<div class = 'quickControl'>" +
                                "<div class = 'col-quick-lead-20' ><input id = 'CountyId'  name = 'CountyId' data-ParentId = 'LeadAssignedToId' data-ControlName = 'Address' data-controlType = '72' data-FieldName = 'County' /></div>" +
                                //next line
                                "</div> <div class='clearfix bottomSpace'></div>" +
                                //country
                                "<div class = 'quickLabel' ><label >Country</label></div>" +
                                "<div class = 'col-quick-lead-20' ><input id = 'CountryId'  name = 'CountryId' data-ParentId = 'LeadAssignedToId' data-ControlName = 'Address' data-controlType = '72' data-FieldName = 'Country' /></div></div>";
                            isAddressDisplayed = true;
                        }
                    }
                    else if (ctrGrp === "Email") {
                        if (!isEmailDisplayed) {
                            //email -Best, email type
                            html +=
                                "<div class = 'quickLabel'><label>Email - Best</label></div>" +
                                "<div class = 'quickControl'>" +
                                "<div class = 'col-quick-lead-20' ><input id = 'EmailType' data-validation='dependRequired' data-dependable='Email' data-empty='Select' name = 'Type' data-ControlName = 'Email' data-controlType = '72' data-FieldName = 'EmailTypeId' /></div>" +
                                //email
                                "<div class = 'col-quick-lead-20' ><input id = 'Email' name = 'Email' type = 'email' data-ControlName = 'Email' data-controlType = '200' data-FieldName = 'HomeEmail' /></div></div>";
                            isEmailDisplayed = true;
                        }
                    }
                }
                //bit (checkbox)
                if (msg1[i].FldTypeId === FieldType.Bit) {
                    if (ctrGrp === LeadQuickPageVm.PHONE || ctrGrp === "Address") {//do nothing
                    } else {
                        let checkBox1 = "k-checkbox";
                        //var checkBox1 = "checkphone fieldlabel1";
                        $("#" + idControl).attr("class", checkBox1);
                        html += "<div class = 'quickLabel'><label>" +
                            msg1[i].Caption +
                            requiredText +
                            "</label></div><div class = 'quickControl'><input type='checkBox' id= '" +
                            idControl +
                            "' name = '" +
                            idControl +
                            //msg1[i].Caption +
                            "' data-ControlName='" + msg1[i].CtrlIdName + "' data-controlType='" + msg1[i].FldTypeId + "' data-FieldName = '" + msg1[i].FldName + "'/></div>";
                    }

                } else if (ctrGrp === LeadQuickPageVm.PHONE || ctrGrp === "Email" || ctrGrp === "Address") {//do nothing
                }
                else if (msg1[i].ParentCtrlId != null) {
                    html += "<div class = 'quickLabel'><label >" +
                        msg1[i].Caption +
                        requiredText +
                        "</label></div><div class = 'quickControl'><input id= '" +
                        idControl + "' name = '" + idControl + "' data-ParentId = '" + msg1[i].ParentCtrlId +
                        "' data-ControlName='" + msg1[i].CtrlIdName + "' data-controlType='" + msg1[i].FldTypeId + "' data-FieldName = '" + msg1[i].FldName + "'/></div>";
                }
                else {
                    if (msg1[i].PropName === "Comments") {//txtarea for comments
                        html += "<div class = 'quickLabel'><label >" +
                            msg1[i].Caption +
                            requiredText +
                            "</label></div><div class = 'quickControl'><textarea class = 'biggerComment' maxlength='100' rows='2' id= '" +
                            idControl +
                            "' name = '" +
                            idControl +
                            "' data-ControlName='" + msg1[i].CtrlIdName + "' data-controlType='" + msg1[i].FldTypeId + "' data-FieldName = '" + msg1[i].FldName + "'></textarea></div>";
                    } else if (msg1[i].PropName === "Note") {//txtarea for notes
                        html += "<div class = 'quickLabel'><label >" +
                            msg1[i].Caption +
                            requiredText +
                            "</label></div><div class = 'quickControl'><textarea class = 'biggerComment' maxlength='50' rows='2' id= '" +
                            idControl +
                            "' name = '" +
                            idControl +
                            "' data-ControlName='" + msg1[i].CtrlIdName + "' data-controlType='" + msg1[i].FldTypeId + "' data-FieldName = '" + msg1[i].FldName + "'></textarea></div>";
                    }
                    else if (msg1[i].PropName === "DistanceToSchool") {
                        html +=
                            "<div class = 'quickLabel'><label>" +
                            msg1[i].Caption +
                            requiredText +
                            "</label></div>" +
                            "<div class = 'quickControl'>" +
                            "<div class = 'col-quick-lead-20' ><input id = '" +
                            idControl +
                            "'  name = '" +
                            idControl +
                            "' data-ControlName = '" +
                            msg1[i].CtrlIdName +
                            "' data-controlType = '" +
                            msg1[i].FldTypeId +
                            "' data-FieldName = '" +
                            msg1[i].FldName +
                            "' />&nbsp;<span style = 'vertical-align:bottom'> miles </span></div> ";
                    }
                    else if (msg1[i].PropName === "ExpectedStart") {
                        // display both the ddl and date picker. hide the one as per the selection of program
                        html += "<div class = 'quickLabel'><label>" +
                            msg1[i].Caption +
                            requiredText +
                            "</label></div>" +
                            "<div class = 'quickControl'>" +
                            "<div class = 'col-quick-lead-20' ><input id = 'ddlExpectedStart' name = '" +
                            idControl +
                            "' data-ControlName = '" +
                            msg1[i].CtrlIdName +
                            "' data-controlType = '" +
                            msg1[i].FldTypeId +
                            "' data-FieldName = '" +
                            msg1[i].FldName +
                            "' /></div> ";
                        html += "<div class = 'quickControl'>" +
                            "<div class = 'col-quick-lead-20' ><input id = 'dpExpectedStart' name = '" +
                            idControl +
                            "' data-ControlName = '" +
                            msg1[i].CtrlIdName +
                            "' data-controlType = '" +
                            msg1[i].FldTypeId +
                            "' data-FieldName = '" +
                            msg1[i].FldName +
                            "' /></div> ";
                    } else {
                        html += "<div class = 'quickLabel'><label >" +
                            msg1[i].Caption +
                            requiredText +
                            "</label></div><div class = 'quickControl'><input id= '" +
                            idControl +
                            "' name = '" +
                            idControl +
                            "' data-ControlName='" +
                            msg1[i].CtrlIdName +
                            "' data-controlType='" +
                            msg1[i].FldTypeId +
                            "' data-FieldName = '" +
                            msg1[i].FldName +
                            "'/></div>";
                    }
                }




                html += "</div> <div class='clearfix bottomSpace'></div>";
                //source info
                if (msg1[i].SectionId === 5558) {
                    $("#dvSourceBar").attr("style", "display:block");
                    $("#dvSource").attr("style", "display:block");
                    $("#dvSource").append(html);
                }
                //other info
                else if (msg1[i].SectionId === 5562) {
                    $("#dvOther").append(html);
                }
                else { // ((msg1[i].SectionId === 5555) || (msg1[i].SectionId === 5556))
                    $('#dvGeneralInfo').append(html);
                }

            }
            //assigning Kendo controls 
            for (var k = 0; k < msg1.length; k++) {

                //textbox for SmallInt,Int,Float,Money,TinyInt,Char,Decimal,Varchar
                if ((msg1[k].FldTypeId === FieldType.SmallInt) ||
                    (msg1[k].FldTypeId === FieldType.Int) ||
                    (msg1[k].FldTypeId === FieldType.Float) ||
                    (msg1[k].FldTypeId === FieldType.Money) ||
                    (msg1[k].FldTypeId === FieldType.TinyInt) ||
                    (msg1[k].FldTypeId === FieldType.Char) ||
                    (msg1[k].FldTypeId === FieldType.Decimal) ||
                    (msg1[k].FldTypeId === FieldType.Varchar)) {
                    //text box
                    let textbox = "k-textbox bRadius";
                    //let idCtrl: string = msg1[k].PropName;
                    //var textbox = "k-textbox fieldlabel1";
                    if (msg1[k].PropName === "Comments" || msg1[k].PropName === "Note") {
                        //do nothing
                    }
                    else {
                        if (msg1[k].PropName === "Dependants" || msg1[k].PropName === "DistanceToSchool") {
                            textbox = "bRadius";
                        }
                        $("#" + msg1[k].PropName).attr("class", textbox);
                    }
                    if (msg1[k].PropName === "Dependants") {
                        $("#" + msg1[k].PropName).kendoNumericTextBox({
                            decimals: 0,
                            format: "n0",
                            step: 1,
                            min: 0,
                            max: 99
                        });
                        //$("#" + msg1[k].PropName).attr("class", "kNumeric");
                    }
                    if (msg1[k].PropName === "DistanceToSchool") {
                        $("#" + msg1[k].PropName).kendoNumericTextBox({
                            decimals: 0,
                            format: "n0",
                            step: 1,
                            min: 0,
                            max: 999
                        });
                        $("#" + msg1[k].PropName).attr("maxlength", "4");
                    }
                    if (msg1[k].PropName === "FirstName") {
                        $("#" + msg1[k].PropName).attr("maxlength", "28");
                    }
                    if (msg1[k].PropName === "NickName") {
                        $("#" + msg1[k].PropName).attr("maxlength", "12");
                    }
                    if (msg1[k].PropName === "MiddleName") {
                        $("#" + msg1[k].PropName).attr("maxlength", "28");
                    }
                    if (msg1[k].PropName === "LastName") {
                        $("#" + msg1[k].PropName).attr("maxlength", "28");
                    }
                    if (msg1[k].PropName === "DriverLicenseNumber") {
                        $("#" + msg1[k].PropName).attr("maxlength", "17");
                    }
                    if (msg1[k].PropName === "Extension") {
                        $("#" + msg1[k].PropName).attr("maxlength", "10");
                        $("#" + msg1[k].PropName).attr("placeholder", "Ext");
                    }
                    if (msg1[k].PropName === "Email") {
                        $("#" + msg1[k].PropName).attr("maxlength", "100");
                    }
                    if (msg1[k].PropName === "Address1") {
                        $("#" + msg1[k].PropName).attr("maxlength", "250");
                        $("#" + msg1[k].PropName).attr("placeholder", "Address Line 1");
                    }
                    if (msg1[k].PropName === "AddressApto") {
                        $("#" + msg1[k].PropName).attr("maxlength", "20");
                        $("#" + msg1[k].PropName).attr("placeholder", "Apartment No.");
                    }
                    if (msg1[k].PropName === "City") {
                        $("#" + msg1[k].PropName).attr("maxlength", "150");
                        $("#" + msg1[k].PropName).attr("placeholder", "City");
                    }
                    if (msg1[k].PropName === "Age") {
                        $("#" + msg1[k].PropName).attr("maxlength", "3");
                        $("#" + msg1[k].PropName).attr("data-validation", "number");
                    }
                    if (msg1[k].PropName === "SSN") {
                        $("#" + msg1[k].PropName)
                            .kendoMaskedTextBox({
                                mask: "000-00-0000",
                                clearPromptChar: false
                            });
                        $("#" + msg1[k].PropName).attr("data-validation", "ssn");
                        $("#" + msg1[k].PropName).attr("maxlength", "12");
                    }
                    if (msg1[k].PropName === "ZipCode") {
                        $("#" + msg1[k].PropName).attr("placeholder", "Zip");
                        $("#" + msg1[k].PropName)
                            .kendoMaskedTextBox({
                                mask: "00000",
                                clearPromptChar: false
                            });
                        $("#" + msg1[k].PropName).attr("data-validation", "zip");
                    }
                    //checking for phone for masking the phone.
                    if (msg1[k].PropName === LeadQuickPageVm.PHONE) {
                        $("#" + msg1[k].PropName).attr("placeholder", "Phone");
                        $("#" + msg1[k].PropName)
                            .kendoMaskedTextBox({
                                mask: "(000)-000-0000",
                                clearPromptChar: false
                            });
                        $("#" + msg1[k].PropName).attr("data-validation", "tel");
                    }
                }
                //datetime
                if (msg1[k].FldTypeId === FieldType.DateTime) {
                    if (msg1[k].PropName === "ExpectedStart") {
                        //if (XMASTER_LEAD_IS_CLOCK_HOUR === "YES") {
                        $("#dpExpectedStart").kendoDatePicker();
                        $("#dpExpectedStart").attr("data-validation", "date");
                        $("#" + msg1[k].PropName).attr("maxlength", "12");
                        //} else {
                        $("#ddlExpectedStart").kendoDropDownList({
                            dataSource: [{}],
                            optionLabel: "Select",
                            dataTextField: "Description",
                            dataBound: function () {
                                $("#ddlExpectedStart").data("kendoDropDownList").list.width("auto");
                                $("#ddlExpectedStart").data("kendoDropDownList").list.css("white-space", "nowrap");
                            },
                            dataValueField: "ID"
                        });
                        //}
                    } else if (msg1[k].PropName === LeadQuickPageVm.BEST_TIME) {
                        $("#" + LeadQuickPageVm.BEST_TIME).kendoTimePicker({
                            interval: 15,
                            min: new Date(2000, 0, 1, 6, 0, 0),
                            format: "hh:mm tt",
                            parseFormats: ["H:mm"]
                        });
                        $("#" + msg1[k].PropName).attr("data-validation", "time");
                    } else if (msg1[k].PropName === LeadQuickPageVm.SOURCE_DATE_TIME) {
                        $("#" + msg1[k].PropName).kendoDateTimePicker();
                        $("#" + msg1[k].PropName).attr("name", "sourceDateTime");
                        $("#" + msg1[k].PropName).attr("data-validation", "datetime");
                    }
                    else if (msg1[k].PropName === "Dob") {
                        $("#Dob").kendoDatePicker({
                            change: () => {
                                let dob = ($("#Dob").data("kendoDatePicker") as any).value();
                                let age = "";
                                if ($("#Age") != null) {
                                    age = $("#Age").val();
                                }
                                this.calculateAge(dob, age);
                            }
                        });
                        $("#" + msg1[k].PropName).attr("data-validation", "date");
                        $("#" + msg1[k].PropName).attr("maxlength", "12");
                    }
                    else {
                        $("#" + msg1[k].PropName).kendoDatePicker();
                        $("#" + msg1[k].PropName).attr("data-validation", "date");
                    }
                }
                //dropdown
                if (msg1[k].FldTypeId === FieldType.UniqueIdentifier) {
                    //let idCtrl: string = msg1[k].PropName;
                    //let controlObject = '#' + idCtrl;
                    if (msg1[k].PropName === "StateId") {
                        let ajaxCall = this.db.getQuickLeadStateValues(msg1[k].PropName, this)
                            .done(msg => {
                                if (msg != undefined) {
                                    $("#" + msg.Name)
                                        .kendoDropDownList({
                                            dataSource: msg.DataSource,
                                            dataTextField: "FullDescription",
                                            dataValueField: "ID",
                                            dataBound: function () {
                                                $("#" + msg.Name).data("kendoDropDownList").list.width("auto");
                                                $("#" + msg.Name).data("kendoDropDownList").list.css("white-space", "nowrap");
                                            },
                                            change: function () {
                                                let _ptr = $("#" + msg.Name).data("kendoDropDownList") as any;
                                                if (_ptr.text() !== "Select") {
                                                    if (_ptr.value() !== "") {
                                                        that.stateId = _ptr.value();
                                                    }
                                                    let displayText = _ptr.text().substr(0, 2);
                                                    _ptr.text(displayText);
                                                } else {
                                                    that.stateId = null;
                                                }
                                            }
                                        });
                                }
                            })
                            .fail(msg => {
                                $("body").css("cursor", "default");
                                MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                            });
                        dropdownlistArray.push(ajaxCall);
                    }  //check for attending HS and making it as yes/no dropdown
                    else if (msg1[k].PropName === "AttendingHs") {
                        $("#" + msg1[k].PropName)
                            .kendoDropDownList({
                                dataSource: [
                                    //{ Description: "Select", ID: "-1" },
                                    { Description: "No", ID: "0" },
                                    { Description: "Yes", ID: "1" }
                                ],
                                dataTextField: "Description",
                                dataValueField: "ID",
                                optionLabel: "Select"
                            });
                    }
                    else if (msg1[k].PropName === "DrvLicStateCode") {
                        let ajaxCall = this.db.getQuickLeadStateValues(msg1[k].PropName, this)
                            .done(msg => {
                                if (msg != undefined) {
                                    $("#" + msg.Name)
                                        .kendoDropDownList({
                                            dataSource: msg.DataSource,
                                            dataTextField: "FullDescription",
                                            dataValueField: "ID",
                                            dataBound: function () {
                                                $("#" + msg.Name).data("kendoDropDownList").list.width("auto");
                                                $("#" + msg.Name).data("kendoDropDownList").list.css("white-space", "nowrap");
                                            },
                                            change: function () {
                                                let _ptr = $("#" + msg.Name).data("kendoDropDownList") as any;
                                                if (_ptr.text() !== "Select") {
                                                    if (_ptr.value() !== "") {
                                                        that.driverLiscStateId = _ptr.value();
                                                    }
                                                    let displayText = _ptr.text().substr(0, 2);
                                                    _ptr.text(displayText);
                                                } else {
                                                    that.driverLiscStateId = null;
                                                }
                                            }
                                        });
                                }
                            })
                            .fail(msg => {
                                $("body").css("cursor", "default");
                                MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                            });
                        dropdownlistArray.push(ajaxCall);
                    }
                }
                //bit (checkbox)
                if (msg1[k].FldTypeId === FieldType.Bit) {
                    //let idCtrl: string = msg1[k].PropName;
                    $("#" + msg1[k].PropName).attr("type", "checkBox");
                    if (msg1[k].PropName === "IsForeignPhone") {
                        $("#" + msg1[k].PropName)
                            .change(function () {
                                // Get check- box checked value
                                let maskedtextbox = $("#Phone").data("kendoMaskedTextBox") as any;
                                let raw = maskedtextbox.raw();
                                if ($(this).is(":checked")) {
                                    maskedtextbox.setOptions({ mask: '999999999999999' });
                                } else {
                                    maskedtextbox.setOptions({ mask: '(000)-000-0000' });
                                }
                                maskedtextbox.value(raw);
                            });
                    }
                    if (msg1[k].PropName === "IsInternational") {
                        let name = "IsInternational";
                        $("#" + name)
                            .change(function () {
                                let maskedtextbox = $("#ZipCode").data("kendoMaskedTextBox") as any;
                                maskedtextbox.value("");
                                let ddlState = $("#StateId").data("kendoDropDownList") as any;
                                if ($(this).is(":checked")) {
                                    $("#" + name).prop("checked", true);
                                    maskedtextbox.setOptions({ mask: "aaaaaaaaaa" });
                                    $("#StateInternational").attr("style", "display: inline-block");
                                    if (ddlState != null) {
                                        ddlState.wrapper.hide();
                                    }

                                } else {
                                    $("#" + name).prop("checked", false);
                                    maskedtextbox.setOptions({ mask: "00000" });
                                    $("#StateInternational").attr("style", "display: none");
                                    if (ddlState != null) {
                                        ddlState.wrapper.show();
                                    }

                                }
                            });
                    }
                }
                if (msg1[k].Required === 1) {
                    $("#" + msg1[k].PropName).attr("required", "required");
                }
            }
            //#Region KendoDropdwn values to the controls
            var noParentIdddl = $(":not([data-ParentId])[data-controlType='72']");
            if (noParentIdddl != null) {
                noParentIdddl.each(function (e, element) {
                    if (element.id === "DrvLicStateCode" || element.id === "AttendingHs" || element.id === "StateId") {
                        //do nothing as they are already assigned
                    } else {
                        var res1 = that.db.getQuickLeadDropDownValues(element.id,
                                $(element).attr("data-FieldName"),
                            XMASTER_GET_CURRENT_CAMPUS_ID, XMASTER_PAGE_USER_OPTIONS_USERID,
                                that)
                            .done(msg => {
                                if (msg != undefined) {
                                    if (msg.Name === "LeadAssignedToId") {
                                        $("#" + element.id)
                                            .kendoDropDownList({
                                                dataSource: msg.DataSource,
                                                dataTextField: "Description",
                                                dataValueField: "ID",
                                                optionLabel: "Select",
                                                change: () => {
                                                    let cId = ($("#LeadAssignedToId").data("kendoDropDownList") as any).value();
                                                    if ($("#AreaId") != null) {
                                                        var cbArea = $("#AreaId").data("kendoDropDownList") as any;
                                                        if (cbArea != null) {
                                                            cbArea.setDataSource(new kendo.data.DataSource());
                                                        }
                                                    }

                                                    that.assignCascadeCampus(cId);
                                                    //todo write logic for lead group
                                                    that.processLeadGroupsByCampus(cId);
                                                },
                                                dataBound: function () {
                                                    $("#" + msg.Name).data("kendoDropDownList").list.width("auto");
                                                    $("#" + msg.Name).data("kendoDropDownList").list.css("white-space", "nowrap");
                                                    if (!isLead) {
                                                        ($("#" + msg.Name).data("kendoDropDownList") as any).value(XMASTER_GET_CURRENT_CAMPUS_ID);
                                                    }
                                                }
                                            });
                                    }
                                    else {
                                        $("#" + element.id)
                                            .kendoDropDownList({
                                                dataSource: msg.DataSource,
                                                dataTextField: "Description",
                                                dataValueField: "ID",
                                                optionLabel: "Select",
                                                dataBound: function () {
                                                    $("#" + msg.Name).data("kendoDropDownList").list.width("auto");
                                                    $("#" + msg.Name).data("kendoDropDownList").list.css("white-space", "nowrap");
                                                }
                                            });
                                    }

                                }
                            });
                        dropdownlistArray.push(res1);
                    }

                });
            }
            var parentIdDll = $("[data-ParentId][data-controlType='72']");
            if (parentIdDll != null) {
                parentIdDll.each((e, element) => {
                    $("#" + element.id)
                        .kendoDropDownList({
                            dataSource: [{}],
                            optionLabel: "Select",
                            dataTextField: "Description",
                            dataValueField: "ID"
                        });
                });
            }
            //#EndRegion KendoDropdwn values to the controls
            //hide the expected start datapicker for 1st time.//todo need to check how it is after save on this page.
            $("#dpExpectedStart").parent().parent().hide();
            this.assignCascadeCampus(XMASTER_GET_CURRENT_CAMPUS_ID);
            //this.initializePage(isLead, msg1);
        }

        processLeadGroupsByCampus(campusId: string) {
            var grpsD = this.db.getLeadGrpList(170, campusId, this)
                .done(msg => {
                    if (msg != undefined) {
                        this.gruppenDb = msg;
                        sessionStorage.setItem(XLEAD_INFO_GROUPS, JSON.stringify(msg));
                    }
                }).fail((msg => {
                    $("body").css("cursor", "default");
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                }));
            // Filter groups by CampusID
            var filteredGroup = new Array<Group>();
            for (var j = 0; j < this.gruppenDb.length; j++) {
                if (this.gruppenDb[j].CampusId === campusId) {
                    filteredGroup.push(this.gruppenDb[j]);
                }
            }

            //Create the check-box containers for the filtered groups.
            filteredGroup.sort(function (a, b) {
                var nA = a.Description.toLocaleLowerCase();
                var nB = b.Description.toLocaleLowerCase();
                if (nA < nB) {
                    return -1;
                } else if (nA > nB) {
                    return 1;
                }
                return 0;
            });
            var html: string;
            //Create the check-box containers for the filtered groups.
            $("#dvLeadGrp").html("");
            for (var i = 0; i < filteredGroup.length; i++) {
                html = `<input type="checkbox" class="k-checkbox" id="${filteredGroup[i].GroupId}" />`;
                html = html + '<label class="k-checkbox-label" for="' + filteredGroup[i].GroupId + '" > ' + filteredGroup[i].Description + ' </label>';
                html = `<div >${html}</div>`;
                $("#dvLeadGrp").append(html);
            }
            $(document).find("html").removeClass('rfdLabel');
        }

        calculateAge(dob: string, age: string) {
            if ($("#Age") != null) {
                if (dob != null && dob !== "") {
                    ($("#Dob").data("kendoDatePicker") as kendo.ui.DatePicker).value(new Date(dob));
                    // Calculate the Age from DOB
                    var calAge = ad.GET_AGE(dob);
                    $("#Age").val(calAge.toString());
                } else {
                    if (age !== "") {
                        $("#Age").val(age);
                    }
                }
            }
        }
        //this method initailizes all the dropdown values with the datasource of slected campus        
        assignCascadeCampus(campusId: string) {
            var parentIdDll = $("[data-ParentId][data-controlType='72']");
            if (parentIdDll != null) {
                parentIdDll.each((e, element) => {
                    var that = this;
                    //get all the data source wrt to campus selected
                    //Region get the source for the selected Campus
                    var res1 = this.db
                        .getQuickLeadDropDownValues(element.id,
                            $(element).attr("data-FieldName"),
                        campusId, XMASTER_PAGE_USER_OPTIONS_USERID,
                            this)
                        .done(msg => {
                            if (msg != undefined) {
                                var filteredDll: any = [];
                                for (var b = 0; b < msg.DataSource.length; b++) {
                                    if (msg.DataSource[b].CampusesIdList != undefined &&
                                        msg.DataSource[b].CampusesIdList.length > 0) {
                                        var listAdminR = msg.DataSource[b].CampusesIdList;
                                        for (let c = 0; c < listAdminR.length; c++) {
                                            if (listAdminR[c].CampusId === campusId) {
                                                filteredDll.push(msg.DataSource[b]);
                                                break;
                                            }
                                        }
                                    }
                                }
                                //EndRegion get the source for the selected Campus
                                //Lead status dropdown
                                if (element.id === "LeadStatusId") {
                                    let campusdll = $("#LeadAssignedToId").data("kendoDropDownList") as any;
                                    let campusId: string;
                                    if (campusdll === null || campusdll === undefined) {
                                        campusId = XMASTER_GET_CURRENT_CAMPUS_ID;
                                    } else {
                                        campusId = campusdll.value();
                                        if (campusId === "") {
                                            campusId = XMASTER_GET_CURRENT_CAMPUS_ID;
                                        }
                                    }

                                    if (campusId.length === 0) {
                                        let cbsLeadStatus = $("#LeadStatusId").data("kendoDropDownList") as any;
                                        cbsLeadStatus.setDataSource(new kendo.data.DataSource());
                                    } else {
                                        //to remove the ** for New Lead
                                        this.filteredLeadStatusDll = filteredDll;
                                        for (var a = 0; a < this.filteredLeadStatusDll.length; a++) {
                                            if (this.filteredLeadStatusDll[a].Description.indexOf("**") === 0) {
                                                that.leadStatusId = filteredDll[a].ID;
                                                this.filteredLeadStatusDll[a].Description = this.filteredLeadStatusDll[a].Description.replace("**", "");
                                                break;
                                            }
                                        }
                                        this.leadDb
                                            .getAllowedStatus(XMASTER_GET_CURRENT_CAMPUS_ID,
                                                that.leadStatusId,
                                                XMASTER_PAGE_USER_OPTIONS_USERID,
                                                this)
                                            .done(msgSel => {
                                                if (msgSel != undefined) {
                                                    sessionStorage
                                                        .setItem(QUICK_ALLOWEDLEADSTATUS, msgSel.StateChangeIdsList);
                                                    var cad = msgSel.StateChangeIdsList;
                                                    var allowedStates: string[];
                                                    if (cad !== undefined && cad !== null && cad.length > 10) {

                                                        allowedStates = cad.split(",");
                                                    } else {
                                                        allowedStates = [];
                                                    }
                                                    this.finalLeadStatusdll = this
                                                        .selectLeadAllowedStates(this.filteredLeadStatusDll,
                                                            allowedStates,
                                                            that.leadStatusId);
                                                    this.finalLeadStatusdll.sort({ field: "Description", dir: "asc" });
                                                    //Region Kendo drop down
                                                    $("#" + msg.Name)
                                                        .kendoDropDownList({
                                                            dataSource: this.finalLeadStatusdll,
                                                            dataTextField: "Description",
                                                            dataValueField: "ID",
                                                            optionLabel: "Select",
                                                            dataBound: function () {
                                                                $("#" + msg.Name)
                                                                    .data("kendoDropDownList")
                                                                    .list.width("auto");
                                                                $("#" + msg.Name)
                                                                    .data("kendoDropDownList")
                                                                    .list.css("white-space", "nowrap");
                                                                ($("#" + msg.Name).data("kendoDropDownList") as any)
                                                                    .value(that.leadStatusId);
                                                            }
                                                        });
                                                    //#endRegion Kendo drop down
                                                }
                                            });
                                    }
                                }
                                else if (element.id === "AreaId") {
                                    //Region Kendo drop down
                                    filteredDll.sort(function (a, b) {
                                        var nA = a.Description.toLocaleLowerCase();
                                        var nB = b.Description.toLocaleLowerCase();
                                        if (nA < nB) {
                                            return -1;
                                        } else if (nA > nB) {
                                            return 1;
                                        }
                                        return 0;
                                    });
                                    $("#" + msg.Name)
                                        .kendoDropDownList({
                                            dataSource: filteredDll,
                                            dataTextField: "Description",
                                            dataValueField: "ID",
                                            optionLabel: "Select",
                                            dataBound: function () {
                                                $("#" + msg.Name).data("kendoDropDownList").list.width("auto");
                                                $("#" + msg.Name).data("kendoDropDownList").list.css("white-space", "nowrap");
                                            },
                                            change: () => {
                                                // Clean Program, Program Version and Expected Start and schedule
                                                var campusId = ($("#LeadAssignedToId").data("kendoDropDownList") as any).value();
                                                var cbprogramVersion = $("#PrgVerId").data("kendoDropDownList") as any;
                                                var cbschedule = $("#ScheduleId").data("kendoDropDownList") as any;
                                                //if (!(XMASTER_LEAD_IS_CLOCK_HOUR === "Yes")) {
                                                // by default let it be drop down
                                                var cbexpectedStart = $("#ddlExpectedStart").data("kendoDropDownList") as any;
                                                if (cbexpectedStart != null) {
                                                    cbexpectedStart.setDataSource(new kendo.data.DataSource());
                                                }
                                                //}


                                                cbschedule.setDataSource(new kendo.data.DataSource());
                                                cbprogramVersion.setDataSource(new kendo.data.DataSource());


                                                //Reload Program
                                                var selectedValue = ($("#AreaId").data("kendoDropDownList") as any).value();
                                                if (selectedValue !== "") {
                                                    this.getItemInformation(campusId, "Program", "ProgramId", selectedValue);
                                                } else {
                                                    var cbprogramLabel = $("#ProgramId").data("kendoDropDownList") as any;
                                                    cbprogramLabel.setDataSource(new kendo.data.DataSource());
                                                }

                                            }
                                        });
                                    //#endRegion Kendo drop down
                                }
                                else if (element.id === "ProgramId") {
                                    $("#ProgramId").kendoDropDownList({
                                        optionLabel: "Select",
                                        dataTextField: "Description",
                                        dataValueField: "ID",
                                        dataBound: function () {
                                            $("#" + element.id).data("kendoDropDownList").list.width("auto");
                                            $("#" + element.id).data("kendoDropDownList").list.css("white-space", "nowrap");
                                        },
                                        change: () => {
                                            // Clean Program Version and Expected Start and schedule
                                            var campusId = ($("#LeadAssignedToId").data("kendoDropDownList") as any).value();
                                            var cbprogramVersion = $("#PrgVerId").data("kendoDropDownList") as any;
                                            var cbschedule = $("#ScheduleId").data("kendoDropDownList") as any;
                                            //if (!(XMASTER_LEAD_IS_CLOCK_HOUR === "Yes")) {
                                            //var cbexpectedStart = $("#ExpectedStart").data("kendoDropDownList") as any;
                                            //cbexpectedStart.setDataSource(new kendo.data.DataSource());
                                            //}


                                            cbschedule.setDataSource(new kendo.data.DataSource());
                                            cbprogramVersion.setDataSource(new kendo.data.DataSource());


                                            //Reload Program Version and Expected Start
                                            var progDll = $("#ProgramId").data("kendoDropDownList") as any;
                                            var dataTable = progDll.dataSource.data();
                                            var calendartype = dataTable[progDll.selectedIndex - 1]["CalendarType"];
                                            var selectedValue = ($("#ProgramId").data("kendoDropDownList") as any).value();
                                            var cbexpectedStart = $("#ddlExpectedStart").data("kendoDropDownList") as any;
                                            if (selectedValue !== "") {
                                                this
                                                    .getItemInformation(campusId,
                                                        "PrgVerId",
                                                        "PrgVerId",
                                                        selectedValue);

                                                //Reload Expected Start
                                                //if (!(XMASTER_LEAD_IS_CLOCK_HOUR === "YES")) {
                                                if (calendartype.toLowerCase() === "clock hour") {
                                                    $("#dpExpectedStart").css("display", "inline");
                                                    $("#dpExpectedStart").css("width", "auto");
                                                    $("#dpExpectedStart").kendoDatePicker();
                                                    cbexpectedStart.wrapper.hide();
                                                } else {
                                                    cbexpectedStart.wrapper.show();
                                                    this.getExpectedLeadItemInfo(campusId, "ExpectedStart", "dllExpectedStart", selectedValue);
                                                    ($("#dpExpectedStart").data("kendoDatePicker") as kendo.ui.DatePicker).value("");
                                                    $("#dpExpectedStart").parent().parent().hide();
                                                }

                                                // }

                                            } else {
                                                cbexpectedStart.wrapper.show();
                                                //this.getExpectedLeadItemInfo(campusId, "ExpectedStart", "ExpectedStart", selectedValue);
                                                $("#dpExpectedStart").parent().parent().hide();
                                            }
                                        }
                                    });
                                }
                                else if (element.id === "PrgVerId") {
                                    $("#PrgVerId").kendoDropDownList({

                                        optionLabel: "Select",
                                        dataTextField: "Description",
                                        dataValueField: "ID",
                                        //dataSource: [],
                                        dataBound: function () {
                                            $("#" + element.id).data("kendoDropDownList").list.width("auto");
                                            $("#" + element.id).data("kendoDropDownList").list.css("white-space", "nowrap");
                                        },
                                        change: () => {
                                            var campusId = ($("#LeadAssignedToId").data("kendoDropDownList") as any).value();
                                            //Reload program version
                                            var cbprogramVersion = $("#PrgVerId").data("kendoDropDownList") as any;
                                            var selectedValue = cbprogramVersion.value();
                                            if (selectedValue !== "") {
                                                this.getItemInformation(campusId, "ProgramScheduleId", "ScheduleId", selectedValue);
                                            }
                                        }
                                    });
                                }
                                else if (element.id === "ScheduleId") {
                                    $("#ScheduleId").kendoDropDownList({
                                        optionLabel: "Select",
                                        dataTextField: "Description",
                                        dataValueField: "ID",
                                        dataBound: function () {
                                            $("#" + element.id).data("kendoDropDownList").list.width("auto");
                                            $("#" + element.id).data("kendoDropDownList").list.css("white-space", "nowrap");
                                        }
                                        //dataSource: []
                                    });
                                }
                                else {
                                    //Region Kendo drop down
                                    $("#" + msg.Name)
                                        .kendoDropDownList({
                                            dataSource: filteredDll,
                                            dataTextField: "Description",
                                            dataValueField: "ID",
                                            optionLabel: "Select",
                                            dataBound: function () {
                                                $("#" + msg.Name).data("kendoDropDownList").list.width("auto");
                                                $("#" + msg.Name)
                                                    .data("kendoDropDownList")
                                                    .list.css("white-space", "nowrap");
                                            }
                                        });
                                    //#endRegion Kendo drop down
                                }
                            }
                        }).fail(msg => {
                            $("body").css("cursor", "default");
                            MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                        });
                    dropdownlistArray.push(res1);
                });
            }
        }
        /* Manage changes of a specific drop down it is used to manage the user change in cascade drop down */
        getItemInformation(campusId: string, fldName: string, targetDropDown: string, value: Object, setting: string = null) {
            // Request the info from server
            let ajaxcall = this.leadDb.getLeadItemInformation(fldName, value, campusId, this)
                .done(msg => {
                    if (msg != undefined) {
                        // Enter the information in the controls
                        var itemdd = $("#" + targetDropDown).data("kendoDropDownList");

                        // Set the data source
                        itemdd.setDataSource(msg);

                        // Optional set the value of the filled drop down
                        if (setting != null) {
                            itemdd.value(setting);
                        }
                    }
                })
                .fail(msg => {
                    $("body").css("cursor", "default");
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
            dropdownlistArray.push(ajaxcall);
        }
        //processExpectedStart(): Date {
        //    var expectedStart: Date;
        //    if (XMASTER_LEAD_IS_CLOCK_HOUR === 'Yes') {
        //        expectedStart = $("#ExpectedStart").data("kendoDatePicker").value();
        //    } else {
        //        // ExpectedStart with Term...
        //        var description = $("#ExpectedStart").data("kendoDropDownList").text();
        //        if (description == null || description === "" || description === X_SELECT_ITEM) {
        //            expectedStart = null;
        //        } else {
        //            var descriptdate: string = description.split("]")[0].slice(1).toString().trim();
        //            expectedStart = new Date(descriptdate);
        //        }
        //    }
        //    return expectedStart;
        //}

        getQuickLeadDatasource(): any {
            var temp = sessionStorage.getItem(XLEAD_QUICK_FIELDS);
            if (temp !== undefined || !(temp === "{}")) {
                var filterDataDb = JSON.parse(temp);
                if (filterDataDb !== undefined) {
                    var result = JSON.parse(filterDataDb.responseText);
                    return result;
                }
            }
            return undefined;
        }
    }


}