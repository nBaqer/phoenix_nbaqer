﻿module AD {

    export class LeadQuickPageDb {

        public getResourcesForQuickPage(mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_FIELDS_GET + "?Command=1" ,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }
       
        public getQuickLeadDropDownValues(ctrlName: string, fldName: string, campusId: string, userId: string, mcontext: any) {
            var model = {
                CtrlName: ctrlName,
                Input: {
                    FldName: fldName,
                    CampusId: campusId,
                    UserId: userId 
                },
            };
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_DDL_SOURCE_GET + "?filter=" + JSON.stringify(model),
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }

        public getSdfList(pgResId: number, campusId: string, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_SDF_GET + "?PageResourceId=" + pgResId + "&CampusId=" + campusId,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }

        public getLeadGrpList(pgResId: number, campusId: string, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_LEADGRP_GET + "?PageResourceId=" + pgResId + "&CampusId=" + campusId,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }

        public getQuickLeadStateValues(ctrlName: string, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_STATE_DDL_GET + "?CtrlName=" + ctrlName,
                type: "GET",
                timeout: 20000,
                dataType:"json"
            });
        }
    }
}