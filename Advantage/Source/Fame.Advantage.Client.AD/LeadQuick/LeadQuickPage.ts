﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../fame.advantage.client.masterpage/advantagevalidation.ts" />
/// <reference path="../../fame.advantage.client.masterpage/advantagemessageboxs.ts" />
/// <reference path="../../fame.advantage.client.masterpage/commonfunctions.ts" />

module AD {
    // This is created in Lead Master Page.
    declare var XMASTER_LEAD_IS_CLOCK_HOUR: string;
    declare var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL: string;
    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;
    export declare var isNewLead: boolean;
    export declare var ajaxChecker: number;
    export declare var leadQuickPageViewModel: LeadQuickPageVm;
    export declare var leadQuickPage: LeadQuickPage;
    export class LeadQuickPage {
    public quickLeadValidatior:kendo.ui.Validator;


        constructor() {
            try {
                AD.leadQuickPageViewModel = new LeadQuickPageVm();
                //var ethis = this;
                $("#QuickToolBar")
                    .kendoToolBar({
                        resizable: true,
                        items: [
                            //regular button
                            { id: "quickLeadToolBarNew", type: "button", icon: "file-add ", text: "New", showIcon: "toolbar", overflow: "never" },
                            { id: "quickLeadToolBarSave", type: "button", text: "Save", icon: "save font-blue-darken-3 ", showIcon: "toolbar", overflow: "never" },
                            { id: "quickLeadToolBarSaveRedirect", type: "button", text: "Save and redirect to Lead page", icon: "save font-blue-darken-4 ", showIcon: "toolbar", overflow: "never" },
                            { id: "quickLeadToolBarCancel", type: "button", text: "Cancel", icon: "minus-outline font-red", showIcon: "toolbar", overflow: "never" }
                        ]
                    });
                AD.isNewLead = false;
                var leadId: string;
                AD.leadQuickPage = this;
                // Always take is new if the click comes from the Menu
                var isnew: string = MasterPage.GET_QUERY_STRING_PARAMETER_BY_NAME("new");
                if (isnew === "0") {
                    // clean is new. that is because comes from the main menu. should always be new
                    sessionStorage.setItem(XLEAD_QUICK_NEW, "true");//now made to true todo
                }
                // Create the page validator
                this.quickLeadValidatior = MasterPage.ADVANTAGE_VALIDATOR("containerQuickInfoPage");
                // Get if it is a new page or use a specific lead
                var newLead = sessionStorage.getItem(XLEAD_QUICK_NEW);
                if (newLead != null && newLead === "true") {
                    // If it is present the TAG XLEAD_QUICK_NEW in the query string
                    // We have a request of new lead setgocontactInformation flag isLead to false
                    AD.isNewLead = false;
                } else {
                    // If it is a normal page of lead, Analise the special case
                    // we have a campus without leads
                    let leadSelected: any = document.getElementById('leadId');
                    leadId = leadSelected.value;
                    AD.isNewLead = true;
                    if ((leadId == undefined || leadId == null || leadId === "")) {
                        // We are in a Campus without Leads.... Open as NEW
                        sessionStorage.setItem(XLEAD_QUICK_NEW, "true");
                        window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadQuick.aspx?resid=268&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Quick&mod=AD");
                        return;
                    }
                }
                $(document).keypress(function (event) {

                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if (keycode === 13) {
                        return false;
                    }
                    return true;
                });

                // Duplicate windows
                $("#WindowsDuplicate").kendoWindow(
                    {
                        actions: ["Close"],
                        title: "Duplicates Window",
                        width: "900px",
                        height: "650px",
                        visible: false,
                        modal: false,
                        resizable: true
                    });

                // Duplicate Windows : Grid for duplicates
                $("#infoPageDupGrid").kendoGrid({
                    toolbar: ["excel", "pdf"],
                    excel: {
                        allPages: true
                    },
                    pdf: {
                        fileName: "AdvantageLeadsManagement.pdf",
                        landscape: true
                    },
                    dataSource: [{}],
                    //height: 600,
                    sortable: true,
                    filterable: false,
                    resizable: true,

                    columns: [

                        { field: "Id", title: "", width: 1 },
                        { field: "FirstName", title: "First Name", attributes: { style: "font-size:11px;" } },
                        { field: "LastName", title: "Last Name", attributes: { style: "font-size:11px;" } },
                        { field: "ssn", title: "SSN", attributes: { style: "font-size:11px;" } },
                        { field: "Dob", title: "DOB", attributes: { style: "font-size:11px;" } },
                        { field: "Address1", title: "Address", attributes: { style: "font-size:11px;" } },
                        { field: "Phone", title: "Phones" },
                        { field: "Email", title: "Email" },
                        { field: "Campus", title: "Campus", attributes: { style: "font-size:11px;" } },
                        { field: "Status", title: "Status", attributes: { style: "font-size:11px;" } },
                        //{ field: "LeadAssignedTo", title: "Lead Assigned To", editor: function (c, o) { fthis.editordd(c, o); }, template: "#=LeadAssignedTo#" },
                        { field: "AdmissionRep", title: "Admission Rep", attributes: { style: "font-size:11px;" } }
                    ]

                });

                // Buttons events for Duplicate windows
                $('#cancelLead').click(() => {
                    var dupWin = $("#WindowsDuplicate").data("kendoWindow") as any;
                    dupWin.close();
                });
                $('#insertLead').click(() => {
                    var dupWin = $("#WindowsDuplicate").data("kendoWindow") as any;
                    dupWin.close();
                    //var customValidation: boolean = this.viewModel.executeCustomValidators();
                    //if (customValidation === false) {
                    //    return;
                    //}
                    //let validator = $("#containerQuickInfoPage").kendoValidator().data("kendoValidator") as any;
                    if (this.quickLeadValidatior.validate()) {
                    // If the form is valid, the Valida-tor will return true
                    AD.leadQuickPageViewModel.postLeadInformationToServer(leadId, true, "insertLead");
                    } else {
                        MasterPage.SHOW_WARNING_WINDOW(X_SAVE_REVIEW_INFO_IN_FORM);
                    }
                });
                AD.leadQuickPageViewModel.getQuickLeadFields(AD.isNewLead, this);
                //handle the toolbar control
                $("#quickLeadToolBarNew")
                    .click(() => {
                        $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(X_LEAD_SAVE_CHANGES))
                            .then(confirmed => {
                                if (confirmed) {
                                    //var save: boolean = confirm("Save Changes?");
                                    //if (save) {
                                    // Click save button
                                    //$("#quickLeadToolBarSave").click();
                                    //let validator = $("#containerQuickInfoPage").kendoValidator().data("kendoValidator") as any;
                                    if (this.quickLeadValidatior.validate()) {
                                        AD.leadQuickPageViewModel.postLeadInformationToServer(leadId, false, "new");
                                        sessionStorage.setItem(XLEAD_QUICK_NEW, "true");
                                        //return;
                                    } else {
                                        MasterPage.SHOW_WARNING_WINDOW(X_SAVE_PLEASE_FILL_REQUIRED_FIELDS);
                                        //return;
                                    }
                                    //window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadQuick.aspx?resid=268&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Quick&mod=AD");
                                } else { //redirect without saving any changes
                                    sessionStorage.setItem(XLEAD_QUICK_NEW, "true");
                                    //window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadQuick.aspx?resid=268&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Quick&mod=AD");
                                    window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                                        "AD/AleadQuick.aspx?resid=268&cmpid=" +
                                        XMASTER_GET_CURRENT_CAMPUS_ID +
                                        "&desc=Quick&mod=AD");
                                }
                            });
                    });
                $("#quickLeadToolBarSave")
                    .click(() => {
                        //let validator = $("#containerQuickInfoPage").kendoValidator().data("kendoValidator") as any;
                        if (this.quickLeadValidatior.validate()) {
                            leadQuickPageViewModel.postLeadInformationToServer(leadId, false, "save");
                        }
                        else {
                            MasterPage.SHOW_WARNING_WINDOW(X_SAVE_PLEASE_FILL_REQUIRED_FIELDS);
                        }
                    });

                $("#quickLeadToolBarSaveRedirect")
                    .click(() => {
                        //$.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE("Save Changes?"))
                        //    .then(confirmed => {
                        //        if (confirmed) {
                                    //var save: boolean = confirm("Save Changes?");
                                    //if (save) {
                                    // Click save button
                                    //$("#quickLeadToolBarSave").click();
                                    //let validator = $("#containerQuickInfoPage").kendoValidator().data("kendoValidator") as any;
                        if (this.quickLeadValidatior.validate()) {
                                        AD.leadQuickPageViewModel
                                            .postLeadInformationToServer(leadId, false, "saveRedirect");
                                        sessionStorage.setItem(XLEAD_NEW, "false");
                                    } else {
                                        MasterPage.SHOW_WARNING_WINDOW(X_SAVE_PLEASE_FILL_REQUIRED_FIELDS);
                                    }
                                    //window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?resid=170&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&mod=AD");
                                //}
                                //else {
                                //    //redirect without saving any changes
                                //    sessionStorage.setItem(XLEAD_NEW, "false");
                                //    window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?resid=170&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&mod=AD");
                                //}
                            //});
                    });

                $("#quickLeadToolBarCancel")
                    .click(() => {
                        AD.leadQuickPageViewModel.cancelLead();
                    });

                // Resize the windows to maintain a correct tab / scroll relation
                var resize = this.controlRezising;
                $(window).on('resize', function () {
                    resize(newLead);
                });

                // Initial tab / scroll relation
                this.controlRezising(newLead);
               
            } catch (e) {
                $("body").css("cursor", "default");
                MasterPage.SHOW_ERROR_WINDOW(e.toString());
            }
    }

        controlRezising(isnew: string) {
            let pageheight: number;
            let pageTop: number;

            if (isnew === "true") {
                //var win = $(this); //this = window
                pageheight = window.innerHeight - 140;
                pageTop = 140;
            } else {
                pageheight = window.innerHeight - 200;
                pageTop = 200;
            }
           
            if (pageheight < 0) {
                pageheight = 15;
            }

            // Assign to wrapper the height
            $("#wrapperQuick").css("max-height", pageheight);
            $("#wrapperQuick").css("top", pageTop);
        }

        validateAjaxRequestFinished() {
            let isfinish = true;
            if (AD.dropdownlistArray !== undefined) {
                if (AD.dropdownlistArray.length > 0) {
                    for (let ajaxValidatorCounter = 0; ajaxValidatorCounter <= AD.dropdownlistArray.length - 1; ajaxValidatorCounter++) {
                        if (AD.dropdownlistArray[ajaxValidatorCounter].readyState !== 4) {
                            isfinish = false;
                        }
                    }
                }
            }
            if (isfinish) {
                let quickLeadDatasource = AD.leadQuickPageViewModel.getQuickLeadDatasource();
                if (quickLeadDatasource !== undefined) {
                    clearInterval(ajaxChecker);
                    AD.leadQuickPageViewModel.initializePage(AD.isNewLead, quickLeadDatasource);
                } else {
                    MasterPage.SHOW_ERROR_WINDOW("data source is null");
                }
            }
        }
    }
} 