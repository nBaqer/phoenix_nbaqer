﻿module AD {

    export interface IDdlSource {
        id: string;
        description: string;
    }

    /*
     * The lead quick fields output model.
     */
    export interface ILeadQuickFields {
        /*
         * Gets or sets the table field id.
         */
        tblFldsId: number;

        /*
         * Gets or sets the field id.
         */
        fldId: number;

        /*
         * Gets or sets the section id.
         */
        sectionId: number;

        /*
         * Gets or sets the value if the field is a dropdown
         */
        ddlId: number;

        /*
         * Gets or sets the field name
         */
        fldName: string;

        /*
         * Gets or sets if the field is required or not
         */
        required: number;

        /*
         * Gets or sets the length of the field
         */
        fldLen: number;

        /*
         * Gets or sets the data type of the field
         */
        fldType: string;

        /*
         * Gets or sets the name given to the label in UI
         */
        caption: string;

        /*
         * Gets or sets the value if it has dropdown mostly it is null
         */
        controlName: string;

        /*
         * Gets or sets the data type of the field
         */
        fldTypeId: number;

        ddlSource: IDdlSource[];
    }
}