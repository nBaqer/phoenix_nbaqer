var ad;
(function (ad) {
    function GET_QUERY_STRING_PARAMETER_BY_NAME(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    ad.GET_QUERY_STRING_PARAMETER_BY_NAME = GET_QUERY_STRING_PARAMETER_BY_NAME;
    function GET_AGE(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
    ad.GET_AGE = GET_AGE;
    function OPEN_TASK_RAD_EMAIL_WINDOW_WITH_ENTITY(entityId, mod) {
        var oWnd = $find('radPop');
        oWnd.setSize(800, 780);
        oWnd.setUrl(XMASTER_GET_BASE_URL + '/defaultTMAddNew.html?addnew=true&entityId=' + entityId + "&mod=" + mod.toString());
        oWnd.show();
    }
    ad.OPEN_TASK_RAD_EMAIL_WINDOW_WITH_ENTITY = OPEN_TASK_RAD_EMAIL_WINDOW_WITH_ENTITY;
    function OPEN_TASK_RAD_TASK_WINDOW_WITH_ENTITY(entityId, mod) {
        var oWnd = $find('radPop');
        oWnd.setSize(800, 780);
        oWnd.setUrl(XMASTER_GET_BASE_URL + '/defaultTM.html?addnew=true&entityId=' + entityId + "&mod=" + mod.toString());
        oWnd.show();
    }
    ad.OPEN_TASK_RAD_TASK_WINDOW_WITH_ENTITY = OPEN_TASK_RAD_TASK_WINDOW_WITH_ENTITY;
    function OPEN_TASK_RAD_SCHEDULE_WINDOW_WITH_ENTITY(entityId, mod) {
        var oWnd = $find('radPop');
        oWnd.setSize(800, 780);
        oWnd.setUrl(XMASTER_GET_BASE_URL + '/defaultTMSchedule.html?addnew=true&entityId=' + entityId + "&mod=" + mod.toString());
        oWnd.show();
    }
    ad.OPEN_TASK_RAD_SCHEDULE_WINDOW_WITH_ENTITY = OPEN_TASK_RAD_SCHEDULE_WINDOW_WITH_ENTITY;
    function SHOW_DATA_SOURCE_ERROR(e) {
        try {
            if (e.xhr != undefined) {
                if (showSessionFinished(e.xhr.statusText)) {
                    return "";
                }
                var display = "";
                if (e.xhr.statusText !== "OK" && e.xhr.status !== 406) {
                    display = "Error: " + e.xhr.statusText + ", ";
                }
                display += (e.xhr.responseText == undefined) ? e.xhr.responseXML : e.xhr.responseText;
                alert(display);
            }
            else {
                var display = "";
                if (e.statusText !== "OK" && e.status !== 406) {
                    display = "Error: " + e.statusText + ", ";
                }
                display += (e.responseText == undefined) ? e.responseXML : e.responseText;
                if ((e.readyState !== 0 && e.responseText !== "" && e.status !== 0 && e.statusText !== "error")) {
                    alert(display);
                }
            }
        }
        catch (ex) {
            alert("Server returned an undefined error");
        }
    }
    ad.SHOW_DATA_SOURCE_ERROR = SHOW_DATA_SOURCE_ERROR;
    function showSessionFinished(statusText) {
        if (statusText === "OK") {
            alert("Session expired");
            return true;
        }
        return false;
    }
    function FORMAT_DATE_TO_MM_DD_YYYY(d) {
        var currDate = d.getDate().toString();
        if (currDate.length < 2) {
            currDate = "0" + currDate;
        }
        var currMonth = (d.getMonth() + 1).toString();
        if (currMonth.length < 2) {
            currMonth = "0" + currMonth;
        }
        var currYear = d.getFullYear();
        return (currMonth + "/" + currDate + "/" + currYear);
    }
    ad.FORMAT_DATE_TO_MM_DD_YYYY = FORMAT_DATE_TO_MM_DD_YYYY;
    function FORMAT_AMPM(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        var strTime = hours + ":" + minutes + " " + ampm;
        return strTime;
    }
    ad.FORMAT_AMPM = FORMAT_AMPM;
})(ad || (ad = {}));
$.fn.bindUp = function (type, fn) {
    type = type.split(/\s+/);
    this.each(function () {
        var len = type.length;
        while (len--) {
            $(this).bind(type[len], fn);
            var evt = $.data(this, 'events')[type[len]];
            evt.splice(0, 0, evt.pop());
        }
    });
};
//# sourceMappingURL=CommonFunctions.js.map