var AD;
(function (AD) {
    var LeadInfoBar = (function () {
        function LeadInfoBar() {
            try {
                this.viewModel = new AD.LeadInfoBarVm();
                var ethis = this;
                var leadGuid = document.getElementById('leadId');
                var isnew = ad.GET_QUERY_STRING_PARAMETER_BY_NAME("new");
                if (isnew === "0") {
                    sessionStorage.setItem(AD.XLEAD_NEW, "false");
                }
                var temp = sessionStorage.getItem("NewLead");
                if (temp != null && temp === "true") {
                    $("#kendoleadBarControl").hide();
                }
                else {
                    if (leadGuid.value !== "") {
                        $("#kendoleadBarControl").show();
                        this.viewModel.getLeadInfoFromServer(leadGuid.value);
                    }
                }
                document.getElementById("infobarToLeadInfoPage").addEventListener("click", this.viewModel.goToLeadInfoScreen);
                kendo.init($("#kendoleadBarControl"));
                kendo.bind($("#kendoleadBarControl"), ethis.viewModel);
            }
            catch (e) {
                alert(e.message + "/n" + e.stack);
            }
        }
        return LeadInfoBar;
    }());
    AD.LeadInfoBar = LeadInfoBar;
})(AD || (AD = {}));
//# sourceMappingURL=InfoLeadBarKendo.js.map