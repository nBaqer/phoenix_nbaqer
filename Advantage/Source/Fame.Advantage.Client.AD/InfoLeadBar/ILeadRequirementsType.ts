﻿module AD {
    export interface ILeadRequirementsType {
        Name: string;
        IsMet: boolean;
        IsDefined: boolean;
    }
}