﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../fame.advantage.client.masterpage/customcontrol/webcamcontrol.ts" />


module AD {

    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;
    declare var AdvantageWebCamControl: MasterPage.WebCamControl;
    declare var Webcam: any;
    export class LeadInfoBar {

        public viewModel: LeadInfoBarVm;
        //public debug: boolean = true;
        thecamera: MasterPage.WebCamControl;

        constructor() {
            try {
                this.viewModel = new LeadInfoBarVm();
                var ethis = this;
                let kendoBarvisible = true;
                var leadGuid: any = document.getElementById('leadId');
                // Prevent is new if the click comes from the Menu
                var isnew: string = MasterPage.GET_QUERY_STRING_PARAMETER_BY_NAME("new");
                if (isnew === "0") {
                    // clean is new. that is because comes from the main menu. never should be new
                    sessionStorage.setItem(AD.XLEAD_NEW, "false");
                }

                // Detect if the page is a NEW lead.
                var temp = sessionStorage.getItem("NewLead");
                var temp2 = sessionStorage.getItem("NewQuickLead");
                var resId = sessionStorage.getItem("resid");
                if (resId != null && resId === "268") {
                    if (temp2 === "true") {
                        kendoBarvisible = false;
                        this.hideShowcontrols(true);
                        //$("#kendoleadBarControl").hide();
                    } else {
                        // This can happen when the campus has not leads
                        if (leadGuid.value !== "") {
                            // Show a lead.. then show in the bar the lead info.
                            kendoBarvisible = true;
                            this.hideShowcontrols(false);
                            //$("#kendoleadBarControl").show();
                            this.viewModel.getLeadInfoFromServer(leadGuid.value);
                            this.viewModel.getLeadImageFromServer(leadGuid.value);
                            //this.viewModel.getLeadRequirementsTypeMet(leadGuid.value, XMASTER_GET_CURRENT_CAMPUS_ID);
                       }
                    }
                } else if (temp != null && temp === "true") {
                    // New Lead. Hide the lead info bar.
                    kendoBarvisible = false;
                    this.hideShowcontrols(true);
                    //$("#kendoleadBarControl").hide();
                }
                else {
                    // This can happen when the campus has not leads
                    if (leadGuid.value !== "") {
                        // Show a lead.. then show in the bar the lead info.
                        kendoBarvisible = true;
                        this.hideShowcontrols(false);
                       // $("#kendoleadBarControl").show();
                        this.viewModel.getLeadInfoFromServer(leadGuid.value);
                        this.viewModel.getLeadImageFromServer(leadGuid.value);
                        //this.viewModel.getLeadRequirementsTypeMet(leadGuid.value, XMASTER_GET_CURRENT_CAMPUS_ID);
                    }
                }

                document.getElementById("infobarToLeadInfoPage").addEventListener("click", this.viewModel.goToLeadInfoScreen);

                if (kendoBarvisible) {
                    this.thecamera = new MasterPage.WebCamControl(leadGuid.value, "AD");
                    var closeWindow = this.windowsClose;
                    var that = this;
                // Call to photo windows
                $("#infobarImage")
                    .click((e) => {

                        let win = $("#webCamWindow").data("kendoWindow") as kendo.ui.Window;
                        win.open().center();
                        win.unbind("close");
                        win.bind("close",
                            () => {
                                closeWindow(that);
                             });
                        // this.thecamera.initializeWebCam();
                        this.thecamera.connectCamera(leadGuid.value, "AD");
                    });
                }


                // This bind the view model with all.-------------------------------------------------------------------------
                kendo.init($("#kendoleadBarControl"));
                kendo.bind($("#kendoleadBarControl"), ethis.viewModel);

                // kendo.ui.progress($("#leadRequirementsMet .progressbar"), true);

            }
            catch (e) {
                MasterPage.SHOW_ERROR_WINDOW(e.message + "/n" + e.stack);
                //alert();

            }
        }
        public refreshHaveMetRequirements() {
            let leadGuid: any = document.getElementById('leadId');
            this.viewModel.getLeadRequirementsTypeMet(leadGuid.value, XMASTER_GET_CURRENT_CAMPUS_ID);
        }

        windowsClose(context: any) {
            let info = MasterPage.Common.detectNavigator().toLowerCase();
            let infoarray = info.split(" ");
            //if (infoarray[0] === "firefox") {
            //    //reload page
            //    window.location.reload(false);
            //} else {
               let leadGuid: any = document.getElementById('leadId');
               context.viewModel.getLeadImageFromServer(leadGuid.value);
            //}
          
        };

        hideShowcontrols(ishide: boolean) {
            if (ishide) {
                $("#kendoleadBarControl").hide();
                $("#kendoleadBarWebCamControl").hide();
            } else {
                $("#kendoleadBarControl").show();
                $("#kendoleadBarWebCamControl").show();
            }
            
        }
    }
} 