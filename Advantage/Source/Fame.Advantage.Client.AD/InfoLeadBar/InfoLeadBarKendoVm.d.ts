/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
declare module AD {
    class LeadInfoBarVm extends kendo.Observable {
        infolead: kendo.data.DataSource;
        db: LeadInfoBarDb;
        constructor();
        getLeadInfoFromServer(leadGuid: string): void;
        getLeadReqStatusFromServer(leadGuid: string): kendo.data.DataSource;
        static formatDate(d: any): string;
        goToLeadInfoScreen(): void;
        static refreshLeadFields(leadFullName: string, programVersionName: string, expectedStart: string, admissionRep: string, dateAssigned: string, currentStatus: string): void;
    }
}
