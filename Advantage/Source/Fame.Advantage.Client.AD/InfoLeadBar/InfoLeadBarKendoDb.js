var AD;
(function (AD) {
    var LeadInfoBarDb = (function () {
        function LeadInfoBarDb() {
        }
        LeadInfoBarDb.prototype.showNotification = function (message) {
            var notif = $("#kNotification").kendoNotification({
                stacking: "down",
                position: { bottom: 30, right: 50 },
                autoHideAfter: 3000,
                templates: [{ type: "info", template: $("#infoTemplate").html() }]
            });
            notif.data("kendoNotification").show({ title: "Advantage", message: message, src: AD.XIMAGE_INFO }, "info");
        };
        LeadInfoBarDb.prototype.getInfoLeadFromServer = function (leadguid, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XGET_SERVICE_LAYER_LEAD_INFOBAR_COMMAND_GET + "?LeadGuid=" + leadguid,
                type: 'GET',
                timeout: 15000,
                dataType: 'json'
            });
        };
        LeadInfoBarDb.prototype.getInfoLeadReqStatusFromServer = function (leadguid) {
            var url = AD.XGET_SERVICE_LAYER_LEAD_REQUIREMENT_STATUS_GET + "?LeadGuid=" + leadguid;
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }
                },
                error: ad.SHOW_DATA_SOURCE_ERROR,
                schema: {
                    model: {
                        RequirementName: "RequirementName",
                        TypeOfRequirement: "TypeOfRequirement",
                        RequirementStatus: "RequirementStatus",
                        IsMandatory: "IsMandatory",
                        ImagePath: "ImagePath"
                    }
                }
            });
            return ds;
        };
        return LeadInfoBarDb;
    }());
    AD.LeadInfoBarDb = LeadInfoBarDb;
})(AD || (AD = {}));
//# sourceMappingURL=InfoLeadBarKendoDb.js.map