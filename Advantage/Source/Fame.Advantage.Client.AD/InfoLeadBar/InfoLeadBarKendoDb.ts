﻿/// <reference path="../ServiceUrls.ts" />
/// <reference path="../../fame.advantage.client.masterpage/serviceurls.ts" />

module AD {

    export class LeadInfoBarDb {

 
        public showNotification(message: string) {

            var notif = $("#kNotification").kendoNotification({
                stacking: "down",
                position: { bottom: 30, right: 50 },
                autoHideAfter: 3000,
                // modal: false,
                templates: [{ type: "info", template: $("#infoTemplate").html() }]

            }) as any;
            notif.data("kendoNotification").show({ title: "Advantage", message: message, src: XIMAGE_INFO }, "info");
        }

        /* Get the info from server */
        public getInfoLeadFromServer(leadguid: string, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_LEAD_INFOBAR_COMMAND_GET + "?LeadGuid=" + leadguid,
                type: 'GET',
                timeout: 25000,
                dataType: 'json'
            });
        }

        /* Get the info from server */
        public getLeadPhotoFromServer(leadguid: string, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: MasterPage.XGET_SERVICE_LAYER_ENTITY_IMAGE + "?Command=1&EntityId=" + leadguid,
                type: "GET",
                timeout: 25000,
                dataType: "json"
            });
        }


        public getInfoLeadReqStatusFromServer(leadguid: string) {
            var url = XGET_SERVICE_LAYER_LEAD_REQUIREMENT_STATUS_GET + "?LeadGuid=" + leadguid;
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        timeout: 25000,
                        dataType: "json",
                        type: "GET"
                    }
                },
                error: MasterPage.SHOW_DATA_SOURCE_ERROR,

                schema: {
                    model: {
                        RequirementName: "RequirementName",
                        TypeOfRequirement: "TypeOfRequirement",
                        RequirementStatus: "RequirementStatus",
                        IsMandatory: "IsMandatory",
                        ImagePath: "ImagePath"
                    }
                }
            });
            return ds;

        }

        public getLeadRequirementsTypeMet(leadId: string, campusId: string , mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_LEAD_REQUIREMENTS_TYPE_MET_GET.replace("@leadId@", leadId) + "?campusId=" + campusId,
                type: 'GET',
                timeout: 25000,
                async: true,
                dataType: 'json'
            });
        }
    }
}

 