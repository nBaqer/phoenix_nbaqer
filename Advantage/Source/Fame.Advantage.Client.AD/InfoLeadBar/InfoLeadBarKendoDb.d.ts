/// <reference path="../ServiceUrls.d.ts" />
declare module AD {
    class LeadInfoBarDb {
        showNotification(message: string): void;
        getInfoLeadFromServer(leadguid: string, mcontext: any): JQueryXHR;
        getInfoLeadReqStatusFromServer(leadguid: string): kendo.data.DataSource;
    }
}
