﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
module AD {

    declare var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL: string;
    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;

    export class LeadInfoBarVm extends kendo.Observable {

        infolead: kendo.data.DataSource;
        db: LeadInfoBarDb;

        constructor() {
            super();
            this.db = new LeadInfoBarDb();
        }

        getLeadImageFromServer(leadGuid: string) {
            try {
                $("body").css("cursor", "progress");

                // Request the info from server
                this.db.getLeadPhotoFromServer(leadGuid, this)
                    .done(msg => {
                        if (msg != undefined) {
                            // Get image from encode64...
                            let imgblob = MasterPage.Common.b64ToBlob(msg.ImageBase64, msg.ExtensionType, 512);
                            if (imgblob !== undefined && imgblob !== null) {

                                $("#infobarImage").removeClass("k-icon").removeClass("k-i-user")
                                    .removeClass("k-icon-64");

                                if ($("#infobarImage img").length === 0) {
                                    $("#infobarImage").append("<img style='height:64px; width:64px;'/>");
                                }

                                $("#infobarImage img").prop("src", URL.createObjectURL(imgblob));

                            } else {
                                if (!$("#infobarImage").hasClass("k-icon")) {
                                    $("#infobarImage").addClass("k-icon").addClass("k-i-user").addClass("k-icon-64");
                                }
                            }

                        }
                    })
                    .fail(msg => {
                        MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    });
            }
            finally {
                $("body").css("cursor", "default");
            }

        }

        getLeadInfoFromServer(leadGuid: string) {
            try {
                $("body").css("cursor", "progress");


                //alert(newLeadGuid + " - " + dupLeadGuid);
                // Request the info from server
                this.db.getInfoLeadFromServer(leadGuid, this)
                    .done(msg => {
                        if (msg != undefined) {

                            LeadInfoBarVm.refreshLeadFields(
                                msg.LeadFullName,
                                msg.ProgramVersionName,
                                msg.ExpectedStart,
                                msg.AdmissionRep,
                                msg.DateAssigned,
                                msg.CurrentStatus);

                            // Get image from encode64...
                            // $("#infobarTableImage").attr("src", msg.LeadImagePath);
                            //logic to disable the enroll button for the enrolled lead
                            let leadtoolBar = new LeadToolbarVm();
                            leadtoolBar.disableEnrollBtn(msg.IsLeadEnrolled);
                            //logic to hide the requirements met for the enrolled lead
                            if (!msg.IsLeadEnrolled) {
                                this.getLeadRequirementsTypeMet(leadGuid, XMASTER_GET_CURRENT_CAMPUS_ID);
                            } else {
                                $("#leadRequirementsMet .loading").hide();
                                $("#leadRequirementsMet").hide();
                            }
                        }
                    })
                    .fail(msg => {
                        MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    });
            }
            finally {
                $("body").css("cursor", "default");
            }
        }


        getLeadRequirementsTypeMet(leadId: string, campusId: string) {
            try {
                //$("body").css("cursor", "progress");
                $("#leadRequirementsMet .loading").removeClass("hidden");

                // Request the requirements types that are met from server
                this.db.getLeadRequirementsTypeMet(leadId, campusId, this)
                    .done(msg => {
                        if (msg != undefined) {

                            $("#leadRequirementsMet li")
                                .each((index, element) => {
                                    if (!$(element).hasClass("loading")) {
                                        $(element).remove();
                                    }
                                });

                            let leadRequirementsType: Array<ILeadRequirementsType> = msg;

                            for (let type of leadRequirementsType) {
                                if (type.IsMet && type.IsDefined) {
                                    $("#leadRequirementsMet").append("<li class='met'> <span class='k-icon k-i-check font-green'></span>" + type.Name + "</li>");
                                }
                                else if (!type.IsDefined) {
                                    $("#leadRequirementsMet").append("<li class='not-defined'>" + type.Name + "</li>");
                                } else {
                                    $("#leadRequirementsMet").append("<li>" + type.Name + "</li>");
                                }

                            }
                        }
                        $("#leadRequirementsMet .loading").hide();
                    })
                    .fail(msg => {
                        MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                        $("#leadRequirementsMet .loading").hide();
                    });
            }
            finally {
            }
        }

        // Get the list of status of requirement from server
        getLeadReqStatusFromServer(leadGuid: string) {
            // Request the info from server
            return this.db.getInfoLeadReqStatusFromServer(leadGuid);
        }

        static formatDate(d) {
            function addZero(n) {
                return n < 10 ? '0' + n : '' + n;
            }

            return addZero(d.getMonth() + 1) + "/" + addZero(d.getDate()) + "/" + d.getFullYear();
        }

        public goToLeadInfoScreen() {
            sessionStorage.setItem(XLEAD_NEW, "false");
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?resid=170&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&mod=AD");
        }

        /*Refresh the lead info banner*/
        static refreshLeadFields(
            leadFullName: string,
            programVersionName: string,
            expectedStart: string,
            admissionRep: string,
            dateAssigned: string,
            currentStatus: string) {

            $("#infoBarTableFullname").text(leadFullName);
            if (programVersionName === "Select") {
                programVersionName = "";
            }
            $("#infoBarTableProgramVersion").text(programVersionName);
            var date: string = "";
            if (expectedStart === "Invalid Date") {
                date = "";
            } else {
                if (expectedStart != null && expectedStart.length > 1) {
                    if (expectedStart.substring(0, 4) === "0001") {
                        date = "";
                    } else {
                        date = this.formatDate(new Date(expectedStart));
                    }
                }
            }
            $("#infoBarTableExpectedStart").text(date);
            date = "";
            $("#infoBarTableAdmissionRep").text(admissionRep);
            if (dateAssigned != null && dateAssigned.length > 1) {
                date = this.formatDate(new Date(dateAssigned));
            }
            $("#infoBarTableDateAssigned").text(date);
            $("#infoBarTableStatus").text(currentStatus);
        }
    }
}  