var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AD;
(function (AD) {
    var LeadInfoBarVm = (function (_super) {
        __extends(LeadInfoBarVm, _super);
        function LeadInfoBarVm() {
            _super.call(this);
            this.db = new AD.LeadInfoBarDb();
        }
        LeadInfoBarVm.prototype.getLeadInfoFromServer = function (leadGuid) {
            try {
                $("body").css("cursor", "progress");
                this.db.getInfoLeadFromServer(leadGuid, this)
                    .done(function (msg) {
                    if (msg != undefined) {
                        LeadInfoBarVm.refreshLeadFields(msg.LeadFullName, msg.ProgramVersionName, msg.ExpectedStart, msg.AdmissionRep, msg.DateAssigned, msg.CurrentStatus);
                        $("#infobarTableImage").attr("src", msg.LeadImagePath);
                    }
                })
                    .fail(function (msg) {
                    ad.SHOW_DATA_SOURCE_ERROR(msg);
                });
            }
            finally {
                $("body").css("cursor", "default");
            }
        };
        LeadInfoBarVm.prototype.getLeadReqStatusFromServer = function (leadGuid) {
            return this.db.getInfoLeadReqStatusFromServer(leadGuid);
        };
        LeadInfoBarVm.formatDate = function (d) {
            function addZero(n) {
                return n < 10 ? '0' + n : '' + n;
            }
            return addZero(d.getMonth() + 1) + "/" + addZero(d.getDate()) + "/" + d.getFullYear();
        };
        LeadInfoBarVm.prototype.goToLeadInfoScreen = function () {
            sessionStorage.setItem(AD.XLEAD_NEW, "false");
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&mod=AD");
        };
        LeadInfoBarVm.refreshLeadFields = function (leadFullName, programVersionName, expectedStart, admissionRep, dateAssigned, currentStatus) {
            $("#infoBarTableFullname").text(leadFullName);
            $("#infoBarTableProgramVersion").text(programVersionName);
            var date = "";
            if (expectedStart === "Invalid Date") {
                date = "";
            }
            else {
                if (expectedStart != null && expectedStart.length > 1) {
                    if (expectedStart.substring(0, 4) === "0001") {
                        date = "";
                    }
                    else {
                        date = this.formatDate(new Date(expectedStart));
                    }
                }
            }
            $("#infoBarTableExpectedStart").text(date);
            date = "";
            $("#infoBarTableAdmissionRep").text(admissionRep);
            if (dateAssigned != null && dateAssigned.length > 1) {
                date = this.formatDate(new Date(dateAssigned));
            }
            $("#infoBarTableDateAssigned").text(date);
            $("#infoBarTableStatus").text(currentStatus);
        };
        return LeadInfoBarVm;
    }(kendo.Observable));
    AD.LeadInfoBarVm = LeadInfoBarVm;
})(AD || (AD = {}));
//# sourceMappingURL=InfoLeadBarKendoVm.js.map