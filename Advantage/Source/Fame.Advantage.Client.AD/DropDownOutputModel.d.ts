declare module AD {
    interface ICampusOutput {
        CampusId: string;
    }
    class CampusOutput {
        CampusId: string;
    }
    interface IDropDownWithCampusOutputModel {
        CampusesIdList: CampusOutput[];
        ID: string;
        Description: string;
    }
    class DropDownWithCampusOutputModel {
        CampusesIdList: CampusOutput[];
        ID: string;
        Description: string;
    }
}
