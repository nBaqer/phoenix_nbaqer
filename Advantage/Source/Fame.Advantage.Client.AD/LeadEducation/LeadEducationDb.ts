﻿module AD {
    import XgetInstitutionAddress = AD.XGET_LEAD_EDUCATION;
    import XfindInstitutionAddress = AD.XFIND_LEAD_EDUCATION;
    import XpostInstitutionAddress = AD.XPOST_LEAD_EDUCATION;
    import XputInstitutionAddress = AD.XPUT_LEAD_EDUCATION;
    import XdeleteInstitutionAddress = AD.XDELETE_LEAD_EDUCATION;
    export class LeadEducationDb {
        public getData(input: ILeadEducationFilterInputModel) {
            return $.ajax({
                url: XgetInstitutionAddress + "?" + $.param(input),
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }
        public findData(input: ILeadEducationFilterInputModel): kendo.data.DataSource {
            return new kendo.data.DataSource({
                pageSize: 5,
                transport: {
                    read: {
                        url(options) {
                            var uri = XfindInstitutionAddress;
                            delete options.baseUrl;
                            return uri;
                        },
                        dataType: "json",
                        timeout: 20000,
                        type: "GET",
                        data: input
                    }
                }
            });
        }
        public postData(input: ILeadEducationInputModel) {
            return $.ajax({
                data: JSON.stringify(input),
                url: XpostInstitutionAddress,
                type: 'POST',
                timeout: 20000,
                dataType: 'json'
            });
        }
        public putData(input: ILeadEducationInputModel) {
            return $.ajax({
                data: JSON.stringify(input),
                url: XputInstitutionAddress,
                type: 'PUT',
                timeout: 20000,
                dataType: 'json'
            });
        }
        public deleteData(input: ILeadEducationFilterInputModel) {
            return $.ajax({
                data: JSON.stringify(input),
                url: XdeleteInstitutionAddress + "?" + $.param(input),
                type: 'DELETE',
                timeout: 20000,
                dataType: 'json'
            });
        }
        public getSdfList(pgResId: number,campusId:string, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_SDF_GET + "?PageResourceId=" + pgResId + "&CampusId=" + campusId,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }
        public getSdfListValues(leadId: string, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_PRIORWORK + "?LeadId=" + leadId + "&Command=" + 8,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }
        public postSdf(om: PriorWorkOutputModel, context: any) {

            $.ajax({
                type: "POST",
                dataType: "json",
                url: XPOST_SERVICE_LAYER_PRIORWORK,
                data: JSON.stringify(om)
            })
                .done(
                result => {
                    MasterPage.SHOW_INFO_WINDOW("Custom fields saved successfully");

                })
                .fail(result => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                });
        }
    }
}