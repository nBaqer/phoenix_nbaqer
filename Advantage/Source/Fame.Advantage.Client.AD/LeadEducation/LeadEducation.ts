﻿/// <reference path="../../Fame.Advantage.Client.SY/Institution/Institution.ts" />
/// <reference path="../../Fame.Advantage.Client.SY/Institution/Models/InstitutionOutputModel.ts" />
module AD {
   

    export class LeadEducation {
        filter: ILeadEducationFilterInputModel;
        db: LeadEducationDb;
        public viewModel: LeadEducationVm;
        public institution: SystemInstitution.Institution;


        constructor(filter: ILeadEducationFilterInputModel) {
            this.filter = filter;
            this.db = new LeadEducationDb();
            MasterPage.Common.enableDisableBtns(false);
        }

        public initialize(gridId: string, template: string, addLinkId: string) {
            this.viewModel = new LeadEducationVm(this.filter, gridId, this.db, template, addLinkId);
            this.viewModel.sdfValidator = MasterPage.ADVANTAGE_VALIDATOR("dvCustomFields");
            $("#sdfSave").kendoButton({
                icon: "check",
                click: (e) => {
                    //call to save sdf values
                    if (this.viewModel.sdfValidator.validate()) {
                        this.viewModel.saveSdfValues();
                    } else {
                        MasterPage.SHOW_WARNING_WINDOW(X_SAVE_PLEASE_FILL_REQUIRED_FIELDS);
                    }
                }
            });
            this.viewModel.bindGrid();   
            this.viewModel.processSdfFields();
        }
    }
}