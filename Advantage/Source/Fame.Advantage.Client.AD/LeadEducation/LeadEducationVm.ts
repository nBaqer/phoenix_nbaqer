﻿/// <reference path="../../Fame.Advantage.Client.SY/CatalogDb.ts" />
/// <reference path="../../Fame.Advantage.Client.SY/Institution/InstitutionDb.ts" />
/// <reference path="../../Fame.Advantage.Client.SY/Institution/Models/InstitutionFilterInputModel.ts" />
/// <reference path="../../Fame.Advantage.Client.SY/Institution/Institution.ts" />
/// <reference path="../../Fame.Advantage.Client.SY/Institution/Models/InstitutionOutputModel.ts" />

module AD {
    import IInstitutionFilterInputModel = SystemInstitution.IInstitutionFilterInputModel;
    import InstitutionOutputModel = SystemInstitution.IInstitutionOutputModel;

    export class LeadEducationVm {
        filter: ILeadEducationFilterInputModel;
        gridId: string;
        db: LeadEducationDb;
        templateId: string;
        grid: kendo.ui.Grid;
        rawDataSource: Array<ILeadEducationOutputModel>;
        dataSource: kendo.data.DataSource;
        popup: kendo.ui.Window;
        addLinkId: string;
        catalogDb: SY.CatalogDb;
        formValidator: kendo.ui.Validator;
        form: any;
        isNewRow: boolean;
        selectedInstitution: any;
        modelEditing: ILeadEducationOutputModel;
        isInstitutionAddedToMru: boolean;
        doesSchoolDefinedExist: boolean;
        institution: SystemInstitution.Institution;
        isDetailsVisible: boolean;
        public sdfValidator: kendo.ui.Validator;
        isInstitutionEditable: boolean;

        constructor(filter: ILeadEducationFilterInputModel, gridId: string, db: LeadEducationDb, templateId: string, addLinkId: string) {
            this.filter = filter;
            this.gridId = gridId;
            this.db = db;
            this.templateId = templateId;
            this.isDetailsVisible = false;
            this.isInstitutionEditable = false;
            if (this.gridId.substr(0, 1) !== "#") {
                this.gridId = "#" + this.gridId;
            }
            if (this.templateId.substr(0, 1) !== "#") {
                this.templateId = "#" + this.templateId;
            }
            if (addLinkId.substr(0, 1) !== "#") {
                this.addLinkId = "#" + addLinkId;
            }
            this.catalogDb = new SY.CatalogDb();
            this.isInstitutionAddedToMru = false;
            this.doesSchoolDefinedExist = false;

            let that = this;
            $(this.addLinkId).click((e: any) => {
                that.isNewRow = true;
                that.isInstitutionEditable = false;
                $("#institutionSection").addClass("hidden");
                that.modelEditing = undefined;
                that.selectedInstitution = undefined;
                that.isInstitutionAddedToMru = false;
                that.doesSchoolDefinedExist = false;
                that.institution = undefined;
                that.isDetailsVisible = false;
                that.grid.addRow();
            });
        }

        public bindGrid() {
            let that = this;
            if (this.grid === undefined || this.grid === null) {
                let gridOptions = {
                    dataSource: this.db.findData(this.filter),
                    sortable: false,
                    selectable: false,
                    scrollable: true,
                    pageable: true,
                    resizable: true,
                    editable: {
                        mode: "popup",
                        template: kendo.template($(this.templateId).html())
                    },
                    edit: (e: any) => {
                        that.onGridEditingMode(e, that);
                    },
                    cancel: (e: any) => {
                        that.isNewRow = false;
                        MasterPage.showPageLoading(false);
                    },
                    columns: this.getGridColumns(that)
                } as kendo.ui.GridOptions;

                $(this.gridId).kendoGrid(gridOptions);

                this.grid = $(this.gridId).data("kendoGrid") as kendo.ui.Grid;
                this.grid.refresh();
                this.grid.table.on("click", ".descriptionClick", (e: any) => {
                    that.isDetailsVisible = true;
                    var row = $(e.currentTarget).closest("tr");
                    var dataItem = that.grid.dataItem(row) as any;
                    this.showInstitutionDetails(dataItem, that);
                });
            }
        }

        getGridColumns(that: LeadEducationVm): Array<kendo.ui.GridColumn> {
            return [
                { field: "Id", width: "0", hidden: true },
                { field: "Level", title: "Level", width: "80px" },
                { field: "Institution", title: "Institution", width: "200px", template: "# if (data.Institution !== undefined && data.Institution !== null) { # <a class='descriptionClick' href='javascript:void(0)'>#=data.Institution.Name#</a>" + "# } #", attributes: { style: "cursor:pointer;" } },
                { field: "Major", title: "Major", width: "50px" },
                { field: "FinalGrade", title: "Final Grade", width: "70px" },
                { field: "Gpa", title: "GPA", width: "50px" },
                { field: "Rank", title: "Rank", width: "50px" },
                { field: "Percentile", title: "Percentile", width: "70px" },
                { field: "Graduate", title: "Graduate", width: "70px", template: "# if (data.Graduate !== undefined && data.Graduate !== null) { if (data.Graduate === true) {# Yes #} else {# No #}}#" },
                { field: "GraduationDate", title: "Grad Date", width: "80px" },
                { field: "CertificateOrDegree", title: "Certificate/Degree", width: "100px" },
                { command: [{ name: "edit", text: " " }], title: "Edit", width: "50px" },
                {
                    command: [
                        {
                            name: "remove",
                            click: (e) => {
                                this.onGridDeleting(e, that);
                            },
                            text: "X"
                        }
                    ], title: "", width: "30px"
                }
            ] as Array<kendo.ui.GridColumn>;
        }

        onGridEditingMode(e: any, that: LeadEducationVm) {
            //Place your Edit Logic Here
            that.popup = e.container.data("kendoWindow");
            that.form = e.container;
            that.formValidator = MasterPage.ADVANTAGE_VALIDATOR("leadEducationFormValidator");
            that.isInstitutionAddedToMru = false;
            that.doesSchoolDefinedExist = false;
            that.isInstitutionEditable = false;
            let btoSave = e.container.find(".k-grid-update");
            btoSave.unbind("click");
            btoSave.bind("click", (e) => {
                that.onGridSaving(e, that);
            });

            if (that.popup !== undefined) {
                that.popup.setOptions({
                    title: "1. Select Level",
                    width: 700,
                    height: 700,
                    actions: ["close"],
                    resizable: false,
                    draggable: true,
                    modal: true
                });
                that.popup.center().open();
                $(that.popup.wrapper[0].firstChild)
                    .after("<div class='k-window-titlebar k-header' style='position: relative;margin-top: 2px;'>&nbsp;<span class='k-window-title' id='leadEducationForm_wnd_title_2'>2. Select School Defined or National for a dropdown list, or New to enter a new institution manually</span><div class='k-window-actions'></div></div>");
            } else {
                that.popup.center().open();
            }

            let institutionDb: SystemInstitution.InstitutionDb = new SystemInstitution.InstitutionDb();

            that.form.find(".k-edit-form-container").attr("style", "width: 90%;");

            let acDdlInstitutionName = that.form.find("#acDdlInstitutionName");

            that.form.find("#ddlGraduate")
                .kendoDropDownList();
            that.form.find("#ddlGraduate").data("kendoDropDownList").select(0);

            that.form.find("#txtFinalGrade").kendoMaskedTextBox({
                mask: "AAAAA"
            });
            that.form.find("#txtGpa").kendoMaskedTextBox({
                mask: "0.00"
            });
            that.form.find("#txtRank").kendoMaskedTextBox({
                mask: "00000"
            });
            that.form.find("#txtPercentile").kendoMaskedTextBox({
                mask: "000"
            });

            that.form.find(".k-edit-form-container").css("cursor", "wait");

            $("#rlSchoolDefined").attr("disabled", "disabled");
            $("#rlNational").attr("disabled", "disabled");
            $("#rlNew").attr("disabled", "disabled");
            that.form.find("#ddlLevel")
                .kendoDropDownList({
                    dataSource: that.catalogDb.getData("Levels", that.filter.CampusId, "", this),
                    dataBound: (e) => {
                        let autoComplete = $(acDdlInstitutionName).data("kendoAutoComplete") as kendo.ui.AutoComplete;
                        autoComplete.setDataSource(institutionDb.findData({ CampusId: that.filter.CampusId, UserId: that.filter.UserId, Level: that.form.find("#ddlLevel").data("kendoDropDownList").value(), InstitutionType: that.form.find("input[name='rgSelect']:checked").val() } as IInstitutionFilterInputModel));
                    },
                    dataTextField: "Description",
                    dataValueField: "ID",
                    optionLabel: "Select",
                    change: (e) => {
                        let selectedSearch = that.form.find("input[name='rgSelect']:checked").val();

                        let text = e.sender.text();
                        if (text === undefined || text === null || text === "Select") {
                            $(acDdlInstitutionName).attr("disabled", "disabled");
                            $(acDdlInstitutionName).addClass("disabled");
                            $(acDdlInstitutionName).attr("required", "required");
                            $(acDdlInstitutionName).removeAttr("required");

                            that.form.find("#acDdlInstitutionName").data("kendoAutoComplete").wrapper.removeClass("hidden");
                            if (selectedSearch !== undefined && selectedSearch !== null) {
                                if (selectedSearch === "1" || selectedSearch === "2") {
                                    that.form.find("#acDdlInstitutionName").attr("required");
                                    that.form.find("#acDdlInstitutionName").data("kendoAutoComplete").wrapper.removeClass("hidden");
                                    that.form.find("#txtInstitutionName").addClass("hidden");
                                    that.form.find("#txtInstitutionName").removeAttr("required");
                                } else {
                                    that.form.find("#acDdlInstitutionName").removeAttr("required");
                                    that.form.find("#acDdlInstitutionName").data("kendoAutoComplete").wrapper.addClass("hidden");
                                    that.form.find("#txtInstitutionName").removeClass("hidden");
                                    that.form.find("#txtInstitutionName").attr("required");
                                }
                            }
                            $("#rlSchoolDefined").attr("disabled", "disabled");
                            $("#rlNational").attr("disabled", "disabled");
                            $("#rlNew").attr("disabled", "disabled");
                        } else {
                            if (that.doesSchoolDefinedExist) {
                                $("#rlSchoolDefined").removeAttr("disabled");
                            }
                            $("#rlNew").removeAttr("disabled");

                            if (selectedSearch !== undefined && selectedSearch !== null) {

                                if (selectedSearch === "1" || selectedSearch === "2") {
                                    $(acDdlInstitutionName).removeAttr("disabled");
                                    $(acDdlInstitutionName).removeClass("disabled");
                                    that.form.find("#acDdlInstitutionName")
                                        .data("kendoAutoComplete")
                                        .wrapper.removeClass("hidden");
                                    $(acDdlInstitutionName).attr("required", "required");
                                    that.form.find("#txtInstitutionName").removeAttr("required");
                                    that.form.find("#txtInstitutionName").addClass("hidden");
                                } else {
                                    that.form.find("#txtInstitutionName").removeClass("hidden");
                                    that.form.find("#txtInstitutionName").attr("required", "required");
                                    that.form.find("#txtInstitutionName").removeAttr("disabled");
                                    that.form.find("#txtInstitutionName").removeClass("disabled");

                                    that.form.find("#acDdlInstitutionName").removeAttr("required");
                                    that.form.find("#acDdlInstitutionName").data("kendoAutoComplete").wrapper.addClass("hidden");
                                }
                            }
                        }
                        if (e.sender.value() === "3" || e.sender.value() === "4") {
                            $("#rlNational").attr("disabled", "disabled");
                            $("#rlNational").prop("checked", false);
                            if (selectedSearch !== undefined && selectedSearch !== null && selectedSearch === "3") {
                                $(acDdlInstitutionName).attr("disabled", "disabled");
                                $(acDdlInstitutionName).addClass("disabled");
                            }
                        } else {
                            $("#rlNational").removeAttr("disabled");
                        }
                        let autoComplete = $(acDdlInstitutionName).data("kendoAutoComplete") as kendo.ui.AutoComplete;
                        autoComplete.value("");

                        autoComplete.setDataSource(institutionDb.findData({ CampusId: that.filter.CampusId, UserId: that.filter.UserId, Level: that.form.find("#ddlLevel").data("kendoDropDownList").value(), InstitutionType: that.form.find("input[name='rgSelect']:checked").val() } as IInstitutionFilterInputModel));
                    }
                });

            let level: any;
            if (!that.isNewRow) {
                level = e.model.LevelId;
            } else {
                level = that.form.find("#ddlLevel").data("kendoDropDownList").value();
            }

            institutionDb.getExistByImportType({ CampusId: that.filter.CampusId, UserId: that.filter.UserId, InstitutionType: "1", Level: level } as SystemInstitution.IInstitutionFilterInputModel)
                .done((result) => {
                    that.doesSchoolDefinedExist = result;
                    if (result === false) {
                        that.form.find("#rlSchoolDefined").attr("disabled", "disabled");
                    } else {
                        that.form.find("#rlSchoolDefined").removeAttr("disabled");
                    }
                    that.form.find(".k-edit-form-container").css("cursor", "default");
                })
                .fail((e) => {
                    that.form.find(".k-edit-form-container").css("cursor", "default");
                });

            acDdlInstitutionName.kendoAutoComplete(
                {
                    dataTextField: "Name",
                    placeholder: "Enter a name to search for an institution",
                    filter: "contains",
                    minLength: 3,
                    dataSource: institutionDb.findData({ CampusId: that.filter.CampusId, UserId: that.filter.UserId, Level: that.form.find("#ddlLevel").data("kendoDropDownList").value(), InstitutionType: that.form.find("input[name='rgSelect']:checked").val() } as IInstitutionFilterInputModel),
                    select: (e) => {
                        that.selectedInstitution = ($(acDdlInstitutionName)
                            .data("kendoAutoComplete") as kendo.ui.AutoComplete)
                            .dataItem(e.item.index());
                        let level = that.form.find("#ddlLevel").data("kendoDropDownList").value();
                        let selectedSearch = that.form.find("input[name='rgSelect']:checked").val();
                        if (selectedSearch === "2" && (level === "1" || level === "2")) {
                            $.when(MasterPage
                                .SHOW_CONFIRMATION_WINDOW_PROMISE("Do you want add this selection to the MRU list?"))
                                .then(confirmed => {
                                    that.isInstitutionAddedToMru = confirmed;
                                });
                        }
                    }
                }
            );
            $(acDdlInstitutionName).attr("disabled", "disabled");
            $(acDdlInstitutionName).addClass("disabled");

            that.form.find("input[name='rgSelect']").change((e) => {
                let gridUpdateButton = that.form.find(".k-grid-update");
                let value = $(e.currentTarget).val();
                if (value === "1" || value === "2") {
                    $(gridUpdateButton).removeClass("hidden");
                    that.form.find("#txtInstitutionName").val("");
                    that.form.find("#acDdlInstitutionName").data("kendoAutoComplete").wrapper.removeClass("hidden");
                    that.form.find("#acDdlInstitutionName").attr("required", "required");
                    that.form.find("#txtInstitutionName").removeAttr("required");
                    that.form.find("#txtInstitutionName").addClass("hidden");
                    that.form.find("#txtInstitutionName").attr("disabled");

                    if (that.form.find("#ddlLevel").data("kendoDropDownList").text() === "Select") {
                        $(acDdlInstitutionName).attr("disabled", "disabled");
                    } else {
                        $(acDdlInstitutionName).removeAttr("disabled");
                        $(acDdlInstitutionName).removeClass("disabled");
                    }
                } else {
                    that.form.find(".k-grid-update").addClass("hidden");
                    that.form.find("#txtInstitutionName").removeClass("hidden");
                    that.form.find("#txtInstitutionName").attr("required", "required");

                    that.form.find("#acDdlInstitutionName").removeAttr("required");
                    that.form.find("#acDdlInstitutionName").data("kendoAutoComplete").wrapper.addClass("hidden");

                    if (that.form.find("#ddlLevel").data("kendoDropDownList").text() === "Select") {
                        that.form.find("#txtInstitutionName").attr("disabled", "disabled");
                        that.form.find("#txtInstitutionName").addClass("disabled");
                    } else {
                        that.form.find("#txtInstitutionName").removeAttr("disabled");
                        that.form.find("#txtInstitutionName").removeClass("disabled");
                    }
                    that.form.find("#txtInstitutionName").unbind("change");
                    that.form.find("#txtInstitutionName").bind("change", () => {
                        that.form.find(".k-edit-form-container").css("cursor", "wait");
                        let institutionName = that.form.find("#txtInstitutionName").val().trim();
                        if (institutionName !== null && institutionName !== undefined && institutionName.length > 0) {
                            institutionDb.getExistByName({
                                CampusId: that.filter.CampusId,
                                UserId: that.filter.UserId,
                                InstitutionType: "1",
                                Level: that.form.find("#ddlLevel").data("kendoDropDownList").value(),
                                Name: that.form.find("#txtInstitutionName").val().trim()
                            } as IInstitutionFilterInputModel).done((result) => {
                                let btoUpdate = that.form.find(".k-grid-update");
                                if (result) {
                                    MasterPage
                                        .SHOW_ERROR_WINDOW_PROMISE("An institution with the name already exist. Switch to School Defined or National List to find the institution.");
                                    $(btoUpdate).addClass("hidden");
                                } else {
                                    $(btoUpdate).removeClass("hidden");
                                }
                                that.form.find(".k-edit-form-container").css("cursor", "default");
                            }).fail(() => {
                                MasterPage
                                    .SHOW_ERROR_WINDOW_PROMISE("Unable to verify if new institution already exist.");
                                that.form.find(".k-edit-form-container").css("cursor", "default");
                            });
                        } else {
                            let btoUpdate = that.form.find(".k-grid-update");
                            MasterPage.SHOW_WARNING_WINDOW_PROMISE("Institution Name is a required field.");
                            $(btoUpdate).addClass("hidden");
                        }
                    });
                }
                if (!that.isNewRow) {
                    that.isInstitutionEditable = value === "1";
                }

                let autoComplete = $(acDdlInstitutionName).data("kendoAutoComplete") as kendo.ui.AutoComplete;
                autoComplete.setDataSource(institutionDb.findData({ CampusId: that.filter.CampusId, UserId: that.filter.UserId, Level: that.form.find("#ddlLevel").data("kendoDropDownList").value(), InstitutionType: that.form.find("input[name='rgSelect']:checked").val() } as IInstitutionFilterInputModel));
                autoComplete.value("");
            });

            if (that.form.find("#txtGradDate").data("kendoDatePicker") !== undefined) {
                that.form.find("#txtGradDate").data("kendoDatePicker").destroy();
                that.form.find("#txtGradDate").children().remove();
            }

            $("#txtGradDate").kendoDatePicker({
                start: "month",
                depth: "month",
                format: "MM/dd/yyyy"
            });

            if (!that.isNewRow) {
                that.form.find("#txtCertificate").val(e.model.CertificateOrDegree);
                that.form.find("#txtComments").val(e.model.Comments);
                that.form.find("#txtFinalGrade").val(e.model.FinalGrade);
                that.form.find("#txtGpa").data("kendoMaskedTextBox").value(e.model.Gpa);

                if (e.model.Graduate === true) {
                    that.form.find("#ddlGraduate").data("kendoDropDownList").value(1);
                } else {
                    that.form.find("#ddlGraduate").data("kendoDropDownList").value(0);
                }

                that.form.find("#txtGradDate").data("kendoDatePicker").value(e.model.GraduationDate);
                if (e.model.Institution !== null && e.model.Institution !== undefined) {
                    that.form.find("#acDdlInstitutionName").data("kendoAutoComplete").value(e.model.Institution.Name);
                    that.form.find("input[name=rgSelect][value=" + e.model.Institution.TypeId + "]").prop("checked", true);
                    if (e.model.Institution.TypeId === 1) {
                        that.isInstitutionEditable = true;
                    }
                }
                
                that.form.find("#ddlLevel").data("kendoDropDownList").value(e.model.LevelId);
                that.form.find("#txtMajor").val(e.model.Major);
                that.form.find("#txtPercentile").val(e.model.Percentile);
                that.form.find("#txtRank").val(e.model.Rank);
                that.selectedInstitution = e.model.Institution;
                that.modelEditing = e.model;
                that.form.find("#acDdlInstitutionName").removeAttr("disabled");
                $(acDdlInstitutionName).removeClass("disabled");


                if (e.model.LevelId === 1 || e.model.LevelId === 2) {
                    if (that.doesSchoolDefinedExist) {
                        $("#rlSchoolDefined").removeAttr("disabled");
                    }
                    $("#rlNew").removeAttr("disabled");
                    $("#rlNational").removeAttr("disabled");
                }
                else if (e.model.LevelId === 3 || e.model.LevelId === 4) {
                    if (that.doesSchoolDefinedExist) {
                        $("#rlSchoolDefined").removeAttr("disabled");
                    }
                    $("#rlNew").removeAttr("disabled");
                }
            }

        }

        onGridDeleting(e: any, that: LeadEducationVm) {
            that.isDetailsVisible = false;
            e.preventDefault();
            let row = $(e.target).closest("tr");
            let data = that.grid.dataItem(row) as any;
            if (data !== undefined && data !== null) {
                $.when(MasterPage
                    .SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure you want to delete this record?"))
                    .then(confirmed => {
                        if (confirmed) {
                            //call the Delete here
                            MasterPage.showPageLoading(true);
                            let input = { CampusId: that.filter.CampusId, Id: data.Id, LeadId: that.filter.LeadId, UserId: that.filter.UserId } as ILeadEducationFilterInputModel;
                            that.db.deleteData(input).done((msg) => {
                                if (msg !== undefined && msg !== null) {
                                    if (msg.ResponseCode !== 200) {
                                        MasterPage.showPageLoading(false);
                                        MasterPage.SHOW_ERROR_WINDOW_PROMISE(msg.ResponseMessage);
                                    } else {
                                        $("#institutionSection").addClass("hidden");
                                        e.preventDefault();
                                        MasterPage.showPageLoading(false);
                                        that.reBindGrid(that);
                                    }
                                } else {
                                    e.preventDefault();
                                    MasterPage.showPageLoading(false);
                                    that.reBindGrid(that);
                                    $("#institutionSection").addClass("hidden");
                                }
                            }).fail((msg) => {
                                //Show Error Here
                                e.preventDefault();
                                MasterPage.showPageLoading(false);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                            });
                        }
                    });
            }
        }

        onGridSaving(e: any, that: LeadEducationVm) {
            //Implement the validator

            let isSelected: boolean = false;

            let selectedSearch = that.form.find("input[name='rgSelect']:checked").val();

            if (selectedSearch === undefined || selectedSearch === null || selectedSearch.length === 0) {
                that.form.find("#rlSelect").addClass("valueRequired");
            } else {
                that.form.find("#rlSelect").removeClass("valueRequired");
                isSelected = true;
            }
            if (that.formValidator.validate() && isSelected) {
                MasterPage.showPageLoading(true);
                let model = {} as ILeadEducationInputModel;
                model.CampusId = that.filter.CampusId;
                model.LeadId = that.filter.LeadId;
                model.UserId = that.filter.UserId;
                model.IsInstitutionAddedToMru = that.isInstitutionAddedToMru;
                model.CertificateOrDegree = that.form.find("#txtCertificate").val();
                model.Comments = that.form.find("#txtComments").val();
                model.FinalGrade = that.form.find("#txtFinalGrade").data("kendoMaskedTextBox").raw();
                model.Gpa = that.form.find("#txtGpa").data("kendoMaskedTextBox").value().replace("_", "").replace("_", "").replace("_", "");
                model.Major = that.form.find("#txtMajor").val();
                model.Percentile = that.form.find("#txtPercentile").data("kendoMaskedTextBox").raw();
                model.Rank = that.form.find("#txtRank").data("kendoMaskedTextBox").raw();

                model.GraduationDate = that.form.find("#txtGradDate").data("kendoDatePicker").value();

                if (that.modelEditing !== undefined) {
                    that.modelEditing.Comments = model.Comments;
                }

                if (that.form.find("#ddlGraduate").val() === "1") {
                    model.Graduate = true;
                } else {
                    model.Graduate = false;
                }

                model.LevelId = that.form.find("#ddlLevel").val();
                model.TypeId = that.form.find("input[name='rgSelect']:checked").val();

                if (model.TypeId === "1" || model.TypeId === "2") {
                    model.InstitutionId = that.selectedInstitution !== undefined ? that.selectedInstitution.Id : undefined;
                    if (model.TypeId === "1" && that.isInstitutionEditable) {
                        var name = that.form.find("#acDdlInstitutionName").val().trim();
                        if (name !== that.selectedInstitution.Name) {
                            model.InstitutionName = name;
                            that.modelEditing.Institution.Name = name;
                        }
                    }
                } else {
                    model.InstitutionName = that.form.find("#txtInstitutionName").val().trim();
                }

                if (that.isNewRow) {
                    that.db.postData(model).done((msg) => {
                        if (msg !== undefined && msg !== null) {
                            if (msg.ResponseCode !== 200) {
                                e.preventDefault();
                                MasterPage.showPageLoading(false);
                                if (msg.ResponseCode === 417) {
                                    MasterPage.SHOW_WARNING_WINDOW_PROMISE(msg.ResponseMessage);
                                } else {
                                    MasterPage.SHOW_ERROR_WINDOW_PROMISE(msg.ResponseMessage);
                                }
                            } else {
                                that.popup.close();
                                MasterPage.showPageLoading(false);
                                that.reBindGrid(that);
                            }
                        } else {
                            that.popup.close();
                            MasterPage.showPageLoading(false);
                            that.reBindGrid(that);
                        }
                    }).fail((msg) => {
                        //Show Fail Message
                        e.preventDefault();
                        MasterPage.showPageLoading(false);
                        MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    });
                } else {
                    model.Id = that.modelEditing.Id;

                    that.db.putData(model).done((msg) => {
                        if (msg !== undefined && msg !== null) {
                            if (msg.ResponseCode !== 200) {
                                e.preventDefault();
                                MasterPage.showPageLoading(false);
                                if (msg.ResponseCode === 417) {
                                    MasterPage.SHOW_WARNING_WINDOW_PROMISE(msg.ResponseMessage);
                                } else {
                                    MasterPage.SHOW_ERROR_WINDOW_PROMISE(msg.ResponseMessage);
                                }
                            } else {
                                that.popup.close();
                                MasterPage.showPageLoading(false);
                                that.reBindGrid(that);
                            }
                        } else {
                            that.popup.close();
                            MasterPage.showPageLoading(false);
                            that.reBindGrid(that);
                        }
                    }).fail((msg) => {
                        //Show Fail Message
                        e.preventDefault();
                        MasterPage.showPageLoading(false);
                        MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    });
                }

            } else {
                MasterPage.showPageLoading(false);
            }
        }

        public saveModel(dataItem: ILeadEducationOutputModel, that: LeadEducationVm) {
            MasterPage.showPageLoading(true);
            let model = {} as ILeadEducationInputModel;
            model.CampusId = that.filter.CampusId;
            model.LeadId = that.filter.LeadId;
            model.UserId = that.filter.UserId;
            model.CertificateOrDegree = dataItem.CertificateOrDegree;
            model.Comments = dataItem.Comments;
            model.FinalGrade = dataItem.FinalGrade;
            model.Gpa = dataItem.Gpa;
            model.Major = dataItem.Major;
            model.Percentile = dataItem.Percentile;
            model.Rank = dataItem.Rank;

            model.GraduationDate = dataItem.GraduationDate;
            model.Graduate = dataItem.Graduate;
            model.LevelId = dataItem.LevelId;
            model.TypeId = dataItem.Institution.TypeId.toString();
            model.InstitutionId = dataItem.Institution.Id;
            model.Id = dataItem.Id;

            that.db.putData(model).done((msg) => {
                if (msg !== undefined && msg !== null) {
                    if (msg.ResponseCode !== 200) {
                        MasterPage.showPageLoading(false);
                        MasterPage.SHOW_ERROR_WINDOW_PROMISE(msg.ResponseMessage);
                    } else {
                        MasterPage.showPageLoading(false);
                    }
                } else {
                    MasterPage.showPageLoading(false);
                }
            }).fail((msg) => {
                //Show Fail Message
                MasterPage.showPageLoading(false);
                MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
            });
        }

        public showInstitutionDetails(dataItem: any, that: LeadEducationVm) {
            if (that.institution === undefined || that.institution === null) {
                that.institution = new SystemInstitution.Institution();
            }

            let institutionPhonesGrid = $("#institutionPhonesGrid").data("kendoGrid") as any;
            if (institutionPhonesGrid !== undefined && institutionPhonesGrid !== null) {
                institutionPhonesGrid.destroy();
                $(institutionPhonesGrid).children().remove();
                $("#institutionPhonesGrid").html("");
            }

            let institutionAddressGrid = $("#institutionAddressGrid").data("kendoGrid") as any;
            if (institutionAddressGrid !== undefined && institutionAddressGrid !== null) {
                institutionAddressGrid.destroy();
                $(institutionAddressGrid).children().remove();
                $("#institutionAddressGrid").html("");
            }

            let institutionContactGrid = $("#institutionContactGrid").data("kendoGrid") as any;
            if (institutionContactGrid !== undefined && institutionContactGrid !== null) {
                institutionContactGrid.destroy();
                $(institutionContactGrid).children().remove();
                $("#institutionContactGrid").html("");
            }

            $("#priorEducationComentsText").val("");
            that.institution.initialize(dataItem.Institution, { CampusId: that.filter.CampusId, UserId: that.filter.UserId } as IInstitutionFilterInputModel, "institutionSection");
            that.institution.bindPhones("institutionPhonesGrid", "institutionPhoneForm", "lnkAddPhone");
            that.institution.bindAddresses("institutionAddressGrid", "institutionAddressForm", "lnkAddAddress");
            that.institution.bindContacts("institutionContactGrid", "institutionContactForm", "lnkAddContact");
            that.bindComments(dataItem, that);
        }

        public bindComments(dataItem: ILeadEducationOutputModel, sender: LeadEducationVm) {
            let model = dataItem;
            let that = sender;
            if (model.Comments !== undefined && model.Comments !== null) {
                $("#institutionSection #priorEducationComentsText").val(model.Comments);
            }
            $("#institutionSection #lnkSaveComments").unbind("click");
            $("#institutionSection #lnkSaveComments").bind("click", (e) => {
                model.Comments = $("#institutionSection #priorEducationComentsText").val();
                that.saveModel(model, that);
            });
            $("#institutionSection #lnkDeleteComments").unbind("click");
            $("#institutionSection #lnkDeleteComments").bind("click", (e) => {
                if ($("#institutionSection #priorEducationComentsText").val() !== undefined && $("#institutionSection #priorEducationComentsText").val() !== null && $("#institutionSection #priorEducationComentsText").val().length > 0) {
                    $.when(MasterPage
                        .SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure you want to delete the comments?"))
                        .then(confirmed => {
                            if (confirmed) {
                                $("#institutionSection #priorEducationComentsText").val("");
                                model.Comments = undefined;
                                that.saveModel(model, that);
                            }
                        });
                }
            });
        }

        reBindGrid(that: LeadEducationVm) {
            that.isInstitutionAddedToMru = false;
            that.doesSchoolDefinedExist = false;
            if (that.grid !== undefined && that.grid !== null) {
                that.grid.destroy();
                that.grid = undefined;
                $(that.grid).children().remove();

            }
            if (that.popup !== undefined && that.popup !== null) {
                that.popup.destroy();
            }

            if (that.form !== undefined && that.form !== null) {

                that.form.find(that.templateId + "Validator").remove();
            }
            //that.rawDataSource = undefined;
            that.dataSource = undefined;
            that.popup = undefined;
            that.form = undefined;
            that.formValidator = undefined;
            that.isNewRow = false;
            that.bindGrid();

            if (that.isDetailsVisible) {
                that.showInstitutionDetails(that.modelEditing, that);
            }
        }
        //start region sdf fields
        public processSdfFields() {
            var resourceId: string = MasterPage.GET_QUERY_STRING_PARAMETER_BY_NAME("resid");
            var sessionStorageName = AD.XPRIOR_Edu_SDF_FIELDS;
            if (resourceId === "145") {
                sessionStorageName = AD.XPRIOR_Edu_SDF_FIELDS;
            } else {
                sessionStorageName = AD.XPRIOR_Edu_SDF_FIELDS_STUDENT;
            }
            //if (sessionStorage.getItem(XPRIOR_WORK_SDF_FIELDS) != null && sessionStorage.getItem(XPRIOR_WORK_SDF_FIELDS) != undefined) {
            if (sessionStorage.getItem(sessionStorageName) != null && sessionStorage.getItem(sessionStorageName) != undefined) {
                var temp:any = Object.create($.parseJSON(sessionStorage.getItem(sessionStorageName)));
                var fixedResponse = temp.responseText.replace(/\\'/g, "'");
                if (fixedResponse !== "[]") {
                    var result = JSON.parse(fixedResponse);
                    if (result != null) {
                        this.displaysdf(result);
                    }
                } else {
                    //var sdfList = this.bo.getSdfList(146, this)
                    var sdfList1 = this.db.getSdfList(Number(resourceId), this.filter.CampusId, this)
                        .done(msg1 => {
                            if (msg1 != undefined) {
                                try {
                                    sessionStorage.setItem(sessionStorageName, JSON.stringify(sdfList1));
                                    if (msg1 != null) {
                                        //call
                                        this.displaysdf(msg1);
                                    }
                                } catch (e) {
                                    $("body").css("cursor", "default");
                                    MasterPage.SHOW_ERROR_WINDOW(e.message);
                                }
                            }
                        }).fail((msg => {
                            $("body").css("cursor", "default");
                            MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                        }));
                }
            }
            else {
                //var sdfList = this.bo.getSdfList(146, this)
                var sdfList = this.db.getSdfList(Number(resourceId), this.filter.CampusId, this)
                    .done(msg => {
                        if (msg != undefined) {
                            try {
                                sessionStorage.setItem(sessionStorageName, JSON.stringify(sdfList));
                                if (msg != null) {
                                    //call
                                    this.displaysdf(msg);
                                }
                            } catch (e) {
                                $("body").css("cursor", "default");
                                MasterPage.SHOW_ERROR_WINDOW(e.message);
                            }
                        }
                    }).fail((msg => {
                        $("body").css("cursor", "default");
                        MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    }));
            }
        }
        public displaysdf(msg: any) {
            let result = MasterPage.AdvantageUserDefinedFields.processSdfFields(msg, "dvCustomFields");
            if (result === 1) {
                $("#dvCustomBar").attr("style", "display:block");
                $("#dvCustomFields").attr("style", "display:block");
                $("#sdfSave").show();
                //call to display values
                this.showsdfValues();
            }
        }
        public showsdfValues() {
            var sdfList = this.db.getSdfListValues(this.filter.LeadId, this)
                .done(msg => {
                    if (msg != undefined) {
                        try {
                            if (msg.SdfList != null) {
                                for (var k = 0; k < msg.SdfList.length; k++) {
                                    var id: string = "#" + msg.SdfList[k].SdfId;
                                    if ($(id).attr("data-role") === 'dropdownlist') {
                                        if ($(id).data("kendoDropDownList") != null) {
                                            $(id).data("kendoDropDownList").value(msg.SdfList[k].SdfValue);
                                            $(id).data("kendoDropDownList").list.width("auto");
                                        }
                                    } else {
                                        if (msg.SdfList[k].SdfValue != null) {
                                            //get as per the datatype
                                            //character or number
                                            if (msg.SdfList[k].DtypeId === 1 || msg.SdfList[k].DtypeId === 2) {
                                                $(id).val(msg.SdfList[k].SdfValue);
                                            }
                                            //date
                                            if (msg.SdfList[k].DtypeId === 3) {
                                                if ($(id).data("kendoDatePicker") != null) {
                                                    ($(id).data("kendoDatePicker") as any)
                                                        .value(msg.SdfList[k].SdfValue);
                                                }
                                            }
                                            //checkbox
                                            if (msg.SdfList[k].DtypeId === 4) {
                                                let isf = msg.SdfList[k].SdfValue;
                                                if (isf != null) {
                                                    if (isf === false || isf === "False") {
                                                        //do nothing
                                                    } else {
                                                        if ($(id) != null) {
                                                            $(id).prop("checked", isf);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (e) {
                            $("body").css("cursor", "default");
                            MasterPage.SHOW_ERROR_WINDOW(e.message);
                        }
                    }
                }).fail((msg => {
                    $("body").css("cursor", "default");
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                }));
        }
        public saveSdfValues() {
            let om = new PriorWorkOutputModel();
            let filter = new PriorWorkInputModel();
            filter.UserId = $("#hdnUserId").val();
            //filter.PriorWorkId = this.db.selectedPriorWork;
            filter.LeadId = this.filter.LeadId;
            filter.Command = 9;
            om.InputModel = filter;
            om.PriorWorkItemList = new Array<PriorWorkItemOutputModel>();

            var $inputs = $("#dvCustomFields :input");
            if ($inputs != null) {
                om.SdfList = [];
                let dType: any;
                $inputs.each(function () {
                    var sdf = new Sdf();
                    sdf.SdfId = $(this).prop("id");
                    dType = $(this).attr("data-dtypeid");
                    if (dType === "4") {
                        sdf.SdfValue = $(this).prop("checked");
                    } else {
                        sdf.SdfValue = $(this).val();
                    }
                    om.SdfList.push(sdf);
                });
            }
            this.db.postSdf(om, this);
        }
        //end region sdf fields
    }
}