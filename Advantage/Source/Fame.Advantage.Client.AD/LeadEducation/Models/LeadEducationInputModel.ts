﻿module AD {
    export interface ILeadEducationInputModel {
        Id: string;
        LeadId: string;
        CampusId: string;
        LevelId: string;
        TypeId: string;
        InstitutionId: string;
        InstitutionName: string;
        Major: string;
        FinalGrade: string;
        Gpa: number;
        Rank: number;
        Percentile: number;
        Graduate: boolean;
        GraduationDate: string;
        CertificateOrDegree: string;
        Comments: string;
        UserId: string;
        IsInstitutionAddedToMru: boolean;
    }
}
