﻿module AD {
    export interface ILeadEducationFilterInputModel {
        Id: string;
        CampusId: string;
        LeadId: string;
        StudentId: string;
        UserId: string;
    }
}