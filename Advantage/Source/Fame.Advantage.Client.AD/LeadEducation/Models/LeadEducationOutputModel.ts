﻿/// <reference path="../../../Fame.Advantage.Client.SY/Institution/Models/InstitutionOutputModel.ts" />
module AD {
    export interface ILeadEducationOutputModel {
        Id: string;
        LeadId: string;
        LevelId: string;
        Level: string;
        Institution: SystemInstitution.IInstitutionOutputModel;
        Major: string;
        FinalGrade: string;
        Gpa: number;
        Rank: number;
        Percentile: number;
        Graduate: boolean;
        GraduationDate: string;
        CertificateOrDegree: string;
        Comments: string;
    }
}