declare module AD {
    class LeadQueueDb {
        getInfoFromServer(assingRepId: string, campusId: string, userId: string, command: number, mcontext: LeadQueueVm): JQueryXHR;
        setLeadInMru(campusId: string, userId: string, leadId: any, mcontext: LeadQueueVm): JQueryXHR;
        postLeadNewPhone(phone: IPhone, mcontext: LeadQueueVm): JQueryXHR;
    }
}
