/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
declare module AD {
    class LeadQueueVm {
        db: LeadQueueDb;
        constructor();
        getInfoFromServer(command?: number, assignedId?: string): void;
        commandGotoLeadPage(grid: kendo.ui.Grid, e: any): void;
        savePhone(selectedRow: any, grid: kendo.ui.Grid): void;
        refreshInfoFromServer(): void;
    }
}
