var AD;
(function (AD) {
    var LeadQueueDb = (function () {
        function LeadQueueDb() {
        }
        LeadQueueDb.prototype.getInfoFromServer = function (assingRepId, campusId, userId, command, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XGET_SERVICE_LAYER_QUEUE_COMMAND_GET +
                    "?AssingRepId=" + assingRepId +
                    "&UserId=" + userId +
                    "&CampusID=" + campusId +
                    "&Command=" + command,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        };
        LeadQueueDb.prototype.setLeadInMru = function (campusId, userId, leadId, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XPOST_SERVICE_LAYER_SET_MRU + "?UserId=" + userId + "&CampusId=" + campusId + "&EntityId=" + leadId + "&TypeEntity=4",
                type: 'POST',
                timeout: 20000,
                dataType: 'text'
            });
        };
        LeadQueueDb.prototype.postLeadNewPhone = function (phone, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XPOST_PHONE_SERVICE_LAYER_SET_MRU,
                data: JSON.stringify(phone),
                type: 'POST',
                timeout: 20000,
                dataType: 'json'
            });
        };
        return LeadQueueDb;
    }());
    AD.LeadQueueDb = LeadQueueDb;
})(AD || (AD = {}));
//# sourceMappingURL=LeadQueueDb.js.map