﻿module AD {

    export class LeadQueueDb {
   
        getInfoFromServer(assingRepId: string, campusId: string, userId: string, command: number, mcontext: LeadQueueVm) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_QUEUE_COMMAND_GET +
                "?AssingRepId=" + assingRepId +
                "&UserId=" + userId +
                "&CampusID=" + campusId +
                "&Command=" + command,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }

        /* Set the LEAD in the user MRU */
        setLeadInMru(campusId: string, userId: string, leadId, mcontext: LeadQueueVm) {
            return $.ajax({
                context: mcontext,
                url: XPOST_SERVICE_LAYER_SET_MRU + "?UserId=" + userId + "&CampusId=" + campusId + "&EntityId=" + leadId + "&TypeEntity=4",
                type: 'POST',
                timeout: 20000,
                dataType: 'text'
            });

        }
        /* Send the changes to the LEad phone to server. Return the updated phone string */
        postLeadNewPhone(phone: IPhone, mcontext: LeadQueueVm) {
            return $.ajax({
                context: mcontext,
                url: XPOST_PHONE_SERVICE_LAYER_SET_MRU,
                data: JSON.stringify(phone),
                type: 'POST',
                timeout: 20000,
                dataType: 'json'
            });
        }
    }
}