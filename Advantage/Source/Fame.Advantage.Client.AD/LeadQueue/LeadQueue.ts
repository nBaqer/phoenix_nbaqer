﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />

module AD {

    // ReSharper disable InconsistentNaming
    /* Open the mail task board */
    declare function openTaskRadWindowWithEntity(entityId: string, mod: number);
    // ReSharper restore InconsistentNaming

    export class LeadQueue {

        vm: LeadQueueVm;
        selectedRow: any;

        constructor() {
            try {
                this.vm = new LeadQueueVm();
                var ethis = this;
                // debugger;

                // Phone Edit Windows Pop Up
                var phonew = $("#queueWindowsPhone").kendoWindow(
                    {
                        actions: ["Close"],
                        title: "Edit Phone Number",
                        width: "400px",
                        height: "150px",
                        visible: false,
                        modal: false,
                        resizable: true
                    }).data("kendoWindow") as kendo.ui.Window;
  
                // Grid for Show the Leads in Queue
                var grid = $("#queueGrid").kendoGrid({
                    //toolbar: ["excel", "pdf"],
                    //excel: {
                    //    allPages: true
                    //},
                    //pdf: {
                    //    fileName: "AdvantageLeadsQueue.pdf",
                    //    landscape: true,
                    //    allPages: true
                    //},
                    dataSource: {
                        data: [],
                        pageSize: 15
                    },
                    //height: 600,
                    sortable: true,
                    filterable: false,
                    resizable: true,
                    pageable: true,

                    columns: [
                        { field: "Id", title: "", width: 1 },
                        {
                            field: "FirstName",
                            title: "First Name",
                            attributes: { style: "font-size:11px;" },
                            template: '<a href="\\#" class = "link">#=FirstName# </a>',
                            width: 100
                        },
                        {
                            field: "LastName",
                            title: "Last Name",
                            attributes: { style: "font-size:11px;" }

                        },
                        {
                            field: "Status",
                            title: "Status",
                            attributes: { style: "font-size:11px;" },
                            template: kendo.template($("#queueGridStatusTemplate").html())

                        },
                        {
                            field: "ProgramInterest",
                            title: "Program Interest",
                            attributes: {
                                title: "tooltip",
                                style: "font-size:11px;"
                            }
                        },
                        {
                            field: "Received",
                            title: "Received",
                            attributes: { style: "font-size:11px; text-align:center" },
                            width: 75
                            , sortable: false
                        },
                        {
                            title: "Activity",
                            attributes: { style: "text-align:center;" },
                            columns: [

                                {
                                    field: "LastActivity"
                                    , title: "Last Activity"
                                },
                                {
                                    field: "DateLast"
                                    , title: "Date"
                                    , template: "#=  (DateLast == null)? '' : kendo.toString(kendo.parseDate(DateLast, 'yyyy-MM-dd'), 'MM/dd/yy') #"
                                    , attributes: { style: "font-size:11px; text-align:center" }
                                    , width: 75
                                    , sortable: false
                                }

                                ,
                                {
                                    field: "Age"
                                    , title: "Age"
                                    , attributes: { style: "font-size:11px; text-align:center" }
                                    , width: 65
                                    , sortable: false
                                },
                                {
                                    field: "NextActivity"
                                    , title: "Next Activity",
                                    attributes: { style: "font-size:11px;" }
                                }
                                ,
                                {
                                    field: "DateNext"
                                    , title: "Date"
                                    , template: "#=  (DateNext == null)? '' : kendo.toString(kendo.parseDate(DateNext, 'yyyy-MM-dd'), 'MM/dd/yy') #"
                                    , attributes: { style: "font-size:11px; text-align:center" }
                                    , width: 75
                                    , sortable: false

                                }
                            ]
                        },

                        {
                            field: "Phone",
                            title: "Phone",
                            attributes: {
                                style: "font-size:11px;"
                            }
                            , template: "<span class='cellphone'> #=Phone# <span> "
                            , width: 95
                        },
                        {
                            field: ""
                            , title: "Edit Phone"
                            , template: "<div class='clickpencil'><img src='../images/Edit_bluePencil.gif' /></div>"
                            , width: 50
                        },
                        {
                            field: ""
                            , title: "Email"
                            , template: "<div class='clickemail'><img src='../images/Email2.gif' /></div>"
                            , width: 50
                        },
                        { field: "IsInternational", title: "", width: 1 }
                    ]


                }).data("kendoGrid") as kendo.ui.Grid;

                /*
                 * Tool Tip for column Interest Program in the grid
                 */
                grid.table.kendoTooltip({
                    filter: "td[title]",
                    content: x => {
                        var target = x.target;
                        return $(target).text();
                    }
                });

                $("#queuesavePhone").kendoButton({
                    click: () => {
                        this.vm.savePhone(this.selectedRow, grid);
                    }
                });

                $("#queuecancelPhone").kendoButton({
                    click() {
                        phonew.close();
                    }
                });

                var maskphone = $("#queuePhoneEdit").kendoMaskedTextBox({}).data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox;

                // International Check-boxes change events
                // Do not use lambda notation here!! the this internal this is lost.
                $('#queueInternationalPhone').change(function () {
                    // Get check- box checked value
                    var maskedtextbox = $("#queuePhoneEdit").data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox;
                    var raw = maskedtextbox.raw();
                    if ($(this).is(":checked")) {
                        maskedtextbox.setOptions({ mask: '999999999999999' });
                    } else {
                        maskedtextbox.setOptions({ mask: '(000)-000-0000' });
                    }
                    maskedtextbox.value(raw);
                });

                /* Get the click over columns with class link (in this case FirstName) */
                grid.table.on('click', '.link', e => {
                    this.vm.commandGotoLeadPage(grid, e);
                });
               
                /* Get the click over Edit Phone */
                grid.table.on('click', '.clickpencil', e => {

                    var w: kendo.ui.Window = $("#queueWindowsPhone").data("kendoWindow") as kendo.ui.Window;
                    this.selectedRow = grid.dataItem($(e.currentTarget).closest("tr"));
                    var inter: boolean = this.selectedRow.IsInternational;
                    ($("#queueInternationalPhone").prop("checked", inter));

                    if (inter) {
                        maskphone.setOptions({ mask: "999999999999999" });
                    } else {
                        maskphone.setOptions({ mask: "(999) 000-0000" });
                    }
                    maskphone.value(this.selectedRow.Phone);
                    w.open();
                    w.center();
                });

                /* Get the click over Edit Phone */
                grid.table.on('click', '.clickemail', e => {

                    this.selectedRow = grid.dataItem($(e.currentTarget).closest("tr"));
                    var leadId: string = this.selectedRow.Id;
                    // send as receptor the lead ID (4 mining type Lead)
                    ad.OPEN_TASK_RAD_EMAIL_WINDOW_WITH_ENTITY(leadId, 4);
                });

                /*
                 * Admission Rep drop-down list
                 */
                $("#ddlqueueAdmm").kendoDropDownList({
                    dataSource: [{}],
                    optionLabel: "Select",
                    dataTextField: "Description",
                    dataValueField: "ID",
                    change: function (e) {
                        e.preventDefault();
                        var value = this.value();
                        // Use the value of the widget
                        if (value === "") {
                            var grid = $("#queueGrid").data("kendoGrid") as kendo.ui.Grid;
                            var ds = new kendo.data.DataSource();
                            grid.setDataSource(ds);
                            MasterPage.SHOW_WARNING_WINDOW("Please Select a valid value");
                            return;
                        }
                        var vm = new LeadQueueVm();
                        vm.getInfoFromServer(2, value);
                    }
                });

                // Get the preliminary info from server
                this.vm.getInfoFromServer();

                // This bind the view model with all.-------------------------------------------------------------------------
                kendo.init($("#containerInfoPage"));
                kendo.bind($("#containerInfoPage"), ethis.vm);
            } catch (e) {
                $("body").css("cursor", "default");
                MasterPage.SHOW_ERROR_WINDOW(e.message + "/n" + e.stack);
            }
        }

        myparseDate(data) {
            if (data == null) {
                return "";
            } else {
                return kendo.toString(kendo.parseDate(data, "yyyy-MM-dd"), "MM/dd/yy");
            }
        }
    }
}