﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
module AD {

    declare var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL: string;
    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;

    export class LeadQueueVm {

        db: LeadQueueDb;

        constructor() {
            this.db = new LeadQueueDb();
        }

        /* Get the information needed by the page from server*/
        /* command 0 (get all information)  */
        /* command 1 (get only the admission rep */
        /* command 2 (get only the lead queue */
        /* command 3 (get the number of lead assigned to the actual Admission Rep */
        getInfoFromServer(command: number = 0, assignedId: string = $("#hdnUserId").val()) {

            // Need to pass userId and campusId
            var campusId: string = XMASTER_GET_CURRENT_CAMPUS_ID;

            // Get the user in the page...
            var userId:string = $("#hdnUserId").val();

            // Request the info from server
            this.db.getInfoFromServer(assignedId,campusId, userId, command, this)
                .done(msg => {
                    if (msg != undefined) {

                        try {
                            var dda = $("#ddlqueueAdmm").data("kendoDropDownList") as kendo.ui.DropDownList;

                            // If it is command 2 don't touch the admission combo-box
                            if (command !== 2) {

                                //Assign list of admission rep to combo-box
                                dda.setDataSource(msg.AdmissionRepList);
                                   
                                // see if the user is in the list of admission rep
                                for (let i = 0; i < msg.AdmissionRepList.length; i++) {
                                    if (msg.AdmissionRepList[i].ID === userId) {
                                        dda.select(i+1);
                                    }
                                }
                            }
                            
                            // Analise if the Admission Rep combo box should be enable or not
                            var isshow = msg.QueueInfoObj.ShowAdmissionRep;
                            if (isshow) {
                                dda.enable(true);
                            
                            } else {
                                dda.enable(false);
                            }

                            var grid: kendo.ui.Grid = $("#queueGrid").data("kendoGrid") as kendo.ui.Grid ;
                            var dataSource: kendo.data.DataSource;

                            //Show information in the Grid if exists
                            var infoitems: any = msg.LeadQueueInfoList;
                            if (infoitems !== undefined && infoitems !== null && infoitems.length > 0) {
                                // Show the result in grid
                                dataSource = new kendo.data.DataSource({
                                    data: msg.LeadQueueInfoList,
                                    pageSize: 15

                                });

                            } else {
                                dataSource = new kendo.data.DataSource();
                            }

                            grid.setDataSource(dataSource);

                            // Set the queue quantity
                            let t: string = `( ${dataSource.total().toString()} )`;
                            //$("#queueQuantity").text(t);
                            $("#menuQQuantity").text(t);
                            //$("#menuQQuantity").val(dataSource.total().toString());

                            //Set the Refresh of the last enter new lead
                            var item: any = dataSource.at(0);
                            if (item != undefined) {
                                $("#queueRefresh").text(item.Received);
                            }

                            // Set the method to refresh the queue each predetermined frequency
                            var refresh: number = msg.QueueInfoObj.RefreshQueueInterval * 1000;
                            if (refresh < 30000) {
                                // Protect again a minimal time of call.
                                refresh = 30000;
                            }
                            window.setInterval(this.refreshInfoFromServer, refresh);

                            $("body").css("cursor", "default");

                        } catch (e) {
                            //If errors clear the cache
                            $("body").css("cursor", "default");
                            alert(e.message);
                        }
                    }
                })
                .fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }

        /* 
         *  Put the selected Lead in MRU list and redirect the browser to Lead Info Page
        */
        commandGotoLeadPage(grid: kendo.ui.Grid, e) {
            var campusId: string = XMASTER_GET_CURRENT_CAMPUS_ID;
            var userId: string = $("#hdnUserId").val();
            var dataItem: any = grid.dataItem($(e.currentTarget).closest("tr"));

            this.db.setLeadInMru(campusId, userId, dataItem.Id, this)
                .done(msg => {
                    if (msg != undefined) {

                        try {

                            // Make a page reload
                            sessionStorage.setItem("NewLead", "false");
                            sessionStorage.setItem(XSUB_MENU_SELECTION, XLEADINFO_PAGE);
                            var url = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?resid=170&NewEnt=1&cmpid=" + campusId + "&desc=Queue&mod=AD";
                            window.location.assign(url);

                            $("body").css("cursor", "default");
                        } catch (e) {

                            $("body").css("cursor", "default");
                            alert(e.message);
                        }
                    }
                })
                .fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }

        /*
         * Save the lead phone number if it is changed
         */
        savePhone(selectedRow: any, grid: kendo.ui.Grid) {
            var maskphone = $("#queuePhoneEdit").data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox;
            if (selectedRow.Phone === maskphone.value()) {
                var phonew = $("#queueWindowsPhone").data("kendoWindow") as kendo.ui.Window;
                phonew.close();
                return; // do not save are equals
            }

            // Save. Create entity IPhone to save
            var phone: IPhone = new Phone();
            phone.Position = 1;
            phone.IsForeignPhone = ($("#queueInternationalPhone").prop("checked"));
            phone.Phone = maskphone.raw();
            phone.UserId = $("#hdnUserId").val();
            phone.LeadId = selectedRow.Id;

            this.db.postLeadNewPhone(phone, this)
                .done(msg => {
                    if (msg != undefined) {
                        try {
                            selectedRow.set("Phone", msg.Phone);
                            selectedRow.set("IsInternational", msg.IsForeignPhone);
                            // selectedRow.set("Phone", maskphone.raw());
                            var phonew = $("#queueWindowsPhone").data("kendoWindow") as kendo.ui.Window;
                            phonew.close();
                            $("body").css("cursor", "default");
                        } catch (e) {

                            $("body").css("cursor", "default");
                            MasterPage.SHOW_ERROR_WINDOW(e.message);
                           // alert(e.message);
                        }
                    }

                })
                .fail(msg => {
                    var phonew = $("#queueWindowsPhone").data("kendoWindow") as kendo.ui.Window;
                    phonew.close();
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }

        refreshInfoFromServer() {
            var dd: kendo.ui.DropDownList = $("#ddlqueueAdmm").data("kendoDropDownList") as kendo.ui.DropDownList;
            var selected = dd.value();
            if (selected !== "") {
                // call here the refresh procedure
                var command: number = 2;
                try {
                    $("body").css("cursor", "wait");
                    var db1 = new LeadQueueDb();
                    var userId: string = $("#hdnUserId").val();
                    db1.getInfoFromServer(selected, XMASTER_GET_CURRENT_CAMPUS_ID, userId, command, this)
                        .done(msg => {
                            if (msg != undefined) {
                                try {
                                    var grid: kendo.ui.Grid = $("#queueGrid").data("kendoGrid") as kendo.ui.Grid;
                                    var dataSource: kendo.data.DataSource;

                                    //Show information in the Grid if exists
                                    var infoitems: any = msg.LeadQueueInfoList;
                                    if (infoitems !== undefined && infoitems !== null && infoitems.length > 0) {
                                        // Show the result in grid
                                        dataSource = new kendo.data.DataSource({
                                            data: msg.LeadQueueInfoList,
                                            pageSize: 15
                                        });

                                    } else {
                                        dataSource = new kendo.data.DataSource();
                                    }

                                    grid.setDataSource(dataSource);

                                    // Set the queue quantity
                                    $("#menuQQuantity").text(`( ${dataSource.total().toString()} )`);
  
                                    //Set the Refresh of the last enter new lead
                                    var item: any = dataSource.at(0);
                                    if (item != undefined) {
                                        $("#queueRefresh").text(item.Received);
                                    }

                                    $("body").css("cursor", "default");

                                } catch (e) {
                                    //If errors clear the cache
                                    $("body").css("cursor", "default");
                                    alert(e.message);
                                }
                            }
                        });

                } catch (e) {
                    $("body").css("cursor", "default");
                    alert(e.message);
                }
            }
        }
    }
}