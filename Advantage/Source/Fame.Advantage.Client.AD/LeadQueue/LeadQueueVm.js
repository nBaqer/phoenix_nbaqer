var AD;
(function (AD) {
    var LeadQueueVm = (function () {
        function LeadQueueVm() {
            this.db = new AD.LeadQueueDb();
        }
        LeadQueueVm.prototype.getInfoFromServer = function (command, assignedId) {
            var _this = this;
            if (command === void 0) { command = 0; }
            if (assignedId === void 0) { assignedId = $("#hdnUserId").val(); }
            var campusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            var userId = $("#hdnUserId").val();
            this.db.getInfoFromServer(assignedId, campusId, userId, command, this)
                .done(function (msg) {
                if (msg != undefined) {
                    try {
                        var dda = $("#ddlqueueAdmm").data("kendoDropDownList");
                        if (command !== 2) {
                            dda.setDataSource(msg.AdmissionRepList);
                            for (var i = 0; i < msg.AdmissionRepList.length; i++) {
                                if (msg.AdmissionRepList[i].ID === userId) {
                                    dda.select(i + 1);
                                }
                            }
                        }
                        var isshow = msg.QueueInfoObj.ShowAdmissionRep;
                        if (isshow) {
                            dda.enable(true);
                        }
                        else {
                            dda.enable(false);
                        }
                        var grid = $("#queueGrid").data("kendoGrid");
                        var dataSource;
                        var infoitems = msg.LeadQueueInfoList;
                        if (infoitems !== undefined && infoitems !== null && infoitems.length > 0) {
                            dataSource = new kendo.data.DataSource({
                                data: msg.LeadQueueInfoList,
                                pageSize: 15
                            });
                        }
                        else {
                            dataSource = new kendo.data.DataSource();
                        }
                        grid.setDataSource(dataSource);
                        var t = "[" + dataSource.total().toString() + "]";
                        $("#queueQuantity").text(t);
                        var item = dataSource.at(0);
                        if (item != undefined) {
                            $("#queueRefresh").text(item.Received);
                        }
                        var refresh = msg.QueueInfoObj.RefreshQueueInterval * 1000;
                        if (refresh < 30000) {
                            refresh = 30000;
                        }
                        window.setInterval(_this.refreshInfoFromServer, refresh);
                        $("body").css("cursor", "default");
                    }
                    catch (e) {
                        $("body").css("cursor", "default");
                        alert(e.message);
                    }
                }
            })
                .fail(function (msg) {
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadQueueVm.prototype.commandGotoLeadPage = function (grid, e) {
            var campusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            var userId = $("#hdnUserId").val();
            var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
            this.db.setLeadInMru(campusId, userId, dataItem.Id, this)
                .done(function (msg) {
                if (msg != undefined) {
                    try {
                        sessionStorage.setItem("NewLead", "false");
                        sessionStorage.setItem(AD.XSUB_MENU_SELECTION, AD.XLEADINFO_PAGE);
                        var url = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&NewEnt=1&cmpId=" + campusId;
                        window.location.assign(url);
                        $("body").css("cursor", "default");
                    }
                    catch (e) {
                        $("body").css("cursor", "default");
                        alert(e.message);
                    }
                }
            })
                .fail(function (msg) {
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadQueueVm.prototype.savePhone = function (selectedRow, grid) {
            var maskphone = $("#queuePhoneEdit").data("kendoMaskedTextBox");
            if (selectedRow.Phone === maskphone.raw()) {
                return;
            }
            var phone = new AD.Phone();
            phone.Position = 1;
            phone.IsForeignPhone = ($("#queueInternationalPhone").prop("checked"));
            phone.Phone = maskphone.raw();
            phone.UserId = $("#hdnUserId").val();
            phone.LeadId = selectedRow.Id;
            this.db.postLeadNewPhone(phone, this)
                .done(function (msg) {
                if (msg != undefined) {
                    try {
                        selectedRow.set("Phone", msg.Phone);
                        selectedRow.set("IsInternational", msg.IsForeignPhone);
                        var phonew = $("#queueWindowsPhone").data("kendoWindow");
                        phonew.close();
                        $("body").css("cursor", "default");
                    }
                    catch (e) {
                        $("body").css("cursor", "default");
                        alert(e.message);
                    }
                }
            })
                .fail(function (msg) {
                var phonew = $("#queueWindowsPhone").data("kendoWindow");
                phonew.close();
                ad.SHOW_DATA_SOURCE_ERROR(msg);
            });
        };
        LeadQueueVm.prototype.refreshInfoFromServer = function () {
            var dd = $("#ddlqueueAdmm").data("kendoDropDownList");
            var selected = dd.value();
            if (selected !== "") {
                var command = 2;
                try {
                    $("body").css("cursor", "wait");
                    var db1 = new AD.LeadQueueDb();
                    var userId = $("#hdnUserId").val();
                    db1.getInfoFromServer(selected, XMASTER_GET_CURRENT_CAMPUS_ID, userId, command, this)
                        .done(function (msg) {
                        if (msg != undefined) {
                            try {
                                var grid = $("#queueGrid").data("kendoGrid");
                                var dataSource;
                                var infoitems = msg.LeadQueueInfoList;
                                if (infoitems !== undefined && infoitems !== null && infoitems.length > 0) {
                                    dataSource = new kendo.data.DataSource({
                                        data: msg.LeadQueueInfoList,
                                        pageSize: 15
                                    });
                                }
                                else {
                                    dataSource = new kendo.data.DataSource();
                                }
                                grid.setDataSource(dataSource);
                                $("#queueQuantity").text(dataSource.total().toString());
                                var item = dataSource.at(0);
                                if (item != undefined) {
                                    $("#queueRefresh").text(item.Received);
                                }
                                $("body").css("cursor", "default");
                            }
                            catch (e) {
                                $("body").css("cursor", "default");
                                alert(e.message);
                            }
                        }
                    });
                }
                catch (e) {
                    $("body").css("cursor", "default");
                    alert(e.message);
                }
            }
        };
        return LeadQueueVm;
    }());
    AD.LeadQueueVm = LeadQueueVm;
})(AD || (AD = {}));
//# sourceMappingURL=LeadQueueVm.js.map