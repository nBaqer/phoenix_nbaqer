var AD;
(function (AD) {
    var LeadQueue = (function () {
        function LeadQueue() {
            var _this = this;
            try {
                this.vm = new AD.LeadQueueVm();
                var ethis = this;
                var phonew = $("#queueWindowsPhone").kendoWindow({
                    actions: ["Close"],
                    title: "Edit Phone Number",
                    width: "400px",
                    height: "150px",
                    visible: false,
                    modal: false,
                    resizable: true
                }).data("kendoWindow");
                var grid = $("#queueGrid").kendoGrid({
                    dataSource: {
                        data: [],
                        pageSize: 15
                    },
                    sortable: true,
                    filterable: false,
                    resizable: true,
                    pageable: true,
                    columns: [
                        { field: "Id", title: "", width: 1 },
                        {
                            field: "FirstName",
                            title: "First Name",
                            attributes: { style: "font-size:11px;" },
                            template: '<a href="\\#" class = "link">#=FirstName# </a>',
                            width: 100
                        },
                        {
                            field: "LastName",
                            title: "Last Name",
                            attributes: { style: "font-size:11px;" }
                        },
                        {
                            field: "Status",
                            title: "Status",
                            attributes: { style: "font-size:11px;" },
                            template: kendo.template($("#queueGridStatusTemplate").html())
                        },
                        {
                            field: "ProgramInterest",
                            title: "Program Interest",
                            attributes: {
                                title: "tooltip",
                                style: "font-size:11px;"
                            }
                        },
                        {
                            field: "Received",
                            title: "Received",
                            attributes: { style: "font-size:11px; text-align:center" },
                            width: 75,
                            sortable: false
                        },
                        {
                            title: "Activity",
                            attributes: { style: "text-align:center;" },
                            columns: [
                                {
                                    field: "LastActivity",
                                    title: "Last Activity"
                                },
                                {
                                    field: "DateLast",
                                    title: "Date",
                                    template: "#=  (DateLast == null)? '' : kendo.toString(kendo.parseDate(DateLast, 'yyyy-MM-dd'), 'MM/dd/yy') #",
                                    attributes: { style: "font-size:11px; text-align:center" },
                                    width: 75,
                                    sortable: false
                                },
                                {
                                    field: "Age",
                                    title: "Age",
                                    attributes: { style: "font-size:11px; text-align:center" },
                                    width: 65,
                                    sortable: false
                                },
                                {
                                    field: "NextActivity",
                                    title: "Next Activity",
                                    attributes: { style: "font-size:11px;" }
                                },
                                {
                                    field: "DateNext",
                                    title: "Date",
                                    template: "#=  (DateNext == null)? '' : kendo.toString(kendo.parseDate(DateNext, 'yyyy-MM-dd'), 'MM/dd/yy') #",
                                    attributes: { style: "font-size:11px; text-align:center" },
                                    width: 75,
                                    sortable: false
                                }
                            ]
                        },
                        {
                            field: "Phone",
                            title: "Phone",
                            attributes: {
                                style: "font-size:11px;"
                            },
                            template: "<span class='cellphone'> #=Phone# <span> ",
                            width: 95
                        },
                        {
                            field: "",
                            title: "Edit Phone",
                            template: "<div class='clickpencil'><img src='../images/Edit_bluePencil.gif' /></div>",
                            width: 50
                        },
                        {
                            field: "",
                            title: "Email",
                            template: "<div class='clickemail'><img src='../images/Email2.gif' /></div>",
                            width: 50
                        },
                        { field: "IsInternational", title: "", width: 1 }
                    ]
                }).data("kendoGrid");
                grid.table.kendoTooltip({
                    filter: "td[title]",
                    content: function (x) {
                        var target = x.target;
                        return $(target).text();
                    }
                });
                $("#queuesavePhone").kendoButton({
                    click: function () {
                        _this.vm.savePhone(_this.selectedRow, grid);
                    }
                });
                $("#queuecancelPhone").kendoButton({
                    click: function () {
                        phonew.close();
                    }
                });
                var maskphone = $("#queuePhoneEdit").kendoMaskedTextBox({}).data("kendoMaskedTextBox");
                $('#queueInternationalPhone').change(function () {
                    var maskedtextbox = $("#queuePhoneEdit").data("kendoMaskedTextBox");
                    var raw = maskedtextbox.raw();
                    if ($(this).is(":checked")) {
                        maskedtextbox.setOptions({ mask: '999999999999999' });
                    }
                    else {
                        maskedtextbox.setOptions({ mask: '(000)-000-0000' });
                    }
                    maskedtextbox.value(raw);
                });
                grid.table.on('click', '.link', function (e) {
                    _this.vm.commandGotoLeadPage(grid, e);
                });
                grid.table.on('click', '.clickpencil', function (e) {
                    var w = $("#queueWindowsPhone").data("kendoWindow");
                    _this.selectedRow = grid.dataItem($(e.currentTarget).closest("tr"));
                    var inter = _this.selectedRow.IsInternational;
                    ($("#queueInternationalPhone").prop("checked", inter));
                    if (inter) {
                        maskphone.setOptions({ mask: "999999999999999" });
                    }
                    else {
                        maskphone.setOptions({ mask: "(999) 000-0000" });
                    }
                    maskphone.value(_this.selectedRow.Phone);
                    w.open();
                    w.center();
                });
                grid.table.on('click', '.clickemail', function (e) {
                    _this.selectedRow = grid.dataItem($(e.currentTarget).closest("tr"));
                    var leadId = _this.selectedRow.Id;
                    ad.OPEN_TASK_RAD_EMAIL_WINDOW_WITH_ENTITY(leadId, 4);
                });
                $("#ddlqueueAdmm").kendoDropDownList({
                    dataSource: [{}],
                    optionLabel: "Select",
                    dataTextField: "Description",
                    dataValueField: "ID",
                    change: function (e) {
                        e.preventDefault();
                        var value = this.value();
                        if (value === "") {
                            var grid = $("#queueGrid").data("kendoGrid");
                            var ds = new kendo.data.DataSource();
                            grid.setDataSource(ds);
                            alert('Please Select a valid value');
                            return;
                        }
                        var vm = new AD.LeadQueueVm();
                        vm.getInfoFromServer(2, value);
                    }
                });
                this.vm.getInfoFromServer();
                kendo.init($("#containerInfoPage"));
                kendo.bind($("#containerInfoPage"), ethis.vm);
            }
            catch (e) {
                $("body").css("cursor", "default");
                alert(e.message + "/n" + e.stack);
            }
        }
        LeadQueue.prototype.myparseDate = function (data) {
            if (data == null) {
                return "";
            }
            else {
                return kendo.toString(kendo.parseDate(data, "yyyy-MM-dd"), "MM/dd/yy");
            }
        };
        return LeadQueue;
    }());
    AD.LeadQueue = LeadQueue;
})(AD || (AD = {}));
//# sourceMappingURL=LeadQueue.js.map