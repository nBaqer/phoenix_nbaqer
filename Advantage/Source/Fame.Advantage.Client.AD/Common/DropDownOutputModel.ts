﻿module AD {
    // ReSharper disable InconsistentNaming
    /*
     * This is common to all drop-down.
     */
    export interface IDropDownOutputModel {
        /*
         * Gets or sets the ID (Guid)
         */

        ID: string;

        /*
         * Gets or sets Description
         */
        Description: string;
    }

    /*
     * This is common to all drop-down.
     */
    export class DropDownOutputModel implements IDropDownOutputModel {
        /*
         * Gets or sets the ID (Guid)
         */
        ID: string;

        /*
         * Gets or sets Description
         */
        Description: string;
    }
    // ReSharper restore InconsistentNaming
}
