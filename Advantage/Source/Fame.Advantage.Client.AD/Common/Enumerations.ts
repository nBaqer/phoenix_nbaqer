﻿module AD {
    //the same table as syFieldTypes in database
    export  enum FieldType {
        SmallInt = 2,
        Int = 3,
        Float = 5,
        Money = 6,
        Bit = 11,
        TinyInt = 17,
        UniqueIdentifier = 72,
        Char = 129,
        Decimal = 131,
        DateTime = 135,
        Varchar = 200
    };

    export enum SystemStatus {
        NewLead = 1,
        ApplicationReceived = 2,
        ApplicationNotAccepted = 3,
        InterviewScheduled = 4,
        Interviewed = 5,
        Enrolled = 6,
        FutureStart = 7,
        NoStart = 8,
        CurrentlyAttending = 9,
        LeaveOfAbsence = 10,
        Suspension = 11,
        Dropped = 12,
        Graduated = 14,
        Active = 15,
        InActive = 16,
        DeadLead = 17,
        WillEnrollintheFuture = 18,
        TransferOut = 19,
        AcademicProbation = 20,
        Externship = 22,
        DisciplinaryProbation = 23,
        WarningProbation = 24,
        Imported = 25,
        Duplicated = 26
    }

    export enum ProgramUnitTypes {
        CreditHour = 0,
        ClockHour = 1,
        Program = 2
    };

    // enumeration to declre the type of R2T4 results.
    export enum R2T4ResultType {
        R2T4Result = 0,
        R2T4Override = 1
    }

    export enum ReportDataSourceType {
        /// <summary>
        /// The api.
        /// Use this when the report uses the rest API as Data Source
        /// </summary>
        RestApi = 1,

        /// <summary>
        /// The sql server.
        /// Use this option when the report uses SQL Server as Data Source.
        /// </summary>
        SqlServer = 2
    }

    export enum ReportOutput {
        /// <summary>
        /// The pdf.
        /// </summary>
        Pdf = 1
    }
}