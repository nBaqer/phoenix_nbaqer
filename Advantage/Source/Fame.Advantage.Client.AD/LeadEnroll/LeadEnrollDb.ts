﻿/// <reference path="../../Fame.Advantage.API.Client/API/Request.ts" />
module AD {
    import Request = Api.Request;
    import RequestType = Api.RequestType;
    import RequestParameterType = Api.RequestParameterType;

    export class LeadEnrollDb {
        public calculateGraduationDate(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Get, "v1/Common/GraduationCalculator/GetCalculatedContractedGraduationDate", data, RequestParameterType.QueryString, onResponseCallback);
        }
        public getNewBadgeNumber(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/StudentSummary/GetNewBadgeNumber", data, RequestParameterType.QueryString, onResponseCallback);
        }
        getDdlSource(pageResourceId: number, campusId: string, userId: string, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_ENROLL_DDL + "?PageResourceId=" + pageResourceId + "&CampusId=" + campusId +"&UserId=" + userId, 
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }
        getIpedsRequiredFields(mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_ENROLL_IPEDS ,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }
        getLeadData(leadId: string, campusId: string, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_ENROLL_INFO + "?LeadId=" + leadId + "&CampusId=" + campusId,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }
        getLeadItemInformation(codeItemToGet: string, value: any, campusId: string, mcontext: any) {

            if (value != null) {
                value = value.toLocaleString();
            }

            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_CATALOGS_COMMAND_GET + "?FldName=" + codeItemToGet + "&CampusID=" + campusId + "&AdditionalFilter=" + value,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }
        /* Post the lead information to server(Enroll)*/
        enrollLead(lead: IEnrollOutputModel, context: any) {
            return $.ajax({
                url: XPOST_SERVICE_LAYER_ENROLL_ENROLL_LEAD,
                type: 'POST',
                dataType: 'json',
                timeout: 20000,
                data: JSON.stringify(lead)
            });
        }

        serverSideValidation(lead: IEnrollOutputModel, context: any) {
            return $.ajax({
                url: XPOST_SERVICE_LAYER_ENROLL_Validate,
                type: 'POST',
                dataType: 'json',
                timeout: 20000,
                data: JSON.stringify(lead)
            });
        }

        // Fixed table with the translation information from fldName to name control
        public staticInformationForCaptionRequirement(): ICaptionRequirement[] {
            return [
                { fldName: "PrgVersionTypeId", labelName: "lblProgVerType", controlName: "ddlProgVerType", isDll: true,dll:[] },
                { fldName: "PrgVerId", labelName: "lblProgVersion", controlName: "ddlProgVersion", isDll: true,dll:[] },    
                { fldName: "EnrollDate", labelName: "lblEnrollmentDate", controlName: "txtEnrollmentDate", isDll: false },
                { fldName: "StartDate", labelName: "lblStartDate", controlName: "txtStartDate", isDll: false },
                { fldName: "ContractedGradDate", labelName: "lblContractedGradDate", controlName: "txtContractedGradDate", isDll: false },
                { fldName: "BillingMethodId", labelName: "lblChargingMethod", controlName: "ddlChargingMethod", isDll: true,dll:[] },
                { fldName: "ExpStartDate", labelName: "lblExpStartDate", controlName: "txtExpStartDate", isDll: false },
                { fldName: "TransferHours", labelName: "lblTransferHrs", controlName: "txtTransferHrs", isDll: false, dll: [] },
                { fldName: "Gender", labelName: "lblGender", controlName: "ddlGender", isDll: true, dll: [] },
                { fldName: "MaritalStatus", labelName: "lblMaritalStatus", controlName: "ddlMaritalStatus", isDll: true, dll: [] },
                { fldName: "Nationality", labelName: "lblNationality", controlName: "ddlNationality", isDll: true, dll: [] },
                { fldName: "Citizen", labelName: "lblCitizenship", controlName: "ddlCitizenship", isDll: true, dll: [] },
                { fldName: "Race", labelName: "lblRace", controlName: "ddlRace", isDll: true, dll: [] },
                { fldName: "DependencyTypeId", labelName: "lblDependencyType", controlName: "ddlDependencyType", isDll: true, dll: [] },
                { fldName: "GeographicTypeId", labelName: "lblGeographicType", controlName: "ddlGeographicType", isDll: true, dll: [] },
                { fldName: "HousingId", labelName: "lblHousingType", controlName: "ddlHousingType", isDll: true, dll: [] },
                { fldName: "admincriteriaid", labelName: "lblAdminCriteria", controlName: "ddlAdminCriteria", isDll: true, dll: [] },
                { fldName: "EnrollStateId", labelName: "lblState", controlName: "ddlState", isDll: true, dll: [] },
                { fldName: "ScheduleId", labelName: "lblSchedule", controlName: "ddlSchedule", isDll: true,dll:[] },
                { fldName: "BirthDate", labelName: "lblDob", controlName: "txtDob", isDll: false },
                { fldName: "ShiftId", labelName: "lblShift", controlName: "ddlShift", isDll: true, dll: [] },
                { fldName: "AttendTypeId", labelName: "lblAttendanceType", controlName: "ddlAttendanceType", isDll: true, dll: [] },
                { fldName: "EdLvlId", labelName: "lblEducationLevel", controlName: "ddlEducationLevel", isDll: true, dll: [] },
                { fldName: "TuitionCategoryId", labelName: "lblTuitionCategory", controlName: "ddlTuitionCategory", isDll: true, dll: [] },
                { fldName: "DegCertSeekingId", labelName: "lblDegree", controlName: "ddlDegree", isDll: true, dll: [] },
                { fldName: "FamilyIncomeID", labelName: "lblFamilyIncome", controlName: "ddlFamilyIncome", isDll: true, dll: [] },
                { fldName: "SSN", labelName: "lblSsn", controlName: "txtSsn", isDll: false },
                { fldName: "FAAdvisorId", labelName: "lblFinAidAdvisor", controlName: "ddlFinAidAdvisor", isDll: true, dll: [] },
                { fldName: "CampusId", labelName: "lblCampus", controlName: "ddlCampus", isDll: true, dll: [] },
                { fldName: "AcademicAdvisor", labelName: "lblAcademicAdvisor", controlName: "ddlAcademicAdvisor", isDll: true, dll: [] }
            ];
        }

    }
}