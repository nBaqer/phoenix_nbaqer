﻿module AD {
    export class EnrollOutputModel implements IEnrollOutputModel {
        constructor() {
            this.leadInfo = new LeadInfoPageOutputModel();
        }

        leadInfo: ILeadInfoPageOutputModel;
        entranceInterviewDate: Date;
        isFirstTimeInSchool: boolean;
        isFirstTimePostSecSchool: boolean;
        shiftId: string;
        internationalLead: boolean;
        nationality: string;
        geographicTypeId: string;
        degCertSeekingId: string;
        chargingMethodId: string;
        disableAutoCharge: boolean;
        thirdPartyContract: boolean;
        programVersionType: string;
        requirementMet: boolean;
        expectedStartDate: Date;
        startDate: Date;
        midPointDate: Date;
        contractedGradDate: Date;
        transferDate: Date;
        tuitionCategory: string;
        finAidAdvisor: string;
        academicAdvisor: string;
        badgeNumber: string;
        transferHours: number;
        futureStartStatusMapped: string;
        notMappedStatus: string;
        isDisabled: boolean;
        enrollmentDate: Date;
        studentNumber: string;
        studentStatus: string;
        stuEnrollId: string;
        studentId: string;
        enrollId: string;
        educationLevel: string;
        fullName: string;
        enrollStateId: string;
        //internationalState: boolean;
        //isIpeds:boolean;
    }
}