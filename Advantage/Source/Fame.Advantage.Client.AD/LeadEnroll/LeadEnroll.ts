﻿module AD {
    export class LeadEnroll {
        public enrollValidator: kendo.ui.Validator;
        public viewModel: LeadEnrollVm;

        constructor() {
            try {
                this.viewModel = new LeadEnrollVm();
                this.enrollValidator = MasterPage.ADVANTAGE_VALIDATOR("dvEnrollment");
                this.viewModel.initializeKendoComponents();
                this.viewModel.getPageResources();
                MasterPage.Common.enableDisableBtns(false);
            } catch (e) {
                $("body").css("cursor", "default");
                MasterPage.SHOW_ERROR_WINDOW(e.message + "/n" + e.stack);
            }
            $("#btnEnrollLead")
                .click(e => {
                    if (this.enrollValidator.validate()) {
                        this.viewModel.enrollLead();
                    }
                    else {
                        MasterPage.SHOW_WARNING_WINDOW(X_SAVE_PLEASE_FILL_REQUIRED_FIELDS);
                    }
                });

            $("#btnCancelEnroll")
                .click(() => {
                    this.viewModel.cancelAndGotoLeadPg();
                });

            $("#btnCalcGradDate")
                .click(() => {
                    if ($("#ddlSchedule").data("kendoDropDownList").value() === "" || $("#txtExpStartDate").val() === "") {
                        MasterPage.SHOW_WARNING_WINDOW(
                            "Schedule and expected start date must be provided to calculate graduation date.");
                    } else {
                        this.viewModel.calculateContractedGradDate();
                    }
                });
            $("#btnNewBadgeNumber")
                .click(() => {
                        this.viewModel.getNewBadgeNumber(); 
                });

        }
    }
}