﻿/// <reference path="../../fame.advantage.client.masterpage/commonfunctions.ts" />
module AD {
    // This must be in the html code of control. ...............................
    declare var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL: string;
    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;
    declare var XMASTER_LEAD_IS_CLOCK_HOUR: string;
    declare var XMASTER_PAGE_USER_OPTIONS_USERID: string;

    export class LeadEnrollVm extends kendo.Observable {
        db: LeadEnrollDb;
        filteredSourcedb: ICaptionRequirement[];
        captionAndRequirementdb: ICaptionRequirement[];
        firstName: string;
        lastName: string;
        internationalLead: boolean;
        internationalAdd: boolean;
        public isClockHourProgram: boolean;
        constructor() {
            super();
            this.db = new LeadEnrollDb();
            this.filteredSourcedb = this.db.staticInformationForCaptionRequirement();
            this.captionAndRequirementdb = this.db.staticInformationForCaptionRequirement();
            this.isClockHourProgram = false;
        }
        getPageResources() {
            $("body").css("cursor", "progress");
            var that = this;
            // Get from Server the information
            this.db.getDdlSource(174, XMASTER_GET_CURRENT_CAMPUS_ID, XMASTER_PAGE_USER_OPTIONS_USERID, this).done(msg => {
                if (msg != undefined) {
                    for (var i = 0; i < msg.length; i++) {
                        var fldName: string = msg[i].FldName;

                        for (var x = 0; x < this.captionAndRequirementdb.length; x++) {

                            if (this.captionAndRequirementdb[x].fldName === fldName) {
                                this.captionAndRequirementdb[x].isrequired = msg[i].Required;
                                this.captionAndRequirementdb[x].dll = msg[i].Dll;
                            }
                        }
                    }
                    //check for ipeds and call for required for ipeds
                    this.getIpedsRequiredList();
                }
            }).fail(msg => {
                $("body").css("cursor", "default");
                MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                return;
            });;
        }

        displayData() {
            var leadElement: any = document.getElementById('leadId');
            var leadGuid = leadElement.value;
            var userId: string = $("#hdnUserId").val();

            MasterPage.SHOW_LOADING(true);
            this.db.getLeadData(leadGuid, XMASTER_GET_CURRENT_CAMPUS_ID, this)
                .done(msg => {
                    MasterPage.SHOW_LOADING(false);
                    if (msg != undefined) {
                        try {
                            //load data
                            //textbox
                            $("#txtEnrollmentId").val(msg.EnrollId);
                            $("#txtLeadName").val(msg.FullName);
                            this.firstName = msg.LeadInfo.FirstName;
                            this.lastName = msg.LeadInfo.LastName;
                            if (msg.BadgeNumber != null) {
                                $("#txtBadgeNumber").val(msg.BadgeNumber);
                            }
                            if (msg.TransferHours != null) {
                                $("#txtTransferHrs").val(msg.TransferHours);
                            }
                            if (msg.LeadInfo.SSN != null) {
                                ($("#txtSsn").data("kendoMaskedTextBox") as any).value(msg.LeadInfo.SSN);
                            }
                            //checkbox
                            if (msg.IsFirstTimeInSchool != null) {
                                $("#chkSchoolfirst").prop("checked", msg.IsFirstTimeInSchool);
                            } else {
                                $("#chkSchoolfirst").prop("checked", false);
                            }
                            if (msg.IsFirstTimePostSecSchool != null) {
                                $("#chkSecondaryschool").prop("checked", msg.IsFirstTimePostSecSchool);
                            } else {
                                $("#chkSecondaryschool").prop("checked", false);
                            }
                            //date
                            if (msg.LeadInfo.Dob != null) {
                                ($("#txtDob").data("kendoDatePicker") as any).value(msg.LeadInfo.Dob);
                            }
                            if (msg.EnrollmentDate != null) {
                                ($("#txtEnrollmentDate").data("kendoDatePicker") as any).value(msg.EnrollmentDate);
                            }
                            if (msg.StartDate != null) {
                                ($("#txtStartDate").data("kendoDatePicker") as any).value(msg.StartDate);
                            }
                            if (msg.MidPointDate != null) {
                                ($("#txtMidPointDate").data("kendoDatePicker") as any).value(msg.MidPointDate);
                            }
                            if (msg.ContractedGradDate != null) {
                                ($("#txtContractedGradDate").data("kendoDatePicker") as any).value(msg.ContractedGradDate);
                            }
                            if (msg.TransferDate != null) {
                                ($("#txtTransferDate").data("kendoDatePicker") as any).value(msg.TransferDate);
                            }
                            if (msg.EntranceInterviewDate != null) {
                                ($("#txtEntranceInterviewDt").data("kendoDatePicker") as any).value(msg.EntranceInterviewDate);
                            }
                            //ddl
                            if (msg.LeadInfo.Gender != null) {
                                ($("#ddlGender").data("kendoDropDownList") as any).value(msg.LeadInfo.Gender);
                            }
                            if (msg.LeadInfo.Dependency != null) {
                                ($("#ddlDependencyType").data("kendoDropDownList") as any).value(msg.LeadInfo.Dependency);
                            }
                            if (msg.LeadInfo.MaritalStatus != null) {
                                ($("#ddlMaritalStatus").data("kendoDropDownList") as any).value(msg.LeadInfo.MaritalStatus);
                            }
                            if (msg.LeadInfo.FamilyIncoming != null) {
                                ($("#ddlFamilyIncome").data("kendoDropDownList") as any).value(msg.LeadInfo.FamilyIncoming);
                            }
                            if (msg.LeadInfo.HousingType != null) {
                                ($("#ddlHousingType").data("kendoDropDownList") as any).value(msg.LeadInfo.HousingType);
                            }
                            if (msg.EducationLevel == null) {
                                msg.EducationLevel = msg.LeadInfo.PreviousEducationId;
                            }
                            if (msg.EducationLevel != null) {
                                ($("#ddlEducationLevel").data("kendoDropDownList") as any).value(msg.EducationLevel);
                            }
                            if (msg.LeadInfo.AdminCriteriaId != null) {
                                ($("#ddlAdminCriteria").data("kendoDropDownList") as any).value(msg.LeadInfo.AdminCriteriaId);
                            }
                            if (msg.DegCertSeekingId != null) {
                                ($("#ddlDegree").data("kendoDropDownList") as any).value(msg.DegCertSeekingId);
                            }
                            if (msg.LeadInfo.AttendTypeId != null) {
                                ($("#ddlAttendanceType").data("kendoDropDownList") as any).value(msg.LeadInfo.AttendTypeId);
                            }
                            if (msg.LeadInfo.RaceId != null) {
                                ($("#ddlRace").data("kendoDropDownList") as any).value(msg.LeadInfo.RaceId);
                            }
                            if (msg.Nationality != null) {
                                ($("#ddlNationality").data("kendoDropDownList") as any).value(msg.Nationality);
                            }
                            if (msg.LeadInfo.Citizenship != null) {
                                ($("#ddlCitizenship").data("kendoDropDownList") as any).value(msg.LeadInfo.Citizenship);
                            }
                            if (msg.GeographicTypeId != null) {
                                ($("#ddlGeographicType").data("kendoDropDownList") as any).value(msg.GeographicTypeId);
                            }
                            if (msg.EnrollStateId != null) {
                                let ddlState = $("#ddlState").data("kendoDropDownList") as kendo.ui.DropDownList;
                                ddlState.value(msg.EnrollStateId);
                            }
                            if (msg.LeadInfo.CampusId != null) {
                                ($("#ddlCampus").data("kendoDropDownList") as any).value(msg.LeadInfo.CampusId);
                            }
                            if (msg.LeadInfo.PrgVerId != null) {
                                ($("#ddlProgVersion").data("kendoDropDownList") as any).value(msg.LeadInfo.PrgVerId);
                                if (msg.LeadInfo.ScheduleId != null) {
                                    this.getItemInformation(XMASTER_GET_CURRENT_CAMPUS_ID, "ProgramScheduleId", "ddlSchedule", msg.LeadInfo.PrgVerId, msg.LeadInfo.ScheduleId);
                                }
                                //expected start date
                                var cbexpectedStart = $("#ddlExpectedStart").data("kendoDropDownList") as any;
                                //if (msg.LeadInfo.ExpectedStart != null) {
                                //if (XMASTER_LEAD_IS_CLOCK_HOUR.toLowerCase() === "yes") {
                                if (ad.checkIsClockHr("ddlProgVersion")) {
                                    //$("#txtExpStartDate").css("display", "inline");
                                    //$("#txtExpStartDate").css("width", "auto");
                                    //$("#txtExpStartDate").kendoDatePicker();
                                    //cbexpectedStart.wrapper.hide();
                                    $("#btnCalcGradDate").removeClass("hidden");
                                    $("#dvExpectedStartDate").removeClass("hidden");
                                    $("#txtExpStartDate").attr("required", "required");
                                    $("#dvExpectedStartDateDrop").addClass("hidden");
                                    $("#ddlExpectedStart").removeAttr("required");
                                    if (msg.LeadInfo.ExpectedStart != null &&
                                        msg.LeadInfo.ExpectedStart != undefined) {
                                        ($("#txtExpStartDate").data("kendoDatePicker") as kendo.ui.DatePicker)
                                            .value(kendo.parseDate(msg.LeadInfo.ExpectedStart));
                                    }
                                } else {
                                    //cbexpectedStart.wrapper.show();
                                    //$("#txtExpStartDate").parent().parent().hide();
                                    $("#btnCalcGradDate").addClass("hidden");
                                    $("#dvExpectedStartDate").addClass("hidden");
                                    $("#txtExpStartDate").removeAttr("required");
                                    $("#dvExpectedStartDateDrop").removeClass("hidden");
                                    $("#ddlExpectedStart").attr("required", "required");
                                    // ExpectedStart with Term...
                                    if (msg.LeadInfo.ExpectedStart != null &&
                                        msg.LeadInfo.ExpectedStart != undefined) {
                                        var dat1: Date = kendo.parseDate(msg.LeadInfo.ExpectedStart);
                                        var lookdate: string = dat1.toLocaleString("en-US").split(" ")[0]
                                            .toString();
                                        lookdate = lookdate.replace(",", "");
                                        this.getExpectedLeadItemInformation(XMASTER_GET_CURRENT_CAMPUS_ID,
                                            "ExpectedStart",
                                            "ddlExpectedStart",
                                            "",
                                            lookdate);
                                    } else {
                                        //get the programId
                                        var cbprogramVersion: any =
                                            $("#" + "ddlProgVersion").data("kendoDropDownList") as any;
                                        var dataTable = cbprogramVersion.dataSource.data();
                                        if (dataTable[cbprogramVersion.selectedIndex - 1] !== undefined && dataTable[cbprogramVersion.selectedIndex - 1] !== null) {
                                            var prgId = dataTable[cbprogramVersion.selectedIndex - 1]["ProgramId"];
                                            this
                                                .getExpectedLeadItemInformation(XMASTER_GET_CURRENT_CAMPUS_ID,
                                                "ExpectedStart",
                                                "ddlExpectedStart",
                                                prgId);
                                        }
                                    }
                                }
                            } else {
                                var cbexpectedStarts = $("#ddlExpectedStart").data("kendoDropDownList") as any;
                                //cbexpectedStarts.wrapper.show();
                                //$("#txtExpStartDate").parent().parent().hide();
                                $("#dvExpectedStartDate").addClass("hidden");
                                $("#dvExpectedStartDateDrop").removeClass("hidden");
                            }
                            if (msg.ProgramVersionType != null) {
                                if (msg.ProgramVersionType === 0) {
                                    ($("#ddlProgVerType").data("kendoDropDownList") as any).value("");
                                } else {
                                    ($("#ddlProgVerType").data("kendoDropDownList") as any).value(msg.ProgramVersionType);
                                }
                            }

                            if (msg.ShiftId != null) {
                                ($("#ddlShift").data("kendoDropDownList") as any).value(msg.ShiftId);
                            }
                            if (msg.ChargingMethodId != null) {
                                ($("#ddlChargingMethod").data("kendoDropDownList") as any).value(msg.ChargingMethodId);
                            }
                            if (msg.TuitionCategory != null) {
                                ($("#ddlTuitionCategory").data("kendoDropDownList") as any).value(msg.TuitionCategory);
                            }
                            if (msg.FinAidAdvisor != null) {
                                ($("#ddlFinAidAdvisor").data("kendoDropDownList") as any).value(msg.FinAidAdvisor);
                            }
                            if (msg.AcademicAdvisor != null) {
                                ($("#ddlAcademicAdvisor").data("kendoDropDownList") as any).value(msg.AcademicAdvisor);
                            }
                            if (msg.LeadInfo.IsDisabled != null) {
                                if (msg.LeadInfo.IsDisabled) {
                                    ($("#ddlDisabled").data("kendoDropDownList") as any).value("1");
                                } else {
                                    ($("#ddlDisabled").data("kendoDropDownList") as any).value("0");
                                }
                            }
                            if (msg.DisableAutoCharge != null) {
                                if (msg.DisableAutoCharge) {
                                    ($("#ddlDisableAutoCharge").data("kendoDropDownList") as any).value("1");
                                } else {
                                    ($("#ddlDisableAutoCharge").data("kendoDropDownList") as any).value("0");
                                }
                            }
                            if (msg.ThirdPartyContract != null) {
                                if (msg.ThirdPartyContract) {
                                    ($("#ddlThirdPartyContract").data("kendoDropDownList") as any).value("1");
                                } else {
                                    ($("#ddlThirdPartyContract").data("kendoDropDownList") as any).value("0");
                                }
                            }
                            //state
                            if (msg.LeadInfo.LeadAddress != null) {
                                if (msg.LeadInfo.LeadAddress.IsInternational != null) {
                                    if (msg.LeadInfo.LeadAddress.IsInternational.toString() === "false" ||
                                        msg.LeadInfo.LeadAddress.IsInternational.toString() === "0") {
                                        let cap = $("#lblState").text();
                                        if (!(cap.indexOf("*") > 0)) {
                                            cap = cap + "<span style=color:red>*</span>";
                                            $("#lblState").html(cap);
                                            $("#ddlState").attr("required", "required");
                                        }
                                        this.internationalAdd = false;
                                    } else {
                                        this.internationalAdd = true;
                                        $("#lblState").html("State of Residence");
                                        $("#ddlState").removeAttr("required");

                                    }
                                } else {
                                    let cap = $("#lblState").text();
                                    if (!(cap.indexOf("*") > 0)) {
                                        cap = cap + "<span style=color:red>*</span>";
                                        $("#lblState").html(cap);
                                        $("#ddlState").attr("required", "required");
                                    }
                                    this.internationalAdd = false;
                                }
                            } else {
                                let cap = $("#lblState").text();
                                if (!(cap.indexOf("*") > 0)) {
                                    cap = cap + "<span style=color:red>*</span>";
                                    $("#lblState").html(cap);
                                    $("#ddlState").attr("required", "required");
                                }

                                this.internationalAdd = false;
                            }

                            if (msg.InternationalLead != null) {
                                if (msg.InternationalLead
                                    .toString() === "false" || msg.InternationalLead.toString() === "0") {
                                    let cap = $("#lblSsn").text();
                                    cap = cap + "<span style=color:red>*</span>";
                                    $("#lblSsn").html(cap);
                                    $("#txtSsn").attr("required", "required");
                                    this.internationalLead = false;
                                } else {
                                    this.internationalLead = true;
                                }
                            } else {
                                this.internationalLead = false;
                            }
                            if (msg.DisableEnrolLeadStatus) {
                                MasterPage.SHOW_WARNING_WINDOW("The Lead is in enrolled status.");
                                $("#dvBlocker").show();
                            } else if (!msg.RequirementMet) {
                                //MasterPage.SHOW_WARNING_WINDOW_PROMISE("Requirements have not been met for enrollment.");
                                $.when(MasterPage.SHOW_WARNING_WINDOW_PROMISE("<b>" + msg.FullName + "</b> has not satisfied all admissions requirements and <b>is not eligible for enrollment.</b>"))
                                    .then(confirmed => {
                                        if (confirmed) {
                                            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/leadRequirements.aspx?resid=826&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Requirements&mod=AD");
                                        }
                                    });
                                $("#popupWindow .confirm_yes").val("Review Requirements");
                                $("#popupWindow .confirm_yes").css("width", "auto");
                                $("#dvBlocker").show();
                            } else if (!msg.IsValidLead) {
                                if (msg.IsDuplicateSsn) {
                                    $.when(MasterPage.SHOW_WARNING_WINDOW_PROMISE("<b>" + msg.FullName + "</b> has a duplicate Ssn number with other lead/student. Please resolve duplicates in order to proceed with enrollment.</b>"))
                                        .then(confirmed => {
                                            if (confirmed) {
                                                window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/leadassignment.aspx?resid=823&mod=AD&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Lead Assignment&mod=AD");
                                            }
                                        });
                                    $("#popupWindow .confirm_yes").val("Review Duplicates");
                                } else {
                                    $.when(MasterPage.SHOW_WARNING_WINDOW_PROMISE("<b>" + msg.FullName + "</b> is missing required lead data and is <b>not ready for enrollment.</b>"))
                                        .then(confirmed => {
                                            if (confirmed) {
                                                window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/ALeadInfoPage.aspx?resid=170&mod=AD&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=View Lead&Validate=true");
                                            }
                                        });
                                    $("#popupWindow .confirm_yes").val("Review Lead Page");
                                }


                                $("#popupWindow .confirm_yes").css("width", "auto");
                                $("#dvBlocker").show();
                            } else if (msg.FutureStartStatusMapped === null || msg.FutureStartStatusMapped === undefined) {
                                MasterPage.SHOW_WARNING_WINDOW("The lead cannot be enrolled as none of the school defined status codes are mapped to future start system status.");
                                $("#dvBlocker").show();
                            }
                            else if (msg.NotMappedStatus != null) {
                                MasterPage.SHOW_WARNING_WINDOW("The lead cannot be enrolled as the Future Start status has been currently mapped to " + msg.NotMappedStatus + ".");
                                $("#dvBlocker").show();
                            }
                            else {
                                $("#dvBlocker").hide();
                            }
                        } catch (e) {
                            $("body").css("cursor", "default");
                            MasterPage.SHOW_ERROR_WINDOW(e.message);
                        }
                    }
                }).fail(err => {
                    MasterPage.SHOW_LOADING(false);
                    MasterPage.SHOW_DATA_SOURCE_ERROR(err);
                });
        }

        enrollLead() {
            var lead: IEnrollOutputModel = new EnrollOutputModel();
            var leadElement: any = document.getElementById('leadId');
            lead.leadInfo.ModUser = XMASTER_PAGE_USER_OPTIONS_USERID;
            lead.leadInfo.LeadId = leadElement.value;
            lead.leadInfo.FirstName = this.firstName;
            lead.leadInfo.LastName = this.lastName;
            lead.internationalLead = this.internationalLead;
            lead.leadInfo.Age = (ad.GET_AGE(($("#txtDob").data("kendoDatePicker") as kendo.ui.DatePicker).value())).toString();
            lead.leadInfo.LeadAddress.IsInternational = this.internationalAdd;
            //lead.isIpeds = this.isIped;
            lead.enrollId = $("#txtEnrollmentId").val();
            lead.badgeNumber = $("#txtBadgeNumber").val();
            lead.leadInfo.CampusId = ($("#ddlCampus").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.leadInfo.Gender = ($("#ddlGender").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.leadInfo.FamilyIncoming = ($("#ddlFamilyIncome").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.leadInfo.HousingType = ($("#ddlHousingType").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.leadInfo.AdminCriteriaId = ($("#ddlAdminCriteria").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.educationLevel = ($("#ddlEducationLevel").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.degCertSeekingId = ($("#ddlDegree").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.leadInfo.AttendTypeId = ($("#ddlAttendanceType").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.leadInfo.RaceId = ($("#ddlRace").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.leadInfo.Citizenship = ($("#ddlCitizenship").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.leadInfo.PrgVerId = ($("#ddlProgVersion").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            var cbprogramVersion: any = $("#" + "ddlProgVersion").data("kendoDropDownList") as any;
            var dataTable = cbprogramVersion.dataSource.data();
            lead.leadInfo.ProgramId = dataTable[cbprogramVersion.selectedIndex - 1]["ProgramId"];
            lead.leadInfo.AreaId = dataTable[cbprogramVersion.selectedIndex - 1]["AreaId"];
            if (($("#ddlProgVerType").data("kendoDropDownList") as kendo.ui.DropDownList).value() === "") {
                lead.programVersionType = "0";
            } else {
                lead.programVersionType = ($("#ddlProgVerType").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            }
            lead.startDate = ($("#txtStartDate").data("kendoDatePicker") as kendo.ui.DatePicker).value();
            lead.expectedStartDate = ad.processExpectedStart("ddlProgVersion", "txtExpStartDate", "ddlExpectedStart"); //this.processExpectedStart();
            //lead.expectedStartDate = ($("#txtExpStartDate").data("kendoDatePicker") as kendo.ui.DatePicker).value();
            lead.contractedGradDate = ($("#txtContractedGradDate").data("kendoDatePicker") as kendo.ui.DatePicker).value();
            lead.enrollmentDate = ($("#txtEnrollmentDate").data("kendoDatePicker") as kendo.ui.DatePicker).value();
            lead.midPointDate = ($("#txtMidPointDate").data("kendoDatePicker") as kendo.ui.DatePicker).value();
            lead.transferDate = ($("#txtTransferDate").data("kendoDatePicker") as kendo.ui.DatePicker).value();
            lead.entranceInterviewDate = ($("#txtEntranceInterviewDt").data("kendoDatePicker") as kendo.ui.DatePicker).value();
            lead.tuitionCategory = ($("#ddlTuitionCategory").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.transferHours = parseFloat($("#txtTransferHrs").val());
            lead.leadInfo.Dob = ($("#txtDob").data("kendoDatePicker") as kendo.ui.DatePicker).value();
            var ssn = ($("#txtSsn").data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox).value();
            ssn = ssn.replace(/[^0-9]/g, "");
            lead.leadInfo.SSN = ssn;
            lead.leadInfo.Dependency = ($("#ddlDependencyType").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.leadInfo.MaritalStatus = ($("#ddlMaritalStatus").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.nationality = ($("#ddlNationality").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.geographicTypeId = ($("#ddlGeographicType").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.enrollStateId = ($("#ddlState").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.leadInfo.ScheduleId = ($("#ddlSchedule").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.shiftId = ($("#ddlShift").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.chargingMethodId = ($("#ddlChargingMethod").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.finAidAdvisor = ($("#ddlFinAidAdvisor").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            lead.academicAdvisor = ($("#ddlAcademicAdvisor").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            if (($("#ddlDisabled").data("kendoDropDownList") as kendo.ui.DropDownList).value() === "1") {
                lead.isDisabled = true;
            } else {
                lead.isDisabled = false;
            }
            if (this.isChargingMethodPaymentPeriod()) {
                if (($("#ddlDisableAutoCharge").data("kendoDropDownList") as kendo.ui.DropDownList).value() === "1") {
                    lead.disableAutoCharge = true;
                } else {
                    lead.disableAutoCharge = false;
                }
            } else {
                lead.disableAutoCharge = false;
            }

            lead.thirdPartyContract = ($("#ddlThirdPartyContract").data("kendoDropDownList") as kendo.ui.DropDownList).value() === "1";

            lead.isFirstTimeInSchool = ($("#chkSchoolfirst").prop("checked"));
            lead.isFirstTimePostSecSchool = ($("#chkSecondaryschool").prop("checked"));
            try {
                //var isvalidatedData = this.db.serverSideValidation(lead, this).done(res => {
                //if (res != undefined) {
                //if (res.toString() !== "good") {
                //    MasterPage.SHOW_ERROR_WINDOW(res.toString());
                //} else {
                // Open progress panel
                MasterPage.Common.displayLoading(document.body, true);
                var result = this.db.enrollLead(lead, this).done(msg => {
                    if (msg != undefined) {
                        if (msg.toString() === "enrolled") {
                            MasterPage.SHOW_INFO_WINDOW("Lead successfully enrolled.");
                            this.updateLeadInfoBar();
                            $("#dvBlocker").show();
                        } else {
                            MasterPage.SHOW_DATA_SOURCE_ERROR(msg.toString());
                        }
                    }
                })
                    .always(() => {

                        // Close progress panel
                        MasterPage.Common.displayLoading(document.body, false);
                    })
                    .fail(err => {
                        if (err.status === 500) {
                            MasterPage.SHOW_ERROR_WINDOW("Server returned an undefined error");//todo display the actual error thrown by SP
                        } else {
                            MasterPage.SHOW_DATA_SOURCE_ERROR(err);
                        }
                    });
                // }
                //}
                //}).fail(err => { MasterPage.SHOW_DATA_SOURCE_ERROR(err); });


            } catch (e) {
                $("body").css("cursor", "default");
                MasterPage.SHOW_ERROR_WINDOW(e.message);
            }

        }
        /* Create Caption and requirement for all register 
         * fields as resources in the database
         */
        configureRequired(db: ICaptionRequirement[]) {
            var caption: string;
            for (var i = 0; i < db.length; i++) {
                var label = $("#" + db[i].labelName);
                if (label != undefined) {

                    caption = label.text();
                    if (db[i].isrequired) {
                        caption = caption + "<span style=color:red>*</span>";
                        label.html(caption);
                    }
                }
                var control = $("#" + db[i].controlName);
                if (control != undefined) {
                    if (db[i].isrequired) {
                        control.attr("required", "required");
                    }
                }
            }
        }
        /*
     * Fill the filtered data DLL information. This array has the items for ddl
     * filtered by the campusId.
     */
        fillFilteredDatadb(campusId: string) {

            for (var i = 0; i < this.captionAndRequirementdb.length; i++) {
                if (this.captionAndRequirementdb[i].dll == undefined || this.captionAndRequirementdb[i].dll.length === 0) {
                    continue;
                }
                this.filteredSourcedb[i].dll = []; // Clean the filtered data
                for (var x = 0; x < this.captionAndRequirementdb[i].dll.length; x++) {

                    if (this.captionAndRequirementdb[i].dll[x].CampusesIdList != undefined
                        && this.captionAndRequirementdb[i].dll[x].CampusesIdList.length > 0) {

                        var list = this.captionAndRequirementdb[i].dll[x].CampusesIdList;
                        for (let j = 0; j < list.length; j++) {

                            if (list[j].CampusId === campusId) {
                                this.filteredSourcedb[i].dll.push(this.captionAndRequirementdb[i].dll[x]);
                                break;
                            }
                        }
                    }
                }
            }
        }

        initializeKendoComponents() {
            var ctlName: string;
            var that = this;
            var pos;
            for (var i = 0; i < this.captionAndRequirementdb.length; i++) {
                if (this.captionAndRequirementdb[i].isDll) {
                    ctlName = this.captionAndRequirementdb[i].controlName;
                    if (!(ctlName === "ddlCampus" || ctlName === "ddlProgVersion" || ctlName === "ddlChargingMethod")) {
                        $("#" + ctlName)
                            .kendoDropDownList({
                                optionLabel: "Select",
                                dataTextField: "Description",
                                dataValueField: "ID"
                            });
                    }

                }
            }
            $("#ddlDisabled").kendoDropDownList({
                dataSource: [
                    { text: "Select", value: "-1" },
                    { text: "Yes", value: "1" },
                    { text: "No", value: "0" }
                ],
                dataTextField: "text",
                dataValueField: "value"
            });
            $("#ddlDisableAutoCharge").kendoDropDownList({
                dataSource: [
                    { text: "No", value: "0" },
                    { text: "Yes", value: "1" }
                ],
                dataTextField: "text",
                dataValueField: "value"
            });
            $("#ddlThirdPartyContract").kendoDropDownList({
                dataSource: [
                    { text: "No", value: "0" },
                    { text: "Yes", value: "1" }
                ],
                dataTextField: "text",
                dataValueField: "value"
            });
            $("#txtDob").kendoDatePicker();
            $("#txtEnrollmentDate").kendoDatePicker();
            $("#txtExpStartDate").kendoDatePicker();
            //$("#txtExpStartDate").css("display", "inline");
            $("#txtStartDate").kendoDatePicker();
            $("#txtMidPointDate").kendoDatePicker();
            $("#txtContractedGradDate").kendoDatePicker();
            $("#txtTransferDate").kendoDatePicker();
            $("#txtEntranceInterviewDt").kendoDatePicker();
            $("#txtSsn").kendoMaskedTextBox({
                mask: "000-00-0000",
                clearPromptChar: false
            });

            //if (XMASTER_LEAD_IS_CLOCK_HOUR.toLowerCase() === "yes") {
            //    $("#ExpectedStart").kendoDatePicker();
            //    $("#ExpectedStart").attr("data-validation", "date");
            //    $("#txtExpStartDate").attr("maxlength", "12");
            //} else {
            $("#ddlExpectedStart").kendoDropDownList({
                dataSource: [{}],
                optionLabel: "Select",
                dataTextField: "Description",
                dataBound: function () {
                    $("#ddlExpectedStart").data("kendoDropDownList").list.width("auto");
                    $("#ddlExpectedStart").data("kendoDropDownList").list.css("white-space", "nowrap");
                },
                dataValueField: "ID"
            });
            //}

            var campus = $("#ddlCampus").kendoDropDownList({
                dataSource: [],
                dataTextField: "Description",
                dataValueField: "ID",
                change: () => {
                    var campusId = ($("#ddlCampus").data("kendoDropDownList") as any).value();
                    var programgrp = $("#ddlProgVersion").data("kendoDropDownList") as any;
                    var cbschedule = $("#ddlSchedule").data("kendoDropDownList") as any;
                    // if (!(XMASTER_LEAD_IS_CLOCK_HOUR === "yes")) {
                    var cbexpectedStart = $("#ddlExpectedStart").data("kendoDropDownList") as any;
                    cbexpectedStart.setDataSource(new kendo.data.DataSource());
                    //}
                    //programgrp.setDataSource(new kendo.data.DataSource());
                    //cbschedule.setDataSource(new kendo.data.DataSource());
                    this.fillFilteredDatadb(campusId);
                    this.refreshCampusInDll(campusId);

                    ////reload the program version
                    //var selectedValue = ($("#ddlCampus").data("kendoDropDownList") as any).value();
                    //if (selectedValue !== "") {
                    //    this.getItemInformation(selectedValue, "PrgVerId", "ddlProgVersion", selectedValue);
                    //} else {
                    //    var cbprogramLabel = $("#ddlProgVersion").data("kendoDropDownList") as any;
                    //    cbprogramLabel.setDataSource(new kendo.data.DataSource());
                    //}
                }
            }).data("kendoDropDownList") as any;

            $("#ddlProgVersion").kendoDropDownList({
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID",
                //dataSource: [],
                change: () => {
                    var campusId = ($("#ddlCampus").data("kendoDropDownList") as any).value();
                    //Reload program version
                    var cbprogramVersion = $("#ddlProgVersion").data("kendoDropDownList") as any;
                    var selectedValue = cbprogramVersion.value();
                    if (selectedValue !== "") {
                        this.getItemInformation(campusId, "ProgramScheduleId", "ddlSchedule", selectedValue);
                    }
                    //Reload Expected Start
                    //if (!(XMASTER_LEAD_IS_CLOCK_HOUR.toLowerCase() === "yes")) {
                    //    this.getExpectedLeadItemInformation(campusId, "ExpectedStart", "txtExpStartDate", "");
                    //}
                    var cbexpectedStart = $("#ddlExpectedStart").data("kendoDropDownList") as any;
                    if (ad.checkIsClockHr("ddlProgVersion")) {
                        $("#btnCalcGradDate").removeClass("hidden");

                        $("#dvExpectedStartDate").removeClass("hidden");
                        $("#txtExpStartDate").attr("required", "required");
                        ($("#txtExpStartDate").data("kendoDatePicker") as kendo.ui.DatePicker).value("");
                        $("#dvExpectedStartDateDrop").addClass("hidden");
                        $("#ddlExpectedStart").removeAttr("required");
                    } else {
                        $("#btnCalcGradDate").addClass("hidden");
                        $("#dvExpectedStartDate").addClass("hidden");
                        $("#dvExpectedStartDateDrop").removeClass("hidden");
                        $("#txtExpStartDate").removeAttr("required");
                        $("#ddlExpectedStart").attr("required", "required");
                        var dataTable = cbprogramVersion.dataSource.data();
                        var prgId = dataTable[cbprogramVersion.selectedIndex - 1]["ProgramId"];
                        this
                            .getExpectedLeadItemInformation(XMASTER_GET_CURRENT_CAMPUS_ID,
                            "ExpectedStart",
                            "ddlExpectedStart",
                            prgId);
                    }
                }
            });

            $("#ddlChargingMethod").kendoDropDownList({
                optionLabel: "Select",
                dataTextField: "Description",
                dataValueField: "ID",
                //dataSource: [],
                change: () => {

                    if (this.isChargingMethodPaymentPeriod()) {
                        $("#disableAutoChargeContainer").show();

                    } else {
                        $("#disableAutoChargeContainer").hide();
                    }

                }

            });
        };

        isChargingMethodPaymentPeriod() {
            var ddl = $("#ddlChargingMethod").data("kendoDropDownList") as any;
            var chargeMethodId = ddl.value();

            var data = ddl.dataSource.data();
            var chargeMethodCode: string = "";

            $.each(data,
                (idx, record) => {
                    if (record["ID"] == chargeMethodId) {

                        chargeMethodCode = record["Code"];
                        return;
                    }
                });

            if (chargeMethodCode === "3") {
                return true;
            }
            return false;
        }
        //processExpectedStart(): Date {
        //    var expectedStart: Date;
        //    if (XMASTER_LEAD_IS_CLOCK_HOUR.toLowerCase() === 'yes') {
        //        expectedStart = ($("#txtExpStartDate").data("kendoDatePicker") as kendo.ui.DatePicker).value();
        //    } else {
        //        // ExpectedStart with Term...
        //        var description = $("#txtExpStartDate").data("kendoDropDownList").text();
        //        if (description == null || description === "" || description === X_SELECT_ITEM) {
        //            expectedStart = null;
        //        } else {
        //            var descriptdate: string = description.split("]")[0].slice(1).toString().trim();
        //            expectedStart = new Date(descriptdate);
        //        }
        //    }
        //    return expectedStart;
        //}
        /* Get the expected Start of the Lead */
        getExpectedLeadItemInformation(campusId: string, fldName: string, targetDropDown: string, value: Object, setting: string = null) {

            // Request the info from server
            this.db.getLeadItemInformation(fldName, value, campusId, this)
                .done(msg => {
                    if (msg != undefined) {
                        // Enter the information in the controls
                        var cbExpected: kendo.ui.DropDownList = $("#" + targetDropDown).data("kendoDropDownList") as any;

                        // Set the data source
                        cbExpected.setDataSource(msg);

                        // Optional set the value of the filled drop down
                        if (setting != null) {

                            // Get the list of drop down
                            var list = cbExpected.dataSource.data();
                            if (list != null) {

                                // Look up through the list to get the item that contain the date
                                for (var i = 0; i < list.length; i++) {
                                    var descriptdate: string = list[i].Description.split("]")[0].slice(1).toString().trim();
                                    if (descriptdate.localeCompare(setting) === 0) {

                                        // Enter the value
                                        cbExpected.value(list[i].ID);
                                        break;
                                    }
                                }
                            }
                        } else {
                            cbExpected.value("");
                        }
                        // Resize the drop down to avoid wrap
                        //    this.resizeDropDown(cbExpected);
                    }
                })
                .fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }

        /* Manage changes of a specific drop down it is used to manage the user change in cascade drop down */
        getItemInformation(campusId: string, fldName: string, targetDropDown: string, value: Object, setting: string = null) {
            // Request the info from server
            this.db.getLeadItemInformation(fldName, value, campusId, this)
                .done(msg => {
                    if (msg != undefined) {
                        // Enter the information in the controls
                        var itemdd: kendo.ui.DropDownList = $("#" + targetDropDown).data("kendoDropDownList") as any;

                        // Set the data source
                        itemdd.setDataSource(msg);
                        if (setting != null) {
                            itemdd.value(setting);
                        }
                    }
                })
                .fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }

        refreshCampusInDll(campusId: string) {
            // Clean Program, Program Version and Expected Start and schedule
            this.initializeCascadeControls(campusId);
            var cbschedule: kendo.ui.DropDownList = $("#ddlSchedule").data("kendoDropDownList") as any;

            cbschedule.setDataSource(new kendo.data.DataSource());

            //if (!(XMASTER_LEAD_IS_CLOCK_HOUR.toLowerCase() === "yes")) {
            var cbexpectedStart = $("#ddlExpectedStart").data("kendoDropDownList") as any;
            cbexpectedStart.setDataSource(new kendo.data.DataSource());
            //}
        }
        //fill the specific ddl with filtered campus data
        initializeCascadeControls(campusId: string) {
            var prgVersion: kendo.ui.DropDownList = $("#ddlProgVersion ").data("kendoDropDownList") as any;
            prgVersion.setDataSource(this.getFilteredDropDownListObject("ddlProgVersion"));

        }
        getFilteredDropDownListObject(controlName: string): any {
            for (var i = 0; i < this.captionAndRequirementdb.length; i++) {
                if (this.filteredSourcedb[i].controlName === controlName) {
                    return this.filteredSourcedb[i].dll;
                }
            }
            return null;
        }

        initializeControls() {

            var ctrl;
            for (var i = 0; i < this.captionAndRequirementdb.length; i++) {
                if (this.captionAndRequirementdb[i].isDll) {
                    if (this.filteredSourcedb[i].dll != null && this.filteredSourcedb[i].dll !== [] && this.filteredSourcedb[i].dll != undefined && this.filteredSourcedb[i].dll.length > 0) {
                        ctrl = $("#" + this.captionAndRequirementdb[i].controlName).data("kendoDropDownList") as any;
                        if (ctrl != null) {
                            ctrl.setDataSource(this.filteredSourcedb[i].dll);
                        }
                    } else if (this.captionAndRequirementdb[i].dll != null && this.captionAndRequirementdb[i].dll !== [] && this.captionAndRequirementdb[i].dll != undefined && this.captionAndRequirementdb[i].dll.length > 0) {
                        ctrl = $("#" + this.captionAndRequirementdb[i].controlName).data("kendoDropDownList") as any;
                        if (ctrl != null) {
                            ctrl.setDataSource(this.captionAndRequirementdb[i].dll);
                        }
                    }
                }
            }
            var cbschedule = $("#ddlSchedule").data("kendoDropDownList") as any;
            cbschedule.setDataSource(new kendo.data.DataSource());
        }

        initializePage() {
            var campusId: string = MasterPage.GET_QUERY_STRING_PARAMETER_BY_NAME("cmpid");

            this.fillFilteredDatadb(campusId);
            this.initializeControls();
            this.configureRequired(this.captionAndRequirementdb);
            this.displayData();
        }

        getIpedsRequiredList() {
            try {
                this.db.getIpedsRequiredFields(this)
                    .done(msg => {
                        if (msg != undefined) {
                            for (var i = 0; i < msg.length; i++) {
                                var fldName: string = msg[i].FldName;

                                for (var x = 0; x < this.captionAndRequirementdb.length; x++) {

                                    if (this.captionAndRequirementdb[x].fldName === fldName) {
                                        this.captionAndRequirementdb[x].isrequired = msg[i].Required;
                                    }
                                }
                            }
                        }
                        this.initializePage();
                    }).fail(
                    err => { MasterPage.SHOW_DATA_SOURCE_ERROR(err); });
            } catch (e) {
                $("body").css("cursor", "default");
                MasterPage.SHOW_ERROR_WINDOW(e.message);
            }
        }

        cancelAndGotoLeadPg() {
            $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure to cancel?"))
                .then(confirmed => {
                    if (confirmed) {
                        sessionStorage.setItem(XLEAD_NEW, "false");
                        window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?resid=170&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&mod=AD");
                    }
                });
        }

        updateLeadInfoBar() {
            $("body").css("cursor", "progress");

            var leadElement: any = document.getElementById('leadId');
            // Request the info from server
            let leadIndodb: LeadInfoBarVm = new LeadInfoBarVm();
            leadIndodb.getLeadInfoFromServer(leadElement.value);
        }

        calculateContractedGradDate() {
            let data = { scheduleId: $("#ddlSchedule").data("kendoDropDownList").value(), startDate: $("#txtExpStartDate").val(), campusId: XMASTER_GET_CURRENT_CAMPUS_ID }
            this.db.calculateGraduationDate(data, (response: any) => {
                if (response !== null && response !== undefined) {
                    if (response.resultStatus === 0) {
                        ($("#txtContractedGradDate").data("kendoDatePicker") as any).value(response.asString);
                    } else {
                        MasterPage.SHOW_ERROR_WINDOW(response.resultStatusMessage);
                    }
                }
            });
        }
        getNewBadgeNumber() {
            let data = { campusId: XMASTER_GET_CURRENT_CAMPUS_ID }
            this.db.getNewBadgeNumber(data, (response: any) => {
                if (response !== null && response !== undefined) {
                    $("#txtBadgeNumber").val(response.result);
                }

            });
        }
    }
}