﻿module ad {


    declare var XMASTER_GET_BASE_URL: string;
    declare var $find: (input: string) => any;

    ///*
    // * Get a query string parameter
    // */
    //export function GET_QUERY_STRING_PARAMETER_BY_NAME(name: string) {
    //    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    //    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    //        results = regex.exec(location.search);
    //    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    //}


    /*
     * Calculate the Age based in birth data as input
     * input: birth date
     */
    export function GET_AGE(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
    //takes the program dropdown list ID name defined in the page and returns boolean if the program is clock hour or not
    export function checkIsClockHr(ddlProgramName) {
        var cbprogramLabel: any = $("#" + ddlProgramName).data("kendoDropDownList") as any;
        var selectedProg = cbprogramLabel.value();
        var dataTable = cbprogramLabel.dataSource.data();
        if (selectedProg != null && selectedProg !== "") {
            var calendartype = dataTable[cbprogramLabel.selectedIndex - 1]["CalendarType"];
            if (calendartype.toLowerCase() === "clock hour") {
                this.isClockHourProgram = true;
            } else {
                this.isClockHourProgram = false;
            }
        } else {
            this.isClockHourProgram = false;
        }
        return this.isClockHourProgram;
    }
    //takes the id name of program drop down, expected start date picker, expected start ddl and returns expected start date
    export function processExpectedStart(ddlProgramName,dpExpectedStart,ddlExpectedstart) {
        var expectedStart: Date;
        //if (XMASTER_LEAD_IS_CLOCK_HOUR === 'Yes') {
        //if (this.checkIsClockHr()) {
        if (ad.checkIsClockHr(ddlProgramName)) {
            expectedStart = ($("#" + dpExpectedStart).data("kendoDatePicker") as kendo.ui.DatePicker).value();
        } else {
            // ExpectedStart with Term...
            var description = $("#" + ddlExpectedstart).data("kendoDropDownList").text();
            if (description == null || description === "" || description === AD.X_SELECT_ITEM) {
                expectedStart = null;
            } else {
                var descriptdate: string = description.split("]")[0].slice(1).toString().trim();
                expectedStart = new Date(descriptdate);
            }
        }
        return expectedStart;
    }
    /// entityId: the entity to populate the email receptor
    /// mod: the type of entity 4: Lead 
    /// only Lead as type of entity is programming!!!!
    export function OPEN_TASK_RAD_EMAIL_WINDOW_WITH_ENTITY(entityId, mod) {
        var oWnd: any = $find('radPop');
        oWnd.setSize(800, 780);
        oWnd.setUrl(XMASTER_GET_BASE_URL + '/defaultTMAddNew.html?addnew=true&entityId=' + entityId + "&mod=" + mod.toString());
        oWnd.show();
    }

    export function OPEN_TASK_RAD_TASK_WINDOW_WITH_ENTITY(entityId, mod) {
        var oWnd: any = $find('radPop');
        oWnd.setSize(800, 780);
        oWnd.setUrl(XMASTER_GET_BASE_URL + '/defaultTM.html?addnew=true&entityId=' + entityId + "&mod=" + mod.toString());
        oWnd.show();
    }

    export function OPEN_TASK_RAD_SCHEDULE_WINDOW_WITH_ENTITY(entityId, mod) {
        var oWnd: any = $find('radPop');
        oWnd.setSize(800, 780);
        oWnd.setUrl(XMASTER_GET_BASE_URL + '/defaultTMSchedule.html?addnew=true&entityId=' + entityId + "&mod=" + mod.toString());
        oWnd.show();
    }

    /* Show error*/
    //export function SHOW_DATA_SOURCE_ERROR(e): string {
    //    try {
    //        if (e.xhr != undefined) {
    //            if (showSessionFinished(e.xhr.statusText)) { return ""; }
    //            let display: string = "";
    //            // Status 406 Continue (Informative Error Not Acceptable)

    //            if (e.xhr.statusText !== "OK" && e.xhr.status !== 406) {
    //                display = "Error: " + e.xhr.statusText + ", ";
    //            }

    //            display += (e.xhr.responseText == undefined) ? e.xhr.responseXML : e.xhr.responseText;
    //            alert(display);
    //        } else {
    //            let display: string = "";
    //            // Status 406 Continue (Informative Error Not Acceptable)
    //            if (e.statusText !== "OK" && e.status !== 406) {
    //                display = "Error: " + e.statusText + ", ";
    //            }
    //            display += (e.responseText == undefined) ? e.responseXML : e.responseText;
    //            if ((e.readyState !== 0 && e.responseText !== "" && e.status !== 0 && e.statusText !== "error")) {
    //                alert(display);
    //            }
    //        }
    //    } catch (ex) {
    //        alert("Server returned an undefined error");
    //    }
    //}


    //function showSessionFinished(statusText: string): boolean {
    //    if (statusText === "OK") {
    //        alert("Session expired");
    //        return true;
    //    }
    //    return false;
    //}

    /* Return date in format MM/DD/YYYY */
    export function FORMAT_DATE_TO_MM_DD_YYYY( d:Date):string {
        var currDate = d.getDate().toString();
        if (currDate.length < 2) { currDate = "0" + currDate }
        var currMonth = (d.getMonth() + 1).toString();
        if (currMonth.length < 2) { currMonth = "0" + currMonth }
        var currYear = d.getFullYear();
        return (currMonth + "/" + currDate + "/" + currYear);
    }

    /*
     * Return HH:MM as string
     * @param d: the datetime with the time to output
     */
    export function FORMAT_AMPM(date:Date) {
        var hours = date.getHours();
        var minutes: string | number = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? "0" + minutes : minutes;
        var strTime = hours + ":" + minutes + " " + ampm;
        return strTime;
    }
    /*
     * Return HH:MM:SS AM/PM as string
     * @param d: the datetime with the time to output
     */
    export function FORMAT_TIME_AMPM(date: Date)
    {
        var numHours: number = date.getHours();
        var hours: string; 
        var minutes: string | number = date.getMinutes();
        var seconds: string | number = date.getSeconds();
        var ampm = numHours >= 12 ? 'PM' : 'AM';
        numHours = numHours % 12;
        hours = numHours == 0 ? "12" : numHours < 10 ? "0" + numHours.toString() : numHours.toString(); // the hour '0' should be '12'
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        var strTime = hours + ":" + minutes + ":" + seconds + " " + ampm;
        return strTime;
    }
    /*
     * Convert JasonDate format as "/Date(1224043200000)/"
     * Return stringDate formated MM/dd/yyyy
     */
    export function FORMAT_JSONDateValue(JsonDateValue: string) 
    {
        var strDataValue: string = JsonDateValue;
        strDataValue = strDataValue.substring(strDataValue.indexOf("(") + 1, strDataValue.indexOf(")") -1);

        //var valueDate = new Date(parseInt(strDataValue.replace(/(^.*()|([+-].*$)/g, '')));
        var valueDate = new Date(parseInt(strDataValue));
        var month = valueDate.getMonth();
        var day = valueDate.getDate();
        var strDate = month + "/" + day + "/" + valueDate.getFullYear();
        if (month < 10) 
        {
            strDate = strDate.substr(0, 0) + '0' + month + strDate.substr(1)
        };
        if (day < 10) 
        {
            strDate = strDate.substr(0, 3) + '0' + day + strDate.substr(4)
        };
        return strDate;
       
    } 
}

$.fn.bindUp = function (type, fn) {

    type = type.split(/\s+/);

    this.each(function () {
        var len = type.length;
        while (len--) {
            $(this).bind(type[len], fn);

            var evt = $.data(this, 'events')[type[len]];
            evt.splice(0, 0, evt.pop());
        }
    });
};