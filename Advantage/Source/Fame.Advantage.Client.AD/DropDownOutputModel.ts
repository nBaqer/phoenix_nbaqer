﻿module AD {
// ReSharper disable InconsistentNaming
    /*
    * Campus output
    */
    export interface ICampusOutput {

     /*
     * Gets or sets the campus ID
     */
       CampusId: string;
    }

    /*
     * Campus output
     */
    export class CampusOutput {
        /*
         * Gets or sets the campus ID
         */
        CampusId: string;
    }

    /*
     * This class enhanced DropDownIntegerOutputModel to support campus Id
     */
    export interface IDropDownWithCampusOutputModel {
        /*
         * The list of campus id that belong the output model
         */
        CampusesIdList: CampusOutput[];

        /*
         * ID (Guid)
         */
        ID: string;

        /*
         * Description
         */
        Description: string;

    }

    /*
     * This class enhanced DropDownIntegerOutputModel to support campus Id
     */
    export class DropDownWithCampusOutputModel {
        /*
         * The list of campus id that belong the output model
         */
        CampusesIdList: CampusOutput[];

        /*
         * ID (Guid)
         */
        ID: string;

        /*
         * Description
         */
        Description: string;
    }
    // ReSharper restore InconsistentNaming
}

