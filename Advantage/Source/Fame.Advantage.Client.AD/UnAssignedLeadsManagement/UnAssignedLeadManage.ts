﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
module AD {

    export class UnAssignedLeadManage {

        public viewModel: UnAssignedLeadManageVm;
        //public debug: boolean = true;

        constructor() {
            try {
            this.viewModel = new UnAssignedLeadManageVm();
                var ethis = this;

                this.viewModel.CreateChartUnAssignedLeads();
                //this.viewModel.CreateChartMyLead();
                var char = $("#UnAssignedWidget").data("kendoChart") as any;
                char.redraw();

                this.viewModel.CreateLeadGrid();
                this.viewModel.setInitialCategory();

                // This bind the view model with all.-------------------------------------------------------------------------
                kendo.init($("#myLeadManagementPage"));
                kendo.bind($("#myLeadManagementPage"), ethis.viewModel);
            }
            catch (e) {
                alert(e.message + "/n" + e.stack);

            }
        }

    }
}