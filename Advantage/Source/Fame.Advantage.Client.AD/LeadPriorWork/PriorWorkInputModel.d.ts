declare module AD {
    interface IPriorWorkInputModel {
        Command: number;
        LeadId: string;
    }
    class PriorWorkInputModel {
        Command: number;
        LeadId: string;
    }
}
