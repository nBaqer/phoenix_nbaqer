﻿module AD {
    // ReSharper disable InconsistentNaming
    /*
     * The prior work input model.
     */
    export interface IPriorWorkInputModel {
        /*
         * Gets or sets the command.
         */

        Command: number;

        /*
         * Gets or sets the Lead
         */
        LeadId: string;

        /**
         * Campus Id
         */
        CampusId: string;

        /**
         * User Id 
         */
        UserId: string;

        /**
         * Gets or sets the Prior Work id to delete
         */
        PriorWorkId: string;
    }

    /*
     * The prior work input model.
     */
    export class PriorWorkInputModel {
        /*
         * Gets or sets the command.
         */
        Command: number;

        /*
         * Gets or sets the Lead
         */
        LeadId: string;

        /**
         * Campus Id
         */
        CampusId: string;

       /**
        * User Id 
        */
        UserId: string;

        /**
         * Gets or sets the Prior Work id to delete
         */
        PriorWorkId: string;
    }
    // ReSharper restore InconsistentNaming
}