declare module AD {
    interface IPriorWorkOutputModel {
        InputModel: PriorWorkInputModel;
        PriorWorkItemList: PriorWorkItemOutputModel[];
        JobStatusItemList: DropDownWithCampusOutputModel[];
        AdTitlesItemList: DropDownWithCampusOutputModel[];
    }
    class PriorWorkOutputModel {
        InputModel: PriorWorkInputModel;
        PriorWorkItemList: PriorWorkItemOutputModel[];
        JobStatusItemList: DropDownWithCampusOutputModel[];
        AdTitlesItemList: DropDownWithCampusOutputModel[];
    }
}
