declare module AD {
    interface IPriorWorkAddressOutputModel {
        AddressId: number;
        PriorWorkId: string;
        Address: string;
        Address1: string;
        AddressApto: string;
        Address2: string;
        City: string;
        State: string;
        ZipCode: string;
        Country: string;
        ModDate: string;
        Moduser: string;
    }
    class PriorWorkAddressOutputModel {
        AddressId: number;
        PriorWorkId: string;
        Address: string;
        Address1: string;
        AddressApto: string;
        Address2: string;
        City: string;
        State: string;
        ZipCode: string;
        Country: string;
        ModDate: string;
        Moduser: string;
    }
}
