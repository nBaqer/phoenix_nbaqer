declare module AD {
    interface IPriorWorkItemOutputModel {
        AddressOutputModel: PriorWorkAddressOutputModel;
        StartDate: Date;
        EndDate: Date;
        JobStatus: string;
        JobStatusId: string;
        Employer: string;
        JobTitle: string;
        SchoolJobTitle: string;
        Contacts: PriorWorkContactOutputModel[];
        JobResponsabilities: string;
        Comments: string;
    }
    class PriorWorkItemOutputModel {
        AddressOutputModel: PriorWorkAddressOutputModel;
        StartDate: Date;
        EndDate: Date;
        JobStatus: string;
        JobStatusId: string;
        Employer: string;
        JobTitle: string;
        SchoolJobTitle: string;
        Contacts: PriorWorkContactOutputModel[];
        JobResponsabilities: string;
        Comments: string;
    }
}
