declare module AD {
    interface IPriorWorkContactOutputModel {
        ContactId: number;
        PriorWorkId: string;
        Title: string;
        Name: string;
        FirstName: string;
        LastName: string;
        MiddleName: string;
        Phone: string;
        Email: string;
        IsActive: number;
    }
    class PriorWorkContactOutputModel {
        ContactId: number;
        PriorWorkId: string;
        Title: string;
        Name: string;
        FirstName: string;
        LastName: string;
        MiddleName: string;
        Phone: string;
        Email: string;
        IsActive: number;
    }
}
