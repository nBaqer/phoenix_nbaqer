﻿module AD {
// ReSharper disable InconsistentNaming
    /*
     * The prior work contact output model.
     */
    export interface IPriorWorkContactOutputModel {
        /*
         * Gets or sets the contact id.
         */

        ContactId: number;

        /*
         * Gets or sets the prior work id.
         *  This is the prior Work ID that the
         *  contact is linked
         */
        PriorWorkId: string;

        /*
         * Gets or sets the title of the contact.
         */
        Title: string;

        /*
         * Gets or sets the name.
         */
        Name: string;

        /*
         * Gets or sets the first name.
         */
        FirstName: string;

        /*
         * Gets or sets the last name.
         */
        LastName: string;

        /*
         * Gets or sets the middle name.
         */
        MiddleName: string;

        /**
         * 0 phone is USA 1 is international
         */
        IsPhoneInternational: boolean;

        /*
         * Gets or sets the phone.
         */
        Phone: string;

        /*
         * Gets or sets the email.
         */
        Email: string;

        /*
         * Gets or sets the is active.
         */
        IsActive: number;

        /*
         * 
         */
        Prefix: DropDownOutputModel;
    }

    /*
     * The prior work contact output model.
     */
    export class PriorWorkContactOutputModel implements IPriorWorkContactOutputModel{
        /*
         * Gets or sets the contact id.
         */
        ContactId: number;

        /*
         * Gets or sets the prior work id.
         *  This is the prior Work ID that the
         *  contact is linked
         */
        PriorWorkId: string;

        /*
         * Gets or sets the title of the contact.
         */
        Title: string;

        /*
         * Gets or sets the name.
         */
        Name: string;

        /*
         * Gets or sets the first name.
         */
        FirstName: string;

        /*
         * Gets or sets the last name.
         */
        LastName: string;

        /*
         * Gets or sets the middle name.
         */
        MiddleName: string;

        /**
        * 0 phone is USA 1 is international
        */
        IsPhoneInternational: boolean;

        /*
         * Gets or sets the phone.
         */
        Phone: string;

        /*
         * Gets or sets the email.
         */
        Email: string;

        /*
         * Gets or sets the is active.
         */
        IsActive: number;

        /*
         * 
         */
        Prefix: DropDownOutputModel;
    }
// ReSharper restore InconsistentNaming
}


