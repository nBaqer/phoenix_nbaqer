﻿module AD {
    // ReSharper disable InconsistentNaming
    /*
     * 
     */
     export interface IPriorWorkDetailItemOutputModel {

        /*
         * 
         */
        ContactId: number;

        /*
         * 
         */
        Comments: string;

        /*
         * 
         */
        Employer: string;

        /*
         * 
         */
        JobResponsabilities: string;

        /*
         * 
         */
        ContactList: PriorWorkContactOutputModel[];

    }

    /*
     * 
     */
    export class PriorWorkDetailItemOutputModel implements IPriorWorkDetailItemOutputModel{
        /*
         * 
         */
        ContactId: number;

        /*
         * 
         */
        Comments: string;

        /*
         * 
         */
        Employer: string;

        /*
         * 
         */
        JobResponsabilities: string;

        /*
         * 
         */
        ContactList: PriorWorkContactOutputModel[];

    }
    // ReSharper restore InconsistentNaming
}

