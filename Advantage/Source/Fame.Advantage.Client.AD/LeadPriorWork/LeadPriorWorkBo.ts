﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />

module AD {

    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;
    declare var XSTUDENT_GET_SHADOW_LEAD: string;

    export class LeadPriorWorksBo {
        dataSource: kendo.data.DataSource;
        public newPriorCountryDataSource: kendo.data.DataSource;
        public newPriorSchoolJobTitleDataSource: kendo.data.DataSource;
        public newPriorJobStatusDataSource: kendo.data.DataSource;
        public newPriorStateDataSource: kendo.data.DataSource;
        public selectedPriorWork: string;
        public leadId:string;

        // Second Grid
        public newPriorContactGridDataSource: kendo.data.DataSource;

        constructor(leadid: string) {
            this.leadId = leadid;
            const campusId: string = XMASTER_GET_CURRENT_CAMPUS_ID;
            var userId: string = $("#hdnUserId").val();
            this.dataSource = new kendo.data.DataSource({
                transport: {
                    read: options => {
                        $.ajax({
                            url: XGET_SERVICE_LAYER_PRIORWORK,
                            type: "GET",
                            dataType: "json",
                            data: { LeadId: leadid, Command: 1 },
                            success: result => {
                                options.success(result);

                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    }
                    ,
                    update: options => {
                        var info: IPriorWorkOutputModel = new PriorWorkOutputModel();
                        info.InputModel = new PriorWorkInputModel();
                        info.InputModel.Command = 3;
                        info.InputModel.UserId = userId;
                        info.InputModel.LeadId = leadid;
                        info.InputModel.PriorWorkId = options.data.models[0].Id;
                        info.AdSchoolJobTitlesItemList = new Array<DropDownOutputModel>(); //options.models[0].Group;
                        info.JobStatusItemList = new Array<DropDownOutputModel>(); //options.models[0].Level;

                        // Insert the Item to be updated
                        info.PriorWorkItemList = new Array<PriorWorkItemOutputModel>();
                        var prior: IPriorWorkItemOutputModel = new PriorWorkItemOutputModel();
                        prior.Id = options.data.models[0].Id;
                        prior.EndDate = options.data.models[0].EndDate;
                        prior.StartDate = options.data.models[0].StartDate;
                        prior.Employer = options.data.models[0].Employer;
                        prior.JobTitle = options.data.models[0].JobTitle;
                        var address: IPriorWorkAddressOutputModel = new PriorWorkAddressOutputModel();
                        let address1 = options.data.models[0].AddressOutputModel.Address1;
                        let model = options.data.models[0];
                        if (address1 !== undefined && address1 !== null && address1 !== "") {
                            address.Address1 = address1;
                            address.Country = model.AddressOutputModel == undefined
                                ? null
                                : model.AddressOutputModel.Country;
                            address.Address2 = model.AddressOutputModel.Address2;
                            address.AddressId = 0;
                            address.ZipCode = ($("#priorZipCode").data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox).raw();
                            address.IsInternational = $("#priorInternational").is(":checked");
                            if (address.IsInternational) {
                                address.StateInternational = model.AddressOutputModel.StateInternational;
                            } else {
                                address.State = (model.AddressOutputModel.State == undefined) ? "" : model.AddressOutputModel.State;
                            }
                            address.City = model.AddressOutputModel.City;
                            prior.AddressOutputModel = address;
                        } else {
                            prior.AddressOutputModel = new PriorWorkAddressOutputModel();
                        }
                        // Combo Box
                        prior.JobStatus = options.data.models[0].JobStatus;
                        prior.SchoolJobTitle = options.data.models[0].SchoolJobTitle;

                        info.PriorWorkItemList.push(prior);

                        //info.ExtraInfoList.push(extra);
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: XPOST_SERVICE_LAYER_PRIORWORK,
                            data: JSON.stringify(info),
                            success: result => {
                                options.success(result);
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    destroy: options => {
                        var info: IPriorWorkOutputModel = new PriorWorkOutputModel();
                        info.InputModel = new PriorWorkInputModel();
                        info.InputModel.Command = 7;
                        info.InputModel.LeadId = leadid;
                        info.InputModel.UserId = userId;
                        info.InputModel.PriorWorkId = options.data.models[0].Id;
                        var extra: PriorWorkItemOutputModel = new PriorWorkItemOutputModel();
                        extra.Id = options.data.models[0].Id;
                        info.PriorWorkItemList = new Array<PriorWorkItemOutputModel>();
                        info.PriorWorkItemList.push(extra);
                        $.ajax({
                            type: "POST",
                            dataType: "text",
                            url: XPOST_SERVICE_LAYER_PRIORWORK,
                            data: JSON.stringify(info),
                            success: result => {
                                options.success(result);
                                // Hide the detail part
                                var detail = $("#WorkDetailSection");
                                detail.css("display", "none");
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    create: options => {
                        var info: IPriorWorkOutputModel = new PriorWorkOutputModel();
                        info.InputModel = new PriorWorkInputModel();
                        // The filter parameters....
                        info.InputModel.Command = 1; // Post Command 1 insert
                        info.InputModel.LeadId = leadid;
                        info.InputModel.UserId = userId;
                        info.InputModel.CampusId = campusId;

                        // Initialize the lists...
                        info.AdSchoolJobTitlesItemList = new Array<DropDownOutputModel>(); //options.models[0].Group;
                        info.JobStatusItemList = new Array<DropDownOutputModel>(); //options.models[0].Level;
                        info.PriorWorkItemList = new Array<PriorWorkItemOutputModel>();

                        // Fill the prior work to return to server
                        var extra: IPriorWorkItemOutputModel = new PriorWorkItemOutputModel();
                        // Simple Fields
                        extra.Id = '00000000-0000-0000-0000-000000000000';
                        extra.Employer = options.data.models[0].Employer;
                        extra.StartDate = options.data.models[0].StartDate;
                        extra.EndDate = options.data.models[0].EndDate;
                        extra.JobTitle = options.data.models[0].JobTitle;
                        // Address
                        var address: IPriorWorkAddressOutputModel = new PriorWorkAddressOutputModel();
                        let address1 = options.data.models[0].AddressOutputModel.Address1;
                        let model = options.data.models[0];
                        if (address1 !== undefined && address1 !== null && address1 !== "") {
                            address.Address1 = address1;
                            address.Country = model.AddressOutputModel == undefined ? null : model.AddressOutputModel.Country;
                            address.Address2 = model.AddressOutputModel.Address2;
                            address.AddressId = 0;
                            address.ZipCode = ($("#priorZipCode").data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox).raw();
                            address.IsInternational = $("#priorInternational").is(":checked");
                            if (address.IsInternational) {
                                address.StateInternational = model.AddressOutputModel.StateInternational;
                            } else {
                                address.State = model.AddressOutputModel.State;
                            }
                            address.City = model.AddressOutputModel.City;
                            extra.AddressOutputModel = address;
                        }
                        // Combo Box
                        extra.JobStatus = options.data.models[0].JobStatus;
                        extra.SchoolJobTitle = options.data.models[0].SchoolJobTitle;

                        // 
                        info.PriorWorkItemList.push(extra);


                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: XPOST_SERVICE_LAYER_PRIORWORK,
                            data: JSON.stringify(info),
                            success: result => {
                                options.success(result);
                                let grid = $("#priorWorkGrid").data("kendoGrid") as kendo.ui.Grid;
                                $.when(grid.dataSource.read())
                                    .then(promise => {
                                        grid.resize(true);
                                        grid.refresh();
                                        // Hide the detail part
                                        var detail = $("#WorkDetailSection");
                                        detail.css("display", "none");
                                    });
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    parameterMap: (options, operation) => {
                        if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                        return null;
                    }
                },
                batch: true,
                pageSize: 2,
                schema: {
                    data: "PriorWorkItemList",
                    total: response => response.PriorWorkItemList.length,
                    model: {
                        id: "Id",
                        fields: {
                            Id: { editable: false, nullable: false },
                            Employer: { editable: true, nullable: false },
                            JobTitle: { editable: true, nullable: false },
                            SchoolJobTitle: {
                                validation: { required: true },
                                defaultValue: { ID: "00000000-0000-0000-0000-000000000000", Description: "" }
                            },
                            JobStatus: {
                                validation: { required: true },
                                defaultValue: { ID: "00000000-0000-0000-0000-000000000000", Description: "" }
                            },
                            StartDate: {
                                editable: true, nullable: true, type: "date",
                                format: "MM/dd/yyyy"
                            },
                            EndDate: { editable: true, nullable: true, type: "date" },

                            AddressOutputModel: {
                                AddressId: { editable: false, nullable: true },
                                PriorWorkId: { editable: false, nullable: true },
                                Address1: { editable: true, nullable: true },
                                Address2: { editable: true, nullable: true },
                                City: { editable: true, nullable: true },
                                StateInternational: { editable: true, nullable: true },
                                State: { editable: true, nullable: true },
                                ZipCode: { editable: true, nullable: true, type: "object" },
                                Country: { editable: true, nullable: true }
                            }
                        }
                    }
                }
            });

            this.newPriorCountryDataSource = new kendo.data.DataSource({
                transport: {
                    read: options => {
                        $.ajax({
                            url: XGET_SERVICE_LAYER_PRIORWORK,
                            type: "GET",
                            dataType: "json",
                            data: {
                                CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                Command: 5 // Get the list of countries
                            },
                            success: result => {
                                options.success(result.CountriesList);
                                let ddl = $("#priorGridTemplateWrapper").find("input[name=newPriorCountry]").data("kendoDropDownList");
                                //ddl.text("United States");
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    }
                }
            });

            this.newPriorSchoolJobTitleDataSource = new kendo.data.DataSource({
                transport: {
                    read: options => {
                        $.ajax({
                            url: XGET_SERVICE_LAYER_PRIORWORK,
                            type: "GET",
                            dataType: "json",
                            data: {
                                CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                Command: 2
                            },
                            success: result => {
                                options.success(result.AdSchoolJobTitlesItemList);
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    }
                }
            });

            this.newPriorJobStatusDataSource = new kendo.data.DataSource({
                transport: {
                    read: options => {
                        $.ajax({
                            url: XGET_SERVICE_LAYER_PRIORWORK,
                            type: "GET",
                            dataType: "json",
                            data: {
                                CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                Command: 3
                            },
                            success: result => {
                                options.success(result.JobStatusItemList);
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    }
                }
            });

            this.newPriorStateDataSource = new kendo.data.DataSource({
                transport: {
                    read: options => {
                        $.ajax({
                            url: XGET_SERVICE_LAYER_PRIORWORK,
                            type: "GET",
                            dataType: "json",
                            data: {
                                CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                Command: 4  // Get USA States  
                            },
                            success: result => {
                                options.success(result.StatesList);
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    }
                }
            });

            this.newPriorContactGridDataSource = new kendo.data.DataSource({
                transport: {
                    read: options => {
                        $.ajax({
                            url: XGET_SERVICE_LAYER_PRIORWORK,
                            type: "GET",
                            dataType: "json",
                            data: { LeadId: leadid, Command: 6, PriorWorkId: this.selectedPriorWork },
                            success: result => {
                                options.success(result.PriorWorkDetailObject);
                                // Fill the two details field Job responsibilities and Comments
                                let respon = $("#priorJobResponsabilities");
                                let comment = $("#priorComment");
                                let label = $("#priorSelecteddEntity");
                                label.text(result.PriorWorkDetailObject.Employer);
                                comment.val(result.PriorWorkDetailObject.Comments);
                                respon.val(result.PriorWorkDetailObject.JobResponsabilities);

                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    update: options => {
                        var info: IPriorWorkOutputModel = new PriorWorkOutputModel();
                        info.InputModel = new PriorWorkInputModel();
                        info.InputModel.Command = 4;
                        info.InputModel.UserId = userId;
                        info.InputModel.LeadId = leadid;
                        info.InputModel.PriorWorkId = this.selectedPriorWork;

                        // Insert the Detail Item to be updated
                        info.PriorWorkDetailObject = new PriorWorkDetailItemOutputModel();
                        info.PriorWorkDetailObject.ContactList = new Array<PriorWorkContactOutputModel>();
                        var contact: IPriorWorkContactOutputModel = new PriorWorkContactOutputModel();
                        contact.ContactId = options.data.models[0].ContactId;
                        contact.PriorWorkId = this.selectedPriorWork;
                        contact.Title = options.data.models[0].Title;
                        contact.Email = options.data.models[0].Email;
                        contact.FirstName = options.data.models[0].FirstName;
                        contact.IsActive = options.data.models[0].IsActive ? 1 : 0;
                        contact.IsPhoneInternational = options.data.models[0].IsPhoneInternational;
                        contact.LastName = options.data.models[0].LastName;
                        contact.MiddleName = options.data.models[0].MiddleName;
                        contact.Phone = ($("#phoneMasked").data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox).raw();
                        contact.Prefix = options.data.models[0].Prefix;

                        info.PriorWorkDetailObject.ContactList.push(contact);

                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: XPOST_SERVICE_LAYER_PRIORWORK,
                            data: JSON.stringify(info),
                            success: result => {
                                options.success(result);
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    destroy: options => {
                        var info: IPriorWorkOutputModel = new PriorWorkOutputModel();
                        info.InputModel = new PriorWorkInputModel();
                        info.InputModel.Command = 8;
                        info.InputModel.LeadId = leadid;
                        info.InputModel.UserId = userId;
                        info.InputModel.PriorWorkId = this.selectedPriorWork;
                        var extra: PriorWorkDetailItemOutputModel = new PriorWorkDetailItemOutputModel();
                        extra.ContactId = options.data.models[0].ContactId;
                        info.PriorWorkDetailObject = extra;
                        $.ajax({
                            type: "POST",
                            dataType: "text",
                            url: XPOST_SERVICE_LAYER_PRIORWORK,
                            data: JSON.stringify(info),
                            success: result => {
                                options.success(result);
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    create: options => {
                        var info: IPriorWorkOutputModel = new PriorWorkOutputModel();
                        info.InputModel = new PriorWorkInputModel();
                        // The filter parameters....
                        info.InputModel.Command = 2; // Post Command 2 insert
                        info.InputModel.LeadId = leadid;
                        info.InputModel.UserId = userId;
                        info.InputModel.CampusId = campusId;
                        info.InputModel.PriorWorkId = this.selectedPriorWork;

                        // Initialize the lists...
                        info.PriorWorkDetailObject = new PriorWorkDetailItemOutputModel();
                        info.PriorWorkDetailObject.ContactList = new Array<PriorWorkContactOutputModel>(); //options.models[0].Level;

                        // Fill the prior work details to return to server
                        // Contact
                        var contact: IPriorWorkContactOutputModel = new PriorWorkContactOutputModel();
                        let model = options.data.models[0];
                        contact.PriorWorkId = this.selectedPriorWork;
                        contact.Email = model.Email == undefined ? "" : model.Email;
                        contact.FirstName = model.FirstName;
                        contact.IsActive = model.IsActive ? 1 : 0;
                        contact.IsPhoneInternational = model.IsPhoneInternational;
                        contact.LastName = model.LastName == null ? "" : model.LastName;
                        contact.MiddleName = model.MiddleName ? "" : model.MiddleName;
                        contact.Phone = ($("#phoneMasked").data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox).raw();
                        contact.Prefix = model.Prefix;
                        contact.Title = model.Title;

                        // 
                        info.PriorWorkDetailObject.ContactList.push(contact);
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: XPOST_SERVICE_LAYER_PRIORWORK,
                            data: JSON.stringify(info),
                            success: result => {
                                options.success(result);
                                let grid = $("#priorWorkContactGrid").data("kendoGrid") as kendo.ui.Grid;
                                $.when(grid.dataSource.read())
                                    .then(promise => {
                                        grid.resize(true);
                                        grid.refresh();
                                    });
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    parameterMap: (options, operation) => {
                        if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                        return null;
                    }
                },
                batch: true,
                pageSize: 2,
                schema: {
                    data: "ContactList",
                    total: response => response.ContactList.length,
                    model: {
                        id: "ContactId",
                        fields: {

                            ContactId: { editable: false, nullable: false, hidden: true },
                            Title: { editable: true, nullable: false, validation: { required: true } },
                            Name: { editable: false, nullable: false, hidden: true },
                            Prefix: {
                                nullable: true,
                                defaultValue: { ID: "00000000-0000-0000-0000-000000000000", Description: "" },
                                hidden: false
                            },
                            FirstName: { editable: true, nullable: false, hidden: true, validation: { required: true } },
                            MiddleName: { editable: true, nullable: false, hidden: true },
                            LastName: { editable: true, nullable: false, hidden: true, validation: { required: true } },
                            IsPhoneInternational: { editable: true, nullable: false },
                            Phone: {
                                editable: true, nullable: true, type: "string"
                                //  format: "MM/dd/yyyy"
                            },
                            Email: { editable: true, nullable: true },
                            IsActive: { editable: true }
                        }
                    }
                }
            });

        }

        addNewRow(): (ev: UIEvent) => any {

            var grid = $("#priorWorkGrid").data("kendoGrid") as any;
            grid.addRow();
            return null;
        }

        addNewRowContact(): (ev: UIEvent) => any {

            var grid = $("#priorWorkContactGrid").data("kendoGrid") as any;
            grid.addRow();
            return null;
        }

        getIsInternationalPhoneEditor(container, options) {
            $('<input id="IsPhoneInternational" name="IsPhoneInternational" type="checkbox" data-bind="checked:IsPhoneInternational" />')
                .appendTo(container);
        }

        getIsActivePhoneEditor(container, options) {
            $('<input name="IsActive" type="checkbox" data-bind="checked:IsActive" />')
                .appendTo(container);
        }

        getPhoneMaskEditor(container, options) {
            $('<input id="phoneMasked" data-text-field="' +
                options.field +
                '" ' +
                'class="k-input k-textbox" ' +
                'type="tel" ' +
                'data-value-field="' +
                options.field +
                '" ' +
                'data-bind="value:' +
                options.field +
                '"/>')
                .appendTo(container).kendoMaskedTextBox({
                    mask: "(000)-000-0000",
                    clearPromptChar: false
                });
        }
       public postSdf(om: PriorWorkOutputModel, context: any) {

            $.ajax({
                type: "POST",
                dataType: "json",
                url: XPOST_SERVICE_LAYER_PRIORWORK,
                data: JSON.stringify(om)
            })
                .done(
                result => {
                    MasterPage.SHOW_INFO_WINDOW("Custom fields saved successfully");

                })
                .fail(result => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                });
        }
        public getSdfList(pgResId: number, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_SDF_GET + "?PageResourceId=" + pgResId + "&CampusId=" + XMASTER_GET_CURRENT_CAMPUS_ID,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }
        public getSdfListValues(leadId: string, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_PRIORWORK + "?LeadId=" + leadId +"&Command="+8,
                type: 'GET',
                timeout: 20000,
                dataType: 'json'
            });
        }
        getPrefixEditor(container, options) {
            $('<input name="' + options.field + '" data-text-field="Description" data-value-field="ID"  data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    dataSource: {
                        transport: {
                            read: options => {
                                $.ajax({
                                    url: XGET_SERVICE_LAYER_PRIORWORK,
                                    type: "GET",
                                    dataType: "json",
                                    data: {
                                        CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                        //LeadId: "00000000-0000-0000-0000-000000000000",
                                        Command: 7
                                    },
                                    success: result => {
                                        options.success(result.PrefixList);
                                    },
                                    error: result => {
                                        options.error(result);
                                        MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                                    }
                                });
                            }
                        },
                        dataValueField: "ID",
                        dataTextField: "Description",
                        autoBind: false
                    },
                    optionLabel: "Select"
                });
        }

        /**
         * Save Comments button
         * @param buttonClickEvent
         */
        saveComments(buttonClickEvent: kendo.ui.ButtonClickEvent) {

            let om = new PriorWorkOutputModel();
            let filter = new PriorWorkInputModel();
            let pw = new PriorWorkItemOutputModel();
            filter.UserId = $("#hdnUserId").val();
            filter.PriorWorkId = this.selectedPriorWork;
            filter.LeadId = this.leadId;
            filter.Command = 5;
            pw.Comments = $("#priorComment").val().toString();
            pw.Id = this.selectedPriorWork;
            om.InputModel = filter;
            om.PriorWorkItemList = new Array<PriorWorkItemOutputModel>();
            om.PriorWorkItemList.push(pw);
            $.ajax({
                type: "POST",
                dataType: "json",
                url: XPOST_SERVICE_LAYER_PRIORWORK,
                data: JSON.stringify(om)
            })
                .done(
                result => {
                    MasterPage.SHOW_INFO_WINDOW("Comments saved successfully");

                })
                .fail(result => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                });
        }
        /**
         * Save Responsibilities button
         * @param buttonClickEvent
         */
        saveResponsabilities(buttonClickEvent: kendo.ui.ButtonClickEvent) {

            let om = new PriorWorkOutputModel();
            let filter = new PriorWorkInputModel();
            let pw = new PriorWorkItemOutputModel();
            filter.UserId = $("#hdnUserId").val();
            filter.PriorWorkId = this.selectedPriorWork;
            filter.LeadId = this.leadId;
            filter.Command = 6;
            pw.JobResponsabilities = $("#priorJobResponsabilities").val().toString();
            pw.Id = this.selectedPriorWork;
            om.InputModel = filter;
            om.PriorWorkItemList = new Array<PriorWorkItemOutputModel>();
            om.PriorWorkItemList.push(pw);
            $.ajax({
                type: "POST",
                dataType: "json",
                url: XPOST_SERVICE_LAYER_PRIORWORK,
                data: JSON.stringify(om)
            })
                .done(
                result => {
                    MasterPage.SHOW_INFO_WINDOW("Job Responsibilities saved successfully");

                })
                .fail(result => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                });
        }
    }
}