﻿module AD {

       // ReSharper disable InconsistentNaming
 
    /*
     * The prior work address output model.
     */
    export interface IPriorWorkAddressOutputModel {
        /*
         * Gets or sets the address id.
         */
        AddressId: number;

        /*
         * Gets or sets the prior work id.
         *  This is the prior Work ID that the
         *  contact is linked
         */
        PriorWorkId: string;

        /*
         * Gets or sets the address id.
         */
        Address: string;

        /*
         * Gets or sets First Part of the Address (max 250 characters)
         */
        Address1: string;

        /*
         * Gets or sets the address Apartment.
         */
        AddressApto: string;

        /*
         * Gets or sets Second Part of the Address (max 250 characters)
         */
        Address2: string;

        /*
         * Gets or sets City of the Address
         */
        City: string;

        /**
         * True if the address is international
         */
        IsInternational: Boolean;

        /*
         * Gets or sets State Name of the Address ID / Description
         */
        State: DropDownOutputModel;

        /**
         * Gets or sets the international state
         */
        StateInternational:string;

        /**
         * Gets or sets Country name of the Address ID/ Description
         */
         Country:DropDownOutputModel;

        /*
         * Gets or sets Zip Code of the Address
         */
        ZipCode: string;

       /*
         * Gets or sets Last Modification Date
         */
        ModDate: string;

        /*
         * Gets or sets Last User Modified this record.
         */
        Moduser: string;

    }

    /*
     * The prior work address output model.
     */
    export class PriorWorkAddressOutputModel implements IPriorWorkAddressOutputModel {
        /*
         * Gets or sets the address id.
         */
        AddressId: number;

        /*
         * Gets or sets the prior work id.
         *  This is the prior Work ID that the
         *  contact is linked
         */
        PriorWorkId: string;

        /*
         * Gets or sets the address id.
         */
        Address: string;

        /**
         * True if the address is international
         */
        IsInternational: Boolean;

        /*
         * Gets or sets First Part of the Address (max 250 characters)
         */
        Address1: string;

        /*
         * Gets or sets the address Apartment.
         */
        AddressApto: string;

        /*
         * Gets or sets Second Part of the Address (max 250 characters)
         */
        Address2: string;

        /*
         * Gets or sets City of the Address
         */
        City: string;

        /*
         * Gets or sets State Name of the Address
         */
        State: DropDownOutputModel;

        /*
         * Gets or sets Zip Code of the Address
         */
        ZipCode: string;

        /*
         * Gets or sets Country name of the Address
         */
        Country: DropDownOutputModel;

        /*
         * Gets or sets Last Modification Date
         */
        ModDate: string;

        /*
         * Gets or sets Last User Modified this record.
         */
        Moduser: string;

        /**
         * To International state if enabled
         */
        StateInternational: string;
    }
}
// ReSharper restore InconsistentNaming

