var AD;
(function (AD) {
    var LeadPriorWorksBo = (function () {
        function LeadPriorWorksBo(leadid) {
            var campusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            var userId = $("#hdnUserId").val();
            var command = 1;
            this.dataSource = new kendo.data.DataSource({
                transport: {
                    read: function (options) {
                        $.ajax({
                            url: AD.XGET_SERVICE_LAYER_PRIORWORK,
                            type: "GET",
                            dataType: "json",
                            data: { LeadId: leadid, Command: command },
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                        return null;
                    }
                },
                batch: true,
                pageSize: 20,
                schema: {
                    data: "PriorWorkItemList",
                    total: function (response) { return response.PriorWorkItemList.length; },
                    model: {
                        id: "Id",
                        fields: {
                            Id: { editable: false, nullable: false },
                            StartDate: { editable: false, nullable: true },
                            EndDate: { editable: false, nullable: true },
                            JobStatus: {
                                validation: { required: true },
                                defaultValue: { ID: "0", Description: "" }
                            },
                            Employer: { editable: false, nullable: false },
                            Address: { editable: false, nullable: false },
                            JobTitle: { editable: true, nullable: false },
                            SchoolJobtitle: {
                                validation: { required: true },
                                defaultValue: { ID: "0", Description: "" }
                            }
                        }
                    }
                }
            });
        }
        LeadPriorWorksBo.prototype.addNewRow = function () {
            var grid = $("#priorWorkGrid").data("kendoGrid");
            grid.addRow();
            return null;
        };
        LeadPriorWorksBo.prototype.getGroupValuesEditor = function (container, options) {
            $('<input required data-text-field="Description" data-value-field="ID"  data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                dataSource: {
                    transport: {
                        read: function (options) {
                            $.ajax({
                                url: AD.XGET_SERVICE_LAYER_EXTRACURRICULAR,
                                type: "GET",
                                dataType: "json",
                                data: {
                                    CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                    LeadId: "00000000-0000-0000-0000-000000000000",
                                    Command: 3
                                },
                                success: function (result) {
                                    options.success(result.GroupItemsList);
                                },
                                error: function (result) {
                                    options.error(result);
                                    ad.SHOW_DATA_SOURCE_ERROR(result);
                                }
                            });
                        }
                    },
                    dataValueField: "ID",
                    dataTextField: "Description",
                    autoBind: false
                },
                optionLabel: "Select"
            });
        };
        LeadPriorWorksBo.prototype.getLevelValuesEditor = function (container, options) {
            $('<input required data-text-field="Description" data-value-field="ID"  data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                dataSource: {
                    transport: {
                        read: function (options) {
                            $.ajax({
                                url: AD.XGET_SERVICE_LAYER_EXTRACURRICULAR,
                                type: "GET",
                                dataType: "json",
                                data: {
                                    CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                    LeadId: "00000000-0000-0000-0000-000000000000",
                                    Command: 2
                                },
                                success: function (result) {
                                    options.success(result.LevelsItemsList);
                                },
                                error: function (result) {
                                    options.error(result);
                                    ad.SHOW_DATA_SOURCE_ERROR(result);
                                }
                            });
                        }
                    },
                    dataValueField: "ID",
                    dataTextField: "Description",
                    autoBind: false
                },
                optionLabel: "Select"
            });
        };
        ;
        return LeadPriorWorksBo;
    }());
    AD.LeadPriorWorksBo = LeadPriorWorksBo;
})(AD || (AD = {}));
//# sourceMappingURL=LeadPriorWorkBo.js.map