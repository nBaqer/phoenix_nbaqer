﻿module AD {
    // ReSharper disable InconsistentNaming
    /*
     * The prior work item output model.
     */
    export interface IPriorWorkItemOutputModel {
        /*
         * Gets or sets The Prior Work Id.
         */
        Id: string;

        /*
         * Gets or sets the address output model.
         */

        AddressOutputModel: PriorWorkAddressOutputModel;

        /*
         * Gets or sets Start Date for work
         */
        StartDate: Date;

        /*
         * Gets or sets the end date.
         */
        EndDate: Date;

        /*
         * Gets or sets the job status.
         */
        JobStatus: DropDownOutputModel;

        /*
         * Gets or sets the job title.
         */
        JobTitle: string;

        /*
         * Gets or sets the employer.
         */
        Employer: string;

        /*
         * Gets or sets the school job title.
         */
        SchoolJobTitle: DropDownOutputModel;

        /*
         * Gets or sets the contacts.
         */
        Contacts: PriorWorkContactOutputModel[];

        /*
         * Gets or sets the job responsibilities.
         */
        JobResponsabilities: string;

        /*
         * Gets or sets the comments.
         */
        Comments: string;
    }

    /*
     * The prior work item output model.
     */
    export class PriorWorkItemOutputModel implements IPriorWorkItemOutputModel {
        /*
         * Gets or sets The Prior Work Id.
         */
        Id: string;

        /*
         * Gets or sets the address output model.
         */
        AddressOutputModel: PriorWorkAddressOutputModel;

        /*
         * Gets or sets Start Date for work
         */
        StartDate: Date;

        /*
         * Gets or sets the end date.
         */
        EndDate: Date;

        /*
         * Gets or sets the job status.
         */
        JobStatus: DropDownOutputModel;

        /*
         * Gets or sets the job title.
         */
        JobTitle: string;

        /*
         * Gets or sets the employer.
         */
        Employer: string;

        /*
         * Gets or sets the school job title.
         */
        SchoolJobTitle: DropDownOutputModel;

        /*
         * Gets or sets the contacts.
         */
        Contacts: PriorWorkContactOutputModel[];

        /*
         * Gets or sets the job responsibilities.
         */
        JobResponsabilities: string;

        /*
         * Gets or sets the comments.
         */
        Comments: string;
        
    }
    // ReSharper restore InconsistentNaming

}



