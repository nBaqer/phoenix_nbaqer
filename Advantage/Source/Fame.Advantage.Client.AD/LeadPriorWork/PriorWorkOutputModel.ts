﻿module AD {
    // ReSharper disable InconsistentNaming
    /*
     * The prior work output model.
     */
    export interface IPriorWorkOutputModel {
        /*
         * Gets or sets the input model.
         */
        InputModel: PriorWorkInputModel;

        /*
         * Gets or sets the prior work item list.
         */
        PriorWorkItemList: PriorWorkItemOutputModel[];

        /*
         * Gets or sets the job status item list.
         */
        JobStatusItemList: DropDownOutputModel[];

        /*
         * Gets or sets the ad titles item list.
         */
        AdSchoolJobTitlesItemList: DropDownOutputModel[];

        /*
         * Gets or sets the states list.
         */
        StatesList: DropDownOutputModel[];

        /*
         * Gets or sets the countries list.
         */
        CountriesList: DropDownOutputModel[];

        /*
         * 
         */
        PriorWorkDetailObject: PriorWorkDetailItemOutputModel;

        /**
         * Get the list of prefix
         */
        PrefixList: DropDownOutputModel[];
        SdfList: Sdf[];
    }

    /*
     * The prior work output model.
     */
    export class PriorWorkOutputModel implements IPriorWorkOutputModel {
        /*
         * Gets or sets the input model.
         */
        InputModel: PriorWorkInputModel;

        /*
         * Gets or sets the prior work item list.
         */
        PriorWorkItemList: PriorWorkItemOutputModel[];

        /*
         * Gets or sets the job status item list.
         */
        JobStatusItemList: DropDownOutputModel[];

        /*
         * Gets or sets the ad titles item list.
         */
        AdSchoolJobTitlesItemList: DropDownOutputModel[];

        /*
         * Gets or sets the states list.
         */
        StatesList: DropDownOutputModel[];

        /*
         * Gets or sets the countries list.
         */
        CountriesList: DropDownOutputModel[];

        /*
         * For detail grid
         */
        PriorWorkDetailObject: PriorWorkDetailItemOutputModel;

        /**
         * Get the list of prefix
         */
        PrefixList: DropDownOutputModel[];
        SdfList: ISdf[];
    }
    // ReSharper restore InconsistentNaming
}

