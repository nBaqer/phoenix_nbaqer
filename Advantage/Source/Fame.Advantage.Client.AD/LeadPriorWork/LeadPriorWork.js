var AD;
(function (AD) {
    var LeadPriorWorks = (function () {
        function LeadPriorWorks(leadid) {
            this.leadId = leadid;
            this.bo = new AD.LeadPriorWorksBo(leadid);
            $("#priorWorkGrid").kendoGrid({
                dataSource: this.bo.dataSource,
                pageable: true,
                columns: [
                    { field: "StartDate", title: "Start Date", width: "150px" },
                    { field: "EndDate", title: "End Date", width: "150px" },
                    {
                        field: "Group",
                        title: "Group",
                        width: "150px",
                        template: "#=Group.Description#"
                    },
                    { field: "Description", title: "Job Status" },
                    { field: "Employer", title: "Employer", width: "150px" },
                    { field: "Address", title: "Address" },
                    { field: "JobTitle", title: "JobTitle", width: "150px" },
                    {
                        field: "SchoolJobTitle",
                        title: "School Job Title",
                        width: "150px",
                        template: "#=Level.Description#"
                    },
                    { command: [{ name: "edit", text: " " }], title: "Edit", width: "50px" },
                    { command: [{ name: "destroy", text: " " }], title: "", width: "50px" }],
                editable: "popup",
                edit: function (e) {
                    if (e.model.isNew()) {
                        e.container.data("kendoWindow").title("Enter a New Prior Work");
                    }
                    else {
                        e.container.data("kendoWindow").title("Edit the Prior Work");
                    }
                }
            });
            $("#priorWorkContactGrid").kendoGrid({
                dataSource: this.bo.dataSource,
                pageable: true,
                columns: [
                    { field: "Title", title: "Title", width: "150px" },
                    { field: "Name", title: "Name", width: "150px" },
                    { field: "Phone", title: "Phone" },
                    { field: "Email", title: "Email", width: "150px" },
                    { field: "Status", title: "Status" },
                    { field: "JobTitle", title: "JobTitle", width: "150px" },
                    { command: [{ name: "edit", text: " " }], title: "Edit", width: "50px" },
                    { command: [{ name: "destroy", text: " " }], title: "", width: "50px" }],
                editable: "popup",
                edit: function (e) {
                    if (e.model.isNew()) {
                        e.container.data("kendoWindow").title("Enter a New Prior Work Contact");
                    }
                    else {
                        e.container.data("kendoWindow").title("Edit the Prior Work Contact");
                    }
                }
            });
            $("#priorJobResponsabilitiesSave").kendoButton({
                spriteCssClass: "k-icon k-save"
            });
            $("#priorJobResponsabilitiesCleanSave").kendoButton({
                spriteCssClass: "k-icon k-delete"
            });
            $("#priorCommentSave").kendoButton({
                spriteCssClass: "k-icon k-save"
            });
            $("#priorCommentCleanSave").kendoButton({
                spriteCssClass: "k-icon k-delete"
            });
            var newRow = document.getElementById("priorPlusImage");
            newRow.addEventListener('click', this.bo.addNewRow, false);
        }
        return LeadPriorWorks;
    }());
    AD.LeadPriorWorks = LeadPriorWorks;
})(AD || (AD = {}));
//# sourceMappingURL=LeadPriorWork.js.map