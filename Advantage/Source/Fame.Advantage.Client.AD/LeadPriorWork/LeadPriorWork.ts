﻿//// <reference path="../../Fame.Advantage.Client.MasterPage/CustomControl/MasterPageCampusDropDown.ts" />
module AD {
    //import QueryStringParameterByName = MasterPage.GET_QUERY_STRING_PARAMETER_BY_NAME;

    export class LeadPriorWorks {

        public bo: LeadPriorWorksBo;
        public leadId: string;
        public priorGridValidator: kendo.ui.Validator;
        public sdfValidator: kendo.ui.Validator;


        constructor(leadid: string) {
            this.leadId = leadid;
            this.bo = new LeadPriorWorksBo(leadid);
            MasterPage.Common.enableDisableBtns(false);
        }
        initialization() {

            let body = $("body");
            body.prop("id", "priorWorkBody");
            this.sdfValidator = MasterPage.ADVANTAGE_VALIDATOR("dvCustomFields");
            //var detailGridClickHandler = this.callDetailGrid.bind(this);
            var template = kendo
                .template("<span class=\"k-link linkcolor\" onclick='manager.callDetailGrid(\"#=Id #\")'> #=Employer # </span>")
                .bind(this);

            var grid = $("#priorWorkGrid")
                .kendoGrid({
                    dataSource: this.bo.dataSource,
                    pageable: true,
                    columns: [
                        { field: "Id", title: "", width: "0px", hidden: true },
                        {
                            field: "StartDate",
                            title: "Start Date",
                            width: "100px",
                            template: "#=  StartDate == null? '' : kendo.toString(kendo.parseDate(StartDate), 'MM/dd/yyyy') #"
                        },
                        {
                            field: "EndDate",
                            title: "End Date",
                            width: "100px",
                            template: "#=  StartDate == null? '' : kendo.toString(kendo.parseDate(EndDate), 'MM/dd/yyyy') #"
                        },
                        {
                            field: "JobStatus",
                            title: "Job Status",
                            template: "#= JobStatus == null ? '' : JobStatus.Description #"
                        },
                        {
                            field: "Employer",
                            title: "Employer",
                            //width: "150px",
                            template: template // kendo.template("<span class=\"k-link linkcolor\" onclick='callDetailGrid(\"#=Id #\")'> #=Employer # </span>").bind(that)
                        },
                        {
                            field: "AddressOutputModel.Address1", title: "Address1", hidden: true
                            , template: "#=AddressOutputModel == null ? '' : AddressOutputModel.Address1 #"
                        },
                        {
                            field: "AddressOutputModel.Address2", title: "Address2", hidden: true
                            , template: "#=AddressOutputModel == null ? '' : AddressOutputModel.Address2 #"
                        },
                        {
                            field: "AddressOutputModel.AddressApto", title: "Address Apt", hidden: true
                            , template: "#=AddressOutputModel == null ? '' : AddressOutputModel.AddressApto #"
                        },
                        {
                            field: "AddressOutputModel.City", title: "City", hidden: true
                            , template: "#=AddressOutputModel == null ? '' : AddressOutputModel.City #"
                        },
                        {
                            field: "AddressOutputModel.State", title: "State", hidden: true
                            , template: "#=AddressOutputModel.State == null ? '' : AddressOutputModel.State.Description #"
                        },
                        {
                            field: "AddressOutputModel.StateInternational", title: "State", hidden: true
                            , template: "#=AddressOutputModel.StateInternational == null ? '' : AddressOutputModel.StateInternational #"
                        },
                        {
                            field: "AddressOutputModel.ZipCode", title: "ZIP", hidden: true
                            , template: "#=AddressOutputModel == null ? '' : AddressOutputModel.ZipCode #"
                        },
                        {
                            field: "AddressOutputModel.Country",
                            title: "Country",
                            hidden: true
                            , template: "#=AddressOutputModel.Country == null ? '' : AddressOutputModel.Country.Description #"

                        },
                        {
                            field: "Address",
                            title: "Address",
                            hidden: false
                            , template:
                            "#=  AddressOutputModel.Address1 == null?\"\": AddressOutputModel.Address1 #" +
                            "#=  ((AddressOutputModel.Address2 == null) || (AddressOutputModel.Address2 == \"\"))?\"\": \", \" + AddressOutputModel.Address2 #" +
                            "#=  ((AddressOutputModel.City == null) || (AddressOutputModel.City == \"\"))?\"\": \", \" + AddressOutputModel.City #" +
                            "#=  ((AddressOutputModel.StateInternational == null) || (AddressOutputModel.StateInternational == \"\"))?\"\": \", \" + AddressOutputModel.StateInternational  #" +
                            "#=  AddressOutputModel.State == null?\"\": \", \" + AddressOutputModel.State.Description #" +
                            "#=  AddressOutputModel.ZipCode == null?\"\": \" \" + AddressOutputModel.ZipCode  + ' ' #" +
                            "#=  AddressOutputModel.Country == null ?\"\": AddressOutputModel.Country.Description #"
                        },

                        {
                            field: "JobTitle",
                            title: "JobTitle",
                            width: "150px"
                        },

                        {
                            field: "SchoolJobTitle",
                            title: "School Job Title",
                            width: "150px",
                            //editor: this.bo.getSchoolJobTitleEditor,
                            template: "#= SchoolJobTitle == null ? '' : SchoolJobTitle.Description #"
                        },
                        {
                            field: "AddressOutputModel.IsInternational",
                            hidden: true,
                            template: "#=AddressOutputModel == null ? False : AddressOutputModel.IsInternational #"
                        },

                        { command: [{ name: "edit", text: " " }], title: "Edit", width: "50px" },
                        {
                            command: [{
                                name: "delet",
                                click: function (e) {
                                    e.preventDefault(); //prevent page scroll reset
                                    var tr = $(e.target).closest("tr"); //get the row for deletion
                                    var data = this.dataItem(tr); //get the row data so it can be referred later
                                    $.when(MasterPage
                                        .SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure you want to delete this record?"))
                                        .then(confirmed => {
                                            if (confirmed) {
                                                grid.dataSource.remove(data); //prepare a "destroy" request
                                                grid.dataSource.sync(); //actually send the request 
                                            }
                                        });
                                },
                                text: "X"
                            }], title: "", width: "50px"
                        }
                    ],
                    //{ command: [{ name: "destroy", text: " " }], title: "", width: "50px" }],
                    editable: {
                        mode: "popup",
                        template: kendo.template($("#priorGridEditor").html())
                    },
                    edit: e => {
                        e.container.attr("id", "priorWorkGridValidate");
                        this.priorGridValidator = MasterPage.ADVANTAGE_VALIDATOR("priorWorkGridValidate");
                        if (e.model.isNew()) {
                            // If model is new create the Address Output Model Object to get Address
                            (e.model as any).AddressOutputModel = new PriorWorkAddressOutputModel();

                            (e.container.data("kendoWindow") as any).title("Enter a New Prior Work");
                        } else {
                            (e.container.data("kendoWindow") as any).title("Edit the Prior Work");
                        }
                        // Read the data source for Countries combo box
                        let country = e.container
                            .find("input[name=newPriorCountry]")
                            .data("kendoDropDownList") as kendo.ui.DropDownList;
                        country.setDataSource(this.bo.newPriorCountryDataSource);
                        country.dataSource.read()
                            .then(() => {
                                let model = e.model;
                                let control = model.get("AddressOutputModel.Country") as DropDownOutputModel;
                                if (e.model.isNew() === false) {
                                    if (control != null) {
                                        country.text(control.Description);
                                    } else {
                                        // IF it is new should be USA

                                        country.text("Select");
                                    }
                                } else {
                                    country.text("United States");
                                }
                                country.trigger("change");

                            });

                        // Set the information for State data-source
                        let state = e.container.find("input[name=AddressOutputModel]").data("kendoDropDownList");
                        state.setDataSource(this.bo.newPriorStateDataSource);
                        state.dataSource.read()
                            .then(() => {
                                if (e.model.isNew() === false) {
                                    let model = e.model;
                                    let desc = model.get("AddressOutputModel.State") as DropDownOutputModel;
                                    if (desc != null) {
                                        state.text(desc.Description);
                                    }
                                }
                            });

                        let jobStatus = e.container.find("input[name=JobStatus]").data("kendoDropDownList");
                        jobStatus.setDataSource(this.bo.newPriorJobStatusDataSource);
                        jobStatus.dataSource.read()
                            .then(() => {
                                if (e.model.isNew() === false) {
                                    let model = e.model;
                                    let control = model.get("JobStatus");
                                    let desc = control == undefined ? "Select" : control.Description;
                                    jobStatus.text(desc);
                                }
                            });


                        let ddSchool = e.container.find("input[name=SchoolJobTitle]").data("kendoDropDownList") as kendo.ui.DropDownList;
                        ddSchool.setDataSource(this.bo.newPriorSchoolJobTitleDataSource);
                        ddSchool.options.optionLabel = "Select";
                        ddSchool.dataSource.read()
                            .then(() => {
                                if (e.model.isNew() === false) {
                                    let model = e.model;
                                    let control = model.get("SchoolJobTitle");
                                    let desc = control == undefined ? "Select" : control.Description;
                                    ddSchool.text(desc);
                                }
                            });

                        let zipcode = e.container.find("#priorZipCode");
                        // ZIP CODE Masked text box
                        let maskedZip = zipcode.kendoMaskedTextBox({
                            mask: "00000",
                            clearPromptChar: true,
                            unmaskOnPost: true
                        }).data("kendoMaskedTextBox") as kendo.ui.MaskedTextBox;

                        let checkIntern = $("#priorInternational");
                        // Create Event on Update Button
                        let stateusa = $("#priorStateUsa");
                        let stateint = $("#StateInternational");
                        checkIntern.change(function () {
                            if ($(this).is(":checked")) {
                                stateusa.removeClass("toggle-vis");
                                stateusa.addClass("toggle-inv");
                                stateint.removeClass("toggle-inv");
                                stateint.addClass("toggle-vis");
                                maskedZip.setOptions({ mask: 'aaaaaaaaaa' });
                            } else {
                                stateusa.removeClass("toggle-inv");
                                stateusa.addClass("toggle-vis");
                                stateint.removeClass("toggle-vis");
                                stateint.addClass("toggle-inv");
                                maskedZip.setOptions({ mask: '00000' });
                            }
                        });

                        let value = e.model.get("AddressOutputModel.IsInternational");
                        checkIntern.prop("checked", value);
                        checkIntern.trigger("change");

                        // this.priorGridValidator = MasterPage.ADVANTAGE_VALIDATOR("priorGridTemplateWrapper");

                        // Get the update button click to put the validator call
                        let button = e.container.find(".k-grid-update");
                        var clickHandler = this.updatePriorGrid.bind(this);
                        button.one("click", clickHandler);
                    },
                    save: e => {
                        if (e.model.isNew()) {
                            // If model is new assign the text-field with the model
                            // e.model[0].AddressOutputModel = new PriorWorkAddressOutputModel();
                        }
                    }

                }).data("kendoGrid");

            // Set Contact Grid
            var contactGrid = $("#priorWorkContactGrid").kendoGrid({
                dataSource: [],
                pageable: true,
                columns: [
                    { field: "ContactId", width: "0", hidden: true },
                    { field: "Title", title: "Title", width: "150px" },
                    {
                        field: "Prefix", title: "Prefix",
                        hidden: true,
                        template: "#=Prefix == null ? '' : Prefix.Description #",
                        editor: this.bo.getPrefixEditor
                    },
                    { field: "FirstName", title: "First Name", width: 0, hidden: true },
                    { field: "MiddleName", title: "Middle Name", width: 0, hidden: true },
                    { field: "LastName", title: "Last Name", width: 0, hidden: true },
                    {
                        field: "Name", title: "Name",
                        template:
                        "#=  Prefix == null?\"\": Prefix.Description + ' ' #" +
                        "#=  FirstName == null?\"\": FirstName + ' ' #" +
                        "#=  MiddleName == null?\"\": MiddleName + ' ' #" +
                        "#=  LastName == null?\"\": LastName  #"
                    },

                    {
                        field: "IsPhoneInternational", title: "Is International?", hidden: true,
                        template: "#=IsPhoneInternational == null ? False : IsPhoneInternational #",
                        editor: this.bo.getIsInternationalPhoneEditor
                    },
                    {
                        field: "Phone", title: "Phone",
                        editor: this.bo.getPhoneMaskEditor,
                        template: (dataItem) => {
                            return this.formatPhoneGrid(dataItem.IsPhoneInternational, dataItem.Phone);
                        }

                    },

                    { field: "Email", title: "Email" },

                    {
                        field: "IsActive", title: "Status", width: "100px",
                        template: "#= IsActive ? \"Active\" : \"Inactive\" #",
                        editor: this.bo.getIsActivePhoneEditor,
                    },

                    { command: [{ name: "edit", text: " " }], title: "Edit", width: "50px" },
                    {
                        command: [{
                            name: "delet",
                            click: function (e) {
                                e.preventDefault(); //prevent page scroll reset
                                var tr = $(e.target).closest("tr"); //get the row for deletion
                                var data = this.dataItem(tr); //get the row data so it can be referred later
                                $.when(MasterPage
                                    .SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure you want to delete this record?"))
                                    .then(confirmed => {
                                        if (confirmed) {
                                            contactGrid.dataSource.remove(data); //prepare a "destroy" request
                                            contactGrid.dataSource.sync(); //actually send the request 
                                        }
                                    });
                            },
                            text: "X"
                        }], title: "", width: "50px"
                    }
                ],
                editable: "popup",
                autoBind: false,
                edit: e => {
                    e.container.attr("id", "ContactGridValidate");
                    this.priorGridValidator = MasterPage.ADVANTAGE_VALIDATOR("ContactGridValidate");
                    if (e.model.isNew()) {
                        (e.container.data("kendoWindow") as any).title("Enter a New Contact");
                    } else {
                        (e.container.data("kendoWindow") as any).title("Edit Contact");
                    }

                    //hidden the not use fields
                    e.container.find(".k-edit-label:first").hide();
                    e.container.find(".k-edit-field:first").hide();
                    e.container.find("k-edit-label:nth-child(7)").hide();
                    e.container.find("label[for=Name]").parent().hide();
                    e.container.find("label[for=Name]").parent().next().hide();

                    // Entering the required fields
                    let label: any = e.container.find("label[for='Title']");
                    label[0].innerHTML = label[0].innerHTML + "<span style='color:red'> *</span>";
                    let label1: any = e.container.find("label[for='FirstName']");
                    label1[0].innerHTML = label1[0].innerHTML + "<span style='color:red'> *</span>";
                    let label2: any = e.container.find("label[for='LastName']");
                    label2[0].innerHTML = label2[0].innerHTML + "<span style='color:red'> *</span>";

                    let inputMail: any = e.container.find("input[name='Email']");
                    inputMail.innerHTML = inputMail.prop("type", "email");

                    // Change the content of Status label
                    let labelSta: any = e.container.find("label[for='IsActive']");
                    labelSta[0].innerHTML = "Active?";

                    // Configure International Options
                    this.updatePhoneMask();

                    //// international implementation
                    let checkIntern = $("#IsPhoneInternational");

                    // Create Event on Update Button
                    checkIntern.change(() => {
                        this.updatePhoneMask();
                    });

                    // Validator
                    e.container.find("a.k-grid-update").bind("click",
                        () => {
                            return this.priorGridValidator.validate();
                        });
                }
            } as any).data("kendoGrid") as kendo.ui.Grid;

            //Buttons declarations

            $("#priorJobResponsabilitiesSave").kendoButton({
                icon: "check",
                click: (e) => {
                    this.bo.saveResponsabilities(e);
                }
                //spriteCssClass: "k-icon k-tick"
            });
            //$("#priorJobResponsabilitiesCleanSave").kendoButton({
            //    spriteCssClass: "k-icon k-delete"
            //});
            $("#priorCommentSave").kendoButton({
                icon: "check",
                click: (e) => {
                    this.bo.saveComments(e);
                }
                //spriteCssClass: "k-icon k-tick"
            });
            $("#sdfSave").kendoButton({
                icon: "check",
                click: (e) => {
                    //call to save sdf values
                    if (this.sdfValidator.validate()) {
                        this.saveSdfValues();
                    } else {
                        MasterPage.SHOW_WARNING_WINDOW(X_SAVE_PLEASE_FILL_REQUIRED_FIELDS);
                    }
                }
                //spriteCssClass: "k-icon k-tick"
            });
            //$("#priorCommentCleanSave").kendoButton({
            //    spriteCssClass: "k-icon k-delete"
            //});

            //// Set the data-source for control
            //grid.setDataSource(this.bo.getExtraCurricularGridInfo());

            // Add AddRow handler main grid
            var newRow = document.getElementById("priorPlusImage");
            newRow.addEventListener('click', this.bo.addNewRow, false);

            var newRowDetail = document.getElementById("priorContactPlusImage");
            newRowDetail.addEventListener('click', this.bo.addNewRowContact, false);

            // call the custom feilds section
            this.processSdfFields();
        }

        //start region sdf fields
        public processSdfFields() {
            var resourceId: string = MasterPage.GET_QUERY_STRING_PARAMETER_BY_NAME("resid");
            var sessionStorageName = AD.XPRIOR_WORK_SDF_FIELDS;
            if (resourceId === "146") {
                sessionStorageName = AD.XPRIOR_WORK_SDF_FIELDS;
            } else {
                sessionStorageName = AD.XPRIOR_WORK_SDF_FIELDS_STUDENT;
            }
            //if (sessionStorage.getItem(XPRIOR_WORK_SDF_FIELDS) != null && sessionStorage.getItem(XPRIOR_WORK_SDF_FIELDS) != undefined) {
            if (sessionStorage.getItem(sessionStorageName) != null && sessionStorage.getItem(sessionStorageName) != undefined) {
                var temp:any = Object.create($.parseJSON(sessionStorage.getItem(sessionStorageName)));
                var fixedResponse = temp.responseText.replace(/\\'/g, "'");
                if (fixedResponse !== "[]") {
                    var result = JSON.parse(fixedResponse);
                    if (result != null) {
                        this.displaysdf(result);
                    }
                } else {
                    //var sdfList = this.bo.getSdfList(146, this)
                    var sdfList1 = this.bo.getSdfList(Number(resourceId), this)
                        .done(msg1 => {
                            if (msg1 != undefined) {
                                try {
                                    sessionStorage.setItem(sessionStorageName, JSON.stringify(sdfList1));
                                    if (msg1 != null) {
                                        //call
                                        this.displaysdf(msg1);
                                    }
                                } catch (e) {
                                    $("body").css("cursor", "default");
                                    MasterPage.SHOW_ERROR_WINDOW(e.message);
                                }
                            }
                        }).fail((msg => {
                            $("body").css("cursor", "default");
                            MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                        }));
                }
            }
            else
                {
            //var sdfList = this.bo.getSdfList(146, this)
                var sdfList = this.bo.getSdfList(Number(resourceId), this)
                .done(msg => {
                    if (msg != undefined) {
                        try {
                            sessionStorage.setItem(sessionStorageName, JSON.stringify(sdfList));
                            if (msg != null) {
                                //call
                                this.displaysdf(msg);
                            }
                        } catch (e) {
                            $("body").css("cursor", "default");
                            MasterPage.SHOW_ERROR_WINDOW(e.message);
                        }
                    }
                }).fail((msg => {
                    $("body").css("cursor", "default");
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    }));
            }
        }
        public displaysdf(msg: any) {
            let result = MasterPage.AdvantageUserDefinedFields.processSdfFields(msg, "dvCustomFields");
            if (result === 1) {
                $("#dvCustomBar").attr("style", "display:block");
                $("#dvCustomFields").attr("style", "display:block");
                $("#sdfSave").show();
                //call to display values
                this.showsdfValues();
            }
        }
        public showsdfValues() {
            var sdfList = this.bo.getSdfListValues(this.leadId,this)
                .done(msg => {
                    if (msg != undefined) {
                        try {
                            if (msg.SdfList != null) {
                                for (var k = 0; k < msg.SdfList.length; k++) {
                                    var id: string = "#" + msg.SdfList[k].SdfId;
                                    if ($(id).attr("data-role") === 'dropdownlist') {
                                        if ($(id).data("kendoDropDownList") != null) {
                                            $(id).data("kendoDropDownList").value(msg.SdfList[k].SdfValue);
                                            $(id).data("kendoDropDownList").list.width("auto");
                                        }
                                    } else {
                                        if (msg.SdfList[k].SdfValue != null) {
                                            //get as per the datatype
                                            //character or number
                                            if (msg.SdfList[k].DtypeId === 1 || msg.SdfList[k].DtypeId === 2) {
                                                $(id).val(msg.SdfList[k].SdfValue);
                                            }
                                            //date
                                            if (msg.SdfList[k].DtypeId === 3) {
                                                if ($(id).data("kendoDatePicker") != null) {
                                                    ($(id).data("kendoDatePicker") as any)
                                                        .value(msg.SdfList[k].SdfValue);
                                                }
                                            }
                                            //checkbox
                                            if (msg.SdfList[k].DtypeId === 4) {
                                                let isf = msg.SdfList[k].SdfValue;
                                                if (isf != null) {
                                                    if (isf === false || isf === "False") {
                                                        //do nothing
                                                    } else {
                                                        if ($(id) != null) {
                                                            $(id).prop("checked", isf);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (e) {
                            $("body").css("cursor", "default");
                            MasterPage.SHOW_ERROR_WINDOW(e.message);
                        }
                    }
                }).fail((msg => {
                    $("body").css("cursor", "default");
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                }));
        }
        public saveSdfValues() {
            let om = new PriorWorkOutputModel();
            let filter = new PriorWorkInputModel();
            filter.UserId = $("#hdnUserId").val();
            filter.PriorWorkId = this.bo.selectedPriorWork;
            filter.LeadId = this.leadId;
            filter.Command = 9;
            om.InputModel = filter;
            om.PriorWorkItemList = new Array<PriorWorkItemOutputModel>();

            var $inputs = $("#dvCustomFields :input");
            if ($inputs != null) {
                om.SdfList = [];
                let dType: any;
                $inputs.each(function () {
                    var sdf = new Sdf();
                    sdf.SdfId = $(this).prop("id");
                    dType = $(this).attr("data-dtypeid");
                    if (dType === "4") {
                        sdf.SdfValue = $(this).prop("checked");
                    } else {
                        sdf.SdfValue = $(this).val();
                    }
                    om.SdfList.push(sdf);
                });
            }
            this.bo.postSdf(om,this);
        }
        //end region sdf fields

        public callDetailGrid(priorWorkId: string) {
            // Get the selected item values necessary for this section
            this.bo.selectedPriorWork = priorWorkId;
            let grid = $("#priorWorkContactGrid").data("kendoGrid") as kendo.ui.Grid;
            grid.setDataSource(this.bo.newPriorContactGridDataSource);
            this.bo.newPriorContactGridDataSource.read()
                .then(promise => {
                    // Show the detail part
                    var detail = $("#WorkDetailSection");
                    detail.css("display", "block");
                });
        }

        public formatPhoneGrid(isinternational: boolean, phone: string) {
            if (isinternational) {
                return phone;
            }

            return MasterPage.Common.formatPhone(phone);
        }

        updatePriorGrid(e) {
            e.preventDefault();
            return this.priorGridValidator.validate();
        }

        /**
         * Check the isInternational to set the phone mask
         */
        updatePhoneMask() {
            let phoneMasked = $("#phoneMasked").data("kendoMaskedTextBox");
            let checkIntern = $("#IsPhoneInternational");
            if (checkIntern.is(":checked")) {
                phoneMasked.setOptions({ mask: '+9999999999999999999999999' });
            } else {
                phoneMasked.setOptions({ mask: '(000)-000-0000' });
            }
        }

    }
} 