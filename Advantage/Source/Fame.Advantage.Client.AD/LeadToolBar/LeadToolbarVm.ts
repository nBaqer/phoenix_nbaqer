﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../fame.advantage.client.masterpage/advantagemessageboxs.ts" />

module AD
{

    // import OpenTaskRadScheduleWindowWithEntity = ad.openTaskRadScheduleWindowWithEntity;

    // ReSharper disable InconsistentNaming
    /* Open the mail task board */
    // declare function openTaskRadWindowWithEntity(entityId: string, mod: number);
    // ReSharper restore InconsistentNaming

    // This must be in the html code of control. ...............................
    declare var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL: string;
    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;
    //declare var XMASTER_GET_BASE_URL: string;
    //declare var $find: (input: string) => any;

    export class LeadToolbarVm extends kendo.Observable
    {

        infolead: kendo.data.DataSource;
        db: LeadInfoBarDb;
        //static flagLead: boolean;
        //flagTask: boolean;
        //flagAppointment: boolean;

        constructor()
        {
            super();
            this.db = new LeadInfoBarDb();
            sessionStorage.setItem(AD.X_FLAG_LEAD, "0");
            sessionStorage.setItem(AD.X_FLAG_TASK, "0");
            sessionStorage.setItem(AD.X_FLAG_APPO, "0");
        }
        ///#region Menu go to

        //public goToLeadInfoScreenNew() {

        //    // If I am in Lead Info Page I should ask for Save...
        //    var resourceId: string = GET_QUERY_STRING_PARAMETER_BY_NAME("resid"); // $.fn.QueryString["resid"];
        //    if (resourceId == "170") {

        //        var save: boolean = confirm("Save Changes?");
        //        if (save) {

        //            // Click save button
        //            $("#leadToolBarSave").click();
        //            return;
        //        }
        //    }

        //    sessionStorage.setItem(XLEAD_NEW, "true");
        //    window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?resid=170&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&mod=AD");
        //}
        //public comingFromLead(): boolean {
        //    return window.location.href.indexOf("AleadInfoPage") > 1
        //}
        public goToLeadInfoScreenNew()
        {
            var control2 = document.getElementById('wrapperLead');
            if (control2 === undefined || control2 === null)
            {
                sessionStorage.setItem(XLEAD_NEW, "true");
                window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?resid=170&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&new=true&mod=AD");
            }
        }

        /* provisional to get a menu to Queue page*/
        public goToAlarmPage()
        {
            var wnd = $("#leadToolBarWindow").data("kendoWindow") as kendo.ui.Window;
            var sume: number = 0;
            sume += Number(sessionStorage.getItem(AD.X_FLAG_LEAD));
            sume += Number(sessionStorage.getItem(AD.X_FLAG_APPO));
            sume += Number(sessionStorage.getItem(AD.X_FLAG_TASK));
            if (sume > 1)
            {
                // Open windows to select witch alarm we want to attend
                // Disable or enable based in the flag values
                let toolbar = $("#alertToolBar").data("kendoToolBar") as kendo.ui.ToolBar;
                toolbar.enable("#btnQueue", (sessionStorage.getItem(AD.X_FLAG_LEAD) === "1"));
                toolbar.enable("#btnTask", (sessionStorage.getItem(AD.X_FLAG_TASK) === "1"));
                toolbar.enable("#btnAppt", (sessionStorage.getItem(AD.X_FLAG_APPO) === "1"));
                wnd.open();
                wnd.center();
            }
            if (sume === 1)
            {
                // Select the link
                if (sessionStorage.getItem(AD.X_FLAG_TASK) === "1")
                {
                    this.goToLeadTaskScreen();
                    wnd.close();
                    return;
                }
                if (sessionStorage.getItem(AD.X_FLAG_APPO) === "1")
                {
                    this.goToLeadScheduleScreen();
                    wnd.close();
                    return;
                }
                sessionStorage.setItem(XSUB_MENU_SELECTION, XQUEUE_PAGE);
                window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/ALeadQueue.aspx?resid=825&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=LeadQueue&mod=AD");
            }
        }

        /*
         * Go to Queue Page
         */
        public goToQueuePage()
        {
            sessionStorage.setItem(XSUB_MENU_SELECTION, XQUEUE_PAGE);
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/ALeadQueue.aspx?resid=825&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=LeadQueue&mod=AD");
        }

        public goToLeadInfoScreen()
        {
            sessionStorage.setItem(XLEAD_NEW, "false");
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?resid=170&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&mod=AD");
        }

        public goToLeadQuickScreen()
        {
            if (window.location.href.indexOf("AleadInfoPage") > 1) {
                return;
            } 
            sessionStorage.setItem(XLEAD_QUICK_NEW, "true");
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadQuick.aspx?resid=268&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Quick&mod=AD");
        }

        public goToLeadNotesScreen()
        {
            if (window.location.href.indexOf("AleadInfoPage") > 1) {
                return;
            } 
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadNotes.aspx?resid=456&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Notes&mod=AD");
        }

        public goToLeadRequirementScreen()
        {
            if (window.location.href.indexOf("AleadInfoPage") > 1) {
                return;
            } 
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/leadRequirements.aspx?resid=826&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Requirements&mod=AD");
        }

        public goToLeadHistoryScreen()
        {
            if (window.location.href.indexOf("AleadInfoPage") > 1) {
                return;
            } 
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadHistory.aspx?resid=838&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=History&mod=AD");
        }
       
        public goToLeadEducationScreen()
        {
            if (window.location.href.indexOf("AleadInfoPage") > 1) {
                return;
            } 
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadEducation.aspx?resid=145&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Education&mod=AD");
        }

        public goToSPriorWorkScreen()
        {
            if (window.location.href.indexOf("AleadInfoPage") > 1) {
                return;
            } 
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadPriorWork.aspx?resid=146&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=PriorWorks&mod=AD");
        }

        public goToLeadExtracurricularScreen()
        {
            if (window.location.href.indexOf("AleadInfoPage") > 1) {
                return;
            } 
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadExtracurricular.aspx?resid=148&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Extracurricular&mod=AD");
        }

        public goToSkillScreen()
        {
            if (window.location.href.indexOf("AleadInfoPage") > 1) {
                return;
            } 
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadSkills.aspx?resid=147&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Skills&mod=AD");
        }

        public goToLeadEnrollmentScreen()
        {
            if (window.location.href.indexOf("AleadInfoPage") > 1) {
                return;
            } 
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadEnrollment.aspx?resid=174&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Enrollment&mod=AD");
        }

        public goToEmailPage()
        {
            var leadElement: any = document.getElementById('leadId');
            var leadGuid: string = leadElement.value;
            // var bo = new LeadToolbarVm();
            ad.OPEN_TASK_RAD_EMAIL_WINDOW_WITH_ENTITY(leadGuid, 4);

        }

        public goToLeadTaskScreen()
        {
            var leadElement: any = document.getElementById('leadId');
            var leadGuid: string = leadElement.value;
            ad.OPEN_TASK_RAD_TASK_WINDOW_WITH_ENTITY(leadGuid, 4);
            const wnd = $("#leadToolBarWindow").data("kendoWindow") as kendo.ui.Window;
            wnd.close();
        }


        public goToLeadScheduleScreen()
        {
            var leadElement: any = document.getElementById('leadId');
            var leadGuid: string = leadElement.value;
            // var bo = new LeadToolbarVm();
            ad.OPEN_TASK_RAD_SCHEDULE_WINDOW_WITH_ENTITY(leadGuid, 4);
            const wnd = $("#leadToolBarWindow").data("kendoWindow") as kendo.ui.Window;
            wnd.close();
        }
        ///#endregion

        /*
         * Get flags with alert information.
         */
        public getFromServerFlagInformation()
        {

            // Get the flag information
            var command = 0;
            try
            {
                $("body").css("cursor", "wait");
                var db1 = new LeadToolBarDb();
                var userId: string = $("#hdnUserId").val();
                db1.getFromServerFlagInformation(XMASTER_GET_CURRENT_CAMPUS_ID, userId, command, this)
                    .done(msg =>
                    {
                        if (msg != undefined)
                        {
                            try
                            {
                                if ((msg.FlagLead || msg.FlagAppointment || msg.FlagTask) === false)
                                {
                                    // No Alarm to set
                                    //let btn = $("#leadToolBarAlert span");
                                    //btn.prop("class", "k-sprite tb-greenCheckMark");
                                    let toolbar = $("#LeadToolbar").data("kendoToolBar") as kendo.ui.ToolBar;
                                    toolbar.enable("#leadToolBarAlert", false);

                                } else
                                {
                                    // Then show the alarm and finish
                                    // let btn = $("#leadToolBarAlert");
                                    // btn.prop("class", "k-sprite tb-alert");
                                    let toolbar = $("#LeadToolbar").data("kendoToolBar") as kendo.ui.ToolBar;
                                    toolbar.enable("#leadToolBarAlert", true);
                                }

                                //Store the values in session
                                sessionStorage.setItem(AD.X_FLAG_LEAD, (msg.FlagLead) ? "1" : "0");
                                sessionStorage.setItem(AD.X_FLAG_TASK, (msg.FlagTask) ? "1" : "0");
                                sessionStorage.setItem(AD.X_FLAG_APPO, (msg.FlagAppointment) ? "1" : "0");

                                // Cursor to normal
                                $("body").css("cursor", "default");

                            } catch (e)
                            {
                                //If errors clear the cache
                                $("body").css("cursor", "default");
                                alert(e.message);
                            }
                        }
                    });

            } catch (e)
            {
                $("body").css("cursor", "default");
                alert(e.message);
            }
        }
        disableEnrollBtn(disable: boolean)
        {
            let enrolltoolbar = $("#LeadToolbar").data("kendoToolBar") as kendo.ui.ToolBar;
            enrolltoolbar.enable("#btn18", !disable);
        }
        goToSetQueueLead(e)
        {
            // Collect necessary parameters...
            try
            {

                var command = (e.id === "btnPriorLead") ? 1 : 2;
                var userId: string = $("#hdnUserId").val();
                var campusId: string = XMASTER_GET_CURRENT_CAMPUS_ID;
                var control: any = document.getElementById('leadId');
                var leadId: string = control.value;

                $("body").css("cursor", "wait");
                var db1 = new LeadToolBarDb();
                db1.setFromServerQueueLead(campusId, userId, leadId, command, this)
                    .done(msg =>
                    {
                        if (msg != undefined)
                        {
                            try
                            {

                                // Reload the page   
                                sessionStorage.setItem("NewLead", "false");
                                var url = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?resid=170&NewEnt=1&cmpid=" + campusId + "&desc=Queue&mod=AD";
                                window.location.assign(url);

                                // Cursor to normal
                                $("body").css("cursor", "default");

                            } catch (e)
                            {
                                //If errors clear the cache
                                $("body").css("cursor", "default");
                                alert(e.message);
                            }
                        }
                    })
                    .fail(msg =>
                    {
                        //If errors clear the cache
                        $("body").css("cursor", "default");
                        MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    });

            } catch (e)
            {
                $("body").css("cursor", "default");
                alert(e.message);
            }

        };

    }



}
