﻿module AD {

    export class LeadToolBarDb {

        /*
         * Get the Lead Flags for pending new leads or task or appointments.
         */
        getFromServerFlagInformation(campusId:string, userId: string, command:number, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_TOOLBAR_FLAGS + "?Command=" + command + "&UserId=" + userId,
                type: "GET",
                timeout: 20000,
                dataType: "json"
            });

        }

        setFromServerQueueLead(campusId: string, userId: string, leadId: string, command: number, mcontext: LeadToolbarVm) {
            return $.ajax({
                context: mcontext,
                url: XGET_SERVICE_LAYER_TOOLBAR_FLAGS + "?Command=" + command
                + "&UserId=" + userId
                + "&CampusID=" + campusId
                + "&LeadId=" + leadId
                ,
                type: "GET",
                timeout: 20000,
                dataType: "json"
            });


        }
    }
} 