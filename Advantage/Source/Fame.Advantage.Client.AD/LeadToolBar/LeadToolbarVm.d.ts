/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
declare module AD {
    class LeadToolbarVm extends kendo.Observable {
        infolead: kendo.data.DataSource;
        db: LeadInfoBarDb;
        constructor();
        goToAlarmPage(): void;
        goToQueuePage(): void;
        goToLeadInfoScreen(): void;
        goToLeadQuickScreen(): void;
        goToLeadNotesScreen(): void;
        goToLeadRequirementScreen(): void;
        goToLeadHistoryScreen(): void;
        goToLeadEducationScreen(): void;
        goToSPriorWorkScreen(): void;
        goToLeadExtracurricularScreen(): void;
        goToSkillScreen(): void;
        goToLeadEnrollmentScreen(): void;
        goToEmailPage(): void;
        goToLeadTaskScreen(): void;
        goToLeadScheduleScreen(): void;
        getFromServerFlagInformation(): void;
        goToSetQueueLead(e: any): void;
    }
}
