var AD;
(function (AD) {
    var LeadToolBarDb = (function () {
        function LeadToolBarDb() {
        }
        LeadToolBarDb.prototype.getFromServerFlagInformation = function (campusId, userId, command, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XGET_SERVICE_LAYER_TOOLBAR_FLAGS + "?Command=" + command + "&UserId=" + userId,
                type: "GET",
                timeout: 20000,
                dataType: "json"
            });
        };
        LeadToolBarDb.prototype.setFromServerQueueLead = function (campusId, userId, leadId, command, mcontext) {
            return $.ajax({
                context: mcontext,
                url: AD.XGET_SERVICE_LAYER_TOOLBAR_FLAGS + "?Command=" + command
                    + "&UserId=" + userId
                    + "&CampusID=" + campusId
                    + "&LeadId=" + leadId,
                type: "GET",
                timeout: 20000,
                dataType: "json"
            });
        };
        return LeadToolBarDb;
    }());
    AD.LeadToolBarDb = LeadToolBarDb;
})(AD || (AD = {}));
//# sourceMappingURL=LeadToolBarDb.js.map