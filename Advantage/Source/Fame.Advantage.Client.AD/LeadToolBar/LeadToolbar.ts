﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />

module AD {

    export class LeadToolbar {

        public viewModel: LeadToolbarVm;
        //public debug: boolean = true;

        constructor() {
            try {
                this.viewModel = new LeadToolbarVm();
                var ethis = this;
                // Support for Kendo Tool Bar
                var toolbar = $("#LeadToolbar").kendoToolBar({
                    resizable: true,
                    items: [
                        //regular button
                        //{
                        //    template: '<a><span class="k-icon k-i-file-add"></span> New</a>',
                        //    overflow: "never"
                        //},
                        { id: "leadToolBarNew", type: "button", icon:"file-add ", text: "New", showIcon: "toolbar", click: ethis.viewModel.goToLeadInfoScreenNew, overflow: "never" },
                        { id: "leadToolBarSave", type: "button", text: "Save", icon: "save font-blue-darken-3 ", showIcon: "toolbar", overflow: "never" },
                        { id: "leadToolBarAlert", type: "button", text: "Alert", icon: "warning font-orange-lighten-1 ", showIcon: "toolbar", click: ethis.viewModel.goToAlarmPage, overflow: "never" },
                        { id: "btn04", type: "button", text: "Quick Lead", icon: "plus-outline font-green ", showIcon: "toolbar", click: ethis.viewModel.goToLeadQuickScreen, overflow: "never" },
                        { id: "btnPriorLead", type: "button", text: "Prior Lead", icon: "arrow-left  ", showIcon: "toolbar", showText: "overflow", click: ethis.viewModel.goToSetQueueLead, overflow: "never"   },
                        { id: "btn06", type: "button", text: "Lead", icon:"file font-blue-darken-3",showIcon:"toolbar", click: ethis.viewModel.goToLeadInfoScreen, overflow: "never" },
                        { id: "btnNextLead", type: "button", text: "Next Lead", icon: "arrow-right", showIcon: "toolbar", showText: "overflow", click: ethis.viewModel.goToSetQueueLead, overflow: "never" },
                        { id: "btn08", type: "button", text: "Task", icon: "check font-purple-darken-4", showIcon: "toolbar", click: ethis.viewModel.goToLeadTaskScreen, overflow: "never" },
                        { id: "btn09", type: "button", text: "Appt", icon: "calendar font-teal-darken-4", showIcon: "toolbar", click: ethis.viewModel.goToLeadScheduleScreen, overflow: "never" },
                        { id: "btn10", type: "button", text: "Email", icon: "email font-lyme-darken-4", showIcon: "toolbar", click: ethis.viewModel.goToEmailPage, overflow: "never" },
                        { id: "btn11", type: "button", text: "Notes", icon: "comment font-light-green-darken-4", showIcon: "toolbar", click: ethis.viewModel.goToLeadNotesScreen, overflow: "never" },
                        { id: "btn12", type: "button", text: "Requirements", icon: "track-changes-accept-all font-cyan-darken-4", showIcon: "toolbar", click: ethis.viewModel.goToLeadRequirementScreen, overflow: "never" },
                        { id: "btn13", type: "button", text: "History", icon: "track-changes font-brown-darken-4", showIcon: "toolbar", click: ethis.viewModel.goToLeadHistoryScreen, overflow: "never" },
                        { id: "btn14", type: "button", text: "Prior Ed", icon: "group-box", showIcon: "toolbar", click: ethis.viewModel.goToLeadEducationScreen, overflow: "never" },
                        { id: "btn15", type: "button", text: "Prior Work", icon: "file-word", showIcon: "toolbar", click: ethis.viewModel.goToSPriorWorkScreen, overflow: "never" },
                        { id: "btn16", type: "button", text: "Extracurricular", icon: "file-ascx", showIcon: "toolbar", enable: true, click: ethis.viewModel.goToLeadExtracurricularScreen, overflow: "never" },

                        { id: "btn17", type: "button", text: "Skills", icon: "aggregate-fields", showIcon: "toolbar", click: ethis.viewModel.goToSkillScreen, overflow: "never" },

                        { id: "btn18", type: "button", text: "Enroll", icon: "check-outline font-green", showIcon: "toolbar", overflow: "never", click: ethis.viewModel.goToLeadEnrollmentScreen },
                        { id: "leadPrintButton", type: "button", text: "Print", icon: "print", showIcon: "toolbar", overflow: "never"  },
                        { id: "leadToolBarDelete", type: "button", text: "Delete", icon: "minus-outline font-red", showIcon: "toolbar", overflow: "never" }
                    ]
                });

                /*
                 * Kendo windows for Alert in the case that more than one field is show
                 */
                $("#leadToolBarWindow").kendoWindow({
                    actions: ["Close"],
                    visible: false,
                    width: 345,
                    height:200,
                    title: "Select a page"
                });
                ethis.viewModel.disableEnrollBtn(true);
                $("#alertToolBar").kendoToolBar({
                    resizable: false,
                    items: [
                        //regular button
                        { id: "btnQueue", type: "button", text: "Lead Queue", icon: "warning", showIcon: "toolbar", click: ethis.viewModel.goToQueuePage },
                        { id: "btnTask", type: "button", text: "Task", icon: "warning", showIcon: "toolbar", click: ethis.viewModel.goToLeadTaskScreen },
                        { id: "btnAppt", type: "button", text: "Appointments", icon: "warning", showIcon: "toolbar", click: ethis.viewModel.goToLeadScheduleScreen },
                    ]
                });


                // Call the method to read alert info from server
                ethis.viewModel.getFromServerFlagInformation();
                // Run each 3 minutes...
                setInterval(ethis.viewModel.getFromServerFlagInformation, 180000);

            }
            catch (e) {
                alert(e.message + "/n" + e.stack);
            }

        }

    }
} 