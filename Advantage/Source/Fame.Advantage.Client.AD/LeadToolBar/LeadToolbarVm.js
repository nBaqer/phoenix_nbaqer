var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AD;
(function (AD) {
    var LeadToolbarVm = (function (_super) {
        __extends(LeadToolbarVm, _super);
        function LeadToolbarVm() {
            _super.call(this);
            this.db = new AD.LeadInfoBarDb();
            sessionStorage.setItem(AD.X_FLAG_LEAD, "0");
            sessionStorage.setItem(AD.X_FLAG_TASK, "0");
            sessionStorage.setItem(AD.X_FLAG_APPO, "0");
        }
        LeadToolbarVm.prototype.goToAlarmPage = function () {
            var wnd = $("#leadToolBarWindow").data("kendoWindow");
            var sume = 0;
            sume += Number(sessionStorage.getItem(AD.X_FLAG_LEAD));
            sume += Number(sessionStorage.getItem(AD.X_FLAG_APPO));
            sume += Number(sessionStorage.getItem(AD.X_FLAG_TASK));
            if (sume > 1) {
                var toolbar_1 = $("#alertToolBar").data("kendoToolBar");
                toolbar_1.enable("#btnQueue", (sessionStorage.getItem(AD.X_FLAG_LEAD) === "1"));
                toolbar_1.enable("#btnTask", (sessionStorage.getItem(AD.X_FLAG_TASK) === "1"));
                toolbar_1.enable("#btnAppt", (sessionStorage.getItem(AD.X_FLAG_APPO) === "1"));
                wnd.open();
                wnd.center();
            }
            if (sume === 1) {
                if (sessionStorage.getItem(AD.X_FLAG_TASK) === "1") {
                    this.goToLeadTaskScreen();
                    wnd.close();
                    return;
                }
                if (sessionStorage.getItem(AD.X_FLAG_APPO) === "1") {
                    this.goToLeadScheduleScreen();
                    wnd.close();
                    return;
                }
                sessionStorage.setItem(AD.XSUB_MENU_SELECTION, AD.XQUEUE_PAGE);
                window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/ALeadQueue.aspx?RESID=825&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=LeadQueue&mod=AD");
            }
        };
        LeadToolbarVm.prototype.goToQueuePage = function () {
            sessionStorage.setItem(AD.XSUB_MENU_SELECTION, AD.XQUEUE_PAGE);
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/ALeadQueue.aspx?RESID=825&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=LeadQueue&mod=AD");
        };
        LeadToolbarVm.prototype.goToLeadInfoScreen = function () {
            sessionStorage.setItem(AD.XLEAD_NEW, "false");
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&mod=AD");
        };
        LeadToolbarVm.prototype.goToLeadQuickScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadQuick.aspx?RESID=268&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Quick&mod=AD");
        };
        LeadToolbarVm.prototype.goToLeadNotesScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadNotes.aspx?RESID=456&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Notes&mod=AD");
        };
        LeadToolbarVm.prototype.goToLeadRequirementScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/leadRequirements.aspx?RESID=826&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Requirements&mod=AD");
        };
        LeadToolbarVm.prototype.goToLeadHistoryScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadHistory.aspx?RESID=000&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=History&mod=AD");
        };
        LeadToolbarVm.prototype.goToLeadEducationScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadEducation.aspx?RESID=145&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Education&mod=AD");
        };
        LeadToolbarVm.prototype.goToSPriorWorkScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadPriorWork.aspx?RESID=146&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=PriorWorks&mod=AD");
        };
        LeadToolbarVm.prototype.goToLeadExtracurricularScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadExtracurricular.aspx?RESID=148&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Extracurricular&mod=AD");
        };
        LeadToolbarVm.prototype.goToSkillScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadSkills.aspx?RESID=147&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Skills&mod=AD");
        };
        LeadToolbarVm.prototype.goToLeadEnrollmentScreen = function () {
            window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadEnrollment.aspx?RESID=174&cmpId=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=Enrollment&mod=AD");
        };
        LeadToolbarVm.prototype.goToEmailPage = function () {
            var leadElement = document.getElementById('leadId');
            var leadGuid = leadElement.value;
            ad.OPEN_TASK_RAD_EMAIL_WINDOW_WITH_ENTITY(leadGuid, 4);
        };
        LeadToolbarVm.prototype.goToLeadTaskScreen = function () {
            var leadElement = document.getElementById('leadId');
            var leadGuid = leadElement.value;
            ad.OPEN_TASK_RAD_TASK_WINDOW_WITH_ENTITY(leadGuid, 4);
            var wnd = $("#leadToolBarWindow").data("kendoWindow");
            wnd.close();
        };
        LeadToolbarVm.prototype.goToLeadScheduleScreen = function () {
            var leadElement = document.getElementById('leadId');
            var leadGuid = leadElement.value;
            ad.OPEN_TASK_RAD_SCHEDULE_WINDOW_WITH_ENTITY(leadGuid, 4);
            var wnd = $("#leadToolBarWindow").data("kendoWindow");
            wnd.close();
        };
        LeadToolbarVm.prototype.getFromServerFlagInformation = function () {
            var command = 0;
            try {
                $("body").css("cursor", "wait");
                var db1 = new AD.LeadToolBarDb();
                var userId = $("#hdnUserId").val();
                db1.getFromServerFlagInformation(XMASTER_GET_CURRENT_CAMPUS_ID, userId, command, this)
                    .done(function (msg) {
                    if (msg != undefined) {
                        try {
                            if ((msg.FlagLead || msg.FlagAppointment || msg.FlagTask) === false) {
                                var toolbar_2 = $("#LeadToolbar").data("kendoToolBar");
                                toolbar_2.enable("#leadToolBarAlert", false);
                            }
                            else {
                                var toolbar_3 = $("#LeadToolbar").data("kendoToolBar");
                                toolbar_3.enable("#leadToolBarAlert", true);
                            }
                            sessionStorage.setItem(AD.X_FLAG_LEAD, (msg.FlagLead) ? "1" : "0");
                            sessionStorage.setItem(AD.X_FLAG_TASK, (msg.FlagTask) ? "1" : "0");
                            sessionStorage.setItem(AD.X_FLAG_APPO, (msg.FlagAppointment) ? "1" : "0");
                            $("body").css("cursor", "default");
                        }
                        catch (e) {
                            $("body").css("cursor", "default");
                            alert(e.message);
                        }
                    }
                });
            }
            catch (e) {
                $("body").css("cursor", "default");
                alert(e.message);
            }
        };
        LeadToolbarVm.prototype.goToSetQueueLead = function (e) {
            try {
                var command = (e.id === "btnPriorLead") ? 1 : 2;
                var userId = $("#hdnUserId").val();
                var campusId = XMASTER_GET_CURRENT_CAMPUS_ID;
                var control = document.getElementById('leadId');
                var leadId = control.value;
                $("body").css("cursor", "wait");
                var db1 = new AD.LeadToolBarDb();
                db1.setFromServerQueueLead(campusId, userId, leadId, command, this)
                    .done(function (msg) {
                    if (msg != undefined) {
                        try {
                            sessionStorage.setItem("NewLead", "false");
                            var url = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "AD/AleadInfoPage.aspx?RESID=170&NewEnt=1&cmpId=" + campusId;
                            window.location.assign(url);
                            $("body").css("cursor", "default");
                        }
                        catch (e) {
                            $("body").css("cursor", "default");
                            alert(e.message);
                        }
                    }
                })
                    .fail(function (msg) {
                    $("body").css("cursor", "default");
                    ad.SHOW_DATA_SOURCE_ERROR(msg);
                });
            }
            catch (e) {
                $("body").css("cursor", "default");
                alert(e.message);
            }
        };
        ;
        return LeadToolbarVm;
    }(kendo.Observable));
    AD.LeadToolbarVm = LeadToolbarVm;
})(AD || (AD = {}));
//# sourceMappingURL=LeadToolbarVm.js.map