var AD;
(function (AD) {
    var LeadToolbar = (function () {
        function LeadToolbar() {
            try {
                this.viewModel = new AD.LeadToolbarVm();
                var ethis = this;
                var toolbar = $("#LeadToolbar").kendoToolBar({
                    resizable: true,
                    items: [
                        { id: "leadToolBarNew", type: "button", spriteCssClass: "tb-new", text: "New", showIcon: "toolbar", overflow: "never" },
                        { id: "leadToolBarSave", type: "button", text: "Save", spriteCssClass: "tb-save", showIcon: "toolbar", overflow: "never" },
                        { id: "leadToolBarAlert", type: "button", text: "Alert", spriteCssClass: "tb-alert", showIcon: "toolbar", click: ethis.viewModel.goToAlarmPage, overflow: "never" },
                        { id: "btn04", type: "button", text: "Quick Lead", spriteCssClass: "tb-plus", showIcon: "toolbar", click: ethis.viewModel.goToLeadQuickScreen, overflow: "never" },
                        { id: "btnPriorLead", type: "button", text: "Prior Lead", spriteCssClass: "tb-arrow-left", showIcon: "toolbar", showText: "overflow", click: ethis.viewModel.goToSetQueueLead, overflow: "never" },
                        { id: "btn06", type: "button", text: "Lead", click: ethis.viewModel.goToLeadInfoScreen, overflow: "never" },
                        { id: "btnNextLead", type: "button", text: "Next Lead", spriteCssClass: "tb-arrow-right", showIcon: "toolbar", showText: "overflow", click: ethis.viewModel.goToSetQueueLead, overflow: "never" },
                        { id: "btn08", type: "button", text: "Task", spriteCssClass: "tb-redcheckmark", showIcon: "toolbar", click: ethis.viewModel.goToLeadTaskScreen, overflow: "never" },
                        { id: "btn09", type: "button", text: "Appt", spriteCssClass: "tb-event", showIcon: "toolbar", click: ethis.viewModel.goToLeadScheduleScreen, overflow: "never" },
                        { id: "btn10", type: "button", text: "Email", spriteCssClass: "tb-email", showIcon: "toolbar", click: ethis.viewModel.goToEmailPage, overflow: "never" },
                        { id: "btn11", type: "button", text: "Notes", spriteCssClass: "tb-notepad", showIcon: "toolbar", click: ethis.viewModel.goToLeadNotesScreen, overflow: "never" },
                        { id: "btn12", type: "button", text: "Requirements", spriteCssClass: "tb-requirement", showIcon: "toolbar", click: ethis.viewModel.goToLeadRequirementScreen, overflow: "never" },
                        { id: "btn13", type: "button", text: "History", spriteCssClass: "tb-books", showIcon: "toolbar", click: ethis.viewModel.goToLeadHistoryScreen, overflow: "never" },
                        { id: "btn14", type: "button", text: "Prior Ed", spriteCssClass: "tb-prior-ed", showIcon: "toolbar", click: ethis.viewModel.goToLeadEducationScreen, overflow: "never" },
                        { id: "btn15", type: "button", text: "Prior Work", spriteCssClass: "tb-priorwork", showIcon: "toolbar", click: ethis.viewModel.goToSPriorWorkScreen, overflow: "never" },
                        { id: "btn16", type: "button", text: "Extracurricular", spriteCssClass: "tb-extracurricular", showIcon: "toolbar", enable: true, click: ethis.viewModel.goToLeadExtracurricularScreen, overflow: "never" },
                        { id: "btn17", type: "button", text: "Skills", spriteCssClass: "tb-skill", showIcon: "toolbar", click: ethis.viewModel.goToSkillScreen, overflow: "never" },
                        { id: "btn18", type: "button", text: "Enroll", spriteCssClass: "tb-greenCheckMark", showIcon: "toolbar", overflow: "never", click: ethis.viewModel.goToLeadEnrollmentScreen },
                        { id: "leadPrintButton", type: "button", text: "Print", spriteCssClass: "tb-printer-Icon-16x16", showIcon: "toolbar", overflow: "never" },
                        { id: "leadToolBarDelete", type: "button", text: "Delete", spriteCssClass: "tb-delete", showIcon: "toolbar", overflow: "never" }
                    ]
                });
                $("#leadToolBarWindow").kendoWindow({
                    actions: ["Close"],
                    visible: false,
                    width: 345,
                    height: 200,
                    title: "Select a page"
                });
                $("#alertToolBar").kendoToolBar({
                    resizable: false,
                    items: [
                        { id: "btnQueue", type: "button", text: "Lead Queue", spriteCssClass: "tb-alert", showIcon: "toolbar", click: ethis.viewModel.goToQueuePage },
                        { id: "btnTask", type: "button", text: "Task", spriteCssClass: "tb-alert", showIcon: "toolbar", click: ethis.viewModel.goToLeadTaskScreen },
                        { id: "btnAppt", type: "button", text: "Appointments", spriteCssClass: "tb-alert", showIcon: "toolbar", click: ethis.viewModel.goToLeadScheduleScreen },
                    ]
                });
                ethis.viewModel.getFromServerFlagInformation();
                setInterval(ethis.viewModel.getFromServerFlagInformation, 180000);
            }
            catch (e) {
                alert(e.message + "/n" + e.stack);
            }
        }
        return LeadToolbar;
    }());
    AD.LeadToolbar = LeadToolbar;
})(AD || (AD = {}));
//# sourceMappingURL=LeadToolbar.js.map