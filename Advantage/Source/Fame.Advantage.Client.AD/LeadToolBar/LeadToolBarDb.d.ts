declare module AD {
    class LeadToolBarDb {
        getFromServerFlagInformation(campusId: string, userId: string, command: number, mcontext: any): JQueryXHR;
        setFromServerQueueLead(campusId: string, userId: string, leadId: string, command: number, mcontext: LeadToolbarVm): JQueryXHR;
    }
}
