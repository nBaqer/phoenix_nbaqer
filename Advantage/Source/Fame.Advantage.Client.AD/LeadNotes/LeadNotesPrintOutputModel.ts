﻿module AD {

    export interface ILeadNotesPrintOutputModel
    {
        campusName: string;
        moduleTitle: string;
        leadFullName: string;
        studentFullName: string;
        status: string;
        admissionRep: string;
        dateAssigned: string;
        programVersion: string;
        expectedStart: string;
        studentId: string;
        startDate: string;
        gradDate: string;
        leadNotesPrintList: ILeadNotePrint[];


}

    export class LeadNotesPrintOutputModel implements ILeadNotesPrintOutputModel
    {
        campusName: string;
        moduleTitle: string;
        leadFullName: string;
        studentFullName: string;
        status: string;
        admissionRep: string;
        dateAssigned: string;
        programVersion: string;
        expectedStart: string;
        studentId: string;
        startDate: string;
        gradDate: string;
        leadNotesPrintList: ILeadNotePrint[];


    }
    export interface ILeadNotePrint  {
        date: string;
        module: string;
        source: string;
        type: string;
        field: string;
        note: string;
        user: string;
     }   
       
    export class LeadNotePrint implements ILeadNotePrint {
        date: string;
        module: string;
        source: string;
        type: string;
        field: string;
        note: string;
        user: string;
    }   

} 