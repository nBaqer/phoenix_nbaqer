declare module AD {
    class LeadNotesBo {
        dataSource: kendo.data.DataSource;
        dddataSource: kendo.data.DataSource;
        constructor();
        addNewRow(): (ev: UIEvent) => any;
        getLeadNotesInfo(moduleCode: string, command: number, leadid: string): kendo.data.DataSource;
        getTypeNotesEditor(container: any, options: any): void;
        textareaEditor(container: any, options: any): void;
        controlCommandEditButton(e: any): void;
    }
}
