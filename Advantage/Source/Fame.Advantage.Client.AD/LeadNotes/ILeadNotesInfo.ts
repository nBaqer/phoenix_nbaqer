﻿module AD {
    
    /*
     * Notes interface
     */
    export interface INotes {
        
 // ReSharper disable InconsistentNaming
        /// <summary>
        /// ID of the notes entered in the Lead Notes Page
        /// The other notes are not editable. 
        /// </summary>
        IdNote: number;

        /// <summary>
        /// The Date of emission or change
        /// </summary>
        NoteDate: Date;

        /// <summary>
        /// The module were capture the notes
        /// </summary>
        ModuleName: string;

        /// <summary>
        /// The Page where wrote the notes
        /// </summary>
        Source: string;

        /// <summary>
        /// The type of notes. free text
        /// </summary>
        Type: string;

        /// <summary>
        /// If the notes is confidential or not
        /// 0 no confidential 1: confidential
        /// </summary>
        ///IsConfidential:number;

        /// <summary>
        /// The field caption in the page where the notes was wrote.
        /// </summary>
        Field: string;

        /// <summary>
        /// The page field Id 
        /// </summary>
        PageFieldId: number;

        /// <summary>
        /// The text of the notes
        /// </summary>
        NoteText: string;

        /// <summary>
        /// The people that wrote or modified the notes
        /// </summary>
        UserName: string;

        /// <summary>
        /// Used to post User Id to server
        /// </summary>
        UserId: string;
    }

    /// <summary>
    /// Input filter class to command the service
    /// </summary>
    export interface ILeadNotesInputInfo {
        /// <summary>
        /// The command to be used to get different operation
        /// 1: Get all notes related to the lead
        /// </summary>
        Command: number;

        /// <summary>
        /// The lead ID that the notes refers
        /// </summary>
        LeadId: string;

        /// <summary>
        /// Module Code
        /// AD for Admission
        /// </summary>
        ModuleCode: string;

        /// <summary>
        /// The user Id
        /// </summary>
        UserId: string;

         /// <summary>
        /// The campus Id
        /// </summary>
        CampusId: string;
    }

   
    /* 
     * Lead notes message interface
    */
    export interface ILeadNotesInfo {
        
        /// <summary>
        /// List of notes for a determinate lead
        /// </summary>
        NotesList: INotes[];

        /// <summary>
        /// List of Advantage modules items
        /// </summary>
        ModulesList: IDropDownItem[];

        /// <summary>
        /// List of Type of Notes items
        /// </summary>
        ///TypeNotesList: IDropDownItem[];

        /// The command to be used to post info
        /// 1: update Notes
        Filter: ILeadNotesInputInfo;
    }

// ReSharper restore InconsistentNaming




} 