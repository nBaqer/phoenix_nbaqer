﻿
module AD
{
     // This is created in Lead Master Page.
    declare var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL: string;
    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;

    export class LeadNotes {

        public bo: LeadNotesBo;
        public moduleInit: string;
        public leadId: string;
        public notesValidator: kendo.ui.Validator;
        public printInfo: ILeadNotesPrintOutputModel = new LeadNotesPrintOutputModel();

        constructor(pLeadId:string, pModuleCode:string) {
            this.leadId = pLeadId;
            this.moduleInit = pModuleCode;
            let body = $("body");
            body.prop("id", "notesBody");
            this.notesValidator = MasterPage.ADVANTAGE_VALIDATOR("notesBody");
            MasterPage.Common.enableDisableBtns(false);
        }
        initOperation() {
            try {

                //var that = this;
                this.bo = new LeadNotesBo();
                var grid = $("#notesGrid").kendoGrid({
                    pdf: {
                        allPages: true
                    },
                    dataSource: [],
                    pageable: true,
                    sortable: true,
                    columns: [
                        // { field: "Id", title: "", width: "1px" },
                        {
                            field: "NoteDate"
                            , title: "Date"
                            , width: "100px"
                            , template: "#=  (NoteDate == null)? '' : kendo.toString(kendo.parseDate(NoteDate, 'yyyy-MM-dd'), 'MM/dd/yy hh:mm:ss tt') #"
                        },

                        {
                            field: "ModuleName",
                            title: "Module",
                            width: "100px"
                            // editor: this.bo.getModuleValuesEditor,
                            // template: "#=Group.Description#"
                        },
                        {
                            field: "Source",
                            title: "Source",
                            width: "110px"
                            //editor: this.bo.getSourceValuesEditor,
                            //template: "#=Source.Description#"
                        },
                        {
                            field: "Type",
                            title: "Type",
                            width: "100px",
                            editor: this.bo.getTypeNotesEditor,
                            template: kendo.template($("#notesTypeGridStyleTemplate").html())
                        },
                        {
                            field: "Field",
                            title: "Field",
                            width: "100px"
                            //editor: this.bo.getTypeNotesEditor,
                            //template: "#=Field.Description#"
                        },
                        {
                            field: "NoteText",
                            title: "Note",

                            editor: this.bo.textareaEditor,
                            template: "#=NoteText#"
                        },

                        {
                            field: "UserName",
                            title: "User",
                            width: "140px"

                        },

                        { command: [{ name: "edit", text: " " }], title: "Edit", width: "50px" }],
                    editable: "popup",
                    edit: e => {
                        var window: kendo.ui.Window = e.container.data("kendoWindow") as any;
                        if (e.model.isNew()) {
                            window.title("Enter a New Note");
                        } else {
                            window.title("Edit the Note");
                        }

                        this.setPopupDimensions(e);

                        // Set limitation and signal required fields
                        let label: any = e.container.find("label[for='Type']");
                        label[0].innerHTML = label[0].innerHTML + "<span style='color:red'> *</span>";
                        let label1: any = e.container.find("label[for='NoteText']");
                        label1[0].innerHTML = label1[0].innerHTML + "<span style='color:red'> *</span>";
                        e.container.find("input[name='Description']").attr('maxlength', "2000");
                        // Validator
                        e.container.find("a.k-grid-update").bind("click",
                            () => {
                                return this.notesValidator.validate();
                            });
                    },
                    dataBound: (eb) => {
                        this.bo.controlCommandEditButton(eb);
                    }
                } as any).data("kendoGrid") as kendo.ui.Grid;

                //Drop down list for modules
                $("#notesModulesDdl").kendoDropDownList(({
                    dataSource: this.bo.dddataSource,
                    dataTextField: "Description",
                    dataValueField: "ID",
                    dataBound: eb => {
                        eb.sender.value(this.moduleInit);
                        this.getNotesFromServer(eb, this);
                    },
                    change: (ex) => {
                        this.getNotesFromServer(ex, this);
                    }
                }) as any);

                // Add AddRow handler
                var newRow = document.getElementById("notesPlusImage");
                newRow.addEventListener('click', this.bo.addNewRow, false);

                // Add Print Event Button
                //$("#notesPrintbutton").click(() => {
                //    var grid: any = $("#notesGrid").data("kendoGrid");
                //    grid.saveAsPDF();
                //});
                /* Hooker for Print Button */
                $("#notesPrintbutton").click(() =>
                {
                    //Get the user id and store it in the session
                    var userId: string = $("#hdnUserId").val();
                    // var printInfo: ILeadNotesPrintOutputModel = new LeadNotesPrintOutputModel();
                    var printPage: string
                    switch (this.moduleInit)
                    {
                        case "AD":
                            this.printInfo = this.fillLeadInfoHeaderToPrint(this.printInfo);
                            this.printInfo = this.fillLeadInfoBodyToPrint(this.printInfo);
                            sessionStorage.removeItem("PRINT_INFO");
                            sessionStorage.setItem("PRINT_INFO", JSON.stringify(this.printInfo));
                            sessionStorage.setItem("USER_ID", userId);
                            printPage = "AD/LeadNotesPrint.aspx";
                            var url: string = this.callPrintPage(this.moduleInit, printPage);
                            window.open(url, "_newtab");
                            break;
                        case "AR":
                            this.printInfo = this.fillStudentInfoHeaderToPrint(this.printInfo);
                            this.printInfo = this.fillLeadInfoBodyToPrint(this.printInfo);
                            sessionStorage.removeItem("PRINT_INFO");
                            sessionStorage.setItem("PRINT_INFO", JSON.stringify(this.printInfo));
                            sessionStorage.setItem("USER_ID", userId);
                            printPage = "SY/StudentNotesPrint.aspx";
                            var url: string = this.callPrintPage(this.moduleInit, printPage)
                            window.open(url, "_newtab");
                            break;
                        default:
                            //message 
                            MasterPage.SHOW_ERROR_WINDOW(X_CALLED_FROM_INCORRECT_MODULE);
                            break;
                    }
                });
            } catch (e)
            {
                $("body").css("cursor", "default");
                MasterPage.SHOW_ERROR_WINDOW(`<p>${e.message}</p>${e.stack}`);
            }
        }

        // Modify the pop-up dimension
        setPopupDimensions(ev)
        {
            //  window.setTimeout(() => {
            ($(".k-edit-form-container").parent().width(670).height(400).data("kendoWindow") as any).center();
            //  }, 100);
        }
        /*
         * Get the Notes information from Server 
         */
        getNotesFromServer(e: any, that: any): void
        {

            // Set the data-source for control
            var grid: kendo.ui.Grid = $("#notesGrid").data("kendoGrid") as any;
            var dllModule: kendo.ui.DropDownList = e.sender; // $("#notesModulesDdl").data("kendoDropDownList") as any;
            var infoFromServer = that.bo.getLeadNotesInfo(dllModule.value(), 1, that.leadId);
            grid.setDataSource(infoFromServer);
            $.when(grid.dataSource.fetch())
                .then(() =>
                {
                    grid.refresh();
                });
        }
        /* 
         * Fill the object to be used for Print Info Lead Page
         * This object is stored in session cache
         */
        fillLeadInfoHeaderToPrint(p: ILeadNotesPrintOutputModel): ILeadNotesPrintOutputModel
        {
            p.campusName = sessionStorage.getItem("CAMPUS_NAME");
            p.moduleTitle = $("#notesModulesDdl").data("kendoDropDownList").text();
            p.leadFullName = $("#infoBarTableFullname").text();
            p.status = $("#infoBarTableStatus").text();
            p.admissionRep = $("#infoBarTableAdmissionRep").text();
            p.dateAssigned = $("#infoBarTableDateAssigned").text();
            p.programVersion = $("#infoBarTableProgramVersion").text();
            p.expectedStart = $("#infoBarTableExpectedStart").text().toString();

            return p;
        }
        fillStudentInfoHeaderToPrint(p: ILeadNotesPrintOutputModel): ILeadNotesPrintOutputModel
        {
            p.campusName = $("#hdnInfoBarCampusDescrip").val();
            p.moduleTitle = $("#notesModulesDdl").data("kendoDropDownList").text(); 
            if (p.moduleTitle === "All") 
            {
                p.moduleTitle = "ALL NOTES";
            }
            p.studentFullName = $("#lblInfoBarNameValue").text();
            p.programVersion = $("#lblProgramVersionValue").text();
            p.status = $("#lblStatusValue").text();
            p.studentId = $("#lblIdentifierValue").text();
            p.startDate = $("#lblInfoBarStartDateValue").text();
            p.gradDate = $("#lblGradDateValue").text();
                   
            return p;
        }
        /*
         * fillInfoBodyToPrint
         */
        fillLeadInfoBodyToPrint(p: ILeadNotesPrintOutputModel): ILeadNotesPrintOutputModel
        {
            var leadNotesGrid: any = $("#notesGrid").data("kendoGrid") as kendo.ui.Grid;
            var leadNotesDs: any = leadNotesGrid.dataSource.data();
            var leadNotesArray = new Array<ILeadNotePrint>();
            // 
            for (var i = 0; i < leadNotesDs.length; i++)
            {
                let info: LeadNotePrint = new LeadNotePrint();
                if (leadNotesDs[i].Type != "Confidential")
                {
                    info.date = ad.FORMAT_DATE_TO_MM_DD_YYYY(leadNotesDs[i].NoteDate)
                        + " "
                        + ad.FORMAT_TIME_AMPM(leadNotesDs[i].NoteDate);

                    info.module = leadNotesDs[i].ModuleName;
                    info.source = leadNotesDs[i].Source;
                    info.type = leadNotesDs[i].Type;
                    info.field = leadNotesDs[i].Field;
                    info.note = leadNotesDs[i].NoteText;
                    info.user = leadNotesDs[i].UserName;
                    leadNotesArray.push(info);
                }
            }

            p.leadNotesPrintList = leadNotesArray;

            return p;
        }
        callPrintPage(pModuleCode: string, printPage: string): string
        {
            var url: string = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL
                + printPage
                + "?resid=206"
                + "&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID
                + "&desc=PrintProfile"
                + "&mod=" + pModuleCode;
                //+ "&leadId=" + leadId;

            return url;
        }
    }
} 