var AD;
(function (AD) {
    var LeadNotesBo = (function () {
        function LeadNotesBo() {
            this.dddataSource = new kendo.data.DataSource({
                transport: {
                    read: function (options) {
                        $.ajax({
                            url: AD.XGET_SERVICE_LAYER_NOTES,
                            type: "GET",
                            dataType: "json",
                            data: {
                                LeadId: "00000000-0000-0000-0000-000000000000",
                                Command: 2
                            },
                            success: function (result) {
                                result.ModulesList.unshift({ Description: "All", ID: "ALL" });
                                options.success(result.ModulesList);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    }
                }
            });
        }
        LeadNotesBo.prototype.addNewRow = function () {
            var grid = $("#notesGrid").data("kendoGrid");
            grid.addRow();
            return null;
        };
        LeadNotesBo.prototype.getLeadNotesInfo = function (moduleCode, command, leadid) {
            var userId = $("#hdnUserId").val();
            this.dataSource = new kendo.data.DataSource({
                transport: {
                    read: function (options) {
                        $.ajax({
                            url: AD.XGET_SERVICE_LAYER_NOTES,
                            type: "GET",
                            dataType: "json",
                            data: {
                                ModuleCode: moduleCode,
                                LeadId: leadid,
                                Command: command,
                                CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                UserId: XMASTER_PAGE_USER_OPTIONS_USERID
                            },
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    update: function (options) {
                        var info = new AD.LeadNotesInfo();
                        info.NotesList = new Array();
                        var notes = new AD.Notes();
                        notes.IdNote = options.data.models[0].IdNote;
                        notes.ModuleName = options.data.models[0].ModuleName;
                        notes.NoteDate = options.data.models[0].NoteDate;
                        notes.NoteText = options.data.models[0].NoteText;
                        notes.Source = options.data.models[0].Source;
                        notes.Field = options.data.models[0].Source;
                        notes.UserId = options.data.models[0].UserId;
                        notes.UserName = options.data.models[0].UserName;
                        notes.Type = options.data.models[0].Type;
                        var inputInfo = new AD.LeadNotesInputInfo();
                        inputInfo.Command = 1;
                        inputInfo.LeadId = leadid;
                        inputInfo.UserId = userId;
                        info.Filter = inputInfo;
                        info.NotesList.push(notes);
                        $.ajax({
                            type: "POST",
                            dataType: "text",
                            url: AD.XPOST_SERVICE_LAYER_NOTES,
                            data: JSON.stringify(info),
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    create: function (options) {
                        var info = new AD.LeadNotesInfo();
                        info.NotesList = new Array();
                        var notes = new AD.Notes();
                        notes.IdNote = options.data.models[0].IdNote;
                        notes.ModuleName = options.data.models[0].ModuleName;
                        notes.NoteDate = options.data.models[0].NoteDate;
                        notes.NoteText = options.data.models[0].NoteText;
                        notes.Source = options.data.models[0].Source;
                        notes.Field = options.data.models[0].Field;
                        notes.PageFieldId = options.data.models[0].PageFieldId;
                        notes.UserId = XMASTER_PAGE_USER_OPTIONS_USERID;
                        notes.UserName = options.data.models[0].UserName;
                        notes.Type = options.data.models[0].Type;
                        var inputInfo = new AD.LeadNotesInputInfo();
                        inputInfo.Command = 2;
                        inputInfo.LeadId = leadid;
                        inputInfo.UserId = XMASTER_PAGE_USER_OPTIONS_USERID;
                        info.Filter = inputInfo;
                        info.NotesList.push(notes);
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: AD.XPOST_SERVICE_LAYER_NOTES,
                            data: JSON.stringify(info),
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                                ad.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                        return null;
                    }
                },
                batch: true,
                pageSize: 12,
                schema: {
                    data: "NotesList",
                    total: function (response) { return response.NotesList.length; },
                    model: {
                        id: "IdNote",
                        fields: {
                            IdNote: { editable: false, nullable: false },
                            ModuleName: {
                                editable: false,
                                validation: { required: true },
                                defaultValue: $("#notesModulesDdl").data("kendoDropDownList").text()
                            },
                            NoteText: {
                                type: "string",
                                validation: { required: true }
                            },
                            Type: { type: "string"
                            },
                            Source: {
                                editable: false,
                                type: "string",
                                defaultValue: "Notes"
                            },
                            UserName: {
                                editable: false,
                                type: "string",
                                defaultValue: XMASTER_PAGE_USER_OPTIONS_USERNAME
                            },
                            UserId: { editable: false },
                            Field: {
                                editable: false,
                                type: "string",
                                defaultValue: "Note"
                            },
                            PageFieldId: {
                                editable: false,
                                defaultValue: 1
                            },
                            NoteDate: {
                                editable: false,
                                type: "Date",
                                defaultValue: new Date()
                            }
                        }
                    }
                }
            });
            return this.dataSource;
        };
        LeadNotesBo.prototype.getTypeNotesEditor = function (container, options) {
            $('<input required name="Note Type" data-text-field="text" data-value-field="text"  data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                dataSource: [{ text: "Notes" }, { text: "Confidential" }],
                optionLabel: "Select"
            });
        };
        LeadNotesBo.prototype.textareaEditor = function (container, options) {
            $('<textarea name="Note Text" required maxlength="2000" data-bind="value: ' + options.field + '" cols="60" rows="5" ></textarea>')
                .appendTo(container);
        };
        LeadNotesBo.prototype.controlCommandEditButton = function (e) {
            var dataItems = e.sender.dataSource.view();
            for (var j = 0; j < dataItems.length; j++) {
                var userId = dataItems[j].get("UserId");
                var sourceFieldId = dataItems[j].get("PageFieldId");
                var row = e.sender.tbody.find("[data-uid='" + dataItems[j].uid + "']");
                if (userId.toUpperCase() !== XMASTER_PAGE_USER_OPTIONS_USERID.toUpperCase() || sourceFieldId !== 1) {
                    var editButton = row.find(".k-grid-edit");
                    editButton.hide();
                }
            }
        };
        return LeadNotesBo;
    }());
    AD.LeadNotesBo = LeadNotesBo;
})(AD || (AD = {}));
//# sourceMappingURL=LeadNotesBo.js.map