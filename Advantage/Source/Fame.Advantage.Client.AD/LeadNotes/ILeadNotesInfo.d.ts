declare module AD {
    interface INotes {
        IdNote: number;
        NoteDate: Date;
        ModuleName: string;
        Source: string;
        Type: string;
        Field: string;
        PageFieldId: number;
        NoteText: string;
        UserName: string;
        UserId: string;
    }
    interface ILeadNotesInputInfo {
        Command: number;
        LeadId: string;
        ModuleCode: string;
        UserId: string;
        CampusId: string;
    }
    interface ILeadNotesInfo {
        NotesList: INotes[];
        ModulesList: IDropDownItem[];
        Filter: ILeadNotesInputInfo;
    }
}
