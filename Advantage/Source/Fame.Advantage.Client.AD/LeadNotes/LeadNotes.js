var AD;
(function (AD) {
    var LeadNotes = (function () {
        function LeadNotes(leadid, whatModule) {
            this.leadId = leadid;
            this.moduleInit = whatModule;
        }
        LeadNotes.prototype.initOperation = function () {
            var _this = this;
            try {
                this.bo = new AD.LeadNotesBo();
                $("#notesGrid").kendoGrid({
                    pdf: {
                        allPages: true
                    },
                    dataSource: [],
                    pageable: true,
                    sortable: true,
                    columns: [
                        {
                            field: "NoteDate",
                            title: "Date",
                            width: "100px",
                            template: "#=  (NoteDate == null)? '' : kendo.toString(kendo.parseDate(NoteDate, 'yyyy-MM-dd'), 'MM/dd/yy hh:mm:ss tt') #"
                        },
                        {
                            field: "ModuleName",
                            title: "Module",
                            width: "100px"
                        },
                        {
                            field: "Source",
                            title: "Source",
                            width: "110px"
                        },
                        {
                            field: "Type",
                            title: "Type",
                            width: "100px",
                            editor: this.bo.getTypeNotesEditor,
                            template: kendo.template($("#notesTypeGridStyleTemplate").html())
                        },
                        {
                            field: "Field",
                            title: "Field",
                            width: "100px"
                        },
                        {
                            field: "NoteText",
                            title: "Note",
                            editor: this.bo.textareaEditor,
                            template: "#=NoteText#"
                        },
                        {
                            field: "UserName",
                            title: "User",
                            width: "140px"
                        },
                        { command: [{ name: "edit", text: " " }], title: "Edit", width: "50px" }],
                    editable: "popup",
                    edit: function (e) {
                        var window = e.container.data("kendoWindow");
                        if (e.model.isNew()) {
                            window.title("Enter a New Note");
                        }
                        else {
                            window.title("Edit the Note");
                        }
                        _this.setPopupDimensions(e);
                    },
                    dataBound: function (eb) {
                        _this.bo.controlCommandEditButton(eb);
                    }
                });
                $("#notesModulesDdl").kendoDropDownList(({
                    dataSource: this.bo.dddataSource,
                    dataTextField: "Description",
                    dataValueField: "ID",
                    dataBound: function (eb) {
                        eb.sender.value(_this.moduleInit);
                        _this.getNotesFromServer(eb, _this);
                    },
                    change: function (ex) {
                        _this.getNotesFromServer(ex, _this);
                    }
                }));
                var newRow = document.getElementById("notesPlusImage");
                newRow.addEventListener('click', this.bo.addNewRow, false);
                $("#notesPrintbutton").click(function () {
                    var grid = $("#notesGrid").data("kendoGrid");
                    grid.saveAsPDF();
                });
            }
            catch (e) {
                $("body").css("cursor", "default");
                alert(e.message + "/n" + e.stack);
            }
        };
        LeadNotes.prototype.setPopupDimensions = function (ev) {
            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(670).height(400).data("kendoWindow").center();
            }, 100);
        };
        LeadNotes.prototype.getNotesFromServer = function (e, that) {
            var grid = $("#notesGrid").data("kendoGrid");
            var dllModule = e.sender;
            var infoFromServer = that.bo.getLeadNotesInfo(dllModule.value(), 1, that.leadId);
            grid.setDataSource(infoFromServer);
        };
        return LeadNotes;
    }());
    AD.LeadNotes = LeadNotes;
})(AD || (AD = {}));
//# sourceMappingURL=LeadNotes.js.map