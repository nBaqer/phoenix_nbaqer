declare module AD {
    class LeadNotes {
        bo: LeadNotesBo;
        moduleInit: string;
        leadId: string;
        constructor(leadid: string, whatModule: string);
        initOperation(): void;
        setPopupDimensions(ev: any): void;
        getNotesFromServer(e: any, that: any): void;
    }
}
