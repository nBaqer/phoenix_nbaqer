﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
module AD
{

    export class LeadNotesPrint
    {

        public moduleCode: string;
        public leadId: string;
        public info: ILeadNotesPrintOutputModel

        constructor(pModuleCode: string)
        {
            //this.leadId = pEntityId;
            this.moduleCode = pModuleCode;
            //var userId: string = sessionStorage.getItem("USER_ID");
            
            switch (this.moduleCode)
            {
                case "AD":
                    this.printAdmitionNotesHeader();
                    this.printNotesBody()
                    break;
                case "AR":
                    this.printAcademicsNotesHeader();
                    this.printNotesBody()
                    break;
                default:
                    //message 
                    MasterPage.SHOW_ERROR_WINDOW(X_CALLED_FROM_INCORRECT_MODULE);
                    break;
            }
             
            // Call to print or preview
            window.print();
            window.onfocus = () =>
            {
                window.close(); // Close the window
            }
        }
        printAdmitionNotesHeader()
        {
            //var userId: string = sessionStorage.getItem("USER_ID");

            this.info = JSON.parse(sessionStorage.getItem("PRINT_INFO"));
            const unselected: string = "Select";

            // Header Info
            document.getElementById("printCampus").innerHTML = this.info.campusName.toUpperCase();
            var d = new Date();
            document.getElementById("printDateTime").innerHTML = ad.FORMAT_DATE_TO_MM_DD_YYYY(d);

            // Get image from encode64...
            $("#hrLeadFullName").val(this.info.leadFullName);
            $("#hrAdmissionsRep").val(this.info.admissionRep === unselected ? "" : this.info.admissionRep);
            $("#hrStatus").val(this.info.status);
            $("#hrDateAssigned").val(this.info.dateAssigned);
            $("#hrProgramVersion").val(this.info.programVersion === unselected ? "" : this.info.programVersion);
            $("#hrExpectedStartDate").val(this.info.expectedStart);
        }
        printAcademicsNotesHeader()
        {
            //var userId: string = sessionStorage.getItem("USER_ID");

            this.info = JSON.parse(sessionStorage.getItem("PRINT_INFO"));
            const unselected: string = "Select";

            // Header Info
            document.getElementById("printCampus").innerHTML = this.info.campusName.toUpperCase();
            document.getElementById("printModule").innerHTML = this.info.moduleTitle.toUpperCase();
            var d = new Date();
            document.getElementById("printDateTime").innerHTML = ad.FORMAT_DATE_TO_MM_DD_YYYY(d);

            // Get image from encode64...
            $("#hrStudentFullName").val(this.info.studentFullName);
            $("#hrProgramVersion").val(this.info.programVersion);
            $("#hrStudentStatus").val(this.info.status);
            $("#hrStartDate").val(this.info.startDate);
            $("#hrStudentId").val(this.info.studentId);
            $("#hrGradDate").val(this.info.gradDate);
        }
        printNotesBody()
        {
            var rowToInsert: string;
            // Dynamically construct the custom field
            rowToInsert = "<thead class='Header' >"
                + "<tr>"
                + "<th class='Col1' > Date </td>"
                + "<th class='Col2' > Module </td>"
                + "<th class='Col3' > Source </td>"
                + "<th class='Col4' > Type </td>"
                + "<th class='Col5' > Field </td>"
                + "<th class='Col6_Header' > Note </td>"
                + "<th class='Col7' > User </td>"
                + "</tr></thead>";
            $("table#LeadNotes").append(rowToInsert);
            rowToInsert = "";
            rowToInsert = "<tbody class='Body' >";
            var notesList: any = this.info.leadNotesPrintList;
            for (var i = 0; i < notesList.length; i++)
            {
                var template: string = "<tr> "
                    + "<td class='Col1' >" + notesList[i].date.toString() + "</td>"
                    + "<td class='Col2' >" + notesList[i].module + "</td>"
                    + "<td class='Col3' >" + notesList[i].source + "</td>"
                    + "<td class='Col4' >" + notesList[i].type + "</td>"
                    + "<td class='Col5' >" + notesList[i].field + "</td>"
                    + "<td class='Col6' >" + notesList[i].note + "</td>"
                    + "<td class='Col7' >" + notesList[i].user + "</td>"
                    + "</tr>";
                rowToInsert = rowToInsert + template;
            }
            rowToInsert = rowToInsert + "</tbody> ";
            $("table#LeadNotes").append(rowToInsert);
        }
    }
} 