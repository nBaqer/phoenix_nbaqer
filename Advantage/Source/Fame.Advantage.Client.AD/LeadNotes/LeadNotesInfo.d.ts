declare module AD {
    class Notes implements INotes {
        IdNote: number;
        NoteDate: Date;
        ModuleName: string;
        Source: string;
        Type: string;
        Field: string;
        NoteText: string;
        UserName: string;
        UserId: string;
        PageFieldId: number;
    }
    class LeadNotesInputInfo implements ILeadNotesInputInfo {
        Command: number;
        LeadId: string;
        ModuleCode: string;
        UserId: string;
        CampusId: string;
    }
    class LeadNotesInfo implements ILeadNotesInfo {
        NotesList: INotes[];
        ModulesList: IDropDownItem[];
        Filter: ILeadNotesInputInfo;
    }
}
