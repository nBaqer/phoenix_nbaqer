﻿module AD {

    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;
    declare var XMASTER_PAGE_USER_OPTIONS_USERNAME: string;
    declare var XMASTER_PAGE_USER_OPTIONS_USERID: string;
    

    export class LeadNotesBo {

        dataSource: kendo.data.DataSource;
        dddataSource: kendo.data.DataSource;

        constructor() {
            this.dddataSource = new kendo.data.DataSource({
                transport: {
                    read: options => {
                        $.ajax({
                            url: XGET_SERVICE_LAYER_NOTES,
                            type: "GET",
                            dataType: "json",
                            data: {
                                LeadId: "00000000-0000-0000-0000-000000000000",
                                Command: 2
                            },
                            success: result => {
                                result.ModulesList.unshift({ Description: "All", ID: "ALL" });
                                options.success(result.ModulesList);
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    }
                }
            });
        }

        //levelDataSource: kendo.data.DataSource;
        //groupDataSource: kendo.data.DataSource;
 
        addNewRow(): (ev: UIEvent) => any {

            var grid:any = $("#notesGrid").data("kendoGrid");
            grid.addRow();
            return null;
        }

        getLeadNotesInfo(moduleCode: string, command: number, leadid:string): kendo.data.DataSource {

            var userId: string = $("#hdnUserId").val();
            this.dataSource = new kendo.data.DataSource({
                transport: {
                    read: options => {
                        $.ajax({
                            url: XGET_SERVICE_LAYER_NOTES,
                            type: "GET",
                            dataType: "json",
                            data: {
                                ModuleCode: moduleCode,
                                LeadId: leadid,
                                Command: command,
                                CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                                UserId: XMASTER_PAGE_USER_OPTIONS_USERID
                            },
                            success: result => {
                                options.success(result);
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    update: options => {
                        var info: ILeadNotesInfo = new LeadNotesInfo();
                        info.NotesList = new Array<Notes>();
                        var notes: INotes = new Notes();
                        notes.IdNote = options.data.models[0].IdNote;
                        notes.ModuleName = options.data.models[0].ModuleName;
                        notes.NoteDate = options.data.models[0].NoteDate;
                        notes.NoteText = options.data.models[0].NoteText;
                        notes.Source = options.data.models[0].Source;
                        notes.Field = options.data.models[0].Source;
                        notes.UserId = options.data.models[0].UserId;
                        notes.UserName = options.data.models[0].UserName;
                        notes.Type = options.data.models[0].Type;
                        var inputInfo = new LeadNotesInputInfo();
                        inputInfo.Command = 1;
                        inputInfo.LeadId = leadid;
                        inputInfo.UserId = userId;
                        info.Filter = inputInfo;
                        info.NotesList.push(notes);
                        $.ajax({
                            type: "POST",
                            dataType: "text",
                            url: XPOST_SERVICE_LAYER_NOTES,
                            data: JSON.stringify(info),
                            success: result => {
                                options.success(result);
                                let grid = $("#notesGrid").data("kendoGrid") as kendo.ui.Grid;
                                $.when(grid.dataSource.read())
                                    .then(promise => {
                                        grid.dataSource.fetch();
                                        grid.resize(true);
                                        grid.refresh();
                                    });
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    create: options => {
                        var info: ILeadNotesInfo = new LeadNotesInfo();
                        info.NotesList = new Array<Notes>();
                        var notes: INotes = new Notes();
                        notes.IdNote = options.data.models[0].IdNote;
                        notes.ModuleName = options.data.models[0].ModuleName;
                        notes.NoteDate = options.data.models[0].NoteDate;
                        notes.NoteText = options.data.models[0].NoteText;
                        notes.Source = options.data.models[0].Source;
                        notes.Field = options.data.models[0].Field;
                        notes.PageFieldId = options.data.models[0].PageFieldId;
                        notes.UserId = XMASTER_PAGE_USER_OPTIONS_USERID;
                        notes.UserName = options.data.models[0].UserName;
                        notes.Type = options.data.models[0].Type;
                        //notes.Type.Description = options.data.models[0].Type.Description;
                        //notes.Type.Id = options.data.models[0].Type.ID;
                        var inputInfo = new LeadNotesInputInfo();
                        inputInfo.Command = 2;
                        inputInfo.LeadId = leadid;
                        inputInfo.UserId = XMASTER_PAGE_USER_OPTIONS_USERID;
                        info.Filter = inputInfo;
                        info.NotesList.push(notes);
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: XPOST_SERVICE_LAYER_NOTES,
                            data: JSON.stringify(info),
                            success: result => {
                                options.success(result);
                                let grid = $("#notesGrid").data("kendoGrid") as kendo.ui.Grid;
                                $.when(grid.dataSource.read())
                                    .then(promise => {
                                        grid.resize(true);
                                        grid.refresh();
                                    });
                                   
                            },
                            error: result => {
                                options.error(result);
                                MasterPage.SHOW_DATA_SOURCE_ERROR(result);
                            }
                        });
                    },
                    parameterMap: (options, operation) => {
                        if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                        return null;
                    }
                },
                batch: true,
                pageSize: 12,
                schema: {
                    data: "NotesList",
                    total: response => response.NotesList.length,
                    model: {
                        id: "IdNote",
                        fields: {
                            IdNote: { editable: false, nullable: false },
                            ModuleName: {
                                editable: false,
                                validation: { required: true },
                                defaultValue: $("#notesModulesDdl").data("kendoDropDownList").text()

                            },
                            NoteText: {
                                type: "string",
                                validation: {required: true}
                            },
                            Type: { type: "string"
                                //validation: { required: true },
                                //editable: false
                                //defaultValue: ""
                            },

                            //IsConfidential: {
                            //    type: "number",
                            //    editable: false

                            //},
                            Source: {
                                editable: false,
                                type: "string",
                                defaultValue: "Notes"
                            },
                            UserName: {
                                editable: false,
                                type: "string",
                                defaultValue: XMASTER_PAGE_USER_OPTIONS_USERNAME
                            },
                            UserId: { editable: false },

                            Field: {
                                editable: false,
                                type: "string",
                                defaultValue: "Note"
                            },
                            PageFieldId: {
                                editable:false,
                                defaultValue: 1
                            },

                            NoteDate: {
                                editable: false,
                                type: "Date",
                                defaultValue: new Date()
                            }

                        }
                    }
                }
            }
            );
            return this.dataSource;
        }

        /*
         * Type Notes Grid Drop-down Editor
         */
        getTypeNotesEditor(container, options) {
            $('<input required name="Note Type" data-text-field="text" data-value-field="text"  data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    dataSource: [{text:"Notes"}, {text:"Confidential"}],
                    //{
                    //    transport: {
                    //        read: options => {
                    //            $.ajax({
                    //                url: XGET_SERVICE_LAYER_NOTES,
                    //                type: "GET",
                    //                dataType: "json",
                    //                data: {
                    //                    CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                    //                    LeadId: "00000000-0000-0000-0000-000000000000",
                    //                    Command: 3
                    //                },
                    //                success: result => {
                    //                    options.success(result.TypeNotesList);
                    //                },
                    //                error: result => {
                    //                    options.error(result);
                    //                    ad.SHOW_DATA_SOURCE_ERROR(result);
                    //                }
                    //            });
                    //        }
                    //    },
                    //    dataValueField: "ID",
                    //    dataTextField: "Description",
                    //    autoBind: false
                    //},
                    optionLabel: "Select"
                });
        }

        /*
         * Custom textarea Grid Editor
         */
        textareaEditor(container, options) {

            $('<textarea name="Note Text" required maxlength="2000" data-bind="value: ' + options.field + '" cols="60" rows="5" ></textarea>')

                .appendTo(container);

        }

        /*
         * Control if the edit button is enable or not based if the notes owner is the actual user.
         */
        controlCommandEditButton(e) {
            // iterate the data items and apply row styles where necessary
            var dataItems = e.sender.dataSource.view();
            for (var j = 0; j < dataItems.length; j++) {
                var userId = dataItems[j].get("UserId");
                var sourceFieldId = dataItems[j].get("PageFieldId");
                var row = e.sender.tbody.find("[data-uid='" + dataItems[j].uid + "']");
                if (userId.toUpperCase() !== XMASTER_PAGE_USER_OPTIONS_USERID.toUpperCase() || sourceFieldId !== 1 ) {

                    var editButton = row.find(".k-grid-edit");
                    editButton.hide();
                }
            }
        }

    }
} 