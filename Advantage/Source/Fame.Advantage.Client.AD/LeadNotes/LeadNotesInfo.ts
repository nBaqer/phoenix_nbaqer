﻿module AD {
    
    // ReSharper disable InconsistentNaming 
    export class Notes implements INotes {
        IdNote: number;
        NoteDate: Date;
        ModuleName: string;
        Source: string;
        Type: string;
        Field: string;
        NoteText: string;
        UserName: string;
        UserId: string;
        PageFieldId: number;
        //IsConfidential: number;
    }

    /*
     * Filter to input information in service
     */
    export class LeadNotesInputInfo implements ILeadNotesInputInfo {
        Command: number;
        LeadId: string;
        ModuleCode: string;
        UserId: string;
        CampusId: string;
    }

    export class LeadNotesInfo implements ILeadNotesInfo {
        NotesList: INotes[];
        ModulesList: IDropDownItem[];
        //TypeNotesList: IDropDownItem[];
        Filter: ILeadNotesInputInfo;
    }
// ReSharper restore InconsistentNaming

} 