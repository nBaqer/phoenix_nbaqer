var AD;
(function (AD) {
    var LeadAssignmentDb = (function () {
        function LeadAssignmentDb() {
        }
        LeadAssignmentDb.prototype.admissionRepDataSource = function () {
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: function () {
                            var uri = AD.XGET_SERVICE_LAYER_LEAD_ASSIGMENT_USER_GET;
                            return uri;
                        },
                        dataType: "json",
                        type: "GET",
                        data: {
                            ReturnReps: "true",
                            UserId: "",
                            CampusId: "",
                            Active: ""
                        }
                    }
                }
            });
            return ds;
        };
        LeadAssignmentDb.prototype.admissionReRepDataSource = function () {
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: function () {
                            var uri = AD.XGET_SERVICE_LAYER_LEAD_ASSIGMENT_USER_GET;
                            return uri;
                        },
                        dataType: "json",
                        type: "GET",
                        data: {
                            ReturnReps: "true",
                            UserId: "",
                            CampusId: "",
                            Active: ""
                        }
                    }
                }
            });
            return ds;
        };
        ;
        LeadAssignmentDb.prototype.leadUnassignedDataSource = function () {
            var ds = new kendo.data.DataSource({
                pageSize: 10,
                transport: {
                    read: {
                        url: function (options) {
                            var uri = AD.XGET_SERVICE_LAYER_LEAD_ASSIGMENT_UNASSIGNED_LEADS_GET;
                            delete options.baseUrl;
                            return uri;
                        },
                        dataType: "json",
                        type: "GET",
                        data: {
                            baseUrl: "",
                            VendorId: "",
                            CampusOfInterest: "",
                            CampusId: ""
                        }
                    }
                }
            });
            return ds;
        };
        LeadAssignmentDb.prototype.leadReassignDataSource = function () {
            var ds = new kendo.data.DataSource({
                pageSize: 20,
                transport: {
                    read: {
                        url: function (options) {
                            var uri = options.baseUrl + "/proxy/api/Lead/GetByReassign";
                            delete options.baseUrl;
                            return uri;
                        },
                        dataType: "json",
                        type: "GET",
                        data: {
                            baseUrl: "",
                            CampusId: "",
                            RepId: "",
                            LastModDate: ""
                        }
                    }
                }
            });
            return ds;
        };
        LeadAssignmentDb.prototype.campusWithActiveLeadDataSource = function () {
            var db = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: AD.XGET_SERVICE_LAYER_LEAD_ASSIGNMENT_CAMPUS_GET,
                        cache: true,
                        dataType: "json",
                        beforeSend: function (req) {
                            var usernameHiddenField = jQuery("#hdnUserName");
                            req.setRequestHeader("Username", usernameHiddenField.val());
                        },
                        type: "GET",
                        data: {
                            baseUrl: "",
                            IncludeActiveLeads: "",
                            UserId: XMASTER_PAGE_USER_OPTIONS_USERID
                        }
                    }
                }
            });
            return db;
        };
        LeadAssignmentDb.prototype.campusOfInterestDataSource = function () {
            var db = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: function (options) {
                            var uri = options.baseUrl + "/proxy/api/Lead/GetCampusOfInterest";
                            delete options.baseUrl;
                            return uri;
                        },
                        dataType: "json",
                        type: "GET",
                        data: {
                            baseUrl: ""
                        }
                    }
                }
            });
            return db;
        };
        LeadAssignmentDb.prototype.campusesDataSource = function () {
            var db = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: AD.XGET_SERVICE_LAYER_LEAD_ASSIGNMENT_CAMPUSES_GET,
                        cache: true,
                        dataType: "json",
                        type: "GET",
                        data: {
                            baseUrl: "",
                            UserId: XMASTER_PAGE_USER_OPTIONS_USERID
                        }
                    }
                }
            });
            return db;
        };
        LeadAssignmentDb.prototype.leadVendorDataSource = function () {
            var db = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: function (options) {
                            var uri = options.baseUrl + "/proxy/api/Lead/LeadVendor/Get";
                            delete options.baseUrl;
                            return uri;
                        },
                        dataType: "json",
                        type: "GET",
                        data: {
                            baseUrl: ""
                        }
                    }
                }
            });
            return db;
        };
        LeadAssignmentDb.prototype.getNotAssignedLeadsCount = function () {
            var url = "../proxy/api/lead/GetLeadCounts";
            return $.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json"
            });
        };
        LeadAssignmentDb.prototype.putAssignedLeadsToServer = function (input) {
            var url = AD.XPOST_SERVICE_UNASSIGNEDLEAD;
            return $.ajax({
                url: url,
                type: "POST",
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify(input)
            });
        };
        LeadAssignmentDb.prototype.updateReAssignedLeadsToServer = function (input) {
            var url = AD.XPOST_SERVICE_UNASSIGNEDLEAD;
            return $.ajax({
                url: url,
                type: "POST",
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify(input)
            });
        };
        return LeadAssignmentDb;
    }());
    AD.LeadAssignmentDb = LeadAssignmentDb;
})(AD || (AD = {}));
//# sourceMappingURL=LeadAssignmentDb.js.map