var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AD;
(function (AD) {
    var LeadAssigmentVm = (function (_super) {
        __extends(LeadAssigmentVm, _super);
        function LeadAssigmentVm() {
            _super.call(this);
            this.selectedLeadIds = [];
            this.reSelectedLeadIds = [];
            this.db = new AD.LeadAssignmentDb();
            this.assignRadio = "rCampus";
            this.reAssignRadio = "reCampus";
            this.isAllLeadsChecked = false;
            this.isAllLeadsReChecked = false;
            this.selectedCampusId = "";
            this.selectedCampus = "";
            this.selectedReCampusId = "";
            this.selectedReCampus = "";
            this.selectedRepId = "";
            this.selectedRep = "";
            this.selectedReRepId = "";
            this.selectedReRep = "";
            this.leadDataSource = this.db.leadUnassignedDataSource();
            this.leadReDataSource = this.db.leadReassignDataSource();
            this.campusGridDataSource = this.db.campusWithActiveLeadDataSource();
            this.campusSource = this.db.campusOfInterestDataSource();
            this.campusRealSource = this.db.campusesDataSource();
            this.campusReSource = this.db.campusesDataSource();
            this.leadVendorDataSource = this.db.leadVendorDataSource();
            this.admissionRepSource = this.db.admissionRepDataSource();
            this.admissionReRepSource = this.db.admissionReRepDataSource();
        }
        LeadAssigmentVm.prototype.readGridDatsSources = function () {
            this.campusGridDataSource.options.transport.read.data.baseUrl = XMASTER_GET_BASE_URL;
            this.campusGridDataSource.options.transport.read.data.UserId = $("#hdnUserId").val();
            this.campusGridDataSource.options.transport.read.data.IncludeActiveLeads = true;
        };
        LeadAssigmentVm.prototype.initializeCampusOfInterestDropdown = function () {
            var that = this;
            this.campusSource.options.transport.read.data.baseUrl = XMASTER_GET_BASE_URL;
            $("#ddlCampuses")
                .kendoDropDownList(({
                dataTextField: "Text",
                dataValueField: "Value",
                dataSource: this.campusSource,
                minLength: 5,
                change: function () {
                    that.leadDataSource.options.transport.read.data.CampusOfInterest = this.value();
                    that.leadDataSource.options.transport.read.data.CampusId = "";
                    that.leadDataSource.read();
                },
                optionLabel: {
                    Text: " All Campuses of Interest ",
                    Value: null
                },
                Text: " All Campuses of Interest "
            }));
        };
        LeadAssigmentVm.prototype.initializeVendorDropdown = function () {
            this.leadVendorDataSource.options.transport.read.data.baseUrl = XMASTER_GET_BASE_URL;
            var that = this;
            jQuery("#ddlVendor")
                .kendoDropDownList({
                dataTextField: "VendorName",
                dataValueField: "Id",
                dataSource: this.leadVendorDataSource,
                minLength: 5,
                change: function () {
                    var val = this.value();
                    if (val === " Advantage ")
                        val = null;
                    that.leadDataSource.options.transport.read.data.VendorId = val;
                    that.leadDataSource.read();
                }
            });
        };
        ;
        LeadAssigmentVm.prototype.initializeCampusRealDropdown = function () {
            var that = this;
            this.campusRealSource.options.transport.read.data.baseUrl = XMASTER_GET_BASE_URL;
            $("#ddlCampuses").kendoDropDownList({
                dataTextField: "Description",
                dataValueField: "ID",
                dataSource: this.campusRealSource,
                minLength: 5,
                change: function () {
                    that.leadDataSource.options.transport.read.data.CampusId = this.value();
                    that.leadDataSource.options.transport.read.data.CampusOfInterest = "";
                    that.leadDataSource.read();
                }
            });
        };
        ;
        LeadAssigmentVm.prototype.bindSelectAllLeads = function () {
            var _this = this;
            $("#ckSelectAllLeads").bind("click", function () {
                var dgrid = $("#leadsGrid").data("kendoGrid");
                var view = dgrid.dataSource.view();
                var chkBox = $("#ckSelectAllLeads")[0];
                if (chkBox.checked) {
                    _this.selectAllLeads(view);
                }
                else {
                    _this.unselectAllLeads(view);
                }
            });
        };
        ;
        LeadAssigmentVm.prototype.selectAllLeads = function (view) {
            var _this = this;
            var kgrid = $("#leadsGrid").data("kendoGrid");
            $.each(view, function (index, viewItem) {
                kgrid.tbody.find("tr[data-uid='" + viewItem.uid + "']").addClass("k-state-selected");
                index = _this.selectedLeadIds.indexOf(viewItem.Id);
                if (index === -1)
                    _this.selectedLeadIds.push(viewItem.Id);
            });
            var kkgrid = $("#leadsGrid").data("kendoGrid");
            kkgrid.tbody.find("tr")
                .each(function () {
                var $this = $(this), ckbox;
                if ($this.hasClass("k-state-selected")) {
                    ckbox = $this.find("td:first input");
                    ckbox.prop("checked", true);
                }
            });
        };
        ;
        LeadAssigmentVm.prototype.selectRow = function (hanthis, context) {
            var checked = hanthis.checked;
            var row = $(hanthis).closest("tr");
            var leadGrid = $("#leadsGrid").data("kendoGrid");
            var dataItem = leadGrid.dataItem(row);
            var checkedItem = dataItem.Id;
            var index = -1;
            if (checked) {
                row.addClass("k-state-selected");
                row.addClass("k-state-selected");
                index = context.viewModel.selectedLeadIds.indexOf(checkedItem);
                if (index === -1)
                    context.viewModel.selectedLeadIds.push(checkedItem);
            }
            else {
                row.removeClass("k-state-selected");
                index = context.viewModel.selectedLeadIds.indexOf(checkedItem);
                if (index !== -1)
                    common.filterFunc(this.selectedLeadIds, checkedItem);
            }
        };
        ;
        LeadAssigmentVm.prototype.unselectAllLeads = function (view) {
            var grid = $("#leadsGrid").data("kendoGrid");
            $.each(view, function (i, viewItem) {
                grid.tbody.find("tr[data-uid='" + viewItem.uid + "']").removeClass("k-state-selected");
            });
            grid.tbody.find("tr").each(function () {
                var $this = $(this);
                var ckbox = $this.find("td:first input");
                ckbox.prop("checked", false);
            });
            this.selectedLeadIds = [];
        };
        ;
        LeadAssigmentVm.prototype.initializeRepGrid = function () {
            var that = this;
            var gridrep = $("#campusRepGrid").data("kendoGrid");
            this.admissionRepSource.options.transport.read.data.baseUrl = XMASTER_GET_BASE_URL;
            var ddlCampuses = $("#ddlCampuses").data("kendoDropDownList");
            this.admissionRepSource.options.transport.read.data.CampusId = (ddlCampuses.dataItem() == null) ? "" : ddlCampuses.dataItem().ID;
            this.admissionRepSource.options.transport.read.data.ReturnReps = true;
            this.admissionRepSource.options.transport.read.data.Active = true;
            if (gridrep !== undefined && gridrep !== null) {
            }
            else {
                $("#campusRepGrid")
                    .kendoGrid({
                    dataSource: this.admissionRepSource,
                    scrollable: true,
                    sortable: false,
                    selectable: "row",
                    columns: [
                        { field: "Id", title: "Id", hidden: true },
                        {
                            field: "ActiveLeadCount",
                            title: "Active Leads",
                            width: 50,
                            template: "#: kendo.toString(ActiveLeadCount, 'n0') #",
                            attributes: { style: "text-align:right;" }
                        },
                        { field: "FullName", title: "Admissions Rep" }
                    ],
                    change: function (e) {
                        var selectedDataItem = e != null ? e.sender.dataItem(e.sender.select()) : null;
                        if (selectedDataItem != null) {
                            that.selectedRepId = selectedDataItem.Id;
                            that.selectedRep = selectedDataItem.FullName;
                        }
                    }
                });
            }
        };
        ;
        LeadAssigmentVm.prototype.getNotAssignedCounts = function () {
            var th = this;
            this.db.getNotAssignedLeadsCount()
                .done(function (data) {
                $("#assignHeader")
                    .html("Assign Leads (<span style='color:red'>" + data.NotAssignedRepCount + "</span>)");
                $("#rCampus").html(data.NotAssignedCampusCount);
                $("#rRep").html(data.NotAssignedRepCount);
            })
                .fail(function (e) {
                th.showNotification("Error occurred getting counts", "center");
                console.log(e);
            });
        };
        ;
        LeadAssigmentVm.prototype.clearSelections = function () {
            this.selectedLeadIds = [];
            this.reSelectedLeadIds = [];
            this.selectedCampusId = "";
            this.selectedCampus = "";
            this.selectedReCampusId = "";
            this.selectedRepId = "";
            this.selectedRep = "";
            this.selectedReRepId = "";
            this.selectedReRep = "";
        };
        ;
        LeadAssigmentVm.prototype.initalizeReCampusDropdown = function () {
            var that = this;
            this.campusReSource.options.transport.read.data.baseUrl = XMASTER_GET_BASE_URL;
            jQuery('#ddlReCampus')
                .kendoDropDownList({
                dataTextField: "Description",
                dataValueField: "ID",
                dataSource: this.campusReSource,
                minLength: 5,
                change: function () {
                    var ddlretDate = $("#ddlReDate").data("kendoDropDownList");
                    that.leadReDataSource.options.transport.read.data.baseUrl = XMASTER_GET_BASE_URL;
                    that.leadReDataSource.options.transport.read.data.CampusId = this.value();
                    that.leadReDataSource.options.transport.read.data.LastModDate = ddlretDate.dataItem().ID;
                    that.leadReDataSource.read();
                    if (that.reAssignRadio === "reRep")
                        that.initializeReAssignRepDropdown();
                    that.initializeReRepGrid();
                }
            });
        };
        ;
        LeadAssigmentVm.prototype.initalizeReDateDropdown = function () {
            var _this = this;
            this.campusReSource.options.transport.read.data.baseUrl = XMASTER_GET_BASE_URL;
            jQuery('#ddlReDate')
                .kendoDropDownList({
                dataTextField: "Description",
                dataValueField: "ID",
                dataSource: [
                    { "ID": "30", "Description": "30 Days" },
                    { "ID": "60", "Description": "60 Days" },
                    { "ID": "90", "Description": "90 Days" },
                    { "ID": "120", "Description": "120 Days" },
                    { "ID": "All", "Description": "All" }
                ],
                minLength: 5,
                change: function (e) {
                    _this.leadReDataSource.options.transport.read.data.baseUrl = XMASTER_GET_BASE_URL;
                    var ddlReCampus = $("#ddlReCampus").data("kendoDropDownList");
                    _this.leadReDataSource.options.transport.read.data.CampusId = ddlReCampus.dataItem().ID;
                    if (_this.reAssignRadio === 'reRep') {
                        var ddlReRep = $("#ddlReRep").data("kendoDropDownList");
                        _this.leadReDataSource.options.transport.read.data.RepId = ddlReRep.dataItem().Id;
                    }
                    var ddlReDate = $("#ddlReDate").data("kendoDropDownList");
                    _this.leadReDataSource.options.transport.read.data.LastModDate = ddlReDate.dataItem().ID;
                    _this.leadReDataSource.read();
                    if (_this.reAssignRadio === 'reRep')
                        _this.initializeReRepGrid();
                }
            });
        };
        ;
        LeadAssigmentVm.prototype.bindReSelectAllLeads = function () {
            var _this = this;
            $("#ckReSelectAllLeads")
                .bind("click", function () {
                var leadregrid = $("#leadsReGrid").data("kendoGrid");
                var view = leadregrid.dataSource.view();
                var chkBox = $("#ckReSelectAllLeads")[0];
                if (chkBox.checked)
                    _this.reSelectAllLeads(view);
                else
                    _this.reUnselectAllLeads(view);
            });
        };
        ;
        LeadAssigmentVm.prototype.reSelectAllLeads = function (view) {
            var _this = this;
            var kgrid = $("#leadsReGrid").data("kendoGrid");
            $.each(view, function (index, viewItem) {
                kgrid.tbody.find("tr[data-uid='" + viewItem.uid + "']").addClass("k-state-selected");
                index = _this.reSelectedLeadIds.indexOf(viewItem.Id);
                if (index === -1)
                    _this.reSelectedLeadIds.push(viewItem.Id);
            });
            var kkgrid = $("#leadsReGrid").data("kendoGrid");
            kkgrid.tbody.find("tr")
                .each(function () {
                var $this = $(this), ckbox;
                if ($this.hasClass("k-state-selected")) {
                    ckbox = $this.find("td:first input");
                    ckbox.prop("checked", true);
                }
            });
        };
        ;
        LeadAssigmentVm.prototype.reSelectRow = function (hanthis, context) {
            var checked = hanthis.checked;
            var row = $(hanthis).closest("tr");
            var leadGrid = $("#leadsReGrid").data("kendoGrid");
            var dataItem = leadGrid.dataItem(row);
            var checkedItem = dataItem.Id;
            var index = -1;
            if (checked) {
                row.addClass("k-state-selected");
                row.addClass("k-state-selected");
                index = context.reSelectedLeadIds.indexOf(checkedItem);
                if (index === -1)
                    context.reSelectedLeadIds.push(checkedItem);
            }
            else {
                row.removeClass("k-state-selected");
                index = context.reSelectedLeadIds.indexOf(checkedItem);
                if (index !== -1)
                    common.filterFunc(context.reSelectedLeadIds, checkedItem);
            }
        };
        ;
        LeadAssigmentVm.prototype.reUnselectAllLeads = function (view) {
            var grid = $("#leadsReGrid").data("kendoGrid");
            $.each(view, function (i, viewItem) {
                grid.tbody.find("tr[data-uid='" + viewItem.uid + "']").removeClass("k-state-selected");
            });
            grid.tbody.find("tr").each(function () {
                var $this = $(this);
                var ckbox = $this.find("td:first input");
                ckbox.prop("checked", false);
            });
            this.selectedLeadIds = [];
        };
        ;
        LeadAssigmentVm.prototype.initializeReCampusGrid = function () {
            var that = this;
            this.campusGridDataSource.options.transport.read.data.baseUrl = XMASTER_GET_BASE_URL;
            this.campusGridDataSource.options.transport.read.data.UserId = $("#hdnUserId").val();
            this.campusGridDataSource.options.transport.read.data.IncludeActiveLeads = true;
            var gridcampus = $("#reCampusRepGrid").data("kendoGrid");
            if (gridcampus !== undefined && gridcampus !== null) {
                gridcampus.dataSource.read();
                gridcampus.refresh();
            }
            else {
                $("#reCampusRepGrid")
                    .kendoGrid({
                    dataSource: this.campusGridDataSource,
                    scrollable: true,
                    sortable: false,
                    resizable: true,
                    selectable: "row",
                    change: function () {
                        var selectedRows = this.select();
                        var dataItem = this.dataItem(selectedRows[0]);
                        that.selectedReCampusId = dataItem.Id;
                        that.selectedReCampus = dataItem.Description;
                    },
                    columns: [
                        {
                            field: "Id",
                            title: "Id",
                            hidden: true
                        },
                        {
                            field: "ActiveLeadCount",
                            title: "Active Leads",
                            width: 50,
                            template: "#: kendo.toString(ActiveLeadCount, 'n0') #",
                            attributes: { style: "text-align:right;" }
                        },
                        { field: "Description", title: "Campus" }
                    ]
                });
            }
        };
        ;
        LeadAssigmentVm.prototype.initializeReRepGrid = function () {
            var _this = this;
            var grid = $("#reAssignedRepGrid").data("kendoGrid");
            this.admissionRepSource.options.transport.read.data.baseUrl = XMASTER_GET_BASE_URL;
            var ddlReCampus = $("#ddlReCampus").data("kendoDropDownList");
            this.admissionRepSource.options.transport.read.data.CampusId = ddlReCampus.dataItem().ID;
            this.admissionRepSource.options.transport.read.data.ReturnReps = true;
            this.admissionRepSource.options.transport.read.data.Active = true;
            this.admissionRepSource.read();
            if (grid !== undefined && grid !== null) {
            }
            else {
                $("#reAssignedRepGrid")
                    .kendoGrid({
                    dataSource: this.admissionRepSource,
                    scrollable: true,
                    sortable: false,
                    resizable: true,
                    selectable: "row",
                    change: function (e) {
                        var item = $("#reAssignedRepGrid").data("kendoGrid").select();
                        var id = "";
                        var descr = "";
                        if (item) {
                            id = item[0].cells[0].innerText;
                            if (!id)
                                id = item[0].cells[0].textContent;
                            descr = item[0].cells[2].innerText;
                            if (!descr)
                                descr = item[0].cells[2].textContent;
                        }
                        _this.selectedReRepId = id;
                        _this.selectedReRep = descr;
                    },
                    columns: [
                        { field: "Id", title: "Id", hidden: true },
                        {
                            field: "ActiveLeadCount",
                            title: "Active Leads",
                            width: 50,
                            template: "#: kendo.toString(ActiveLeadCount, 'n0') #",
                            attributes: { style: "text-align:right;" }
                        },
                        { field: "FullName", title: "Admissions Rep" }
                    ]
                });
            }
        };
        ;
        LeadAssigmentVm.prototype.initializeReGrid = function (repId) {
            var that = this;
            var leadGrid = $("#leadsReGrid").data("kendoGrid");
            var ddlreCampus = $("#ddlReCampus").data("kendoDropDownList");
            var ddlreDate = $("#ddlReDate").data("kendoDropDownList");
            that.leadReDataSource.options.transport.read.data.baseUrl = XMASTER_GET_BASE_URL;
            that.leadReDataSource.options.transport.read.data.CampusId = (ddlreCampus.dataItem() == null) ? "" : ddlreCampus.dataItem().ID;
            if (repId !== "") {
                that.leadReDataSource.options.transport.read.data.LastModDate = "All";
                that.leadReDataSource.options.transport.read.data.RepId = repId;
            }
            else {
                that.leadReDataSource.options.transport.read.data.LastModDate = ddlreDate.dataItem().ID;
                that.leadReDataSource.options.transport.read.data.RepId = "";
            }
            that.leadReDataSource.read();
            if (leadGrid !== undefined && leadGrid !== null) {
            }
            else {
                var dataSrc = that.leadReDataSource;
                $("#leadsReGrid")
                    .kendoGrid({
                    dataSource: dataSrc,
                    sortable: true,
                    selectable: false,
                    scrollable: true,
                    resizable: true,
                    pageable: {
                        info: true,
                        refresh: false,
                        pageSizes: 20,
                        previousNext: true,
                        numeric: false
                    },
                    columns: [
                        {
                            template: "<input type='checkbox' class='recheckbox' />",
                            width: 24,
                            headerTemplate: '<input type="checkbox" id="ckReSelectAllLeads" />'
                        },
                        { field: "Id", title: "Id", hidden: true },
                        { field: "FullName", title: "Name", width: 150 },
                        { field: "ProgramVersion", title: "Program Version", width: 100 },
                        { field: "Campus", title: "Campus", width: 100 },
                        { field: "City", title: "City", width: 100 },
                        { field: "State", title: "State", width: 60 },
                        { field: "Zip", title: "Zip", width: 60 }
                    ]
                });
                var lGrid = $("#leadsGrid").data('kendoGrid');
                if (lGrid) {
                    lGrid.dataSource.data([]);
                }
                var grid = $('#leadsReGrid').data('kendoGrid');
                grid.table.on("click", ".recheckbox", function () {
                    that.reSelectRow(this, that);
                });
                that.bindReSelectAllLeads();
            }
        };
        ;
        LeadAssigmentVm.prototype.updateReControls = function (mode, from) {
            if (mode === "campus") {
                this.initializeReCampusGrid();
                $("#reAssignedRepGrid").attr("style", "height:89%; width: 99%; display:none");
                $("#reCampusRepGrid").attr("style", "height:89%; width: 99%");
                this.initializeReGrid("");
                $("#btnReAssignLeadsToCampus").html("Reassign Leads to this Campus");
                $("#step3ReHeader").html("3.&nbsp;&nbsp;Select New Campus");
                $("#blueLabels2").html("Current Campus");
                $("#qZoneCenter").css("display", "inline");
                $("#qZoneRight").css("display", "none");
            }
            else {
                $("#qZoneCenter").css("display", "none");
                $("#qZoneRight").css("display", "inline");
                $("#reAssignedRepGrid").attr("style", "height:89%; width: 99%");
                $("#reCampusRepGrid").attr("style", "height:89%; width: 99%; display:none");
                if (from === "change") {
                    this.initializeReAssignRepDropdown();
                }
                var repId = null;
                var ddlReRep = $("#ddlReRep").data("kendoDropDownList");
                if (ddlReRep.dataItem()) {
                    repId = ddlReRep.dataItem().Id;
                }
                this.initializeReRepGrid();
                this.initializeReGrid(repId);
                $("#btnReAssignLeadsToCampus").html("Reassign Leads to this Rep");
                $("#step3ReHeader").html("3.&nbsp;&nbsp;Select New Admissions Rep");
                $("#blueLabels2").html("Campus");
            }
            $("#ckReSelectAllLeads").removeAttr("checked");
        };
        ;
        LeadAssigmentVm.prototype.initializeReAssignRepDropdown = function () {
            var that = this;
            var ddlreCampus = $("#ddlReCampus").data("kendoDropDownList");
            that.admissionReRepSource.options.transport.read.data.baseUrl = XMASTER_GET_BASE_URL;
            that.admissionReRepSource.options.transport.read.data.CampusId = (ddlreCampus.dataItem() == null) ? "" : ddlreCampus.dataItem().ID;
            that.admissionReRepSource.options.transport.read.data.ReturnReps = true;
            that.admissionReRepSource.options.transport.read.data.Active = true;
            that.admissionReRepSource.read();
            jQuery('#ddlReRep')
                .kendoDropDownList({
                dataTextField: "FullName",
                dataValueField: "Id",
                dataSource: that.admissionReRepSource,
                minLength: 5,
                change: function () {
                    var ddlReCampus = $("#ddlReCampus").data("kendoDropDownList");
                    that.leadReDataSource.options.transport.read.data.CampusId = ddlReCampus.dataItem().ID;
                    that.leadReDataSource.options.transport.read.data.RepId = this.value();
                    that.updateReControls("rep", "ddlRepInitialize");
                }
            });
        };
        ;
        LeadAssigmentVm.prototype.bindReAssignButton = function () {
            var that = this;
            $("#btnReAssignLeadsToCampus").unbind('click');
            $("#btnReAssignLeadsToCampus")
                .bind("click", function () {
                var leadIds = [];
                var selRadio = that.reAssignRadio;
                if (that.reSelectedLeadIds.length === 0) {
                    that.showNotification("No leads were selected to reassign to this campus.", "center");
                    return;
                }
                var campusId;
                var repId;
                var msg;
                var type;
                var ddlreCampus = $("#ddlReCampus").data("kendoDropDownList");
                if (selRadio === "reRep") {
                    if (!that.selectedReRep) {
                        that.showNotification("No rep was selected.", "center");
                        return;
                    }
                    else {
                        repId = that.selectedReRepId;
                        campusId = "";
                        msg = that.selectedReRep;
                        type = "rep";
                    }
                }
                else {
                    if (!that.selectedReCampusId) {
                        that.showNotification("No campus was selected.", "center");
                        return;
                    }
                    else if (that.selectedReCampusId === ddlreCampus.dataItem().ID) {
                        that.showNotification("Please choose a different campus than the currently assigned campus.", "center");
                        return;
                    }
                    else {
                        repId = "";
                        campusId = that.selectedReCampusId;
                        msg = that.selectedReCampus;
                        type = "campus";
                    }
                }
                $.each(that.reSelectedLeadIds, function (index, value) {
                    var leadId = { Id: value };
                    leadIds[index] = leadId;
                });
                var input = {
                    LeadIds: leadIds,
                    CampusId: campusId,
                    Repid: repId,
                    UserId: $("#hdnUserId").val()
                };
                that.updateReAssignLeads(input, msg, type);
            });
        };
        ;
        LeadAssigmentVm.prototype.updateReAssignLeads = function (input, msg, type) {
            var that = this;
            this.db.updateReAssignedLeadsToServer(input)
                .done(function () {
                that.showNotification("Leads reassigned to " + msg, "center");
                that.updateReControls(type, "other");
                that.reSelectedLeadIds = [];
                that.selectedReCampusId = "";
                that.selectedReCampus = "";
                that.selectedReRepId = "";
                that.selectedReRep = "";
            })
                .fail(function (e) {
                that.showNotification("Error occurred during update", "center");
                console.log(e);
            });
        };
        LeadAssigmentVm.prototype.positionAtCenter = function (e) {
            if (!$("." + e.sender._guid)[1]) {
                var element = e.element.parent(), eWidth = element.width(), eHeight = element.height(), wWidth = $(window).width(), wHeight = $(window).height(), newTop, newLeft;
                newLeft = Math.floor(wWidth / 2 - eWidth / 2);
                newTop = Math.floor(wHeight / 2 - eHeight / 2);
                e.element.parent().css({ top: newTop, left: newLeft, zIndex: 22222 });
            }
        };
        ;
        LeadAssigmentVm.prototype.showNotification = function (message, placement) {
            var notificationWidget;
            if (placement === "center") {
                notificationWidget = $("#kNotification").kendoNotification({
                    stacking: "down",
                    show: this.positionAtCenter,
                    autoHideAfter: 3000,
                    templates: [{ type: "info", template: $("#infoTemplate").html() }]
                })
                    .data("kendoNotification");
            }
            else {
                notificationWidget = $("#kNotification")
                    .kendoNotification({
                    stacking: "down",
                    position: { bottom: 30, right: 50 },
                    autoHideAfter: 3000,
                    templates: [{ type: "info", template: $("#infoTemplate").html() }]
                })
                    .data("kendoNotification");
            }
            notificationWidget.show({
                title: "&nbsp;&nbsp;&nbsp;&nbsp;Campus Selected",
                message: message,
                src: XMASTER_GET_BASE_URL + "/images/info.jpg"
            });
        };
        ;
        return LeadAssigmentVm;
    }(kendo.Observable));
    AD.LeadAssigmentVm = LeadAssigmentVm;
})(AD || (AD = {}));
//# sourceMappingURL=LeadAssigmentVm.js.map