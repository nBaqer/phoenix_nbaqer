var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AD;
(function (AD) {
    var LeadDuplicateVm = (function (_super) {
        __extends(LeadDuplicateVm, _super);
        function LeadDuplicateVm() {
            var _this = this;
            _super.call(this);
            this.updateDuplicate = function () {
                _this.SendCommand(1);
            };
            this.deleteDuplicate = function () {
                _this.SendCommand(2);
            };
            this.createLeadFromA = function () {
                _this.SendCommand(3);
            };
            this.createLeadFromB = function () {
                _this.SendCommand(4);
            };
            this.DecoreItems = function () {
                var datas = $("#duplicateHeaderLeadlistView").data("kendoListView");
                if (typeof datas !== "undefined") {
                    var ds = datas.dataSource.data();
                    if (ds == null || ds.length < 3) {
                        return;
                    }
                    if (ds[1].From === ds[2].From) {
                        $(".item2").css("background", "transparent");
                        $("#buttonDupUpdateLeadB").css("display", "none");
                        $("#buttonDupDeleteLeadA").css("display", "none");
                        $("#buttonDupCreateLeadA").css("display", "inline");
                        $("#buttonDupCreateLeadB").css("display", "inline");
                    }
                    else {
                        $(".item2:not(:first)").css("background", "yellow");
                        $("#buttonDupUpdateLeadB").css("display", "inline");
                        $("#buttonDupDeleteLeadA").css("display", "inline");
                        $("#buttonDupCreateLeadA").css("display", "inline");
                        $("#buttonDupCreateLeadB").css("display", "none");
                    }
                    ;
                    if (ds[1].AcquiredDate === ds[2].AcquiredDate) {
                        $(".item1").css("background", "transparent");
                    }
                    else {
                        $(".item1:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].FullName === ds[2].FullName) {
                        $(".item3").css("background", "transparent");
                    }
                    else {
                        $(".item3:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].Address1 === ds[2].Address1) {
                        $(".item4").css("background", "transparent");
                    }
                    else {
                        $(".item4:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].Address2 === ds[2].Address2) {
                        $(".item5").css("background", "transparent");
                    }
                    else {
                        $(".item5:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].City === ds[2].City) {
                        $(".item6").css("background", "transparent");
                    }
                    else {
                        $(".item6:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].State === ds[2].State) {
                        $(".item7").css("background", "transparent");
                    }
                    else {
                        $(".item7:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].Zip === ds[2].Zip) {
                        $(".item8").css("background", "transparent");
                    }
                    else {
                        $(".item8:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].Status === ds[2].Status) {
                        $(".item9").css("background", "transparent");
                    }
                    else {
                        $(".item9:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].Phone === ds[2].Phone) {
                        $(".item10").css("background", "transparent");
                    }
                    else {
                        $(".item10:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].Email === ds[2].Email) {
                        $(".item11").css("background", "transparent");
                    }
                    else {
                        $(".item11:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].SourceInfo === ds[2].SourceInfo) {
                        $(".item12").css("background", "transparent");
                    }
                    else {
                        $(".item12:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].AdmissionsRep === ds[2].AdmissionsRep) {
                        $(".item13").css("background", "transparent");
                    }
                    else {
                        $(".item13:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].Campus === ds[2].Campus) {
                        $(".item14").css("background", "transparent");
                    }
                    else {
                        $(".item14:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].ProgramOfInterest === ds[2].ProgramOfInterest) {
                        $(".item15").css("background", "transparent");
                    }
                    else {
                        $(".item15:not(:first)").css("background", "yellow");
                    }
                    ;
                    if (ds[1].Comments === ds[2].Comments) {
                        $(".item16").css("background", "transparent");
                    }
                    else {
                        $(".item16:not(:first)").css("background", "yellow");
                    }
                    ;
                }
            };
            this.db = new AD.LeadDuplicateDb();
            this.duplicateLeadlistView = this.db.getDuplicateLeadsList();
            this.dummyDataSource = kendo.data.DataSource.create({
                data: [
                    {}
                ]
            });
        }
        LeadDuplicateVm.prototype.GetDuplicateInformationFromServer = function () {
            try {
                $("body").css("cursor", "progress");
                var listView = $("#duplicateLeadlistView").data("kendoListView");
                var newLead = listView.select();
                var newLeadGuid = $.map(newLead, function (item) {
                    return listView.dataSource.view()[$(item).index()].NewLeadGuid;
                });
                var dupLeadGuid = $.map(newLead, function (item) {
                    return listView.dataSource.view()[$(item).index()].DuplicateGuid;
                });
                var datasource = this.db.getDuplicatePairForServer(newLeadGuid, dupLeadGuid);
                var visor = $("#duplicateHeaderLeadlistView").data("kendoListView");
                visor.setDataSource(datasource);
                var btn1 = $("#buttonDupUpdateLeadB").data("kendoButton");
                var btn2 = $("#buttonDupDeleteLeadA").data("kendoButton");
                var btn3 = $("#buttonDupCreateLeadA").data("kendoButton");
                var btn4 = $("#buttonDupCreateLeadB").data("kendoButton");
                btn1.enable(true);
                btn2.enable(true);
                btn3.enable(true);
                btn4.enable(true);
            }
            finally {
                $("body").css("cursor", "default");
            }
        };
        LeadDuplicateVm.prototype.SendCommand = function (command) {
            switch (command) {
                case 1:
                    if (confirm("If you continue the information of Lead A will be transferred to Lead B. Are you sure?") === false)
                        return;
                    break;
                case 2:
                    if (confirm("Are you sure to delete Lead A?") === false)
                        return;
                    break;
                case 3:
                    if (confirm("Are you sure to create a new Lead with Lead A?") === false)
                        return;
                    break;
                case 4:
                    if (confirm("Are you sure to create a new Lead with Lead B?") === false)
                        return;
                    break;
                default:
                    alert("Command Not recognize. Operation is aborted!");
                    return;
            }
            try {
                $("body").css("cursor", "progress");
                var list = $("#duplicateLeadlistView").data("kendoListView");
                var newLead = list.select();
                var newLeadGuid = $.map(newLead, function (item) { return list.dataSource.view()[$(item).index()].NewLeadGuid; });
                var dupLeadGuid = $.map(newLead, function (item) { return list.dataSource.view()[$(item).index()].DuplicateGuid; });
                var userId = $("#hdnUserId").val();
                this.duplicateLeadlistView = this.db.commandAndGetDuplicateLeadsList(command, newLeadGuid, dupLeadGuid, userId);
                list.setDataSource(this.duplicateLeadlistView);
                var datasource = $("#duplicateHeaderLeadlistView").data("kendoListView").dataSource;
                datasource.remove(datasource.at(2));
                datasource.remove(datasource.at(1));
                datasource.remove(datasource.at(0));
                this.db.showNotificationCentered("Operation confirmed!");
            }
            finally {
                $("body").css("cursor", "default");
            }
        };
        return LeadDuplicateVm;
    }(kendo.Observable));
    AD.LeadDuplicateVm = LeadDuplicateVm;
})(AD || (AD = {}));
//# sourceMappingURL=LeadDuplicateVm.js.map