declare module AD {
    class LeadAssignment {
        viewModel: LeadAssigmentVm;
        rbReAssignCampus: boolean;
        rbReAssignRep: boolean;
        rbCampus: boolean;
        rbRep: boolean;
        constructor();
        updateControls(mode: string): void;
        initializeGrid(): void;
        initializeCampusGrid(): void;
        updateAssignLeads(input: any, msg: any, type: any): void;
        selectTab(e: any, that: any): void;
        bindRadioChange(): void;
        bindAssignButton(): void;
        bindReRadioChange(): void;
    }
}
