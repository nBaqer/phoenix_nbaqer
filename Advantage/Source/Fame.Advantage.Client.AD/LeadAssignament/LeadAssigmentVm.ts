﻿module AD {
    import DataSourceTransportOptions = kendo.data.DataSourceTransportOptions;

    declare var XMASTER_GET_BASE_URL;
    declare var common: any;


    export class LeadAssigmentVm extends kendo.Observable {


        //----------------------------------------------------------------------------------------
        // View Model that represents the data to be bound to the 
        // view
        //----------------------------------------------------------------------------------------
        db: LeadAssignmentDb;
        selectedLeadIds = [];
        reSelectedLeadIds = [];
        //----------------------------------------------------------------------------------------
        // Properties
        //----------------------------------------------------------------------------------------
        assignRadio: string;
        reAssignRadio: string;
        isAllLeadsChecked: boolean;
        isAllLeadsReChecked: boolean;
        selectedCampusId: string;
        selectedCampus: string;
        selectedReCampusId: string;
        selectedReCampus: string;
        selectedRepId: string;
        selectedRep: string;
        selectedReRepId: string;
        selectedReRep: string;
        leadDataSource: kendo.data.DataSource; //  datasources.leadUnassignedDataSource;
        leadReDataSource: kendo.data.DataSource; // datasources.leadReassignDataSource;
        campusGridDataSource: kendo.data.DataSource; // datasources.campusWithActiveLeadDataSource;
        areaSource: kendo.data.DataSource; // datasources.AreaOfInterestDataSource;
        campusRealSource: kendo.data.DataSource; // datasources.campusesDataSource;
        campusReSource: kendo.data.DataSource; // datasources.campusesDataSource;
        leadVendorDataSource: kendo.data.DataSource; // datasources.leadVendorDataSource;
        admissionRepSource: kendo.data.DataSource; // datasources.admissionRepDataSource;
        admissionReRepSource: kendo.data.DataSource; // datasources.admissionReRepDataSource;
        //timeout: number;

        constructor() {
            super();

            //----------------------------------------------------------------------------------------
            // Properties
            //----------------------------------------------------------------------------------------
            this.db = new LeadAssignmentDb();
            this.assignRadio = "rCampus";
            this.reAssignRadio = "reCampus";
            this.isAllLeadsChecked = false;
            this.isAllLeadsReChecked = false;
            this.selectedCampusId = "";
            this.selectedCampus = "";
            this.selectedReCampusId = "";
            this.selectedReCampus = "";
            this.selectedRepId = "";
            this.selectedRep = "";
            this.selectedReRepId = "";
            this.selectedReRep = "";
            this.leadDataSource = this.db.leadUnassignedDataSource();
            this.leadReDataSource = this.db.leadReassignDataSource();
            this.campusGridDataSource = this.db.campusWithActiveLeadDataSource();
            this.areaSource = this.db.areaOfInterestDataSource();
            this.campusRealSource = this.db.campusesDataSource();
            this.campusReSource = this.db.campusesDataSource();
            this.leadVendorDataSource = this.db.leadVendorDataSource();
            this.admissionRepSource = this.db.admissionRepDataSource();
            this.admissionReRepSource = this.db.admissionReRepDataSource();
            //this.timeout = 0;

        } /* Constructor End*/

        /* Initialize the Campus Grid data-source with parameters*/
        readGridDatsSources() {
            (this.campusGridDataSource.options.transport.read as any).data.baseUrl = XMASTER_GET_BASE_URL;
            (this.campusGridDataSource.options.transport.read as any).data.UserId = $("#hdnUserId").val();
            (this.campusGridDataSource.options.transport.read as any).data.IncludeActiveLeads = true;
        }

        /* Init campus of interest drop-down*/
        initializeAreaOfInterestDropdown() {
            let that = this;
            (this.areaSource.options.transport.read as any).data.baseUrl = XMASTER_GET_BASE_URL;
            // setup incrementTypeId, and IncrmentTypeText
            $("#ddlCampuses")
                .kendoDropDownList(({
                    dataTextField: "Description",
                    dataValueField: "ID",
                    dataSource: this.areaSource,
                    minLength: 5,
                    change: function () {

                        // ReSharper disable SuspiciousThisUsage
                        if ($("input[name=radio]:checked").val() === "rCampus") 
                        {
                            (that.leadDataSource.options.transport.read as any).data.AreaOfInterest = this.value();
                        }
                        else if ($("input[name=radio]:checked").val() === "rRep")
                        {
                            (that.leadDataSource.options.transport.read as any).data.CampusId = this.value();
                        }
                        // ReSharper restore SuspiciousThisUsage
                        that.leadDataSource.read();
                        that.setPageSize("leadsGrid", 10);
                    },
                    optionLabel: {
                        Description: " All Areas of Interest ",
                        ID: ""
                    },
                    Text: " All Areas of Interest "

                }) as any);
        }

        /*Init Vendor DropDown List*/
        initializeVendorDropdown() {
            (this.leadVendorDataSource.options.transport.read as any).data.baseUrl = XMASTER_GET_BASE_URL;
            var that = this;
            // setup incrementTypeId, and IncrmentTypeText
            jQuery("#ddlVendor")
                .kendoDropDownList({
                    dataTextField: "VendorName",
                    dataValueField: "Id",
                    dataSource: this.leadVendorDataSource,
                    minLength: 5,
                    change: function () {
                        // ReSharper disable SuspiciousThisUsage
                        var val = this.value();
                        // ReSharper restore SuspiciousThisUsage
                        if (val === " Advantage ") val = null;
                        (that.leadDataSource.options.transport.read as any).data.VendorId = val;
                        that.leadDataSource.read();
                        that.setPageSize("leadsGrid", 10);
                    }
                });
        };

        /*Init the campus Drop- down with the real campus*/
        initializeCampusRealDropdown() {
            var that = this;
            (this.campusRealSource.options.transport.read as any).data.baseUrl = XMASTER_GET_BASE_URL;
            // setup incrementTypeId, and IncrmentTypeText
            $("#ddlCampuses").kendoDropDownList({
                dataTextField: "Description",
                dataValueField: "ID",
                dataSource: this.campusRealSource,
                minLength: 5,
                change: function () {

                    // ReSharper disable SuspiciousThisUsage
                    if ($("input[name=radio]:checked").val() === "rCampus") {
                        (that.leadDataSource.options.transport.read as any).data.AreaOfInterest = this.value();
                    }
                    else if ($("input[name=radio]:checked").val() === "rRep") {
                        (that.leadDataSource.options.transport.read as any).data.CampusId = this.value();
                    }
                    that.leadDataSource.read();
                    that.setPageSize("leadsGrid", 10);

                    // Update the Admission rep list because the campus was changed
                    (that.admissionRepSource.options.transport.read as any).data.CampusId = this.value();
                    (that.admissionRepSource.options.transport.read as any).data.ReturnReps = true;
                    (that.admissionRepSource.options.transport.read as any).data.Active = true;
                    that.admissionRepSource.read();
                    // ReSharper restore SuspiciousThisUsage
                }
            });
        };

        /*Bind the click event of ckSelectAllLeads*/
        bindSelectAllLeads() {
            $("#ckSelectAllLeads").bind("click", () => {
                let dgrid: kendo.ui.Grid = $("#leadsGrid").data("kendoGrid") as any;
                var view = dgrid.dataSource.view();
                var chkBox: any = $("#ckSelectAllLeads")[0];
                if (chkBox.checked) {
                    this.selectAllLeads(view);
                }
                else {
                    this.unselectAllLeads(view);
                }
            });
        };


        /* Select all leads */
        selectAllLeads(view) {
            let kgrid: kendo.ui.Grid = $("#leadsGrid").data("kendoGrid") as any;
            // Do not eliminate index as parameter! did not work!
            $.each(view, (index, viewItem:any) => {
                kgrid.tbody.find("tr[data-uid='" + viewItem.uid + "']").addClass("k-state-selected");
                index = this.selectedLeadIds.indexOf(viewItem.Id);
                if (index === -1) this.selectedLeadIds.push(viewItem.Id);
            });

            let kkgrid: kendo.ui.Grid = $("#leadsGrid").data("kendoGrid") as any;
            kkgrid.tbody.find("tr")
                .each(function () {
                    // ReSharper disable SuspiciousThisUsage
                    var $this = $(this), ckbox;
                    // ReSharper restore SuspiciousThisUsage

                    if ($this.hasClass("k-state-selected")) {
                        ckbox = $this.find("td:first input");
                        ckbox.prop("checked", true);
                    }
                });
        };

        /* Select the grid row in leads Grids*/
        selectRow(hanthis: any, context: any) {
            var checked = hanthis.checked;
            var row = $(hanthis).closest("tr");
            var leadGrid: kendo.ui.Grid = $("#leadsGrid").data("kendoGrid") as any;
            var dataItem: any = leadGrid.dataItem(row);
            var checkedItem = dataItem.Id;
            var index = -1;

            if (checked) {
                row.addClass("k-state-selected");
                row.addClass("k-state-selected");
                index = context.viewModel.selectedLeadIds.indexOf(checkedItem);

                if (index === -1) context.viewModel.selectedLeadIds.push(checkedItem);

            } else {

                row.removeClass("k-state-selected");
                index = context.viewModel.selectedLeadIds.indexOf(checkedItem);
                if (index !== -1) common.filterFunc(this.selectedLeadIds, checkedItem);
            }
        };

        //-------------------------------------------------------------------------------------------------------
        // Unselected all items in the grid
        //-------------------------------------------------------------------------------------------------------
        unselectAllLeads(view) {
            let grid: kendo.ui.Grid = $("#leadsGrid").data("kendoGrid") as any;
            $.each(view, (i, viewItem:any) => {
                grid.tbody.find("tr[data-uid='" + viewItem.uid + "']").removeClass("k-state-selected");
            });
            grid.tbody.find("tr").each(function () {
                var $this = $(this);
                var ckbox = $this.find("td:first input");
                ckbox.prop("checked", false);
            });

            this.selectedLeadIds = [];

        };
        /* Initialize Grid campusGrid for Admission Rep */
        initializeRepGrid() {
            var that = this;
            var gridrep: kendo.ui.Grid = $("#campusRepGrid").data("kendoGrid") as any;
            //if ($("#campusRepGrid").data("kendoGrid")) {
            //    gridcampus.dataSource.data([]);
            //    gridcampus.unbind("change");
            //    //gridcampus.destroy();
            //}

            (this.admissionRepSource.options.transport.read as any).data.baseUrl = XMASTER_GET_BASE_URL;
            let ddlCampuses: kendo.ui.DropDownList = $("#ddlCampuses").data("kendoDropDownList") as any;
            (this.admissionRepSource.options.transport.read as any).data.CampusId = (ddlCampuses.dataItem() == null) ? "" : ddlCampuses.dataItem().ID;
            (this.admissionRepSource.options.transport.read as any).data.ReturnReps = true;
            (this.admissionRepSource.options.transport.read as any).data.Active = true;
            this.admissionRepSource.read();
            if (gridrep !== undefined && gridrep !== null) {
            } else {
                //setup kendo grid for log
                $("#campusRepGrid")
                    .kendoGrid({
                        dataSource: this.admissionRepSource,
                        scrollable: true,
                        sortable: false,
                        selectable: "row",
                        columns: [
                            { field: "Id", title: "Id", hidden: true },
                            {
                                field: "ActiveLeadCount",
                                title: "Active Leads",
                                width: 50,
                                template: "#: kendo.toString(ActiveLeadCount, 'n0') #",
                                attributes: { style: "text-align:right;" }
                            },
                            { field: "FullName", title: "Admissions Rep" }
                        ],
                        change: e => {
                            var selectedDataItem: any = e != null ? e.sender.dataItem(e.sender.select()) : null;
                            if (selectedDataItem != null) {
                                that.selectedRepId = selectedDataItem.Id;
                                that.selectedRep = selectedDataItem.FullName;
                            }
                        }
                    });
            }
            //$("#campusGrid th[data-field=Description]").html("Admissions Rep");
        };

        /* Get the quantity of no assigned leads*/
        getNotAssignedCounts() {
            let th = this;
            this.db.getNotAssignedLeadsCount()
                .done(data => {
                    var totalLeadNotAssigned = data.NotAssignedRepCount + data.NotAssignedCampusCount;
                    $("#assignHeader")
                        .html("Assign Leads (<span class='font-red-for-primary'>" + totalLeadNotAssigned + "</span>)");
                    $("#rCampus").html(data.NotAssignedCampusCount);
                    $("#rRep").html(data.NotAssignedRepCount);
                })
                .fail(e => {
                    MasterPage.SHOW_ERROR_WINDOW("Error occurred getting counts");
                    //th.showNotification(, "center");
                    console.log(e);
                });
        };

        /* Clear Selections*/
        clearSelections() {
            this.selectedLeadIds = [];
            this.reSelectedLeadIds = [];
            this.selectedCampusId = "";
            this.selectedCampus = "";
            this.selectedReCampusId = "";
            this.selectedRepId = "";
            this.selectedRep = "";
            this.selectedReRepId = "";
            this.selectedReRep = "";
        };

        //-------------------------------------------------------------------------------------------------------
        // Re-Assign functions
        //-------------------------------------------------------------------------------------------------------

        initalizeReCampusDropdown() {
            let that = this;
            (this.campusReSource.options.transport.read as any).data.baseUrl = XMASTER_GET_BASE_URL;
            // setup incrementTypeId, and IncrmentTypeText
            jQuery('#ddlReCampus')
                .kendoDropDownList({
                    dataTextField: "Description",
                    dataValueField: "ID",
                    dataSource: this.campusReSource,
                    minLength: 5,
                    change: function () {
                        let ddlretDate: kendo.ui.DropDownList = $("#ddlReDate").data("kendoDropDownList") as any;
                        (that.leadReDataSource.options.transport.read as any).data.baseUrl = XMASTER_GET_BASE_URL;
                        (that.leadReDataSource.options.transport.read as any).data.CampusId = this.value();
                        (that.leadReDataSource.options.transport.read as any).data.LastModDate = ddlretDate.dataItem().ID;
                        that.leadReDataSource.read();

                        if (that.reAssignRadio === "reRep")
                            that.initializeReAssignRepDropdown();
                        that.initializeReRepGrid();

                        if ($('#rdoReRep').prop("checked")) {
                            that.updateReControls("rep", "change");
                        } else {
                            that.updateReControls("campus", "other");
                        }

                        that.setPageSize("leadsReGrid", 20);
                    }
                });
        };

        initalizeReDateDropdown() {
            (this.campusReSource.options.transport.read as any).data.baseUrl = XMASTER_GET_BASE_URL;
            // setup incrementTypeId, and IncrmentTypeText
            jQuery('#ddlReDate')
                .kendoDropDownList({
                    dataTextField: "Description",
                    dataValueField: "ID",
                    dataSource: [
                        { "ID": "30", "Description": "30 Days" },
                        { "ID": "60", "Description": "60 Days" },
                        { "ID": "90", "Description": "90 Days" },
                        { "ID": "120", "Description": "120 Days" },
                        { "ID": "All", "Description": "All" }
                    ],
                    minLength: 5,
                    change: e => {

                        (this.leadReDataSource.options.transport.read as any).data.baseUrl = XMASTER_GET_BASE_URL;
                        let ddlReCampus: kendo.ui.DropDownList = $("#ddlReCampus").data("kendoDropDownList") as any;
                        (this.leadReDataSource.options.transport.read as any).data.CampusId = ddlReCampus.dataItem().ID;

                        if (this.reAssignRadio === 'reRep') {
                            let ddlReRep: kendo.ui.DropDownList = $("#ddlReRep").data("kendoDropDownList") as any;
                            (this.leadReDataSource.options.transport.read as any).data.RepId = ddlReRep.dataItem().Id;
                        }
                        let ddlReDate: kendo.ui.DropDownList = $("#ddlReDate").data("kendoDropDownList") as any;
                        (this.leadReDataSource.options.transport.read as any).data.LastModDate = ddlReDate.dataItem().ID;
                        this.leadReDataSource.read();

                        if (this.reAssignRadio === 'reRep') {
                            this.initializeReRepGrid();
                        }
                        this.setPageSize("leadsReGrid", 20);
                    }

                });
        };

        bindReSelectAllLeads() {
            $("#ckReSelectAllLeads")
                .bind("click",
                () => {
                    var leadregrid: kendo.ui.Grid = $("#leadsReGrid").data("kendoGrid") as any;
                    var view = leadregrid.dataSource.view();

                    var chkBox: any = $("#ckReSelectAllLeads")[0];
                    if (chkBox.checked) this.reSelectAllLeads(view);
                    else this.reUnselectAllLeads(view);
                });
        };

        reSelectAllLeads(view) {

            let kgrid: kendo.ui.Grid = $("#leadsReGrid").data("kendoGrid") as any;
            // Do not eliminate index as parameter! did not work!
            $.each(view, (index, viewItem:any) => {
                kgrid.tbody.find("tr[data-uid='" + viewItem.uid + "']").addClass("k-state-selected");
                index = this.reSelectedLeadIds.indexOf(viewItem.Id);
                if (index === -1) this.reSelectedLeadIds.push(viewItem.Id);
            });


            let kkgrid: kendo.ui.Grid = $("#leadsReGrid").data("kendoGrid") as any;
            kkgrid.tbody.find("tr")
                .each(function () {
                    // ReSharper disable SuspiciousThisUsage
                    var $this = $(this), ckbox;
                    // ReSharper restore SuspiciousThisUsage

                    if ($this.hasClass("k-state-selected")) {
                        ckbox = $this.find("td:first input");
                        ckbox.prop("checked", true);
                    }
                });
        };

        /* Actions when select a row in Leads Table*/
        reSelectRow(hanthis: any, context: any) {

            let checked = hanthis.checked;
            var row = $(hanthis).closest("tr");
            var leadGrid: kendo.ui.Grid = $("#leadsReGrid").data("kendoGrid") as any;
            var dataItem: any = leadGrid.dataItem(row);
            var checkedItem = dataItem.Id;
            var index = -1;

            if (checked) {
                row.addClass("k-state-selected");
                row.addClass("k-state-selected");
                index = context.reSelectedLeadIds.indexOf(checkedItem);

                if (index === -1) context.reSelectedLeadIds.push(checkedItem);

            } else {

                row.removeClass("k-state-selected");
                index = context.reSelectedLeadIds.indexOf(checkedItem);
                if (index !== -1) common.filterFunc(context.reSelectedLeadIds, checkedItem);
            }
        };

        /* Un-select all row in the Reassigned Lead Grid */
        reUnselectAllLeads(view) {

            let grid: kendo.ui.Grid = $("#leadsReGrid").data("kendoGrid") as any;
            $.each(view, (i, viewItem:any) => {
                grid.tbody.find("tr[data-uid='" + viewItem.uid + "']").removeClass("k-state-selected");
            });
            grid.tbody.find("tr").each(function () {
                var $this = $(this);
                var ckbox = $this.find("td:first input");
                ckbox.prop("checked", false);
            });

            this.reSelectedLeadIds = [];
        };

        /* Initialize in Re-assigned Leads the Campus Grid List*/
        initializeReCampusGrid() {
            let that = this;
            (this.campusGridDataSource.options.transport.read as any).data.baseUrl = XMASTER_GET_BASE_URL;
            (this.campusGridDataSource.options.transport.read as any).data.UserId = $("#hdnUserId").val();
            (this.campusGridDataSource.options.transport.read as any).data.IncludeActiveLeads = true;

            let gridcampus: kendo.ui.Grid = $("#reCampusRepGrid").data("kendoGrid") as any;

            if (gridcampus !== undefined && gridcampus !== null) {
                gridcampus.dataSource.read();
                gridcampus.refresh();
                //if ($("#reCampusRepGrid").data("kendoGrid")) {
                //    gridcampus.dataSource.data([]);
                //    gridcampus.unbind("change");
                //    gridcampus.destroy();
                //}
            } else {
                //setup kendo grid for log
                $("#reCampusRepGrid")
                    .kendoGrid({
                        dataSource: this.campusGridDataSource,
                        scrollable: true,
                        sortable: false,
                        resizable: true,
                        dataBound: function (e) {
                            let ddlReCampus = jQuery("#ddlReCampus").data("kendoDropDownList") as kendo.ui.DropDownList;
                            var items = that.campusGridDataSource.data() as any;
                            for (var index = 0; index <= items.length - 1; index++) {
                                let element = items[index] as any;
                                if (element.Id === ddlReCampus.value()) {
                                    that.campusGridDataSource.remove(element);
                                }
                            }
                        },
                        selectable: "row",
                        change: function () {

                            var selectedRows = this.select();
                            var dataItem = this.dataItem(selectedRows[0]);

                            that.selectedReCampusId = dataItem.Id;
                            that.selectedReCampus = dataItem.Description;
                        },

                        columns: [
                            {
                                field: "Id",
                                title: "Id",
                                hidden: true
                            },
                            {
                                field: "ActiveLeadCount",
                                title: "Active Leads",
                                width: 50,
                                template: "#: kendo.toString(ActiveLeadCount, 'n0') #",
                                attributes: { style: "text-align:right;" }
                            },
                            { field: "Description", title: "Campus" }
                        ]
                    });
            }


        };

        /* Init the grid or ReAssigned lead Page to Admission Rep.*/
        initializeReRepGrid() {
            let grid: kendo.ui.Grid = $("#reAssignedRepGrid").data("kendoGrid") as any;

            (this.admissionRepSource.options.transport.read as any).data.baseUrl = XMASTER_GET_BASE_URL;
            let ddlReCampus: kendo.ui.DropDownList = $("#ddlReCampus").data("kendoDropDownList") as any;
            (this.admissionRepSource.options.transport.read as any).data.CampusId = ddlReCampus.dataItem().ID;
            (this.admissionRepSource.options.transport.read as any).data.ReturnReps = true;

            (this.admissionRepSource.options.transport.read as any).data.Active = true;
            let repSelected = ($("#ddlReRep").data("kendoDropDownList") as kendo.ui.DropDownList).value();
            if (repSelected != null || repSelected != undefined) {
                this.admissionRepSource.filter({ field: "Id", operator: "notequalto", value: repSelected });
            }
            this.admissionRepSource.read();
            if (grid !== undefined && grid !== null) {
            } else {
                //setup kendo grid 
                $("#reAssignedRepGrid")
                    .kendoGrid({
                        dataSource: this.admissionRepSource,
                        scrollable: true,
                        sortable: false,
                        resizable: true,
                        selectable: "row",
                        change: e => {
                            var item: any = $("#reAssignedRepGrid").data("kendoGrid").select();
                            var id = "";
                            var descr = "";
                            if (item) {
                                id = item[0].cells[0].innerText;
                                if (!id) id = item[0].cells[0].textContent;

                                descr = item[0].cells[2].innerText;
                                if (!descr) descr = item[0].cells[2].textContent;
                            }


                            this.selectedReRepId = id;
                            this.selectedReRep = descr;

                        },
                        columns: [
                            { field: "Id", title: "Id", hidden: true },
                            {
                                field: "ActiveLeadCount",
                                title: "Active Leads",
                                width: 50,
                                template: "#: kendo.toString(ActiveLeadCount, 'n0') #",
                                attributes: { style: "text-align:right;" }
                            },
                            { field: "FullName", title: "Admissions Rep" }
                        ]
                    });
            }
        };

        /* Initialize reassign lead grid*/
        initializeReGrid(repId) {
            let that = this;
            var leadGrid: kendo.ui.Grid = $("#leadsReGrid").data("kendoGrid") as any;

            let ddlreCampus: kendo.ui.DropDownList = $("#ddlReCampus").data("kendoDropDownList") as any;
            let ddlreDate: kendo.ui.DropDownList = $("#ddlReDate").data("kendoDropDownList") as any;
            (that.leadReDataSource.options.transport.read as any).data.baseUrl = XMASTER_GET_BASE_URL;
            (that.leadReDataSource.options.transport.read as any).data.CampusId = (ddlreCampus.dataItem() == null) ? "" : ddlreCampus.dataItem().ID;

            if (repId !== "") {
                (that.leadReDataSource.options.transport.read as any).data.LastModDate = "All";
                (that.leadReDataSource.options.transport.read as any).data.RepId = repId;
            } else {
                (that.leadReDataSource.options.transport.read as any).data.LastModDate = ddlreDate.dataItem().ID;
                (that.leadReDataSource.options.transport.read as any).data.RepId = "";
            }
            that.leadReDataSource.read();
            if (leadGrid !== undefined && leadGrid !== null) {
            }
            else {
                // Create the Grid if not exists
                var dataSrc = that.leadReDataSource;

                //setup kendo grid
                $("#leadsReGrid")
                    .kendoGrid({
                        dataSource: dataSrc,
                        //dataBound: function () {
                        //    this.pager.element.find(".k-status-text")
                        //        .remove()
                        //        .end()
                        //        .append(`<div class="k-status-text">&nbsp;Number of Leads: ${
                        //        this.dataSource.view().length
                        //        }</div>`);
                        //},
                        dataBound: function () {
                            var dsView = this.dataSource.view();
                            var index = -1;
                            for (var i = 0; i < dsView.length; i++) {
                                if (this.reSelectedLeadIds !== undefined) {
                                    index = this.reSelectedLeadIds.indexOf(dsView[i].Id);
                                    if (index !== -1) {
                                        this.tbody.find("tr[data-uid='" + dsView[i].uid + "']")
                                            .addClass("k-state-selected")
                                            .find(".checkbox")
                                            .attr("checked", "checked");
                                    }
                                }
                            }
                        },
                        sortable: true,
                        selectable: false,
                        scrollable: true,
                        resizable: true,
                        pageable: {
                            info: true,
                            refresh: false,
                            pageSizes: 20,
                            previousNext: true,
                            numeric: false
                        },
                        columns: [
                            {
                                template: "<input type='checkbox' class='recheckbox' />",
                                width: 24,
                                headerTemplate:
                                    '<input type="checkbox" id="ckReSelectAllLeads" />'
                                //'<input type="checkbox" data-bind="checked: isAllLeadsReChecked" id="ckReSelectAllLeads" />'
                            },
                            { field: "Id", title: "Id", hidden: true },
                            { field: "FullName", title: "Name", width: 150 },
                            { field: "ProgramVersion", title: "Program Version", width: 100 },
                            { field: "Campus", title: "Campus", width: 100 },
                            { field: "City", title: "City", width: 100 },
                            { field: "State", title: "State", width: 60 },
                            { field: "Zip", title: "Zip", width: 60 }
                        ]
                    });

                var lGrid: kendo.ui.Grid = $("#leadsGrid").data('kendoGrid') as any;
                if (lGrid) {
                    lGrid.dataSource.data([]);
                }
                var grid: kendo.ui.Grid = $('#leadsReGrid').data('kendoGrid') as any;

                //bind click event to the checkbox
                grid.table.on("click",
                    ".recheckbox",
                    function () {
                        // ReSharper disable SuspiciousThisUsage
                        that.reSelectRow(this, that);
                        // ReSharper restore SuspiciousThisUsage
                    });
                that.bindReSelectAllLeads();
            }
        };

        /* Update controls in reassign page
         * mode: campus and rep
         */
        updateReControls(mode: string, from: string) {

            if (mode === "campus") {
                //this.viewModel.initalizeReCampusDropdown();
                this.initializeReCampusGrid();
                //this.viewModel.initalizeReDateDropdown();
                //this.initializeReAssignRepDropdown(); //initialize but leave not show
                $("#reAssignedRepGrid").attr("style", "height:89%; width: 99%; display:none");
                $("#reCampusRepGrid").attr("style", "height:89%; width: 99%");

                this.initializeReGrid("");

                $("#btnReAssignLeadsToCampus").html("Reassign Leads to this Campus");
                $("#step3ReHeader").html("3.&nbsp;&nbsp;Select New Campus");
                $("#blueLabels2").html("Current Campus");
                $("#qZoneCenter").css("display", "inline");
                $("#qZoneRight").css("display", "none");

            } else {
                // Initialize admission lead section...................
                // Selected re-admission change represent Grid
                $("#qZoneCenter").css("display", "none");
                $("#qZoneRight").css("display", "inline");
                $("#reAssignedRepGrid").attr("style", "height:89%; width: 99%");
                $("#reCampusRepGrid").attr("style", "height:89%; width: 99%; display:none");
                if (from === "change") {
                    this.initializeReAssignRepDropdown();
                }
                var repId = null;
                var ddlReRep: kendo.ui.DropDownList = $("#ddlReRep").data("kendoDropDownList") as any;
                if (ddlReRep.dataItem()) {
                    repId = ddlReRep.dataItem().Id;
                }

                // Initialize Re assign Admission list Grid
                this.initializeReRepGrid();

                // Initialize possibles lead to change admission rep. in grid
                this.initializeReGrid(repId);
                $("#btnReAssignLeadsToCampus").html("Reassign Leads to this Rep");
                $("#step3ReHeader").html("3.&nbsp;&nbsp;Select New Admissions Rep");
                $("#blueLabels2").html("Campus");
            }

            // commons...
            $("#ckReSelectAllLeads").removeAttr("checked");
        };

        /* Initialize Admission Rep Drop Down Control*/
        initializeReAssignRepDropdown() {
            let that = this;
            let ddlreCampus: kendo.ui.DropDownList = $("#ddlReCampus").data("kendoDropDownList") as any;
            (that.admissionReRepSource.options.transport.read as any).data.baseUrl = XMASTER_GET_BASE_URL;
            (that.admissionReRepSource.options.transport.read as any).data.CampusId = (ddlreCampus.dataItem() == null) ? "" : ddlreCampus.dataItem().ID;
            (that.admissionReRepSource.options.transport.read as any).data.ReturnReps = true;
            (that.admissionReRepSource.options.transport.read as any).data.Active = true;
            that.admissionReRepSource.read();
            // setup incrementTypeId, and IncrmentTypeText
            jQuery('#ddlReRep')
                .kendoDropDownList({
                    dataTextField: "FullName",
                    dataValueField: "Id",
                    dataSource: that.admissionReRepSource,
                    minLength: 5,
                    change: function () {
                        var ddlReCampus: kendo.ui.DropDownList = $("#ddlReCampus").data("kendoDropDownList") as any;
                        (that.leadReDataSource.options.transport.read as any).data.CampusId = ddlReCampus.dataItem().ID;
                        // ReSharper disable SuspiciousThisUsage
                        (that.leadReDataSource.options.transport.read as any).data.RepId = this.value();
                        // ReSharper restore SuspiciousThisUsage
                        that.updateReControls("rep", "ddlRepInitialize");
                        that.setPageSize("leadsReGrid", 20);

                    }
                });
        };
        setPageSize(gridName: string, pgSize: number) {
            var grid: kendo.ui.Grid = $("#" + gridName).data("kendoGrid") as any;
            grid.dataSource.pageSize(pgSize);
        }
        bindReAssignButton() {
            let that = this;
            $("#btnReAssignLeadsToCampus").unbind('click');
            $("#btnReAssignLeadsToCampus")
                .bind("click",
                function () {

                    var leadIds = [];
                    var selRadio = that.reAssignRadio;
                    if (that.reSelectedLeadIds.length === 0) {
                        if (selRadio === "reRep")
                            MasterPage.SHOW_WARNING_WINDOW("No leads were selected to reassign to this admissions rep.");
                        // this.viewModel.showNotification(, "center");
                        else
                            MasterPage.SHOW_WARNING_WINDOW("No leads were selected to reassign to this campus.");
                        //that.showNotification(, "center");
                        return;
                    }

                    var campusId: string;
                    var repId: string;
                    var msg: string;
                    var type: string;

                    var ddlreCampus: kendo.ui.DropDownList = $("#ddlReCampus").data("kendoDropDownList") as any;
                    if (selRadio === "reRep") {
                        if (!that.selectedReRep) {
                            MasterPage.SHOW_WARNING_WINDOW("No rep was selected.");
                            return;
                        } else {

                            repId = that.selectedReRepId;
                            campusId = "";
                            msg = that.selectedReRep;
                            type = "rep";
                        }

                    } else {

                        if (!that.selectedReCampusId) {
                            MasterPage.SHOW_WARNING_WINDOW("No campus was selected.");
                            // that.showNotification("No campus was selected.", "center");
                            return;
                        } else if (that.selectedReCampusId === ddlreCampus.dataItem().ID) {
                            MasterPage.SHOW_WARNING_WINDOW("Please choose a different campus than the currently assigned campus.");
                            //that.showNotification("Please choose a different campus than the currently assigned campus.", "center");
                            return;
                        } else {

                            repId = "";
                            campusId = that.selectedReCampusId;
                            msg = that.selectedReCampus;
                            type = "campus";
                        }
                    }

                    $.each(that.reSelectedLeadIds, function (index, value) {
                        var leadId = { Id: value };
                        leadIds[index] = leadId;
                    });

                    var input = {
                        LeadIds: leadIds,
                        CampusId: campusId,
                        Repid: repId,
                        UserId: $("#hdnUserId").val()
                    };

                    that.updateReAssignLeads(input, msg, type);
                });
        };

        updateReAssignLeads(input, msg, type) {
            let that = this;
            this.db.updateReAssignedLeadsToServer(input)
                .done(() => {
                    //that.showNotification("Leads reassigned to " + msg, "center");
                    that.updateReControls(type, "other");
                    that.reSelectedLeadIds = [];
                    that.selectedReCampusId = "";
                    that.selectedReCampus = "";
                    that.selectedReRepId = "";
                    that.selectedReRep = "";
                    MasterPage.SHOW_INFO_WINDOW("Leads reassigned to " + msg);
                })
                .fail(e => {
                    MasterPage.SHOW_ERROR_WINDOW("Error occurred during update");
                    //that.showNotification(, "center");
                    console.log(e);
                });
        }

        //-------------------------------------------------------------------------------------------------------
        // notification functions
        //-------------------------------------------------------------------------------------------------------

        //positionAtCenter(e) {

        //    if (!$("." + e.sender._guid)[1]) {
        //        var element = e.element.parent(),
        //            eWidth = element.width(),
        //            eHeight = element.height(),
        //            wWidth = $(window).width(),
        //            wHeight = $(window).height(),
        //            newTop,
        //            newLeft;

        //        newLeft = Math.floor(wWidth / 2 - eWidth / 2);
        //        newTop = Math.floor(wHeight / 2 - eHeight / 2);

        //        e.element.parent().css({ top: newTop, left: newLeft, zIndex: 22222 });
        //    }
        //};

        /* Show notification screen*/
        //showNotification(message: string, placement: string) {

        //    var notificationWidget;

        //    if (placement === "center") {
        //        notificationWidget = $("#kNotification").kendoNotification({
        //            stacking: "down",
        //            show: this.positionAtCenter,
        //            autoHideAfter: 3000,
        //            templates: [{ type: "info", template: $("#infoTemplate").html() }]

        //        })
        //            .data("kendoNotification");
        //    } else {

        //        notificationWidget = $("#kNotification")
        //            .kendoNotification({
        //                stacking: "down",
        //                position: { bottom: 30, right: 50 },
        //                autoHideAfter: 3000,
        //                templates: [{ type: "info", template: $("#infoTemplate").html() }]

        //            })
        //            .data("kendoNotification");
        //    }

        //    notificationWidget.show({
        //        title: "&nbsp;&nbsp;&nbsp;&nbsp;",
        //        message: message,
        //        src: XMASTER_GET_BASE_URL + "/images/info.jpg"
        //    });
        //};
    }
} 