var AD;
(function (AD) {
    var LeadDuplicateDb = (function () {
        function LeadDuplicateDb() {
        }
        LeadDuplicateDb.prototype.showNotification = function (message) {
            var notif = $("#kNotification").kendoNotification({
                stacking: "down",
                position: { bottom: 30, right: 50 },
                autoHideAfter: 3000,
                templates: [{ type: "info", template: $("#infoTemplate").html() }]
            });
            notif.data("kendoNotification").show({ title: "Advantage", message: message, src: AD.XIMAGE_INFO }, "info");
        };
        LeadDuplicateDb.prototype.showNotificationCentered = function (message) {
            var notif = $("#kNotification").kendoNotification({
                stacking: "down",
                show: this.onShow,
                autoHideAfter: 3000,
                templates: [{ type: "info", template: $("#infoTemplate").html() }]
            });
            notif.data("kendoNotification").show({ title: "Advantage", message: message, src: AD.XIMAGE_INFO }, "info");
        };
        LeadDuplicateDb.prototype.onShow = function (e) {
            if (!$("." + e.sender._guid)[1]) {
                var element = e.element.parent(), eWidth = element.width(), eHeight = element.height(), wWidth = $(window).width(), wHeight = $(window).height();
                var newLeft = Math.floor(wWidth / 2 - eWidth / 2);
                var newTop = Math.floor(wHeight / 2 - eHeight / 2);
                e.element.parent().css({ top: newTop, left: newLeft });
            }
        };
        LeadDuplicateDb.prototype.getDuplicateLeadsList = function () {
            var url = AD.XGET_SERVICE_LAYER_LEAD_DUPLICATES_GET;
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }
                },
                error: ad.SHOW_DATA_SOURCE_ERROR,
                schema: {
                    model: {
                        NewLeadGuid: "NewLeadGuid",
                        DuplicateGuid: "DuplicateGuid",
                        PosibleDuplicateFullName: "PosibleDuplicateFullName"
                    }
                }
            });
            return ds;
        };
        LeadDuplicateDb.prototype.commandAndGetDuplicateLeadsList = function (command, newleadguid, duplicateguid, userid) {
            var url = AD.XGET_SERVICE_LAYER_LEAD_DUPLICATES_COMMAND_GET + "?Command=" + command + "&LeadNewGuid=" + newleadguid + "&LeadDuplicateGuid=" + duplicateguid + "&UserId=" + userid;
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }
                },
                error: ad.SHOW_DATA_SOURCE_ERROR,
                schema: {
                    model: {
                        NewLeadGuid: "NewLeadGuid",
                        DuplicateGuid: "DuplicateGuid",
                        PosibleDuplicateFullName: "PosibleDuplicateFullName"
                    }
                }
            });
            return ds;
        };
        LeadDuplicateDb.prototype.getDuplicatePairForServer = function (newleadguid, duplicateguid) {
            var url = AD.XGET_SERVICE_LAYER_LEAD_DUPLICATES_PAIR_GET + "?LeadNewGuid=" + newleadguid + "&LeadDuplicateGuid=" + duplicateguid;
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }
                },
                error: ad.SHOW_DATA_SOURCE_ERROR,
                schema: {
                    model: {
                        Header: "Header",
                        AcquiredDate: "AcquiredDate",
                        Id: "Id",
                        From: "From",
                        FullName: "FullName",
                        Address1: "Address1",
                        Address2: "Address2",
                        City: "City",
                        State: "State",
                        Zip: "Zip",
                        Status: "Status",
                        Phone: "Phone",
                        Email: "Email",
                        SourceInfo: "SourceInfo",
                        AdmissionsRep: "AdmissionsRep",
                        CampusOfInterest: "CampusOfInterest",
                        Campus: "Campus",
                        ProgramOfInterest: "ProgramOfInterest",
                        Comments: "CampusOfInterest"
                    }
                }
            });
            return ds;
        };
        return LeadDuplicateDb;
    }());
    AD.LeadDuplicateDb = LeadDuplicateDb;
})(AD || (AD = {}));
//# sourceMappingURL=LeadDuplicateDb.js.map