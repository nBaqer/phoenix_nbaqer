declare module AD {
    class LeadAssignmentDb {
        admissionRepDataSource(): kendo.data.DataSource;
        admissionReRepDataSource(): kendo.data.DataSource;
        leadUnassignedDataSource(): kendo.data.DataSource;
        leadReassignDataSource(): kendo.data.DataSource;
        campusWithActiveLeadDataSource(): kendo.data.DataSource;
        campusOfInterestDataSource(): kendo.data.DataSource;
        campusesDataSource(): kendo.data.DataSource;
        leadVendorDataSource(): kendo.data.DataSource;
        getNotAssignedLeadsCount(): JQueryXHR;
        putAssignedLeadsToServer(input: any): JQueryXHR;
        updateReAssignedLeadsToServer(input: any): JQueryXHR;
    }
}
