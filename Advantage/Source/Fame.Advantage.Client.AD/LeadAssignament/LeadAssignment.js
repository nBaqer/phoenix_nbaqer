var AD;
(function (AD) {
    var LeadAssignment = (function () {
        function LeadAssignment() {
            var _this = this;
            this.viewModel = new AD.LeadAssigmentVm();
            this.rbReAssignCampus = true;
            this.rbReAssignRep = false;
            this.rbCampus = true;
            this.rbRep = false;
            $("#tabstrip").kendoTabStrip({
                select: function (e) {
                    _this.selectTab(e, _this);
                },
                animation: {
                    close: {
                        duration: 200,
                        effects: "fadeOut"
                    },
                    open: {
                        duration: 200,
                        effects: "fadeIn"
                    }
                }
            });
            this.viewModel.initializeCampusRealDropdown();
            this.viewModel.initializeVendorDropdown();
            this.viewModel.initializeCampusOfInterestDropdown();
            this.viewModel.initalizeReDateDropdown();
            this.viewModel.initalizeReCampusDropdown();
            this.viewModel.initializeReAssignRepDropdown();
            this.initializeGrid();
            this.initializeCampusGrid();
            this.viewModel.getNotAssignedCounts();
            this.bindRadioChange();
            this.bindAssignButton();
            this.bindReRadioChange();
            this.viewModel.bindReAssignButton();
        }
        LeadAssignment.prototype.updateControls = function (mode) {
            var dll = $("#ddlCampuses").data("kendoDropDownList");
            var leadGrid = $("#leadsGrid").data('kendoGrid');
            leadGrid.dataSource.data([]);
            this.viewModel.leadDataSource.options.transport.read.data.baseUrl = XMASTER_GET_BASE_URL;
            if (mode === "campus") {
                this.viewModel.initializeCampusOfInterestDropdown();
                $("#lblCampus").html("Campus of Interest");
                $("#campusRepGrid").attr("style", "height:89%; width: 99%; display:none");
                $("#campusGrid").attr("style", "height:89%; width: 99%");
                this.initializeCampusGrid();
                this.viewModel.leadDataSource.options.transport.read.data.CampusId = "";
                this.viewModel.leadDataSource.options.transport.read.data.CampusOfInterest = dll.dataItem().value;
                $("#lblCampus").html("Campus of Interest");
                $("#btnAssignLeadsToCampus").html("Assign Leads to this Campus");
                $("#step3Header").html("3.&nbsp;&nbsp;Select Campus");
            }
            else {
                this.viewModel.initializeCampusRealDropdown();
                $("#campusGrid").attr("style", "height:89%; width: 99%; display:none");
                $("#campusRepGrid").attr("style", "height:89%; width: 99%");
                this.viewModel.initializeRepGrid();
                this.viewModel.leadDataSource.options.transport.read.data.CampusId = dll.dataItem().ID;
                this.viewModel.leadDataSource.options.transport.read.data.CampusOfInterest = "";
                $("#btnAssignLeadsToCampus").html("Assign Leads to this Rep");
                $("#step3Header").html("3.&nbsp;&nbsp;Select Admissions Rep");
                $("#campusGrid th[data-title=FullName] .k-link").html("Admissions Rep");
                $("#lblCampus").html("Campus");
            }
            this.viewModel.leadDataSource.read();
            this.viewModel.getNotAssignedCounts();
            $("#ckSelectAllLeads").removeAttr("checked");
        };
        ;
        LeadAssignment.prototype.initializeGrid = function () {
            var leadGrid = $("#leadsReGrid").data("kendoGrid");
            this.viewModel.leadDataSource.options.transport.read.data.baseUrl = XMASTER_GET_BASE_URL;
            if (leadGrid !== undefined && leadGrid !== null) {
            }
            else {
                $("#leadsGrid")
                    .kendoGrid({
                    dataSource: this.viewModel.leadDataSource,
                    sortable: true,
                    selectable: false,
                    scrollable: true,
                    resizable: true,
                    pageable: {
                        info: true,
                        refresh: false,
                        pageSizes: 10,
                        previousNext: true,
                        numeric: false
                    },
                    columns: [
                        {
                            template: "<input type=\"checkbox\" class=\"checkbox\" />",
                            width: 24,
                            headerTemplate: "<input type='checkbox'  id='ckSelectAllLeads' />"
                        },
                        { field: "Id", title: "Id", hidden: true },
                        { field: "FullName", title: "Name", width: 150 },
                        { field: "ProgramOfInterest", title: "Program of Interest", width: 100 },
                        { field: "CampusOfInterest", title: "Campus of Interest", width: 100 },
                        { field: "City", title: "City", width: 100 },
                        { field: "State", title: "State", width: 60 },
                        { field: "Zip", title: "Zip", width: 60 }
                    ]
                });
                var grid = $('#leadsGrid').data('kendoGrid');
                $("ckSelectAllLeads").attr("checked", "false");
                var that_1 = this;
                grid.table.on("click", ".checkbox", function () {
                    that_1.viewModel.selectRow(this, that_1);
                });
                this.viewModel.bindSelectAllLeads();
            }
        };
        ;
        LeadAssignment.prototype.initializeCampusGrid = function () {
            var _this = this;
            this.viewModel.readGridDatsSources();
            var gridcampus = $("#campusGrid").data("kendoGrid");
            if (gridcampus !== undefined && gridcampus !== null) {
            }
            else {
                $("#campusGrid")
                    .kendoGrid({
                    dataSource: this.viewModel.campusGridDataSource,
                    scrollable: true,
                    sortable: false,
                    resizable: true,
                    selectable: true,
                    columns: [
                        { field: "Id", title: "Id", hidden: true },
                        {
                            field: "ActiveLeadCount",
                            title: "Active Leads",
                            width: 50,
                            template: "#: kendo.toString(ActiveLeadCount, 'n0') #",
                            attributes: { style: "text-align:right;" }
                        },
                        { field: "Description", title: "Campus" }
                    ],
                    change: function (e) {
                        var selectedDataItem = e != null ? e.sender.dataItem(e.sender.select()) : null;
                        if (selectedDataItem != null) {
                            _this.viewModel.selectedCampusId = selectedDataItem.Id;
                            _this.viewModel.selectedCampus = selectedDataItem.Description;
                        }
                    }
                });
            }
            this.viewModel.campusGridDataSource.read();
            $("#campusGrid th[data-field=Description]").html("Campus");
        };
        ;
        LeadAssignment.prototype.updateAssignLeads = function (input, msg, type) {
            var _this = this;
            var that = this;
            this.viewModel.db.putAssignedLeadsToServer(input)
                .done(function () {
                _this.viewModel.showNotification("Leads assigned to " + msg, "center");
                _this.updateControls(type);
                _this.viewModel.leadDataSource.read();
                _this.viewModel.selectedLeadIds = [];
                that.viewModel.selectedCampusId = "";
                that.viewModel.selectedCampus = "";
                that.viewModel.selectedRepId = "";
                that.viewModel.selectedRep = "";
            })
                .fail(function (e) {
                _this.viewModel.showNotification("Error occurred during update", "center");
                console.log(e);
            });
        };
        ;
        LeadAssignment.prototype.selectTab = function (e, that) {
            var tabId = e.item.id;
            var rdoCampus = document.getElementById("rdoCampus");
            var rdoRep = document.getElementById("rdoRep");
            var rdoReCampus = document.getElementById("rdoReCampus");
            var rdoReRep = document.getElementById("rdoReRep");
            this.rbCampus = rdoCampus.checked;
            this.rbRep = rdoRep.checked;
            this.rbReAssignCampus = rdoReCampus.checked;
            this.rbReAssignRep = rdoReRep.checked;
            if (tabId === "ReassignToCampsuRep") {
                if (this.rbReAssignCampus) {
                    rdoReCampus.checked = "checked";
                    rdoReRep.removeAttribute("checked");
                    that.viewModel.updateReControls("campus", "other");
                }
                if (this.rbReAssignRep) {
                    rdoReCampus.removeAttribute("checked");
                    rdoReRep.checked = "checked";
                    that.viewModel.updateReControls("rep", "change");
                }
            }
            if (tabId === "assignToCampusRep") {
                if (this.rbCampus) {
                    rdoReCampus.checked = "checked";
                    that.updateControls("campus");
                }
                if (this.rbRep) {
                    rdoReRep.checked = "checked";
                    that.updateControls("rep");
                }
            }
        };
        ;
        LeadAssignment.prototype.bindRadioChange = function () {
            var that = this;
            $("input[name='radio']").change(function () {
                var selRadio = $(this).val();
                that.viewModel.assignRadio = selRadio;
                that.viewModel.clearSelections();
                if (selRadio === "rRep")
                    that.updateControls("rep");
                else
                    that.updateControls("campus");
            });
        };
        ;
        LeadAssignment.prototype.bindAssignButton = function () {
            var _this = this;
            $("#btnAssignLeadsToCampus").bind("click", function () {
                var leadIds = [];
                var selRadio = _this.viewModel.assignRadio;
                if (_this.viewModel.selectedLeadIds.length === 0) {
                    if (selRadio === "rRep")
                        _this.viewModel.showNotification("No leads were selected to assign to this admissions rep.", "center");
                    else
                        _this.viewModel.showNotification("No leads were selected to assign to this campus.", "center");
                    return;
                }
                var campusId;
                var repId;
                var msg;
                var type;
                if (selRadio === "rRep") {
                    if (!_this.viewModel.selectedRep) {
                        _this.viewModel.showNotification("No Admissions rep was selected.", "center");
                        return;
                    }
                    else {
                        repId = _this.viewModel.selectedRepId;
                        campusId = "";
                        msg = _this.viewModel.selectedRep;
                        type = "rep";
                    }
                }
                else {
                    if (!_this.viewModel.selectedCampus) {
                        _this.viewModel.showNotification("No campus was selected.", "center");
                        return;
                    }
                    else {
                        repId = "";
                        campusId = _this.viewModel.selectedCampusId;
                        msg = _this.viewModel.selectedCampus;
                        type = "campus";
                    }
                }
                $.each(_this.viewModel.selectedLeadIds, function (index, value) {
                    var leadId = { Id: value };
                    leadIds[index] = leadId;
                });
                var input = {
                    LeadIds: leadIds,
                    CampusId: campusId,
                    Repid: repId,
                    UserId: $("#hdnUserId").val()
                };
                _this.updateAssignLeads(input, msg, type);
            });
        };
        ;
        LeadAssignment.prototype.bindReRadioChange = function () {
            var that = this;
            $("input[name='reradio']")
                .change(function () {
                var selRadio = $(this).val();
                that.viewModel.reAssignRadio = selRadio;
                that.viewModel.clearSelections();
                if (selRadio === "reRep") {
                    that.viewModel.updateReControls("rep", "change");
                }
                else {
                    that.viewModel.updateReControls("campus", "other");
                }
            });
        };
        ;
        return LeadAssignment;
    }());
    AD.LeadAssignment = LeadAssignment;
})(AD || (AD = {}));
//# sourceMappingURL=LeadAssignment.js.map