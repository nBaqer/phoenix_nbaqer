﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
module AD {

    export class LeadDuplicateVm extends kendo.Observable {

        duplicateLeadlistView: kendo.data.DataSource;
        dummyDataSource: kendo.data.DataSource;
        db: LeadDuplicateDb;
       // duplicatesCount: number;
        constructor() {
            super();
            this.db = new LeadDuplicateDb();
            //this.duplicatesCount = 0;
            this.duplicateLeadlistView = this.db.getDuplicateLeadsList();
            this.duplicateLeadlistView.unbind("change");
            this.duplicateLeadlistView.bind("change", this.duplicateLeadlistViewChanged);
            this.duplicateLeadlistView.read();
            this.dummyDataSource = kendo.data.DataSource.create({
                data: [
                    {}
                ]
            });
        }

        duplicateLeadlistViewChanged(e) {
            let duplicatesCount = e.items.length;
            if (duplicatesCount === 0) {
                $("#duplicateLeadTab .k-link").html("Duplicates (" + duplicatesCount + ")");
            }
            else {
                $("#duplicateLeadTab .k-link").html("Duplicates (<span style='color:red'>" + duplicatesCount + "</span>)");
            }
        }

        getDuplicateInformationFromServer() {
            try {
                $("body").css("cursor", "progress");
                // Read from list the selected value
                var listView: any = $("#duplicateLeadlistView").data("kendoListView");
                var newLead = listView.select();

                var newLeadGuid = $.map(newLead, function (item) {
                    return listView.dataSource.view()[$(item).index()].NewLeadGuid;
                });
                var dupLeadGuid = $.map(newLead, function (item) {
                    return listView.dataSource.view()[$(item).index()].DuplicateGuid;
                });

                // Request the info from server
                var datasource = this.db.getDuplicatePairForServer(newLeadGuid, dupLeadGuid);
               
                // Get the list to be assigned
                var visor: kendo.ui.ListView = $("#duplicateHeaderLeadlistView").data("kendoListView") as any;
                visor.setDataSource(datasource);

                // Enable buttons
                var btn1 = $("#buttonDupUpdateLeadB").data("kendoButton") as any;
                var btn2 = $("#buttonDupDeleteLeadA").data("kendoButton") as any;
                var btn3 = $("#buttonDupCreateLeadA").data("kendoButton") as any;
                var btn4 = $("#buttonDupCreateLeadB").data("kendoButton") as any;
                btn1.enable(true);
                btn2.enable(true);
                btn3.enable(true);
                btn4.enable(true);

            }
            finally {
                $("body").css("cursor", "default");
            }
        }

        updateDuplicate = () => {
            this.sendCommand(1);
        };

        deleteDuplicate = () => {
            this.sendCommand(2);
        };


        createLeadFromA = () => {
            this.sendCommand(3);
        };

        createLeadFromB = () => {
            this.sendCommand(4);
        };

        decoreItems = () => {
            var datas = $("#duplicateHeaderLeadlistView").data("kendoListView") as any;
            if (typeof datas !== "undefined") {
                var ds = datas.dataSource.data();
                if (ds == null || ds.length < 3) {
                    return;
                }

                let item1 = $(".item1");
                if (item1[2].innerText.indexOf("12:00:00 AM") > -1) {
                    item1[2].innerText = item1[2].innerText.replace("12:00:00 AM", "");
                }
                if (item1[1].innerText.indexOf("12:00:00 AM") > -1) {
                    item1[1].innerText = item1[1].innerText.replace("12:00:00 AM", "");
                }

                // ReSharper disable UnknownCssClass
                if (ds[1].From === ds[2].From) {
                    $(".item2").css("background", "transparent");
                    $("#buttonDupUpdateLeadB").css("display", "none");
                    $("#buttonDupDeleteLeadA").css("display", "none");
                    $("#buttonDupCreateLeadA").css("display", "inline");
                    $("#buttonDupCreateLeadB").css("display", "inline");
                } else {
                    $(".item2:not(:first)").css("background", "yellow");
                    $("#buttonDupUpdateLeadB").css("display", "inline");
                    $("#buttonDupDeleteLeadA").css("display", "inline");
                    $("#buttonDupCreateLeadA").css("display", "inline");
                    $("#buttonDupCreateLeadB").css("display", "none");
                };

                if (ds[1].AcquiredDate === ds[2].AcquiredDate) { $(".item1").css("background", "transparent"); } else { $(".item1:not(:first)").css("background", "yellow"); };
                if (ds[1].FullName === ds[2].FullName) { $(".item3").css("background", "transparent"); } else { $(".item3:not(:first)").css("background", "yellow"); };
                if (ds[1].Address1 === ds[2].Address1) { $(".item4").css("background", "transparent"); } else { $(".item4:not(:first)").css("background", "yellow"); };
                if (ds[1].Address2 === ds[2].Address2) { $(".item5").css("background", "transparent"); } else { $(".item5:not(:first)").css("background", "yellow"); };
                if (ds[1].City === ds[2].City) { $(".item6").css("background", "transparent"); } else { $(".item6:not(:first)").css("background", "yellow"); };
                if (ds[1].State === ds[2].State) { $(".item7").css("background", "transparent"); } else { $(".item7:not(:first)").css("background", "yellow"); };
                if (ds[1].Zip === ds[2].Zip) { $(".item8").css("background", "transparent"); } else { $(".item8:not(:first)").css("background", "yellow"); };
                if (ds[1].Status === ds[2].Status) { $(".item9").css("background", "transparent"); } else { $(".item9:not(:first)").css("background", "yellow"); };
                if (ds[1].Phone === ds[2].Phone) { $(".item10").css("background", "transparent"); } else { $(".item10:not(:first)").css("background", "yellow"); };
                if (ds[1].Email === ds[2].Email) { $(".item11").css("background", "transparent"); } else { $(".item11:not(:first)").css("background", "yellow"); };
                if (ds[1].SourceInfo === ds[2].SourceInfo) { $(".item12").css("background", "transparent"); } else { $(".item12:not(:first)").css("background", "yellow"); };
                if (ds[1].AdmissionsRep === ds[2].AdmissionsRep) { $(".item13").css("background", "transparent"); } else { $(".item13:not(:first)").css("background", "yellow"); };
                if (ds[1].Campus === ds[2].Campus) { $(".item14").css("background", "transparent"); } else { $(".item14:not(:first)").css("background", "yellow"); };
                if (ds[1].ProgramOfInterest === ds[2].ProgramOfInterest) { $(".item15").css("background", "transparent"); } else { $(".item15:not(:first)").css("background", "yellow"); };
                if (ds[1].Comments === ds[2].Comments) { $(".item16").css("background", "transparent"); } else { $(".item16:not(:first)").css("background", "yellow"); };

                // ReSharper restore UnknownCssClass   
            }
        };

        // Send Command to Server to execute the action
        // Valid values are:
        // 1,2,3,4
        sendCommand(command: number) {
            switch (command) {
                case 1:
                    {
                        $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(X_MESSAGE_DUPLICATED_LEAD_A_TO_B))
                            .then(confirmed => {
                                if (confirmed) {
                                    // YES was selected
                                    this.executeSend(1);
                                } else {
                                    // NO was selected
                                    return;
                                }
                            });
                        break;
                    }
                case 2:
                    {
                        $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(X_MESSAGE_DUPLICATED_DELETE_A))
                            .then(confirmed => {
                                if (confirmed) {
                                    // YES was selected
                                    this.executeSend(2);
                                } else {
                                    // NO was selected
                                    return;
                                }
                            });
                        break;
                    }
                case 3:
                    {
                        $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(X_MESSAGE_DUPLICATED_NEW_LEAD_A))
                            .then(confirmed => {
                                if (confirmed) {
                                    // YES was selected
                                    this.executeSend(3);
                                } else {
                                    // NO was selected
                                    return;
                                }
                            });
                        break;
                    }
                case 4:
                    {
                        $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(X_MESSAGE_DUPLICATED_NEW_LEAD_B))
                            .then(confirmed => {
                                if (confirmed) {
                                    // YES was selected
                                    this.executeSend(4);
                                } else {
                                    // NO was selected
                                    return;
                                }
                            });
                        break;
                    }
                default:
                    {
                        MasterPage.SHOW_WARNING_WINDOW("Command Not recognize. Operation is aborted!");
                        return;
                    }
            }
        }

        /**
        * Execute the action sending a request to server.
        * @param command the command number
        */
        executeSend(command: number) {
            try {
                $("body").css("cursor", "progress");
                var list: kendo.ui.ListView = $("#duplicateLeadlistView").data("kendoListView") as any;
                var newLead = list.select();
                var newLeadGuid = $.map(newLead, item => list.dataSource.view()[$(item).index()].NewLeadGuid);
                var dupLeadGuid = $.map(newLead, item => list.dataSource.view()[$(item).index()].DuplicateGuid);

                // Get user id
                var userId = $("#hdnUserId").val();

                this.db.commandsDuplicates(this, command, newLeadGuid, dupLeadGuid, userId)
                    .done(msg => {
                        if (msg != undefined) {
                            list.dataSource.data(msg);
                            //this.duplicateLeadlistView = new kendo.data.DataSource({
                            //    data: msg
                            //});
                           // list.setDataSource(this.duplicateLeadlistView);

                            var datasource = ($("#duplicateHeaderLeadlistView").data("kendoListView") as any).dataSource;
                            datasource.remove(datasource.at(2));
                            datasource.remove(datasource.at(1));
                            datasource.remove(datasource.at(0));

                            // Show message of done..
                            MasterPage.SHOW_INFO_WINDOW("Operation confirmed!");
                        }
                    })
                    .fail(msg => {
                        $("body").css("cursor", "default");
                        MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                        return;
                    });
            }
            finally {
                $("body").css("cursor", "default");
            }
        }
    }
} 