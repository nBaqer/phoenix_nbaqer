﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
module AD {

    export class LeadDuplicate {

        public viewModel: LeadDuplicateVm;
        //public debug: boolean = true;

        constructor() {
            try {
            this.viewModel = new LeadDuplicateVm();
            var ethis = this;

            //if (this.viewModel.duplicatesCount === 0) {
            //    $("#duplicateLeadTab .k-link").html("Duplicates (" + this.viewModel.duplicatesCount + ")");
            //}
            //else {
            //    $("#duplicateLeadTab .k-link").html("Duplicates (<span style='color:red'>" + this.viewModel.duplicatesCount + "</span>)");
            //}

            $("#duplicateLeadlistView").kendoListView({
                dataSource: ethis.viewModel.duplicateLeadlistView
                , template: kendo.template($("#templateDuplicateLeadListView").html())
                , selectable: "single"
                , change: () => {
                    this.viewModel.getDuplicateInformationFromServer();
                    
                }
            });

            

            $("#duplicateHeaderLeadlistView").kendoListView({
                template: kendo.template($("#templateduplicateHeaderLeadlistView").html()),
                dataBound: ethis.viewModel.decoreItems
            });

            $("#buttonDupUpdateLeadB").kendoButton({
                icon: "folder-up",
                enable:false,
                click: ethis.viewModel.updateDuplicate
            });

            $("#buttonDupDeleteLeadA").kendoButton({
                icon: "close",
                enable:false,
                click: ethis.viewModel.deleteDuplicate
            });

            $("#buttonDupCreateLeadA").kendoButton({
                icon: "plus",
                enable:false,
                click: ethis.viewModel.createLeadFromA
            });

            $("#buttonDupCreateLeadB").kendoButton({
                icon: "plus",
                enable: false,
                click: ethis.viewModel.createLeadFromB
            });

            $("#buttonDupCreateLeadB").css("display", "none");



           // This bind the view model with all.-------------------------------------------------------------------------
            kendo.init($("#leadDuplicatePagebind"));
            kendo.bind($("#leadDuplicatePagebind"), ethis.viewModel);
            }
            catch (e) {
                MasterPage.SHOW_ERROR_WINDOW(e.message + "/n" + e.stack);
           }
        }

    }
} 