/// <reference path="../ServiceUrls.d.ts" />
declare module AD {
    class LeadDuplicateDb {
        showNotification(message: string): void;
        showNotificationCentered(message: string): void;
        onShow(e: any): void;
        getDuplicateLeadsList(): kendo.data.DataSource;
        commandAndGetDuplicateLeadsList(command: number, newleadguid: string, duplicateguid: string, userid: string): kendo.data.DataSource;
        getDuplicatePairForServer(newleadguid: string, duplicateguid: string): kendo.data.DataSource;
    }
}
