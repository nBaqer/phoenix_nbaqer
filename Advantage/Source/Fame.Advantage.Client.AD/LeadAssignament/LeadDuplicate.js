var AD;
(function (AD) {
    var LeadDuplicate = (function () {
        function LeadDuplicate() {
            var _this = this;
            try {
                this.viewModel = new AD.LeadDuplicateVm();
                var ethis = this;
                $("#duplicateLeadlistView").kendoListView({
                    dataSource: ethis.viewModel.duplicateLeadlistView,
                    template: kendo.template($("#templateDuplicateLeadListView").html()),
                    selectable: "single",
                    change: function () {
                        _this.viewModel.GetDuplicateInformationFromServer();
                    },
                    dataBinding: function () {
                        var listview = ($("#duplicateLeadlistView").data("kendoListView"));
                        var count = listview.dataItems.length;
                        if (count === 0) {
                            $("#duplicateLeadTab").children(".k-link").html("Duplicates (" + count + ")");
                        }
                        else {
                            $("#duplicateLeadTab").children(".k-link").html("Duplicates (<span style='color:red'>" + count + "</span>)");
                        }
                        $("#duplicateLeadTab").children(".k-link").css("font-weight", "bold");
                    }
                });
                $("#duplicateHeaderLeadlistView").kendoListView({
                    template: kendo.template($("#templateduplicateHeaderLeadlistView").html()),
                    dataBound: ethis.viewModel.DecoreItems
                });
                $("#buttonDupUpdateLeadB").kendoButton({
                    icon: "folder-up",
                    enable: false,
                    click: ethis.viewModel.updateDuplicate
                });
                $("#buttonDupDeleteLeadA").kendoButton({
                    icon: "close",
                    enable: false,
                    click: ethis.viewModel.deleteDuplicate
                });
                $("#buttonDupCreateLeadA").kendoButton({
                    icon: "plus",
                    enable: false,
                    click: ethis.viewModel.createLeadFromA
                });
                $("#buttonDupCreateLeadB").kendoButton({
                    icon: "plus",
                    enable: false,
                    click: ethis.viewModel.createLeadFromB
                });
                $("#buttonDupCreateLeadB").css("display", "none");
                kendo.init($("#leadDuplicatePagebind"));
                kendo.bind($("#leadDuplicatePagebind"), ethis.viewModel);
            }
            catch (e) {
                alert(e.message + "/n" + e.stack);
            }
        }
        return LeadDuplicate;
    }());
    AD.LeadDuplicate = LeadDuplicate;
})(AD || (AD = {}));
//# sourceMappingURL=LeadDuplicate.js.map