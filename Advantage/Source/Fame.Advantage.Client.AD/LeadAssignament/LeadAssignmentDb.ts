﻿module AD {
    declare var XMASTER_PAGE_USER_OPTIONS_USERID: string;

    export class LeadAssignmentDb {

        /* Get the users that are Admission Rep.*/
        admissionRepDataSource(): kendo.data.DataSource {
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: () => {
                            var uri = XGET_SERVICE_LAYER_LEAD_ASSIGMENT_USER_GET;
                            return uri;
                        },
                        dataType: "json",
                        type: "GET",
                        data: {
                            ReturnReps: "true",
                            UserId: "",
                            CampusId: "",
                            Active: ""
                        }
                    }
                }
            });
            return ds;
        }

     
        admissionReRepDataSource(): kendo.data.DataSource {
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: () => {
                            var uri = XGET_SERVICE_LAYER_LEAD_ASSIGMENT_USER_GET;
                            return uri;
                        },
                        dataType: "json",
                        type: "GET",
                        data: {
                            ReturnReps: "true",
                            UserId: "",
                            CampusId: "",
                            Active: ""
                        }
                    }
                }
            });
            return ds;
        };

        /* Get the users that are Admission Rep.*/
        leadUnassignedDataSource(): kendo.data.DataSource {
            var ds = new kendo.data.DataSource({
                pageSize: 10,
                transport: {
                    read: {
                        url: function(options) {
                            var uri = XGET_SERVICE_LAYER_LEAD_ASSIGMENT_UNASSIGNED_LEADS_GET; //options.baseUrl + "/proxy/api/Lead/GetByUnassigned";
                            delete options.baseUrl;
                            return uri;
                        },
                        dataType: "json",
                        type: "GET",
                        data: {
                            baseUrl: "",
                            VendorId: "",
                            AreaOfInterest: "",
                            CampusId: ""
                        }
                    }
                }
            });
            return ds;
        }

        leadReassignDataSource(): kendo.data.DataSource {
            var ds = new kendo.data.DataSource({
                pageSize: 20,
                transport: {
                    read: {
                         url: options => {
                             var uri =  options.baseUrl + "/proxy/api/Lead/GetByReassign";
                            delete options.baseUrl;
                            return uri;
                        },
                        dataType: "json",
                        type: "GET",
                        data: {
                            baseUrl: "",
                            CampusId: "",
                            RepId: "",
                            LastModDate: ""
                        }
                    }
                }
            });
            return ds;
        }

        campusWithActiveLeadDataSource(): kendo.data.DataSource {
           
            var db = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: XGET_SERVICE_LAYER_LEAD_ASSIGNMENT_CAMPUS_GET,
                        cache:true,
                        //options => {
                        //    var uri = options.baseUrl + "/proxy/api/CampusWithLead/GetWithActiveLeads";
                        //    delete options.baseUrl;
                        //    return uri;
                        //},
                        dataType: "json",
                        beforeSend: req => {
                            var usernameHiddenField = jQuery("#hdnUserName");
                            req.setRequestHeader("Username", usernameHiddenField.val());
                        },
                        type: "GET",
                        data: {
                            baseUrl: "",
                            IncludeActiveLeads: "",
                            UserId: XMASTER_PAGE_USER_OPTIONS_USERID
                        }
                    }
                }
            });
            return db;
        }

        areaOfInterestDataSource(): kendo.data.DataSource {
            var db = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: options => {
                            var uri = options.baseUrl + "/proxy/api/Lead/GetAreaOfInterest";
                            delete options.baseUrl;
                            return uri;
                        },
                        dataType: "json",
                        type: "GET",
                        data: {
                            baseUrl: ""
                        }
                    }
                }
            });
            return db;
        }
        campusesDataSource(): kendo.data.DataSource {
            var db = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: XGET_SERVICE_LAYER_LEAD_ASSIGNMENT_CAMPUSES_GET,
                        cache: true,
                        dataType: "json",
                        type: "GET",
                        data: {
                            baseUrl: "",
                            UserId: XMASTER_PAGE_USER_OPTIONS_USERID
                        }
                    }
                }
            });
            return db;
        }

        leadVendorDataSource(): kendo.data.DataSource {
            var db = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: options => {
                            var uri = options.baseUrl + "/proxy/api/Lead/LeadVendor/Get";
                            delete options.baseUrl;
                            return uri;
                        },
                        dataType: "json",
                        type: "GET",
                        data: {
                            baseUrl: ""

                        }
                    }
                }
            });
            return db;
        }

        /* Get the quantity of leads not assigned */
        getNotAssignedLeadsCount() {
            var url = "../proxy/api/lead/GetLeadCounts";
            return $.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                dataType: "json"
            });
        }

        /*Put assigned lead to server */
        putAssignedLeadsToServer(input) {
            var url = XPOST_SERVICE_UNASSIGNEDLEAD; //"../proxy/api/lead/UpdateAssignLeads";
            return $.ajax({
                url: url,
                type: "POST",
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify(input)
            });
        }

        /* Update the Lead reassigned in the server */
        updateReAssignedLeadsToServer(input) {
            var url = XPOST_SERVICE_UNASSIGNEDLEAD;
            return $.ajax({
                url: url,
                type: "POST",
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify(input)
            });
        }
    }
} 