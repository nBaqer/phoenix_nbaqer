﻿module AD {

    declare var common: any;
    declare var XMASTER_GET_BASE_URL;

    export class LeadAssignment {

        public viewModel: LeadAssigmentVm;
        leadAssignment: LeadAssignment;

        /*Radio-button status*/
        rbReAssignCampus: boolean;
        rbReAssignRep: boolean;
        rbCampus: boolean;
        rbRep: boolean;

        /*Class Constructor invoke this to initialize page*/
        constructor() {
            this.viewModel = new LeadAssigmentVm();

            /* Radio button initial values*/
            this.rbReAssignCampus = true;
            this.rbReAssignRep = false;
            this.rbCampus = true;
            this.rbRep = false;

            // setup Ajax for the page
            //common.ajaxSetup(this.viewModel);

            /*Set Up Tab Strip */
            $("#tabstrip").kendoTabStrip({
                select: e => {
                    this.selectTab(e, this);
                },
                animation: {
                    // fade-out current tab over 1000 milliseconds
                    close: {
                        duration: 200,
                        effects: "fadeOut"
                    },
                    // fade-in new tab over 500 milliseconds
                    open: {
                        duration: 200,
                        effects: "fadeIn"
                    }
                }
            });
            this.leadAssignment = this;
            this.initialize();

        }

        initialize() {
            //----------------------------------------------------------------------------------------
            // Initialize Grids and DropDownList
            //----------------------------------------------------------------------------------------
            this.viewModel.initializeCampusRealDropdown();
            this.viewModel.initializeVendorDropdown();
            this.viewModel.initializeAreaOfInterestDropdown();
            this.viewModel.initalizeReDateDropdown();
            this.viewModel.initalizeReCampusDropdown();
            this.viewModel.initializeReAssignRepDropdown();
            this.initializeGrid();
            this.initializeCampusGrid();
            this.viewModel.getNotAssignedCounts();

            /* Radio button assign and re-assign tabs*/
            this.bindRadioChange();
            this.bindAssignButton();
            this.bindReRadioChange();
            this.viewModel.bindReAssignButton();
        }

        //#region Assigned to campus and Admission Reps....

        /*Update all controls
         * mode: campus or rep
         */
        updateControls(mode: string) {
            //let campusGrid: kendo.ui.Grid = $("#campusGrid").data("kendoGrid") as any;
            let dll: kendo.ui.DropDownList = $("#ddlCampuses").data("kendoDropDownList") as kendo.ui.DropDownList;
            let leadGrid: kendo.ui.Grid = $("#leadsGrid").data('kendoGrid') as any;
            leadGrid.dataSource.data([]);
            (this.viewModel.leadDataSource.options.transport.read as any).data.baseUrl = XMASTER_GET_BASE_URL;
            if (mode === "campus") {
                this.viewModel.initializeAreaOfInterestDropdown();
                $("#lblCampus").html("Area of Interest");
                $("#campusRepGrid").attr("style", "height:89%; width: 99%; display:none");
                $("#campusGrid").attr("style", "height:89%; width: 99%");
                // Initialize The campus Grid
                this.initializeCampusGrid();

                // Parametrize Lead Grid with Campus of interest
                (this.viewModel.leadDataSource.options.transport.read as any).data.CampusId = "";
                (this.viewModel.leadDataSource.options.transport.read as any).data.AreaOfInterest = dll.dataItem().value;
                $("#lblCampus").html("Area of Interest");
                $("#btnAssignLeadsToCampus").html("Assign Leads to this Campus");
                $("#step3Header").html("3.&nbsp;&nbsp;Select Campus");
            } else {
                this.viewModel.initializeCampusRealDropdown();

                $("#campusGrid").attr("style", "height:89%; width: 99%; display:none");
                $("#campusRepGrid").attr("style", "height:89%; width: 99%");

                //Initiate GridCampus with Admission Rep.
                this.viewModel.initializeRepGrid();

                // Parametrize Lead Grid to real campus
                (this.viewModel.leadDataSource.options.transport.read as any).data.CampusId = dll.value();
                (this.viewModel.leadDataSource.options.transport.read as any).data.AreaOfInterest = "";
                $("#btnAssignLeadsToCampus").html("Assign Leads to this Rep");
                $("#step3Header").html("3.&nbsp;&nbsp;Select Admissions Rep");
                $("#campusGrid th[data-title=FullName] .k-link").html("Admissions Rep");
                $("#lblCampus").html("Campus");

            }

            // Update Lead Table
            this.viewModel.leadDataSource.read();
            this.viewModel.getNotAssignedCounts();
            $("#ckSelectAllLeads").removeAttr("checked");
        };

        /* Initialize the grid */
        initializeGrid() {
            let leadGrid: kendo.ui.Grid = $("#leadsGrid").data("kendoGrid") as any;
            (this.viewModel.leadDataSource.options.transport.read as any).data.baseUrl = XMASTER_GET_BASE_URL;

            if (leadGrid !== undefined && leadGrid !== null) {
            } else {
                //setup kendo grid for log
                let that = this;
                $("#leadsGrid")
                    .kendoGrid({
                        dataSource: this.viewModel.leadDataSource,
                        // height: 590,
                        //dataBound: function() {

                        //    // ReSharper disable SuspiciousThisUsage
                        //    this.pager.element.find(".k-status-text")
                        //        .remove()
                        //        .end()
                        //        .append('<div class="k-status-text">&nbsp;Number of Leads: ' +
                        //            this.dataSource.view().length +
                        //            '</div>');
                        //    // ReSharper restore SuspiciousThisUsage
                        //},
                        dataBound : function() {
                            var dsView = this.dataSource.view();
                            var index = -1;
                            for (var i = 0; i < dsView.length; i++) {
                                index = that.viewModel.selectedLeadIds.indexOf(dsView[i].Id);
                                if (index !== -1) {
                                    this.tbody.find("tr[data-uid='" + dsView[i].uid + "']")
                                        .addClass("k-state-selected")
                                        .find(".checkbox")
                                        .attr("checked", "checked");
                                }
                            }
                        },
                        sortable: true,
                        selectable: false,
                        scrollable: true,
                        resizable: true,
                        pageable: {
                            info: true,
                            refresh: false,
                            pageSizes: 10,
                            previousNext: true,
                            numeric: false
                        },
                        columns: [
                            {
                                template: "<input type=\"checkbox\" class=\"checkbox\" />",
                                width: 24,
                                headerTemplate:
                                // '<input type="checkbox" data-bind="checked: isAllLeadsChecked" id="ckSelectAllLeads" />'
                                "<input type='checkbox'  id='ckSelectAllLeads' />"
                            },
                            { field: "Id", title: "Id", hidden: true },
                            { field: "FullName", title: "Name", width: 150 },
                            //{ field: "ProgramOfInterest", title: "Program of Interest", width: 100 },
                            { field: "AreaOfInterest", title: "Area of Interest", width: 100 },
                            { field: "City", title: "City", width: 100 },
                            { field: "State", title: "State", width: 60 },
                            { field: "Zip", title: "Zip", width: 60 }
                        ]
                    });

                var grid: kendo.ui.Grid = $('#leadsGrid').data('kendoGrid') as any;
                $("ckSelectAllLeads").attr("checked", "false");

                //bind click event to the checkbox
                //let that = this;
                grid.table.on("click",
                    ".checkbox",
                    function () {
                        // ReSharper disable SuspiciousThisUsage
                        that.viewModel.selectRow(this, that);
                        // ReSharper restore SuspiciousThisUsage
                    });

                this.viewModel.bindSelectAllLeads();
            }
        };

        /* Initialize Campus Grid */
        initializeCampusGrid() {
            // let that = this;
            this.viewModel.readGridDatsSources();
            var gridcampus: kendo.ui.Grid = $("#campusGrid").data("kendoGrid") as any;

            //if ($("#campusGrid").data("kendoGrid")) {
            //    gridcampus.dataSource.data([]);
            //    gridcampus.unbind("change");
            //    gridcampus.destroy();
            //}
            if (gridcampus !== undefined && gridcampus !== null) {
            } else {
                //setup kendo grid for log
                $("#campusGrid")
                    .kendoGrid({
                        dataSource: this.viewModel.campusGridDataSource,
                        scrollable: true,
                        sortable: false,
                        resizable: true,
                        selectable: true, // "row",
                        columns: [
                            { field: "Id", title: "Id", hidden: true },
                            {
                                field: "ActiveLeadCount",
                                title: "Active Leads",
                                width: 50,
                                template: "#: kendo.toString(ActiveLeadCount, 'n0') #",
                                attributes: { style: "text-align:right;" }
                            },
                            { field: "Description", title: "Campus" }
                        ],
                        change: e => {
                            var selectedDataItem: any = e != null ? e.sender.dataItem(e.sender.select()) : null;
                            if (selectedDataItem != null) {
                                this.viewModel.selectedCampusId = selectedDataItem.Id;
                                this.viewModel.selectedCampus = selectedDataItem.Description;
                            }
                        }
                    });
            }
            this.viewModel.campusGridDataSource.read();
            //let gridd: any = $('#campusGrid').data('kendoGrid');
            //gridd.dataSource.read();
            $("#campusGrid th[data-field=Description]").html("Campus");
            //gridd.dataSource.refresh();
        };

        /* Update the assigned lead in server*/
        updateAssignLeads(input, msg, type) {
            let that = this;
            this.viewModel.db.putAssignedLeadsToServer(input)
                .done(() => {
                    //this.viewModel.showNotification(, "center");
                    this.updateControls(type);
                    this.viewModel.leadDataSource.read();
                    this.viewModel.selectedLeadIds = [];
                    that.viewModel.selectedCampusId = "";
                    that.viewModel.selectedCampus = "";
                    that.viewModel.selectedRepId = "";
                    that.viewModel.selectedRep = "";
                    MasterPage.SHOW_INFO_WINDOW("Leads assigned to " + msg);
                }
                )
                .fail((e) => {
                    MasterPage.SHOW_ERROR_WINDOW("Error occurred during update");
                    //this.viewModel.showNotification(, "center");
                    console.log(e);
                });
        };

        /*The tab is selected*/
        selectTab(e, that) {

            //this.initialize();

            var tabId = e.item.id;
            /* Get the radio-button */
            let rdoCampus: any = document.getElementById("rdoCampus");
            let rdoRep: any = document.getElementById("rdoRep");
            let rdoReCampus: any = document.getElementById("rdoReCampus");
            let rdoReRep: any = document.getElementById("rdoReRep");

            /* Update the flags */
            this.rbCampus = rdoCampus.checked;
            this.rbRep = rdoRep.checked;
            this.rbReAssignCampus = rdoReCampus.checked;
            this.rbReAssignRep = rdoReRep.checked;

            if (tabId === "ReassignToCampsuRep") {

                if (this.rbReAssignCampus) {
                    rdoReCampus.checked = "checked";
                    rdoReRep.removeAttribute("checked");
                    that.viewModel.updateReControls("campus", "other");
                }
                if (this.rbReAssignRep) {
                    rdoReCampus.removeAttribute("checked");
                    rdoReRep.checked = "checked";
                    that.viewModel.updateReControls("rep", "change");
                }
            }
            if (tabId === "assignToCampusRep") {
                if (this.rbCampus) {
                    rdoReCampus.checked = "checked";
                    that.updateControls("campus");
                }
                if (this.rbRep) {
                    rdoReRep.checked = "checked";
                    that.updateControls("rep");
                }
            }
        };

        bindRadioChange() {
            let that = this;
            $("input[name='radio']").change(function () {
                // ReSharper disable SuspiciousThisUsage
                var selRadio = $(this).val();
                // ReSharper restore SuspiciousThisUsage
                that.viewModel.assignRadio = selRadio;
                that.viewModel.clearSelections();
                if (selRadio === "rRep")
                    that.updateControls("rep");
                else
                    that.updateControls("campus");

                var leadsGrid: kendo.ui.Grid = $("#leadsGrid").data("kendoGrid") as any;
                if (leadsGrid !== undefined && leadsGrid !== null) {
                    leadsGrid.pager.page(1);
                    //leadsGrid.refresh();
                    that.viewModel.setPageSize("leadsGrid", 10);
                }
            });
        };

        bindAssignButton() {
            $("#btnAssignLeadsToCampus").bind("click", () => {
                var leadIds = [];
                var selRadio = this.viewModel.assignRadio;

                if (this.viewModel.selectedLeadIds.length === 0) {
                    if (selRadio === "rRep")
                        MasterPage.SHOW_WARNING_WINDOW("No leads were selected to assign to this admissions rep.");
                        // this.viewModel.showNotification(, "center");
                    else
                        MasterPage.SHOW_WARNING_WINDOW("No leads were selected to assign to this campus.");
                        // this.viewModel.showNotification(, "center");
                    return;
                }

                var campusId: string;
                var repId: string;
                var msg: string;
                var type: string;


                if (selRadio === "rRep") {
                    if (!this.viewModel.selectedRep) {
                        MasterPage.SHOW_WARNING_WINDOW("No Admissions rep was selected.");
                        //this.viewModel.showNotification(, "center");
                        return;
                    } else {
                        repId = this.viewModel.selectedRepId;
                        campusId = "";
                        msg = this.viewModel.selectedRep;
                        type = "rep";
                    }
                } else {

                    if (!this.viewModel.selectedCampus) {
                        MasterPage.SHOW_WARNING_WINDOW("No campus was selected.");
                        //this.viewModel.showNotification(, "center");
                        return;
                    } else {
                        repId = "";
                        campusId = this.viewModel.selectedCampusId;
                        msg = this.viewModel.selectedCampus;
                        type = "campus";
                    }
                }

                $.each(this.viewModel.selectedLeadIds,
                    (index, value) => {
                        var leadId = { Id: value };
                        leadIds[index] = leadId;
                    });

                var input = {
                    LeadIds: leadIds,
                    CampusId: campusId,
                    Repid: repId,
                    UserId: $("#hdnUserId").val()
                };

                this.updateAssignLeads(input, msg, type);
            });
        };
        //#endregion

        /***************************************************/
        /* RE ASSIGNED TAP                                 */
        /***************************************************/



        /*Event re-admission radio button selected change*/
        bindReRadioChange() {
            let that = this;
            $("input[name='reradio']")
                .change(function () {
                    // ReSharper disable SuspiciousThisUsage
                    var selRadio = $(this).val();
                    // ReSharper restore SuspiciousThisUsage
                    that.viewModel.reAssignRadio = selRadio;
                    that.viewModel.clearSelections();

                    if (selRadio === "reRep") {
                        //$(".updateBlocker").show();
                        that.viewModel.updateReControls("rep", "change");
                    } else {
                        // $(".updateBlocker").show();
                        that.viewModel.updateReControls("campus", "other");
                    }
                    var leadregrid: kendo.ui.Grid = $("#leadsReGrid").data("kendoGrid") as any;
                    if (leadregrid !== undefined && leadregrid !== null) {
                        leadregrid.pager.page(1);
                        leadregrid.refresh();
                        that.viewModel.setPageSize("leadsReGrid", 20);
                    }
                });
        };
    }
}