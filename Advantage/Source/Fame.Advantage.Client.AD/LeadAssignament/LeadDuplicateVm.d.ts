/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
declare module AD {
    class LeadDuplicateVm extends kendo.Observable {
        duplicateLeadlistView: kendo.data.DataSource;
        dummyDataSource: kendo.data.DataSource;
        db: LeadDuplicateDb;
        constructor();
        GetDuplicateInformationFromServer(): void;
        updateDuplicate: () => void;
        deleteDuplicate: () => void;
        createLeadFromA: () => void;
        createLeadFromB: () => void;
        DecoreItems: () => void;
        SendCommand(command: number): void;
    }
}
