﻿/// <reference path="../ServiceUrls.ts" />
module AD {

    export class LeadDuplicateDb {
        /*
         * Get list of possible duplicate Leads
         */
        public getDuplicateLeadsList() {
            var url = XGET_SERVICE_LAYER_LEAD_DUPLICATES_GET;
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }

                },
                error: MasterPage.SHOW_DATA_SOURCE_ERROR,

                schema: {
                    model: {
                        NewLeadGuid: "NewLeadGuid",
                        DuplicateGuid: "DuplicateGuid",
                        PosibleDuplicateFullName: "PosibleDuplicateFullName"
                    }
                }
            });
            return ds;
        }

        public commandsDuplicates(mcontext:any, command: number, newLeadGuid: string, duplicateGuid: string, userId: string) {
            var url = XGET_SERVICE_LAYER_LEAD_DUPLICATES_COMMAND_GET +
                "?Command=" +
                command +
                "&LeadNewGuid=" +
                newLeadGuid +
                "&LeadDuplicateGuid=" +
                duplicateGuid +
                "&UserId=" +
                userId;
            return $.ajax({
                context: mcontext,
                url: url,
                type: "GET",
                timeout: 20000,
                dataType: "json"
            });

        }

        // Send a command to server and return the updated list of new duplicates.
        public commandAndGetDuplicateLeadsList(command: number, newleadguid: string, duplicateguid: string, userid: string) {
            var url = XGET_SERVICE_LAYER_LEAD_DUPLICATES_COMMAND_GET + "?Command=" + command + "&LeadNewGuid=" + newleadguid + "&LeadDuplicateGuid=" + duplicateguid + "&UserId=" + userid;
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }

                },
                error: (e)=> {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(e);
                } ,

                schema: {
                    model: {
                        NewLeadGuid: "NewLeadGuid",
                        DuplicateGuid: "DuplicateGuid",
                        PosibleDuplicateFullName: "PosibleDuplicateFullName"
                    }
                }
            });
            return ds;
        }

        public getDuplicatePairForServer(newleadguid: string, duplicateguid: string) {
            var url = XGET_SERVICE_LAYER_LEAD_DUPLICATES_PAIR_GET + "?LeadNewGuid=" + newleadguid + "&LeadDuplicateGuid=" + duplicateguid;
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "GET"
                    }

                },
                error: MasterPage.SHOW_DATA_SOURCE_ERROR,

                schema: {
                    model: {
                        Header: "Header",
                        AcquiredDate: "AcquiredDate",
                        Id: "Id",
                        From: "From",
                        FullName: "FullName",
                        Address1: "Address1",
                        Address2: "Address2",
                        City: "City",
                        State: "State",
                        Zip: "Zip",
                        Status: "Status",
                        Phone: "Phone",
                        Email: "Email",
                        SourceInfo: "SourceInfo",
                        AdmissionsRep: "AdmissionsRep",
                        CampusOfInterest: "CampusOfInterest",
                        Campus: "Campus",
                        ProgramOfInterest: "ProgramOfInterest",
                        Comments: "CampusOfInterest"

                    }
                }
            });
            return ds;
        }

    }
}

