// This has all references to API.WEB in Advantage.....
module AD {
  
    // Duplicate page
    export var XGET_SERVICE_LAYER_LEAD_DUPLICATES_GET = "../proxy/api/Lead/LeadDuplicates/Get";
    export var XGET_SERVICE_LAYER_LEAD_DUPLICATES_PAIR_GET = "../proxy/api/Lead/LeadDuplicates/GetDuplicate";
    export var XGET_SERVICE_LAYER_LEAD_DUPLICATES_COMMAND_GET = "../proxy/api/Lead/LeadDuplicates/ButtomCommandDuplicate";

    //Lead Assignment
    export var XGET_SERVICE_LAYER_LEAD_ASSIGNMENT_CAMPUS_GET = "../proxy/api/CampusWithLead/GetWithActiveLeads";
    export var XGET_SERVICE_LAYER_LEAD_ASSIGNMENT_CAMPUSES_GET = "../proxy/api/Campuses";
    export var XGET_SERVICE_LAYER_LEAD_ASSIGMENT_USER_GET = "../proxy/api/User/Users";
    export var XPOST_SERVICE_UNASSIGNEDLEAD = "../proxy/api/Lead/Leads/UpdateAssignLeads";
    export var XGET_SERVICE_LAYER_LEAD_ASSIGMENT_UNASSIGNED_LEADS_GET = "../proxy/api/Lead/GetByUnassigned";

    // Lead Info Bar
    export var XGET_SERVICE_LAYER_LEAD_INFOBAR_COMMAND_GET = "../proxy/api/Lead/Leads/GetLeadInfoBar";
    export var XIMAGE_INFO = "../../images/info.jpg";
    export var XGET_SERVICE_LAYER_LEAD_REQUIREMENT_STATUS_GET = "../proxy/api/Lead/Leads/GetLeadRequirementStatus";

    // Lead Info Page
    export var XGET_SERVICE_LAYER_LEAD_GROUPS_COMMAND_GET = "../proxy/api/Lead/Leads/GetLeadGroupInformation"; 
    export var XGET_SERVICE_LAYER_SCHOOL_DEFINED_FIELDS_COMMAND_GET = "../proxy/api/Lead/Leads/GetLeadSchoolCustomFields"; 
    export var XGET_SERVICE_LAYER_INFOPAGE_RESOURCES_GET = "../proxy/api/Lead/Leads/GetInfoPageResources";
    export var XGET_SERVICE_LAYER_CATALOGS_COMMAND_GET = "../proxy/api/Catalogs/Catalogs/Get";
    export var XGET_SERVICE_LAYER_LEAD_DEMOGRAPHIC_COMMAND_GET = "../proxy/api/Lead/Leads/GetLeadInfoDemographicFields";
    export var XGET_SERVICE_LAYER_CONFIGURATION_SETTING_GET = "../proxy/api/SystemStuff/ConfigurationAppSettings/Get";
    export var XPOST_SERVICE_LAYER_LEAD_INFORMATION = "../proxy/api/Lead/Leads/PostInfoPageLeadFields";
    export var XGET_SERVICE_LAYER_LEAD_IS_ERASABLE_COMMAND = "../proxy/api/Lead/Leads/IsLeadErasable";
    export var XPOST_SERVICE_LAYER_LEAD_DELETE_COMMAND = "../proxy/api/Lead/Leads/PostLeadDelete";
 
    //Queue Page
    export var XGET_SERVICE_LAYER_QUEUE_COMMAND_GET = "../proxy/api/Lead/LeadQueue/Get";
    export var XPOST_SERVICE_LAYER_SET_MRU = "../proxy/api/SystemStuff/LeadStuffRouter/Mru/Post";
    export var XPOST_PHONE_SERVICE_LAYER_SET_MRU = "../proxy/api/Lead/LeadQueue/Post";

    //Lead Tool Bar
    export var XGET_SERVICE_LAYER_TOOLBAR_FLAGS = "../proxy/api/Lead/LeadToolBar/Get";

    // Extracurricular Page
    export var XGET_SERVICE_LAYER_EXTRACURRICULAR = "../proxy/api/Lead/LeadExtraCurricular/Get";
    export var XPOST_SERVICE_LAYER_EXTRACURRICULAR = "../proxy/api/Lead/LeadExtraCurricular/Post";

    // Skills Page
    export var XGET_SERVICE_LAYER_SKILLS = "../proxy/api/Lead/LeadSkills/Get";
    export var XPOST_SERVICE_LAYER_SKILLS = "../proxy/api/Lead/LeadSkills/Post";

    //Notes Page
    export var XGET_SERVICE_LAYER_NOTES = "../proxy/api/Lead/LeadNotes/Get";
    export var XPOST_SERVICE_LAYER_NOTES = "../proxy/api/Lead/LeadNotes/Post";

    // Prior Work Page
    export var XGET_SERVICE_LAYER_PRIORWORK = "../proxy/api/Lead/LeadPriorWork/Get";
    export var XPOST_SERVICE_LAYER_PRIORWORK = "../proxy/api/Lead/LeadPriorWork/Post";
    export var XPRIOR_WORK_SDF_FIELDS = "PriorWorkSdfFields";
    export var XPRIOR_WORK_SDF_FIELDS_STUDENT = "PriorWorkSdfFieldsStudent";
    //prior education sdf
    export var XPRIOR_Edu_SDF_FIELDS = "PriorEduSdfFields";
    export var XPRIOR_Edu_SDF_FIELDS_STUDENT = "PriorEduSdfFieldsStudent";
    //LeadEnrollPage
    export var XGET_SERVICE_LAYER_ENROLL_DDL = "../proxy/api/Lead/LeadEnrollment/GetCaptionAndRequiredList";
    export var XGET_SERVICE_LAYER_ENROLL_IPEDS = "../proxy/api/Lead/LeadEnrollment/GetRequiredIpedsList";
    export var XGET_SERVICE_LAYER_ENROLL_INFO = "../proxy/api/Lead/LeadEnrollment/GetLeadData";
    export var XPOST_SERVICE_LAYER_ENROLL_ENROLL_LEAD = "../proxy/api/Lead/LeadEnrollment/EnrollLead";
    export var XPOST_SERVICE_LAYER_ENROLL_Validate = "../proxy/api/Lead/LeadEnrollment/ServerSideValidation";
    //Session Cache Objects name
    export var XLEAD_INFO_DLLS_REQUIRED_ITEMS = "captionRequirementdb";
    export var XLEAD_INFO_SDF_FIELDS = "SdfFields";
    export var XLEAD_INFO_GROUPS = "LeadGroups";
    export var XLEAD_NEW = "NewLead";
    export var XLEAD_QUICK_FIELDS = "quickLeadFields";
    export var XLEAD_QUICK_NEW = "NewQuickLead";
    export var QUICK_ALLOWEDLEADSTATUS = "allowedQuickLeadStatus";
    export var XLEAD_QUICK_INFO_SDF_FIELDS = "SdfQuickFields";
    export var XSUB_MENU_SELECTION = "currentSubLevelName";
    export var XQUEUE_PAGE = "Queue";
    export var XLEADINFO_PAGE = "Leads";
    //storing the dropdown datasource
    export var QUICK_CAMPUS = "CampusId";
    export var QUICK_LEADSTATUS = "LeadStatusId";
    export var QUICK_ADMIN_REP = "AdmissionRepId";
    export var QUICK_PROG = "ProgramId";
    export var QUICK_PROG_VER = "PrgVerId";
    export var QUICK_SCHEDULE = "ScheduleId";
    
    // Associated with tool bar Alert
    export var X_FLAG_LEAD = "X_FLAG_LEAD";
    export var X_FLAG_TASK = "X_FLAG_TASK";
    export var X_FLAG_APPO = "X_FLAG_APPO";

    //Quick Lead Page
    export var XGET_SERVICE_LAYER_FIELDS_GET = "../proxy/api/Lead/LeadQuick/Get";
    export var XGET_SERVICE_LAYER_DDL_SOURCE_GET = "../proxy/api/Lead/LeadQuick/GetQuickLeadDropDownValues";
    export var XGET_SERVICE_LAYER_SDF_GET = "../proxy/api/Lead/LeadQuick/GetSdfList";
    export var XGET_SERVICE_LAYER_LEADGRP_GET = "../proxy/api/Lead/LeadQuick/GetLeadGroupInformation";
    export var XGET_SERVICE_LAYER_STATE_DDL_GET = "../proxy/api/Lead/LeadQuick/GetQuickLeadStateValues";

    //History Page
    export var XGET_SERVICE_LAYER_HISTORY = "../proxy/api/Lead/LeadHistory/Get";

    export var XGET_SERVICE_LAYER_LEAD_REQUIREMENTS_TYPE_MET_GET = "../proxy/api/Leads/@leadId@/Requirements/GetRequirementTypesMet";

    //Lead Education
    export var XGET_LEAD_EDUCATION = "../proxy/api/Lead/Education/Get";
    export var XFIND_LEAD_EDUCATION = "../proxy/api/Lead/Education/Find";
    export var XPOST_LEAD_EDUCATION = "../proxy/api/Lead/Education/Post";
    export var XPUT_LEAD_EDUCATION = "../proxy/api/Lead/Education/Put";
    export var XDELETE_LEAD_EDUCATION = "../proxy/api/Lead/Education/Delete";
}