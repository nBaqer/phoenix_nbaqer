﻿using System;

namespace FAME.Advantage.MultiTenantHost.Lib.Presenters
{
    public class PresenterException : ApplicationException
    {
        public PresenterException() {}
        public PresenterException(string message) : base(message) {}
        public PresenterException(string message, Exception innerException) : base(message, innerException) {}
        public PresenterException(string message, params object[] parameters) : base(String.Format(message, parameters)) { }
    }
}
