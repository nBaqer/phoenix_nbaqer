﻿using System;
using System.Linq;
using System.Web;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.MultiTenant;

namespace FAME.Advantage.MultiTenantHost.Lib.Presenters
{
    public class ManageHostingPresenter : BasePresenter
    {
        public void AddTenant(AddTenantArgs args)
        {
            RunInUnitOfWork((userRepository, tenantRepository) =>
            {
                AssertTenantDoesNotExist(args.Name, tenantRepository);

                var userId = HttpContext.Current.User.Identity.Name;

                var user = GetUser(Guid.Parse(userId), userRepository);

                var tenant = SaveNewTenant(args, user.UserName, tenantRepository);

                AddTenantToCurrentUser(user, tenant, userRepository);
            });
        }

        private TenantUser GetUser(Guid id, IRepositoryWithTypedID<Guid> repository)
        {
            return repository.Get<TenantUser>(id);
        }

        private void AddTenantToCurrentUser(TenantUser user, Tenant tenant, IRepositoryWithTypedID<Guid> repository)
        {
            user.AddTenant(tenant, true, false);
            repository.Save(user);
        }


        private static Tenant SaveNewTenant(AddTenantArgs args, string userName, IRepositoryWithTypedID<int> r)
        {
            var connection = new DataConnection(args.DatabaseServerName, args.DatabaseName, args.DatabaseUserName, args.DatabasePassword);
            var environment = r.Load<TenantEnvironment>(args.EnvironmentId);
            var entity = new Tenant(args.Name, userName, connection, environment);

            r.Save(entity);

            return entity;
        }

        public void MigrateTenantToNewEnvironment(int tenantId, int environmentId)
        {
            RunInUnitOfWork(r => {
                var tenant = r.Get<Tenant>(tenantId);
                var environment = r.Load<TenantEnvironment>(environmentId);
                tenant.MigrateToNewEnvironment(environment);

                r.Save(tenant);
            });
        }

        private void AssertTenantDoesNotExist(string name, IRepositoryWithTypedID<int> repository)
        {
            var exists = repository.Query<Tenant>().Any(t => t.Name == name);

            if (exists) {
                throw new PresenterException("Tenant already exists");
            }
        }

        public TenantViewModel[] GetTenants()
        {
            return RunInUnitOfWork(r => r.Query<Tenant>()
                .Select(t => new TenantViewModel {
                    Id = t.ID,
                    Name = t.Name,
                    VersionNumber = t.Environment.VersionNumber
                }).ToArray());
        }

        public TenantEnvironmentViewModel[] GetEnvironments()
        {
            return RunInUnitOfWork(r => r.Query<TenantEnvironment>()
                .Select(t => new TenantEnvironmentViewModel {
                    Id = t.ID,
                    Name = t.Name,
                    VersionNumber = t.VersionNumber
                }).ToArray());
        }
    }

    public class AddTenantArgs
    {
        public string Name { get; set; }
        public string DatabaseServerName { get; set; }
        public string DatabaseName { get; set; }
        public string DatabaseUserName { get; set; }
        public string DatabasePassword { get; set; }
        public int EnvironmentId { get; set; }
    }
}
