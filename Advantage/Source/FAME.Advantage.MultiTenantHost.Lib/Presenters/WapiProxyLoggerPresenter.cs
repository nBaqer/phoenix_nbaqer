﻿using System.Collections.Generic;
using System.Linq;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.MultiTenant;
using FAME.Advantage.MultiTenantHost.Lib.Models;

namespace FAME.Advantage.MultiTenantHost.Lib.Presenters
{
    /// <summary>
    /// Data Access Wapi Proxy Logger.
    /// </summary>
    public class WapiProxyLoggerPresenter : BasePresenter
    {
        #region Get The logger records filter  or not
        /// <summary>
        /// Get the record filter or not
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IEnumerable<WapiProxyLoggerOutputModel> GetWapiProxyLogRecords(IWapiProxyLoggerInputModel filter)
        {
            return RunInUnitOfWork(() =>
            {
                var repository = Resolve<IRepositoryWithTypedID<int>>();

                var records = repository.Query<WapiLog>();
                if (filter.CalledService != WapiProxyLoggerInputModel.XALL)
                {

                    records = records.Where(x => x.ServiceInvoqued == filter.CalledService);

                }

                if (filter.Company != WapiProxyLoggerInputModel.XALL)
                {
                    records = records.Where(x => x.Company == filter.Company);

                }

                if (filter.TenantName != WapiProxyLoggerInputModel.XALL)
                {
                    records = records.Where(x => x.TenantName == filter.TenantName);

                }

                if (filter.IsOk != 2)
                {
                    records = records.Where(x => x.IsOk == filter.IsOk);

                }

                var re1 = records.OrderByDescending(x => x.ID).ToList();

                var output = re1.Select(t => new WapiProxyLoggerOutputModel
                {
                    Id = t.ID,
                    CalledService = t.ServiceInvoqued,
                    Comment = t.Comment,
                    DateMod = t.DateMod,
                    Company = t.Company,
                    IsOk = t.IsOk,
                    NumberOfRecords = t.NumberOfRecords,
                    ServiceDescrip = t.OperationDescription,
                    TenantName = t.TenantName
                }).ToList();

                output = output.Take(1000).ToList();

                return output;
            });
        }

        #endregion

        #region Clear Log Records
        /// <summary>
        /// Clear the log record by TRUNCATE the table
        /// </summary>
        public void ClearLogRecords()
        {
            RunInUnitOfWork(() =>
            {
                var repository = Resolve<IRepositoryWithTypedID<int>>();
                var query = repository.Session.CreateSQLQuery("TRUNCATE TABLE WapiLog");
                query.ExecuteUpdate();
            });
        }

        #endregion

        #region DropDown Filter values

        public object GetCalledOperationInLoggedRecords()
        {
            return RunInUnitOfWork(() =>
            {
                var repository = Resolve<IRepositoryWithTypedID<int>>();
                var records = repository.Query<WapiLog>().Take(1000);
                var output = records.Select(x => x.ServiceInvoqued).Distinct().ToArray();
                return output;
            });

        }

        public object GetCalledCompaniesInLoggedRecords()
        {
            return RunInUnitOfWork(() =>
            {
                var repository = Resolve<IRepositoryWithTypedID<int>>();
                var records = repository.Query<WapiLog>().Take(1000);
                var output = records.Select(x => x.Company).Distinct().ToArray();
                return output;
            });
        }

        public object GetTenantNameInLoggedRecords()
        {
            return RunInUnitOfWork(() =>
           {
               var repository = Resolve<IRepositoryWithTypedID<int>>();
               var records = repository.Query<WapiLog>().Take(1000);
               var output = records.Select(x => x.TenantName).Distinct().ToArray();
               return output;
           });
        }
        #endregion
    }
}
