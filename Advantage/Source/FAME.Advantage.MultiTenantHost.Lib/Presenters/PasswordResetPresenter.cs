﻿using System;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.Security;
using FAME.Advantage.Domain.MultiTenant;

namespace FAME.Advantage.MultiTenantHost.Lib.Presenters
{
    public class PasswordResetPresenter : BasePresenter
    {
        public void AssertValidUser(string providerUserKey)
        {
            var myUser = Membership.GetUser(providerUserKey);

            if (myUser == null) {
                throw new PresenterException("Invalid username. Please try again.");
            }

            if (!myUser.IsApproved) {
                throw new PresenterException("Your account is currently marked as Inactive. Please contact your system administrator.");
            }

            if (myUser.IsLockedOut) {
                throw new PresenterException("The user account is currently locked. Please contact your system administrator.");
            }
        }

        public string ResetPassword(string userName, string passwordAnswer)
        {
            var user = Membership.GetUser(userName);
            var userId = user.ProviderUserKey.ToString();

            var oldPassword = user.ResetPassword(passwordAnswer);
            var newPassword = GetPassword();

            // Change the user's old password to new password
            user.ChangePassword(oldPassword, newPassword);

            // Update user Creation date and Last Password change date 
            // This step is needed to force the user to change password after password reset
            //Resolve<IPasswordResetService>().ResetPasswordDates(Guid.Parse(userId));

            return newPassword;
        }

        private string GetPassword()
        {
            // new password that is 10 characters, 4 lowercase letters, 4 numbers, and 2 uppercase letters. 
            var builder1 = new StringBuilder();

            builder1.Append(RandomString(4, lowerCase: true));
            builder1.Append(RandomNumeric(1000, 9999));
            builder1.Append(RandomString(2, lowerCase: false));
            builder1.Append(RandomSymbol());

            return builder1.ToString();
        }

        private string RandomNumeric(int min, int max)
        {
            var random = new Random();
            return random.Next(min, max).ToString(CultureInfo.InvariantCulture);
        }

        private string RandomString(int size, bool lowerCase)
        {
            var builder = new StringBuilder();
            var random = new Random();

            for (var i = 0; i < size; i++) {
                builder.Append(Convert.ToChar(Convert.ToInt32((26 * random.NextDouble() + 65))));
            }

            var randomString = builder.ToString();

            return lowerCase 
                ? randomString.ToLower() 
                : randomString;
        } 

        private string RandomSymbol()
        {
            return "*";
        }

        public string GetPasswordResetEmail(string userName, string password, string originalMessage)
        {
            var nonLinkableEmail = GetNonLinkableEmail(userName);
            var link = GetPasswordResetURL(userName, password);

            var builder = new StringBuilder(originalMessage);
            builder.Append(@"<p style=""text-decoration:none;"">Your password has been reset " + nonLinkableEmail + "</p>");
            builder.Append(@"<p>According to our records you have requested that your password be reset.</p>");
            builder.Append(@"<p> Click here to setup your password: " + link + "</p>");
            builder.Append(@"<p> If you have any questions or trouble logging on please contact a site administrator</p>");
            builder.Append(@"<p> Thanks, </p>");
            builder.Append(@"<p> Advantage Support </p>");

            return builder.ToString();
        }

        private string GetNonLinkableEmail(string email)
        {
            var parts = GetEmailParts(email);
            
            return String.Format("<span>{0}</span><span>@</span><span>{1}</span><span>{2}</span>",
                    parts.UserName,
                    parts.DomainName,
                    parts.TopLevelDomain);
        }

        private EmailParts GetEmailParts(string email)
        {
            var atIndex = email.IndexOf("@", StringComparison.Ordinal);
            var afterAt = email.Substring(atIndex + 1);
            var dotIndex = afterAt.LastIndexOf(".", StringComparison.Ordinal);
            var userName = email.Substring(0, atIndex);
            var domainName = afterAt.Substring(0, dotIndex);
            var topLevel = afterAt.Substring(dotIndex);
            return new EmailParts {
                UserName = userName,
                DomainName = domainName,
                TopLevelDomain = topLevel,
            };
        }

        private class EmailParts
        {
            public string UserName;
            public string DomainName;
            public string TopLevelDomain;
        }

        private string GetPasswordResetURL(string userName, string password)
        {
            const string PASSWORD_RESET_URL = "~/PasswordResetDefault.aspx?email={0}&tpass={1}";
            var passwordResetURL = String.Format(PASSWORD_RESET_URL, SafeEncode(userName), SafeEncode(password));
            return GetCurrentServerName() + VirtualPathUtility.ToAbsolute(passwordResetURL);
        }

        private static string SafeEncode(string input)
        {
            if (!String.IsNullOrEmpty(input)) {
                input = input.Trim();
            }

            return HttpContext.Current.Server.UrlEncode(input);
        }

        private static string GetCurrentServerName()
        {
            var url = HttpContext.Current.Request.Url;
            return url.Scheme + Uri.SchemeDelimiter + url.Host;
        }
    }
}
