﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LoginPresenter.cs" company="FAME INC">
//   2017
// </copyright>
// <summary>
//   Defines the LoginPresenter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.MultiTenantHost.Lib.Presenters
{
    using System;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.Security;

    using FAME.Advantage.Api.Library.Models;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.MultiTenant;
    using FAME.Advantage.MultiTenantHost.Lib.Infrastructure.API;
    using FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Extensions;

    using FluentAssertions;

    using StructureMap.TypeRules;

    /// <summary>
    /// The login presenter.
    /// </summary>
    public class LoginPresenter : BasePresenter
    {
        /// <summary>
        /// The get advantage url.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetAdvantageUrl(string user)
        {
            return this.RunInUnitOfWork(
                () =>
                    {
                        var tenantUser = this.GetTenantUser(user);

                        if (tenantUser != null)
                        {
                            Tenant tenantDetails = tenantUser.GetDefaultTenant();
                            return tenantUser.GetApplicationURL(tenantDetails);
                        }

                        return string.Empty;
                    });
        }

        /// <summary>
        /// The get version.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetVersion()
        {
            return this.GetType().Assembly.GetName().Version.ToString();
        }

        /// <summary>
        /// The get user information.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="tenantId">
        /// The optional tenant Id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public UserInformation GetUserInformation(string user, int? tenantId)
        {
            return this.RunInUnitOfWork(
                () =>
                    {
                        var tenantUser = this.GetTenantUser(user);

                        if (tenantUser != null)
                        {
                            Tenant tenantDetails = !tenantId.HasValue ? tenantUser.GetDefaultTenant() : tenantUser.GetTenant(tenantId.Value);
                            var token = this.Session["AdvantageApiToken"].As<TokenResponse>();
                            var userInformationRequest = new User(tenantDetails.GetConnectionString());
                            UserInformation userInformation = userInformationRequest.GetUserInformation(token.Token);

                            if (userInformation.Type.Code == "Vendor")
                            {
                                this.Response.BufferOutput = true;
                                var apiUrl = new Api(tenantDetails.GetConnectionString()).GetAdvantageApiUrl().ToLower();
                                var swagger = apiUrl.Replace("/api", string.Empty) + "/api/swagger";
                                userInformation.Url = swagger;

                            }
                            else
                            {
                                userInformation.Url = tenantUser.GetApplicationURL(tenantDetails);
                            }
                            return userInformation;
                        }

                        return null;
                    });

        }

        /// <summary>
        /// The login.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <exception cref="PresenterException">
        /// handles the exception
        /// </exception>
        public void Login(string userName, string password)
        {
            if (!IsValidUserName(userName))
            {
                throw new PresenterException("Invalid username or password. Please try again");
            }

            if (!Membership.ValidateUser(userName, password))
            {
                this.HandleLoginFailure(userName);
            }

            var membershipUser = Membership.GetUser(userName);
            var userId = membershipUser.GetId();

            FormsAuthentication.SetAuthCookie(userId, true);

            this.RunInUnitOfWork(
                () =>
                    {
                        var tenantUsr = this.GetTenantUser(userId);
                        if (tenantUsr.IsSupportUser())
                        {
                            this.Session["TenantUser"] = userName.ToLower();
                            this.Session["SupportAccount"] = password;
                            this.Response.Redirect("~/TenantPicker.aspx", true);
                            return;
                        }

                        if (membershipUser.IsPasswordReset())
                        {
                            this.Session["TenantUser"] = userName.ToLower();
                            this.Response.Redirect("PasswordResetDefault.aspx");
                            return;
                        }

                        // start adding the logic to store the API token in session
                        Tenant tenantDetails = tenantUsr.GetDefaultTenant();
                        var tokenReq = new Token(tenantDetails.GetConnectionString());
                        var token = tokenReq.GetToken(tenantDetails.Name, userName, password);
                        this.Session["AdvantageApiToken"] = token;
                        this.Session["Account"] = userName;

                        var userInformationRequest = new User(tenantDetails.GetConnectionString());
                        var userInformation = userInformationRequest.GetUserInformation(token.Token);

                        if (userInformation == null)
                        {
                            SecurityHelper.RedirectToLoginPage("The user does not have the correct permissions to access the system. Please contact a system administrator.");
                        }
                        else
                        {
                            if (userInformation.HasMultipleTenants)
                            {
                                this.Session["SupportAccount"] = password;
                                this.Response.Redirect("~/TenantPicker.aspx", true);
                                return;
                            }


                            if (userInformation.TermsOfUse == null || !userInformation.TermsOfUse.HasAcceptedLatestTermsOfUse)
                            {


                                this.Session["SupportAccount"] = password;
                                this.Response.Redirect("~/TermsOfUse.aspx", true);
                                return;
                            }
                            else
                            {
                                if (userInformation.Type.Code == "Vendor")
                                {
                                    this.Response.BufferOutput = true;
                                    var apiUrl = new Api(tenantDetails.GetConnectionString()).GetAdvantageApiUrl().ToLower();
                                    var swagger = apiUrl.Replace("/api", string.Empty) + "/api/swagger";
                                    this.Response.Redirect(swagger);
                                }

                                this.Response.Redirect(tenantUsr.GetApplicationURL(tenantDetails));
                            }
                            this.Response.Redirect(tenantUsr.GetApplicationURL(tenantDetails));
                        }
                    });
        }

        /// <summary>
        /// The redirect to dashboard.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        public void RedirectToDashboard(string username)
        {

            var url = this.GetAdvantageUrl(username);

            this.Response.Redirect(url);

        }

        /// <summary>
        /// The has user accepted latest terms.
        /// </summary>
        /// <param name="userVersion">
        /// The user version.
        /// </param>
        /// <param name="assemblyVersion">
        /// The assembly version.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool HasUserAcceptedLatestTerms(string userVersion, string assemblyVersion)
        {
            var userCodeList = userVersion.Split('.');
            var assemblyCodeList = assemblyVersion.Split('.');

            int parsedUserVersionPart1;
            int parsedUserVersionPart2;
            int parsedUserVersionPart3;
            int parsedUserVersionPart4;
            int parsedAssemblyVersionPart1;
            int parsedAssemblyVersionPart2;
            int parsedAssemblyVersionPart3;
            int parsedAssemblyVersionPart4;

            if (int.TryParse(userCodeList[0], out parsedUserVersionPart1)
                && int.TryParse(userCodeList[1], out parsedUserVersionPart2)
                && int.TryParse(userCodeList[2], out parsedUserVersionPart3)
                && int.TryParse(userCodeList[3], out parsedUserVersionPart4)
                && int.TryParse(assemblyCodeList[0], out parsedAssemblyVersionPart1)
                && int.TryParse(assemblyCodeList[1], out parsedAssemblyVersionPart2)
                && int.TryParse(assemblyCodeList[2], out parsedAssemblyVersionPart3)
                && int.TryParse(assemblyCodeList[3], out parsedAssemblyVersionPart4))
            {
                return parsedUserVersionPart1 >= parsedAssemblyVersionPart1
                       && parsedUserVersionPart2 >= parsedAssemblyVersionPart2
                       && parsedUserVersionPart3 >= parsedAssemblyVersionPart3
                       && parsedUserVersionPart4 >= parsedAssemblyVersionPart4;
            }

            return false;

        }

        /// <summary>
        /// The is valid user name.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool IsValidUserName(string userName)
        {
            return Regex.IsMatch(
                userName,
                @"^(([^<>()[\]\\.,;:\s@\""]+(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$");
        }

        /// <summary>
        /// The handle login failure.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <exception cref="PresenterException">
        /// to handle exceptions
        /// </exception>
        private void HandleLoginFailure(string userName)
        {
            var user = Membership.GetUser(userName);

            if (user == null)
            {
                throw new PresenterException("Invalid username. Please try again.");
            }

            if (!user.IsApproved)
            {
                throw new PresenterException(
                    "The user account is currently marked as Inactive. Please contact your system administrator.");
            }

            if (user.IsLockedOut)
            {
                throw new PresenterException(
                    "The user account is currently locked. Please contact your system administrator.");
            }

            // DE9543 - we are changing this message output to a redirect to avoid doubling the FailedPasswordAttemptCount
            SecurityHelper.RedirectToLoginPage("Your login attempt was not successful. Please try again.");
        }

        /// <summary>
        /// The get tenant user.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="TenantUser"/>.
        /// </returns>
        private TenantUser GetTenantUser(string userId)
        {
            var repository = Resolve<IRepository>();
            return repository.Get<TenantUser>(Guid.Parse(userId));
        }




        /// <summary>
        /// The get advantage api url.
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetAdvantageApiUrl(string email, string tenantName = null)
        {
            var membershipUser = Membership.GetUser(email);
            var userId = membershipUser.GetId();


            return this.RunInUnitOfWork(
                () =>
                    {
                        var tenantUsr = this.GetTenantUser(userId);
                        var matchingTenant = tenantName != null ? tenantUsr.Tenants.FirstOrDefault(ten => ten.Tenant.Name == tenantName)?.Tenant : null;

                        // start adding the logic to store the API token in session
                        Tenant tenantDetails = matchingTenant ?? tenantUsr.GetDefaultTenant();
                        var api = new Api(tenantDetails.GetConnectionString());
                        return api.GetAdvantageApiUrl();
                    });

        }
    }
}