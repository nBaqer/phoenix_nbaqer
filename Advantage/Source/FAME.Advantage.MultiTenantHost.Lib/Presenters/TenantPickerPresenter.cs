﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TenantPickerPresenter.cs" company="FAME INC">
//   2017
// </copyright>
// <summary>
//   Defines the TenantPickerPresenter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.MultiTenantHost.Lib.Presenters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.MultiTenant;
    using FAME.Advantage.MultiTenantHost.Lib.Infrastructure.API;

    using FluentAssertions;

    /// <summary>
    /// The tenant picker presenter.
    /// </summary>
    public class TenantPickerPresenter : BasePresenter
    {
        /// <summary>
        /// The _user name.
        /// </summary>
        private readonly string _userName;

        /// <summary>
        /// Initializes a new instance of the <see cref="TenantPickerPresenter"/> class.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        public TenantPickerPresenter(string userName)
        {
            _userName = userName;
        }

        /// <summary>
        /// The pick tenant.
        /// </summary>
        /// <param name="tenantID">
        /// The tenant id.
        /// </param>
        public void PickTenant(int tenantID)
        {
            this.RunInUnitOfWork(
                () =>
                    {
                        var tenantUsr = this.GetTenantUser();
                        Tenant tenantDetails = tenantUsr.GetTenant(tenantID);
                        var tokenReq = new Token(tenantDetails.GetConnectionString());
                        var pwd = this.Session["SupportAccount"].ToString();
                        var token = tokenReq.GetToken(tenantDetails.Name, tenantUsr.UserName, pwd);
                        this.Session["AdvantageApiToken"] = token;

                        var userInformationRequest = new User(tenantDetails.GetConnectionString());
                        var userInformation = userInformationRequest.GetUserInformation(token.Token);

                        if (userInformation != null && !tenantUsr.IsSupportUser())
                        {
                            if (userInformation.TermsOfUse == null || !userInformation.TermsOfUse.HasAcceptedLatestTermsOfUse)
                            {
                                this.Response.Redirect("~/TermsOfUse.aspx?tenantId=" + tenantID, true);
                                return;
                            }
                            else
                            {

                                if (userInformation.Type.Code == "Vendor")
                                {
                                    this.Response.BufferOutput = true;
                                    var apiUrl = new Api(tenantDetails.GetConnectionString()).GetAdvantageApiUrl().ToLower();
                                    var swagger = apiUrl.Replace("/api", string.Empty) + "/api/swagger";
                                    this.Response.Redirect(swagger);
                                }

                                this.Response.Redirect(this.GetTenantUser().GetApplicationURL(tenantID), false);
                            }
                        }
                    });

            this.RunInUnitOfWork(() => this.Response.Redirect(this.GetTenantUser().GetApplicationURL(tenantID), false));
        }

        /// <summary>
        /// The get tenants.
        /// </summary>
        /// <returns>
        /// the list of the tenants that belong to the user
        /// </returns>
        public IEnumerable<TenantViewModel> GetTenants()
        {
            return this.RunInUnitOfWork(() =>
            {
                var tenantUser = this.GetTenantUser();

                return tenantUser.Tenants
                    .Select(t => new TenantViewModel
                    {
                        Id = t.Tenant.ID,
                        Name = t.Tenant.Name,
                        VersionNumber = t.Tenant.Environment.VersionNumber
                    }).ToArray();
            });
        }

        /// <summary>
        /// The get tenants with corresponding url data.
        /// </summary>
        /// <returns>
        /// the list of the tenants that belong to the user along with their url information
        /// </returns>
        public IEnumerable<TenantViewModel> GetTenantsWithUrls()
        {
            return this.RunInUnitOfWork(() =>
                {
                    var tenantUser = this.GetTenantUser();

                    var tenants = new List<TenantViewModel>();
                    foreach (var tenant in tenantUser.Tenants)
                    {
                        var apiUrl = string.Empty;
                        string url;
                        var tenantId = tenant.Tenant.ID;
                        try
                        {
                            Tenant tenantDetails = tenantUser.GetTenant(tenantId);
                            Api apiObject = new Api(tenantDetails.GetConnectionString());
                            if (tenantUser.IsSupportUser())
                            {
                                apiUrl = apiObject.GetAdvantageApiUrl().ToLower();
                                url = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority
                                      + System.Web.VirtualPathUtility.ToAbsolute(tenantUser.GetApplicationURL(tenantId));
                            }
                            else
                            {
                                var userType = apiObject.GetUserType(tenantUser.ID);

                                this.Response.BufferOutput = true;
                                if (userType == "Vendor")
                                {
                                    apiUrl = apiObject.GetAdvantageApiUrl().ToLower();
                                    url = apiUrl.Replace("/api", string.Empty) + "/api/swagger";
                                }
                                else
                                {
                                    url = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority
                                          + System.Web.VirtualPathUtility.ToAbsolute(tenantUser.GetApplicationURL(tenantId));
                                }
                            }
                           
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            url = string.Empty;
                        }


                        tenants.Add(
                            new TenantViewModel
                            {
                                Id = tenant.Tenant.ID,
                                Name = tenant.Tenant.Name,
                                VersionNumber = tenant.Tenant.Environment.VersionNumber,
                                Url = url,
                                ApiUrl = apiUrl
                            });
                    }

                    return tenants;
                });
        }

        /// <summary>
        /// The is support user.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsSupportUser()
        {
            return this.RunInUnitOfWork(() => this.GetTenantUser().IsSupportUser());
        }

        /// <summary>
        /// The get tenant user.
        /// </summary>
        /// <returns>
        /// The <see cref="TenantUser"/>.
        /// </returns>
        private TenantUser GetTenantUser()
        {
            var repository = Resolve<IRepository>();
            return repository.Get<TenantUser>(Guid.Parse(_userName));
        }
    }

    /// <summary>
    /// The tenant view model.
    /// </summary>
    public class TenantViewModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the version number.
        /// </summary>
        public string VersionNumber { get; set; }

        /// <summary>
        /// Gets or sets the url.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the api url.
        /// </summary>
        public string ApiUrl { get; set; }
    }

    /// <summary>
    /// The tenant environment view model.
    /// </summary>
    public class TenantEnvironmentViewModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the version number.
        /// </summary>
        public string VersionNumber { get; set; }
    }
}
