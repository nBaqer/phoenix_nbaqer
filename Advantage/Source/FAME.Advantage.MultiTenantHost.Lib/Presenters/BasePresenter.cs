﻿using System;
using System.Diagnostics;
using System.Web;
using System.Web.SessionState;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Persistence.Infrastructure.UoW;
using Microsoft.Practices.ServiceLocation;

namespace FAME.Advantage.MultiTenantHost.Lib.Presenters
{
    [Serializable]
    public class BasePresenter
    {
        private readonly IUnitOfWork _unitOfWork;

        public BasePresenter() 
            : this(Resolve<IUnitOfWork>()) { }

        public BasePresenter(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [DebuggerStepThrough]
        protected void RunInUnitOfWork(Action action)
        {
            using (var scope = _unitOfWork.CreateScope()) {
                action.Invoke();
                scope.Complete();
            }
        }

        [DebuggerStepThrough]
        protected void RunInUnitOfWork(Action<IRepositoryWithTypedID<int>> action)
        {
            using (var scope = _unitOfWork.CreateScope())
            {
                action.Invoke(Resolve<IRepositoryWithTypedID<int>>());
                scope.Complete();
            }
        }

        [DebuggerStepThrough]
        protected void RunInUnitOfWork(Action<IRepositoryWithTypedID<Guid>, IRepositoryWithTypedID<int>> action)
        {
            using (var scope = _unitOfWork.CreateScope())
            {
                action.Invoke(Resolve<IRepositoryWithTypedID<Guid>>(), Resolve<IRepositoryWithTypedID<int>>());
                scope.Complete();
            }
        }

        [DebuggerStepThrough]
        protected T RunInUnitOfWork<T>(Func<T> action)
        {
            using (var scope = _unitOfWork.CreateScope()) {
                var val = action.Invoke();
                scope.Complete();

                return val;
            }
        }

        [DebuggerStepThrough]
        protected T RunInUnitOfWork<T>(Func<IRepositoryWithTypedID<int>, T> action)
        {
            using (var scope = _unitOfWork.CreateScope()) {
                var val = action.Invoke(Resolve<IRepositoryWithTypedID<int>>());
                scope.Complete();

                return val;
            }
        }

        [DebuggerStepThrough]
        protected static T Resolve<T>()
        {
            return ServiceLocator.Current.GetInstance<T>();
        }

        protected HttpSessionState Session
        {
            get { return HttpContext.Current.Session; }
        }

        protected HttpRequest Request
        {
            get { return HttpContext.Current.Request; }
        }

        protected HttpResponse Response
        {
            get { return HttpContext.Current.Response; }
        }
    }
}
