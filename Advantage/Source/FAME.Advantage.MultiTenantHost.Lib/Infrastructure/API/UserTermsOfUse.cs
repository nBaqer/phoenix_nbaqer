﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserTermsOfUse.cs" company="FAME INC">
//   Fame Inc 2018
// </copyright>
// <summary>
//   Defines the UserTermsOfUse type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.API
{
    using System;

    /// <summary>
    /// The user terms of use.
    /// </summary>
    public class UserTermsOfUse
    {
        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the version accepting.
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether has accepted latest terms of use.
        /// </summary>
        public bool HasAcceptedLatestTermsOfUse { get; set; }

        /// <summary>
        /// Gets or sets the last version accepted.
        /// </summary>
        public string LastVersionAccepted { get; set; }

        /// <summary>
        /// Gets or sets the last version accepted date.
        /// </summary>
        public DateTime? LastVersionAcceptedDate { get; set; }

    }
}
