﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Api.cs" company="FAME INC">
//   Fame Inc 2018
// </copyright>
// <summary>
//   Defines the LoginPresenter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.API
{
    using System;
    using System.Data;
    using System.Data.SqlClient;

    /// <summary>
    /// The api.
    /// </summary>
    public class Api
    {
        /// <summary>
        /// The tenant connection string.
        /// </summary>
        private readonly string tenantConnectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="Api"/> class.
        /// </summary>
        /// <param name="tenantConnectionString">
        /// The tenant connection string.
        /// </param>
        public Api(string tenantConnectionString)
        {
            this.tenantConnectionString = tenantConnectionString;
        }

        /// <summary>
        /// The get advantage API url.
        /// </summary>
        /// <returns>
        /// The advantage API URL
        /// </returns>
        public string GetAdvantageApiUrl()
        {
            SqlConnection connection = new SqlConnection(this.tenantConnectionString);
            string query =
                "SELECT value FROM syConfigAppSetValues AS CV INNER JOIN syConfigAppSettings AS CS ON CS.SettingId = CV.SettingId WHERE KeyName = \'AdvantageApiURL\'";
            SqlCommand sqlCmd = new SqlCommand(query, connection);
            try
            {
                connection.Open();
                var result = sqlCmd.ExecuteScalar();
                sqlCmd.Dispose();
                if (result != null)
                {
                    return result.ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// The get advantage user type.
        /// </summary>
        /// <param name="userId">
        /// The user Id.
        /// </param>
        /// <returns>
        /// The advantage API URL
        /// </returns>
        public string GetUserType(Guid userId)
        {
            SqlConnection connection = new SqlConnection(this.tenantConnectionString);
            string query =
                "SELECT syusertype.Code FROM dbo.syUsers syuser INNER JOIN dbo.syUserType syusertype ON syusertype.UserTypeId = syuser.UserTypeId WHERE syuser.UserId = @UserId";
            SqlCommand sqlCmd = new SqlCommand(query, connection);
            try
            {
                sqlCmd.Parameters.Add("@UserId", SqlDbType.NVarChar);
                sqlCmd.Parameters["@UserId"].Value = userId.ToString();

                connection.Open();
                var result = sqlCmd.ExecuteScalar();
                sqlCmd.Dispose();
                if (result != null)
                {
                    return result.ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
