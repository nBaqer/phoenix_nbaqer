﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TokenRequest.cs" company="Fame Inc">
//   2017
// </copyright>
// <summary>
//   Defines the TokenRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.API
{
    /// <summary>
    /// The token request class that will be given to the API url.
    /// </summary>
    public class TokenRequest
    {
        /// <summary>
        /// Gets or sets the tenant name.
        /// </summary>
        public string TenantName { get; set; }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; }
    }
}
