﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Token.cs" company="Fame Inc">
//   2017
// </copyright>
// <summary>
//   Defines the TokenRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.API
{
    using System;
    using System.Data.SqlClient;
    using System.Net.Http;
    using System.Text;

    using FAME.Advantage.Api.Library.Models;
    using FAME.Advantage.MultiTenantHost.Lib.Presenters;

    using Newtonsoft.Json;

    /// <summary>
    /// The token request.
    /// </summary>
    [Serializable]
    public class Token : BasePresenter
    {
        /// <summary>
        /// The default path.
        /// </summary>
        private const string Path = "/v1/Token";

        /// <summary>
        /// The api.
        /// </summary>
        private readonly Api api;

        /// <summary>
        /// The connection string of tenant.
        /// </summary>
        private readonly string tenantConnectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="Token"/> class.
        /// </summary>
        /// <param name="tenantConnectionString">
        /// The tenant connection string.
        /// </param>
        public Token(string tenantConnectionString)
        {
            this.tenantConnectionString = tenantConnectionString;
            this.api = new Api(tenantConnectionString);
        }

        /// <summary>
        /// Gets or sets the response content.
        /// </summary>
        public TokenResponse TokenResponse { get; set; }

        /// <summary>
        /// The get token method that will call the API and get the response.
        /// </summary>
        /// <param name="tenantName">
        /// The tenant name.
        /// </param>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <returns>
        /// The <see cref="TokenResponse"/>.
        /// </returns>
        public TokenResponse GetToken(string tenantName, string userName, string password)
        {
            var url = this.api.GetAdvantageApiUrl();
            TokenResponse tokenResponse = new Advantage.Api.Library.Token(url).GetToken(tenantName, userName, password);

            return tokenResponse;
        }

        /// <summary>
        /// The get advantage API url.
        /// </summary>
        /// <returns>
        /// The advantage API URL
        /// </returns>
        public string GetAdvantageApiUrl()
        {
            SqlConnection connection = new SqlConnection(this.tenantConnectionString);
            string query =
                "SELECT value FROM syConfigAppSetValues AS CV INNER JOIN syConfigAppSettings AS CS ON CS.SettingId = CV.SettingId WHERE KeyName = \'AdvantageApiURL\'";
            SqlCommand sqlCmd = new SqlCommand(query, connection);
            try
            {
                connection.Open();
                var result = sqlCmd.ExecuteScalar();
                sqlCmd.Dispose();
                if (result != null)
                {
                    return result.ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
