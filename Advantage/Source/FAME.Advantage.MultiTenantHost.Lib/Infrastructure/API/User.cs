﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="User.cs" company="Fame Inc">
//   Fame Inc 2018
// </copyright>
// <summary>
//   Defines the User type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.API
{
    using System;
    using System.Net.Http;
    using System.Text;

    using FAME.Advantage.Api.Library.Models;

    using Newtonsoft.Json;

    using Telerik.Web.UI.com.hisoftware.api2;

    /// <summary>
    /// The user.
    /// </summary>
    public class User
    {
        /// <summary>
        /// The api.
        /// </summary>
        private readonly Api api;

        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        /// <param name="tenantConnectionString">
        /// The tenant connection string.
        /// </param>
        public User(string tenantConnectionString)
        {
            this.api = new Api(tenantConnectionString);
        }

        /// <summary>
        /// The get user information.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public UserInformation GetUserInformation(string token)
        {
            var userInformation = new UserInformation();
            var url = this.api.GetAdvantageApiUrl();
            string path = "/v1/SystemCatalog/user/GetCurrentUserInformation";
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            try
            {
                var response = client.GetAsync(url + path).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                userInformation = JsonConvert.DeserializeObject<UserInformation>(result);
                return userInformation;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return userInformation;
        }

        /// <summary>
        /// The accept.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="userId">
        /// The user Id.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool AcceptTermsOfUse(string token, string userId, string version)
        {
            var url = this.api.GetAdvantageApiUrl();
            string path = "/v1/SystemCatalog/user/AcceptTermsOfUse";
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            var userTermsOfUse = new UserTermsOfUse() { UserId = userId, Version = version };

            try
            {
                var response = client.PostAsync(url + path, new StringContent(JsonConvert.SerializeObject(userTermsOfUse), Encoding.UTF8, "application/json")).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                var accepted = JsonConvert.DeserializeObject<UserTermsOfUse>(result).HasAcceptedLatestTermsOfUse;
                return accepted;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
