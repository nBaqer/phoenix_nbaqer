﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserInformation.cs" company="FAME INC">
//   Fame Inc 2018
// </copyright>
// <summary>
//   Defines the User Information type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.API
{
    /// <summary>
    /// The user information.
    /// </summary>
    public class UserInformation
    {
        /// <summary>
        /// Gets or sets the user type.
        /// </summary>
        public UserType Type { get; set; }

        /// <summary>
        /// Gets or sets the terms of use.
        /// </summary>
        public UserTermsOfUse TermsOfUse { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether has multiple tenants.
        /// </summary>
        public bool HasMultipleTenants { get; set; }

        /// <summary>
        /// Gets or sets the url.
        /// </summary>
        public string Url { get; set; }
    }
}
