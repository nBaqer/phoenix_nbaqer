﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserType.cs" company="FAME INC">
//   Fame Inc 2018
// </copyright>
// <summary>
//   Defines the LoginPresenter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.API
{
    /// <summary>
    /// The user type.
    /// </summary>
    public class UserType
    {
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code { get; set; }
    }
}
