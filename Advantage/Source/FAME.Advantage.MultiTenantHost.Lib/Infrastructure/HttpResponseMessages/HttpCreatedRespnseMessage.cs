﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Extensions;

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.HttpResponseMessages
{
    internal sealed class HttpCreatedRespnseMessage : HttpResponseMessage
    {
        public HttpCreatedRespnseMessage(Uri location, string etag) : base(HttpStatusCode.Created)
        {
            if (location == null) throw new ArgumentNullException("location");
            if (string.IsNullOrWhiteSpace(etag)) throw new ArgumentException("etag cannot be null or empty");

            Headers.Location = location;

            var quotedEtag = etag.PutQuotesAround();
            Headers.ETag = new EntityTagHeaderValue(quotedEtag);
        }
    }
}
