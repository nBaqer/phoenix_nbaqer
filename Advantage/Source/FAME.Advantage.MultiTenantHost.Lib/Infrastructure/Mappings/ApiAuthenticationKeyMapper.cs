﻿using AutoMapper;
using FAME.Advantage.Domain.MultiTenant;
using FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Automapper;
using FAME.Advantage.MultiTenantHost.Lib.Models.ApiAuthenticationKey;

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Mappings
{
    using AutoMapper.Configuration;

    public class ApiAuthenticationKeyMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression configuration)
        {
            configuration.CreateMap<ApiAuthenticationKey, ApiAuthenticationKeyOutputModel>()
                .ForMember(dest => dest.Id, options => options.MapFrom(source => source.ID))
                .ForMember(dest => dest.Key, options => options.MapFrom(source => source.Key));
        }
    }
}
