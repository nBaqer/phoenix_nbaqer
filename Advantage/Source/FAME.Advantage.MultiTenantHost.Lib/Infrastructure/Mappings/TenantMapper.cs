﻿using AutoMapper;
using FAME.Advantage.Domain.MultiTenant;
using FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Automapper;
using FAME.Advantage.MultiTenantHost.Lib.Models.Tenant;

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Mappings
{
    using AutoMapper.Configuration;

    public class TenantMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Tenant, TenantOutputModel>()
                .ForMember(dest => dest.Id, options => options.MapFrom(source => source.ID))
                .ForMember(dest => dest.Name, options => options.MapFrom(source => source.Name));
        }
    }
}
