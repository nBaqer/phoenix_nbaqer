﻿using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Extensions;
using FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Automapper;
using FluentValidation;
using Microsoft.Practices.ServiceLocation;
using StructureMap;

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Bootstrapping
{
    using AutoMapper.Configuration;

    public static class Bootstrapper
    {
        private static Container container;
        public static void Bootstrap()
        {
            InitializeLogging();

            InitStructureMap();

            InitServiceLocator();

            InitializeAutoMapper();
        }

        private static void InitializeLogging()
        {
            log4net.Config.XmlConfigurator.Configure();
        }
        public static Container Container
        {
            get
            {
                return container;
            }
        }
        private static void InitServiceLocator()
        {
            ServiceLocator.SetLocatorProvider(container.GetInstance<IServiceLocator>);
        }

        private static void InitStructureMap()
        {
            container = new Container(x =>
                x.Scan(s => {
                    s.TheCallingAssembly();
                    s.LookForRegistries();
                    s.AddAllTypesOf<IMapperDefinition>();
                    s.ConnectImplementationsToTypesClosing(typeof(AbstractValidator<>));
                })
            );
        }

        private static void InitializeAutoMapper()
        {
            var configuration = new MapperConfigurationExpression();
            var mappingDefinitions = container.GetAllInstances<IMapperDefinition>();
            mappingDefinitions.Each(mappingDefinition => mappingDefinition.CreateDefinition(configuration));

            Mapper.Initialize(configuration);

            Mapper.AssertConfigurationIsValid();
        }
    }
}
