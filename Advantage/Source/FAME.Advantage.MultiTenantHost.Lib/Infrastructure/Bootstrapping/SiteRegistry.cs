﻿using FAME.Advantage.Domain.Infrastructure.ServiceLocation;
using log4net;
using Microsoft.Practices.ServiceLocation;
using StructureMap.Configuration.DSL;
using StructureMap;

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Bootstrapping
{
    public class SiteRegistry : Registry
    {
        public SiteRegistry()
        {
            Scan(x => {
                x.TheCallingAssembly();
                x.WithDefaultConventions();
            });

            For<IServiceLocator>().Use<StructureMapServiceLocator>();

            For<ILog>().Use(LogManager.GetLogger(Globals.GlobalLoggerName));
        }
    }
}
 