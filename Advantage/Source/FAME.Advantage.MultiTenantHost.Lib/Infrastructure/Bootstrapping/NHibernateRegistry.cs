﻿using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.MultiTenant.Persistence;
using FAME.Advantage.Domain.Persistence.Infrastructure.NHibernate;
using FAME.Advantage.Domain.Persistence.Infrastructure.UoW;
using NHibernate;
using StructureMap.Configuration.DSL;
using System;
using StructureMap;
using StructureMap.Web;

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Bootstrapping
{
    public class NHibernateRegistry : Registry
    {
        public NHibernateRegistry()
        {
            // Handle Setter Injection
            this.Policies.SetAllProperties(s => s.OfType<IUnitOfWork>());

            // Persistence Infrastructure
            For<IRepository>().Use<NHibernateRepository>();
            For<IRepositoryWithTypedID<Guid>>().Use<NHibernateRepository<Guid>>();
            For<IRepositoryWithTypedID<int>>().Use<NHibernateRepository<int>>();

            For<IUnitOfWork>()
                .HybridHttpOrThreadLocalScoped()
                .Use<UnitOfWork>();

            ForSingletonOf<ISessionFactory>()
                .Use(c => new MultiTenantSessionFactoryConfig().CreateSessionFactory());
        }
    }
}
