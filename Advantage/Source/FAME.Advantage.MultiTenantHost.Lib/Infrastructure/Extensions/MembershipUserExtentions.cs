﻿using System.Diagnostics;
using System.Web.Security;

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Extensions
{
    public static class MembershipUserExtentions
    {
        public static bool IsPasswordReset(this MembershipUser user)
        {
            // If user password creation date matches last password change date then it means user never changed password
            return user.LastPasswordChangedDate == user.CreationDate;
        }

        public static string GetId(this MembershipUser user)
        {
            Debug.Assert(user != null, "membershipUser != null");
            Debug.Assert(user.ProviderUserKey != null, "membershipUser.ProviderUserKey != null");
            return user.ProviderUserKey.ToString();
        }
    }
}
