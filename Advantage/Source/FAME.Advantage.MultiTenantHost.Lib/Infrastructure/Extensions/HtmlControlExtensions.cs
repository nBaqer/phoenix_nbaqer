﻿using System.Collections;
using System.Web.UI.WebControls;

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Extensions
{
    public static class HtmlControlExtensions
    {
        public static void LookupList(this DropDownList dropDown, IEnumerable lookups)
        {
            dropDown.DataTextField = "Name";
            dropDown.DataValueField = "ID";
            dropDown.DataSource = lookups;
            dropDown.DataBind();
            dropDown.Items.Insert(0, new ListItem("Select", ""));
            dropDown.SelectedIndex = 0;
        }
    }
}
