﻿using System;

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Extensions
{
    internal static class StringExtensions
    {
        public static string PutQuotesAround(this string input)
        {
            if (string.IsNullOrWhiteSpace(input)) throw new ArgumentException("input cannot be null or empty");

            return @"""" + input + @"""";
        }
    }
}
