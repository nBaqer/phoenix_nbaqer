﻿
namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Automapper
{
    using AutoMapper.Configuration;

    public interface IMapperDefinition
    {
        void CreateDefinition(MapperConfigurationExpression Mapper);
    }
}
