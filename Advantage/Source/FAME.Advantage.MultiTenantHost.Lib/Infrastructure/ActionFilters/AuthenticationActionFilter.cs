﻿using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.ActionFilters
{
    public class AuthenticationActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                actionContext.Response =
                        actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "You are unauthorized to access this resource");    
            }
        }
    }
}
