﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.ActionFilters
{
    public sealed class ValidationActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.ModelState.IsValid)
            {
                var errors = new Dictionary<string, IEnumerable<string>>();
                foreach (var keyValuePair in actionContext.ModelState)
                {
                    var errorMessages =
                        keyValuePair.Value.Errors.Where(e => !string.IsNullOrWhiteSpace(e.ErrorMessage)).Select(e => e.ErrorMessage);

                    if (errorMessages.Any())
                    {
                        errors[keyValuePair.Key] = keyValuePair.Value.Errors.Select(e => e.ErrorMessage);
                    }
                }

                if (errors.Any())
                {
                    actionContext.Response =
                        actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, errors);
                }
            }
        }
    }
}
