﻿using FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Extensions;
using log4net;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Web.Http.Filters;

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.ActionFilters
{
    public sealed class GlobalErrorHandler : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.ThereIsAnExceptionInTheContext())
            {
                LogTheException(actionExecutedContext.Exception);
            }
        }

        private static void LogTheException(Exception exception)
        {
            var logger = ServiceLocator.Current.GetInstance<ILog>();
            logger.Error("Unhandled Exception: " + exception);
        }
    }
}
