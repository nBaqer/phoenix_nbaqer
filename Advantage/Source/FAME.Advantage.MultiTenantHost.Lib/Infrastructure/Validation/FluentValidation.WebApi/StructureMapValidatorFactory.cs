﻿using System;
using FluentValidation;
using FluentValidation.Internal;
using StructureMap;

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Validation.FluentValidation.WebApi
{
    public class StructureMapValidatorFactory : IValidatorFactory
    {
        private readonly InstanceCache _cache = new InstanceCache();

        public IValidator<T> GetValidator<T>()
        {
            throw new NotSupportedException("Generic implementation is not supported");
        }

        public IValidator GetValidator(Type type)
        {
            if (type == null) return null;

            var abstractValidatorType = typeof (AbstractValidator<>);
            var validatorForType = abstractValidatorType.MakeGenericType(type);

            var validator = Bootstrapping.Bootstrapper.Container.TryGetInstance(validatorForType);

            return validator != null ? _cache.GetOrCreateInstance(validator.GetType()) as IValidator : null;
        }
    }
}
