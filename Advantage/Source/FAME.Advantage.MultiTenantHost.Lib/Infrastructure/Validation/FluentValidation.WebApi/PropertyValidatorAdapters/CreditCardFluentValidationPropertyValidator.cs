﻿using System.Collections.Generic;
using System.Web.Http.Validation;
using FluentValidation.Internal;
using FluentValidation.Validators;

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Validation.FluentValidation.WebApi.PropertyValidatorAdapters {
    internal class CreditCardFluentValidationPropertyValidator : FluentValidationPropertyValidator {
		public CreditCardFluentValidationPropertyValidator(IEnumerable<ModelValidatorProvider> providers, PropertyRule rule, IPropertyValidator validator)
			: base(providers, rule, validator)
		{
		}
	}
}