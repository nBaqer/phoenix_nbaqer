using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Metadata;
using System.Web.Http.Validation;
using FluentValidation;
using FluentValidation.Internal;
using FluentValidation.Validators;

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Validation.FluentValidation.WebApi.PropertyValidatorAdapters {
    public class FluentValidationPropertyValidator : ModelValidator {
		public IPropertyValidator Validator { get; private set; }
		public PropertyRule Rule { get; private set; }

		public FluentValidationPropertyValidator(IEnumerable<ModelValidatorProvider> providers, PropertyRule rule, IPropertyValidator validator)
			: base(providers)
		{
			Validator = validator;

			Rule = rule;
		}

		public override IEnumerable<ModelValidationResult> Validate(ModelMetadata metadata, object container)
		{
			var fakeRule = new PropertyRule(null, x => metadata.Model, null, null, metadata.ModelType, null)
			{
				PropertyName = metadata.PropertyName,
				DisplayName = Rule == null ? null : Rule.DisplayName,
				RuleSet = Rule == null ? null : Rule.RuleSet
			};

			var fakeParentContext = new ValidationContext(container);
			var context = new PropertyValidatorContext(fakeParentContext, fakeRule, metadata.PropertyName);
			var result = Validator.Validate(context);

			return result.Select(failure => new ModelValidationResult { Message = failure.ErrorMessage });
		}
	}
}