﻿using System.Collections.Generic;
using System.Web.Http.Validation;
using FluentValidation.Internal;
using FluentValidation.Validators;

namespace FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Validation.FluentValidation.WebApi.PropertyValidatorAdapters {
    internal class RequiredFluentValidationPropertyValidator : FluentValidationPropertyValidator {
		public RequiredFluentValidationPropertyValidator(IEnumerable<ModelValidatorProvider> providers, PropertyRule rule, IPropertyValidator validator)
			: base(providers, rule, validator)
		{
		}

		public override bool IsRequired {
			get { return true; }
		}
	}
}