﻿using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.MultiTenant;
using FAME.Advantage.MultiTenantHost.Lib.Infrastructure.HttpResponseMessages;
using FAME.Advantage.MultiTenantHost.Lib.Models.ApiAuthenticationKey;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FAME.Advantage.MultiTenantHost.Lib.Controllers
{
    public class ApiAuthenticationKeyController : ApiController
    {
        private readonly IRepositoryWithTypedID<int> _repository;

        public ApiAuthenticationKeyController(IRepositoryWithTypedID<int> repository)
        {
            _repository = repository;
        }

        [HttpPost]
        public HttpResponseMessage Post(InsertApiAuthenticationKeyModel input)
        {
            if (input == null) throw new HttpResponseException(HttpStatusCode.BadRequest);

            var tenant = _repository.Load<Tenant>(input.TenantId);
            var key = new ApiAuthenticationKey(tenant);

            _repository.Save(key);

            return new HttpCreatedRespnseMessage(Request.RequestUri, key.ID.ToString(CultureInfo.InvariantCulture));
        }

        [HttpDelete]
        public void Delete(int? id)
        {
            if (!id.HasValue || id == 0) throw new HttpResponseException(HttpStatusCode.BadRequest);

            var authenticationKey = _repository.Query<ApiAuthenticationKey>().SingleOrDefault(x => x.ID == id.Value);

            if (authenticationKey == null) throw new HttpResponseException(HttpStatusCode.NotFound);

            _repository.Delete(authenticationKey);
        }
    }
}