﻿using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.MultiTenant;
using FAME.Advantage.MultiTenantHost.Lib.Models.Tenant;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
/*DO NOT REMOVE THE FOLLOWING REFERENCES*/
using Telerik.Web.UI; 
using Telerik.Web.Design;
using Telerik.Web.UI.Skins;
/*DO NOT REMOVE THESE REFERENCES*/

namespace FAME.Advantage.MultiTenantHost.Lib.Controllers
{
    public class TenantController : ApiController
    {
        private readonly IRepositoryWithTypedID<int> _repository;

        public TenantController(IRepositoryWithTypedID<int> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IEnumerable<TenantOutputModel> Get()
        {
            var tenants = _repository.Query<Tenant>().ToArray();
            return Mapper.Map<IEnumerable<TenantOutputModel>>(tenants);
        }

        [HttpGet]
        public TenantOutputModel Get(int? id)
        {
            if (!id.HasValue || id == 0) throw new HttpResponseException(HttpStatusCode.BadRequest);

            var tenant = _repository.Query<Tenant>().SingleOrDefault(t => t.ID == id);

            if (tenant == null) throw new HttpResponseException(HttpStatusCode.NotFound);

            return Mapper.Map<TenantOutputModel>(tenant);
        }
    }
}
