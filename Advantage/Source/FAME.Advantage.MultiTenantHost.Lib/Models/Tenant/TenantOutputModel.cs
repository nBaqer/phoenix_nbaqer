﻿using System.Collections.Generic;
using FAME.Advantage.MultiTenantHost.Lib.Models.ApiAuthenticationKey;

namespace FAME.Advantage.MultiTenantHost.Lib.Models.Tenant
{
    public class TenantOutputModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<ApiAuthenticationKeyOutputModel> Keys { get; set; }
    }
}
