﻿
namespace FAME.Advantage.MultiTenantHost.Lib.Models.ApiAuthenticationKey
{
    public class ApiAuthenticationKeyOutputModel
    {
        public int Id { get; set; }
        public string Key { get; set; }
    }
}
