﻿using System;

namespace FAME.Advantage.MultiTenantHost.Lib.Models
{
    public class WapiProxyLoggerOutputModel
    {
        public int Id { get; set; }
        public string TenantName { get; set; }
        public DateTime DateMod { get; set; }
        public int NumberOfRecords  { get; set; }
        public string Company { get; set; }
        public string CalledService { get; set; }
        public string ServiceDescrip { get; set; }
        public int IsOk { get; set; }
        public string Comment { get; set; }
    }
}
