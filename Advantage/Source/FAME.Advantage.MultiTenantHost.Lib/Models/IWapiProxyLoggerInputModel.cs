﻿namespace FAME.Advantage.MultiTenantHost.Lib.Models
{
    public interface IWapiProxyLoggerInputModel
    {
        string TenantName { get; set; }
        string Company { get; set; }
        string CalledService { get; set; }
        int IsOk { get; set; }
    }
}