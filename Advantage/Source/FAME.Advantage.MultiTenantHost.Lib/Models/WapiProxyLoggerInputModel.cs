﻿using System;

namespace FAME.Advantage.MultiTenantHost.Lib.Models
{
    [Serializable]
    public class WapiProxyLoggerInputModel : IWapiProxyLoggerInputModel
    {
        public const string XALL = "ALL";
        
        /// <summary>
        /// Use factory insteand constructor ro create the class.
        /// </summary>
        /// <returns></returns>
        public static IWapiProxyLoggerInputModel Factory()
        {
            
            var input = new WapiProxyLoggerInputModel
            {
                CalledService = XALL,
                Company = XALL,
                IsOk = 2,
                TenantName = XALL
            };
            return input;

        }
        
        public string TenantName { get; set; }
        public string Company { get; set; }
        public string CalledService { get; set; }
        public int IsOk { get; set; }
    }
}
