﻿using FAME.Advantage.MultiTenantHost.Lib.Models.ApiAuthenticationKey;
using FluentValidation;

namespace FAME.Advantage.MultiTenantHost.Lib.Validators.ApiAuthenticationKey
{
    public class InsertApiAuthenticationKeyModelValidator : AbstractValidator<InsertApiAuthenticationKeyModel>
    {
        public InsertApiAuthenticationKeyModelValidator()
        {
            RuleFor(x => x.TenantId).NotEmpty().GreaterThan(0);
        }
    }
}