﻿using System;
using System.Web;
using System.Web.Security;

namespace FAME.Advantage.MultiTenantHost.Lib
{
    public static class SecurityHelper
    {
        public static void RedirectToLoginPage()
        {
            var response = HttpContext.Current.Response;
            response.Redirect(FormsAuthentication.LoginUrl);
        }

        public static void RedirectToLoginPage(string errorMessage)
        {
            var response = HttpContext.Current.Response;
            response.Redirect(String.Format("{0}?errmsg={1}", FormsAuthentication.LoginUrl, HttpUtility.UrlEncode(errorMessage)));
        }

        public static void LogOut()
        {
            HttpContext.Current.Session.Abandon();
            FormsAuthentication.SignOut();
            RedirectToLoginPage();
        }
    }
}
