﻿using System.ComponentModel;
using System.Configuration.Install;

namespace WapiWindowsService
{
    /// <summary>
    /// Project installer component for WAPI Windows Service.
    /// </summary>
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        private void serviceInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {

        }
    }
}
