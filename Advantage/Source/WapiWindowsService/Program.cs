﻿using System.ServiceProcess;

namespace WapiWindowsService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun =
            { 
                new WapiWindowsService() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
