﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiWindowsService.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   WAPI Windows Service Main class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiWindowsService
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Timers;
    using System.IO;
    using WapiDll;
    using WapiDll.Data.Contract;
    using Timer = System.Timers.Timer;

    /// <summary>
    /// WAPI Windows Service Main class.
    /// </summary>
    public partial class WapiWindowsService : ServiceBase
    {
        #region public fields
        /// <summary>
        /// The trace source.
        /// </summary>
        public static readonly TraceSource Ts = new TraceSource("TraceWapiWs");
        #endregion

        #region fields

        /// <summary>
        /// Gets or sets This timer is created to scan the on-demand operation subsystem.<br/>
        /// its purpose is go to proxy and watch if the on-demand flag in <br/>
        /// database was set to 1. in this case begin a on-demand execution of<br/>
        /// the operation server.
        /// </summary>
        private Timer onDemandTimer;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WapiWindowsService"/> class. 
        /// WAPI Windows Service Constructor
        /// </summary>
        public WapiWindowsService()
        {
            // Debugger.Launch();
            this.InitializeComponent();
            this.WapiServiceList = new List<IWapiService>();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets List of <see cref="IWapiService"/> created operation service object.<br/>
        /// Each operation setting is executed in his specific object. This list
        /// hold all created object.
        /// </summary>
        public IList<IWapiService> WapiServiceList { get; protected set; }

        /// <summary>
        /// Gets or sets Hold a array of the different companies that can be access through the windows service.<br/>
        /// Each company in general represent a tenant. This is useful in multi-tenant environment.<br/>
        /// it is also used with only one tenant
        /// </summary>
        public string[] ArrayOfCompanies { get; set; }

        #endregion

        #region Windows Service Events

        /// <summary>
        /// Service Start
        /// </summary>
        /// <param name="args">Optional argument to be passed to the service</param>
        protected override void OnStart(string[] args)
        {
            Debugger.Launch(); // <-- Simple form to debug a web services 
            string filename = @"WapiWindowService.log";
            string filePath = AppDomain.CurrentDomain.BaseDirectory + filename;
            if (File.Exists(filePath))
            {
                var f = new FileInfo(filePath);
                if (f.Length > 1048576)
                {
                    FileStream fileStream = File.Open(filePath, FileMode.Open);
                    fileStream.SetLength(0);
                    fileStream.Close();
                }
            }
            Ts.TraceInformation($"Starting Service at {DateTime.Now}. {Environment.NewLine}");
            
            // Read from configuration file
            try
            {
                var proxyUrl = ConfigurationManager.AppSettings.Get("UrlWapiProxy");

                // Get the list of associated companies
                var companySecret = ConfigurationManager.AppSettings.Get("SecretCompanyKeys");
                Ts.TraceInformation($"Configuration Parameters proxyUrl: {proxyUrl}{Environment.NewLine}");
                string[] separator = { "," };
                this.ArrayOfCompanies = companySecret.Split(separator, StringSplitOptions.RemoveEmptyEntries);

                // Loop through the list and get the list of operation for each Company
                foreach (string s in this.ArrayOfCompanies)
                {
                    try
                    {
                        string transmReason;
                        var listOfOperations = WapiService.GetListOperationsFromProxy(s, proxyUrl, out transmReason);
                        if (listOfOperations == null)
                        {
                            if (transmReason != "OK")
                            {
                                throw new ArgumentException(transmReason);
                            }

                            Ts.TraceData(TraceEventType.Warning, 7, $"Company Code {s} does not return any service active at {DateTime.Now}{Environment.NewLine}");
                            continue;
                        }

                        // Loop for each operation in the list and create a new instance of wapiDll for each operation in the list
                        foreach (IWapiOperationsSettings operation in listOfOperations)
                        {
                            operation.UrlProxy = proxyUrl;
                            operation.LogFile = ConfigurationManager.AppSettings.Get("LogFile");
                            operation.SizeLogFile = int.Parse(ConfigurationManager.AppSettings.Get("LogSize"));
                            operation.Tracer = Ts;
                            operation.SecretCodeCompany = s;
                            IList<IWapiService> wapiServiceList = this.WapiServiceList;
                            switch (operation.CodeExtOperationMode.ToUpper())
                            {
                                case "VOYANT_HISTORICAL":
                                    {
                                        var service = WapiServiceVoyantHistorical.Factory(operation, ref wapiServiceList);
                                        this.WapiServiceList.Add(service);
                                        break;
                                    }

                                case "VOYANT_CURRENT":
                                    {
                                        var service = WapiServiceVoyantCurrent.Factory(operation, ref wapiServiceList);
                                        this.WapiServiceList.Add(service);
                                        break;
                                    }

                                case "WAPI_TEST_SYSTEM":
                                    {
                                        var service = WapiServiceTestSystem.Factory(operation, ref wapiServiceList);
                                        this.WapiServiceList.Add(service);
                                        break;
                                    }

                                case "KLASS_APP_SERVICE":
                                    {
                                        var service = WapiServiceKlassApp.Factory(operation, ref wapiServiceList);
                                        this.WapiServiceList.Add(service);
                                        break;
                                    }

                                case "AFA_INTEGRATION_SERVICE":
                                    {
                                        var service = WapiServiceAfaIntegration.Factory(operation, ref wapiServiceList);
                                        this.WapiServiceList.Add(service);
                                        break;
                                    }

                                case "ADVANTAGE_API_SERVICE":
                                    {
                                        var service = WapiServiceAdvantageApi.Factory(operation, ref wapiServiceList);
                                        this.WapiServiceList.Add(service);
                                        break;
                                    }

                                case "TC_FILEWATCH_SERVICE":
                                    {
                                        var service = WapiServiceTCFileWatch.Factory(operation, ref wapiServiceList);
                                        this.WapiServiceList.Add(service);
                                        break;
                                    }

                                default:
                                    {
                                        Debug.WriteLine("Operation not implemented");
                                        Ts.TraceData(TraceEventType.Error, 2, $"{DateTime.Now} - Operation {operation.CodeExtOperationMode} is not implemented{Environment.NewLine}");
                                        throw new NotImplementedException();
                                    }
                            }
                        }

                        foreach (var service in this.WapiServiceList)
                        {
                            try
                            {
                                service.StartTimer();
                                Debug.WriteLine($"Created {service.Operation.CodeOperation}");
                                Ts.TraceInformation(service.Operation.CodeExtOperationMode + " Created...");
                                service.PostLogToServer(
                                    service.Operation,
                                    $"Operation {service.Operation.CodeOperation} successfully created",
                                    0);
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = $"Exception starting Wapi Service Operation of {service.Operation} at {DateTime.Now} :{Environment.NewLine} Exception Message: {ex.Message}{Environment.NewLine} Stack Trace:{ex.StackTrace}{Environment.NewLine}";
                                Ts.TraceData(
                                    TraceEventType.Error,
                                    3,
                                    errorMessage);
                                service.PostLogToServer(
                                    service.Operation,
                                    errorMessage,
                                    0);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        Ts.TraceData(
                            TraceEventType.Error,
                            3,
                            $"Exception starting Wapi Services for company {s} at {DateTime.Now} :{Environment.NewLine} Exception Message: {ex.Message}{Environment.NewLine} Stack Trace:{ex.StackTrace}{Environment.NewLine}");
                    }
                    // End of loop
                }

                try
                {
                    // End of Loop
                    // Create the time and associated a event to control the OnDemand Service
                    Ts.TraceInformation("Creation of Services successful...");
                    var timeinterval = int.Parse(ConfigurationManager.AppSettings.Get("ClockTick")); // seconds...
                    this.onDemandTimer = new Timer(timeinterval * 1000);
                    this.onDemandTimer.Elapsed += this.OnDemandTimerElapsed;
                    this.onDemandTimer.Start();
                    Ts.TraceInformation("Start Service event successful...");

                    IWapiService advantageApiInfor =
                        WapiServiceList.FirstOrDefault(x => x.Operation.CodeExtOperationMode == "ADVANTAGE_API_SERVICE");
                    if (advantageApiInfor != null)
                    {
                        WapiServiceAutomaticFSAPCheck wapiServiceAutomaticFSAPCheck = new WapiServiceAutomaticFSAPCheck(advantageApiInfor);
                        wapiServiceAutomaticFSAPCheck.ExecuteActions();
                    }

                    IWapiService timeClockFileWatch =
                       WapiServiceList.FirstOrDefault(x => x.Operation.CodeExtOperationMode == "TC_FILEWATCH_SERVICE");
                    if (advantageApiInfor != null)
                    {
                        timeClockFileWatch.ExecuteActions();
                    }
                }
                catch (Exception ex)
                {
                    Ts.TraceData(
                        TraceEventType.Error,
                        3,
                        $"Exception starting WapiServiceAutomaticFSAPCheck Service at {DateTime.Now} :{Environment.NewLine} Exception Message: {ex.Message}{Environment.NewLine} Stack Trace:{ex.StackTrace}{Environment.NewLine}");
                }
            }
            catch (Exception ex)
            {
                Ts.TraceData(
                    TraceEventType.Error,
                    3,
                    $"Exception starting Windows Service at {DateTime.Now} :{Environment.NewLine} Exception Message: {ex.Message}{Environment.NewLine} Stack Trace:{ex.StackTrace}{Environment.NewLine}");
                // log the exception and say that the service is stopped.
                Ts.TraceData(TraceEventType.Stop, 1, $"Stopping Windows service at {DateTime.Now} :{Environment.NewLine} because of Exception starting Windows Service:{Environment.NewLine} Exception Message: {ex.Message}{Environment.NewLine} Stack Trace:{ex.StackTrace}{Environment.NewLine}");
                this.Stop(); // Stop the service is we have a error.
            }
        }

        /// <summary>
        /// Stop Windows Service.
        /// </summary>
        protected override void OnStop()
        {
            Ts.TraceInformation("Service was Stopped");
        }

        #endregion

        #region Timer Event

        /// <summary>
        /// On Demand Timer elapsed event
        /// Used for poll the on-demand operation flag on the server.
        /// </summary>
        /// <param name="sender">The timer that raise the event</param>
        /// <param name="e">
        /// See the <see cref="ElapsedEventArgs"/>
        /// </param>
        private void OnDemandTimerElapsed(object sender, ElapsedEventArgs e)
        {
            this.onDemandTimer.Stop();
            // Debug.WriteLine("Timer was Stopped");
            Ts.TraceData(TraceEventType.Verbose, 7, $"Timer was Stopped at {DateTime.Now}{Environment.NewLine}");

            // Write here the on demand logic....
            try
            {
                foreach (string company in this.ArrayOfCompanies)
                {
                    string company1 = company;
                    IList<IWapiService> op =
                        this.WapiServiceList.Where(x => x.Operation.SecretCodeCompany == company1).ToList();
                    if (op.Count > 0)
                    {
                        IList<IOnDemandFlag> flags = op[0].GetOnDemandFlags();
                        foreach (IOnDemandFlag onDemandFlag in flags)
                        {
                            if (onDemandFlag.OnDemandOperation != 0)
                            {
                                IOnDemandFlag flag = onDemandFlag;
                                IWapiService service = op.SingleOrDefault(x => x.Operation.ID == flag.Id);
                                if (service != null)
                                {
                                    try
                                    {
                                        // See if the service is executing now. if it is executing, report the status and does not executed again
                                        if (service.ServiceTimer.Enabled == false)
                                        {
                                            // service is executing....
                                            service.PostLogToServer(
                                                service.Operation,
                                                $"On Demand report service {service.Operation} is executing at {DateTime.Now}. On demand canceled{Environment.NewLine}",
                                                0);
                                            service.PostOnDemandFlag(flag.Id, 0);
                                            Ts.TraceData(
                                                TraceEventType.Warning,
                                                11,
                                                $"On Demand report service  {service.Operation} is executing at {DateTime.Now}. On demand canceled{Environment.NewLine}",
                                                service.Operation);
                                        }
                                        else
                                        {
                                            service.ExecuteActions();
                                            service.PostOnDemandFlag(flag.Id, 0);
                                            service.PostLogToServer(
                                                service.Operation,
                                                $"Service {service.Operation.CodeOperation} was executed successfully On Demand: {service.OptionalMessage}",
                                                0);
                                            Ts.TraceInformation(
                                                 $"Service {service.Operation.CodeOperation} was executed successfully On Demand: {service.OptionalMessage}",
                                                service.Operation.CodeOperation);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Ts.TraceData(
                                            TraceEventType.Error,
                                            10,
                                            $"Service {service.Operation.CodeOperation} Terminate with Exception {ex.Message} On Demand at {DateTime.Now} - Stake Trace:{ex.StackTrace}{Environment.NewLine} ");
                                        service.PostLogToServer(
                                             service.Operation,
                                             $"Service {service.Operation.CodeOperation} Terminate with Exception {ex.Message} On Demand at {DateTime.Now} - Stake Trace:{ex.StackTrace}{Environment.NewLine} ",
                                             1);
                                        service.PostOnDemandFlag(flag.Id, 0);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Ts.TraceData(TraceEventType.Error, 12, $"Exception: {ex.Message} at {DateTime.Now} - StackTrace: {ex.StackTrace}{Environment.NewLine}");
                Debug.WriteLine($"Exception {ex.Message}");
            }
            finally
            {
                var timeinterval = int.Parse(ConfigurationManager.AppSettings.Get("ClockTick")); // seconds...
                this.onDemandTimer.Interval = timeinterval * 1000;
                this.onDemandTimer.Start();
                Ts.TraceData(TraceEventType.Verbose, 7, $"Timer was Started at {DateTime.Now}.{Environment.NewLine}");
            }
        }

        #endregion
    }
}
