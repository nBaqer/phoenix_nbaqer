ECHO OFF
REM =========================================================================================
REM INSTALL WAPI WINDOWS SERVICE V1.0 Advantage 3.7 Fame - Author JAGG
REM (C) Copyright 2014 FAME
REM =========================================================================================
REM =========================================================================================
REM Conditions
REM You must run this script directly in the windows service installation directory!
REM Maybe you need to change the script is your server has the install utility in other file
REM =========================================================================================
REM This Batch uninstall the WAPI Windows Service In your computer.
REM !!!Before uninstallation, you must to Stopped it using Advantage Configuration Page!!!
REM =========================================================================================
ECHO ON
PAUSE

C:\Windows\Microsoft.NET\Framework\v4.0.30319\InstallUtil.exe /u "WapiWindowsService.exe"

PAUSE