ECHO OFF 
Goto :Start
rem =========================================================================
rem INSTALL WAPI WINDOWS SERVICE V1.0 Advantage 3.7 Fame - Author JAGG
rem (C) Copyright 2014 FAME
rem =========================================================================
rem =========================================================================
rem Conditions
rem Run this script directly in the windows service installation directory!
rem Maybe you need to change the script is your server has the install 
rem utility in other file.
rem =========================================================================
rem This Batch install the WAPI Windows Service In your computer.
rem You can Start it using Advantage Configuration Page.
rem =========================================================================
:Start
ECHO ON
PAUSE

C:\Windows\Microsoft.NET\Framework\v4.0.30319\InstallUtil.exe "WapiWindowsService.exe"


ECHO CLick any key to close this.
PAUSE

