﻿Option Explicit On
Option Strict On

Imports FAME.Advantage.Domain.Infrastructure.Entities
Imports FAME.Advantage.Domain.Persistence.Infrastructure.UoW
Imports Microsoft.Practices.ServiceLocation

Namespace Helpers

    Public Class UnitOfWorkHelper

        <DebuggerStepThrough>
        Public Shared Sub RunInUnitOfWork(action As Action)
            Dim unitOfWork = Resolve(Of IUnitOfWork)()

            Using scope = unitOfWork.CreateScope()
                action.Invoke()
                scope.Complete()
            End Using
        End Sub

        <DebuggerStepThrough>
        Public Shared Sub RunInUnitOfWork(action As Action(Of IRepository))
            Dim unitOfWork = Resolve(Of IUnitOfWork)()

            Using scope = unitOfWork.CreateScope()
                action.Invoke(Resolve(Of IRepository)())
                scope.Complete()
            End Using
        End Sub

        <DebuggerStepThrough>
        Public Shared Sub RunInUnitOfWork(action As Action(Of IRepositoryWithTypedID(Of Int32)))
            Dim unitOfWork = Resolve(Of IUnitOfWork)()

            Using scope = unitOfWork.CreateScope()
                action.Invoke(Resolve(Of IRepositoryWithTypedID(Of Int32))())
                scope.Complete()
            End Using
        End Sub

        <DebuggerStepThrough>
        Public Shared Sub RunInUnitOfWork(action As Action(Of IRepository, IRepositoryWithTypedID(Of Int32)))
            Dim unitOfWork = Resolve(Of IUnitOfWork)()

            Using scope = unitOfWork.CreateScope()
                action.Invoke(Resolve(Of IRepository), Resolve(Of IRepositoryWithTypedID(Of Int32))())
                scope.Complete()
            End Using
        End Sub

        <DebuggerStepThrough>
        Public Shared Function RunInUnitOfWork(Of T)(action As Func(Of T)) As T
            Dim unitOfWork = Resolve(Of IUnitOfWork)()

            Using scope = unitOfWork.CreateScope()
                Dim val = action.Invoke()
                scope.Complete()

                Return val
            End Using
        End Function

        <DebuggerStepThrough>
        Public Shared Function RunInUnitOfWork(Of T)(action As Func(Of IRepository, T)) As T
            Dim unitOfWork = Resolve(Of IUnitOfWork)()

            Using scope = unitOfWork.CreateScope()
                Dim val = action.Invoke(Resolve(Of IRepository)())
                scope.Complete()

                Return val
            End Using
        End Function

        <DebuggerStepThrough>
        Public Shared Function RunInUnitOfWork(Of T)(action As Func(Of IRepositoryWithTypedID(Of Int32), T)) As T
            Dim unitOfWork = Resolve(Of IUnitOfWork)()

            Using scope = unitOfWork.CreateScope()
                Dim val = action.Invoke(Resolve(Of IRepositoryWithTypedID(Of Int32))())
                scope.Complete()

                Return val
            End Using
        End Function

        <DebuggerStepThrough>
        Private Shared Function Resolve(Of T)() As T
            Return ServiceLocator.Current.GetInstance(Of T)()
        End Function
    End Class
End Namespace