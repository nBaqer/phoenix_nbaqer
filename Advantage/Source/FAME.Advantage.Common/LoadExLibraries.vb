﻿Imports System.IO
Imports System.Runtime.InteropServices

Public Class LoadExLibraries
    <DllImport("kernel32", SetLastError:=True)> _
    Private Shared Function LoadLibrary(lpFileName As String) As IntPtr
    End Function
    Public Sub Load()
        Dim folder As String = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), "..\Microsoft.NET\Framework\v2.0.50727")
        folder = Path.GetFullPath(folder)
        LoadLibrary(Path.Combine(folder, "vjsnativ.dll"))
        LoadLibrary(Path.Combine(folder, "vjscor.dll"))
        LoadLibrary(Path.Combine(folder, "vjslib.dll"))
    End Sub
End Class
