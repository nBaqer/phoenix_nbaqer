﻿
Imports System.Collections.Generic

Namespace StudentRegistration.Lib
    Public Class [Class]
        Public Property newClassId() As String
            Get
                Return m_newClassId
            End Get
            Set(value As String)
                m_newClassId = value
            End Set
        End Property
        Private m_newClassId As String
        Public Property oldClassId() As String
            Get
                Return m_oldClassId
            End Get
            Set(value As String)
                m_oldClassId = value
            End Set
        End Property
        Private m_oldClassId As String
        'needed for archive action
        Public Property TermId() As String
            Get
                Return m_TermId
            End Get
            Set(value As String)
                m_TermId = value
            End Set
        End Property
        Private m_TermId As String
        Public Property CourseComponents() As IEnumerable(Of CourseComponent)
            Get
                Return m_CourseComponents
            End Get
            Set(value As IEnumerable(Of CourseComponent))
                m_CourseComponents = value
            End Set
        End Property
        Private m_CourseComponents As IEnumerable(Of CourseComponent)
    End Class
    Public Class CourseComponent
        Public Property ComponentId() As String
            Get
                Return m_ComponentId
            End Get
            Set(value As String)
                m_ComponentId = value
            End Set
        End Property
        Private m_ComponentId As String
        Public Property ComponentName() As String
            Get
                Return m_ComponentName
            End Get
            Set(value As String)
                m_ComponentName = value
            End Set
        End Property
        Private m_ComponentName As String
        'public decimal Score { get; set; }
    End Class
    Public Class [Student]
        Public Property StudentEnrollmentId() As String
            Get
                Return m_StudentEnrollmentId
            End Get
            Set(value As String)
                m_StudentEnrollmentId = value
            End Set
        End Property
        Private m_StudentEnrollmentId As String
        Public Property Classes() As IEnumerable(Of [Class])
            Get
                Return m_Classes
            End Get
            Set(value As IEnumerable(Of [Class]))
                m_Classes = value
            End Set
        End Property
        Private m_Classes As IEnumerable(Of [Class])

    End Class
End Namespace


