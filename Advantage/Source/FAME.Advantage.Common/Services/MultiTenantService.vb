﻿Imports FAME.Advantage.Domain.Infrastructure.Entities
Imports FAME.Advantage.Domain.MultiTenant
Imports FAME.Advantage.Common.Helpers
Imports Microsoft.Practices.ServiceLocation

Namespace Services

    Public Interface IMultiTenantService

        Function GetConnectionString(userId As Guid, tenantId As Integer) As String

        Function GetConnectionStringVariants(userId As Guid, tenantId As Integer) As ConnectionStringVariants

        Sub UpdateTenantAccess(userId As Guid, tenantId As Integer)

        Sub ResetPasswordDates(userId As Guid)

        Function GetTenantName(userId As Guid,ByVal tenantId As Integer) As String
    End Interface

    Public Class MultiTenantService
        Implements IMultiTenantService
        Public Function GetTenantName(userId As Guid,ByVal tenantId As Integer) As String Implements IMultiTenantService.GetTenantName
            Return UnitOfWorkHelper.RunInUnitOfWork(Function(r As IRepository)
                Dim user = r.Get(Of TenantUser)(userId)
                Return user.GetTenant(tenantId).Name
            End Function)
        End Function
        Public Function GetConnectionString(userId As Guid, tenantId As Integer) As String Implements IMultiTenantService.GetConnectionString
            Return UnitOfWorkHelper.RunInUnitOfWork(Function(r As IRepository)
                                                        Dim user = r.Get(Of TenantUser)(userId)
                                                        Return user.GetConnectionString(tenantId)
                                                    End Function)
        End Function

        Public Function GetConnectionStringVariants(userId As Guid, tenantId As Integer) As ConnectionStringVariants Implements IMultiTenantService.GetConnectionStringVariants
            Return UnitOfWorkHelper.RunInUnitOfWork(Function(r As IRepository)
                                                        Dim user = r.Get(Of TenantUser)(userId)
                                                        Return New ConnectionStringVariants() With { _
                                                            .ConnectionString = user.GetConnectionString(tenantId), _
                                                            .OleDbConnectionString = user.GetOleDbConnectionString(tenantId) _
                                                        }
                                                    End Function)
        End Function

        Public Sub UpdateTenantAccess(userId As Guid, tenantId As Integer) Implements IMultiTenantService.UpdateTenantAccess
            UnitOfWorkHelper.RunInUnitOfWork(Sub(r As IRepository, r2 As IRepositoryWithTypedID(Of Integer))
                                                 Dim user = r.Get(Of TenantUser)(userId)
                                                 Dim tenant = r2.Get(Of Tenant)(tenantId)
                                                 user.AddTenant(tenant)

                                                 r.Save(user)
                                             End Sub)
        End Sub

        Public Sub ResetPasswordDates(userId As Guid) Implements IMultiTenantService.ResetPasswordDates
            Dim action As Action = Sub()
                                       Dim passwordReset = ServiceLocator.Current.GetInstance(Of IPasswordResetService)()
                                       passwordReset.ResetPasswordDates(userId)
                                   End Sub

            UnitOfWorkHelper.RunInUnitOfWork(action)
        End Sub
    End Class

    Public Class ConnectionStringVariants
        Public Property ConnectionString() As String
        Public Property OleDbConnectionString() As String
    End Class
End Namespace