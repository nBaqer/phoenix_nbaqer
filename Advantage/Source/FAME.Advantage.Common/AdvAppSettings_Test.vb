﻿
Imports System
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Web

<Serializable()> _
Public NotInheritable Class AdvAppSettings_Test

    Public tmpAppSettings As New Hashtable(StringComparer.OrdinalIgnoreCase)
    Public DataD As New Dictionary(Of Guid, Hashtable)
#Region "Public Methods"

    Public Sub New()

    End Sub
    Public Function AppSettings(p1 As String) As Object
        ' Return tmpAppSettings(p1)
        'Dim Campusid As Guid=
        '= New Guid(HttpContext.Current.Request.Params("cmpid").ToString)
        ''     '= 'from application
        Dim MyItem As String
        Dim MyHashtable As Hashtable
        Try
            If HttpContext.Current.Session("CurrentCampus") Is Nothing Then
                'When you first hit the login page you need to getsome global settings
                'from the config tables but you do not have a defaultcampus for your username
                'since you have not logged in yet. The "GlobalCampus"Guid will be used for these settings
                'for example, The "schoolname" setting is a pre-loginconfig setting
                Dim GlobalCampus As Guid
                For Each k As Guid In DataD.Keys
                    GlobalCampus = k
                Next
                MyHashtable = DataD.Item(GlobalCampus)
                MyItem = MyHashtable.Item(p1).ToString
            Else
                MyHashtable = DataD.Item(New Guid(HttpContext.Current.Session("CurrentCampus").ToString))
                MyItem = MyHashtable.Item(p1).ToString
            End If


        Catch ex As Exception
            Throw ex
        End Try
        Return MyItem
    End Function
    Public Function AppSettings(p1 As String, campus As String) As Object
        If String.IsNullOrWhiteSpace(campus) Then
            Return AppSettings(p1)
        Else
            Dim MyItem As String
            Dim MyHashtable As New Hashtable

            Dim Campusid As Guid = New Guid(campus)        '= 'from application
            Try
                MyHashtable = DataD.Item(Campusid)
                MyItem = MyHashtable.Item(p1).ToString
            Catch ex As Exception
                Throw ex
            End Try
            Return MyItem
        End If


    End Function
    Public Function Count(campus As String) As Integer
        Dim Campusid As Guid = New Guid(campus)
        Dim MyHashtable As Hashtable = DataD.Item(Campusid)
        Return MyHashtable.Count
    End Function
    Public Sub RetrieveSettings(Optional ByVal strCampusId As String = "")

        Dim dbCon As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("ConnectionString"))
        Dim myCommand As SqlCommand = New SqlCommand("sp_GetAppSettings", dbCon)
        myCommand.CommandType = CommandType.StoredProcedure
        myCommand.Parameters.AddWithValue("@CampusId", strCampusId)
        'opening the connection
        dbCon.Open()
        Try
            Dim MyDataReader As SqlDataReader
            'set the datareader with the data
            MyDataReader = myCommand.ExecuteReader

            'Load the Key\Value pair into the hash table
            Call BuildCollection(tmpAppSettings)

            Call BuildCampusCollection()
        Catch ex As Exception

        Finally
            If dbCon.State = ConnectionState.Open Then dbCon.Close()
        End Try
    End Sub
    Public Sub GetCampusSettings(strCampusId As String, tblHash As Hashtable)

        Dim dbCon As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("ConnectionString"))
        Dim myCommand As SqlCommand = New SqlCommand("sp_GetAppSettings", dbCon)
        myCommand.CommandType = CommandType.StoredProcedure
        myCommand.Parameters.AddWithValue("@CampusId", strCampusId)
        'opening the connection
        dbCon.Open()
        Try
            Dim MyDataReader As SqlDataReader
            'set the datareader with the data
            MyDataReader = myCommand.ExecuteReader

            'Load the Key\Value pair into the hash table
            Call BuildCollection(tblHash)


        Catch ex As Exception

        Finally
            If dbCon.State = ConnectionState.Open Then dbCon.Close()
        End Try
    End Sub
    Public Function GetSetting(ByVal strKey As String) As String
        Dim MyDataReader As SqlDataReader
        While MyDataReader.Read()
            If strKey = MyDataReader("KeyName").ToString Then
                GetSetting = MyDataReader("Value").ToString
                Return GetSetting
            End If
        End While

    End Function

    Public Sub BuildCollection(tblHash As Hashtable)
        'Load the key/value pairs into the collection
        Dim strLastKey As String = ""
        Dim MyDataReader As SqlDataReader
        'First, lets clear any values before inserting
        If tblHash.Count > 0 Then
            tblHash.Clear()
        End If
        'Load the new values
        'Note: We can only have one key/value pair per hashtable. Whenthis is initiated prior to login, there will be no campus
        'associated with the pairs, therefore, all values, regardlessof campus will be returned. In this case, we may get duplicate keys.
        'We needed to account for this, hence the GoTo. This will onlyoccur on the initial load.
        While MyDataReader.Read()

            If strLastKey = MyDataReader("KeyName").ToString Then
                GoTo SkipAdd
            End If
            tblHash.Add(MyDataReader("KeyName"), MyDataReader("Value"))
SkipAdd:
            strLastKey = MyDataReader("KeyName").ToString
        End While


        'We will also add the connection information so we can just do a find And replace
        tblHash.Add("ConString",System.Configuration.ConfigurationManager.AppSettings("ConString"))
        tblHash.Add("ConnectionString",System.Configuration.ConfigurationManager.AppSettings("ConnectionString"))
        tblHash.Add("AdvantageConnectionString",System.Configuration.ConfigurationManager.AppSettings("AdvantageConnectionString"))
        'DataD.Clear()
        'DataD.Add(New Guid("D4D42C06-8712-4C86-A8E1-9575C8F19186"),AppSettings)

        'Dim str As String = "GradesFormat"
        'Dim rtn As String = AppSettings2(str)
        'Dim strResult As String = String.Empty
    End Sub
    Public Function GetAllCampuses() As DataTable
        Dim dbCon As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("ConnectionString"))
        Dim myCommand As SqlCommand = New SqlCommand("usp_GetAllCampuses", dbCon)
        myCommand.CommandType = CommandType.StoredProcedure
        Dim myDataSet As New DataSet
        Dim myDataAdapter As SqlDataAdapter
        'opening the connection
        dbCon.Open()
        Try

            'set the datareader with the data
            myDataAdapter = New SqlDataAdapter(myCommand)
            myDataAdapter.Fill(myDataSet)
            Return myDataSet.Tables(0)
        Catch ex As Exception

        Finally
            If dbCon.State = ConnectionState.Open Then dbCon.Close()
        End Try
    End Function
    Public Sub BuildCampusCollection()
        Dim campusId As Guid

        Dim dtCampuses As DataTable = GetAllCampuses()
        Dim tblHash(dtCampuses.Rows.Count) As Hashtable
        DataD.Clear()
        For i = 0 To dtCampuses.Rows.Count - 1
            campusId = New Guid(dtCampuses.Rows(i)("CampusId").ToString)
            tblHash(i) = New Hashtable(StringComparer.OrdinalIgnoreCase)
            GetCampusSettings(campusId.ToString, tblHash(i))
            DataD.Add(campusId, tblHash(i))
        Next

    End Sub

#End Region



End Class