﻿Imports System
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Configuration
Imports Microsoft.Practices.ServiceLocation
Imports FAME.Advantage.Common.Services
Imports System.Web

<Serializable()> _
Public NotInheritable Class PortalAdvAppSettings

    Private ReadOnly tmpAppSettings As New Hashtable(StringComparer.OrdinalIgnoreCase)
    Private ReadOnly dataD As New Dictionary(Of Guid, Hashtable)

    Private ReadOnly connectionString As String
    Private ReadOnly oleDbConnectionString As String


#Region "Get App Settings"
    Public Shared Function GetAppSettings() As AdvAppSettings
        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If
        Return myAdvAppSettings
    End Function
#End Region

    Public Sub New()
        'Dim variants = GetConnectionStringVariants()
        connectionString = ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString
        oleDbConnectionString = ConfigurationManager.ConnectionStrings("ClientConnectionString").ConnectionString

        RetrieveSettings()
    End Sub

    Public Function AppSettings(p1 As String) As Object
        ' Return tmpAppSettings(p1)
        Dim campusid As Guid = Guid.Empty
        Dim campusIdStr As String
        '= New Guid(HttpContext.Current.Request.Params("cmpid").ToString)
        ''     '= 'from application
        Dim myItem As String
        Dim myHashtable As Hashtable
        If (IsNothing(HttpContext.Current.Request.Params("cmpid"))) Then
            campusIdStr = String.Empty
        Else
            campusIdStr = HttpContext.Current.Request.Params("cmpid").ToString
        End If

        If Not String.IsNullOrEmpty(campusIdStr) Then
            campusid = Guid.Parse(campusIdStr)
        End If


        If campusid = Guid.Empty Then
            'When you first hit the login page you need to getsome global settings
            'from the config tables but you do not have a defaultcampus for your username
            'since you have not logged in yet. The "GlobalCampus"Guid will be used for these settings
            'for example, The "schoolname" setting is a pre-loginconfig setting
            Dim globalCampus As Guid
            For Each k As Guid In dataD.Keys
                globalCampus = k
            Next
            myHashtable = dataD.Item(globalCampus)
        Else



            If Not String.IsNullOrEmpty(campusIdStr) Then

                Dim capus As Guid = New Guid(campusIdStr)
                myHashtable = dataD.Item(capus)

            Else
                Dim capus As Guid = New Guid(HttpContext.Current.Session("CurrentCampus").ToString())
                myHashtable = dataD.Item(capus)
            End If

        End If
        If (IsNothing(myHashtable.Item(p1)) = True OrElse IsNothing(myHashtable.Item(p1).ToString()) = True) Then
            Trace.WriteLine("Missing the config Setting " + p1)
            Return Nothing
        End If
        myItem = myHashtable.Item(p1).ToString()
        Return myItem
    End Function

    Public Function AppSettings(p1 As String, campus As String) As String
        If String.IsNullOrWhiteSpace(campus) Then
            Return CType(AppSettings(p1), String)
        Else
            Dim myItem As String
            Dim myHashtable As Hashtable

            Dim campusId As Guid = New Guid(campus)        '= 'from application
            myHashtable = dataD.Item(campusId)
            myItem = myHashtable.Item(p1).ToString

            Return myItem
        End If
    End Function

    Public Function Count(campus As String) As Integer
        Dim campusId As Guid = New Guid(campus)
        Return dataD.Item(campusId).Count
    End Function

    Public Sub RetrieveSettings(Optional ByVal strCampusId As String = "")
        Dim dbCon As New SqlConnection(connectionString)
        Dim myCommand As SqlCommand = New SqlCommand("sp_GetAppSettings", dbCon)
        myCommand.CommandType = CommandType.StoredProcedure
        myCommand.Parameters.AddWithValue("@CampusId", strCampusId)

        dbCon.Open()
        Try
            'set the datareader with the data
            Dim reader = myCommand.ExecuteReader()

            'Load the Key\Value pair into the hash table
            Call BuildCollection(tmpAppSettings, reader)

            Call BuildCampusCollection()
        Catch ex As Exception

        Finally
            If dbCon.State = ConnectionState.Open Then dbCon.Close()
        End Try
    End Sub

    Public Sub GetCampusSettings(strCampusId As String, tblHash As Hashtable)
        Dim dbCon As New SqlConnection(connectionString)
        Dim myCommand As SqlCommand = New SqlCommand("sp_GetAppSettings", dbCon)
        myCommand.CommandType = CommandType.StoredProcedure
        myCommand.Parameters.AddWithValue("@CampusId", strCampusId)
        dbCon.Open()

        Try
            'set the datareader with the data
            Dim reader = myCommand.ExecuteReader

            'Load the Key\Value pair into the hash table
            Call BuildCollection(tblHash, reader)
        Catch ex As Exception

        Finally
            If dbCon.State = ConnectionState.Open Then dbCon.Close()
        End Try
    End Sub

    Public Sub BuildCollection(tblHash As Hashtable, reader As SqlDataReader)
        'Load the key/value pairs into the collection
        Dim strLastKey As String = ""

        'First, lets clear any values before inserting
        If tblHash.Count > 0 Then
            tblHash.Clear()
        End If

        'Load the new values
        'Note: We can only have one key/value pair per hashtable. Whenthis is initiated prior to login, there will be no campus
        'associated with the pairs, therefore, all values, regardlessof campus will be returned. In this case, we may get duplicate keys.
        'We needed to account for this, hence the GoTo. This will onlyoccur on the initial load.
        While reader.Read()
            If strLastKey = reader("KeyName").ToString Then
                GoTo SkipAdd
            End If

            tblHash.Add(reader("KeyName"), reader("Value"))

SkipAdd:
            strLastKey = reader("KeyName").ToString
        End While

        tblHash.Add("ConString", oleDbConnectionString)
        tblHash.Add("ConnectionString", connectionString)
        tblHash.Add("AdvantageConnectionString", connectionString)
    End Sub

    Public Function GetAllCampuses() As DataTable
        Dim dbCon As New SqlConnection(connectionString)
        Dim myCommand As SqlCommand = New SqlCommand("usp_GetAllCampuses", dbCon)
        myCommand.CommandType = CommandType.StoredProcedure

        Dim myDataSet As New DataSet
        Dim myDataAdapter As SqlDataAdapter
        dbCon.Open()

        Try
            'set the datareader with the data
            myDataAdapter = New SqlDataAdapter(myCommand)
            myDataAdapter.Fill(myDataSet)
            Return myDataSet.Tables(0)
        Finally
            If dbCon.State = ConnectionState.Open Then dbCon.Close()
        End Try
    End Function

    Public Sub BuildCampusCollection()
        Dim campusId As Guid

        Dim dtCampuses As DataTable = GetAllCampuses()
        Dim tblHash(dtCampuses.Rows.Count) As Hashtable

        dataD.Clear()
        For i = 0 To dtCampuses.Rows.Count - 1
            campusId = New Guid(dtCampuses.Rows(i)("CampusId").ToString)
            tblHash(i) = New Hashtable(StringComparer.OrdinalIgnoreCase)

            GetCampusSettings(campusId.ToString, tblHash(i))
            dataD.Add(campusId, tblHash(i))
        Next

    End Sub

    Private Function GetConnectionStringVariants() As ConnectionStringVariants
        Dim userId = Guid.Parse(HttpContext.Current.Session("TenantUserId").ToString())
        Dim tenantId = CInt(HttpContext.Current.Session("TenantNameId"))

        Return GetConnectionStringVariants(userId, tenantId)
    End Function

    Private Function GetConnectionStringVariants(userId As Guid, tenantID As Integer) As ConnectionStringVariants
        Dim service = ServiceLocator.Current.GetInstance(Of IMultiTenantService)()
        Return service.GetConnectionStringVariants(userId, tenantID)
    End Function
End Class