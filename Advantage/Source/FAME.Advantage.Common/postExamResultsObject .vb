﻿Imports System
Imports System.Collections.Generic

Namespace FAME.Advantage.Common
    Public Class postExamResultsObject 

        Private _clssectionId As String
        Public Overridable Property clssectionId As String
            Get
                Return Me._clssectionId
            End Get
            Set(ByVal value As String)
                Me._clssectionId = value
            End Set
        End Property

        Private _instrGrdBkWgtDetailId As String
        Public Overridable Property instrGrdBkWgtDetailId As String
            Get
                Return Me._instrGrdBkWgtDetailId
            End Get
            Set(ByVal value As String)
                Me._instrGrdBkWgtDetailId = value
            End Set
        End Property
        Private _dbIdName As String
        Public Overridable Property dbIdName As String
            Get
                Return Me._dbIdName
            End Get
            Set(ByVal value As String)
                Me._dbIdName = value
            End Set
        End Property
        Private _dbId As String
        Public Overridable Property dbId As String
            Get
                Return Me._dbId
            End Get
            Set(ByVal value As String)
                Me._dbId = value
            End Set
        End Property
        Private _componentDescription As String
        Public Overridable Property componentDescription As String
            Get
                Return Me._componentDescription
            End Get
            Set(ByVal value As String)
                Me._componentDescription = value
            End Set
        End Property

        
        Private _score As Double
        Public Overridable Property score As Double
            Get
                Return Me._score
            End Get
            Set(ByVal value As Double)
                Me._score = value
            End Set
        End Property
        
        Private _deleteScore As Boolean
        Public Overridable Property deleteScore As Boolean
            Get
                Return Me._deleteScore
            End Get
            Set(ByVal value As Boolean)
                Me._deleteScore = value
            End Set
        End Property

        Private _type As Integer
        Public Overridable Property type As Integer
            Get
                Return Me._type
            End Get
            Set(ByVal value As Integer)
                Me._type = value
            End Set
        End Property

        Private _resnum As Integer
        Public Overridable Property resnum As Integer
            Get
                Return Me._resnum
            End Get
            Set(ByVal value As Integer)
                Me._resnum = value
            End Set
        End Property

        Private _deleteDate As Boolean
        Public Overridable Property deleteDate As Boolean
            Get
                Return Me._deleteDate
            End Get
            Set(ByVal value As Boolean)
                Me._deleteDate = value
            End Set
        End Property
        Private _updateWithTodaysDate As Boolean
        Public Overridable Property updateWithTodaysDate As Boolean
            Get
                Return Me._updateWithTodaysDate
            End Get
            Set(ByVal value As Boolean)
                Me._updateWithTodaysDate = value
            End Set
        End Property

        Private _postDate As Nullable(Of DateTime)
        Public Overridable Property postDate As Nullable(Of DateTime)
            Get
                Return Me._postDate
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._postDate = value
            End Set
        End Property

        Private _originalPostDate As Nullable(Of DateTime)
        Public Overridable Property originalPostDate As Nullable(Of DateTime)
            Get
                Return Me._originalPostDate
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                Me._originalPostDate = value
            End Set
        End Property


        'Private _prefix As System.Nullable(Of System.Guid)
        'Public Overridable Property Prefix As System.Nullable(Of System.Guid)
        '    Get
        '        Return Me._prefix
        '    End Get
        '    Set(ByVal value As System.Nullable(Of System.Guid))
        '        Me._prefix = value
        '    End Set
        'End Property



    End Class
End Namespace


