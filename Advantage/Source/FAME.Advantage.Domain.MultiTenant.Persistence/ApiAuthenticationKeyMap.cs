﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.MultiTenant.Persistence
{
    public class ApiAuthenticationKeyMap : ClassMap<ApiAuthenticationKey>
    {
        public ApiAuthenticationKeyMap()
        {
            Id(x => x.ID).Column("Id").GeneratedBy.Identity();

            Map(x => x.Key).Column("`Key`").Not.Nullable();
            References(x => x.Tenant).Column("TenantId");
        }
    }
}
