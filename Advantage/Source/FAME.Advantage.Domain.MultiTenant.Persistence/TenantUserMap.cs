﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.MultiTenant.Persistence
{
    public class TenantUserMap : ClassMap<TenantUser>
    {
        public TenantUserMap()
        {
            Table("aspnet_Users");

            Id(x => x.ID).Column("UserID");
            Map(x => x.UserName).Not.Nullable().ReadOnly();

            HasMany(x => x.Tenants)
                .Access.CamelCaseField(Prefix.Underscore)
                .KeyColumn("UserID")
                .Not.KeyNullable()
                .Inverse()
                .Cascade.All()
                .Not.LazyLoad();
            ;
        }
    }
}
