﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.MultiTenant.Persistence
{
    /// <summary>
    /// Mapping table WAPITenantCompanySecret
    /// </summary>
    public class WapiTenantCompanySecretMap : ClassMap<WapiTenantCompanySecret>
    {
        public WapiTenantCompanySecretMap()
        {
            Table("WAPITenantCompanySecret");
            
            Id(x => x.ID).Column("Id").GeneratedBy.Identity();
            Map(x => x.ExternalCompanyCode).Column("ExternalCompanyCode").Not.Nullable();
            References(x => x.TenantObj).Column("TenantId").Not.Nullable();
        }
    }
}
