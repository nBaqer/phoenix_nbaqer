﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.MultiTenant.Persistence
{
    public class DataConnectionMap : ClassMap<DataConnection>
    {
        public DataConnectionMap()
        {
            Id(x => x.ID).Column("ConnectionStringId").GeneratedBy.Identity();
            
            Not.LazyLoad();

            Map(x => x.ServerName).Not.Nullable();
            Map(x => x.DatabaseName).Not.Nullable();
            Map(x => x.UserName).Not.Nullable();
            Map(x => x.Password).Not.Nullable();
        }
    }
}
