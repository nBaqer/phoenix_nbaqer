﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.MultiTenant.Persistence
{
    public class TenantMap : ClassMap<Tenant>
    {
        public TenantMap()
        {
            Id(x => x.ID).Column("TenantID").GeneratedBy.Identity();

            References(x => x.Environment).Column("EnvironmentID").Not.LazyLoad();
            HasMany(x => x.Keys).KeyColumn("TenantId").Not.LazyLoad();
            HasMany(x => x.WapiCompanySecretsList).KeyColumn("TenantId");

            Component(x => x.Connection, c => {
                c.Map(x => x.ServerName);
                c.Map(x => x.DatabaseName);
                c.Map(x => x.UserName);
                c.Map(x => x.Password);
            });

            Map(x => x.Name).Column("TenantName").Not.Nullable();
            Map(x => x.CreatedDate).Not.Nullable();
            Map(x => x.ModifiedDate).Not.Nullable();
            Map(x => x.ModifiedBy).Not.Nullable();
        }
    }
}
