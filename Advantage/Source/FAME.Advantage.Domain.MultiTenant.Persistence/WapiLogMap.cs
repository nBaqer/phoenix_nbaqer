﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.MultiTenant.Persistence
{
    public class WapiLogMap : ClassMap<WapiLog>
    {
        public WapiLogMap()
        {
            Table("WAPILog");
            
            Id(x => x.ID).Column("Id").GeneratedBy.Identity();
            Map(x => x.Comment).Column("Comment").Not.Nullable();
            Map(x => x.Company).Column("Company").Not.Nullable();
            Map(x => x.DateMod).Column("DateMod").Not.Nullable();
            Map(x => x.IsOk).Column("IsOk").Not.Nullable();
            Map(x => x.NumberOfRecords).Column("NumberOfRecords").Not.Nullable();
            Map(x => x.OperationDescription).Column("OperationDescription").Not.Nullable();
            Map(x => x.ServiceInvoqued).Column("ServiceInvoqued").Not.Nullable();
            Map(x => x.TenantName).Column("TenantName").Not.Nullable();
        }
    }
}
