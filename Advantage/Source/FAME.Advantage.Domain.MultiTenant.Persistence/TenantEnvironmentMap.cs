﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.MultiTenant.Persistence
{
    public class TenantEnvironmentMap : ClassMap<TenantEnvironment>
    {
        public TenantEnvironmentMap()
        {
            Table("Environment");

            Id(x => x.ID).Column("EnvironmentID").GeneratedBy.Identity();

            Map(x => x.Name).Column("EnvironmentName").Not.Nullable();
            Map(x => x.VersionNumber).Not.Nullable();
            Map(x => x.ApplicationURL).Not.Nullable();
        }
    }
}
