﻿using FAME.Advantage.Domain.Persistence.Infrastructure.NHibernate;

namespace FAME.Advantage.Domain.MultiTenant.Persistence
{
    public sealed class MultiTenantSessionFactoryConfig : BaseSessionFactoryConfig<MultiTenantSessionFactoryConfig>
    {
        private const string CONNECTION_STRING_KEY = "TenantAuthDBConnection";

        public MultiTenantSessionFactoryConfig()
            : base(CONNECTION_STRING_KEY, "web")
        {
        }
    }
}
