﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.MultiTenant.Persistence
{
    public class TenantUserMembershipMap : ClassMap<TenantUserMembership>
    {
        public TenantUserMembershipMap()
        {
            Table("TenantUsers");

            Id(x => x.ID).Column("TenantUserID").GeneratedBy.Identity();

            References(x => x.Tenant).Column("TenantID").Not.Nullable().Cascade.None();
            References(x => x.User).Column("UserID").Not.Nullable().Cascade.None();

            Map(x => x.IsDefaultTenant);
            Map(x => x.IsSupportUser);
        }
    }
}
