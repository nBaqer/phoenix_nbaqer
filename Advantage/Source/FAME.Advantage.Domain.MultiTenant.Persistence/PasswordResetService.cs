﻿using System;
using NHibernate;

namespace FAME.Advantage.Domain.MultiTenant.Persistence
{
    public class PasswordResetService : IPasswordResetService
    {
        private static class StoredProcedure
        {
            public const string RESET_PASSWORD_DATES = "exec dbo.USP_PasswordDate_Update @UserId = :UserId";
        }

        private readonly ISessionFactory _sessionFactory;

        public PasswordResetService(ISessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;
        }

        public void ResetPasswordDates(Guid userId)
        {
            using (var session = _sessionFactory.OpenStatelessSession()) {
                session.CreateSQLQuery(StoredProcedure.RESET_PASSWORD_DATES)
                    .SetGuid("UserId", userId)
                    .ExecuteUpdate();
            }
        }
    }
}
