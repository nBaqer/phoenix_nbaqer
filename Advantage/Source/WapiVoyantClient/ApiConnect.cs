﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;

namespace WapiVoyantClient
{
    /// <summary>
    /// ApiConnect has the necessary fields and methods to get connected to the https://api.voyantanalytics.com/
    /// </summary>
    public class ApiConnect : IApiConnect
    {
        /// <summary>
        /// API_KEY, to be provided by the user
        /// </summary>
        public string ApiKey { get; set; } // = "D8JkdNktu5";

        /// <summary>
        /// API_SECRET, to be provided by the user
        /// </summary>
        public string ApiSecret { get; set; } //= "4C97722E8EA92";

        /// <summary>
        /// API_PATH, The path is normally https://api.voyantanalytics.com/
        /// </summary>
        public string ApiPath { get; set; } //"https://devapi.voyantanalytics.com/";


        public static IApiConnect Factory(string apikey, string apisecret, string apipath)
        {

            IApiConnect voyant = new ApiConnect();
            voyant.ApiKey = apikey;
            voyant.ApiPath = apipath;
            voyant.ApiSecret = apisecret;
            return voyant;
        }
   

        /// <summary>
        /// create querystring embedded with consumer_key, nonce, timestamp
        /// </summary>
        /// <returns>
        /// querystring e.g consumer_key=IDIJEIJ& nonce=iaosjdiofweuiodjoa& timestamp=2342342
        /// </returns>
        public string CreateParameters()
        {

            string queryString = "";

            // extract timespan and nonce from the dictionary
            Dictionary<string, string> dictIn = CreateInputs();

            string timespan = dictIn["TimeSpan"];
            string nonce = dictIn["UniqueId"];

            var dict = new Dictionary<string, string>
            {
                {"consumer_key", ApiKey},
                {"nonce", nonce},
                {"timestamp", timespan}
            };

            // loop the dictionary to write querystring
            foreach (KeyValuePair<string, string> entry in dict)
            {
                queryString += entry.Key + "=" + entry.Value + "&";
            }
            queryString = queryString.Trim('&');

            return queryString;
        }

        /// <summary>
        /// generate signature
        /// </summary>
        /// <param name="url">
        /// e.g. https://api.voyantanalytics.com/model/52b41e9fe4b0ad00ae9818e2
        /// </param>
        /// <param name="restType">e.g. GET/POST</param>
        /// <param name="parameters">
        // ReSharper disable CSharpWarnings::CS1570
        /// e.g. consumer_key=D8JkdNktu5&nonce=G974TVGTU7TLDM0E5LYN1AVTQAK0W8U1JGGT&timestamp=1387572592</param>
        // ReSharper restore CSharpWarnings::CS1570
        /// <returns></returns>
        public  string GetSig(string url, HttpVerb restType, string parameters)
        {

            string path = ApiPath + url;

            string raw = restType + "&" + Uri.EscapeDataString(path) + "&" + Uri.EscapeDataString(parameters);

            byte[] toSignBytes = Encoding.UTF8.GetBytes(raw);
            byte[] secretBytes = Encoding.UTF8.GetBytes(ApiSecret);
            var signer = new HMACSHA1(secretBytes);
            byte[] sigBytes = signer.ComputeHash(toSignBytes);

            string sig = Convert.ToBase64String(sigBytes);
            return sig;
        }

        /// <summary>
        /// Sends requests
        /// </summary>
        /// <param name="url">e.g. data, model, data/org/</param>
        /// <param name="restType">e.g. HttpVerb.POST, HttpVerb.GET, HttpVerb.PUT and HttpVerb.DELETE</param>
        /// <param name="sig">signature</param>
        /// <param name="queryString">
        // ReSharper disable CSharpWarnings::CS1570
        /// e.g. ?consumer_key=sfsdfsdf&nonce=KDFJIDJIE3&timestamp=234234&signature=jioijsodfw343=</param>
        // ReSharper restore CSharpWarnings::CS1570
        /// <returns>JSON</returns>
        public string GetRequest(string url, HttpVerb restType, string sig, string queryString)
        {
            try
            {

                var connect = new Connect(ApiPath + url);
                string parameters = "?" + queryString + "&signature=" + sig;
                string json = connect.MakeRequest(parameters);
                return json;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        /// <summary>
        /// Posts requests
        /// </summary>
        /// <param name="url">e.g data, model, data/org/</param>
        /// <param name="sig">signature</param>
        /// <param name="queryString">
        // ReSharper disable CSharpWarnings::CS1570
        /// e.g. ?consumer_key=sfsdfsdf&nonce=KDFJIDJIE3&timestamp=234234&signature=jioijsodfw343=</param>
        // ReSharper restore CSharpWarnings::CS1570
        /// <param name="payload">e.g{"A":1,"B":2} in JSON format</param>
        /// <returns>Response from the server</returns>
        public string PostRequest(string url, string sig, string queryString, string payload)
        {
            try
            {
                string parameters = queryString + "&signature=" + sig;
                var connect = new Connect(endPoint: ApiPath + url, method: HttpVerb.POST, postData: payload);
                string req = connect.MakeRequest(parameters);
                return req;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

      

        /// <summary>
        /// Creates necessary inputs for the querysting: nonce and timestamp generated through this method.
        /// </summary>
        /// <returns>Dictionary of TimeSpan and the Unique ID keys: TimeSpan and UniqueId</returns>
        public static Dictionary<string, string> CreateInputs()
        {
            // Random alpha numeric ID generator of size 36
             var id = DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture);
            //string id =   Utility.RandomString(36);

            TimeSpan timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

            // time span calcuated from 1970 Jan 1
            string timeStamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString(CultureInfo.InvariantCulture);

            var dict = new Dictionary<string, string> { { "TimeSpan", timeStamp }, { "UniqueId", id } };

            // TimeSpan and UniqueId added to the Dictionary

            return dict;
        }
    }
}
