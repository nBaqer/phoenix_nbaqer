﻿using System;
using System.Text;
using System.IO;
using System.Net;

/// <summary>
/// Verb for REST operations
/// </summary>
public enum HttpVerb
{
    // ReSharper disable InconsistentNaming
    GET,
    POST,
    PUT,
    DELETE
    // ReSharper restore InconsistentNaming
}

namespace WapiVoyantClient
{
    /// <summary>
    /// This class connects to the remote server to perform GET, POST, PUT and DELETE operations remotely.
    /// </summary>
    public class Connect
    {
        public string EndPoint { get; set; }
        public HttpVerb Method { get; set; }
        public string ContentType { get; set; }
        public string PostData { get; set; }

        public Connect()
        {
            EndPoint = "";
            Method = HttpVerb.GET;
            ContentType = "application/json";
            PostData = "";
        }

        public Connect(string endPoint)
        {
            EndPoint = endPoint;
            Method = HttpVerb.GET;
            ContentType = "application/json";
            PostData = "";
        }

        public Connect(string endPoint, HttpVerb method)
        {
            EndPoint = endPoint;
            Method = method;
            ContentType = "application/json";
            PostData = "";
        }

        public Connect(string endPoint, HttpVerb method, string postData)
        {
            EndPoint = endPoint;
            Method = method;
            ContentType = "application/json";
            PostData = postData;
        }

        public string MakeRequest()
        {
            return MakeRequest("");
        }

        /// <summary>
        /// This method performs the operation as per the user parameters and posted data for request and response.
        /// </summary>
        // ReSharper disable CSharpWarnings::CS1570
        /// <param name="parameters">QueryString e.g. ?consumer_key=sfsdfsdf&nonce=KDFJIDJIE3&timestamp=234234&signature=jioijsodfw343=</param>
        // ReSharper restore CSharpWarnings::CS1570
        /// <returns>Response from the server</returns>
        public string MakeRequest(string parameters)
        {
            var request = (HttpWebRequest)WebRequest.Create(EndPoint + parameters);

            request.Method = Method.ToString();
            request.ContentLength = 0;
            request.ContentType = ContentType;

            try
            {
                // Request
                if (!string.IsNullOrEmpty(PostData) && Method == HttpVerb.POST)
                {
                    //var encoding = new UTF8Encoding();
                    var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(PostData);
                    request.ContentLength = bytes.Length;

                    using (var writeStream = request.GetRequestStream())
                    {
                        writeStream.Write(bytes, 0, bytes.Length);
                    }
                }

                // Response
                var res = request.GetResponse();
                var response = (HttpWebResponse)res;

                var responseValue = string.Empty;

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    var message = String.Format("Request failed. Received HTTP {0}", response.StatusCode);
                    throw new ApplicationException(message);
                }

                using (var responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                        }
                    }
                }
                return responseValue;

            }
            catch (WebException ex)
            {
                using (WebResponse response = ex.Response)
                {
                    var httpResponse = (HttpWebResponse)response;
                    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    {
                        string text = string.Empty;
                        if (data != null)
                            using (var reader = new StreamReader(data))
                            {
                                text = reader.ReadToEnd();
                                Console.WriteLine(text);
                            }
                        throw new ApplicationException(string.Format("ERROR: Returned Code: {0} - Reason: {1} - Result: {2} - Request Address: {3}",
                            httpResponse.StatusCode, httpResponse.StatusDescription, text, request.Address));
                    }
                }

                
            }

            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
    }
}
