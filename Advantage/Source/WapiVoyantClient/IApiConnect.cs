namespace WapiVoyantClient
{
    public interface IApiConnect
    {
        /// <summary>
        /// API_KEY, to be provided by the user
        /// </summary>
        string ApiKey { get; set; }

        /// <summary>
        /// API_SECRET, to be provided by the user
        /// </summary>
        string ApiSecret { get; set; }

        /// <summary>
        /// API_PATH, The path is normally https://api.voyantanalytics.com/
        /// </summary>
        string ApiPath { get; set; }

        /// <summary>
        /// create querystring embedded with consumer_key, nonce, timestamp
        /// </summary>
        /// <returns>
        // ReSharper disable CSharpWarnings::CS1570
        /// querystring e.g consumer_key=IDIJEIJ&nonce=iaosjdiofweuiodjoa&timestamp=2342342
        // ReSharper restore CSharpWarnings::CS1570
        /// </returns>
        string CreateParameters();

        /// <summary>
        /// gerenate signature
        /// </summary>
        /// <param name="url">
        /// e.g. https://api.voyantanalytics.com/model/52b41e9fe4b0ad00ae9818e2
        /// </param>
        /// <param name="restType">e.g. GET/POST</param>
        /// <param name="parameters">
        // ReSharper disable CSharpWarnings::CS1570
        /// e.g. consumer_key=D8JkdNktu5&nonce=G974TVGTU7TLDM0E5LYN1AVTQAK0W8U1JGGT&timestamp=1387572592</param>
        // ReSharper restore CSharpWarnings::CS1570
        /// <returns></returns>
        string GetSig(string url, HttpVerb restType, string parameters);

        /// <summary>
        /// Sends requests
        /// </summary>
        /// <param name="url">e.g. data, model, data/org/</param>
        /// <param name="restType">e.g. HttpVerb.POST, HttpVerb.GET, HttpVerb.PUT and HttpVerb.DELETE</param>
        /// <param name="sig">signature</param>
        /// <param name="queryString">
        // ReSharper disable CSharpWarnings::CS1570
        /// e.g. ?consumer_key=sfsdfsdf&nonce=KDFJIDJIE3&timestamp=234234&signature=jioijsodfw343=</param>
        // ReSharper restore CSharpWarnings::CS1570
        /// <returns>JSON</returns>
        string GetRequest(string url, HttpVerb restType, string sig, string queryString);

        /// <summary>
        /// Posts requests
        /// </summary>
        /// <param name="url">e.g data, model, data/org/</param>
        /// <param name="sig">signature</param>
        /// <param name="queryString">
        // ReSharper disable CSharpWarnings::CS1570
        /// e.g. ?consumer_key=sfsdfsdf&nonce=KDFJIDJIE3&timestamp=234234&signature=jioijsodfw343=</param>
        // ReSharper restore CSharpWarnings::CS1570
        /// <param name="payload">e.g{"A":1,"B":2} in JSON format</param>
        /// <returns>Response from the server</returns>
        string PostRequest(string url, string sig, string queryString, string payload);
    }
}