﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Advantage.Data.ORM
Imports Advantage.Business.Objects
Imports Advantage.Business.Logic.Layer.Assemblers
Imports System.Web
Imports System.Security.Cryptography
Imports System.Security

Namespace Advantage.Business.Logic.Layer

    Public Class UserStateDataProvider
        Inherits IDataProvider

        Private _connString As String

        Public Sub New(ByVal context As HttpContext, ByVal connectionString As String)
            MyBase.New(context, connectionString)
            _connString = connectionString
        End Sub

        'Private Function getUserDetailsFromDatabase(ByVal AdvantageUser As User) As List(Of User)
        '    Dim query As IQueryable(Of SyUser) = dbContext.SyUsers

        '    If (Not String.IsNullOrEmpty(AdvantageUser.UserName) AndAlso Not String.IsNullOrEmpty(AdvantageUser.Password)) Then
        '        If AdvantageUser.UserName.ToLower.Trim = "super" Or AdvantageUser.UserName.ToLower.Trim = "support" Then 'Pass in encrypted password
        '            Dim strPassword As String
        '            If AdvantageUser.UserName.ToLower.Trim = "super" Or AdvantageUser.UserName.ToLower.Trim = "support" Then
        '                strPassword = Encrypt(AdvantageUser.Password).ToLower.Trim
        '                query = query.Where(Function(s) s.UserName.ToLower.Trim.Equals(AdvantageUser.UserName.ToLower.Trim) And s.Password.ToLower.Trim.Equals(strPassword)) '.FirstOrDefault()
        '            Else
        '                strPassword = (AdvantageUser.Password).ToLower.Trim
        '                query = query.Where(Function(s) s.UserName.ToLower.Trim.Equals(AdvantageUser.UserName.ToLower.Trim) And s.Password.ToLower.Trim.Equals(strPassword)) '.FirstOrDefault()
        '            End If

        '        Else
        '            query = query.Where(Function(s) s.UserName.ToLower.Trim.Equals(AdvantageUser.UserName.ToLower.Trim) And s.Password.ToLower.Trim.Equals(AdvantageUser.Password.ToLower.Trim)) '.FirstOrDefault()
        '        End If
        '    End If

        '    Dim result As List(Of User) = (New UserStateAssembler(_connString)).AssembleContracts(query)
        '    Return result
        'End Function

        Private Function getUserDetailsByUserIdFromDatabase(ByVal UserId As String) As List(Of User)
            Dim query As IQueryable(Of SyUser) = dbContext.SyUsers
            If (Not String.IsNullOrEmpty(UserId)) Then
                query = query.Where(Function(s) s.UserId.ToString.ToLower.Trim.Equals(UserId.ToString.ToLower.Trim))
            End If

            Dim result As List(Of User) = (New UserStateAssembler(_connString)).AssembleContracts(query)
            Return result
        End Function

        Private Function IsUserActive(ByVal advantageUser As User) As Integer
            Try
                Select Case advantageUser.AccountActive
                    Case Is = True
                        'DE7560 - if no roles assigned the will not allowed to login
                        'Return BO.UserStatus.Success
                        If advantageUser.UserRolesCampusGroups.Count = 0 Then
                            Return UserStatus.Fail_InsufficientRoles

                        ElseIf advantageUser.CampusId.ToString() = Guid.Empty.ToString() Then

                            Return UserStatus.Fail_InactiveCampus
                        Else
                            Return UserStatus.Success
                        End If
                    Case Is = False
                        If IsUserSpecial(advantageUser.UserName) Then Return UserStatus.Success
                        If Not IsUserSpecial(advantageUser.UserName) Then Return UserStatus.Fail_InactiveUser
                    Case Else
                        Return UserStatus.Fail_InvalidCredentials
                End Select
                Return UserStatus.Fail_InvalidCredentials
            Catch ex As Exception
                Return UserStatus.Fail_InvalidCredentials
            End Try
        End Function

        Private Function IsUserSpecial(ByVal UserName As String) As Boolean

            Select Case UserName.ToString.ToLower.Trim
                Case Is = "sa", "support", "super"
                    Return True
                Case Else
                    Return False
            End Select

        End Function

        Private Function IsSpecialUserHavingFullAccess(ByRef AdvantageUserState As User) As User
            Select Case IsUserSpecial(AdvantageUserState.UserName.ToLower)
                Case Is = True
                    AdvantageUserState.FullPermission = True
                Case Else
                    AdvantageUserState.FullPermission = False
            End Select
            Return AdvantageUserState
        End Function

        Private Function getCampusesFromUserObject(ByRef AdvantageUserState As User) As List(Of Campus)
            Dim getCampuses As New List(Of Campus)
            For Each item In AdvantageUserState.UserRolesCampusGroups.OrderBy(Function(s) New Guid(s.CampGrpId.ToString))
                For Each iterateCampus In item.UserCampuses.OrderBy(Function(s) s.CampusId)
                    Dim CampusId As String = iterateCampus.CampusId.ToString
                    Dim boolCampusAlreadyExists As Boolean = False

                    'Check whether the Role - Resource - Campus Combination already exists. The Role-Resource-Campus combination needs to be unique.
                    If getCampuses.Count >= 1 Then
                        Dim collectionItem As IEnumerable(Of Campus) = getCampuses.Where(Function(s) s.CampusId.ToString.Equals(CampusId)).ToList()
                        If collectionItem.Count >= 1 Then boolCampusAlreadyExists = True
                    End If
                    If boolCampusAlreadyExists = False Then
                        Dim dtObj As Object = dbContext.UspCampusdescriptionCampusid(iterateCampus.CampusId.ToString)
                        For Each CampusItem In dtObj
                            getCampuses.Add(New Campus() With { _
                                                   .CampusId = CampusItem(0), .CampCode = CampusItem(1), .CampDescrip = CampusItem(2), _
                                                   .Address1 = CampusItem(3), .Address2 = CampusItem(4), .City = CampusItem(5), _
                                                   .Phone1 = CampusItem(6), .Phone2 = CampusItem(7), .Phone3 = CampusItem(8), _
                                                   .Fax = CampusItem(9), .Email = CampusItem(10), .Website = CampusItem(11), _
                                                   .IsCorporate = CampusItem(12), .TranscriptAuthZnTitle = CampusItem(13), .TranscriptAuthZnName = CampusItem(14), _
                                                   .TCSourcePath = CampusItem(15), .IsRemoteServer = CampusItem(16), .PortalContactEmail = CampusItem(17), _
                                                   .RemoteServerUsrNm = CampusItem(18), .RemoteServerPwd = CampusItem(19), .SourceFolderLoc = CampusItem(20), _
                                                   .TargetFolderLoc = CampusItem(21), .UseCampusAddress = CampusItem(22), .InvAddress1 = CampusItem(23), _
                                                   .InvAddress2 = CampusItem(24), .InvCity = CampusItem(25), .InvStateID = CampusItem(26), _
                                                   .InvStateDescription = CampusItem(27), .InvZip = CampusItem(28), .InvCountryID = CampusItem(29), _
                                                   .InvPhone1 = CampusItem(30), .InvFax = CampusItem(31), .FLSourcePath = CampusItem(32), _
                                                   .FLTargetPath = CampusItem(33), .FLExceptionPath = CampusItem(34), .IsRemoteServerFL = CampusItem(35), _
                                                   .RemoteServerUsrNmFL = CampusItem(36), .RemoteServerPwdFL = CampusItem(37), .SourceFolderLocFL = CampusItem(38), _
                                                   .TargetFolderLocFL = CampusItem(39), .StateId = CampusItem(40), .StateDescription = CampusItem(41), _
                                                   .Zip = CampusItem(42), .CountryId = CampusItem(43), .CountryDescription = CampusItem(44), _
                                                   .StatusId = CampusItem(45), .StatusDescription = CampusItem(46)})
                        Next CampusItem
                    End If
                Next iterateCampus
            Next item


            If getCampuses.Count >= 1 Then getCampuses = getCampuses.FindAll(Function(s) s.StatusDescription.ToLower.ToString = "active")


            Return getCampuses
        End Function
        'Private Function setPageLevelPermissionForRegularUser(ByRef AdvantageUserState As User) As List(Of PageLevelPermission)
        '    Dim boolRoleAlreadyExists As Boolean = False
        '    Dim boolCampGrpAlreadyExists As Boolean = False
        '    Dim strPreviousRoleId As String = ""
        '    Dim strPreviousCampGrpId As String = ""
        '    Dim PreviousResourceId As Short
        '    Dim getUserRoles As New List(Of PageLevelPermission)
        '    For Each item In AdvantageUserState.UserRolesCampusGroups.OrderBy(Function(s) New Guid(s.RoleId.ToString))
        '        'boolRoleAlreadyExists = IIf(item.RoleId.Value.ToString.Equals(strPreviousRoleId.ToString), True, False) 'Needed as we want to store unique resourceids in the collection
        '        'boolCampGrpAlreadyExists = IIf(item.CampGrpId.Value.ToString.Equals(strPreviousCampGrpId.ToString), True, False) 'Needed as we want to store unique resourceids in the collection
        '        'Step 3: The user's roleid is known at this point and we need to access the pages the user role has access to and also the access level. In other words the
        '        'permission the user has on every single page in Advantage. Due to the large dataset, we need to Sort the collection first by ResourceId and then by 
        '        'AccessLevel in descending order (Key is to start with higher access level)
        '        For Each iterateCampus In item.UserCampuses.OrderBy(Function(s) s.CampusId)
        '            Try
        '                If item.UserRoles(0) Is Nothing Then
        '                    Exit Try
        '                End If
        '                For Each iterateAccessLevel In item.UserRoles.OrderBy(Function(s) s.ResourceID).ThenByDescending(Function(s) s.AccessLevel)  'Second For Loop equivalent to Order by ResourceId Asc, AccessLevel Desc
        '                    Dim addPageLevelAccess As New PageLevelPermission() With _
        '                             { _
        '                                .ResourceID = iterateAccessLevel.ResourceID, _
        '                                .AccessLevel = iterateAccessLevel.AccessLevel, _
        '                                .CampusId = iterateCampus.CampusId, _
        '                                .RoleId = item.RoleId}

        '                    'Comment added by Balaji on 05/25/2011
        '                    'A user may have two or more roles.If the first role has full access to page
        '                    'then we need to set full access property to true. There is a possiblility that second 
        '                    'role may have read only access and in that scenario, the second role should not
        '                    'override the access set by first role. Thats the reason we have Exit For Statement
        '                    'in the preceding code 
        '                    Dim ResourceId As String = iterateAccessLevel.ResourceID.ToString  'Step neccessary due to a warning that use of iteration variable inside a lambda expression may create problem
        '                    Dim CampusId As String = iterateCampus.CampusId.ToString
        '                    Dim RoleId As String = item.RoleId.ToString
        '                    Dim RoleResourceCampusCombinationExists As Boolean = False

        '                    'The following validation is not needed as it will stop the roles from getting in and also
        '                    'if another developer wants to use the object for other purpose they may not be able to use it.
        '                    'The goal is to develop a user object with all bells and whistles, not to limit the scope

        '                    'Commented by Balaji on May 27 2011
        '                    'Check whether the Role - Resource - Campus Combination already exists. The Role-Resource-Campus combination needs to be unique.
        '                    If getUserRoles.Count >= 1 Then
        '                        Dim collectionItem As IEnumerable(Of PageLevelPermission) = getUserRoles.Where(Function(s) s.RoleId.ToString.Equals(RoleId) And s.ResourceID.ToString.Equals(ResourceId) And s.CampusId.ToString.Equals(CampusId)).ToList()
        '                        If collectionItem.Count >= 1 Then RoleResourceCampusCombinationExists = True
        '                    End If
        '                    If ((Not iterateAccessLevel.ResourceID = PreviousResourceId) And (RoleResourceCampusCombinationExists = False)) Then
        '                        setPageLevelPermission(iterateAccessLevel.AccessLevel, addPageLevelAccess)
        '                        getUserRoles.Add(addPageLevelAccess)
        '                    End If
        '                    PreviousResourceId = iterateAccessLevel.ResourceID
        '                Next iterateAccessLevel
        '            Catch ex As Exception

        '            End Try

        '        Next iterateCampus
        '        'End If
        '        strPreviousRoleId = item.RoleId.ToString  'Keep track of the last roleid 
        '        strPreviousCampGrpId = item.CampGrpId.ToString 'Keep track of the last campgrpid
        '    Next item
        '    Return getUserRoles
        'End Function

        'Private Function setPageLevelPermission(ByVal AccessLevel As Integer, ByRef PageLevelAccess As PageLevelPermission) As PageLevelPermission
        '    Select Case AccessLevel
        '        Case Is = 15 'Full Access
        '            PageLevelAccess.PageLevelFullAccess = True
        '            PageLevelAccess.PageLevelEditAccess = True
        '            PageLevelAccess.PageLevelCreateAccess = True
        '            PageLevelAccess.PageLevelDeleteAccess = True
        '        Case Is = 14 'Edit, Create, Delete
        '            PageLevelAccess.PageLevelEditAccess = True
        '            PageLevelAccess.PageLevelCreateAccess = True
        '            PageLevelAccess.PageLevelDeleteAccess = True
        '        Case Is = 13 'Edit, Create, ReadOnly
        '            PageLevelAccess.PageLevelEditAccess = True
        '            PageLevelAccess.PageLevelCreateAccess = True
        '            PageLevelAccess.PageLevelReadOnlyAccess = True
        '        Case Is = 12 'Edit, Create
        '            PageLevelAccess.PageLevelEditAccess = True
        '            PageLevelAccess.PageLevelCreateAccess = True
        '            ''Added by Sara on March 7th 2012 for Edit and Delete
        '        Case Is = 10
        '            PageLevelAccess.PageLevelEditAccess = True
        '            PageLevelAccess.PageLevelDeleteAccess = True
        '        Case Is = 8 'Edit
        '            PageLevelAccess.PageLevelEditAccess = True
        '            ''Added by Sara on March 7th 2012 for Add and Delete
        '        Case Is = 6 'Add and Delete
        '            PageLevelAccess.PageLevelCreateAccess = True
        '            PageLevelAccess.PageLevelDeleteAccess = True
        '        Case Is = 4 'Create
        '            PageLevelAccess.PageLevelCreateAccess = True
        '        Case Is = 2 'Delete
        '            PageLevelAccess.PageLevelDeleteAccess = True
        '        Case Is = 1 'Read Only
        '            PageLevelAccess.PageLevelReadOnlyAccess = True
        '        Case Else
        '            'US3141 
        '            'PageLevelAccess.PageLevelReadOnlyAccess = True
        '            PageLevelAccess.PageLevelReadOnlyAccess = False
        '    End Select
        '    Return PageLevelAccess
        'End Function

        Private Function setLoginFlag(ByRef AdvantageUserState As User) As User
            Select Case IsUserActive(AdvantageUserState)
                Case Is = UserStatus.Success
                    With AdvantageUserState
                        If .ShowDefaultCampus = True Then
                            '.RedirectURL = "SY/defaultcampus.aspx?resid=263&mod=" + .ModuleCode + "&cmpid=" + .CampusId.ToString
                            '.RedirectURL = "studentinfo.aspx?resid=170&mod=" + .ModuleCode + "&cmpid=" + .CampusId.ToString
                            .RedirectURL = "dash.aspx?resid=264&mod=" + .ModuleCode + "&cmpid=" + .CampusId.ToString + "&desc=dashboard"
                        Else
                            '.RedirectURL = "SY/homepage.aspx?resid=264&mod=" + .ModuleCode + "&cmpid=" + .CampusId.ToString
                            '.RedirectURL = "SY/homepage.aspx?resid=170&mod=" + .ModuleCode + "&cmpid=" + .CampusId.ToString
                            '.RedirectURL = "studentinfo.aspx?resid=170&mod=" + .ModuleCode + "&cmpid=" + .CampusId.ToString
                            .RedirectURL = "dash.aspx?resid=264&mod=" + .ModuleCode + "&cmpid=" + .CampusId.ToString + "&desc=dashboard"
                        End If
                        .ReturnMessage = "Successful login"
                        .IsLoginSuccessful = True
                    End With
                Case Is = UserStatus.Fail_InvalidCredentials
                    With AdvantageUserState
                        .RedirectURL = ""
                        .ReturnMessage = "Incorrect username or password"
                        .IsLoginSuccessful = False
                    End With
                Case Is = UserStatus.Fail_InactiveUser
                    With AdvantageUserState
                        .RedirectURL = ""
                        .ReturnMessage = "The account that you entered is not active - please contact the system administrator."
                        .IsLoginSuccessful = False
                    End With
                Case Is = UserStatus.Fail_InsufficientRoles
                    With AdvantageUserState
                        .RedirectURL = ""
                        .ReturnMessage = "The user currently has no access to an active campus- please contact system administrator."
                        .IsLoginSuccessful = False
                    End With
                    ''Added to verify if the logged in user has rights to any active campus
                    ''The user should have rights to any one of the active campuses
                    ''DE 7669
                Case Is = UserStatus.Fail_InactiveCampus
                    With AdvantageUserState
                        .RedirectURL = ""
                        .ReturnMessage = "The user currently has no access to an active campus- please contact system administrator."
                        .IsLoginSuccessful = False
                    End With
                Case Else
                    With AdvantageUserState
                        .RedirectURL = ""
                        .ReturnMessage = "Incorrect username or password"
                    End With
            End Select
            Return AdvantageUserState
        End Function
        Private Function getResourceCampusPermission(ByVal UserId As String) As List(Of PageLevelPermission)
            Dim dtObj As Object = dbContext.USPGetResourceCampusPermission(New Guid(UserId))
            Dim lstPageLevelPermissions As New List(Of PageLevelPermission)
            For Each item In dtObj
                Dim addPageLevelAccess As New PageLevelPermission() With _
                                  { _
                                     .ResourceID = item(0), _
                                     .CampusId = item(1), _
                                     .AccessLevel = item(2), _
                                     .RoleId = Guid.Empty, _
                                     .PageLevelFullAccess = item(3), _
                                     .PageLevelEditAccess = item(4), _
                                     .PageLevelCreateAccess = item(5), _
                                     .PageLevelDeleteAccess = item(6), _
                                     .PageLevelReadOnlyAccess = item(7)}
                lstPageLevelPermissions.Add(addPageLevelAccess)
            Next
            Return lstPageLevelPermissions
        End Function

        Private Function getAssignedRoles(ByVal UserId As String) As List(Of AssignedRoles)
            Dim dtObj As Object = dbContext.USPGetGetRolesByUser(New Guid(UserId))
            Dim lstRoles As New List(Of AssignedRoles)
            For Each item In dtObj
                Dim addRoles As New AssignedRoles() With _
                                  { _
                                     .UserId = item(0), _
                                     .UserName = item(1), _
                                     .RoleId = item(2), _
                                     .Role = item(3)}

                lstRoles.Add(addRoles)
            Next
            Return lstRoles
        End Function

        Public Function getUserLoginState(ByVal AdvantageUser As User) As User
            Dim objAdvantageUserState As New User
            'Try
            Dim list = getUserDetailsByUserIdFromDatabase(AdvantageUser.UserId.ToString)
            If list.Count > 0 Then


                objAdvantageUserState = getUserDetailsByUserIdFromDatabase(AdvantageUser.UserId.ToString).Item(0)
                'Comments added by Balaji on May 31st 2011

                'Get All Campuses user has access to
                objAdvantageUserState.UserCampuses = getCampusesFromUserObject(objAdvantageUserState)

                'if the user is is support set the property in user state object
                If objAdvantageUserState.UserName.ToUpper() = "SUPPORT" Then
                    objAdvantageUserState.IsUserSupport = True
                End If

                'get the assigned roles for the user. If the user has a system administrator 
                'role, set the property isUserSA
                Dim assignedRoles = getAssignedRoles(objAdvantageUserState.UserId.ToString)
                For Each ar As AssignedRoles In assignedRoles
                    If ar.Role.ToUpper() = "SYSTEM ADMINISTRATOR" Then
                        objAdvantageUserState.IsUserSA = True
                        Exit For
                    End If
                Next

                ' If user is a special user no need to check permission, as user has full access 
                If IsSpecialUserHavingFullAccess(objAdvantageUserState).FullPermission = False Then

                    'If user is not a special user, then get the page level/campus level/role level permission for each page
                    objAdvantageUserState.UserRoles = getResourceCampusPermission(objAdvantageUserState.UserId.ToString)
                End If
            Else
                objAdvantageUserState.AccountActive = False
                Throw New SecurityException("User is not correctly registered")
            End If
            'Catch ex As Exception
            '    objAdvantageUserState.AccountActive = False 'Set Account Active to False, when user does not exist in the database
            'End Try
            ''Added to verify if the logged in user has rights to any active campus
            ''The user should have rights to any one of the active campuses
            ''DE 7669


            If objAdvantageUserState.AccountActive = True Or objAdvantageUserState.IsUserSuper = True Or objAdvantageUserState.IsUserSA = True Or objAdvantageUserState.IsUserSupport = True Then
                objAdvantageUserState.CampusId = GetDefaultCampusForGivenUser(objAdvantageUserState.UserId.ToString)
            Else
                objAdvantageUserState.CampusId = Guid.Empty
            End If

            'Set the Login Success/Failure Flag and also set the target page
            objAdvantageUserState = setLoginFlag(objAdvantageUserState)
            Return objAdvantageUserState
        End Function


        Public Function GetDefaultCampusForGivenUser(ByVal userId As String) As Guid
            Dim dtCampusObject As Object = dbContext.UspGetDefaultCampusforaGivenUser(New Guid(userId))
            Dim strCampusId As String = ""
            For Each item In dtCampusObject
                If Not item(0) Is Nothing Then
                    strCampusId = item(0).ToString
                End If
            Next
            If strCampusId = "" Then
                Return Guid.Empty
            Else
                Dim syusertoUpdate As SyUser = dbContext.SyUsers.Where(Function(c) c.UserId = New Guid(userId)).FirstOrDefault()
                syusertoUpdate.CampusId = New Guid(strCampusId)
                dbContext.SaveChanges()

                Return (New Guid(strCampusId))
            End If
        End Function

        Public Function getResources(ByVal ResourceId As Nullable(Of Int32)) As List(Of Resources)
            Dim query As IQueryable(Of SyResource) = dbContext.SyResources
            query = query.Where(Function(s) s.ResourceTypeID = 3)
            Dim objPageState As List(Of Resources) = (New NavigationStateAssembler).AssembleContracts(query)
            Return objPageState
        End Function
        Public Function GetNavigation() As Object
            Dim query As Object = dbContext.UspAdmissionsCommonNavigations
            Return query
        End Function
        Public Function GetCommonTaskNavigation(ByVal intSchoolOptions As Integer) As Object
            Dim query As Object = dbContext.UspAdvantageCommonTaskNavigationGetList(intSchoolOptions)
            Return query
        End Function
        Public Function GetReportsNavigation(ByVal intSchoolOptions As Integer) As Object
            Dim query As Object = dbContext.UspAdvantageReportsNavigationGetList(intSchoolOptions)
            Return query
        End Function
        Public Function GetMaintenanceNavigation(ByVal intSchoolOptions As Integer) As Object
            Dim query As Object = dbContext.UspAdvantageMaintenanceNavigationGetList(intSchoolOptions)
            Return query
        End Function
        Private Function Encrypt(ByVal myString As String) As String
            Dim myKey As String = "Advantage"
            Dim cryptDES3 As New TripleDESCryptoServiceProvider()
            Dim cryptMD5Hash As New MD5CryptoServiceProvider()

            cryptDES3.Key = cryptMD5Hash.ComputeHash(ASCIIEncoding.ASCII.GetBytes(myKey))
            cryptDES3.Mode = CipherMode.ECB
            Dim desdencrypt As ICryptoTransform = cryptDES3.CreateEncryptor()
            Dim buff() As Byte = ASCIIEncoding.ASCII.GetBytes(myString)
            Encrypt = Convert.ToBase64String(desdencrypt.TransformFinalBlock(buff, 0, buff.Length))
        End Function
    End Class
End Namespace
