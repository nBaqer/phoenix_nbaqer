﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports BO = Advantage.Business.Objects
Imports CO = FAME.AdvantageV1.Common
Imports System.Web

Namespace Advantage.Business.Logic.Layer
    Public Class NavigationRoutines
        Inherits IDataProvider
#Region "Constructor"
        Public Sub New(ByVal context As HttpContext, ByVal connectionString As String)
            MyBase.New(context, connectionString)
        End Sub
#End Region
#Region "Private Routines"
        Public Function BuildNavigationDataTable(ByVal CurrentUserState As BO.User) As DataTable
            Dim dtObj As Object = dbContext.UspAdmissionsCommonNavigations
            Dim dt As New DataTable
            Dim dtRow As DataRow
            Dim pObj As CO.UserPagePermissionInfo = New CO.UserPagePermissionInfo
            With dt
                .Columns.Add(New DataColumn("ChildResourceId"))
                .Columns.Add(New DataColumn("ChildResource"))
                .Columns.Add(New DataColumn("ChildResourceURL"))
                .Columns.Add(New DataColumn("ParentResourceId"))
                .Columns.Add(New DataColumn("EnableResource"))
            End With
            For Each item In dtObj
                dtRow = dt.NewRow
                dtRow("ChildResourceId") = item(0)
                dtRow("ChildResource") = item(1)
                dtRow("ChildResourceURL") = item(2)
                If item(2) Is Nothing Then
                    dtRow("ChildResourceURL") = System.DBNull.Value
                Else
                    dtRow("ChildResourceURL") = item(2) + "?RESID=" + item(0).ToString.Trim + "&mod=AD" + "&cmpId=" + CurrentUserState.CampusId.ToString
                End If
                dtRow("ParentResourceId") = item(3)
                pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(CurrentUserState, item(0), CurrentUserState.CampusId.ToString())
                If (pObj.HasFull = False And pObj.HasEdit = False And pObj.HasAdd = False _
                    And pObj.HasDelete = False And pObj.HasDisplay = False And pObj.HasNone = True) _
                    Then dtRow("EnableResource") = False Else dtRow("EnableResource") = True
                dt.Rows.Add(dtRow)
            Next
            Return dt
        End Function
        Public Function BuildCommonTaskAndTabMenuItems(ByVal intSchoolOption As Integer, ByVal CurrentUserState As BO.User) As List(Of BO.CommonTaskMenuItem)
            Dim dtObj As Object = dbContext.UspAdvantageCommonTaskNavigationGetList(intSchoolOption)
            Dim lstMenuItems As New List(Of BO.CommonTaskMenuItem)
            For Each item In dtObj
                lstMenuItems.Add(New BO.CommonTaskMenuItem() With { _
                                                                .ModuleResourceId = item(0), _
                                                                .ChildResourceId = item(1), _
                                                                .ChildResourceDescription = item(2), _
                                                                .ChildResourceURL = SetURL(item(0), item(1), item(3), CurrentUserState, item(2)), _
                                                                .ParentResourceId = item(4), _
                                                                .EnableMenu = IsMenuItemEnabled(CurrentUserState, item(1)) _
                                                                })
            Next
            Return lstMenuItems
        End Function
        Public Function BuildReportMenuItems(ByVal intSchoolOption As Integer, ByVal CurrentUserState As BO.User) As List(Of BO.CommonTaskMenuItem)
            Dim dtObj As Object = dbContext.UspAdvantageReportsNavigationGetList(intSchoolOption)
            Dim lstMenuItems As New List(Of BO.CommonTaskMenuItem)
            For Each item In dtObj
                lstMenuItems.Add(New BO.CommonTaskMenuItem() With { _
                                                                .ModuleResourceId = item(0), _
                                                                .ChildResourceId = item(1), _
                                                                .ChildResourceDescription = item(2), _
                                                                .ChildResourceURL = SetURL(item(0), item(1), item(3), CurrentUserState, item(2)), _
                                                                .ParentResourceId = item(4), _
                                                                .EnableMenu = IsMenuItemEnabled(CurrentUserState, item(1)) _
                                                                })
            Next
            Return lstMenuItems
        End Function
        Public Function BuildMaintenanceMenuItems(ByVal intSchoolOption As Integer, ByVal CurrentUserState As BO.User) As List(Of BO.CommonTaskMenuItem)
            Dim dtObj As Object = dbContext.UspAdvantageMaintenanceNavigationGetList(intSchoolOption)
            Dim lstMenuItems As New List(Of BO.CommonTaskMenuItem)
            For Each item In dtObj
                lstMenuItems.Add(New BO.CommonTaskMenuItem() With { _
                                                                .ModuleResourceId = item(0), _
                                                                .ChildResourceId = item(1), _
                                                                .ChildResourceDescription = item(2), _
                                                                .ChildResourceURL = SetURLMaintenance(item(0), item(1), item(3), CurrentUserState, item(2), ""), _
                                                                .ParentResourceId = item(4), _
                                                                .EnableMenu = IsMenuItemEnabled(CurrentUserState, item(1)) _
                                                                })
            Next
            Return lstMenuItems
        End Function
        Public Function BuildStudentTabMenuItems(ByVal intSchoolOption As Integer, ByVal CurrentUserState As BO.User) As List(Of BO.CommonTaskMenuItem)
            Dim dtObj As Object = dbContext.USPStudentTabsGetList(CurrentUserState.CampusId.ToString, intSchoolOption)
            Dim lstMenuItems As New List(Of BO.CommonTaskMenuItem)
            For Each item In dtObj
                lstMenuItems.Add(New BO.CommonTaskMenuItem() With { _
                                                                .ModuleResourceId = item(0), _
                                                                .ChildResourceId = item(1), _
                                                                .ChildResourceDescription = item(2), _
                                                                .ChildResourceURL = SetURL(item(0), item(1), item(3), CurrentUserState, item(2)), _
                                                                .ParentResourceId = item(4), _
                                                                .EnableMenu = IsMenuItemEnabled(CurrentUserState, item(1)) _
                                                                })
            Next
            Return lstMenuItems
        End Function
        Public Function BuildLeadTabMenuItems(ByVal intSchoolOption As Integer, ByVal CurrentUserState As BO.User) As List(Of BO.CommonTaskMenuItem)
            Dim dtObj As Object = dbContext.USPLeadTabsGetList(CurrentUserState.CampusId.ToString, intSchoolOption)
            Dim lstMenuItems As New List(Of BO.CommonTaskMenuItem)
            For Each item In dtObj
                lstMenuItems.Add(New BO.CommonTaskMenuItem() With { _
                                                                .ModuleResourceId = item(0), _
                                                                .ChildResourceId = item(1), _
                                                                .ChildResourceDescription = item(2), _
                                                                .ChildResourceURL = SetURL(item(0), item(1), item(3), CurrentUserState, item(2)), _
                                                                .ParentResourceId = item(4), _
                                                                .EnableMenu = IsMenuItemEnabled(CurrentUserState, item(1)) _
                                                                })
            Next
            Return lstMenuItems
        End Function
        Public Function BuildEmployerTabMenuItems(ByVal intSchoolOption As Integer, ByVal CurrentUserState As BO.User) As List(Of BO.CommonTaskMenuItem)
            Dim dtObj As Object = dbContext.USPEmployerTabsGetList(CurrentUserState.CampusId.ToString, intSchoolOption)
            Dim lstMenuItems As New List(Of BO.CommonTaskMenuItem)
            For Each item In dtObj
                lstMenuItems.Add(New BO.CommonTaskMenuItem() With { _
                                                                .ModuleResourceId = item(0), _
                                                                .ChildResourceId = item(1), _
                                                                .ChildResourceDescription = item(2), _
                                                                .ChildResourceURL = SetURL(item(0), item(1), item(3), CurrentUserState, item(2)), _
                                                                .ParentResourceId = item(4), _
                                                                .EnableMenu = IsMenuItemEnabled(CurrentUserState, item(1)) _
                                                                })
            Next
            Return lstMenuItems
        End Function
        Public Function BuildAdmissionsMenuItems(ByVal intSchoolOption As Integer, ByVal CurrentUserState As BO.User) As List(Of BO.CommonTaskMenuItem)
            Dim dtObj As Object = dbContext.USPADNavigationGetList(intSchoolOption)
            Dim lstMenuItems As New List(Of BO.CommonTaskMenuItem)
            For Each item In dtObj
                lstMenuItems.Add(New BO.CommonTaskMenuItem() With { _
                                                                .ModuleResourceId = item(0), _
                                                                .ChildResourceId = item(1), _
                                                                .ChildResourceDescription = item(2), _
                                                                .ChildResourceURL = SetURL(item(0), item(1), item(3), CurrentUserState, item(2)), _
                                                                .ParentResourceId = item(4), _
                                                                .EnableMenu = IsMenuItemEnabled(CurrentUserState, item(1)), _
                                                                .ResourceTypeId = item(9) _
                                                                })
            Next
            Return lstMenuItems
        End Function
        Public Function BuildCommonTaskMenuItems(ByVal intSchoolOption As Integer, ByVal CurrentUserState As BO.User) As List(Of BO.CommonTaskMenuItem)

            Dim dtObj As Object = dbContext.UspAdvantageCommonTaskNavigationByModuleGetList(CurrentUserState.CampusId.ToString, _
                                                                                          intSchoolOption) 'dbContext.USPCommonTaskAll(intSchoolOption)


            Dim lstMenuItems As New List(Of BO.CommonTaskMenuItem)
            For Each item In dtObj
                lstMenuItems.Add(New BO.CommonTaskMenuItem() With { _
                                                                .ModuleResourceId = item(0), _
                                                                .ChildResourceId = item(1), _
                                                                .ChildResourceDescription = item(2), _
                                                                .ChildResourceURL = SetURL(item(0), item(1), item(3), CurrentUserState, item(2)), _
                                                                .ParentResourceId = item(4), _
                                                                .EnableMenu = IsMenuItemEnabled(CurrentUserState, item(1)), _
                                                                .ParentResource = IsRoot(item(4)), _
                                                                .DisplaySequence = item(7) _
                                                                                })
            Next
            Return lstMenuItems
        End Function
        Public Function BuildCommonTaskMenuItemsCache(ByVal intSchoolOption As Integer, ByVal CurrentUserState As BO.User) As Object

            Dim dtObj As Object = dbContext.UspAdvantageCommonTaskNavigationByModuleGetList(CurrentUserState.CampusId.ToString, _
                                                                                          intSchoolOption) 'dbContext.USPCommonTaskAll(intSchoolOption)

            Return dtObj

            'Dim lstMenuItems As New List(Of BO.CommonTaskMenuItem)
            'For Each item In dtObj
            '    lstMenuItems.Add(New BO.CommonTaskMenuItem() With { _
            '                                                    .ModuleResourceId = item(0), _
            '                                                    .ChildResourceId = item(1), _
            '                                                    .ChildResourceDescription = item(2), _
            '                                                    .ChildResourceURL = SetURL(item(0), item(1), item(3), CurrentUserState, item(2)), _
            '                                                    .ParentResourceId = item(4), _
            '                                                    .EnableMenu = IsMenuItemEnabled(CurrentUserState, item(1)), _
            '                                                    .ParentResource = IsRoot(item(4)), _
            '                                                    .DisplaySequence = item(7) _
            '                                                                    })
            'Next
            'Return lstMenuItems
        End Function
        Public Function BuildReportCascadeMenuItems(ByVal intSchoolOption As Integer, ByVal CurrentUserState As BO.User) As List(Of BO.CommonTaskMenuItem)
            Dim dtObj As Object = dbContext.UspAdvantageReportsNavigationByModuleGetList(CurrentUserState.CampusId.ToString, _
                                                                                         intSchoolOption) 'dbContext.USPCommonTaskAll(intSchoolOption)
            Dim lstMenuItems As New List(Of BO.CommonTaskMenuItem)
            For Each item In dtObj
                lstMenuItems.Add(New BO.CommonTaskMenuItem() With { _
                                                                .ModuleResourceId = item(0), _
                                                                .ChildResourceId = item(1), _
                                                                .ChildResourceDescription = item(2), _
                                                                .ChildResourceURL = SetURL(item(0), item(1), item(3), CurrentUserState, item(2)), _
                                                                .ParentResourceId = item(4), _
                                                                .EnableMenu = IsMenuItemEnabled(CurrentUserState, item(1)), _
                                                                .ParentResource = IsRoot(item(4)), _
                                                                .DisplaySequence = item(7) _
                                                                                })
            Next
            Return lstMenuItems
        End Function
        Public Function BuildReportCascadeMenuItemsCache(ByVal intSchoolOption As Integer, ByVal CurrentUserState As BO.User) As Object
            Dim dtObj As Object = dbContext.UspAdvantageReportsNavigationByModuleGetList(CurrentUserState.CampusId.ToString, _
                                                                                         intSchoolOption) 'dbContext.USPCommonTaskAll(intSchoolOption)

            Return dtObj
            'Dim lstMenuItems As New List(Of BO.CommonTaskMenuItem)
            'For Each item In dtObj
            '    lstMenuItems.Add(New BO.CommonTaskMenuItem() With { _
            '                                                    .ModuleResourceId = item(0), _
            '                                                    .ChildResourceId = item(1), _
            '                                                    .ChildResourceDescription = item(2), _
            '                                                    .ChildResourceURL = SetURL(item(0), item(1), item(3), CurrentUserState, item(2)), _
            '                                                    .ParentResourceId = item(4), _
            '                                                    .EnableMenu = IsMenuItemEnabled(CurrentUserState, item(1)), _
            '                                                    .ParentResource = IsRoot(item(4)), _
            '                                                    .DisplaySequence = item(7) _
            '                                                                    })
            'Next
            'Return lstMenuItems
        End Function
        Public Function BuildMaintenanceForAllModules(ByVal intSchoolOption As Integer, ByVal CurrentUserState As BO.User) As List(Of BO.CommonTaskMenuItem)
            'Dim dtObj As Object = dbContext.USPMaintenanceAll(intSchoolOption)
            Dim dtObj As Object = dbContext.UspAdvantageMaintenanceNavigationByModuleGetList(CurrentUserState.CampusId.ToString, _
                                                                                             intSchoolOption, _
                                                                                             New Guid(CurrentUserState.UserId.ToString))
            Dim lstMenuItems As New List(Of BO.CommonTaskMenuItem)
            For Each item In dtObj
                lstMenuItems.Add(New BO.CommonTaskMenuItem() With { _
                                                                .ModuleResourceId = item(0), _
                                                                .ChildResourceId = item(1), _
                                                                .ChildResourceDescription = item(2), _
                                                                .ChildResourceURL = SetURLMaintenance(item(0), item(1), item(3), CurrentUserState, item(2), ""), _
                                                                .ParentResourceId = item(4), _
                                                                .EnableMenu = IsMenuItemEnabled(CurrentUserState, item(1)), _
                                                                .ParentResource = IsRoot(item(4)), _
                                                                .DisplaySequence = item(7) _
                                                                })
            Next
            Return lstMenuItems
        End Function
        Public Function BuildMaintenanceForAllModulesCache(ByVal intSchoolOption As Integer, ByVal CurrentUserState As BO.User) As Object
            Dim dtObj As Object = dbContext.UspAdvantageMaintenanceNavigationByModuleGetList(CurrentUserState.CampusId.ToString, _
                                                                                             intSchoolOption, _
                                                                                             New Guid(CurrentUserState.UserId.ToString))

            Return dtObj
            'Dim lstMenuItems As New List(Of BO.CommonTaskMenuItem)
            'For Each item In dtObj
            '    lstMenuItems.Add(New BO.CommonTaskMenuItem() With { _
            '                                                    .ModuleResourceId = item(0), _
            '                                                    .ChildResourceId = item(1), _
            '                                                    .ChildResourceDescription = item(2), _
            '                                                    .ChildResourceURL = SetURLMaintenance(item(0), item(1), item(3), CurrentUserState, item(2), ""), _
            '                                                    .ParentResourceId = item(4), _
            '                                                    .EnableMenu = IsMenuItemEnabled(CurrentUserState, item(1)), _
            '                                                    .ParentResource = IsRoot(item(4)), _
            '                                                    .DisplaySequence = item(7) _
            '                                                    })
            'Next
            'Return lstMenuItems
        End Function
        Public Function BuildTabs(ByVal intSchoolOption As Integer, ByVal CurrentUserState As BO.User, _
                                  Optional ByVal strVID As String = "") As List(Of BO.CommonTaskMenuItem)
            'Dim dtObj As Object = dbContext.USPMaintenanceAll(intSchoolOption)
            Dim dtObj As Object = dbContext.UspAdvantageTabsByModuleGetList(CurrentUserState.CampusId.ToString, _
                                                                            intSchoolOption)
            Dim lstMenuItems As New List(Of BO.CommonTaskMenuItem)
            For Each item In dtObj
                lstMenuItems.Add(New BO.CommonTaskMenuItem() With { _
                                                                .ModuleResourceId = item(0), _
                                                                .TabId = item(1), _
                                                                .ChildResourceId = item(2), _
                                                                .ChildResourceDescription = item(3), _
                                                                .ChildResourceURL = SetURLMaintenance(item(0), item(2), item(4), CurrentUserState, item(3), strVID), _
                                                                .ParentResourceId = item(5), _
                                                                .EnableMenu = IsMenuItemEnabled(CurrentUserState, item(2)), _
                                                                .ParentResource = IsRoot(item(5)), _
                                                                .DisplaySequence = item(8) _
                                                                })
            Next
            Return lstMenuItems
        End Function
        Public Function BuildTabsCache(ByVal intSchoolOption As Integer, ByVal CurrentUserState As BO.User, _
                                  Optional ByVal strVID As String = "") As Object
            'Dim dtObj As Object = dbContext.USPMaintenanceAll(intSchoolOption)
            Dim dtObj As Object = dbContext.UspAdvantageTabsByModuleGetList(CurrentUserState.CampusId.ToString, _
                                                                            intSchoolOption)

            Return dtObj

            'Dim lstMenuItems As New List(Of BO.CommonTaskMenuItem)
            'For Each item In dtObj
            '    lstMenuItems.Add(New BO.CommonTaskMenuItem() With { _
            '                                                    .ModuleResourceId = item(0), _
            '                                                    .TabId = item(1), _
            '                                                    .ChildResourceId = item(2), _
            '                                                    .ChildResourceDescription = item(3), _
            '                                                    .ChildResourceURL = SetURLMaintenance(item(0), item(2), item(4), CurrentUserState, item(3), strVID), _
            '                                                    .ParentResourceId = item(5), _
            '                                                    .EnableMenu = IsMenuItemEnabled(CurrentUserState, item(2)), _
            '                                                    .ParentResource = IsRoot(item(5)), _
            '                                                    .DisplaySequence = item(8) _
            '                                                    })
            'Next
            'Return lstMenuItems
        End Function
        Public Function BuildTools(ByVal intSchoolOption As Integer, ByVal CurrentUserState As BO.User) As List(Of BO.CommonTaskMenuItem)
            Dim dtObj As Object = dbContext.USPToolResourcesList
            Dim lstMenuItems As New List(Of BO.CommonTaskMenuItem)
            For Each item In dtObj
                lstMenuItems.Add(New BO.CommonTaskMenuItem() With { _
                                                             .ChildResourceId = item(1), _
                                                             .ChildResourceDescription = item(2), _
                                                             .ChildResourceURL = SetURLMaintenanceforTools(item(1), item(2), CurrentUserState)
                 })
            Next
            Return lstMenuItems
        End Function
        Public Function BuildToolsCache(ByVal intSchoolOption As Integer, ByVal CurrentUserState As BO.User) As Object
            Dim dtObj As Object = dbContext.USPToolResourcesList
            Return dtObj
            'Dim lstMenuItems As New List(Of BO.CommonTaskMenuItem)
            'For Each item In dtObj
            '    lstMenuItems.Add(New BO.CommonTaskMenuItem() With { _
            '                                                 .ChildResourceId = item(1), _
            '                                                 .ChildResourceDescription = item(2), _
            '                                                 .ChildResourceURL = SetURLMaintenanceforTools(item(1), item(2), CurrentUserState)
            '     })
            'Next
            'Return lstMenuItems
        End Function
        Public Function BuildCampuses(ByVal CurrentUserState As BO.User) As List(Of BO.Campus)
            Dim cmpInputList As New List(Of BO.Campus)
            Dim cmpOutputList As New List(Of BO.Campus)
            cmpInputList = CurrentUserState.UserCampuses
            For Each item In cmpInputList
                Dim dtObj As Object = dbContext.UspCampusdescriptionCampusid(item.CampusId.ToString)
                For Each CampusItem In dtObj
                    cmpOutputList.Add(New BO.Campus() With { _
                                               .CampusId = CampusItem(0), .CampCode = CampusItem(1), .CampDescrip = CampusItem(2), _
                                               .Address1 = CampusItem(3), .Address2 = CampusItem(4), .City = CampusItem(5), _
                                               .Phone1 = CampusItem(6), .Phone2 = CampusItem(7), .Phone3 = CampusItem(8), _
                                               .Fax = CampusItem(9), .Email = CampusItem(10), .Website = CampusItem(11), _
                                               .IsCorporate = CampusItem(12), .TranscriptAuthZnTitle = CampusItem(13), .TranscriptAuthZnName = CampusItem(14), _
                                               .TCSourcePath = CampusItem(15), .IsRemoteServer = CampusItem(16), .PortalContactEmail = CampusItem(17), _
                                               .RemoteServerUsrNm = CampusItem(18), .RemoteServerPwd = CampusItem(19), .SourceFolderLoc = CampusItem(20), _
                                               .TargetFolderLoc = CampusItem(21), .UseCampusAddress = CampusItem(22), .InvAddress1 = CampusItem(23), _
                                               .InvAddress2 = CampusItem(24), .InvCity = CampusItem(25), .InvStateID = CampusItem(26), _
                                               .InvStateDescription = CampusItem(27), .InvZip = CampusItem(28), .InvCountryID = CampusItem(29), _
                                               .InvPhone1 = CampusItem(30), .InvFax = CampusItem(31), .FLSourcePath = CampusItem(32), _
                                               .FLTargetPath = CampusItem(33), .FLExceptionPath = CampusItem(34), .IsRemoteServerFL = CampusItem(35), _
                                               .RemoteServerUsrNmFL = CampusItem(36), .RemoteServerPwdFL = CampusItem(37), .SourceFolderLocFL = CampusItem(38), _
                                               .TargetFolderLocFL = CampusItem(39), .StateId = CampusItem(40), .StateDescription = CampusItem(41), _
                                               .Zip = CampusItem(42), .CountryId = CampusItem(43), .CountryDescription = CampusItem(44), _
                                               .StatusId = CampusItem(45), .StatusDescription = CampusItem(46)})
                Next CampusItem
            Next item
            Return cmpOutputList
        End Function
        'Private Function getCampusDescription(ByVal CampusId As String) As String
        '    Dim dtObj As Object = dbContext.UspCampusdescriptionCampusid(CampusId)
        '    For Each item In dtObj
        '        Return item(1).ToString
        '    Next
        'End Function
        Public Function IsRoot(ByVal ParentResourceId As Integer) As String
            If ParentResourceId.ToString Is Nothing Or _
                ParentResourceId = 0 Then
                Return "root"
            Else
                Return ""
            End If
        End Function
        Public Function SetURL(ByVal ModuleResourceId As Integer, _
                                ByVal ResourceId As Integer, _
                                ByVal ResourceURL As String, _
                                ByVal CurrentUserState As BO.User, _
                                ByVal ResourceDescription As String) As String
            Dim ChildResourceURL As String
            Dim ModuleCode As String = (New BO.Modules).getModuleCode(ModuleResourceId)
            If ResourceURL Is Nothing Then
                ChildResourceURL = System.DBNull.Value.ToString
            Else
                If ResourceId = 203 Then
                    ChildResourceURL = "SY/StudentTabNewMenuStyle.aspx" + "?RESID=" + ResourceId.ToString.Trim + "&mod=" + ModuleCode.ToString.Trim + "&cmpId=" + CurrentUserState.CampusId.ToString + "&desc=" + ResourceDescription.ToString.Trim
                ElseIf ResourceId = 271 Then
                    'ChildResourceURL = "SY/Navigation.aspx" + "?RESID=" + ResourceId.ToString.Trim + "&mod=" + ModuleCode.ToString.Trim + "&cmpId=" + CurrentUserState.CampusId.ToString
                    ChildResourceURL = "SY/TestNavigation.aspx" + "?RESID=" + ResourceId.ToString.Trim + "&mod=" + ModuleCode.ToString.Trim + "&cmpId=" + CurrentUserState.CampusId.ToString
                Else
                    ChildResourceURL = ResourceURL + "?RESID=" + ResourceId.ToString.Trim + "&mod=" + ModuleCode.ToString.Trim + "&cmpId=" + CurrentUserState.CampusId.ToString + "&desc=" + ResourceDescription.ToString.Trim
                End If
                'ChildResourceURL = ResourceURL + "?RESID=" + ResourceId.ToString.Trim + "&mod=" + ModuleCode.ToString.Trim + "&cmpId=" + CurrentUserState.CampusId.ToString
            End If
            Return ChildResourceURL
        End Function
        Public Function SetURLMaintenance(ByVal ModuleResourceId As Integer, _
                                ByVal ResourceId As Integer, _
                                ByVal ResourceURL As String, _
                                ByVal CurrentUserState As BO.User, _
                                ByVal ResourceDescription As String,
                                ByVal strVID As String) As String
            Dim ChildResourceURL As String
            Dim ModuleCode As String = (New BO.Modules).getModuleCode(ModuleResourceId)
            Dim getMaintenancePageTemplateList As List(Of Integer) = addMaintenanceItemsForBasePage()

            If ResourceURL Is Nothing Then
                ChildResourceURL = System.DBNull.Value.ToString
            Else
                Dim intIndex As Integer = getMaintenancePageTemplateList.FirstOrDefault(Function(i) i = ResourceId)
                If intIndex >= 1 Then
                    ChildResourceURL = "~/SY/MaintenanceBase.aspx" + "?RESID=" + ResourceId.ToString.Trim + "&mod=" + ModuleCode.ToString.Trim + "&cmpId=" + CurrentUserState.CampusId.ToString + "&desc=" + ResourceDescription.ToString.Trim
                Else
                    Select Case ResourceId
                        'Case "159", "155", "203", "213", "327", "530", "531", "538", _
                        '"169", "625", "286", "380", "628", "374", "539", "200", _
                        '"230", "740", "372", "614", "301", "741", "679", "742", _
                        '"116", "535", "534", "303", "380", "542", "301", _
                        '"479", "175", "652", "177", "543",
                        '"739", "97", "95", "380", "539", _
                        '"92", "738", "112", "114", "541", _
                        '"532" 'Student Tabs
                        Case "90", "92", "95", "97", "112", _
                                "114", "116", "155", "159", "169", _
                                "175", "177", "200", "203", "213", _
                                "230", "286", "301", "303", "327", _
                                "372", "374", "380", "479", "530", _
                                "531", "532", "534", "535", "538", _
                                "539", "541", "542", "543", "614", _
                                "625", "628", "652", "738", "739", _
                                "740", "741", "742", _
                                "52", "55", "69", "79", "82", _
                                "86", "111", "145", "146", "147", _
                                "148", "170", "225", "281", "313", _
                                "456", "484", "113", "334"

                            If strVID.Trim = "" Then
                                ChildResourceURL = ResourceURL + "?RESID=" + ResourceId.ToString.Trim + "&mod=" + ModuleCode.ToString.Trim + "&cmpId=" + CurrentUserState.CampusId.ToString + "&VID=" + "&desc=" + ResourceDescription.ToString.Trim
                            Else
                                ChildResourceURL = ResourceURL + "?RESID=" + ResourceId.ToString.Trim + "&mod=" + ModuleCode.ToString.Trim + "&cmpId=" + CurrentUserState.CampusId.ToString + "&VID=" + strVID + "&desc=" + ResourceDescription.ToString.Trim
                            End If
                        Case Else
                            ChildResourceURL = ResourceURL + "?RESID=" + ResourceId.ToString.Trim + "&mod=" + ModuleCode.ToString.Trim + "&cmpId=" + CurrentUserState.CampusId.ToString + "&desc=" + ResourceDescription.ToString.Trim
                    End Select

                End If
            End If
            Return ChildResourceURL
        End Function
        Public Function SetURLMaintenanceforTools(ByVal ResourceId As Integer, ByVal ResourceDescription As String, ByVal CurrentUserState As BO.User) As String
            Dim ChildResourceURL As String
            Select Case ResourceId
                Case Is = 283
                    'ChildResourceURL = "PL/DocManagement.aspx" + "?RESID=" + ResourceId.ToString.Trim + "&mod=PL" '&cmpId=" + CurrentUserState.CampusId.ToString + "&desc=" + ResourceDescription.ToString.Trim
                    ChildResourceURL = "PL/DocManagement.aspx" + "?RESID=" + ResourceId.ToString.Trim + "&mod=PL" + "&cmpId=" + CurrentUserState.CampusId.ToString ' "&desc=" + ResourceDescription.ToString.Trim
                Case Is = 331
                    'ChildResourceURL = "SY/ProcessMessages.aspx" + "?RESID=" + ResourceId.ToString.Trim + "&mod=SY" '&cmpId=" + CurrentUserState.CampusId.ToString + "&desc=" + ResourceDescription.ToString.Trim
                    ChildResourceURL = "SY/ProcessMessages.aspx" + "?RESID=" + ResourceId.ToString.Trim + "&mod=SY" + "&cmpId=" + CurrentUserState.CampusId.ToString '+ "&desc=" + ResourceDescription.ToString.Trim
                Case Is = 141
                    'ChildResourceURL = "SY/SystemWideEmail.aspx" + "?RESID=" + ResourceId.ToString.Trim + "&mod=SY" '&cmpId=" + CurrentUserState.CampusId.ToString + "&desc=" + ResourceDescription.ToString.Trim
                    ChildResourceURL = "SY/SystemWideEmail.aspx" + "?RESID=" + ResourceId.ToString.Trim + "&mod=SY" + "&cmpId=" + CurrentUserState.CampusId.ToString '+ "&desc=" + ResourceDescription.ToString.Trim
                Case Else
                    'ChildResourceURL = "DefaultTM.html" + "?RESID=" + ResourceId.ToString.Trim + "&mod=SY"
                    ChildResourceURL = "DefaultTM.html" + "?RESID=" + ResourceId.ToString.Trim + "&mod=SY" + "&cmpId=" + CurrentUserState.CampusId.ToString
            End Select
            Return ChildResourceURL
        End Function
        Public Function IsMenuItemEnabled(ByVal CurrentUserState As BO.User, ByVal ResourceId As Integer) As Boolean
            Dim pObj As CO.UserPagePermissionInfo = New CO.UserPagePermissionInfo
            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(CurrentUserState, ResourceId, CurrentUserState.CampusId.ToString())
            'pObj = CheckUserPermissionByCampusAndResourceMenu(CurrentUserState, ResourceId, CurrentUserState.CampusId.ToString)
            If (pObj.HasFull = False And pObj.HasEdit = False And pObj.HasAdd = False _
                    And pObj.HasDelete = False And pObj.HasDisplay = False And pObj.HasNone = True) _
                    Then
                Return False
            Else
                Return True
            End If
        End Function
        Public Function IsMenuItemEnabledDS(ByVal CurrentUserState As BO.User, ByVal ResourceId As Integer) As Boolean
            Dim pObj As CO.UserPagePermissionInfo = New CO.UserPagePermissionInfo
            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(CurrentUserState, ResourceId, CurrentUserState.CampusId.ToString())
            'pObj = CheckUserPermissionByCampusAndResourceMenu(CurrentUserState, ResourceId, CurrentUserState.CampusId.ToString)
            If (pObj.HasFull = False And pObj.HasEdit = False And pObj.HasAdd = False _
                    And pObj.HasDelete = False And pObj.HasDisplay = False And pObj.HasNone = True) _
                    Then
                Return False
            Else
                Return True
            End If
        End Function
        Private Function addMaintenanceItemsForBasePage() As List(Of Integer)
            Dim baseMaintenanceCollection As New List(Of Integer)
            With baseMaintenanceCollection
                ''Few resources commented by Sara on March 8 2012 since they don't fall under the maintenance criteria of Code, Descrip, status and cmpGrp.

                'Admissions
                .Add(491)
                .Add(12)
                '.Add(28)
                .Add(29)
                .Add(239)
                ' .Add(339)
                .Add(388)
                .Add(387)
                .Add(20)
                .Add(389)
                .Add(390)
                ''.Add(339)
                .Add(13)
                .Add(16)
                .Add(18)
                ''.Add(296)
                .Add(35)
                .Add(216)
                '' .Add(215)
                .Add(212)
                .Add(34)
                .Add(241)



                'Academic Records
                .Add(39)
                .Add(67)
                .Add(4)
                .Add(316)
                .Add(350)
                .Add(624)
                .Add(323)
                .Add(297)
                .Add(289)
                .Add(10)
                .Add(161)
                .Add(100)
                .Add(5)

                'Student Accounts
                .Add(72)
                .Add(571)

                'Placement
                .Add(302)
                .Add(24)
                .Add(27)
                .Add(282)
                .Add(304)
                .Add(25)
                .Add(93)
                .Add(99)

                'Human Resources
                '.Add(299)
                '.Add(298)

                'System
                .Add(31)
                .Add(22)
                .Add(481)
                .Add(788)
            End With
            Return baseMaintenanceCollection
        End Function
        Public Function BuildSetUpRequiredFields() As List(Of BO.SetUpRequiredFieldResources)
            'Dim dtObj As Object = dbContext.USPMaintenanceAll(intSchoolOption)
            Dim dtObj As Object = dbContext.USPGetRequiredFields()
            Dim lstResources As New List(Of BO.SetUpRequiredFieldResources)
            For Each item In dtObj

                lstResources.Add(New BO.SetUpRequiredFieldResources() With { _
                                                            .ModuleResourceId = item(0), _
                                                            .ResourceId = item(1), _
                                                            .Resource = item(2), _
                                                            .ResourceTypeId = item(3), _
                                                            .TabId = item(4) _
                                                         })

            Next
            Return lstResources
        End Function
        'Public Function CheckUserPermissionByCampusAndResourceMenu(ByVal AdvantageUserState As BO.User, _
        '                                                     ByVal ResourceId As String, _
        '                                                     ByVal CampusId As String) As CO.UserPagePermissionInfo
        '    Dim pObj As CO.UserPagePermissionInfo = New CO.UserPagePermissionInfo
        '    Try
        '        Select Case AdvantageUserState.FullPermission
        '            Case Is = True
        '                With pObj
        '                    .HasFull = True
        '                    .HasAdd = True
        '                    .HasDelete = True
        '                    .HasEdit = True
        '                    .HasNone = False
        '                End With
        '            Case Else

        '                Dim result As List(Of BO.PageLevelPermission) = getResourceCampusPermissionMenu(AdvantageUserState.UserId.ToString, ResourceId, CampusId)

        '                'Dim result As IEnumerable(Of BO.PageLevelPermission) = AdvantageUserState.UserRoles.Where(Function(s) s.ResourceID.ToString.Equals(ResourceId) _
        '                '                                                                                              And s.CampusId.ToString.Equals(CampusId)).OrderByDescending(Function(p) p.AccessLevel).ToList().First()
        '                If result.Count >= 1 Then
        '                    If result(0).PageLevelFullAccess = True Then pObj.HasFull = True
        '                    If result(0).PageLevelEditAccess = True Then pObj.HasEdit = True
        '                    If result(0).PageLevelCreateAccess = True Then pObj.HasAdd = True
        '                    If result(0).PageLevelDeleteAccess = True Then pObj.HasDelete = True
        '                    If result(0).PageLevelReadOnlyAccess = True And _
        '                        result(0).PageLevelEditAccess = False And _
        '                        result(0).PageLevelCreateAccess = False And _
        '                        result(0).PageLevelDeleteAccess = False Then pObj.HasDisplay = True
        '                    If result(0).PageLevelReadOnlyAccess = False And _
        '                     result(0).PageLevelEditAccess = False And _
        '                     result(0).PageLevelCreateAccess = False And _
        '                     result(0).PageLevelDeleteAccess = False Then
        '                        pObj.HasNone = True
        '                    Else
        '                        pObj.HasNone = False
        '                    End If
        '                Else
        '                    pObj.HasNone = True
        '                End If
        '        End Select
        '    Catch ex As Exception
        '        pObj.HasNone = True 'when it errors out, set permission to none
        '    End Try
        '    Return pObj
        'End Function
        'Private Function getResourceCampusPermissionMenu(ByVal UserId As String, _
        '                                                 ByVal ResourceId As Integer, _
        '                                                 ByVal CampusId As String) As List(Of BO.PageLevelPermission)
        '    Dim dtObj As Object = dbContext.USPGetResourceCampusPermissionMenu(New Guid(UserId), ResourceId, New Guid(CampusId))
        '    Dim lstPageLevelPermissions As New List(Of BO.PageLevelPermission)
        '    For Each item In dtObj
        '        Dim addPageLevelAccess As New BO.PageLevelPermission() With _
        '                          { _
        '                             .ResourceID = item(0), _
        '                             .CampusId = item(1), _
        '                             .AccessLevel = item(2), _
        '                             .RoleId = Guid.Empty, _
        '                             .PageLevelFullAccess = item(3), _
        '                             .PageLevelEditAccess = item(4), _
        '                             .PageLevelCreateAccess = item(5), _
        '                             .PageLevelDeleteAccess = item(6), _
        '                             .PageLevelReadOnlyAccess = item(7)}
        '        lstPageLevelPermissions.Add(addPageLevelAccess)
        '    Next
        '    Return lstPageLevelPermissions
        'End Function
        Public Function getResourceCampusPermission(ByVal UserId As String) As List(Of BO.PageLevelPermission)
            Dim dtObj As Object = dbContext.USPGetResourceCampusPermission(New Guid(UserId)) 'dbContext.USPCommonTaskAll(intSchoolOption)
            Dim lstPageLevelPermissions As New List(Of BO.PageLevelPermission)
            For Each item In dtObj
                Dim addPageLevelAccess As New BO.PageLevelPermission() With _
                                  { _
                                     .ResourceID = item(0), _
                                     .AccessLevel = item(1), _
                                     .CampusId = item(2), _
                                     .RoleId = Guid.Empty}
                setPageLevelPermission(item(1), addPageLevelAccess)
                lstPageLevelPermissions.Add(addPageLevelAccess)
            Next
            Return lstPageLevelPermissions
        End Function
        Private Function setPageLevelPermission(ByVal AccessLevel As Integer, ByRef PageLevelAccess As BO.PageLevelPermission) As BO.PageLevelPermission
            Select Case AccessLevel
                Case Is = 15 'Full Access
                    PageLevelAccess.PageLevelFullAccess = True
                    PageLevelAccess.PageLevelEditAccess = True
                    PageLevelAccess.PageLevelCreateAccess = True
                    PageLevelAccess.PageLevelDeleteAccess = True
                Case Is = 14 'Edit, Create, Delete
                    PageLevelAccess.PageLevelEditAccess = True
                    PageLevelAccess.PageLevelCreateAccess = True
                    PageLevelAccess.PageLevelDeleteAccess = True
                Case Is = 13 'Edit, Create, ReadOnly
                    PageLevelAccess.PageLevelEditAccess = True
                    PageLevelAccess.PageLevelCreateAccess = True
                    PageLevelAccess.PageLevelReadOnlyAccess = True
                Case Is = 12 'Edit, Create
                    PageLevelAccess.PageLevelEditAccess = True
                    PageLevelAccess.PageLevelCreateAccess = True
                    ''Added by Sara on March 7th 2012 for Edit and Delete
                Case Is = 10
                    PageLevelAccess.PageLevelEditAccess = True
                    PageLevelAccess.PageLevelDeleteAccess = True
                Case Is = 8 'Edit
                    PageLevelAccess.PageLevelEditAccess = True
                    ''Added by Sara on March 7th 2012 for Add and Delete
                Case Is = 6 'Add and Delete
                    PageLevelAccess.PageLevelCreateAccess = True
                    PageLevelAccess.PageLevelDeleteAccess = True
                Case Is = 4 'Create
                    PageLevelAccess.PageLevelCreateAccess = True
                Case Is = 2 'Delete
                    PageLevelAccess.PageLevelDeleteAccess = True
                Case Is = 1 'Read Only
                    PageLevelAccess.PageLevelReadOnlyAccess = True
                Case Else
                    'US3141 
                    'PageLevelAccess.PageLevelReadOnlyAccess = True
                    PageLevelAccess.PageLevelReadOnlyAccess = False
            End Select
            Return PageLevelAccess
        End Function
#End Region

    End Class
End Namespace

