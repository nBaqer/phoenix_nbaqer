﻿
Imports System.Collections.Generic
Imports System.Linq
Imports DAL = Advantage.Data.ORM
Imports BO = Advantage.Business.Objects
Namespace Advantage.Business.Logic.Layer.Assemblers
    Friend NotInheritable Class StudentAddressAssembler
#Region "Convert Persistent Objects to Business Objects"
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.ArStudAddress)) As List(Of BO.StudentAddress)
            Dim targetBusObjects As List(Of BO.StudentAddress) = New List(Of BO.StudentAddress)
            For Each sourceObj As DAL.ArStudAddress In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return CType(targetBusObjects, List(Of BO.StudentAddress))
        End Function
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IEnumerable(Of DAL.ArStudAddress)) As List(Of BO.StudentAddress)
            Dim targetBusObjects As List(Of BO.StudentAddress) = New List(Of BO.StudentAddress)
            For Each sourceObj As DAL.ArStudAddress In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return CType(targetBusObjects, List(Of BO.StudentAddress))
        End Function
        Friend Shared Function AssembleContract(ByVal sourcePersistentObject As DAL.ArStudAddress) As BO.StudentAddress
            Dim targetBusObject As New BO.StudentAddress
            With targetBusObject
                .Address1 = sourcePersistentObject.Address1
                .Address2 = sourcePersistentObject.Address2
                .City = sourcePersistentObject.City
                .StateId = sourcePersistentObject.StateId
                .StudentId = sourcePersistentObject.StudentId.ToString()
                .StdAddressId = sourcePersistentObject.StdAddressId.ToString()
            End With
            Return targetBusObject
        End Function
#End Region
    End Class
End Namespace
