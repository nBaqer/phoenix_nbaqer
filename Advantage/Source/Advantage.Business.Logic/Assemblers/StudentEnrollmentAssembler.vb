﻿
Imports System.Collections.Generic
Imports System.Linq
Imports DAL = Advantage.Data.ORM
Imports BO = Advantage.Business.Objects
Namespace Advantage.Business.Logic.Layer.Assemblers
    Public Class StudentEnrollmentAssembler
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.ArStuEnrollment)) As List(Of BO.StudentEnrollment)
            Dim targetBusObjects As List(Of BO.StudentEnrollment) = New List(Of BO.StudentEnrollment)
            For Each sourceObj As DAL.ArStuEnrollment In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return CType(targetBusObjects, List(Of BO.StudentEnrollment))
        End Function
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IEnumerable(Of DAL.ArStuEnrollment)) As List(Of BO.StudentEnrollment)
            Dim targetBusObjects As List(Of BO.StudentEnrollment) = New List(Of BO.StudentEnrollment)
            For Each sourceObj As DAL.ArStuEnrollment In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return CType(targetBusObjects, List(Of BO.StudentEnrollment))
        End Function
        Friend Shared Function AssembleContract(ByVal sourcePersistentObject As DAL.ArStuEnrollment) As BO.StudentEnrollment
            Dim targetBusObject As New BO.StudentEnrollment
            With targetBusObject
                .LDA = sourcePersistentObject.LDA
                .ExpGradDate = sourcePersistentObject.ExpGradDate
                .StartDate = sourcePersistentObject.StartDate

            End With
            Return targetBusObject
        End Function
    End Class
End Namespace

