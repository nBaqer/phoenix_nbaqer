﻿
Imports System.Collections.Generic
Imports System.Linq
Imports DAL = Advantage.Data.ORM
Imports BO = Advantage.Business.Objects

Namespace Advantage.Business.Logic.Layer.Assemblers
    Friend NotInheritable Class UserAssembler
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.SyUser)) As List(Of BO.User)
            Dim targetBusObjects As List(Of BO.User) = New List(Of BO.User)
            For Each sourceObj As DAL.SyUser In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return targetBusObjects
        End Function
        Friend Shared Function AssembleContract(ByVal sourcePersistentObject As DAL.SyUser) As BO.User
            Dim targetBusObject As New BO.User
            With targetBusObject
                .AccountActive = sourcePersistentObject.AccountActive
                .ShowDefaultCampus = sourcePersistentObject.ShowDefaultCampus
                .UserId = sourcePersistentObject.UserId
                .IsAdvantageSuperUser = sourcePersistentObject.IsAdvantageSuperUser
                .ModuleId = sourcePersistentObject.ModuleId
                .CampusId = sourcePersistentObject.CampusId
                .UserName = sourcePersistentObject.UserName
                If .UserRolesCampusGroups IsNot Nothing Then
                    .UserRolesCampusGroups = UserRoleCampusGroupAssembler.AssembleContracts(sourcePersistentObject.SyUsersRolesCampGrps)
                End If
                .ModuleCode = (New BO.Modules).getModuleCode(.ModuleId)
                Select Case .UserName.ToLower
                    Case Is = "sa"
                        .IsUserSA = True
                        .FullPermission = True
                    Case Is = "support"
                        .IsUserSuper = True
                        .FullPermission = True
                    Case Is = "super"
                        .IsUserSupport = True
                        .FullPermission = True
                End Select
            End With
            Return targetBusObject
        End Function
    End Class
    Friend NotInheritable Class RoleAssembler
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.SyRole)) As List(Of BO.Role)
            Dim targetBusObjects As List(Of BO.Role) = New List(Of BO.Role)
            For Each sourceObj As DAL.SyRole In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return targetBusObjects
        End Function
        Friend Shared Function AssembleContract(ByVal sourcePersistentObject As DAL.SyRole) As BO.Role
            Dim targetBusObject As New BO.Role
            With targetBusObject
                .Role = sourcePersistentObject.Role
                .RoleId = sourcePersistentObject.RoleId
                .SysRoleId = sourcePersistentObject.SysRoleId
            End With
            Return targetBusObject
        End Function
    End Class

    Friend NotInheritable Class CampusGroupAssembler
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.SyCampGrp)) As List(Of BO.CampusGroup)
            Dim targetBusObjects As List(Of BO.CampusGroup) = New List(Of BO.CampusGroup)
            For Each sourceObj As DAL.SyCampGrp In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return CType(targetBusObjects, List(Of BO.CampusGroup))
        End Function
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IEnumerable(Of DAL.SyCampGrp)) As List(Of BO.CampusGroup)
            Dim targetBusObjects As List(Of BO.CampusGroup) = New List(Of BO.CampusGroup)
            For Each sourceObj As DAL.SyCampGrp In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return CType(targetBusObjects, List(Of BO.CampusGroup))
        End Function
        Friend Shared Function AssembleContract(ByVal sourcePersistentObject As DAL.SyCampGrp) As BO.CampusGroup
            Dim targetBusObject As New BO.CampusGroup
            With targetBusObject
                .CampGrpCode = sourcePersistentObject.CampGrpCode
                .CampGrpDescrip = sourcePersistentObject.CampGrpDescrip
                .CampGrpId = sourcePersistentObject.CampGrpId
            End With
            Return targetBusObject
        End Function
    End Class

    Friend NotInheritable Class UserRoleCampusGroupAssembler
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.SyUsersRolesCampGrp)) As List(Of BO.UserRolesCampusGroup)
            Dim targetBusObjects As List(Of BO.UserRolesCampusGroup) = New List(Of BO.UserRolesCampusGroup)
            For Each sourceObj As DAL.SyUsersRolesCampGrp In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return CType(targetBusObjects, List(Of BO.UserRolesCampusGroup))
        End Function
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IEnumerable(Of DAL.SyUsersRolesCampGrp)) As List(Of BO.UserRolesCampusGroup)
            Dim targetBusObjects As List(Of BO.UserRolesCampusGroup) = New List(Of BO.UserRolesCampusGroup)
            For Each sourceObj As DAL.SyUsersRolesCampGrp In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return CType(targetBusObjects, List(Of BO.UserRolesCampusGroup))
        End Function
        Friend Shared Function AssembleContract(ByVal sourcePersistentObject As DAL.SyUsersRolesCampGrp) As BO.UserRolesCampusGroup
            Dim targetBusObject As New BO.UserRolesCampusGroup
            With targetBusObject
                .UserId = sourcePersistentObject.UserId
                .RoleId = sourcePersistentObject.RoleId
                .CampGrpId = sourcePersistentObject.CampGrpId
                .UserRoles = PageLevelPermissionAssembler.AssembleContracts(sourcePersistentObject.SyRole.SyRlsResLvls)
                .UserCampuses = CampusAssembler.AssembleContracts(sourcePersistentObject.SyCampGrp.SyCmpGrpCmps)
            End With
            Return targetBusObject
        End Function
    End Class
    Friend NotInheritable Class ModulesAssembler
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.SyModule)) As List(Of BO.Modules)
            Dim targetBusObjects As List(Of BO.Modules) = New List(Of BO.Modules)
            For Each sourceObj As DAL.SyModule In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return CType(targetBusObjects, List(Of BO.Modules))
        End Function
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IEnumerable(Of DAL.SyModule)) As List(Of BO.Modules)
            Dim targetBusObjects As List(Of BO.Modules) = New List(Of BO.Modules)
            For Each sourceObj As DAL.SyModule In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return CType(targetBusObjects, List(Of BO.Modules))
        End Function
        Friend Shared Function AssembleContract(ByVal sourcePersistentObject As DAL.SyModule) As BO.Modules
            Dim targetBusObject As New BO.Modules
            With targetBusObject
                .ModuleCode = sourcePersistentObject.ModuleCode
            End With
            Return targetBusObject
        End Function
    End Class

    Friend NotInheritable Class PageLevelPermissionAssembler
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.SyRlsResLvl)) As List(Of BO.PageLevelPermission)
            Dim targetBusObjects As List(Of BO.PageLevelPermission) = New List(Of BO.PageLevelPermission)
            For Each sourceObj As DAL.SyRlsResLvl In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return CType(targetBusObjects, List(Of BO.PageLevelPermission))
        End Function
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IEnumerable(Of DAL.SyRlsResLvl)) As List(Of BO.PageLevelPermission)
            Dim targetBusObjects As List(Of BO.PageLevelPermission) = New List(Of BO.PageLevelPermission)
            For Each sourceObj As DAL.SyRlsResLvl In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return CType(targetBusObjects, List(Of BO.PageLevelPermission))
        End Function
        Friend Shared Function AssembleContract(ByVal sourcePersistentObject As Object) As BO.PageLevelPermission
            Dim targetBusObject As New BO.PageLevelPermission
            With targetBusObject
                .ResourceID = sourcePersistentObject.ResourceID
                .AccessLevel = sourcePersistentObject.AccessLevel
            End With
            Return targetBusObject
        End Function
    End Class
    Friend NotInheritable Class UserRoleResourceAccessLevelsAssembler
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.SyUsersRolesCampGrp)) As List(Of BO.UserRolesCampusGroup)
            Dim targetBusObjects As List(Of BO.UserRolesCampusGroup) = New List(Of BO.UserRolesCampusGroup)
            For Each sourceObj As DAL.SyUsersRolesCampGrp In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return CType(targetBusObjects, List(Of BO.UserRolesCampusGroup))
        End Function
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IEnumerable(Of DAL.SyUsersRolesCampGrp)) As List(Of BO.UserRolesCampusGroup)
            Dim targetBusObjects As List(Of BO.UserRolesCampusGroup) = New List(Of BO.UserRolesCampusGroup)
            For Each sourceObj As DAL.SyUsersRolesCampGrp In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return CType(targetBusObjects, List(Of BO.UserRolesCampusGroup))
        End Function
        Friend Shared Function AssembleContract(ByVal sourcePersistentObject As DAL.SyUsersRolesCampGrp) As BO.UserRolesCampusGroup
            Dim targetBusObject As New BO.UserRolesCampusGroup
            With targetBusObject
                .UserId = sourcePersistentObject.UserId
                .RoleId = sourcePersistentObject.RoleId
                .CampGrpId = sourcePersistentObject.CampGrpId
            End With
            Return targetBusObject
        End Function
    End Class

    Friend NotInheritable Class CampusAssembler
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.SyCmpGrpCmp)) As List(Of BO.CampusAndCampusGroup)
            Dim targetBusObjects As List(Of BO.CampusAndCampusGroup) = New List(Of BO.CampusAndCampusGroup)
            For Each sourceObj As DAL.SyCmpGrpCmp In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return CType(targetBusObjects, List(Of BO.CampusAndCampusGroup))
        End Function
        Friend Shared Function AssembleContracts(ByVal sourcePersistentObjects As IEnumerable(Of DAL.SyCmpGrpCmp)) As List(Of BO.CampusAndCampusGroup)
            Dim targetBusObjects As List(Of BO.CampusAndCampusGroup) = New List(Of BO.CampusAndCampusGroup)
            For Each sourceObj As DAL.SyCmpGrpCmp In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return CType(targetBusObjects, List(Of BO.CampusAndCampusGroup))
        End Function
        Friend Shared Function AssembleContract(ByVal sourcePersistentObject As DAL.SyCmpGrpCmp) As BO.CampusAndCampusGroup
            Dim targetBusObject As New BO.CampusAndCampusGroup
            With targetBusObject
                .CampusId = sourcePersistentObject.CampusId
            End With
            Return targetBusObject
        End Function
    End Class
End Namespace


