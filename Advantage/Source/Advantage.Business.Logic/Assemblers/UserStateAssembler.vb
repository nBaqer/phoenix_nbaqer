﻿
Imports System.Collections.Generic
Imports System.Linq
Imports DAL = Advantage.Data.ORM
Imports BO = Advantage.Business.Objects

Namespace Advantage.Business.Logic.Layer.Assemblers
    Public Class UserStateAssemblerBaseClass(Of D, U)
        Public Overridable Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of D)) As List(Of U)
            Dim targetBusObjects As List(Of U) = New List(Of U)
            For Each sourceObj As D In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return targetBusObjects
        End Function
        Public Overridable Function AssembleContracts(ByVal sourcePersistentObjects As IEnumerable(Of D)) As List(Of U)
            Dim targetBusObjects As List(Of U) = New List(Of U)
            For Each sourceObj As D In sourcePersistentObjects
                targetBusObjects.Add(AssembleContract(sourceObj))
            Next
            Return targetBusObjects
        End Function
        Public Overridable Function AssembleContract(ByVal sourcePersistentObject As D) As U
        End Function
    End Class
    Public Class UserStateAssembler
        Inherits UserStateAssemblerBaseClass(Of DAL.SyUser, BO.User)
        Dim mruProvider As MRURoutines
        Dim _connString As String
        Public Sub New(Optional ByVal ConnectionString As String = "")
            _connString = ConnectionString
        End Sub
        Public Overrides Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.SyUser)) As List(Of BO.User)
            Return MyBase.AssembleContracts(sourcePersistentObjects)
        End Function
        Public Overrides Function AssembleContract(ByVal sourcePersistentObject As DAL.SyUser) As BO.User
            Dim targetBusObject As New BO.User
            With targetBusObject
                .AccountActive = sourcePersistentObject.AccountActive
                .ShowDefaultCampus = sourcePersistentObject.ShowDefaultCampus
                .UserId = sourcePersistentObject.UserId
                .IsAdvantageSuperUser = sourcePersistentObject.IsAdvantageSuperUser
                .ModuleId = sourcePersistentObject.ModuleId

                If sourcePersistentObject.CampusId Is Nothing Then
                    .CampusId = System.Guid.Empty
                Else
                    .CampusId = sourcePersistentObject.CampusId
                End If

                'Dim query As Object = dbContext.UspGetDefaultCampusforaGivenUser(sourcePersistentObject.UserId)
                'If Not query.ToString() Is Nothing Then
                '    .CampusId = New Guid(query.ToString())
                'End If
                .UserName = sourcePersistentObject.UserName
                If .UserRolesCampusGroups IsNot Nothing Then
                    .UserRolesCampusGroups = (New UserRoleCampusGroupStateAssembler).AssembleContracts(sourcePersistentObject.SyUsersRolesCampGrps)
                    ' .CampusId = New Guid(.UserRolesCampusGroups(0).UserCampuses(0).CampusId.ToString)
                End If

                .ModuleCode = (New BO.Modules).getModuleCode(.ModuleId)
                Select Case .UserName.ToLower
                    Case Is = "sa"
                        .FullPermission = True
                    Case Is = "support"
                        .IsUserSupport = True
                        .FullPermission = True
                    Case Is = "super"
                        .IsUserSuper = True
                        .FullPermission = True
                End Select
            End With
            Return targetBusObject
        End Function
    End Class
    Public Class NavigationStateAssembler
        Inherits UserStateAssemblerBaseClass(Of DAL.SyResource, BO.Resources)

        Public Overrides Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.SyResource)) As List(Of BO.Resources)
            Return MyBase.AssembleContracts(sourcePersistentObjects)
        End Function
        Public Overrides Function AssembleContracts(ByVal sourcePersistentObjects As IEnumerable(Of DAL.SyResource)) As List(Of Objects.Resources)
            Return MyBase.AssembleContracts(sourcePersistentObjects)
        End Function
        Public Overrides Function AssembleContract(ByVal sourcePersistentObject As DAL.SyResource) As BO.Resources
            Dim targetBusObject As New BO.Resources
            With targetBusObject
                .ResourceID = sourcePersistentObject.ResourceID
                .ResourceTypeID = sourcePersistentObject.ResourceTypeID
                .ResourceURL = sourcePersistentObject.ResourceURL
                .Resource = sourcePersistentObject.Resource
                .NavigationNodes = (New NavigationNodesParentStateAssembler).AssembleContracts(sourcePersistentObject.SyNavigationNodes)
            End With
            Return targetBusObject
        End Function
    End Class
    Public Class NavigationNodesParentStateAssembler
        Inherits UserStateAssemblerBaseClass(Of DAL.SyNavigationNode, BO.NavigationNodes)
        Public Overrides Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.SyNavigationNode)) As List(Of BO.NavigationNodes)
            Return MyBase.AssembleContracts(sourcePersistentObjects)
        End Function
        Public Overrides Function AssembleContracts(ByVal sourcePersistentObjects As IEnumerable(Of DAL.SyNavigationNode)) As List(Of Objects.NavigationNodes)
            Return MyBase.AssembleContracts(sourcePersistentObjects)
        End Function
        Public Overrides Function AssembleContract(ByVal sourcePersistentObject As DAL.SyNavigationNode) As BO.NavigationNodes
            Dim targetBusObject As New BO.NavigationNodes
            With targetBusObject
                .ResourceId = sourcePersistentObject.ResourceId
                .HierarchyId = sourcePersistentObject.HierarchyId
                .ParentId = sourcePersistentObject.ParentId
                .ChildNavigationNodes = (New NavigationNodesChildStateAssembler).AssembleContracts(sourcePersistentObject.SyNavigationNodes.Where(Function(s) s.HierarchyId.ToString.Equals(s.ParentId.ToString)))
            End With
            Return targetBusObject
        End Function
    End Class
    Public Class NavigationNodesChildStateAssembler
        Inherits UserStateAssemblerBaseClass(Of DAL.SyNavigationNode, BO.NavigationNodes)
        Public Overrides Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.SyNavigationNode)) As List(Of BO.NavigationNodes)
            Return MyBase.AssembleContracts(sourcePersistentObjects)
        End Function
        Public Overrides Function AssembleContracts(ByVal sourcePersistentObjects As IEnumerable(Of DAL.SyNavigationNode)) As List(Of Objects.NavigationNodes)
            Return MyBase.AssembleContracts(sourcePersistentObjects)
        End Function
        Public Overrides Function AssembleContract(ByVal sourcePersistentObject As DAL.SyNavigationNode) As BO.NavigationNodes
            Dim targetBusObject As New BO.NavigationNodes
            With targetBusObject
                .ResourceId = sourcePersistentObject.ResourceId
                .HierarchyId = sourcePersistentObject.HierarchyId
                .ParentId = sourcePersistentObject.ParentId
            End With
            Return targetBusObject
        End Function
    End Class
    Public Class UserRoleCampusGroupStateAssembler
        Inherits UserStateAssemblerBaseClass(Of DAL.SyUsersRolesCampGrp, BO.UserRolesCampusGroup)
        Public Overrides Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.SyUsersRolesCampGrp)) As List(Of BO.UserRolesCampusGroup)
            Return MyBase.AssembleContracts(sourcePersistentObjects.OrderBy(Function(s) s.CampGrpDescrip))
        End Function
        Public Overrides Function AssembleContracts(ByVal sourcePersistentObjects As IEnumerable(Of DAL.SyUsersRolesCampGrp)) As List(Of BO.UserRolesCampusGroup)
            Return MyBase.AssembleContracts(sourcePersistentObjects.OrderBy(Function(s) s.CampGrpDescrip))
        End Function
        Public Overrides Function AssembleContract(ByVal sourcePersistentObject As DAL.SyUsersRolesCampGrp) As BO.UserRolesCampusGroup
            Dim targetBusObject As New BO.UserRolesCampusGroup
            With targetBusObject
                .UserId = sourcePersistentObject.UserId
                .RoleId = sourcePersistentObject.RoleId
                .CampGrpId = sourcePersistentObject.CampGrpId
                .CampGrpDescrip = sourcePersistentObject.CampGrpDescrip
                .UserRoles = (New PageLevelPermissionStateAssembler).AssembleContracts(sourcePersistentObject.SyRole.SyRlsResLvls)
                .UserCampuses = (New CampusStateAssembler).AssembleContracts(sourcePersistentObject.SyCampGrp.SyCmpGrpCmps)
            End With
            Return targetBusObject
        End Function
    End Class
    Public Class PageLevelPermissionStateAssembler
        Inherits UserStateAssemblerBaseClass(Of DAL.SyRlsResLvl, BO.PageLevelPermission)
        Public Overrides Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.SyRlsResLvl)) As List(Of BO.PageLevelPermission)
            Return MyBase.AssembleContracts(sourcePersistentObjects)
        End Function
        Public Overrides Function AssembleContracts(ByVal sourcePersistentObjects As IEnumerable(Of DAL.SyRlsResLvl)) As List(Of BO.PageLevelPermission)
            Return MyBase.AssembleContracts(sourcePersistentObjects)
        End Function
        Public Overrides Function AssembleContract(ByVal sourcePersistentObject As DAL.SyRlsResLvl) As Objects.PageLevelPermission
            Dim targetBusObject As New BO.PageLevelPermission
            With targetBusObject
                .RoleId = sourcePersistentObject.RoleId
                .ResourceID = sourcePersistentObject.ResourceID
                .AccessLevel = sourcePersistentObject.AccessLevel
            End With
            Return targetBusObject
        End Function
    End Class
    Public Class CampusStateAssembler
        Inherits UserStateAssemblerBaseClass(Of DAL.SyCmpGrpCmp, BO.CampusAndCampusGroup)
        Public Overrides Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.SyCmpGrpCmp)) As List(Of BO.CampusAndCampusGroup)
            Return MyBase.AssembleContracts(sourcePersistentObjects)
        End Function
        Public Overrides Function AssembleContracts(ByVal sourcePersistentObjects As IEnumerable(Of DAL.SyCmpGrpCmp)) As List(Of BO.CampusAndCampusGroup)
            Return MyBase.AssembleContracts(sourcePersistentObjects)
        End Function
        Public Overrides Function AssembleContract(ByVal sourcePersistentObject As DAL.SyCmpGrpCmp) As Objects.CampusAndCampusGroup
            Dim targetBusObject As New BO.CampusAndCampusGroup
            With targetBusObject
                .CampusId = sourcePersistentObject.CampusId
            End With
            Return targetBusObject
        End Function
    End Class
    'Public Class CampusLevelAssembler
    '    Inherits UserStateAssemblerBaseClass(Of DAL.SyCampuse, BO.Campuses)
    '    Public Overrides Function AssembleContracts(ByVal sourcePersistentObjects As IQueryable(Of DAL.SyCampuse)) As List(Of BO.Campuses)
    '        Return MyBase.AssembleContracts(sourcePersistentObjects)
    '    End Function
    '    Public Overrides Function AssembleContracts(ByVal sourcePersistentObjects As IEnumerable(Of DAL.SyCampuse)) As List(Of BO.Campuses)
    '        Return MyBase.AssembleContracts(sourcePersistentObjects)
    '    End Function
    '    Public Overrides Function AssembleContract(ByVal sourcePersistentObject As DAL.SyCampuse) As Objects.Campus
    '        Dim targetBusObject As New BO.Campus
    '        With targetBusObject
    '            .CampusId = sourcePersistentObject.CampusId
    '            .CampusDescription = sourcePersistentObject.CampDescrip
    '        End With
    '        Return targetBusObject
    '    End Function
    'End Class
End Namespace
