﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports Advantage.Business.Objects
Imports BO = Advantage.Business.Objects
Imports CO = FAME.AdvantageV1.Common
Imports System.Web
Imports FAME.AdvantageV1.Common
Namespace Advantage.Business.Logic.Layer
    Public Class MRURoutines
        Inherits IDataProvider
#Region "Constructor"
        Public Sub New(ByVal context As HttpContext, ByVal connectionString As String)
            MyBase.New(context, connectionString)
        End Sub
        Public Function BuildMRUEntitiesListForNonSAUser(ByVal CurrentUserState As BO.User) As List(Of Integer)
            Dim iMRUType As New List(Of Integer)
            Dim strUserRoles As String = ""
            If CurrentUserState.FullPermission = True Then 'sa or SuperUser, user can see all entities - Lead,Student, Employer, Employee, etc
                With iMRUType
                    .Add(1)
                    .Add(2)
                    .Add(3)
                    .Add(4)
                    '.Add(5)
                End With
                Return iMRUType
            Else 'Non Special User
                'Dim strPreviousRoleId As String = ""
                'For Each item In CurrentUserState.UserRoles.OrderBy(Function(s) s.RoleId.ToString)
                '    If Not strPreviousRoleId.ToString.Trim = item.RoleId.ToString.Trim Then
                '        strUserRoles &= item.RoleId.ToString & ","
                '    End If
                '    strPreviousRoleId = item.RoleId.ToString
                'Next
                'strUserRoles = strUserRoles.Substring(0, InStrRev(strUserRoles, ",") - 1)
                Dim dtObj As Object = dbContext.USPBuildMRUEntitiesByRoleAndCampus(CurrentUserState.UserId.ToString)
                For Each item In dtObj
                    If Not iMRUType.Contains(item(3)) Then
                        iMRUType.Add(item(3))
                    End If
                Next
                Return iMRUType
            End If
        End Function
        Public Function BuildStudentMRU(ByVal CurrentUserState As BO.User) As List(Of BO.StudentMRU)
            'Dim dtObj As Object = dbContext.USPMaintenanceAll(intSchoolOption)
            Dim dtObj As Object = dbContext.UspGetStudentMRU(New Guid(CurrentUserState.UserId.ToString), New Guid(CurrentUserState.CampusId.ToString))
            Dim lstStudentMRUs As New List(Of BO.StudentMRU)
            For Each item In dtObj

                lstStudentMRUs.Add(New BO.StudentMRU() With { _
                                                         .Id = item(0).ToString & ":" & item(5).ToString, _
                                                         .Name = item(9), _
                                                         .Counter = item(1), _
                                                         .ChildId = item(2), _
                                                         .Type = item(3), _
                                                         .UserId = item(4), _
                                                         .CampusId = item(5),
                                                         .CampusName = item(10), _
                                                         .SortOrder = item(6), _
                                                         .IsSticky = item(7), _
                                                         .StudentId = item(8), _
                                                         .LeadId = item(11), _
                                                         .ProgramVersionDesc = item(13), _
                                                         .EnrollmentStatus = item(14)})

                'Order from database
                'MRUId (0),
                '[Counter](1),
                'ChildId(2),
                'MRUTypeId (3),
                'UserId(4),
                'CampusId, 5
                'SortOrder,6
                'IsSticky,7
                'StudentId,8
                'FullName,9
                'CampusDescrip,10
                'LeadId'11
            Next
            Return lstStudentMRUs
        End Function
        Public Function BuildLeadMRU(ByVal CurrentUserState As BO.User) As List(Of BO.LeadMRU)
            'Dim dtObj As Object = dbContext.USPMaintenanceAll(intSchoolOption)
            Dim dtObj As Object = dbContext.UspGetLeadMRU(New Guid(CurrentUserState.UserId.ToString), New Guid(CurrentUserState.CampusId.ToString))
            Dim lstLeadMRUs As New List(Of BO.LeadMRU)
            For Each item In dtObj

                lstLeadMRUs.Add(New BO.LeadMRU() With { _
                                                         .Id = item(0).ToString, _
                                                         .Name = item(9), _
                                                         .Counter = item(1), _
                                                         .ChildId = item(2), _
                                                         .Type = item(3), _
                                                         .UserId = item(4), _
                                                         .CampusId = item(5),
                                                         .CampusName = item(10), _
                                                         .SortOrder = item(6), _
                                                         .IsSticky = item(7), _
                                                         .LeadId = item(8), _
                                                         .ProgramVersionDesc = item(12), _
                                                         .EnrollmentStatus = item(13)})

            Next
            Return lstLeadMRUs
        End Function
        Public Function BuildEmployerMRU(ByVal CurrentUserState As BO.User) As List(Of BO.EmployerMRU)
            'Dim dtObj As Object = dbContext.USPMaintenanceAll(intSchoolOption)
            Dim dtObj As Object = dbContext.UspGetEmployerMRU(New Guid(CurrentUserState.UserId.ToString), New Guid(CurrentUserState.CampusId.ToString))
            Dim lstEmployerMRUs As New List(Of BO.EmployerMRU)
            For Each item In dtObj

                lstEmployerMRUs.Add(New BO.EmployerMRU() With { _
                                                         .Id = item(0).ToString, _
                                                         .Name = item(9), _
                                                         .Counter = item(1), _
                                                         .ChildId = item(2), _
                                                         .Type = item(3), _
                                                         .UserId = item(4), _
                                                         .CampusId = item(5),
                                                         .CampusName = item(10), _
                                                         .SortOrder = item(6), _
                                                         .IsSticky = item(7), _
                                                         .EmployerId = item(8), _
                                                          .ProgramVersionDesc = "", _
                                                         .EnrollmentStatus = ""
                                                         })

            Next
            Return lstEmployerMRUs
        End Function
        Public Function BuildEmployeeMRU(ByVal CurrentUserState As BO.User) As List(Of BO.EmployeeMRU)
            'Dim dtObj As Object = dbContext.USPMaintenanceAll(intSchoolOption)
            Dim dtObj As Object = dbContext.UspGetEmployeeMRU(New Guid(CurrentUserState.UserId.ToString), New Guid(CurrentUserState.CampusId.ToString))
            Dim lstEmployeeMRUs As New List(Of BO.EmployeeMRU)
            For Each item In dtObj

                lstEmployeeMRUs.Add(New BO.EmployeeMRU() With { _
                                                         .Id = item(0).ToString, _
                                                         .Name = item(9), _
                                                         .Counter = item(1), _
                                                         .ChildId = item(2), _
                                                         .Type = item(3), _
                                                         .UserId = item(4), _
                                                         .CampusId = item(5),
                                                         .CampusName = item(10), _
                                                         .SortOrder = item(6), _
                                                         .IsSticky = item(7), _
                                                         .EmployeeId = item(8), _
                                                         .ProgramVersionDesc = "", _
                                                         .EnrollmentStatus = ""
                                                         })

            Next
            Return lstEmployeeMRUs
        End Function

        Public Function BuildStudentStatusBar(ByVal studentId As String, ByVal campusId As String, Optional ByVal stuEnrollId As String = "") As AdvantageStateInfo
            Dim dtObj As Object
            If stuEnrollId = "" Then
                dtObj = dbContext.USPGetStudentDetailsForStatusBar(New Guid(studentId.ToString), New Guid(campusId.ToString), Nothing)
            Else
                dtObj = dbContext.USPGetStudentDetailsForStatusBar(New Guid(studentId.ToString), New Guid(campusId.ToString), New Guid(stuEnrollId.ToString))
            End If
            Dim objAdvantageStateInfo As New AdvantageStateInfo
            For Each item In dtObj
                With objAdvantageStateInfo
                    .NameValue = item(0).ToString
                    .CampusDescrip = item(1).ToString
                    .PrgVerDescrip = item(2).ToString
                    .PrgVerTypeDescrip = item(3).ToString
                    .PrgVerExtendedDescrip = .PrgVerDescrip & IIf(.PrgVerTypeDescrip <> "", " (" & .PrgVerTypeDescrip & ")", "")
                    If IsDate(item(4)) Then
                        .StartDate = item(4)
                    End If
                    If IsDate(item(5)) Then
                        .GradDate = item(5)
                    End If

                    .EnrollmentStatus = item(6).ToString

                    If Not DBNull.Value.Equals(item(7)) And Not IsNothing(item(7)) Then
                        .StudentIdentifier = item(7).ToString
                    Else
                        .StudentIdentifier = ""
                    End If

                    .StudentIdentifierCaption = item(8).ToString
                    If IsNumeric(item(9)) Then
                        .SystemStatus = item(9)
                    End If
                    .PrgVerId = item(10).ToString
                    .StudentId = item(11).ToString
                    If Not IsNothing(item(12)) Then
                        .LeadId = item(12).ToString
                    End If
                    .CampusId = item(13).ToString
                    .StuEnrollId = item(14).ToString
                    .EnrollmentDetails = BuildActiveEnrollmentDetailsForStatusBar(studentId.ToString, campusId.ToString())

                    If Not DBNull.Value.Equals(item(15)) And Not IsNothing(item(15)) Then
                        .TitleIVStatustatus = item(15).ToString
                    Else
                        .TitleIVStatustatus = ""
                    End If

                End With
            Next
            Return objAdvantageStateInfo
        End Function

        Public Function BuildLeadStatusBar(ByVal leadId As String) As AdvantageStateInfo
            If leadId.Trim = "" Then Return Nothing
            Dim dtObj As Object = dbContext.USPGetLeadDetailsForStatusBar(New Guid(leadId.ToString))
            Dim objAdvantageStateInfo As New AdvantageStateInfo
            For Each item In dtObj
                With objAdvantageStateInfo
                    .NameValue = item(0)
                    .LeadId = item(1).ToString
                    .PrgVerDescrip = item(2)
                    If IsDate(item(3)) Then
                        .StartDate = item(3)
                    End If
                    If IsDate(item(4)) Then
                        .GradDate = item(4)
                    End If
                    .EnrollmentStatus = item(5)
                    If Not IsNothing(item(6)) Then
                        .StudentId = item(6).ToString
                        .EnrollmentDetails = BuildActiveEnrollmentDetailsForStatusBar(.StudentId)
                    End If
                    If IsDate(item(7)) Then
                        .AssignedDate = item(7)
                    End If
                    .AdmissionsRep = item(8).ToString
                    .CampusDescrip = item(9).ToString
                    .CampusId = item(10).ToString
                    If IsNumeric(item(11)) Then
                        .SystemStatus = item(11)
                    End If
                End With
            Next
            Return objAdvantageStateInfo
        End Function
        Public Function BuildEmployerStatusBar(ByVal employerId As String) As AdvantageStateInfo
            Dim dtObj As Object = dbContext.USPGetEmployerDetailsForStatusBar(New Guid(employerId.ToString))
            Dim objAdvantageStateInfo As New AdvantageStateInfo

            For Each item In dtObj
                With objAdvantageStateInfo
                    .NameValue = item(0)
                    .EmployerId = item(1).ToString
                    .Address1 = If(item(2) Is Nothing, "", item(2).ToString())

                    .Address2 = If(item(3) Is Nothing, "", item(3).ToString())
                    .City = If(item(4) Is Nothing, "", item(4).ToString())
                    .State = If(item(6) Is Nothing, "", item(6).ToString())
                    .CampusId = If(item(7) Is Nothing, "", item(7).ToString())
                    .CampusDescrip = If(item(8) Is Nothing, "", item(8).ToString())
                    .Zip = If(item(9) Is Nothing, "", item(9).ToString())

                    .Phone = item(10)
                End With
            Next
            Return objAdvantageStateInfo
        End Function
        Public Function BuildEmployeeStatusBar(ByVal employeeId As String) As AdvantageStateInfo
            Dim dtObj As Object = dbContext.USPGetEmployeeDetailsForStatusBar(New Guid(employeeId.ToString))
            Dim objAdvantageStateInfo As New AdvantageStateInfo
            For Each item In dtObj
                With objAdvantageStateInfo
                    .NameValue = item(0)
                    .EmployeeId = item(1).ToString
                    .Address1 = If(item(4) Is Nothing, "", item(4).ToString())
                    .Address2 = If(item(5) Is Nothing, "", item(5).ToString())
                    .City = If(item(6) Is Nothing, "", item(6).ToString())
                    .State = If(item(7) Is Nothing, "", item(7).ToString())
                    .Zip = If(item(8) Is Nothing, "", item(8).ToString())
                    .CampusId = If(item(9) Is Nothing, "", item(9).ToString())
                End With
            Next
            Return objAdvantageStateInfo
        End Function

        Public Function BuildActiveEnrollmentDetailsForStatusBar(ByVal studentId As String) As List(Of EnrollmentStateInfo)
            Dim dtObj As Object = dbContext.USPGetStudentEnrollmentDetailsForStatusBar(New Guid(studentId.ToString))
            Dim lstEnrollmentDetails As New List(Of EnrollmentStateInfo)
            For Each item In dtObj

                lstEnrollmentDetails.Add(New EnrollmentStateInfo() With { _
                                                         .CampusDescription = item(0).ToString,
                                                         .PrgVerDescrip = item(1).ToString,
                                                         .PrgVerTypeDescrip = item(2).ToString,
                                                         .PrgVerExtendedDescrip = .PrgVerDescrip & IIf(.PrgVerTypeDescrip <> "", " (" & .PrgVerTypeDescrip & ")", ""),
                                                         .StartDate = IIf(IsDate(item(3)), item(3), Nothing),
                                                         .GradDate = IIf(IsDate(item(4)), item(4), Nothing),
                                                         .EnrollmentStatus = item(5).ToString,
                                                         .SystemStatus = IIf(IsNumeric(item(6)), item(6), Nothing),
                                                         .PrgVerId = item(7).ToString,
                                                         .CampusId = item(8).ToString,
                                                         .StuEnrollId = item(9).ToString})
            Next
            Return lstEnrollmentDetails
        End Function

        Public Function BuildActiveEnrollmentDetailsForStatusBar(ByVal studentId As String, ByVal campusId As String) As List(Of EnrollmentStateInfo)
            Dim dtObj As Object = dbContext.USPGetStudentEnrollmentDetailsForStatusBar(New Guid(studentId.ToString), New Guid(campusId.ToString))
            Dim lstEnrollmentDetails As New List(Of EnrollmentStateInfo)
            For Each item In dtObj

                lstEnrollmentDetails.Add(New EnrollmentStateInfo() With { _
                                                         .CampusDescription = item(0).ToString,
                                                         .PrgVerDescrip = item(1).ToString,
                                                         .PrgVerTypeDescrip = item(2).ToString,
                                                         .PrgVerExtendedDescrip = .PrgVerDescrip & IIf(.PrgVerTypeDescrip <> "", " (" & .PrgVerTypeDescrip & ")", ""),
                                                         .StartDate = IIf(IsDate(item(3)), item(3), Nothing),
                                                         .GradDate = IIf(IsDate(item(4)), item(4), Nothing),
                                                         .EnrollmentStatus = item(5).ToString,
                                                         .SystemStatus = IIf(IsNumeric(item(6)), item(6), Nothing),
                                                         .PrgVerId = item(7).ToString,
                                                         .CampusId = item(8).ToString,
                                                         .StuEnrollId = item(9).ToString})
            Next
            Return lstEnrollmentDetails
        End Function

        Public Sub UpdateMRUList(ByVal EntityId As String, _
                                                             ByVal UserId As String, _
                                                             ByVal CampusId As String, _
                                                             ByVal ModUser As String)

            If Not EntityId.Length = 0 Then
                dbContext.USPUpdateMRUList(New Guid(UserId.ToString), _
                                    ModUser, _
                                    New Guid(EntityId.ToString), _
                                     New Guid(CampusId.ToString))
            End If
        End Sub
        Public Sub UpdateEmployerMRUList(ByVal EntityId As String, _
                                                         ByVal UserId As String, _
                                                         ByVal CampusId As String, _
                                                         ByVal ModUser As String)

            If Not EntityId.Length = 0 Then
                dbContext.USPUpdateEmployerMRUList(New Guid(UserId.ToString), _
                                    ModUser, _
                                    New Guid(EntityId.ToString), _
                                     New Guid(CampusId.ToString))
            End If
        End Sub
        Public Sub UpdateEmployeeMRUList(ByVal EntityId As String, _
                                                            ByVal UserId As String, _
                                                            ByVal CampusId As String, _
                                                            ByVal ModUser As String)

            If Not EntityId.Length = 0 Then
                dbContext.USPUpdateEmployeeMRUList(New Guid(UserId.ToString), _
                                    ModUser, _
                                    New Guid(EntityId.ToString), _
                                     New Guid(CampusId.ToString))
            End If
        End Sub
        Public Function getLastEntityUserWorkedWith(ByVal UserId As String, _
                                                    ByVal MRUTypeId As Int32,
                                                    ByVal campusId As String) As String
            Dim dtObj As Object = dbContext.USPGetLastEntityUserWorkedWithGetList(New Guid(UserId), _
                                                                                  MRUTypeId, _
                                                                              New Guid(campusId))
            Dim strEntityId As String = ""
            For Each item In dtObj
                strEntityId = item(0).ToString
            Next
            Return strEntityId
        End Function
        Public Function getLastEmployeeEntityUserWorkedWith(ByVal UserId As String, _
                                                   ByVal MRUTypeId As Int32) As String
            Dim dtObj As Object = dbContext.USPGetLastEmployeeEntityUserWorkedWithGetList(New Guid(UserId), _
                                                                                  MRUTypeId)
            Dim strEntityId As String = ""
            For Each item In dtObj
                strEntityId = item(0).ToString
            Next
            Return strEntityId
        End Function
        Public Function getMRUEntityUserWorkedWith(ByVal Userid As String, _
                                                   ByVal MRUTypeId As Int32, _
                                                   ByVal campusId As String) As Object
            Dim dtObj As Object = dbContext.USPGetLastEntityUserWorkedWithGetList(New Guid(Userid), MRUTypeId, New Guid(campusId))
            Return dtObj
        End Function
        Public Function getStuEnrollmentDetails(ByVal LeadId As String) As String
            Dim strEnrollmentId_CampusId As String = ""
            Dim dtObj As Object = dbContext.USPGetStudentByLead(New Guid(LeadId))
            For Each item In dtObj
                strEnrollmentId_CampusId = item(0).ToString & ":" & item(1).ToString
            Next
            Return strEnrollmentId_CampusId
        End Function
        Public Function DeleteMRUFromList(ByVal MRUID As String) As String
            Dim dtObj As Object = dbContext.USPDeleteMRU(New Guid(MRUID.ToString))
            Dim strMRUTypeID As String = ""
            For Each item In dtObj
                If item(0) IsNot Nothing Then
                    strMRUTypeID = item(0).ToString
                End If
            Next
            Return strMRUTypeID
        End Function
        Public Function getURL(ByVal ResourceId As Integer) As String
            Dim dtObj As Object = dbContext.USPGetURL(ResourceId)
            Dim strURL As String = ""
            For Each item In dtObj
                strURL = item(0).ToString
            Next
            Return strURL
        End Function
        Public Sub InsertMRU(ByVal MRUTypeId As Integer, ByVal EntityId As String, ByVal UserId As String, ByVal CampusId As String)
            dbContext.USPInsertMRU(MRUTypeId, New Guid(EntityId.ToString), New Guid(UserId), New Guid(CampusId))
        End Sub
        Public Function IsUserAPowerUserInLoggedInCampus(ByVal UserId As System.Nullable(Of System.Guid), _
                                                         ByVal CampusId As System.Nullable(Of System.Guid)) As Integer
            Dim dtObj As Object = dbContext.IsUserAPowerUserInLoggedInCampus(UserId, CampusId)
            Dim intUserAPowerUser As Integer = 0
            For Each item In dtObj
                intUserAPowerUser = item(0).ToString
                Exit For
            Next
            Return intUserAPowerUser
        End Function

        Public Function GetEntityNameByEntityType(ByVal entityTypeId As System.Nullable(Of System.Guid), _
                                                         ByVal entityType As String) As String

            Dim dtObj As Object = dbContext.GetEntityNameByEntityType(entityTypeId, entityType)
            For Each item In dtObj
                Return item(0).ToString
            Next

            Return ""

        End Function

        Public Function getMruTypeForResource(ByVal resourceId As Int32) As Int32

            Dim intMRuType As Int32 = 0

            Dim dtMRuObject As Object = dbContext.GetMenuSettingsForResource(resourceId)
            For Each setting As Object In dtMRuObject
                Int32.TryParse(setting(0), intMRuType)

            Next

            Return intMRuType

        End Function

        Public Function getStatusBarStateForResource(ByVal resourceId As Int32) As Boolean

            Dim hideStatusBar As Boolean = False

            Dim dtMRuObject As Object = dbContext.GetMenuSettingsForResource(resourceId)
            For Each setting As Object In dtMRuObject
                Boolean.TryParse(setting(1), hideStatusBar)

            Next

            Return hideStatusBar

        End Function

#Region "Search Page Routines for MRU"
        Private Function getAllEntityPagesByModule(ByVal intModuleResourceId As Integer, _
                                                   ByVal CampusId As String, _
                                                   ByVal intTabId As Integer) As Object

            Return (dbContext.USPTabsPermissions(intModuleResourceId, intTabId))
        End Function
        Public Function getTest(ByVal UserId As String) As Object

            Return (dbContext.UspGetDefaultCampusforaGivenUser(New Guid(UserId)))
        End Function
        Public Function checkEntityPageSecurity(ByVal CurrentUserState As BO.User, _
                                                 ByVal intModuleResourceId As Integer, _
                                                 ByVal CampusId As String,
                                                 ByVal intTabId As Integer) As ArrayList

            Dim dtObj As Object = getAllEntityPagesByModule(intModuleResourceId, CampusId, intTabId)
            Dim uppInfo As New UserPagePermissionInfo
            Dim arrUserPagePermissions As New ArrayList
            For Each item In dtObj
                uppInfo = SecurityRoutines.CheckUserPermissionStudentPagesByCampusAndResource(CurrentUserState, _
                                                                                              item(2).ToString, _
                                                                                              CurrentUserState.CampusId.ToString, _
                                                                                              item(4).ToString)
                If Not uppInfo.HasNone Then
                    arrUserPagePermissions.Add(uppInfo)
                End If
            Next
            Return arrUserPagePermissions
        End Function
        Public Function checkEntityPageSecurityStudentSearch(ByVal CurrentUserState As BO.User, _
                                                 ByVal intModuleResourceId As Integer, _
                                                 ByVal CampusId As String,
                                                 ByVal intTabId As Integer, ByVal intSchoolOption As Integer) As ArrayList

            Dim dtObj As Object = BuildTabsByModuleGetListPermission(CampusId, intSchoolOption, intModuleResourceId, intTabId) 'getAllEntityPagesByModule(intModuleResourceId, CampusId, intTabId)
            Dim uppInfo As New UserPagePermissionInfo
            Dim arrUserPagePermissions As New ArrayList
            For Each item In dtObj
                uppInfo = SecurityRoutines.CheckUserPermissionStudentPagesByCampusAndResource(CurrentUserState, _
                                                                                              item(2).ToString, _
                                                                                              CurrentUserState.CampusId.ToString, _
                                                                                              item(4).ToString)
                If Not uppInfo.HasNone Then
                    arrUserPagePermissions.Add(uppInfo)
                End If
            Next
            Return arrUserPagePermissions
        End Function
        Public Function BuildTabsByModuleGetListPermission(ByVal campusId As String, ByVal schoolEnumerator As Integer?, ByVal ModuleResourceId As Integer, ByVal TabId As Integer) As Object()
            Dim dtObj As Object = dbContext.UspAdvantageTabsByModuleGetListPermission(campusId, schoolEnumerator, ModuleResourceId, TabId)
            Return dtObj
        End Function
        Public Function DoesUserHasAccessToSubModuleResource(ByVal arrUserPagePermissions As ArrayList, ByVal resourceId As Integer) As Boolean
            Dim uppInfo As UserPagePermissionInfo
            'Check each object in the arraylist to see if the resource id matches the resource id passed in.
            'If there is a match return true.
            For Each uppInfo In arrUserPagePermissions
                If uppInfo.ResourceId = resourceId Then
                    Return True
                End If
            Next
            Return False
        End Function
        Public Function PermissionForAllCampuses(ByVal UserId As String) As Object()
            Dim dtObj As Object = dbContext.PermissionForAllCampuses(New Guid(UserId))
            Return dtObj
        End Function
        Public Function checkEntityPageSecurityStudentSearchAllCampuses(ByVal CurrentUserState As BO.User, _
                                                 ByVal intModuleResourceId As Integer, _
                                                 ByVal CampusId As String,
                                                 ByVal intTabId As Integer, ByVal intSchoolOption As Integer) As ArrayList

            Dim dtObj As Object = BuildTabsByModuleGetListPermission(CampusId, intSchoolOption, intModuleResourceId, intTabId) 'getAllEntityPagesByModule(intModuleResourceId, CampusId, intTabId)
            Dim uppInfo As New UserPagePermissionInfo
            Dim arrUserPagePermissions As New ArrayList
            For Each item In dtObj
                uppInfo = CheckUserPermissionStudentPagesByCampusAndResourceAllCampuses(CurrentUserState, _
                                                                                              item(2).ToString, _
                                                                                              CurrentUserState.CampusId.ToString, _
                                                                                              item(4).ToString)
                If Not uppInfo.HasNone Then
                    arrUserPagePermissions.Add(uppInfo)
                End If
            Next
            Return arrUserPagePermissions
        End Function
        Public Function CheckUserPermissionStudentPagesByCampusAndResourceAllCampuses(ByVal AdvantageUserState As BO.User, _
                                                            ByVal ResourceId As String, _
                                                            ByVal CampusId As String, _
                                                            ByVal pageURL As String) As CO.UserPagePermissionInfo
            Dim pObj As CO.UserPagePermissionInfo = New CO.UserPagePermissionInfo
            Try
                Select Case AdvantageUserState.FullPermission
                    Case Is = True
                        With pObj
                            .HasFull = True
                            .HasAdd = True
                            .HasDelete = True
                            .HasEdit = True
                            .HasNone = False
                            .ResourceId = ResourceId
                            .Url = pageURL
                        End With
                    Case Else
                        Dim allCampusPermission As List(Of BO.PageLevelPermission) = getResourceCampusPermissionAllCampuses(AdvantageUserState.UserId.ToString)
                        Dim result As IEnumerable(Of BO.PageLevelPermission) = allCampusPermission.Where(Function(s) s.ResourceID.ToString.Equals(ResourceId)).OrderByDescending(Function(p) p.AccessLevel).ToList()
                        If result.Count >= 1 Then

                            If result(0).PageLevelFullAccess = True Then pObj.HasFull = True
                            If result(0).PageLevelEditAccess = True Then pObj.HasEdit = True
                            If result(0).PageLevelCreateAccess = True Then pObj.HasAdd = True
                            If result(0).PageLevelDeleteAccess = True Then pObj.HasDelete = True
                            If result(0).PageLevelReadOnlyAccess = True And _
                                result(0).PageLevelEditAccess = False And _
                                result(0).PageLevelCreateAccess = False And _
                                result(0).PageLevelDeleteAccess = False Then pObj.HasDisplay = True
                            If result(0).PageLevelReadOnlyAccess = False And _
                             result(0).PageLevelEditAccess = False And _
                             result(0).PageLevelCreateAccess = False And _
                             result(0).PageLevelDeleteAccess = False Then
                                pObj.HasNone = True
                            Else
                                pObj.HasNone = False
                            End If

                            pObj.ResourceId = ResourceId
                            pObj.Url = pageURL
                        Else
                            pObj.HasNone = True
                            pObj.ResourceId = ResourceId
                        End If
                End Select
            Catch ex As Exception
                pObj.HasNone = True 'when it errors out, set permission to none
                pObj.ResourceId = ResourceId
            End Try
            Return pObj
        End Function
        Private Function getResourceCampusPermissionAllCampuses(ByVal UserId As String) As List(Of BO.PageLevelPermission)
            Dim dtObj As Object = dbContext.PermissionForAllCampuses(New Guid(UserId))
            Dim lstPageLevelPermissions As New List(Of BO.PageLevelPermission)
            For Each item In dtObj
                Dim addPageLevelAccess As New BO.PageLevelPermission() With _
                                  { _
                                     .ResourceID = item(0), _
                                     .AccessLevel = item(1), _
                                     .RoleId = Guid.Empty, _
                                     .PageLevelFullAccess = item(2), _
                                     .PageLevelEditAccess = item(3), _
                                     .PageLevelCreateAccess = item(4), _
                                     .PageLevelDeleteAccess = item(5), _
                                     .PageLevelReadOnlyAccess = item(6)}
                lstPageLevelPermissions.Add(addPageLevelAccess)
            Next
            Return lstPageLevelPermissions
        End Function
#End Region


#End Region
#Region "Get Default Campus"
        Public Function getDefaultCampusForGivenUser(ByVal UserId As String) As String
            Dim dtCampusObject As Object = dbContext.UspGetDefaultCampusforaGivenUser(New Guid(UserId))
            Dim strCampusId As String = ""
            For Each item In dtCampusObject
                strCampusId = item(0).ToString
            Next
            Return strCampusId
        End Function
        Public Function getCampusForStudent(ByVal StudentId As String) As String
            Dim dtCampusObject As Object = dbContext.USPGetCampusId(New Guid(StudentId))
            Dim strCampusId As String = ""
            For Each item In dtCampusObject
                strCampusId = item(0).ToString
            Next
            Return strCampusId
        End Function
        Public Function getCampusForLead(ByVal LeadId As String) As String
            Dim dtCampusObject As Object = dbContext.USPGetCampusIdForLead(New Guid(LeadId))
            Dim strCampusId As String = ""
            For Each item In dtCampusObject
                strCampusId = item(0).ToString
            Next
            Return strCampusId
        End Function
        Public Function getCampusForEmployee(ByVal EmployeeId As String) As String
            Dim dtCampusObject As Object = dbContext.USPGetCampusIdForEmployee(New Guid(EmployeeId))
            Dim strCampusId As String = ""
            For Each item In dtCampusObject
                strCampusId = item(0).ToString
            Next
            Return strCampusId
        End Function
#End Region

    End Class
End Namespace
