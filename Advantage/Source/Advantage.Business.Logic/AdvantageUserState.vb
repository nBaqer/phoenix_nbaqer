﻿
Imports System.Linq
Imports DAL = Advantage.Data.ORM
Imports System.Web
Namespace Advantage.Business.Logic.Layer
    ' "State"
    MustInherit Class UserState

        Protected _account As UserAccount
        Protected _userid As String
        Protected _Password As String

        'Properties
        Public MustOverride Function isUserActive(ByVal UserId As String) As Boolean
        Public MustOverride Function isSpecialUser(ByVal UserId As String) As Boolean
        Public MustOverride Function CanUserLogin() As Boolean
    End Class
    ' "Context"
    Class UserAccount
        Private _state As UserState
        Private _username As String
        Private _password As String
        Private _defaultcampusid As String
        Private _modulecode As String
        Private _returnMessage As String
        Private _returnURL As String
        Private _connectionString As String

        'Constructor
        Public Sub New()
            _state = New SuccessUserState(Me)
        End Sub
        Public Property UserName() As String
            Get
                Return _username
            End Get
            Set(ByVal Value As String)
                _username = Value
            End Set
        End Property
        Public Property Password() As String
            Get
                Return _password
            End Get
            Set(ByVal Value As String)
                _password = Value
            End Set
        End Property
        Public Property DefaultCampusId() As String
            Get
                Return _defaultcampusid
            End Get
            Set(ByVal value As String)
                _defaultcampusid = value
            End Set
        End Property
        Public Property ModuleCode() As String
            Get
                Return _modulecode
            End Get
            Set(ByVal value As String)
                _modulecode = value
            End Set
        End Property
        Public Property ReturnMessage() As String
            Get
                Return _returnMessage
            End Get
            Set(ByVal value As String)
                _returnMessage = value
            End Set
        End Property
        Public Property ReturnURL() As String
            Get
                Return _returnURL
            End Get
            Set(ByVal value As String)
                _returnURL = value
            End Set
        End Property
        Public Property UserState() As UserState
            Get
                Return _state
            End Get
            Set(ByVal value As UserState)
                _state = value
            End Set
        End Property
        Public Property ConnectionString() As String
            Get
                Return _connectionString
            End Get
            Set(ByVal value As String)
                _connectionString = value
            End Set
        End Property
        Public Function IsUserActive(ByVal UserId As String) As Boolean
            Return _state.isUserActive(UserId)
        End Function
        Public Function IsSpecialUser(ByVal UserId As String) As Boolean
            Return _state.isSpecialUser(UserId)
        End Function
        Public Function CanUserLogin() As Boolean
            Return _state.CanUserLogin
        End Function
    End Class
    Class SuccessUserState
        Inherits UserState
        'Overloaded Constructors

        Protected dbContext As DAL.AdvantageDomainModelEntities
        Private Context As HttpContext
        Public Sub New(ByVal account As UserAccount)
            Me._account = account
            dbContext = ContextFactory.GetPerRequestContext(Context, Me._account.ConnectionString)
        End Sub
        Public Overrides Function CanUserLogin() As Boolean
            Dim query As IQueryable(Of DAL.SyUser) = dbContext.SyUsers
            If (Not String.IsNullOrEmpty(Me._account.UserName) AndAlso Not String.IsNullOrEmpty(Me._account.Password)) Then
                query = query.Where(Function(s) s.UserName.Equals(Me._account.UserName) And s.Password.Equals(Me._account.Password))
            End If
            For Each item In query
                Select Case item.AccountActive
                    Case Is = "1"
                End Select
            Next
            Return False
        End Function

        Public Overrides Function isSpecialUser(ByVal UserId As String) As Boolean
            return False
        End Function

        Public Overrides Function isUserActive(ByVal UserId As String) As Boolean
            return False
        End Function
    End Class
End Namespace
