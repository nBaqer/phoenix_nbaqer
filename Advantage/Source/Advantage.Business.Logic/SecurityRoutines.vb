﻿Imports BO = Advantage.Business.Objects
Imports CO = FAME.AdvantageV1.Common

Public Class SecurityRoutines
    Public Shared Function CheckUserPermissionByCampusAndResource(ByVal AdvantageUserState As BO.User, _
                                                            ByVal ResourceId As String, _
                                                            ByVal CampusId As String) As CO.UserPagePermissionInfo
        Dim pObj As CO.UserPagePermissionInfo = New CO.UserPagePermissionInfo
        Try
            Select Case AdvantageUserState.FullPermission
                Case Is = True
                    With pObj
                        .HasFull = True
                        .HasAdd = True
                        .HasDisplay = True
                        .HasDelete = True
                        .HasEdit = True
                        .HasNone = False
                    End With
                Case Else
                    Dim result As IEnumerable(Of BO.PageLevelPermission) = AdvantageUserState.UserRoles.Where(Function(s) s.ResourceID.ToString.Equals(ResourceId) _
                                                                    And s.CampusId.ToString.Equals(CampusId)).OrderByDescending(Function(p) p.AccessLevel).ToList()


                    If result.Count >= 1 Then
                        If result(0).PageLevelFullAccess = True Then pObj.HasFull = True
                        If result(0).PageLevelEditAccess = True Then pObj.HasEdit = True
                        If result(0).PageLevelCreateAccess = True Then pObj.HasAdd = True
                        If result(0).PageLevelDeleteAccess = True Then pObj.HasDelete = True
                        If result(0).PageLevelReadOnlyAccess = True And _
                            result(0).PageLevelEditAccess = False And _
                            result(0).PageLevelCreateAccess = False And _
                            result(0).PageLevelDeleteAccess = False Then pObj.HasDisplay = True
                        If result(0).PageLevelReadOnlyAccess = False And _
                         result(0).PageLevelEditAccess = False And _
                         result(0).PageLevelCreateAccess = False And _
                         result(0).PageLevelDeleteAccess = False Then
                            pObj.HasNone = True
                        Else
                            pObj.HasNone = False
                        End If
                    Else
                        pObj.HasNone = True
                    End If
                    Exit Try
            End Select
        Catch ex As Exception
            pObj.HasNone = True 'when it errors out, set permission to none
        End Try
        Return pObj
    End Function
    Public Shared Function CheckUserPermissionStudentPagesByCampusAndResource(ByVal AdvantageUserState As BO.User, _
                                                            ByVal ResourceId As String, _
                                                            ByVal CampusId As String, _
                                                            ByVal pageURL As String) As CO.UserPagePermissionInfo
        Dim pObj As CO.UserPagePermissionInfo = New CO.UserPagePermissionInfo
        Try
            Select Case AdvantageUserState.FullPermission
                Case Is = True
                    With pObj
                        .HasFull = True
                        .HasAdd = True
                        .HasDelete = True
                        .HasEdit = True
                        .HasNone = False
                        .ResourceId = ResourceId
                        .URL = pageURL
                    End With
                Case Else
                    Dim result As IEnumerable(Of BO.PageLevelPermission) = AdvantageUserState.UserRoles.Where(Function(s) s.ResourceID.ToString.Equals(ResourceId) And s.CampusId.ToString.Equals(CampusId)).OrderByDescending(Function(p) p.AccessLevel).ToList()
                    If result.Count >= 1 Then

                        If result(0).PageLevelFullAccess = True Then pObj.HasFull = True
                        If result(0).PageLevelEditAccess = True Then pObj.HasEdit = True
                        If result(0).PageLevelCreateAccess = True Then pObj.HasAdd = True
                        If result(0).PageLevelDeleteAccess = True Then pObj.HasDelete = True
                        If result(0).PageLevelReadOnlyAccess = True And _
                            result(0).PageLevelEditAccess = False And _
                            result(0).PageLevelCreateAccess = False And _
                            result(0).PageLevelDeleteAccess = False Then pObj.HasDisplay = True
                        If result(0).PageLevelReadOnlyAccess = False And _
                         result(0).PageLevelEditAccess = False And _
                         result(0).PageLevelCreateAccess = False And _
                         result(0).PageLevelDeleteAccess = False Then
                            pObj.HasNone = True
                        Else
                            pObj.HasNone = False
                        End If

                        pObj.ResourceId = ResourceId
                        pObj.URL = pageURL
                    Else
                        pObj.HasNone = True
                        pObj.ResourceId = ResourceId
                    End If
            End Select
        Catch ex As Exception
            pObj.HasNone = True 'when it errors out, set permission to none
            pObj.ResourceId = ResourceId
        End Try
        Return pObj
    End Function
    Public Shared Function CheckUserPermissionByCampusAndResourceForEntity(ByVal AdvantageUserState As BO.User, _
                                                          ByVal iResource As List(Of String), _
                                                          ByVal CampusId As String) As CO.UserPagePermissionInfo
        Dim pObj As CO.UserPagePermissionInfo = New CO.UserPagePermissionInfo

        Try
            For Each item As String In iResource
                Dim strResourceId As String = item
                Dim result As IEnumerable(Of BO.PageLevelPermission) = AdvantageUserState.UserRoles.Where(Function(s) s.ResourceID.ToString.Equals(strResourceId) And s.CampusId.ToString.Equals(CampusId)).OrderByDescending(Function(p) p.AccessLevel).ToList()
                If result.Count >= 1 Then
                    If result(0).PageLevelFullAccess = True Then pObj.HasFull = True
                    If result(0).PageLevelEditAccess = True Then pObj.HasEdit = True
                    If result(0).PageLevelCreateAccess = True Then pObj.HasAdd = True
                    If result(0).PageLevelDeleteAccess = True Then pObj.HasDelete = True
                    If result(0).PageLevelReadOnlyAccess = True And _
                        result(0).PageLevelEditAccess = False And _
                        result(0).PageLevelCreateAccess = False And _
                        result(0).PageLevelDeleteAccess = False Then pObj.HasDisplay = True
                    If result(0).PageLevelReadOnlyAccess = False And _
                     result(0).PageLevelEditAccess = False And _
                     result(0).PageLevelCreateAccess = False And _
                     result(0).PageLevelDeleteAccess = False Then
                        pObj.HasNone = True
                    Else
                        pObj.HasNone = False
                    End If
                    If pObj.HasNone = False Then
                        pObj.ResourceId = CInt(strResourceId)
                        Return pObj
                        Exit Function
                    End If
                Else
                    pObj.HasNone = True
                End If
            Next

        Catch ex As Exception
            pObj.HasNone = True 'when it errors out, set permission to none
        End Try
        Return pObj
    End Function


End Class


