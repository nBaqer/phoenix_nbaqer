﻿Imports Advantage.Data.ORM
Imports System.Threading
Imports System.Web

Namespace Advantage.Business.Logic.Layer
    Public Class ContextFactory
        Public Shared Function GetPerRequestContext(ByVal context As HttpContext, ByVal connectionString As String) As AdvantageDomainModelEntities
            Dim key As String = ""
            Dim dbContext As AdvantageDomainModelEntities = Nothing
            key = HttpContext.Current.GetHashCode().ToString("x") + Thread.CurrentContext.ContextID.ToString()

            If dbContext Is Nothing Then
                dbContext = New AdvantageDomainModelEntities(connectionString)
            Else
                dbContext = CType(context.Items(key), AdvantageDomainModelEntities)
                If dbContext Is Nothing Then
                    dbContext = New AdvantageDomainModelEntities(connectionString)
                    context.Items(key) = context
                End If
            End If
            Return dbContext
        End Function
    End Class
End Namespace
