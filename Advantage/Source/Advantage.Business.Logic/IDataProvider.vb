﻿Imports DAL = Advantage.Data.ORM
Imports System.Web

Namespace Advantage.Business.Logic.Layer

    Public MustInherit Class IDataProvider

        Protected dbContext As DAL.AdvantageDomainModelEntities

        Public Sub New(ByVal context As HttpContext, ByVal connectionString As String)
            dbContext = ContextFactory.GetPerRequestContext(context, connectionString)
        End Sub
    End Class
End Namespace
