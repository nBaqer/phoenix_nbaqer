﻿Imports Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports Microsoft.ApplicationInsights.Channel
Imports Microsoft.ApplicationInsights.DataContracts
Imports Microsoft.ApplicationInsights.Extensibility

Namespace Infrastruct.Initializer
    Public Class AppInsightsTelemetryInitializer
        Implements ITelemetryInitializer

        Public Sub Initialize(telemetry As ITelemetry) Implements ITelemetryInitializer.Initialize
                If (Not telemetry.Context.Properties.ContainsKey("Advantage_App")) Then
                    telemetry.Context.Properties.Add("Advantage_App", "Site")
                Else
                    telemetry.Context.Properties("Advantage_App") = "Site"
                End If
        End Sub
    End Class

End Namespace