﻿Option Explicit On
Option Strict On

Imports System.Web
Imports System.Web.Security

Namespace Infrastruct.Helpers

    Public Class MultiTenantHostHelper

        Private Const PASSWORD_RESET_URL As String = "PasswordResetDefault.aspx?email={0}&tpass={1}"

        Public Shared Sub RedirectToLogin()
            HttpContext.Current.Response.Redirect(FormsAuthentication.LoginUrl)
        End Sub

        Public Shared Sub RedirectToLogin(errorMessage As String)
            HttpContext.Current.Response.Redirect(String.Format("{0}?errmsg={1}", FormsAuthentication.LoginUrl, SafeEncode(errorMessage)))
        End Sub

        Public Shared Function GetPasswordResetURL(userName As String, password As String) As String
            Dim passwordResetURL = FormsAuthentication.LoginUrl.Replace("default.aspx", PASSWORD_RESET_URL)
            passwordResetURL = String.Format(passwordResetURL, SafeEncode(userName), SafeEncode(password))

            Return GetCurrentServerName() + VirtualPathUtility.ToAbsolute(passwordResetURL)
        End Function
        Public Shared Function GetPasswordResetURL() As String
            Dim passwordResetURL = FormsAuthentication.LoginUrl

            Return GetCurrentServerName() + VirtualPathUtility.ToAbsolute(passwordResetURL)
        End Function
        Private Shared Function SafeEncode(ByVal input As String) As String
            If Not String.IsNullOrEmpty(input) Then
                input = input.Trim()
            End If

            Return HttpContext.Current.Server.UrlEncode(input)
        End Function

        Public Shared Function GetCurrentServerName() As String
            Dim url = HttpContext.Current.Request.Url
            Return url.Scheme + Uri.SchemeDelimiter + url.Host
        End Function
    End Class
End Namespace