﻿Imports Microsoft.Practices.ServiceLocation
Imports StructureMap

Namespace Infrastruct.Bootstrapping

    Public Class Bootstrapper
        Private Shared _container As Container
        Public Shared Sub Bootstrap()
            InitLogging()

            InitStructureMap()

            InitServiceLocator()
        End Sub
        Public Shared ReadOnly Property Container As Container
            Get
                Return _container
            End Get
        End Property

        Private Shared Sub InitServiceLocator()
            ServiceLocator.SetLocatorProvider(Function()
                                                  Return _container.GetInstance(Of IServiceLocator)()
                                              End Function)
        End Sub

        Private Shared Sub InitStructureMap()
            _container = New Container()
            container.Configure(
                Sub(c)
                    c.Scan(Sub(s)
                               s.TheCallingAssembly()
                               s.LookForRegistries()
                           End Sub)
                End Sub)
        End Sub

        Private Shared Sub InitLogging()
            log4net.Config.XmlConfigurator.Configure()
        End Sub
    End Class
End Namespace