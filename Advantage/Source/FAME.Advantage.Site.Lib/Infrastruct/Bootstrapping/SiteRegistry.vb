﻿Imports FAME.Advantage.Common.Services
Imports FAME.Advantage.Domain.Infrastructure.Entities
Imports FAME.Advantage.Domain.Infrastructure.ServiceLocation
Imports FAME.Advantage.Domain.MultiTenant
Imports FAME.Advantage.Domain.MultiTenant.Persistence
Imports FAME.Advantage.Domain.Persistence.Infrastructure.NHibernate
Imports FAME.Advantage.Domain.Persistence.Infrastructure.UoW
Imports Microsoft.Practices.ServiceLocation
Imports NHibernate
Imports StructureMap
Imports StructureMap.Configuration.DSL
Imports StructureMap.Graph
Imports StructureMap.Web

Namespace Infrastruct.Bootstrapping

    Public Class SiteRegistry
        Inherits Registry

        Public Sub New()
            Scan(Sub(s As IAssemblyScanner)
                     s.TheCallingAssembly()
                     s.AssemblyContainingType(Of IMultiTenantService)()
                     s.AssemblyContainingType(Of IPasswordResetService)()
                     s.AssemblyContainingType(Of PasswordResetService)()
                     s.WithDefaultConventions()
                 End Sub)


            Me.Policies.SetAllProperties(Sub(c)
                                             c.OfType(Of IUnitOfWork)()
                                         End Sub)

            [For](Of IServiceLocator)().Use(Of StructureMapServiceLocator)()

            [For](Of IRepository)().Use(Of NHibernateRepository)()
            [For](Of IRepositoryWithTypedID(Of Int32))().Use(Of NHibernateRepository(Of Int32))()

            [For](Of IUnitOfWork)() _
                .HybridHttpOrThreadLocalScoped() _
                .Use(Of UnitOfWork)()

            ForSingletonOf(Of ISessionFactory) _
                .Use(New MultiTenantSessionFactoryConfig().CreateSessionFactory()
                     )

            ForSingletonOf(Of ISessionFactory) _
                .Use(New MultiTenantSessionFactoryConfig().CreateSessionFactory()
                     )
        End Sub
    End Class
End Namespace