﻿Option Explicit On
Option Strict On

Imports System.Web
Imports System.Web.SessionState
Imports FAME.Advantage.Common.Helpers
Imports FAME.Advantage.Domain.Infrastructure.Entities
Imports FAME.Advantage.Site.Lib.Infrastruct.Bootstrapping
Imports StructureMap

Namespace Presenters

    Public MustInherit Class BasePresenter

        <DebuggerStepThrough>
        Protected Sub RunInUnitOfWork(action As Action)
            UnitOfWorkHelper.RunInUnitOfWork(action)
        End Sub

        <DebuggerStepThrough>
        Protected Sub RunInUnitOfWork(action As Action(Of IRepository))
            UnitOfWorkHelper.RunInUnitOfWork(action)
        End Sub

        <DebuggerStepThrough>
        Protected Sub RunInUnitOfWork(action As Action(Of IRepositoryWithTypedID(Of Int32)))
            UnitOfWorkHelper.RunInUnitOfWork(action)
        End Sub

        <DebuggerStepThrough>
        Protected Function RunInUnitOfWork(Of T)(action As Func(Of T)) As T
            Return UnitOfWorkHelper.RunInUnitOfWork(action)
        End Function

        <DebuggerStepThrough>
        Protected Function RunInUnitOfWork(Of T)(action As Func(Of IRepository, T)) As T
            Return UnitOfWorkHelper.RunInUnitOfWork(action)
        End Function

        <DebuggerStepThrough>
        Protected Function RunInUnitOfWork(Of T)(action As Func(Of IRepositoryWithTypedID(Of Int32), T)) As T
            Return UnitOfWorkHelper.RunInUnitOfWork(action)
        End Function

        Protected ReadOnly Property Session() As HttpSessionState
            Get
                Return HttpContext.Current.Session
            End Get
        End Property

        Protected ReadOnly Property Request() As HttpRequest
            Get
                Return HttpContext.Current.Request
            End Get
        End Property

        Protected ReadOnly Property Response() As HttpResponse
            Get
                Return HttpContext.Current.Response
            End Get
        End Property

        <DebuggerStepThrough>
        Protected Shared Function Resolve(Of T)() As T
            Return Bootstrapper.Container.GetInstance(Of T)()
        End Function
    End Class
End Namespace