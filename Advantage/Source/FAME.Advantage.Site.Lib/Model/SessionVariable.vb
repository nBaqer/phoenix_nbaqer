﻿Option Explicit On
Option Strict On

Imports System.Web

Namespace Model

    Public Class SessionVariable(Of T)

        Private ReadOnly _key As String
        Private ReadOnly _defaultValue As T

        Public Sub New(key As String)
            Me.New(key, Nothing)
        End Sub

        Public Sub New(key As String, defaultValue As T)
            _key = key
            _defaultValue = defaultValue
        End Sub

        Public Function GetValue() As T
            Dim value = HttpContext.Current.Session(_key)

            If value Is Nothing Then
                Return _defaultValue
            Else
                Return CType(value, T)
            End If
        End Function

        Public Sub SetValue(value As T)
            HttpContext.Current.Session(_key) = value
        End Sub
    End Class
End Namespace