﻿Option Explicit On
Option Strict On

Imports FAME.Advantage.Site.Lib.Model
Imports System.Collections.Generic
Imports Advantage.Business.Objects

''' <summary>
''' AdvantageSession provides a facade to the ASP.NET Session object.
'''All access to Session variables must be through this class.
''' </summary>
''' <remarks></remarks>
Public NotInheritable Class AdvantageSession

    Private Shared ReadOnly PAGE_THEME As SessionVariable(Of Integer) _
        = New SessionVariable(Of Integer)("PageTheme", Site.Lib.PageTheme.None)
    Private Shared ReadOnly USER_STATE As SessionVariable(Of User) _
        = New SessionVariable(Of User)("UserState")
    Private Shared ReadOnly USER_NAME As SessionVariable(Of String) _
        = New SessionVariable(Of String)("UserName")
    Private Shared ReadOnly COMMON_TASK_MENU_ITEM As SessionVariable(Of List(Of CommonTaskMenuItem)) _
        = New SessionVariable(Of List(Of CommonTaskMenuItem))("CommonTaskMenuAndTabItems")
    Private Shared ReadOnly REPORT_MENU_ITEMS As SessionVariable(Of List(Of CommonTaskMenuItem)) _
        = New SessionVariable(Of List(Of CommonTaskMenuItem))("ReportMenuItems")
    Private Shared ReadOnly MAINTENANCE_MENU_ITEMS As SessionVariable(Of List(Of CommonTaskMenuItem)) _
        = New SessionVariable(Of List(Of CommonTaskMenuItem))("MaintenanceMenuItems")
    Private Shared ReadOnly STUDENT_TAB_ITEMS As SessionVariable(Of List(Of CommonTaskMenuItem)) _
        = New SessionVariable(Of List(Of CommonTaskMenuItem))("StudentTabItems")
    Private Shared ReadOnly LEAD_TAB_ITEMS As SessionVariable(Of List(Of CommonTaskMenuItem)) _
        = New SessionVariable(Of List(Of CommonTaskMenuItem))("LeadTabItems")
    Private Shared ReadOnly EMPLOYER_TAB_ITEMS As SessionVariable(Of List(Of CommonTaskMenuItem)) _
        = New SessionVariable(Of List(Of CommonTaskMenuItem))("EmployerTabItems")
    Private Shared ReadOnly MODULE_NAVIGATION As SessionVariable(Of List(Of CommonTaskMenuItem)) _
        = New SessionVariable(Of List(Of CommonTaskMenuItem))("ModuleNavigation")
    Private Shared ReadOnly COMMON_TASK_NAVIGATION As SessionVariable(Of List(Of CommonTaskMenuItem)) _
        = New SessionVariable(Of List(Of CommonTaskMenuItem))("CommonTaskNavigation")
    Private Shared ReadOnly MAINTENANCE As SessionVariable(Of List(Of CommonTaskMenuItem)) _
        = New SessionVariable(Of List(Of CommonTaskMenuItem))("MaintenanceForAllModules")
    Private Shared ReadOnly TAB_MENU_ITEMS As SessionVariable(Of List(Of CommonTaskMenuItem)) _
        = New SessionVariable(Of List(Of CommonTaskMenuItem))("TabMenus")
    Private Shared ReadOnly TOOL_MENU_ITEMS As SessionVariable(Of List(Of CommonTaskMenuItem)) _
        = New SessionVariable(Of List(Of CommonTaskMenuItem))("ToolsMenu")
    Private Shared ReadOnly PAGE_BREADCRUMB As SessionVariable(Of String) _
        = New SessionVariable(Of String)("PageBreadCrumb")
    Private Shared ReadOnly STUDENT_MRU As SessionVariable(Of List(Of StudentMRU)) _
        = New SessionVariable(Of List(Of StudentMRU))("StudentMRU")
    Private Shared ReadOnly LEAD_MRU As SessionVariable(Of List(Of LeadMRU)) _
        = New SessionVariable(Of List(Of LeadMRU))("LeadMRU")
    Private Shared ReadOnly EMPLOYER_MRU As SessionVariable(Of List(Of EmployerMRU)) _
        = New SessionVariable(Of List(Of EmployerMRU))("EmployerMRU")
    Private Shared ReadOnly EMPLOYEE_MRU As SessionVariable(Of List(Of EmployeeMRU)) _
        = New SessionVariable(Of List(Of EmployeeMRU))("EmployeeMRU")

    Shared Sub New()
        PAGE_THEME = New SessionVariable(Of Integer)("PageTheme", Site.Lib.PageTheme.None)
    End Sub

    Public Shared Property PageTheme() As Integer
        Get
            Return PAGE_THEME.GetValue()
        End Get
        Set(value As Integer)
            PAGE_THEME.SetValue(value)
        End Set
    End Property

    ''' <summary>
    ''' This Property will hold the state of the user such as userid, default campus id, default module code,etc.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Property UserState() As User
        Get
            Return USER_STATE.GetValue()
        End Get
        Set(ByVal value As User)
            USER_STATE.SetValue(value)
        End Set
    End Property

    Public Shared Property UserName() As String
        Get
            Return USER_NAME.GetValue()
        End Get
        Set(ByVal value As String)
            USER_NAME.SetValue(value)
        End Set
    End Property

    Public Shared Property CommonTaskMenuItems() As List(Of CommonTaskMenuItem)
        Get
            Return COMMON_TASK_MENU_ITEM.GetValue()
        End Get
        Set(ByVal value As List(Of CommonTaskMenuItem))
            COMMON_TASK_MENU_ITEM.SetValue(value)
        End Set
    End Property

    Public Shared Property ReportMenuItems() As List(Of CommonTaskMenuItem)
        Get
            Return REPORT_MENU_ITEMS.GetValue()
        End Get
        Set(ByVal value As List(Of CommonTaskMenuItem))
            REPORT_MENU_ITEMS.SetValue(value)
        End Set
    End Property

    Public Shared Property MaintenanceMenuItems() As List(Of CommonTaskMenuItem)
        Get
            Return MAINTENANCE_MENU_ITEMS.GetValue()
        End Get
        Set(ByVal value As List(Of CommonTaskMenuItem))
            MAINTENANCE_MENU_ITEMS.SetValue(value)
        End Set
    End Property

    Public Shared Property StudentTabsMenuItems() As List(Of CommonTaskMenuItem)
        Get
            Return STUDENT_TAB_ITEMS.GetValue()
        End Get
        Set(ByVal value As List(Of CommonTaskMenuItem))
            STUDENT_TAB_ITEMS.SetValue(value)
        End Set
    End Property

    Public Shared Property LeadTabsMenuItems() As List(Of CommonTaskMenuItem)
        Get
            Return LEAD_TAB_ITEMS.GetValue()
        End Get
        Set(ByVal value As List(Of CommonTaskMenuItem))
            LEAD_TAB_ITEMS.SetValue(value)
        End Set
    End Property

    Public Shared Property EmployerTabsMenuItems() As List(Of CommonTaskMenuItem)
        Get
            Return EMPLOYER_TAB_ITEMS.GetValue()
        End Get
        Set(ByVal value As List(Of CommonTaskMenuItem))
            EMPLOYER_TAB_ITEMS.SetValue(value)
        End Set
    End Property

    Public Shared Property ModuleNavigation() As List(Of CommonTaskMenuItem)
        Get
            Return MODULE_NAVIGATION.GetValue()
        End Get
        Set(ByVal value As List(Of CommonTaskMenuItem))
            MODULE_NAVIGATION.SetValue(value)
        End Set
    End Property

    Public Shared Property CommonTaskNavigation() As List(Of CommonTaskMenuItem)
        Get
            Return COMMON_TASK_NAVIGATION.GetValue()
        End Get
        Set(ByVal value As List(Of CommonTaskMenuItem))
            COMMON_TASK_NAVIGATION.SetValue(value)
        End Set
    End Property

    Public Shared Property MaintenanceForAllModules() As List(Of CommonTaskMenuItem)
        Get
            Return MAINTENANCE.GetValue()
        End Get
        Set(ByVal value As List(Of CommonTaskMenuItem))
            MAINTENANCE.SetValue(value)
        End Set
    End Property

    Public Shared Property TabMenus() As List(Of CommonTaskMenuItem)
        Get
            Return TAB_MENU_ITEMS.GetValue()
        End Get
        Set(ByVal value As List(Of CommonTaskMenuItem))
            TAB_MENU_ITEMS.SetValue(value)
        End Set
    End Property

    Public Shared Property ToolsMenu() As List(Of CommonTaskMenuItem)
        Get
            Return TOOL_MENU_ITEMS.GetValue()
        End Get
        Set(ByVal value As List(Of CommonTaskMenuItem))
            TOOL_MENU_ITEMS.SetValue(value)
        End Set
    End Property

    Public Shared Property PageBreadCrumb() As String
        Get
            Return PAGE_BREADCRUMB.GetValue()
        End Get
        Set(ByVal value As String)
            PAGE_BREADCRUMB.SetValue(value)
        End Set
    End Property

    Public Shared Property StudentMRU() As List(Of StudentMRU)
        Get
            Return STUDENT_MRU.GetValue()
        End Get
        Set(ByVal value As List(Of StudentMRU))
            STUDENT_MRU.SetValue(value)
        End Set
    End Property

    Public Shared Property LeadMRU() As List(Of LeadMRU)
        Get
            Return LEAD_MRU.GetValue()
        End Get
        Set(ByVal value As List(Of LeadMRU))
            LEAD_MRU.SetValue(value)
        End Set
    End Property

    Public Shared Property EmployerMRU() As List(Of EmployerMRU)
        Get
            Return EMPLOYER_MRU.GetValue()
        End Get
        Set(ByVal value As List(Of EmployerMRU))
            EMPLOYER_MRU.SetValue(value)
        End Set
    End Property

    Public Shared Property EmployeeMRU() As List(Of EmployeeMRU)
        Get
            Return EMPLOYEE_MRU.GetValue()
        End Get
        Set(ByVal value As List(Of EmployeeMRU))
            EMPLOYEE_MRU.SetValue(value)
        End Set
    End Property
End Class