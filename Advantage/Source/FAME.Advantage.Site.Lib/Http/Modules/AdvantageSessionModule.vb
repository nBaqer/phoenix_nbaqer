﻿
Imports FAME.Advantage.Site.Lib.Infrastruct.Helpers
Imports log4net
Imports System.Runtime.Remoting.Messaging
Imports Microsoft.VisualBasic
Imports System.Web
Imports Advantage.Business.Objects
Imports Advantage.Business.Logic.Layer
Imports FAME.Advantage.Common
Imports System.Web.SessionState
Imports Microsoft.Practices.ServiceLocation
Imports FAME.Advantage.Common.Services

Namespace Http.Modules

    Public Class AdvantageSessionModule
        Implements IHttpModule, IRequiresSessionState

        Private ReadOnly logger As ILog

        Public Sub New()
            logger = LogManager.GetLogger([GetType]())
        End Sub

        Public Sub Dispose() Implements IHttpModule.Dispose
        End Sub

        Public Sub Init(context As HttpApplication) Implements IHttpModule.Init
            AddHandler context.PreRequestHandlerExecute, AddressOf PreRequestHandlerExecute
        End Sub

        Private Sub PreRequestHandlerExecute(ByVal s As Object, ByVal e As EventArgs)
            Dim context As HttpContext = HttpContext.Current
            Dim request As HttpRequest = context.Request
            Dim session = context.Session

            If (request.Url.ToString().Contains("ServiceMethods.aspx")) Then
                Return
            End If

            If Not context.User.Identity.IsAuthenticated Then
                MultiTenantHostHelper.RedirectToLogin("Authentication could not be established")
                Return
            End If

            Try
                '5/31/2013 Janet Robinson DE9675 clicking on help books return to login on IE only
                If Not IsNothing(request.Url) Then
                    Dim strCurrentPage = request.Url.ToString()
                    If strCurrentPage.ToLower().Contains("help") Then Exit Sub
                End If

                If Not IsNothing(request.UrlReferrer) Then
                    Dim strPreviousPage As String = request.UrlReferrer.ToString

                    'Populate the Advantage Session Object only when user comes from login page 
                    If IsFromLoginPages(strPreviousPage) Then
                        logger.InfoFormat("UserID: {0}", request.QueryString("UserId"))
                        logger.InfoFormat("TenantID: {0}", request.QueryString("Tenantid"))

                        Dim strTenantAuthUserId = request.QueryString("UserId")
                        session("TenantUserId") = strTenantAuthUserId

                        Dim intTenantid = CInt(request.QueryString("Tenantid"))
                        session("TenantNameId") = intTenantid

                        Dim myAppSettings As New AdvAppSettings
                        session("AdvAppSettings") = myAppSettings

                        Dim strCustomerDBConnectionString = getConnectionString(strTenantAuthUserId, intTenantid)
                        Dim dataUserProvider = New UserStateDataProvider(context, strCustomerDBConnectionString)

                        Dim addUser As New User() With {
                                .UserId = New Guid(strTenantAuthUserId)
                                }

                        Dim advantageUserState = dataUserProvider.getUserLoginState(addUser)

                        'Added by Balaji on Mar 21 2012
                        'To keep track of old campus when user switches to new campus
                        AdvantageSession.UserState = advantageUserState 'Load user object in session
                        AdvantageSession.UserName = advantageUserState.UserName
                        AdvantageSession.PageTheme = PageTheme.Blue_Theme

                    End If
                Else
                    'If session Is Nothing And (request.Path.Contains("proxy/api") = False) Then
                    '    MultiTenantHostHelper.RedirectToLogin("AdvAppSettings are missing")
                    '    Return
                    'End If
                    'If DirectCast(session("AdvAppSettings"), AdvAppSettings) Is Nothing And (request.Path.Contains("proxy/api") = False) Then
                    '    MultiTenantHostHelper.RedirectToLogin("AdvAppSettings are missing")
                    '    Return
                    'End If
                    'Exit Sub
                End If
            Catch ex As Exception
                logger.Error(ex)
                MultiTenantHostHelper.RedirectToLogin(ex.Message)
                Exit Sub
            End Try
        End Sub

        Private Shared Function IsFromLoginPages(ByVal strPreviousPage As String) As Boolean
            Dim page As String = strPreviousPage.ToLower()

            Return page.Contains("default.aspx") _
                   OrElse page.Contains("passwordresetdefault.aspx") _
                   OrElse page.Contains("tenantpicker.aspx") _
                   OrElse page.Contains("termsofuse.aspx")
        End Function

        Private Function getConnectionString(ByVal strTenantUserId As String, ByVal strTenantNameid As Integer) As String
            Dim service = ServiceLocator.Current.GetInstance(Of IMultiTenantService)()
            Return service.GetConnectionString(Guid.Parse(strTenantUserId), strTenantNameid)
        End Function
    End Class
End Namespace