﻿
Imports System.Collections.Generic
Imports BO = Advantage.Business.Objects
Namespace Advantage.Provider.Contracts
    Public Interface IStudentDataProvider
        'Get Student, Enrollment and Address Details
        Function GetStudent(ByVal searchString As String) As List(Of BO.Student)
        Function GetStudentEnrollment(ByVal StudentId As String) As List(Of BO.StudentEnrollment)
        Function GetStudentAddress(ByVal StudentId As String) As List(Of BO.StudentAddress)

        'Delete Student 
        Sub DeleteStudent(ByVal StudentId As String)

        'Delete Student Address
        Sub DeleteStudentAddress(ByVal StudentAddressId As String)
        
        'Update Student
        Sub UpdateStudent(ByVal editedStudent As BO.Student)

        'Update Student Address
        Sub UpdateStudentAddress(ByVal editedStudentAddress As BO.StudentAddress)

        'Add Student Address
        Sub AddStudentAddress(ByVal editedStudentAddress As BO.StudentAddress)

        Sub Dispose()
    End Interface
End Namespace
