﻿using System.Collections.Generic;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using WapiAdvantage.Abstract;
using WapiAdvantage.Contract;
using WapiAdvantage.Data;

namespace WapiLead
{
    /// <summary>
    /// This class provide the necessary support to connect with Fame Advantage Lead Services
    /// You only need to use the exposed methods.
    /// </summary>
    public class WapiLead : WapiProxy
    {
        #region Properties

        /// <summary>
        /// This field is filled by the constructor.
        /// it is your vendor code. (supplied by FAME)
        /// </summary>
        private readonly string vendorCode;

        #endregion

        #region Constant Operation you are able to do

        /// <summary>
        /// Test Operation: Echo operation. Return a fix value, you can use it to test the connexion with Lead server <br/>
        /// Parameters: None<br/>
        /// Return: May the force be with you.
        /// </summary>
        protected const string XOPERATION_ECHOSL = "ECHOSL";

        #region Production Operations
 
        /// <summary>
        /// Get  the ProspectId code of the last lead accepted by Advantage.<br/>
        /// Parameters [FromUri]<br/>
        /// None<br/>
        /// Return: The ProspectId of the last lead accepted by Advantage. or null if does not exists one.
        /// </summary>
        /// <remarks>Use only in PRODUCTION!</remarks>
        protected const string X_GET_LEAD_LASTLEAD = "GET_LEAD_LASTLEAD";

        /// <summary>
        /// Post the lead information you can post a individual lead or multiple leads.<br/>
        /// In all cases use a list of WapiLeadsOutputModel as parameter. <br/>
        /// Parameters [FromBody]<br/>
        /// A List of WapiLeadsOutputModel<br/>
        /// The optional fields that you don't use leave then empty or null.<br/>
        /// Return: As return you get a list of rejected lead with the cause of rejection in LeadStatus field. <br/>
        /// if all are accepted you can a empty list of WapiLeadsOutputModel.
        /// </summary>
        /// <remarks>Use only in PRODUCTION!</remarks>
        protected const string X_POST_LEAD_IMPORTER = "POST_LEAD_IMPORTER";
        #endregion

        #region SandBox Operations. Use this for your test

        /// <summary>
        /// Get  the ProspectId code of the last lead accepted by Advantage.<br/>
        /// Parameters [FromUri]<br/>
        /// None<br/>
        /// Return: The ProspectId of the last lead accepted by Advantage. or null if does not exists one.
        /// </summary>
        /// <remarks>SANDBOX FOR TEST USE!</remarks>
        protected const string X_GET_LEAD_LASTLEAD_SANDBOX = "GET_LEAD_LASTLEAD_SANDBOX";

        /// <summary>
        /// Post the lead information you can post a individual lead or multiple leads.<br/>
        /// In all cases use a list of WapiLeadsOutputModel as parameter. <br/>
        /// Parameters [FromBody]<br/>
        /// A List of WapiLeadsOutputModel<br/>
        /// The optional fields that you don't use leave then empty or null.<br/>
        /// Return: As return you get a list of rejected lead with the cause of rejection in LeadStatus field. <br/>
        /// if all are accepted you can a empty list of WapiLeadsOutputModel.
        /// </summary>
        /// <remarks>SANDBOX FOR TEST USE!</remarks>
        protected const string X_POST_LEAD_IMPORTER_SANDBOX = "POST_LEAD_IMPORTER_SANDBOX";
        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for WapiLead
        /// To use this you must create a ProxyConfigurationObject
        /// </summary>
        /// <param name="proxyconfig"> >Proxy Configuration parameters. See  <see cref="ProxyConfiguration"/></param>
        /// <param name="vendorCode">The supplied vendor code to your company</param>
        public WapiLead(IProxyConfiguration proxyconfig, string vendorCode)
            : base(proxyconfig)
        {
            this.vendorCode = vendorCode;
        }
        #endregion

        #region Production Methods

        /// <summary>
        /// Return the last lead ProspectId accepted by Advantage 
        /// </summary>
        /// <returns>
        /// The ProspectId or null or empty
        /// </returns>
        /// <remarks>Use only in production environment</remarks>
        public string GetLastLeadOperation()
        {
           var prospectid = GetMessageFromWapi(X_GET_LEAD_LASTLEAD, string.Format("?vendorCode={0}", vendorCode));
           return prospectid;
        }

        /// <summary>
        /// Export a list of lead to advantage and return possible rejections.
        /// </summary>
        /// <param name="leadList">The lead list to export to advantage</param>
        /// <returns>List of rejected Leads. See field LeadStatus to get the cause of rejection</returns>
        /// <remarks>Use this only in production environment!</remarks>
        public IList<WapiLeadsOutputModel> PostWapiLeadsOutputModels(IList<WapiLeadsOutputModel> leadList)
        {
            var json = new JavaScriptSerializer().Serialize(leadList);
            var jsonstring = PostMessageToWapi(X_POST_LEAD_IMPORTER, json, null);
            var listRejected = JsonConvert.DeserializeObject<IList<WapiLeadsOutputModel>>(jsonstring);
            return listRejected;
        }
        #endregion

        #region SandBox Method

        /// <summary>
        /// Return the last lead ProspectId accepted by Advantage 
        /// </summary>
        /// <returns>
        /// The ProspectId or null or empty
        /// </returns>
        /// <remarks>Use only in production environment</remarks>
        public string GetLastLeadOperationSandBox()
        {
            var prospectid = GetMessageFromWapi(X_GET_LEAD_LASTLEAD_SANDBOX, string.Format("?vendorCode={0}", vendorCode));
            return prospectid;
        }

        /// <summary>
        /// Export a list of lead to advantage and return possible rejections.
        /// </summary>
        /// <param name="leadList">The lead list to export to advantage</param>
        /// <returns>List of rejected Leads. See field LeadStatus to get the cause of rejection</returns>
        /// <remarks>Use this only in Sandbox Test environment!</remarks>
        public IList<WapiLeadsOutputModel> PostWapiLeadsOutputModelsSandBox(IList<WapiLeadsOutputModel> leadList)
        {
            var json = new JavaScriptSerializer().Serialize(leadList);
            var listRejected = PostWapiLeadsJsonSandBox(json);
            return listRejected;
        }

        /// <summary>
        /// Export a json object with lead to advantage and return possible rejections.
        /// </summary>
        /// <param name="json">The lead list to export to advantage</param>
        /// <returns>List of rejected Leads. See field LeadStatus to get the cause of rejection</returns>
        /// <remarks>Use this only in Sandbox Test environment!</remarks>
        /// <remarks>Be sure before use this that your json object is correct!!</remarks>
        public IList<WapiLeadsOutputModel> PostWapiLeadsJsonSandBox(string json)
        {
            var jsonstring = PostMessageToWapi(X_POST_LEAD_IMPORTER_SANDBOX, json, null);
            var listRejected = JsonConvert.DeserializeObject<IList<WapiLeadsOutputModel>>(jsonstring);
            return listRejected;
        }

        /// <summary>
        /// Send the leads object and return a json string.
        /// </summary>
        /// <param name="leadList"></param>
        /// <returns></returns>
        public string PostLeadsAndReturnJsonRejected(string json)
        {
            var jsonstring = PostMessageToWapi(X_POST_LEAD_IMPORTER_SANDBOX, json, null);
            return jsonstring;

        }

     

        #endregion



    }
}
