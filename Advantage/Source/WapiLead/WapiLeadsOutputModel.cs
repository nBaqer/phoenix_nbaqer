﻿using System;

namespace WapiLead
{
    /// <summary>
    /// This class hold the information to be exported to Advantage
    /// </summary>
    [Serializable]
    public class WapiLeadsOutputModel
    {
        /// <summary>
        /// Lead identification name in campaign, this is not the internal Lead ID
        /// </summary>
        public string ProspectID { get; set; }

        /// <summary>
        /// The name of Vendor
        /// </summary>
        public string VendorName { get; set; }

        /// <summary>
        /// Code of the company that supply the Lead
        /// </summary>
        public string VendorCode { get; set; }

        /// <summary>
        /// Id of the Campaign
        /// </summary>
        public string CampaignId { get; set; }

        /// <summary>
        /// The school account with the vendor, if does not exists put "none"
        /// </summary>
        public string AccountId { get; set; }

        /// <summary>
        /// Lead First Name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Lead First Name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Lead phone home
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// [cell, home, work other]
        /// </summary>
        public string PhoneType { get; set; }

        /// <summary>
        /// Lead Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// The type of email [home, work other, none]
        /// </summary>
        public string EmailType { get; set; }

        /// <summary>
        /// Lead Address 1
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// Lead County
        /// </summary>
        public string County { get; set; }

        /// <summary>
        /// Lead City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Lead State ID. Use Standard notation for State FL, GA etc.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Lead ZIP Code
        /// </summary>
        public string Zip { get; set; }

        /// <summary>
        /// Country Lead Address
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Lead Program of interest
        /// </summary>
        public string ProgramOfInterest { get; set; }

        /// <summary>
        /// The date that Lead was contacted or hi/she contact us.
        /// Data and time format: conforms to ISO 8601 
        /// Example: 2012-04-23T18:25:43.511Z
        /// </summary>
        public DateTime DateOfContact { get; set; }

        /// <summary>
        /// Lead Middle Name
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Lead Address line 2
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Lead optional Phone
        /// </summary>
        public string Phone2 { get; set; }

        /// <summary>
        /// [cell, home, work, other]
        /// </summary>
        public string Phone2Type { get; set; }

        /// <summary>
        /// Optional Email
        /// </summary>
        public string Email2 { get; set; }

        /// <summary>
        /// The type of email [home, work, other, none]
        /// </summary>
        public string Email2Type { get; set; }

        /// <summary>
        /// SSN
        /// </summary>
        public string SSN { get; set; }

        /// <summary>
        /// Lead birth date
        /// Data and time format: conforms to ISO 8601 
        /// Example: 2012-04-23T18:25:43.511Z
        /// </summary>

        public DateTime BirthDate { get; set; }
        /// <summary>
        /// Lead Prefix
        ///  </summary>
        public string Prefix { get; set; }
        /// <summary>
        /// Lead Suffix
        /// </summary>

        public string Suffix { get; set; }
        /// <summary>
        /// The graduation year of the lead
        /// </summary>

        public int GraduationYear { get; set; }
        /// <summary>
        /// Some comments
        /// </summary>

        public string Comments { get; set; }
        /// <summary>
        /// THe source of the Lead
        /// </summary>

        public string Source { get; set; }
        /// <summary>
        /// [incomplete, duplicate]
        /// </summary>
        public string LeadStatus { get; set; }

        /// <summary>
        /// Expected Start Date.
        /// Data and time format: conforms to ISO 8601 
        /// Example: 2012-04-23T18:25:43.511Z
        /// </summary>
        public DateTime ExpStartDate { get; set; }

        /// <summary>
        /// Gender admit [male, female, unknown]
        /// </summary>
        public string Gender { get; set; }

    }
}
