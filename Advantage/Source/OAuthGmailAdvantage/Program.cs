﻿using System;
using System.IO;
using System.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Util.Store;

namespace OAuthGMail
{
    public class Program
    {
        static readonly string[] Scopes = { GmailService.Scope.GmailSend };

        static void Main(string[] args)
        {
            UserCredential credential;
            var appBinDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            var storeFilePath = new Uri(appBinDirectory + @"/GmailOauth/gmailAdvantageService.json").LocalPath;
            var secretPath = new Uri(appBinDirectory + "/client_secret_AdvantageService.json").LocalPath;

            using (var stream = new FileStream(secretPath, FileMode.Open, FileAccess.Read))
            {
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(storeFilePath, true)).Result;
                Console.WriteLine("Credential file saved to: " + storeFilePath);
            }


        }
    }
}
