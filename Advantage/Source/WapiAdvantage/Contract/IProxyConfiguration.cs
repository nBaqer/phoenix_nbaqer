﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IProxyConfiguration.cs" company="FAME">
//   © Copyright  2014, 2017
// </copyright>
// <summary>
//   Author: JAGG- Advantage - WapiAdvantage - 
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace WapiAdvantage.Contract
{
    /// <summary>
    /// Hold necessary information to proxy operation.
    /// </summary>
    public interface IProxyConfiguration
    {
        /// <summary>
        /// Gets or sets Advantage Proxy Address (mandatory)
        /// </summary>
        string ProxyUrl { get; set; }

        /// <summary>
        /// Gets or sets Secret Company Key (mandatory)
        /// </summary>
        string CompanyKey { get; set; }

        /// <summary>
        ///  Gets or sets Log File (optional)
        /// </summary>
        string LogFile { get; set; }

        /// <summary>
        /// Gets or sets  Log file length (optional)
        /// </summary>
        int LogKb { get; set; }
    }
}