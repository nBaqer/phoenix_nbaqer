﻿// --------------------------------------------------------------------------------------------
// <copyright file="IWapiProxy.cs" company="FAME">
//   ©Copyright 2014 FAME W-API  
// </copyright>
//
// Advantage - WapiDll - IWapiProxy.cs
//---------------------------------------------------------------------------------------------

namespace WapiAdvantage.Contract
{
    using System;

    /// <summary>
    /// API with the WAPI Proxy
    /// </summary>
    public interface IWapiProxy
    {
        /// <summary>
        /// Gets or sets Configuration proxy class.
        /// </summary>
        IProxyConfiguration ConfigProxy { get; set; }

        /// <summary>
        /// Send a GET operation over the proxy
        /// </summary>
        /// <param name="commandName">The operation name invoked</param>
        /// <param name="paramstring">
        /// A optional query string 
        /// </param>
        /// <returns>
        /// The result as a JSON formatted string.
        /// </returns>
        string GetMessageFromWapi(string commandName, string paramstring = "");

        /// <summary>
        /// Send a POST operation over the proxy
        /// </summary>
        /// <param name="commandName">The operation name invoked</param>
        /// <param name="payload">A JSON formatted string to be send is the message body</param>
        /// <param name="paramstring">A optional parameter string</param>
        /// <returns>Empty string is all OK, if not return the error cause</returns>
        string PostMessageToWapi(string commandName, string payload, string paramstring = "");

        /// <summary>
        /// Write the log information and create  a new thread
        /// this method should be implement asynchronously
        /// </summary>
        /// <param name="message">Message to be write in proxy</param>
        /// <param name="commandname">The executed command</param>
        void WriteLogAsync(string message, string commandname);

        /// <summary>
        /// This method should be implemented by WriteLogAsync in a new thread.
        /// </summary>
        /// <param name="message">Message to be write in proxy</param>
        void WriteLog(Object message);
    }
}