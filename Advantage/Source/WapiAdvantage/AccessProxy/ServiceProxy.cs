﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceProxy.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Direct operation with the WAPI Proxy
//   This operation are usually used in the WAPI API DLL
//   to support more high level procedures that mask the complexity
//   of the PROXY Login and operation.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

// ReSharper disable InconsistentNaming
namespace WapiAdvantage.AccessProxy
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Net;
    using System.Net.Http;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;

    using global::WapiAdvantage.Data;

    /// <summary>
    /// Direct operation with the WAPI Proxy
    /// This operation are usually used in the WAPI API DLL
    /// to support more high level procedures that mask the complexity
    /// of the PROXY Login and operation.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Reviewed. Suppression is OK here.")]
    public class ServiceProxy
    {
        #region Service Codes

        /// <summary>
        /// Request to proxy the Public key, necessary to encrypt the token.
        /// </summary>
        public const string SEND_PUBLIC_KEY = "SEND_PUBLIC_KEY";

        #endregion

        #region Header Codes

        /// <summary>
        /// The Proxy header name to supply the access key
        /// </summary>
        public const string HEADER_ACCESS_KEY = "ACCESS_KEY";

        /// <summary>
        /// The proxy header name to supply the service requested. 
        /// </summary>
        public const string HEADER_SERVICE_CODE = "SERVICE_CODE";

        /// <summary>
        /// The proxy header name to supply the company secret key.
        /// </summary>
        public const string HEADER_COMPANY_SECRET = "COMPANY_SECRET";

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceProxy"/> class.
        /// </summary>
        public ServiceProxy()
        {
            this.OperationResult = new ResultObj();
        }

        #region Properties

        /// <summary>
        /// Gets or sets the list of result of operations
        /// </summary>
        public ResultObj OperationResult { get; protected set; }

        #endregion

        #region Infrastructure Operation: Get public and access key

        /// <summary>
        /// Get the public key from server
        /// </summary>
        /// <param name="wapiAddress">The proxy address</param>
        /// <returns>
        /// The public key 
        /// if not return string empty.
        /// </returns>
        public string GetPublicKey(string wapiAddress)
        {
            var servicesUrl = wapiAddress;
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get
            };

            request.Headers.Add(HEADER_SERVICE_CODE, SEND_PUBLIC_KEY);
            var client = new HttpClient();
            var publickey = string.Empty;
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
                {
                    var response = taskwithmsg.Result;

                    ////Debug.WriteLine(response.ToString());
                    publickey = response.Content.ReadAsStringAsync().Result;
                    Debug.WriteLine(publickey);
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        Debug.WriteLine(
                            "{0}: {1} - {2} ",
                            response.StatusCode.ToString(),
                            response.ReasonPhrase,
                            publickey);
                        this.OperationResult.Operation = SEND_PUBLIC_KEY;
                        this.OperationResult.Status = (int)response.StatusCode;
                        this.OperationResult.ReasonPhrase = response.ReasonPhrase;

                        publickey = string.Empty;
                    }
                });
            task.Wait();

            return publickey;
        }

        /// <summary>
        /// Request a Valid operation key.
        /// You need this key to communicate with the service.
        /// The key travel encrypted.
        /// </summary>
        /// <param name="companySecret">Your specific company secret key</param>
        /// <param name="keyPublic">
        /// The public key of the WAPI Proxy. used to encrypt the company key</param>
        /// <param name="serviceUrl">
        /// The address of the WAPI proxy.
        /// </param>
        /// <returns>
        /// Return empty if the operation was not successful.
        /// if not return a API key. 
        /// </returns>
        public string GetAccessKey(string companySecret, string keyPublic, string serviceUrl)
        {
            // Encrypt the secret key
            // Exists then Encrypt
            var rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(keyPublic);
            var dataToEncrypt = Encoding.UTF8.GetBytes(companySecret);
            var encrypted = rsa.Encrypt(dataToEncrypt, true);
            var readyToSend = Convert.ToBase64String(encrypted);

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(serviceUrl),
                Method = HttpMethod.Get
            };

            request.Headers.Add(HEADER_COMPANY_SECRET, readyToSend);
            var client = new HttpClient();
            var accesskey = string.Empty;
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                accesskey = response.Content.ReadAsStringAsync().Result;
                Debug.WriteLine(accesskey);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Debug.WriteLine("{0}: {1} - {2} ", response.StatusCode.ToString(), response.ReasonPhrase, accesskey);
                    this.OperationResult.Operation = SEND_PUBLIC_KEY;
                    this.OperationResult.Status = (int)response.StatusCode;
                    this.OperationResult.ReasonPhrase = response.ReasonPhrase;

                    accesskey = string.Empty;
                }
            });
            task.Wait();

            return accesskey;
        }

        #endregion

        #region Basic GET and POST operation

        /// <summary>
        /// Make a GET request to the WAPI Proxy
        /// </summary>
        /// <param name="operation">The name of the operation to be executed by the proxy</param>
        /// <param name="accessKey">A valid access key</param>
        /// <param name="servicesUrl">The Proxy address, plus the query string if it is necessary</param>
        /// <param name="responseReason">
        /// return empty if it is successful the request, 
        /// if not successful return the reason of error.
        /// </param>
        /// <returns>
        /// The proxy operation response, usually a JSON string.
        /// </returns>
        public string SendOperation(string operation, string accessKey, string servicesUrl, out string responseReason)
        {
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get
            };

            request.Headers.Add(HEADER_SERVICE_CODE, operation);
            request.Headers.Add(HEADER_ACCESS_KEY, accessKey);
            var client = new HttpClient();
            client.Timeout = TimeSpan.FromMinutes(15);
            var operationResult = string.Empty;
            var reason = string.Empty;
            var errors = string.Empty;
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
                {
                    if (taskwithmsg.Status == TaskStatus.Faulted || taskwithmsg.Status == TaskStatus.Canceled)
                    {
                        errors = taskwithmsg.Status == TaskStatus.Canceled ? $"Task was canceled" : this.GetErrorString(taskwithmsg);

                        Debug.WriteLine($"operation fail: {operation} errors: {errors}");
                    }
                    else
                    {
                        var response = taskwithmsg.Result;
                        operationResult = response.Content.ReadAsStringAsync().Result;
                        reason = response.ReasonPhrase;
                        
                        // Debug.WriteLine(opResult);
                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            Debug.WriteLine(
                                "{0}: {1} - {2} ",
                                response.StatusCode.ToString(),
                                response.ReasonPhrase,
                                operationResult);
                            this.OperationResult.Operation = operation;
                            this.OperationResult.Status = (int)response.StatusCode;
                            this.OperationResult.ReasonPhrase = response.ReasonPhrase;
                            this.OperationResult.ExtendedReason = operationResult;
                        }
                    }
                });
            task.Wait();

            responseReason = (errors == string.Empty) ? reason : errors;
            return operationResult;
        }

        /// <summary>
        /// Send a Post operation to WAPI Proxy
        /// </summary>
        /// <param name="operation">The name of the invoked proxy service</param>
        /// <param name="accessKey">A valid access key</param>
        /// <param name="servicesUrl">
        /// THe proxy URL
        /// Can has a query string.
        /// </param>
        /// <param name="responseReason">
        /// If the code return was not 200 (OK) return the error reason.
        /// if return a string empty the operation was successful.
        /// </param>
        /// <param name="data">
        /// The string of data to be send to server.
        /// Should be a JSON formatted string.
        /// </param>
        /// <returns>
        /// The Response Content as String.
        /// </returns>
        public string PostOperation(string operation, string accessKey, string servicesUrl, out string responseReason, string data)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add(HEADER_SERVICE_CODE, operation);
            client.DefaultRequestHeaders.Add(HEADER_ACCESS_KEY, accessKey);
            client.DefaultRequestHeaders.Add("ContentType", "application/json");
            client.Timeout = TimeSpan.FromMinutes(30);
            var operationResult = string.Empty;
            var reason = string.Empty;
            var task = client.PostAsync(new Uri(servicesUrl), new StringContent(data, Encoding.Default, "application/json")).ContinueWith(taskwithmsg =>
                {
                    var response = taskwithmsg.Result;
                    operationResult = response.Content.ReadAsStringAsync().Result;

                    // Debug.WriteLine(opResult);
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        reason = response.ReasonPhrase;
                        Debug.WriteLine(
                            "{0}: {1} - {2} ",
                            response.StatusCode.ToString(),
                            response.ReasonPhrase,
                            operationResult);
                        this.OperationResult.Operation = SEND_PUBLIC_KEY;
                        this.OperationResult.Status = (int)response.StatusCode;
                        this.OperationResult.ReasonPhrase = response.ReasonPhrase;
                    }
                });
            task.Wait();

            responseReason = reason;
            return operationResult;
        }

        #endregion

        /// <summary>
        /// The get error string.
        /// </summary>
        /// <param name="resTask">
        /// The res task.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetErrorString(Task<HttpResponseMessage> resTask)
        {
            string output = string.Empty;
            if (resTask.Exception != null)
            {
                output = resTask.Exception.Message;
                Exception except = resTask.Exception;
                while (except.InnerException != null)
                {
                    output += except.InnerException.Message;
                    except = except.InnerException;
                }
            }

            return output;
        }
    }
}
