﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProxyConfiguration.cs" company="FAME">
//   2014
// </copyright>
// <summary>
// Advantage - WapiAdvantage - ProxyConfiguration.cs  
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace WapiAdvantage.Data
{
    using global::WapiAdvantage.Contract;

    /// <summary>
    /// Hold necessary information to configuration the proxy class WapiProxy.
    /// </summary>
    public class ProxyConfiguration: IProxyConfiguration
    {
        #region Implementation of IProxyConfiguration

        /// <summary>
        /// Advantage Proxy Address (mandatory)
        /// </summary>
        public string ProxyUrl { get; set; }

        /// <summary>
        /// Secret Company Key (mandatory)
        /// </summary>
        public string CompanyKey { get; set; }

        /// <summary>
        /// Log File (optional)<br/>
        /// Include the path and the filename. 
        /// </summary>
        public string LogFile { get; set; }

        /// <summary>
        /// Log file length in KB (optional)
        /// </summary>
        public int LogKb { get; set; }

        #endregion
    }
}