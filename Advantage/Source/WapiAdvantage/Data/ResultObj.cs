﻿namespace WapiAdvantage.Data
{
    /// <summary>
    /// Object to send back HTTP response or other responses.
    /// </summary>
    public class ResultObj
    {
        /// <summary>
        /// The HTTP status code number returned
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// The explanation about the status
        /// </summary>
        public string ReasonPhrase { get; set; }
          
        /// <summary>
        /// The operation that was involved
        /// </summary>
        public string Operation { get; set; }

        /// <summary>
        /// A extended optional explanation about the operation involved.
        /// </summary>
        public string ExtendedReason { get; set; }
  }
}
