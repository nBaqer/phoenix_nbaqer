﻿#region header
// // --------------------------------------------------------------------------------------------
// // Advantage - WapiAdvantage - WapiAdvantage.cs
// // Author: JAGG
// // © Copyright FAME 2014, 2014
// //---------------------------------------------------------------------------------------------
#endregion

using WapiAdvantage.Abstract;
using WapiAdvantage.Contract;
using WapiAdvantage.Data;

namespace WapiAdvantage
{
    /// <summary>
    /// This is a simple and functional implementation of WapiProxy abstract class.
    /// </summary>
    public class WapiAdvantage : WapiProxy
    {
        #region Operation Constants

        /// <summary>
        /// Test Operation Get the same parameter that you send.<br/>
        /// Parameters [FromUri]<br/>
        /// Three aleatory parameters<br/>
        /// Return: The same three parameters values concatenated.
        /// </summary>
        protected const string XOPERATION_ECHO_GET_PARAM = "ECHO_GET_PARAM";

        /// <summary>
        /// Test Operation Post. Get the same parameter and content body that you send.<br/>
        /// Parameters [FromBody]<br/>
        /// Three aleatory parameters<br/>
        /// Return: The same three parameters values concatenated.
        /// </summary>
        protected const string XOPERATION_ECHO_POST_PARAM = "ECHO_POST_PARAM";

        /// <summary>
        /// Test Operation: Echo operation. <br/>
        /// Parameters: None<br/>
        /// Return: May the force be with you.
        /// </summary>
        protected const string XOPERATION_ECHOSL = "ECHOSL";

        /// <summary>
        /// Windows Service: Get on demand flag operation.<br/>
        /// Parameter [FromUri]:<br/>
        /// id int : if it is missing or -1 return all flags. other value represent the service setting Id<br/>
        ///          and only return the value of the specific flag.
        /// </summary>
        protected const string XOPERATION_GET_ON_DEMAND_FLAG = "GET_ON_DEMAND_FLAG";

        /// <summary>
        /// Windows Service: Start/Stop the windows Service.<br/>
        /// Parameters [FromBody]:<br/>
        /// ServiceName string : The name of the service to change<br/>
        /// Command string : init, start, stop
        /// </summary>
        protected const string XOPERATION_INITWSERV = "INITWSERV";

        /// <summary>
        /// Windows Service: Post Date of last execution of specific Service.<br/>
        /// Parameters [FromUri]:<br/>
        /// Id int : The Id of the specific External Operation Settings.<br/>
        /// Value string : The DateTime value.
        /// </summary>
        protected const string XOPERATION_POST_DATA_LAST_EXECUTION = "POST_DATA_LAST_EXECUTION";

        /// <summary>
        /// Windows Service: Get the programmed external operation to be executed by the Advantage Windows Service<br/>
        /// Parameters [FromUri]:<br/>
        /// IdOperation int : 0 get all operations otherwise the ID of the specific operation.<br/>
        /// OnlyActives int : 0 Get All 1 Get Only Actives<br/>
        /// OperationMode string : string empty ignore parameter, Get Operations with the specific operation Mode.<br/>
        /// CompanyCode string : ALL : no filtered by company. Get operation possibles for the specific Company Key.
        /// </summary>
        protected const string XOPERATION_RSETTING = "RSETTING";

        /// <summary>
        /// Windows Service: Set/Reset on demand flag.<br/>
        /// Parameters [FromUri]:<br/>
        /// Id int : The Id of the specific External Operation Settings.<br/>
        /// Value int : 0 Reset 1 Set Flag. 
        /// </summary>
        protected const string XOPERATION_SET_ON_DEMAND_FLAG = "SET_ON_DEMAND_FLAG";

        /// <summary>
        /// Log Operation: Write in the Advantage Operation Log Table a row.<br/>
        /// Parameters [FromBody]<br/>
        /// Int64 Id <br/>
        ///  ServiceCode string : The code of the service that send the log information<br/>
        ///  CompanyCode string : The company secret key.<br/>
        ///  DateExecution DateTime : When was executed the logged operation.<br/>
        ///  NextPlanningDate DateTime: Next time that is planning to executed the operation<br/>
        ///  IsError int: If the operation was with error.<br/>
        ///  Comment string : A free comment.
        ///</summary>
        /// <remark> <b>This operation is intended to be used by the Advantage windows Service 
        /// due the confidential information involved</b></remark>
        protected const string XOPERATION_WLOG = "WLOG";

        /// <summary>
        /// VOYANT : Get fake student data<br/>
        /// Parameters: None<br/>
        /// Return a empty JSON object 
        /// </summary>
        protected const string XOPERATION_TEST_GET_FAKE = "TEST_GET_FAKE";

        /// <summary>
        /// VOYANT: Post to Advantage the URL of the dashboard prediction info.<br/>
        /// Parameters  [FromBody]:<br/>
        /// DashboardUrl: The dashboard control URL.
        /// </summary>
        protected const string XOPERATION_VOYANT_DASHBOARD_INFO = "VOYANT_DASHBOARD_INFO";

        /// <summary>
        /// VOYANT: Get to VOYANT the student information.<br/>
        /// Parameter[FromUri]:<br/>
        /// RequestHistorical int : <br/>
        /// 0 - Request historical data: Get graduated, dropped out, withdraw, transferred or probation<br/>
        /// 1 = Request Current data: Get students information currently attended. 
        /// </summary>
        protected const string XOPERATION_VOYANT_DATA = "VOYANT_DATA";


        #endregion

        /// <summary>
        /// Constructor for WapiAdvantage
        /// </summary>
        /// <param name="proxyconfig"> >Proxy Configuration parameters. See  <see cref="ProxyConfiguration"/></param>
        public WapiAdvantage(IProxyConfiguration proxyconfig)
            : base(proxyconfig)
        {

        }
    }
}