﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiProxy.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   This Abstract class is the base class to be
//   used as base for specific implementation
//   of Advantage Proxy class. JAGG
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiAdvantage.Abstract
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Threading;

    using global::WapiAdvantage.AccessProxy;

    using global::WapiAdvantage.Contract;

    using global::WapiAdvantage.Data;

    /// <summary>
    /// This Abstract class is the base class to be
    /// used as base for specific implementation
    /// of Advantage Proxy class
    /// </summary>
    public abstract class WapiProxy : IWapiProxy
    {
        #region Constants

        /// <summary>
        /// Message returned by the server when the access key is not longer valid
        /// </summary>
        private const string WRONG_ACCESS_KEY = "WRONG_ACCESS_KEY";

        #endregion

        #region Constructors

        // ReSharper disable UnusedMember.Local

        // ReSharper restore UnusedMember.Local

        /// <summary>
        /// Constructor to be used
        /// </summary>
        /// <param name="config">
        /// Holds the settings to configure the proxy class
        /// </param>
        protected WapiProxy(IProxyConfiguration config)
        {
            this.ConfigProxy = config;
            this.proxyKey = Guid.Empty.ToString();
            this.OperationResultList = new List<ResultObj>();
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="WapiProxy"/> class from being created. 
        /// Disable default parameter less constructor
        /// </summary>
        private WapiProxy()
        {
        }

        #endregion

        #region Implementation of IWapiProxy

        /// <summary>
        /// Gets or sets Secret company key
        /// </summary>
        public IProxyConfiguration ConfigProxy { get; set; }

        /// <summary>
        /// Gets or sets the list of result of operations
        /// </summary>
        public IList<ResultObj> OperationResultList;

        /// <summary>
        /// The key token to communicate with the advantage proxy
        /// this key is get from proxy directly in the login process.
        /// </summary>
        private string proxyKey;

        /// <summary>
        /// Public key to be used in encryption
        /// This key is supplied by the proxy.
        /// </summary>
        private string publicKey;

        /// <summary>
        /// Send a GET operation over the proxy
        /// </summary>
        /// <param name="commandName">The operation name invoked</param>
        /// <param name="paramstring">
        /// A optional query string. 
        /// </param>
        /// <example>
        /// <code>paramstring</code> Example Format:
        /// <code>
        /// ?key=valor
        /// ?key=valor&amp;key1=valor1&amp;key2=valor2
        /// </code>
        /// </example>
        /// <returns>
        /// The result as a JSON <code>formated</code> string.
        /// </returns>
        public virtual string GetMessageFromWapi(string commandName, string paramstring = "")
        {
            string result = string.Empty;
            string responseReason = string.Empty;
            try
            {
                this.OperationResultList = new List<ResultObj>(); // Clear result list
                var repeat = 3; // Hold the number of try of the routine before abandon.
                var proxy = new ServiceProxy();

                while (repeat > 0)
                {
                    if (Guid.Parse(this.proxyKey) != Guid.Empty)
                    {
                        // We have a proxy key try to use it....
                        result = proxy.SendOperation(
                            commandName,
                            this.proxyKey,
                            this.ConfigProxy.ProxyUrl + paramstring,
                            out responseReason);
                        Debug.WriteLine($"Operation send to proxy: {commandName}");
                        if (responseReason == "OK")
                        {
                            // OK
                            var res = new ResultObj { Status = 400, ReasonPhrase = "Successful", Operation = commandName };
                            this.OperationResultList.Add(res);

                            // WriteLogAsync("Successful", commandName);
                            return result;
                        }

                        // Error see if the error if because the accessKey is wrong or old.
                        if (responseReason.ToUpper() != WRONG_ACCESS_KEY)
                        {
                            // log information
                            this.OperationResultList.Add(proxy.OperationResult);

                            throw new ApplicationException(result);
                        }

                        // Other error notify and finish
                        this.OperationResultList.Add(proxy.OperationResult);
                        Debug.WriteLine("Access key wrong or void. program continue to required a new...");
                        this.proxyKey = Guid.Empty.ToString();
                    }

                    this.publicKey = proxy.GetPublicKey(this.ConfigProxy.ProxyUrl);
                    if (string.IsNullOrEmpty(this.publicKey))
                    {
                        // Log information
                        this.OperationResultList.Add(proxy.OperationResult);

                        // WriteLogAsync("Error:Public Key return null", commandName);
                        return null; // Indicate that some error occurred. Here we can log error in the operation
                    }

                    // Send encrypted companyCode
                    this.proxyKey = proxy.GetAccessKey(
                        this.ConfigProxy.CompanyKey,
                        this.publicKey,
                        this.ConfigProxy.ProxyUrl);
                    if (string.IsNullOrEmpty(this.proxyKey))
                    {
                        // Log information
                        this.OperationResultList.Add(proxy.OperationResult);

                        // WriteLogAsync("Error:Access Key returned empty", commandName);
                        return null; // Indicate that some error occurred. Here we can log error in the operation
                    }

                    repeat = repeat - 1;
                }
            }
            catch (Exception exception)
            {
                Exception newException = new Exception("Get Message From Wapi exception, commandName: " + commandName + ".Proxy url:" + this.ConfigProxy.ProxyUrl + " paramstring:" + paramstring + ". Proxy Result: " + result + ". Response Reason:"+ responseReason + ". Inner Exception: " + exception.Message + ". Stack Trace: " + exception.StackTrace, exception );
                throw newException;
            }

            return null;
        }

        /// <summary>
        /// Send a POST operation over the proxy
        /// </summary>
        /// <param name="commandName">The operation name invoked</param>
        /// <param name="payload">A JSON formatted string to be send is the message body</param>
        /// <param name="paramstring">
        /// A optional query string. 
        /// Format one parameter:  ?key=valor 
        /// Format several parameters: ?key=valor&amp;key1=valor1&amp;key2=valor2
        /// </param>
        /// <returns>Empty string is all OK, if not return the error cause</returns>
        public virtual string PostMessageToWapi(string commandName, string payload, string paramstring = "")
        {
            this.OperationResultList = new List<ResultObj>(); // Clear result list
            var repeat = 3; // Hold the number of try of the routine before abandon.
            var proxy = new ServiceProxy();

            while (repeat > 0)
            {
                if (Guid.Parse(this.proxyKey) != Guid.Empty)
                {
                    // We have a proxy key try to use it....
                    string responseReason;
                    var result = proxy.PostOperation(
                        commandName,
                        this.proxyKey,
                        this.ConfigProxy.ProxyUrl + paramstring,
                        out responseReason,
                        payload);
                    if (string.IsNullOrEmpty(responseReason))
                    {
                        // OK
                        var res = new ResultObj { Status = 400, ReasonPhrase = "Successful", Operation = commandName };
                        this.OperationResultList.Add(res);

                        // WriteLogAsync("Successful", commandName);
                        return result;
                    }

                    // Error see if the error if because the accessKey is wrong or old.
                    if (responseReason.ToUpper() != WRONG_ACCESS_KEY)
                    {
                        // log information
                        this.OperationResultList.Add(proxy.OperationResult);

                        // WriteLogAsync("Error: Access Key no longer valid", commandName);
                        return null;
                    }

                    this.proxyKey = Guid.Empty.ToString();
                }

                this.publicKey = proxy.GetPublicKey(this.ConfigProxy.ProxyUrl);
                if (string.IsNullOrEmpty(this.publicKey))
                {
                    // Log information
                    this.OperationResultList.Add(proxy.OperationResult);

                    // WriteLogAsync("Error:Public Key return null", commandName);
                    return null; // Indicate that some error occurred. Here we can log error in the operation
                }

                // Send encrypted companyCode
                this.proxyKey = proxy.GetAccessKey(
                    this.ConfigProxy.CompanyKey,
                    this.publicKey,
                    this.ConfigProxy.ProxyUrl);
                if (string.IsNullOrEmpty(this.proxyKey))
                {
                    // Log information
                    this.OperationResultList.Add(proxy.OperationResult);

                    // WriteLogAsync("Error:Access Key returned empty", commandName);
                    return null; // Indicate that some error occurred. Here we can log error in the operation
                }

                repeat = repeat - 1;
            }

            return null;
        }

        /// <summary>
        /// Write the log information and create  a new thread
        /// this method should be implement asynchronously
        /// </summary>
        /// <param name="message">Message to be write in proxy</param>
        /// <param name="commandname">
        /// The operation name invoked
        /// Usually used to put the operation involved in the log message.
        /// </param>
        public virtual void WriteLogAsync(string message, string commandname)
        {
            message = string.Format("{0} : {1}", commandname, message);
            ThreadPool.QueueUserWorkItem(this.WriteLog, message);
        }

        /// <summary>
        /// This method should be implemented by WriteLogAsync in a new thread.
        /// </summary>
        /// <param name="message">Message to be write in proxy</param>
        public virtual void WriteLog(object message)
        {
            message = string.Format("{0} - {1}", DateTime.Now.ToUniversalTime(), message);

            var file = this.ConfigProxy.LogFile;
            var l = this.ConfigProxy.LogKb;

            // Test if exists the file
            if (string.IsNullOrEmpty(file))
            {
                return;
            }

            // If not exists create and/or open
            if (File.Exists(file) == false)
            {
                File.CreateText(file);
            }

            if (l != 0)
            {
                var f = new FileInfo(file);

                // see file length > max length
                if (f.Length > l * 1024)
                {
                    string[] lines = File.ReadAllLines(file);
                    var cut = lines.Length / 2;
                    var newlines = new string[lines.Length - cut];
                    var x = 0;
                    for (int i = cut; i < lines.Length - 1; i++)
                    {
                        newlines[x] = lines[i];
                        x++;
                    }

                    // Delete the first half of the file
                    File.WriteAllLines(file, newlines);
                }
            }

            // Add the new line
            var app = new List<string> { (string)message };
            File.AppendAllLines(file, app);
        }

        #endregion
    }
}