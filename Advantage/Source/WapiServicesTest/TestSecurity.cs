﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestSecurity.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the TestSecurity type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServicesTest
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using WapiServices.App_Start;
    using WapiServices.Security;

    /// <summary>
    /// The test security.
    /// </summary>
    [TestClass]
    public class TestSecurity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestSecurity"/> class.
        /// </summary>
        public TestSecurity()
        {
            InitService.GetSettingFromWebConfig();
            InitService.GetProxySettings();
        }

        /// <summary>
        /// The test decrypt.
        /// </summary>
        [TestMethod]
        public void TestDecrypt()
        {
            Encryption.InitEncryption();
            const string Plainvalue = "TEST_VALUE123456";
            var returnedValue = Encryption.Encrypt(Plainvalue);
            Console.WriteLine(returnedValue);
            Assert.IsFalse(returnedValue == string.Empty);
            var res = Encryption.DecryptCompanyCode(returnedValue);
            Console.WriteLine(res);
            Assert.AreEqual(Plainvalue, res);
        }
    }
}
