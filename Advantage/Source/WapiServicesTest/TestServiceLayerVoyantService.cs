﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestServiceLayerVoyantService.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the TestServiceLayerVoyantService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServicesTest
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Net.Http;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// The test service layer <code>Voyant</code> service.
    /// </summary>
    [TestClass]
    public class TestServiceLayerVoyantService
    {
            /// <summary>
        /// This contains is different to the constant used by WAPI
        /// and it is only used to test purposes.
        /// </summary>
        private readonly string apiKey;

        /// <summary>
        /// The service layer WAPI security.
        /// </summary>
        private readonly string serviceLayerWapiSecurity;

        /// <summary>
        /// The service later WAPI student info.
        /// </summary>
        private readonly string serviceLaterWapiStudentInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestServiceLayerVoyantService"/> class.
        /// </summary>
        public TestServiceLayerVoyantService()
        {
            this.serviceLayerWapiSecurity = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_WAPI_VOYANT_FAKE");
            this.serviceLaterWapiStudentInfo = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_WAPI_VOYANT_STUDENT_INFO");
            this.apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }

        /// <summary>
        /// The get fake test.
        /// </summary>
        [TestMethod]
        public void GetFakeTest()
        {
            // send the login request as headers...
            string apikey = this.apiKey;
            string servicesUrl = this.serviceLayerWapiSecurity;
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                var res = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(res);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// The get student information test.
        /// </summary>
        [TestMethod]
        public void GetStudentInformationTest()
        {
            // send the login request as headers...
            string apikey = this.apiKey;
            string servicesUrl = this.serviceLaterWapiStudentInfo + "?RequestHistorical=1";
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                var res = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(res);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// The get student information current test.
        /// </summary>
        [TestMethod]
        public void GetStudentInformationCurrentTest()
        {
            // send the login request as headers...
            string apikey = this.apiKey;
            string servicesUrl = this.serviceLaterWapiStudentInfo + "?RequestHistorical=0";
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                var res = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(res);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }
    }
}
