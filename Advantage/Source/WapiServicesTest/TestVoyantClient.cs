﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestVoyantClient.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Summary description for TestVoyantClient
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServicesTest
{
    using System;
    using System.Configuration;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using WapiVoyantClient;

    /// <summary>
    /// Summary description for TestVoyantClient
    /// </summary>
    [TestClass]
    public class TestVoyantClient
    {
        /// <summary>
        /// The API key.
        /// </summary>
        private string apikey;

        /// <summary>
        /// The API secret.
        /// </summary>
        private string apisecret;

        /// <summary>
        /// The API path.
        /// </summary>
        private string apipath;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestVoyantClient"/> class.
        /// </summary>
        public TestVoyantClient()
        {
            this.apikey = ConfigurationManager.AppSettings.Get("API_KEY");
            this.apisecret = ConfigurationManager.AppSettings.Get("API_SECRET");
            this.apipath = ConfigurationManager.AppSettings.Get("API_PATH");
        }

        /// <summary>
        /// The test context instance.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion

        /// <summary>
        /// The test get create inputs.
        /// </summary>
        [TestMethod]
        public void TestGetCreateInputs()
        {
            var vy = ApiConnect.CreateInputs();
            string val1;
            vy.TryGetValue("TimeSpan", out val1);
            string val2;
            vy.TryGetValue("UniqueId", out val2);
            Console.WriteLine("TimeSpan: {0} UniqueId: {1}", val1, val2);
            Assert.IsFalse(string.IsNullOrEmpty(val1));
            Assert.IsFalse(string.IsNullOrEmpty(val2));
        }

        /// <summary>
        /// The test get authenticate key.
        /// </summary>
        [TestMethod]
        public void TestGetAuthenticateKey()
        {
            var vy = ApiConnect.Factory(this.apikey, this.apisecret, this.apipath);
            var parameters = vy.CreateParameters();
            Console.WriteLine(parameters);
            Assert.IsTrue(parameters.Contains("consumer_key"));
            Assert.IsTrue(parameters.Contains(this.apikey));
            Assert.IsTrue(parameters.Contains("timestamp"));
        }

        /// <summary>
        /// The test get signature.
        /// </summary>
        [TestMethod]
        public void TestGetSignature()
        {
            var vy = ApiConnect.Factory(this.apikey, this.apisecret, this.apipath);
            var parameters = vy.CreateParameters();
            var sign = vy.GetSig(string.Empty, HttpVerb.GET, parameters);
            Console.WriteLine(sign);
        }

        /// <summary>
        /// The test get request.
        /// </summary>
        [TestMethod]
        public void TestGetRequest()
        {
            var vy = ApiConnect.Factory(this.apikey, this.apisecret, this.apipath);
            var parameters = vy.CreateParameters();
            var sign = vy.GetSig("data", HttpVerb.GET, parameters);
            var response = vy.GetRequest("data", HttpVerb.GET, sign, parameters);
            Console.WriteLine(response);
        }
    }
}
