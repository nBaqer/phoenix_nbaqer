﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WapiAdvantage.Data;

namespace WapiServicesTest
{
    [TestClass]
    public class TestWapiAdvantage
    {

        private readonly string wapiAddress;
        private readonly string secretKey;

        public TestWapiAdvantage()
        {
            wapiAddress = ConfigurationManager.AppSettings.Get("WAPI_ADDRESS");
            secretKey = "FAME_TEST_WZ4LA8";
        }


        [TestMethod]
        public void TestGetInfo()
        {
            var operation = new ProxyConfiguration { ProxyUrl = wapiAddress, CompanyKey = secretKey };
            var dll = new WapiAdvantage.WapiAdvantage(operation);
            var result = dll.GetMessageFromWapi("ECHO");
            Assert.IsNotNull(result, "Fail, null was returned");
            Console.WriteLine(result);

        }

        [TestMethod]
        public void TestWithLogInfo()
        {
            var operation = new ProxyConfiguration { ProxyUrl = wapiAddress, CompanyKey = secretKey, LogFile = @"d:\WapiLog.log", LogKb = 1 };
            var dll = new WapiAdvantage.WapiAdvantage(operation);
            var result = dll.GetMessageFromWapi("ECHO");
            Assert.IsNotNull(result, "Fail, null was returned");
            Console.WriteLine(result); 
        }
    }
}
