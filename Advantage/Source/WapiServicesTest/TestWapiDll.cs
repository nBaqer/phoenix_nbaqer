﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestWapiDll.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the TestWapiDll type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServicesTest
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Threading;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using WapiDll;
    using WapiDll.Data;
    using WapiDll.Data.Contract;

    /// <summary>
    /// The test WAPI DLL.
    /// </summary>
    [TestClass]
    public class TestWapiDll
    {
        /// <summary>
        /// The WAPI address.
        /// </summary>
        private readonly string wapiAddress;

        /// <summary>
        /// The secret key.
        /// </summary>
        private readonly string secretKey;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestWapiDll"/> class.
        /// </summary>
        public TestWapiDll()
        {
            this.wapiAddress = ConfigurationManager.AppSettings.Get("WAPI_ADDRESS");
            this.secretKey = "FAME_2154673_default";
        }

        /// <summary>
        /// The get list operations from proxy.
        /// </summary>
        [TestMethod]
        public void GetListOperationsFromProxy()
        {
            string reason;
            var result = WapiService.GetListOperationsFromProxy(this.secretKey, this.wapiAddress, out reason);
            foreach (IWapiOperationsSettings s in result)
            {
                Console.WriteLine("Id {0} Operation: {1} Last Execution: {2}", s.ID, s.CodeOperation, s.DateLastExecution);
            }

            Assert.IsNotNull(result, "Fail, null was returned");
        }

        /// <summary>
        /// The create WAPI DLL instance test.
        /// </summary>
        [TestMethod]
        public void CreateWapiDllInstanceTest()
        {
            string reason;
            var resultList = WapiService.GetListOperationsFromProxy(this.secretKey, this.wapiAddress, out reason);
            Assert.IsNotNull(resultList, "Fail, null was returned");
            IList<IWapiService> wapiOperation = new List<IWapiService>();
            foreach (IWapiOperationsSettings settings in resultList)
            {
                IWapiService wapi = WapiServiceVoyantHistorical.Factory(settings, ref wapiOperation);
                wapiOperation.Add(wapi);
            }

            Assert.IsTrue(wapiOperation.Count == resultList.Count);
        }

        /// <summary>
        /// The get on demand flag from proxy.
        /// </summary>
        [TestMethod]
        public void GetOnDemandFlagFromProxy()
        {
            var operation = new WapiOperationsSettings
                                {
                                    UrlProxy = this.wapiAddress,
                                    SecretCodeCompany = this.secretKey
                                };
            var dll = new WapiServiceVoyantHistorical { Operation = operation };
            var result = dll.GetOnDemandFlags();
            Assert.IsNotNull(result, "Fail, null was returned");
            foreach (IOnDemandFlag onDemandFlag in result)
            {
                Console.WriteLine("id: {0} - FlagValue: {1} ", onDemandFlag.Id, onDemandFlag.OnDemandOperation);
            }
        }

        /// <summary>
        /// The post on demand flag from proxy.
        /// </summary>
        [TestMethod]
        public void PostOnDemandFlagFromProxy()
        {
            var operation = new WapiOperationsSettings();
            operation.UrlProxy = this.wapiAddress;
            operation.ID = 1;
            operation.SecretCodeCompany = this.secretKey;
            var dll = new WapiServiceVoyantHistorical { Operation = operation };
            dll.PostOnDemandFlag(operation.ID, 1);
            Thread.Sleep(1000);

            // Read the value
            var result = dll.GetOnDemandFlags(operation.ID);
            Assert.IsNotNull(result, "Fail, null was returned");
            foreach (IOnDemandFlag onDemandFlag in result)
            {
                Console.WriteLine("id: {0} - FlagValue: {1} ", onDemandFlag.Id, onDemandFlag.OnDemandOperation);
                Assert.AreEqual(1, onDemandFlag.OnDemandOperation);
                Assert.AreEqual(1, onDemandFlag.Id);
            }

            // Return the value
            dll.PostOnDemandFlag(operation.ID, 0);
            result = dll.GetOnDemandFlags(operation.ID);
            Assert.IsNotNull(result, "Fail, null was returned");
            foreach (IOnDemandFlag onDemandFlag in result)
            {
                Console.WriteLine("id: {0} - FlagValue: {1} ", onDemandFlag.Id, onDemandFlag.OnDemandOperation);
                Assert.AreEqual(0, onDemandFlag.OnDemandOperation);
                Assert.AreEqual(1, onDemandFlag.Id);
            }
        }
    }
}
