﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestServiceLayerOperationSettingService.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the TestServiceLayerOperationSettingService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServicesTest
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Net;
    using System.Net.Http;

    using FAME.Advantage.Messages.SystemStuff.Maintenance;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// The test service layer operation setting service.
    /// </summary>
    [TestClass]
    public class TestServiceLayerOperationSettingService
    {
        // ReSharper disable StyleCop.SA1600
        private readonly string serviceLayerWapiOperationSettings;

        private readonly string serviceLayerWapiCatalogSettings;
        private readonly string serviceLayerWapiUpdateOperationSetting;
        private readonly string serviceLayerWapiOnDemandFlags;
        private readonly string serviceLayerWapiDateLastExecutionPost;
        private readonly string serviceLayerWapiDateLastExecutionGet;
        private readonly string serviceLayerWapiTruncLoggerPost;
        private readonly string serviceLayerWapiSingleOnDemandFlag;
        private readonly string serviceLayerGetPRogramVersion;
        private readonly string apiKey;
        // ReSharper restore StyleCop.SA1600
        
        /// <summary>
        /// Initializes a new instance of the <see cref="TestServiceLayerOperationSettingService"/> class.
        /// </summary>
        public TestServiceLayerOperationSettingService()
        {
            this.serviceLayerWapiCatalogSettings =
                ConfigurationManager.AppSettings.Get("SERVICE_LAYER_WAPI_OPERATION_SETTINGS_CATALOG");
            this.serviceLayerWapiOperationSettings =
                ConfigurationManager.AppSettings.Get("SERVICE_LAYER_WAPI_OPERATION_SETTINGS_TEST");
            this.serviceLayerWapiUpdateOperationSetting = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_WAPI_OPERATION_SETTINGS_UPDATE");
            this.serviceLayerWapiOnDemandFlags = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_WAPI_ON_DEMAND_FLAGS");
            this.serviceLayerWapiDateLastExecutionPost = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_WAPI_DATE_LAST_EXECUTION_POST");
            this.serviceLayerWapiDateLastExecutionGet = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_WAPI_DATE_LAST_EXECUTION_GET");
            this.serviceLayerWapiTruncLoggerPost = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_WAPI_TRUNC_LOGGER_EXECUTION_POST");
            this.serviceLayerWapiSingleOnDemandFlag = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_WAPI_SINGLE_ON_DEMAND_FLAGS");

            this.serviceLayerGetPRogramVersion = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PROGRAM_VERSION");

            this.apiKey = ConfigurationManager.AppSettings.Get("APIKEY");

        }

        #region Test WAPI Operation Settings

        /// <summary>
        /// The operation settings test service layer get all.
        /// </summary>
        [TestMethod]
        public void OperationSettingsTestServiceLayerGetAll()
        {
            // send the login request as headers...
            string apikey = this.apiKey;
            string servicesUrl = this.serviceLayerWapiOperationSettings + "?IdOperation=0";
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };

            // request.Headers.Add("ServiceName", "WapiWindowsService");
            // request.Headers.Add("Command", "");
            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            });
            task.Wait();
        }

        [TestMethod]
        public void OperationSettingsTestServiceLayerGet1()
        {
            // send the login request as headers...
            string apikey = this.apiKey;
            string servicesUrl = this.serviceLayerWapiOperationSettings + "?IdOperation=1";
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };

            // request.Headers.Add("ServiceName", "WapiWindowsService");
            // request.Headers.Add("Command", "");
            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            });
            task.Wait();
        }

        [TestMethod]
        public void OperationSettingsTestServiceLayerActivosGet()
        {
            // send the login request as headers...
            string apikey = this.apiKey;
            string servicesUrl = this.serviceLayerWapiOperationSettings + "?IdOperation=0&OnlyActives=1";
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };

            // request.Headers.Add("ServiceName", "WapiWindowsService");
            // request.Headers.Add("Command", "");
            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            });
            task.Wait();
        }

        [TestMethod]
        public void UpdateOperationSetting()
        {
            var list = this.OperationSettingsGetAll();
            if (list.Count == 0)
            {
                Assert.Inconclusive("It is not possible to test because does not exist a operation in database");
            }

            var operation = list[0];
            var actualDescription = operation.DescripExtCompany;
            const string NEW_DESCRIPTION = "TEST OPERATION";
            string apikey = this.apiKey;
            operation.DescripExtCompany = NEW_DESCRIPTION;

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", apikey);
            var task =
                client.PostAsJsonAsync(this.serviceLayerWapiUpdateOperationSetting, operation)
                    .ContinueWith(
                        taskwithmsg =>
                            {
                                var response = taskwithmsg.Result;
                                Console.WriteLine(response.ToString());
                                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                                return response;
                            });
            task.Wait();

            // Restaure value
            operation.DescripExtCompany = actualDescription;
            var task1 =
                client.PostAsJsonAsync(this.serviceLayerWapiUpdateOperationSetting, operation)
                    .ContinueWith(
                        taskwithmsg =>
                            {
                                var response = taskwithmsg.Result;
                                Console.WriteLine(response.ToString());
                                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                                return response;
                            });
            task1.Wait();
        }

        #endregion

        #region Catalog Tests

        /// <summary>
        /// The get external company.
        /// </summary>
        [TestMethod]
        public void GetExternalCompany()
        {
            const string Filter = "COMPANY";
            string servicesUrl = this.serviceLayerWapiCatalogSettings + "?CatalogOperation=" + Filter;
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", this.apiKey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();

        }

        /// <summary>
        /// The get operation mode.
        /// </summary>
        [TestMethod]
        public void GetOperationMode()
        {
            const string Filter = "MODE";
            string servicesUrl = this.serviceLayerWapiCatalogSettings + "?CatalogOperation=" + Filter;
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", this.apiKey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            });
            task.Wait();

        }

        /// <summary>
        /// The get service allowed.
        /// </summary>
        [TestMethod]
        public void GetServiceAllowed()
        {
            const string Filter = "WSERVICES";
            string servicesUrl = this.serviceLayerWapiCatalogSettings + "?CatalogOperation=" + Filter;
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", this.apiKey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });

            task.Wait();
        }

        #endregion

        /// <summary>
        /// The get all on demand flags test.
        /// </summary>
        [TestMethod]
        public void GetAllOnDemandFlagsTest()
        {
            // send the login request as headers...
            string apikey = this.apiKey;
            string servicesUrl = this.serviceLayerWapiOnDemandFlags;
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };

            // request.Headers.Add("ServiceName", "WapiWindowsService");
            // request.Headers.Add("Command", "");
            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// Note this test fail if there are not declare any operation in server
        /// </summary>
        [TestMethod]
        public void GetOneOnDemandFlagsTest()
        {
            // send the login request as headers...
            string apikey = this.apiKey;
            string servicesUrl = this.serviceLayerWapiOnDemandFlags + "?Id=1";
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            });
            task.Wait();
        }


        /// <summary>
        /// Note this test fail if there are not declare any operation in server
        /// </summary>
        [TestMethod]
        public void GetOneOnDemandFlagAndResultTest()
        {
            // send the login request as headers...
            string apikey = this.apiKey;
            string servicesUrl = this.serviceLayerWapiSingleOnDemandFlag + "?Id=1";
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            });
            task.Wait();
        }

        /// <summary>
        /// The truncate <code>syWapiLogger</code> test.
        /// </summary>
        [TestMethod]
        public void TruncSyWapiLoggerTest()
        {
            // send the login request as headers...
            string apikey = this.apiKey;
            var client = new HttpClient();
         
   
            client.DefaultRequestHeaders.Add("AuthKey", apikey);
            var task = client.PostAsJsonAsync(this.serviceLayerWapiTruncLoggerPost, string.Empty).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                return response;
            });
            task.Wait();
        }

        // Helpers
        public IList<WapiOperationSettingOutputModel> OperationSettingsGetAll()
        {
            // send the login request as headers...
            string apikey = this.apiKey;
            string servicesUrl = this.serviceLayerWapiOperationSettings + "?IdOperation=0&OperationMode=all&CompanyCode=all";
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                var res = response.Content.ReadAsStringAsync().Result;
                return res;
            });
            task.Wait();
            var list = (IList<WapiOperationSettingOutputModel>)JsonConvert.DeserializeObject(task.Result, typeof(List<WapiOperationSettingOutputModel>));
            return list;
        }

        /// <summary>
        /// NOte this test fail if there are not declare any operation in server
        /// </summary>
        [TestMethod]
        public void SetDateLastExecutionTest()
        {
            // Get the Actual DateLastExecution and store in hold
            Console.WriteLine("Get actual value of LastExecution.............................");
            string apikey = this.apiKey;
            string hold = DateTime.Now.ToLongTimeString();

            // Get the actual value of Date
            string servicesUrl = this.serviceLayerWapiDateLastExecutionGet + "?Id=1";
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                var res = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(res);
                var val = JArray.Parse(res);
                var elem = JObject.Parse(val[0].ToString());
                hold = (string)elem.GetValue("Value");
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();

            try
            {
                // Write a value four day before
                Console.WriteLine("Change the value for one four day before........................");
                var storedTime = DateTime.Now.AddDays(-1);
                servicesUrl = this.serviceLayerWapiDateLastExecutionPost + "?Id=1&Value=" + storedTime;

                client = new HttpClient();
                client.DefaultRequestHeaders.Add("AuthKey", apikey);
                task = client.PostAsJsonAsync(servicesUrl, string.Empty).ContinueWith(taskwithmsg =>
                {
                    var response = taskwithmsg.Result;
                    Console.WriteLine(response.ToString());
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    return response;
                });
                task.Wait();

                // Read if the value was cjanged ....................................................
                Console.WriteLine("Read if the value was changed.....................................");
                servicesUrl = this.serviceLayerWapiDateLastExecutionGet + "?Id=1";
                client = new HttpClient();
                request = new HttpRequestMessage
                {
                    RequestUri = new Uri(servicesUrl),
                    Method = HttpMethod.Get,
                };

                request.Headers.Add("AuthKey", apikey);
                task = client.SendAsync(request).ContinueWith(taskwithmsg =>
                {
                    var response = taskwithmsg.Result;
                    Console.WriteLine(response.ToString());
                    var res = response.Content.ReadAsStringAsync().Result;
                    Console.WriteLine(res);
                    var val = JArray.Parse(res);
                    var elem = JObject.Parse(val[0].ToString());
                    var newVal = (string)elem.GetValue("Value");
                    Assert.AreEqual(storedTime.Day, DateTime.Parse(newVal).Day);
                });
                task.Wait();
            }
            finally
            {
                // return the original value
                Console.WriteLine("Write the original value.......................................");

                servicesUrl = this.serviceLayerWapiDateLastExecutionPost + "?Id=1&Value=" + hold;

                client = new HttpClient();
                client.DefaultRequestHeaders.Add("AuthKey", apikey);
                task = client.PostAsJsonAsync(servicesUrl, string.Empty).ContinueWith(taskwithmsg =>
                {
                    var response = taskwithmsg.Result;
                    Console.WriteLine(response.ToString());
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    return response;
                });
                task.Wait();
            }
        }

        /// <summary>
        /// The test version service.
        /// </summary>
        [TestMethod]
        public void TestVersionService()
        {
            // send the login request as headers...
            string apikey = this.apiKey;

            // Get the actual value of Date
            string servicesUrl = this.serviceLayerGetPRogramVersion;
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                return response;
            });
            task.Wait();
        }
    }
}
