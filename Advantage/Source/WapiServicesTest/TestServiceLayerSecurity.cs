﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WapiServicesTest
{
    [TestClass]
    public class TestServiceLayerSecurity
    {
        /// <summary>
        /// This constants is different to the constant used by WAPI
        /// and it is only used to test purposes.
        /// </summary>
        private readonly string apiKey;
        private readonly string serviceLayerWapiSecurity;

        public TestServiceLayerSecurity()
        {
            this.serviceLayerWapiSecurity =
                ConfigurationManager.AppSettings.Get("SERVICE_LAYER_WAPI_OPERATION_SECURITY");
            this.apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }


        /// <summary>
        /// Test if the procedure can create the key in 
        /// container, previously key is deleted 
        /// created key is deleted.
        /// </summary>
        [TestMethod]
        public void TestCreateKeyIfNotExists()
        {
            this.CreateTheKey();
            this.DeleteTheKey();
            var key = this.CreateTheKey();
            this.DeleteTheKey();
            Assert.IsTrue(key.ToUpper().Contains("ERROR") == false);
        }

        [TestMethod]
        public void TestCreateKeyAndKeyExists()
        {
            var key = this.CreateTheKey();
            if (key.ToUpper().Contains("ERROR") == false)
            {
                key = this.CreateTheKey();
            }

            this.DeleteTheKey();
            Assert.IsTrue(key.ToUpper().Contains("ERROR"));
        }


        [TestMethod]
        public void TestGetTheKeyExists()
        {
            // Create the key if not exists
            this.CreateTheKey();
            var res = this.GetTheKey();
            Assert.IsTrue(res.ToUpper().Contains("ERROR") == false);
        }

        [TestMethod]
        public void TestGetTheKeyIfNotExists()
        {
            // Create the key if not exists
            this.DeleteTheKey();
            var res = this.GetTheKey();
            Assert.IsTrue(res.ToUpper().Contains("ERROR"));
        }


        /// <summary>
        /// 2: delete key if exists
        /// Delete existing key, create it and then delete it.
        /// </summary>
        [TestMethod]
        public void TestDeleteTheKey()
        {
            this.CreateTheKey();
            var res = this.DeleteTheKey();
            var key = this.GetTheKey();
            Assert.IsTrue(res.ToUpper().Contains("KEY DELETED"));
            Assert.IsTrue(key.ToUpper().Contains("ERROR"));
        }

        #region private helper functions

        private string DeleteTheKey()
        {
            string result = string.Empty;

            // send the login request as headers...
            string apikey = this.apiKey;
            string servicesUrl = this.serviceLayerWapiSecurity + "?operation=42";
            var client = new HttpClient();
            var request = new HttpRequestMessage { RequestUri = new Uri(servicesUrl), Method = HttpMethod.Get, };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(
                taskwithmsg =>
                    {
                        var response = taskwithmsg.Result;
                        Console.WriteLine(response.ToString());
                        result = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine(result);
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    });
            task.Wait();
            return result;
        }

        /// <summary>
        /// Create the key
        /// </summary>
        private string CreateTheKey()
        {
            string result = string.Empty;

            // send the login request as headers...
            string apikey = this.apiKey;
            string servicesUrl = this.serviceLayerWapiSecurity + "?operation=41";
            var client = new HttpClient();
            var request = new HttpRequestMessage { RequestUri = new Uri(servicesUrl), Method = HttpMethod.Get, };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(
                taskwithmsg =>
                    {
                        var response = taskwithmsg.Result;
                        Console.WriteLine(response.ToString());
                        result = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine(result);
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    });
            task.Wait();
            return result;
        }

        /// <summary>
        /// Operation 3 get the key
        /// </summary>
        /// <returns>
        /// The key if exists
        /// A error message
        /// </returns>
        private string GetTheKey()
        {
            var result = string.Empty;

            // send the login request as headers...
            string apikey = this.apiKey;
            string servicesUrl = this.serviceLayerWapiSecurity + "?operation=43";
            var client = new HttpClient();
            var request = new HttpRequestMessage { RequestUri = new Uri(servicesUrl), Method = HttpMethod.Get, };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(
                taskwithmsg =>
                    {
                        var response = taskwithmsg.Result;
                        Console.WriteLine(response.ToString());
                        result = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine(result);
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    });
            task.Wait();
            return result;
        }

        #endregion

    }
}
