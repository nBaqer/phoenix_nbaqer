﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestProxyLayer.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   The test proxy layer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServicesTest
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Net.Http;
    using System.Security.Cryptography;
    using System.Text;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Newtonsoft.Json;

    /// <summary>
    /// The test proxy layer.
    /// </summary>
    [TestClass]
    public class TestProxyLayer
    {
        /// <summary>
        /// The WAPI address.
        /// </summary>
        private readonly string wapiAddress;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestProxyLayer"/> class.
        /// </summary>
        public TestProxyLayer()
        {
            this.wapiAddress = ConfigurationManager.AppSettings.Get("WAPI_ADDRESS");
        }

        #region Elemental Positive testing

        /// <summary>
        /// Test the WAPI access. Test communication
        /// This test only test the communication with the proxy. no login information is send
        /// </summary>
        [TestMethod]
        public void EchoTest()
        {
            var servicesUrl = this.wapiAddress;
            const string Operation = "ECHO";
            var client = new HttpClient();

            var request = new HttpRequestMessage
           {
               RequestUri = new Uri(servicesUrl),
               Method = HttpMethod.Get
           };

            request.Headers.Add("SERVICE_CODE", Operation);

            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// The test get public key.
        /// </summary>
        [TestMethod]
        public void TestGetPublicKey()
        {
            var publickey = this.GetPublicKey();
            Console.WriteLine(publickey);
            Assert.AreNotEqual(null, publickey);
            Assert.AreNotEqual(string.Empty, publickey);
        }

        /// <summary>
        /// The test get access key.
        /// </summary>
        [TestMethod]
        public void TestGetAccessKey()
        {
            // Get Public Key
            var publickey = this.GetPublicKey();

            // Request for the ACCESS_KEY 
            var accessKey = this.GetAccessKey("FAME_2154673_default", publickey);
            Console.WriteLine(accessKey);
            Assert.AreNotEqual(null, accessKey);
            Assert.AreNotEqual(string.Empty, accessKey);
        }

        /// <summary>
        /// TEST: Make a authorized operation
        /// </summary>
        [TestMethod]
        public void EchoDbTest()
        {
            // Get Public Key
            var publickey = this.GetPublicKey();

            // Request for the ACCESS_KEY 
            var accessKey = this.GetAccessKey("FAME_2154673_default", publickey);

            // Send the operation ECHOSL
            string result = this.SendOperation("ECHOSL", accessKey);
            Console.WriteLine(result);
            Assert.AreNotEqual(null, result);
            Assert.AreNotEqual(string.Empty, result);
        }

        /// <summary>
        /// TEST: Make a authorized operation
        /// </summary>
        [TestMethod]
        public void GetFakeTest()
        {
            // Get Public Key
            var publickey = this.GetPublicKey();

            // Request for the ACCESS_KEY 
            var accessKey = this.GetAccessKey("FAME_2154673_default", publickey);

            // Send the operation ECHOSL
            string result = this.SendOperation("VOYANT_GET_FAKE", accessKey);
            Console.WriteLine(result);
            Assert.AreNotEqual(null, result);
            Assert.AreNotEqual(string.Empty, result);
        }

        /// <summary>
        /// TEST: Make a authorized operation
        /// </summary>
        [TestMethod]
        public void GetVoyantStudentHistoricalTest()
        {
            // Get Public Key
            var publickey = this.GetPublicKey();

            // Request for the ACCESS_KEY 
            var accessKey = this.GetAccessKey("FAME_2154673_default", publickey);

            // Send the operation ECHOSL
            string result = this.SendOperation("VOYANT_DATA", accessKey, "RequestHistorical=1");
            Console.WriteLine("Payload lenght: {0} kb", result.Length / 1000);
            Console.WriteLine(result);
            Assert.AreNotEqual(null, result);
            Assert.AreNotEqual(string.Empty, result);
        }

        /// <summary>
        /// TEST: Make a authorized operation
        /// </summary>
        [TestMethod]
        public void GetVoyantStudentWeeklyTest()
        {
            // Get Public Key
            var publickey = this.GetPublicKey();

            // Request for the ACCESS_KEY 
            var accessKey = this.GetAccessKey("FAME_2154673_default", publickey);

            // Send the operation ECHOSL
            string result = this.SendOperation("VOYANT_DATA", accessKey, "RequestHistorical=0");
            Console.WriteLine("Payload lenght: {0} kb", result.Length / 1000);
            Console.WriteLine(result);
            Assert.AreNotEqual(null, result);
            Assert.AreNotEqual(string.Empty, result);
        }

        /// <summary>
        /// TEST: Make a authorized operation
        /// </summary>
        [TestMethod]
        public void PostVoyantDashBoardUrlTest()
        {
            // Get Public Key
            var publickey = this.GetPublicKey();

            // Request for the ACCESS_KEY 
            var accessKey = this.GetAccessKey("FAME_2154673_default", publickey);

            // Send the operation ECHOSL
            // var data = new[] { "http://localhost/Advantage/Current/Site/VoyantDefault.html" };
            // Serialise the data we are sending in to JSON
            // string serialisedData = JsonConvert.SerializeObject(data);
            var serialisedData = "{\"DashboardUrl\":\"http://localhost/Advantage/Current/Site/VoyantDefault.html\"}";

            string result = this.PostOperation("VOYANT_DASHBOARD_INFO", accessKey, serialisedData);
            Console.WriteLine(result);
            Assert.AreNotEqual(null, result);
            Assert.AreNotEqual(string.Empty, result);
        }

        /// <summary>
        /// TEST: Make a authorized operation
        /// </summary>
        [TestMethod]
        public void EchoGetParamTest()
        {
            // Get Public Key
            var publickey = this.GetPublicKey();

            // Request for the ACCESS_KEY 
            var accessKey = this.GetAccessKey("FAME_2154673_default", publickey);

            // Send the operation ECHOSL
            const string Param = "param1=p1&param2=p2&param3=p3";
            string result = this.SendOperation("ECHO_GET_PARAM", accessKey, Param);
            Console.WriteLine(result);
            Assert.AreEqual("\"p1p2p3\"", result);
        }

        /// <summary>
        /// The echo SL test.
        /// Return the force 
        /// </summary>
        [TestMethod]
        public void EchoSlTest()
        {
            // Get Public Key
            var publickey = this.GetPublicKey();

            // Request for the ACCESS_KEY 
            var accessKey = this.GetAccessKey("FAME_2154673_default", publickey);

            // Send the operation ECHOSL
            string result = this.SendOperation("ECHOSL", accessKey);
            Console.WriteLine(result);
            Assert.AreEqual("[\"May\",\"the\",\"Force\",\"be\",\"with\",\"you\"]", result);
        }

        /// <summary>
        /// The echo post test.
        /// </summary>
        [TestMethod]
        public void EchoPostTest()
        {
            // Get Public Key
            var publickey = this.GetPublicKey();

            // Request for the ACCESS_KEY 
            var accessKey = this.GetAccessKey("FAME_2154673_default", publickey);

            // Send the operation ECHOSL
            var data = new[] { "This", " is", " test", " input", " data." };

            // Serialize the data we are sending in to JSON
            string serialisedData = JsonConvert.SerializeObject(data);

            string result = this.PostOperation("ECHO_POST_PARAM", accessKey, serialisedData);
            Console.WriteLine(result);
            Assert.AreEqual("\"Successfully uploaded: This, is, test, input, data.\"", result);
        }

        #endregion

        #region Echo Negative Testing

        /// <summary>
        /// Test the WAPI access. Test the authentication phase.
        /// </summary>
        [TestMethod]
        public void ErrorCompanyNotAuthorizedTest()
        {
            // Get Public Key
            var publickey = this.GetPublicKey();

            // Request for the ACCESS_KEY 
            var accessKey = this.GetAccessKey("FALSE_COMPANY_CODE", publickey);
            Console.WriteLine(accessKey);
            Assert.AreEqual("Bad Format or Fail authorization", accessKey);
        }

        /// <summary>
        /// If the company is not listed a authentication error is returned
        /// </summary>
        [TestMethod]
        public void ErrorOperationNotExistTest()
        {
            // Get Public Key
            var publickey = this.GetPublicKey();

            // Request for the ACCESS_KEY 
            var accessKey = this.GetAccessKey("FAME_2154673_default", publickey);

            // Send the operation ECHOSL
            string result = this.SendOperation("XXXX", accessKey);
            Console.WriteLine(result);
            Assert.AreEqual("Your are not right to do the operation", result);
        }

        #endregion

        #region Private method

        /// <summary>
        /// Return the public key from server
        /// </summary>
        /// <returns>The public key from server</returns>
        public string GetPublicKey()
        {
            var servicesUrl = this.wapiAddress;
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get
            };

            request.Headers.Add("SERVICE_CODE", "SEND_PUBLIC_KEY");
            var client = new HttpClient();
            var publickey = string.Empty;
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                publickey = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(publickey);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();

            return publickey;
        }

        /// <summary>
        /// Send the Secret key coded and return the access key
        /// from server.
        /// </summary>
        /// <param name="companySecret">The company secret key</param>
        /// <param name="keyPublic">the public key supplied by proxy</param>
        /// <returns>The access token to communicate</returns>
        private string GetAccessKey(string companySecret, string keyPublic)
        {
            // Encrypt the secret key
            // Exists then Encrypt
            var rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(keyPublic);
            var dataToEncrypt = Encoding.UTF8.GetBytes(companySecret);
            var encrypted = rsa.Encrypt(dataToEncrypt, true);
            var readyToSend = Convert.ToBase64String(encrypted);

            var servicesUrl = this.wapiAddress;
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get
            };

            request.Headers.Add("COMPANY_SECRET", readyToSend);
            var client = new HttpClient();
            var accesskey = string.Empty;
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                accesskey = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(accesskey);
            });
            task.Wait();

            return accesskey;
        }

        /// <summary>
        /// Send a operation to server
        /// </summary>
        /// <param name="operation">Operation Name</param>
        /// <param name="accessKey">Access Key</param>
        /// <returns>Send result to operation to server</returns>
        private string SendOperation(string operation, string accessKey)
        {
            var servicesUrl = this.wapiAddress;
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get
            };

            request.Headers.Add("SERVICE_CODE", operation);
            request.Headers.Add("ACCESS_KEY", accessKey);
            var client = new HttpClient();
            var operationResult = string.Empty;
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                operationResult = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(operationResult);
            });
            task.Wait();

            return operationResult;
        }

        /// <summary>
        /// The send operation.
        /// </summary>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <param name="accessKey">
        /// The access key.
        /// </param>
        /// <param name="queryString">
        /// The query string.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string SendOperation(string operation, string accessKey, string queryString)
        {
            var servicesUrl = this.wapiAddress + "?" + queryString;
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get
            };

            request.Headers.Add("SERVICE_CODE", operation);
            request.Headers.Add("ACCESS_KEY", accessKey);
            var client = new HttpClient();
            var operationResult = string.Empty;
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                operationResult = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(operationResult);
            });
            task.Wait();

            return operationResult;
        }

        /// <summary>
        /// The post operation.
        /// </summary>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <param name="accessKey">
        /// The access key.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string PostOperation(string operation, string accessKey, string data)
        {
            var servicesUrl = this.wapiAddress;
            var operationResult = string.Empty;
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("SERVICE_CODE", operation);
            client.DefaultRequestHeaders.Add("ACCESS_KEY", accessKey);
            client.DefaultRequestHeaders.Add("ContentType", "application/json");

            var task = client.PostAsync(new Uri(servicesUrl), new StringContent(data, Encoding.Default, "application/json")).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                operationResult = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(operationResult);
            });
            task.Wait();
            return operationResult;
        }

        #endregion
    }
}
