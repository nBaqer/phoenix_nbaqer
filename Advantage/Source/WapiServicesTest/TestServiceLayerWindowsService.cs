﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WapiServicesTest
{
    [TestClass]
    public class TestServiceLayerWindowsService
    {
        private readonly string serviceLayerTestEchoAddress;
        private readonly string serviceLayerWapiWindowsService;
        private readonly string serviceLayerWapiWindowsServicePost;
       
        private readonly string apiKey;

        public TestServiceLayerWindowsService()
        {
            serviceLayerTestEchoAddress = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_ECHO_TEST");
            serviceLayerWapiWindowsService = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_WINDOWS_SERVICE_TEST");
            serviceLayerWapiWindowsServicePost = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_WINDOWS_SERVICE_TEST_POST");
            
            apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }

        #region Test ECHO Service

        /// <summary>
        /// Test the service layer directly.
        /// Usefull to determinate if the service can be access directly.
        /// </summary>
        [TestMethod]
        public void EchoSlTestServiceLayer()
        {
            //send the login request as headers...
            string apikey = apiKey;
            string servicesUrl = serviceLayerTestEchoAddress;
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode );
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            });
            task.Wait();

        }

        #endregion

        #region Test Control of Windows Service

        /// <summary>
        /// Test the service layer directly.
        /// Usefull to determinate if the service can be access directly.
        /// </summary>
        [TestMethod]
        public void WindowsServiceTestServiceLayerGet()
        {
            //send the login request as headers...
            string apikey = apiKey;
            string servicesUrl = serviceLayerWapiWindowsService + "?ServiceName=WapiWindowsService&Command=";
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };

            //request.Headers.Add("ServiceName", "WapiWindowsService");
            //request.Headers.Add("Command", "");

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            });
            task.Wait();
        }

        /// <summary>
        /// Test if the init command is received.
        /// If the windows service is not installed, also the test return 
        /// OK, no matter the response is 400 bad request.
        /// Test the service layer directly.
        /// Usefull to determinate if the service can be access directly.
        /// </summary>
        [TestMethod]
        public void WindowsServiceTestServiceLayerPost()
        {
            //send the login request as headers...
            string apikey = apiKey;
            string servicesUrl = serviceLayerWapiWindowsServicePost;
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", apikey);
            var postData = new Dictionary<string, string>();
            postData.Add("ServiceName", "WapiWindowsService");
            postData.Add("Command", "init");

            var task = client.PostAsJsonAsync(servicesUrl, postData).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                var strResponse = response.ToString();
                Console.WriteLine(strResponse);
                var strContentResponse = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(strContentResponse);
                if (strContentResponse.Contains("Service Not Installed"))
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
                    return;
                }
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// Test if the start command is received.
        /// If the windows service is not installed, also the test return 
        /// OK, no matter the response is 400 bad request.
        /// Test the service layer directly.
        /// Usefull to determinate if the service can be access directly.
        /// </summary>
        [TestMethod]
        public void WindowsServiceTestServiceLayerCommandStart()
        {
            //send the login request as headers...
            string apikey = apiKey;
            string servicesUrl = serviceLayerWapiWindowsServicePost;
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", apikey);
            var postData = new Dictionary<string, string> { { "ServiceName", "WapiWindowsService" }, { "Command", "start" } };

            var task = client.PostAsJsonAsync(servicesUrl, postData).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                var strResponse = response.ToString();
                Console.WriteLine(strResponse);
                var strContentResponse = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(strContentResponse);
                if (strContentResponse.Contains("Service Not Installed"))
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
                    return;
                }
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// Test if the start command is received.
        /// If the windows service is not installed, also the test return 
        /// OK, no matter the response is 400 bad request.
        /// Test the service layer directly.
        /// Usefull to determinate if the service can be access directly.
        /// </summary>
        [TestMethod]
        public void WindowsServiceTestServiceLayerCommandStop()
        {
            //send the login request as headers...
            string apikey = apiKey;
            string servicesUrl = serviceLayerWapiWindowsServicePost;
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", apikey);
            var postData = new Dictionary<string, string> { { "ServiceName", "WapiWindowsService" }, { "Command", "stop" } };


            var task = client.PostAsJsonAsync(servicesUrl, postData).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                var strResponse = response.ToString();
                Console.WriteLine(strResponse);
                var strContentResponse = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(strContentResponse);
                if (strContentResponse.Contains("Service Not Installed"))
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
                    return;
                }
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        #endregion

      }
}
