﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestWapiKlassAppUiController.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The Mobile tester.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServicesTest.ServiceLayer
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Net.Http;
    
    //// using System.Web.Helpers;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///  The Mobile tester.
    /// </summary>
    [TestClass]
    public class TestWapiKlassAppUiController
    {
        /// <summary>
        /// This constants is different to the constant used by WAPI
        /// and it is only used to test purposes.
        /// </summary>
        private readonly string apiKey;

        /// <summary>
        /// The service layer lead info bar get.
        /// </summary>
        private readonly string serviceLayerMobileGet;
       
        /// <summary>
        /// Initializes a new instance of the <see cref="TestWapiKlassAppUiController"/> class. 
         /// </summary>
        public TestWapiKlassAppUiController()
        {
            this.serviceLayerMobileGet = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_GET_MOBILE_CONFIGURATION");
            this.apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }

        /// <summary>
        /// The get mobile configuration.
        /// </summary>
        [TestMethod]
        public void GetKlassAppConfig()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = $"{this.serviceLayerMobileGet}?Command=1";

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// The initialize configuration table.
        /// This test truncate the Configuration kLASSAPP table and
        /// restore the values in insert state.
        /// </summary>
        [TestMethod]
        public void InitializeConfigurationTable()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = $"{this.serviceLayerMobileGet}?Command=3&UserId=864335EE-C9C6-47C9-BBCB-DEEE766F27A1";

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// The get if configuration is OK
        /// </summary>
        [TestMethod]
        public void GetIfConfigurationIsOk()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = $"{this.serviceLayerMobileGet}?Command=4&UserId=864335EE-C9C6-47C9-BBCB-DEEE766F27A1";

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }
    }
}
