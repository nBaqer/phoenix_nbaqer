﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestWapiVendorsLeadController.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the TestWapiVendorsLeadController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServicesTest.ServiceLayer
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Net.Http;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// The test WAPI vendors lead controller.
    /// </summary>
    [TestClass]
    public class TestWapiVendorsLeadController
    {
        /// <summary>
        /// This constants is different to the constant used by WAPI
        /// and it is only used to test purposes.
        /// </summary>
        private readonly string apiKey;

        /// <summary>
        /// The service layer WAPI.
        /// </summary>
        private readonly string serviceLayerWapi;

        /// <summary>
        /// The service layer WAPI campaign.
        /// </summary>
        private readonly string serviceLayerWapiCampaign;

        /// <summary>
        /// The service layer WAPI pay for list.
        /// </summary>
        private readonly string serviceLayerWapiPayForList;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestWapiVendorsLeadController"/> class.
        /// </summary>
        public TestWapiVendorsLeadController()
        {
            this.serviceLayerWapi = ConfigurationManager.AppSettings.Get("XGET_WAPI_VENDORS_LIST_URL");
            this.serviceLayerWapiCampaign = ConfigurationManager.AppSettings.Get("XGET_WAPI_CAMPAIGNS_LIST_URL");
            this.serviceLayerWapiPayForList = ConfigurationManager.AppSettings.Get("XGET_WAPI_PAYFOR_LIST_URL");
            this.apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }

        #region Vendors

        /// <summary>
        /// The get list of all vendors.
        /// </summary>
        [TestMethod]
        public void GetListOfAllVendors()
        {
            // send the login request as headers...
            string apikey = this.apiKey;
            string servicesUrl = this.serviceLayerWapi;
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// The get list of one vendor.
        /// </summary>
        [TestMethod]
        public void GetListOfOneVendor()
        {
            // send the login request as headers...
            string apikey = this.apiKey;
            string servicesUrl = this.serviceLayerWapi + "/?VendorId=1";
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        #endregion

        #region Campaign

        /// <summary>
        /// The get list of all lead campaign.
        /// </summary>
        [TestMethod]
        public void GetListOfAllLeadCampaign()
        {
            // send the login request as headers...
            string apikey = this.apiKey;
            string servicesUrl = this.serviceLayerWapiCampaign;
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        #endregion

        /// <summary>
        /// The get list of all pat for method.
        /// </summary>
        [TestMethod]
        public void GetListOfAllPatForMethod()
        {
            // send the login request as headers...
            string apikey = this.apiKey;
            string servicesUrl = this.serviceLayerWapiPayForList;
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }
    }
}
