﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestProxyKlassAppOperation.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The test proxy KLASSAPP operation.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServicesTest.Mobile
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Net.Http;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// The test proxy <code>klass-app</code> operation.
    /// </summary>
    [TestClass]
    public class TestProxyKlassAppOperation
    {
        /// <summary>
        /// The WAPI address.
        /// </summary>
        private readonly string wapiAddress;

        /// <summary>
        /// The authorized user.
        /// </summary>
        private readonly string authorizedUser;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestProxyKlassAppOperation"/> class. 
        /// </summary>
        public TestProxyKlassAppOperation()
        {
            this.wapiAddress = ConfigurationManager.AppSettings.Get("WAPI_ADDRESS");
            this.authorizedUser = "FAME_DEFAULT_HMC_3.9_636335777011372451";
        }

        /// <summary>
        /// Test the WAPI access. Test communication
        /// This test only test the communication with the proxy. no login information is send
        /// </summary>
        [TestMethod]
        public void GetStudentsTest()
        {
            ////var servicesUrl = this.wapiAddress;
            const string Operation = "KLASS_GET_STUDENT_INFORMATION";

            // Get Public Key
            string publickey = this.GetPublicKey().Result;

            // Request for the ACCESS_KEY 
            var accessKey = this.GetAccessKey(this.authorizedUser, publickey);

            // Send the operation ECHOSL
            string result = this.SendOperation(Operation, accessKey, string.Empty);
            var payloadlength = result.Length / 1000;
            if (payloadlength < 1)
            {
                Console.WriteLine($"Payload length: {result.Length} bytes");
            }
            else
            {
                 Console.WriteLine($"Payload length: {payloadlength} kilobytes");
            }
           
            ////Console.WriteLine(result);
            Assert.AreNotEqual(null, result);
            Assert.AreNotEqual(string.Empty, result);
        }

        /// <summary>
        /// Test the WAPI access. Test communication
        /// This test only test the communication with the proxy. no login information is send
        /// </summary>
        [TestMethod]
        public void GetConfigurationStatusCommand4Test()
        {
            const string Operation = "KLASS_GET_CONFIGURATION_STATUS";

            // Get Public Key
            var publickey = this.GetPublicKey().Result;

            // Request for the ACCESS_KEY 
            var accessKey = this.GetAccessKey(this.authorizedUser, publickey);

            // Send the operation ECHOSL
            string result = this.SendOperation(Operation, accessKey, "?COMMAND=4");
            var len = result.Length / 1000;
            Console.WriteLine("------------------ Payload length: {0} KB", len);
            ////Console.WriteLine(result);
            Assert.AreNotEqual(null, result);
            Assert.AreNotEqual(string.Empty, result);
        }

        #region Private method

        /// <summary>
        /// Return the public key from server
        /// </summary>
        /// <returns>The public key from server</returns>
        public async Task<string> GetPublicKey()
        {
            var servicesUrl = this.wapiAddress;
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get
            };

            request.Headers.Add("SERVICE_CODE", "SEND_PUBLIC_KEY");
            var client = new HttpClient();

            HttpResponseMessage ret = await client.SendAsync(request);
            string publickey = ret.Content.ReadAsStringAsync().Result;
            Console.WriteLine(publickey);
            Assert.AreEqual(HttpStatusCode.OK, ret.StatusCode);

            ////var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            ////{
            ////    var response = taskwithmsg.Result;
            ////    Console.WriteLine(response.ToString());
            ////    publickey = response.Content.ReadAsStringAsync().Result;
            ////    Console.WriteLine(publickey);
            ////    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            ////});
            ////task.Wait();

            return publickey;
        }

        /// <summary>
        /// Send the Secret key coded and return the access key
        /// from server.
        /// </summary>
        /// <param name="companySecret">The company secret key</param>
        /// <param name="keyPublic">the public key supplied by proxy</param>
        /// <returns>The access token to communicate</returns>
        private string GetAccessKey(string companySecret, string keyPublic)
        {
            // Encrypt the secret key
            // Exists then Encrypt
            var rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(keyPublic);
            var dataToEncrypt = Encoding.UTF8.GetBytes(companySecret);
            var encrypted = rsa.Encrypt(dataToEncrypt, true);
            var readyToSend = Convert.ToBase64String(encrypted);

            var servicesUrl = this.wapiAddress;
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get
            };

            request.Headers.Add("COMPANY_SECRET", readyToSend);
            var client = new HttpClient();
            var accesskey = string.Empty;
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                accesskey = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(accesskey);
            });
            task.Wait();

            return accesskey;
        }

        /// <summary>
        /// Send a operation to server
        /// </summary>
        /// <param name="operation">Operation Name</param>
        /// <param name="accessKey">Access Key</param>
        /// <returns>Send result to operation to server</returns>
        private string SendOperation(string operation, string accessKey)
        {
            var servicesUrl = this.wapiAddress;
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get
            };

            request.Headers.Add("SERVICE_CODE", operation);
            request.Headers.Add("ACCESS_KEY", accessKey);
            var client = new HttpClient();
            var operationResult = string.Empty;
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                operationResult = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(operationResult);
            });
            task.Wait();

            return operationResult;
        }

        /// <summary>
        /// The send operation.
        /// </summary>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <param name="accessKey">
        /// The access key.
        /// </param>
        /// <param name="queryString">
        /// The query string.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string SendOperation(string operation, string accessKey, string queryString)
        {
            var servicesUrl = this.wapiAddress + "?" + queryString;
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get
            };

            request.Headers.Add("SERVICE_CODE", operation);
            request.Headers.Add("ACCESS_KEY", accessKey);
            var client = new HttpClient();
            client.Timeout = TimeSpan.FromMinutes(15);
            var operationResult = string.Empty;
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                if (taskwithmsg.Status == TaskStatus.Canceled)
                {
                    operationResult = "Operation was canceled. Probably by Timeout";
                }
                else
                {
                    var response = taskwithmsg.Result;
                    Console.WriteLine(response.ToString());
                    operationResult = response.Content.ReadAsStringAsync().Result;
                    Console.WriteLine(operationResult);
                }
            });
            task.Wait();

            return operationResult;
        }

        /// <summary>
        /// The post operation.
        /// </summary>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <param name="accessKey">
        /// The access key.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string PostOperation(string operation, string accessKey, string data)
        {
            var servicesUrl = this.wapiAddress;
            var operationResult = string.Empty;
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("SERVICE_CODE", operation);
            client.DefaultRequestHeaders.Add("ACCESS_KEY", accessKey);
            client.DefaultRequestHeaders.Add("ContentType", "application/json");

            var task = client.PostAsync(new Uri(servicesUrl), new StringContent(data, Encoding.Default, "application/json")).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                operationResult = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(operationResult);
            });
            task.Wait();
            return operationResult;
        }

        #endregion

    }
}
