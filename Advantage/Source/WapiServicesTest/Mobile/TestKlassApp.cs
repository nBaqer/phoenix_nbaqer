﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestKlassApp.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the UnitTest1 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiServicesTest.Mobile
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// The unit test 1.
    /// </summary>
    [TestClass]
    public class TestKlassApp
    {
        /// <summary>
        /// The API School Name.
        /// </summary>
        private readonly string apikey;

        /// <summary>
        /// The API secret.
        /// </summary>
        private readonly string apisecret;

        /// <summary>
        /// The API path.
        /// </summary>
        private readonly string apipath;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestKlassApp"/> class. 
        /// </summary>
        public TestKlassApp()
        {
            this.apikey = ConfigurationManager.AppSettings.Get("KLASS_API_KEY");
            this.apisecret = ConfigurationManager.AppSettings.Get("KLASS_API_SECRET");
            this.apipath = ConfigurationManager.AppSettings.Get("KLASS_API_PATH");
        }

        /// <summary>
        /// The get student by college id.
        /// </summary>
        [TestMethod]
        public void GetStudentByCollegeId()
        {
            // Sending information to KlassApp ...........................................
            // Create the client
            var client = new HttpClient();

            // Create the headers
            client.DefaultRequestHeaders.Add("auth_id", this.apikey);
            client.DefaultRequestHeaders.Add("auth_token", this.apisecret);

            // Compose the URI
            var uri = new Uri(this.apipath + "users/1.0/search?search={\"college_id\":\"" + "XMAN1" + "\"}");

            // Post to Service............
            var response = this.GetFromKlassAppAsync(client, uri).Result;
            Console.WriteLine("Code: {0} - Reason: {1}", response.StatusCode, response.ReasonPhrase);
            var result = response.Content.ReadAsStringAsync().Result;
            Console.WriteLine(result);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
        }

        /// <summary>
        /// The get from KLASSAPP asynchrony.
        /// </summary>
        /// <param name="client">
        /// The client.
        /// </param>
        /// <param name="uri">
        /// The uri.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<HttpResponseMessage> GetFromKlassAppAsync(HttpClient client, Uri uri)
        {
            var task = client.GetAsync(uri);
            var msg = await task;
            return msg;
        }
    }
}
