﻿module MasterPage {

    var visibleTextCharacters: number;

    /* Notification Center Support */
    export function CREATE_NOTIFICATION_CENTER(tag: string, appendTo: string, visibleCharacters: number): kendo.ui.Notification {
        visibleTextCharacters = visibleCharacters;
        $(document.body).on("click", "a.viewMore", function (e) {
            if (this.className === "viewMore") {
                e.preventDefault();
                let element = $("#" + this.id);
                let siblin = element.siblings(".moreText");
                siblin.toggle(); // hidden more text link
                element.toggle(); // show less text link
                let paren = $("#" + this.id).closest(".k-notification");
                paren.css("height", "");
                let notwrapper = $("#" + this.id).closest(".k-notification-wrap");
                notwrapper.css("white-space", "normal");
            }
            return false;
        });

        $(document.body)
            .on("click", "a.viewLess", function () {
                if (this.className === "viewLess") {
                    let element = $("#" + this.id);
                    let siblin = element.parent(".moreText");
                    let wrapper = element.closest(".k-notification-wrap");
                    let more = wrapper.children(".viewMore");
                    siblin.toggle(); // show view more text link
                    more.toggle(); // hidden 
                }
            });

        var notificationWidget = $(`#${tag}`)
            .kendoNotification({
                stacking: "down",
                appendTo: `#${appendTo}`,
                height: 40,
                hideOnClick: false,
                autoHideAfter: 0,
                button: true
                //,
                //templates: [{
                //    type: "info",
                //    template: $("#infoTemplate").html()
                //}] 

            })
            .data("kendoNotification");

        return notificationWidget as kendo.ui.Notification;
    }

    export function SHOW_NOTIFICATION_INFO(notificator: kendo.ui.Notification, message: any) {
        showNotification(notificator, message, "info");
    }

    export function SHOW_NOTIFICATION_SUCCESS(notificator: kendo.ui.Notification, message: any) {
        showNotification(notificator, message, "success");
    }

    export function SHOW_NOTIFICATION_WARNING(notificator: kendo.ui.Notification, message: any) {
        showNotification(notificator, message, "warning");
    }

    export function SHOW_NOTIFICATION_ERROR(notificator: kendo.ui.Notification, message: any) {
        showNotification(notificator, message, "error");
    }

    function showNotification(notificator: kendo.ui.Notification, message: any, typeNotification: string) {
        let messageToShow: any;
        if (message.length > visibleTextCharacters) {
            let tempShowIt: any = message.substring(0, visibleTextCharacters);
            let viewMoreId = "id" + createUUID();
            let viewLessId = "id1" + createUUID();

            let tempNoShowIt = message.substring(visibleTextCharacters);
            tempNoShowIt = `<a id='${viewMoreId}' href='#' class='viewMore' title='View more success messages'> view more...</a><span id='viewMoreMore' class='moreText' style='display:none'>${tempNoShowIt} <a id='${viewLessId}' href='#' class='viewLess' title='View less text'>view less...</a> </span>`;
            messageToShow = tempShowIt + tempNoShowIt;
        } else {
            messageToShow = message;
        }

        notificator.show(messageToShow, typeNotification);
    }

    function createUUID() {
        // http://www.ietf.org/rfc/rfc4122.txt
        var s = [];
        var hexDigits = "0123456789abcdef";
        for (var i = 0; i < 36; i++) {
            s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
        }
        s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
        s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
        s[8] = s[13] = s[18] = s[23] = "-";

        var uuid = s.join("");
        return uuid;
    }
}