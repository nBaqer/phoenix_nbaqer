﻿module MasterPage {
    export var XLEAD_NEW = "NewLead";

    // Menu access
    export var XGET_SERVICE_LAYER_MENU_GET_IF_PAGE_ENABLED_FOR_USER = "../proxy/api/SystemStuff/Menu/GetIsPageEnabledForUser";
    export var XREDIRECT_TO_DASH = "../dash.aspx?resid=264&desc=dashboard&userid=";

    // Jobs Address
    export var XGET_SERVICE_LAYER_QUEUE_COMMAND_GET = "/proxy/api/Lead/LeadQueue/Get";

    // Image Photo Access
    export var XPOST_SERVICE_LAYER_ENTITY_IMAGE = "../proxy/api/Lead/Image/Post";
    export var XGET_SERVICE_LAYER_ENTITY_IMAGE = "../proxy/api/Lead/Image/Get";
    export var XPOST_SERVICE_LAYER_IMAGE_CAMERA = "../proxy/api/Lead/Image/PostImg";

} 