﻿module MasterPage {
    export function showDialogBox(helpText, selectedHelp) {
        let dialog = $('#dialog'),
            helpIconId = selectedHelp;

        helpIconId.click(event => {
            $('#dialog').css('display', 'block');
            dialog.data("kendoDialog").open();
            dialog.data("kendoDialog").content(helpText);
            event.preventDefault();
            event.stopPropagation();
        });

        dialog.kendoDialog({
            width: "400px",
            title: "Help",
            closable: true,
            modal: true
        });
    }

    export function showDialogPopup(message) {
        let dialog = $('#studentAwardsDialog');

        dialog.kendoDialog({
            width: "435px",
            title: "Student Awards ",
            closable: true,
            modal: true,
            actions: [{ text: 'OK', primary: true }],
            close: onClose
        });

        $('#studentAwardsDialog').css('display', 'block');
        dialog.data("kendoDialog").open();
        dialog.data("kendoDialog").content(message);
        $($("#studentAwardsDialog").next().find("button")[0]).css("width", "75px");
        $($("#studentAwardsDialog").next().find("button")[0]).css("border-radius", "5px");
        $($("#studentAwardsDialog").next().find("button")[0]).css("margin", "10px 175px 20px");
    }

    var dialog = $('#studentAwardsDialog');
    function onClose() {
        dialog.fadeOut();
    }
}

