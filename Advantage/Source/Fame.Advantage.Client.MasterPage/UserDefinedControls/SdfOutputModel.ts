﻿namespace MasterPage {
    /*
    * The SDF (School Defined Fields ) Output Model
    */

    export class SdfOutputModel implements ISdfOutputModel {
        // ReSharper disable InconsistentNaming 
        /*
         * Gets or sets custom field Id for the specific School Define Fields
         *  This field exists in the view. Store it in order to be able
         *  to post the change value to database.
         */
        SdfId: string;

        /*
         * Gets or sets The control type
         */
        ControlType: string;

        /*
         * Gets or sets The Description of the control (label)
         */
        Description: string;

        /*
         * Gets or sets The value assigned
         */
        AssignedValue: string;

        /*
         * Gets or sets If the control is required or not
         */
        Required: string;

        /*
         * Gets or sets List of values (case of combo box or other list controls)
         */
        ListOfValues: string[];

        /*
         * Gets or sets The Action of the control can be two
         *  NEW
         *  LEAD
         */
        Actions: string;

        /*
         * Gets or sets Size of the controls in characters
         */
        Size: number;

        /*
         * Gets or sets The number of decimal if the field has a number.
         */
        Decimals: number;

        /*
         * Gets or sets the data type id.
         */
        DtypeId: number;

        /*
         * Gets or sets the range list.
         */
        Range: SdfRangeOutputModel;

        /*
         * Gets or sets the position.
         */
        Position: number;
        // ReSharper restore InconsistentNaming
    }
}