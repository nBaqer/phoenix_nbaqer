﻿/// <reference path="../../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
/// <reference path="isdfoutputmodel.ts" />

namespace MasterPage {

    export class AdvantageUserDefinedFields {

        /**
         * 
         * @param sdfList[]: The list of SDF fields to insert 
         * @param insertionPoint The Id of div tag where the html of the SDF page should be inserted
         * return a informational number
         * 1 Operation Normal, SDF where positioned
         * 2 No SDF where found in the page
         * 3 The SDF are not positioned. Go to configuration page ans configure them.
         */
        public static processSdfFields(sdfList: ISdfOutputModel[], insertionPoint: string): number {
            // Test if there are something to show.
            if (sdfList.length === undefined || sdfList.length === 0) {
                return 2;
            }
           
            // Inject the necessary CSS
            const checkboxrule = "input[type='checkbox']{margin-top:6px;}";
            const udfFieldsCss = ".udfFields{display:table-cell;min-width:315px;height:35px;margin:5px;padding: 5px; vertical-align:top;}";
            const udflabelCss = ".udfLabelControl {width: 145px;float: left;position: relative;margin: 5px 5px 6px 0;}";
            const udfTextBoxCss = ".udfTextBoxCss {width: 150px !important;" +
               "position: relative;float: left;padding-left: 2px;padding-top: 0 !important;margin: 5px 5px 5px 0;}";
            const udfDatepickerCss = "#custom .k-datepicker {width:150px !important;}";
            const udfDropdownCss = "#custom .k-dropdown {width: 150px !important;}";

            AdvantageUserDefinedFields.injectStyles(checkboxrule + udfFieldsCss + udflabelCss + udfTextBoxCss + udfDatepickerCss + udfDropdownCss);

            //Create the school defined control
            var html = "<div id='udf1' class='udfFields'></div><div id='udf2' class='udfFields' ></div><div id='udf3' class='udfFields'></div><div class='clearfix'> </div>" +
                "<div id='udf4' class='udfFields'></div><div id='udf5' class='udfFields'></div><div id='udf6' class='udfFields' ></div><div class='clearfix'> </div>" +
                "<div id='udf7' class='udfFields'></div><div id='udf8' class='udfFields' ></div><div id='udf9' class='udfFields' ></div><div class='clearfix'> </div>" +
                "<div id='udf10' class='udfFields'></div><div id='udf11' class='udfFields' ></div><div id='udf12' class='udfFields' ></div><div class='clearfix'> </div>" +
                "<div id='udf13' class='udfFields'></div><div id='udf14' class='udfFields' ></div><div id='udf15' class='udfFields' ></div>";
            // Append the matrix code
            $(`#${insertionPoint}`).append(html);

            // Append the controls inside the matrix based in position
            for (let i = 0; i < sdfList.length; i++) {
                let udf = sdfList[i];
                let udfhtml = AdvantageUserDefinedFields.processField(udf, i);
                if (udf.Position === 0) {
                    this.setInEmptySlot(sdfList,udfhtml);
                } else {
                    $("#udf" + udf.Position).append(udfhtml);
                }
            }

            //Create dynamically the Kendo control by determinate the type of control
            for (var x = 0; x < sdfList.length; x++) {
                AdvantageUserDefinedFields.processField2(sdfList[x]);
            }
            return 1;
        }

        static processField(control: ISdfOutputModel, i: number): string {

            let html: string = "";
            var requiredtext = "";
            if (control.Required === "True") {
                requiredtext = "<span style=color:red>*</span>";
            }
            var idcontrol = control.SdfId;
            html += "<label class='udfLabelControl' >" +
                control.Description +
                requiredtext +
                "</label><input id='" +
                idcontrol +
                "' name='" +
                idcontrol +
                "' /> ";
            return html;
        }

        static processField2(control: ISdfOutputModel) {
            var idCtrl: string = control.SdfId; //
            let myControl = $(`#${idCtrl}`);
            $("#" + idCtrl).attr("data-dtypeid", control.DtypeId);
            if (control.ControlType === "0" || control.ControlType === "1") {
                // Type normal input
                //var textbox = $("#" + idCtrl).attr("class");
                var textbox = "k-textbox udfTextBoxCss";
                //apply the kendo controls by data type
                //character||numeric
                if (control.DtypeId === 1 || control.DtypeId === 2) {
                    myControl.attr("class", textbox);
                    myControl.attr("maxlength", control.Size);
                    if (control.DtypeId === 2) {
                        let controlId = idCtrl;
                        $(`#${controlId}`).attr("data-validation", "decimal");
                        if (control.Range != null) {
                            myControl.attr("data-validation", "rangeNumber");
                            myControl.attr("data-name", control.Description);

                            myControl.attr("data-min", control.Range.MinVal);
                            myControl.attr("data-max", control.Range.MaxVal);
                        }
                    }
                }
                //date
                if (control.DtypeId === 3) {
                    myControl.kendoDatePicker();
                    myControl.attr("data-validation", "date");
                    if (control.Range != null) {
                        myControl.attr("data-validation", "rangeDate");
                        myControl.attr("data-name", control.Description);
                        myControl.attr("data-min", control.Range.MinVal);
                        myControl.attr("data-max", control.Range.MaxVal);

                    }
                }
                //checkbox
                if (control.DtypeId === 4) {
                    myControl.attr("type", "checkBox");
                }
            }

            if (control.ControlType === "2") {
                // combo box
                myControl
                    .kendoDropDownList({
                        dataSource: control.ListOfValues,
                        optionLabel: 'Select',
                        dataBound: () => {
                            let control = $(`#${idCtrl}`).data("kendoDropDownList") as kendo.ui.DropDownList;
                            control.list.width("auto");
                            control.list.css("white-space", "nowrap");
                        }
                    });
            }

            if (control.Required === "True") {
                myControl.attr('required', 'required');
            }

        }

        static injectStyles(rule) {
            $("<div />", {
                html: "&shy;<style>" + rule + "</style>"
            }).appendTo("body");
        }

        static setInEmptySlot(sdfList:ISdfOutputModel[], udfhtml:string) {
            // Get the slot potentially empties
            for (let i = 1; i < 16; i++) {
                // See if the position i is in the list of sdfOutputModel
                for (let j = 0; j < sdfList.length; j++) {
                    if (i === sdfList[j].Position) {
                       break; 
                    }
                }
                // See if it is occupied
                let slot = $("#udf" + i).children();
                if (slot.length === 0) {
                    $("#udf" + i).append(udfhtml);
                    break;
                }
            }
        }
    }

}