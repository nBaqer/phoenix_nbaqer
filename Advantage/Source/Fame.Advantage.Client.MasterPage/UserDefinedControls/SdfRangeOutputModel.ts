﻿/// <reference path="isdfrangeoutputmodel.ts" />

namespace MasterPage {
    /*
     * Custom Field Range values
     */
    export class SdfRangeOutputModel implements ISdfRangeOutputModel {
        // ReSharper disable InconsistentNaming
        /*
         * Gets or sets the school defined id.
         */
        SdfId: string;

        /*
         * Gets or sets the min val.
         */
        MinVal: string;

        /*
         * Gets or sets the max val.
         */
        MaxVal: string;
        // ReSharper restore InconsistentNaming
    }
}
