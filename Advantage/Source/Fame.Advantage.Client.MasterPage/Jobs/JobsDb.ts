﻿module MasterPage {

    declare var XMASTER_GET_BASE_URL: string;
 
    export class JobsDb {

        /* Get the info for the number of lead in queue for a Admission rep.*/
        getQueueInfoFromServer(theUrl: string,
            assingRepId: string,
            campusId: string,
            userId: string,
            command: number,
            mcontext: any
        ) {
            return $.ajax({
                context: mcontext,
                url: XMASTER_GET_BASE_URL + theUrl +
                    "?AssingRepId=" +
                    assingRepId +
                    "&UserId=" +
                    userId +
                    "&CampusID=" +
                    campusId +
                    "&Command=" +
                    command,
                type: 'GET',
                timeout: 30000,
                dataType: 'json'
            });
        }


    }
}