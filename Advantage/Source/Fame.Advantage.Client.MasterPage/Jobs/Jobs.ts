﻿module MasterPage {

    export class Jobs {

        public leadInQueue: LeadInQueueJob;

        constructor() {
            this.leadInQueue = new LeadInQueueJob();
            let assignedId: string = $("#hdnUserId").val();

            // Execute first time the jobs
            this.leadInQueue.getQueueInfoFromServer(assignedId);
        }
    }

}