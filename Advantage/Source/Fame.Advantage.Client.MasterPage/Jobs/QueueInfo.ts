﻿module MasterPage {

    /*
     * Info relative a configure the Lead Queue page
     */
    export interface IQueueInfo {
        /*
         * if true, instruct queue page to show the Admission Rep combo-box.
         */
// ReSharper disable InconsistentNaming
        ShowAdmissionRep: boolean;

        /*
         * The time that we send this server response
         */
        DeliveryTime: Date;

        /*
         * The interval of time between refresh the query in the client
         *  In Seconds.
         */
        RefreshQueueInterval: number;

        /*
         * 
         */
        NumberOfLeadPending: number;
// ReSharper restore InconsistentNaming
    }

    /*
     * Info relative a configure the Lead Queue page
     */
    export class QueueInfo {
// ReSharper disable InconsistentNaming
        /*
         * if true, instruct queue page to show the Admission Rep combo-box.
         */
        ShowAdmissionRep: boolean;

        /*
         * The time that we send this server response
         */
        DeliveryTime: Date;

        /*
         * The interval of time between refresh the query in the client
         *  In Seconds.
         */
        RefreshQueueInterval: number;

        /*
         * 
         */
        NumberOfLeadPending: number;
// ReSharper restore InconsistentNaming
    }

}