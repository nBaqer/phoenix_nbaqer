﻿module MasterPage {

    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;

    export class LeadInQueueJob {

        public db: JobsDb;
  
        constructor() {
            this.db = new JobsDb();
        }

        /* Get the information needed by the page from server*/
        getQueueInfoFromServer(assignedId: string) {

            // Need to pass userId and campusId
            var campusId: string = XMASTER_GET_CURRENT_CAMPUS_ID;
            let command: number = 3;

            // Get the user in the page...
            var userId: string = $("#hdnUserId").val();
            let tUrl: string = XGET_SERVICE_LAYER_QUEUE_COMMAND_GET;
            //if (tUrl.toLowerCase().indexOf("site") === -1) {
            //    tUrl = tUrl.replace("../", "../site/");
            //}

            // Request the info from server
            this.db.getQueueInfoFromServer(tUrl, assignedId, campusId, userId, command, this)
                .done(msg => {
                    if (msg != undefined && msg.QueueInfoObj !== undefined) {
                        try {
                            let control = $("#menuQQuantity");
                            if (control !== undefined && control !== null) {

                                // Refresh value in Menu
                                let info: IQueueInfo = msg.QueueInfoObj;
                                $("#menuQQuantity").text(`(${info.NumberOfLeadPending})`);
                            }

                            // Refresh the interval of time of the job
                            // Set the method to refresh the queue each predetermined frequency
                            let refresh: number = msg.QueueInfoObj.RefreshQueueInterval * 1000;
                            if (refresh < 40000) {
                                // Protect again a minimal time of call.
                                refresh = 40000;
                            }

                            window.setTimeout( () =>{
                                this.refreshQueueInfo(this);
                            }, refresh);
                        } catch (e) {
                            //If errors clear the cache
                            alert(e.message);
                       }
                    }
                })
                .fail(msg => {
                    SHOW_DATA_SOURCE_ERROR(msg);
                });
        }

        refreshQueueInfo(that: any) {

            let control = $("#menuQQuantity");
            let resid = GET_QUERY_STRING_PARAMETER_BY_NAME("resid");
            if (control !== undefined && control !== null && resid !== "825") {
                let assign: string = $("#hdnUserId").val();
                that.getQueueInfoFromServer(assign);
            } else {
                window.setTimeout(this.refreshQueueInfo, 1000);
            }
        }
    }
}