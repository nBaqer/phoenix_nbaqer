/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../Fame.Advantage.Client.AD/TextMessages.ts" />

module MasterPage {
    declare var XMASTER_GET_CAMPUSES_URL: string;
    declare var XMASTER_GET_USER_ID: string;
    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;
    declare var XMASTER_GET_SWITCHING_CAMPUS: string;
    declare var XMASTER_GET_BASE_URL: string;
    // ReSharper disable InconsistentNaming
    declare var __doPostBack;
    // ReSharper restore InconsistentNaming
    export interface ICampus {
        ID: string;
        Description: string;
    }

    export class Campus implements ICampus {
        public ID: string;
        public Description: string;
    }

    export class MasterPageCampusDropDownViewModel extends kendo.Observable {

        // Filters Section......................
        //public MasterPageCampusDropDownItems: kendo.data.DataSource;
        public CampusId: string;
        public XMASTER_CAMPUS_LIST: string = "XMasterCampusList";
        public timeOutAlert: number;
        public isCampusAssigned : boolean = false;
        constructor() {
            super();
            // Read from DataAccess the info from database.
            //this.MasterPageCampusDropDownItems = this.GetMasterPageCampusItems(kendo);
            this.CampusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            this.timeOutAlert = 0;
        }

        public assingCampusListToControl() {
            $("body").css("cursor", "wait");
            var temp = sessionStorage.getItem(this.XMASTER_CAMPUS_LIST);
            if (temp == null || temp === "{}") {

                // Get value from Server.
                this.getCampusList(XMASTER_GET_USER_ID, this)
                    .done(msg => {
                        if (msg != undefined) {
                            var list = new Array<Campus>();
                            for (var i = 0; i < msg.length; i++) {
                                var campus = new Campus();
                                campus.ID = msg[i].ID;
                                campus.Description = msg[i].Description;
                                list.push(campus);
                            }

                            var ds = new kendo.data.DataSource();
                            for (var j1 = 0; j1 < list.length; j1++) {
                                ds.add(list[j1]);
                            }

                            sessionStorage.setItem(this.XMASTER_CAMPUS_LIST, JSON.stringify(list));
                            $("#MasterCampusDropDown").data("kendoDropDownList").setDataSource(ds);
                            $("body").css("cursor", "default");
                            this.isCampusAssigned = true;
                        }

                    })
                    .fail((msg => {
                        $("body").css("cursor", "default");
                    }));
            } else {
                var campusList: Array<Campus> = JSON.parse(temp);// $.parseJSON(temp);
                var ds1 = new kendo.data.DataSource();

                for (var j = 0; j < campusList.length; j++) {
                    ds1.add(campusList[j]);
                }

                $("#MasterCampusDropDown").data("kendoDropDownList").setDataSource(ds1);
                $("#MasterCampusDropDown").data("kendoDropDownList").value(XMASTER_GET_CURRENT_CAMPUS_ID);
                this.isCampusAssigned = true;
                $("body").css("cursor", "default");
            }
        }

        public delayedInformation() {
            if (this.timeOutAlert > 0) {
                clearTimeout(this.timeOutAlert);
            }
            this.timeOutAlert = setTimeout(() => this.showInformationDialog(XMASTER_GET_SWITCHING_CAMPUS), 500);
        }

        public showInformationDialog(changeCampus: string) {
            this.refreshTooltip();
            var val = changeCampus.split(",");
            var status = val[0];
            var statusText = val[1];
            var text = $("#MasterCampusDropDown").data("kendoDropDownList").text();
            statusText = statusText + " " + text;
            if (status === "1") {
                $.ajax({
                    async: true,
                    url: XMASTER_GET_BASE_URL + "dash.aspx/HideNotificationSwitchingCampus",
                    type: "POST",
                    timeout: 15000,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json"
                }).done((msg) => {}).fail((err) => {});
                //alert(statusText);
                MasterPage.SHOW_INFO_WINDOW_PROMISE(statusText);
            }
        }

        /* Campus Drop down state change handler */
        public onMasterCampusDropDownChange = e => {
            this.refreshTooltip();

            // Clean the new lead flag to false
            sessionStorage.setItem(MasterPage.XLEAD_NEW, "false");
            sessionStorage.setItem("filterShowInactiveCampus", "false");
            sessionStorage.setItem("filterShowInactiveLeadStatus", "false");
            sessionStorage.setItem("filterShowInactiveAdminRep", "false");
            sessionStorage.setItem("CampusFilterOptions_" + $("#hdnUserId").val(), "");
            sessionStorage.setItem("LeadStatusFilterOptions_" + $("#hdnUserId").val(), "");
            sessionStorage.setItem("AdminRepFilterOptions_" + $("#hdnUserId").val(), "");
            sessionStorage.setItem("CampusFilterDescriptionOptions_" + $("#hdnUserId").val(), "");
            sessionStorage.setItem("LeadStatusFilterDescriptionOptions_" + $("#hdnUserId").val(), "");
            sessionStorage.setItem("AdminRepFilterDescriptionOptions_" + $("#hdnUserId").val(), "");
            sessionStorage.setItem("CampusFilterOptionsColor_" + $("#hdnUserId").val(), "");
            __doPostBack("MasterCampusDropDown", e.sender.value() + "," + $("#MasterCampusDropDown").val());
        };

        public onMasterCampusDropDownDataBound = () => {
            var dropdownlist = $("#MasterCampusDropDown").data("kendoDropDownList");
            dropdownlist.select(dataItem => dataItem.ID === this.CampusId);
            // this.refreshTooltip();
            this.delayedInformation();
        };

        refreshTooltip() {
            var dd = $("#MasterCampusDropDown").data("kendoDropDownList");
            // var tt = $(".ddmasterTooltip").data("kendoTooltip");
            if (dd.text().length > 30) {
                // tt.options.content = dd.text();
                //  tt.options.showAfter = 100;
                // Set the control to drop down list to max input
                dd.list.width("auto");

            }
            else {
                //   tt.options.content = "";
                //    tt.options.showAfter = 100000;
                dd.list.width("default");
            }
            // tt.refresh();
            //  tt.options.autoHide = true;
            //tt.options.position = "top";
        }

        getCampusList(userId: string, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: XMASTER_GET_CAMPUSES_URL + "?UserId=" + userId,
                type: "GET",
                timeout: 15000,
                dataType: "json"
            });
        }
    }
}
