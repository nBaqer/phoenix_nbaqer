﻿namespace MasterPage {

    export class WebCamControlBo {

        /**
         * Detect if Flash plug-in is installed and enabled.
         */
        isFlashInstalled(): boolean {
            // return false;
            try {
                var fo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
                if (fo) {
                    return true;
                }
            } catch (e) {
                if (navigator.mimeTypes
                    && navigator.mimeTypes['application/x-shockwave-flash'] != undefined
                    && navigator.mimeTypes['application/x-shockwave-flash'].enabledPlugin) {
                    return true;
                }
            }
            return false;
        }

        isCameraWithFlashDetectionInTheSystem() {
            return true;
        }

        /**
         * Return true if the computer has camera.
         */
        //isCameraWithHtml5DetectionInTheSystem() {
        //    var n = <any>navigator;
        //    n.getUserMedia = n.getUserMedia || n.webkitGetUserMedia || n.mozGetUserMedia || n.msGetUserMedia;
        //    return (n.getUserMedia);
        //}

        /* Post the lead information to server*/
        postCameraImage(output: ImageOutputModel, context: any) {
            return $.ajax({
                context: context,
                url: XPOST_SERVICE_LAYER_IMAGE_CAMERA,
                type: 'POST',
                dataType: "text",
                timeout: 25000,
                data: JSON.stringify(output)
            });
        }

        /**
         * Return the canvas image in Base 64
         * @param canvasName
         */
        createObjectImageToSend(canvasName: string): ImageOutputModel {
            let output = new ImageOutputModel();
            let canva = document.getElementById(canvasName) as HTMLCanvasElement;
            output.ImgLenth = 0;
            output.ImageBase64 = canva.toDataURL("image/jpeg");
            output.ExtensionType = "image/jpeg";
            return output;
        }
    }
}