﻿namespace MasterPage {

    // ReSharper disable InconsistentNaming
    /*
     * The image input model to be send to service layer
     */
    export interface IImageInputModel {
        /// <summary>
        /// Gets or sets the image id.
        /// </summary>
        ImageId: string;

        /// <summary>
        /// Gets or sets the entity id.
        /// </summary>
        EntityId: string;

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        UserId: string;

        /// <summary>
        /// Gets or sets the module id.
        /// </summary>
        ModuleId: string;

        /// <summary>
        /// Gets or sets the command.
        /// </summary>
        Command: number;
    }

    /*
     * The image input model to be send to service layer
     */
    export class ImageInputModel implements IImageInputModel {
        /// <summary>
        /// Gets or sets the image id.
        /// </summary>
        ImageId: string;

        /// <summary>
        /// Gets or sets the entity id.
        /// </summary>
        EntityId: string;

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        UserId: string;

        /// <summary>
        /// Gets or sets the module id.
        /// </summary>
        ModuleId: string;

        /// <summary>
        /// Gets or sets the command.
        /// </summary>
        Command: number;
    }
    // ReSharper restore InconsistentNaming
}

