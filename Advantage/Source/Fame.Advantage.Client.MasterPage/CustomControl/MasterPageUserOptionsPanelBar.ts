/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />

module MasterPage {

    export class MasterPageUserOptionsPanelBar {

        public MasterViewModelPb: MasterPageUserOptionsPanelBarVm;

        constructor() {
            this.MasterViewModelPb = new MasterPageUserOptionsPanelBarVm();
            kendo.bind($("#MasterPageUserOptionsPanelBar"), this.MasterViewModelPb);

            // kendo panel bar
            $("#MasterPageUserOptionsPanelBar").kendoPanelBar(
                {
                    expandMode: "single",
                    select: this.MasterViewModelPb.onSelectMenuUser,
                    //onblur: this.MasterViewModelPb.CollapsePanelBar
                });

            // Bind observable viewmodel
            kendo.init($("#MasterPageUserOptionsPanelBar"));
            $(document).click(event => {
                if ($(event.target).parents().index($("#MasterPageUserOptionsPanelBar")) === -1) {
                    var panel: kendo.ui.PanelBar = $("#MasterPageUserOptionsPanelBar").data("kendoPanelBar") as any;
                    if (panel !== undefined && panel !== null) {
                        panel.collapse($("#UserOptionBotton"), false);
                    }
                }
            });
        }
    }

    $(document).ready(() => {

        // ReSharper disable UnusedLocals
        var masterPageUserOptions = new MasterPageUserOptionsPanelBar();
        // ReSharper restore UnusedLocals

    });
} 