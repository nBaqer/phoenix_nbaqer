﻿module MasterPage {
    // ReSharper disable InconsistentNaming

    /*
     * The class to send information to server relative to image
     */
    export interface IImageOutputModel {

        /// <summary>
        /// Gets or sets Image in byte array
        /// </summary>
        ImageBase64: string;

        /// <summary>
        /// Gets or sets Image size
        /// </summary>
        ImgLenth: number;

        /// <summary>
        /// Gets or sets Type de Image <code>jpg, png, bmp</code>
        /// </summary>
        ExtensionType: string;

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        Filter: ImageInputModel;
    }

    /*
     * The DTO to output image to server
     */
    export class ImageOutputModel implements IImageOutputModel {
        /// <summary>
        /// Gets or sets Image in byte array
        /// </summary>
        ImageBase64: string;

        /// <summary>
        /// Gets or sets Image size
        /// </summary>
        ImgLenth: number;

        /// <summary>
        /// Gets or sets Type de Image <code>jpg, png, bmp</code>
        /// </summary>
        ExtensionType: string;

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        Filter: ImageInputModel;
    }

    // ReSharper restore InconsistentNaming
}

