﻿/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="imageinputmodel.ts" />
/// <reference path="webcamcontrolbo.ts" />
/// <reference path="imageoutputmodel.ts" />
/// <reference path="jquerywebcam.ts" />

namespace MasterPage {

    declare var webcam: any;
    declare var XMASTER_PAGE_USER_OPTIONS_USERID;

    export const CANVAS_WIDTH: number = 240;
    export const CANVAS_HEIGHT: number = 240;

    export class WebCamControl {

        //#region Properties
        public bo: WebCamControlBo;
        pos: number = 0;
        ctx = null;
        cam = null;
        image: HTMLImageElement;
        canvas: HTMLCanvasElement;
        camera: webcam;

        /**
         * The entity Id for the photo
         */
        public entityId: string;
        /**
         * The mod determine the type of entity (Lead Student etc)
         */
        public modCode: string;
        /**
         * Different to OK mining that something is not configured. See status text
         */
        public status: string;

        //#endregion

        constructor(entity: string, mod: string) {
            // Store parameters 
            this.entityId = entity;
            this.modCode = mod;

            // Define default status
            this.status = "OK";
            this.bo = new WebCamControlBo();

            // Create the window
            $("#webCamWindow")
                .kendoWindow({
                    title: "Photo Manager"
                });

            //#region Create the buttons....

            /**
             * Take the photo from web camera and show in preview................
             * Some action happen in the on-capture event of the camera
             */
            $("#takePhoto")
                .kendoButton({
                    click: () => {
                        webcam.capture();
                    }
                });

            /**
             * Take the photo from web camera and show in preview................
             */
            $("#takePhotoCancel")
                .kendoButton({

                    click: () => {
                        // hidden stream
                        $("#Advantage_camera").show();
                        // Show photo take
                        $("#Advantage_result").hide();
                        $("#takePhoto").data("kendoButton").enable(true);
                        $("#takePhotoCancel").data("kendoButton").enable(false);
                        $("#takePhotoUpload").data("kendoButton").enable(false);
                    }
                });

            /**
             * Upload the web cam capture picture to the server................
             */
            $("#takePhotoUpload")
                .kendoButton({
                    click: () => {
                        //Upload to server the image
                        // Create the thumbnail and output element....
                        let output: ImageOutputModel = this.bo.createObjectImageToSend("Advantage_result");
                        output.Filter = new ImageInputModel();
                        output.Filter.Command = 1;
                        ////output.Filter.EntityId = $("#shadowLead").val();
                        output.Filter.EntityId = this.entityId;

                        output.Filter.ModuleId = this.modCode;
                        output.Filter.UserId = XMASTER_PAGE_USER_OPTIONS_USERID;
                        this.bo.postCameraImage(output, this)
                            .done(msg => {
                                // hidden stream
                                $("#Advantage_camera").show();
                                // Show photo take
                                $("#Advantage_result").hide();
                                $("#takePhoto").data("kendoButton").enable(true);
                                $("#takePhotoCancel").data("kendoButton").enable(false);
                                $("#takePhotoUpload").data("kendoButton").enable(false);
                            })
                            .fail(msg => {
                                SHOW_ERROR_WINDOW("Fail in upload Image");
                                $("#Advantage_camera").show();
                                // Show photo take
                                $("#Advantage_result").hide();
                                $("#takePhoto").data("kendoButton").enable(true);
                                $("#takePhotoCancel").data("kendoButton").enable(false);
                                $("#takePhotoUpload").data("kendoButton").enable(false);
                            });
                    }
                });

            /**
             *  Close the dialog...............................................
             */
            $("#webCamDone")
                .kendoButton({
                    click: () => {
                        let win = $("#webCamWindow").data("kendoWindow");
                        win.close();
                    }
                });

            //#endregion

            var onselect = this.onUpdateSelect;
            var onupload = this.onUpload;
            var context = this;
            // Create the upload component
            $("#uploadphoto")
                .kendoUpload({
                    async: {
                        saveUrl: XPOST_SERVICE_LAYER_ENTITY_IMAGE + "?EntityId=" + this.entityId + "&UserId=" + XMASTER_PAGE_USER_OPTIONS_USERID,
                        autoUpload: false
                    },
                    multiple: false,
                    select: (ex) => {
                        onselect(ex);
                    },
                    upload: (ex) => {
                        var contextual = context;
                        onupload(ex, contextual);
                    },

                    error: (err) => {
                        MasterPage.SHOW_ERROR_WINDOW("The " + err.operation + " operation fail");
                    }
                });

            var configure = this.configureInterface;
            $("#webCamOrFile")
                .change(function () {
                    if ($(this).is(":checked")) {
                        // Disable buttons for photo
                        configure(false, false, 0, false);
                    } else {
                        configure(true, true, 0, false);
                        if (webcam !== null) {
                            $("#takePhoto").data("kendoButton").enable(true);
                            $("#help1").hide();
                        }
                    }
                });

            // Configure the interface.....................................
            try {
                let navigator = MasterPage.Common.detectNavigator();
                //alert(navigator);
                if (navigator.toLocaleLowerCase().indexOf("firefox") > -1) {
                    // If it is FireFox forgot camera
                    //alert(navigator);
                    this.configureInterface(false, false, 2, true);
                    return;
                }

                // Need to force flash
                if (this.bo.isFlashInstalled()) {

                    if (this.bo.isCameraWithFlashDetectionInTheSystem()) {
                        // Enable camera
                        this.configureInterface(true, true, 0, false);
                        this.initializeWebCam();

                    } else {
                        // Camera not detected
                        this.configureInterface(false, true, 2, true);
                        return;
                    }

                } else {
                    // Flash no detected
                    this.configureInterface(false, true, 1, true);
                    return;
                }
            } catch (e) {
                // One error occurred. Interface is configured only browse file 
                this.configureInterface(false, false, 3, true);
                return;
            }
        }

        //#region interface configuration
        configureInterface(enablecamera: boolean, forceflash: boolean, needHelp: number, disblecheckbox: boolean) {
            // Button of camera are always disable. Upload camera activate them
            $("#takePhoto").data("kendoButton").enable(false);
            $("#takePhotoCancel").data("kendoButton").enable(false);
            $("#takePhotoUpload").data("kendoButton").enable(false);
            if (enablecamera) {
                // Configure dialog to enable Camera ................
                // Active help with the cause
                $("#help1").show();

                // Configure the image visors
                $("#Advantage_camera").show();
                $("#Advantage_result").hide();
                $("#imagefromFile").hide();

                // Configure the checkbox to only browse files
                $("#webCamOrFile").prop("checked", false);
                $("#webCamOrFile").prop("disabled", false);

                // Configure the upload component
                $("#uploadphoto").data("kendoUpload").enable(false);

            } else {
                // Configure dialog to disable Camera ................
                // Active wait load flash ....
                $("#help1").hide();

                // Configure the image visors
                $("#Advantage_camera").hide();
                $("#Advantage_result").hide();
                $("#imagefromFile").show();

                // Configure the checkbox to only browse files
                $("#webCamOrFile").prop("checked", true);
                $("#webCamOrFile").prop("disabled", disblecheckbox);

                // Configure the upload component
                $("#uploadphoto").data("kendoUpload").enable(true);
            }
        }
        //#endregion

        //#region Configure Camera
        initializeWebCam() {
            var canva = document.getElementById("Advantage_result") as HTMLCanvasElement;
            var ctx1 = canva.getContext("2d");
            var img = ctx1.getImageData(0, 0, MasterPage.CANVAS_WIDTH, MasterPage.CANVAS_HEIGHT);

            this.camera = $("#Advantage_camera").webcam(({
                width: MasterPage.CANVAS_WIDTH,
                height: MasterPage.CANVAS_HEIGHT,
                mode: "callback",
                swffile: "../Scripts/WebCam/jscam.swf",
                quality: 80,

                onLoad: () => {
                    $("#help1").hide(); // cancel wait icon
                    // Detect if exists camera in the system
                    let listCamera: string[] = webcam.getCameraList();
                    if (listCamera.length > 0) {
                        // We have camera
                        $("#takePhoto").data("kendoButton").enable(true);
                        $("#takePhotoCancel").data("kendoButton").enable(false);
                        $("#takePhotoUpload").data("kendoButton").enable(false);
                        webcam.attachCamera(0);
                    } else {
                        $("#takePhoto").data("kendoButton").enable(false);
                        $("#takePhotoCancel").data("kendoButton").enable(false);
                        $("#takePhotoUpload").data("kendoButton").enable(false);
                        MasterPage.SHOW_WARNING_WINDOW("No camera is present, but you can load the image from file");
                    }
                },

                //onTick: function() { },
                onSave: (data) => {
                    var col = data.split(";");
                    let po: number = +sessionStorage.getItem("imgPos");

                    for (var i = 0; i < MasterPage.CANVAS_WIDTH; i++) {
                        var tmp = parseInt(col[i]);
                        img.data[po + 0] = (tmp >> 16) & 0xff;
                        img.data[po + 1] = (tmp >> 8) & 0xff;
                        img.data[po + 2] = tmp & 0xff;
                        img.data[po + 3] = 0xff;
                        po += 4;
                    }
                    sessionStorage.setItem("imgPos", po.toString());

                    // (4 * 240 * 240)
                    if (po >= 0x38400) {
                        ctx1.putImageData(img, 0, 0);
                    }

                    $("#takePhoto").data("kendoButton").enable(false);
                    $("#takePhotoCancel").data("kendoButton").enable(true);
                    $("#takePhotoUpload").data("kendoButton").enable(true);

                },

                onCapture: () => {
                    webcam.save();
                    // hidden stream
                    $("#Advantage_camera").hide();
                    // Show photo take
                    $("#Advantage_result").show();
                    let val = sessionStorage.getItem("imgPos");
                    console.log(val);
                    sessionStorage.setItem("imgPos", "0");
                },

                debug: (typ, error: string) => {
                    $("#takePhotoResultLabel").text(typ + ": " + error);
                }
            }));

            //webcam.pauseCamera();
        }

        connectCamera(entity: string, module: string) {
            this.entityId = entity;
            this.modCode = module;
           // webcam.setCamera(0);
            webcam.resumeCamera();
        }

        //#endregion

        //#region Upload files REgion

        onUpdateSelect(e) {
            $.each(e.files, function (index, value) {
                let ext = value.extension.toLowerCase();
                if (ext !== ".jpg" && ext !== ".png") {
                    MasterPage.SHOW_WARNING_WINDOW("Only .png or .jpg files can be uploaded");
                    e.preventDefault();
                    return false;
                }
                if (value.name.length > 50) {
                    MasterPage.SHOW_WARNING_WINDOW("The filename has more than 50 characters");
                    e.preventDefault();
                    return false;
                }

                if (value.size > 1024000) {
                    MasterPage.SHOW_WARNING_WINDOW("The filename has more than 1 MB");
                    e.preventDefault();
                    return false;
                }

                let fileraw: Blob = e.files[0].rawFile.slice();
                if (fileraw !== undefined && fileraw !== null) {
                    $("#imagefromFile").removeClass("k-icon").removeClass("k-i-image")
                        .removeClass("k-icon-256");

                    if ($("#imagefromFile img").length === 0) {
                        $("#imagefromFile").append("<img style='height:240px; width:240px;'/>");
                    }

                    $("#infobarImage img").prop("src", URL.createObjectURL(fileraw));
                } else {
                    if (!$("#infobarImage").hasClass("k-icon")) {
                        $("#infobarImage").addClass("k-icon").addClass("k-i-image").addClass("k-icon-256");
                    }
                }
                //$("#imagefromFile").prop("src", URL.createObjectURL(fileraw));
                return true;
                //console.log("Name: " + value.name);
                //console.log("Size: " + value.size + " bytes");
                //console.log("Extension: " + value.extension);

            });
        }

        onUpload(e, context:any) {
            e.preventDefault();
            var postImage = context.bo.postCameraImage;
            // Upload manually the file
            let fileraw: Blob = e.files[0].rawFile.slice();
            var reader = new FileReader();
            reader.readAsDataURL(fileraw);
            reader.onloadend = () => {
                let base64Data = reader.result;
                let output = new ImageOutputModel();
                output.ImgLenth = e.files[0].size;
                output.ImageBase64 = base64Data;
                let ext = e.files[0].extension;
                if (ext === ".jpg" || ext === ".jpeg") {
                    output.ExtensionType = "image/jpeg";
                } else {
                    output.ExtensionType = "image/png";
                }
                output.Filter = new ImageInputModel();
                output.Filter.Command = 1;
                output.Filter.EntityId = context.entityId;
                output.Filter.ModuleId = context.modCode;
                output.Filter.UserId = XMASTER_PAGE_USER_OPTIONS_USERID;
                postImage(output, this)
                    .done(msg => {
                       SHOW_INFO_WINDOW("Image Uploaded");     
                    })
                    .fail(msg => {
                        SHOW_ERROR_WINDOW("Fail in upload Image");
                    });

            }
            return true;
        }
        //#endregion
    }
}