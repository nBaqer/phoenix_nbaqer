module MasterPage
{

    // This must be in the html code of control. ...............................
    declare var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL: string;
    declare var XMASTER_PAGE_USER_OPTIONS_CURRENT_BUILD: string;
    declare var XMASTER_PAGE_USER_OPTIONS_USERID: string;
    declare var XMASTER_PAGE_USER_OPTIONS_USERNAME: string;
    declare var XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID: string;

    // ReSharper disable InconsistentNaming
    declare var __doPostBack;
    // ReSharper restore InconsistentNaming

    // View Model for user options panel Bar 
    export class MasterPageUserOptionsPanelBarVm extends kendo.Observable
    {

        //public text: string;
        public imageUrl: string;
        public value: string;
        public userId: string;
        public campusId: string;
        public items: any;

        constructor()
        {
            super();
            this.userId = XMASTER_PAGE_USER_OPTIONS_USERID;
            this.imageUrl = "k-icon k-i-user font-white";
            this.value = "Root";
            this.campusId = XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID;


            this.items = [
                {
                    text: XMASTER_PAGE_USER_OPTIONS_USERNAME,
                    imageUrl: "k-icon k-i-user",
                    value: "1User"
                },
                {
                    text: "Change Password",
                    imageUrl: "k-icon k-i-gears",
                    value: "2Pass"
                },

                {
                    text: "Log Out",
                    imageUrl: "k-icon k-i-lock",
                    value: "5Out"
                }
            ];
        }

        // event handler for select
        public onSelectMenuUser(e)
        {
            var id = e.item.id;
            switch (id)
            {
                case "1User":
                    {
                        var panel: kendo.ui.PanelBar = $("#MasterPageUserOptionsPanelBar").data("kendoPanelBar") as any;
                        if (panel != null)
                        {
                            panel.collapse($("#UserOptionBotton"), false);
                        }
                        var url = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "SY/SummaryOfRolesAndCampus.aspx?resid=383" + "&UID=" + XMASTER_PAGE_USER_OPTIONS_USERID + "&UName=" + XMASTER_PAGE_USER_OPTIONS_USERNAME;
                        window.open(url);
                        break;
                    }
                case "2Pass":
                    {
                        //Go to change password.  
                        var panel: kendo.ui.PanelBar = $("#MasterPageUserOptionsPanelBar").data("kendoPanelBar") as any;
                        if (panel != null)
                        {
                            panel.collapse($("#UserOptionBotton"), false);
                        }
                        var url1 = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "ChangePassword.aspx?resid=1100&cmpid=" + XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID;
                        $(location).attr("href", url1);
                        break;
                    }
                case "BUILD":
                    {
                        break;
                    }
                //case "4Help":
                //    {
                //        window.open(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "Help/AdvantageHelp.htm");
                //        break;
                //    }
                case "5Out":
                    {
                        var panel: kendo.ui.PanelBar = $("#MasterPageUserOptionsPanelBar").data("kendoPanelBar") as any;
                        if (panel != null)
                        {
                            panel.collapse($("#UserOptionBotton"), false);
                        }
                        sessionStorage.clear();
                        var logId = $("#hdnUserImpersonationMasterLogId").val();
                        if (logId)
                        {

                            var urlPath = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "proxy/api/SystemStuff/" + logId + "/UserImpersonationLogs";
                            $.ajax({
                                url: urlPath,
                                type: "PUT",
                                complete: function ()
                                {
                                    __doPostBack("MasterComboBoxs", id);
                                }
                            });
                        } else
                        {
                            __doPostBack("MasterComboBoxs", id);
                        }
                        break;
                    }

                default:
                    {
                        var panel: kendo.ui.PanelBar = $("#MasterPageUserOptionsPanelBar").data("kendoPanelBar") as any;
                        panel.collapse($("li", "#MasterPageUserOptionsPanelBar"), true);
                    }
            }
        }
    }
} 