/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />

module MasterPage {

    // This must be in the HTML code of control. If 
    // true the control is enabled.
    declare var XMASTER_GET_ENABLE_CAMPUS_CB: boolean;

    export class MasterPageCampusDropDown {

        public MasterCampusViewModel: MasterPageCampusDropDownViewModel;
        public SelectedEnrollment: kendo.Observable;

        constructor() {
            this.MasterCampusViewModel = new MasterPageCampusDropDownViewModel;

            try {
                // DropDownList Code ..................................
                // var masterPageCampusDropDownViewModel = this.viewModel;
                $("#MasterCampusDropDown").kendoDropDownList(
                    {
                        dataTextField: "Description",
                        dataValueField: "ID",
                        dataSource: [], // this.MasterCampusViewModel.MasterPageCampusDropDownItems,
                        enable: XMASTER_GET_ENABLE_CAMPUS_CB,
                        valuePrimitive: true,
                        value: this.MasterCampusViewModel.CampusId,
                        change: this.MasterCampusViewModel.onMasterCampusDropDownChange,
                        dataBound: this.MasterCampusViewModel.onMasterCampusDropDownDataBound,
                        autoBind: true

                    });

                this.MasterCampusViewModel.assingCampusListToControl();
            } catch (e) {
                MasterPage.SHOW_ERROR_WINDOW_PROMISE(MasterPage.Common.DEFAULT_ERROR_MESSAGE + e.stack);
            }
        }
        /**
         * Initialize the campus change dropdown with a custom confirmation message dialog box .
         * Declare a Content holder at the bottom of the page and with the ContentPlaceHolderID as BottomScriptContentPlaceHolder. Inside there declare a script tag.
         * Declare a document ready event to execute this function.
         * @param message
         */
        public initializeDropDownWithConfirmationOnChange(message: string) {
            var that = this;
            var dropdown = $("#MasterCampusDropDown").data("kendoDropDownList");
            if (dropdown !== undefined && dropdown !== null) {
                dropdown.setOptions({
                    change: (e: any) => {
                        if (that.MasterCampusViewModel.isCampusAssigned) {
                            e.preventDefault();
                            $.when(MasterPage
                                .SHOW_CONFIRMATION_WINDOW_PROMISE(message))
                                .then(confirmed => {
                                    if (confirmed) {
                                        that.MasterCampusViewModel.onMasterCampusDropDownChange(e);
                                    }
                                });
                        }
                    }
                });
            }
        }
    }
} 