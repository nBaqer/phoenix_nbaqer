﻿//interface IJQueryWebCamOptions {
//    extern?: any, // external select token to support jQuery dialogs
//    append?: boolean, //  append object instead of overwriting
//    width?: number,
//    height?: number,
//    mode?: string, // callback | save | stream
//    swffile?: string, // relative address to the swf file
//    quality?: number, // jpeg quality
//    debug?: () => void,
//    oncapture?: () => void,
//    onTick?: () => void,
//    onSave?: () => void,
//    onLoad?: () => void;
//}

interface webcam {
    capture(delay: number);
    save(file: string);
    getCameraList();
    setCamera(index: number);
    pauseCamera();
    resumeCamera();
    attachCamera(x:any);
}

//interface JQuery {
//    webcam(options?: IJQueryWebCamOptions): any;
//}
interface JQuery {
    webcam(
        extern?: any, // external select token to support jQuery dialogs
        append?: boolean, //  append object instead of overwriting
        width?: number,
        height?: number,
        mode?: string, // callback | save | stream
        swffile?: string, // relative address to the swf file
        quality?: number, // jpeg quality
        debug?: () => void,
        oncapture?: () => void,
        onTick?: () => void,
        onSave?: () => void,
        onLoad?: () => void
    ): any
}