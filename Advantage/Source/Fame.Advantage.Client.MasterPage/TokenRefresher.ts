﻿/// <reference path="../fame.advantage.api.client/api/request.ts" />
module MasterPage {
    declare var XMASTER_GET_BASE_URL;

    export class TokenRefresher {

        tokenRefreshMilliseconds = 180000;
        apiUrl;

        constructor() {
            this.apiUrl = sessionStorage.getItem("AdvantageApiUrl") || $("#hdnApiUrl").val();
        }

        public Begin() {
            var that = this;
            var tokenIsBeingRefreshed = false;
            var ranAlreadyOnPageLoad = false;

            function refreshToken() {

                if (tokenIsBeingRefreshed)
                    return;

                tokenIsBeingRefreshed = true;
                var refreshToken = setTimeout(function () {

                    var req = new Api.Request();
                    req.shouldShowIsLoading = false;
                    req.send(Api.RequestType.Post, 'v1/token/RefreshToken', {}, Api.RequestParameterType.Body, (response, jqXHR) => {
                        $.ajax({
                            async: true,
                            url: XMASTER_GET_BASE_URL + "dash.aspx/RefreshToken",
                            data: JSON.stringify({ token: jqXHR.getResponseHeader("Authorization") }),
                            type: "POST",
                            timeout: 15000,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json"
                        }).done((re) => {
                            tokenIsBeingRefreshed = false;
                            ranAlreadyOnPageLoad = true;
                        }).fail((er) => {
                            tokenIsBeingRefreshed = false;
                            ranAlreadyOnPageLoad = true;
                        });
                    });

                }, ranAlreadyOnPageLoad === false ? 1000 : that.tokenRefreshMilliseconds);
            }

            // Check for mousemove, could add other events here such as checking for key presses ect.
            $(document).bind('mousemove', function () {
                refreshToken();
            });
        }
    }
} 