﻿
module MasterPage {

    declare var XMASTER_GET_BASE_URL: string;
    declare var $find: (input: string) => any;

    /*
     * Get a query string parameter
     */
    export function GET_QUERY_STRING_PARAMETER_BY_NAME(name: string) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    export function updateQueryStringParameter(uri, key, value) {
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
            return uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
            return uri + separator + key + "=" + value;
        }
    }
    /* Test if a string is Alphabetic only (no number no symbols)
 * true if the string is Alphabetic
 * true also if the string is empty or undefined or null return true
 * false: if contain number or symbols
 */
    export function VALIDATE_ALPHABETIC_STRING(text: string) {
        if (text === null || text === undefined || text === "") {
            return true;
        }
        let regExp = new RegExp("^[a-z]+$", "gi");
        let testRes = regExp.test(text);
        if (testRes) {
            return true;
        }
        return false;
    }

    export function formatDecimalMask(num) {
        var str = num.toString(), parts = false, output = [], i = 1, formatted = null;
        if (str.indexOf(".") > 0) {
            parts = str.split(".");
            str = parts[0];
        }
        str = str.split("").reverse();
        for (var j = 0, len = str.length; j < len; j++) {
            if (str[j] !== ",") {
                output.push(str[j]);
                if (i % 3 === 0 && j < (len - 1)) {
                    output.push(",");
                }
                i++;
            }
        }
        formatted = output.reverse().join("");
        return (formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
    }

    export class Common {

        public static DEFAULT_ERROR_MESSAGE: string = "An error has ocurred during your request. ";

        public static formatPhone(phonenum: string) {
            var regexObj = /^(?:\+?1[-. ]?)?(?:\(?([0-9]{3})\)?[-. ]?)?([0-9]{3})[-. ]?([0-9]{4})$/;
            if (regexObj.test(phonenum)) {
                var parts = phonenum.match(regexObj);
                var phone = "";
                if (parts[1]) { phone += "(" + parts[1] + ") "; }
                phone += parts[2] + "-" + parts[3];
                return phone;
            }
            else {
                //invalid phone number
                return phonenum;
            }
        }

        public static b64ToBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }

        public static show(container: JQuery | string) {
            var $this: JQuery;
            if (typeof container === 'string') {
                $this = $(container);
            } else {
                $this = container;
            }
            var innerInput = $this.find('input')
            if (innerInput) {
                innerInput.prop('hidden', false);
            }
            $this.show();

        }
        public static hide(container: JQuery | string) {
            var $this: JQuery;
            if (typeof container === 'string') {
                $this = $(container);
            } else {
                $this = container;
            }
            var innerInput = $this.find('input')
            if (innerInput) {
                innerInput.prop('hidden', true);
            }
            $this.hide();

        }
        public static detectNavigator(): string {
            var ua = navigator.userAgent, tem,
                m = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if (/trident/i.test(m[1])) {
                tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                return "IE " + (tem[1] || "");
            }
            if (m[1] === "Chrome") {
                tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
                if (tem != null) return tem.slice(1).join(" ").replace("OPR", "Opera");
            }
            m = m[2] ? [m[1], m[2]] : [navigator.appName, navigator.appVersion, "-?"];
            if ((tem = ua.match(/version\/(\d+)/i)) != null) m.splice(1, 1, tem[1]);
            return m.join(" ");
        }

        public static displayLoading(target: HTMLElement, visibility: boolean) {
            var element = $(target);
            kendo.ui.progress(element, visibility);
        }
        public static enableDisableBtns(enable: boolean) {
            let toolbar = $("#LeadToolbar").data("kendoToolBar") as kendo.ui.ToolBar;
            if (!(toolbar === null || toolbar === undefined)) {
                toolbar.enable("#leadToolBarSave", enable);
                toolbar.enable("#leadPrintButton", enable);
                toolbar.enable("#leadToolBarDelete", enable);
            }
        };

        /**
         * Set the breadCrumb value
         * @param title: text to be show
         */
        public static setTitle(title: string) {
            $("#PagebreadCrumb").html(`/ ${title}`);
        }
    }


    ///*
    // * Calculate the Age based in birth data as input
    // * input: birth date
    // */
    //export function GET_AGE(dateString) {
    //    var today = new Date();
    //    var birthDate = new Date(dateString);
    //    var age = today.getFullYear() - birthDate.getFullYear();
    //    var m = today.getMonth() - birthDate.getMonth();
    //    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    //        age--;
    //    }
    //    return age;
    //}


    ///// entityId: the entity to populate the email receptor
    ///// mod: the type of entity 4: Lead 
    ///// only Lead as type of entity is programming!!!!
    //export function OPEN_TASK_RAD_EMAIL_WINDOW_WITH_ENTITY(entityId, mod) {
    //    var oWnd: any = $find('radPop');
    //    oWnd.setSize(800, 780);
    //    oWnd.setUrl(XMASTER_GET_BASE_URL + '/defaultTMAddNew.html?addnew=true&entityId=' + entityId + "&mod=" + mod.toString());
    //    oWnd.show();
    //}

    //export function OPEN_TASK_RAD_TASK_WINDOW_WITH_ENTITY(entityId, mod) {
    //    var oWnd: any = $find('radPop');
    //    oWnd.setSize(800, 780);
    //    oWnd.setUrl(XMASTER_GET_BASE_URL + '/defaultTM.html?addnew=true&entityId=' + entityId + "&mod=" + mod.toString());
    //    oWnd.show();
    //}

    //export function OPEN_TASK_RAD_SCHEDULE_WINDOW_WITH_ENTITY(entityId, mod) {
    //    var oWnd: any = $find('radPop');
    //    oWnd.setSize(800, 780);
    //    oWnd.setUrl(XMASTER_GET_BASE_URL + '/defaultTMSchedule.html?addnew=true&entityId=' + entityId + "&mod=" + mod.toString());
    //    oWnd.show();
    //}




    ///* Return date in format MM/DD/YYYY */
    //export function FORMAT_DATE_TO_MM_DD_YYYY( d:Date):string {
    //    var currDate = d.getDate().toString();
    //    if (currDate.length < 2) { currDate = "0" + currDate }
    //    var currMonth = (d.getMonth() + 1).toString();
    //    if (currMonth.length < 2) { currMonth = "0" + currMonth }
    //    var currYear = d.getFullYear();
    //    return (currMonth + "/" + currDate + "/" + currYear);
    //}

    ///*
    // * Return HH:MM as string
    // * @param d: the datetime with the time to output
    // */
    //export function FORMAT_AMPM(date:Date) {
    //    var hours = date.getHours();
    //    var minutes: string | number = date.getMinutes();
    //    var ampm = hours >= 12 ? 'pm' : 'am';
    //    hours = hours % 12;
    //    hours = hours ? hours : 12; // the hour '0' should be '12'
    //    minutes = minutes < 10 ? "0" + minutes : minutes;
    //    var strTime = hours + ":" + minutes + " " + ampm;
    //    return strTime;
    //}


}

//$.fn.bindUp = function (type, fn) {

//    type = type.split(/\s+/);

//    this.each(function () {
//        var len = type.length;
//        while (len--) {
//            $(this).bind(type[len], fn);

//            var evt = $.data(this, 'events')[type[len]];
//            evt.splice(0, 0, evt.pop());
//        }
//    });
//};




