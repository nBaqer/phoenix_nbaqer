﻿module MasterPage {

    declare var XMASTER_GET_BASE_URL;
    declare var XMASTER_GET_CURRENT_CAMPUS_ID;
    declare var storageCache;
    declare var common: any;
    declare var $find: (input: string) => any;
    export class MenuItemBo {

        public db: MenuItemDb;
        public subLevelNameFromCache: any;
        public isSubMenuOpen: boolean;
        public isResponsive: boolean;

        constructor() {
            this.db = new MenuItemDb();
            this.isSubMenuOpen = false;
            this.isResponsive = false;
        }

        //-----------------------------------------------------------
        // Initialize the Kendo SubMenu
        //-----------------------------------------------------------
        initializeKendoSubMenu(data) {
            if (data.length > 0) {
                let that = this;
                $("#subMenu").kendoMenu({
                    dataSource: data,
                    select: e => {
                        if (this.isSubMenuSameAndOpen(e.item)) {
                            this.hideNavBar();
                        } else {
                            this.setSubMenuState(e, false);
                            this.getPageGroupsAndPages($('#hdnSelectedModule').val(), this.getMenuText(e.item));
                        }
                        e.preventDefault();
                        that.isSubMenuOpen = true;
                    }
                });

            } else {
                if (this.IsDeviceTablet()) {
                    $("#subMenu").remove();
                }
            }
        };

        //-----------------------------------------------------------
        // Initialize the Kendo SubMenu
        //-----------------------------------------------------------
        initializeKendoSubMenuWithLinks(data) {
            $("#subMenu").kendoMenu({
                dataSource: data,
                select: e => {
                    if (this.isSubMenuSameAndOpen(e.item)) {
                        this.hideNavBar();
                    } else {
                        this.setSubMenuState(e, false);
                        this.getPageGroupsAndPages($('#hdnSelectedModule').val(), this.getMenuText(e.item));
                    }
                }
            });
        };

        handleStorageExpiration() {
            var lsCookie = storageCache.getFromSessionCache("AdvantageLocalStorageExpiration");
            if (!lsCookie) {
                localStorage.clear();
            }
        }

        isImpersonating() {

            let bReturn = false;
            let isImp = $("#hdnImpIsImpersonating").val();
            if (isImp) {
                isImp = isImp.toUpperCase();
                if (isImp === "TRUE") bReturn = true;
            }

            return bReturn;
        };

        baseUrl() {
            return XMASTER_GET_BASE_URL;
        };

        getMenuText(item) {
            var menuText = item.textContent;
            if (!menuText) menuText = item.innerText;
            return menuText;
        };

        //-----------------------------------------------------------
        // show the NavBar
        //-----------------------------------------------------------
        showNavBar() {
            $(".content").fadeIn();
            $(".layer1").fadeIn();;
            $(".heading").fadeIn();
            $(".headerFooter").fadeIn();
            if (this.IsDeviceTablet()) {
                $(".layer1").detach().appendTo($(".kendoMenuPane").parent());
            }
        };

        //-----------------------------------------------------------
        // hide the NavBar
        //-----------------------------------------------------------
        hideNavBar() {
            $(".content").fadeOut();
            $(".layer1").fadeOut();
            $(".headerFooter").fadeOut();
            $(".heading").fadeOut();
        };

        //-----------------------------------------------------------
        // save the module in local cache object
        //-----------------------------------------------------------
        saveModuleState(mouduleName) {
            storageCache.insertInSessionCache('currentModuleName', mouduleName);
            storageCache.removeFromSessionCache('currentSubLevelName');
        };

        //-----------------------------------------------------------
        // save the module in local cache object
        //-----------------------------------------------------------
        saveSubLevelState(subLevelName) {
            storageCache.insertInSessionCache('currentSubLevelName', subLevelName);
        };

        //-----------------------------------------------------------
        // check if the page group control can be hidden based on 
        // whether the user clicked on the same sub menu item and 
        // whether the control is already open
        //-----------------------------------------------------------
        isSubMenuSameAndOpen(thisMenuItem) {

            let bReturn = false;
            const previousSelectedSub = storageCache.getFromSessionCache("currentSubLevelName");
            const selectedSub = this.getMenuText(thisMenuItem);

            if (previousSelectedSub === selectedSub && $('#pagegroupdata').is(":visible")) bReturn = true;
            return bReturn;
        };

        //-----------------------------------------------------------
        // set the selected state for the module level menu items
        //-----------------------------------------------------------
        setModuleState(e, isFromSelection) {

            var menuItem = e;
            if (isFromSelection === false)
                menuItem = e.item;

            $('#pagegroupdata').html("");
            $("#menu").find(".k-state-selected").removeClass("k-state-selected");
            $(menuItem).addClass("k-state-selected");
            $('#hdnSelectedModule').val(this.getMenuText(menuItem));

            this.saveModuleState(this.getMenuText(menuItem));
            this.hideNavBar();
        };

        //-----------------------------------------------------------
        // set the selected state for the sub menu level menu items
        //-----------------------------------------------------------
        setSubMenuState(e, isFromSelection) {
            let that = this;
            var menuItem = e;
            if (isFromSelection === false)
                menuItem = e.item;

            $("#subMenu").find(".k-state-selected").removeClass("k-state-selected");
            $(menuItem).addClass("k-state-selected");
            this.saveSubLevelState(this.getMenuText(menuItem));
            //if (isFromSelection === true)
            //    that.setMenuResponsive(e.item);
        };

        //-----------------------------------------------------------
        // hide the RadPane to show the sub menu
        //-----------------------------------------------------------
        resizeRadPane(clientId) {

            var pane = $find(clientId) as any;
            if (pane != null) {
                pane.set_height(52);
            }
        }

        //-----------------------------------------------------------
        // retrieve the campus id from the either the campus control,
        // the campus variable, or query string
        //-----------------------------------------------------------
        getCurrentCampusId() {

            var campusId = $("#MasterCampusDropDown").val();
            if (!campusId) campusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            if (!campusId) campusId = GET_QUERY_STRING_PARAMETER_BY_NAME("cmpid"); // $.QueryString["cmpid"];

            return campusId;
        };

        //-----------------------------------------------------------
        // get SubMenu from cache or call method to get from service
        //-----------------------------------------------------------
        getSubMenu(e) {
            let that = this;
            var currentCampusId = this.getCurrentCampusId();
            if (!currentCampusId) currentCampusId = GET_QUERY_STRING_PARAMETER_BY_NAME("cmpid"); //$.QueryString["cmpid"];
            var userId = $('#hdnUserId').val();

            if (this.getMenuText(e.item) === 'Reports') {
                window.location.href = this.baseUrl() +
                    "Reports/ReportHome.aspx?resid=689&desc=reportHome&userid=" +
                    userId +
                    "&cmpid=" +
                    currentCampusId;
            } else if (this.getMenuText(e.item) === 'Maintenance') {
                window.location.href = this.baseUrl() +
                    "SY/MaintenanceHome.aspx?resid=688&desc=maintenanceHome&userid=" +
                    userId +
                    "&cmpid=" +
                    currentCampusId;
            } else {

                var cacheKey = this.getMenuText(e.item) + "_" + userId + "_" + currentCampusId + "_" + "_submenuitem";


                if (this.isImpersonating())
                    localStorage.clear();

                var cachedData = JSON.parse(storageCache.getFromSessionCache(cacheKey));

                that.setMenuResponsive(e.item);

                if (cachedData) {
                    if (this.getMenuText(e.item) === "Admissions")
                        this.initializeKendoSubMenuWithLinks(cachedData);
                    else
                        this.initializeKendoSubMenu(cachedData);

                    this.resizeRadPane($("#hdnKendoPane").val());

                } else {
                    if (this.getMenuText(e.item) === "Admissions")
                        this.getSubFromServiceWithLinks(this.getMenuText(e.item), e.item);
                    else
                        this.getSubFromService(this.getMenuText(e.item), e.item);
                }
            }

        };

        setMenuResponsive(selector) {
            if (this.IsDeviceTablet()) {
                $("#subMenu").remove();
                let subMenu = document.createElement("ul");
                subMenu.id = "subMenu";
                $(selector).append(subMenu);
                this.isResponsive = true;
            } else {
                this.isResponsive = false;
                $("#subMenu").remove();
                let subMenu = document.createElement("ul");
                subMenu.id = "subMenu";
                $("#menuRow2").append(subMenu);
            }

        }

        //-----------------------------------------------------------
        // get sub menu from service
        //-----------------------------------------------------------
        getSubFromService(subMenuName, selectedItem) {
            let thatSelectedItem = selectedItem;
            let that = this;
            if (subMenuName === 'Reports' || subMenuName === 'Maintenance') {
                $('.topPane').fadeOut();
                if (this.IsDeviceTablet()) {
                    $("#subMenu").remove();
                }
                return;
            }

            var currentCampusId = this.getCurrentCampusId();
            if (!currentCampusId) currentCampusId = GET_QUERY_STRING_PARAMETER_BY_NAME("cmpid"); // $.QueryString["cmpid"];

            var userId = $('#hdnUserId').val();

            var cacheKey = subMenuName + "_" + userId + "_" + currentCampusId + "_" + "_submenuitem";
            var uri;
            if (subMenuName === "Maintenance") {
                uri = this.baseUrl() + 'proxy/api/SystemStuff/FastReports/Menu/GetPageGroupAndPages';
            } else {
                uri = this.baseUrl() + "proxy/api/SystemStuff/{menuName}/Menu";
                uri = uri.supplant({ menuName: subMenuName });
            }
            if (currentCampusId) {
                var filter = { 'CampusId': currentCampusId };

                var requestData2 = $.getJSON(uri, filter, data => {
                    that.setMenuResponsive(thatSelectedItem);
                    this.initializeKendoSubMenu(data);
                    this.resizeRadPane($("#hdnKendoPane").val());

                    storageCache.insertInSessionCache(cacheKey, JSON.stringify(data));

                    var sub = $("#subMenu");
                    if (sub.length) {
                        for (var x = 0; x < sub[0].children.length; x++) {

                            var subName = this.getMenuText(sub[0].children[x]);
                            if (subName === this.subLevelNameFromCache)
                                this.setSubMenuState(sub[0].children[x], true);
                        }
                    }

                });
            }
        };

        //-----------------------------------------------------------
        // get sub menu from service
        //-----------------------------------------------------------
        getSubFromServiceWithLinks(subMenuName, selectedItem) {
            let thatSelectedItem = selectedItem;
            let that = this;

            if (subMenuName === 'Reports' || subMenuName === 'Maintenance') {
                if (this.IsDeviceTablet()) {
                    $("#subMenu").remove();
                }
                return;
            }

            var currentCampusId = this.getCurrentCampusId();
            if (!currentCampusId) currentCampusId = GET_QUERY_STRING_PARAMETER_BY_NAME("cmpid"); //  $.QueryString["cmpid"];

            var userId = $('#hdnUserId').val();

            var cacheKey = subMenuName + "_" + userId + "_" + currentCampusId + "_" + "_submenuitem";

            var uri: any = this.baseUrl() + "proxy/api/SystemStuff/GetSubLevelItemsWithLinks";
            uri = uri.supplant({ menuName: subMenuName });

            if (currentCampusId) {
                var filter = { 'CampusId': currentCampusId, UserId: userId, MenuWithLinks: subMenuName };

                var requestData2 = $.getJSON(uri, filter, data => {

                    // Enter a span tag to make room for the Queue assigned to the Admission Rep
                    var arrayLength = data.length;
                    for (var i = 0; i < arrayLength; i++) {
                        if (data[i].text === "Queue") {
                            data[i].text = "Queue <span id=menuQQuantity></span>";
                            data[i].encoded = false;
                        }
                    }

                    that.setMenuResponsive(thatSelectedItem);
                    this.initializeKendoSubMenuWithLinks(data);
                    this.resizeRadPane($("#hdnKendoPane").val());

                    storageCache.insertInLocalCache(cacheKey, JSON.stringify(data));

                    var sub = $("#subMenu");
                    if (sub.length) {

                        for (var x = 0; x < sub[0].children.length; x++) {

                            let subName: string = this.getMenuText(sub[0].children[x]);
                            let cachevalue: string = this.subLevelNameFromCache;
                            if (subName.trim() === "Queue") {
                                if (cachevalue !== undefined && cachevalue !== null) {
                                    if (cachevalue.indexOf("Queue") === 0) {
                                        this.setSubMenuState(sub[0].children[x], true);
                                    }
                                }
                            } else {
                                if (subName === cachevalue) {
                                    this.setSubMenuState(sub[0].children[x], true);
                                }
                            }
                        }
                    }
                });
            }
        };

        //-----------------------------------------------------------
        // Ajax call for page groups and pages
        //-----------------------------------------------------------
        getPageGroupsAndPages(selectedModule, selectedSubLevel) {

            var currentCampusId = this.getCurrentCampusId();
            if (!currentCampusId) currentCampusId = GET_QUERY_STRING_PARAMETER_BY_NAME("cmpid"); //$.QueryString["cmpid"];

            var userId = $('#hdnUserId').val();


            var cacheKey = selectedModule +
                "_" +
                selectedSubLevel +
                "_" +
                userId +
                "_" +
                currentCampusId +
                "_" +
                "_pagegroup_pages";

            if (this.isImpersonating())
                localStorage.clear();

            var cachedData = JSON.parse(storageCache.getFromSessionCache(cacheKey));

            if (cachedData) {
                this.renderLinks(cachedData);
            } else {


                var filter = {
                    'CampusId': currentCampusId,
                    'UserId': userId,
                    'IsImpersonating':
                    this.isImpersonating()
                };
                var uri: any;
                if (selectedModule === "Maintenance") {
                    uri = this.baseUrl() + 'proxy/api/SystemStuff/FastReports/Menu/GetPageGroupAndPages';
                } else {
                    uri = this.baseUrl() + "proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}";
                    uri = uri.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                }
                var requestData3 = $.getJSON(uri,
                    filter,
                    data => {
                        this.renderLinks(data);
                        storageCache.insertInSessionCache(cacheKey, JSON.stringify(data));
                    });
            }
        };

        //-----------------------------------------------------------
        // Create NavBar content (this is the panel that is open with the links list
        //-----------------------------------------------------------
        renderLinks(data) {

            var htmlData = "<table border=0 cellpadding='10' cellspacing='10' ><tr>";
            for (var i = 0; i < data.length; i++) {

                if (data[i].items.length > 0) {
                    htmlData += "<td valign='top' align='left' ><span class='linkHeader'>" +
                        data[i].text +
                        "</span><br><br>";
                    for (var x = 0; x < data[i].items.length; x++) {
                        if (data[i].items[x].IsEnabled === "True") {
                            if (data[i].items[x].url.toLowerCase().indexOf("&ispopup=1") >= 0) {
                                if (data[i].items[x].url.toLowerCase().indexOf("resid=870") >= 0) {
                                    htmlData += "<a href='#' onclick=\"openKendoWindow('" + data[i].items[x].url + "','Grad Date Calculator');\">" + data[i].items[x].text + "</a><br>";
                                }
                                else {
                                    htmlData += "<a href='#' onclick=\"openRadWindow('" +
                                        data[i].items[x].url +
                                        "');\">" +
                                        data[i].items[x].text +
                                        "</a><br>";
                                }
                            } else {
                                htmlData += "<a class='menuItemLink' href='" +
                                    data[i].items[x].url +
                                    "'>" +
                                    data[i].items[x].text +
                                    "</a><br>";
                            }

                        } else {
                            htmlData += "<span class='disabledLink'>" + data[i].items[x].text + "</span><br>";
                        }
                    }

                    htmlData += "</td>";
                }
            }

            htmlData += "</tr></table>";

            $('#pagegroupdata').html(htmlData);

            if (htmlData.indexOf("<a ") > -1) {
                this.showNavBar();
            }
        };

        //-----------------------------------------------------------
        // Redirect the page to the dashboard if the user is not enabled for this page
        //-----------------------------------------------------------
        redirectIfPageNotEnabled() {
            var pageName = common.pageFromUrl();

            if (pageName.toLowerCase() !== 'dash.aspx' && pageName.toLowerCase() !== 'userimpersonation.aspx') {
                ;
                var cmpId = this.getCurrentCampusId();
                var userId = $('#hdnUserId').val();
                this.db.redirectPageService(cmpId, userId, pageName);
            }
        };


        //-----------------------------------------------------------
        // Method returning whether current device accessing page is tablet
        //-----------------------------------------------------------
        IsDeviceTablet() {
            return ($(window).width() <= 1024 && $(window).height() <= 1024) || /*general responsive/tablet*/
                ($(window).width() === 834 && $(window).height() === 1112) ||   /*ipad pro 10.5*/
                ($(window).width() === 1112 && $(window).height() === 834) ||   /*ipad pro 10.5*/
                ($(window).width() === 1366 && $(window).height() === 1024) ||  /*ipad pro 12.9*/
                ($(window).width() === 1024 && $(window).height() === 1366);    /*ipad pro 12.9*/
        };
    }
}