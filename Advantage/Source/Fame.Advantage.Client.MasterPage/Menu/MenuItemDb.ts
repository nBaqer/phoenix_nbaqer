﻿module MasterPage {

    export class MenuItemDb {

        constructor() {

        }

        redirectPageService(campusId: string, userId: string, pageName: string) {

            var filter = { 'CampusId': campusId, 'UserId': userId, PageName: pageName };
            var uri = XGET_SERVICE_LAYER_MENU_GET_IF_PAGE_ENABLED_FOR_USER;
            var requestData5 = $.getJSON(uri,
                filter,
                data => {
                    if (data === false) {
                        window.location.href = XREDIRECT_TO_DASH + userId;
                    }
                });
        }

 

    }


}