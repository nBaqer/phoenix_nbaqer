﻿module MasterPage {

    declare var common: any;
    declare var storageCache: any;

    export class MenuItem {

        public bo: MenuItemBo;

        constructor() {
            this.bo = new MenuItemBo();
            this.bo.handleStorageExpiration();
            common.ajaxSetupBasic();
            //-----------------------------------------------------------
            // Event handlers for NavBar
            //-----------------------------------------------------------
            $(".heading,.headerFooter")
                .click(() => {
                    //event.preventDefault();
                    this.bo.hideNavBar();
                });

            $("#menu").kendoMenu({
                dataSource: [
                    { "Id": 2, "text": "Admissions" },
                    { "Id": 1, "text": "Academics" },
                    { "Id": 8, "text": "Faculty" },
                    { "Id": 6, "text": "Student Accounts" },
                    { "Id": 3, "text": "Financial Aid" },
                    { "Id": 5, "text": "Placement" },
                    { "Id": 4, "text": "Human Resources" },
                    { "Id": 10, "text": "Reports" },
                    { "Id": 13, "text": "Tools" },
                    { "Id": 11, "text": "Maintenance" }
                ]
                // The System functionality was merged with the Maintenance module. { "Id": 12, "text": "System" }]
                ,
                select: e => {

                    var that = this.bo;
                    var isSubMenuClick = $(e.item).parent('#subMenu').length > 0;

                    //responsive/tablet settings
                    if (this.bo.IsDeviceTablet() && !isSubMenuClick) {
                        this.bo.setModuleState(e, false);
                        this.bo.getSubMenu(e);
                    }

                    //desktop settings
                    if (!this.bo.IsDeviceTablet()) {
                        this.bo.setModuleState(e, false);
                        this.bo.getSubMenu(e);
                    }
                }
            });

            //-----------------------------------------------------------
            // Check for module and sub-level item in cache.  If it exits,
            // set the module and sub-level menus from cache.
            //-----------------------------------------------------------
            var moduleMenu = $("#menu");
            let selectedItem = undefined;
            var moduleName = storageCache.getFromSessionCache('currentModuleName');
            this.bo.subLevelNameFromCache = storageCache.getFromSessionCache('currentSubLevelName');
            if (moduleName != null) {
                for (var i = 0; i < moduleMenu[0].children.length; i++) {
                    var menuName = this.bo.getMenuText(moduleMenu[0].children[i]);
                    if (menuName === moduleName) {
                        this.bo.setModuleState(moduleMenu[0].children[i], true);
                        selectedItem = moduleMenu[0].children[i];
                    }
                }
                this.bo.setMenuResponsive(selectedItem);

                if (moduleName === "Admissions")
                    this.bo.getSubFromServiceWithLinks(moduleName, selectedItem);
                else
                    this.bo.getSubFromService(moduleName, selectedItem);
            }

            //-----------------------------------------------------------
            // If NavBar is visible and user clicks outside the control, 
            // hide it.
            //-----------------------------------------------------------
            $(document).click(event => {
                if (!this.bo.IsDeviceTablet()) {
                    if ($(event.target).parents().index($('#menuContent')) === -1) {
                        if ($('#menuContent').is(":visible")) {
                            this.bo.hideNavBar();
                        }
                    }
                }

            });

            // call method to hide the NavBar
            this.bo.hideNavBar();

            //-----------------------------------------------------------
            // Methods and code dealing with tablet support
            //-----------------------------------------------------------

            //toggle menu visibility on btnTabletMode click
            var tabletMenuBtn = $('#btnTabletMode');

            tabletMenuBtn.on('click', function () {
                tabletToggletMenuVisiblity();
            })

            //hide menu by default if accessing on tablet
            if (this.bo.IsDeviceTablet()) {
                tabletHideTabletMenu();
            }

            //-----------------------------------------------------------
            // If menu is visible and user is on mobile device and user clicks outside the control, 
            // hide it.
            //-----------------------------------------------------------
            $(document).click(event => {
                if (this.bo.IsDeviceTablet()) {
                    if ($(event.target).attr('id') !== tabletMenuBtn.attr('id') && $(event.target).parents().index($('#menuContent')) === -1) {
                        if (tabletIsNavBarVisible()) {
                            tabletHideTabletMenu();
                        }
                    }
                }
            });

            $(window).resize(() => {

                if (!this.bo.IsDeviceTablet()) {
                    $('#menuContent').show();
                    if ($("#responsiveRootMenu").children().length === 0) {
                        $("#subMenu").detach().appendTo("#responsiveRootMenu");
                    }
                }


                if (this.bo.IsDeviceTablet()) {
                    var moduleMenu = $("#menu");
                    let selectedItem = undefined;
                    var moduleName = storageCache.getFromSessionCache('currentModuleName');
                    this.bo.subLevelNameFromCache = storageCache.getFromSessionCache('currentSubLevelName');
                    if (moduleName != null) {
                        for (var i = 0; i < moduleMenu[0].children.length; i++) {
                            var menuName = this.bo.getMenuText(moduleMenu[0].children[i]);
                            if (menuName === moduleName) {
                                this.bo.setModuleState(moduleMenu[0].children[i], true);
                                selectedItem = moduleMenu[0].children[i];
                            }
                        }
                        this.bo.setMenuResponsive(selectedItem);

                        if (moduleName === "Admissions")
                            this.bo.getSubFromServiceWithLinks(moduleName, selectedItem);
                        else
                            this.bo.getSubFromService(moduleName, selectedItem);
                    }
                }
            });

            //local functions
            function tabletToggletMenuVisiblity() {
                $('#menuContent').toggle();
                $('#menuContent').parent().toggle();
            }

            function tabletHideTabletMenu() {
                $('#menuContent').hide();
                $('#menuContent').parent().hide();
            }

            function tabletIsNavBarVisible() {
                return $('#menuContent').is(":visible");
            }

            function isSubMenuOpen() {
                return $('#subMenu').children().length;
            }
        }
    }
}