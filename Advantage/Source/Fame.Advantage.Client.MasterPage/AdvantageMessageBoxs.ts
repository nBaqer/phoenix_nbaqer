﻿module MasterPage {
    /* Show error*/
    export function SHOW_DATA_SOURCE_ERROR(e) {
        try {
            if (e.xhr != undefined) {
                if (showSessionFinished(e.xhr.statusText)) { return ""; }
                let display: string = "";
                // Status 406 Continue (Informative Error Not Acceptable)

                if (e.xhr.statusText !== "OK" && e.xhr.status !== 406) {
                    display = "Error: " + e.xhr.statusText + ", ";
                }

                display += (e.xhr.responseText == undefined) ? e.xhr.responseXML : e.xhr.responseText;
                if (e.xhr.status === 406) {
                    SHOW_WARNING_WINDOW(display);
                } else {
                    SHOW_ERROR_WINDOW(display);
                }

            } else {
                let display: string = "";
                // Status 406 Continue (Informative Error Not Acceptable)
                if (e.statusText !== "OK" && e.status !== 406) {
                    display = "Error: " + e.statusText + ", ";
                }
                display += (e.responseText == undefined) ? e.responseXML : e.responseText;
                if (e.status === 406) {
                    SHOW_WARNING_WINDOW(display);
                } else {
                    if ((e.readyState !== 0 && e.responseText !== "" && e.status !== 0 && e.statusText !== "error")) {
                        SHOW_ERROR_WINDOW(display);
                    }
                }

            }
        } catch (ex) {
            SHOW_ERROR_WINDOW("Server returned an undefined error");
            //alert("Server returned an undefined error");
        }
        return "";
    };

    export function SHOW_LOADING(shown: boolean) {
        var pageLoading = $(document).find(".page-loading");
        if (pageLoading.length === 0)
            $(document).find("body").append('<div class="page-loading"></div>');

        kendo.ui.progress($(".page-loading"), shown);
    }

    /**
     * Create a Confirmation Dialog Message that way for user selection
     * You are able to put your code in the yes or not case.
     * Use The Promise syntax to use it.
     * @param message
     */
    export function SHOW_CONFIRMATION_WINDOW_PROMISE(message: any) {
        return showWindow('#confirmationTemplate', message);
    };

    /**
     * Create a Warning Windows that way for the confirmation
     * Use the promise syntax to use it
     * @param message
     */
    export function SHOW_WARNING_WINDOW_PROMISE(message: any) {
        return showErrorWarningInfoWindow('#warningDialogTemplate', message, 400);
    };

    /**
     * Create a Info Windows that way for the confirmation
     * Use the promise syntax to use it
     * You are able to put your code in the return of the function.
     * @param message
     */
    export function SHOW_INFO_WINDOW_PROMISE(message: any) {
        return showErrorWarningInfoWindow('#infoDialogTemplate', message, 400);
    };

    /**
    * Create a Info Windows that way for the confirmation
    * Use the promise syntax to use it
    * You are able to put your code in the return of the function.
    * @param message
    */
    export function SHOW_ERROR_WINDOW_PROMISE(message: any) {
        return showErrorWarningInfoWindow('#errorDialogTemplate', message, 400);
    };



    /**
     * Create a WARNING Windows that way for the confirmation
     * Use this if you do not need to put code after the dialog.
     * @param message
     */
    export function SHOW_WARNING_WINDOW(message: any) {
        $.when(SHOW_WARNING_WINDOW_PROMISE(message))
            .then(confirmed => {
                return true;
            });
    }

    /**
      * Create a Error Windows that way for the confirmation
      * Use this if you do not need to put code after the dialog.
      * @param message
      */
    export function SHOW_ERROR_WINDOW(message: any) {
        $.when(SHOW_ERROR_WINDOW_PROMISE(message))
            .then(confirmed => {
                return true;
            });
    }

    /**
     * Create a Info Windows that way for the confirmation
     * Use this if you do not need to put code after the dialog.
     * @param message
     */
    export function SHOW_INFO_WINDOW(message: any) {
        $.when(SHOW_INFO_WINDOW_PROMISE(message))
            .then(confirmed => {
                return true;
            });
    }

    function showWindow(template, message) {
        var dfd = $.Deferred();
        var result = false;
        var win = $("<div id='popupWindow'></div>")
            .appendTo("body")
            .kendoWindow({
                width: 400,
                resizable: false,
                title: false,
                modal: true,
                visible: false,
                scrollable: false,
                close: function (e) {
                    this.destroy();
                    dfd.resolve(result);
                }
            })
            .data("kendoWindow") as kendo.ui.Window;

        win.content($(template).html()).center().open();
        $('.popupMessage').html(message);

        $("#popupWindow .confirm_yes").val('Yes');
        $("#popupWindow .confirm_no").val('No');

        $("#popupWindow .confirm_no").click(() => {
            win.close();
        });

        $("#popupWindow .confirm_yes").click(() => {
            result = true;
            win.close();
        });

        return dfd.promise();
    };

    function showErrorWarningInfoWindow(template, message, wWidth: number) {
        var dfd = $.Deferred();
        var result = false;
        var win = $("<div id='popupWindow'></div>")
            .appendTo("body")
            .kendoWindow({
                width: wWidth,
                resizable: false,
                title: false,
                modal: true,
                visible: false,
                scrollable: false,
                close: function (e) {
                    this.destroy();
                    dfd.resolve(result);
                }
            })
            .data("kendoWindow") as kendo.ui.Window;

        win.content($(template).html()).center().open();
        if (message != null) {
            if (message.length > 200) {
                $("#wrapperMessage").css("overflow-y", "scroll");
            }
        }
        //replace new lines with breaks
        message = message.replace(/(?:\r\n|\r|\n)/g, '<br />');

        $('.popupMessage').html(message);
        $("#popupWindow .confirm_yes").val('OK');
        $("#popupWindow .confirm_yes").click(() => {
            result = true;
            win.close();
        });

        return dfd.promise();
    };

    function showSessionFinished(statusText: string): boolean {
        if (statusText === "OK") {
            alert("Session expired");
            return true;
        }
        return false;
    }

    export function showPageLoading(show) {
        var pageLoading = $(document).find(".page-loading");
        if (pageLoading.length === 0)
            $(document).find("body").append('<div class="page-loading"></div>');
        kendo.ui.progress($(".page-loading"), show);
    }

    export function resizeContainer(element: JQuery, widthPercentage?: number, heightPercentage?: number) {

        if (widthPercentage) {
            var parentWidth = element.parent().innerWidth() * (widthPercentage / 100);

            element.width(parentWidth);

        }

        if (heightPercentage) {
            var parentHeight = element.parent().innerHeight() * (heightPercentage / 100);
            element.height(parentHeight);

        }
    }
}