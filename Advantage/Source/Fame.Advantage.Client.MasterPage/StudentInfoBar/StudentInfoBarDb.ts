﻿namespace MasterPage {

    export class StudentInfoBarDb {

        /* Get the info from server */
        public getLeadPhotoFromServer(leadguid: string, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: MasterPage.XGET_SERVICE_LAYER_ENTITY_IMAGE + "?Command=1&EntityId=" + leadguid,
                type: "GET",
                timeout: 25000,
                dataType: "json"
            });
        }
    }
}