﻿namespace MasterPage {

    export class StudentInfoBarBo {

        db: StudentInfoBarDb;

        constructor() {
            this.db = new StudentInfoBarDb();
        }

        getLeadImageFromServer(leadGuid: string) {
            try {
                $("body").css("cursor", "progress");

                // Request the info from server
                this.db.getLeadPhotoFromServer(leadGuid, this)
                    .done(msg => {
                        if (msg != undefined) {
                            // Get image from encode64...
                            let imgblob = MasterPage.Common.b64ToBlob(msg.ImageBase64, msg.ExtensionType, 512);
                            if ($("#studentphoto span").length > 0) {
                                $("#studentphoto span").remove();
                                $("#studentphoto").append("<img src='" + URL.createObjectURL(imgblob) + "'  style='width: 60px; height: 60px;'>");
                            }
                            else {
                                $("#studentphoto").prop("src", URL.createObjectURL(imgblob));
                            }
                        }
                    })
                    .fail(msg => {
                        MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    });
            }
            finally {
                $("body").css("cursor", "default");
            }
        }


    }



}