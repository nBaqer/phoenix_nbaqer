﻿module MasterPage {

    declare var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL: string;
    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;

    export class StudentInfoBar
    {
        thecamera: WebCamControl;
        bo: StudentInfoBarBo;

        constructor() {
            try {
                this.bo = new StudentInfoBarBo();
                var hidden: any = document.getElementById("shadowLead1");
                var shadowLeadValue1 = hidden.textContent;
                if ($("#wonderbar") !== undefined) {
                    this.thecamera = new WebCamControl(shadowLeadValue1, "AD");
                    var closeWindow = this.windowsClose;
                    var that = this;
                    // Call to photo windows
                    $("#studentphoto")
                        .click((e) => {

                            let win = $("#webCamWindow").data("kendoWindow") as kendo.ui.Window;
                            win.open().center();
                            win.unbind("close");
                            win.bind("close",
                                () => {
                                    closeWindow(that);
                                });
                            // this.thecamera.initializeWebCam();
                            this.thecamera.connectCamera(shadowLeadValue1, "AD");
                        });
                }

                $("#infobarToStudentSummaryPage").on('click', function () {
                    window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "PL/StudentMasterSummary.aspx?resid=868&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=InfoPageQuick&mod=AD");
                })

                
            } catch (e) {
                MasterPage.SHOW_ERROR_WINDOW(e.message + "/n" + e.stack);
            } 
        }

        windowsClose(context: any) {
            ////let info = Common.detectNavigator().toLowerCase();
            ////let info-array = info.split(" ");
            let leadGuid: any = document.getElementById('shadowLead1');
            context.bo.getLeadImageFromServer(leadGuid.textContent);
        };

    }



}