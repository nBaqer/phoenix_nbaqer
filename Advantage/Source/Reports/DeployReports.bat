@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
SETLOCAL
GOTO Start

:Usage
ECHO.
ECHO DeployReport:
ECHO   Deploys a folder of RDLs to a ReportServer
ECHO Usage:
ECHO   DeployReports TargetServer TargetFolder [SourceFolder] [SourceFileSpec] 
ECHO     [LogFile] [User] [Password] [Timeout] [DeployScript] [RsExe]
ECHO Parameters:
ECHO   TargetServer   URL of target report server
ECHO                  Example: http://qafa:8080/ReportServer
ECHO   TargetFolder   Folder on target server to deploy reports
ECHO                  Example: /FaaReportServer
ECHO   SourceFolder   Optional: Folder containing RDLs to be deployed
ECHO                  Default: Sources folder under location of 
ECHO                  DeployReports.bat if it exists, otherwise defaults to 
ECHO                  location of DeployReports.bat
ECHO   SourceFileSpec Optional: File spec of source files to deploy
ECHO                  Default: *.rdl
ECHO   LogFile        Optional: Y/N
ECHO                    Y=Send output to log file
ECHO                    N=Send output to console
ECHO                  Default: N
ECHO   User           Optional: User with SSRS deployment rights
ECHO                  Default: Current user
ECHO   Password       Optional: Password for user with SSRS deployment rights
ECHO                  Default: Current password
ECHO   Timeout        Optional: Timeout for rs.exe
ECHO                  Default: 60 seconds
ECHO   DeployScript   Optional: rss deployment script to run
ECHO                  Default: DeployReports.rss
ECHO   RsExe   		  Optional: Fully qualified path of rs.exe
ECHO                  Default: Default install folder for SQL2008
GOTO End

:Start
SET TargetServer=%1
SET TargetFolder=%~2
SET SourceFolder=%~3
SET SourceFileSpec=%~4
SET LogFile=%~5
SET User=%~6
SET Password=%~7
SET Timeout=%~8
SET DeployScript=%~9

IF "%TargetServer%"=="?" GOTO Usage
IF "%TargetServer%"=="/?" GOTO Usage
IF "%TargetServer%"=="" GOTO Usage
IF "%TargetFolder%"=="" GOTO Usage
IF "%SourceFolder%"=="" IF EXIST %~dp0Sources SET SourceFolder=%~dp0Sources
IF "%SourceFolder%"=="" SET SourceFolder=%~dp0
IF "%SourceFileSpec%"=="" SET SourceFileSpec=*.rdl
IF "%LogFile%"=="" SET LogFile=N
IF "%Timeout%"=="" SET Timeout=60
IF "%DeployScript%"=="" SET DeployScript=%~dp0DeployReports.rss

SET OutputFolder=%~dp0
SET LogFileName=%OutputFolder%%~n0.log
SET ErrFileName=%OutputFolder%%~n0.error

SHIFT
SET RsExe=%~s9
IF "%RsExe%"==""       SET RsExe=%OutputFolder%rs.exe
IF NOT EXIST "%RsExe%" SET RsExe=C:\Program Files (x86)\Microsoft SQL Server\130\Tools\Binn\rs.exe
IF NOT EXIST "%RsExe%" SET RsExe=C:\Program Files\Microsoft SQL Server\130\Tools\Binn\rs.exe
IF NOT EXIST "%RsExe%" SET RsExe=C:\Program Files (x86)\Microsoft SQL Server\120\Tools\Binn\rs.exe
IF NOT EXIST "%RsExe%" SET RsExe=C:\Program Files\Microsoft SQL Server\120\Tools\Binn\rs.exe
IF NOT EXIST "%RsExe%" SET RsExe=C:\Program Files (x86)\Microsoft SQL Server\110\Tools\Binn\rs.exe
IF NOT EXIST "%RsExe%" SET RsExe=C:\Program Files\Microsoft SQL Server\110\Tools\Binn\rs.exe
IF NOT EXIST "%RsExe%" SET RsExe=C:\Program Files (x86)\Microsoft SQL Server\100\Tools\Binn\rs.exe
IF NOT EXIST "%RsExe%" SET RsExe=C:\Program Files\Microsoft SQL Server\100\Tools\Binn\rs.exe
IF NOT EXIST "%RsExe%" SET RsExe=C:\Program Files (x86)\Microsoft SQL Server\100\Shared\100\Tools\Binn\rs.exe
IF NOT EXIST "%RsExe%" SET RsExe=C:\Program Files (x86)\Microsoft SQL Server\90\Tools\Binn\rs.exe
IF NOT EXIST "%RsExe%" SET RsExe=C:\Program Files\Microsoft SQL Server\90\Tools\Binn\rs.exe
IF NOT EXIST "%RsExe%" SET RsExe=C:\Program Files (x86)\Microsoft SQL Server\100\Shared\100\Tools\Binn\rs.exe
IF NOT EXIST "%RsExe%" SET RsExe=C:\Program Files (x86)\Microsoft SQL Server\80\Tools\Binn\rs.exe
IF NOT EXIST "%RsExe%" SET RsExe=C:\Program Files\Microsoft SQL Server\80\Tools\Binn\rs.exe

SET Credentials=
IF NOT "%User%"=="" SET Credentials=-u "%User%" -p "%Password%"

SET RssCmd="%RsExe%" -i "%DeployScript%" -s %TargetServer% %Credentials% -l %Timeout% -v TargetFolder="%TargetFolder%" -v SourceFolder="%SourceFolder%" -v SourceFileSpec="%SourceFileSpec%"

IF /i NOT %Debug%==ON GOTO SkipDebug
ECHO.
ECHO TargetServer    %TargetServer%
ECHO TargetFolder    %TargetFolder%
ECHO SourceFolder    %SourceFolder%
ECHO SourceFileSpec  %SourceFileSpec%
ECHO LogFile         %LogFile%
ECHO User            %User%
ECHO Password        %Password%
ECHO Timeout         %Timeout%
ECHO DeployScript    %DeployScript%
ECHO RsExe           %RsExe%
ECHO.
ECHO RssCmd          %RssCmd%
ECHO.
PAUSE
:SkipDebug

SET ExitCode=
IF EXIST "%ErrFileName%" DEL "%ErrFileName%" /f/q

SET RedirLog=
IF /i "%LogFile%"=="Y" SET RedirLog=^>^> "%LogFileName%"

ECHO Deploying: %SourceFolder%%SourceFileSpec% %RedirLog%
ECHO To: %TargetServer%%TargetFolder% %RedirLog%
ECHO. %RedirLog%
%RssCmd% %RedirLog% 2>&1
SET ExitCode=%ERRORLEVEL%
IF NOT %ExitCode%==0 ECHO Exit Code: %ExitCode% %RedirLog%
ECHO. %RedirLog%

:End
IF NOT %ExitCode%==0 ECHO Exit Code: %ExitCode% >> "%ErrFileName%"
EXIT /b %ExitCode%
ENDLOCAL
