﻿using FAME.Advantage.Domain.Campuses;
using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Users;

namespace FAME.Advantage.Services.Lib.Tester.DomainWrap
{
    public class FakeCampusGroup : CampusGroup
    {
        public static FakeCampusGroup Factory(
                string campusGrpcode,
                string campusDescription,
                IList<FakeCampus> listCampuses = null,
                IList<FakeUser> listUsers = null
                )
        {
            var wrap = new FakeCampusGroup();
            wrap.ID = Guid.NewGuid();
            wrap.CampusGrpCode = campusGrpcode;
            wrap.CampusGrpDescription = campusDescription;
            wrap.Campuses = new List<Campus>();
            if (listCampuses != null)
            {
                foreach (Campus c in listCampuses)
                {
                    wrap.Campuses.Add(c);
                }
            }

            wrap.ListUsers = new List<User>(); 
            if (listUsers != null)
            {
                foreach(FakeUser fu in listUsers)
                {
                    wrap.ListUsers.Add(fu);
                }

            }
           
            return wrap;
        }
    }
}
