﻿using FAME.Advantage.Domain.Campuses;
using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Campuses.CampusGroups;

namespace FAME.Advantage.Services.Lib.Tester.DomainWrap
{
    public class FakeCampus : Campus
    {
        public static FakeCampus Factory(string description, FakeStatusOption status, IList<FakeCampusGroup> listcampusgroups = null)
        {
            var wrap = new FakeCampus();
            wrap.ID = Guid.NewGuid();
            wrap.Status = status;
            wrap.Description = description;
            wrap.CampusGroup = new List<CampusGroup>();
            if (listcampusgroups != null)
            {
                foreach (CampusGroup cg in listcampusgroups)
                {
                    wrap.CampusGroup.Add(cg);
                }
            }

            return wrap;
        }
    }
}
