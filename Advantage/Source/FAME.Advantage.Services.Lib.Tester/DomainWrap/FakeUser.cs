﻿using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Users;
using System;
using System.Collections.Generic;

namespace FAME.Advantage.Services.Lib.Tester.DomainWrap
{
    public class FakeUser : User
    {
        public static FakeUser Factory(string username, IList<FakeCampusGroup> campusGroup = null)
        {
            var wrap = new FakeUser();
            wrap.ID = Guid.NewGuid();
            wrap.UserName = username;
            wrap.CampusGroup = new List<CampusGroup>();
            if (campusGroup != null)
            {
                foreach (CampusGroup cg in campusGroup)
                    wrap.CampusGroup.Add(cg);
            }

            return wrap;
        }
    }
}
