﻿using FAME.Advantage.Domain.Campuses;
using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.StatusesOptions;

namespace FAME.Advantage.Services.Lib.Tester.DomainWrap
{
    /// <summary>
    /// Fake class to mimic domain Status Options (syStatuses)
    /// </summary>
    public class FakeStatusOption : StatusOption
    {
        /// <summary>
        ///Fake status options from table syStatuses.
        /// </summary>
        /// <param name="statusString">For Advantage valid values are Active - Inactive</param>
        /// <param name="fakecampuses">Optional list of campus</param>
        /// <returns>A Fake Status option object</returns>
        public static FakeStatusOption Factory(string statusString, string statuscode, List<FakeCampus> fakecampuses = null)
        {
            var wrap = new FakeStatusOption();
            wrap.ID = Guid.NewGuid();
            wrap.Status = statusString;
            wrap.StatusCode = statuscode;
            wrap.Campuses = new List<Campus>();
            if (fakecampuses != null)
            {
                foreach (Campus c in fakecampuses)
                {
                    wrap.Campuses.Add(c);
                }
            }

            return wrap;
        }
    }

}
