﻿using FluentAssertions.Execution;
using FluentAssertions.Specialized;
using System.Web.Http;

namespace FAME.Advantage.Services.Lib.Tester.Extensions
{
    internal static class HttpResponseExtension
    {
        public static void ResponseTextShouldBe<T>(this ExceptionAssertions<T> assertions, string expectedResponseText) where T : HttpResponseException
        {
            var httpRespnseException = ((HttpResponseException) assertions.Subject);
            var responseText = httpRespnseException.Response.Content.ReadAsStringAsync().Result;

            Execute.Assertion //was Execute.Verification
               .ForCondition(responseText.Equals(expectedResponseText))
               .FailWith("Expected Response message to be {0}{reason}, but found {1}", expectedResponseText, responseText);
        }
    }
}
