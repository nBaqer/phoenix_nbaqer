﻿using FAME.Advantage.Services.Lib.Infrastructure.Extensions;
using FluentAssertions;
using NUnit.Framework;
using System;

namespace FAME.Advantage.Services.Lib.Tester.Extensions
{
    [TestFixture]
    public class GuidExtensionsTester
    {
        [Test]
        public void IsEmpty_should_return_false_when_guid_has_value()
        {
            var guid = Guid.NewGuid();

            guid.IsEmpty().Should().BeFalse();
        }

        [Test]
        public void IsEmpty_should_return_true_when_guid_is_initialized_to_empty()
        {
            var emptyGuid = Guid.Empty;

            emptyGuid.IsEmpty().Should().BeTrue();
        }

        [Test]
        public void IsEmpty_should_return_true_when_guid_is_parsed_from_empty_value()
        {
            const string emptyGuidString = "00000000-0000-0000-0000-000000000000";

            var emptyGuid = Guid.Parse(emptyGuidString);

            emptyGuid.IsEmpty().Should().BeTrue();
        }
    }
}
