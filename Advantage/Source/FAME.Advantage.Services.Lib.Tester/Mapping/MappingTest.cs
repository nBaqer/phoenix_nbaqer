﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FAME.Advantage.Services.Lib.Infrastructure.Bootstrapping;
using NUnit.Framework;

namespace FAME.Advantage.Services.Lib.Tester.Mapping
{
    [TestFixture]
    public class MappingTest
    {
         [Test]
        public void TestMappings()
        {
            Bootstrapper.Bootstrap();
             Mapper.AssertConfigurationIsValid();

        }

    }
}
