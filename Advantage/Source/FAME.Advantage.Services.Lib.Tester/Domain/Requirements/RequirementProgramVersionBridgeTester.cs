﻿using System;
using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FAME.Advantage.Domain.Requirements;
using FAME.Advantage.Domain.Student.Enrollments;
using FluentAssertions;
using NUnit.Framework;
using Rhino.Mocks;

namespace FAME.Advantage.Services.Lib.Tester.Domain.Requirements
{
    [TestFixture]
    public class RequirementProgramVersionBridgeTester
    {
        //private readonly ProgramVersion _programVersion = new ProgramVersion("Program Version", "Some ProgramVersion");

        //private readonly DocumentRequirement
        //    _documentRequirement = new DocumentRequirement("Document Requirement 1", "Some document requirement 1");

        //private readonly System.Collections.Generic.List<RequirementGroupDefinition> _requirementGroupDefinitions = new System.Collections.Generic.List<RequirementGroupDefinition>
        //    {
        //        new RequirementGroupDefinition(0, true, new DocumentRequirement("Document Requirement 1", "Some document requirement 1")),
        //        new RequirementGroupDefinition(1, true, new DocumentRequirement("Document Requirement 2", "Some document requirement 2"))
        //    };

        //private readonly RequirementGroup _requirementGroup;

        //public RequirementProgramVersionBridgeTester()
        //{
        //    _requirementGroup = new RequirementGroup("Requirement Group", "Some Requirement Group", true, _requirementGroupDefinitions);
        //}

        //[Test]
        //public void should_initialize_correctly()
        //{
        //    var requirementProgramVersionBridge = new RequirementProgramVersionBridge(_programVersion, _requirementGroup, _documentRequirement);
        //    requirementProgramVersionBridge.Should().NotBeNull();
        //}

        //[Test]
        //public void should_throw_when_initializing_with_missing_dependencies()
        //{
        //    Action initialize = () => new RequirementProgramVersionBridge(_programVersion, null, null);

        //    initialize.ShouldThrow<ApplicationException>()
        //        .WithMessage("Both requirementGroup and documentRequirement cannot be null");
        //}

        //[Test]
        //public void Requirements_should_return_correctly()
        //{
        //    var requirementProgramVersionBridge = new RequirementProgramVersionBridge(_programVersion, _requirementGroup, _documentRequirement);
        //    var requirements = requirementProgramVersionBridge.Requirements;

        //    requirements.Should().HaveCount(3);
        //}

        //[Test]
        //public void Requirements_should_not_return_nulls_when_documentRequirement_is_null()
        //{
        //    var requirementProgramVersionBridge = new RequirementProgramVersionBridge(_programVersion, _requirementGroup, null);
        //    var requirements = requirementProgramVersionBridge.Requirements;

        //    requirements.Should().HaveCount(2);
        //    requirements.Should().NotContainNulls();
        //}

        //[Test]
        //public void Requirements_should_not_return_nulls_when_requirementGroup_is_null()
        //{
        //    var requirementProgramVersionBridge = new RequirementProgramVersionBridge(_programVersion, null, _documentRequirement);
        //    var requirements = requirementProgramVersionBridge.Requirements;

        //    requirements.Should().HaveCount(1);
        //    requirements.Should().NotContainNulls();
        //}
    }
}
