﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImpersonatorTester.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the ImpersonatorTester type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Tester.Bo
{
    using System;
    using System.Security.Principal;
    using System.ServiceProcess;

    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff;

    using NUnit.Framework;

    /// <summary>
    /// The impersonator tester.
    /// </summary>
    [TestFixture]
    public class ImpersonatorTester
    {
        /// <summary>
        /// The initialization.
        /// </summary>
        [OneTimeSetUp]
        public void Init()
        {
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        [OneTimeSetUp]
        public void Dispose()
        {
        }

        /// <summary>
        /// The sample test.
        /// </summary>
        [Test]
        public void SampleTest()
        {
            Console.WriteLine(System.Reflection.MethodBase.GetCurrentMethod().Name);
        }

        /// <summary>
        /// The enter context unit test.
        /// </summary>
        [Test]
        public void EnterContextUnitTest()
        {
            Console.WriteLine(System.Reflection.MethodBase.GetCurrentMethod().Name);

            WrapperImpersonationContext context = new WrapperImpersonationContext(null, "wapiuser", "s!sfame1234");
            context.Enter();
            Console.WriteLine("identity=" + WindowsIdentity.GetCurrent().Name + ".");
            
            var sc = new ServiceController("AdobeARMservice", Environment.MachineName);
            
            sc.Start();

            context.Leave();
            Console.WriteLine("identity=" + WindowsIdentity.GetCurrent().Name + ".");
        }
    }
}
