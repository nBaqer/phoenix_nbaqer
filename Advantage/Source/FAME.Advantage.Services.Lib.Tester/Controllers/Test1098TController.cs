﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Test1098TController.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   The test 1098 t controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Tester.Controllers
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Net.Http;

    using NUnit.Framework;

    /// <summary>
    /// The test 1098 t controller.
    /// </summary>
    [TestFixture]
    public class Test1098TController
    {
        /// <summary>
        /// The API key.
        /// </summary>
        private readonly string apiKey;

        /// <summary>
        /// The service layer lead task get.
        /// </summary>
        private readonly string serviceLayerLeadTaskGet;

        /// <summary>
        /// Initializes a new instance of the <see cref="Test1098TController"/> class. 
        /// </summary>
        public Test1098TController()
        {
            this.serviceLayerLeadTaskGet = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_1098T_GET");
            this.apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }

        /// <summary>
        /// Prepared for UNIT_TEST_3.8
        /// </summary>
        [Test]
        public void Send1098TToFameTester()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format(
                "{0}?CampusId={1}&UserId={2}&Year=2012",
                this.serviceLayerLeadTaskGet,
                ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE"),
                ConfigurationManager.AppSettings.Get("USER_SUPPORT"));

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query), 
                Method = HttpMethod.Get, 
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }
    }
}
