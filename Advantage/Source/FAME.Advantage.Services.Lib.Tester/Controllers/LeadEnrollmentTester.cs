﻿using System;

namespace FAME.Advantage.Services.Lib.Tester.Controllers
{
    using System.Configuration;
    using System.Net;
    using System.Net.Http;

    using NUnit.Framework;

    /// <summary>
    /// The lead enrollment tester.
    /// </summary>
    [TestFixture]
    public class LeadEnrollmentTester
    {
        /// <summary>
        /// The API key.
        /// </summary>
        private readonly string apiKey;

        /// <summary>
        /// The get drop down list source.
        /// </summary>
        private readonly string serviceLayerGetDdlSource;
        private readonly string serviceLayerGetLeadInfo;
        private readonly string serviceLayerGetStudentNum;
        private readonly string serviceLayerGetStudentEnrollNum;
        private readonly string serviceLayerGetStudentId;
        private readonly string testLead;

        public LeadEnrollmentTester()
        {
            this.apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
            this.serviceLayerGetDdlSource = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_ENROLL_DDL_SOURCE");
            this.serviceLayerGetLeadInfo = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_GET_LEAD_INFO");
            this.serviceLayerGetStudentNum = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_GET_STUDENT_SEQ");
            this.serviceLayerGetStudentId = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_GET_STUDENT_ID");
            this.serviceLayerGetStudentEnrollNum = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_GET_STUDENT_ENROLL_NUM");
        }

        /// <summary>
        /// The get caption and required list.
        /// </summary>
        [Test]
        public void GetCaptionAndRequiredList()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?PageResourceId={1}&CampusId={2}", this.serviceLayerGetDdlSource, "174", "3F5E839A-589A-4B2A-B258-35A1A8B3B819");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// The get lead data.
        /// </summary>
        [Test]
        public void GetLeadData()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?LeadId={1}&CampusId={2}", this.serviceLayerGetLeadInfo, "1AAD931C-1757-41D2-9867-0448829332D3", "3F5E839A-589A-4B2A-B258-35A1A8B3B819");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// The generate student id sequence.
        /// </summary>
        [Test]
        public void GenerateStudentIdSeq()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?stuId={1}", this.serviceLayerGetStudentNum, 1);

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// The generate student id.
        /// </summary>
        [Test]
        public void GenerateStudentId()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?firstName={1}&lastName={2}", this.serviceLayerGetStudentId, "Aaron", "Edmonds");//2091NEA069

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// The generate student enroll.
        /// </summary>
        [Test]
        public void GenerateStudentEnroll()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?firstName={1}&lastName={2}", this.serviceLayerGetStudentEnrollNum, "Aaron", "Neil");//2091NEA069

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }
    }
}
