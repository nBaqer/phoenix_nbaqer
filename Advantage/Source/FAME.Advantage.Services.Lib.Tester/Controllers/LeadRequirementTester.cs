﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Services.Lib.Tester.Controllers
{
    using System.Net;
    using System.Net.Http;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Messages.Lead;
    using FAME.Advantage.Services.Lib.Controllers.Lead;
    using FAME.Advantage.Services.Lib.Infrastructure.Bootstrapping;
    using FAME.Advantage.Services.Lib.Tester.Model;
    using FAME.Advantage.Services.Lib.Tester.Model.Routes;
    using NUnit.Framework;

    using Rhino.Mocks;

    using StructureMap;
    using StructureMap.Configuration.DSL;

    [TestFixture]
    public class LeadRequirementTester
    {
        /// <summary>
        /// The get requirements by lead test.
        /// </summary>
        [Test]
        public void GetRequirementsByLead()
        {
            Request request = new Request();
            request.Path = LeadRequirement.GetRequirementsByLeadId.Replace("{leadId}", "92D28A77-2C23-45F4-9B7E-3721E3FA0313");
            request.QueryString = "CampusId=3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            request.SendRequest(HttpMethod.Get);
            Assert.AreEqual(HttpStatusCode.OK, request.ResponseStatusCode);
        }

        /// <summary>
        /// The get requirement types met for a given lead test.
        /// </summary>
        [Test]
        public void GetRequirementTypesMet()
        {
            Request request = new Request();
            request.Path = LeadRequirement.GetRequirementTypesMet.Replace("{leadId}", "92D28A77-2C23-45F4-9B7E-3721E3FA0313");
            request.QueryString = "CampusId=3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            request.SendRequest(HttpMethod.Get);
            Assert.AreEqual(HttpStatusCode.OK, request.ResponseStatusCode);
        }

        [Test]
        public void TestCSharp6()
        {
            Request request = null;
            string queryString = request?.QueryString; // null if customers is null
        }

        [Test]
        public void TestRequirementController()
        {
            /* var fakeRepository = MockRepository.GenerateStrictMock<IRepository>();
             var fakeRepositoryInt = MockRepository.GenerateStrictMock<IRepositoryWithTypedID<int>>();

             ObjectFactory.Configure(x => x.For<IRepository>().Use(fakeRepository));
             ObjectFactory.Configure(x => x.For<IRepositoryWithTypedID<int>>().Use(fakeRepositoryInt));
             var controller = ObjectFactory.GetInstance<LeadRequirementsController>();*/


            // MvcApplication app = new MvcApplication();
            /* 
             var irepository = ObjectFactory.GetInstance<IRepository>();
             var irepositoryInt = ObjectFactory.GetInstance<IRepositoryWithTypedID<int>>();

             var uow = 

             var nhibernateRegistry = new FAME.Advantage.Services.Lib.Infrastructure.Bootstrapping.NHibernateRegistry();


             var nHibernateRep = new  FAME.Advantage.Domain.Persistence.Infrastructure.NHibernate.NHibernateRepository()

             Registry registry = new NHibernateRegistry();


             LeadRequirementsController controller = new LeadRequirementsController(irepository, irepositoryInt);
             //controller.ControllerContext = app.;

             controller.GetRequirementsByLeadId(
                 Guid.Parse("92D28A77-2C23-45F4-9B7E-3721E3FA0313"),
                 new GetLeadRequirementInputModel()
                     {
                         CampusId = Guid.Parse("3F5E839A-589A-4B2A-B258-35A1A8B3B819"),
                         IsMandatory =  true
                 });*/
        }
    }
}
