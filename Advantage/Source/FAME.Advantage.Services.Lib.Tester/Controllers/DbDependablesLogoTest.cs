﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using FAME.Advantage.Messages.SystemStuff.Maintenance;
using NUnit.Framework;

namespace FAME.Advantage.Services.Lib.Tester.Controllers
{
    [TestFixture]
    public class DbDependablesLogoTest
    {
       
        private readonly string apiKey;
        /// <summary>
        /// Constructor. Initialize here...
        /// </summary>
        public DbDependablesLogoTest()
        {
           apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }


        [Test]
        public void GetAllLogosFromDatabase()
        {
            string apikey = apiKey;
            const string SERVICES_URL = "http://localhost/Advantage/Current/Services/api/SystemStuff/Maintenance/Logo/Get"; 
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(SERVICES_URL),
                Method = HttpMethod.Post,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            });
            task.Wait();
        }

        [Test]
        public void UpdateLogoInfoTest()
        {
            string apikey = apiKey;
            const string SERVICES_URL = "http://localhost/Advantage/Current/Services/api/SystemStuff/Maintenance/Logo/UpdateLogo";
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", apikey);
          var js = new SchoolLogoOutputModel
          {
              ID = 1,
              Description = "description",
              ImageCode = "micode",
              OfficialUse = true
          };
            
            var task = client.PostAsJsonAsync(SERVICES_URL,js).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            });
            task.Wait();
        }

    }

}

