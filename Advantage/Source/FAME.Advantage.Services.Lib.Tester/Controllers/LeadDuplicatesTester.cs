﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using NUnit.Framework;

namespace FAME.Advantage.Services.Lib.Tester.Controllers
{
    [TestFixture]
    public class LeadDuplicatesTester
    {
        /// <summary>
        /// This constants is different to the constant used by WAPI
        /// and it is only used to test purposes.
        /// </summary>
        private readonly string apiKey;
        private readonly string serviceLayerDuplicatesGet;
        private readonly string serviceLayerDuplicatesGetDuplicate;
        private readonly string serviceLayerButtomCommandDuplicate;

        public LeadDuplicatesTester()
        {
            serviceLayerDuplicatesGet = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_DUPLICATES_GET");
            serviceLayerDuplicatesGetDuplicate = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_DUPLICATES_GETDUP");
            serviceLayerButtomCommandDuplicate = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_DUPLICATES_COMMAND_GET");
            apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        } 
        
        [Test]
        public void GetAllDuplicates()
        {
            string apikey = apiKey;
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(serviceLayerDuplicatesGet),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            });
            task.Wait();
        }

        [Test]
        public void GetPairNewDuplicateLead()
        {
            string apikey = apiKey;
            var client = new HttpClient();
            var queryString = serviceLayerDuplicatesGetDuplicate +
                              "?LeadNewGuid=4E3E160B-4BDB-4A92-BB3B-2FDA3755AEAF&LeadDuplicateGuid=27862A35-4B11-4523-96D2-003666EC7F67";
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(queryString),
                Method = HttpMethod.Get,
                

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            });
            task.Wait();
        }

        [Test]
        public void ButtomCommandDuplicateTest()
        {
            string apikey = apiKey;
            var client = new HttpClient();
            var queryString = serviceLayerButtomCommandDuplicate +
                              "?Command=1&LeadNewGuid=09AD6A40-2B64-4ED7-AFD4-00346A023EE7&LeadDuplicateGuid=27862A35-4B11-4523-96D2-003666EC7F67";
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(queryString),
                Method = HttpMethod.Get,


            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            });
            task.Wait();
        }

    }
}
