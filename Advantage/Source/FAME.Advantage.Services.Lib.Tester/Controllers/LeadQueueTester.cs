﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadQueueTester.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the LeadQueueTester type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Tester.Controllers
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Net.Http;

    using NUnit.Framework;

    [TestFixture]
    public class LeadQueueTester
    {
        private readonly string apiKey;
        private readonly string serviceLayerLeadTaskGet;

        public LeadQueueTester()
        {
            serviceLayerLeadTaskGet = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_QUEUE_GET");
            apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }


        [Test]
        public void GetLeadInfoTask()
        {
            string apikey = apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?CampusId={1}&UserId={2}&Command=2"
                , serviceLayerLeadTaskGet
                , "3F5E839A-589A-4B2A-B258-35A1A8B3B819"
                , "93C461C4-398E-4F30-AE04-A230D0EC7835");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// Command 1 Get only Admission Rep List
        /// </summary>
        [Test]
        public void GetLeadAdmissionRepList()
        {
            string apikey = apiKey;
            var client = new HttpClient();
            var query =
                $"{this.serviceLayerLeadTaskGet}?CampusId={"3F5E839A-589A-4B2A-B258-35A1A8B3B819"}&UserId={"93C461C4-398E-4F30-AE04-A230D0EC7835"}&Command=1";

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// Get all
        /// </summary>
        [Test]
        public void GetLeadAllInfoList()
        {
            string apikey = apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?CampusId={1}&UserId={2}&Command=0"
                , serviceLayerLeadTaskGet
                , "3F5E839A-589A-4B2A-B258-35A1A8B3B819"
                , "93C461C4-398E-4F30-AE04-A230D0EC7835");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }


    }
}
