﻿using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student;
using FAME.Advantage.Domain.Student.Enrollments;
using FAME.Advantage.Services.Lib.Infrastructure;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;
using FAME.Advantage.Services.Lib.Tester.Infrastructure.Bootstrapping;
using log4net;
using NUnit.Framework;
using Rhino.Mocks;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FAME.Advantage.Services.Lib.Tester.Controllers
{
    [TestFixture]
    public class StudentControllerTester
    {
        private readonly static Guid StudentId = new Guid("54AA7A1A-4FAC-4FC5-9FC6-9B2E76AC9B22");

        private static readonly IEnumerable<Enrollment> Enrollments = new[] { new Enrollment(Guid.Empty, new DateTime(2013, 01, 01), Guid.Empty, Guid.Empty, null, null) };
        private readonly Student[] _students = { new Student("John", "Doe", "Smith", null, null, null, Enrollments) { ID = StudentId } };

        private IRepository BuildFakeRepositoryInstance()
        {
            var fakeRepository = MockRepository.GenerateStrictMock<IRepository>();
            fakeRepository.Stub(x => x.Query<Student>()).Return(new EnumerableQuery<Student>(_students));
            return fakeRepository;
        }

        public StudentControllerTester()
        {
            Bootstrapper.Initialize(new Container(x =>
            {
                x.Scan(scan =>
                {
                    scan.AssemblyContainingType<IMapperDefinition>();
                    scan.AddAllTypesOf<IMapperDefinition>();
                });
                x.For<ILog>().Use(LogManager.GetLogger(Globals.GlobalLoggerName));
            }));

            Bootstrapper.Bootstrap();
        }

        //[Test]
        //public void Get_should_return_400_bad_request_when_id_is_0()
        //{
        //    ObjectFactory.Configure(x => x.For<IRepository>().Use(BuildFakeRepositoryInstance()));
        //    var studentController = ObjectFactory.GetInstance<StudentController>();

        //    Action get = () => studentController.Get(Guid.Empty);
        //    get.ShouldThrow<HttpBadRequestResponseException>()
        //     .ResponseTextShouldBe("Student Id is required");
        //}

        //[Test]
        //public void Get_should_return_404_not_found_when_student_is_not_found()
        //{
        //    ObjectFactory.Configure(x => x.For<IRepository>().Use(BuildFakeRepositoryInstance()));
        //    var studentController = ObjectFactory.GetInstance<StudentController>();

        //    Action get = () => studentController.Get(Guid.NewGuid());
        //    get.ShouldThrow<HttpNotFoundResponseException>()
        //     .ResponseTextShouldBe("Student not found");
        //}

        //[Test]
        //public void Get_should_return_the_student_correctly()
        //{
        //    ObjectFactory.Configure(x => x.For<IRepository>().Use(BuildFakeRepositoryInstance()));
        //    var studentController = ObjectFactory.GetInstance<StudentController>();

        //    var student = studentController.Get(StudentId);

        //    student.FirstName.Should().Be("John");
        //    student.LastName.Should().Be("Smith");
        //}
    }
}
