﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Services.Lib.Tester.Controllers.SystemStuff.Institutions
{
    using System.Net;
    using System.Net.Http;

    using FAME.Advantage.Services.Lib.Tester.Model;
    using FAME.Advantage.Services.Lib.Tester.Model.Routes;

    using NUnit.Framework;

    /// <summary>
    /// The institution controller tester.
    /// </summary>
    [TestFixture]
    public class InstitutionControllerTester
    {
        /// <summary>
        /// The get empty id.
        /// </summary>
        [Test]
        public void GetEmptyId()
        {
            string campusId, userId, id;
            campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            id = string.Empty;
            Request request = new Request();
            request.Path = InstitutionRoutes.Get;
            request.QueryString = "CampusId=" + campusId + "&Id=" + id + "&UserId=" + userId;
            request.SendRequest(HttpMethod.Get);
            Console.WriteLine(request.ResponseContent);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(HttpStatusCode.InternalServerError, request.ResponseStatusCode);
        }

        /// <summary>
        /// The get with id.
        /// </summary>
        [Test]
        public void GetWithId()
        {
            string campusId, userId, id;
            campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            id = "4CF2F4E1-E594-4F6E-A587-000329236235";
            Request request = new Request();
            request.Path = Model.Routes.InstitutionRoutes.Get;
            request.QueryString = "CampusId=" + campusId + "&Id=" + id + "&UserId=" + userId;
            request.SendRequest(HttpMethod.Get);
            Console.WriteLine(request.ResponseContent);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(HttpStatusCode.OK, request.ResponseStatusCode);
        }

        /// <summary>
        /// The find Institution by name.
        /// </summary>
        [Test]
        public void Find()
        {
            string campusId, userId, name;
            campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            name = "Jack";
            Request request = new Request();
            request.Path = Model.Routes.InstitutionRoutes.Find;
            request.QueryString = "CampusId=" + campusId + "&UserId=" + userId + "&Name=" + name;
            request.SendRequest(HttpMethod.Get);
            Console.WriteLine(request.ResponseContent);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(HttpStatusCode.OK, request.ResponseStatusCode);
        }


        /// <summary>
        /// The insert Phone.
        /// </summary>
        [Test]
        public void InsertPhone()
        {
            var campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            var userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            var status = "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965";
            var institutionId = "4CF2F4E1-E594-4F6E-A587-000329236235";
            var phone = "789456132";
            Request request = new Request();
            request.Path = InstitutionRoutes.PostPhone;
            request.Body = new Dictionary<string, string>();
            request.Body.Add("CampusId", campusId);
            request.Body.Add("UserId", userId);
            request.Body.Add("StatusId", status);
            request.Body.Add("Phone", phone);
            request.Body.Add("InstitutionId", institutionId);
            request.QueryString = string.Empty;
            request.SendRequest(HttpMethod.Post);
            Console.WriteLine(request.ResponseContent);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(HttpStatusCode.OK, request.ResponseStatusCode);
        }


        /// <summary>
        /// The Edit Phone.
        /// </summary>
        [Test]
        public void EditPhone()
        {
            var campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            var userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            var status = "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965";
            var institutionId = "4CF2F4E1-E594-4F6E-A587-000329236235";
            var phone = "123467890";
            var id = "9894E80A-F2D0-454A-AC7E-12061B92E878";
            Request request = new Request();
            request.Path = InstitutionRoutes.PutPhone;
            request.Body = new Dictionary<string, string>();
            request.Body.Add("CampusId", campusId);
            request.Body.Add("UserId", userId);
            request.Body.Add("StatusId", status);
            request.Body.Add("Phone", phone);
            request.Body.Add("InstitutionId", institutionId);
            request.Body.Add("Id", id);
            request.QueryString = string.Empty;
            request.SendRequest(HttpMethod.Put);
            Console.WriteLine(request.ResponseContent);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(HttpStatusCode.OK, request.ResponseStatusCode);
        }

        /// <summary>
        /// The Delete Phone.
        /// </summary>
        [Test]
        public void DeletePhone()
        {
            var campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            var userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            var id = "9894E80A-F2D0-454A-AC7E-12061B92E878";
            var institutionId = "4CF2F4E1-E594-4F6E-A587-000329236235";

            Request request = new Request();
            request.Path = InstitutionRoutes.DeletePhone;
            request.Body = new Dictionary<string, string>();
            request.QueryString = "CampusId=" + campusId + "&UserId=" + userId + "&InstitutionId=" + institutionId + "&Id=" + id;
            request.SendRequest(HttpMethod.Delete);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(true, request.ResponseText.Contains("deleted"));
        }

        /// <summary>
        /// The insert Contact.
        /// </summary>
        [Test]
        public void InsertContact()
        {
            var campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            var userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            var status = "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965";
            var institutionId = "4CF2F4E1-E594-4F6E-A587-000329236235";
            var firstName = "Test 2";
            var lastName = "Contact 2";
            var prefix = "BF9FBD8D-BB69-41BF-8609-AA03FCBA058D";
            var suffix = "C1FCE650-7FA9-4245-ABD0-EC35C1A230D4";

            Request request = new Request();
            request.Path = InstitutionRoutes.PostContact;
            request.Body = new Dictionary<string, string>();
            request.Body.Add("CampusId", campusId);
            request.Body.Add("UserId", userId);
            request.Body.Add("StatusId", status);
            request.Body.Add("FirstName", firstName);
            request.Body.Add("LastName", lastName);
            request.Body.Add("PrefixId", prefix);
            request.Body.Add("SuffixId", suffix);
            request.Body.Add("InstitutionId", institutionId);
            request.QueryString = string.Empty;
            request.SendRequest(HttpMethod.Post);
            Console.WriteLine(request.ResponseContent);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(HttpStatusCode.OK, request.ResponseStatusCode);
        }


        /// <summary>
        /// The Edit Contact.
        /// </summary>
        [Test]
        public void EditContact()
        {
            var campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            var userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            var status = "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965";
            var institutionId = "4CF2F4E1-E594-4F6E-A587-000329236235";
            var id = "CD6B4637-3E92-4A10-8579-410A19F888EC";
            var firstName = "Test 2";
            var lastName = "Contact 2";
            var prefix = "BF9FBD8D-BB69-41BF-8609-AA03FCBA058D";
            var suffix = "C1FCE650-7FA9-4245-ABD0-EC35C1A230D4";
            var title = "Unit Tester";
            Request request = new Request();
            request.Path = InstitutionRoutes.PutContact;
            request.Body = new Dictionary<string, string>();
            request.Body.Add("CampusId", campusId);
            request.Body.Add("UserId", userId);
            request.Body.Add("StatusId", status);
            request.Body.Add("FirstName", firstName);
            request.Body.Add("LastName", lastName);
            request.Body.Add("PrefixId", prefix);
            request.Body.Add("SuffixId", suffix);
            request.Body.Add("InstitutionId", institutionId);
            request.Body.Add("Title", title);
            request.Body.Add("Id", id);
            request.QueryString = string.Empty;
            request.SendRequest(HttpMethod.Put);
            Console.WriteLine(request.ResponseContent);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(HttpStatusCode.OK, request.ResponseStatusCode);
        }

        /// <summary>
        /// The Delete Contact.
        /// </summary>
        [Test]
        public void DeleteContact()
        {
            var campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            var userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            var id = "CD6B4637-3E92-4A10-8579-410A19F888EC";
            var institutionId = "4CF2F4E1-E594-4F6E-A587-000329236235";

            Request request = new Request();
            request.Path = InstitutionRoutes.DeleteContact;
            request.Body = new Dictionary<string, string>();
            request.QueryString = "CampusId=" + campusId + "&UserId=" + userId + "&InstitutionId=" + institutionId + "&Id=" + id;
            request.SendRequest(HttpMethod.Delete);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(true, request.ResponseText.Contains("deleted"));
        }

        /// <summary>
        /// The insert Address.
        /// </summary>
        [Test]
        public void InsertAddress()
        {
            var campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            var userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            var status = "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965";
            var institutionId = "4CF2F4E1-E594-4F6E-A587-000329236235";
            var address1 = "123123 Federal Hwy.";
            var isMailingAddress = true;
            var city = "Ft. Laudardale";
            var zipCode = 23442;
            var countryId = "D73BFC65-DB8C-48AF-8780-583BF9FAE42E";
            var stateId = "5F73028F-8CC4-40C1-9AC2-9FB709E790B7";
            var typeId = "21402DB9-9F68-4473-A255-6512B070EA7B";

            Request request = new Request();
            request.Path = InstitutionRoutes.PostAddress;
            request.Body = new Dictionary<string, string>();
            request.Body.Add("CampusId", campusId);
            request.Body.Add("UserId", userId);
            request.Body.Add("StatusId", status);
            request.Body.Add("Address1", address1);
            request.Body.Add("City", city);
            request.Body.Add("TypeId", typeId);
            request.Body.Add("StateId", stateId);
            request.Body.Add("ZipCode", zipCode.ToString());
            request.Body.Add("CountryId", countryId);
            request.Body.Add("InstitutionId", institutionId);
            request.Body.Add("IsMailingAddress", isMailingAddress.ToString());
            request.QueryString = string.Empty;
            request.SendRequest(HttpMethod.Post);
            Console.WriteLine(request.ResponseContent);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(HttpStatusCode.OK, request.ResponseStatusCode);
        }


        /// <summary>
        /// The Edit Address.
        /// </summary>
        [Test]
        public void EditAddress()
        {
            var campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            var userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            var status = "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965";
            var institutionId = "4CF2F4E1-E594-4F6E-A587-000329236235";
            var address1 = "123123 Federal Hwy.";
            var isMailingAddress = true;
            var city = "Ft. Laudardale";
            var zipCode = 23442;
            var country = "USA";
            var state = "Florida";
            var typeId = "21402DB9-9F68-4473-A255-6512B070EA7B";
            var id = "337C8FA2-A3D4-4EF0-973A-94CE9D9E1127";
            var IsInternational = true;

            Request request = new Request();
            request.Path = InstitutionRoutes.PutAddress;
            request.Body = new Dictionary<string, string>();
            request.Body.Add("CampusId", campusId);
            request.Body.Add("UserId", userId);
            request.Body.Add("StatusId", status);
            request.Body.Add("Address1", address1);
            request.Body.Add("City", city);
            request.Body.Add("TypeId", typeId);
            request.Body.Add("State", state);
            request.Body.Add("ZipCode", zipCode.ToString());
            request.Body.Add("ForeignCountry", country);
            request.Body.Add("InstitutionId", institutionId);
            request.Body.Add("IsMailingAddress", isMailingAddress.ToString());
            request.Body.Add("IsInternational", IsInternational.ToString());
            request.Body.Add("Id", id);
            request.QueryString = string.Empty;
            request.SendRequest(HttpMethod.Put);
            Console.WriteLine(request.ResponseContent);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(HttpStatusCode.OK, request.ResponseStatusCode);
        }

        /// <summary>
        /// The Delete Address.
        /// </summary>
        [Test]
        public void Delete()
        {
            var campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            var userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            var id = "337C8FA2-A3D4-4EF0-973A-94CE9D9E1127";
            var institutionId = "4CF2F4E1-E594-4F6E-A587-000329236235";

            Request request = new Request();
            request.Path = InstitutionRoutes.DeleteAddress;
            request.Body = new Dictionary<string, string>();
            request.QueryString = "CampusId=" + campusId + "&UserId=" + userId + "&InstitutionId=" + institutionId + "&Id=" + id;
            request.SendRequest(HttpMethod.Delete);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(true, request.ResponseText.Contains("deleted"));
        }
    }
}
