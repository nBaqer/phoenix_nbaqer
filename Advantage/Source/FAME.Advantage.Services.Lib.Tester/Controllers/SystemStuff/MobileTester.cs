﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MobileTester.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The Mobile tester.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Tester.Controllers.SystemStuff
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Http;
    using System.Web.Helpers;

    using NUnit.Framework;

    /// <summary>
    ///  The Mobile tester.
    /// </summary>
    [TestFixture]
    public class MobileTester
    {
        /// <summary>
        /// This constants is different to the constant used by WAPI
        /// and it is only used to test purposes.
        /// </summary>
        private readonly string apiKey;

        /// <summary>
        /// The service layer lead info bar get.
        /// </summary>
        private readonly string serviceLayerMobileGet;

        /// <summary>
        /// The service layer mobile get for WS.
        /// </summary>
        private readonly string serviceLayerMobileGetWs;
        ////private readonly string serviceLayerLeadGroup;
        ////private readonly string serviceLayerLeadResources;
        ////private readonly string serviceLayerLeadDemographic;
        ////private readonly string serviceLayerLeadAcademics;
        ////private readonly string serviceLayerIsLeadErasable;

        /// <summary>
        /// Initializes a new instance of the <see cref="MobileTester"/> class.
        /// </summary>
        public MobileTester()
        {
            this.serviceLayerMobileGet = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_GET_MOBILE_CONFIGURATION");
            this.serviceLayerMobileGetWs = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_GET_MOBILE_CONFIGURATION_WS");
            ////this.serviceLayerLeadGroup = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEADGROUPS_COMMAND_GET");
            ////this.serviceLayerLeadResources = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_INFO_RESOURCES_GET");
            ////this.serviceLayerLeadDemographic = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_DEMOGRAPHIC_COMMAND_GET");
            ////this.serviceLayerLeadAcademics = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_ITEMS_COMMAND_GET");
            ////this.serviceLayerIsLeadErasable = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_IS_ERASABLE_COMMAND_GET");
            this.apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }

        /// <summary>
        /// The get mobile configuration.
        /// </summary>
        [Test]
        public void GetKlassAppConfig()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = $"{this.serviceLayerMobileGet}?Command=1";

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// The initialize configuration table.
        /// This test truncate the Configuration kLASSAPP table and
        /// restore the values in insert state.
        /// </summary>
        [Test]
        public void InitializeConfigurationTable()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = $"{this.serviceLayerMobileGet}?Command=3&UserId=864335EE-C9C6-47C9-BBCB-DEEE766F27A1";

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// The get mobile configuration.
        /// </summary>
        [Test]
        public void SettingAndGetKlassAppConfig()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = $"{this.serviceLayerMobileGet}?Command=2&UserId=864335EE-C9C6-47C9-BBCB-DEEE766F27A1";

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            string content = string.Empty;
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                content = response.Content.ReadAsStringAsync().Result;
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Console.WriteLine(response.ToString());
                    Console.WriteLine();
                    Console.WriteLine(content);
                }

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var configs = Json.Decode(content);
                foreach (var config in configs.ConfigurationSetting)
                {
                    Console.WriteLine("{0} - {1}", config.ItemLabel, config.LastOperationLog);
                }
            });

            task.Wait();

            ////foreach (var config in config.ConfigurationSetting)
            ////               {
            ////                  Assert.AreEqual("OK", config.LastOperationLog, string.Format("Error in {0} - {1}", config.ItemLabel, config.LastOperationLog));
            ////               }

        }

        /// <summary>
        /// The get if configuration is OK
        /// </summary>
        [Test]
        public void GetIfConfigurationIsOk()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = $"{this.serviceLayerMobileGet}?Command=4&UserId=864335EE-C9C6-47C9-BBCB-DEEE766F27A1";

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            string content;
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                content = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine();
                Console.WriteLine(content);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });

            task.Wait();
        }


        /// <summary>
        /// The get if configuration is OK
        /// </summary>
        [Test]
        public void GetStudentsToKlassApp()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = this.serviceLayerMobileGetWs;

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };
            client.Timeout = TimeSpan.FromSeconds(480);

            request.Headers.Add("AuthKey", apikey);
            string content = string.Empty;
            Stopwatch watch = Stopwatch.StartNew();

            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                watch.Stop();
                Console.WriteLine($"Time Elapsed: {watch.Elapsed}");
                var response = taskwithmsg.Result;
                content = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine();
                Console.Write($"............. Payload Length: {content.Length / 1000} KB");
                Console.WriteLine();
                Console.WriteLine(content);


                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                ////var configs = Json.Decode(content);
            });

            task.Wait();
        }

        [Test]
        public void SendCleanTokenToKlassApp()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = $"{this.serviceLayerMobileGet}?Command=10&UserId=864335EE-C9C6-47C9-BBCB-DEEE766F27A1";

            var request = new HttpRequestMessage
                              {
                                  RequestUri = new Uri(query),
                                  Method = HttpMethod.Get,

                              };
            client.Timeout = TimeSpan.FromSeconds(480);

            request.Headers.Add("AuthKey", apikey);
            string content = string.Empty;
            Stopwatch watch = Stopwatch.StartNew();

            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
                {
                    watch.Stop();
                    Console.WriteLine($"Time Elapsed: {watch.Elapsed}");
                    var response = taskwithmsg.Result;
                    content = response.Content.ReadAsStringAsync().Result;
                    Console.WriteLine(response.ToString());
                    Console.WriteLine();
                    Console.Write($"............. Payload Length: {content.Length / 1000} KB");
                    Console.WriteLine();
                    Console.WriteLine(content);


                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    ////var configs = Json.Decode(content);
                });

            task.Wait();
        }


    }
}
