﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using NUnit.Framework;

namespace FAME.Advantage.Services.Lib.Tester.Controllers
{
    [TestFixture]
    public class LeadNotesTest
    {
         private readonly string apiKey;
        private readonly string serviceLayerGet;

        public LeadNotesTest()
        {
            serviceLayerGet = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_NOTES_GET");
            apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }

        /// <summary>
        /// Test Command 1 get the list of Notes
        /// </summary>
        [Test]
        public void GetCommand1Test()
        {
            string apikey = apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?LeadId={1}&UserId={2}&CampusId={3}&Command=1&ModuleCode=AD"
                , serviceLayerGet
                , "EDAF7F55-6AB8-4E92-84D8-82D8F651B577"
                , "9C46FE6D-C23D-43AD-B829-155D431AAACA"
                , "3F5E839A-589A-4B2A-B258-35A1A8B3B819");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        [Test]
        public void GetCommand2Test()
        {
            string apikey = apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?LeadId={1}&UserId={2}&Command=2&ModuleCode=AD"
                , serviceLayerGet
                , "EDAF7F55-6AB8-4E92-84D8-82D8F651B577"
                , "9C46FE6D-C23D-43AD-B829-155D431AAACA");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// 
        /// </summary>
        [Test]
        public void GetCommand3Test()
        {
            string apikey = apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?LeadId={1}&UserId={2}&Command=3&ModuleCode=AD"
                , serviceLayerGet
                , "EDAF7F55-6AB8-4E92-84D8-82D8F651B577"
                , "9C46FE6D-C23D-43AD-B829-155D431AAACA");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }




    }
}
