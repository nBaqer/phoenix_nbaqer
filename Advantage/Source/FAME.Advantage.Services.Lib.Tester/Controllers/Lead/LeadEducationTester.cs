﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadEducationTester.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.Tester.Controllers.Lead.LeadEducationTester
// </copyright>
// <summary>
//   The lead education tester.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Tester.Controllers.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.Services.Lib.Tester.Model;
    using FAME.Advantage.Services.Lib.Tester.Model.Routes;

    using FluentNHibernate.Mapping;

    using NUnit.Framework;

    /// <summary>
    /// The lead education tester.
    /// </summary>
    [TestFixture]
    public class LeadEducationTester
    {
        /// <summary>
        /// The get.
        /// </summary>
        [Test]
        public void GetEmptyId()
        {
            string campusId, userId, id, leadId;
            campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            leadId = "C9082C09-0BFF-43AF-A239-E0AF9FE1B253";
            id = string.Empty;
            Request request = new Request();
            request.Path = LeadEducationRoutes.Get;
            request.QueryString = "CampusId=" + campusId + "&Id=" + id + "&UserId=" + userId + "&LeadId=" + leadId;
            request.SendRequest(HttpMethod.Get);
            Console.WriteLine(request.ResponseContent);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(HttpStatusCode.InternalServerError, request.ResponseStatusCode);
        }

        /// <summary>
        /// The get with id.
        /// </summary>
        [Test]
        public void GetWithId()
        {
            string campusId, userId, id, leadId;
            campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762";
            id = "1E0A8527-686C-4D54-87E9-078ABCC059DF";
            Request request = new Request();
            request.Path = LeadEducationRoutes.Get;
            request.QueryString = "CampusId=" + campusId + "&Id=" + id + "&UserId=" + userId + "&LeadId=" + leadId;
            request.SendRequest(HttpMethod.Get);
            Console.WriteLine(request.ResponseContent);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(HttpStatusCode.OK, request.ResponseStatusCode);
        }

        /// <summary>
        /// The find.
        /// </summary>
        [Test]
        public void Find()
        {
            string campusId, userId, leadId;
            campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762";
            Request request = new Request();
            request.Path = LeadEducationRoutes.Find;
            request.QueryString = "CampusId=" + campusId + "&UserId=" + userId + "&LeadId=" + leadId;
            request.SendRequest(HttpMethod.Get);
            Console.WriteLine(request.ResponseContent);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(HttpStatusCode.OK, request.ResponseStatusCode);
        }

        /// <summary>
        /// The insert.
        /// </summary>
        [Test]
        public void Insert()
        {
            var campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            var userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762";
            var typeId = "2";
            var institutionId = "4CF2F4E1-E594-4F6E-A587-000329236235";
            var levelId = "1";
            Request request = new Request();
            request.Path = LeadEducationRoutes.Post;
            request.Body = new Dictionary<string, string>();
            request.Body.Add("CampusId", campusId);
            request.Body.Add("UserId", userId);
            request.Body.Add("LeadId", leadId);
            request.Body.Add("TypeId", typeId);
            request.Body.Add("InstitutionId", institutionId);
            request.Body.Add("LevelId", levelId);
            request.QueryString = string.Empty;
            request.SendRequest(HttpMethod.Post);
            Console.WriteLine(request.ResponseContent);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(HttpStatusCode.OK, request.ResponseStatusCode);
        }


        /// <summary>
        /// The Edit.
        /// </summary>
        [Test]
        public void Edit()
        {
            var campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            var userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762";
            var typeId = "2";
            var institutionId = "4CF2F4E1-E594-4F6E-A587-000329236235";
            var levelId = "1";
            var id = "69E1DA46-71A3-43CC-99F5-A08EC28D20D2";
            var gpa = 2.8;
            var precentile = 85;

            Request request = new Request();
            request.Path = LeadEducationRoutes.Put;
            request.Body = new Dictionary<string, string>();
            request.Body.Add("CampusId", campusId);
            request.Body.Add("UserId", userId);
            request.Body.Add("LeadId", leadId);
            request.Body.Add("TypeId", typeId);
            request.Body.Add("InstitutionId", institutionId);
            request.Body.Add("LevelId", levelId);
            request.Body.Add("Id", id);
            request.Body.Add("Gpa", gpa.ToString());
            request.Body.Add("Percentile", precentile.ToString());
            request.QueryString = string.Empty;
            request.SendRequest(HttpMethod.Put);
            Console.WriteLine(request.ResponseContent);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(HttpStatusCode.OK, request.ResponseStatusCode);
        }

        /// <summary>
        /// The Edit.
        /// </summary>
        [Test]
        public void Delete()
        {
            var campusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819";
            var userId = "864335EE-C9C6-47C9-BBCB-DEEE766F27A1";
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762";
            var id = "69E1DA46-71A3-43CC-99F5-A08EC28D20D2";

            Request request = new Request();
            request.Path = LeadEducationRoutes.Delete;
            request.Body = new Dictionary<string, string>();
            request.QueryString = "CampusId=" + campusId + "&UserId=" + userId + "&LeadId=" + leadId + "&Id=" + id;
            request.SendRequest(HttpMethod.Delete);
            Console.WriteLine(request.ResponseText);
            Assert.AreEqual(request.ResponseText, string.Empty);
        }
    }
}
