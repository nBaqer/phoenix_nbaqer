﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadPriorWorkTester.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the LeadPriorWorkTester type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Tester.Controllers.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using Messages.Common;
    using Messages.Lead.PriorWork;
    using Newtonsoft.Json.Linq;
    using NUnit.Framework;

    /// <summary>
    ///  The lead prior work tester.
    /// </summary>
    [TestFixture]
    public class LeadPriorWorkTester
    {
        /// <summary>
        ///  The API key.
        /// </summary>
        private readonly string apiKey;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadPriorWorkTester"/> class.
        /// </summary>
        public LeadPriorWorkTester()
        {
            this.apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }


        /// <summary>
        /// The poke service test.
        /// </summary>
        [Test]
        public void PokeServiceTest()
        {
            var client = new HttpClient();
            var query = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_GET") + "?Command=100";
            var request = new HttpRequestMessage { RequestUri = new Uri(query), Method = HttpMethod.Get, };

            request.Headers.Add("AuthKey", this.apiKey);
            var task = client.SendAsync(request).ContinueWith(
                taskwithmsg =>
                    {
                        var response = taskwithmsg.Result;
                        Console.WriteLine(response.ToString());
                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    });
            task.Wait();
        }

        #region Get tests   
        /// <summary>
        /// The get service command 1 test.
        /// Test the service that get All prior work for a specific Lead or Student
        /// </summary>
        [Test]
        public void GetServiceCommand1Test()
        {
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan
            var client = new HttpClient();
            var query = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_GET")
                        + string.Format("?Command=1&LeadId={0}", leadId);
            var request = new HttpRequestMessage { RequestUri = new Uri(query), Method = HttpMethod.Get, };

            request.Headers.Add("AuthKey", this.apiKey);
            var task = client.SendAsync(request).ContinueWith(
                taskwithmsg =>
                    {
                        var response = taskwithmsg.Result;
                        Console.WriteLine(response.ToString());
                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    });
            task.Wait();
        }

        /// <summary>
        /// The get service command 2 test.
        /// Get Drop Down content for Ad Titles (all)
        /// </summary>
        [Test]
        public void GetServiceCommand2Test()
        {
            var client = new HttpClient();
            var query = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_GET") + "?Command=2";
            var request = new HttpRequestMessage { RequestUri = new Uri(query), Method = HttpMethod.Get, };

            request.Headers.Add("AuthKey", this.apiKey);
            var task = client.SendAsync(request).ContinueWith(
                taskwithmsg =>
                    {
                        var response = taskwithmsg.Result;
                        Console.WriteLine(response.ToString());
                        var content = response.Content.ReadAsStringAsync().Result;

                        Console.WriteLine(content);
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        var json = JObject.Parse(content);
                        var output = json["AdSchoolJobTitlesItemList"].ToList();
                        Assert.AreEqual(output.Count, 4);
                    });
            task.Wait();
        }

        /// <summary>
        /// The get service command 3 test.
        /// Get Drop Down content for plJobStatus (by campus and active)
        /// </summary>
        [Test]
        public void GetServiceCommand3Test()
        {
            var client = new HttpClient();
            var campusId = ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE_EASTGATE");
            var query = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_GET")
                        + string.Format("?Command=3&CampusId={0}", campusId);
            var request = new HttpRequestMessage { RequestUri = new Uri(query), Method = HttpMethod.Get, };

            request.Headers.Add("AuthKey", this.apiKey);
            var task = client.SendAsync(request).ContinueWith(
                taskwithmsg =>
                    {
                        var response = taskwithmsg.Result;
                        Console.WriteLine(response.ToString());
                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    });
            task.Wait();
        }

        /// <summary>
        /// The get service command 4 test Get Drop Down the States of USA order by description
        /// </summary>
        [Test]
        public void GetServiceCommand4Test()
        {
            var campusId = ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE_EASTGATE");

            // var LeadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan
            var client = new HttpClient();
            var query = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_GET")
                        + string.Format("?Command=4&CampusId={0}", campusId);
            var request = new HttpRequestMessage { RequestUri = new Uri(query), Method = HttpMethod.Get, };

            request.Headers.Add("AuthKey", this.apiKey);
            var task = client.SendAsync(request).ContinueWith(
                taskwithmsg =>
                    {
                        var response = taskwithmsg.Result;
                        Console.WriteLine(response.ToString());
                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    });
            task.Wait();
        }

        /// <summary>
        /// Command 5 Get the Drop Down Countries of USA
        /// </summary>
        [Test]
        public void GetServiceCommand5Test()
        {
            var campusId = ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE_EASTGATE");

            // var LeadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan
            var client = new HttpClient();
            var query = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_GET")
                        + string.Format("?Command=5&CampusId={0}", campusId);
            var request = new HttpRequestMessage { RequestUri = new Uri(query), Method = HttpMethod.Get, };

            request.Headers.Add("AuthKey", this.apiKey);
            var task = client.SendAsync(request).ContinueWith(
                taskwithmsg =>
                    {
                        var response = taskwithmsg.Result;
                        Console.WriteLine(response.ToString());
                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    });
            task.Wait();
        }

        /// <summary>
        /// The get service command 6 test.
        /// Get the Prior Work details and contacts
        /// </summary>
        [Test]
        public void GetServiceCommand6Test()
        {
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan

            // Preparing the test
            this.DeleteInsertedFieldFromPriorWorkTable(leadId);
            var priorWorkId = this.InsertAPriorWorkItemWithContactAndAddress(leadId);

            var client = new HttpClient();
            var query = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_GET")
                        + string.Format("?Command=6&LeadId={0}&PriorWorkId={1}", leadId, priorWorkId);
            var request = new HttpRequestMessage { RequestUri = new Uri(query), Method = HttpMethod.Get, };

            request.Headers.Add("AuthKey", this.apiKey);
            var task = client.SendAsync(request).ContinueWith(
                taskwithmsg =>
                {
                    var response = taskwithmsg.Result;
                    Console.WriteLine(response.ToString());
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                });
            task.Wait();
        }

        /// <summary>
        ///  The get service command 7 test.
        /// </summary>
        [Test]
        public void GetServiceCommand7Test()
        {
            var campusId = ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE_EASTGATE");

            // var LeadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan
            var client = new HttpClient();
            var query = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_GET")
                        + string.Format("?Command=7&CampusId={0}", campusId);
            var request = new HttpRequestMessage { RequestUri = new Uri(query), Method = HttpMethod.Get, };

            request.Headers.Add("AuthKey", this.apiKey);
            var task = client.SendAsync(request).ContinueWith(
                taskwithmsg =>
                {
                    var response = taskwithmsg.Result;
                    Console.WriteLine(response.ToString());
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                });
            task.Wait();
        }

        #endregion

        #region Post

        /// <summary>
        /// The post service minimal command 1 test.
        /// Only the required fields
        /// </summary>
        [Test]
        public void PostDetectMissingRequiredFieldCommand1Test()
        {
            var servicesUrl = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_POST");
            var campusId = ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE_EASTGATE");
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan
            var userId = ConfigurationManager.AppSettings.Get("USER_SUPPORT");

            // Clean the table of before inserting...
            this.DeleteInsertedFieldFromPriorWorkTable(leadId);

            var mess = PriorWorkOutputModel.Factory();
            mess.InputModel.CampusId = campusId;
            mess.InputModel.LeadId = leadId;
            mess.InputModel.UserId = userId;
            mess.InputModel.Command = 1;
            mess.AdSchoolJobTitlesItemList = null;
            mess.JobStatusItemList = null;

            var pw = LeadPriorWorkTester.MinimaPriorWorkItemOutputModel();
            pw.JobTitle = string.Empty; // This is a required field. we put empty to test the rejection
            mess.PriorWorkItemList.Add(pw as PriorWorkItemOutputModel);

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", this.apiKey);
            var task = client.PostAsJsonAsync(servicesUrl, mess).ContinueWith(
                taskwithmsg =>
                    {
                        var response = taskwithmsg.Result;
                        Console.WriteLine(response.ToString());
                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                        Assert.AreEqual(HttpStatusCode.NotAcceptable, response.StatusCode);
                        return response;
                    });
            task.Wait();
        }

        /// <summary>
        ///  The post service minimal command 1 test.
        /// Only the required fields
        /// </summary>
        [Test]
        public void PostServiceMinimalCommand1Test()
        {
            var servicesUrl = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_POST");
            var campusId = ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE_EASTGATE");
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan
            var userId = ConfigurationManager.AppSettings.Get("USER_SUPPORT");

            // Clean the table of before inserting...
            this.DeleteInsertedFieldFromPriorWorkTable(leadId);

            var mess = PriorWorkOutputModel.Factory();
            mess.InputModel.CampusId = campusId;
            mess.InputModel.LeadId = leadId;
            mess.InputModel.UserId = userId;
            mess.InputModel.Command = 1;
            mess.AdSchoolJobTitlesItemList = null;
            mess.JobStatusItemList = null;

            var pw = PriorWorkItemOutputModel.Factory();
            pw.Employer = "Juana Bacallao";
            pw.JobTitle = "Tarugo";
            mess.PriorWorkItemList.Add(pw as PriorWorkItemOutputModel);

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", this.apiKey);
            var task = client.PostAsJsonAsync(servicesUrl, mess).ContinueWith(
                taskwithmsg =>
                    {
                        var response = taskwithmsg.Result;
                        Console.WriteLine(response.ToString());
                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        return response;
                    });
            task.Wait();
        }

        /// <summary>
        ///  The post service minimal command 1 test.
        ///  Prior Work Item: Only the required fields
        ///  Address: Minimal only Address1
        /// </summary>
        [Test]
        public void PostServiceMinimalWidthAddressCommand1Test()
        {
            var servicesUrl = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_POST");
            var campusId = ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE_EASTGATE");
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan
            var userId = ConfigurationManager.AppSettings.Get("USER_SUPPORT");

            // Clean the table of before inserting...
            this.DeleteInsertedFieldFromPriorWorkTable(leadId);

            var mess = PriorWorkOutputModel.Factory();
            mess.InputModel.CampusId = campusId;
            mess.InputModel.LeadId = leadId;
            mess.InputModel.UserId = userId;
            mess.InputModel.Command = 1;
            mess.AdSchoolJobTitlesItemList = null;
            mess.JobStatusItemList = null;

            var pw = LeadPriorWorkTester.MinimaPriorWorkItemOutputModel();
            var address = new PriorWorkAddressOutputModel { Address1 = "RequiredAddress1" };
            pw.AddressOutputModel = address;
            mess.PriorWorkItemList.Add(pw as PriorWorkItemOutputModel);

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", this.apiKey);
            var task = client.PostAsJsonAsync(servicesUrl, mess).ContinueWith(
                taskwithmsg =>
                {
                    var response = taskwithmsg.Result;
                    Console.WriteLine(response.ToString());
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    return response;
                });
            task.Wait();
        }

        /// <summary>
        /// The post with complete address command 1 test.
        /// </summary>
        [Test]
        public void PostWithCompleteAddressUsaCommand1Test()
        {
            var servicesUrl = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_POST");
            var campusId = ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE_EASTGATE");
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan
            var userId = ConfigurationManager.AppSettings.Get("USER_SUPPORT");

            // Clean the table of before inserting...
            this.DeleteInsertedFieldFromPriorWorkTable(leadId);

            var mess = PriorWorkOutputModel.Factory();
            mess.InputModel.CampusId = campusId;
            mess.InputModel.LeadId = leadId;
            mess.InputModel.UserId = userId;
            mess.InputModel.Command = 1;
            mess.AdSchoolJobTitlesItemList = null;
            mess.JobStatusItemList = null;

            var pw = LeadPriorWorkTester.MinimaPriorWorkItemOutputModel();
            var address = LeadPriorWorkTester.CreateCompleteUsaPriorWorkAddressOutputModel(userId);
            pw.AddressOutputModel = address;

            mess.PriorWorkItemList.Add(pw as PriorWorkItemOutputModel);

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", this.apiKey);
            var task = client.PostAsJsonAsync(servicesUrl, mess).ContinueWith(
                taskwithmsg =>
                    {
                        var response = taskwithmsg.Result;
                        Console.WriteLine(response.ToString());
                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        return response;
                    });
            task.Wait();
        }

        /// <summary>
        /// The post with address command 1 test.
        /// Address is international
        /// </summary>
        [Test]
        public void PostWithInternationalAddressCommand1Test()
        {
            var servicesUrl = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_POST");
            var campusId = ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE_EASTGATE");
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan
            var userId = ConfigurationManager.AppSettings.Get("USER_SUPPORT");

            // Clean the table of before inserting...
            this.DeleteInsertedFieldFromPriorWorkTable(leadId);

            var mess = PriorWorkOutputModel.Factory();
            mess.InputModel.CampusId = campusId;
            mess.InputModel.LeadId = leadId;
            mess.InputModel.UserId = userId;
            mess.InputModel.Command = 1;
            mess.AdSchoolJobTitlesItemList = null;
            mess.JobStatusItemList = null;

            var pw = LeadPriorWorkTester.MinimaPriorWorkItemOutputModel();
            var address = LeadPriorWorkTester.CreateInternationalPriorWorkAddressOutputModel(userId);
            pw.AddressOutputModel = address;

            mess.PriorWorkItemList.Add(pw as PriorWorkItemOutputModel);

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", this.apiKey);
            var task = client.PostAsJsonAsync(servicesUrl, mess).ContinueWith(
                taskwithmsg =>
                    {
                        var response = taskwithmsg.Result;
                        Console.WriteLine(response.ToString());
                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        return response;
                    });
            task.Wait();
        }

        /// <summary>
        ///  The post with international address and all prior work fields command 1 test.
        /// </summary>
        [Test]
        public void PostWithInternationalAddressAndAllPriorWorkFieldsCommand1Test()
        {
            var servicesUrl = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_POST");
            var campusId = ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE_EASTGATE");
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan
            var userId = ConfigurationManager.AppSettings.Get("USER_SUPPORT");

            // Clean the table of before inserting...
            this.DeleteInsertedFieldFromPriorWorkTable(leadId);

            var mess = PriorWorkOutputModel.Factory();
            mess.InputModel.CampusId = campusId;
            mess.InputModel.LeadId = leadId;
            mess.InputModel.UserId = userId;
            mess.InputModel.Command = 1;
            mess.AdSchoolJobTitlesItemList = null;
            mess.JobStatusItemList = null;

            var pw = LeadPriorWorkTester.MinimaPriorWorkItemOutputModel();
            pw.Comments = "Comment AllFields";
            pw.EndDate = new DateTime(2015, 12, 31);
            pw.StartDate = new DateTime(2014, 12, 31);
            pw.JobStatus = new DropDownOutputModel()
            {
                ID = "FD6E4CD5-0B1B-413A-93EC-7EC67D715F6A",
                Description = "Full-time"
            };
            pw.SchoolJobTitle = new DropDownOutputModel()
            {
                ID = "473EBC4F-8440-4570-A021-458726D7DFA5",
                Description = "Medical Office"
            };
            var address = LeadPriorWorkTester.CreateInternationalPriorWorkAddressOutputModel(userId);
            pw.AddressOutputModel = address;

            mess.PriorWorkItemList.Add(pw as PriorWorkItemOutputModel);

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", this.apiKey);
            var task = client.PostAsJsonAsync(servicesUrl, mess).ContinueWith(
                taskwithmsg =>
                    {
                        var response = taskwithmsg.Result;
                        Console.WriteLine(response.ToString());
                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                        return response;
                    });
            task.Wait();
        }

        /// <summary>
        ///  The post service minimal command 2 test.
        ///  Prior Work Item: Only the required fields
        ///  Address: Minimal only Address1
        /// </summary>
        [Test]
        public void PostServicePriorWorkContactCommand2Test()
        {
            var servicesUrl = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_POST");
            var campusId = ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE_EASTGATE");
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan
            var userId = ConfigurationManager.AppSettings.Get("USER_SUPPORT");

            // Clean the table of before inserting...
            this.DeleteInsertedFieldFromPriorWorkTable(leadId);
            var priorWorkId = this.InsertAPriorWorkItemWithContactAndAddress(leadId);

            var mess = PriorWorkOutputModel.Factory();

            // Fill Input Object
            mess.InputModel.CampusId = campusId;
            mess.InputModel.LeadId = leadId;
            mess.InputModel.UserId = userId;
            mess.InputModel.PriorWorkId = priorWorkId;
            mess.InputModel.Command = 2;

            // Fill the details
            var details = new PriorWorkDetailItemOutputModel { ContactList = new List<PriorWorkContactOutputModel>() };
            var contact = new PriorWorkContactOutputModel() { FirstName = "CONTACT TEST FIRST NAME", LastName = "CONTACT TEST LAST NAME", Title = "TITLE TEST" };
            details.ContactList.Add(contact);
            mess.PriorWorkDetailObject = details;

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", this.apiKey);
            var task = client.PostAsJsonAsync(servicesUrl, mess).ContinueWith(
                taskwithmsg =>
                {
                    var response = taskwithmsg.Result;
                    Console.WriteLine(response.ToString());
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    return response;
                });
            task.Wait();
        }



        #endregion

        #region Post Delete
        /// <summary>
        ///  The post delete prior work command 7 test.
        /// </summary>
        [Test]
        public void PostDeletePriorWorkCommand7Test()
        {
            var servicesUrl = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_POST");
            var campusId = ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE_EASTGATE");
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan
            var userId = ConfigurationManager.AppSettings.Get("USER_SUPPORT");

            // Insert if not exists a prior work with contact and address
            var priorId = this.InsertAPriorWorkItemWithContactAndAddress(leadId);

            Assert.IsNotNull(priorId, "Error in infrastructure of test. no Prior work exists in database");

            // Create input model
            var mess = PriorWorkOutputModel.Factory();
            mess.InputModel.CampusId = campusId;
            mess.InputModel.LeadId = leadId;
            mess.InputModel.UserId = userId;
            mess.InputModel.Command = 7; // Delete Prior Work
            mess.InputModel.PriorWorkId = priorId;
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", this.apiKey);
            var task = client.PostAsJsonAsync(servicesUrl, mess).ContinueWith(
                taskwithmsg =>
                {
                    var response = taskwithmsg.Result;
                    Console.WriteLine(response.ToString());
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    return response;
                });
            task.Wait();
        }
        #endregion

        #region Post
        /// <summary>
        ///  The post service minimal command 1 test.
        /// Case 1: Actual has address returned has Address
        /// Only the required fields as send for Prior Work Item
        /// </summary>
        [Test]
        public void PostUpdateWidthExistingAddressCommand3Test()
        {
            var servicesUrl = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_POST");
            var campusId = ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE_EASTGATE");
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan
            var userId = ConfigurationManager.AppSettings.Get("USER_SUPPORT");

            // Clean the table of before inserting...
            this.DeleteInsertedFieldFromPriorWorkTable(leadId);

            // Insert a record with Address
            this.InsertAPriorWorkItemWithContactAndAddress(leadId);
            var priorId = this.InsertAPriorWorkItemWithContactAndAddress(leadId);

            // Create the record
            var mess = PriorWorkOutputModel.Factory();
            mess.InputModel.CampusId = campusId;
            mess.InputModel.LeadId = leadId;
            mess.InputModel.UserId = userId;
            mess.InputModel.Command = 3;
            mess.InputModel.PriorWorkId = priorId;
            mess.AdSchoolJobTitlesItemList = null;
            mess.JobStatusItemList = null;

            var pw = LeadPriorWorkTester.MinimaPriorWorkItemOutputModel(priorId);
            var address = LeadPriorWorkTester.CreateCompleteUsaPriorWorkAddressOutputModel(userId);
            pw.AddressOutputModel = address;
            pw.SchoolJobTitle = new DropDownOutputModel
            {
                Description = "Dental Assistant",
                ID = "0D71F7B0-E94B-4901-9CA7-BBDA956FE1CB"
            };

            mess.PriorWorkItemList.Add(pw as PriorWorkItemOutputModel);

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", this.apiKey);
            var task = client.PostAsJsonAsync(servicesUrl, mess).ContinueWith(
                taskwithmsg =>
                {
                    var response = taskwithmsg.Result;
                    Console.WriteLine(response.ToString());
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    return response;
                });
            task.Wait();
        }

        /// <summary>
        ///  The post service minimal command 1 test.
        /// Case 1: Actual has address returned has Address
        /// Only the required fields
        /// </summary>
        [Test]
        public void PostUpdateWidthExistingAddressConvertToInternationalCommand3Test()
        {
            var servicesUrl = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_POST");
            var campusId = ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE_EASTGATE");
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan
            var userId = ConfigurationManager.AppSettings.Get("USER_SUPPORT");

            // Clean the table of before inserting...
            this.DeleteInsertedFieldFromPriorWorkTable(leadId);

            // Insert a record with Address
            this.InsertAPriorWorkItemWithContactAndAddress(leadId);
            var priorId = this.InsertAPriorWorkItemWithContactAndAddress(leadId);

            // Create the record
            var mess = PriorWorkOutputModel.Factory();
            mess.InputModel.CampusId = campusId;
            mess.InputModel.LeadId = leadId;
            mess.InputModel.UserId = userId;
            mess.InputModel.Command = 3;
            mess.InputModel.PriorWorkId = priorId;
            mess.AdSchoolJobTitlesItemList = null;
            mess.JobStatusItemList = null;

            var pw = LeadPriorWorkTester.MinimaPriorWorkItemOutputModel(priorId);
            var address = LeadPriorWorkTester.CreateInternationalPriorWorkAddressOutputModel(userId);
            pw.AddressOutputModel = address;

            mess.PriorWorkItemList.Add(pw as PriorWorkItemOutputModel);

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", this.apiKey);
            var task = client.PostAsJsonAsync(servicesUrl, mess).ContinueWith(
                taskwithmsg =>
                {
                    var response = taskwithmsg.Result;
                    Console.WriteLine(response.ToString());
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    var hasChildren = this.ExistsPriorWorkAddressAssociated(priorId);
                    Assert.IsTrue(hasChildren, "The Address was deleted and should not!");
                    return response;
                });
            task.Wait();
        }

        /// <summary>
        ///  The post service minimal command 1 test.
        /// Case 2: SERVER has address CLIENT returned item does NOT have
        /// Only the required fields
        /// </summary>
        [Test]
        public void PostUpdateWidthNoAddressCommand3Test()
        {
            var servicesUrl = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_POST");
            var campusId = ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE_EASTGATE");
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan
            var userId = ConfigurationManager.AppSettings.Get("USER_SUPPORT");

            // Clean the table of before inserting...
            this.DeleteInsertedFieldFromPriorWorkTable(leadId);

            // Insert a record with Address
            this.InsertAPriorWorkItemWithContactAndAddress(leadId);
            var priorId = this.InsertAPriorWorkItemWithContactAndAddress(leadId);

            // Create the record
            var mess = PriorWorkOutputModel.Factory();
            mess.InputModel.CampusId = campusId;
            mess.InputModel.LeadId = leadId;
            mess.InputModel.UserId = userId;
            mess.InputModel.Command = 3;
            mess.InputModel.PriorWorkId = priorId;
            mess.AdSchoolJobTitlesItemList = null;
            mess.JobStatusItemList = null;

            var pw = LeadPriorWorkTester.MinimaPriorWorkItemOutputModel(priorId);
            var address = LeadPriorWorkTester.CreateCompleteUsaPriorWorkAddressOutputModel(userId);
            address.Address1 = string.Empty; // Address 1 empty mining that the address is not more valid
            pw.AddressOutputModel = address;

            mess.PriorWorkItemList.Add(pw as PriorWorkItemOutputModel);

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", this.apiKey);
            var task = client.PostAsJsonAsync(servicesUrl, mess).ContinueWith(
                taskwithmsg =>
                {
                    var response = taskwithmsg.Result;
                    Console.WriteLine(response.ToString());
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    var hasChildren = this.ExistsPriorWorkAddressAssociated(priorId);
                    Assert.IsFalse(hasChildren, "The Address was not deleted");
                    return response;
                });
            task.Wait();
        }

        /// <summary>
        ///  The post service minimal command 1 test.
        /// Case 3: SERVER NO has Address CLIENT returned also NOT
        /// (particular case Address1 = string.Empty)
        /// Only the required fields
        /// </summary>
        [Test]
        public void PostUpdateNoAddressOriginalNoAddressCommand3Test()
        {
            var servicesUrl = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_POST");
            var campusId = ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE_EASTGATE");
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan
            var userId = ConfigurationManager.AppSettings.Get("USER_SUPPORT");

            // Clean the table of before inserting...
            this.DeleteInsertedFieldFromPriorWorkTable(leadId);

            // Insert a record with Address
            var priorId = this.InsertAPriorWorkItemWithOutContactAndAddress(leadId);

            // Create the record
            var mess = PriorWorkOutputModel.Factory();
            mess.InputModel.CampusId = campusId;
            mess.InputModel.LeadId = leadId;
            mess.InputModel.UserId = userId;
            mess.InputModel.Command = 3;
            mess.InputModel.PriorWorkId = priorId;
            mess.AdSchoolJobTitlesItemList = null;
            mess.JobStatusItemList = null;

            var pw = LeadPriorWorkTester.MinimaPriorWorkItemOutputModel(priorId);
            var address = LeadPriorWorkTester.CreateCompleteUsaPriorWorkAddressOutputModel(userId);
            address.Address1 = string.Empty; // Clear the string empty 
            pw.AddressOutputModel = address;

            mess.PriorWorkItemList.Add(pw as PriorWorkItemOutputModel);

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", this.apiKey);
            var task = client.PostAsJsonAsync(servicesUrl, mess).ContinueWith(
                taskwithmsg =>
                {
                    var response = taskwithmsg.Result;
                    Console.WriteLine(response.ToString());
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    var hasChildren = this.ExistsPriorWorkAddressAssociated(priorId);
                    Assert.IsFalse(hasChildren, "An Address should not exists");
                    return response;
                });
            task.Wait();
        }

        /// <summary>
        ///  The post service minimal command 1 test.
        /// Case 4: Actual no have Address returned has a new
        /// Only the required fields
        /// </summary>
        [Test]
        public void PostUpdateWidthAddressButOriginalNotCommand3Test()
        {
            var servicesUrl = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_PRIORWORK_COMMAND_POST");
            var campusId = ConfigurationManager.AppSettings.Get("CAMPUS_CHATTANOOGA_COLLEGE_EASTGATE");
            var leadId = "2F353435-4E39-4600-BEDD-B1BEABA3C762"; // John Grant Bryan
            var userId = ConfigurationManager.AppSettings.Get("USER_SUPPORT");

            // Clean the table of before inserting...
            this.DeleteInsertedFieldFromPriorWorkTable(leadId);

            // Insert a record with Address
            var priorId = this.InsertAPriorWorkItemWithOutContactAndAddress(leadId);

            // Create the record
            var mess = PriorWorkOutputModel.Factory();
            mess.InputModel.CampusId = campusId;
            mess.InputModel.LeadId = leadId;
            mess.InputModel.UserId = userId;
            mess.InputModel.Command = 3;
            mess.InputModel.PriorWorkId = priorId;
            mess.AdSchoolJobTitlesItemList = null;
            mess.JobStatusItemList = null;

            var pw = LeadPriorWorkTester.MinimaPriorWorkItemOutputModel(priorId);
            var address = LeadPriorWorkTester.CreateCompleteUsaPriorWorkAddressOutputModel(userId);
            pw.AddressOutputModel = address;

            mess.PriorWorkItemList.Add(pw as PriorWorkItemOutputModel);

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", this.apiKey);
            var task = client.PostAsJsonAsync(servicesUrl, mess).ContinueWith(
                taskwithmsg =>
                {
                    var response = taskwithmsg.Result;
                    Console.WriteLine(response.ToString());
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                    var hasChildren = this.ExistsPriorWorkAddressAssociated(priorId);
                    Assert.IsTrue(hasChildren, "The Address was not Added");
                    return response;
                });
            task.Wait();
        }
        #endregion

        #region Refactoring

        /// <summary>
        ///  The create prior work address output model.
        /// </summary>
        /// <param name="userId">
        ///  The user ID.
        /// </param>
        /// <returns>
        /// The <see cref="PriorWorkAddressOutputModel"/>.
        /// </returns>
        private static PriorWorkAddressOutputModel CreateInternationalPriorWorkAddressOutputModel(string userId)
        {
            var address = new PriorWorkAddressOutputModel
            {
                Address1 = "InternationalAddress1",
                Address2 = "InternationalAddress2",
                City = "InternationalCity",
                Country = null,
                StateInternational = "International Address",
                IsInternational = true,
                Moduser = userId,
                ModDate = DateTime.Now,
                State = null,
                ZipCode = "11111111",
                AddressApto = "111"
            };
            return address;
        }

        /// <summary>
        ///  The create complete USA prior work address output model.
        /// </summary>
        /// <param name="userId">
        ///  The user ID.
        /// </param>
        /// <returns>
        /// The <see cref="PriorWorkAddressOutputModel"/>.
        /// </returns>
        private static PriorWorkAddressOutputModel CreateCompleteUsaPriorWorkAddressOutputModel(string userId)
        {
            var address = new PriorWorkAddressOutputModel
            {
                Address1 = "UsaCompleteAddres1",
                Address2 = "UsaCompleteAddress2",
                City = "UsaCompleteCity",
                Country =
                                      new DropDownOutputModel()
                                      {
                                          ID = "D73BFC65-DB8C-48AF-8780-583BF9FAE42E",
                                          Description = "United States"
                                      },
                IsInternational = false,
                AddressApto = "UsaComplete",
                StateInternational = string.Empty,
                Moduser = userId,
                ModDate = DateTime.Now,
                State =
                                      new DropDownOutputModel()
                                      {
                                          ID = "5F73028F-8CC4-40C1-9AC2-9FB709E790B7",
                                          Description = "Florida"
                                      },
                ZipCode = "22222"
            };
            address.AddressApto = "222";
            return address;
        }

        /// <summary>
        /// The minimal prior work item output model.
        /// </summary>
        /// <param name="priorId">
        /// The prior Id. Optional parameter
        /// </param>
        /// <returns>
        /// The <see cref="IPriorWorkItemOutputModel"/>.
        /// </returns>
        private static IPriorWorkItemOutputModel MinimaPriorWorkItemOutputModel(string priorId = null)
        {
            var pw = PriorWorkItemOutputModel.Factory();
            pw.Employer = "EmployerTestingModel";
            pw.JobTitle = "EmployerTestingTitle";
            pw.Id = priorId;
            return pw;
        }

        #endregion

        #region Private infrastructure Methods
        /// <summary>
        ///  The delete inserted field from prior work table.
        /// </summary>
        /// <param name="leadId">
        ///  The lead ID.
        /// </param>
        private void DeleteInsertedFieldFromPriorWorkTable(string leadId)
        {
            var connStr = ConfigurationManager.AppSettings.Get("ADVANTAGE_CONNECTION_STRING");
            var conn = new SqlConnection(connStr);
            var command = new SqlCommand { CommandText = "DELETE FROM dbo.adLeadEmployment WHERE LeadId = @LeadId" };
            command.Parameters.AddWithValue("@LeadId", leadId);
            command.Connection = conn;
            conn.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        ///  The insert a prior work item with contact and address.
        /// </summary>
        /// <param name="leadId">
        ///  The lead ID.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>. Id of prior Work inserted
        /// </returns>
        private string InsertAPriorWorkItemWithContactAndAddress(string leadId)
        {
            var newIdent = Guid.NewGuid().ToString();
            var sqlString =
                "         IF NOT EXISTS(SELECT[StEmploymentId]                          "
                + "             FROM    dbo.adLeadEmployment                              "
                + "             WHERE   Comments = 'TEST FOR PRIOR WORK DO NOT DELETE')   "
                + " BEGIN                                                                 "
                + "                                                          "
                + "     BEGIN TRANSACTION InsertPriorWork;                   "
                + "                                                          "
                + "         BEGIN TRY                                        "
                + "                                                          "
                + "         INSERT INTO dbo.adLeadEmployment                 "
                + "                (StEmploymentId                           "
                + "                , LeadId                                  "
                + "                , JobTitleId                              "
                + "                , JobStatusId                             "
                + "                , StartDate                               "
                + "                , EndDate                                 "
                + "                , Comments                                "
                + "                , ModUser                                 "
                + "                , ModDate                                 "
                + "                , JobResponsibilities                     "
                + "                , EmployerName                            "
                + "                , EmployerJobTitle                        "
                + "                )                                         "
                + "         VALUES(@NewIdent                                 "
                + "                 , @LeadId                                "
                + "                 , 'FEA0C1C9-85BB-4707-AA64-ABAC5015B6B4' "
                + "                 , 'FD6E4CD5-0B1B-413A-93EC-7EC67D715F6A' "
                + "                 , GETDATE()                              "
                + "                 , GETDATE()                              "
                + "                 , 'TEST FOR PRIOR WORK DO NOT DELETE'    "
                + "                 , 'SUPPORT'               "
                + "                 , GETDATE()               "
                + "                 , 'None'                  "
                + "                 , 'Billy the kid'         "
                + "                 , 'Cowboy'                "
                + "                 );                        "
                + "                                           "
                + "        INSERT  INTO dbo.PriorWorkAddress  "
                + "                (StEmploymentId            "
                + "                , Address1                 "
                + "                , Apartment                "
                + "                , Address2                 "
                + "                , City                     "
                + "                , IsInternational          "
                + "                , StateInternational       "
                + "                , StateId                  "
                + "                , ZipCode                  "
                + "                , CountryId                "
                + "                , ModUser                  "
                + "                , ModDate                  "
                + "                )                          "
                + "         VALUES(@NewIdent                  "
                + "                 , N'The Rodeo'            "
                + "                 , N'234'                  "
                + "                 , N'The malaquita'        "
                + "                 , N'Silver City'          "
                + "                 , 0                       "
                + "                 , N''                     "
                + "                 , NULL                    "
                + "                 , N'34567'                "
                + "                 , NULL                    "
                + "                 , N'SUPPORT'              "
                + "                 , GETDATE()               "
                + "                 );                        "
                + "         INSERT INTO dbo.PriorWorkContact  "
                + "                (StEmploymentId            "
                + "                , JobTitle                 "
                + "                , FirstName                "
                + "                , MiddleName               "
                + "                , LastName                 "
                + "                , PrefixId                 "
                + "                , Phone                    "
                + "                , Email                    "
                + "                , IsActive                 "
                + "                , ModUser                  "
                + "                , ModDate                  "
                + "                , IsDefault                "
                + "                )                          "
                + "         VALUES(@NewIdent                  "
                + "                 , N'Capataz'              "
                + "                 , N'Rapido'                         "
                + "                 , N'P'                          "
                + "                  , N'Perez'                         "
                + "                 , '36F99A05-DE14-4A6D-94EC-0B8C99082C82' "
                + "                 , N''                               "
                + "                 , N'rapido@hotmail.com'             "
                + "                 , 1                                 "
                + "                 , N'SUPPORT'                     "
                + "                 , GETDATE()                         "
                + "                 , 1                                 "
                + "                 );                                  "
                + "                                                     "
                + "         COMMIT TRANSACTION InsertPriorWork;         "
                + "         END TRY                                     "
                + "     BEGIN CATCH                                     "
                + "         ROLLBACK TRANSACTION InsertPriorWork;       "
                + "         END CATCH;                                               "
                + "         END;                                                                 ";
            var connStr = ConfigurationManager.AppSettings.Get("ADVANTAGE_CONNECTION_STRING");
            var conn = new SqlConnection(connStr);
            var command = new SqlCommand() { CommandText = sqlString, Connection = conn };
            command.Parameters.AddWithValue("@LeadId", leadId);
            command.Parameters.AddWithValue("@NewIdent", newIdent);
            conn.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }

            var sql =
                "SELECT StEmploymentId FROM dbo.adLeadEmployment WHERE Comments ='TEST FOR PRIOR WORK DO NOT DELETE'";
            var command2 = new SqlCommand() { CommandText = sql, Connection = conn };
            conn.Open();
            try
            {
                var output = command2.ExecuteScalar();
                return output.ToString();
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        ///  The insert a prior work item with out contact and address.
        /// </summary>
        /// <param name="leadId">
        ///  The lead ID.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string InsertAPriorWorkItemWithOutContactAndAddress(string leadId)
        {
            var newIdent = Guid.NewGuid().ToString();
            var sqlString =
                "         IF NOT EXISTS(SELECT[StEmploymentId]                          "
                + "             FROM    dbo.adLeadEmployment                              "
                + "             WHERE   Comments = 'TEST FOR PRIOR WORK DO NOT DELETE')   "
                + " BEGIN                                                                 "
                + "         INSERT INTO dbo.adLeadEmployment                 "
                + "                (StEmploymentId                           "
                + "                , LeadId                                  "
                + "                , JobTitleId                              "
                + "                , JobStatusId                             "
                + "                , StartDate                               "
                + "                , EndDate                                 "
                + "                , Comments                                "
                + "                , ModUser                                 "
                + "                , ModDate                                 "
                + "                , JobResponsibilities                     "
                + "                , EmployerName                            "
                + "                , EmployerJobTitle                        "
                + "                )                                         "
                + "         VALUES(@NewIdent                                 "
                + "                 , @LeadId                                "
                + "                 , 'FEA0C1C9-85BB-4707-AA64-ABAC5015B6B4' "
                + "                 , 'FD6E4CD5-0B1B-413A-93EC-7EC67D715F6A' "
                + "                 , GETDATE()                              "
                + "                 , GETDATE()                              "
                + "                 , 'TEST FOR PRIOR WORK DO NOT DELETE'    "
                + "                 , 'SUPPORT'               "
                + "                 , GETDATE()               "
                + "                 , 'None'                  "
                + "                 , 'Billy the kid'         "
                + "                 , 'Cowboy'                "
                + "                 );                        "
                + "         END;                              ";
            var connStr = ConfigurationManager.AppSettings.Get("ADVANTAGE_CONNECTION_STRING");
            var conn = new SqlConnection(connStr);
            var command = new SqlCommand() { CommandText = sqlString, Connection = conn };
            command.Parameters.AddWithValue("@LeadId", leadId);
            command.Parameters.AddWithValue("@NewIdent", newIdent);
            conn.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }

            var sql =
                "SELECT StEmploymentId FROM dbo.adLeadEmployment WHERE Comments ='TEST FOR PRIOR WORK DO NOT DELETE'";
            var command2 = new SqlCommand() { CommandText = sql, Connection = conn };
            conn.Open();
            try
            {
                var output = command2.ExecuteScalar();
                return output.ToString();
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        ///  The exists prior work address associated.
        /// </summary>
        /// <param name="priorId">
        ///  The prior ID.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool ExistsPriorWorkAddressAssociated(string priorId)
        {
            var sql = " SELECT *                                                              "
                      + " FROM    dbo.adLeadEmployment al                                       "
                      + " JOIN dbo.PriorWorkAddress pw ON pw.StEmploymentId = al.StEmploymentId "
                      + " WHERE al.StEmploymentId = @PriorId                                    ";

            var connStr = ConfigurationManager.AppSettings.Get("ADVANTAGE_CONNECTION_STRING");
            var conn = new SqlConnection(connStr);
            var command = new SqlCommand { CommandText = sql };
            command.Parameters.AddWithValue("@PriorId", priorId);
            command.Connection = conn;
            conn.Open();
            try
            {
                var ret = command.ExecuteScalar();
                return ret != null;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion
    }
}
