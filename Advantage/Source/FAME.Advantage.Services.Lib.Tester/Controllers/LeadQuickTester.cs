﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadQuickTester.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   The lead quick tester.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Tester.Controllers
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Net.Http;
    using System.Web.Script.Serialization;

    using NUnit.Framework;

    /// <summary>
    /// The drop down list value.
    /// </summary>
    public class DdlValue
    {
        /// <summary>
        /// Gets or sets the field name.
        /// </summary>
        public string FldName { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public string CampusId { get; set; }

        /// <summary>
        /// Gets or sets the additional filter.
        /// </summary>
        public string AdditionalFilter { get; set; }
    }

    /// <summary>
    /// The quick lead input.
    /// </summary>
    public class QuickLeadInput
    {
        /// <summary>
        /// The control name.
        /// </summary>
        public string CtrlName;

        /// <summary>
        /// The input.
        /// </summary>
        public DdlValue Input;
    }

    /// <summary>
    /// The lead quick tester.
    /// </summary>
    [TestFixture]
    public class LeadQuickTester
    {
        /// <summary>
        /// The API key.
        /// </summary>
        private readonly string apiKey;

        /// <summary>
        /// The service layer get.
        /// </summary>
        private readonly string serviceLayerGet;
        private readonly string serviceLayerGetDdl;

        /// <summary>
        /// The service layer get school defined field.
        /// </summary>
        private readonly string serviceLayerGetSdf;

        /// <summary>
        /// The service layer get lead group.
        /// </summary>
        private readonly string serviceLayerGetLeadGrp;

        /// <summary>
        /// The service layer get state.
        /// </summary>
        private readonly string serviceLayerGetState;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadQuickTester"/> class.
        /// </summary>
        public LeadQuickTester()
        {
            this.serviceLayerGet = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_QUICK_FIELDS_GET");
            this.serviceLayerGetDdl = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_QUICK_DDL_GET");
            this.serviceLayerGetSdf = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_QUICK_SDF_GET");
            this.serviceLayerGetLeadGrp = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_QUICK_LEADGRP_GET");
            this.serviceLayerGetState = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_QUICK_State_GET");
            this.apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }

        /// <summary>
        /// The get command 1 test.
        /// </summary>
        [Test]
        public void GetCommand1Test()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?Command=1", this.serviceLayerGet);

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// Testing if we get the data source of the state dropdown.
        /// </summary>
        [Test]
        public void GetQuickLeadStateValuesTest()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?CtrlName={1}", this.serviceLayerGetState, "StateId");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// Testing if we get the data source of the Driver License state dropdown.
        /// </summary>
        [Test]
        public void GetQuickLeadDrivStateValuesTest()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?CtrlName={1}", this.serviceLayerGetState, "DrvLicStateCode");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// Testing if we get the data source of the dropdown.
        /// </summary>
        [Test]
        public void GetQuickLeadDropDownValuesAdminRepTest()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var obj = new QuickLeadInput
            {
                CtrlName = "AdmissionRepId",
                Input = new DdlValue
                {
                    FldName = "AdmissionsRep",
                    CampusId = "3F5E839A-589A-4B2A-B258-35A1A8B3B819"
                }
            };
            var json = new JavaScriptSerializer().Serialize(obj);
            var query = string.Format("{0}?filter={1}", this.serviceLayerGetDdl, json);

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }
       
        /// <summary>
        /// Test to get the school /custom define field controls details.
        /// </summary>
        [Test]
        public void GetSdfListTest()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?PageResourceId={1}&CampusId={2}", this.serviceLayerGetSdf, "170", "3F5E839A-589A-4B2A-B258-35A1A8B3B819");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// Test to get the LeadGroup controls details.
        /// </summary>
        [Test]
        public void GetLeadGroupInformationTest()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?PageResourceId={1}&CampusId={2}", this.serviceLayerGetLeadGrp, "170", "3F5E839A-589A-4B2A-B258-35A1A8B3B819");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }
    }
}
