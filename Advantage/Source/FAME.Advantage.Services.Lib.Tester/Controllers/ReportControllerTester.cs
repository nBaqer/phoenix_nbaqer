﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using NUnit.Framework;

namespace FAME.Advantage.Services.Lib.Tester.Controllers
{
    [TestFixture]
    public class ReportControllerTester
    {
        private readonly string apiKey;
        private readonly string serviceLayerAddress;

        public ReportControllerTester()
        {
            serviceLayerAddress = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_REPORT");
            apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }


        /// <summary>
        /// Prepared for MAU_3.7.3
        /// </summary>
        [Test]
        public void GetAppSettings()
        {
            string apikey = apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?Command={1}&Directory={2}"
                , serviceLayerAddress + "/GET"
                , "2"
                , "");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

      
    }
}
