﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using NUnit.Framework;

namespace FAME.Advantage.Services.Lib.Tester.Controllers
{
    [TestFixture]
    public class ResourcesControllerTester
    {
        ///// <summary>
        ///// This constants is different to the constant used by WAPI
        ///// and it is only used to test purposes.
        ///// </summary>
        //private readonly string apiKey;
        //private readonly string serviceLayerResourcesGet;
    

        //public ResourcesControllerTester()
        //{
        //    serviceLayerResourcesGet = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_RESOURCES_CAPTION_REQUIRED_GET");
        //     apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        //} 
        
        //[Test]
        //public void GetAllResourcesForPage170()
        //{
        //    // performance measurement
        //    var sw = new Stopwatch();
        //    sw.Start();
        //    string apikey = apiKey;
        //    var query = string.Format("{0}?PageResourceId={1}&CodeLang={2}", serviceLayerResourcesGet, "170", "EN-US");
        //    var client = new HttpClient();
        //    var request = new HttpRequestMessage
        //    {
        //        RequestUri = new Uri(query),
        //        Method = HttpMethod.Get,

        //    };

        //    request.Headers.Add("AuthKey", apikey);
        //    var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
        //    {
        //        var response = taskwithmsg.Result;
        //        Console.WriteLine(response.ToString());
        //        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
        //        sw.Stop(); // <- comment this in release
        //        Console.WriteLine("Elapsed={0}", sw.Elapsed);
        //        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        //    });
        //    task.Wait();
        //}

        //[Test]
        //public void GetAllResourcesForPage206()
        //{
        //    string apikey = apiKey;
        //    var query = string.Format("{0}?PageResourceId={1}&CodeLang={2}", serviceLayerResourcesGet, "206", "EN-US");
        //    var client = new HttpClient();
        //    var request = new HttpRequestMessage
        //    {
        //        RequestUri = new Uri(query),
        //        Method = HttpMethod.Get,

        //    };

        //    request.Headers.Add("AuthKey", apikey);
        //    var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
        //    {
        //        var response = taskwithmsg.Result;
        //        Console.WriteLine(response.ToString());
        //        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
        //        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        //    });
        //    task.Wait();
        //}


    }


}
