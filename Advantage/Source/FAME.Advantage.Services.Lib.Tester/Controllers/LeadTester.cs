﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadTester.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Defines the LeadTester type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Tester.Controllers
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Net.Http;

    using NUnit.Framework;

    /// <summary>
    ///  The lead tester.
    /// </summary>
    [TestFixture]
    public class LeadTester
    {
        /// <summary>
        /// This constants is different to the constant used by WAPI
        /// and it is only used to test purposes.
        /// </summary>
        private readonly string apiKey;

        /// <summary>
        /// The service layer lead info bar get.
        /// </summary>
        private readonly string serviceLayerLeadInfobarGet;
        private readonly string serviceLayerLeadStatusReq;
        private readonly string serviceLayerLeadGroup;
        private readonly string serviceLayerLeadResources;
        private readonly string serviceLayerLeadDemographic;
        private readonly string serviceLayerLeadAcademics;
        private readonly string serviceLayerIsLeadErasable;

       public LeadTester()
        {
            this.serviceLayerLeadInfobarGet = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_INFOBAR_COMMAND_GET");
            this.serviceLayerLeadStatusReq = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_INFOBAR_GETREQ");
            this.serviceLayerLeadGroup = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEADGROUPS_COMMAND_GET");
            this.serviceLayerLeadResources = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_INFO_RESOURCES_GET");
            this.serviceLayerLeadDemographic = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_DEMOGRAPHIC_COMMAND_GET");
            this.serviceLayerLeadAcademics = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_ITEMS_COMMAND_GET");
            this.serviceLayerIsLeadErasable = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_LEAD_IS_ERASABLE_COMMAND_GET");
            this.apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }

        /// <summary>
        /// The get lead info bar.
        /// </summary>
        [Test]
        public void GetLeadInfoBar()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format(
                "{0}?LeadGuid={1}&UserId={2}",
                 this.serviceLayerLeadInfobarGet,
                 "E14789A9-0B30-44D8-9FCA-000AA4ABC3CD",
                 "440174AD-6628-4A6A-B6A0-B4994E4F7B93");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        ///  The get lead info bar required step.
        /// </summary>
        [Test]
        public void GetLeadInfoBarRequiredStep()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?LeadGuid={1}", this.serviceLayerLeadStatusReq, "E14789A9-0B30-44D8-9FCA-000AA4ABC3CD");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            });
            task.Wait();
        }

        [Test]
        public void GetAllLeadGroup()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?LeadId={1}&CampusId={2}", this.serviceLayerLeadGroup, "0", "0");
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            });
            task.Wait();
        }

        [Test]
        public void TestIsLeadErasable()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?LeadId={1}", this.serviceLayerIsLeadErasable, "DA4BC7E6-6567-4B17-82B9-97B26F460B10");
            var request = new HttpRequestMessage
            {

                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        [Test]
        public void GetAllResourcesForLeadInfoPage()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?PageResourceId={1}&CampusId={2}", this.serviceLayerLeadResources, "170", "3F5E839A-589A-4B2A-B258-35A1A8B3B819");
            var request = new HttpRequestMessage
            {

                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }


        // [Test]
        // public void GetAllLeadSdfForALead()
        // {
        // string apikey = apiKey;
        // var client = new HttpClient();
        // var query = string.Format("{0}?LeadId={1}", serviceLayerLeadSdf, "2787a4c7-c899-434f-993a-0b9c871a6931");
        // var request = new HttpRequestMessage
        // {

        // RequestUri = new Uri(query),
        // Method = HttpMethod.Get,

        // };

        // request.Headers.Add("AuthKey", apikey);
        // var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
        // {
        // var response = taskwithmsg.Result;
        // Console.WriteLine(response.ToString());
        // Console.WriteLine(response.Content.ReadAsStringAsync().Result);
        // Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        // });
        // task.Wait();
        // }
        [Test]
        public void GetInfoPageDemographicForALead()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
          
            var query = string.Format("{0}?LeadId={1}&UserId={2}"
           , this.serviceLayerLeadDemographic
           , "E14789A9-0B30-44D8-9FCA-000AA4ABC3CD"
           , "440174AD-6628-4A6A-B6A0-B4994E4F7B93");

            var request = new HttpRequestMessage
            {

                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        [Test]
        public void GetInfoPageAcademicsForALead()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?LeadId={1}", this.serviceLayerLeadAcademics, "25952556-5E8E-4F7E-860A-F440223E07D2");
            var request = new HttpRequestMessage
            {

                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        
    }
}
