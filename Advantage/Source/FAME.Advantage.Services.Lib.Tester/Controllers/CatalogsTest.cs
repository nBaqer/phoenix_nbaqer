﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using NUnit.Framework;

namespace FAME.Advantage.Services.Lib.Tester.Controllers
{
    [TestFixture]
    public class CatalogsTest
    {
        /// <summary>
        /// This constants is different to the constant used by WAPI
        /// and it is only used to test purposes.
        /// </summary>
        private readonly string apiKey;
        private readonly string serviceLayerLeadInfobarGet;

        public CatalogsTest()
        {
            serviceLayerLeadInfobarGet = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_CATALOGS_COMMAND_GET");
            apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }

        /// <summary>
        /// FldName is case sensitive
        /// </summary>
        [Test]
        public void GetCatalogProgramID()
        {
            string apikey = apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?FldName={1}", serviceLayerLeadInfobarGet, "ProgramID");

            var request = new HttpRequestMessage
            {

                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// Program with calendar type
        /// </summary>
        [Test]
        public void GetCatalogProgram()
        {
            string apikey = apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?FldName={1}", serviceLayerLeadInfobarGet, "Program");

            var request = new HttpRequestMessage
            {

                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        [Test]
        public void GetCatalogProgramIDWidthCampusAndAdditionalFilter()
        {
            string apikey = apiKey;
            var client = new HttpClient();
            const string CAMPUS_ID = "3F5E839A-589A-4B2A-B258-35A1A8B3B819"; // specific for IDTI -DEV 3.8
                const string PROGRAMGRPID = "3ED43B22-60E2-42A9-AB50-42D324BA5EFE";
                var query = string.Format("{0}?FldName={1}&CampusId={2}&AdditionalFilter={3}", serviceLayerLeadInfobarGet, "ProgramID", CAMPUS_ID, PROGRAMGRPID);

            var request = new HttpRequestMessage
            {

                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }


        

        /// <summary>
        /// FldName is case sensitive
        /// </summary>
        [Test]
        public void GetProgramScheduleCatalogWithAditionalFilter()
        {
            string apikey = apiKey;
            const string PROGRAM_VERSION_KEY = "6ED5536A-0257-4EB3-A638-C695394322F0";
            var client = new HttpClient();
            var query = string.Format("{0}?FldName={1}&AdditionalFilter={2}", serviceLayerLeadInfobarGet, "ProgramScheduleId", PROGRAM_VERSION_KEY);

            var request = new HttpRequestMessage
            {

                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// FldName is case sensitive
        /// </summary>
        [Test]
        public void GetCatalogExpectedStart()
        {
            string apikey = apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?FldName={1}", serviceLayerLeadInfobarGet, "ExpectedStart");

            var request = new HttpRequestMessage
            {

                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// FldName is case sensitive
        /// </summary>
        [Test]
        public void GetCatalogExpectedStartFilterByCampus()
        {
            string apikey = apiKey;
            const string CAMPUS_ID = "3F5E839A-589A-4B2A-B258-35A1A8B3B819"; // specific for IDTI -DEV 3.8
            var client = new HttpClient();
            var query = string.Format("{0}?FldName={1}&CampusId={2}", serviceLayerLeadInfobarGet, "ExpectedStart", CAMPUS_ID);

            var request = new HttpRequestMessage
            {

                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// FldName is case sensitive
        /// </summary>
        [Test]
        public void GetCatalogExpectedStartFilterByCampusAndProgram()
        {
            string apikey = apiKey;
            const string CAMPUS_ID = "3F5E839A-589A-4B2A-B258-35A1A8B3B819"; // specific for IDTI -DEV 3.8
            const string PROGRAM_KEY = "271807AF-12C2-4D8A-8E03-8754726B469E";
            var client = new HttpClient();
            var query = string.Format("{0}?FldName={1}&CampusId={2}&AdditionalFilter={3}", serviceLayerLeadInfobarGet, "ExpectedStart", CAMPUS_ID, PROGRAM_KEY);

            var request = new HttpRequestMessage
            {

                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }
    }
}
