﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Messages.SystemStuff.Maintenance.Smtp;
using Newtonsoft.Json;
using NUnit.Framework;

namespace FAME.Advantage.Services.Lib.Tester.Controllers
{
    [TestFixture]
    public class SmtpSendEmailControllerTester
    {
         private readonly string apiKey;
         private readonly string serviceLayerAddress;

         public SmtpSendEmailControllerTester()
        {
            serviceLayerAddress = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_SMTP_EMAIL");
            apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }

         [Test]
         public void SendMessageBySmtp()
         {
             string apikey = apiKey;


             var message = new SmtpSendMessageInputModel();

             var mess = "<div style= \"background-color:red;\"> Test Message</div>";
             var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(mess);
             message.Body64 = Convert.ToBase64String(plainTextBytes);
             message.From = "test@hotmail.com";
             message.Subject = "Test Message";
             message.Cc = string.Empty;
             message.Command = 1;
             message.To = "jguirado@fameinc.com";


             var servicesUrl = serviceLayerAddress + "/POST";

             var client = new HttpClient();
             client.DefaultRequestHeaders.Add("AuthKey", apikey);
             var task = client.PostAsJsonAsync(servicesUrl, message).ContinueWith(taskwithmsg =>
             {
                 var response = taskwithmsg.Result;
                 Console.WriteLine(response.ToString());
                 Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                 Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                 return response;
             });
             task.Wait();
         }

         [Test]
         public void SendMessageByOAuthSmtp()
         {
             string apikey = apiKey;


             var message = new SmtpSendMessageInputModel();

             var mess = "<div style= \"background-color:red;\"> Test Message</div>";
             var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(mess);
             message.Body64 = Convert.ToBase64String(plainTextBytes);
             message.From = "jguirado@fameinc.com";
             message.Subject = "Test Message";
             message.Cc = string.Empty;
             message.Command = 1;
             message.To = "freedeveloper@hotmail.com";


             var servicesUrl = serviceLayerAddress + "/POST";

             var client = new HttpClient();
             client.DefaultRequestHeaders.Add("AuthKey", apikey);
             var task = client.PostAsJsonAsync(servicesUrl, message).ContinueWith(taskwithmsg =>
             {
                 var response = taskwithmsg.Result;
                 Console.WriteLine(response.ToString());
                 Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                 Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                 return response;
             });
             task.Wait();



         }





    }
}
