﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiWindowsServiceControllerTester.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The WapiWindowsServiceController module page test.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Tester.Controllers.Maintenance
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Net;
    using System.Net.Http;

    using NUnit.Framework;

    /// <summary>
    ///  The WapiWindowsServiceController module page test.
    /// </summary>
    [TestFixture]
    public class WapiWindowsServiceControllerTester
    {
        /// <summary>
        ///  The API key.
        /// </summary>
        private readonly string apiKey;

        /// <summary>
        ///  The service layer get.
        /// </summary>
        private readonly string serviceLayerGet;

        private readonly string serviceLayerPost;

        private readonly string serviceName;

        /// <summary>
        /// Initializes a new instance of the <see cref="WapiWindowsServiceControllerTester"/> class.
        /// </summary>
        public WapiWindowsServiceControllerTester()
        {
            this.serviceLayerGet = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_GET_WAPI_WINDOWS_SERVER");
            this.serviceLayerPost = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_POST_WAPI_WINDOWS_SERVER");
            this.apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
            this.serviceName = "WapiWindowsService";
        }

        /// <summary>
        ///  The get entities command 1.
        /// </summary>
        [Test]
        public void GetWindowsServiceStatus()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = $"{this.serviceLayerGet}?ServiceName={this.serviceName}";

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                var result = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        [Test]
        public void StartWindowsService()
        {
            string apikey = this.apiKey;
            var dict = new Dictionary<string, string> { { "ServiceName", this.serviceName }, { "Command", "start" } };

            var servicesUrl = this.serviceLayerPost;

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", apikey);
            var task = client.PostAsJsonAsync(servicesUrl, dict).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                return response;
            });
            task.Wait();
        }
    }
}
