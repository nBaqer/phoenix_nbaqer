﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SdfModulePageTest.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the SdfModulePageTest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Tester.Controllers.Maintenance
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Net.Http;

    using NUnit.Framework;

    /// <summary>
    ///  The SDF module page test.
    /// </summary>
    [TestFixture]
    public class SdfModulePageTest
    {
        /// <summary>
        ///  The API key.
        /// </summary>
        private readonly string apiKey;

        /// <summary>
        ///  The service layer get.
        /// </summary>
        private readonly string serviceLayerGet;

        /// <summary>
        /// Initializes a new instance of the <see cref="SdfModulePageTest"/> class.
        /// </summary>
        public SdfModulePageTest()
        {
            this.serviceLayerGet = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_MAINTENANCE_SDF_MODULE_PAGE_GET");
            this.apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }

        /// <summary>
        ///  The get entities command 1.
        /// </summary>
        [Test]
        public void GetEntitiesCommand1()
        {
            string apikey = this.apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?Command=1", this.serviceLayerGet);

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                var result = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        /// The get pages by entities command 2.
        /// </summary>
        [Test]
        public void GetPagesByEntitiesCommand2()
        {
            string apikey = this.apiKey;
            int leadEntity = 395;
            var client = new HttpClient();
            var query = string.Format("{0}?Command=2&LeadEntity={1}", this.serviceLayerGet, leadEntity);

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                var result = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

        /// <summary>
        ///  The get UDF lists command 3.
        /// </summary>
        [Test]
        public void GetUdfListsCommand3()
        {
            string apikey = this.apiKey;
            int pageResourceId = 170;

            var client = new HttpClient();
            var query = string.Format("{0}?Command=3&PageResourceId={1}", this.serviceLayerGet, pageResourceId);

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,
            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                var result = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }
    }
}
