﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SmtpControllerTester.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the SmtpControllerTester type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Tester.Controllers
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Net.Http;

    using NUnit.Framework;

    [TestFixture]
    public class SmtpControllerTester
    {
         private readonly string apiKey;
         private readonly string serviceLayerAddress;

        public SmtpControllerTester()
        {
            serviceLayerAddress = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_REPORT");
            apiKey = ConfigurationManager.AppSettings.Get("APIKEY");
        }


        /// <summary>
        /// Prepared for MAU_3.7.3
        /// </summary>
        [Test]
        public void GetSSrsListOfReport()
        {
            string apikey = apiKey;
            var client = new HttpClient();
            var query = string.Format("{0}?Command={1}"
                                        , serviceLayerAddress + "/GET"
                                        , "1");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(query),
                Method = HttpMethod.Get,

            };

            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ToString());
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            });
            task.Wait();
        }

      

    }
}
