﻿namespace FAME.Advantage.Services.Lib.Tester.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using FAME.Advantage.Domain.Campuses;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Users;
    using FAME.Advantage.Messages.Campuses;
    using FAME.Advantage.Messages.Common;
    using FAME.Advantage.Services.Lib.Controllers.Campuses;
    using FAME.Advantage.Services.Lib.Infrastructure;
    using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
    using FAME.Advantage.Services.Lib.Mappings.Common;
    using FAME.Advantage.Services.Lib.Tester.DomainWrap;
    using FAME.Advantage.Services.Lib.Tester.Infrastructure.Bootstrapping;

    using FluentAssertions;

    using log4net;

    using NUnit.Framework;

    using Rhino.Mocks;

    using StructureMap;

    [TestFixture]
    public class CampusesControllerTester
    {
        // UserID
        private Guid userId = new Guid("B174E031-B6F2-4099-AC90-29DB8775D979");

        // Define fake database.....
        private IList<FakeStatusOption> listofstatus = new List<FakeStatusOption>();
        private IList<FakeCampus> listofcampus = new List<FakeCampus>();
        private IList<FakeUser> listUser = new List<FakeUser>();
        private IList<FakeCampusGroup> listCampusGrp = new List<FakeCampusGroup>();


        private IRepository BuildFakeRepositoryInstance()
        {
            // Fill fake database....
            listofstatus = new List<FakeStatusOption>();
            listofstatus.Add(FakeStatusOption.Factory("Active", "A"));
            listofstatus.Add(FakeStatusOption.Factory("Inactive", "I"));

            listofcampus = new List<FakeCampus>();
            listofcampus.Add(FakeCampus.Factory("Delta Technical College - Horn Lake", listofstatus[0], null));
            listofcampus.Add(FakeCampus.Factory("Delta Force - Captain America", listofstatus[0], null));
            listofcampus.Add(FakeCampus.Factory("California Lives - Los siete enanitos", listofstatus[1], null));

            listCampusGrp = new List<FakeCampusGroup>();
            listCampusGrp.Add(FakeCampusGroup.Factory("CodeCampus", "Nuevos Horizontes", listofcampus, null));
            listCampusGrp.Add(FakeCampusGroup.Factory("CodeCampus2", "Nuevos Horizontes 2", listofcampus, null));

            listUser = new List<FakeUser>();
            listUser.Add(FakeUser.Factory("Pablo Neruda", listCampusGrp));
            listUser.Add(FakeUser.Factory("Cristobal Colon", listCampusGrp));

            // get the user id
            userId = listUser[0].ID;

            // Generate It!
            var fakeRepository = MockRepository.GenerateStrictMock<IRepository>();
            fakeRepository.Stub(x => x.Query<User>()).Return(new EnumerableQuery<User>(listUser));
            fakeRepository.Stub(x => x.Query<Campus>()).Return(new EnumerableQuery<Campus>(listofcampus));
            return fakeRepository;
        }

        /// <summary>
        /// Constructor. Initialize here...
        /// </summary>
        public CampusesControllerTester()
        {
            Bootstrapper.Initialize(new Container(
                x =>
                {
                    x.Scan(scan =>
                    {
                        scan.AssemblyContainingType<IMapperDefinition>();
                        scan.AddAllTypesOf<IMapperDefinition>();
                    });
                    x.For<ILog>().Use(LogManager.GetLogger(Globals.GlobalLoggerName));
                }
            ));

            Bootstrapper.Bootstrap();

            // Initialize your mapper object!! if you use one in the query to test 
            var mapp = new DropDownOutputMappers();
            //mapp.CreateDefinition();
        }

        [Test]
        public void ShoulReturnAListIfAllCampusesIfNotSet()
        {
            Bootstrapper.Bootstrap();

            Bootstrapper.Container.Configure(x => x.For<IRepository>().Use(BuildFakeRepositoryInstance()));
            var campusController = Bootstrapper.Container.GetInstance<CampusesController>();
            var filter = Bootstrapper.Container.GetInstance<CampusesInputModel>();
            filter.IncludeAll = true;
            var listcampuses = campusController.Get(filter).ToList();
            Assert.True(listcampuses.Count() == 3, "Fail: Should be three campus list");
            Assert.True(listcampuses[0].Description == "Delta Technical College - Horn Lake", "Fail, this is not the correct campus");
            Assert.True(listcampuses[1].Description == "Delta Force - Captain America", "Fail, this is not the correct campus");
            Assert.True(listcampuses[0].ID == listofcampus[0].ID.ToString(), "Fail, Incorrect ID");
            Assert.True(listcampuses[1].ID == listofcampus[1].ID.ToString(), "Fail, Incorrect ID");
            foreach (DropDownOutputModel odd in listcampuses)
            {
                Console.WriteLine("{0} - {1} ", odd.ID, odd.Description);
            }
        }

        [Test]
        public void GetShouldReturnErrorIfyouSendEmptyGuid()
        {
            Bootstrapper.Bootstrap();

            Bootstrapper.Container.Configure(x => x.For<IRepository>().Use(BuildFakeRepositoryInstance()));
            var campusController = Bootstrapper.Container.GetInstance<CampusesController>();
            var filter = Bootstrapper.Container.GetInstance<CampusesInputModel>();
            filter.UserId = new Guid("00000000-0000-0000-0000-000000000000");
            Action get = () => campusController.Get(filter);
            get.ShouldThrow<HttpBadRequestResponseException>();
        }

        [Test]
        public void GetShouldReturnCount0IfUserIdDoesnotHaveCampus()
        {
            Bootstrapper.Bootstrap();

            Bootstrapper.Container.Configure(x => x.For<IRepository>().Use(BuildFakeRepositoryInstance()));
            var campusController = Bootstrapper.Container.GetInstance<CampusesController>();
            var filter = Bootstrapper.Container.GetInstance<CampusesInputModel>();
            filter.UserId = new Guid("B9AB7C23-9AF5-47AF-A776-9E0B13C81CA3");
            var listcampuses = campusController.Get(filter);
            Assert.False(listcampuses.Count() != 0, "Fail: Should be a empty campus list");
        }

        [Test]
        public void GetShouldReturnTheFollowingCampus()
        {
            Bootstrapper.Bootstrap();

            Bootstrapper.Container.Configure(x => x.For<IRepository>().Use(BuildFakeRepositoryInstance()));
            var campusController = Bootstrapper.Container.GetInstance<CampusesController>();
            var filter = Bootstrapper.Container.GetInstance<CampusesInputModel>();
            filter.UserId = userId;
            var listcampuses = campusController.Get(filter).ToList();
            Assert.True(listcampuses.Count() == 2, "Fail: Should be two campus list");
            Assert.True(listcampuses[0].Description == "Delta Technical College - Horn Lake", "Fail, this is not the correct campus");
            Assert.True(listcampuses[1].Description == "Delta Force - Captain America", "Fail, this is not the correct campus");
            Assert.True(listcampuses[0].ID == listofcampus[0].ID.ToString(), "Fail, Incorrect ID");
            Assert.True(listcampuses[1].ID == listofcampus[1].ID.ToString(), "Fail, Incorrect ID");
            foreach (DropDownOutputModel odd in listcampuses)
            {
                Console.WriteLine("{0} - {1} ", odd.ID, odd.Description);
            }
        }
    }
}