﻿using FAME.Advantage.Domain.Infrastructure.ServiceLocation;
using FAME.Advantage.Services.Lib.Infrastructure;
using FAME.Advantage.Services.Lib.Infrastructure.ActionFilters;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
using FAME.Advantage.Services.Lib.Infrastructure.Services;
using FAME.Advantage.Services.Lib.Tester.Extensions;
using FluentAssertions;
using Microsoft.Practices.ServiceLocation;
using NUnit.Framework;
using Rhino.Mocks;
using StructureMap;
using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using FAME.Advantage.Services.Lib.Tester.Infrastructure.Bootstrapping;

namespace FAME.Advantage.Services.Lib.Tester.ActionFilters
{
    [TestFixture]
    public class AuthorizeAttributeTester
    {
        private const string ValidApiAuthenticationKey = "1C0EBB7D-30CD-4385-BC4A-2E2C5F5ACA7E";
        private const string InvalidApiAuthenticationKey = "SOME KEY";

        public AuthorizeAttributeTester()
        {
            Bootstrapper.Initialize(new Container(
                x =>
                {
                    x.For<ApiAuthenticationKeyLookupService>().Use(AuthorizeAttributeTester.GetStubForApiAuthenticationKeyLookupService());
                    x.For<IServiceLocator>().Use<StructureMapServiceLocator>();
                }));
            Bootstrapper.Bootstrap();
            ServiceLocator.SetLocatorProvider(Bootstrapper.Container.GetInstance<IServiceLocator>);
        }

        [Test]
        public void OnActionExecuting_should_return_correctly()
        {
            var authorizeActionFilter = new AuthorizeAttribute();
            var httpActionContext = BuildValidHttpActionContext();

            authorizeActionFilter.OnActionExecuting(httpActionContext);
        }

        [Test]
        public void OnActionExecuting_should_throw_when_no_auth_header_is_found()
        {
            var authorizeActionFilter = new AuthorizeAttribute();
            var httpActionContext = BuildHttpActionContextWithNoAuthHeader();

            Action onActionExecuting = () => authorizeActionFilter.OnActionExecuting(httpActionContext);
            onActionExecuting.ShouldThrow<HttpUnauthorizedResponseException>()
                .ResponseTextShouldBe("Access Denied");
        }

        [Test]
        public void OnActionExecuting_should_throw_when_no_auth_key_is_not_valid()
        {
            var authorizeActionFilter = new AuthorizeAttribute();
            var httpActionContext = BuildHttpActionContextWithInvalidKey();

            Action onActionExecuting = () => authorizeActionFilter.OnActionExecuting(httpActionContext);
            onActionExecuting.ShouldThrow<HttpUnauthorizedResponseException>()
                .ResponseTextShouldBe("Access Denied");
        }

        private static ApiAuthenticationKeyLookupService GetStubForApiAuthenticationKeyLookupService()
        {
            var stubApiAuthenticationKeyLookupService = MockRepository.GenerateStrictMock<ApiAuthenticationKeyLookupService>(null, null);
            stubApiAuthenticationKeyLookupService.Stub(x => x.GetCachedApiAuthenticationKeys()).Return(null);
            stubApiAuthenticationKeyLookupService.Stub(x => x.ContainsKey(ValidApiAuthenticationKey)).Return(true);
            stubApiAuthenticationKeyLookupService.Stub(x => x.ContainsKey(InvalidApiAuthenticationKey)).Return(false);

            return stubApiAuthenticationKeyLookupService;
        }

        private static HttpActionContext BuildValidHttpActionContext()
        {
            var request = new HttpRequestMessage();
            request.Headers.Add(Globals.AuthenticationKeyHeaderName, ValidApiAuthenticationKey);

            var controllerContext = new HttpControllerContext { Request = request };
            var httpActionContext = new HttpActionContext { ControllerContext = controllerContext };

            return httpActionContext;
        }

        private static HttpActionContext BuildHttpActionContextWithInvalidKey()
        {
            var request = new HttpRequestMessage();
            request.Headers.Add(Globals.AuthenticationKeyHeaderName, InvalidApiAuthenticationKey);

            var controllerContext = new HttpControllerContext { Request = request };
            var httpActionContext = new HttpActionContext { ControllerContext = controllerContext };

            return httpActionContext;
        }

        private static HttpActionContext BuildHttpActionContextWithNoAuthHeader()
        {
            var request = new HttpRequestMessage();
            var controllerContext = new HttpControllerContext { Request = request };
            var httpActionContext = new HttpActionContext { ControllerContext = controllerContext };

            return httpActionContext;
        }
    }
}
