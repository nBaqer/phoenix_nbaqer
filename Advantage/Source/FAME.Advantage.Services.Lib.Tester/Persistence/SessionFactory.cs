﻿using NHibernate;

namespace FAME.Advantage.Services.Lib.Tester.Persistence
{
    public static class SessionFactory
    {
        private static readonly ISessionFactory INSTANCE =
            new AdvantageTestsSessionFactoryConfig().CreateSessionFactory();

        public static ISessionFactory Instance
        {
            get { return INSTANCE; }
        }
    }
}
