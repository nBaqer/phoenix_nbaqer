﻿using FAME.Advantage.Domain.Persistence.Infrastructure.NHibernate;

namespace FAME.Advantage.Services.Lib.Tester.Persistence
{
    public sealed class AdvantageTestsSessionFactoryConfig : BaseSessionFactoryConfig<AdvantageSessionFactoryConfig>
    {
        private const string CONNECTION_STRING_NAME = "Advantage";

        public AdvantageTestsSessionFactoryConfig()
            : base(CONNECTION_STRING_NAME, "thread_static")
        {
        }
    }
}
