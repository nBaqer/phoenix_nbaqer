﻿using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.StatusesOptions;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;


namespace FAME.Advantage.Services.Lib.Tester.Persistence.Campus
{
    class CampusPersistenceTester : DomainPersistenceTester
    {
        [Test]
        public void ShouldSaveAndLoadCampus()
        {
            var gcg = Guid.NewGuid();
            var cam = Guid.NewGuid();
            var sta = Guid.Parse("B30578D9-3533-455E-BE8E-EC54CDBF3CB2");
            var status = new StatusOption("Active","A", null);
            var campusGroup = new CampusGroup(gcg,"CampusAlfa","Atlanta City", Guid.Empty, false);
            var campusGroups = new List<CampusGroup>();
            campusGroups.Add(campusGroup);
            var campus = new FAME.Advantage.Domain.Campuses.Campus(cam,"Code","My test Fake Campus", "Address1","Address2","City",sta, campusGroups, "32040", status);

            var newCampus = VerifyPersistence(campus);

            newCampus.ID.Should().NotBe(Guid.Empty);
            newCampus.Description.Should().Be("My Test Fake Campus");
            newCampus.Status.StatusCode.Should().Be("A");

            var cgroups = newCampus.CampusGroup;
            cgroups.Should().NotBeNull();
            var group = cgroups.Single();

            group.CampusGrpCode.Should().Be("CampusAlfa");

        }
    }
}
