﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace FAME.Advantage.Services.Lib.Tester.Model
{
    using System.Net;
    using System.Net.Http;

    public class Request
    {
        /// <summary>
        /// The Advantage Web API URL loaded from the Application Config.
        /// </summary>
        public string Url
        {
            get
            {
                return ConfigurationManager.AppSettings["ADVANTAGE_WEB_API_URL"];
            }
        }

        /// <summary>
        /// The Advantage Web API Authentication Key loaded from the Application Config
        /// </summary>
        public string AuthKey
        {
            get
            {
                return ConfigurationManager.AppSettings["APIKEY"];
            }
        }

        public string Path { get; set; }

        public string QueryString { get; set; }

        public Dictionary<string, string> Body { get; set; }

        public string ResponseText { get; set; }
        public HttpStatusCode ResponseStatusCode { get; set; }

        public string ResponseContent { get; set; }

        public void SendRequest(HttpMethod method)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("AuthKey", this.AuthKey);
            if (method == HttpMethod.Get)
            {
                var request = new HttpRequestMessage
                {
                    RequestUri = new Uri(this.Url + this.Path + "?" + this.QueryString),
                    Method = method,
                };

                var task = client.SendAsync(request).ContinueWith(
                   taskwithmsg =>
                       {
                           this.ResponseStatusCode = taskwithmsg.Result.StatusCode;
                           this.ResponseContent = taskwithmsg.Result.ToString();
                           this.ResponseText = taskwithmsg.Result.Content.ReadAsStringAsync().Result.ToString();
                           Console.WriteLine(this.ResponseText);
                           Console.WriteLine(this.ResponseContent);
                       });
                task.Wait();
            }
            else if (method == HttpMethod.Post)
            {
                var encodedContent = new FormUrlEncodedContent(this.Body);
                HttpContent content = encodedContent;
                var task = client.PostAsync(this.Url + this.Path + "?" + this.QueryString, content).ContinueWith(
                    taskwithmsg =>
                    {
                        this.ResponseStatusCode = taskwithmsg.Result.StatusCode;
                        this.ResponseContent = taskwithmsg.Result.ToString();
                        this.ResponseText = taskwithmsg.Result.Content.ReadAsStringAsync().Result.ToString();
                        Console.WriteLine(this.ResponseText);
                        Console.WriteLine(this.ResponseContent);
                    });
                task.Wait();

            }
            else if (method == HttpMethod.Put)
            {
                var encodedContent = new FormUrlEncodedContent(this.Body);
                HttpContent content = encodedContent;
                var task = client.PutAsync(this.Url + this.Path + "?" + this.QueryString, content).ContinueWith(
                    taskwithmsg =>
                    {
                        this.ResponseStatusCode = taskwithmsg.Result.StatusCode;
                        this.ResponseContent = taskwithmsg.Result.ToString();
                        this.ResponseText = taskwithmsg.Result.Content.ReadAsStringAsync().Result.ToString();
                        Console.WriteLine(this.ResponseText);
                        Console.WriteLine(this.ResponseContent);
                    });
                task.Wait();
            }
            else if (method == HttpMethod.Delete)
            {
                var task = client.DeleteAsync(this.Url + this.Path + "?" + this.QueryString).ContinueWith(
                    taskwithmsg =>
                    {
                        this.ResponseStatusCode = taskwithmsg.Result.StatusCode;
                        this.ResponseContent = taskwithmsg.Result.ToString();
                        this.ResponseText = taskwithmsg.Result.Content.ReadAsStringAsync().Result.ToString();
                        Console.WriteLine(this.ResponseText);
                        Console.WriteLine(this.ResponseContent);
                    });
                task.Wait();
            }
        }
    }
}
