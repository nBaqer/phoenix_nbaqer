﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionRoutes.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.Tester.Model.Routes.LeadInstitutionRoutes
// </copyright>
// <summary>
//   The lead institution routes.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Tester.Model.Routes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The lead institution routes.
    /// </summary>
    public static class InstitutionRoutes
    {
        /// <summary>
        /// Gets the get route path.
        /// </summary>
        public static string Get
        {
            get
            {
                return "Lead/Institution/Get";
            }
        }

        /// <summary>
        /// The find.
        /// </summary>
        public static string Find
        {
            get
            {
                return "Lead/Institution/Find";
            }
        }

        ///// <summary>
        ///// The delete.
        ///// </summary>
        ////public static string Delete => "Lead/Institution/Delete";

        ///// <summary>
        ///// The update.
        ///// </summary>
        ////public static string Put => "Lead/Institution/Put";

        ///// <summary>
        ///// The insert.
        ///// </summary>
        ////public static string Post => "Lead/Institution/Post";

        /// <summary>
        /// The get phone.
        /// </summary>
        public static string GetPhone
        {
            get
            {
                return "Lead/Institution/GetPhone";
            }
        }

        /// <summary>
        /// The find phone.
        /// </summary>
        public static string FindPhone
        {
            get
            {
                return "Lead/Institution/FindPhone";
            }
        }

        /// <summary>
        /// The delete phone.
        /// </summary>
        public static string DeletePhone
        {
            get
            {
                return "Lead/Institution/DeletePhone";
            }
        }

        /// <summary>
        /// The put phone.
        /// </summary>
        public static string PutPhone
        {
            get
            {
                return "Lead/Institution/PutPhone";
            }
        }

        /// <summary>
        /// The post phone.
        /// </summary>
        public static string PostPhone
        {
            get
            {
                return "Lead/Institution/PostPhone";
            }
        }

        /// <summary>
        /// The get address.
        /// </summary>
        public static string GetAddress
        {
            get
            {
                return "Lead/Institution/GetAddress";
            }
        }

        /// <summary>
        /// The find address.
        /// </summary>
        public static string FindAddress
        {
            get
            {
                return "Lead/Institution/FindAddress";
            }
        }

        /// <summary>
        /// The delete address.
        /// </summary>
        public static string DeleteAddress
        {
            get
            {
                return "Lead/Institution/DeleteAddress";
            }
        }

        /// <summary>
        /// The put address.
        /// </summary>
        public static string PutAddress
        {
            get
            {
                return "Lead/Institution/PutAddress";
            }
        }

        /// <summary>
        /// The post address.
        /// </summary>
        public static string PostAddress
        {
            get
            {
                return "Lead/Institution/PostAddress";
            }
        }

        /// <summary>
        /// The get contact.
        /// </summary>
        public static string GetContact
        {
            get
            {
                return "Lead/Institution/GetContact";
            }
        }

        /// <summary>
        /// The find contact.
        /// </summary>
        public static string FindContact
        {
            get
            {
                return "Lead/Institution/Find";
            }
        }

        /// <summary>
        /// The delete contact.
        /// </summary>
        public static string DeleteContact
        {
            get
            {
                return "Lead/Institution/DeleteContact";
            }
        }

        /// <summary>
        /// The put contact.
        /// </summary>
        public static string PutContact
        {
            get
            {
                return "Lead/Institution/PutContact";
            }
        }

        /// <summary>
        /// The post contact.
        /// </summary>
        public static string PostContact
        {
            get
            {
                return "Lead/Institution/PostContact";
            }
        }
    }
}
