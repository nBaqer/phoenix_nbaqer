﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadEducationRoutes.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.Tester.Model.Routes.LeadEducationRoutes
// </copyright>
// <summary>
//   The lead education routes.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Tester.Model.Routes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The lead education routes.
    /// </summary>
    public static class LeadEducationRoutes
    {
        /// <summary>
        /// Gets the get.
        /// </summary>
        public static string Get
        {
            get
            {
                return "Lead/Education/Get";
            }
        }

        /// <summary>
        /// The find.
        /// </summary>
        public static string Find
        {
            get
            {
                return "Lead/Education/Find";
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        public static string Delete
        {
            get
            {
                return "Lead/Education/Delete";
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        public static string Put
        {
            get
            {
                return "Lead/Education/Put";
            }
        }

        /// <summary>
        /// The insert.
        /// </summary>
        public static string Post
        {
            get
            {
                return "Lead/Education/Post";
            }
        }
    }
}
