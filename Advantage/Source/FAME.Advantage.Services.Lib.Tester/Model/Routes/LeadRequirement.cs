﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Services.Lib.Tester.Model.Routes
{
    public static class LeadRequirement
    {
        public static string GetRequirementsList
        {
            get
            {
                return "Leads/{leadId}/Requirements/GetRequirementsList";
            }
        }

        public static string GetRequirementsByLeadId
        {
            get
            {
                return "Leads/{leadId}/Requirements/GetRequirementsByLeadId";
            }
        }

        public static string GetRequirementTypesMet
        {
            get
            {
                return "Leads/{leadId}/Requirements/GetRequirementTypesMet";
            }
        }

        public static string GetOptionalRequirementsByLeadId
        {
            get
            {
                return "Leads/{leadId}/Requirements/GetOptionalRequirementsByLeadId";
            }
        }

        public static string GetReqGroupsByLeadId
        {
            get
            {
                return "Leads/{leadId}/Requirements/GetReqGroupsByLeadId";
            }
        }

        public static string GetRequirementsByGroupId
        {
            get
            {
                return "Leads/{grpId}/Requirements/GetRequirementsByGroupId";
            }
        }

        public static string GetRequirementsDetailsByReqId
        {
            get
            {
                return "Leads/{reqId}/Requirements/GetRequirementsDetailsByReqId";
            }
        }

        public static string GetLeadTransactionReceipt
        {
            get
            {
                return "Leads/Requirements/GetLeadTransactionReceipt";
            }
        }

        public static string PostRequirement
        {
            get
            {
                return "Leads/Requirements/PostRequirement";
            }
        }

        public static string DeleteRequirementHistoryRecord
        {
            get
            {
                return "Leads/Requirements/DeleteRequirementHistoryRecord";
            }
        }

        public static string PostDocumentFile
        {
            get
            {
                return "Leads/Requirements/PostDocumentFile";
            }
        }

        public static string VoidTransaction
        {
            get
            {
                return "Leads/Requirements/VoidTransaction";
            }
        }
    }
   
}
