﻿using AutoMapper;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;
using FluentValidation;
using log4net;
using log4net.Appender;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using StructureMap;

namespace FAME.Advantage.Services.Lib.Tester.Infrastructure.Bootstrapping
{
    using AutoMapper.Configuration;

    using FAME.Advantage.Domain.Infrastructure.Extensions;

    internal static class Bootstrapper
    {
        private static Container container;

        public static Container Container
        {
            get
            {
                return container;
            }
        }

        public static void Initialize(Container container) {
            if (container != null) {
                container.Dispose();
            }
            Bootstrapper.container = container;
        }

        internal static void Bootstrap()
        {
            InitializeLogging();

            InitializeAutoMapper();
        }

        private static void InitializeAutoMapper()
        {
            if (container == null)
            {
                container = new Container(x =>
                {
                    x.Scan(scan =>
                    {
                        scan.TheCallingAssembly();
                        scan.WithDefaultConventions();
                        scan.ConnectImplementationsToTypesClosing(typeof(AbstractValidator<>));
                        scan.AddAllTypesOf<IMapperDefinition>();
                    });
                });
            };
            var configuration = new MapperConfigurationExpression();
            var mappingDefinitions = container.GetAllInstances<IMapperDefinition>();
            EnumerableExtensions.Each(mappingDefinitions, mappingDefinition => mappingDefinition.CreateDefinition(configuration));
            Mapper.Initialize(configuration);


            Mapper.AssertConfigurationIsValid();
        }

        private static void InitializeLogging()
        {
            BuildAndConfigureTheLogger();
        }

        private static void BuildAndConfigureTheLogger()
        {
            var hierarchy = (Hierarchy)LogManager.GetRepository();
            hierarchy.Root.RemoveAllAppenders();

            var patternLayout = new PatternLayout { ConversionPattern = "%d [%2%t] %-5p [%-10c]   %m%n%n" };
            patternLayout.ActivateOptions();

            var traceAppender = new TraceAppender { Layout = patternLayout };
            traceAppender.ActivateOptions();

            log4net.Config.BasicConfigurator.Configure(traceAppender);
        }
    }
}
