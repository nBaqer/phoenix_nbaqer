﻿using FAME.Advantage.Services.Lib.Infrastructure.Services;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Net.Http;

namespace FAME.Advantage.Services.Lib.Tester.Services
{
    [TestFixture]
    public class UploadifyServiceTester
    {
        private readonly byte[] _file = new byte[2000];

        public UploadifyServiceTester()
        {
            new Random().NextBytes(_file);
        }

        [Test]
        public void UploadifyService_should_construct_correctly()
        {
            var multiPartRequest = BuildValidRequestMessageForTest();
            var uploadifyService = new UploadifyService(multiPartRequest);

            uploadifyService.Should().NotBeNull();
        }

        [Test]
        public void UploadifyService_should_throw_when_request_is_not_multi_part()
        {
            var nonMultiPartRequest = new HttpRequestMessage {Content = new StringContent("SomeContent")};

            Action instantiateUploadifyService = () => new UploadifyService(nonMultiPartRequest);

            instantiateUploadifyService
                .ShouldThrow<Exception>()
                .WithMessage("Multipart content was not found");
        }

        [Test]
        public void UploadifyService_should_throw_when_multipart_does_not_contain_a_filedata_part()
        {
            var invalidRequest = BuildInvalidRequestMessage();

            Action instantiateUploadifyService = () => new UploadifyService(invalidRequest);

            instantiateUploadifyService
                .ShouldThrow<Exception>()
                .WithMessage("Multipart of name 'Filedata' was not found");
        }

        [Test]
        public void GetFileName_should_reuturn_correctly()
        {
            var multiPartRequest = BuildValidRequestMessageForTest();
            var uploadifyService = new UploadifyService(multiPartRequest);

            var fileName = uploadifyService.GetFileName();

            fileName.Should().Be(@"""myFile.txt""");
        }

        [Test]
        public void GetFileBytes_should_return_correctly()
        {
            var multiPartRequest = BuildValidRequestMessageForTest();
            var uploadifyService = new UploadifyService(multiPartRequest);

            var fileBytes = uploadifyService.GetFileBytes();

            fileBytes.Should().BeEquivalentTo(_file);
        }

        private HttpRequestMessage BuildValidRequestMessageForTest()
        {
            var content = new ByteArrayContent(_file);
            content.Headers.Add("Content-Disposition", @"form-data; name=""Filedata""; filename=""myFile.txt""");
            var multiPartContent = new MultipartContent {content};
            var httpRequestMessage = new HttpRequestMessage {Content = multiPartContent};

            return httpRequestMessage;
        }

        private HttpRequestMessage BuildInvalidRequestMessage()
        {
            var content = new ByteArrayContent(_file);
            content.Headers.Add("Content-Disposition", @"form-data; name=""NOT FILE DATA""; filename=""myFile.txt""");
            var multiPartContent = new MultipartContent { content };
            var httpRequestMessage = new HttpRequestMessage { Content = multiPartContent };

            return httpRequestMessage;
        }
    }
}
