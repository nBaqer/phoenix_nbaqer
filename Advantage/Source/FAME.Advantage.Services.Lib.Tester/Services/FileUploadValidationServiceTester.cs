﻿using FAME.Advantage.Services.Lib.Infrastructure.Services;
using FluentAssertions;
using NUnit.Framework;
using System;

namespace FAME.Advantage.Services.Lib.Tester.Services
{
    [TestFixture]
    public class FileUploadValidationServiceTester
    {
        private readonly byte[] _file = new byte[2000];
        private readonly byte[] _emptyFile = new byte[0];
        private readonly byte[] _largeFile = new byte[11000000];
        private const string FileName = "myfile.txt";
        private const string InvalidFileName = "myFile.xxx";

        public FileUploadValidationServiceTester()
        {
            var random = new Random();
            random.NextBytes(_file);
            random.NextBytes(_largeFile);
        }

        [Test]
        public void DocumentUploadValidationService_should_contruct_correctly()
        {
            var documentUploadValidationService = new FileUploadValidationService(FileName, _file);
            documentUploadValidationService.Should().NotBeNull();
        }

        [Test]
        public void DocumentUploadValidationService_should_throw_when_filename_is_empty_string()
        {
            Action instantiateDocumentUploadValidationService =
                () => new FileUploadValidationService(string.Empty, _file);

            instantiateDocumentUploadValidationService
                .ShouldThrow<ArgumentException>()
                .WithMessage("fileName cannot be null or empty");
        }

        [Test]
        public void DocumentUploadValidationService_should_throw_when_filename_is_null()
        {
            Action instantiateDocumentUploadValidationService =
                () => new FileUploadValidationService(null, _file);

            instantiateDocumentUploadValidationService
                .ShouldThrow<ArgumentException>()
                .WithMessage("fileName cannot be null or empty");
        }

        [Test]
        public void DocumentUploadValidationService_should_throw_when_file_is_null()
        {
            Action instantiateDocumentUploadValidationService =
                () => new FileUploadValidationService(FileName, null);

            instantiateDocumentUploadValidationService
                .ShouldThrow<ArgumentNullException>()
                .WithMessage("Value cannot be null.\r\nParameter name: fileBytes");
        }

        [Test]
        public void ValidateFileForUpload_should_validate_correctly()
        {
            var documentUploadValidationService = new FileUploadValidationService(FileName, _file);

            Action validate = documentUploadValidationService.ValidateFileForUpload;

            validate.ShouldNotThrow();
        }

        [Test]
        public void ValidateFileForUpload_should_throw_when_file_is_empty()
        {
            var documentUploadValidationService = new FileUploadValidationService(FileName, _emptyFile);

            Action validate = documentUploadValidationService.ValidateFileForUpload;

            validate
                .ShouldThrow<FileUploadValidatorException>()
                .WithMessage("File is empty");
        }

        [Test]
        public void ValidateFileForUpload_should_throw_when_file_is_too_large()
        {
            var documentUploadValidationService = new FileUploadValidationService(FileName, _largeFile);

            Action validate = documentUploadValidationService.ValidateFileForUpload;

            validate
                .ShouldThrow<FileUploadValidatorException>()
                .WithMessage("File size exceeds the maximum size");
        }

        [Test]
        public void ValidateFileForUpload_should_throw_when_file_extension_is_not_supported()
        {
            var documentUploadValidationService = new FileUploadValidationService(InvalidFileName, _file);

            Action validate = documentUploadValidationService.ValidateFileForUpload;

            validate
                .ShouldThrow<FileUploadValidatorException>()
                .WithMessage("File type '.xxx' is not supported");
        }
    }
}
