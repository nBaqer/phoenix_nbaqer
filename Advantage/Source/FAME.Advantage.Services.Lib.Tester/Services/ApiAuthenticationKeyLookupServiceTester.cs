﻿using FAME.Advantage.Domain.MultiTenant;
using FAME.Advantage.Services.Lib.Infrastructure.Caching;
using FAME.Advantage.Services.Lib.Infrastructure.Services;
using FluentAssertions;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using NHibernate;
using NUnit.Framework;
using Rhino.Mocks;
using StructureMap;
using System;
using System.Linq;
using FAME.Advantage.Services.Lib.Tester.Infrastructure.Bootstrapping;

namespace FAME.Advantage.Services.Lib.Tester.Services
{
    [TestFixture]
    public class ApiAuthenticationKeyLookupServiceTester
    {
        private const string Key = "RANDOMLY GENERATED KEY";

        private readonly Tenant _tenant = new Tenant("Tenant Name", "User",
            new DataConnection("(local)", "Advantage", "User", "Password"),
            new TenantEnvironment("Environment", "0.1", "http://fameinc.com"));

        private readonly ApiAuthenticationKeyLookupService _apiAuthenticationKeyLookupService;

        public ApiAuthenticationKeyLookupServiceTester()
        {
            Bootstrapper.Initialize(new Container(x =>
            {
                x.Scan(scan =>
                {
                    scan.AssemblyContainingType<ICacheVariableFactory>();
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                });

                x.For<CacheManager>().Use(CacheFactory.GetCacheManager());
            }));

            Bootstrapper.Bootstrap();

            var stubApiAuthenticationKey = MockRepository.GeneratePartialMock<ApiAuthenticationKey>(_tenant);
            stubApiAuthenticationKey.Stub(x => x.Key).Return(Key);

            var cacheVariableFactory = Bootstrapper.Container.GetInstance<ICacheVariableFactory>();
            _apiAuthenticationKeyLookupService =
                MockRepository.GeneratePartialMock<ApiAuthenticationKeyLookupService>(default(IStatelessSession), cacheVariableFactory);
            _apiAuthenticationKeyLookupService.Stub(x => x.EagerLoadAuthenticationKeys()).Return(new[] { stubApiAuthenticationKey });
        }

        [Test]
        public void ContainsKey_should_return_true_when_key_is_present()
        {
            _apiAuthenticationKeyLookupService.ContainsKey(Key).Should().BeTrue();
        }

        [Test]
        public void ContainsKey_should_return_false_when_key_is_not_present()
        {
            _apiAuthenticationKeyLookupService.ContainsKey("SOME KEY").Should().BeFalse();
        }

        [Test]
        public void ContainsKey_should_return_true_when_key_has_to_be_loaded_into_cache()
        {
            //Load keys into cache
            var keys = _apiAuthenticationKeyLookupService._apiAuthenticationKeys.Value;
            keys.Any().Should().BeTrue();

            //Clear keys from cache
            _apiAuthenticationKeyLookupService._apiAuthenticationKeys.Clear();
            _apiAuthenticationKeyLookupService._apiAuthenticationKeys.HasValue.Should().BeFalse();

            _apiAuthenticationKeyLookupService.ContainsKey(Key).Should().BeTrue();
        }

        [Test]
        public void GetTenantConnectionStringForKey_should_return_correctly()
        {
            var connectionString = _apiAuthenticationKeyLookupService.GetTenantConnectionStringForKey(Key);

            var expectedConnectionString = "Server=(local);database=Advantage;uid=User;pwd=Password;";
            connectionString.Should().Be(expectedConnectionString);
        }

        [Test]
        public void GetTenantConnectionStringForKey_should_throw_when_key_is_not_found()
        {
            Action getConnectionString = () => _apiAuthenticationKeyLookupService.GetTenantConnectionStringForKey("SOME KEY");

            getConnectionString.ShouldThrow<ApplicationException>()
                .WithMessage("The requested key was not found");
        }
    }
}
