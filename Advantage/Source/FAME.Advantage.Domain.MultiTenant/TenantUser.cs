﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Infrastructure.Extensions;

namespace FAME.Advantage.Domain.MultiTenant
{
    public class TenantUser : DomainEntityWithTypedID<Guid>
    {
        private IList<TenantUserMembership> _tenants; 

        public virtual string UserName { get; protected set; }

        protected TenantUser()
        {
            _tenants = new List<TenantUserMembership>();
        }

        public virtual IEnumerable<TenantUserMembership> Tenants
        {
            get { return _tenants.AsEnumerable(); }
            protected set { _tenants = (IList<TenantUserMembership>)value; }
        }

        public virtual bool IsSupportUser()
        {
            return _tenants.Any(t => t.IsSupportUser);
        }

        public virtual string GetConnectionString(int tenantId)
        {
            return GetTenant(tenantId).GetConnectionString();
        }

        public virtual string GetOleDbConnectionString(int tenantId)
        {
            return GetTenant(tenantId).GetOleDbConnectionString();
        }

        public virtual string GetApplicationURL()
        {
            return GetApplicationURL(GetDefaultTenant());
        }

        public virtual string GetApplicationURL(int tenantId)
        {
            return GetApplicationURL(GetTenant(tenantId));
        }

        public virtual string GetApplicationURL(Tenant tenant)
        {
            var environment = tenant.Environment;
            var applicationURL = environment.ApplicationURL
                .Replace("{version}", environment.VersionNumber)
                .Replace("{tenant-id}", tenant.ID.ToString(CultureInfo.InvariantCulture))
                .Replace("{user-id}", ID.ToString());

            return applicationURL;
        }

        public virtual Tenant GetDefaultTenant()
        {
            var tenant = _tenants.FirstOrDefault(t => t.IsDefaultTenant) ?? _tenants.FirstOrDefault();

            if (tenant == null) {
                throw new DomainException("No tenants found for user {0}", ID);
            }

            return tenant.Tenant;
        }

        public virtual Tenant GetTenant(int id)
        {
            var tenant = _tenants.SingleOrDefault(t => t.Tenant.ID == id);

            if (tenant == null) {
                throw new DomainException("User is not a member of tenant {0}", id);
            }

            return tenant.Tenant;
        }

        public virtual void AddTenant(Tenant tenant)
        {
            AddTenant(tenant, isSupport: false, isDefault: false);
        }

        public virtual void AddTenant(Tenant tenant, bool isSupport, bool isDefault)
        {
            var existingTenant = GetMembership(tenant);

            if (existingTenant != null) {
                existingTenant.Update(isSupport, isDefault);
            } else {
                _tenants.Add(new TenantUserMembership(this, tenant, isSupport, isDefault));
            }
        }

        public virtual void RemoveTenant(Tenant tenant, IRepositoryWithTypedID<int> repository)
        {
            // Since this domain object is technically managed by the 
            // Membership provider, we can't rely on a cascading delete to 
            // clean up these domain objects, so this needs to be done manually.
            var existingTenant = GetMembership(tenant);

            if (existingTenant != null) {
                _tenants.Remove(existingTenant);
            }

            repository.Delete(existingTenant);
        }

        public virtual void ClearTenants(IRepositoryWithTypedID<int> repository)
        {
            // Since this domain object is technically managed by the 
            // Membership provider, we can't rely on a cascading delete to 
            // clean up these domain objects, so this needs to be done manually.
            _tenants.Each(t => repository.Delete(t));
            _tenants.Clear();
        }

        private TenantUserMembership GetMembership(Tenant tenant)
        {
            return _tenants.SingleOrDefault(t => t.Tenant == tenant);
        }
    }
}
