﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.MultiTenant
{
    public class DataConnection :  DomainEntityWithTypedID<int>, IEquatable<DataConnection>
    {
        public string ServerName { get; private set; }
        public string DatabaseName { get; private set; }
        public string UserName { get; private set; }
        public string Password { get; private set; }

        public DataConnection() {}

        public DataConnection(string serverName, string databaseName, string userName, string password)
        {
            ServerName = serverName;
            DatabaseName = databaseName;
            UserName = userName;
            Password = password;
        }

        public string GetConnectionString(string versionNumber)
        {
            const string CONNECTION_STRING_TEMPLATE = "Server={0};database={1};uid={2};pwd={3};";
            return string.Format(CONNECTION_STRING_TEMPLATE, ServerName, ApplyVersionNumber(DatabaseName, versionNumber), UserName, Password);
        }

        public string GetOleDbConnectionString(string versionNumber)
        {
            var connectionString = GetConnectionString(versionNumber);
            return "Provider = SQLOLEDB;" + connectionString;
        }

        private string ApplyVersionNumber(string input, string versionNumber)
        {
            return input.Replace("{version}", versionNumber);
        }

        #region Override Equality
        public bool Equals(DataConnection other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            return string.Equals(ServerName, other.ServerName) && string.Equals(DatabaseName, other.DatabaseName) && string.Equals(UserName, other.UserName) && string.Equals(Password, other.Password);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != this.GetType()) {
                return false;
            }
            return Equals((DataConnection) obj);
        }

        public override int GetHashCode()
        {
            unchecked {
                var hashCode = (ServerName != null ? ServerName.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (DatabaseName != null ? DatabaseName.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (UserName != null ? UserName.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Password != null ? Password.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(DataConnection left, DataConnection right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(DataConnection left, DataConnection right)
        {
            return !Equals(left, right);
        }
        #endregion
    }
}