﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.MultiTenant
{
    public class Tenant : DomainEntityWithTypedID<int>
    {
        public virtual string Name { get; protected set; }
        public virtual DataConnection Connection { get; protected set; }
        public virtual TenantEnvironment Environment { get; protected set; }
        public virtual string ModifiedBy { get; protected set; }
        public virtual DateTime CreatedDate { get; protected set; }
        public virtual DateTime ModifiedDate { get; protected set; }
        public virtual IList<ApiAuthenticationKey> Keys { get; protected set; }
        public virtual IList<WapiTenantCompanySecret> WapiCompanySecretsList { get; protected set; }  

        protected Tenant() { }

        public Tenant(string name, string createdBy, DataConnection connection, TenantEnvironment environment)
        {
            Name = name;
            Connection = connection;
            Environment = environment;
            ModifiedBy = createdBy;
            CreatedDate = DateTime.Now;
            ModifiedDate = CreatedDate;
        }

        public virtual void MigrateToNewEnvironment(TenantEnvironment environment)
        {
            Environment = environment;
            ModifiedDate = DateTime.Now;
        }

        public virtual string GetConnectionString()
        {
            return Connection.GetConnectionString(Environment.VersionNumber);
        }

        public virtual string GetOleDbConnectionString()
        {
            return Connection.GetOleDbConnectionString(Environment.VersionNumber);
        }
    }
}