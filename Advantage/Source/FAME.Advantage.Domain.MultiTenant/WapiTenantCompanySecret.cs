﻿using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.MultiTenant
{
    public class WapiTenantCompanySecret : DomainEntityWithTypedID<int>
    {
        protected WapiTenantCompanySecret()
        {

        }

        public WapiTenantCompanySecret(string externalCompanyCode, Tenant tenantObj)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            ExternalCompanyCode = externalCompanyCode;
            TenantObj = tenantObj;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public virtual string ExternalCompanyCode { get; protected set; }
        public virtual Tenant TenantObj { get; protected set; }
    }
}
