﻿using System;

namespace FAME.Advantage.Domain.MultiTenant
{
    public interface IPasswordResetService
    {
        void ResetPasswordDates(Guid userId);
    }
}
