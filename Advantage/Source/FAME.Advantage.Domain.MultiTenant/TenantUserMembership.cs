﻿using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.MultiTenant
{
    public class TenantUserMembership : DomainEntityWithTypedID<int>
    {
        public virtual TenantUser User { get; protected set; }
        public virtual Tenant Tenant { get; protected set; }
        public virtual bool IsSupportUser { get; protected set; }
        public virtual bool IsDefaultTenant { get; protected set; }

        protected TenantUserMembership() {}

        public TenantUserMembership(TenantUser user, Tenant tenant, bool isSupportUser, bool isDefaultTenant)
        {
            User = user;
            Tenant = tenant;
            IsSupportUser = isSupportUser;
            IsDefaultTenant = isDefaultTenant;
        }

        public virtual void Update(bool isSupport, bool isDefault)
        {
            IsSupportUser = isSupport;
            IsDefaultTenant = isDefault;
        }
    }
}
