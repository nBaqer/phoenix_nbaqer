﻿using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.MultiTenant
{
    public class TenantEnvironment : DomainEntityWithTypedID<int>
    {
        public virtual string Name { get; protected set; }
        public virtual string VersionNumber { get; protected set; }
        public virtual string ApplicationURL { get; protected set; }

        protected TenantEnvironment() {}

        public TenantEnvironment(string name, string versionNumber, string applicationURL)
        {
            Name = name;
            VersionNumber = versionNumber;
            ApplicationURL = applicationURL;
        }
    }
}