﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;

namespace FAME.Advantage.Domain.MultiTenant
{
    /// <summary>
    /// Domain for table WapiLog
    /// </summary>
    public class WapiLog : DomainEntityWithTypedID<int>
    {
        protected WapiLog() { }

        public WapiLog(string tenantName,
            DateTime dateMod,
            int numberOfRecords,
            string company,
            string serviceInvoqued,
            string operationDescription,
            int isOk,
            string comment)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            TenantName = tenantName;
            DateMod = dateMod;
            NumberOfRecords = numberOfRecords;
            Company = company;
            ServiceInvoqued = serviceInvoqued;
            OperationDescription = operationDescription;
            IsOk = isOk;
            Comment = comment;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }


        public virtual string TenantName { get; set; }
        public virtual DateTime DateMod { get; set; }
        public virtual int NumberOfRecords { get; set; }
        public virtual string Company { get; set; }
        public virtual string ServiceInvoqued { get; set; }
        public virtual string OperationDescription { get; set; }
        public virtual int IsOk { get; set; }
        public virtual string Comment { get; set; }
    }
}
