﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApiAuthenticationKey.cs" company="FAME">
// 2013-2017
// </copyright>
// <summary>
//   Defines the ApiAuthenticationKey type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.MultiTenant
{
    using System;

    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The API authentication key.
    /// </summary>
    public class ApiAuthenticationKey : DomainEntityWithTypedID<int>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApiAuthenticationKey"/> class.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        public ApiAuthenticationKey(Tenant tenant)
        {
            // ReSharper disable VirtualMemberCallInConstructor
            this.Tenant = tenant;
            this.Key = this.GenerateKey();
            // ReSharper restore VirtualMemberCallInConstructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiAuthenticationKey"/> class.
        /// </summary>
        protected ApiAuthenticationKey()
        {
        }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        public virtual string Key { get; protected set; }

        /// <summary>
        /// Gets or sets the tenant.
        /// </summary>
        public virtual Tenant Tenant { get; protected set; }

        /// <summary>
        /// The generate key.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        protected virtual string GenerateKey()
        {
            return Guid.NewGuid().ToString();
            ////return new SecretKeyGenerator().NewKey();
        }
    }
}
