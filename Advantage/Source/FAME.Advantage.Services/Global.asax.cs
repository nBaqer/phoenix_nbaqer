﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Global.asax.cs" company="FAME Inc.">
//   FAME Inc. 2017
//   FAME.Advantage.Services.MvcApplication
// </copyright>
// <summary>
//   The Web API Application Starting Point
//   Web API Application Configurations like routes, dependency injection, JSON formatter settings are applied in here.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services
{
    using System.Web;
    using System.Web.Http;

    using FAME.Advantage.Services.App_Start;
    using FAME.Advantage.Services.Lib.Formatters.MultipartDataMediaFormatter;
    using FAME.Advantage.Services.Lib.Infrastructure.ActionFilters;
    using FAME.Advantage.Services.Lib.Infrastructure.Bootstrapping;

    using AuthorizeAttribute = FAME.Advantage.Services.Lib.Infrastructure.ActionFilters.AuthorizeAttribute;

    // Note: For instructions on enabling IIS6 or IIS7 classic moded, 
    // visit http://go.microsoft.com/?LinkId=9394801
    
    /// <summary>
    /// The Web API Application Starting Point 
    /// Web API Application Configurations like routes, dependency injection, JSON formatter settings are applied in here.
    /// </summary>
    public class MvcApplication : HttpApplication
    {
        /// <summary>
        /// The Application Start event. Configure in here the WEB API Application
        /// </summary>
        protected void Application_Start()
        {
            RegisterActionFilters();

            GlobalConfiguration.Configuration.Formatters.Add(new FormMultipartEncodedMediaTypeFormatter());
            
            // Configure that json admit recursive object
            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            GlobalConfiguration.Configure(WebApiConfig.Register);


            Bootstrapper.Bootstrap();
        }

        /// <summary>
        /// The register action filters.
        /// </summary>
        private static void RegisterActionFilters()
        {
            var filters = GlobalConfiguration.Configuration.Filters;

            filters.Add(new AuthorizeAttribute());
            filters.Add(new ValidationActionFilter());
            filters.Add(new UnitOfWorkActionFilter());
            filters.Add(new GlobalErrorHandler());
        }
    }
}