﻿(function (jQuery) {
    //Taken from: http://javascript.crockford.com/remedial.html
    //May want to use a better performing method of string intrapolation for intensive applications
    String.prototype.supplant = function (o) {
        return this.replace(/{([^{}]*)}/g,
            function (a, b) {
                var r = o[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            }
        );
    };

    var ajaxSetup = function () {
        ajaxSetup.addAuthKeyToRequest = function (xhr) {
            var apiAuthenticationKey = jQuery('#apiAuthenticationKey').val();
            if (!apiAuthenticationKey) {
                alert("No Api Authentication Key Provided... Request is being cancelled");
                xhr.abort();
                return;
            }

            xhr.setRequestHeader('AuthKey', apiAuthenticationKey);
            xhr.setRequestHeader('Username', jQuery('#username').val());
        };

        jQuery.ajaxSetup({
            timeout: 25000, //25 seconds,
            dataType: 'json',
            contentType: 'application/json',
            beforeSend: ajaxSetup.addAuthKeyToRequest
        });

        jQuery(document)
            .ajaxStart(function () {
                jQuery('#loading').show();
            }).ajaxComplete(function () {
                jQuery('#loading').hide();
            }).ajaxSuccess(function() {
                alert("Request was successful. Check HTTP traffic.");
            }).ajaxError(function (event, jqXHR) {
                if (jqXHR.statusText === "canceled") return;

                switch (jqXHR.status) {
                    case 401:
                        alert("Your request resulted in an Unauthorized response. Check your Api Authentication Key");
                        break;
                    default:
                        alert("Error in making request. Check HTTP traffic.");
                        break;
                }
            });
    };

    window.common = window.common || {};
    window.common.ajaxSetup = ajaxSetup;
})(window.jQuery);