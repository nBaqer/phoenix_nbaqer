﻿(function (jQuery, common) {
    jQuery(function() {
        $("#tabs").tabs();
        common.ajaxSetup();

        jQuery('#getAllLogos').click(function (event) {
            event.preventDefault();
            //var campusId = jQuery('#campusIdForStudent').val();
            //var programVersionId = jQuery('#programVersionForStudent').val();
            //var enrollStatusId = jQuery('#enrollmentStatusForStudent').val();
            //var startDate = jQuery('#startDate').val();
            var filter = {ImageId: null, ImageCode: null} ;
            //var studentId = jQuery('#studentIdForStudent').val();
            var uri = 'api/SystemStuff/Maintenance';

            jQuery.get(uri, filter);
        });

        jQuery('#getLogoById').click(function (event) {
            event.preventDefault();
            //var campusId = jQuery('#campusIdForStudent').val();
            //var programVersionId = jQuery('#programVersionForStudent').val();
            //var enrollStatusId = jQuery('#enrollmentStatusForStudent').val();
            //var startDate = jQuery('#startDate').val();
            var id = jQuery('#imageId').val();
            var filter = { ImageId: id, ImageCode: null };
            var uri = 'api/SystemStuff/Maintenance';

            jQuery.get(uri, filter);
        });

        jQuery('#getLogoByCode').click(function (event) {
            event.preventDefault();
            //var campusId = jQuery('#campusIdForStudent').val();
            //var programVersionId = jQuery('#programVersionForStudent').val();
            //var enrollStatusId = jQuery('#enrollmentStatusForStudent').val();
            //var startDate = jQuery('#startDate').val();
            var code = jQuery('#imageCode').val();
            var filter = { ImageId: null, ImageCode: code };
            var uri = 'api/SystemStuff/Maintenance';

            jQuery.get(uri, filter);
        });

        //Get enrollments..................................................................................................

        jQuery('#GetAllEnrollments').click(function(event) {
            event.preventDefault();
            var filter = { CampusId: null, ProgramVersionId: null, EnrollmentStatusId: null, StudentGroupId: null, PageNumber: 1, PageSize: 50, StudentId: null };
            jQuery.get('api/Students/Enrollments/Enrollments/GetCbItemsEnrollments', filter);
        });

        jQuery('#GetEnrollmentsByStudentId').click(function (event) {
            event.preventDefault();
            var studentId = jQuery("#EnrollmentStudentId").val();
            var filter = { CampusId: null, ProgramVersionId: null, EnrollmentStatusId: null, StudentGroupId: null, PageNumber: 1, PageSize: 50, StudentId: studentId };

            jQuery.get('api/Students/Enrollments/Enrollments/GetCbItemsEnrollments', filter);
        });


    });

})(window.jQuery, window.common);