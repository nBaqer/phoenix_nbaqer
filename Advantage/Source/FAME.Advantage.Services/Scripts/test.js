﻿(function (jQuery, common) {
    jQuery(function () {
        $("#tabs").tabs();
        common.ajaxSetup();

        jQuery('#getStudent').click(function (event) {
            event.preventDefault();
            var campusId = jQuery('#campusIdForStudent').val();
            var programVersionId = jQuery('#programVersionForStudent').val();
            var enrollStatusId = jQuery('#enrollmentStatusForStudent').val();
            var startDate = jQuery('#startDate').val();
            var filter = { CampusId: campusId, ProgramVersionId: programVersionId, EnrollmentStatusId: enrollStatusId, StartDate: startDate };

            var studentId = jQuery('#studentIdForStudent').val();
            var uri = 'api/Students/{studentId}'.supplant({ studentId: studentId });

            jQuery.get(uri, filter);
        });

        jQuery('#getEnrollments').click(function (event) {
            event.preventDefault();
            var campusId = jQuery('#campusId').val();
            var programVersionId = jQuery('#programVersionId').val();
            var enrollmentStatusId = jQuery('#enrollmentStatusForEnrollments').val();
            var studentGroupId = jQuery('#studentGroupId').val();
            var pageNumber = jQuery('#getEnrollmentsPageNumber').val();
            var pageSize = jQuery('#getEnrollmentsPageSize').val();

            var filter = { CampusId: campusId, ProgramVersionId: programVersionId, EnrollmentStatusId: enrollmentStatusId, StudentGroupId: studentGroupId, PageNumber: pageNumber, PageSize: pageSize };

            jQuery.get('api/Students/Enrollments/Enrollments/GetAll', filter);
            //jQuery.get('api/Students/Enrollments', filter);
        });

        jQuery('#getCampuses').click(function (event) {
            event.preventDefault();
            var filter = { IncludeAll: true };
            jQuery.get('api/Campuses',filter);
        });

        jQuery('#getCampusesByGrpId').click(function (event) {
            event.preventDefault();
            var grpId = jQuery('#grpId').val();
            var filter = { CampusGrpId: grpId };

            jQuery.get('api/Campuses/', filter);
        });

        jQuery('#getCampusbyUserId').click(function (event) {
            event.preventDefault();
            var campusUserId = jQuery('#CampusUserId').val();
            var filter = { UserId: campusUserId };

            jQuery.get('api/Campuses/', filter);
        });

        jQuery('#getProgramVersions').click(function (event) {
            event.preventDefault();
            var status = jQuery("#status").val();
            var campusId = jQuery("#campusIdProg").val();
            var filter = { StatusCode: status, CampusId: campusId };

            jQuery.get('api/Campuses/CampusGroups/ProgramVersions', filter);
        });

        // Term Test Region .....................................................................................

        jQuery('#getTermsAll').click(function (event) {
            event.preventDefault();
            //var status = jQuery("#terms_StatusCode").val();
            //var campusId = jQuery("#terms_CampusId").val();
            //var filter = { StatusCode: status };

            var uri = 'api/Students/Enrollments/Terms/GetDdTermItems';
            jQuery.get(uri, null);
        });

        jQuery('#getTermsByEnrollment').click(function (event) {
            event.preventDefault();
            //var status = jQuery("#terms_StatusCode").val();
            var enrollmentId = jQuery("#terms_StuEnrollId").val();
            var filter = { StuEnrollmentId: enrollmentId };

            var uri = 'api/Students/Enrollments/Terms/GetDdTermItems';
            jQuery.get(uri, filter);
        });



        jQuery('#getStudentGroups').click(function (event) {
            event.preventDefault();
            var status = jQuery("#statusForStudentGroups").val();
            var campusId = jQuery("#campusIdForStudentGroups").val();
            var filter = { StatusCode: status, CampusId: campusId };

            jQuery.get('api/Campuses/CampusGroups/StudentGroups', filter);
        });

        jQuery('#getStudentSearch').click(function (event) {
            event.preventDefault();
            var searchTerm = jQuery("#txtSearchTerm").val();

            var filter = { SearchTerm: searchTerm };

            jQuery.get('api/StudentSearch/GetBySearchTerm', filter);
        });

        jQuery('#getStudentMru').click(function (event) {
            event.preventDefault();
            var userId = jQuery("#txtMruUserId").val();
            var campusId = jQuery("#txtMruCampusId").val();

            var mruTypeId = jQuery("#txtMruTypeId").val();

            var filter = { UserId: userId, CampusId: campusId };

            
            if (mruTypeId == 1)
                jQuery.get('api/StudentSearch/GetByMru', filter);
            else if (mruTypeId == 4)
                jQuery.get('api/LeadSearch/GetByMru', filter);
            else if (mruTypeId == 3)
                jQuery.get('api/EmployeesSearch/GetByMru', filter);
        });

        jQuery('#getLeadVendors').click(function (event) {
            event.preventDefault();

            jQuery.get('api/Lead/LeadVendor/Get');
        });

        jQuery('#getCampusOfInterest').click(function (event) {
            event.preventDefault();

            jQuery.get('api/Lead/GetCampusOfInterest');
        });

        jQuery('#getCampusWithActiveLead').click(function (event) {
            event.preventDefault();

            jQuery.get('api/CampusWithLead/GetWithActiveLeads');
        });

        jQuery('#getLeadsCounts').click(function (event) {
            event.preventDefault();

            jQuery.get('api/Lead/GetLeadCounts');
        });


        jQuery('#getLeadContacts').click(function (event) {
            event.preventDefault();
            var leadId = jQuery("#txtcontactLeadId").val();
            var filter = { LeadGuid: leadId };

            jQuery.get('api/Lead/GetLeadContactInfo', filter);
        });

        

       


        jQuery('#getLeadsUnassignedToCampus').click(function (event) {
            event.preventDefault();
            var vendorId = jQuery("#txtVendorid").val();

            var camp = jQuery("#txtCampusOfInterest").val();

            var filter = { VendorId: vendorId, CampusOfInterest: camp };

            jQuery.get('api/Lead/GetByUnassignedToCampus', filter);
        });

        jQuery('#getLeadSearch').click(function (event) {
            event.preventDefault();
            var searchTerm = jQuery("#txtLeadSearchTerm").val();

            var filter = { SearchTerm: searchTerm };

            jQuery.get('api/LeadsSearch/GetBySearchTerm', filter);
        });

        jQuery('#getEmployerSearch').click(function (event) {
            event.preventDefault();
            var searchTerm = jQuery("#txtEmpSearchTerm").val();

            var filter = { SearchTerm: searchTerm };

            jQuery.get('api/EmployersSearch/GetBySearchTerm', filter);
        });

        jQuery('#getEmployeeSearch').click(function (event) {
            event.preventDefault();
            var searchTerm = jQuery("#txteSearchTerm").val();

            var filter = { SearchTerm: searchTerm };

            jQuery.get('api/EmployeesSearch/GetBySearchTerm', filter);
        });

        jQuery('#getEnrollmentStatuses').click(function (event) {
            event.preventDefault();
            var status = jQuery("#statusForEnrollmentStatus").val();
            var campusId = jQuery("#campusIdForEnrollmentStatus").val();
            var levelId = jQuery("#statusLevelId").val();
            var filter = { StatusCode: status, CampusId: campusId, StatusLevelId: levelId };

            jQuery.get('api/Campuses/CampusGroups/EnrollmentStatuses', filter);
        });


        jQuery('#getEnrollmentTransactions').click(function (event) {
            event.preventDefault();
            var frDate = jQuery("#trFromDate").val();
            var toDate = jQuery("#trToDate").val();
            var filter = { FromDate: frDate, ToDate: toDate };

            jQuery.get('api/Students/Enrollments/GetTransactionsByPayPeriod', filter);
        });

        jQuery('#getEnrollmentTransactionsSummary').click(function (event) {
            event.preventDefault();
            var frDate = jQuery("#trFromDate2").val();
            var toDate = jQuery("#trToDate2").val();
            var sType = jQuery("#SummaryType").val();
            var filter = { FromDate: frDate, ToDate: toDate, SummaryType: sType };

            jQuery.get('api/Students/Enrollments/GetTransactionsSummary', filter);
        });

        jQuery('#getEnrollmentTransactionProjections').click(function (event) {
            event.preventDefault();
            var crhrValue = jQuery("#crhrValue").val();
            var incrementTypeId = jQuery("#incrementTypeId").val();
            
            var filter = { ThreshValue: crhrValue, IncrementType: incrementTypeId };

            jQuery.get('api/Students/Enrollments/GetProjectedPaymentPeriodCharges', filter);
        });

        jQuery('#getEnrollmentTransactionProjectionsSum').click(function (event) {
            event.preventDefault();
            var crhrValueSum = jQuery("#crhrValueSum").val();
            var incrementTypeIdSum = jQuery("#incrementTypeIdSum").val();

            var filter = { ThreshValue: crhrValueSum, IncrementType: incrementTypeIdSum };

            jQuery.get('api/Students/Enrollments/GetProjectedPaymentPeriodSummary', filter);
        });


        jQuery('#getSystemModules').click(function (event) {
            event.preventDefault();
            jQuery.get('api/SystemStuff/States/');
        });

        jQuery('#getStates').click(function (event) {
            event.preventDefault();
            jQuery.get('api/SystemStuff/States/');
        });

        jQuery('#getMenuItems').click(function (event) {
            event.preventDefault();
            jQuery.get('api/SystemStuff/Menu/');
        });


        jQuery('#getMenuItemsByName').click(function (event) {
            event.preventDefault();
            var menuName = jQuery("#menuuName").val();

            var uri = 'api/SystemStuff/{menuName}/menu'.supplant({ menuName: menuName });


            jQuery.get(uri);
        });


        jQuery('#getDocumentStatuses').click(function (event) {
            event.preventDefault();
            jQuery.get('api/Students/Enrollments/Requirements/DocumentStatuses/');
        });

        jQuery('#getFilesByDocumentId').click(function (event) {
            event.preventDefault();

            var documentId = jQuery('#documentId').val();

            var uri = 'api/Students/Enrollments/Requirements/Documents/{documentId}/Files'.supplant({ documentId: documentId });
            jQuery.get(uri);
        });

        jQuery('#getFileContentsByFileId').click(function (event) {
            event.preventDefault();

            var fileId = jQuery('#fileId').val();

            var uri = 'api/Students/Enrollments/Requirements/Documents/Files/{fileId}/Contents'.supplant({ fileId: fileId });

            jQuery.ajax({
                url: uri,
                contentType: 'application/octet-stream',
                dataType: ''
            });
        });

        jQuery('#getStudentDocuments').click(function (event) {
            event.preventDefault();

            var studentId = jQuery('#studentIdForStudentDocuments').val();
            var campusId = jQuery('#campusIdForStudentDocuments').val();
            var isApproved = jQuery("#isApprovedForStudentDocuments").val();
            var page = jQuery("#getStudentDocumentsPage").val();
            var pageSize = jQuery("#getStudentDocumentsPageSize").val();

            var filter = { CampusId: campusId, StudentId: studentId, IsApproved: isApproved, Page: page, PageSize: pageSize };

            jQuery.get('api/Students/Enrollments/Requirements/Documents/', filter);
        });


        jQuery('#getDocumentsForApproval').click(function (event) {
            event.preventDefault();

            var page = jQuery("#getDocumentsForApprovalPage").val();
            var pageSize = jQuery("#getDocumentsForApprovalPageSize").val();
            var campusId = jQuery("#getDocumentsForApprovalCampusId").val();


            var filter = { CampusId: campusId, IsApproved: false, FileUploaded: true, Page: page, PageSize: pageSize };

            jQuery.get('api/Students/Enrollments/Requirements/Documents/', filter);
        });

        jQuery('#getCampusGroups').click(function (event) {
            event.preventDefault();
            jQuery.get('api/CampusGroups/');
        });

        jQuery('#getCampusGroupCmps').click(function (event) {
            event.preventDefault();
            jQuery.get('api/CampusGroupCmps/');
        });

        jQuery('#getTestRequirements').click(function (event) {
            event.preventDefault();
            jQuery.get('api/TestRequirements/');
        });

        jQuery('#getDocumentRequirements').click(function (event) {
            event.preventDefault();

            var campusId = jQuery('#getDocumentRequirementsCampusId').val();
            var userId = jQuery('#getDocumentRequirementsUserId').val();
            var filter = { CampusId: campusId, StatusCode: 'A', UserId: userId };

            var studentId = jQuery('#getDocumentRequirementsStudentId').val();
            var uri = "api/Students/{studentId}/Requirements/Documents".supplant({ studentId: studentId });

            jQuery.get(uri, filter);
        });

        jQuery('#getLeadReqList').click(function (event) {
            event.preventDefault();

            var campusId = jQuery('#getreqListCampusId').val();
           
            var filter = { CampusId: campusId, StatusCode: 'A' };

            var leadId = jQuery('#getreqListLeadId').val();
            var uri = "api/Leads/{leadId}/Requirements/GetRequirementsList".supplant({ leadId: leadId });

            jQuery.get(uri, filter);
        });

        jQuery('#getLeadRequirements').click(function (event) {
            event.preventDefault();

            var campusId = jQuery('#getDocumentRequirementsLeadCampusId').val();
            var userId = jQuery('#getDocumentRequirementsLeadUserId').val();
            var isMand = jQuery('#getreqIsMandatory').val();
            var filter = { CampusId: campusId, StatusCode: 'A', UserId: userId, IsMandatory: isMand };

            var leadId = jQuery('#getDocumentRequirementsLeadId').val();
            var uri = "api/Leads/{leadId}/Requirements/GetRequirementsByLeadId".supplant({ leadId: leadId });

            jQuery.get(uri, filter);
        });

        jQuery('#getLeadRequirementGroups').click(function (event) {
            event.preventDefault();

            var campusId = jQuery('#getDocumentRequirementsLeadCampusId').val();
            var userId = jQuery('#getDocumentRequirementsLeadUserId').val();
            var isMand = jQuery('#getreqIsMandatory').val();
            var filter = { CampusId: campusId, StatusCode: 'A', UserId: userId, IsMandatory: isMand };

            var leadId = jQuery('#getDocumentRequirementsLeadId').val();
            var uri = "api/Leads/{leadId}/Requirements/GetReqGroupsByLeadId".supplant({ leadId: leadId });

            jQuery.get(uri, filter);
        });

        jQuery('#getLeadRequirementsByGroupId').click(function (event) {
            event.preventDefault();

            var leadId = jQuery('#getDocumentRequirementsgrpLeadId').val();
            var campusId = jQuery('#getDocumentReqGrpsLeadCampusId').val();
            var userId = jQuery('#getDocumentReqGrpsLeadUserId').val();
            var isMand = jQuery('#getreqGrpsIsMandatory').val();
            var filter = { CampusId: campusId, StatusCode: 'A', UserId: userId, IsMandatory: isMand, LeadId: leadId };

            var grpId = jQuery('#getDocumentRequirementsGrpId').val();
            var uri = "api/Leads/{grpId}/Requirements/GetRequirementsByGroupId".supplant({ grpId: grpId });

            jQuery.get(uri, filter);
        });


        

        


        

        jQuery('#getRequirements').click(function (event) {
            event.preventDefault();
            jQuery.get('api/RequirementEffectiveDates/');
        });


        jQuery('#saveDoc').click(function (event) {
            event.preventDefault();
            var studentDocId = jQuery('#documentIdForSave').val();
            var url = 'api/Students/Enrollments/Requirements/Documents/{' + studentDocId + '}';
            var filter = { IsApproved: false };
            $.ajax({
                url: url,
                type: 'PUT',
                data: JSON.stringify(filter)
            });
        });

        jQuery('#impPut').click(function (event) {
            event.preventDefault();
            var logId = '3';
            var url = 'api/SystemStuff/' + logId + '/UserImpersonationLogs';
            $.ajax({
                url: url,
                type: 'PUT'
            });
        });

        jQuery('#impPost').click(function (event) {
            event.preventDefault();
            var filter = { ImpersonatedUser: 'cwood' };
            var url = 'api/SystemStuff/UserImpersonationLogs/NewLog';
            $.ajax({
                url: url,
                type: 'POST',
                data: JSON.stringify(filter)
            });
        });



        jQuery('#getRolesForUser').click(function (event) {
            event.preventDefault();

            var campusId = jQuery('#campusIdForRoles').val();
            var userId = jQuery('#userIdForRoles').val();
            var resourceId = jQuery('#resourceId').val();

            var url = 'api/User/{' + userId + '}/UserRoles';

            var filter = { CampusId: campusId, ResourceId: resourceId };

            jQuery.get(url, filter);
        });


    });

    // Get Courses Classes................................................................................................
    jQuery('#getAllCourses').click(function (event) {
        event.preventDefault();
        var url = 'api/Students/Enrollments/CourseClass/GetDdCourseClass';
        jQuery.get(url, null);
    });




    jQuery('#getCoursesByTerm').click(function (event) {
        event.preventDefault();

        var termId = jQuery('#TermIdForCourses').val();
        var url = 'api/Students/Enrollments/CourseClass/GetDdCourseClass';
        var filter = { TermId: termId };

        jQuery.get(url, filter);
    });



    jQuery('#getUserforUsers').click(function (event) {
        event.preventDefault();

        var userId = jQuery('#getUserUserId').val();

        var url = 'api/User/Users';

        var filter = { UserId: userId, Active: true };

        jQuery.get(url, filter);
    });

    jQuery('#getRepUsers').click(function (event) {
        event.preventDefault();

        var cId = jQuery('#repCampusId').val();

        var url = 'api/User/Users';

        var filter = { CampusId: cId, ReturnReps: true };

        jQuery.get(url, filter);
    });


   


    jQuery('#getImpLog').click(function (event) {
        event.preventDefault();

        var url = 'api/SystemStuff/UserImpersonationLogs';

        var filter = { StartDate: jQuery('#logStart').val(), EndDate: jQuery('#logEnd').val() };


        jQuery.get(url, filter);
    });



})(window.jQuery, window.common);