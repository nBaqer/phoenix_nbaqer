﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace FAME.Advantage.Services.App_Start
{
    /// <summary>
    /// Swagger Access Message Handler verifies the user is authenticated to enable the navigation to the Swagger API UI.
    /// </summary>
    public class SwaggerAccessMessageHandler : DelegatingHandler
    {
        /// <summary>
        /// The Override for SendAsync of the delegating handler. Called when a request is made to the swagger api.
        /// </summary>
        /// <param name="request">The request to swagger api</param>
        /// <param name="cancellationToken">The cancellation token of the request to swagger api</param>
        /// <returns>The requested url if authenticated, otherwise the authentication url for login</returns>
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (IsSwagger(request) && !Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.Redirect);
                // Redirect to login URL
                string uri = string.Format("{0}://{1}", request.RequestUri.Scheme, request.RequestUri.Authority);
                response.Headers.Location = new Uri(uri);
                return Task.FromResult(response);
            }
            else
            {
                return base.SendAsync(request, cancellationToken);
            }
        }

        private bool IsSwagger(HttpRequestMessage request)
        {
            return request.RequestUri.PathAndQuery.Contains("/swagger");
        }
    }
}