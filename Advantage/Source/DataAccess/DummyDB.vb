Option Explicit On 

Public Class DummyDB
    'Public Sub AddDummy(ByVal DummyInfo As DummyInfo)
    '    Dim db As New DataAccess
    '    Dim strSQL As New StringBuilder
    '    With strSQL
    '        .Append("INSERT INTO syDummy")
    '        .Append("(DummyId, Descrip, DummyCode, StatusId, StateId, CampGrpId, ModUser, ModDate) ")
    '        .Append("VALUES(?,?,?,?,?,?,?,?)")
    '    End With
    '    db.AddParameter("@DummyId", DummyInfo.DummyId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@Descrip", DummyInfo.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@DummyCode", DummyInfo.DummyCode, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
    '    db.AddParameter("@StatusId", DummyInfo.Status, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@StateId", DummyInfo.StateId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@CampGrpId", DummyInfo.CampGrpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@ModUser", DummyInfo.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@ModDate", Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '    Try
    '        db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    db.ClearParameters()
    '    strSQL.Remove(0, strSQL.Length)

    'End Sub
    'Public Sub UpdateDummy(ByVal DummyInfo As DummyInfo)
    '    ' First update the header record
    '    Dim db As New DataAccess
    '    Dim strSQL As New StringBuilder
    '    With strSQL
    '        .Append("UPDATE syDummy SET ")
    '        .Append("Descrip = ?")
    '        .Append(",DummyCode = ?")
    '        .Append(",StatusId = ?")
    '        .Append(",StateId = ?")
    '        .Append(",CampGrpId = ?")
    '        .Append(",ModUser = ?")
    '        .Append(",ModDate = ?")
    '        .Append(" WHERE DummyId = ? ")
    '    End With


    '    db.AddParameter("@Descrip", DummyInfo.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@DummyCode", DummyInfo.DummyCode, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
    '    db.AddParameter("@StatusId", DummyInfo.Status, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@StateId", DummyInfo.StateId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@CampGrpId", DummyInfo.CampGrpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@ModUser", DummyInfo.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@ModDate", Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '    db.AddParameter("@DummyId", DummyInfo.DummyId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    Try
    '        db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    db.ClearParameters()
    '    strSQL.Remove(0, strSQL.Length)
    'End Sub
    'Public Sub DeleteDummy(ByVal DummyId As Guid)
    '    ' Next delete the header record
    '    Dim db As New DataAccess
    '    Dim strSQL As New StringBuilder
    '    With strSQL
    '        .Append("DELETE FROM syDummy WHERE DummyId = ?")
    '    End With
    '    db.AddParameter("@DummyId", DummyId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    Try
    '        db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    db.ClearParameters()
    '    strSQL.Remove(0, strSQL.Length)

    'End Sub
    'Public Function GetDummyDetails(ByVal DummyId As Guid) As DummyInfo
    '    ' We need to get all of the saPayments and saRefunds for this particular StudentAwardId
    '    ' Read the Refunds table and the payments table and merge the results together into one result set
    '    Dim db As New DataAccess
    '    Dim strSQL As New StringBuilder

    '    With strSQL
    '        .Append(" SELECT DummyId, Descrip, DummyCode, StatusId, StateId, CampGrpId, ModUser, ModDate")
    '        .Append(" FROM syDummy")
    '        .Append(" WHERE DummyId = ? ")
    '    End With
    '    db.OpenConnection()
    '    db.AddParameter("@DummyId", DummyId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    '   Execute the query
    '    Dim dr As OleDbDataReader = db.RunParamSQLDataReader(strSQL.ToString)

    '    Dim DummyInfo As New DummyInfo

    '    While dr.Read()

    '        '   set properties with data from DataReader
    '        With DummyInfo
    '            .DummyId = dr("DummyId")
    '            .Descrip = dr("Descrip")
    '            .DummyCode = dr("DummyCode")
    '            .Status = dr("StatusId")
    '            .StateId = dr("StateId")
    '            .CampGrpId = dr("CampGrpId")
    '            .ModUser = dr("ModUser")
    '            .ModDate = dr("ModDate")
    '        End With

    '    End While

    '    db.ClearParameters()
    '    strSQL.Remove(0, strSQL.Length)

    '    If Not dr.IsClosed Then dr.Close()
    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    Return DummyInfo
    'End Function
    'Public Function PopulateDataList(ByVal ActiveStatus As Guid) As DataSet
    '    Dim db As New DataAccess
    '    Dim ds As New DataSet
    '    Dim da As New OleDbDataAdapter
    '    Dim strSQLString As New StringBuilder

    '    With strSQLString
    '        .Append("SELECT DummyId, Descrip")
    '        .Append(" FROM syDummy")
    '        .Append(" WHERE StatusId = ?")
    '        .Append(" ORDER By Descrip")
    '    End With
    '    db.OpenConnection()
    '    db.AddParameter("@StatusId", ActiveStatus, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    Try
    '        da = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da.Fill(ds, "DummyList")
    '        Return ds
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try

    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    'End Function
    'Public Function PopulateDropDown(ByVal ActiveStatus As Guid) As DataSet
    '    Dim db As New DataAccess
    '    Dim ds As New DataSet
    '    Dim da As New OleDbDataAdapter
    '    Dim strSQLString As New StringBuilder

    '    With strSQLString
    '        .Append("SELECT StateId, StateDescrip ")
    '        .Append(" FROM syStates ")
    '        .Append(" WHERE StatusId = ? ")
    '    End With
    '    db.OpenConnection()
    '    db.AddParameter("@StatusId", ActiveStatus, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    Try
    '        da = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da.Fill(ds, "StateList")
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    db.ClearParameters()
    '    strSQLString.Remove(0, strSQLString.Length)

    '    ' add any additional drop downlists here
    '    Dim da2 As New OleDbDataAdapter

    '    With strSQLString
    '        .Append("SELECT StatusId, Status ")
    '        .Append(" FROM syStatuses")
    '    End With
    '    db.OpenConnection()
    '    Try
    '        da2 = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da2.Fill(ds, "StatusList")
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try

    '    db.ClearParameters()
    '    strSQLString.Remove(0, strSQLString.Length)

    '    Dim da3 As New OleDbDataAdapter

    '    With strSQLString
    '        .Append("SELECT CampGrpId, CampGrpDescrip ")
    '        .Append(" FROM syCampGrps")
    '    End With
    '    db.OpenConnection()
    '    Try
    '        da3 = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da3.Fill(ds, "CampGrpList")
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try

    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    Return ds
    'End Function
End Class
