Imports FAME.Advantage.Common

Public Class EntranceTestDB
    'Public Function GetMinScore(ByVal TestId As String) As Integer
    '    'all updates must be encapsulated as one transaction
    '    Dim db As New DataAccess


    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

    '    Dim intTestExists As Integer
    '    Try
    '        Dim sb As New StringBuilder
    '        With sb
    '            .Append("select MinScore from adEntrTests where EntrTestId=? ")
    '        End With
    '        db.AddParameter("@EntrTestId", TestId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
    '        Dim intMinScore As Integer
    '        While dr.Read()
    '            intMinScore = dr("MinScore")
    '        End While

    '        If Not dr.IsClosed Then dr.Close()

    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)
    '        Return intMinScore
    '    Finally
    '    End Try
    'End Function
    'Public Function GetTestDS(ByVal showActiveOnly As String, ByVal PrgVerId As String) As DataSet

    '    '   create dataset
    '    Dim ds As New DataSet


    '    'Get All The Test Details 
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("SELECT ")
    '        .Append("       RSD.PrgVerTestDetailId, ")
    '        .Append("       RSD.PrgVerTestId, ")
    '        .Append("       RSD.MinScore, ")
    '        .Append("       RSD.EntrTestId, ")
    '        .Append("       RSD.ViewOrder,ST.StatusId,ST.Status, ")
    '        .Append("   RSD.Required as Required, ")
    '        .Append(" RSD.Required as EditRequired ")
    '        .Append("FROM   adPrgVerTestDetails RSD, adPrgVerTest RS,arPrgVersions PV,syStatuses ST,adEntrTests ET ")
    '        .Append("WHERE  RSD.PrgVerTestId=RS.PrgVerTestId and RS.PrgVerId = PV.PrgVerID and Pv.StatusId = ST.StatusId ")
    '        .Append(" and RSD.EntrTestId = ET.EntrTestId and ET.STatusId = ST.StatusId  ")
    '        If showActiveOnly = "True" Then
    '            .Append("AND    ST.Status = 'Active' ")
    '            .Append("ORDER BY PV.PrgVerDescrip ")
    '        ElseIf showActiveOnly = "False" Then
    '            .Append("AND    ST.Status = 'Inactive' ")
    '            .Append("ORDER BY PV.PrgVerDescrip ")
    '        Else
    '            .Append("ORDER BY ST.Status,PV.PrgVerDescrip asc")
    '        End If
    '    End With

    '    '   build select command
    '    Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(GetAdvAppSettings.AppSettings("ConString")))

    '    '   Create adapter to handle RateScheduleDetails table
    '    Dim da As New OleDbDataAdapter(sc)

    '    ' Fill TestDetails Table
    '    da.Fill(ds, "TestDetails")


    '    'Build Query For Test Table
    '    sb = New StringBuilder
    '    With sb
    '        '   with subqueries
    '        .Append("SELECT RS.PrgVerTestId, ")
    '        .Append("    RS.PrgVerId, ")
    '        .Append(" RS.PrevEduId, ")
    '        '.Append(" (Select EdLvlDescrip from adEdLvls where EdLvlId = Rs.PrevEduId) as Education, ")
    '        .Append(" (Select  Case  when Rs.PrevEduId is NULL then 'Any' else (Select EdLvlDescrip from adEdLvls where EdLvlId = Rs.PrevEduId) End) as Education, ")
    '        .Append("    RS.ModUser, ")
    '        .Append("    RS.ModDate, ")
    '        .Append(" PV.PrgVerDescrip,ST.StatusId,ST.Status ")
    '        .Append(" FROM adPrgVerTest RS,arPrgVersions PV,syStatuses ST ")
    '        .Append(" where RS.PrgVerId = Pv.PrgVerId and PV.StatusId = ST.StatusId ")
    '        If showActiveOnly = "True" Then
    '            .Append("AND    ST.Status = 'Active' ")
    '            .Append("ORDER BY PV.PrgVerDescrip ")
    '        ElseIf showActiveOnly = "False" Then
    '            .Append("AND    ST.Status = 'Inactive' ")
    '            .Append("ORDER BY PV.PrgVerDescrip ")
    '        Else
    '            .Append("ORDER BY ST.Status,PV.PrgVerDescrip asc")
    '        End If
    '    End With


    '    '   modify select command
    '    da.SelectCommand.CommandText = sb.ToString

    '    '   fill saRateSchedules table
    '    da.Fill(ds, "Test")

    '    'Build Query For Categories table
    '    sb = New StringBuilder
    '    With sb
    '        .Append("SELECT   TC.EntrTestId, TC.StatusId, TC.EntrTestDescrip,ST.StatusId,ST.Status ")
    '        .Append("FROM     adEntrTests TC, syStatuses ST ")
    '        .Append("WHERE    TC.StatusId = ST.StatusId ")
    '        If showActiveOnly = "True" Then
    '            .Append("AND    ST.Status = 'Active' ")
    '            .Append("ORDER BY TC.EntrTestDescrip ")
    '        ElseIf showActiveOnly = "False" Then
    '            .Append("AND    ST.Status = 'Inactive' ")
    '            .Append("ORDER BY TC.EntrTestDescrip ")
    '        Else
    '            .Append("ORDER BY ST.Status,TC.EntrTestDescrip asc")
    '        End If
    '    End With


    '    '   modify select command
    '    da.SelectCommand.CommandText = sb.ToString

    '    '   fill TuitionCategories table
    '    da.Fill(ds, "TestCategories")

    '    'Set Primary Key For TestDetails Table
    '    Dim pk0(0) As DataColumn
    '    pk0(0) = ds.Tables("TestDetails").Columns("PrgVerTestDetailId")
    '    ds.Tables("TestDetails").PrimaryKey = pk0


    '    'set primary key for Test Table
    '    Dim pk1(0) As DataColumn
    '    pk1(0) = ds.Tables("Test").Columns("PrgVerTestId")
    '    ds.Tables("Test").PrimaryKey = pk1

    '    'set primary key for TestCategories table
    '    Dim pk2(0) As DataColumn
    '    pk2(0) = ds.Tables("TestCategories").Columns("EntrTestId")
    '    ds.Tables("TestCategories").PrimaryKey = pk2

    '    'set foreign key column in saRateScheduleDetails
    '    Dim fk0(0) As DataColumn
    '    fk0(0) = ds.Tables("TestDetails").Columns("PrgVerTestId")

    '    'set foreign key column in saRateScheduleDetails
    '    Dim fk1(0) As DataColumn
    '    fk1(0) = ds.Tables("TestDetails").Columns("EntrTestId")


    '    'add relationship between test table and testdetails table (prgVerTestId column)
    '    ds.Relations.Add("RateSchedulesRateScheduleDetails", pk1, fk0)

    '    'add relationship between testcategories table and testdetails table(prgVerTestId column)
    '    ds.Relations.Add("TuitionCategoriesRateScheduleDetails", pk2, fk1)

    '    Return ds
    'End Function
    'Public Function GetDataListDS(ByVal showActiveOnly As String) As DataSet
    '    '   create dataset
    '    Dim ds As New DataSet
    '    Dim sb As StringBuilder


    '    sb = New StringBuilder
    '    '   build select command
    '    Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(GetAdvAppSettings.AppSettings("ConString")))

    '    '   Create adapter to handle RateScheduleDetails table
    '    Dim da As New OleDbDataAdapter(sc)
    '    With sb
    '        '   with subqueries
    '        .Append("SELECT RS.PrgVerTestId, ")
    '        .Append("    RS.PrgVerId, ")
    '        .Append(" RS.PrevEduId, ")
    '        .Append(" (Select EdLvlDescrip from adEdLvls where EdLvlId = Rs.PrevEduId) as Education, ")
    '        .Append("    RS.ModUser, ")
    '        .Append("    RS.ModDate, ")
    '        .Append(" PV.PrgVerDescrip,ST.StatusId,ST.Status ")
    '        .Append(" FROM adPrgVerTest RS,arPrgVersions PV,syStatuses ST ")
    '        .Append(" where RS.PrgVerId = Pv.PrgVerId and PV.StatusId = ST.StatusId ")
    '        If showActiveOnly = "True" Then
    '            .Append("AND    ST.Status = 'Active' ")
    '            .Append("ORDER BY PV.PrgVerDescrip ")
    '        ElseIf showActiveOnly = "False" Then
    '            .Append("AND    ST.Status = 'Inactive' ")
    '            .Append("ORDER BY PV.PrgVerDescrip ")
    '        Else
    '            .Append("ORDER BY ST.Status,PV.PrgVerDescrip asc")
    '        End If
    '    End With


    '    '   modify select command
    '    da.SelectCommand.CommandText = sb.ToString

    '    '   fill saRateSchedules table
    '    da.Fill(ds, "Test")

    'End Function
    'Public Function CheckTestExistForPrgVersionAndEducation(ByVal PrgVerId As String, ByVal PrevEduId As String) As Integer
    '    'all updates must be encapsulated as one transaction
    '    Dim db As New DataAccess


    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

    '    Dim intTestExists As Integer
    '    Try
    '        Dim sb As New StringBuilder
    '        With sb
    '            .Append("select count(*) as TestExists from adPrgVerTest where PrgVerId=? and PrevEduId=? ")
    '        End With
    '        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@PrevEduId", PrevEduId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        intTestExists = db.RunParamSQLScalar(sb.ToString)
    '        Return intTestExists
    '    Finally
    '    End Try
    'End Function
    'Public Function UpdateTestDS(ByVal ds As DataSet) As String


    '    'all updates must be encapsulated as one transaction
    '    Dim connection As New OleDbConnection(GetAdvAppSettings.AppSettings("ConString"))
    '    connection.Open()
    '    Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

    '    Try
    '        'build the sql query for the RateScheduleDetails data adapter
    '        Dim sb As New StringBuilder
    '        With sb
    '            .Append("SELECT ")
    '            .Append("       RSD.PrgVerTestDetailId, ")
    '            .Append("       RSD.PrgVerTestId, ")
    '            .Append("       RSD.MinScore, ")
    '            .Append("       RSD.EntrTestId, ")
    '            .Append("       RSD.ViewOrder,RSD.Required ")
    '            .Append("FROM   adPrgVerTestDetails RSD ")
    '        End With

    '        'build select command
    '        Dim RateScheduleDetailsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

    '        'Create adapter to handle RateScheduleDetails table
    '        Dim RateScheduleDetailsDataAdapter As New OleDbDataAdapter(RateScheduleDetailsSelectCommand)

    '        'Build insert, update and delete commands for RateScheduleDetails table
    '        Dim cb As New OleDbCommandBuilder(RateScheduleDetailsDataAdapter)

    '        'Build select query for the RateSchedules data adapter
    '        sb = New StringBuilder
    '        With sb
    '            '   with subqueries
    '            .Append("SELECT RS.PrgVerTestId, ")
    '            .Append("    RS.PrgVerId, ")
    '            .Append(" RS.PrevEduId, ")
    '            .Append("    RS.ModUser, ")
    '            .Append("    RS.ModDate ")
    '            .Append("FROM  adPrgVerTest RS ")
    '        End With

    '        '   build select command
    '        Dim RateSchedulesSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

    '        '   Create adapter to handle RateSchedules table
    '        Dim RateSchedulesDataAdapter As New OleDbDataAdapter(RateSchedulesSelectCommand)

    '        '   build insert, update and delete commands for RateSchedules table
    '        Dim cb1 As New OleDbCommandBuilder(RateSchedulesDataAdapter)


    '        '   insert added rows in adPrgVerTest table
    '        RateSchedulesDataAdapter.Update(ds.Tables("Test").Select(Nothing, Nothing, DataViewRowState.Added))

    '        '   insert added rows in adPrgVerTestDetails table
    '        RateScheduleDetailsDataAdapter.Update(ds.Tables("TestDetails").Select(Nothing, Nothing, DataViewRowState.Added))

    '        '   delete rows in  adPrgVerTestDetails table
    '        RateScheduleDetailsDataAdapter.Update(ds.Tables("TestDetails").Select(Nothing, Nothing, DataViewRowState.Deleted))

    '        '   delete rows in adPrgVerTest table
    '        RateSchedulesDataAdapter.Update(ds.Tables("Test").Select(Nothing, Nothing, DataViewRowState.Deleted))

    '        '   update rows in RateScheduleDetails table
    '        RateScheduleDetailsDataAdapter.Update(ds.Tables("TestDetails").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

    '        '   update rows in RateSchedules table
    '        RateSchedulesDataAdapter.Update(ds.Tables("Test").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

    '        '   everything went fine - commit transaction
    '        groupTrans.Commit()

    '        '   return no errors
    '        Return ""

    '    Catch ex As OleDb.OleDbException
    '        '   rollback transaction
    '        groupTrans.Rollback()

    '        ' return error message
    '        Return DALExceptions.BuildErrorMessage(ex)
    '    Finally
    '        'close connection
    '        connection.Close()
    '    End Try
    'End Function
    'Public Function GetAllPrgVersionTest(ByVal showActiveOnly As Boolean) As DataSet

    '    Dim db As New DataAccess


    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append(" select t2.PrgVerId,t2.PrgVerDescrip,t1.PrgVerTestId,t1.PrevEduId ")
    '        .Append(" from adPrgVerTest t1,arPrgVersions t2,syStatuses t3 ")
    '        .Append("  where t1.PrgVerId = t2.PrgVerId and ")
    '        .Append("  t2.StatusId = t3.StatusId ")
    '        If showActiveOnly Then
    '            .Append(" AND t3.Status = 'Active' ")
    '        End If
    '    End With
    '    'return dataset
    '    Return db.RunSQLDataSet(sb.ToString)
    'End Function
    'Public Function UpdatePrgVerDocuments(ByVal PrgVerId As String, ByVal user As String, ByVal selectedDegrees() As String) As Integer
    '    '   connect to the database
    '    Dim db As New DataAccess


    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

    '    '   First we have to delete all existing selections
    '    '   build the query
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("DELETE FROM adPrgVerDocs WHERE PrgVerId = ? ")
    '    End With

    '    '   delete all selected items
    '    db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '    db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '    db.ClearParameters()
    '    sb.Remove(0, sb.Length)

    '    '   Insert one record per each Item in the Selected Group
    '    Dim i As Integer
    '    For i = 0 To selectedDegrees.Length - 1

    '        'build query
    '        With sb
    '            .Append("INSERT INTO adPrgVerDocs (PrgVerId, DocId,ModDate, ModUser) ")
    '            .Append("VALUES(?,?,?,?)")
    '        End With

    '        'add parameters
    '        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@DocId", DirectCast(selectedDegrees.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'execute query
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)
    '    Next
    '    Return 0
    '    'Close Connection
    '    db.CloseConnection()

    'End Function
    'Public Function GetPrgVerDocs(ByVal PrgVerId As String) As DataSet

    '    Dim db As New DataAccess


    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder

    '    With sb
    '        .Append(" Select A.DocumentId as DocumentId,A.DocumentDescrip as DocumentDescrip")
    '        .Append(" from cmDocuments A,adPrgVerDocs B where ")
    '        .Append(" A.DocumentId = B.DocId and B.PrgVerId= ? ")
    '        .Append(" Order By DocumentDescrip ")
    '    End With

    '    'Add Parameter
    '    db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


    '    'return dataset
    '    Return db.RunParamSQLDataSet(sb.ToString)

    '    db.CloseConnection()

    'End Function
    'Public Function GetAllAvailableDocuments(ByVal PrgVerId As String) As DataSet

    '    '   connect to the database
    '    Dim db As New DataAccess


    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder

    '    '   build the sql query
    '    With sb
    '        .Append("select CT.DocumentId,CT.DocumentDescrip ")
    '        .Append("FROM     cmDocuments CT,syStatuses ST ")
    '        .Append("WHERE    CT.StatusId = ST.StatusId ")
    '        .Append(" AND     ST.Status = 'Active' ")
    '        .Append(" and CT.DocumentID not in (select DocId from adPrgVerDocs where PrgVerId=?) ")
    '        .Append("ORDER BY CT.DocumentDescrip ")
    '    End With

    '    db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    'return dataset
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
#Region "Get AdvAppsetting for Manage Config entry"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
End Class
