Imports FAME.Advantage.Common

Public Class AMCPriorAttendanceDB
    Public Function GetStudentListing() As DataTable
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT DISTINCT t1.SSN,t2.LastName + ', ' + t2.FirstName AS Student ")
            .Append("FROM AMC_Attendance t1, arStudent t2 ")
            .Append("WHERE t1.SSN = t2.SSN ")
            .Append("ORDER BY Student ")
        End With

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function

    Public Function GetStudentCoursesWithAttendance(ByVal ssn As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT DISTINCT t1.CourseId,t2.CourseName + ' V' + t2.Version + ' (' + t2.CourseNo + ')' AS Course ")
            .Append("FROM AMC_Attendance t1, AMC_Courses t2 ")
            .Append("WHERE t1.CourseId = t2.CourseId ")
            .Append("AND t1.SSN = ? ")
            .Append("ORDER BY Course ")
        End With

        db.AddParameter("ssn", ssn, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function GetStudentAttendanceForCourse(ByVal ssn As String, ByVal courseId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT t1.AttendanceId,Sequence,Attendance ")
            .Append("FROM AMC_Attendance t1 ")
            .Append("WHERE SSN = ? ")
            .Append("AND CourseId = ? ")
            .Append("ORDER BY Sequence ")
        End With

        db.AddParameter("ssn", ssn, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("csid", courseId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Sub UpdateStudentAMCPriorAttendance(ByVal attId As Integer, ByVal attendance As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("UPDATE AMC_Attendance ")
            .Append("SET Attendance = ? ")
            .Append("WHERE AttendanceId = ? ")
        End With

        db.AddParameter("attendance", attendance, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("attid", attId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)

    End Sub
End Class
