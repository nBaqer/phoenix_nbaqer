
Imports System.Data.OleDb
Imports System.Data
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common.Tables
Imports FAME.AdvantageV1.DataAccess.FAME.DataAccessLayer

Public Class RegisterDB

    ''' <summary>
    ''' Application Stting for get some other differents values from config.
    ''' </summary>
    ''' <remarks></remarks>
    Private ReadOnly myAdvAppSettings As AdvAppSettings

    Sub New()
        myAdvAppSettings = AdvAppSettings.GetAppSettings()
    End Sub

    Public Function GetAllStatuses() As DataSet
        Dim ds As DataSet

        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.ClsSectStatusId,Descrip ")
                .Append("FROM syClsSectStatuses t1 ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetAllProgramsOfferedInCampus(ByVal campusId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("Select DISTINCT a.ProgId, a.ProgDescrip, ")
                .Append(" case when (select ShiftDescrip from arShifts where shiftid=a.shiftid) is null  then ")
                .Append(" a.ProgDescrip ")
                .Append(" else ")
                .Append("a.ProgDescrip + ' (' + (select ShiftDescrip from arShifts where shiftid=a.shiftid) + ')'  ")
                .Append(" end as ShiftDescrip  ")
                .Append(" from arPrograms a,  ")
                .Append("syStatuses c ")
                .Append("WHERE (CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = ? ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("AND    a.StatusId = c.StatusId ")
                .Append(" AND     c.Status = 'Active' ")
                .Append("ORDER BY a.ProgDescrip ")


            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@cmpId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "Programs")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function

    Public Function GetCurrentAndFutureTermsForProgram(ByVal progId As String, Optional ByVal campusid As String = "") As DataSet
        Dim ds As DataSet

        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.TermId,t1.TermDescrip,t1.StartDate ")
                .Append("FROM arTerm t1 , syStatuses t2   ")
                .Append("WHERE  t1.StatusId=t2.StatusId and t2.Status='Active' and t1.EndDate >= ? and t1.IsModule = 0 AND t1.ProgramVersionId IS NULL ")
                .Append("AND (t1.ProgId = ? or t1.ProgId is NULL) ")
                If Not campusid = "" Then
                    .Append("AND ( t1.CampGrpId in (Select Distinct CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append(" OR (CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')))")
                End If
                ''Query Modidfied By Saraswathi to show the terms which has externship courses past the end date of the term
                ''mantis 15439
                .Append(" Union ")
                .Append(" Select T1.TermId,T1.TermDescrip,t1.StartDate from arClassSections C,arTerm T1,arReqs R , syStatuses t2   where  ")
                .Append(" t1.StatusId=t2.StatusId and t2.Status='Active' and ")
                .Append(" T1.TermId = C.TermId AND t1.ProgramVersionId IS NULL ")
                .Append("  and (t1.ProgId = ?")
                .Append(" or t1.ProgId is NULL)")
                .Append(" and t1.IsModule = 0 ")
                .Append(" and R.ReqId=C.ReqId and R.IsExternShip=1 and C.EndDate>= getdate() ")
                If Not campusid = "" Then
                    .Append("AND ( t1.CampGrpId in (Select Distinct CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append(" OR (t1.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')))")
                End If
                .Append("ORDER BY t1.StartDate ")
            End With

            db.AddParameter("@edate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@progId", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not campusid = "" Then
                db.AddParameter("@campusid", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@progId", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not campusid = "" Then
                db.AddParameter("@campusid", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            db.CloseConnection()
            Return ds

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.Message)
        End Try

        'Return the 
    End Function

    Public Function GetCurrentAndFutureTermsForStudent(ByVal stuEnrollId As String, Optional ByVal campusid As String = "") As DataSet
        Dim ds As DataSet

        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.TermId,t1.TermDescrip ")
                .Append("FROM arTerm t1 , syStatuses t2   ")
                .Append("WHERE  t1.StatusId=t2.StatusId and t2.Status='Active' and t1.EndDate >= ? and t1.IsModule = 0 AND t1.ProgramVersionId IS NULL ")
                .Append("AND (t1.ProgId is NULL or ( ")
                .Append(" Progid =(select Progid from arPrograms where Progid =(select progid from arPrgVersions where prgverid= ")
                .Append(" (select PrgVerid from arStuEnrollments where stuEnrollid= ? ))) )) ")
                If Not campusid = "" Then
                    .Append(" AND ( t1.CampGrpId in (Select Distinct CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append(" OR (CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')))")
                End If
                .Append(" ORDER BY t1.StartDate ")
            End With

            db.AddParameter("@edate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not campusid = "" Then
                db.AddParameter("@campusid", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            db.CloseConnection()
            Return ds

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.Message)
        End Try

        'Return the 
    End Function
    Public Function PopulateClsSectDL(ByVal Term As String, ByVal Shift As String, ByVal CampusId As String, ByVal Course As String, Optional ByVal allTerm As String = "") As DataSet
        Dim ds As DataSet
        Try
            ds = GetClsSects(Term, Shift, CampusId, Course, allTerm)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return ds
    End Function

    Public Function GetClsSects(ByVal Term As String, ByVal Shift As String, ByVal CampusId As String, ByVal course As String, Optional ByVal allTerm As String = "") As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try
            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT a.ClsSectionId, a.ClsSection, a.ReqId, a.StartDate, a.EndDate, a.MaxStud,a.ShiftId, b.Descrip as Class, b.Code ")
                .Append("FROM arClassSections a, arReqs b ")
                .Append("WHERE a.ReqId = b.ReqId a.ProgramVersionDefinitionId IS NULL ")
                .Append("AND a.EndDate  > ? ")
                .Append("AND a.CampusId = ? ")
                If Term.ToString <> "" Then
                    .Append(" and (a.TermId = ?) ")
                End If
                If Shift.ToString <> "" Then
                    .Append(" and (a.ShiftId = ?) ")
                End If
                If course.ToString <> "" Then
                    .Append(" and (a.ReqId = ?) ")
                End If
                If allTerm = "NotTermModule" Then
                    .Append(" and 0 = (select IsModule from arTerm where TermId=a.TermId) ")
                ElseIf allTerm = "TermModule" Then
                    .Append(" and 1 = (select IsModule from arTerm where TermId=a.TermId) ")
                End If

            End With

            db.AddParameter("@edate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@cmpid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Term.ToString <> "" Then
                ' Add the PrgVerId and ChildId to the parameter list
                db.AddParameter("@TermId", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If Shift.ToString <> "" Then
                ' Add the PrgVerId and ChildId to the parameter list
                db.AddParameter("@ShiftId", Shift, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            If course.ToString <> "" Then
                ' Add the PrgVerId and ChildId to the parameter list
                db.AddParameter("@ReqId", course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            With sb
                .Append("Select a.TestId, count(*) as Cnt ")
                .Append("from arResults a, arClassSections b ")
                .Append("where(a.TestId = b.ClsSectionId) ")
                If Term.ToString <> "" Then
                    .Append("and b.TermId = ? ")
                End If
                If Shift.ToString <> "" Then
                    .Append("and b.ShiftId = ? ")
                End If
                If allTerm = "NotTermModule" Then
                    .Append(" and 0 = (select IsModule from arTerm where TermId=b.TermId) ")
                ElseIf allTerm = "TermModule" Then
                    .Append(" and 1 = (select IsModule from arTerm where TermId=b.TermId) ")
                End If
                ''Added by Saraswathi lakshmanan on March 16 2009
                ''to fix mantis 15478: BUG: Several courses show more students registered than there actually are. 
                .Append(" and a.ResultId not in (select ResultId from arResults R, arGradeSystemDetails GSD where R.GrdSysDetailId=GSD.GrdSysDetailId ")
                .Append(" and GSD.IsDrop=1 and TestId=a.TestID) ")
                .Append("group by a.TestId ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            If Term.ToString <> "" Then
                db.AddParameter("@TermId", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            ' Add the PrgVerId and ChildId to the parameter list
            If Shift.ToString <> "" Then
                db.AddParameter("@shift", Shift, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "RemStdInfo")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try
        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetClsSections(ByVal progId As String, ByVal term As String, ByVal shift As String, ByVal campusId As String, ByVal course As String, Optional ByVal allTerm As String = "") As DataTable
        Dim dt As New DataTable
        Dim da As OleDbDataAdapter


        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT distinct a.ClsSectionId, a.ClsSection, a.ReqId, a.StartDate, a.EndDate, a.MaxStud,a.ShiftId, b.Descrip as Class, b.Code ")
            .Append("FROM arClassSections a, arReqs b,arProgVerDef c,arPrgVersions d,arPrograms e  ")
            .Append("WHERE a.ReqId = b.ReqId AND a.ProgramVersionDefinitionId IS NULL ")
            .Append("AND a.EndDate  >= ? ")
            .Append("AND a.CampusId = ? ")
            If term.ToString <> "" Then
                .Append(" and (a.TermId = ?) ")
            End If
            If shift.ToString <> "" Then
                .Append(" and (a.ShiftId = ?) ")
            End If
            If course.ToString <> "" Then
                .Append(" and (a.ReqId = ?) ")
            End If
            If allTerm = "NotTermModule" Then
                .Append(" and 0 = (select IsModule from arTerm where TermId=a.TermId) ")
            ElseIf allTerm = "TermModule" Then
                .Append(" and 1 = (select IsModule from arTerm where TermId=a.TermId) ")
            End If
            .Append(" and (b.Reqid=c.Reqid or b.reqid in(select reqid from dbo.arReqGrpDef where Grpid=c.reqid)) ")
            .Append(" and c.PrgVerId=d.PrgVerid and d.ProgId=e.ProgId and (a.ShiftId=e.Shiftid or e.shiftid is null) ")
            If progId <> "" Then
                .Append(" and e.Progid= ? ")
            End If
        End With

        db.AddParameter("@edate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If term.ToString <> "" Then
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@TermId", term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If shift.ToString <> "" Then
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ShiftId", shift, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        If course.ToString <> "" Then
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ReqId", course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If progId <> "" Then
            db.AddParameter("@ProgId", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        '   Execute the query
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(dt)
        Finally
            db.CloseConnection()
        End Try
        dt.TableName = "TermClsSects"
        Return dt

    End Function

    Public Function GetStudentsRegisterPerClass(ByVal term As String, ByVal shift As String) As DataTable
        Dim sb As New StringBuilder

        'With sb
        '    .Append(" SELECT section.ClsSectionId As TestId, COUNT(*) As Cnt FROM  ")
        '    .Append(" (  ")
        '    .Append(" SELECT  stu.StudentId, MAX(CAST(enroll.StuEnrollId AS VARCHAR(36))) EnrollID FROM dbo.arStuEnrollments enroll  ")
        '    .Append(" JOIN dbo.arStudent stu ON stu.StudentId = enroll.StudentId  ")
        '    .Append(" JOIN dbo.arResults res ON res.StuEnrollId = enroll.StuEnrollId  ")
        '    .Append(" JOIN dbo.syStatusCodes code ON code.StatusCodeId = enroll.StatusCodeId  ")
        '    .Append(" JOIN dbo.sySysStatus syssta ON syssta.SysStatusId = code.SysStatusId  ")
        '    .Append(" WHERE syssta.PostAcademics = 1 OR syssta.SysStatusId = 7  ")
        '    .Append(" GROUP BY stu.StudentId  ")
        '    .Append(" ) AS Table1  ")
        '    .Append(" JOIN dbo.arResults resu ON resu.StuEnrollId = Table1.EnrollID  ")
        '    .Append(" JOIN dbo.arClassSections section ON section.ClsSectionId = resu.TestId  ")
        '    .Append(" JOIN arTerm term ON term.TermId = section.TermId  ")
        '    '.Append(" JOIN dbo.arGradeSystemDetails details ON details.GrdSysDetailId = resu.GrdSysDetailId  ")
        '    .Append(" WHERE term.IsModule = 0  ")
        '    '.Append(" AND details.IsDrop = 0  ")
        '    .Append(" AND (@TermId = '00000000-0000-0000-0000-000000000000' OR term.TermId  = @TermId)  ")
        '    .Append(" AND (@Shift = '00000000-0000-0000-0000-000000000000' OR term.ShiftId  = @Shift)  ")
        '    .Append(" GROUP BY section.ClsSectionId  ")
        '    .Append(" ORDER BY Cnt  ")
        'End With


        With sb
            .Append(" SELECT T1.TestId  ")
            .Append("        ,COUNT(*) AS Cnt  ")
            .Append(" FROM    ( SELECT DISTINCT   enroll.StudentId  ")
            .Append("                    ,result.TestId  ")
            .Append("           FROM      dbo.arStudent stu  ")
            .Append("           JOIN      dbo.arStuEnrollments enroll ON enroll.StudentId = stu.StudentId  ")
            .Append("           JOIN      arResults result ON result.StuEnrollId = enroll.StuEnrollId  ")
            ' .Append(" WHERE result.DroppedInAddDrop = 0 ")
            .Append("         ) AS T1  ")
            .Append("   ")
            .Append(" JOIN    dbo.arClassSections section ON section.ClsSectionId = T1.TestId  ")
            .Append(" JOIN    arTerm term ON term.TermId = section.TermId  ")
            .Append(" WHERE   term.IsModule = 0  AND term.ProgramVersionId IS NULL ")
            .Append("        AND ( @TermId = '00000000-0000-0000-0000-000000000000' OR term.TermId = @TermId  ")
            .Append("            )  ")
            .Append("        AND ( @Shift = '00000000-0000-0000-0000-000000000000'  ")
            .Append("              OR term.ShiftId = @Shift  ")
            .Append("            )  ")
            .Append(" GROUP BY TestId  ")
            .Append(" ORDER BY Cnt  ")
        End With

        Dim conn As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim command As SqlCommand = New SqlCommand(sb.ToString(), conn)
        term = If(String.IsNullOrWhiteSpace(term), (Guid.Empty).ToString(), term)
        shift = If(String.IsNullOrWhiteSpace(shift), (Guid.Empty).ToString(), shift)
        command.Parameters.AddWithValue("@TermId", term)
        command.Parameters.AddWithValue("@Shift", shift)
        Dim dataAdapt As SqlDataAdapter = New SqlDataAdapter()
        dataAdapt.SelectCommand = command
        Dim dataTable As DataTable = New DataTable()
        conn.Open()
        Try
            dataAdapt.Fill(dataTable)
        Finally
            conn.Close()
        End Try
        dataTable.TableName = "RemStdInfo"
        Return dataTable

    End Function

    ''' <summary>
    ''' Get the class quorum for one specific class id 
    ''' </summary>
    ''' <param name="clsSectId">Class section id, it is also TestId in ArResult table</param>
    ''' <returns>Nothing is the class section does not exists, The ClassQuorumInfo otherwise </returns>
    ''' <remarks></remarks>
    Public Function GetClassQuorum(clsSectId As String) As ClassQuorumInfo
        Dim sb As New StringBuilder
        With sb
            .Append(" SELECT  s.ClsSectionId AS ClsSectionId ")
            .Append("        ,COUNT(TestId) AS RegisterStudent ")
            .Append("        ,s.MaxStud AS MaxStudentInClass ")
            .Append("        ,CASE WHEN ( COUNT(TestId) < s.MaxStud ) THEN 1 ")
            .Append("              ELSE 0 ")
            .Append("         END AS IsSeatAvailable ")
            .Append(" FROM    dbo.arClassSections s ")
            .Append(" LEFT JOIN dbo.arResults ON arResults.TestId = s.ClsSectionId ")
            .Append(" WHERE   s.ClsSectionId = @ClassSectionId ")
            .Append(" GROUP BY s.ClsSectionId ")
            .Append("        ,TestId ")
            .Append("        ,s.MaxStud ")
        End With

        Dim conn As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim command As SqlCommand = New SqlCommand(sb.ToString(), conn)
        command.Parameters.AddWithValue("@ClassSectionId", clsSectId)
        Dim info As ClassQuorumInfo = Nothing
        conn.Open()
        Try
            Dim reader As SqlDataReader = command.ExecuteReader()
            While reader.Read()
                info = New ClassQuorumInfo()
                info.ClsSectionId = reader("ClsSectionId").ToString()
                info.IsSeatAvailable = reader("IsSeatAvailable")
                info.MaxStudentInClass = reader("MaxStudentInClass")
                info.RegisterStudent = reader("RegisterStudent")
            End While
        Finally
            conn.Close()
        End Try

        Return info
    End Function

    Public Function GetAvailSelectStdsFrClsSect_SP(ByVal clsSectId As String, ByVal campusId As String) As DataSet
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Dim termId As String = GetTermForClassSection(clsSectId)

        If termId <> "00000000-0000-0000-0000-000000000000" Then
            db.AddParameter("@ProgID", New Guid(termId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If
        db.AddParameter("@ClsSectionID", New Guid(clsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@CampusID", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.OpenConnection()

        Try
            If termId <> "00000000-0000-0000-0000-000000000000" Then
                da = db.RunParamSQLDataAdapter_SP("dbo.USP_AR_RegSt_GetAvailableStudentsfortheProgAndClsSection")
            Else
                da = db.RunParamSQLDataAdapter_SP("dbo.USP_AR_RegSt_GetAvailableStudentsforAllProgsAndClsSection")
            End If
            da.Fill(ds, "AvailStds")
        Finally
            db.CloseConnection()
        End Try

        db.ClearParameters()
        db.AddParameter("@TestId", New Guid(clsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter_SP("dbo.USP_AR_RegStd_GetSelectedStudentsforClsId")
            da.Fill(ds, "SelectedStuds")

        Finally
            db.CloseConnection()
        End Try

        Return ds

    End Function

    Public Function GetTransfersForStudent_SP(ByVal stuEnrollId As String, ByVal clsSectId As String) As Boolean
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")


        db.AddParameter("@StuEnrollID", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        db.AddParameter("@clsSectionId", New Guid(clsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        db.OpenConnection()

        Try

            da = db.RunParamSQLDataAdapter_SP("dbo.GetStudentHasTransferGrade")

            da.Fill(ds, "TransferCounts")

            Dim transCount As Int32

            transCount = ds.Tables(0).Rows(0)("TransferCount")
            If transCount > 0 Then
                Return True
            Else
                Return False
            End If
        Catch
            Return False

        Finally
            db.CloseConnection()
        End Try


    End Function

    Public Function GetAvailSelectStdsFrClsSect(ByVal clsSectId As String, ByVal campusId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            Dim termId As String = GetTermForClassSection(clsSectId)

            With sb
                .Append("SELECT * FROM ( ")
                .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName,t1.StartDate, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t4.ReqId as ReqId, t3.Descrip As Req ")
                .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6 ")
                .Append("WHERE t1.StudentId=t5.StudentId AND ")
                .Append("t3.ReqId = t4.ReqId AND ")
                .Append("t1.PrgVerId = t4.PrgVerId AND ")
                .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? ")
                'Added to exclude the Continuing Ed program versions
                .Append("AND t6.IsContinuingEd = 0 ")
                If termId <> "00000000-0000-0000-0000-000000000000" Then
                    .Append(" and t6.ProgId = '" & termId & "'")
                End If



                .Append("UNION ")

                .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName,t1.StartDate, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t7.ReqId as ReqId, t3.Descrip As Req ")
                .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7 ")
                .Append("WHERE t1.StudentId=t5.StudentId AND ")
                .Append("t3.ReqId = t4.ReqId AND ")
                .Append("t1.PrgVerId = t4.PrgVerId AND ")
                .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")
                .Append("t3.ReqTypeId = 2 AND ")
                .Append("t4.ReqId = t7.GrpId ")
                'Added to exclude the Continuing Ed program versions
                .Append("AND t6.IsContinuingEd = 0 ")
                If termId <> "00000000-0000-0000-0000-000000000000" Then
                    .Append(" and t6.ProgId = '" & termId & "'")
                End If

                .Append("UNION ")

                .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName,t1.StartDate, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t8.ReqId as ReqId, t3.Descrip As Req ")
                .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7, arReqGrpDef t8 ")
                .Append("WHERE t1.StudentId=t5.StudentId AND ")
                .Append("t3.ReqId = t4.ReqId AND ")
                .Append("t1.PrgVerId = t4.PrgVerId AND ")
                .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")
                .Append("t3.ReqTypeId = 2 AND ")
                .Append("t4.ReqId = t7.GrpId AND ")
                .Append("t7.ReqId = t8.GrpId ")
                'Added to exclude the Continuing Ed program versions
                .Append("AND t6.IsContinuingEd = 0 ")
                If termId <> "00000000-0000-0000-0000-000000000000" Then
                    .Append(" and t6.ProgId = '" & termId & "'")
                End If

                .Append("UNION ")

                .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName,t1.StartDate, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t9.ReqId as ReqId, t3.Descrip As Req ")
                .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7, arReqGrpDef t8, arReqGrpDef t9 ")
                .Append("WHERE t1.StudentId=t5.StudentId AND ")
                .Append("t3.ReqId = t4.ReqId AND ")
                .Append("t1.PrgVerId = t4.PrgVerId AND ")
                .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")
                .Append("t3.ReqTypeId = 2 AND ")
                .Append("t4.ReqId = t7.GrpId AND ")
                .Append("t7.ReqId = t8.GrpId AND ")
                .Append("t8.ReqId = t9.GrpId ")
                'Added to exclude the Continuing Ed program versions
                .Append("AND t6.IsContinuingEd = 0 ")
                If termId <> "00000000-0000-0000-0000-000000000000" Then
                    .Append(" and t6.ProgId = '" & termId & "'")
                End If

                .Append("UNION ")

                .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName,t1.StartDate, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t10.ReqId as ReqId, t3.Descrip As Req ")
                .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7, arReqGrpDef t8, arReqGrpDef t9, arReqGrpDef t10 ")
                .Append("WHERE t1.StudentId=t5.StudentId AND ")
                .Append("t3.ReqId = t4.ReqId AND ")
                .Append("t1.PrgVerId = t4.PrgVerId AND ")
                .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")
                .Append("t3.ReqTypeId = 2 AND ")
                .Append("t4.ReqId = t7.GrpId AND ")
                .Append("t7.ReqId = t8.GrpId AND ")
                .Append("t8.ReqId = t9.GrpId AND ")
                .Append("t9.ReqId = t10.GrpId ")
                'Added to exclude the Continuing Ed program versions
                .Append("AND t6.IsContinuingEd = 0 ")
                If termId <> "00000000-0000-0000-0000-000000000000" Then
                    .Append(" and t6.ProgId = '" & termId & "'")
                End If

                .Append("UNION ")

                .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName,t1.StartDate, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t11.ReqId as ReqId, t3.Descrip As Req ")
                .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7, arReqGrpDef t8, arReqGrpDef t9, arReqGrpDef t10, arReqGrpDef t11 ")
                .Append("WHERE t1.StudentId=t5.StudentId AND ")
                .Append("t3.ReqId = t4.ReqId AND ")
                .Append("t1.PrgVerId = t4.PrgVerId AND ")
                .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")
                .Append("t3.ReqTypeId = 2 AND ")
                .Append("t4.ReqId = t7.GrpId AND ")
                .Append("t7.ReqId = t8.GrpId AND ")
                .Append("t8.ReqId = t9.GrpId AND ")
                .Append("t9.ReqId = t10.GrpId AND ")
                .Append("t10.ReqId = t11.GrpId ")
                'Added to exclude the Continuing Ed program versions
                .Append("AND t6.IsContinuingEd = 0 ")
                If termId <> "00000000-0000-0000-0000-000000000000" Then
                    .Append(" and t6.ProgId = '" & termId & "'")
                End If

                .Append("UNION ")

                .Append("SELECT distinct t1.StuEnrollId,t1.ShiftId, t5.LastName,t1.StartDate, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,t12.ReqId as ReqId, t3.Descrip As Req ")
                .Append("FROM arStuEnrollments t1, arReqs t3,arProgVerDef t4, arStudent t5, arPrgVersions t6, arReqGrpDef t7, arReqGrpDef t8, arReqGrpDef t9, arReqGrpDef t10, arReqGrpDef t11, arReqGrpDef t12 ")
                .Append("WHERE t1.StudentId=t5.StudentId AND ")
                .Append("t3.ReqId = t4.ReqId AND ")
                .Append("t1.PrgVerId = t4.PrgVerId AND ")
                .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")
                .Append("t3.ReqTypeId = 2 AND ")
                .Append("t4.ReqId = t7.GrpId AND ")
                .Append("t7.ReqId = t8.GrpId AND ")
                .Append("t8.ReqId = t9.GrpId AND ")
                .Append("t9.ReqId = t10.GrpId AND ")
                .Append("t10.ReqId = t11.GrpId AND ")
                .Append("t11.ReqId = t12.GrpId AND ")
                'Added to exclude the Continuing Ed program versions
                .Append("t6.IsContinuingEd = 0  ")
                If termId <> "00000000-0000-0000-0000-000000000000" Then
                    .Append(" and t6.ProgId = '" & termId & "'")
                End If

                '---------------------------------------------------------------------------------------------------

                '-- This section is the new union

                '---------------------------------------------------------------------------------------------------

                .Append("UNION ")


                .Append("SELECT distinct t1.StuEnrollId, t1.ShiftId,t5.LastName,t1.StartDate, t5.FirstName,t1.StatusCodeId, t1.ExpGradDate, t1.PrgVerId,t6.PrgVerDescrip,  ")

                .Append("(SELECT ReqId FROM arClassSections WHERE ClsSectionId = ?) AS ReqId, ")

                .Append("(SELECT t1.Descrip FROM arReqs t1, arClassSections t2 WHERE t1.ReqId = t2.ReqId AND t2.ClsSectionId = ?) AS Req ")

                .Append("FROM arStuEnrollments t1, arStudent t5, arPrgVersions t6 ")

                .Append("WHERE t1.StudentId=t5.StudentId AND ")

                .Append("t1.PrgVerId = t6.PrgVerId AND t1.CampusId = ? AND ")

                .Append("t6.IsContinuingEd = 1   ")
                If termId <> "00000000-0000-0000-0000-000000000000" Then
                    .Append(" and t6.ProgId = '" & termId & "'")
                End If

                .Append(")R5, arClassSections R6 ")




                '------------------------------------------------------------------------------------------------------

                '--New union code ends here

                '------------------------------------------------------------------------------------------------------


                .Append("WHERE R5.ReqId = R6.ReqId and R5.StartDate < R6.EndDate ")

                .Append(" and R5.StuEnrollId not in ")
                .Append("  (Select Distinct(t7.StuEnrollId)from arResults t7,arClassSections t8 ")
                .Append("   where t8.ReqId in (Select ReqId from arClassSections where ClsSectionId = ?) ")
                .Append("  and t7.TestId = t8.ClsSectionId and t7.GrdSysDetailId is NULL) ")

                .Append("and R5.StatusCodeId IN (Select StatusCodeId from syStatusCodes where SysStatusId IN (7,9,13,20)) ")

                .Append("and ((R5.ShiftId = R6.ShiftId) or (R5.ShiftId is Null and R6.ShiftId is NULL)) ")

                '.Append("and R5.StuEnrollId not in (Select Distinct StuEnrollId from arTransferGrades t9,arGradeSystemDetails t10 ")
                '.Append(" where t9.GrdSysDetailID=t10.GrdSysDetailID and t10.IsPass=1 and t9.ReqId in (Select ReqId from arClassSections where ClsSectionId = ?)) ")

                .Append("and R6.clsSectionId= ? ")

                'check for Equivalent Courses
                .Append(" and R6.ClsSectionId not in ")
                .Append(" (select ClsSectionId  from arClassSections where Reqid in( ")
                .Append(" select c.EquivReqid from arResults a,arClassSections b,arCourseEquivalent  c,arGradeSystemDetails d    where  ")
                .Append(" a.TestId = b.ClsSectionId And a.StuEnrollId = R5.StuEnrollId  and a.GrdSysDetailId=d.GrdSysDetailId and d.IsPass=1 ")
                .Append(" and b.Reqid=c.Reqid and c.EquivReqid =R5.ReqId )) ")

                .Append("order by R5.ExpGradDate ")

            End With
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@campusid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@campusid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@campusid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@campusid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@campusid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@campusid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@campusid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@ClsSectId8", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@ClsSectId8", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@campusid8", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@ClsSectId", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClsSectId3", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ' db.AddParameter("@ClsSectId2", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "AvailStds")
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            With sb
                '   build the sql query
                .Append(" Select distinct ")

                .Append("t1.StuEnrollId, ")

                .Append("t1.GrdSysDetailId, ")

                .Append("t1.ModUser, ")

                .Append("t5.LastName, ")

                .Append("t5.FirstName, ")

                .Append("t6.ExpGradDate, ")

                .Append("t7.ReqId ")

                .Append("FROM arResults t1, arStudent t5, arStuEnrollments t6, arClassSections t7 ")

                .Append("where   t1.TestId = ? ")

                .Append("and     t1.StuEnrollId = t6.StuEnrollId ")

                .Append("and     t6.StudentId = t5.StudentId ")

                .Append("and     t1.TestId = t7.ClsSectionId ")
                'Added on 3/31/2005 by Troy. This is to accommodate dropping courses during the add/drop period.
                'During this period no grade is recorded when a course is dropped. Ideally the record should
                'be deleted from the arResults table. However, if we actually deleted the record it would no
                'longer appear in the instructor's grade book. It is good to preserve the scores that were already
                'recorded in the grade book just in case the student decide to take up the course again.
                'The arResults table was modified to include a DroppedInAddDrop field. This defaults to false
                'but is set to true when a course is dropped during the add/drop period.
                .Append("and t1.DroppedInAddDrop = 0 ")
                'Added on 10/06/05 by Bhavana. This is to exclude students w/ drop grades.
                .Append("and t1.ResultId not in ")
                .Append("(select ResultId from arResults R, arGradeSystemDetails GSD where R.GrdSysDetailId=GSD.GrdSysDetailId and GSD.IsDrop=1 and TestId=?) ")
            End With


            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ClsSectId", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ClsSectId5", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "SelectedStuds")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetStdClsSects(ByVal StuEnrollId As String, ByVal ClsSectId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder


            With sb
                .Append("SELECT a.TestId, b.ClsSection, b.StartDate,b.EndDate  ")
                .Append("FROM arResults a, arClassSections b ")
                .Append("WHERE a.StuEnrollId = ? and a.TestId = b.ClsSectionId  and ")
                .Append("b.TermId in (Select b.TermId from arClassSections b where b.ClsSectionId = ?) ")
                'Modified by Michelle R. Rodriguez on 01/23/2006
                'Exclude class sections that have overriden conflicts
                .Append("and not exists (select * from arOverridenConflicts where leftClsSectionId = ? or rightClsSectionId = ?) ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClssectId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClssectId2", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClssectId3", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetStdClsSects_SP(ByVal StuEnrollId As String, ByVal ClsSectId As String) As DataSet
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        '   connect to the database
        Dim db As New SQLDataAccess
        Try



            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ClssectId", New Guid(ClsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter_SP("USP_AR_RegStd_GetStdClsSects")
            da.Fill(ds, "TermClsSects")



        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetStdClsSectsFrAllEnrollments(ByVal StuEnrollId As String, ByVal ClsSectId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            With sb
                .Append("SELECT a.TestId, b.ClsSection, b.ReqId,b.TermId,  ")
                .Append("(Select ReqId from arClassSections b where b.ClsSectionId = ? ) as CourseId, ")
                .Append(" (select Descrip from arReqs where ReqId=b.ReqId) as CourseDescrip ")
                .Append(" FROM arResults a, arClassSections b WHERE a.StuEnrollId in ")
                .Append("(Select StuEnrollId from arStuEnrollments where StudentId in ")
                .Append("(Select StudentId from arStuEnrollments where StuEnrollId = ?)) ")
                .Append("and a.TestId = b.ClsSectionId  and ")
                .Append("b.TermId in (Select b.TermId from arClassSections b where b.ClsSectionId = ?) ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ClssectId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClssectId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetCoursePrereqs(ByVal ClsSectId As String, ByVal PrgverId As String, ByVal Campusid As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try
            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            With sb

                .Append(" Select arReqs.ReqID as PreCoReqId from arReqs,arProgVerDef where arReqs.reqid in (   Select PreCoreqId from arCourseReqs b where   ")
                .Append(" b.ReqId in (Select a.ReqId from arClassSections a where a.ClsSectionId = ?) ")
                .Append(" and (prgverId is Null or  prgverid=?) ")
                .Append(" and b.CourseReqTypId = 1 )")
                .Append(" and  arReqs.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' and arReqs.ReqTypeId = 1  ")
                .Append(" And (arReqs.ReqId=arProgVerDef.ReqId  or arReqs.ReqId in ")
                .Append(" (select Reqid from arReqGrpDef where GrpId in(select Reqid from arProgVerDef where    prgverid= ?))) ")
                .Append(" and arProgVerDef.PrgVerId = ?")
                .Append(" AND arReqs.CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId =  ?) ")

            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ClsSectId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PrgverId", PrgverId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PrgverId", PrgverId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PrgverId", PrgverId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CampusID", Campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "CoursePrereqs")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetCoursePrereqs_Sp(ByVal ClsSectId As String, ByVal PrgverId As String, ByVal Campusid As String) As DataSet
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Dim db As New SQLDataAccess
        Try


            '   connect to the database

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")


            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ClsSectId", New Guid(ClsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@PrgverId", New Guid(PrgverId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@CampusID", New Guid(Campusid), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter_SP("USP_AR_GetCoursepreReqs")
            da.Fill(ds, "CoursePrereqs")

            'Close Connection


        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetPrereqClsSects(ByVal PreReqId As String, Optional ByVal stuEnrollId As String = "") As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder



            With sb
                .Append("Select b.PreCoReqId, c.ClsSectionId from arClassSections c, arCourseReqs b where c.ReqId = b.PreCoReqId ")
                .Append("and b.CourseReqTypId = 1 and b.PreCoReqId = ? ")
                If stuEnrollId <> "" Then
                    .Append("and c.StartDate >= (select ExpStartDate from arStuEnrollments where StuEnrollId=?) ")
                    .Append("and c.ShiftId=(select ShiftId from arStuEnrollments where StuEnrollId=?) ")
                    .Append("and c.CampusId=(select CampusId from arStuEnrollments where StuEnrollId=?) ")
                    .Append("and c.StartDate <= GetDate() ")

                End If
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@Prereq", PreReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If stuEnrollId <> "" Then
                db.AddParameter("@sid1", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@sid2", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@sid3", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "PrereqClsSects")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetPrereqClsSects_Sp(ByVal PreReqId As String, Optional ByVal stuEnrollId As String = "") As DataSet
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        '   connect to the database
        Dim db As New SQLDataAccess
        Try


            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            db.AddParameter("@PreReqID", New Guid(PreReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            If stuEnrollId <> "" Then
                db.AddParameter("@StuEnrollID", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            End If
            '   Execute the query
            db.OpenConnection()
            If stuEnrollId <> "" Then
                da = db.RunParamSQLDataAdapter_SP("USP_AR_GetPrereqClsSectswithStuEnrollid")
            Else
                da = db.RunParamSQLDataAdapter_SP("USP_AR_GetPrereqClsSectsWOutStuEnrollid")
            End If

            da.Fill(ds, "PrereqClsSects")



        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function AddStdToClsSect(ByVal storedGuid As String, ByVal StuEnrollId As String, ByVal user As String, ByVal CampusId As String)
        Dim db As New DataAccess

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try


            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            'First do a search for the "default grade" in the gradesystem details tbl to
            'get the guid for this particular default grade. Later insert this guid w/
            'the student record in arResults.

            'Insert row into syCampGrps table
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT INTO arResults(TestId,StuEnrollId,ModUser,ModDate) ")
                .Append("VALUES(?,?,?,?)")
            End With

            db.AddParameter("@campgrpid", storedGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@campusid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   apply fees
            Dim result As String = (New TransactionsDB).ApplyFeesByCourse(StuEnrollId, storedGuid, user, CampusId, Date.Today)

            '   commit transaction
            groupTrans.Commit()

            '   register Gradebook Components

            'The grade book components for the course need to be registered only for Ross
            'for other school record will be inserted in to this table when instructor 
            'posts grade
            'If SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "true" Or _
            'SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "yes" Then
            result = RegisterStudentForGradeBookComponents(StuEnrollId, storedGuid, user, Date.Now)
            'End If


            '   return with no errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            Else
                '   report an error to the client
                DALExceptions.BuildErrorMessage(ex)
            End If

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function GetStdGrade(ByVal StuEnrollId As String, ByVal ClsSectId As String, Optional ByVal CoReqId As String = "") As String
        Dim sGrade As String = String.Empty

        Try
            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            'Changed this function to include sub-query for StudentId & StuEnrollId to accomodate
            'situation when the Student did the course (and received a grade) in another enrollment.
            '#BN 05/06/05
            If CoReqId <> "" Then
                With sb
                    .Append("Select X.ABC ")
                    .Append("from ")
                    .Append("( ")
                    .Append("SELECT Coalesce ((Select top 1 b.Grade FROM arResults a, arGradeSystemDetails b ")
                    .Append("WHERE a.StuEnrollId = ?   ")
                    .Append("and a.TestId in  ")
                    .Append(" (Select c.ClsSectionId from arClassSections c where c.ReqId =  ?) and ")
                    .Append("b.GrdSysDetailId = a.GrdSysDetailId   order by a.moddate desc ),'')  as ABC ")
                    .Append("UNION ALL ")
                    .Append("Select Coalesce ((SELECT Top 1 b.Grade FROM arTransferGrades a, arGradeSystemDetails b,arTerm c ")
                    .Append("WHERE a.StuEnrollId = ? ")
                    .Append("and ")
                    .Append("a.ReqId = ? and ")
                    ''order by Changed by Saraswarthi Lakshmanan on July 15 2009
                    .Append(" b.GrdSysDetailId = a.GrdSysDetailId  and a.TermId=c.TermId order by c.StartDate desc,a.moddate desc),'') as ABC ")
                    .Append(" ) X where X.ABC <> '' ")
                End With
                ' Add the student enrollid and ClsSectId to the parameter list
                db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@req", CoReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StdId3", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@req2", CoReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                With sb
                    .Append("SELECT distinct b.Grade ")
                    .Append("FROM arResults a, arGradeSystemDetails b, arClassSections c ")
                    .Append(" WHERE a.StuEnrollId in  ")
                    .Append("(Select StuEnrollId from arStuEnrollments where StudentId in ") 'added 05/06/05
                    .Append(" (Select StudentId from arStuEnrollments where StuEnrollId = ?)) ")
                    '.Append("and a.TestId = ? ")
                    .Append(" and a.TestId = c.ClsSectionId ")
                    .Append(" and c.ReqId = (Select ReqId from arClassSections where ClsSectionId = ?) ")
                    .Append("and b.GrdSysDetailId = a.GrdSysDetailId")
                    .Append("UNION ALL ")
                    .Append("Select distinct b.Grade ")
                    .Append("FROM arTransferGrades a, arGradeSystemDetails b ")
                    .Append("WHERE a.StuEnrollId in  ")
                    .Append("(Select StuEnrollId from arStuEnrollments where StudentId in  ")
                    .Append("(Select StudentId from arStuEnrollments where StuEnrollId = ?)) ")
                    .Append("and a.ReqId in (Select ReqId from arClassSections where ClsSectionId = ?) ")
                    .Append("and b.GrdSysDetailId = a.GrdSysDetailId ")
                End With

                ' Add the student enrollid and ClsSectId to the parameter list
                db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@testId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StdId2", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@testId2", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            '   build the sql query

            '   Execute the query
            db.OpenConnection()

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            Dim rowcount As Integer = 0
            While dr.Read()
                If CoReqId <> "" Then
                    ''Modified by Saraswathi lakshmanan on july 14 2009
                    ''All the grades are added
                    rowcount = rowcount + 1
                    If rowcount > 1 Then
                        sGrade = sGrade + ";" + dr("ABC")
                    Else
                        sGrade = dr("ABC")
                    End If

                Else
                    sGrade = dr("Grade")
                End If
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return sGrade

    End Function

    Public Function GetStdGrade_Sp(ByVal StuEnrollId As String, ByVal ClsSectId As String, Optional ByVal CoReqId As String = "") As String
        Dim sGrade As String = String.Empty
        '   connect to the database
        Dim db As New SQLDataAccess
        Try

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            'Changed this function to include sub-query for StudentId & StuEnrollId to accomodate
            'situation when the Student did the course (and received a grade) in another enrollment.
            '#BN 05/06/05
            If CoReqId <> "" Then

                ' Add the student enrollid and ClsSectId to the parameter list
                db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@CoreqID", New Guid(CoReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            Else
                ' Add the student enrollid and ClsSectId to the parameter list
                db.AddParameter("@StuEnrollid", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@ClsSectID", New Guid(ClsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            End If
            '   build the sql query

            '   Execute the query
            '   db.OpenConnection()
            Dim dr As SqlDataReader
            If CoReqId <> "" Then
                dr = db.RunParamSQLDataReader_SP("USP_AR_RegStd_GetStudentGradewithCoreqID")
            Else
                dr = db.RunParamSQLDataReader_SP("USP_AR_RegStd_GetStudentGrade")
            End If
            'Execute the query


            Dim rowcount As Integer = 0
            While dr.Read()
                If CoReqId <> "" Then
                    ''All the grades are added
                    rowcount = rowcount + 1
                    If rowcount > 1 Then
                        sGrade = sGrade + ";" + dr("ABC")
                    Else
                        sGrade = dr("ABC")
                    End If

                Else
                    sGrade = dr("Grade")
                End If
            End While



        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return sGrade

    End Function

    Public Function GetStudentGrade(ByVal StuEnrollId As String, ByVal CoReqId As String) As String
        Dim sGrade As String = String.Empty

        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            'Changed this function to include sub-query for StudentId & StuEnrollId to accomodate
            'situation when the Student did the course (and received a grade) in another enrollment.
            '#BN 05/06/05

            With sb
                .Append("Select X.ABC ")
                .Append("from ")
                .Append("( ")
                .Append("SELECT Coalesce ((Select Top 1 b.Grade FROM arResults a, arGradeSystemDetails b ")
                .Append("WHERE a.StuEnrollId = ?   ")
                .Append("and a.TestId in  ")
                .Append(" (Select c.ClsSectionId from arClassSections c where c.ReqId =  ?) and ")
                .Append("b.GrdSysDetailId = a.GrdSysDetailId  order by a.moddate desc ),'')  as ABC ")
                .Append("UNION ALL ")


                .Append("Select Coalesce ((SELECT Top 1 b.Grade FROM arTransferGrades a, arGradeSystemDetails b,arTerm c   ")
                .Append("WHERE a.StuEnrollId = ? ")
                .Append("and ")
                .Append("a.ReqId = ? and ")
                ''order by Changed by Saraswarthi Lakshmanan on July 15 2009
                .Append(" b.GrdSysDetailId = a.GrdSysDetailId   and a.TermId=c.TermId order by c.StartDate desc,a.ModDate desc),'') as ABC ")
                .Append(" ) X where X.ABC <> '' ")
            End With
            ' Add the student enrollid and ClsSectId to the parameter list
            db.AddParameter("@StdId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@req", CoReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StdId3", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@req2", CoReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            '   build the sql query

            '   Execute the query
            db.OpenConnection()

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()

                sGrade = dr("ABC")

            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return sGrade

    End Function

    Public Function DoesPrgVerHaveGrdOverride(ByVal PrgVerId As String, ByVal ReqId As String) As String

        Dim count As String
        Dim sGrdSysDetailId As String

        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("Select count(*) as Count, GrdSysDetailId from arProgVerDef where ReqId = ? and PrgVerId = ? and GrdSysDetailId is not null ")
                .Append("GROUP BY GrdSysDetailId ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ReqId", ReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                sGrdSysDetailId = dr("GrdSysDetailId").ToString
                count = dr("Count").ToString
            End While

            'ds = db.RunParamSQLDataSet(sb.ToString)
            'sb.Remove(0, sb.Length)

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return sGrdSysDetailId & "," & count

    End Function

    Public Function DoesPrgVerHaveGrdOverride_SP(ByVal PrgVerId As String, ByVal ReqId As String) As String
        Dim count As String
        Dim sGrdSysDetailId As String
        Dim db As New SQLDataAccess
        Try


            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@PrgVerId", New Guid(PrgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ReqId", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)


            ' db.OpenConnection()
            'Execute the query
            Dim dr As SqlDataReader = db.RunParamSQLDataReader_SP("USP_AR_RegStd_DoesPrgVerHaveGrdOverride")

            While dr.Read()
                sGrdSysDetailId = dr("GrdSysDetailId").ToString
                count = dr("Count").ToString
            End While

            'Close Connection
            '   db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return sGrdSysDetailId & "," & count

    End Function

    Public Function GetHigherGrades(ByVal GrdSysDetailId As String) As String
        Dim sGrade As String = String.Empty
        Try
            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("select Grade from arGradeSystemDetails where Vieworder <= ")
                .Append("((Select b.ViewOrder from arGradeSystemDetails b where b.GrdSystemId in ")
                .Append("(Select Distinct a.GrdSystemId from arGradeSystemDetails a where a.GrdSysDetailId = ?) ")
                .Append("and b.GrdSysDetailId = ?)) ")
                .Append("and GrdSystemId in  ")
                .Append("(Select Distinct a.GrdSystemId from arGradeSystemDetails a where a.GrdSysDetailId = ?) ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@GrdSysDetailId", GrdSysDetailId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@GrdSysDetailId", GrdSysDetailId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@GrdSysDetailId", GrdSysDetailId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)


            While dr.Read()
                sGrade = sGrade & dr("Grade") & ","
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return sGrade

    End Function

    Public Function GetHigherGrades_Sp(ByVal grdSysDetailId As String) As String
        Dim sGrade As String = String.Empty
        Dim db As New SQLDataAccess
        Try
            '   connect to the database

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@GrdSysDetailId", New Guid(grdSysDetailId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            'Execute the query
            Dim dr As SqlDataReader = db.RunParamSQLDataReader_SP("USP_AR_RegStd_GetHigherGrades")


            While dr.Read()
                sGrade = sGrade & dr("Grade") & ","
            End While


            'Close Connection


        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return sGrade

    End Function

    Public Function IsPassingGrade(ByVal Grade As String, ByVal PrgVerId As String) As Integer

        Dim isPass As Integer
        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                ''All the grades are passed and if any one of the grade is ispass then, it is considered to be ispass
                ''fixed for mantis case 16830
                If Grade.Contains(";") Then
                    Dim grades() As String = Grade.Split(";")
                    Dim leng As Integer = grades.Length
                    Dim strGrade As String = ""
                    Dim i As Integer
                    If leng > 0 Then

                        For i = 0 To leng - 1
                            strGrade = strGrade + "'" + grades(i) + "',"
                        Next
                        strGrade = strGrade.Substring(0, strGrade.Length - 1)
                    End If

                    .Append("select IsPass from arGradeSystemDetails where Grade in (" + strGrade + ") and ")
                    .Append("GrdSystemID in (select GrdSystemId from arPrgVersions where PrgVerId=?) order by ispass desc ")

                    db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    .Append("select IsPass from arGradeSystemDetails where Grade=? and ")
                    .Append("GrdSystemID in (select GrdSystemId from arPrgVersions where PrgVerId=?) ")
                    db.AddParameter("@Grade", Grade, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If


            End With

            ' Add the PrgVerId and ChildId to the parameter list


            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)


            While dr.Read()
                If (dr("IsPass") = True) Then
                    isPass = 1
                    Exit While
                Else
                    isPass = 0
                End If

            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return isPass

    End Function

    Public Function IsPassingGrade_SP(ByVal Grade As String, ByVal PrgVerId As String) As Integer

        Dim isPass As Integer
        '   connect to the database
        Dim db As New SQLDataAccess
        Try

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim strGrade As String = ""
            ''All the grades are passed and if any one of the grade is ispass then, it is considered to be ispass
            ''fixed for mantis case 16830
            If Grade.Contains(";") Then
                Dim grades() As String = Grade.Split(";")
                Dim leng As Integer = grades.Length

                Dim i As Integer
                If leng > 0 Then
                    For i = 0 To leng - 1
                        strGrade = strGrade + "'" + grades(i) + "',"
                    Next
                    strGrade = strGrade.Substring(0, strGrade.Length - 1)
                End If
            Else
                strGrade = "'" + Grade + "'"
            End If
            db.AddParameter("@Grade", Grade, SqlDbType.VarChar, , ParameterDirection.Input)
            db.AddParameter("@PrgVerId", New Guid(PrgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            '  db.OpenConnection()
            'Execute the query
            Dim dr As SqlDataReader = db.RunParamSQLDataReader_SP("USP_AR_RegStd_IsPassingGrade")


            While dr.Read()
                If (dr("IsPass") = True) Then
                    isPass = 1
                    Exit While
                Else
                    isPass = 0
                End If

            End While

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return isPass

    End Function

    Public Function DoesReqHaveCoReqs(ByVal ReqId As String) As Integer

        Dim rowCount As Integer
        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("Select count(*) as Count from arCourseReqs a where a.ReqId = ? and CourseReqTypId = 2 ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@PrgVerId", ReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try

        'Return the datatable in the dataset
        Return rowCount

    End Function

    Public Function DoesReqHaveCoReqs_Sp(ByVal reqId As String) As Integer
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim coreqs As Integer

        Try
            db.AddParameter("@ReqId", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet("dbo.USP_AR_DoesReqhaveCoReqs", Nothing, "SP")
            coreqs = ds.Tables(0).Rows(0)(0).ToString()
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw New BaseException(ex.Message)
            Else
                Throw New BaseException(ex.InnerException.ToString)
            End If
        Finally
            db.Dispose()
        End Try

        Return coreqs

    End Function
    Public Function GetCourseCoreqs(ByVal ReqId As String, ByVal TermId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("Select a.PreCoReqId, b.descrip from arCourseReqs a, arReqs b ")
                .Append("where a.CourseReqTypId = 2  and a.ReqId = ? and a.PreCoReqId = b.ReqId ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ReqId", ReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "CourseCoreqs")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetCourseCoreqs_Sp(ByVal reqId As String, ByVal termId As String) As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet

        Try
            db.AddParameter("@ReqId", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet("dbo.USP_AR_GetCourseCoReqs", Nothing, "SP")
        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            db.Dispose()
        End Try

        Return ds

    End Function

    Public Function GetCoreqClsSects_SP(ByVal ReqId As String, ByVal TermId As String) As DataSet
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Dim db As New SQLDataAccess
        Try


            '   connect to the database

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ReqId", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@TermId", New Guid(TermId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter_SP("USP_AR_GetCoreqClsSects")
            da.Fill(ds, "CoreqClsSects")

            'Close Connection


        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function IsCourseComplete(ByVal stuEnrollId As String, ByVal clsSectId As String, Optional ByVal coReqId As String = "") As Boolean

        Dim rowCount As Integer

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        If (coReqId <> "") Then
            With sb
                .Append("Select Sum(Num) ")
                .Append("from ")
                .Append("(Select Coalesce((Select count(*) as Count from arResults a where a.StuEnrollId in ")
                .Append("(Select StuEnrollId from arStuEnrollments where StudentId in ")
                .Append("(Select StudentId from arStuEnrollments where StuEnrollId = ?)) ")
                .Append("and a.TestId in (Select c.ClsSectionId from arClassSections c where c.ReqId = ?)),0) as Num ")

                .Append("union all ")

                .Append("Select Coalesce((Select count(*) as Count from arTransferGrades t9 where ")
                .Append("t9.StuEnrollId = ? and ")
                .Append("t9.ReqId = ?),0) as Num ")
                .Append(" )R1 ")
            End With

            ' Add the StdId and ClsSectId to the parameter list
            db.AddParameter("@Std", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CoReqId", coReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Std2", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CoReqId2", coReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Else
            With sb
                .Append("SELECT Sum(Num) from " & vbCrLf)
                .Append("( " & vbCrLf)
                .Append("Select Coalesce((Select count(*) as Count from arResults a, arClassSections b where a.StuEnrollId in " & vbCrLf)
                .Append("(Select StuEnrollId from arStuEnrollments where StudentId in " & vbCrLf)
                .Append("(Select StudentId from arStuEnrollments where StuEnrollId = ?)) " & vbCrLf)
                .Append("and a.TestId=b.ClsSectionId " & vbCrLf)
                .Append("and a.IsCourseCompleted = 1 " & vbCrLf)
                .Append("and b.ReqId = (Select ReqId from arClassSections where ClsSectionId = ?)),0 )as Num " & vbCrLf)

                .Append("union all " & vbCrLf)

                .Append("Select Coalesce((Select count(*) as Count from arTransferGrades t9 where " & vbCrLf)
                .Append("t9.StuEnrollId = ? and " & vbCrLf)
                .Append("t9.IsCourseCompleted = 1 and " & vbCrLf)
                .Append("t9.ReqId in (Select ReqId from arClassSections where ClsSectionId = ?)),0)as Num " & vbCrLf)
                .Append(")R1 " & vbCrLf)
            End With


            ' Add the StdId and ClsSectId to the parameter list

            db.AddParameter("@Std", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClsSectId", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Std3", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClsSectId2", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        db.OpenConnection()
        Try

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)

        Finally
            If Not (db Is Nothing) Then
                db.CloseConnection()
            End If

        End Try

        If rowCount > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function IsCourseDropped(ByVal stuEnrollId As String, ByVal clsSectId As String) As Boolean
        Dim rowCount As Integer

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT Sum(Num) from ")
            .Append("( ")
            .Append("Select Coalesce((Select count(*) as Count from arResults a, arClassSections b where a.StuEnrollId in ")
            .Append("(Select StuEnrollId from arStuEnrollments where StudentId in ")
            .Append("(Select StudentId from arStuEnrollments where StuEnrollId = ? )) ")
            .Append("and a.TestId=b.ClsSectionId  ")
            .Append("and a.DroppedInAddDrop = 1  ")
            .Append("and b.ReqId = (Select ReqId from arClassSections where ClsSectionId = ?)),0 )as Num ")

            .Append(")R1 ")
        End With


        ' Add the StdId and ClsSectId to the parameter list

        db.AddParameter("@Std", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ClsSectId", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()
        Try

            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)

        Finally
            If Not (db Is Nothing) Then
                db.CloseConnection()
            End If

        End Try

        Return (rowCount > 0)

    End Function

    Public Function HasStdAttemptedReq(ByVal StuEnrollId As String, ByVal ClsSectId As String, Optional ByVal CoReqId As String = "") As Integer
        Dim rowCount As Integer


        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        If (CoReqId <> "") Then
            With sb
                .Append("Select Sum(Num) ")
                .Append("from ")
                .Append("(Select Coalesce((Select count(*) as Count from arResults a where a.StuEnrollId in ")
                .Append("(Select StuEnrollId from arStuEnrollments where StudentId in ")
                .Append("(Select StudentId from arStuEnrollments where StuEnrollId = ?)) ")
                .Append("and a.TestId in (Select c.ClsSectionId from arClassSections c where c.ReqId = ?)),0) as Num ")

                .Append("union all ")

                .Append("Select Coalesce((Select count(*) as Count from arTransferGrades t9 where ")
                .Append("t9.StuEnrollId = ? and ")
                .Append("t9.ReqId = ?),0) as Num ")
                .Append(" )R1 ")
            End With

            ' Add the StdId and ClsSectId to the parameter list
            db.AddParameter("@Std", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CoReqId", CoReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Std2", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CoReqId2", CoReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            With sb
                .Append("SELECT Sum(Num) from " & vbCrLf)
                .Append("( " & vbCrLf)
                .Append("Select Coalesce((Select count(*) as Count from arResults a, arClassSections b where a.StuEnrollId in " & vbCrLf)
                .Append("(Select StuEnrollId from arStuEnrollments where StudentId in " & vbCrLf)
                .Append("(Select StudentId from arStuEnrollments where StuEnrollId = ?)) " & vbCrLf)
                .Append("and a.TestId=b.ClsSectionId " & vbCrLf)
                .Append("and b.ReqId = (Select ReqId from arClassSections where ClsSectionId = ?)),0 )as Num " & vbCrLf)

                .Append("union all " & vbCrLf)

                .Append("Select Coalesce((Select count(*) as Count from arTransferGrades t9 where " & vbCrLf)
                .Append("t9.StuEnrollId = ? and " & vbCrLf)
                .Append("t9.ReqId in (Select ReqId from arClassSections where ClsSectionId = ?)),0)as Num " & vbCrLf)
                .Append(")R1 " & vbCrLf)
            End With

            ' Add the StdId and ClsSectId to the parameter list
            db.AddParameter("@Std", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClsSectId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Std3", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClsSectId2", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        db.OpenConnection()
        Try
            'Execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        Finally
            db.CloseConnection() 'Close Connection
        End Try
        'Return the datatable in the dataset

        Return rowCount
    End Function

    Public Function GetAttemptedTermStartDate(ByVal stuEnrollId As String, ByVal clsSectId As String, Optional ByVal coReqId As String = "") As DateTime

        '   connect to the database
        Dim db As New SQLDataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@StuEnrollID", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@ClsSectionID", New Guid(clsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Dim dt As DataTable
        dt = db.RunParamSQLDataSet_SP("dbo.USP_AttemptedClass_TermStartDate", "SP").Tables(0)

        Dim dtTermStartDate As DateTime = Nothing
        For Each dr As DataRow In dt.Rows
            If (DateTime.TryParse(dr("TermStartDate"), dtTermStartDate)) = False Then
                Exit For
            End If
        Next

        Return dtTermStartDate
    End Function

    Public Function GetAttemptedClassStartDate(ByVal stuEnrollId As String, ByVal clsSectId As String, Optional ByVal coReqId As String = "") As DateTime

        '   connect to the database
        Dim db As New SQLDataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@StuEnrollID", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@ClsSectionID", New Guid(clsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Dim dt As DataTable
        dt = db.RunParamSQLDataSet_SP("dbo.USP_AttemptedClass_ClassStartDate", "SP").Tables(0)

        Dim dtTermStartDate As DateTime = Nothing

        For Each dr As DataRow In dt.Rows
            If (DateTime.TryParse(dr("ClassStartDate"), dtTermStartDate)) = False Then
                Exit For
            End If
        Next

        Return dtTermStartDate
    End Function

    Public Function HasStdAttemptedReq_Sp(ByVal StuEnrollId As String, ByVal ClsSectId As String, Optional ByVal CoReqId As String = "") As Integer


        Dim rowCount As Integer
        'Dim sGrdSysDetailId As String
        '   connect to the database
        Dim db As New SQLDataAccess
        Try

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            'Dim sb As New System.Text.StringBuilder
            If (CoReqId <> "") Then
                ' Add the StdId and ClsSectId to the parameter list
                db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@CoReqId", New Guid(CoReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Else
                db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@ClsSectionID", New Guid(ClsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            End If

            '    db.OpenConnection()

            If (CoReqId <> "") Then
                rowCount = db.RunParamSQLScalar_SP("USP_AR_HasStudentAttemptedReqswithCoReqs")
            Else
                rowCount = db.RunParamSQLScalar_SP("USP_AR_HasStudentAttemptedReqswithoutCoReqs")
            End If

        Catch ex As Exception

            Throw New BaseException(ex.InnerException.ToString)
        Finally
            'Close Connection
            db.CloseConnection()

        End Try

        'Return the datatable in the dataset

        Return rowCount
    End Function

    Public Function IsStdRegFrClsSect(ByVal StuEnrollId As String, ByVal ClsSectId As String) As Integer

        Dim rowCount As Integer


        Try

            '   connect to the database

            Dim db As New DataAccess

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder



            '   build the sql query



            With sb

                .Append("Select count(*) as Count from arResults a where a.StuEnrollId = ? and a.TestId = ? ")

            End With


            ' Add the PrgVerId and ChildId to the parameter list

            db.AddParameter("@Grade", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@PrgVerId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)



            db.OpenConnection()

            'Execute the query

            '   execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection

            db.CloseConnection()


        Catch ex As Exception

            Throw New BaseException(ex.InnerException.ToString)

        End Try

        'Return the datatable in the dataset

        Return rowCount
    End Function

    Public Function IsStdRegFrClsSect_SP(ByVal StuEnrollId As String, ByVal ClsSectId As String) As Integer


        Dim rowCount As Integer
        Dim db As New SQLDataAccess
        Try


            '   connect to the database

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ClsSectID", New Guid(ClsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            '   db.OpenConnection()
            rowCount = db.RunParamSQLScalar_SP("USP_AR_IsStdRegFrClsSect")

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
        'Return the datatable in the dataset
        Return rowCount
    End Function

    Public Function DoesStdHaveFinalGrade(ByVal StuEnrollId As String, ByVal ClsSectId As String, Optional ByVal CampusId As String = "") As Integer

        Dim rowCount As Integer

        Try


            '   connect to the database

            Dim db As New DataAccess

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder

            With sb
                If myAdvAppSettings.AppSettings("GradesFormat", CampusId).ToLower = "numeric" Then
                    .Append("Select count(*) as Count from arResults a where a.StuEnrollId = ? and a.TestId = ? and  a.Score is not null and a.Score <> 0 ")
                Else
                    .Append("Select count(*) as Count from arResults a where a.StuEnrollId = ? and a.TestId = ? and a.GrdSysDetailId is not null ")
                End If


            End With


            ' Add the stdenrollid and testid to the parameter list

            db.AddParameter("@Grade", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@Testid", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query

            '   execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection

            db.CloseConnection()


        Catch ex As Exception

            Throw New BaseException(ex.InnerException.ToString)

        End Try


        'Return the datatable in the dataset
        Return rowCount

    End Function

    Public Function DoesStdHaveOverride(ByVal StuEnrollId As String, ByVal PrereqId As String) As Integer


        Dim rowCount As Integer
        Try

            '   connect to the database

            Dim db As New DataAccess

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder

            With sb

                .Append("Select count(*) as Count from arOverridenPreReqs a where a.StuEnrollId = ?  ")
                .Append("and a.ReqId in (Select ReqId from arClassSections where ClsSectionId = ?) ")

            End With


            ' Add the stdenrollid and testid to the parameter list

            db.AddParameter("@Grade", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@Testid", PrereqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query

            '   execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection

            db.CloseConnection()


        Catch ex As Exception

            Throw New BaseException(ex.InnerException.ToString)

        End Try


        'Return the datatable in the dataset
        Return rowCount

    End Function

    Public Function DoesStdHaveOverride_Sp(ByVal StuEnrollId As String, ByVal PrereqId As String) As Integer

        Dim rowCount As Integer
        Dim db As New SQLDataAccess
        Try

            '   connect to the database

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@PreReqID", New Guid(PrereqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            '  db.OpenConnection()
            rowCount = db.RunParamSQLScalar_SP("USP_AR_DoesStudentHaveOverride")
            'Close Connection
            '  db.CloseConnection()
        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        Finally
            db.CloseConnection()
        End Try
        'Return the datatable in the dataset
        Return rowCount

    End Function

    Public Function GetClsSectDates(ByVal ClsSectId As String) As String

        Dim sdate As String
        Dim edate As String

        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("Select StartDate, EndDate from arClassSections where ClsSectionId = ? ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ReqId", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                sdate = dr("StartDate").ToString
                edate = dr("EndDate").ToString
            End While

            'ds = db.RunParamSQLDataSet(sb.ToString)
            'sb.Remove(0, sb.Length)

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return sdate & "," & edate

    End Function

    Public Function GetClsSectDates_sp(ByVal ClsSectId As String) As String

        Dim sdate As String
        Dim edate As String
        Dim db As New SQLDataAccess
        Try


            '   connect to the database

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")


            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@ClsSectId", New Guid(ClsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)


            '   db.OpenConnection()
            'Execute the query
            Dim dr As SqlDataReader = db.RunParamSQLDataReader_SP("USP_AR_RegStd_GetClsSectDates")

            While dr.Read()
                sdate = dr("StartDate").ToString
                edate = dr("EndDate").ToString
            End While

            'ds = db.RunParamSQLDataSet(sb.ToString)
            'sb.Remove(0, sb.Length)


            'Close Connection

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            db.CloseConnection()

        End Try

        'Return the datatable in the dataset
        Return sdate & "," & edate

    End Function

    Public Function IsStudentCurrentlyRegisteredInCourse(ByVal StuEnrollId As String, ByVal ClsSectId As String) As Integer

        Dim rowCount As Integer

        Try

            '   connect to the database

            Dim db As New DataAccess

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder



            '   build the sql query

            With sb

                .Append("Select count(*) as Count from arResults a, arClassSections b where a.StuEnrollId = ? ")
                .Append("and a.TestId in (Select c.ClsSectionId from arClassSections c where ReqId in(Select ReqId from arClassSections where ClsSectionId = ?)) ")
                .Append("and a.TestId = b.ClsSectionId ")
                .Append("and b.TermId in (Select TermId from arClassSections where ClsSectionId = ?) ")

            End With


            ' Add the PrgVerId and ChildId to the parameter list

            db.AddParameter("@Grade", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@clssectiond", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@clssectid", ClsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query

            '   execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)


            'Close Connection

            db.CloseConnection()


        Catch ex As Exception

            Throw New BaseException(ex.InnerException.ToString)

        End Try

        'Return the datatable in the dataset

        Return rowCount
    End Function

    Public Function IsStudentCurrentlyRegisteredInCourse_Sp(ByVal stuEnrollId As String, ByVal clsSectId As String) As Integer
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim courses As Integer

        Try
            db.AddParameter("@StuEnrollID", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ClsSectID", clsSectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet("dbo.USP_AR_RegStd_IsStudentRegisteredinCourse", Nothing, "SP")
            courses = ds.Tables(0).Rows(0)(0).ToString()
        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        Finally
            db.Dispose()
        End Try

        Return courses
    End Function

    Public Function DoesStdHavePassingGrdForNumeric_sp(ByVal StuEnrollId As String, ByVal ReqId As String, ByVal PrgVerId As String, ByVal ClsSectId As String) As Boolean

        Dim returnValue As Boolean
        Dim db As New SQLDataAccess
        Try

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ReqID", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@PrgVerID", New Guid(PrgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ClsSectID", New Guid(ClsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ReturnValue", returnValue, SqlDbType.Bit, , ParameterDirection.Output)
            ' db.OpenConnection()
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_AR_RegStd_DoesStdHavePassingGradeforNumeric")
            'Close Connection

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        Finally
            db.CloseConnection()
        End Try
        'Return the datatable in the dataset
        Return returnValue

    End Function

    Public Function DoesStdHavePassingGrd_sp(ByVal StuEnrollId As String, ByVal ReqId As String, ByVal PrgVerId As String, ByVal ClsSectId As String) As Boolean
        Dim returnValue As Boolean
        Dim db As New SQLDataAccess
        Try

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StuEnrollID", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ReqID", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@PrgVerID", New Guid(PrgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ClsSectID", New Guid(ClsSectId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ReturnValue", returnValue, SqlDbType.Bit, , ParameterDirection.Output)
            '  db.OpenConnection()
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_AR_RegStd_DoesStdHavePassingGrade")
            'Close Connection
            'db.CloseConnection()
        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        Finally
            db.CloseConnection()
        End Try
        'Return the datatable in the dataset
        Return returnValue

    End Function

    Public Function RegisterStudentForGradeBookComponents(ByVal StuEnrollId As String, ByVal ClsSectionId As String, ByVal user As String, ByVal rightNow As Date) As String
        Dim db As New DataAccess

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            'Modified by Balaji on 04/03/2009 to fix issue 13655
            'When a student is registered for a class, records were inserted in to arResults table for all schools
            'For Ross Type School (defined by ShowRossOnlyTabs entry in web.config), in addition to inserting records in to arresults, records were inserted into arGrdBkResults table
            'It was realized later that for non-ross type schools, if student is registered in an Externship class then record needs to be inserted into 
            'arGrdBkResults table, otherwise records will not show up in Post Externship Attendance and Attendance cannot be posted for the externship hours attended by the student
            If myAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "true" Or
              myAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "yes" Then
                With sb
                    .Append("insert into arGrdBkResults (GrdBkResultId, ClsSectionId, InstrGrdBkWgtDetailId, Comments, StuEnrollId, ModUser, ModDate) ")
                    .Append("select ")
                    .Append("		NewId() as GrdBkResultId,  ")
                    .Append("		CS.ClsSectionId, ")
                    .Append("		GBWD.InstrGrdBkWgtDetailId, ")
                    .Append("       GCT.Descrip, ")
                    .Append("		R.StuEnrollId, ")
                    .Append("		?, ")
                    .Append("		? ")
                    .Append("from	arResults R, arClassSections CS, arGrdBkWeights GBW, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT  ")
                    .Append("where	R.StuEnrollId=? ")
                    .Append("and		R.TestId=CS.ClsSectionId  ")
                    .Append("and		CS.ReqId=GBW.ReqId ")
                    .Append("and		GBW.InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId  ")
                    .Append("and		GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId  ")
                    .Append("and		GBW.EffectiveDate = (Select max(EffectiveDate) from arGrdBkWeights where ReqId=GBW.ReqId and EffectiveDate <= CS.StartDate) ")
                    .Append("and		not exists (select *  ")
                    .Append("					from arGrdBkResults  ")
                    .Append("					where ClsSectionId=CS.ClsSectionId ")
                    .Append("					and	  InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
                    .Append("					and	  StuEnrollId=?) ")
                    .Append("and    CS.ClsSectionId= ? ")
                End With
            Else
                With sb
                    .Append("insert into arGrdBkResults (GrdBkResultId, ClsSectionId, InstrGrdBkWgtDetailId, Comments, StuEnrollId, ModUser, ModDate) ")
                    .Append("select ")
                    .Append("		NewId() as GrdBkResultId,  ")
                    .Append("		CS.ClsSectionId, ")
                    .Append("		GBWD.InstrGrdBkWgtDetailId, ")
                    .Append("       GCT.Descrip, ")
                    .Append("		R.StuEnrollId, ")
                    .Append("		?, ")
                    .Append("		? ")
                    .Append("from	arResults R, arClassSections CS, arGrdBkWeights GBW, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT,arReqs RQ  ")
                    .Append("where	R.StuEnrollId=? ")
                    .Append("and		R.TestId=CS.ClsSectionId  ")
                    .Append("and		CS.ReqId=GBW.ReqId ")
                    .Append("and		GBW.InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId  ")
                    .Append("and		GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId  and CS.ReqId=RQ.ReqId and RQ.IsExternShip=1  and GCT.SysComponentTypeid=544 and R.score is null and R.GrdSysDetailid is null ")
                    .Append("and		GBW.EffectiveDate = (Select max(EffectiveDate) from arGrdBkWeights where ReqId=GBW.ReqId and EffectiveDate <= CS.StartDate) ")
                    .Append("and		not exists (select *  ")
                    .Append("					from arGrdBkResults  ")
                    .Append("					where ClsSectionId=CS.ClsSectionId ")
                    .Append("					and	  InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
                    .Append("					and	  StuEnrollId=?) ")
                    .Append("and    CS.ClsSectionId= ? ")
                End With
            End If
            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'stuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'stuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'stuEnrollId
            db.AddParameter("@ClsSectionId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   commit transaction
            groupTrans.Commit()

            '   return with no errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            ''   do not report sql lost connection
            'If Not groupTrans.Connection Is Nothing Then
            '    '   report an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
            'End If

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function DeleteGradeBookComponentsForAClassSection(ByVal StuEnrollId As String, ByVal ClsSectionId As String, ByVal user As String) As String
        Dim db As New DataAccess

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            With sb
                .Append("Delete from arGrdBkResults GBR, arClassSections CS  ")
                .Append("where	GBR.ClsSectionId=CS.ClsSectionId ")
                .Append("and	StuEnrollId=? ")
                .Append("and	CS.ClsSectionId=? ")
            End With
            'stuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'stuEnrollId
            db.AddParameter("@ClsSectionId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   commit transaction
            groupTrans.Commit()

            '   return with no errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            ''   do not report sql lost connection
            'If Not groupTrans.Connection Is Nothing Then
            '    '   report an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
            'End If

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    ''For use in Portal , While Registering a student fr the course, IF the prerequisite are taken in the previous term and they are not graded then allow the student to register in the course.
    Public Function IsStudentRegisteredandIsCourseInDifferentTerm(ByVal stuEnrollId As String, ByVal reqid As String, ByVal clsSectId As String, ByVal campusId As String) As Boolean
        Dim rowCount As Integer
        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            With sb
                .Append("Select count(*) as Count from arResults a, arClassSections b,arTerm C where a.StuEnrollId = ? ")
                .Append(" and a.TestId in (Select c.ClsSectionId from arClassSections c where ReqId=? and CampusId=?) ")
                .Append(" and a.TestId = b.ClsSectionId ")
                .Append(" and b.TermId <> (Select TermId from arClassSections where ClsSectionId = ? and CampusId=?)")
                .Append(" and b.CampusID=? ")
                .Append(" and a.GrdSysDetailid is NULL ")
                .Append(" and b.TermId=C.TermID and C.StartDate<(Select arTerm.StartDate from arClassSections,arTerm where ClsSectionId=? and CampusID=? and arterm.TermID=arClassSections.TermID) ")


            End With
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@StuEnrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Reqid", reqid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CampusID", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClsSectionId", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CampusID", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CampusID", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClsSectionId", clsSectId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CampusID", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)



            db.OpenConnection()
            '   execute the query
            rowCount = db.RunParamSQLScalar(sb.ToString)
            'Close Connection
            db.CloseConnection()
        Catch ex As Exception
            Throw New BaseException(ex.InnerException.ToString)
        End Try
        'Return the datatable in the dataset
        If rowCount = 0 Then
            Return False
        ElseIf rowCount >= 1 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function GetTermForClassSection(ByVal clsSectionId As String) As String
        Dim rtn As String = "00000000-0000-0000-0000-000000000000"


        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append(" select isnull(ProgId,'00000000-0000-0000-0000-000000000000') as Progid from arTerm,arClassSections where arTerm.TermId=arClassSections.TermId and arClassSections.ClsSectionId= ? ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@ReqId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        Try

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()
                rtn = dr("Progid").ToString

            End While

        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        End Try

        'Return the datatable in the dataset
        Return rtn


    End Function

    'Public Function HasStudentPassedSpeedTestsForCourse(ByVal stuEnrollId As String, ByVal reqId As String) As Boolean
    '    Dim rowCount As Integer


    '    Try

    '        Dim db As New DataAccess
    '        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
    '        Dim sb As New StringBuilder

    '        '   build the sql query
    '        With sb
    '            .Append("select count(*) from ( ")
    '            .Append("select s.*, ")
    '            .Append("       (select count(*) ")
    '            .Append("        from arPostScoreDictationSpeedTest psdt ")
    '            .Append("        where psdt.grdcomponenttypeid=S.GrdComponentTypeId ")
    '            .Append("        and psdt.StuEnrollId=? ")
    '            .Append("        and psdt.Speed >= S.MinimumScore) As ActualNumberOfTestsPassed ")
    '            .Append("from ")
    '            .Append("( ")
    '            .Append("select rq.Descrip,gct.Descrip as Component,r.EffectiveDate,r.NumberOfTests,r.MinimumScore,rq.ReqId, g.grdcomponenttypeid ")
    '            .Append("from arBridge_GradeComponentTypes_Courses g, arRules_GradeComponentTypes_Courses r, arReqs rq, arGrdComponentTypes gct ")
    '            .Append("where g.GrdComponentTypeId_ReqId=r.GrdComponentTypeId_ReqId ")
    '            .Append("and g.ReqId=rq.ReqId ")
    '            .Append("and g.GrdComponentTypeId=gct.GrdComponentTypeId ")
    '            'The effective date must be based on the last time the student took the course
    '            .Append("and r.EffectiveDate <= (select top 1 cs.StartDate ")
    '            .Append("                        from arResults rs, arClassSections cs ")
    '            .Append("                        where rs.StuEnrollId=? ")
    '            .Append("                        and rs.TestId=cs.ClsSectionId ")
    '            .Append("                        and cs.ReqId=? ")
    '            .Append("                        and cs.ReqId=g.ReqId ")
    '            .Append("                        order by cs.StartDate desc ")
    '            .Append("                        ) ")
    '            .Append("and r.EffectiveDate = (select max(EffectiveDate) from 	")
    '            .Append("                           ( ")
    '            .Append("                               select r.EffectiveDate ")
    '            .Append("                               from arBridge_GradeComponentTypes_Courses g, arRules_GradeComponentTypes_Courses r, arReqs rq, arGrdComponentTypes gct ")
    '            .Append("                               where g.GrdComponentTypeId_ReqId=r.GrdComponentTypeId_ReqId ")
    '            .Append("                               and g.ReqId=rq.ReqId ")
    '            .Append("                               and g.GrdComponentTypeId=gct.GrdComponentTypeId ")
    '            .Append("                              and r.EffectiveDate <= (select top 1 cs.StartDate ")
    '            .Append("                                                       from arResults rs, arClassSections cs ")
    '            .Append("                                                       where rs.StuEnrollId=? ")
    '            .Append("                                                       and rs.TestId=cs.ClsSectionId ")
    '            .Append("                                                       and cs.ReqId=? ")
    '            .Append("                                                       and cs.ReqId=g.ReqId ")
    '            .Append("                                                       order by cs.StartDate desc ")
    '            .Append("                                                       ) ")
    '            .Append("                           ) R	")
    '            .Append("                       ) ")
    '            .Append(") S ")
    '            .Append(") T ")
    '            .Append("where NumberOfTests > ActualNumberOfTestsPassed ")
    '        End With


    '        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@ReqId", reqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@ReqId", reqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   open the connection
    '        db.OpenConnection()
    '        '   execute the query
    '        rowCount = db.RunParamSQLScalar(sb.ToString)
    '        'Close Connection
    '        db.CloseConnection()

    '    Catch ex As Exception
    '        If ex.InnerException Is Nothing Then
    '            Throw New BaseException(ex.Message)
    '        Else
    '            Throw New BaseException(ex.InnerException.Message)
    '        End If

    '    End Try

    '    'The query returns how many speed tests for a course for an enrollment have NOT been passed.
    '    'If 0 is returned then it means that all the speed tests have been satisifed
    '    'If any value greater than zero is returned it means that there are that number of speed tests that have
    '    'NOT been satisfied. For eg. if 2 is returned then it means that there are 2 speed tests not satisfied.
    '    If rowCount > 0 Then
    '        Return False
    '    Else
    '        Return True
    '    End If


    'End Function

    Public Function HasStudentPassedSpeedTestsForCourse_sp(ByVal stuEnrollId As String, ByVal reqId As String) As Boolean
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim notPassed As Integer

        Try
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ReqId", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet("dbo.USP_AR_HasStudentPassedSpeedTestsForCourse", Nothing, "SP")
            notPassed = ds.Tables(0).Rows(0)(0).ToString()
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw New BaseException(ex.Message)
            Else
                Throw New BaseException(ex.InnerException.Message)
            End If
        Finally
            db.Dispose()
        End Try

        If notPassed > 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    'Public Function ChkStudSchedConflict_SP(ByVal stuEnrollID As String, ByVal clsSectID As String) As String

    '    Dim errorString As String
    '    Dim db As New SQLDataAccess
    '    Try
    '        '   connect to the database

    '        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

    '        ' Add the PrgVerId and ChildId to the parameter list
    '        db.AddParameter("@StuEnrollId", New Guid(stuEnrollID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
    '        db.AddParameter("@ClsSectID", New Guid(clsSectID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
    '        db.AddParameter("@ErrorString", New Guid(errorString), SqlDbType.UniqueIdentifier, , ParameterDirection.Output)

    '        '   Execute the query
    '        db.RunParamSQLExecuteNoneQuery_SP("USP_AR_RegStd_CheckStdScheduleConflict")

    '    Catch ex As Exception
    '        Throw New BaseException(ex.Message)
    '    Finally
    '        db.CloseConnection()
    '    End Try

    '    Return errorString
    'End Function

    Public Function IsStudentHold(ByVal stuEnrollId As String) As String
        Dim ds As DataSet

        '   connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder


        '   build the sql query
        With sb
            .Append("select count (*)as IsRegHold   from adStuGrpStudents SGS,adStudentGroups SG ,arStuEnrollments SE ")
            .Append(" where SGS.StudentId=SE.StudentId and SGS.StuGrpId=SG.StuGrpId and SG.IsRegHold=1 AND SGS.IsDeleted=0 and SGS.StuEnrollId=? ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)


        'Return the datatable in the dataset
        Return ds.Tables(0).Rows(0)(0).ToString

    End Function

    Public Function IsStudentTranscriptHold(ByVal stuEnrollId As String) As String
        Dim ds As DataSet

        '   connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("select count (*)as IsRegHold   from adStuGrpStudents SGS,adStudentGroups SG ,arStuEnrollments SE ")
            .Append(" where SGS.StudentId=SE.StudentId and SGS.StuGrpId=SG.StuGrpId and SG.IsTransHold=1 AND SGS.IsDeleted=0 and SGS.StuEnrollId=? ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)

        'Return the datatable in the dataset
        Return ds.Tables(0).Rows(0)(0).ToString

    End Function
    ''''
    Public Function IsStudentEligibleForRegistration(ByVal stuEnrollId As String, ByVal clsSectionId As String) As String
        Dim db As New SQLDataAccess
        Dim ds As DataSet
        Dim strReturnMessage As String

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@StuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, 50, ParameterDirection.Input)
            db.AddParameter("@ClsSectionId", New Guid(clsSectionId), SqlDbType.UniqueIdentifier, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_IsStudentEligibleForRegistration_Count", "IsStudentEligibleForRegistration")

            If Not ds Is Nothing Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    strReturnMessage = dr(0)
                    Return strReturnMessage
                Next
            Else
                Return "no"
            End If
        Catch ex As Exception
            Return "no"
        Finally
            db.CloseConnection()
        End Try
        Return "no"
    End Function

    Public Function GetProgAcademicCalendar(ByVal stuEnrollID As String) As String
        Dim sAcadCal As String = String.Empty

        Try

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder



            With sb
                .Append("Select D.ACDescrip ")
                .Append("from ")
                .Append("arStuEnrollments A ")
                .Append("INNER JOIN dbo.arPrgVersions B ON A.PrgVerId = B.PrgVerId ")
                .Append("INNER JOIN dbo.arPrograms C ON B.ProgId = C.ProgId ")
                .Append("INNER JOIN syAcademicCalendars D ON C.ACId = D.ACId ")
                .Append("WHERE a.StuEnrollId = ?   ")
            End With
            ' Add the student enroll id and ClsSectId to the parameter list
            db.AddParameter("@StdId", stuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   build the sql query

            '   Execute the query
            db.OpenConnection()

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read()

                sAcadCal = dr("ACDescrip")

            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return sAcadCal
    End Function

    Public Function IsCourseExamsOnly(ByVal courseCode As String, ByVal resourceName As String) As Boolean

        Try

            Dim myDataSet As New DataSet()
            Using myConnection As New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
                Using myCommand As New SqlCommand("USP_IsCourseExamsOnly", myConnection)
                    myCommand.CommandType = CommandType.StoredProcedure
                    myCommand.Parameters.Add(New SqlParameter("@CourseCode", SqlDbType.VarChar)).Value = courseCode
                    myCommand.Parameters.Add(New SqlParameter("@ResourceName", SqlDbType.VarChar)).Value = resourceName
                    Dim mySqlDataAdapter As New SqlDataAdapter(myCommand)
                    mySqlDataAdapter.Fill(myDataSet)
                    myCommand.Connection.Close()
                End Using
            End Using

            Dim bIsCourseExamsOnly As Boolean
            If myDataSet.Tables(0).Rows(0).Item(0) = 0 Then
                bIsCourseExamsOnly = False
            Else
                bIsCourseExamsOnly = True
            End If

            Return bIsCourseExamsOnly
        Catch ex As Exception
            Return Nothing
        Finally

        End Try
    End Function

    Public Function HasStudentCompletedClass(ByVal stuEnrollid As String, ByVal clsSectionId As String) As Boolean

        Try

            Dim myDataSet As New DataSet()
            Using myConnection As New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
                Using myCommand As New SqlCommand("USP_CheckIfClassWasAlreadyCompleted", myConnection)
                    myCommand.CommandType = CommandType.StoredProcedure
                    myCommand.Parameters.Add(New SqlParameter("@StuEnrollId", SqlDbType.UniqueIdentifier)).Value = New Guid(stuEnrollid)
                    myCommand.Parameters.Add(New SqlParameter("@ClsSectionId", SqlDbType.UniqueIdentifier)).Value = New Guid(clsSectionId)
                    Dim mySqlDataAdapter As New SqlDataAdapter(myCommand)
                    mySqlDataAdapter.Fill(myDataSet)
                    myCommand.Connection.Close()
                End Using
            End Using

            Dim bIsClassAlreadyCompleted = myDataSet.Tables(0).Rows(0)("gradedcount") >= 1
            Return bIsClassAlreadyCompleted
        Catch ex As Exception
            Return Nothing
        Finally

        End Try
    End Function

    Public Function GetStudentIdFromEnrollment(enrollmentId)
        Const sql As String = "SELECT StudentId FROM dbo.arStuEnrollments WHERE StuEnrollId = @EnrollId "

        Dim conn As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim command = New SqlCommand(sql, conn)
        command.Parameters.AddWithValue("@EnrollId", enrollmentId)
        conn.Open()
        Try
            Dim studentId As String = command.ExecuteScalar().ToString()
            Return studentId
        Finally
            conn.Close()
        End Try
    End Function

    Public Function GetRegisterInformation(stuEnrollIdList As List(Of String), reqId As String) As List(Of ArResultsClass)
        Dim sb = New StringBuilder()
        sb.Append(" SELECT Score, res.GrdSysDetailId as GrdSysDetailId, res.Cnt, res.Hours, res.IsInComplete as IsInComplete, DroppedInAddDrop ")
        sb.Append(" ,IsTransfered, isClinicsSatisfied, res.DateDetermined, IsCourseCompleted, res.ModUser, res.ModDate ")
        sb.Append(" ,IsGradeOverridden, GradeOverriddenBy, GradeOverriddenDate, res.StuEnrollId, TestId, req.Code as courseCode, det.Grade as grade ")
        sb.Append(" ,term.TermCode as termCode, term.TermDescrip ")
        sb.Append("  FROM arResults res ")
        sb.Append(" JOIN dbo.arClassSections sec ON sec.ClsSectionId = res.TestId ")
        sb.Append(" JOIN dbo.arTerm term ON term.TermId = sec.TermId ")
        sb.Append(" JOIN dbo.arStuEnrollments enroll ON enroll.StuEnrollId = res.StuEnrollId ")
        sb.Append(" JOIN dbo.arReqs req ON req.ReqId = sec.ReqId ")
        sb.Append(" JOIN dbo.arGradeSystemDetails det ON det.GrdSysDetailId =  res.GrdSysDetailId ")
        sb.Append(" WHERE req.ReqId = @reqID ")
        sb.Append(" AND enroll.StuEnrollId = @StuEnrollId ")
        sb.Append(" AND det.IsPass=1")
        Dim conn As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim resultlist As List(Of ArResultsClass) = New List(Of ArResultsClass)()
        Dim commandList As List(Of SqlCommand) = New List(Of SqlCommand)(
                                                 )
        For Each stuEnrollId As String In stuEnrollIdList
            Dim command = New SqlCommand(sb.ToString(), conn)
            command.Parameters.AddWithValue("@StuEnrollId", stuEnrollId)
            command.Parameters.AddWithValue("@reqID", reqId)
            commandList.Add(command)
        Next


        conn.Open()
        Try
            For Each comm As SqlCommand In commandList
                'Temporal list 
                Dim tempList As List(Of ArResultsClass) = New List(Of ArResultsClass)()
                ' Read ArResults
                Dim reader = comm.ExecuteReader()
                While reader.Read()
                    Dim arInfo = New ArResultsClass()
                    Dim cnt = reader("Cnt")
                    arInfo.Cnt = If((cnt Is DBNull.Value), Nothing, DirectCast(cnt, Nullable(Of Integer)))
                    Dim dd = reader("DateDetermined")
                    arInfo.DateDetermined = If((dd Is DBNull.Value), Nothing, DirectCast(dd, Nullable(Of DateTime)))
                    arInfo.DroppedInAddDrop = CBool(reader("DroppedInAddDrop"))
                    arInfo.GradeOverridenBy = reader("GradeOverriddenBy").ToString()
                    arInfo.IsGradeOverriden = CBool(reader("IsGradeOverridden"))
                    Dim dov = reader("GradeOverriddenDate")
                    arInfo.GradeOverridenDate = If((dov Is DBNull.Value), Nothing, DirectCast(dov, Nullable(Of DateTime)))
                    arInfo.GrdSysDetailId = reader("GrdSysDetailId").ToString()
                    Dim h = reader("Hours")
                    arInfo.Hours = If((h Is DBNull.Value), Nothing, DirectCast(h, Nullable(Of Integer)))
                    Dim ic = reader("isClinicsSatisfied")
                    arInfo.IsClinicsSatisfied = If((ic Is DBNull.Value), Nothing, DirectCast(ic, Nullable(Of Boolean)))
                    Dim it = reader("IsTransfered")
                    arInfo.IsTransferred = If((it Is DBNull.Value), Nothing, DirectCast(it, Nullable(Of Boolean)))
                    Dim modd = reader("ModDate")
                    arInfo.ModDate = If((modd Is DBNull.Value), DateTime.Now, DirectCast(modd, Nullable(Of DateTime)))
                    arInfo.ModUser = reader("ModUser").ToString()
                    Dim sc = reader("Score")
                    arInfo.Score = If((sc Is DBNull.Value), Nothing, DirectCast(sc, Nullable(Of Decimal)))
                    arInfo.EnrollmentId = reader("StuEnrollId").ToString()
                    Dim test = reader("TestId")
                    arInfo.TestId = If((test Is DBNull.Value), Nothing, test.ToString())
                    Dim isinco = reader("IsInComplete")
                    arInfo.IsIncomplete = If((isinco Is DBNull.Value), Nothing, DirectCast(isinco, Nullable(Of Boolean)))
                    arInfo.IsCourseCompleted = CBool(reader("IsCourseCompleted"))
                    arInfo.Grade = reader("grade").ToString()
                    arInfo.CourseCode = reader("courseCode").ToString()
                    arInfo.TermCode = reader("termCode").ToString()
                    arInfo.TermDescription = reader("TermDescrip").ToString()
                    tempList.Add(arInfo)

                End While
                reader.Close()

                ' Check retakes
                If tempList.Count > 0 Then
                    Dim theSelected As ArResultsClass
                    If tempList.Count > 1 Then
                        'Then retakes exists
                        theSelected = GetTheSelectedByPolicy(tempList)
                    Else
                        theSelected = tempList(0)
                    End If
                    resultlist.Add(theSelected)
                End If
            Next

            Return resultlist
        Finally
            conn.Close()
        End Try
    End Function

    Private Function GetTheSelectedByPolicy(ByVal list As List(Of ArResultsClass)) As ArResultsClass
        If myAdvAppSettings.AppSettings("SettingRetakeTestPolicy").ToString().ToUpper() = "TAKEBEST" Then
            'Take the best
            If myAdvAppSettings.AppSettings("GradesFormat").ToString().ToUpper() = "LETTER" Then
                'Get all Grades, letters
                Dim grades As String
                For Each result As ArResultsClass In list
                    If String.IsNullOrEmpty(grades) Then
                        grades += "'" + result.Grade + "'"
                    Else
                        grades += ",'" + result.Grade + "'"
                    End If

                Next
                Dim sb = New StringBuilder()
                sb.Append(" SELECT TOP 1 Grade FROM dbo.arGradeSystemDetails  ")
                sb.Append(" JOIN arGradeSystems ags ON ags.GrdSystemId = arGradeSystemDetails.GrdSystemId  ")
                sb.Append(" JOIN dbo.syStatuses stat ON stat.StatusId = ags.StatusId  ")
                sb.Append(" LEFT JOIN dbo.arGradeScaleDetails ON arGradeScaleDetails.GrdSysDetailId = arGradeSystemDetails.GrdSysDetailId  ")
                sb.Append(" WHERE stat.StatusCode = 'A'  ")
                sb.Append(" AND Grade IN ( " + grades + ")")
                sb.Append(" ORDER BY IsPass DESC, GPA DESC  ")
                Dim conn As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
                Dim command = New SqlCommand(sb.ToString(), conn)
                conn.Open()
                Try
                    Dim grade As String = command.ExecuteScalar()
                    For Each results As ArResultsClass In list
                        If (results.Grade = grade) Then
                            Return results
                        End If
                    Next
                    Return list(0)
                Finally
                    conn.Close()
                End Try

            Else
                'Get the best score numeric
                Dim best As ArResultsClass = list(0)
                For Each resultsClass As ArResultsClass In list
                    If (best.Score < resultsClass.Score) Then
                        best = resultsClass
                    End If
                Next
                Return best
            End If
        Else
            'Take The Last
            Dim last As ArResultsClass = list(0)
            For Each resultsClass As ArResultsClass In list
                If (last.ModDate < resultsClass.ModDate) Then
                    last = resultsClass
                End If
            Next
            Return last
        End If

    End Function

    ''' <summary>
    ''' This should only be used in transfer operation and not in Register operation
    ''' because Future Start is included
    ''' </summary>
    ''' <param name="studentId"></param>
    ''' <param name="reqid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetActiveEnrollmentsWithRequerimentId(studentId As String, reqid As String, Optional ByVal overridePostAcademics As Boolean = False) As IList(Of String)
        Dim sql = New StringBuilder()
        sql.Append("       SELECT  enroll.StuEnrollId ")
        sql.Append("       FROM    arStuEnrollments enroll ")
        sql.Append("        JOIN    dbo.syStatusCodes codes ON codes.StatusCodeId = enroll.StatusCodeId")
        sql.Append("        JOIN    dbo.sySysStatus status ON status.SysStatusId = codes.SysStatusId")
        sql.Append("       JOIN    dbo.arPrgVersions ver ON ver.PrgVerId = enroll.PrgVerId ")
        sql.Append("       JOIN    dbo.arProgVerDef def ON def.PrgVerId = ver.PrgVerId ")
        sql.Append("       WHERE   enroll.StudentId = @StudentId ")

        If overridePostAcademics = False Then
            sql.Append("               AND  (status.PostAcademics = 1 OR status.SysStatusId = 7) ") 'Added Future Start
        End If

        sql.Append("               AND ( def.ReqId IN ( SELECT GrpId ")
        sql.Append("                                    FROM   dbo.arReqGrpDef ")
        sql.Append("                                    WHERE  ReqId = @reqId ) ")
        sql.Append("                     OR def.ReqId = @reqId ")
        sql.Append("                   ) ")


        Dim conn As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim command = New SqlCommand(sql.ToString(), conn)
        command.Parameters.AddWithValue("@StudentId", studentId)
        command.Parameters.AddWithValue("@reqId", reqid)
        conn.Open()
        Try
            Dim list = New List(Of String)()
            Dim reader = command.ExecuteReader()
            While reader.Read()
                list.Add(reader(0).ToString())
            End While

            Return list
        Finally
            conn.Close()
        End Try
    End Function

    ''' <summary>
    ''' Get all enrollments id from the student with the Requirement given. All enrollments of the student level are gotten
    ''' (drop, externship transfer out etc)
    ''' </summary>
    ''' <param name="studentId"></param>
    ''' <param name="reqId"></param>
    ''' <returns>List of Enrollment Id as string list</returns>
    Public Function GetAllStudentEnrollmentsWithRequerimentId(ByVal studentId As String, ByVal reqId As String) As IList(Of String)
        Dim sql = New StringBuilder()
        sql.Append("       SELECT  enroll.StuEnrollId ")
        sql.Append("       FROM    arStuEnrollments enroll ")
        sql.Append("        JOIN    dbo.syStatusCodes codes ON codes.StatusCodeId = enroll.StatusCodeId")
        sql.Append("        JOIN    dbo.sySysStatus status ON status.SysStatusId = codes.SysStatusId")
        sql.Append("       JOIN    dbo.arPrgVersions ver ON ver.PrgVerId = enroll.PrgVerId ")
        sql.Append("       JOIN    dbo.arProgVerDef def ON def.PrgVerId = ver.PrgVerId ")
        sql.Append("       WHERE   enroll.StudentId = @StudentId ")
        sql.Append("               AND  status.StatusLevelId = 2 ") 'Get all enrollments as student of this student
        sql.Append("               AND ( def.ReqId IN ( SELECT GrpId ")
        sql.Append("                                    FROM   dbo.arReqGrpDef ")
        sql.Append("                                    WHERE  ReqId = @reqId ) ")
        sql.Append("                     OR def.ReqId = @reqId ")
        sql.Append("                   ) ")


        Dim conn As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim command = New SqlCommand(sql.ToString(), conn)
        command.Parameters.AddWithValue("@StudentId", studentId)
        command.Parameters.AddWithValue("@reqId", reqId)
        conn.Open()
        Try
            Dim list = New List(Of String)()
            Dim reader = command.ExecuteReader()
            While reader.Read()
                list.Add(reader(0).ToString())
            End While

            Return list
        Finally
            conn.Close()
        End Try
    End Function

    ''' <summary>
    ''' Get the list of all enrollment active associated with the student id 
    ''' and with the Class Element that we want to register.
    ''' This apply to Register the class in the enrollments, but NOT to enter Attendance or Score.
    ''' </summary>
    ''' <param name="enrollId"></param>
    ''' <param name="classId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>

    Public Function GetActiveEnrollmentsWithClassAssociatedForRegisterClass(ByVal enrollId As String, ByVal classId As String) As List(Of String)
        Dim db As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)

        Dim command As SqlCommand = New SqlCommand("GetActiveEnrollmentsWithClassAssociatedForRegisterClass", db)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@StuEnrollId", enrollId)
        command.Parameters.AddWithValue("@ClassId", classId)

        Dim list = New List(Of String)
        db.Open()
        Try
            Dim sqlreader As SqlDataReader = command.ExecuteReader()
            While sqlreader.Read()
                Dim data As String = sqlreader(0).ToString()
                list.Add(data)
            End While

            Return list
        Finally
            db.Close()
        End Try

    End Function

    ''' <summary>
    ''' Get the list of all enrollment active associated with the student id 
    ''' and with the Class Element that we want to register.
    ''' This apply to post Grade (Score) or Attendance, but not to register a class to the enrollment
    ''' </summary>
    ''' <param name="enrollId">The student that we want to register to class</param>
    ''' <param name="classId">The class to be registered with the student.</param>
    ''' <returns>List(Of String) with the enrollmentID</returns>

    Public Function GetActiveEnrollmentWithClassAssociated(ByVal enrollId As String, ByVal classId As String) As List(Of String)


        Dim db As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)

        Dim command As SqlCommand = New SqlCommand("GetActiveEnrollmentsWithClassAssociated", db)
        command.CommandType = CommandType.StoredProcedure

        command.Parameters.AddWithValue("@StuEnrollId", enrollId)
        command.Parameters.AddWithValue("@ClassId", classId)

        Dim list = New List(Of String)
        db.Open()
        Try
            Dim sqlreader As SqlDataReader = command.ExecuteReader()
            While sqlreader.Read()
                Dim data As String = sqlreader(0).ToString()
                list.Add(data)
            End While

            Return list
        Finally
            db.Close()
        End Try


    End Function

    ''' <summary>  
    ''' Check if a determinate enrollment is Already registered for a determinate Class
    ''' </summary>
    ''' <param name="stuEnrollId">Enrollment to be checked</param>
    ''' <param name="classId">Curse again is checked the enrollment as registered</param>
    ''' <returns>Already Registered if registered if not empty string</returns>
    ''' <remarks></remarks>

    Public Function CheckIfEnrollmentIsAlreadyRegister(ByVal stuEnrollId As String, ByVal classId As String) As String
        Dim sb As New StringBuilder
        With sb
            .Append(" SELECT count(*) as Count ")
            .Append(" FROM arResults a ")
            .Append(" WHERE")
            .Append("  a.StuEnrollId = @StuEnrollId ")
            .Append("  AND a.TestID= @ClassId")
        End With
        Dim conn As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim command As SqlCommand = New SqlCommand(sb.ToString(), conn)
        command.Parameters.AddWithValue("@StuEnrollId", stuEnrollId)
        command.Parameters.AddWithValue("@ClassId", classId)
        conn.Open()
        Try
            Dim count As Integer = command.ExecuteScalar()
            Return If(count > 0, "Already Registered", String.Empty)
        Finally
            conn.Close()
        End Try
    End Function

    ''' <summary>
    ''' Register with the associated ClassId all enrollment in the list.
    ''' This include the record in ArResults and in Grade Book
    ''' </summary>
    ''' <param name="registrationClassInfos"></param>
    ''' <returns>string.Empty</returns>
    ''' <remarks>
    ''' This does not validate if the list of enrollments were previously register for the class
    ''' Be sure that the enrollments are verified first.
    ''' </remarks>

    Public Function RegisterClassInEnrollmentsOfStudents(ByVal registrationClassInfos As List(Of RegistrationClassInfo)) As String

        'Create list of command to be executed in Transaction
        Dim commandList = New List(Of SqlCommand)

        'Create the support for the query operation
        Dim conn As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim sqlTrans As SqlTransaction

        'Loop to Prepare all commands
        For Each info As RegistrationClassInfo In registrationClassInfos

            'Prepare insertion in ArResult
            Dim commandResult As SqlCommand = PrepareCommandResult(info)
            commandResult.Connection = conn
            Dim commandRegisterStudentForGradeBookComponents As SqlCommand = PreparecommandRegisterStudentFor(info)
            commandRegisterStudentForGradeBookComponents.Connection = conn
            commandList.Add(commandResult)
            commandList.Add(commandRegisterStudentForGradeBookComponents)
        Next

        'Open and prepare transaction
        conn.Open()
        sqlTrans = conn.BeginTransaction("RegisterInGradeBook")
        For Each command As SqlCommand In commandList
            command.Transaction = sqlTrans
        Next

        'Execute Transaction
        Try
            For Each command As SqlCommand In commandList
                command.ExecuteNonQuery()
            Next

            'Commit Transaction 
            sqlTrans.Commit()
            Return String.Empty
        Catch ex As Exception

            sqlTrans.Rollback()
            Throw
        Finally

            conn.Close()
        End Try

    End Function

    Public Function UnRegisterClassInEnrollmentsOfStudent(ByVal registrationInfolist As List(Of RegistrationClassInfo)) As String
        'Create list of command to be executed in Transaction
        Dim commandList = New List(Of SqlCommand)

        'Create the support for the query operation
        Dim conn As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)
        Dim sqlTrans As SqlTransaction

        'Loop to Prepare all commands
        For Each info As RegistrationClassInfo In registrationInfolist

            'Prepare delete operations
            Dim commandResult As SqlCommand = PrepareCommandDeleteFromTables(info)
            commandResult.Connection = conn
            commandList.Add(commandResult)
        Next

        'Open and prepare transaction
        conn.Open()
        sqlTrans = conn.BeginTransaction("UnRegisterInGradeBook")
        For Each command As SqlCommand In commandList
            command.Transaction = sqlTrans
        Next

        'Execute Transaction
        Try
            For Each command As SqlCommand In commandList
                command.ExecuteNonQuery()
            Next

            'Commit Transaction 
            sqlTrans.Commit()
            Return String.Empty
        Catch ex As Exception

            sqlTrans.Rollback()
            Throw
        Finally

            conn.Close()
        End Try

    End Function

    Private Function PrepareCommandDeleteFromTables(ByVal info As RegistrationClassInfo) As SqlCommand
        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()
        With sb
            .Append("Delete from arGrdBkResults ")
            .Append("WHERE ClsSectionId = @ClsSectionId ")
            .Append("AND StuEnrollId =  @StuEnrollId ; ")
            .Append("DELETE FROM arResults ")
            .Append("WHERE TestId = @ClsSectionId ")
            .Append("AND StuEnrollId =  @StuEnrollId ; ")
            .Append("DELETE from dbo.atClsSectAttendance where ")
            .Append(" ClsSectionId= @ClsSectionId and stuEnrollId = @StuEnrollId ; ")
            .Append("DELETE from dbo.syStudentAttendanceSummary where ")
            .Append(" ClsSectionId= @ClsSectionId and stuEnrollId = @StuEnrollId ; ")
        End With

        command.CommandText = sb.ToString()
        command.Parameters.AddWithValue("@ClsSectionId", info.ClassId)
        command.Parameters.AddWithValue("@StuEnrollId", info.StuEnrollmentId)
        Return command
    End Function

    Private Function PreparecommandRegisterStudentFor(ByVal info As RegistrationClassInfo) As SqlCommand
        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()

        '   Prepare arGrdBkResults Insert 
        If myAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "true" Or
         myAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "yes" Then
            With sb
                .Append("insert into arGrdBkResults (GrdBkResultId, ClsSectionId, InstrGrdBkWgtDetailId, Comments, StuEnrollId, ModUser, ModDate) ")
                .Append("select ")
                .Append("		NewId() as GrdBkResultId,  ")
                .Append("		CS.ClsSectionId, ")
                .Append("		GBWD.InstrGrdBkWgtDetailId, ")
                .Append("       GCT.Descrip, ")
                .Append("		R.StuEnrollId, ")
                .Append("		@ModUser, ")
                .Append("		@ModDate ")
                .Append("from	arResults R, arClassSections CS, arGrdBkWeights GBW, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT  ")
                .Append("where	R.StuEnrollId= @StuEnrollId ")
                .Append("and		R.TestId=CS.ClsSectionId  ")
                .Append("and		CS.ReqId=GBW.ReqId ")
                .Append("and		GBW.InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId  ")
                .Append("and		GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId  ")
                .Append("and		GBW.EffectiveDate = (Select max(EffectiveDate) from arGrdBkWeights where ReqId=GBW.ReqId and EffectiveDate <= CS.StartDate) ")
                .Append("and		not exists (select *  ")
                .Append("					from arGrdBkResults  ")
                .Append("					where ClsSectionId=CS.ClsSectionId ")
                .Append("					and	  InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
                .Append("					and	  StuEnrollId= @StuEnrollId) ")
                .Append("and    CS.ClsSectionId= @ClsSectionId ")
            End With
        Else

            With sb
                .Append("insert into arGrdBkResults (GrdBkResultId, ClsSectionId, InstrGrdBkWgtDetailId, Comments, StuEnrollId, ModUser, ModDate) ")
                .Append("select ")
                .Append("		NewId() as GrdBkResultId,  ")
                .Append("		CS.ClsSectionId, ")
                .Append("		GBWD.InstrGrdBkWgtDetailId, ")
                .Append("       GCT.Descrip, ")
                .Append("		R.StuEnrollId, ")
                .Append("		@ModUser, ")
                .Append("		@ModDate ")
                .Append("from	arResults R, arClassSections CS, arGrdBkWeights GBW, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT,arReqs RQ  ")
                .Append("where	R.StuEnrollId= @StuEnrollId ")
                .Append("and		R.TestId=CS.ClsSectionId  ")
                .Append("and		CS.ReqId=GBW.ReqId ")
                .Append("and		GBW.InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId  ")
                .Append("and		GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId  and CS.ReqId=RQ.ReqId and RQ.IsExternShip=1  and GCT.SysComponentTypeid=544 and R.score is null and R.GrdSysDetailid is null ")
                .Append("and		GBW.EffectiveDate = (Select max(EffectiveDate) from arGrdBkWeights where ReqId=GBW.ReqId and EffectiveDate <= CS.StartDate) ")
                .Append("and		not exists (select *  ")
                .Append("					from arGrdBkResults  ")
                .Append("					where ClsSectionId=CS.ClsSectionId ")
                .Append("					and	  InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
                .Append("					and	  StuEnrollId= @StuEnrollId) ")
                .Append("and    CS.ClsSectionId= @ClsSectionId ")
            End With

        End If

        command.CommandText = sb.ToString()
        command.Parameters.AddWithValue("@ModUser", info.UserName)
        command.Parameters.AddWithValue("@ModDate", DateTime.Now)
        command.Parameters.AddWithValue("@StuEnrollId", info.StuEnrollmentId)
        command.Parameters.AddWithValue("@ClsSectionId", info.ClassId)

        Return command

    End Function

    Private Function PrepareCommandResult(ByVal info As RegistrationClassInfo) As SqlCommand
        Dim command As SqlCommand = New SqlCommand()
        Dim sb As StringBuilder = New StringBuilder()
        '   Prepare arResults Insert 
        With sb
            .Append(" INSERT INTO arResults(TestId,StuEnrollId,ModUser,ModDate) ")
            .Append(" VALUES(@ClassId,@StudentEnrollId,@UserDate,@ModDate) ")
        End With

        command.CommandText = sb.ToString()
        command.Parameters.AddWithValue("@ClassId", info.ClassId)
        command.Parameters.AddWithValue("@StudentEnrollId", info.StuEnrollmentId)
        command.Parameters.AddWithValue("@UserDate", info.UserName)
        command.Parameters.AddWithValue("@ModDate", DateTime.Now)

        Return command
    End Function

    ''' <summary>
    ''' This function is to generate the script for data hot fix to do the Record Transfer Grades for Student. There is another function that is used by the page Record Transfer Grades for Student which is <code>InsertAllcademicInformationInDataBaseInBulk</code>.
    ''' Once the script is generated you can bring that script to sql server. The generated script does not include the variable tables used for inserts and it doesn't include the try catch and tranctions statements. Those needs to be manually added for the final script.
    ''' </summary>
    ''' <param name="insertionRegisterList">The list of records(<code>ArResults, atClsSectAttendance and arGrdBkResults </code> that will be used to create the insert statements.</param>
    ''' <returns></returns>
    Public Function GenerateInsertScriptForAllcademicInformation(ByVal insertionRegisterList As IList(Of CorrectionRegInfo)) As String
        Dim script As New StringBuilder
        For Each res As CorrectionRegInfo In insertionRegisterList
            For Each info As ArResultsClass In res.ArInfoList
                Dim sb = New StringBuilder()
                sb.Append("IF NOT EXISTS(SELECT TOP 1 * FROM arResults WHERE ")
                sb.Append(" TestId = '" + Guid.Parse(info.TestId).ToString() + "'  ")
                sb.Append(" AND Score = " + If(info.Score Is Nothing, "null", info.Score.Value.ToString()) + " ")
                sb.Append(" AND GrdSysDetailId = " + If(String.IsNullOrEmpty(info.GrdSysDetailId), "null", "'" + Guid.Parse(info.GrdSysDetailId).ToString() + "'") + " ")
                sb.Append(" AND Cnt = " + If(info.Cnt Is Nothing, "null", info.Cnt.Value) + " ")
                sb.Append(" AND Hours = " + If(info.Hours Is Nothing, "null", info.Hours.Value) + " ")
                sb.Append(" AND StuEnrollId = '" + Guid.Parse(info.EnrollmentId).ToString() + "' ")
                sb.Append(" AND IsInComplete = " + If(info.IsIncomplete Is Nothing, "null", IIf(info.IsIncomplete.Value, "1", "0")) + " ")
                sb.Append(" AND DroppedInAddDrop = " + If(info.DroppedInAddDrop, "1", "0") + " ")
                sb.Append(" AND IsTransfered = " + If(info.IsTransferred, "1", "0") + " ")
                sb.Append(" AND isClinicsSatisfied = " + If(info.IsClinicsSatisfied, "1", "0") + " ")
                sb.Append(" AND DateDetermined = " + If(info.DateDetermined Is Nothing, "null", "'" + info.DateDetermined.Value.ToString() + "'") + " ")
                sb.Append(" AND IsCourseCompleted = " + If(info.IsCourseCompleted, "1", "0") + " ")
                sb.Append(" AND IsGradeOverridden =  " + If(info.IsGradeOverriden, "1", "0") + " ")
                sb.Append(" AND GradeOverriddenBy =  " + If(String.IsNullOrEmpty(info.GradeOverridenBy), "null", "'" + info.GradeOverridenBy + "'") + " ")
                sb.Append(" AND GradeOverriddenDate = " + If(info.GradeOverridenDate Is Nothing, "null", "'" + info.GradeOverridenDate.Value.ToString() + "'") + " ")
                sb.Append(")")
                sb.AppendLine("")
                sb.AppendLine("BEGIN")
                sb.AppendLine(" INSERT @arResults ")
                sb.Append("         ( ResultId ")
                sb.Append("         ,TestId ")
                sb.Append("         ,Score ")
                sb.Append("         ,GrdSysDetailId ")
                sb.Append("         ,Cnt ")
                sb.Append("         ,Hours ")
                sb.Append("         ,StuEnrollId ")
                sb.Append("         ,IsInComplete ")
                sb.Append("         ,DroppedInAddDrop ")
                sb.Append("         ,ModUser ")
                sb.Append("         ,ModDate ")
                sb.Append("         ,IsTransfered ")
                sb.Append("         ,isClinicsSatisfied ")
                sb.Append("         ,DateDetermined ")
                sb.Append("         ,IsCourseCompleted ")
                sb.Append("         ,IsGradeOverridden ")
                sb.Append("         ,GradeOverriddenBy ")
                sb.Append("         ,GradeOverriddenDate ")
                sb.Append("         ) ")
                sb.Append(" VALUES  ( '" + Guid.NewGuid().ToString() + "' ")
                sb.Append("         , '" + Guid.Parse(info.TestId).ToString() + "'  ")
                sb.Append("         , " + If(info.Score Is Nothing, "null", info.Score.Value.ToString()) + " ")
                sb.Append("         , " + If(String.IsNullOrEmpty(info.GrdSysDetailId), "null", "'" + Guid.Parse(info.GrdSysDetailId).ToString() + "'") + " ")
                sb.Append("         , " + If(info.Cnt Is Nothing, "null", info.Cnt.Value) + " ")
                sb.Append("         , " + If(info.Hours Is Nothing, "null", info.Hours.Value) + " ")
                sb.Append("         , '" + Guid.Parse(info.EnrollmentId).ToString() + "' ")
                sb.Append("         , " + If(info.IsIncomplete Is Nothing, "null", IIf(info.IsIncomplete.Value, "1", "0")) + " ")
                sb.Append("         , " + If(info.DroppedInAddDrop, "1", "0") + " ")
                sb.Append("         , " + If(String.IsNullOrEmpty(info.ModUser), "null", "'" + info.ModUser + "'") + " ")
                sb.Append("         ,GETDATE() ")
                sb.Append("         , " + If(info.IsTransferred, "1", "0") + " ")
                sb.Append("         , " + If(info.IsClinicsSatisfied, "1", "0") + " ")
                sb.Append("         ," + If(info.DateDetermined Is Nothing, "null", "'" + info.DateDetermined.Value.ToString() + "'") + " ")
                sb.Append("         , " + If(info.IsCourseCompleted, "1", "0") + " ")
                sb.Append("         , " + If(info.IsGradeOverriden, "1", "0") + " ")
                sb.Append("         , " + If(String.IsNullOrEmpty(info.GradeOverridenBy), "null", "'" + info.GradeOverridenBy + "'") + " ")
                sb.Append("         , " + If(info.GradeOverridenDate Is Nothing, "null", "'" + info.GradeOverridenDate.Value.ToString() + "'") + " ")
                sb.Append("         ) ")
                sb.AppendLine("")
                sb.AppendLine("END")
                sb.AppendLine("")
                script.AppendLine(sb.ToString())
            Next
            For Each info As GradeBookResultInfo In res.GradeBookList
                ' Insert in arGrdBkResults
                Dim sb1 = New StringBuilder()
                sb1.Append("IF NOT EXISTS(SELECT TOP 1 * FROM arGrdBkResults WHERE ")
                sb1.Append(" ClsSectionId = '" + info.ClsSectionId.ToString() + "'  ")
                sb1.Append(" AND InstrGrdBkWgtDetailId = '" + info.InstrGrdBkWgtDetailId.ToString() + "'  ")
                sb1.Append(" AND Score = " + If(info.Score Is Nothing, "null", info.Score.Value.ToString()) + "  ")
                sb1.Append(" AND Comments = " + If(String.IsNullOrEmpty(info.Comments), "null", "'" + info.Comments + "'") + "  ")
                sb1.Append(" AND StuEnrollId = '" + info.StuEnrollId.ToString() + "'  ")
                sb1.Append(" AND ResNum = " + info.ResNum.ToString() + "  ")
                sb1.Append(" AND PostDate = " + If(info.PostDate Is Nothing, "null", "'" + info.PostDate.Value.ToString() + "'") + "  ")
                sb1.Append(" AND IsCompGraded = " + If(info.IsCompGraded, "1", "0") + "  ")
                sb1.Append(" AND isCourseCredited = " + If(info.IsCourseCredited, "1", "0") + "  ")
                sb1.Append(")")
                sb1.AppendLine("")
                sb1.AppendLine("BEGIN")
                sb1.AppendLine(" INSERT  @arGrdBkResults  ")
                sb1.Append("         ( GrdBkResultId  ")
                sb1.Append("         ,ClsSectionId  ")
                sb1.Append("         ,InstrGrdBkWgtDetailId  ")
                sb1.Append("         ,Score  ")
                sb1.Append("         ,Comments  ")
                sb1.Append("         ,StuEnrollId  ")
                sb1.Append("         ,ModUser  ")
                sb1.Append("         ,ModDate  ")
                sb1.Append("         ,ResNum  ")
                sb1.Append("         ,PostDate  ")
                sb1.Append("         ,IsCompGraded  ")
                sb1.Append("         ,isCourseCredited  ")
                sb1.Append("         )  ")
                sb1.Append(" VALUES  ( '" + Guid.NewGuid().ToString() + "'  ")
                sb1.Append("         , '" + info.ClsSectionId.ToString() + "'  ")
                sb1.Append("         , '" + info.InstrGrdBkWgtDetailId.ToString() + "'  ")
                sb1.Append("         , " + If(info.Score Is Nothing, "null", info.Score.Value.ToString()) + "  ")
                sb1.Append("         , " + If(String.IsNullOrEmpty(info.Comments), "null", "'" + info.Comments + "'") + "  ")
                sb1.Append("         , '" + info.StuEnrollId.ToString() + "'  ")
                sb1.Append("         , 'SUPPORT'  ")
                sb1.Append("         , GETDATE()  ")
                sb1.Append("         , " + info.ResNum.ToString() + "  ")
                sb1.Append("         , " + If(info.PostDate Is Nothing, "null", "'" + info.PostDate.Value.ToString() + "'") + "  ")
                sb1.Append("         ,  " + If(info.IsCompGraded, "1", "0") + "  ")
                sb1.Append("         , " + If(info.IsCourseCredited, "1", "0") + "  ")
                sb1.Append("         )  ")
                sb1.AppendLine("")
                sb1.AppendLine("END")
                sb1.AppendLine("")
                script.AppendLine(sb1.ToString())
            Next
            For Each info As AtClsSectAttendance In res.AttendanceList
                ' Insert in atClsSectAttendance
                Dim sb2 = New StringBuilder()
                sb2.Append("IF NOT EXISTS(SELECT TOP 1 * FROM atClsSectAttendance WHERE ")
                sb2.Append(" StudentId = null ")
                sb2.Append(" AND ClsSectionId = '" + Guid.Parse(info.ClsSectionId).ToString() + "'  ")
                sb2.Append(" AND ClsSectMeetingId = " + If(String.IsNullOrEmpty(info.ClsSectMeetingId), "null", "'" + Guid.Parse(info.ClsSectMeetingId).ToString() + "'") + "  ")
                sb2.Append(" AND MeetDate = '" + info.MeetDate.ToString() + "' ")
                sb2.Append(" AND Actual = " + info.Actual.ToString() + "  ")
                sb2.Append(" AND Tardy =  " + If(info.Tardy, "1", "0") + "  ")
                sb2.Append(" AND Excused = " + If(info.Excused, "1", "0") + "  ")
                sb2.Append(" AND Comments = " + If(String.IsNullOrEmpty(info.Comments), "null", "'" + info.Comments + "'") + "  ")
                sb2.Append(" AND StuEnrollId = '" + Guid.Parse(info.StuEnrollId).ToString() + "'  ")
                sb2.Append(" AND Scheduled = " + If(info.Scheduled Is Nothing, "null", info.Scheduled.Value.ToString()) + "  ")
                sb2.Append(")")
                sb2.AppendLine("")
                sb2.AppendLine("BEGIN")
                sb2.AppendLine("  INSERT @atClsSectAttendance ")
                sb2.Append("          ( ClsSectAttId ")
                sb2.Append("          ,StudentId ")
                sb2.Append("          ,ClsSectionId ")
                sb2.Append("          ,ClsSectMeetingId ")
                sb2.Append("          ,MeetDate ")
                sb2.Append("          ,Actual ")
                sb2.Append("          ,Tardy ")
                sb2.Append("          ,Excused ")
                sb2.Append("          ,Comments ")
                sb2.Append("          ,StuEnrollId ")
                sb2.Append("          ,Scheduled ")
                sb2.Append("          ) ")
                sb2.Append("  VALUES  ( '" + Guid.NewGuid().ToString() + "' ")
                sb2.Append("          , null ")
                sb2.Append("          , '" + Guid.Parse(info.ClsSectionId).ToString() + "'  ")
                sb2.Append("          , " + If(String.IsNullOrEmpty(info.ClsSectMeetingId), "null", "'" + Guid.Parse(info.ClsSectMeetingId).ToString() + "'") + "  ")
                sb2.Append("          , '" + info.MeetDate.ToString() + "' ")
                sb2.Append("          , " + info.Actual.ToString() + "  ")
                sb2.Append("          , " + If(info.Tardy, "1", "0") + "  ")
                sb2.Append("          , " + If(info.Excused, "1", "0") + "  ")
                sb2.Append("          , " + If(String.IsNullOrEmpty(info.Comments), "null", "'" + info.Comments + "'") + "  ")
                sb2.Append("          , '" + Guid.Parse(info.StuEnrollId).ToString() + "'  ")
                sb2.Append("          , " + If(info.Scheduled Is Nothing, "null", info.Scheduled.Value.ToString()) + "  ")
                sb2.Append("          ) ")
                sb2.AppendLine("")
                sb2.AppendLine("END")
                sb2.AppendLine("")
                script.AppendLine(sb2.ToString())
            Next
        Next
        Dim outputScript As String = script.ToString()
        Return outputScript
    End Function

    ''' <summary>
    ''' Converted the previous transacction command based function for <code>InsertAllcademicInformationIn</code> into one that executes the inserts in bulk.
    ''' </summary>
    ''' <param name="insertionRegisterList">The list of records(<code>ArResults, atClsSectAttendance and arGrdBkResults </code> that will be bulk inserted</param>
    Public Sub InsertAllcademicInformationInDataBaseInBulk(ByVal insertionRegisterList As IList(Of CorrectionRegInfo))
        Dim arInfoTypeMappings As New Dictionary(Of String, String)

        arInfoTypeMappings.Add("ResultId", "guid")
        arInfoTypeMappings.Add("TestId", "guid")
        arInfoTypeMappings.Add("GrdSysDetailId", "guid")
        arInfoTypeMappings.Add("StuEnrollId", "guid")
        arInfoTypeMappings.Add("EnrollmentId", "guid")

        Dim attendanceTypeMappings As New Dictionary(Of String, String)
        attendanceTypeMappings.Add("ClsSectAttId", "guid")
        attendanceTypeMappings.Add("StudentId", "guid")
        attendanceTypeMappings.Add("ClsSectionId", "guid")
        attendanceTypeMappings.Add("ClsSectMeetingId", "guid")
        attendanceTypeMappings.Add("StuEnrollId", "guid")

        Dim gradebookTypeMapping As New Dictionary(Of String, String)
        gradebookTypeMapping.Add("PostDate", "shortdatetime")
        gradebookTypeMapping.Add("IsCompGraded", "object")
        gradebookTypeMapping.Add("IsCourseCredited", "object")

        Dim arInfoDataTable As DataTable = (New List(Of ArResultsClass)()).AsDataTable(arInfoTypeMappings)
        Dim attendanceDataTable As DataTable = (New List(Of AtClsSectAttendance)()).AsDataTable(attendanceTypeMappings)
        Dim gradeBookDataTable As DataTable = (New List(Of GradeBookResultInfo)()).AsDataTable()

        For Each record As CorrectionRegInfo In insertionRegisterList
            For Each result As ArResultsClass In record.ArInfoList
                result.ResultId = Guid.NewGuid().ToString()
            Next

            For Each attendance As AtClsSectAttendance In record.AttendanceList
                attendance.ClsSectAttId = Guid.NewGuid().ToString()
            Next

            For Each gradeBook As GradeBookResultInfo In record.GradeBookList
                gradeBook.GrdBkResultId = Guid.NewGuid()
                gradeBook.ModUser = "SUPPORT"
                gradeBook.ModDate = DateTime.Now
            Next

            Dim arInfoListData As DataTable = record.ArInfoList.AsDataTable(arInfoTypeMappings)
            Dim attendanceListData As DataTable = record.AttendanceList.AsDataTable(attendanceTypeMappings)
            Dim gradebookListData As DataTable = record.GradeBookList.AsDataTable()

            For Each row In arInfoListData.Rows
                arInfoDataTable.ImportRow(row)
            Next

            For Each row In attendanceListData.Rows
                attendanceDataTable.ImportRow(row)
            Next
            For Each row In gradebookListData.Rows
                gradeBookDataTable.ImportRow(row)
            Next
        Next

        Dim connString = myAdvAppSettings.AppSettings("ConnectionString").ToString
        Dim conn = New SqlConnection(connString)
        conn.Open()
        Dim transaction As SqlTransaction
        transaction = conn.BeginTransaction()

        Try
            Dim arInfoMapping As New Dictionary(Of String, String)

            arInfoMapping.Add("StuEnrollId", "EnrollmentId")
            arInfoMapping.Add("ResultId", "ResultId")
            arInfoMapping.Add("TestId", "TestId")
            arInfoMapping.Add("Score", "Score")
            arInfoMapping.Add("GrdSysDetailId", "GrdSysDetailId")
            arInfoMapping.Add("Cnt", "Cnt")
            arInfoMapping.Add("Hours", "Hours")
            arInfoMapping.Add("IsInComplete", "IsInComplete")
            arInfoMapping.Add("DroppedInAddDrop", "DroppedInAddDrop")
            arInfoMapping.Add("ModUser", "ModUser")
            arInfoMapping.Add("ModDate", "ModDate")
            arInfoMapping.Add("IsTransfered", "IsTransferred")
            arInfoMapping.Add("isClinicsSatisfied", "IsClinicsSatisfied")
            arInfoMapping.Add("DateDetermined", "DateDetermined")
            arInfoMapping.Add("IsCourseCompleted", "IsCourseCompleted")
            arInfoMapping.Add("IsGradeOverridden", "IsGradeOverriden")
            arInfoMapping.Add("GradeOverriddenBy", "GradeOverridenBy")
            arInfoMapping.Add("GradeOverriddenDate", "GradeOverridenDate")

            BulkInsert.BuilkInsertDataTable("arResults", arInfoDataTable, conn, arInfoMapping, transaction, False)

            Dim gradebookMapping As New Dictionary(Of String, String)
            gradebookMapping.Add("GrdBkResultId", "GrdBkResultId")
            gradebookMapping.Add("ClsSectionId", "ClsSectionId")
            gradebookMapping.Add("InstrGrdBkWgtDetailId", "InstrGrdBkWgtDetailId")
            gradebookMapping.Add("Score", "Score")
            gradebookMapping.Add("Comments", "Comments")
            gradebookMapping.Add("StuEnrollId", "StuEnrollId")
            gradebookMapping.Add("ModUser", "ModUser")
            gradebookMapping.Add("ModDate", "ModDate")
            gradebookMapping.Add("ResNum", "ResNum")
            gradebookMapping.Add("PostDate", "PostDate")
            gradebookMapping.Add("IsCompGraded", "IsCompGraded")
            gradebookMapping.Add("isCourseCredited", "isCourseCredited")

            BulkInsert.BuilkInsertDataTable("arGrdBkResults", gradeBookDataTable, conn, gradebookMapping, transaction, False)

            Dim attendanceMapping As New Dictionary(Of String, String)
            attendanceMapping.Add("ClsSectAttId", "ClsSectAttId")
            attendanceMapping.Add("ClsSectionId", "ClsSectionId")
            attendanceMapping.Add("ClsSectMeetingId", "ClsSectMeetingId")
            attendanceMapping.Add("MeetDate", "MeetDate")
            attendanceMapping.Add("Actual", "Actual")
            attendanceMapping.Add("Tardy", "Tardy")
            attendanceMapping.Add("Excused", "Excused")
            attendanceMapping.Add("Comments", "Comments")
            attendanceMapping.Add("StuEnrollId", "StuEnrollId")
            attendanceMapping.Add("Scheduled", "Scheduled")


            BulkInsert.BuilkInsertDataTable("atClsSectAttendance", attendanceDataTable, conn, attendanceMapping, transaction, False)

            transaction.Commit()

        Catch ex As Exception
            transaction.Rollback()
            Throw
        Finally
            conn.Close()
        End Try
    End Sub

    Public Function GetAllAcademicInformationForOneClass(registeredEnrollment As CorrectionRegInfo, arInfo As ArResultsClass) As CorrectionRegInfo
        Dim connString = myAdvAppSettings.AppSettings("ConnectionString").ToString
        Dim conn = New SqlConnection(connString)

        ' Prepare get info from arGrdBkResults
        Const sql As String = "SELECT InstrGrdBkWgtDetailId, Score, Comments, ResNum, ModUser, PostDate, IsCompGraded, isCourseCredited, ClsSectionId " + "FROM dbo.arGrdBkResults WHERE StuEnrollId = @StuEnroll AND ClsSectionId = @TestId"

        Dim command1 = New SqlCommand(sql, conn)
        command1.Parameters.AddWithValue("@StuEnroll", arInfo.EnrollmentId)
        command1.Parameters.AddWithValue("@TestId", arInfo.TestId)

        ' Prepare to get info from atClsSectAttendance
        Const sql1 As String = "SELECT ClsSectMeetingId, ClsSectionId, MeetDate, Actual, Tardy, Excused, Comments, Scheduled  " + "FROM atClsSectAttendance WHERE StuEnrollId = @StuEnroll AND ClsSectionId = @TestId "
        Dim command2 = New SqlCommand(sql1, conn)
        command2.Parameters.AddWithValue("@StuEnroll", arInfo.EnrollmentId)
        command2.Parameters.AddWithValue("@TestId", arInfo.TestId)

        conn.Open()
        Try
            ' Read arGrdBkResults
            Dim reader1 = command1.ExecuteReader()
            While reader1.Read()

                Dim gr = New GradeBookResultInfo()

                Dim sc = reader1("Score")
                gr.Score = If((sc Is DBNull.Value), Nothing, DirectCast(sc, Nullable(Of Decimal)))
                Dim comm = reader1("Comments")
                gr.Comments = If((comm Is DBNull.Value), Nothing, comm.ToString())
                If (Not String.IsNullOrWhiteSpace(arInfo.EnrollmentId)) Then
                    gr.StuEnrollId = Guid.Parse(arInfo.EnrollmentId)
                End If
                Dim post = reader1("PostDate")
                gr.PostDate = If((post Is DBNull.Value), Nothing, DirectCast(post, Nullable(Of DateTime)))
                Dim isc = reader1("IsCompGraded")
                gr.IsCompGraded = If((isc Is DBNull.Value), Nothing, DirectCast(isc, Nullable(Of Boolean)))
                Dim icc = reader1("IsCourseCredited")
                gr.IsCourseCredited = If((icc Is DBNull.Value), Nothing, DirectCast(icc, Nullable(Of Boolean)))
                If (Not String.IsNullOrWhiteSpace(reader1("ClsSectionId").ToString())) Then
                    gr.ClsSectionId = Guid.Parse(reader1("ClsSectionId").ToString())
                End If
                If (Not String.IsNullOrWhiteSpace(reader1("InstrGrdBkWgtDetailId").ToString())) Then
                    gr.InstrGrdBkWgtDetailId = Guid.Parse(reader1("InstrGrdBkWgtDetailId").ToString())
                End If
                gr.ModUser = reader1("ModUser").ToString()
                gr.ResNum = DirectCast(reader1("ResNum"), Integer)

                registeredEnrollment.GradeBookList.Add(gr)
            End While
            reader1.Close()

            ' Read atClsSectAttendance
            Dim reader2 = command2.ExecuteReader()
            While reader2.Read()
                Dim at = New AtClsSectAttendance()

                Dim sch = reader2("Scheduled")
                at.Scheduled = If((sch Is DBNull.Value), Nothing, DirectCast(sch, Nullable(Of Decimal)))
                Dim co = reader2("Comments")
                at.Comments = If((co Is DBNull.Value), Nothing, co.ToString())
                Dim clm = reader2("ClsSectMeetingId")
                at.ClsSectMeetingId = If((clm Is DBNull.Value), Nothing, clm.ToString())
                at.ClsSectionId = reader2("ClsSectionId").ToString()
                at.Actual = DirectCast(reader2("Actual"), Decimal)
                at.Tardy = CBool(reader2("Tardy"))
                at.Excused = CBool(reader2("Excused"))
                Dim md = reader2("MeetDate")
                at.MeetDate = If((md Is DBNull.Value), Nothing, DirectCast(md, DateTime))
                registeredEnrollment.AttendanceList.Add(at)
            End While

            ' Return all Academics information
            Return registeredEnrollment
        Finally
            conn.Close()
        End Try
    End Function


End Class
