Imports FAME.Advantage.Common

Public Class EmployerMasterDB

    Public Function GetEmployerList(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            Dim temp As String = paramInfo.OrderBy
            If temp.IndexOf("syCampGrps.CampGrpDescrip") <> -1 Then
                temp = temp.Replace("syCampGrps.CampGrpDescrip", "CampGrpDescrip")
            End If
            If temp.IndexOf("plEmployers.EmployerDescrip") <> -1 Then
                temp = temp.Replace("plEmployers.EmployerDescrip", "EmployerDescrip")
            End If
            If temp.IndexOf("plIndustries.IndustryDescrip") <> -1 Then
                temp = temp.Replace("plIndustries.IndustryDescrip", "IndustryDescrip")
            End If
            If temp.IndexOf("syStates.StateDescrip") <> -1 Then
                temp = temp.Replace("syStates.StateDescrip", "StateDescrip")
            End If
            'strOrderBy &= " ORDER BY " & paramInfo.OrderBy
            strOrderBy &= " ORDER BY " & temp
        End If

        With sb
            .Append("SELECT DISTINCT EmployerID,EmployerDescrip,Code,syStatuses.Status,Address1,Address2,City,")
            .Append("(SELECT StateDescrip FROM syStates WHERE StateId=plEmployers.StateId) AS StateDescrip,")
            .Append("OtherState,Zip,ForeignZip,Phone,ForeignPhone,Fax,ForeignFax,Email,ParentID,")
            .Append("(SELECT A.EmployerDescrip FROM plEmployers A WHERE A.EmployerID=plEmployers.ParentID) AS Parent,")
            .Append("(SELECT IndustryDescrip FROM plIndustries WHERE IndustryID=plEmployers.IndustryID) AS IndustryDescrip,")
            .Append("(SELECT CampGrpDescrip FROM syCampGrps WHERE CampGrpID=plEmployers.CampGrpID) AS CampGrpDescrip,plEmployers.ModDate ")
            .Append("FROM plEmployers,syStatuses ")
            .Append("WHERE plEmployers.StatusId=syStatuses.StatusId ")
            .Append(strWhere)
            .Append(strOrderBy)
            '.Append("SELECT DISTINCT EmployerID,EmployerDescrip,Code,Address1,Address2,")
            '.Append("City,syStates.StateDescrip,Zip,Phone,Fax,Email,ParentID,")
            '.Append("(SELECT A.EmployerDescrip FROM plEmployers A WHERE A.EmployerID=plEmployers.ParentID) AS Parent,")
            '.Append("plIndustries.IndustryDescrip,syCampGrps.CampGrpDescrip,plEmployers.ModDate ")
            '.Append("FROM plEmployers,syStates,syStatuses,syCampGrps,plIndustries ")
            '.Append("WHERE plEmployers.StateId=syStates.StateId ")
            '.Append("AND plEmployers.StatusId=syStatuses.StatusId ")
            '.Append("AND plEmployers.IndustryID=plIndustries.IndustryID ")
            '.Append("AND plEmployers.CampGrpID = syCampGrps.CampGrpID ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
End Class
