Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common.Reports


Public Class ReportsDB
    Public Function GetSimpleReportSQL(ByVal sqlID As Integer) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("SELECT SQLStmt,WhereClause,OrderByClause ")
            .Append("FROM syRptSQL ")
            .Append("WHERE RptSQLId = ?")
        End With

        db.AddParameter("@rptsqlid", sqlID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
        Return ds
    End Function

    Public Function GetSimpleReportDataSet(ByVal paramInfo As Common.ReportParamInfo) As DataSet
        Dim strSQL As String
        Dim strWhere As String
        Dim strOrderBy As String
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim ds2 As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            'We need to get the SQL statement for this report
            ds2 = GetSimpleReportSQL(paramInfo.SqlId)
            strSQL = ds2.Tables(0).Rows(0)("SQLStmt").ToString()
            strWhere = ds2.Tables(0).Rows(0)("WhereClause").ToString()
            strOrderBy = ds2.Tables(0).Rows(0)("OrderByClause").ToString()

            If strWhere <> "" Then
                If paramInfo.FilterList <> "" Then
                    strWhere &= " AND " & paramInfo.FilterList
                End If
            Else
                If paramInfo.FilterList <> "" Then
                    strWhere &= "WHERE " & paramInfo.FilterList
                End If
            End If

            If strWhere <> "" Then
                If paramInfo.FilterOther <> "" Then
                    strWhere &= " AND " & paramInfo.FilterOther
                End If
            Else
                If paramInfo.FilterOther <> "" Then
                    strWhere &= "WHERE " & paramInfo.FilterOther
                End If
            End If

            If strOrderBy <> "" Then
                If paramInfo.OrderBy <> "" Then
                    If strOrderBy.StartsWith("ORDER BY") Then
                        strOrderBy &= "," & paramInfo.OrderBy
                    Else
                        'May start with 'GROUP BY'
                        strOrderBy &= " ORDER BY " & paramInfo.OrderBy
                    End If
                End If
            Else
                If paramInfo.OrderBy <> "" Then
                    strOrderBy &= "ORDER BY " & paramInfo.OrderBy
                End If
            End If

            If strWhere <> "" Then
                strSQL &= " " & strWhere
            End If

            If strOrderBy <> "" Then
                strSQL &= " " & strOrderBy
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            'If sqlID = 6 Then
            'sb.Append(" group by B.EmployerDescrip,B.Code,A.EmployerID,plJobTitles.JobTitleDescrip,FirstName,A.ContactID ")
            'End If
            ds = db.RunSQLDataSet(strSQL)

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return ds
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception(ex.Message)
            Else
                Throw New Exception(ex.InnerException.ToString)
            End If
        End Try
    End Function

    Public Function GetSimpleReportSQLCount(ByVal sqlID As Integer) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT RptCountStmt ")
            .Append("FROM syRptSQLCount ")
            .Append("WHERE RptSQLId = ?")
        End With

        db.AddParameter("@rptsqlid", sqlID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        Return ds.Tables(0).Rows(0)("RptCountStmt")

    End Function

    Public Function GetRecordCount(ByVal sqlID As Integer, Optional ByVal sortParams As String = "",
            Optional ByVal filterListParams As String = "", Optional ByVal filterOtherParams As String = "") As String
        Dim strSQL As String
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strRecordCount As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        ' Try
        'We need to get the SQL statement for this report
        strSQL = GetSimpleReportSQLCount(sqlID)

        With sb
            .Append(strSQL)
        End With

        If filterListParams <> "" Then
            sb.Append(" AND " & filterListParams)
        End If

        If filterOtherParams <> "" Then
            sb.Append(" AND " & filterOtherParams)
        End If

        'If sortParams <> "" Then
        '    sb.Append(" ORDER BY " & sortParams)
        'End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        'Execute the query
        Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)
        While dr.Read()
            strRecordCount = dr("EmployerCount")
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return strRecordCount
        'Catch ex As Exception
        '    'If ex.InnerException Is Nothing Then
        '    Throw New Exception(ex.Message)
        '    'End If
        'End Try
    End Function

    Public Function GetSQLByResource(ByVal ResourceID As Integer) As String
        'Dim strSQL As String
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        'Dim ds As New DataSet
        Dim strSqlidValue As String = String.Empty

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append(" Select RptSQLID from syRptProps where ResourceID=? ")
        End With

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        db.AddParameter("@ResourceID", ResourceID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            strSqlidValue = dr("RptSQLID")
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return strSqlidValue
    End Function

    Public Function GetOBJName(ByVal ObjID As Integer) As String
        'Dim strSQL As String
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        'Dim ds As New DataSet
        Dim objName As String

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append(" Select Descrip from syBusObjects where BusObjectId=? ")
        End With

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        db.AddParameter("@ResourceID", ObjID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        'Execute the query
        objName = CType(db.RunParamSQLScalar(sb.ToString), String)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return objName
    End Function

    Public Function GetReportInfo(ByVal resourceID As Integer) As Common.ReportInfo
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        'Dim ds As New DataSet
        Dim rptInfo As New Common.ReportInfo

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT t1.RptSQLId,t1.RptObjId,t2.ResourceURL,t2.Resource ")
            .Append("FROM syRptProps t1, syResources t2 ")
            .Append("WHERE t1.ResourceID=t2.ResourceId ")
            .Append("AND t1.ResourceID=?")
        End With

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        db.AddParameter("@ResourceID", resourceID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read
            If dr("RptObjId").ToString = "" Then
                rptInfo.ObjId = 0
            Else
                rptInfo.ObjId = dr("RptObjId")
            End If
            If dr("RptSQLId").ToString = "" Then
                rptInfo.SqlId = 0
            Else
                rptInfo.SqlId = dr("RptSQLId")
            End If

            rptInfo.ResourceID = resourceID
            rptInfo.Url = dr("ResourceURL")
            rptInfo.Resource = dr("Resource")
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return rptInfo

    End Function

    Public Function GetPlacementStats(ByVal sqlID As Integer, Optional ByVal sortParams As String = "",
          Optional ByVal filterListParams As String = "", Optional ByVal filterOtherParams As String = "") As String
        Dim strSql As String
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        'Dim ds As New DataSet
        Dim strRecordCount As String = String.Empty

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        ' Try
        'We need to get the SQL statement for this report
        strSql = GetSimpleReportSQLCount(sqlID)

        With sb
            .Append(strSql)
        End With

        If filterListParams <> "" Then
            sb.Append(" AND " & filterListParams)
        End If

        If filterOtherParams <> "" Then
            sb.Append(" AND " & filterOtherParams)
        End If

        'If sortParams <> "" Then
        '    sb.Append(" ORDER BY " & sortParams)
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        'Execute the query
        Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)
        While dr.Read()

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return strRecordCount
    End Function

    Public Function GetSchoolLogo(Optional ByVal useOfficialLogo As Boolean = False) As Common.AdvantageLogoImage ' As Byte()
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        'Dim objName As Byte()
        Dim logoName As String = GetLogoName(useOfficialLogo)

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT TOP 1 Image, ImgLen, ContentType  ")
            .Append("FROM sySchoolLogo ")
            If useOfficialLogo Then
                If logoName = "" Then
                    .Append("WHERE OfficialUse=1 ")
                Else
                    .Append("WHERE ImgFile='" + logoName + "' ")
                End If
            Else
                If logoName = "" Then
                    .Append("WHERE OfficialUse=0 ")
                Else
                    .Append("WHERE ImgFile='" + logoName + "' ")
                End If
            End If
            'Retrieve the smallest logo
            .Append("ORDER BY ImgLen")
        End With

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        'Execute the query
        Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)
        'While dr.Read()
        '    objName = dr("Image")
        'End While

        'Return objName
        dr.Read()

        'this is the AdvantageLogoImage object
        Dim advLogoImage As New Common.AdvantageLogoImage

        If dr.HasRows Then

            'populate AdvantageLogoImage
            With advLogoImage
                .SchoolId = 0
                .Image = dr("image")
                .ImageLength = dr("ImgLen")
                If dr("ContentType") Is DBNull.Value Then .ContentType = "image/jpeg" Else .ContentType = dr("ContentType")
            End With

        End If

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return advLogoImage

    End Function
    Private Function GetLogoName(ByVal useOfficial As Boolean) As String
        Dim logoName As String

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        If useOfficial Then
            logoName = myAdvAppSettings.AppSettings("LogoForTranscripts")
        Else
            logoName = myAdvAppSettings.AppSettings("StandardLogo")
        End If
        If logoName = Nothing Then Return "" Else Return logoName
    End Function
    Public Function GetSchoolLogo(ByVal schoolId As Integer) As Common.AdvantageLogoImage
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        ' Dim objName As Byte()

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT Image, ImgLen, ContentType FROM sySchoolLogo WHERE ImgId=?")
        End With

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        'add parameter schoolId to the query
        db.AddParameter("@SchoolId", schoolId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        'this is the AdvantageLogoImage object
        Dim advLogoImage As New Common.AdvantageLogoImage

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        dr.Read()

        If dr.HasRows Then

            'populate AdvantageLogoImage
            With advLogoImage
                .SchoolId = schoolId
                .Image = dr("image")
                .ImageLength = dr("ImgLen")
                If dr("ContentType") Is DBNull.Value Then .ContentType = "image/jpeg" Else .ContentType = dr("ContentType")
            End With

        End If

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return advLogoImage

    End Function
    Public Function GetRequiredFiltersForReports(ByVal resourceId As Integer) As DataSet
        'connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Build the sql query
        With sb
            .Append("   SELECT DISTINCT t1.RptParamId,t1.RptCaption AS Caption ")
            .Append("   FROM syRptParams t1 where t1.ResourceId = ? and t1.Required=1 ")
        End With

        'return dataset
        db.AddParameter("@ResourceId", resourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    '' Code Adde by kamalesh Ahuja on 08th Oct 2010 to resolve mantis issue id 19726
    Public Function GetCurrentlyAttendingStatusId(ByVal objID As String) As Boolean
        'Dim strSQL As String
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As DataSet
        'Dim objName As String

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append(" select * from syStatusCodes where SysStatusId not in (9,20) and StatusCodeId in ('" & objID & "')")
        End With

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()
        Try
            'Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)
            Return Not (ds.Tables(0).Rows.Count > 0)
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try

    End Function
    ''''''''''''''''''
    ''' <summary>
    ''' Get by name the given item in Report server table Catalog.
    ''' </summary>
    ''' <param name="report">
    ''' The name of the report or folder to begin fast report
    ''' </param>
    ''' <returns>
    ''' [Path], [Name], [Type], [Description] FROM  [Catalog]
    ''' </returns>
    Public Function GetReportServerItem(report As String) As ReportServerCatalog

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connectionString As String = myAdvAppSettings.AppSettings("ReportServerConexion")
        Dim sql As String = "SELECT  [Path], [Name], [Type], [Description] FROM  [Catalog] WHERE [Name] = @ReportName"
        Dim connection = New SqlConnection(connectionString)
        Dim command = New SqlCommand(sql, connection)
        command.Parameters.AddWithValue("@ReportName", report)
        connection.Open()
        Try
            Dim result = New ReportServerCatalog()
            Dim dr As SqlDataReader = command.ExecuteReader()
            While dr.Read()

                result.Path = dr(0).ToString()
                result.Name = dr(1).ToString()
                result.Type = Convert.ToInt32(dr(2))
                result.Description = dr(3).ToString()
            End While

            Return result

        Finally
            connection.Close()
        End Try


    End Function
End Class
