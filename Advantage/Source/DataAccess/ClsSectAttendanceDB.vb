
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class ClsSectAttendanceDB
    ''' <summary>
    ''' Store the connection string for this class
    ''' </summary>
    ''' <remarks></remarks>
    Private ReadOnly conString As String

    ''' <summary>
    ''' Application Setting for get some other different values from config.
    ''' </summary>
    ''' <remarks></remarks>                                                                                                             
    Private ReadOnly myAdvAppSettings As AdvAppSettings
    Protected ReadOnly currentUser As String

    Sub New()
        myAdvAppSettings = AdvAppSettings.GetAppSettings()
        conString = myAdvAppSettings.AppSettings("ConString")
        currentUser = HttpContext.Current.Session("UserName")
    End Sub

    Public Function GetTerms() As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        db.ConnectionString = conString

        With sb
            .Append("SELECT TermId, TermDescrip ")
            .Append("FROM arTerm")
        End With

        ds = db.RunSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function
    Public Function GetTermsStudentIsRegisteredFor(stuEnrollId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter


        db.ConnectionString = conString
        Try
            With sb
                .Append("SELECT DISTINCT ")
                .Append("       B.TermId,C.TermDescrip,C.StartDate,C.EndDate ")
                .Append("FROM   arResults A,arClassSections B,arTerm C ")
                .Append("WHERE  A.TestId=B.ClsSectionId ")
                .Append("       AND B.TermId=C.TermId ")
                .Append("       AND C.StartDate <= '" & Date.Today & "'")
                .Append("       AND A.StuEnrollId=? ")
                .Append("ORDER BY C.StartDate,C.EndDate,C.TermDescrip ")
            End With

            db.AddParameter("StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "Terms")
            db.ClearParameters()
            sb.Remove(0, sb.Length)


            With sb
                .Append("Select IsFutureStart = (CASE WHEN (B.SysStatusId = 7) THEN 'True' ELSE 'False' END) ") 'Added by BN to accomodate status change of future starts to Curr Attending. 
                .Append("from arStuEnrollments a, syStatusCodes b ")
                .Append("where a.StuEnrollId = ? and ")
                .Append("a.StatusCodeId = b.StatusCodeId ")
            End With

            ' Add the StuEnrollId to the parameter list
            db.AddParameter("StuEnrollId2", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "FutureStart")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        Return ds
    End Function

    Public Function GetClsSectionsForTerm(termID As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        db.ConnectionString = conString

        With sb
            .Append("SELECT t1.ClsSectionId,t2.Descrip,t1.ClsSection ")
            .Append("FROM arClassSections t1, arReqs t2 ")
            .Append("WHERE t1.ReqId = t2.ReqId ")
            .Append("AND t1.TermId = ?")
        End With

        db.AddParameter("termid", termID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    ''Added BY Saraswathi Lakshmanan to fix
    '' issue 14296: Instructor Supervisors and Education Directors 
    ''need to be able to post and modify attendance. 
    ''Optional Parameter Added

    Public Function GetClsSectionsForTermForAttendance(termID As String, shiftID As String, asOfClsStartDate As Date, campusId As String, instructorId As String, userName As String, Optional ByVal isAcademicAdvisor As Boolean = False) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet



        db.ConnectionString = conString

        With sb
            .Append("SELECT t4.ClsSectionId,t2.Descrip,t1.ClsSection, t2.code ")
            .Append("FROM arClassSections t1, arReqs t2, arAttUnitType t3, arClassSectionTerms t4 ")
            .Append("WHERE(t1.ReqId = t2.ReqId) ")
            .Append("AND t2.UnitTypeId = t3.UnitTypeId ")
            .Append("AND t4.TermId = ?  ")
            .Append("AND t4.ClsSectionId = t1.ClsSectionId  ")
            .Append("AND t3.UnitTypeDescrip <> 'None' ")
            .Append("AND t1.CampusId = ? ")
            If Not shiftID = "00000000-0000-0000-0000-000000000000" Then
                .Append("AND t1.ShiftID = ? ")
            End If
            If Not asOfClsStartDate = Nothing Then
                .Append("AND t1.StartDate >= ?")
            End If

            ''Added BY Saraswathi Lakshmanan to fix
            '' issue 14296: Instructor Supervisors and Education Directors (Academic Advisors)
            ''need to be able to post and modify attendance. 

            If userName.ToLower <> "sa" And userName.ToLower <> "support" And isAcademicAdvisor = False Then

                .Append(" AND (t1.InstructorId in (Select InstructorId ")
                .Append(" from arInstructorsSupervisors where")
                .Append(" SupervisorId= ? )")
                .Append(" OR t1.InstructorId= ? )")

            End If
            .Append(" order by t2.Descrip,t2.code ")
        End With

        db.AddParameter("termid", termID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        If Not shiftID = "00000000-0000-0000-0000-000000000000" Then
            db.AddParameter("shiftID", shiftID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        If Not asOfClsStartDate = Nothing Then
            db.AddParameter("StartDate", asOfClsStartDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        ''Added BY Saraswathi Lakshmanan to fix
        '' issue 14296: Instructor Supervisors and Education Directors (Academic Advisors)
        ''need to be able to post and modify attendance. 

        If userName.ToLower <> "sa" And userName.ToLower <> "support" And isAcademicAdvisor = False Then
            db.AddParameter("instrid", instructorId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("instrid", instructorId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        End If
        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function GetClsSectionDateRange(classSectionId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet


        db.ConnectionString = conString

        With sb
            .Append("SELECT StartDate, EndDate ")
            .Append("FROM arClassSections ")
            .Append("WHERE ClsSectionId = ? ")
        End With

        db.AddParameter("termid", classSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function

    Public Function GetStudentsInClsSection(clsSectionId As String, clsSectMeetingID As String) As DataTable
        Dim db As New DataAccess

        db.ConnectionString = conString
        Try

            db.AddParameter("@TestId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ClsSectMeetingID", clsSectMeetingID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Dim ds As DataSet = db.RunParamSQLDataSetUsingSP("GetAttendance_StudentList", Nothing)
            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try

    End Function

    Public Function GetClsSectionMeetingDays(clsSectionId As String, usePeriods As Boolean) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet


        db.ConnectionString = conString
        If usePeriods Then
            With sb
                .Append("select * from (SELECT  ")
                .Append("		PWD.WorkDayId, ")
                .Append("		WD.WorkdaysDescrip, ")
                .Append("		TI1.TimeIntervalDescrip, ")
                .Append("		TI2.TimeIntervalDescrip As EndTime, ")
                .Append(" (cast(datediff(mi,TI1.TimeIntervalDescrip,TI2.TimeIntervalDescrip) as decimal)/60)-( CAST(ISNULL(CSM.BreakDuration, 0)AS DECIMAL) / 60 ) as duration, ")
                .Append("		CSM.ClsSectionId ,CSM.StartDate,CSM.EndDate  , CSM.InstructionTypeID,ACT.InstructionTypeDescrip,CSM.ClsSectMeetingId,P.PeriodDescrip,ISNULL(R.UseTimeClock,0) AS UseTimeClock     ")
                .Append("FROM	arClsSectMeetings CSM, syPeriods P, syPeriodsWorkDays PWD, plWorkdays WD, cmTimeInterval TI1, cmTimeInterval TI2 , arInstructionType ACT,arClassSections CS,arReqs R  ")
                .Append("WHERE ")
                .Append("		CSM.PeriodId=P.PeriodId ")
                .Append("AND	P.PeriodId=PWD.PeriodId ")
                .Append("AND	PWD.WorkDayId=WD.WorkDaysId ")
                .Append("AND	P.StartTimeId = TI1.TimeIntervalId  ")
                .Append("AND	P.EndTimeId = TI2.TimeIntervalId  ")
                .Append(" AND CS.ClsSectionId=CSM.ClsSectionId and CS.Reqid=R.Reqid ")
                .Append(" AND ACT.InstructionTypeID=CSM.InstructionTypeID ")
                .Append("AND	CSM.ClsSectionId = ? ")
                .Append(" AND R.UnitTypeId <> '2600592A-9739-4A13-BDCE-7A25FE4A7478' AND R.UnitTypeId is NOT NULL ") 'Mantis :DE6714 modified by balaji on 11/11/2011
                .Append("UNION ALL ")
                .Append("SELECT  ")
                .Append("		PWD.WorkDayId, ")
                .Append("		WD.WorkdaysDescrip, ")
                .Append("		TI1.TimeIntervalDescrip, ")
                .Append("		TI2.TimeIntervalDescrip, ")
                .Append(" (cast(datediff(mi,TI1.TimeIntervalDescrip,TI2.TimeIntervalDescrip) as decimal)/60)-( CAST(ISNULL(CSM.BreakDuration, 0)AS DECIMAL) / 60 ) as duration, ")
                .Append("		CSM.ClsSectionId,CSM.StartDate,CSM.EndDate , CSM.InstructionTypeID,ACT.InstructionTypeDescrip,CSM.ClsSectMeetingId,P.PeriodDescrip,ISNULL(R.UseTimeClock,0) AS UseTimeClock     ")
                .Append("FROM	arClsSectMeetings CSM, syPeriods P, syPeriodsWorkDays PWD, plWorkdays WD, cmTimeInterval TI1, cmTimeInterval TI2  , arInstructionType ACT ,arClassSections CS,arReqs R   ")
                .Append("WHERE ")
                .Append("		CSM.AltPeriodId=P.PeriodId ")
                .Append("AND	P.PeriodId=PWD.PeriodId ")
                .Append("AND	PWD.WorkDayId=WD.WorkDaysId ")
                .Append("AND	P.StartTimeId = TI1.TimeIntervalId  ")
                .Append("AND	P.EndTimeId = TI2.TimeIntervalId  ")
                .Append(" AND CS.ClsSectionId=CSM.ClsSectionId and CS.Reqid=R.Reqid ")
                .Append(" AND ACT.InstructionTypeID=CSM.InstructionTypeID ")
                .Append("AND	CSM.ClsSectionId = ? ")
                .Append(" AND R.UnitTypeId <> '2600592A-9739-4A13-BDCE-7A25FE4A7478' AND R.UnitTypeId is NOT NULL) T ORDER BY T.ClsSectionId,T.ClsSectMeetingId ") 'Mantis :DE6714 modified by balaji on 11/11/2011
            End With
            db.AddParameter("clssectionid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("clssectionid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Else
            With sb
                .Append("SELECT DISTINCT t1.WorkDaysId, t2.WorkDaysDescrip,t3.TimeIntervalDescrip,t4.TimeIntervalDescrip As EndTime, ")
                .Append(" (cast(datediff(mi,t3.TimeIntervalDescrip,t4.TimeIntervalDescrip) as decimal)/60)-( CAST(ISNULL(t1.BreakDuration, 0)AS DECIMAL) / 60 ) as duration, ")
                .Append(" t1.ClsSectionId,t1.StartDate,t1.EndDate, ")
                '.Append(" (select startDate from arClassSections where ClsSectionId=t1.ClsSectionId) as StartDate, ")
                '.Append(" (select EndDate from arClassSections where ClsSectionId=t1.ClsSectionId) as EndDate  , ")
                .Append(" t1.InstructionTypeID,ACT.InstructionTypeDescrip,t1.ClsSectMeetingId,t2.WorkDaysDescrip as PeriodDescrip,t2.ViewOrder,ISNULL(R.UseTimeClock,0) AS UseTimeClock    ")
                .Append(" FROM   arClsSectMeetings t1, plWorkDays t2, cmTimeInterval t3, cmTimeInterval t4  , arInstructionType ACT,arClassSections CS,arReqs R    ")
                .Append(" WHERE  t1.WorkDaysId = t2.WorkDaysId ")
                .Append("       AND t1.TimeIntervalId = t3.TimeIntervalId ")
                .Append("       AND t1.EndIntervalId = t4.TimeIntervalId ")
                .Append(" AND CS.ClsSectionId=t1.ClsSectionId and CS.Reqid=R.Reqid ")
                .Append(" AND ACT.InstructionTypeID=t1.InstructionTypeID ")
                .Append(" AND R.UnitTypeId <> '2600592A-9739-4A13-BDCE-7A25FE4A7478' AND R.UnitTypeId is NOT NULL ") 'Mantis :DE6714 modified by balaji on 11/11/2011
                .Append("       AND t1.ClsSectionId = ?  order by t2.ViewOrder  ")
            End With
            db.AddParameter("clssectionid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function


    Public Function GetClsSectionMeetingDaysgivenClssectandmeeting(clsSectionId As String, ClsSectMeetingID As String, usePeriods As Boolean) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet


        db.ConnectionString = conString
        If usePeriods Then
            With sb
                .Append("SELECT  ")
                .Append("		PWD.WorkDayId, ")
                .Append("		WD.WorkdaysDescrip, ")
                .Append("		TI1.TimeIntervalDescrip, ")
                .Append("		TI2.TimeIntervalDescrip As EndTime, ")
                .Append(" (cast(datediff(mi,TI1.TimeIntervalDescrip,TI2.TimeIntervalDescrip) as decimal)/60)-( CAST(ISNULL(CSM.BreakDuration, 0)AS DECIMAL) / 60 ) as duration, ")
                .Append("		ClsSectionId ,CSM.StartDate,CSM.EndDate  ,CSM.ClsSectMeetingID, CSM.InstructionTypeID,ACT.InstructionTypeDescrip    ")
                .Append("FROM	arClsSectMeetings CSM, syPeriods P, syPeriodsWorkDays PWD, plWorkdays WD, cmTimeInterval TI1, cmTimeInterval TI2 , arInstructionType ACT  ")
                .Append("WHERE ")
                .Append("		CSM.PeriodId=P.PeriodId ")
                .Append("AND	P.PeriodId=PWD.PeriodId ")
                .Append("AND	PWD.WorkDayId=WD.WorkDaysId ")
                .Append("AND	P.StartTimeId = TI1.TimeIntervalId  ")
                .Append("AND	P.EndTimeId = TI2.TimeIntervalId  ")
                .Append(" AND ACT.InstructionTypeID=CSM.InstructionTypeID ")
                .Append("AND	CSM.ClsSectionId = ? ")
                .Append(" AND CSM.ClsSectmeetingID =? ")
                .Append("UNION ALL ")
                .Append("SELECT  ")
                .Append("		PWD.WorkDayId, ")
                .Append("		WD.WorkdaysDescrip, ")
                .Append("		TI1.TimeIntervalDescrip, ")
                .Append("		TI2.TimeIntervalDescrip, ")
                .Append(" (cast(datediff(mi,TI1.TimeIntervalDescrip,TI2.TimeIntervalDescrip) as decimal)/60)-( CAST(ISNULL(CSM.BreakDuration, 0)AS DECIMAL) / 60 ) as duration, ")
                .Append("		ClsSectionId,CSM.StartDate,CSM.EndDate , CSM.ClsSectMeetingID,CSM.InstructionTypeID,ACT.InstructionTypeDescrip    ")
                .Append("FROM	arClsSectMeetings CSM, syPeriods P, syPeriodsWorkDays PWD, plWorkdays WD, cmTimeInterval TI1, cmTimeInterval TI2  , arInstructionType ACT  ")
                .Append("WHERE ")
                .Append("		CSM.AltPeriodId=P.PeriodId ")
                .Append("AND	P.PeriodId=PWD.PeriodId ")
                .Append("AND	PWD.WorkDayId=WD.WorkDaysId ")
                .Append("AND	P.StartTimeId = TI1.TimeIntervalId  ")
                .Append("AND	P.EndTimeId = TI2.TimeIntervalId  ")
                .Append(" AND ACT.InstructionTypeID=CSM.InstructionTypeID ")
                .Append("AND	CSM.ClsSectionId = ? ")
                .Append(" AND CSM.ClsSectmeetingID =? ")
            End With
            db.AddParameter("@clssectionid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@clsSectMeetingId", ClsSectMeetingID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.AddParameter("@clssectionid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@clsSectMeetingId", ClsSectMeetingID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Else
            With sb
                .Append("SELECT DISTINCT t1.WorkDaysId, t2.WorkDaysDescrip,t3.TimeIntervalDescrip,t4.TimeIntervalDescrip As EndTime, ")
                .Append(" cast(datediff(mi,t3.TimeIntervalDescrip,t4.TimeIntervalDescrip) as decimal)/60 as duration, ")
                .Append(" t1.ClsSectionId, t1.StartDate,t1.EndDate, ")
                '.Append(" (select startDate from arClassSections where ClsSectionId=t1.ClsSectionId) as StartDate, ")
                '.Append(" (select EndDate from arClassSections where ClsSectionId=t1.ClsSectionId) as EndDate  ,")
                .Append(" t1.ClsSectMeetingID, t1.InstructionTypeID,ACT.InstructionTypeDescrip ")
                .Append(" FROM   arClsSectMeetings t1, plWorkDays t2, cmTimeInterval t3, cmTimeInterval t4  , arInstructionType ACT  ")
                .Append(" WHERE  t1.WorkDaysId = t2.WorkDaysId ")
                .Append("       AND t1.TimeIntervalId = t3.TimeIntervalId ")
                .Append("       AND t1.EndIntervalId = t4.TimeIntervalId ")
                .Append(" AND ACT.InstructionTypeID=t1.InstructionTypeID ")
                .Append("       AND t1.ClsSectionId = ? ")
                .Append(" AND T1.ClsSectmeetingID =? ")
            End With
            db.AddParameter("@clssectionid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@clsSectMeetingId", ClsSectMeetingID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function
    ''Modified by Saraswathi lakshmanan to add the InstructionTypeID  for ByClass and clock Hr Project
    ''Added on August 15 2011
    Public Function GetClsSectionMeetingDays(StuEnrollId As String, TermId As String, usePeriods As Boolean) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet


        db.ConnectionString = conString
        If usePeriods Then
            With sb
                .Append("select * from (SELECT  ")
                .Append("		PWD.WorkDayId, ")
                .Append("		WD.WorkdaysDescrip, ")
                .Append("		TI1.TimeIntervalDescrip, ")
                .Append("		TI2.TimeIntervalDescrip As EndTime, ")
                .Append(" (cast(datediff(mi,TI1.TimeIntervalDescrip,TI2.TimeIntervalDescrip) as decimal)/60)-( CAST(ISNULL(CSM.BreakDuration, 0)AS DECIMAL) / 60 ) as duration, ")
                .Append("		CSM.ClsSectionId  ,CSM.StartDate,CSM.EndDate, CSM.InstructionTypeID,ACT.InstructionTypeDescrip,CSM.ClsSectMeetingId,P.PeriodDescrip,ISNULL(R.UseTimeClock,0) AS UseTimeClock    ")
                .Append("FROM	arClsSectMeetings CSM, syPeriods P, syPeriodsWorkDays PWD, plWorkdays WD, cmTimeInterval TI1, cmTimeInterval TI2, arInstructionType ACT,arClassSections CS,arReqs R ")
                .Append("WHERE ")
                .Append("		CSM.PeriodId=P.PeriodId ")
                .Append("AND	P.PeriodId=PWD.PeriodId ")
                .Append("AND	PWD.WorkDayId=WD.WorkDaysId ")
                .Append("AND	P.StartTimeId = TI1.TimeIntervalId  ")
                .Append("AND	P.EndTimeId = TI2.TimeIntervalId  ")
                .Append(" AND ACT.InstructionTypeID=CSM.InstructionTypeID ")
                .Append(" AND CS.ClsSectionId=CSM.ClsSectionId and CS.Reqid=R.Reqid ")
                .Append("AND	CSM.ClsSectionId IN (SELECT R.TestId FROM arResults R, arClassSections CS WHERE R.StuEnrollId=? AND R.TestId=CS.ClsSectionId AND CS.TermId IN (" & TermId & "))  ")
                .Append(" AND R.UnitTypeId <> '2600592A-9739-4A13-BDCE-7A25FE4A7478' AND R.UnitTypeId is NOT NULL") 'Mantis :DE6714 modified by balaji on 11/11/2011
                .Append(" UNION ALL ")
                .Append("SELECT  ")
                .Append("		PWD.WorkDayId, ")
                .Append("		WD.WorkdaysDescrip, ")
                .Append("		TI1.TimeIntervalDescrip, ")
                .Append("		TI2.TimeIntervalDescrip As EndTime, ")
                .Append(" (cast(datediff(mi,TI1.TimeIntervalDescrip,TI2.TimeIntervalDescrip) as decimal)/60)-( CAST(ISNULL(CSM.BreakDuration, 0)AS DECIMAL) / 60 ) as duration, ")
                .Append("		CSM.ClsSectionId  ,CSM.StartDate,CSM.EndDate, CSM.InstructionTypeID,ACT.InstructionTypeDescrip ,CSM.ClsSectMeetingId,P.PeriodDescrip,ISNULL(R.UseTimeClock,0) AS UseTimeClock   ")
                .Append("FROM	arClsSectMeetings CSM, syPeriods P, syPeriodsWorkDays PWD, plWorkdays WD, cmTimeInterval TI1, cmTimeInterval TI2, arInstructionType ACT,arClassSections CS,arReqs R ")
                .Append("WHERE ")
                .Append("		CSM.AltPeriodId=P.PeriodId ")
                .Append("AND	P.PeriodId=PWD.PeriodId ")
                .Append("AND	PWD.WorkDayId=WD.WorkDaysId ")
                .Append("AND	P.StartTimeId = TI1.TimeIntervalId  ")
                .Append("AND	P.EndTimeId = TI2.TimeIntervalId  ")
                .Append(" AND CS.ClsSectionId=CSM.ClsSectionId and CS.Reqid=R.Reqid ")
                .Append(" AND ACT.InstructionTypeID=CSM.InstructionTypeID ")
                .Append("AND	CSM.ClsSectionId IN (SELECT R.TestId FROM arResults R, arClassSections CS WHERE R.StuEnrollId=? AND R.TestId=CS.ClsSectionId AND CS.TermId IN (" & TermId & "))  ")
                .Append(" AND R.UnitTypeId <> '2600592A-9739-4A13-BDCE-7A25FE4A7478' AND R.UnitTypeId is NOT NULL)T  ORDER BY   T.ClsSectionId,T.ClsSectMeetingId ") 'Mantis :DE6714 modified by balaji on 11/11/2011
            End With

            db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'db.AddParameter("TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'db.AddParameter("TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Else
            With sb
                .Append("SELECT DISTINCT t1.WorkDaysId, t2.WorkDaysDescrip,t3.TimeIntervalDescrip,t4.TimeIntervalDescrip As EndTime, ")
                .Append(" (cast(datediff(mi,t3.TimeIntervalDescrip,t4.TimeIntervalDescrip) as decimal)/60)-( CAST(ISNULL(t1.BreakDuration, 0)AS DECIMAL) / 60 ) as duration, ")
                .Append(" t1.ClsSectionId  ,t1.startDate,t1.EndDate,")
                '.Append(" (select startDate from arClassSections where ClsSectionId=t1.ClsSectionId) as StartDate, ")
                '.Append(" (select EndDate from arClassSections where ClsSectionId=t1.ClsSectionId) as EndDate , ")
                .Append("t1.InstructionTypeID,ACT.InstructionTypeDescrip,t1.ClsSectMeetingId,t2.WorkDaysDescrip as PeriodDescrip,t2.ViewOrder,ISNULL(R.UseTimeClock,0) AS UseTimeClock    ")
                .Append("FROM   arClsSectMeetings t1,plWorkDays t2,cmTimeInterval t3,cmTimeInterval t4 , arInstructionType ACT,arClassSections CS,arReqs R  ")
                .Append("WHERE  t1.WorkDaysId=t2.WorkDaysId ")
                .Append("       AND t1.TimeIntervalId = t3.TimeIntervalId ")
                .Append("       AND t1.EndIntervalId = t4.TimeIntervalId ")
                .Append("       AND EXISTS (SELECT * FROM arResults t, arClassSections t2 ")
                .Append("                   WHERE t.StuEnrollId=? ")
                .Append("                   AND t.TestId=t2.ClsSectionId ")
                '.Append("                   AND t2.TermId=? ") 
                .Append("                   AND t2.TermId IN (" & TermId & ") ")
                .Append("                   AND t.TestId=t1.ClsSectionId) ")
                .Append(" AND CS.ClsSectionId=t1.ClsSectionId and CS.Reqid=R.Reqid ")
                '.Append(" AND ACT.InstructionTypeID=t1.InstructionTypeID order by t2.ViewOrder  ")
                '.Append(" AND R.UnitTypeId <> '2600592A-9739-4A13-BDCE-7A25FE4A7478' AND R.UnitTypeId is NOT NULL") 'Mantis :DE6714 modified by balaji on 11/11/2011
                ''Script error on August 02 2012
                .Append(" AND ACT.InstructionTypeID=t1.InstructionTypeID ")
                .Append(" AND R.UnitTypeId <> '2600592A-9739-4A13-BDCE-7A25FE4A7478' AND R.UnitTypeId is NOT NULL  order by t2.ViewOrder  ")

            End With
            db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'db.AddParameter("TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        End If

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function
    Public Function GetStudentsAttendanceForClsSection(clsSectionId As String, startDate As DateTime, endDate As DateTime) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet



        db.ConnectionString = conString

        With sb
            .Append("SELECT t1.StuEnrollId, t1.MeetDate,t1.Actual,t1.Tardy,t1.Excused ")
            .Append("FROM atClsSectAttendance t1 ")
            .Append("WHERE t1.ClsSectionId = ? ")
            .Append("AND t1.MeetDate >= ? ")
            .Append("AND t1.MeetDate <= ?")
        End With

        db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("startdate", startDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("enddate", endDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function


    Public Function GetStudentsAttendanceForClsSectionGivenClsSectandMeeting(clsSectionId As String, clsSectMeetingID As String,
                                                                             startDate As DateTime, endDate As DateTime,
                                                                             Optional ByVal campusId As String = "") As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet



        db.ConnectionString = conString

        Dim strUnitTypeId = ""
        Dim strTrackAttendance = ""

        Try
            With sb
                .Append("Select Top 1 Value from ( ")
                .Append(" Select Distinct Value from syConfigAppSetValues where SettingId=72 and CampusId=? ")
                .Append(" Union Select Distinct Value from syConfigAppSetValues where SettingId=72 and CampusId is NULL ")
                .Append(" ) t1")
            End With
            db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            strTrackAttendance = db.RunParamSQLScalar(sb.ToString)
        Catch ex As Exception
            strTrackAttendance = ""
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try

        Try
            With sb
                .Append(" Select Top 1 UnitTypeId from  ")
                .Append(" (Select Top 1 t2.UnitTypeId from arClassSections t1, arReqs t2 where t1.ReqId=t2.ReqId and t1.ClsSectionId=? ")
                .Append(" Union ")
                .Append(" Select Top 1 t1.UnitTypeId from arPrgVersions t1, arStuEnrollments t2, atClsSectAttendance t3 ")
                .Append(" where t1.PrgVerId=t2.PrgVerId and t2.StuEnrollId=t3.StuEnrollId and t3.ClsSectMeetingId=? and ")
                .Append(" t3.ClsSectionId=? ")
                .Append(" ) t1 ")
            End With
            db.AddParameter("@clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ClsSectMeetingID", clsSectMeetingID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@clssectid1", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            strUnitTypeId = db.RunParamSQLScalar(sb.ToString).ToString
        Catch ex As Exception
            strUnitTypeId = ""
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try

        With sb
            .Append("SELECT t1.StuEnrollId, t1.MeetDate,t1.Actual,t1.Tardy,t1.Excused,t1.Scheduled ")
            .Append(" FROM atClsSectAttendance t1 ")
            .Append(" WHERE t1.ClsSectionId = ? ")
            .Append(" AND t1.MeetDate >= ? ")
            .Append(" AND t1.MeetDate <= ?")
            .Append(" And ClsSectMeetingID= ? ")
            .Append(" AND (Actual<>9999.0 AND Actual<>999.0) ")
        End With
        'End If

        db.AddParameter("@clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@startdate", startDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@enddate", endDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@ClsSectMeetingID", clsSectMeetingID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function


    Public Overloads Sub AddStudentClsSectionAttendanceRecord(stuEnrollId As String, clsSectionId As String,
        meetDate As DateTime, actual As Decimal, tardy As Boolean)
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        'Dim ds As New DataSet


        db.ConnectionString = conString

        With sb
            .Append("INSERT INTO atClsSectAttendance(stuEnrollId,ClsSectionId,MeetDate,Actual,Tardy,ModUser,ModDate) ")
            .Append("VALUES(?,?,?,?,?,?,GETDATE())")
        End With

        db.AddParameter("stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("meetdate", meetDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("actual", actual, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
        db.AddParameter("tardy", tardy, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
        db.AddParameter("ModUser", currentUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)

        'Update the Student Status from Future Start to CurrentlyAttending
        UpdateStudentFromFutureStartToCurrentlyAttending(stuEnrollId, meetDate)
    End Sub

    Public Overloads Sub AddStudentClsSectionAttendanceRecord(stuEnrollId As String, clsSectionId As String,
        meetDate As DateTime, actual As Decimal, tardy As Boolean, excused As Boolean, Optional scheduled As Integer = 0, Optional ClsSectMeetingId As String = "")
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        'Dim ds As New DataSet



        db.ConnectionString = conString

        With sb
            .Append("Delete from atClsSectAttendance where (Actual = 9999 or Actual=999) and stuEnrollId	= ?  AND ClsSectionId= ? AND MeetDate= ?; ")
            If ClsSectMeetingId <> "" Then
                .Append("INSERT INTO atClsSectAttendance(stuEnrollId,ClsSectionId,MeetDate,Actual,Tardy,Excused,scheduled,ClsSectMeetingId,ModUser,ModDate) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,GETDATE())")

            Else
                .Append("INSERT INTO atClsSectAttendance(stuEnrollId,ClsSectionId,MeetDate,Actual,Tardy,Excused,ModUser,ModDate) ")
                .Append("VALUES(?,?,?,?,?,?,?,GETDATE())")
            End If

        End With
        db.AddParameter("stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("meetdate", meetDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("meetdate", meetDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("actual", actual, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
        db.AddParameter("tardy", tardy, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
        db.AddParameter("excused", excused, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
        If ClsSectMeetingId <> "" Then
            db.AddParameter("scheduled", scheduled, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
            db.AddParameter("ClsSectMeetingId", ClsSectMeetingId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        db.AddParameter("ModUser", currentUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()

        'Update the Student Status from Future Start to CurrentlyAttending
        UpdateStudentFromFutureStartToCurrentlyAttending(stuEnrollId, meetDate)
    End Sub

    Public Overloads Sub AddStudentClsSectionAttendanceRecordAll(stuEnrollId As String, clsSectionId As String,
        meetDate As DateTime, actual As Decimal, tardy As Boolean, excused As Boolean, Optional scheduled As Integer = 0, Optional ClsSectMeetingId As String = "")
        Dim db As New DataAccess
        Dim sb As StringBuilder
        'Dim ds As New DataSet
        Dim stuEnrollmentId As String

        Dim regDb = New RegisterDB()

        Dim enrollments As List(Of String) = regDb.GetActiveEnrollmentWithClassAssociated(stuEnrollId, clsSectionId.ToString)




        Dim groupTrans As OleDbTransaction = db.StartTransaction()
        Try
            For Each stuEnroll As String In enrollments


                stuEnrollmentId = stuEnroll

                sb = New StringBuilder()

                db.ConnectionString = conString

                With sb
                    .Append("Delete from atClsSectAttendance where (Actual = 9999 or Actual=999) and stuEnrollId	= ?  AND ClsSectionId= ? AND MeetDate= ?; ")
                    If ClsSectMeetingId <> "" Then
                        .Append("INSERT INTO atClsSectAttendance(stuEnrollId,ClsSectionId,MeetDate,Actual,Tardy,Excused,scheduled,ClsSectMeetingId,ModUser,ModDate) ")
                        .Append("VALUES(?,?,?,?,?,?,?,?,?,GETDATE())")

                    Else
                        .Append("INSERT INTO atClsSectAttendance(stuEnrollId,ClsSectionId,MeetDate,Actual,Tardy,Excused,ModUser,ModDate) ")
                        .Append("VALUES(?,?,?,?,?,?,?,GETDATE())")
                    End If

                End With
                db.AddParameter("stuEnrollId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("meetdate", meetDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
                db.AddParameter("stuEnrollId", stuEnrollmentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("meetdate", meetDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
                db.AddParameter("actual", actual, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                db.AddParameter("tardy", tardy, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                db.AddParameter("excused", excused, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                If ClsSectMeetingId <> "" Then
                    db.AddParameter("scheduled", scheduled, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
                    db.AddParameter("ClsSectMeetingId", ClsSectMeetingId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("ModUser", currentUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()

                'Update the Student Status from Future Start to CurrentlyAttending
                UpdateStudentFromFutureStartToCurrentlyAttending(stuEnrollmentId, meetDate)




            Next

            '   commit transaction 
            groupTrans.Commit()

        Catch ex As OleDbException
            groupTrans.Rollback()
            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Sub

    Public Overloads Sub UpdateStudentClsSectionAttendanceRecord(stuEnrollId As String,
        clsSectionId As String, meetDate As DateTime, actual As Decimal, tardy As Boolean)
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        'Dim ds As New DataSet


        db.ConnectionString = conString

        With sb
            .Append("UPDATE atClsSectAttendance ")
            .Append("SET Actual = ?,Tardy = ?,ModDate = GETDATE(), ModUser = ? ")
            .Append("WHERE stuEnrollId = ? ")
            .Append("AND ClsSectionId = ? ")
            .Append("AND MeetDate = ?")
        End With

        db.AddParameter("actual", actual, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
        db.AddParameter("tardy", tardy, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
        db.AddParameter("ModUser", currentUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("meetdate", meetDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)

        'Update the Student Status from Future Start to CurrentlyAttending
        UpdateStudentFromFutureStartToCurrentlyAttending(stuEnrollId, meetDate)
    End Sub

    Public Overloads Sub UpdateStudentClsSectionAttendanceRecord(stuEnrollId As String,
        clsSectionId As String, meetDate As DateTime, actual As Decimal,
        tardy As Boolean, excused As Boolean, Optional ClsSectMeetingId As String = "")
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        'Dim ds As New DataSet


        db.ConnectionString = conString

        With sb
            .Append("UPDATE atClsSectAttendance ")
            .Append("SET Actual = ?, Tardy = ?, Excused = ?,ModDate = GETDATE(), ModUser = ? ")
            .Append("WHERE stuEnrollId = ? ")
            .Append("AND ClsSectionId = ? ")
            .Append("AND MeetDate = ?")
            If ClsSectMeetingId <> "" Then
                .Append(" and ClsSectMeetingId = ? ")
            End If
        End With

        db.AddParameter("actual", actual, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
        db.AddParameter("tardy", tardy, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
        db.AddParameter("excused", excused, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
        db.AddParameter("ModUser", currentUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("meetdate", meetDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        If ClsSectMeetingId <> "" Then
            db.AddParameter("ClsSectMeetingId", ClsSectMeetingId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        db.RunParamSQLExecuteNoneQuery(sb.ToString)

        'Update the Student Status from Future Start to CurrentlyAttending
        UpdateStudentFromFutureStartToCurrentlyAttending(stuEnrollId, meetDate)
    End Sub

    Public Overloads Sub UpdateStudentClsSectionAttendanceRecordAll(stuEnrollId As String,
        clsSectionId As String, meetDate As DateTime, actual As Decimal,
        tardy As Boolean, excused As Boolean, Optional ClsSectMeetingId As String = "")
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        'Dim ds As New DataSet

        Dim stuId As String

        Dim regDb = New RegisterDB()

        Dim enrollments As List(Of String) = regDb.GetActiveEnrollmentWithClassAssociated(stuEnrollId, clsSectionId.ToString)


        Dim groupTrans As OleDbTransaction = db.StartTransaction()
        Try
            For Each stuEnroll As String In enrollments
                stuId = stuEnroll

                db.ConnectionString = conString

                With sb
                    .Append("UPDATE atClsSectAttendance ")
                    .Append("SET Actual = ?, Tardy = ?, Excused = ?, ModDate = GETDATE(), ModUser = ?  ")
                    .Append("WHERE stuEnrollId = ? ")
                    .Append("AND ClsSectionId = ? ")
                    .Append("AND MeetDate = ?")
                    If ClsSectMeetingId <> "" Then
                        .Append(" and ClsSectMeetingId = ? ")
                    End If
                End With

                db.AddParameter("actual", actual, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                db.AddParameter("tardy", tardy, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                db.AddParameter("excused", excused, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                db.AddParameter("ModUser", currentUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("stuEnrollId", stuId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("meetdate", meetDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
                If ClsSectMeetingId <> "" Then
                    db.AddParameter("ClsSectMeetingId", ClsSectMeetingId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                db.RunParamSQLExecuteNoneQuery(sb.ToString)

                'Update the Student Status from Future Start to CurrentlyAttending
                UpdateStudentFromFutureStartToCurrentlyAttending(stuId, meetDate)
            Next

            '   commit transaction 
            groupTrans.Commit()

        Catch ex As OleDbException
            groupTrans.Rollback()
            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Sub



    Public Sub DeleteStudentClsSectionAttendanceRecordAll(stuEnrollId As String, clsSectionId As String, meetDate As DateTime, Optional ClsSectMeetingId As String = "")
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        'Dim ds As New DataSet


        db.ConnectionString = conString




        Dim stuId As String

        Dim regDb = New RegisterDB()

        Dim enrollments As List(Of String) = regDb.GetActiveEnrollmentWithClassAssociated(stuEnrollId, clsSectionId.ToString)


        Dim groupTrans As OleDbTransaction = db.StartTransaction()
        Try
            For Each stuEnroll As String In enrollments
                stuId = stuEnroll

                With sb
                    .Append("DELETE atClsSectAttendance ")
                    .Append("WHERE stuEnrollId = ? ")
                    .Append("AND ClsSectionId = ? ")
                    .Append("AND MeetDate = ?")
                    If ClsSectMeetingId <> "" Then
                        .Append(" and ClsSectMeetingId = ? ")
                    End If
                End With

                db.AddParameter("stuEnrollId", stuId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("meetdate", meetDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
                If ClsSectMeetingId <> "" Then
                    db.AddParameter("ClsSectMeetingId", ClsSectMeetingId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                db.RunParamSQLExecuteNoneQuery(sb.ToString)
            Next

            '   commit transaction 
            groupTrans.Commit()

        Catch ex As OleDbException
            groupTrans.Rollback()
            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Sub

    Public Sub DeleteStudentClsSectionAttendanceRecord(stuEnrollId As String, clsSectionId As String, meetDate As DateTime, Optional ClsSectMeetingId As String = "")
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        'Dim ds As New DataSet


        db.ConnectionString = conString

        With sb
            .Append("DELETE atClsSectAttendance ")
            .Append("WHERE stuEnrollId = ? ")
            .Append("AND ClsSectionId = ? ")
            .Append("AND MeetDate = ?")
            If ClsSectMeetingId <> "" Then
                .Append(" and ClsSectMeetingId = ? ")
            End If
        End With

        db.AddParameter("stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("meetdate", meetDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        If ClsSectMeetingId <> "" Then
            db.AddParameter("ClsSectMeetingId", ClsSectMeetingId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
    End Sub

    Public Function GetDefaultCurrentlyAttendingStatusFromEnrollment(StuEnrollId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim currentAttendingDefaultStatusQuery As New StringBuilder
        Dim programVersionIdQuery As New StringBuilder
        Dim campusGroupIdQuery As New StringBuilder
        Dim campusGroupId = ""
        Dim programVersionId = ""
        Dim currentlyAttendingStatusId = ""

        With programVersionIdQuery
            .Append("SELECT PrgVerId FROM dbo.arStuEnrollments WHERE StuEnrollId = ?")
        End With

        Try
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Dim programVersionIdObject = db.RunParamSQLScalar(programVersionIdQuery.ToString)
            programVersionId = Convert.ToString(programVersionIdObject)
            db.ClearParameters()
        Catch ex As Exception
        End Try

        With campusGroupIdQuery
            .Append("SELECT CampGrpId FROM dbo.arPrgVersions WHERE PrgVerId  = ? ")
        End With

        Try
            db.AddParameter("@PrgVerId", programVersionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Dim campusGroupIdObject = db.RunParamSQLScalar(campusGroupIdQuery.ToString)
            campusGroupId = Convert.ToString(campusGroupIdObject)
            db.ClearParameters()
        Catch ex As Exception
        End Try

        With currentAttendingDefaultStatusQuery
            .Append("SELECT dbo.GetCurrentlyAttendingDefaultStatusIdForCampusGroup( ? )")
        End With

        Try
            db.AddParameter("@campusGroupId1", campusGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Dim currentAttendingDefaultStatusQueryIdObject = db.RunParamSQLScalar(currentAttendingDefaultStatusQuery.ToString)
            currentlyAttendingStatusId = Convert.ToString(currentAttendingDefaultStatusQueryIdObject)
            db.ClearParameters()
        Catch ex As Exception
        End Try
        Return currentlyAttendingStatusId
    End Function

    Public Sub UpdateStudentFromFutureStartToCurrentlyAttending(StuEnrollId As String, meetdate As DateTime)
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim currentAttendingDefaultStatusQuery As New StringBuilder
        Dim programVersionIdQuery As New StringBuilder
        Dim campusGroupIdQuery As New StringBuilder
        Dim campusGroupId = ""
        Dim programVersionId = ""
        Dim currentlyAttendingStatusId = ""

        With programVersionIdQuery
            .Append("SELECT PrgVerId FROM dbo.arStuEnrollments WHERE StuEnrollId = ?")
        End With

        Try
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Dim programVersionIdObject = db.RunParamSQLScalar(programVersionIdQuery.ToString)
            programVersionId = Convert.ToString(programVersionIdObject)
            db.ClearParameters()
        Catch ex As Exception
        End Try

        With campusGroupIdQuery
            .Append("SELECT CampGrpId FROM dbo.arPrgVersions WHERE PrgVerId  = ? ")
        End With

        Try
            db.AddParameter("@PrgVerId", programVersionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Dim campusGroupIdObject = db.RunParamSQLScalar(campusGroupIdQuery.ToString)
            campusGroupId = Convert.ToString(campusGroupIdObject)
            db.ClearParameters()
        Catch ex As Exception
        End Try

        With currentAttendingDefaultStatusQuery
            .Append("SELECT dbo.GetCurrentlyAttendingDefaultStatusIdForCampusGroup( ? )")
        End With

        Try
            db.AddParameter("@campusGroupId1", campusGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Dim currentAttendingDefaultStatusQueryIdObject = db.RunParamSQLScalar(currentAttendingDefaultStatusQuery.ToString)
            currentlyAttendingStatusId = Convert.ToString(currentAttendingDefaultStatusQueryIdObject)
            db.ClearParameters()
        Catch ex As Exception
        End Try

        'Dim MyAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    MyAdvAppSettings = New AdvAppSettings
        'End If
        'db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            '    .Append("Update arStuEnrollments set StatusCodeId=(select Distinct StatusCodeId from syStatusCodes where StatusCodeDescrip='Currently Attending') where StuEnrollId=? and StatusCodeId=(select Distinct StatusCodeId from syStatusCodes where StatusCodeDescrip='Future Start') ")
            ''DE8240  QA: Students status is not changed to currently attending when attendance is posted.
            ''Aug 15 2012
            'DE8192 - QA: Changing status from future start to currently attending when posting attendance should pick up the active statuses. - 10/5/2012
            .Append("INSERT INTO [dbo].[syStudentStatusChanges] ([StuEnrollId] ,[OrigStatusId] ,[NewStatusId],[CampusId],[ModDate],[ModUser],[IsReversal],[DropReasonId],[DateOfChange],[Lda],[CaseNumber],[RequestedBy],[HaveBackup],[HaveClientConfirmation]) ")
            .Append("SELECT StuEnrollId,e.StatusCodeId  OrigStatusId, (?) NewStatusId,e.CampusId CampusId,GETDATE() ModDate ,? ModUser  ,0 IsReversal ,NULL DropReasonId ,? DateOfChange,NULL Lda,NULL CaseNumber,NULL RequestedBy,NULL HaveBackup,NULL HaveClientConfirmation ")
            .Append("FROM dbo.arStuEnrollments  e ")
            .Append("JOIN dbo.adLeads l ON e.StudentId = l.StudentId ")
            .Append(" WHERE StuEnrollId = ? AND e.StatusCodeId IN (
                                                 SELECT DISTINCT StatusCodeId
                                                 FROM   syStatusCodes
                                                 WHERE  SysStatusId = 7
                                                 )")
            .Append("Update arStuEnrollments set StatusCodeId=(?), ModDate = GETDATE() , ModUser = ? WHERE StuEnrollId=? and StatusCodeId in (select Distinct StatusCodeId from syStatusCodes where SysStatusId=7) ")

        End With
        db.AddParameter("@currentlyAttendingStatusId1", currentlyAttendingStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModUser", currentUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DateOfChange", meetdate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@currentlyAttendingStatusId2", currentlyAttendingStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModUser", currentUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As Exception
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            db.CloseConnection()
        End Try
    End Sub
    Public Function GetClassSectionAttendanceType(clsSectionId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet
        Dim result = ""


        db.ConnectionString = conString
        With sb
            .Append("SELECT t3.UnitTypeDescrip ")
            .Append("FROM arClassSections t1, arReqs t2, arAttUnitType t3 ")
            .Append("WHERE t1.ClsSectionId = ? ")
            .Append("AND t1.ReqId = t2.ReqId ")
            .Append("AND t2.UnitTypeId = t3.UnitTypeId ")
        End With
        db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                result = ds.Tables(0).Rows(0)("UnitTypeDescrip")
            End If
        End If

        Return result
    End Function
    Public Function GetCancelDays(clsSectionId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet


        db.ConnectionString = conString

        With sb
            .Append("Select StartDate,EndDate,ClsSectionId from arUnschedClosures where ClsSectionId = ? ")
            '.Append(" and StartDate >= ? and EndDate <= ? ")
        End With

        db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'db.AddParameter("startdate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'db.AddParameter("enddate", EndDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString())

        Return ds.Tables(0)
    End Function
    Public Function GetCancelDaysForMeeting(clsSectionMeetingId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet


        db.ConnectionString = conString

        With sb
            .Append("Select StartDate,EndDate,ClsSectionId,ClsMeetingId from arUnschedClosures where ClsMeetingId = ? ")
            '.Append(" and StartDate >= ? and EndDate <= ? ")
        End With

        db.AddParameter("clsSectionMeetingId", clsSectionMeetingId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'db.AddParameter("startdate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'db.AddParameter("enddate", EndDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString())

        Return ds.Tables(0)
    End Function
    Public Function GetReScheduleDaysForMeeting(clsSectionMeetingId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet


        db.ConnectionString = conString

        With sb
            .Append("Select StartDate,EndDate,ClsSectionId,ClsMeetingId,ClassReSchduledTo from arUnschedClosures where ClsMeetingId = ? ")
            '.Append(" and StartDate >= ? and EndDate <= ? ")
        End With

        db.AddParameter("clsSectionMeetingId", clsSectionMeetingId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'db.AddParameter("startdate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'db.AddParameter("enddate", EndDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString())

        Return ds.Tables(0)
    End Function
    Public Function IsMeetDateUnposted(clsSectionId As String, meetDate As DateTime) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim numRecords As Integer

        db.ConnectionString = conString

        With sb
            .Append("SELECT COUNT(MeetDate) ")
            .Append("FROM atClsSectAttendance ")
            .Append("WHERE ClsSectionId = ? ")
            .Append("AND MeetDate = ?")
        End With

        db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("meetdate", meetDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        numRecords = db.RunParamSQLScalar(sb.ToString)

        If numRecords = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function IsDateHoliday(dtm As DateTime) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim count As Integer


        db.ConnectionString = conString

        With sb
            .Append("SELECT COUNT(*) ")
            .Append("FROM syHolidays ")
            .Append("WHERE HolidayEndDate >= ? and HolidayStartDate <= ? ")
        End With

        db.AddParameter("@dtm", dtm, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@dtm", dtm, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        count = CInt(db.RunParamSQLScalar(sb.ToString))

        If count = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    Public Function GetClsSectionsForStudent(StuEnrollId As String, termID As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet


        db.ConnectionString = conString

        With sb
            '   Retrieve class sections for a specific term  
            '   or all class sections corresponding all terms
            .Append("SELECT t1.ClsSectionId,t2.Descrip,t1.ClsSection,t1.StartDate,t1.EndDate,t2.code,")
            .Append("       (SELECT FullName FROM syUsers WHERE UserId=t1.InstructorId) AS Instructor ")
            .Append("FROM arResults t,arClassSections t1, arReqs t2, arAttUnitType t3 ")
            .Append("WHERE t.StuEnrollId = ? ")
            .Append("AND t.TestId=t1.ClsSectionId ")
            .Append("AND t1.TermId = ? ")
            .Append("AND t1.ReqId = t2.ReqId ")
            .Append("AND t2.UnitTypeId = t3.UnitTypeId ")
            .Append("AND t3.UnitTypeDescrip <> 'None' ")
        End With

        db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("termid", termID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function GetClsSectionsForStudent(StuEnrollId As String, termID As String, PrgVerTracksAttendance As Boolean) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet



        db.ConnectionString = conString

        With sb
            .Append("SELECT t1.ClsSectionId,t2.Descrip,t1.ClsSection,t1.StartDate,t1.EndDate,t2.code,")
            .Append("       (SELECT FullName FROM syUsers WHERE UserId=t1.InstructorId) AS Instructor, ")
            .Append("       (SELECT TermDescrip FROM arTerm WHERE TermId=t1.TermId) AS TermDescrip, ")
            .Append(" ISNULL((select IsDrop from arGradeSystemDetails where GrdSysDetailId=t.GrdSysDetailId),0) AS IsDrop ")
            .Append("FROM arResults t,arClassSections t1, arReqs t2, arAttUnitType t3 ")
            .Append("WHERE t.StuEnrollId = ? ")

            .Append("AND t.TestId=t1.ClsSectionId ")
            '.Append(" and      (  t.GrdSysDetailId not in (select GrdSysDetailId from arGradeSystemDetails where IsDrop=1) ")
            '.Append(" or t.GrdSysDetailId is null) ")
            If termID <> "" Then
                .Append(" AND t1.TermId IN (" & termID & ") ")
                'Else
                '    .Append("AND t1.TermId = '00000000-0000-0000-0000-000000000000' ")
            End If
            .Append("AND t1.ReqId = t2.ReqId ")
            .Append("AND t2.UnitTypeId = t3.UnitTypeId ")
            'If Not PrgVerTracksAttendance Then
            .Append("AND t3.UnitTypeDescrip <> 'None' ORDER BY t1.StartDate,t2.descrip,t2.code  ")
            'End If
        End With

        db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'db.AddParameter("termid", termID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function GetCourseTypesandClsSectionsForStudent(StuEnrollId As String, termID As String, PrgVerTracksAttendance As Boolean) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet


        db.ConnectionString = conString

        With sb
            .Append("SELECT t1.ClsSectionId,t2.Descrip,t1.ClsSection,t1.StartDate,t1.EndDate,")
            .Append("       (SELECT FullName FROM syUsers WHERE UserId=t1.InstructorId) AS Instructor, ")
            .Append("       (SELECT TermDescrip FROM arTerm WHERE TermId=t1.TermId) AS TermDescrip, CSM.InstructionTypeID,ACT.InstructionTypeDescrip ")
            .Append("FROM arResults t,arClassSections t1, arReqs t2, arAttUnitType t3, arClsSectMeetings CSM, arInstructionType ACT ")
            .Append("WHERE t.StuEnrollId = ? ")

            .Append("AND t.TestId=t1.ClsSectionId ")
            .Append(" And t1.ClsSectionId=CSM.ClsSectionID And ACT.InstructionTypeID=CSM.InstructionTypeID  ")
            .Append(" and      (  t.GrdSysDetailId not in (select GrdSysDetailId from arGradeSystemDetails where IsDrop=1) ")
            .Append(" or t.GrdSysDetailId is null) ")
            If termID <> "" Then
                .Append(" AND t1.TermId IN (" & termID & ") ")
                'Else
                '    .Append("AND t1.TermId = '00000000-0000-0000-0000-000000000000' ")
            End If
            .Append("AND t1.ReqId = t2.ReqId ")
            .Append("AND t2.UnitTypeId = t3.UnitTypeId ")
            If Not PrgVerTracksAttendance Then
                .Append("AND t3.UnitTypeDescrip <> 'None' ")
            End If
        End With

        db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'db.AddParameter("termid", termID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function GetAllClsSectionsForStudent(StuEnrollId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        db.ConnectionString = conString

        With sb
            .Append("SELECT t1.ClsSectionId,t2.Descrip,t1.ClsSection,t1.StartDate,t2.code ")
            .Append("FROM arResults t,arClassSections t1, arReqs t2, arAttUnitType t3 ")
            .Append("WHERE t.StuEnrollId = ? ")
            .Append("AND t.TestId=t1.ClsSectionId ")
            .Append("AND t1.ReqId = t2.ReqId ")
            .Append("AND t2.UnitTypeId = t3.UnitTypeId ")
            .Append("AND t3.UnitTypeDescrip <> 'None' ")
        End With

        db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function
    ''Modified by Saraswathi lakshmanan to add the InstructionTypeID for BY Class and Clock Hr Project
    ''Added on August 15 2011

    Public Function GetStudentAttendance(StuEnrollId As String, clsSectionId As String, TermId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet



        db.ConnectionString = conString

        If clsSectionId = Guid.Empty.ToString Then
            With sb
                .Append("SELECT t1.StuEnrollId,t1.MeetDate,t1.Actual,t1.Tardy,t1.Excused,t1.ClsSectionId,CSM.InstructionTypeID, ACT.InstructionTypeDescrip,t1.Scheduled,t1.ClsSectMeetingId ")
                .Append("FROM   atClsSectAttendance t1, arClassSections t2, arClsSectMeetings CSM, arInstructionType ACT ")
                .Append("WHERE  t1.ClsSectionId=t2.ClsSectionId AND t1.ClsSectMeetingId=CSM.ClsSectMeetingId ")
                .Append("       AND EXISTS (SELECT * FROM arResults t, arClassSections t2 ")
                .Append("                       WHERE t.StuEnrollId=? ")
                .Append("                       AND t.TestId=t2.ClsSectionId ")
                '.Append("                       AND t2.TermId=? ")
                .Append("                       AND t2.TermId IN (" & TermId & ") ")
                .Append("                       AND t.TestId=t1.ClsSectionId) ")
                .Append("       AND t1.StuEnrollId = ? AND t1.MeetDate >= t2.StartDate ")
                .Append("       AND t1.MeetDate < '" & Date.Today.AddDays(1) & "' ")
                .Append(" AND CSM.ClsSectionID=t2.ClsSectionID and CSM.InstructionTypeID= ACT.InstructionTypeID  AND t1.Actual<> 999 and t1.Actual<> 9999 ")
            End With
            db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'db.AddParameter("TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Else
            With sb
                .Append("SELECT t1.StuEnrollId,t1.MeetDate,t1.Actual,t1.Tardy,t1.Excused,t1.ClsSectionId,CSM.InstructionTypeID, ACT.InstructionTypeDescrip,t1.Scheduled,t1.ClsSectMeetingId    ")
                .Append("FROM   atClsSectAttendance t1, arClassSections t2 , arClsSectMeetings CSM, arInstructionType ACT ")
                .Append("WHERE  t1.ClsSectionId=t2.ClsSectionId AND t1.ClsSectMeetingId=CSM.ClsSectMeetingId ")
                .Append("       AND t1.ClsSectionId = ? ")
                .Append("       AND t1.StuEnrollId = ? ")
                .Append("       AND t1.MeetDate >= t2.StartDate ")
                .Append("       AND t1.MeetDate < '" & Date.Today.AddDays(1) & "' ")
                .Append(" AND CSM.ClsSectionID=t2.ClsSectionID and CSM.InstructionTypeID= ACT.InstructionTypeID   AND t1.Actual<> 999 and t1.Actual<>9999 ")
            End With

            db.AddParameter("ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds.Tables(0)
    End Function

    Public Function GetLOAInfoForStudentsInClassSection(clsSectionId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet


        db.ConnectionString = conString

        With sb
            .Append("SELECT StuEnrollId,StartDate,EndDate ")
            .Append("FROM arStudentLOAs ")
            .Append("WHERE StuEnrollId IN")
            .Append("(SELECT StuEnrollId FROM arResults WHERE TestId = ?) ")
        End With

        db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)


    End Function

    Public Function GetLOASuspandHolidaysforStudentsInClassSection(clsSectionId As String, campusID As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet



        db.ConnectionString = conString

        With sb
            .Append("SELECT L.StuEnrollId,StartDate,EndDate  ")
            .Append("FROM arStudentLOAs  L, arResults R WHERE R.StuEnrollId=L.StuEnrollId and  R.TestId=? ;")
            .Append("SELECT S.StuEnrollId,StartDate,EndDate ")
            .Append("FROM arStdSuspensions  S, arResults R WHERE R.StuEnrollId=S.StuEnrollId AND R.TestId=? ;")
            .Append("SELECT HolidayID, HolidayStartDate,HolidayEndDate,AllDay,StartTimeId,(SELECT TimeIntervalDescrip FROM dbo.cmTimeInterval WHERE TimeIntervalId=StartTimeId ) AS StartTime,EndTimeId , (SELECT TimeIntervalDescrip FROM dbo.cmTimeInterval WHERE TimeIntervalId=EndTimeId ) AS EndTime ")
            .Append(" FROM dbo.syHolidays H WHERE")
            .Append(" H.StatusID in(SELECT StatusID FROM dbo.syStatuses WHERE Status='Active') ")
            .Append(" and H.CampGrpId IN (SELECT CampGrpId FROM dbo.syCmpGrpCmps WHERE campusID=?);")
            .Append("SELECT R.StuEnrollId,dbo.GetLDA(S.StuEnrollId) AS LDA  ")
            .Append("FROM arResults R,arStuEnrollments S WHERE  R.stuEnrollid=S.StuEnrollId and  R.TestId=? ;")
            .Append(" SELECT  R.StuEnrollId ,CS.StartDate,CS.EndDate FROM    arResults R ,dbo.arClassSections CS,dbo.arGradeSystemDetails GSD,arStuEnrollments S ")
            .Append(" WHERE R.TestId = CS.ClsSectionId AND R.GrdSysDetailId =GSD.GrdSysDetailId AND  R.stuEnrollid = S.StuEnrollId AND gsd.IsDrop=1  AND R.TestId = ? ;")
        End With

        db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("CampusId", campusID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)
        ds.Tables(0).TableName = "LOADays"
        ds.Tables(1).TableName = "SuspensionDays"
        ds.Tables(2).TableName = "Holidays"
        ds.Tables(3).TableName = "LDA"
        ds.Tables(4).TableName = "DropCourses"
        Return ds


    End Function


    Public Function GetDropAndLOAInfoForStudent(stuEnrollId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        db.ConnectionString = conString

        With sb
            .Append("SELECT ")
            .Append("       t3.StuEnrollId,t3.StartDate,")
            .Append("       t3.StatusCodeId,t3.DateDetermined,")
            .Append("       t4.StatusCodeDescrip,t4.SysStatusId,dbo.GetLDA(t3.StuEnrollId) AS LDA ")
            .Append("FROM   arStuEnrollments t3,syStatusCodes t4  ")
            .Append("WHERE  t3.StuEnrollId=? AND t3.StatusCodeId=t4.StatusCodeId ")
            .Append(";")
            .Append("SELECT ")
            .Append("       StuEnrollId,StartDate,EndDate ")
            .Append("FROM   arStudentLOAs ")
            .Append("WHERE  StuEnrollId=? ")
            .Append(";")
            .Append("SELECT ")
            .Append("       StuEnrollId,StartDate,EndDate ")
            .Append("FROM   arStdSuspensions ")
            .Append("WHERE  StuEnrollId=? ")
        End With

        db.AddParameter("StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "DropInfo"
            ds.Tables(1).TableName = "LOAInfo"
            ds.Tables(2).TableName = "SuspInfo"
        End If

        Return ds
    End Function

    Public Function DoesEnrollmentHaveStartDate(stuEnrollId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim i As Integer

        With sb
            .Append("SELECT COUNT(*) ")
            .Append("FROM arStuEnrollments ")
            .Append("WHERE StuEnrollId = ? ")
            .Append("AND StartDate IS NOT NULL ")
        End With

        db.AddParameter("enrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        i = CInt(db.RunParamSQLScalar(sb.ToString))

        If i = 0 Then
            Return False
        Else
            Return True
        End If

    End Function
    Public Function GetCurrAttendingStatus() As String
        'Dim ds As New DataSet
        'Dim da As OleDbDataAdapter
        Dim sStatusCodeId As String = String.Empty

        Try


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = conString
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT DISTINCT ")
                .Append("       A.StatusCodeID AS StatusCodeID,")
                .Append("       A.StatusCodeDescrip AS StatusCodeDescrip ")
                .Append("FROM   syStatusCodes A ")
                .Append("WHERE  A.sysStatusId = 9 ")
            End With

            db.OpenConnection()


            'Execute the query
            Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)

            While dr.Read()
                sStatusCodeId = dr("StatusCodeID").ToString
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return status code Id
        Return sStatusCodeId

    End Function
    Public Function ChangeStatusToCurrAttending(StuEnrollId As String, NewStatus As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        'Set the connection string
        db.ConnectionString = conString

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()
        Try
            With sb
                .Append("UPDATE arStuEnrollments Set StatusCodeId = ?, ModUser = ?, ModDate = GETDATE() ")
                .Append("WHERE StuEnrollId = ?")
            End With
            db.AddParameter("@grade", NewStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModUser", currentUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@stdenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            '   commit transaction 
            groupTrans.Commit()

            '   return without errors
            Return String.Empty
            'Catch ex As System.Exception
            '    'Return an Error To Client
            '    Return -1
        Catch ex As OleDbException
            groupTrans.Rollback()
            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function DoesPrgVersionTrackAttendance(strGuid As String, IsPrgVersion As Boolean) As Boolean
        Dim result As Boolean
        Dim db As New DataAccess

        db.ConnectionString = conString
        Dim sb As New StringBuilder
        If IsPrgVersion Then
            '   strGuid is PrgVersionId
            '   build the sql query
            With sb
                .Append("SELECT")
                .Append("       TracksAttendance=(CASE WHEN (UnitTypeId='" & AdvantageCommonValues.PresentAbsentGuid & "' OR UnitTypeId='" & AdvantageCommonValues.MinutesGuid & "' OR UnitTypeId= '" & AdvantageCommonValues.ClockHoursGuid & "' ) THEN 1 ELSE 0 END) ")
                .Append("FROM   arPrgVersions ")
                .Append("WHERE  PrgVerId=?")
            End With

        Else
            '   strGuid is StuEnrollid
            '   build the sql query
            With sb
                .Append("SELECT")
                .Append("       TracksAttendance=(CASE WHEN (P.UnitTypeId='" & AdvantageCommonValues.PresentAbsentGuid & "' OR P.UnitTypeId='" & AdvantageCommonValues.MinutesGuid & "' OR P.UnitTypeId= '" & AdvantageCommonValues.ClockHoursGuid & "' ) THEN 1 ELSE 0 END) ")
                .Append("FROM   arStuEnrollments E,arPrgVersions P ")
                .Append("WHERE  E.StuEnrollId=? AND E.PrgVerId=P.PrgVerId ")
            End With
        End If

        db.AddParameter("@Guid", strGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.OpenConnection()
        Try
            'Execute the query
            result = db.RunParamSQLScalar(sb.ToString)

            'Return boolean
            Return result
        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function GetTardiesMakingAbsenceForPrgVersion(strGuid As String, IsPrgVersion As Boolean) As Integer
        Dim result As Integer

        'connect to the database
        Dim db As New DataAccess
        db.ConnectionString = conString
        Dim sb As New StringBuilder

        If IsPrgVersion Then
            '   strGuid is PrgVersionId
            '   build the sql query
            With sb
                .Append("SELECT TardiesMakingAbsence ")
                .Append("FROM   arPrgVersions ")
                .Append("WHERE  PrgVerId=? AND TrackTardies=1")
            End With

        Else
            '   strGuid is StuEnrollid
            '   build the sql query
            With sb
                .Append("SELECT P.TardiesMakingAbsence ")
                .Append("FROM   arStuEnrollments E,arPrgVersions P ")
                .Append("WHERE  E.StuEnrollId=? AND E.PrgVerId=P.PrgVerId AND P.TrackTardies=1")
            End With
        End If

        db.AddParameter("@Guid", strGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.OpenConnection()
        Try
            'Execute the query
            Dim obj As Object = db.RunParamSQLScalar(sb.ToString)

            If Not (obj Is DBNull.Value) Then
                result = obj
            End If
            'Return integer
            Return result

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function GetTardiesMakingAbsenceForCourse(strGuid As String, IsCourse As Boolean) As Integer
        Dim result As Integer

        Try



            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = conString
            Dim sb As New StringBuilder

            If IsCourse Then
                '   strGuid is ReqId
                '   build the sql query
                With sb
                    .Append("SELECT TardiesMakingAbsence ")
                    .Append("FROM   arReqs ")
                    .Append("WHERE  ReqId=? AND TrackTardies=1")
                End With

            Else
                '   strGuid is ClsSectionId
                '   build the sql query
                With sb
                    .Append("SELECT R.TardiesMakingAbsence ")
                    .Append("FROM   arClassSections A,arReqs R ")
                    .Append("WHERE  A.ClsSectionId=? AND A.ReqId=R.ReqId AND R.TrackTardies=1")
                End With
            End If

            db.AddParameter("@Guid", strGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            Dim obj As Object = db.RunParamSQLScalar(sb.ToString)

            If Not (obj Is DBNull.Value) Then
                result = obj
            End If

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return integer
        Return result
    End Function
    Public Function GetTardiesMakingAbsenceForStudent(strGuid As String) As Integer
        Dim result = 0

        Try



            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = conString
            Dim sb As New StringBuilder


            With sb
                .Append(" SELECT Top 1 Req.TardiesMakingAbsence ")
                .Append(" FROM   arStuEnrollments E,arPrgVersions P, arAttUnitType t3 ,arProgVerdef PVD,arReqs req ")
                .Append(" WHERE  E.StuEnrollId= ? ")
                .Append(" AND E.PrgVerId=P.PrgVerId ")
                .Append(" AND Req.UnitTypeId=t3.UnitTypeId ")
                .Append(" and P.PrgVerId=PVD.PrgVerId ")
                .Append(" and PVD.Reqid=req.ReqId and req.TrackTardies=1 ")
            End With


            db.AddParameter("@Guid", strGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            Dim obj As Object = db.RunParamSQLScalar(sb.ToString)

            If Not (obj Is DBNull.Value) Then
                result = obj
            End If

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return integer
        Return result
    End Function


    Public Function GetPrgVersionAttendanceType(strGuid As String, IsPrgVersion As Boolean) As String
        Dim result = ""

        Try


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = conString
            Dim sb As New StringBuilder

            If IsPrgVersion Then
                '   strGuid is PrgVersionId
                '   build the sql query
                With sb
                    .Append("SELECT t3.UnitTypeDescrip ")
                    .Append("FROM   arPrgVersions P, arAttUnitType t3 ")
                    .Append("WHERE  P.PrgVerId=?")
                    .Append("       AND P.UnitTypeId=t3.UnitTypeId")
                    .Append("       AND (t3.UnitTypeDescrip LIKE '%Present%' OR t3.UnitTypeDescrip LIKE '%Minute%' or t3.UnitTypeDescrip LIKE '%Clock Hours%' )")
                End With

            Else
                '   strGuid is StuEnrollid
                '   build the sql query
                With sb
                    .Append("SELECT t3.UnitTypeDescrip ")
                    .Append("FROM   arStuEnrollments E,arPrgVersions P, arAttUnitType t3 ")
                    .Append("WHERE  E.StuEnrollId=?")
                    .Append("       AND E.PrgVerId=P.PrgVerId")
                    .Append("       AND P.UnitTypeId=t3.UnitTypeId")
                    .Append("       AND (t3.UnitTypeDescrip LIKE '%Present%' OR t3.UnitTypeDescrip LIKE '%Minute%'  OR t3.UnitTypeDescrip LIKE '%Clock Hours%')")
                End With
            End If

            db.AddParameter("@Guid", strGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            Dim obj As Object = db.RunParamSQLScalar(sb.ToString)

            If Not (obj Is DBNull.Value) Then
                result = obj
            End If

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return string
        Return result
    End Function

    Public Function GetStudentAttendanceType(strGuid As String) As String
        Dim result = ""

        Try


            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = conString
            Dim sb As New StringBuilder


            '   strGuid is PrgVersionId
            '   build the sql query
            With sb
                .Append(" SELECT Top 1 t3.UnitTypeDescrip ")
                .Append(" FROM   arStuEnrollments E,arPrgVersions P, arAttUnitType t3 ,arProgVerdef PVD,arReqs req ")
                .Append(" WHERE  E.StuEnrollId= ? ")
                .Append(" AND E.PrgVerId=P.PrgVerId ")
                .Append(" AND Req.UnitTypeId=t3.UnitTypeId ")
                .Append(" and P.PrgVerId=PVD.PrgVerId ")
                .Append(" and PVD.Reqid=req.ReqId ")

            End With


            db.AddParameter("@Guid", strGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.OpenConnection()

            'Execute the query
            Dim obj As Object = db.RunParamSQLScalar(sb.ToString)

            If Not (obj Is DBNull.Value) Then
                result = obj
            End If
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return string
        Return result
    End Function

    Public Function GetPercentageAttendanceInfo(stuEnrollId As String, cutOffDate As Date, clsSectionId As String) As AttendancePercentageInfo

        Dim dt As New DataTable
        'check if attendance is tracked at program level
        Dim doesPrgVerTrackAttendance As Boolean = DoesPrgVersionTrackAttendance(stuEnrollId, False)

        'get attendance type
        Dim attendanceType As String
        Dim tardiesMakingOneAbsence As Integer
        If doesPrgVerTrackAttendance Then
            attendanceType = GetPrgVersionAttendanceType(stuEnrollId, False)
            tardiesMakingOneAbsence = GetTardiesMakingAbsenceForPrgVersion(stuEnrollId, False)
        Else
            attendanceType = GetClassSectionAttendanceType(clsSectionId)
            tardiesMakingOneAbsence = GetTardiesMakingAbsenceForCourse(clsSectionId, False)
        End If


        Dim totalPresent = 0
        Dim totalAbsent = 0
        Dim totalTardies = 0
        Dim totalExcused = 0
        Dim totalScheduled As Decimal = 0.0
        'Dim totalAttended As Decimal = 0.0
        Dim totalmakeUp = 0
        For i = 0 To dt.Rows.Count - 1
            Dim row As DataRow = dt.Rows(i)
            Dim actual = CType(row("Actual"), Integer)

            'set tardy value
            Dim tardy = CType(row("Tardy"), Boolean)

            Dim excused = CType(row("Excused"), Boolean)
            If excused Then
                totalExcused += 1
            Else
                Select Case attendanceType
                    Case "Present Absent"
                        totalScheduled += 1
                        Select Case tardy
                            Case True
                                totalTardies += 1
                                totalPresent += 1
                            Case False
                                If actual = 0 Then
                                    totalPresent += 1
                                Else
                                    totalAbsent += 1
                                End If
                        End Select
                    Case "Minutes"
                        'calculate duration of the meeting
                        Dim meetingDuration As Integer = CType(row("EndTime"), DateTime).Subtract(CType(row("StartTime"), DateTime)).TotalMinutes
                        totalScheduled += meetingDuration
                        totalPresent += meetingDuration
                        If actual < meetingDuration Then
                            totalTardies += 1
                        End If
                    Case "None"
                End Select
            End If
        Next

        'return attendance display object
        Dim totalPresentAdjusted = totalPresent
        If totalTardies <> 0 And tardiesMakingOneAbsence <> 0 Then
            totalPresentAdjusted = totalPresent - (totalTardies / tardiesMakingOneAbsence)
        End If
        Dim totalAbsenceAdjusted = totalAbsent
        Dim totalTardiesAdjusted = 0
        If totalTardies <> 0 And tardiesMakingOneAbsence <> 0 Then
            totalTardiesAdjusted = totalTardies Mod tardiesMakingOneAbsence
        End If
        Dim totalExcusedAdjusted = totalExcused

        Return New AttendancePercentageInfo(totalPresent, totalAbsent, totalTardies, totalExcused, totalPresentAdjusted, totalAbsenceAdjusted, totalTardiesAdjusted, totalExcusedAdjusted, tardiesMakingOneAbsence, attendanceType, totalScheduled, totalmakeUp)
    End Function

    Public Function GetAttendancePercentageDT(StuEnrollId As String, cutOffDateObj As Object, clsSectionId As String) As DataTable

        Dim cutOffDate As Date = Date.Now

        If Not cutOffDateObj Is Nothing Then
            cutOffDate = CType(cutOffDateObj, Date)
        End If

        Dim db As New DataAccess


        db.ConnectionString = conString

        Dim sb As New StringBuilder
        With sb
            .Append("SET DATEFIRST 7; ")
            .Append("SELECT distinct ")
            .Append("       CSA.ClsSectionId,")
            .Append("       TI.TimeIntervalDescrip as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.EndIntervalId) as EndTime, ")
            .Append("		CSA.MeetDate, ")
            .Append("       CSA.Actual, ")
            .Append("		CSA.Tardy, ")
            .Append("       CSA.Excused, ")

            .Append("       (SELECT t3.UnitTypeDescrip FROM arClassSections t1, arReqs t2, arAttUnitType t3 WHERE t1.ClsSectionId = CSA.ClsSectionId AND t1.ReqId = t2.ReqId AND t2.UnitTypeId = t3.UnitTypeId) As AttendanceType, ")
            .Append(" datediff(MI, TI.TimeIntervalDescrip, (Select TimeIntervalDescrip from cmTimeInterval where ")
            .Append(" TimeIntervalId=CSM.EndIntervalId)) as duration,CSM.InstructionTypeID,CSA.Scheduled ")
            .Append(" FROM ")
            .Append("		atClsSectAttendance CSA, plWorkDays WD, arClsSectMeetings CSM, cmTimeInterval TI ")
            .Append("WHERE ")
            .Append("       CSA.ClsSectionId = CSM.ClsSectionId AND CSA.ClsSectMeetingId=CSM.ClsSectMeetingId  ")
            '.Append("AND	CSM.WorkDaysId=(Select WorkDaysId from plWorkDays where ViewOrder=DATEPART(dw, MeetDate) -1) ")
            .Append("AND	WD.ViewOrder=DATEPART(dw,MeetDate)-1  AND CSA.Actual<> 999 and CSA.Actual<> 9999 ")
            .Append("AND	CSM.TimeIntervalId=TI.timeIntervalId ")
            .Append("AND	StuEnrollId=? ")
            .Append("AND    CSA.MeetDate <= ? ")
            If Not clsSectionId Is Nothing Then
                .Append("AND    CSA.ClsSectionId = ? ")
            End If
            .Append("ORDER BY ")
            .Append("       CSA.ClsSectionId,CSA.MeetDate ")
        End With

        'add student enrollment parameter
        db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'add cutOff date parameter
        db.AddParameter("CutOffDate", cutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        If Not clsSectionId Is Nothing Then
            'add student enrollment parameter
            db.AddParameter("StuEnrollId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        'return datatable
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function
    Public Function GetAttendancePercentageDT_SP(StuEnrollId As String, cutOffDate As Date, clsSectionId As String) As DataTable

        'Dim ds As DataSet
        Dim db As New SQLDataAccess


        db.ConnectionString = conString

        'add studen enrollment parameter
        db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        'add cutOff date parameter
        db.AddParameter("@cutOffDate", cutOffDate, SqlDbType.DateTime, , ParameterDirection.Input)
        ''Added

        If Not clsSectionId Is Nothing Then

            db.AddParameter("@clsSectionId", New Guid(clsSectionId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Else
            db.AddParameter("@clsSectionId", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        End If

        ''
        'return datatable
        Try
            Return db.RunParamSQLDataSet_SP("dbo.usp_GetAttendancePercentageDT").Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try




    End Function
    Public Function GetAttendancePercentageDT(StuEnrollId As String, clsSectionId As String) As DataTable

        Dim db As New DataAccess


        db.ConnectionString = conString

        Dim sb As New StringBuilder
        With sb
            .Append("SET DATEFIRST 7; ")
            .Append("SELECT distinct ")
            .Append("       CSA.ClsSectionId,")
            .Append("       TI.TimeIntervalDescrip as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.EndIntervalId) as EndTime, ")
            .Append("		CSA.MeetDate, ")
            .Append("       CSA.Actual, ")
            .Append("		CSA.Tardy, ")
            .Append("       CSA.Excused, ")

            .Append("       (SELECT t3.UnitTypeDescrip FROM arClassSections t1, arReqs t2, arAttUnitType t3 WHERE t1.ClsSectionId = CSA.ClsSectionId AND t1.ReqId = t2.ReqId AND t2.UnitTypeId = t3.UnitTypeId) As AttendanceType,CSM.InstructionTypeID,CSA.Scheduled  ")

            .Append("FROM ")
            .Append("		atClsSectAttendance CSA, plWorkDays WD, arClsSectMeetings CSM, cmTimeInterval TI ,arResults R ")
            .Append("WHERE ")
            .Append("       CSA.ClsSectionId = CSM.ClsSectionId AND CSA.ClsSectMeetingId=CSM.ClsSectMeetingId ")
            '.Append("AND	CSM.WorkDaysId=(Select WorkDaysId from plWorkDays where ViewOrder=DATEPART(dw, MeetDate) -1) ")
            .Append("AND	WD.ViewOrder=DATEPART(dw,MeetDate)-1 AND CSA.Actual<> 999 and CSA.Actual <> 9999 ")
            .Append("AND	CSM.TimeIntervalId=TI.timeIntervalId ")
            .Append("AND	CSA.StuEnrollId=? ")
            .Append(" and CSA.ClsSectionId=R.TestId and CSA.StuEnrollId=R.StuEnrollId and   (R.GrdSysDetailId not in (select GrdSysDetailId from arGradeSystemDetails where IsDrop=1)")
            .Append(" or R.GrdSysDetailId is null) ")
            If Not clsSectionId Is Nothing Then
                .Append(" AND    CSA.ClsSectionId = ? ")
            End If
            .Append("ORDER BY ")
            .Append("       CSA.ClsSectionId,CSA.MeetDate ")
        End With

        'add studen enrollment parameter
        db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        If Not clsSectionId Is Nothing Then
            'add student enrollment parameter
            db.AddParameter("StuEnrollId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        'return datatable
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function
    Public Function GetAttendanceMinutes(StuEnrollId As String, cutOffDateObj As Object, Optional clsSectionId As String = "", Optional ByVal EndDateobj As Object = Nothing) As DataTable
        'Added date range filter for US3768


        Dim dStartDate As Date
        Dim dEndDate As Date
        Dim bDateRange = False

        Dim cutOffDate As Date = Date.Now

        If Not cutOffDateObj Is Nothing Then
            cutOffDate = CType(cutOffDateObj, Date)
        End If

        If Not EndDateobj Is Nothing Then
            dStartDate = CType(EndDateobj, Date)
            dEndDate = cutOffDate
            bDateRange = True
        End If

        Dim db As New DataAccess


        db.ConnectionString = conString

        Dim sb As New StringBuilder
        With sb
            .Append(" SELECT distinct       CSA.ClsSectionId,  ")
            .Append("CSA.MeetDate,        CSA.Actual, ")
            .Append("CSA.Tardy,        CSA.Excused, ")
            .Append("(SELECT t3.UnitTypeDescrip FROM arClassSections t1, arReqs t2, arAttUnitType t3 WHERE ")
            .Append("t1.ClsSectionId = CSA.ClsSectionId AND t1.ReqId = t2.ReqId AND t2.UnitTypeId = t3.UnitTypeId) As AttendanceType,")
            .Append(" WorkDaysDescrip, CS.TermID,CSA.ClsSectMeetingId,CSA.Scheduled,CSM.InstructionTypeID    FROM ")
            .Append(" arClassSections CS,atClsSectAttendance CSA,arClsSectMeetings CSM,plWorkDays WD ")
            .Append(" WHERE 		CS.ClsSectionId=CSA.ClsSectionId and   CSA.ClsSectionId = CSM.ClsSectionId AND CSA.ClsSectMeetingId=CSM.ClsSectMeetingId ")
            .Append("          AND	WD.ViewOrder=DATEPART(dw,MeetDate)-1 ")
            .Append(" AND	StuEnrollId= ? ")
            If clsSectionId <> "" Then
                .Append(" AND	CS.ClsSectionId = ? ")
            End If
            If bDateRange Then
                .Append(" AND    CSA.MeetDate >= ?  AND    CSA.MeetDate <= ? ")
            Else
                .Append(" AND    CSA.MeetDate <= ? ")
            End If
            .Append(" AND CSA.Actual<> 999 and CSA.Actual<> 9999 ")
            .Append("  ORDER by  CSA.ClsSectionId, CSA.MeetDate ")
        End With

        'add studen enrollment parameter
        db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        If clsSectionId <> "" Then
            db.AddParameter("clsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If bDateRange Then
            'add Start date parameter
            db.AddParameter("dStartDate", dStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'add End date parameter
            db.AddParameter("dEndDate", dEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        Else
            'add cutOff date parameter
            db.AddParameter("CutOffDate", cutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        'return datatable
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function
    Public Function GetAttendanceMinutes_SP(StuEnrollId As String, cutOffDate As Date) As DataTable

        'Dim ds As DataSet
        Dim db As New SQLDataAccess


        db.ConnectionString = conString

        'add studen enrollment parameter
        db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        'add cutOff date parameter
        db.AddParameter("@cutOffDate", cutOffDate, SqlDbType.DateTime, , ParameterDirection.Input)
        ''Added


        ''
        'return datatable
        Try
            Return db.RunParamSQLDataSet_SP("dbo.usp_GetAttendanceMinutes").Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try


    End Function
    Public Function GetImportedAttendance_SP(StuEnrollId As String) As DataTable
        'Dim ds As DataSet
        Dim db As New SQLDataAccess

        db.ConnectionString = conString

        'add studen enrollment parameter
        db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        'return datatable
        Try
            Return db.RunParamSQLDataSet_SP("dbo.usp_GetImportedAttendance").Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

    End Function

    Public Function GetImportedAttendanceBetweenDateRange_SP(StuEnrollId As String, cutOffDate As Date, meetStartDate As Date) As DataTable

        'Dim ds As DataSet
        Dim db As New SQLDataAccess


        db.ConnectionString = conString

        'add studen enrollment parameter
        db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        'add cutOff date parameter
        db.AddParameter("@cutOffDate", cutOffDate, SqlDbType.DateTime, , ParameterDirection.Input)

        'add cutOff date parameter
        db.AddParameter("@meetStartDate", meetStartDate, SqlDbType.DateTime, , ParameterDirection.Input)

        'return datatable
        Try
            Return db.RunParamSQLDataSet_SP("dbo.usp_GetImportedAttendanceBetweenDateRange").Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try


    End Function

    Public Function GetAttendanceMinutes(StuEnrollId As String, clsSectionId As String) As DataTable

        Dim cutOffDate As Date = Date.Now

        Dim db As New DataAccess


        db.ConnectionString = conString

        Dim sb As New StringBuilder
        With sb
            .Append(" SET DATEFIRST 7; ")
            .Append(" SELECT distinct       CSA.ClsSectionId,     (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.TimeIntervalId) as StartTime, ")
            .Append("(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.EndIntervalId) as EndTime, ")
            .Append("CSA.MeetDate,        CSA.Actual, ")
            .Append("CSA.Tardy,        CSA.Excused, ")
            .Append("(SELECT t3.UnitTypeDescrip FROM arClassSections t1, arReqs t2, arAttUnitType t3 WHERE ")
            .Append("t1.ClsSectionId = CSA.ClsSectionId AND t1.ReqId = t2.ReqId AND t2.UnitTypeId = t3.UnitTypeId) As AttendanceType,")
            .Append(" WorkDaysDescrip, CS.TermID,CSA.ClsSectMeetingId,CSA.Scheduled,CSM.InstructionTypeID       FROM ")
            .Append(" arClassSections CS,atClsSectAttendance CSA,arClsSectMeetings CSM,plWorkDays WD,cmTimeInterval TI ")
            .Append(" WHERE 		CS.ClsSectionId=CSA.ClsSectionId and   CSA.ClsSectionId = CSM.ClsSectionId AND CSA.ClsSectMeetingId=CSM.ClsSectMeetingId ")
            .Append("          AND	WD.ViewOrder=DATEPART(dw,MeetDate)-1    AND CSA.Actual<> 999 and CSA.Actual<> 9999 ")
            .Append(" AND	StuEnrollId= ?  AND    CSA.MeetDate <= ? AND    CSA.ClsSectionId =  ? ")
            .Append("  ORDER by  CSA.ClsSectionId, CSA.MeetDate ")
        End With

        'add studen enrollment parameter
        db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.AddParameter("CutOffDate", cutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        'add cutOff date parameter

        db.AddParameter("clsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'return datatable
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function
    Public Function GetAttendanceMinutes(StuEnrollId As String) As DataTable

        Dim db As New DataAccess


        db.ConnectionString = conString

        Dim sb As New StringBuilder
        With sb
            .Append(" SET DATEFIRST 7; ")
            .Append(" SELECT distinct       CSA.ClsSectionId,     (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.TimeIntervalId) as StartTime, ")
            .Append("(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.EndIntervalId) as EndTime, ")
            .Append("CSA.MeetDate,        CSA.Actual, ")
            .Append("CSA.Tardy,        CSA.Excused, ")
            .Append("(SELECT t3.UnitTypeDescrip FROM arClassSections t1, arReqs t2, arAttUnitType t3 WHERE ")
            .Append("t1.ClsSectionId = CSA.ClsSectionId AND t1.ReqId = t2.ReqId AND t2.UnitTypeId = t3.UnitTypeId) As AttendanceType,")
            .Append(" WorkDaysDescrip, CS.TermID,CSM.InstructionTypeID,CSA.ClsSectMeetingId,CSA.Scheduled   FROM ")
            .Append(" arClassSections CS,atClsSectAttendance CSA,arClsSectMeetings CSM,plWorkDays WD,cmTimeInterval TI,arResults R ")
            .Append(" WHERE 		CS.ClsSectionId=CSA.ClsSectionId and   CSA.ClsSectionId = CSM.ClsSectionId AND CSA.ClsSectMeetingId=CSM.ClsSectMeetingId ")
            .Append("          AND	WD.ViewOrder=DATEPART(dw,MeetDate)-1    AND CSA.Actual<> 999 and CSA.Actual<> 9999 ")
            .Append(" AND	CSA.StuEnrollId= ? ")
            .Append(" and CS.ClsSectionId=R.TestId and CSA.StuEnrollId=R.StuEnrollId ")
            '        and   (R.GrdSysDetailId not in (select GrdSysDetailId from arGradeSystemDetails where IsDrop=1)")
            '.Append(" or R.GrdSysDetailId is null) ")
            .Append("  ORDER by  CSA.ClsSectionId, CSA.MeetDate ")
        End With

        'add studen enrollment parameter
        db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'add cutOff date parameter


        'return datatable
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function

    Public Function GetAttendancePercentageDT(StuEnrollId As String, cutOffDateObj As Object) As DataTable

        Dim cutOffDate As Date = Date.Now

        If Not cutOffDateObj Is Nothing Then
            cutOffDate = CType(cutOffDateObj, Date)
        End If

        Dim db As New DataAccess



        db.ConnectionString = conString

        Dim sb As New StringBuilder
        With sb
            .Append("SET DATEFIRST 7; ")
            .Append(" SELECT  	distinct	CSA.ClsSectionId,       TI1.TimeIntervalDescrip as StartTime,        (Select TimeIntervalDescrip from ")
            .Append(" cmTimeInterval where TimeIntervalId=CSM.EndIntervalId) as EndTime, 		CSA.MeetDate,        CSA.Actual, 		CSA.Tardy, ")
            .Append(" CSA.Excused,        (SELECT t3.UnitTypeDescrip FROM arClassSections t1, arReqs t2, arAttUnitType t3 WHERE ")
            .Append(" t1.ClsSectionId = CSA.ClsSectionId AND t1.ReqId = t2.ReqId AND t2.UnitTypeId = t3.UnitTypeId) As AttendanceType,CSM.InstructionTypeID,CSA.Scheduled   FROM	atClsSectAttendance CSA, ")
            .Append(" arClsSectMeetings CSM, syPeriods P, syPeriodsWorkDays PWD, plWorkdays WD, cmTimeInterval TI1, cmTimeInterval TI2 ")
            .Append(" WHERE CSM.PeriodId = P.PeriodId And P.PeriodId = PWD.PeriodId And PWD.WorkDayId = WD.WorkDaysId And P.StartTimeId = TI1.TimeIntervalId ")
            .Append(" AND	P.EndTimeId = TI2.TimeIntervalId  AND	CSA.ClsSectionId = CSM.ClsSectionId AND CSA.ClsSectMeetingId=CSM.ClsSectMeetingId     AND CSA.Actual<> 999 and CSA.Actual<> 9999 ")
            .Append(" AND	StuEnrollId= ? ")
            .Append(" AND    CSA.MeetDate <= ? ")
            .Append(" and (select datepart(hh,TimeIntervalDescrip) from syPeriods,cmTimeInterval where ")
            .Append(" syPeriods.startTimeId = cmTimeInterval.TimeIntervalId and Periodid=CSM.Periodid)=datepart(hh,MeetDate) ")
            .Append(" and (select datepart(Mi,TimeIntervalDescrip) from syPeriods,cmTimeInterval where ")
            .Append(" syPeriods.startTimeId = cmTimeInterval.TimeIntervalId and Periodid=CSM.Periodid)=datepart(mi,meetdate) ")
            '.Append(" UNION ALL ")
            '.Append(" select distinct CSA.ClsSectionId,       TI1.TimeIntervalDescrip as StartTime,        (Select TimeIntervalDescrip from ")
            '.Append(" cmTimeInterval where TimeIntervalId=CSM.EndIntervalId) as EndTime, 		CSA.MeetDate,        CSA.Actual, 		CSA.Tardy, ")
            '.Append(" CSA.Excused,        (SELECT t3.UnitTypeDescrip FROM arClassSections t1, arReqs t2, arAttUnitType t3 WHERE ")
            '.Append(" t1.ClsSectionId = CSA.ClsSectionId AND t1.ReqId = t2.ReqId AND t2.UnitTypeId = t3.UnitTypeId) As AttendanceType  FROM	atClsSectAttendance CSA, ")
            '.Append(" arClsSectMeetings CSM, syPeriods P, syPeriodsWorkDays PWD, plWorkdays WD, cmTimeInterval TI1, ")
            '.Append(" cmTimeInterval TI2 WHERE ")
            '.Append(" CSA.ClsSectionId = CSM.ClsSectionId and ")
            '.Append(" CSM.AltPeriodId=P.PeriodId AND	P.PeriodId=PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P.StartTimeId = TI1.TimeIntervalId  AND	 ")
            '.Append(" P.EndTimeId = TI2.TimeIntervalId And CSA.ClsSectionId = CSM.ClsSectionId ")
            '.Append(" AND	StuEnrollId= ? ")
            '.Append(" AND    CSA.MeetDate <= ? ")
            .Append(" ORDER BY  CSA.ClsSectionId, CSA.MeetDate ")
        End With

        'add studen enrollment parameter
        db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'add cutOff date parameter
        db.AddParameter("CutOffDate", cutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        'add studen enrollment parameter
        'db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''add cutOff date parameter
        'db.AddParameter("CutOffDate", cutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)



        'return datatable
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function
    Public Function GetAttendancePercentageDT_SP(StuEnrollId As String, cutOffDateObj As Object) As DataTable
        'Dim ds As DataSet
        Dim db As New SQLDataAccess


        db.ConnectionString = conString

        'add studen enrollment parameter
        db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        'add cutOff date parameter
        db.AddParameter("@cutOffDate", cutOffDateObj, SqlDbType.DateTime, , ParameterDirection.Input)
        ''Added

        ''
        'return datatable
        Try
            Return db.RunParamSQLDataSet_SP("dbo.usp_GetAttendancePercentageDT_TR").Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

    End Function
    Public Function GetAttendancePercentageDTForReport(StuEnrollId As String, cutOffDateObj As Object, Optional clsSectionid As String = "", Optional ByVal meetstartdate As Object = Nothing) As DataTable
        'Added date range filter for US3768
        Dim dStartDate As Date
        Dim dBeginDate As Date
        Dim bDateRange = False

        Dim cutOffDate As Date = Date.Now

        If Not cutOffDateObj Is Nothing Then
            cutOffDate = CType(cutOffDateObj, Date)
        End If

        If Not meetstartdate Is Nothing Then
            dBeginDate = CType(meetstartdate, Date)
            dStartDate = cutOffDate
            bDateRange = True
        End If
        Dim db As New DataAccess


        db.ConnectionString = conString

        Dim sb As New StringBuilder
        With sb
            ''Query modidifed by Saraswathi on Nov 12 2008
            ''When the student has failed, the original Scheduled and Completed hrs are calculated
            ''the new Scheduled and Completed hrs are not taken into account.
            ''fixed based on mantis issue 14859
            .Append(" SET DATEFIRST 7;  ")
            .Append(" SELECT    distinct CSA.ClsSectionId,null as startTime,Null as endtime, ")
            .Append(" CSA.MeetDate,        CSA.Actual, 		CSA.Tardy,  CSA.Excused,        (SELECT t3.UnitTypeDescrip FROM ")
            .Append(" arClassSections t1, arReqs t2, arAttUnitType t3 WHERE  t1.ClsSectionId = CSA.ClsSectionId AND t1.ReqId = t2.ReqId AND ")
            .Append(" t2.UnitTypeId = t3.UnitTypeId) As AttendanceType , ")
            .Append(" datediff(MI,(select T1.TimeIntervalDescrip  from cmTimeInterval T1, cmTimeinterval T2 where T1.TimeIntervalId=P.StartTimeId ")
            .Append(" and T2.TimeIntervalId=P.EndTimeId and ")
            .Append(" left(datename(dw,CSA.meetdate),3)=WD.WorkDaysDescrip and CSA.ClsSectionId = CSM.ClsSectionId ),")
            .Append(" (select T2.TimeIntervalDescrip from cmTimeInterval T1,cmTimeinterval T2 where T1.TimeIntervalId=P.StartTimeId  and T2.TimeIntervalId=P.EndTimeId ")
            .Append(" and left(datename(dw,csa.meetdate),3)=WD.WorkDaysDescrip  and CSA.ClsSectionId = CSM.ClsSectionId ")
            .Append(" ))  as duration,CSM.InstructionTypeID,CSA.Scheduled  ")
            .Append("  FROM	atClsSectAttendance CSA,  arClsSectMeetings CSM, syPeriods P ")
            ''Added
            .Append(",arClassSections a ")
            ''
            .Append(" , syPeriodsWorkDays PWD, plWorkdays WD ")
            .Append(" WHERE CSM.PeriodId = P.PeriodId AND CSA.ClsSectMeetingId=CSM.ClsSectMeetingId  and")
            ''Added
            .Append("  a.ClsSectionid=CSA.ClsSectionId and")
            ''
            If clsSectionid <> "" Then
                .Append(" a.ClsSectionid= ? and ")
            End If
            .Append(" P.PeriodId = PWD.PeriodId and CSA.Actual<> 999 and CSA.Actual<> 9999 and ")
            .Append(" PWD.WorkDayId = WD.WorkDaysId And ")
            .Append(" CSA.ClsSectionId = CSM.ClsSectionId  AND	StuEnrollId=  ?   ")

            If bDateRange Then
                .Append(" AND    CSA.MeetDate <= ?  And  CSA.MeetDate >= ? ")
            Else
                .Append(" AND    CSA.MeetDate <= ?  ")
            End If

            .Append(" and datediff(MI,(select T1.TimeIntervalDescrip  from cmTimeInterval T1, cmTimeinterval T2 where ")
            .Append(" T1.TimeIntervalId=P.StartTimeId  and T2.TimeIntervalId=P.EndTimeId and ")
            .Append(" left(datename(dw,CSA.meetdate),3)=WD.WorkDaysDescrip and CSA.ClsSectionId = CSM.ClsSectionId ), ")
            .Append(" (select T2.TimeIntervalDescrip from cmTimeInterval T1,cmTimeinterval T2 where T1.TimeIntervalId=P.StartTimeId  and T2.TimeIntervalId=P.EndTimeId ")
            .Append(" and left(datename(dw,csa.meetdate),3)=WD.WorkDaysDescrip  and CSA.ClsSectionId = CSM.ClsSectionId ")
            .Append(" )) is not null ")
            .Append(" and CONVERT(VARCHAR(10),CSA.meetdate,111) >= CONVERT(VARCHAR(10),CSM.StartDate,111) and ")
            .Append(" CONVERT(VARCHAR(10),CSA.meetdate,111) <= CONVERT(VARCHAR(10),CSM.EndDate,111) ")
            .Append(" and a.StartDate=(Select Min(StartDate) from arClassSections b,atClsSectAttendance bt where ")
            .Append(" ReqId= a.reqId ")
            .Append(" and StuEnrollid= ? ")
            '.Append(" and b.clsSectionId=bt.ClsSectionId)")
            'DE9156
            If bDateRange Then
                .Append(" and b.clsSectionId=bt.ClsSectionId and b.ClsSectionId=a.ClsSectionId)")
            Else
                .Append(" and b.clsSectionId=bt.ClsSectionId)")
            End If
            .Append(" and (select datepart(hh,TimeIntervalDescrip) from syPeriods,cmTimeInterval where ")
            .Append(" syPeriods.startTimeId = cmTimeInterval.TimeIntervalId and Periodid=CSM.Periodid)=datepart(hh,MeetDate) ")
            .Append(" and (select datepart(Mi,TimeIntervalDescrip) from syPeriods,cmTimeInterval where ")
            .Append(" syPeriods.startTimeId = cmTimeInterval.TimeIntervalId and Periodid=CSM.Periodid)=datepart(mi,meetdate) ")
            .Append(" ORDER BY  CSA.ClsSectionId,CSA.MeetDate ")
        End With

        'add studen enrollment parameter
        If clsSectionid <> "" Then
            db.AddParameter("clsSectionid", clsSectionid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        End If
        db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        If bDateRange Then
            'add Start date parameter
            db.AddParameter("dStartDate", dStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'add End date parameter
            db.AddParameter("dEndDate", dBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        Else
            'add cutOff date parameter
            db.AddParameter("CutOffDate", cutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        ''Added
        db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ''
        'return datatable
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function
    Public Function GetAttendancePercentageDTForReport_SP(StuEnrollId As String, cutOffDate As Date) As DataTable

        'Dim ds As DataSet
        Dim db As New SQLDataAccess

        db.ConnectionString = conString

        'add studen enrollment parameter
        db.AddParameter("@stuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        'add cutOff date parameter
        db.AddParameter("@cutOffDate", cutOffDate, SqlDbType.DateTime, , ParameterDirection.Input)
        ''Added

        ''
        'return datatable
        Try
            Return db.RunParamSQLDataSet_SP("dbo.usp_GetAttendancePercentageDTForReport").Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try


    End Function
    Public Function GetAttendancePercentageDT(StuEnrollId As String) As DataTable



        Dim db As New DataAccess


        db.ConnectionString = conString

        Dim sb As New StringBuilder
        With sb
            .Append("SET DATEFIRST 7; ")
            .Append(" SELECT  	distinct	CSA.ClsSectionId,       TI1.TimeIntervalDescrip as StartTime,        (Select TimeIntervalDescrip from ")
            .Append(" cmTimeInterval where TimeIntervalId=CSM.EndIntervalId) as EndTime, 		CSA.MeetDate,        CSA.Actual, 		CSA.Tardy, ")
            .Append(" CSA.Excused,        (SELECT t3.UnitTypeDescrip FROM arClassSections t1, arReqs t2, arAttUnitType t3 WHERE ")
            .Append(" t1.ClsSectionId = CSA.ClsSectionId AND t1.ReqId = t2.ReqId AND t2.UnitTypeId = t3.UnitTypeId) As AttendanceType,CSM.InstructionTypeID,   CSA.Scheduled FROM	atClsSectAttendance CSA, ")
            .Append(" arClsSectMeetings CSM, syPeriods P, syPeriodsWorkDays PWD, plWorkdays WD, cmTimeInterval TI1, cmTimeInterval TI2,arResults R ")
            .Append(" WHERE CSM.PeriodId = P.PeriodId And P.PeriodId = PWD.PeriodId And PWD.WorkDayId = WD.WorkDaysId And P.StartTimeId = TI1.TimeIntervalId ")
            .Append(" AND	P.EndTimeId = TI2.TimeIntervalId  AND	CSA.ClsSectionId = CSM.ClsSectionId AND CSA.ClsSectMeetingId=CSM.ClsSectMeetingId and   CSA.Actual<> 999 and CSA.Actual<> 9999 ")
            .Append(" and convert(char, TI1.TimeIntervalDescrip, 114)=convert(char, CSA.MeetDate, 114) ")
            .Append(" AND	CSA.StuEnrollId= ? ")
            .Append(" and CSA.ClsSectionId=R.TestId and CSA.StuEnrollId=R.StuEnrollId ")
            '        and   (R.GrdSysDetailId not in (select GrdSysDetailId from arGradeSystemDetails where IsDrop=1)")
            '.Append(" or R.GrdSysDetailId is null) ")
            .Append(" UNION  ")

            .Append("SELECT distinct ")
            .Append("       CSA.ClsSectionId,")
            .Append("       TI.TimeIntervalDescrip as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.EndIntervalId) as EndTime, ")
            .Append("		CSA.MeetDate, ")
            .Append("       CSA.Actual, ")
            .Append("		CSA.Tardy, ")
            .Append("       CSA.Excused, ")

            .Append("       (SELECT t3.UnitTypeDescrip FROM arClassSections t1, arReqs t2, arAttUnitType t3 WHERE t1.ClsSectionId = CSA.ClsSectionId AND t1.ReqId = t2.ReqId AND t2.UnitTypeId = t3.UnitTypeId) As AttendanceType,CSM.InstructionTypeID,CSA.Scheduled  ")

            .Append("FROM ")
            .Append("		atClsSectAttendance CSA, plWorkDays WD, arClsSectMeetings CSM, cmTimeInterval TI ,arResults R ")
            .Append("WHERE ")
            .Append("       CSA.ClsSectionId = CSM.ClsSectionId AND CSA.ClsSectMeetingId=CSM.ClsSectMeetingId ")
            '.Append("AND	CSM.WorkDaysId=(Select WorkDaysId from plWorkDays where ViewOrder=DATEPART(dw, MeetDate) -1) ")
            .Append("AND	WD.ViewOrder=DATEPART(dw,MeetDate)-1 AND CSA.Actual<> 999 and CSA.Actual <> 9999 ")
            .Append("AND	CSM.TimeIntervalId=TI.timeIntervalId ")
            .Append("AND	CSA.StuEnrollId=? and   CSA.Actual<> 999 and CSA.Actual<> 9999 ")
            .Append(" and CSA.ClsSectionId=R.TestId and CSA.StuEnrollId=R.StuEnrollId ")
            '        and   (R.GrdSysDetailId not in (select GrdSysDetailId from arGradeSystemDetails where IsDrop=1)")
            '.Append(" or R.GrdSysDetailId is null) ")
            .Append(" ORDER BY  CSA.ClsSectionId, CSA.MeetDate ")
        End With

        'add studen enrollment parameter
        db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'add cutOff date parameter


        'add studen enrollment parameter
        db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''add cutOff date parameter
        'db.AddParameter("CutOffDate", cutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)



        'return datatable
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function


    'Public Function ChangeStatusToFutureStart(ByVal StuEnrollId As String, ByVal user As String, Optional ByVal futureStartStatusId As String = "") As String
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim futureStartStatus As String


    '    '   Set the connection string
    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

    '    If futureStartStatusId = "" Then
    '        futureStartStatus = GetFutureStartStatus()
    '    Else
    '        futureStartStatus = futureStartStatusId
    '    End If

    '    '   We must encapsulate all DB updates in one transaction
    '    Dim groupTrans As OleDbTransaction = db.StartTransaction()

    '    Try
    '        '   Update the arStuEnrollments table
    '        With sb
    '            .Append("UPDATE     arStuEnrollments ")
    '            .Append("SET        StatusCodeId=?, DateDetermined = Null, ModDate=?, ModUser=? ")
    '            .Append("WHERE      StuEnrollId=? ")
    '        End With

    '        '   StatusCodeId
    '        db.AddParameter("@StatusCodeId", futureStartStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   ModDate
    '        Dim now As DateTime = Date.Now
    '        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   StuEnrollId
    '        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
    '        '
    '        '
    '        '   Clear out parameter list
    '        db.ClearParameters()
    '        '
    '        '
    '        '   Clear out string builder
    '        sb.Remove(0, sb.Length)
    '        '
    '        '
    '        '
    '        '   Update arStudent table
    '        With sb
    '            .Append("UPDATE     arStudent ")
    '            .Append("SET        StudentStatus=?, ModDate=?, ModUser=?  ")
    '            .Append("WHERE      arStudent.StudentId = (SELECT StudentId FROM arStuEnrollments WHERE StuEnrollId=?)")
    '        End With

    '        '   StudentStatus
    '        db.AddParameter("@StudentStatus", AdvantageCommonValues.ActiveGuid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   ModDate
    '        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   StuEnrollId
    '        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

    '        '   commit transaction 
    '        groupTrans.Commit()

    '        '   return without errors
    '        Return ""

    '    Catch ex As OleDbException
    '        '   rollback transaction if there were errors
    '        groupTrans.Rollback()

    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function

    'Public Function ChangeStatusToFutureStartNew(ByVal StuEnrollId As String, ByVal user As String, Optional ByVal futureStartStatusId As String = "", Optional ByVal reEnrollmentDate As String = "") As String
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim futureStartStatus As String


    '    '   Set the connection string
    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

    '    If futureStartStatusId = "" Then
    '        futureStartStatus = GetFutureStartStatus()
    '    Else
    '        futureStartStatus = futureStartStatusId
    '    End If

    '    '   We must encapsulate all DB updates in one transaction
    '    Dim groupTrans As OleDbTransaction = db.StartTransaction()

    '    Try
    '        '   Update the arStuEnrollments table
    '        With sb
    '            .Append("UPDATE     arStuEnrollments ")
    '            .Append("SET        LDA = null,DropReasonId = null, StatusCodeId=?, DateDetermined = Null, ModDate=?, ModUser=? ,ReEnrollmentDate=? ")
    '            .Append("WHERE      StuEnrollId=? ")
    '        End With

    '        '   StatusCodeId
    '        db.AddParameter("@StatusCodeId", futureStartStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   ModDate
    '        Dim now As DateTime = Date.Now
    '        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


    '        ' ReEnrollmentDate
    '        If (reEnrollmentDate <> "") Then

    '            db.AddParameter("@EnrollDate", reEnrollmentDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@EnrollDate", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If


    '        '   StuEnrollId
    '        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
    '        '
    '        '
    '        '   Clear out parameter list
    '        db.ClearParameters()
    '        '
    '        '
    '        '   Clear out string builder
    '        sb.Remove(0, sb.Length)
    '        '
    '        '
    '        '
    '        '   Update arStudent table
    '        With sb
    '            .Append("UPDATE     arStudent ")
    '            .Append("SET        StudentStatus=?, ModDate=?, ModUser=?  ")
    '            .Append("WHERE      arStudent.StudentId = (SELECT StudentId FROM arStuEnrollments WHERE StuEnrollId=?)")
    '        End With

    '        '   StudentStatus
    '        db.AddParameter("@StudentStatus", AdvantageCommonValues.ActiveGuid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   ModDate
    '        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   StuEnrollId
    '        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

    '        '   commit transaction 
    '        groupTrans.Commit()

    '        '   return without errors
    '        Return ""

    '    Catch ex As OleDbException
    '        '   rollback transaction if there were errors
    '        groupTrans.Rollback()

    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function

    'Public Function GetFutureStartStatus() As String
    '    'Dim ds As New DataSet
    '    'Dim da As OleDbDataAdapter
    '    Dim sStatusCodeId As String

    '    Try

    '        '   connect to the database
    '        Dim db As New DataAccess
    '        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
    '        Dim sb As New StringBuilder

    '        '   build the sql query
    '        With sb
    '            .Append("SELECT DISTINCT ")
    '            .Append("       A.StatusCodeID AS StatusCodeID,")
    '            .Append("       A.StatusCodeDescrip AS StatusCodeDescrip ")
    '            .Append("FROM   syStatusCodes A ")
    '            .Append("WHERE  A.sysStatusId = 7 ")
    '        End With

    '        db.OpenConnection()

    '        'Execute the query
    '        Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)

    '        While dr.Read()
    '            sStatusCodeId = dr("StatusCodeID").ToString
    '        End While

    '        If Not dr.IsClosed Then dr.Close()
    '        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    Catch ex As Exception
    '        Throw New BaseException(ex.Message)
    '    End Try

    '    'Return status code Id
    '    Return sStatusCodeId

    'End Function

    'Public Function ChangeStatusFromGradToActive(ByVal StuEnrollId As String, _
    '                    ByVal userName As String, _
    '                    Optional ByVal currAttendingStatusId As String = "", _
    '                    Optional ByVal RevisedExpGradDate As String = "") As String
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim currAttStatus As String


    '    'Set the connection string
    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

    '    If currAttendingStatusId = "" Then
    '        currAttStatus = GetCurrAttendingStatus()
    '    Else
    '        currAttStatus = currAttendingStatusId
    '    End If

    '    '   we must encapsulate all DB updates in one transaction
    '    Dim groupTrans As OleDbTransaction = db.StartTransaction()

    '    Try

    '        With sb
    '            .Append("UPDATE     arStuEnrollments ")
    '            .Append("SET        StatusCodeId=?,")
    '            If Not (RevisedExpGradDate = "") Then
    '                .Append("           ExpGradDate=?,")
    '            End If
    '            .Append("           ModDate=?, ModUser=? ")
    '            .Append("WHERE      StuEnrollId=? ")
    '        End With

    '        '   StatusCodeId
    '        db.AddParameter("@StatusCodeId", currAttStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   ExpGradDate
    '        If Not (RevisedExpGradDate = "") Then
    '            db.AddParameter("@ExpGradDate", RevisedExpGradDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If

    '        '   ModDate
    '        Dim now As DateTime = Date.Now
    '        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   ModUser
    '        db.AddParameter("@ModUser", userName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.AddParameter("@stdenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
    '        '
    '        '   Clear out parameter list
    '        db.ClearParameters()
    '        '
    '        '
    '        '   Clear out string builder
    '        sb.Remove(0, sb.Length)
    '        '
    '        '
    '        '
    '        '   Update arStudent table
    '        With sb
    '            .Append("UPDATE     arStudent ")
    '            .Append("SET        StudentStatus=?, ModDate=?, ModUser=?  ")
    '            .Append("WHERE      arStudent.StudentId = (SELECT StudentId FROM arStuEnrollments WHERE StuEnrollId=?)")
    '        End With

    '        '   StudentStatus
    '        db.AddParameter("@StudentStatus", AdvantageCommonValues.ActiveGuid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   ModDate
    '        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '   ModUser
    '        db.AddParameter("@ModUser", userName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '   StuEnrollId
    '        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

    '        '   commit transaction 
    '        groupTrans.Commit()

    '        '   return without errors
    '        Return ""

    '    Catch ex As OleDbException
    '        '   rollback transaction if there were errors
    '        groupTrans.Rollback()

    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function

    Public Function GetInstructorName(clsSectionId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim instructor As String

        Try

            db.ConnectionString = conString

            '   build the sql query
            With sb
                .Append("SELECT U.FullName ")
                .Append("FROM   arClassSections A,syUsers U ")
                .Append("WHERE  A.InstructorId=U.UserId AND A.ClsSectionId=?")
            End With

            db.OpenConnection()
            db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            instructor = db.RunParamSQLScalar(sb.ToString)


        Catch ex As Exception

            Throw New BaseException(ex.Message)

        Finally

            'Close Connection
            db.CloseConnection()

        End Try

        'Return instructor name
        Return instructor
    End Function

    Public Function IsTimeIntervalClsSection(clsSectionId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim usesTimeInterval As Boolean

        Try



            db.ConnectionString = conString

            '   build the sql query
            With sb
                .Append("SELECT TOP 1")
                .Append("       TimeIntervalId ")
                .Append("FROM   arClsSectMeetings ")
                .Append("WHERE  ClsSectionId=?")
            End With

            db.OpenConnection()
            db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            Dim obj As Object = db.RunParamSQLScalar(sb.ToString)

            usesTimeInterval = Not (obj Is Nothing)

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return usesTimeInterval
    End Function
    Public Function GetStudentStatus(StuEnrollId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim status As String

        Try

            db.ConnectionString = conString

            '   build the sql query
            With sb
                .Append(" Select t4.SysStatusId    FROM  arStuEnrollments t3,  syStatusCodes t4 ")
                .Append(" WHERE t3.StatusCodeId = t4.StatusCodeId  AND t3.StuEnrollId = ? ")
            End With

            db.OpenConnection()
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            status = db.RunParamSQLScalar(sb.ToString)


        Catch ex As Exception

            Throw New BaseException(ex.Message)

        Finally

            'Close Connection
            db.CloseConnection()

        End Try

        'Return instructor name
        Return status
    End Function
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''Added BY Saraswathi Lakshmanan to fix
    '' issue 14296: Instructor Supervisors and Education Directors(AcademicAdvisors)  
    ''need to be able to post and modify attendance. 
    '' To Find if the User is a Academic Advisors

    Public Function GetIsRoleAcademicAdvisor(UserId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim count As Integer


        db.ConnectionString = conString

        '''SysRoleId=4 is the role of Academic Advisor
        ''' 
        With sb
            .Append("select Count(roleId) from syRoles where roleId in ")
            .Append("(select roleId from syUsersRolesCampGrps where ")
            .Append("UserId = ? ")
            .Append("and CampgrpId in (select CampgrpId ")
            .Append("from syCmpGrpCmps where CampusId=(Select CampusId from ")
            .Append("syUsers where UserId= ? )))")
            .Append(" and  SysRoleId=4 ")
        End With

        db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        count = db.RunParamSQLScalar(sb.ToString)

        If count = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    Public Function GetIsRoleAcademicAdvisorORInstructorSuperVisor(UserId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim count As Integer


        db.ConnectionString = conString

        '''SysRoleId=1 is System Administrator 
        '''SysRoleId=4 is the role of Academic Advisor or InstructorSuperVisor
        '''SysRoleId=11 is Instructors Supervisor
        With sb
            .Append("select Count(roleId) from syRoles where roleId in ")
            .Append("(select roleId from syUsersRolesCampGrps where ")
            .Append("UserId = ? ")
            .Append("and CampgrpId in (select CampgrpId ")
            .Append("from syCmpGrpCmps where CampusId=(Select CampusId from ")
            .Append("syUsers where UserId= ? )))")
            .Append(" and  SysRoleId in (1,4,11) ")
        End With

        db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        count = db.RunParamSQLScalar(sb.ToString)

        If count = 0 Then
            Return False
        Else
            Return True
        End If

    End Function
    ''Added by Saraswathi to get the CohortStartDates of the Past and Current Active terms

    Public Function GetClsSectionsForCohortStartDateForAttendance(CohortStartDate As String, campusId As String, instructorId As String, userName As String, Optional ByVal isAcademicAdvisor As Boolean = False) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet



        db.ConnectionString = conString

        'With sb
        '    .Append("SELECT t1.ClsSectionId,t2.Descrip,t1.ClsSection ")
        '    .Append("FROM arClassSections t1, arReqs t2, arAttUnitType t3 ")
        '    .Append("WHERE t1.ReqId = t2.ReqId ")
        '    .Append("AND t2.UnitTypeId = t3.UnitTypeId ")
        '    .Append("AND t1.TermId = ? ")
        '    .Append("AND t3.UnitTypeDescrip <> 'None' ")
        '    .Append("AND t1.CampusId = ? ")
        '    If userName <> "sa" Then
        '        .Append("AND t1.InstructorId = ?")
        '    End If
        'End With

        With sb
            .Append("SELECT t4.ClsSectionId,t2.Descrip,t1.ClsSection ")
            .Append("FROM arClassSections t1, arReqs t2, arAttUnitType t3, arClassSectionTerms t4 ")
            .Append("WHERE(t1.ReqId = t2.ReqId) ")
            .Append("AND t2.UnitTypeId = t3.UnitTypeId ")
            .Append("AND t1.CohortStartDate = ?   ")
            .Append("AND t1.TermId = t4.TermId   ")
            .Append("AND t4.ClsSectionId = t1.ClsSectionId  ")
            .Append("AND t3.UnitTypeDescrip <> 'None' ")
            .Append("AND t1.CampusId = ? ")

            ''Added BY Saraswathi Lakshmanan to fix
            '' issue 14296: Instructor Supervisors and Education Directors (Academic Advisors)
            ''need to be able to post and modify attendance. 

            If userName <> "sa" And isAcademicAdvisor = False Then

                .Append(" AND (t1.InstructorId in (Select InstructorId ")
                .Append(" from arInstructorsSupervisors where")
                .Append(" SupervisorId= ? )")
                .Append(" OR t1.InstructorId= ? )")

            End If
            .Append(" order by t2.Descrip ")
        End With

        db.AddParameter("CohortStartDate", CohortStartDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''Added BY Saraswathi Lakshmanan to fix
        '' issue 14296: Instructor Supervisors and Education Directors (Academic Advisors)
        ''need to be able to post and modify attendance. 

        If userName <> "sa" And isAcademicAdvisor = False Then
            db.AddParameter("instrid", instructorId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("instrid", instructorId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        End If
        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function GetCampusIDoftheEnrollment(StuEnrollId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        'Dim ds As New DataSet
        Dim campusID As String


        db.ConnectionString = conString

        '''SysRoleId=4 is the role of Academic Advisor or InstructorSuperVisor
        ''' 
        With sb
            .Append("select CampusID from arStuEnrollments where StuEnrollId=? ")

        End With

        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        campusID = db.RunParamSQLScalar(sb.ToString).ToString

        Return campusID

    End Function
    Public Function GetCourseTypesForStudent(StuEnrollId As String, termID As String, PrgVerTracksAttendance As Boolean, ClsSectionID As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet



        db.ConnectionString = conString

        With sb
            .Append("SELECT distinct  CSM.InstructionTypeID,ACT.InstructionTypeDescrip  ")
            .Append("FROM arResults t,arClassSections t1, arReqs t2, arAttUnitType t3, arClsSectMeetings CSM, arInstructionType ACT ")
            .Append("WHERE t.StuEnrollId = ? ")
            .Append("And CSM.ClsSectionID= t1.ClsSectionID and CSM.InstructionTypeID= ACT.InstructionTypeID  ")
            .Append(" AND t.TestId=t1.ClsSectionId ")
            .Append(" and      (  t.GrdSysDetailId not in (select GrdSysDetailId from arGradeSystemDetails where IsDrop=1) ")
            .Append(" or t.GrdSysDetailId is null) ")
            If termID <> "" Then
                .Append(" AND t1.TermId IN (" & termID & ") ")
                'Else
                '    .Append("AND t1.TermId = '00000000-0000-0000-0000-000000000000' ")
            End If
            .Append("AND t1.ReqId = t2.ReqId ")
            .Append("AND t2.UnitTypeId = t3.UnitTypeId ")
            If Not PrgVerTracksAttendance Then
                .Append("AND t3.UnitTypeDescrip <> 'None' ")
            End If
            If ClsSectionID <> "" Then
                .Append(" AND t1.ClsSectionID IN (" & ClsSectionID & ") ")
            End If
        End With

        db.AddParameter("StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'db.AddParameter("termid", termID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function GetCoursetypesfortheClsSection(ClsSectionID As String) As DataTable


        'Dim ds As DataSet
        Dim db As New SQLDataAccess


        db.ConnectionString = conString

        'add studen enrollment parameter
        db.AddParameter("@ClsSectionID", New Guid(ClsSectionID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Try
            Return db.RunParamSQLDataSet_SP("dbo.usp_GetCourseTypesFromClsSection").Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function


    Public Function GetClsSectMeetingsforClsSection(ClsSectionID As String, useperiods As Boolean) As DataTable


        'Dim ds As DataSet
        Dim db As New SQLDataAccess


        db.ConnectionString = conString

        'add studen enrollment parameter
        db.AddParameter("@ClsSectionID", New Guid(ClsSectionID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Try
            If useperiods Then
                Return db.RunParamSQLDataSet_SP("dbo.usp_GetClsSectMeetingsFromClsSectionwithPeriods").Tables(0)
            Else
                Return db.RunParamSQLDataSet_SP("dbo.usp_GetClsSectMeetingsFromClsSectionwithoutPeriods").Tables(0)
            End If

        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Sub PostClsSectAttendance(ByVal xmlValues As String, ByVal modUser As String)
        Dim db As New SQLDataAccess



        db.ConnectionString = conString
        Try
            'Call the procedure to insert more than one record all at once

            db.AddParameter("@AttValues", xmlValues, SqlDbType.VarChar, , ParameterDirection.Input)
            db.AddParameter("@ModUser", modUser, SqlDbType.VarChar, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("USP_PostClsSectAttendance_Insert", Nothing)
        Catch ex As Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub


    Public Function InsertClsSectAttendanceInfo(ClsSectAttObj As ClsSectAttendanceInfo) As String

        Dim db As New SQLDataAccess
        'Dim sb As New StringBuilder
        'Dim dt As New DataTable

        Try


            'Set the connection string
            db.ConnectionString = conString

            db.AddParameter("@StuEnrollid", ClsSectAttObj.StuEnrollId, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ClsSectionId", ClsSectAttObj.ClsSectionId, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@ClsSectMeetingId", ClsSectAttObj.ClsSectMeetingId, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            db.AddParameter("@MeetDate", ClsSectAttObj.MeetDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@Actual", ClsSectAttObj.Actual, SqlDbType.Decimal, , ParameterDirection.Input)
            db.AddParameter("@Scheduled", ClsSectAttObj.Scheduled, SqlDbType.Decimal, , ParameterDirection.Input)

            db.AddParameter("@Tardy", ClsSectAttObj.Tardy, SqlDbType.Bit, , ParameterDirection.Input)
            db.AddParameter("@Excused", ClsSectAttObj.Excused, SqlDbType.Bit, , ParameterDirection.Input)


            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_ClsSectAttendance_Insert")

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return String.Empty
    End Function

    Public Function GetCancelDays_byClass(clsSectionId As String, ClsSectMeetingID As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet



        db.ConnectionString = conString

        With sb
            .Append("Select StartDate,EndDate,ClsSectionId, ClsMeetingId from arUnschedClosures where ClsSectionId = ? and ClsMeetingId=? ")
            '.Append(" and StartDate >= ? and EndDate <= ? ")
        End With

        db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("clssectMeetingid", ClsSectMeetingID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'db.AddParameter("startdate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        'db.AddParameter("enddate", EndDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString())

        Return ds.Tables(0)
    End Function
#Region "Get AdvAppsetting for Manage Config entry"
    Private Function GetAdvAppSettings() As AdvAppSettings

        Return myAdvAppSettings
    End Function
#End Region
End Class
