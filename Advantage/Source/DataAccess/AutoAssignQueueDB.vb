Imports System.Data
Imports System.Text
Imports System.Xml
Public Class AutoAssignQueueDB
    Public Sub AddAutoAssignQueue(ByVal AutoAssignQueueInfo As AutoAssignQueueInfo)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("INSERT INTO cmAutoAssignQueue")
            .Append(" (AutoAssignQueueId, AutoAssignRuleId, BasisValuePKId, DateToCreate)")
            .Append(" VALUES (?,?,?,?)")
        End With
        db.AddParameter("@Param1", AutoAssignQueueInfo.AutoAssignQueueId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param2", AutoAssignQueueInfo.AutoAssignRuleId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param3", AutoAssignQueueInfo.BasisValuePKId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param4", AutoAssignQueueInfo.DateToCreate, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex.InnerException)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub

    Public Sub DeleteAutoAssignQueue(ByVal AutoAssignQueueID As Guid)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("DELETE FROM cmAutoAssignQueue WHERE AutoAssignQueueId = ?")
        End With
        db.AddParameter("@AutoAssignRuleId", AutoAssignQueueID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub

    Public Function GetAutoAssignQueueList() As DataSet
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Dim ds As New DataSet
        With strSQL
            .Append("SELECT a.*, b.* FROM cmAutoAssignQueue a, cmAutoAssignRule b")
            .Append(" WHERE a.AutoAssignRuleId = b.AutoAssignRuleId AND ")
            .Append(" a.DateToCreate <= ?")
        End With
        db.AddParameter("@DateToCreate", Now.Date, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        db.OpenConnection()
        Try
            ds = db.RunParamSQLDataSet(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.CloseConnection()
        db.ClearParameters()
        Return ds
    End Function
    Public Sub AutoAssignWindowsService()
        Dim ds As DataSet
        Dim tbl As New DataTable
        Dim row As DataRow
        Dim AutoAssignDB As New AutoAssignDB
        Dim AutoAssignQueueDB As New AutoAssignQueueDB
        ds = AutoAssignQueueDB.GetAutoAssignQueueList()
        tbl = ds.Tables(0)
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("AutoAssignQueueId")}
        End With
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row In tbl.Rows
                AutoAssignDB.ApplyRule(row, row("ObjectName"), XmlConvert.ToString(row("BasisValuePKId")))
            Next
        End If
    End Sub
End Class
