Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class RptParams
#Region "Private variables and objects"
    Private m_Context As HttpContext
#End Region

#Region "Public Constructors"
    Public Sub New()
        MyBase.New()

        m_Context = HttpContext.Current
    End Sub
#End Region

#Region "Private Methods"
    Private Function GetDDLInfo(ByVal ddlId As Integer) As DataTable
        '**************************************************************************************************
        'Purpose:       Returns a dt with the DDL info for the requested ddl.
        '               collection.
        'Parameters:
        '[ddlId]        Id of the DDL that info is needed for.
        'Returns:       DataTable
        'Created:       Troy Richards, 9/9/2003
        'Notes:         This sub relies on the ResourceId passed in the querystring or in the form's params 
        '               collection when there is a postback.
        '**************************************************************************************************
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim da As OleDbDataAdapter
        Dim resourceId As Integer
        Dim sb As New System.Text.StringBuilder
        Dim Lang As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            resourceId = CInt(m_Context.Request.Params("resid"))

            With sb
                .Append("SELECT t1.RptParamId,t3.FldName,t4.DDLId,t5.TblName,")
                .Append("t6.FldName as DispText,t7.FldName as DispValue,t4.ResourceId as DDLResourceId ")
                .Append("FROM syRptParams t1,syTblFlds t2,syFields t3,syDDLS t4,syTables t5,")
                .Append("syFields t6,syFields t7 ")
                .Append("WHERE t1.TblFldsId = t2.TblFldsId ")
                .Append("AND t2.FldId=t3.FldId ")
                .Append("AND t3.DDLId=t4.DDLId ")
                .Append("AND t4.TblId=t5.TblId ")
                .Append("AND t4.DispFldId=t6.FldId ")
                .Append("AND t4.ValFldId=t7.FldId ")
                .Append("AND t1.ResourceId=? ")
                .Append("AND t4.DDLId=?")
            End With

            db.AddParameter("@resid", resourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ddlid", ddlId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ds = db.RunParamSQLDataSet(sb.ToString, "RptParamDDL")
            Return ds.Tables(0)
        Catch ex As System.Exception
            Throw New BaseException("Error retrieving parameters for report - " & ex.InnerException.Message)
        Finally
            db.Dispose()
            db = Nothing
        End Try
    End Function
#End Region

#Region "Public Methods"
    Public Function GetSavedPrefsList() As DataTable
        '**************************************************************************************************
        'Purpose:       Retrieves the preferences saved for a given report by a specific user  
        'Parameters:
        'Returns:       DataTable containing the preference id and the preference name.
        'Created:       Troy Richards, 9/5/2003
        'Notes:         This sub relies on the ResourceId passed in the querystring or in the form's params 
        '               collection when there is a postback.It also relies on the user id that is saved to
        '               his/her session when he/she logs in. This explains why there is no need to pass any
        '               parameters.
        '**************************************************************************************************
        Dim userName As String
        Dim resourceId As String
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Try
            resourceId = CInt(m_Context.Request.Params("resid"))
            userName = m_Context.Session("UserName")
            'resourceId = 100
            'userName = "troy"

            With sb
                .Append("SELECT PrefId,PrefName ")
                .Append("FROM syRptUserPrefs ")
                .Append("WHERE UserId = ? ")
                .Append("AND ResourceId = ? ")
                .Append("ORDER BY PrefName")
            End With

            db.AddParameter("@usrname", userName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@resid", resourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.ConnectionString = "ConString"
            ds = db.RunParamSQLDataSet(sb.ToString)
            Return ds.Tables(0)
        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New BaseException("Error retrieving preferences list - " & ex.Message)
            Else
                Throw New BaseException("Error retrieving preferences list - " & ex.InnerException.Message)
            End If

        Finally
            db.Dispose()
            db = Nothing
        End Try

    End Function

    Public Function GetUserRptPrefs(ByVal prefId As String) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            'This section deals with fetching the sort preferences. Note that
            'the caption will be retrieved in the ui. For now we store the
            'value 'Empty'.
            With sb
                .Append("SELECT t1.RptParamId,t1.Seq,'Empty' as Caption ")
                .Append("FROM syRptSortPrefs t1 ")
                .Append("WHERE t1.PrefId = ? ")
                .Append("ORDER BY t1.Seq")
            End With

            db.AddParameter("@prefid", prefId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "SortPrefs")
            sb.Remove(0, sb.Length)

            'This section deals with fetching the FilterList prefs
            With sb
                .Append("SELECT RptParamId,FldValue ")
                .Append("FROM syRptFilterListPrefs ")
                .Append("WHERE PrefId=?")
            End With

            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "FilterListPrefs")
            sb.Remove(0, sb.Length)

            'This section deals with fetching the FiltherOther prefs
            With sb
                .Append("SELECT RptParamId,OpId,OpValue ")
                .Append("FROM syRptFilterOtherPrefs ")
                .Append("WHERE PrefId=?")
            End With

            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "FilterOtherPrefs")
            sb.Remove(0, sb.Length)

        Catch ex As System.Exception
            Throw New BaseException("Error retrieving sort preferences - " & ex.Message)
        Finally
            'Close connection.
            db.CloseConnection()
            db.Dispose()
            db = Nothing
        End Try

        Return ds

    End Function

    Public Function GetRptParams() As DataSet
        '**************************************************************************************************
        'Purpose:       Retrieves all the necessary info to set up the parameters for the requested report   
        'Parameters:    None
        'Returns:       DataSet containing the parameters information. 
        'Created:       Troy Richards, 9/5/2003
        'Notes:         This sub relies on the ResourceId passed in the querystring or in the form's params 
        '               collection when there is a postback.
        '**************************************************************************************************
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim da As OleDbDataAdapter
        Dim resourceId As Integer
        Dim sb As New System.Text.StringBuilder
        Dim Lang As String
        Dim objAllowParams As Object

        Lang = m_Context.Items("Language")
        Lang = Lang.ToUpper
        resourceId = CInt(m_Context.Request.Params("resid"))

        db.ConnectionString = "ConString"

        Try
            With sb
                'query to retrieve report params
                .Append("SELECT DISTINCT t1.RptParamId,t1.RptCaption AS Caption,")
                .Append("t1.Required,t1.SortSec,t1.FilterListSec,t1.FilterOtherSec,t2.TblId,t8.TblName,t5.FldName,t5.FldId,t5.DDLId,t5.FldLen,t7.FldType,t3.FldDescrip ")
                .Append("FROM syRptParams t1, syTblFlds t2, syFldCaptions t3,syLangs t4,syFields t5,syFieldTypes t7,syTables t8 ")
                .Append("WHERE t1.TblFldsId=t2.TblFldsId ")
                .Append("AND t2.TblId=t8.Tblid ")
                .Append("AND t2.FldId=t3.FldId ")
                .Append("AND t3.LangId=t4.LangId ")
                .Append("AND t2.FldId=t5.FldId ")
                .Append("AND t5.FldTypeId=t7.FldTypeId ")
                .Append("AND t1.ResourceId = ? ")
                .Append("AND t4.LangName = ? ")

                ''For Attendance History Multiple students report and print Attendance Sheet report
                ''The order in which the filter list items are bound is based on the caption and not based on the required fields
                ''Modified by Saraswathi lakshmanan on July 27 2009
                If resourceId = 591 Or resourceId = 589 Or resourceId = 590 Then
                    .Append("ORDER BY t3.FldDescrip DESC,t1.RptCaption;")
                Else
                    .Append("ORDER BY t1.Required desc,t3.FldDescrip desc,t1.RptCaption;")
                End If

                'query to retrieve report properties
                .Append("SELECT t.AllowParams,t.AllowDescrip,t.AllowInstruct,t.AllowNotes,")
                'Code Commented by Vijay Ramteke May, 11 2009
                '.Append("t.AllowFilters,t.AllowSortOrder,t.SelectFilters,t.SelectSortOrder,t.AllowStudentGroup ")
                'Code Commented by Vijay Ramteke May, 11 2009
                'Code Added by Vijay Ramteke May, 11 2009
                .Append("t.AllowFilters,t.AllowSortOrder,t.SelectFilters,t.SelectSortOrder,t.AllowStudentGroup,t.AllowDateIssue,t.AllowProgramGroup,t.ShowClassDates,t.ShowProjExceedsBal ")
                'Code Added by Vijay Ramteke May, 11 2009
                'Added by Vijay Ramteke Feb 08, 2010
                .Append(", t.ShowCosts, t.ShowExpectedFunding, t.ShowCategoryBreakdown ")
                'Added by Vijay Ramteke Feb 08, 2010
                'Added by Vijay Ramteke March 05, 2010
                .Append(", t.ShowEnrollmentGroup, t.ShowDisbNotBeenPaid,t.ShowLegalDisclaimer,t.BaseCAOnRegClasses  ")
                'Added by Vijay Ramteke March 05, 2010
                'New Code Added By Vijay Ramteke September 28, 2010 For Mantis Id 17685
                .Append(", t.ShowTransferCampus, t.ShowTransferProgram,t.ShowLDA  ")
                'New Code Added By Vijay Ramteke September 28, 2010 For Mantis Id 17685
                'New Code Added By Vijay Ramteke October 27, 2010 For Rally Id US1178
                .Append(", t.ShowUseStuCurrStatus  ")
                'New Code Added By Vijay Ramteke October 27, 2010 For Rally Id US1178
                ''added for attendance signature line in attendnace history reports
                .Append(", t.ShowUseSignLineForAttnd  ")
                .Append(", t.ShowTermProgressDescription  ")
                .Append("FROM syRptProps t ")
                .Append("WHERE t.ResourceId = ? ")
            End With

            db.AddParameter("@resid", resourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@langname", Lang, DataAccess.OleDbDataType.OleDbString, 5, ParameterDirection.Input)
            db.AddParameter("@resid", resourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'ds = db.RunParamSQLDataSet(sb.ToString, "RptParams")
            ds = db.RunParamSQLDataSet(sb.ToString)

            If ds.Tables.Count > 0 Then
                ds.Tables(0).TableName = "RptParams"
                If ds.Tables.Count > 1 Then ds.Tables(1).TableName = "RptProps"
            End If
            Return ds

        Catch ex As System.Exception
            Throw New BaseException("Error retrieving parameters for report - " & ex.InnerException.Message)
        Finally
            db.Dispose()
            'db.CloseConnection()
        End Try

    End Function

    Public Function GetRptParamsForFilterOther(ByVal resourceId As Integer) As DataTable
        '**************************************************************************************************
        'Purpose:       Retrieves the parameters for the FilterOther section for the requested report   
        'Parameters:
        '[resourceId]   The resourceid of the .aspx page that has the report.
        'Returns:       DataTable containing the parameters information. 
        'Created:       Troy Richards, 9/8/2003
        '**************************************************************************************************
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim da As OleDbDataAdapter
        Dim sb As New System.Text.StringBuilder
        Dim Lang As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try
            Lang = m_Context.Items("Language")
            Lang = Lang.ToUpper

            With sb
                .Append("SELECT t1.RptParamId,t1.RptCaption AS Caption,t1.Required,t2.FldId,t5.FldName,t6.FldType,t5.FldLen,t7.TblName ")
                .Append("FROM syRptParams t1, syTblFlds t2, syFldCaptions t3,syLangs t4,syFields t5,syFieldTypes t6,syTables t7  ")
                .Append("WHERE t1.TblFldsId=t2.TblFldsId ")
                .Append("AND t2.FldId=t3.FldId ")
                .Append("AND t3.LangId=t4.LangId ")
                .Append("AND t2.FldId=t5.FldId ")
                .Append("AND t5.FldTypeId=t6.FldTypeId ")
                .Append("AND t1.FilterOtherSec = 1 ")
                .Append("AND t1.ResourceId = ? ")
                .Append("AND t4.LangName = ? ")
                .Append(" AND t2.TblId=t7.TblId ")
                .Append("ORDER BY t1.RptCaption")
            End With

            db.AddParameter("@resid", resourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@langname", Lang, DataAccess.OleDbDataType.OleDbString, 5, ParameterDirection.Input)

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ds = db.RunParamSQLDataSet(sb.ToString)
            Return ds.Tables(0)
        Catch ex As System.Exception
            Throw New BaseException("Error retrieving FilterOther section parameters for report - " & ex.InnerException.Message)
        Finally
            db.Dispose()
            db = Nothing
        End Try
    End Function

    Public Sub AddUserPrefInfo(ByVal prefId As String, ByVal userId As String,
            ByVal resId As Integer, ByVal prefName As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim sGuid As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim intPrefCount As Integer = 0
        With sb
            .Append("select count(*) as RowCount from syRptUserPrefs where prefId=? and ResourceId=? and PrefName=? ")
        End With
        db.AddParameter("@prefid", prefId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@resid", resId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@name", prefName, DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)

        Try
            intPrefCount = db.RunParamSQLScalar(sb.ToString)
        Catch ex As System.Exception
        Finally
            sb.Remove(0, sb.Length)
            db.ClearParameters()
        End Try

        If intPrefCount = 0 Then
            With sb
                .Append("INSERT INTO syRptUserPrefs")
                .Append("(PrefId,UserId,ResourceId,PrefName)")
                .Append("VALUES(?,?,?,?)")
            End With

            db.AddParameter("@prefid", prefId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@resid", resId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@name", prefName, DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)

            Try
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
            Catch ex As System.Exception
                Throw New BaseException("Error adding user preference basic info: " & ex.InnerException.Message)
            Finally
                db.Dispose()
                db = Nothing
            End Try
        End If

    End Sub

    Public Sub UpdateUserPrefInfo(ByVal prefId As String, ByVal prefName As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("UPDATE syRptUserPrefs ")
            .Append("SET PrefName = ? ")
            .Append("WHERE PrefId = ?")
        End With

        db.AddParameter("@prefname", prefName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prefid", prefId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As System.Exception
            Throw New BaseException("Error updating user preference basic info: " & ex.InnerException.Message)
        Finally
            db.Dispose()
            db = Nothing
        End Try

    End Sub

    Public Sub DeleteUserPrefInfo(ByVal prefId As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim sGuid As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            'This section deals with deleting the sort prefs.
            With sb
                .Append("DELETE FROM syRptSortPrefs ")
                .Append("WHERE PrefId=?")
            End With

            db.AddParameter("@prefid", prefId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'This section deals with deleting the filter list prefs.
            With sb
                .Append("DELETE FROM syRptFilterListPrefs ")
                .Append("WHERE PrefId=?")
            End With

            db.AddParameter("@prefid", prefId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'This section deals with deleting the filter other prefs.
            With sb
                .Append("DELETE FROM syRptFilterOtherPrefs ")
                .Append("WHERE PrefId=?")
            End With

            db.AddParameter("@prefid", prefId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'This section deals with deleting the general info. It should be done
            'last so as not to violate any referential constraints.
            With sb
                .Append("DELETE FROM syRptUserPrefs ")
                .Append("WHERE PrefId=?")
            End With

            db.AddParameter("@prefid", prefId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As System.Exception
            Throw New BaseException("Error deleting preference info for user: " & ex.InnerException.Message)
        Finally
            db.Dispose()
            db = Nothing
        End Try
    End Sub

    Public Sub AddUserSortPrefs(ByVal prefId As String, ByVal rptParamId As Integer,
            ByVal Seq As Integer)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim sGuid As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        sGuid = Guid.NewGuid.ToString
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("INSERT INTO syRptSortPrefs")
            .Append("(SortPrefId,PrefId,RptParamId,Seq)")
            .Append("VALUES(?,?,?,?)")
        End With

        db.AddParameter("@sortprefid", sGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prefid", prefId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@rptparamid", rptParamId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@seq", Seq, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As System.Exception
            Throw New BaseException("Error adding sort preference: " & ex.InnerException.Message)
        Finally
            db.Dispose()
            db = Nothing
        End Try
    End Sub

    Public Sub DeleteUserSortPrefs(ByVal prefId As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("DELETE FROM syRptSortPrefs ")
            .Append("WHERE PrefId = ?")
        End With

        db.AddParameter("@prefid", prefId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As System.Exception
            Throw New BaseException("Error deleting user sort preference info: " & ex.InnerException.Message)
        Finally
            db.Dispose()
            db = Nothing
        End Try
    End Sub

    Public Function GetFilterListValues(ByVal ddlId As Integer, Optional ByVal showall As Boolean = False, Optional userId As String = "") As DataTable
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim resourceId As Integer
        Dim sb As New StringBuilder
        'Dim userId As String = m_Context.Current.Session("UserId").ToString

        Select Case ddlId
            Case 5

                With sb
                    .Append(" SELECT A.CampusId,A.CampDescrip,B.CampGrpId FROM syCampuses A,dbo.syCmpGrpCmps B WHERE  ")
                    .Append(" A.CampusId=B.CampusId ORDER BY A.CampDescrip ")
                End With


            Case 7
                'Added by Michelle R. Rodriguez on 01/18/2005.
                'Special case: Campus Group DDL.
                If m_Context.Current.Session("UserName").ToString = "sa" Then
                    ' Advantage Power User. He/She can see ALL campus groups.
                    With sb
                        .Append("SELECT DISTINCT A.CampGrpId, A.CampGrpDescrip,A.CampGrpId ")
                        .Append("FROM syCampGrps A ")
                        'Modified by balaji on 11/20/2006 to exclude All Campus Groups
                        '.Append(" WHERE A.CampGrpDescrip <> 'All' ")
                        .Append("ORDER BY A.CampGrpDescrip")
                    End With

                Else
                    'If the campus group all is the only campus group assigned, the load all the campus group in general.
                    Dim data As DataSet
                    With New CampusGroupsDB
                        data = .GetAllCampusGroupsByUser(userId)
                        If (Not data Is Nothing) Then

                            If (data.Tables.Count > 0) Then

                                If (data.Tables(0).Rows.Count > 0) Then
                                    If (Not data.Tables(0).Rows(0)("IsAllCampusGrp") Is DBNull.Value) Then
                                        If (CType(data.Tables(0).Rows(0)("IsAllCampusGrp").ToString(), Boolean)) Then
                                            data = .GetAllCampusGroups()
                                        End If
                                    End If
                                End If
                            End If
                        End If

                    End With
                    If (Not data Is Nothing) Then

                        If (data.Tables.Count > 0) Then
                            Return data.Tables(0)
                        End If
                    End If
                    'If the Is All Campus group is not the only campus group then get the assigned campus by roles 
                    'Enforce security: need to get list of campus group that user has access to.
                    With sb
                        .Append("SELECT DISTINCT A.CampGrpId, B.CampGrpDescrip,A.CampGrpId ")
                        .Append("FROM syUsersRolesCampGrps A, syCampGrps B ")
                        .Append("WHERE A.CampGrpId = B.CampGrpId ")
                        .Append("AND A.UserId=? ")
                        'Modified by balaji on 11/20/2006 to exclude All Campus Groups
                        '.Append(" AND B.CampGrpDescrip <> 'All' ")
                        .Append("ORDER BY B.CampGrpDescrip")
                    End With
                    db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
            Case 9
                'Added by Anatoly Sljussar on 10/23/2006.
                'Special case: Job Group.
                Dim campusId As String = GetCampusIdFromContext(m_Context.Current)
                With sb
                    .Append("SELECT ")
                    .Append("       JobCatId, ")
                    .Append("       JobCatDescrip,CampGrpId ")
                    .Append(" FROM   plJobCats JC, syStatuses S ")
                    .Append(" Where  ")
                    .Append("       JC.StatusId=S.StatusId ")
                    If showall = False Then
                        .Append(" AND    S.Status='Active' ")
                    End If
                    '.Append("AND    CampGrpId in (select t1.CampGrpId from syCmpGrpCmps t1, syCmpGrpCmps t2 where t1.CampusId=t2.CampusId and t1.CampGrpId in (select CampGrpId from syCmpGrpCmps where CampusId=?)) ")
                    .Append(" ORDER BY   JobCatDescrip ")

                    'db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    ds = db.RunParamSQLDataSet(sb.ToString)
                    Return ds.Tables(0)
                End With
            Case 53
                Dim campusId As String = GetCampusIdFromContext(m_Context.Current)
                With sb
                    .Append("SELECT ")
                    .Append("       AcademicYearCode as CompOpId, ")
                    .Append("       AcademicYearDescrip as CompOpText ")
                    .Append(" FROM   saAcademicYears AY INNER JOIN syStatuses s ")
                    .Append(" ON s.StatusId = AY.StatusId  ")
                    If showall = False Then
                        .Append(" WHERE S.Status='Active' ")
                    End If
                    .Append(" ORDER BY   AcademicYearDescrip ")
                    ds = db.RunParamSQLDataSet(sb.ToString)
                    Dim dr = ds.Tables(0).NewRow()
                    dr(0) = "1"
                    dr(1) = "Select"
                    ds.Tables(0).Rows.InsertAt(dr, 0)
                    Return ds.Tables(0)
                End With
            Case 54
                'Added by Anatoly Sljussar on 10/23/2006.
                'Special case: FundSources.
                Dim campusId As String = GetCampusIdFromContext(m_Context.Current)
                With sb
                    .Append("SELECT ")
                    .Append("       FundSourceId, ")
                    .Append("       FundSourceDescrip,CampGrpId ")
                    .Append(" FROM   saFundSources FS, syStatuses S ")
                    .Append(" Where  ")
                    .Append("       FS.StatusId=S.StatusId ")
                    If showall = False Then
                        .Append(" AND    S.Status='Active' ")
                    End If
                    '.Append("AND    CampGrpId in (select t1.CampGrpId from syCmpGrpCmps t1, syCmpGrpCmps t2 where t1.CampusId=t2.CampusId and t1.CampGrpId in (select CampGrpId from syCmpGrpCmps where CampusId=?)) ")
                    .Append(" ORDER BY   FundSourceDescrip ")

                    'db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    ds = db.RunParamSQLDataSet(sb.ToString)
                    Return ds.Tables(0)
                End With
            Case 74
                'Added by Anatoly Sljussar on 10/23/2006.
                'Special case: Job Title.
                Dim campusId As String = GetCampusIdFromContext(m_Context.Current)
                With sb
                    .Append("SELECT ")
                    .Append("       TitleId, ")
                    .Append("       TitleDescrip,CampGrpId ")
                    .Append(" FROM   adTitles T, syStatuses S ")
                    .Append(" Where  ")
                    .Append("       T.StatusId=S.StatusId ")
                    If showall = False Then
                        .Append(" AND    S.Status='Active' ")
                    End If
                    '.Append(" AND    CampGrpId in (select t1.CampGrpId from syCmpGrpCmps t1, syCmpGrpCmps t2 where t1.CampusId=t2.CampusId and t1.CampGrpId in (select CampGrpId from syCmpGrpCmps where CampusId=?)) ")
                    .Append(" ORDER BY   TitleDescrip ")

                    'db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    ds = db.RunParamSQLDataSet(sb.ToString)
                    Return ds.Tables(0)
                End With
            Case 81
                'Added by Michelle R. Rodriguez on 02/10/2005.
                'Special case: StuEnrollmentStatus DDL.
                With sb
                    .Append("SELECT DISTINCT D.StatusCodeId, D.StatusCodeDescrip,D.campgrpid  ")
                    .Append("FROM sySysStatus A,syStatuses B,syStatusLevels C,syStatusCodes D ")
                    .Append("WHERE A.StatusId=B.StatusId AND B.Status='Active' ")
                    .Append("AND A.StatusLevelId=C.StatusLevelId AND C.StatusLevelId=2 ")
                    .Append("AND D.SysStatusId=A.SysStatusId ")
                    .Append("ORDER BY D.StatusCodeDescrip ")

                    ds = db.RunSQLDataSet(sb.ToString)
                    Return ds.Tables(0)
                End With
                'Enforce security: need to get list of enrollment statuses that user has access to.
                '.Append("SELECT DISTINCT D.StatusCodeId, D.StatusCodeDescrip ")
                '.Append("FROM sySysStatus A,syStatuses B,syStatusLevels C,syStatusCodes D,syUsersRolesCampGrps E ")
                '.Append("WHERE A.StatusId=B.StatusId AND B.Status='Active' ")
                '.Append("AND A.StatusLevelId=C.StatusLevelId AND C.StatusLevelId=2 ")
                '.Append("AND D.SysStatusId=A.SysStatusId AND E.CampGrpId = D.CampGrpId ")
                '.Append("AND E.UserId=? ")
                '.Append("ORDER BY D.StatusCodeDescrip")
                'db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Case 83
                'Added by Anatoly Sljussar on 10/23/2006.
                'Special case: PrgVersions.
                Dim campusId As String = GetCampusIdFromContext(m_Context.Current)
                Dim resId As String = m_Context.Current.Items("ResourceId")

                With sb

                    If resId = 629 Then
                        .Append("SELECT ")
                        .Append("       PrgVerId, ")
                        .Append(" case when (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=PV.ProgId) is null  then ")
                        .Append(" PV.PrgVerDescrip ")
                        .Append(" else ")
                        .Append(" PV.PrgVerDescrip + ' (' + (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=PV.ProgId) + ')'  ")
                        .Append(" end as   PrgVerDescrip,")
                        .Append(" PV.campgrpid ")
                        .Append("FROM   arPrgVersions PV, syStatuses S,arPrograms P ")
                        .Append("Where  ")
                        .Append("       PV.StatusId=S.StatusId  and P.ProgId=PV.ProgId and P.ACId=5 ")  ' ACId = 5 is a clock hour program
                        If showall = False Then
                            .Append("AND    S.Status='Active' ")
                        End If
                        '.Append("AND    CampGrpId in (select t1.CampGrpId from syCmpGrpCmps t1, syCmpGrpCmps t2 where t1.CampusId=t2.CampusId and t1.CampGrpId in (select CampGrpId from syCmpGrpCmps where CampusId=?)) ")
                        .Append("ORDER BY   PrgVerDescrip ")
                    Else
                        .Append("SELECT ")
                        .Append("       PrgVerId, ")
                        .Append(" case when (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=PV.ProgId) is null  then ")
                        .Append(" PV.PrgVerDescrip ")
                        .Append(" else ")
                        .Append(" PV.PrgVerDescrip + ' (' + (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=PV.ProgId) + ')'  ")
                        .Append(" end as   PrgVerDescrip,")
                        .Append(" campgrpid ")
                        .Append("FROM   arPrgVersions PV, syStatuses S ")
                        .Append("Where  ")
                        .Append("       PV.StatusId=S.StatusId ")
                        If showall = False Then
                            .Append("AND    S.Status='Active' ")
                        End If
                        '.Append("AND    CampGrpId in (select t1.CampGrpId from syCmpGrpCmps t1, syCmpGrpCmps t2 where t1.CampusId=t2.CampusId and t1.CampGrpId in (select CampGrpId from syCmpGrpCmps where CampusId=?)) ")
                        .Append("ORDER BY   PrgVerDescrip ")

                    End If


                    'db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    ds = db.RunParamSQLDataSet(sb.ToString)
                    Return ds.Tables(0)
                End With


            Case 89
                'Added by Michelle R. Rodriguez on 11/30/2005.
                'Special case: LeadStatus DDL.
                'this is an Anatoly placeholder
                Dim campusId As String = GetCampusIdFromContext(m_Context.Current)
                With sb
                    .Append("SELECT DISTINCT D.StatusCodeId, D.StatusCodeDescrip,D.campgrpid ")
                    .Append("FROM sySysStatus A,syStatuses B,syStatusLevels C,syStatusCodes D ")
                    .Append("WHERE A.StatusId=B.StatusId AND B.Status='Active' ")
                    .Append("AND A.StatusLevelId=C.StatusLevelId AND C.StatusLevelId=1 ")
                    .Append("AND D.SysStatusId=A.SysStatusId ")
                    '.Append("AND D.CampGrpId in (select t1.CampGrpId from syCmpGrpCmps t1, syCmpGrpCmps t2 where t1.CampusId=t2.CampusId and t1.CampGrpId in (select CampGrpId from syCmpGrpCmps where CampusId=?)) ")
                    .Append("ORDER BY D.StatusCodeDescrip ")

                    'db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    ds = db.RunParamSQLDataSet(sb.ToString)
                    Return ds.Tables(0)
                End With
            Case 60
                'Added by Anatoly Sljussar on 10/23/2006.
                'Special case: Employer.
                Dim campusId As String = GetCampusIdFromContext(m_Context.Current)
                With sb
                    .Append("SELECT ")
                    .Append("       EmployerId, ")
                    .Append("       EmployerDescrip ,campgrpid ")
                    .Append("FROM   plEmployers E, syStatuses S ")
                    .Append("Where  ")
                    .Append("       E.StatusId=S.StatusId ")
                    If showall = False Then
                        .Append("AND    S.Status='Active' ")
                    End If
                    '.Append("AND    CampGrpId in (select t1.CampGrpId from syCmpGrpCmps t1, syCmpGrpCmps t2 where t1.CampusId=t2.CampusId and t1.CampGrpId in (select CampGrpId from syCmpGrpCmps where CampusId=?)) ")
                    .Append("ORDER BY   EmployerDescrip ")

                    'db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    ds = db.RunParamSQLDataSet(sb.ToString)
                    Return ds.Tables(0)
                End With
            Case 49
                'Added by Michelle R. Rodriguez on 09/19/2005.
                'Special case: Term DDL.

                Dim resId As String = m_Context.Current.Items("ResourceId")
                If resId = "262" Then
                    Dim curDate As String = DateTime.Now.Date.ToShortDateString()
                    With sb
                        .Append("SELECT     TermId,TermDescrip ,campgrpid ")
                        .Append("FROM       arTerm ")
                        .Append("WHERE   StatusId = (SELECT statusid FROM systatuses WHERE Status='Active') ")
                        .Append("ORDER BY   StartDate,EndDate,TermDescrip ")

                        ds = db.RunSQLDataSet(sb.ToString)
                        Return ds.Tables(0)
                    End With
                End If
                With sb
                    .Append("SELECT     TermId,TermDescrip ,campgrpid ")
                    .Append("FROM       arTerm ")
                    .Append("ORDER BY   StartDate,EndDate,TermDescrip ")

                    ds = db.RunSQLDataSet(sb.ToString)
                    Return ds.Tables(0)
                End With
            Case 41
                'Added by Anatoly Sljussar on 10/23/2006.
                'Special case: Industry.
                Dim campusId As String = GetCampusIdFromContext(m_Context.Current)
                With sb
                    .Append("SELECT ")
                    .Append("       IndustryId, ")
                    .Append("       IndustryDescrip ,campgrpid ")
                    .Append("FROM   plIndustries ")
                    '.Append("Where  CampGrpId in (select t1.CampGrpId from syCmpGrpCmps t1, syCmpGrpCmps t2 where t1.CampusId=t2.CampusId and t1.CampGrpId in (select CampGrpId from syCmpGrpCmps where CampusId=?)) ")
                    .Append("ORDER BY   IndustryDescrip ")

                    'db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    ds = db.RunParamSQLDataSet(sb.ToString)
                    Return ds.Tables(0)
                End With
            Case 38
                'Added by Michelle R. Rodriguez on 06/29/2006.
                'Special case: Courses DDL.
                'Added by Balaji on 6.11.2014
                Dim resId As String = m_Context.Current.Items("ResourceId")
                If resId = "262" Then
                    Dim curDate As String = DateTime.Now.Date.ToShortDateString()
                    With sb
                        .Append("SELECT     arReqs.ReqId,")
                        .Append("'(' + Code + ') ' + Descrip  AS Descrip, ")
                        .Append("campgrpid,arReqs.Descrip AS descrip1 ")
                        .Append("FROM       arReqs Inner Join arClassSections CS on arReqs.ReqId = CS.ReqId ")
                        .Append("WHERE      ReqTypeId=1 ")
                        .Append("AND CS.TermId in (")
                        .Append("SELECT  DISTINCT   TermId ")
                        .Append("FROM       arTerm ")
                        .Append("WHERE      EndDate >= '" + curDate + "' ")
                        .Append(") ")
                        .Append("ORDER BY   descrip1,code ")

                        ds = db.RunSQLDataSet(sb.ToString)
                        Return ds.Tables(0)
                    End With
                Else
                    '2/6/2013 US3580 Janet Robinson
                    With sb
                        .Append("SELECT     ReqId,")
                        .Append("'(' + Code + ') ' + Descrip  AS Descrip, ")
                        .Append("campgrpid,arReqs.Descrip AS descrip1 ")
                        .Append("FROM       arReqs ")
                        .Append("WHERE      ReqTypeId=1 ")
                        .Append("ORDER BY   descrip1,code ")

                        ds = db.RunSQLDataSet(sb.ToString)
                        Return ds.Tables(0)
                    End With
                End If


            Case 102
                Dim resId As String = m_Context.Current.Items("ResourceId")
                If resId = "262" Then
                    Dim curDate As String = DateTime.Now.Date.ToShortDateString()
                    With sb
                        .Append("SELECT     Distinct(U.UserId), U.FullName, arReqs.CampGrpId as CampGrpId ")
                        .Append("FROM       arReqs Inner Join arClassSections CS on arReqs.ReqId = CS.ReqId ")
                        .Append("inner join syUsers U on CS.InstructorId = U.UserId ")
                        .Append("WHERE   ")
                        .Append(" CS.TermId in (")
                        .Append("SELECT  DISTINCT   TermId ")
                        .Append("FROM       arTerm ")
                        .Append("WHERE      EndDate >= '" + curDate + "' ")
                        .Append(") ")
                        .Append("ORDER BY U.FullName ")
                        ds = db.RunSQLDataSet(sb.ToString)
                        Return ds.Tables(0)
                    End With
                Else
                    With sb
                        .Append("SELECT     Distinct(U.UserId), U.FullName, arReqs.CampGrpId as CampGrpId ")
                        .Append("FROM       arReqs Inner Join arClassSections CS on arReqs.ReqId = CS.ReqId ")
                        .Append("inner join syUsers U on CS.InstructorId = U.UserId ")
                        .Append("ORDER BY U.FullName ")

                        ds = db.RunSQLDataSet(sb.ToString)
                        Return ds.Tables(0)
                    End With
                End If
            Case Else
                Dim sTemplate As String
                Dim dr As DataRow
                Dim displayField As String = "%DisplayField%"
                Dim valueField As String = "%ValueField%"
                Dim tableName As String = "%TableName%"
                Dim dtDDLDefs As DataTable

                '' New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19644
                Dim resId As String = m_Context.Current.Items("ResourceId")
                '' New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19644

                'We first need to retrieve the information for the DDLS.
                dtDDLDefs = GetDDLInfo(ddlId)

                Try
                    'Only one row is returned so we can point dr to the first row.

                    db.ConnectionString = "ConString"
                    dr = dtDDLDefs.Rows(0)
                    'We can now set up the template
                    If (dr("TblName").ToString() = "arPrograms") Then
                        If dr("DispValue").ToString() = "ProgId" Then
                            sTemplate = " SELECT distinct ProgId," &
"case when (select ShiftDescrip from arShifts where arShifts.shiftid=arPrograms.shiftid ) is null  then arPrograms.ProgDescrip " &
 "else  arPrograms.ProgDescrip + ' (' + (select ShiftDescrip from arShifts where  arShifts.shiftid=arPrograms.shiftid ) + ')'  " &
 "end as   ProgDescrip ,arPrograms.CampGrpId FROM arPrograms ,syStatuses S Where  arPrograms.StatusId=S.StatusId AND S.Status='Active' " &
       "ORDER BY ProgDescrip          "
                        Else
                            sTemplate = "SELECT distinct %ValueField%,%DisplayField% " &
                                                                ",arPrograms.CampGrpId FROM %TableName% " &
                                                                 ",syStatuses S Where  arPrograms.StatusId=S.StatusId AND S.Status='Active'" &
                                                                 " ORDER BY %DisplayField%"
                        End If

                    ElseIf (dr("TblName").ToString() = "arGradeSystemDetails") Then
                        sTemplate = "SELECT distinct %ValueField%,%DisplayField% " &
                                      ",S.CampGrpId FROM %TableName% " &
                                       ",arGradeSystems S Where  arGradeSystemDetails.GrdSystemId=S.GrdSystemId " &
                                       " ORDER BY %DisplayField%"
                    ElseIf (dr("TblName").ToString() = "rptInstructor") Then
                        sTemplate = "SELECT distinct %ValueField%,%DisplayField% " &
                                      ",S.CampGrpId FROM %TableName% " &
                                       ",syUsersRolesCampGrps S Where  rptInstructor.Instructorid=S.UserId " &
                                       " ORDER BY %DisplayField%"
                    ElseIf (dr("TblName").ToString() = "rptAdmissionsRep") Then
                        sTemplate = "SELECT distinct %ValueField%,%DisplayField% " &
                                      ",S.CampGrpId FROM %TableName% " &
                                       ",syUsersRolesCampGrps S Where  rptAdmissionsRep.AdmissionsRepID=S.UserId " &
                                       " ORDER BY %DisplayField%"
                    ElseIf (dr("TblName").ToString() = "plJobSchedule") Then
                        sTemplate = "SELECT distinct %ValueField%,%DisplayField% " &
                                     ",CamGrpId FROM %TableName% " &
                                     " ORDER BY %DisplayField%"
                    ElseIf (dr("TblName").ToString() = "syRoles" Or dr("TblName").ToString() = "syStatuses") Then
                        sTemplate = "SELECT distinct %ValueField%,%DisplayField% " &
                                     ",(select CampGrpId from syCampgrps Where CampGrpCode='All') as CampGrpId FROM %TableName% " &
                                     " ORDER BY %DisplayField%"
                        ''Added by Saraswathi lakshmanan on July 21 2009
                        ''For payment types thhe table does not have campGrpid
                    ElseIf dr("TblName").ToString() = "saPaymentTypes" Then
                        sTemplate = "SELECT distinct %ValueField%,%DisplayField% " &
                                     ",(select CampGrpId from syCampgrps Where CampGrpCode='All') as CampGrpId FROM %TableName% " &
                                     " ORDER BY %DisplayField%"
                        '' New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19644
                    ElseIf dr("TblName").ToString().ToLower = "adleadgroups" And (resId = 247 Or resId = 249) Then
                        sTemplate = "SELECT distinct %ValueField%,%DisplayField% " &
                                     ",CampGrpId FROM %TableName% Where UseForScheduling=1 " &
                                     " ORDER BY %DisplayField%"
                        '' New Code Added By Vijay Ramteke On August 28, 2010 For Mantis Id 19644
                    ElseIf dr("TblName").ToString().ToLower = "adleadgroups" And resId = 493 Then
                        sTemplate = "SELECT distinct %ValueField%,%DisplayField% " &
                                     ",CampGrpId FROM %TableName% Where StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' and UseForStudentGroupTracking = 0 " &
                                     " ORDER BY %DisplayField%"
                        ''Added By Edwin S. on April 24,2018 AD-4552
                    ElseIf dr("TblName").ToString().ToLower = "adleadgroups" And (resId = 229) Then
                        sTemplate = "SELECT distinct %ValueField%,%DisplayField% " &
                                    ",CampGrpId FROM %TableName% Where StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'" &
                                    " ORDER BY %DisplayField%"
                        '' New Code Added By Vijay Ramteke On October 21, 2010 For Rally Id 1051
                    ElseIf dr("TblName").ToString().ToLower = "satranscodes" And resId = 650 Then
                        sTemplate = "SELECT distinct %ValueField%,%DisplayField% " &
                                     ",CampGrpId FROM %TableName%, syStatuses Where saTransCodes.StatusId=syStatuses.StatusId and syStatuses.Status='Active' And SysTransCodeId not in (11,12,13,14,15,16) " &
                                     " ORDER BY %DisplayField%"
                        '' New Code Added By Vijay Ramteke On October 21, 2010 For Rally Id 1051
                        'ElseIf dr("TblName").ToString().ToLower = "sytypeofrequirement" And resId = 493 Then
                        '    'John G - added to resolve issue with order by creating ambigouus field error
                        '    sTemplate = "SELECT distinct %ValueField%,%DisplayField% " & _
                        '                 ",CampGrpId FROM %TableName% ORDER BY %TableName%.%DisplayField%"
                        '' New Code Added By Vijay Ramteke On October 21, 2010 For Rally Id 1051
                    Else
                        sTemplate = "SELECT distinct %ValueField%,%DisplayField% " &
                                     ",CampGrpId FROM %TableName% " &
                                     " ORDER BY %DisplayField%"
                    End If

                    'Replace the TableName token
                    sTemplate = sTemplate.Replace(tableName, dr("TblName").ToString())
                    'Replace the DisplayField token
                    sTemplate = sTemplate.Replace(displayField, dr("DispText").ToString())
                    'Replace the ValueField token
                    sTemplate = sTemplate.Replace(valueField, dr("DispValue").ToString())



                    sb.Append(sTemplate)

                Catch ex As System.Exception
                    Throw New BaseException("Error getting info for DDL - " & ex.Message)
                End Try
        End Select

        Try
            ds = db.RunParamSQLDataSet(sb.ToString)
            Return ds.Tables(0)

        Catch ex As System.Exception
            Throw New BaseException("Error getting info for DDL - " & ex.Message)

        Finally
            db.Dispose()
            db = Nothing
        End Try
    End Function
    Private Function GetCampusIdFromContext(ByVal context As HttpContext) As String
        Return context.Request.Params("cmpid")
    End Function
    Public Sub AddUserFilterListPrefs(ByVal prefId As String, ByVal rptParamId As Integer,
            ByVal fldValue As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim sGuid As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        sGuid = Guid.NewGuid.ToString
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("INSERT INTO syRptFilterListPrefs")
            .Append("(FilterListPrefId,PrefId,RptParamId,FldValue) ")
            .Append("VALUES(?,?,?,?)")
        End With

        db.AddParameter("@filterlistprefid", sGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prefid", prefId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@rptparamid", rptParamId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@fldvalue", fldValue, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As System.Exception
            Throw New BaseException("Error adding filter list preference: " & ex.InnerException.Message)
        Finally
            db.Dispose()
            db = Nothing
        End Try
    End Sub

    Public Sub DeleteUserFilterListPrefs(ByVal prefId As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("DELETE FROM syRptFilterListPrefs ")
            .Append("WHERE PrefId = ?")
        End With

        db.AddParameter("@prefid", prefId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As System.Exception
            Throw New BaseException("Error deleting user filter list preference info: " & ex.InnerException.Message)
        Finally
            db.Dispose()
            db = Nothing
        End Try
    End Sub

    Public Sub AddUserFilterOherPrefs(ByVal prefid As String, ByVal rptParamId As Integer,
            ByVal opId As Integer, ByVal fldValue As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim sGuid As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        sGuid = Guid.NewGuid.ToString
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("INSERT INTO syRptFilterOtherPrefs")
            .Append("(FilterOtherPrefId,PrefId,RptParamId,OpId,OpValue) ")
            .Append("VALUES(?,?,?,?,?)")
        End With

        db.AddParameter("@filterotherprefid", sGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prefid", prefid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@rptparamid", rptParamId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@opid", opId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@opvalue", fldValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As System.Exception
            Throw New BaseException("Error adding filter others preference: " & ex.InnerException.Message)
        Finally
            db.Dispose()
            db = Nothing
        End Try
    End Sub

    Public Sub DeleteUserFilterOtherPrefs(ByVal prefId As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("DELETE FROM syRptFilterOtherPrefs ")
            .Append("WHERE PrefId = ?")
        End With

        db.AddParameter("@prefid", prefId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As System.Exception
            Throw New BaseException("Error deleting user filter others preference info: " & ex.InnerException.Message)
        Finally
            db.Dispose()
            db = Nothing
        End Try
    End Sub

    Public Function HasRptParams() As Boolean
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim da As OleDbDataAdapter
        Dim resourceId As Integer
        Dim sb As New System.Text.StringBuilder
        Dim objAllowParams As Object

        resourceId = CInt(m_Context.Request.Params("resid"))
        db.ConnectionString = "ConString"

        Try
            With sb
                .Append("SELECT AllowParams FROM syRptProps ")
                .Append("WHERE ResourceId = ? ")
            End With

            db.AddParameter("@resid", resourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            objAllowParams = db.RunParamSQLScalar(sb.ToString)

            If Not objAllowParams Is Nothing Then
                Return Convert.ToBoolean(objAllowParams)
            Else
                Return False
            End If

        Catch ex As System.Exception
            Throw New BaseException("Error retrieving parameters for report - " & ex.InnerException.Message)
        Finally
            db.Dispose()
            'db.CloseConnection()
        End Try
    End Function
    ''Added by Saraswathi lakshmanan On May 11 2009
    ''Added by saraswathi lakshmanan
    ''Added on May 11 2009
    ''Fix for mantis case: 14745
    ''When a campus group is selected, The campus available in htat campus group is found. 
    ''And  the other campus groups which holds the same campus from the selected group is found.
    ''Thus the entities whose, campus groups mapped to the selected campus groups are listed.
    ''EG: Miami Campusgroup has Miami campus and Orlando Campus group has orlando campus.
    ''Florida campus Group has MI and Or campus. Tampa Campus group has Tampa Campus
    ''All campus group has MI, Or, Ta Campuses
    ''When we select Florida Campus Group, Anything mapped to Florida, Miami, Orlando and All are listed 
    Public Function GetCampusgroups(ByVal CampGrpId As String) As DataSet
        '''
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try

            With sb
                .Append("Select  CampGrpId from SyCampGrps where CampGrpId in (Select Distinct campGrpId from  syCmpGrpCmps where CampusId in ( Select CampusID from syCmpGrpCmps where ")
                .Append(CampGrpId + "))")
            End With

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()
            ds = db.RunSQLDataSet(sb.ToString)

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            sb.Remove(0, sb.Length)
            Return ds
        Catch ex As System.Exception
            Throw New BaseException("Error retrieving parameters for report - " & ex.InnerException.Message)

        End Try

    End Function
    Public Sub AddUserPrefForDailyCompletedHours(ByVal prefId As String, ByVal CampGrpId As String,
          ByVal TermId As String, ByVal PrgVerId As String, ByVal FirstName As String, ByVal LastName As String,
          ByVal TermStartDate As DateTime, ByVal TermEndDate As DateTime, ByVal AttendanceStartDate As DateTime,
          ByVal AttendanceEndDate As DateTime)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim sGuid As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("INSERT INTO tblPrefReport")
            .Append("(PrefReportId,PrefId,CampGrpId,TermId,PrgVerId,FirstName,LastName,TermStartDate,TermEndDate, ")
            .Append(" AttendanceStartDate,AttendanceEndDate,ModUser,ModDate)")
            .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)")
        End With
        db.AddParameter("@PrefReportId", System.Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@prefid", prefId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        If Not CampGrpId.ToString = "" Then
            db.AddParameter("@CampGrpId", CampGrpId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If Not TermId.ToString = "" Then
            db.AddParameter("@TermId", TermId.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TermId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not PrgVerId.ToString = "" Then
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@PrgVerId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not FirstName.ToString = "" Then
            db.AddParameter("@FirstName", FirstName.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@FirstName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not LastName.ToString = "" Then
            db.AddParameter("@LastName", LastName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@LastName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not TermStartDate.ToString = "1/1/1900" Then
            db.AddParameter("@TermStartDate", TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        Else
            db.AddParameter("@TermStartDate", "1/1/1900", DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        If Not TermEndDate.ToString = "1/1/1900" Then
            db.AddParameter("@TermEndDate", TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        Else
            db.AddParameter("@TermEndDate", "1/1/1900", DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        If Not AttendanceStartDate.ToString = "1/1/1900" Then
            db.AddParameter("@AttendanceStartDate", AttendanceStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        Else
            db.AddParameter("@AttendanceStartDate", "1/1/1900", DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        If Not AttendanceEndDate.ToString = "1/1/1900" Then
            db.AddParameter("@AttendanceEndDate", AttendanceEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        Else
            db.AddParameter("@AttendanceEndDate", "1/1/1900", DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        db.AddParameter("@ModUser", "sa", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As System.Exception
            Throw New BaseException("Error adding user preference basic info: " & ex.InnerException.Message)
        Finally
            db.Dispose()
            db = Nothing
        End Try
    End Sub
    Public Sub UpdateUserPrefForDailyCompletedHours(ByVal prefId As String, ByVal CampGrpId As String,
        ByVal TermId As String, ByVal PrgVerId As String, ByVal FirstName As String, ByVal LastName As String,
        ByVal TermStartDate As DateTime, ByVal TermEndDate As DateTime, ByVal AttendanceStartDate As DateTime,
        ByVal AttendanceEndDate As DateTime)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim sGuid As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append("Update tblPrefReport ")
            .Append("set CampGrpId=?,TermId=?,PrgVerId=?,FirstName=?,LastName=?,TermStartDate=?,TermEndDate=?, ")
            .Append(" AttendanceStartDate=?,AttendanceEndDate=?,ModUser=?,ModDate=?")
            .Append(" Where PrefId=? ")
        End With
        If Not CampGrpId.ToString = "" Then
            db.AddParameter("@CampGrpId", CampGrpId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If Not TermId.ToString = "" Then
            db.AddParameter("@TermId", TermId.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@TermId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not PrgVerId.ToString = "" Then
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@PrgVerId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not FirstName.ToString = "" Then
            db.AddParameter("@FirstName", FirstName.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@FirstName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not LastName.ToString = "" Then
            db.AddParameter("@LastName", LastName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@LastName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not TermStartDate.ToString = "1/1/1900" Then
            db.AddParameter("@TermStartDate", TermStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        Else
            db.AddParameter("@TermStartDate", "1/1/1900", DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        If Not TermEndDate.ToString = "1/1/1900" Then
            db.AddParameter("@TermEndDate", TermEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        Else
            db.AddParameter("@TermEndDate", "1/1/1900", DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        If Not AttendanceStartDate.ToString = "1/1/1900" Then
            db.AddParameter("@AttendanceStartDate", AttendanceStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        Else
            db.AddParameter("@AttendanceStartDate", "1/1/1900", DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        If Not AttendanceEndDate.ToString = "1/1/1900" Then
            db.AddParameter("@AttendanceEndDate", AttendanceEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        Else
            db.AddParameter("@AttendanceEndDate", "1/1/1900", DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End If
        db.AddParameter("@ModUser", "sa", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@prefid", prefId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        Catch ex As System.Exception
            Throw New BaseException("Error adding user preference basic info: " & ex.InnerException.Message)
        Finally
            db.Dispose()
            db = Nothing
        End Try
    End Sub
    Public Function GetUserRptPrefsForDailyCompletedHours(ByVal prefId As String) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            'This section deals with fetching the sort preferences. Note that
            'the caption will be retrieved in the ui. For now we store the
            'value 'Empty'.
            With sb
                .Append(" SELECT Distinct CampGrpId,termId,PrgVerId,FirstName,LastName,termstartdate, ")
                .Append(" termenddate,attendancestartdate,attendanceenddate ")
                .Append("FROM tblPrefReport ")
                .Append("WHERE PrefId = ? ")
            End With

            db.AddParameter("@prefid", prefId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "FilterPrefs")
            sb.Remove(0, sb.Length)

        Catch ex As System.Exception
            Throw New BaseException("Error retrieving preferences - " & ex.Message)
        Finally
            'Close connection.
            db.CloseConnection()
            db.Dispose()
            db = Nothing
        End Try

        Return ds

    End Function
    Public Sub DeleteUserRptPrefsForDailyCompletedHours(ByVal prefId As String)
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            'This section deals with fetching the sort preferences. Note that
            'the caption will be retrieved in the ui. For now we store the
            'value 'Empty'.
            With sb
                .Append(" Delete from  ")
                .Append(" tblPrefReport ")
                .Append("WHERE PrefId = ? ")
            End With

            db.AddParameter("@prefid", prefId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            sb.Remove(0, sb.Length)
            db.ClearParameters()

            With sb
                .Append(" Delete from  ")
                .Append(" syRptUserPrefs ")
                .Append("WHERE PrefId = ? ")
            End With

            db.AddParameter("@prefid", prefId.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            sb.Remove(0, sb.Length)
            db.ClearParameters()




        Catch ex As System.Exception
            Throw New BaseException("Error retrieving preferences - " & ex.Message)
        Finally
            'Close connection.
            db.CloseConnection()
            db.Dispose()
            db = Nothing
        End Try



    End Sub
    ''New Code Added By Vijay Ramteke On September 08, 2010 For Mantis Id 19639
    Public Function HasStudentGroupForScheduling(Optional ByVal CampusId As String = "") As Boolean
        '   connect to the database
        Dim intRecordCount As Integer = 0
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         Count(L.LeadGrpId)  ")
            .Append("FROM     adLeadGroups L, syStatuses ST ")
            .Append("WHERE    L.StatusId = ST.StatusId ")
            .Append(" and ST.Status='Active' ")
            .Append(" and L.UseForScheduling = 1 ")
            If Not CampusId = "" Then
                .Append("AND L.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = '" & CampusId & "') ")
            End If
        End With
        intRecordCount = db.RunParamSQLScalar(sb.ToString)
        If intRecordCount > 0 Then
            Return True
            Exit Function
        Else
            Return False
            Exit Function
        End If
        '   return dataset
        Return False
    End Function
    ''New Code Added By Vijay Ramteke On September 08, 2010 For Mantis Id 19639
#End Region

End Class

