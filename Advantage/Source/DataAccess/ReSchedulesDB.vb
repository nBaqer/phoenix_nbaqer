
Imports FAME.AdvantageV1.Common
Imports FAME.Advantage.Common


Public Class ReSchedulesDB
    Public Function GetStudentsList(ByVal progId As String, ByVal campusid As String) As DataTable
        '   connect to the database
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim strStuID As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier").ToUpper()
        If StudentIdentifier = "SSN" Then
            strStuID = "S.SSN AS SSN,"
        ElseIf StudentIdentifier = "ENROLLMENTID" Then
            strStuID = "E.EnrollmentId AS SSN,"
        ElseIf StudentIdentifier = "STUDENTID" Then
            strStuID = "S.StudentNumber AS SSN,"
        End If
        '   get lead statuses that map to 'New Lead' system status.
        '   build the sql query
        With sb
            .Append(" SELECT ")
            .Append(" E.StuEnrollId,S.LastName,S.FirstName," & strStuID)
            .Append("   P.ProgId,P.ProgDescrip,E.CohortStartDate as CurrentCohortStart,Code as CourseCode,R.TestId as ClsSectionId, ")
            .Append(" CS.Reqid,CS.EndDate, ")
            .Append(" (select min(arTerm.StartDate) from arClassSections,arTerm where  ")
            .Append(" arClassSections.TermId=arTerm.TermId and  ")
            .Append(" Reqid=CS.Reqid and  ")
            .Append(" arClassSections.StartDate > CS.EndDate  ")
            .Append(" and ProgId=P.ProgId) as NewCohortStart,E.PrgVerId,")
            .Append(" (select termId from arTerm where StartDate= ")
            .Append(" (select min(arTerm.StartDate) from arClassSections,arTerm where ")
            .Append(" arClassSections.TermId=arTerm.TermId and ")
            .Append(" Reqid=CS.Reqid and arClassSections.StartDate > CS.EndDate ")
            .Append(" and ProgId=P.ProgId) and ProgId=P.ProgId) as TermId ")
            .Append(" ,E.PrgVerId,PVD.ReqSeq,''  as ReqCode  ")
            .Append("            FROM   arStuEnrollments E,arStudent S,syStatusCodes A,syStatuses B,sySysStatus C,arPrgVersions PV,arPrograms P,arResults R, ")
            .Append(" arGradeSystemDetails G,arClassSections CS,arReqs Req,arProgVerDef PVD ")
            .Append(" WHERE E.StudentId = S.StudentId ")
            .Append(" AND E.StatusCodeId=A.StatusCodeId  ")
            .Append(" AND A.SysStatusId=C.SysStatusId  ")
            .Append(" AND C.SysStatusId IN (9,20)  ")
            .Append(" AND A.StatusId=B.StatusId  ")
            .Append(" AND B.Status='Active'  ")
            .Append(" and E.PrgVerId=PV.PrgVerId ")
            .Append(" and PV.ProgId=P.ProgId ")
            .Append(" and PV.PrgVerId=PVD.PrgVerId ")
            .Append(" and CS.Reqid=PVD.Reqid ")
            .Append(" and E.StuEnrollId=R.StuEnrollID  ")
            .Append(" and R.GrdSysDetailId=G.GrdSysDetailId and G.IsPass=0  and G.IsInComplete=0 and G.IsDrop=0 ")
            .Append(" and R.TestId=CS.ClsSectionId  ")
            .Append(" and CS.Reqid=Req.ReqId ")
            If progId <> "" Then
                .Append(" and P.ProgId= ? ")
            End If
            .Append(" AND E.CampusId= ?   ")
            .Append(" and Req.Reqid not in( ")
            .Append(" select Reqid from arResults,arClassSections ")
            .Append(" where  GrdSysDetailId is null and arResults.TestId=arClassSections.ClsSectionId and ")
            .Append(" stuEnrollId=E.stuEnrollId) ")
            .Append(" and R.TestId in ")
            .Append(" (Select top 1 a.Testid FROM arResults a, arGradeSystemDetails b  ")
            .Append(" WHERE a.StuEnrollId = E.StuEnrollId ")
            .Append(" and a.TestId in  ")
            .Append(" (Select c.ClsSectionId from arClassSections c where c.ReqId =  CS.Reqid) and ")
            .Append(" b.GrdSysDetailId = a.GrdSysDetailId   order by a.moddate desc ) order by E.StuEnrollId,CS.StartDate ")

        End With
        If progId <> "" Then
            db.AddParameter("@prgid", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@campid", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        '   return dataset
        Return ds.Tables(0)
    End Function



    Public Function ReScheduleStudentsForClasses(ByVal dtStudentClasses As DataTable, ByVal modUser As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dr As DataRow
        Dim result As String
        db.OpenConnection()
        Dim drClass As DataRow
        Dim dtClass As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try

            For Each dr In dtStudentClasses.Rows
                ReScheduleStudent(dr, modUser)

            Next





            Return ""
        Catch ex As Exception

            If ex.InnerException Is Nothing Then
                Return ex.Message
            Else
                Return ex.InnerException.Message
            End If
        Finally
            db.CloseConnection()
        End Try

        Return ""
    End Function

    Public Function ReScheduleStudent(ByVal dr As DataRow, ByVal modUser As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        'Dim dr As DataRow
        Dim result As String
        db.OpenConnection()
        Dim groupTrans As OleDbTransaction = db.StartTransaction()
        Dim drClass As DataRow
        Dim dtClass As DataTable
        Dim dtFailedClass As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try



            'dtClass = GetClassListForStudents(dr("TermId"), dr("prgVerId"), dr("ReqSeq"))
            DeleteStdFrmGrdBkResults(dr("StuEnrollId"))
            dtClass = GetClassListForStudents(dr("StuEnrollId"))

            For Each drClass In dtClass.Rows
                With sb
                    ''.Append("INSERT INTO arResults(StuEnrollId,TestId,ModUser,ModDate) ")
                    ''.Append("VALUES(?,?,?,?) ")
                    'If Not drClass("GrdSysDetailId") Is Nothing Then
                    '    .Append("INSERT INTO arResults(StuEnrollId,TestId,ModUser,ModDate) ")
                    '    .Append("VALUES(?,?,?,?) ")
                    'Else

                    .Append(" update arResults set TestId= (select ClsSectionId from arClassSections where ")
                    .Append(" TermId= ? ")
                    .Append(" and Reqid=(select Reqid from arClassSections where ClsSectionId= ? )) ")
                    .Append(" ,modUser= ? ,modDate= ? ")
                    .Append(" where stuEnrollId= ?  and TestId= ? and GrdSysDetailId is null ")
                    'End If
                End With

                Dim sDate As DateTime = Utilities.GetAdvantageDBDateTime(Date.Now)

                db.AddParameter("termId", dr("TermId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("testid", drClass("ClsSectionId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("user", modUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("date", sDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
                db.AddParameter("sid", dr("StuEnrollId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("testid", drClass("ClsSectionId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Next


            dtFailedClass = GetFailedRecordsTestid(dr)
            For Each drClass In dtFailedClass.Rows
                With sb
                    .Append("INSERT INTO arResults(StuEnrollId,TestId,ModUser,ModDate) ")
                    .Append("VALUES(?,?,?,?) ")

                End With

                Dim sDate As DateTime = Utilities.GetAdvantageDBDateTime(Date.Now)

                db.AddParameter("sid", dr("StuEnrollId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("testid", drClass("ClsSectionId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("user", modUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("date", sDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Next



            groupTrans.Commit()

            UpdateStudentEnrollment(dr)


            'dtClass = GetClassListForStudents(dr("TermId"), dr("prgVerId"), dr("ReqSeq"))
            dtClass = GetClassListForStudents(dr("StuEnrollId"))
            For Each drClass In dtClass.Rows
                'If SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "true" Or _
                'SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "yes" Then
                result = RegisterStudentForGradeBookComponents(dr("StuEnrollId").ToString, drClass("ClsSectionId").ToString, modUser, Date.Now)
                'End If
            Next

            Return ""
        Catch ex As Exception
            groupTrans.Rollback()
            If ex.InnerException Is Nothing Then
                Return ex.Message
            Else
                Return ex.InnerException.Message
            End If
        Finally
            db.CloseConnection()
        End Try

        Return ""
    End Function
    Private Function GetFailedRecordsTestid(ByVal dr As DataRow) As DataTable

        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" select ClsSectionId from arClassSections where TermId= ?  and Reqid in ( " & dr("ReqCode").ToString & ")")

        End With
        db.AddParameter("@termId", dr("TermId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

        '   get lead statuses that map to 'New Lead' system status.
        '   build the sql query



    End Function

    Private Function RegisterStudentForGradeBookComponents(ByVal StuEnrollId As String, ByVal ClsSectionId As String, ByVal user As String, ByVal rightNow As Date) As String
        Dim db As New DataAccess

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New System.Text.StringBuilder
            'Modified by Balaji on 04/03/2009 to fix issue 13655
            'When a student is registered for a class, records were inserted in to arResults table for all schools
            'For Ross Type School (defined by ShowRossOnlyTabs entry in web.config), in addition to inserting records in to arresults, records were inserted into arGrdBkResults table
            'It was realized later that for non-ross type schools, if student is registered in an Externship class then record needs to be inserted into 
            'arGrdBkResults table, otherwise records will not show up in Post Externship Attendance and Attendance cannot be posted for the externship hours attended by the student
            If MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "true" Or _
              MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "yes" Then
                With sb
                    .Append("insert into arGrdBkResults (GrdBkResultId, ClsSectionId, InstrGrdBkWgtDetailId, Comments, StuEnrollId, ModUser, ModDate) ")
                    .Append("select ")
                    .Append("		NewId() as GrdBkResultId,  ")
                    .Append("		CS.ClsSectionId, ")
                    .Append("		GBWD.InstrGrdBkWgtDetailId, ")
                    .Append("       GCT.Descrip, ")
                    .Append("		R.StuEnrollId, ")
                    .Append("		?, ")
                    .Append("		? ")
                    .Append("from	arResults R, arClassSections CS, arGrdBkWeights GBW, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT  ")
                    .Append("where	R.StuEnrollId=? ")
                    .Append("and		R.TestId=CS.ClsSectionId  ")
                    .Append("and		CS.ReqId=GBW.ReqId ")
                    .Append("and		GBW.InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId  ")
                    .Append("and		GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId  ")
                    .Append("and		GBW.EffectiveDate = (Select max(EffectiveDate) from arGrdBkWeights where ReqId=GBW.ReqId and EffectiveDate <= CS.StartDate) ")
                    .Append("and		not exists (select *  ")
                    .Append("					from arGrdBkResults  ")
                    .Append("					where ClsSectionId=CS.ClsSectionId ")
                    .Append("					and	  InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
                    .Append("					and	  StuEnrollId=?) ")
                    .Append("and    CS.ClsSectionId= ? ")
                End With
            Else
                With sb
                    .Append("insert into arGrdBkResults (GrdBkResultId, ClsSectionId, InstrGrdBkWgtDetailId, Comments, StuEnrollId, ModUser, ModDate) ")
                    .Append("select ")
                    .Append("		NewId() as GrdBkResultId,  ")
                    .Append("		CS.ClsSectionId, ")
                    .Append("		GBWD.InstrGrdBkWgtDetailId, ")
                    .Append("       GCT.Descrip, ")
                    .Append("		R.StuEnrollId, ")
                    .Append("		?, ")
                    .Append("		? ")
                    .Append("from	arResults R, arClassSections CS, arGrdBkWeights GBW, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT,arReqs RQ  ")
                    .Append("where	R.StuEnrollId=? ")
                    .Append("and		R.TestId=CS.ClsSectionId  ")
                    .Append("and		CS.ReqId=GBW.ReqId ")
                    .Append("and		GBW.InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId  ")
                    .Append("and		GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId  and CS.ReqId=RQ.ReqId and RQ.IsExternShip=1 ")
                    .Append("and		GBW.EffectiveDate = (Select max(EffectiveDate) from arGrdBkWeights where ReqId=GBW.ReqId and EffectiveDate <= CS.StartDate) ")
                    .Append("and		not exists (select *  ")
                    .Append("					from arGrdBkResults  ")
                    .Append("					where ClsSectionId=CS.ClsSectionId ")
                    .Append("					and	  InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
                    .Append("					and	  StuEnrollId=?) ")
                    .Append("and    CS.ClsSectionId= ? ")
                End With
            End If
            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'stuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'stuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'stuEnrollId
            db.AddParameter("@ClsSectionId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   commit transaction
            groupTrans.Commit()

            '   return with no errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            ''   do not report sql lost connection
            'If Not groupTrans.Connection Is Nothing Then
            '    '   report an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
            'End If

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    'Private Function GetClassListForStudents(ByVal termId As String, ByVal prgVerid As String, ByVal Reqseq As String) As DataTable
    Private Function GetClassListForStudents(ByVal stuEnrollId As String) As DataTable
        '   connect to the database
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   get lead statuses that map to 'New Lead' system status.
        '   build the sql query
        With sb
            '.Append(" select ClsSectionId from arClassSections where Termid =  ? ")
            '.Append(" and Reqid in(select Reqid from arProgVerDef ")
            '.Append(" where PrgVerId= ?  and ReqSeq >= ? ) ")

            '.Append(" select R.TestId as ClsSectionId,GrdSysDetailId from arResults R,arGradeSystemDetails G  where ")
            '.Append(" R.GrdSysDetailId=G.GrdSysDetailId and G.IsPass=0 and ")
            '.Append(" R.stuEnrollid= ? ")
            '.Append(" union ")
            .Append(" select R.TestId  as ClsSectionId,GrdSysDetailId from arResults R   where ")
            .Append(" R.stuEnrollid= ? ")
            .Append(" and R.GrdSysDetailId is null ")

        End With

        'db.AddParameter("@termId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'db.AddParameter("@prgVerid", prgVerid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@reqseq", Reqseq, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        '   return dataset
        Return ds.Tables(0)
    End Function

    Private Function GetOldClassListForStudents(ByVal stuEnrollId As String) As DataTable
        '   connect to the database
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   get lead statuses that map to 'New Lead' system status.
        '   build the sql query
        With sb
            .Append(" select * from arResults,arClassSections ")
            .Append(" where  GrdSysDetailId is null and arResults.TestId= arClassSections.ClsSectionId and ")
            .Append(" stuEnrollId= ? ")

        End With

        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)



        ds = db.RunParamSQLDataSet(sb.ToString)

        '   return dataset
        Return ds.Tables(0)
    End Function
    Private Function DeleteStdFrmGrdBkResults(ByVal StuEnrollId As String)

        Dim db As New DataAccess
        Dim sb As New StringBuilder
        '   we must encapsulate all DB updates in one transaction
        'Dim groupTrans As OleDbTransaction = db.StartTransaction()
        Dim dtClass As DataTable
        Dim drClass As DataRow
        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   delete fees
            'Dim result As String = (New TransactionsDB).DeleteAppliedFeesByCourse(StuEnrollId, storedGuid, user)

            'If Not result = "" Then
            '    Return result
            'End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")



            dtClass = GetOldClassListForStudents(StuEnrollId)
            For Each drClass In dtClass.Rows
                With sb
                    .Append("Delete from arGrdBkResults ")
                    .Append("WHERE ClsSectionId = ? ")
                    .Append("AND StuEnrollId = ? ; ")
                    '.Append("DELETE FROM arResults ")
                    '.Append("WHERE TestId = ? ")
                    '.Append("AND StuEnrollId = ? ;")

                End With


                db.AddParameter("@ClsSectionId", drClass("clsSectionId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'db.AddParameter("@ClsSectionId", drClass("clsSectionId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Next




            '   commit transaction 
            'groupTrans.Commit()

            ''   delete Gradebook Components
            'Dim result As String = DeleteGradeBookComponentsForAClassSection(StuEnrollId, clsSectionId, user)

            '   return with no errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            ' groupTrans.Rollback()

            '   do not report sql lost connection
            'If Not groupTrans.Connection Is Nothing Then
            '    ' Report the error
            'Else
            '    '   report an error to the client
            '    DALExceptions.BuildErrorMessage(ex)
            'End If

        Finally
            'Close Connection
            'db.CloseConnection()
        End Try

    End Function
    Private Function UpdateStudentEnrollment(ByVal drClass As DataRow)

        Dim db As New DataAccess
        Dim sb As New StringBuilder
        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        'Dim drClass As DataRow
        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   delete fees
            'Dim result As String = (New TransactionsDB).DeleteAppliedFeesByCourse(StuEnrollId, storedGuid, user)

            'If Not result = "" Then
            '    Return result
            'End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")





            With sb
                .Append(" update t1 set t1.CohortStartDate= ? ,t1.ExpGradDate=t2.EndDate from arStuEnrollments t1,arTerm t2 ")
                .Append(" where t1.StuEnrollId = ?  and t2.TermId =  ? ")

            End With


            db.AddParameter("@CohortStart", drClass("NewCohortStart").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", drClass("StuEnrollId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@termId", drClass("termId").ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
            db.ClearParameters()
            sb.Remove(0, sb.Length)





            '   commit transaction 
            groupTrans.Commit()

            ''   delete Gradebook Components
            'Dim result As String = DeleteGradeBookComponentsForAClassSection(StuEnrollId, clsSectionId, user)

            '   return with no errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            Else
                '   report an error to the client
                DALExceptions.BuildErrorMessage(ex)
            End If

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
End Class
