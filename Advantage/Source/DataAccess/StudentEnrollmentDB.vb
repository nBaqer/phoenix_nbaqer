Public Class StudentEnrollmentDB
    Public Sub UpdateStudent(ByVal StudentID As String, ByVal StuEnrollmentID As String)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("update arStuEnrollments set StudentID = ? WHERE StuEnrollID= ?")
        End With
        db.AddParameter("@StudentID", StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollmentID", StuEnrollmentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
    End Sub
    Public Function IsThisEnrollmentBeingUsed(ByVal StuEnrollmentID As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        With sb
            .Append("Select ")
            .Append("    	(select Count(*) from arResults where StuEnrollId=?) + ")
            .Append("   	(select Count(*) from arSAPChkResults where StuEnrollId= ? ) + ")
            .Append("       (select Count(*) from arTransferGrades where StuEnrollId= ? ) + ")
            .Append("       (select Count(*) from atClsSectAttendance where StuEnrollId= ? ) + ")
            .Append("       (select Count(*) from faStudentAwards where StuEnrollId= ? ) + ")
            .Append("       (select Count(*) from faStudentPaymentPlans where StuEnrollId= ? ) + ")
            .Append("       (select Count(*) from saTransactions where StuEnrollId= ? and saTransactions.Voided=0) ")
        End With

        db.AddParameter("@StuEnrollmentID", StuEnrollmentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollmentID", StuEnrollmentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollmentID", StuEnrollmentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollmentID", StuEnrollmentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollmentID", StuEnrollmentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollmentID", StuEnrollmentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollmentID", StuEnrollmentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return 
        Return CType(db.RunParamSQLScalar(sb.ToString), Boolean)

    End Function
    'Added By Vijay Ramteke on Feb 18, 2010
    Public Sub UpdateExpectedGraduationDate(ByVal ExpectedGraduationDate As DateTime, ByVal StuEnrollmentID As String)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("update arStuEnrollments set ExpGradDate = ? WHERE StuEnrollID= ?")
        End With
        db.AddParameter("@ExpGradDate", ExpectedGraduationDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollmentID", StuEnrollmentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
    End Sub
    'Added By Vijay Ramteke on Feb 18, 2010

End Class
