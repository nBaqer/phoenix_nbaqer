
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.DataAccess.FAME.DataAccessLayer

Public Class LeadDB
#Region "Get AdvAppsetting object"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If
        Return myAdvAppSettings
    End Function
#End Region

    Public Function AddStudentEducation(ByVal educationinfo As PlacementInfo, ByVal user As String) As String

        ''Connect To The Database
        Dim db As New DataAccess
        'Dim commonvalues As New AdvantageCommonValues

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try

            'If collgenotlisted is empty
            Dim strEducationInstId As String = Guid.NewGuid.ToString
            Dim strStatusId As String = AdvantageCommonValues.ActiveGuid

            If Not educationinfo.OtherCollegeNotListed = "" And educationinfo.EducationInstType = "College" Then
                'insert into adColleges
                Dim sb1 As New StringBuilder
                With sb1
                    .Append("Insert adColleges(CollegeId,CollegeCode,StatusId,CollegeName,ModUser,ModDate, CampGrpId ) ")
                    .Append(" Values(?,?,?,?,?,?,?) ")
                End With

                'CollegeId
                db.AddParameter("@CollegeId", strEducationInstId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                ' CollegeCode
                If educationinfo.OtherCollegeNotListed.ToString.Length < 7 Then
                    db.AddParameter("@CollegeCode", educationinfo.OtherCollegeNotListed.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CollegeCode", educationinfo.OtherCollegeNotListed.ToString.Substring(0, 7), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                'Mid(1, educationinfo.OtherCollegeNotListed.ToString, 8)
                ' StatusId
                db.AddParameter("@StatusId", strStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'EducationInstType
                db.AddParameter("@CollegeName", educationinfo.OtherCollegeNotListed, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                ''ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                ''ModDate
                db.AddParameter("@ModDate", educationinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                ''All campus Groups ID
                db.AddParameter("@CampGrpId", educationinfo.CampGrpid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


                ''Execute The Query
                db.RunParamSQLExecuteNoneQuery(sb1.ToString)
                db.ClearParameters()
                sb1.Remove(0, sb1.Length)

            ElseIf Not educationinfo.OtherCollegeNotListed = "" And educationinfo.EducationInstType = "Schools" Then
                'insert into syInstitutions
                Dim sb2 As New StringBuilder
                With sb2
                    .Append("Insert syInstitutions(HSId,HSCode,StatusId,HSName,ModUser,ModDate, CampGrpId) ")
                    .Append(" Values(?,?,?,?,?,?,?) ")
                End With

                'CollegeId
                db.AddParameter("@HSId", strEducationInstId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                ' HSCode
                If educationinfo.OtherCollegeNotListed.ToString.Length < 7 Then
                    db.AddParameter("@HSCode", educationinfo.OtherCollegeNotListed.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@HSCode", educationinfo.OtherCollegeNotListed.ToString.Substring(0, 7), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                ' StatusId
                db.AddParameter("@StatusId", strStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'EducationInstType
                db.AddParameter("@HSName", educationinfo.OtherCollegeNotListed, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                ''ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                ''ModDate
                db.AddParameter("@ModDate", educationinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                ''All campus Groups ID
                db.AddParameter("@CampGrpId", educationinfo.CampGrpid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


                ''Execute The Query
                db.RunParamSQLExecuteNoneQuery(sb2.ToString)
                db.ClearParameters()
                sb2.Remove(0, sb2.Length)
            End If


            '    'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Insert adLeadEducation(LeadEducationId,LeadId,EducationInstId, ")
                .Append(" EducationInstType, ")
                .Append(" GraduatedDate,FinalGrade,CertificateId,Comments,ModUser,ModDate,Major) ")
                .Append(" Values(?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            'Primary Key Value
            db.AddParameter("@StuEducationId", educationinfo.StEmploymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StudentId
            db.AddParameter("@StudentId", educationinfo.LeadID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ' EducationInstId
            If Not educationinfo.OtherCollegeNotListed = "" Then
                db.AddParameter("@EducationInstId", strEducationInstId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@EducationInstId", educationinfo.EducationInstId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'EducationInstType
            db.AddParameter("@EducationInstType", educationinfo.EducationInstType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Graduated Date
            If educationinfo.GraduationDate = "" Then
                db.AddParameter("@GraduatedDate", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@GraduatedDate", educationinfo.GraduationDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            'FinalGrade
            If educationinfo.FinalGrade = Guid.Empty.ToString Or educationinfo.FinalGrade = "" Then
                db.AddParameter("@FinalGrade", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@FinalGrade", educationinfo.FinalGrade, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Certificate
            If educationinfo.Certificate = Guid.Empty.ToString Or educationinfo.Certificate = "" Then
                db.AddParameter("@CertificateId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CertificateId", educationinfo.Certificate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Comments
            db.AddParameter("@Comments", educationinfo.Comments, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)

            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", educationinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'Major
            If educationinfo.Major = Guid.Empty.ToString Or educationinfo.Major = "" Then
                db.AddParameter("@Major", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Major", educationinfo.Major, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            ''Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            educationinfo.IsInDb = True

            'Retun Without Errors
            Return ""
        Catch ex As OleDbException
            'Return an Error To Client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    'Public Function AddLead(ByVal leadinfo As LeadMasterInfo, ByVal user As String, ByVal strObjective As String) As String
    '    ''Connect To The Database
    '    Dim db As New DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '    'Do an Update
    '    Try
    '        '    'Build The Query
    '        Dim sb As New StringBuilder
    '        Dim strStatus As String = "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965"

    '        With sb
    '            .Append("Insert into adLeads(LeadId,FirstName, ")
    '            .Append(" LastName,MiddleName,SSN,Phone,HomeEmail, ")
    '            .Append(" Address1,Address2,City,StateId,Zip,WorkEmail, ")
    '            .Append("AddressType,Prefix,Suffix,BirthDate,Sponsor,AdmissionsRep,Gender, ")
    '            .Append("Race,MaritalStatus,FamilyIncome,Children,PhoneType,SourceCategoryID,SourceTypeID,SourceDate, ")
    '            .Append("AreaID,ProgramID,PrgVerId,ExpectedStart,ShiftID,Nationality,Citizen,DrivLicStateID,DrivLicNumber,AlienNumber,Comments,ModUser,ModDate,SourceAdvertisement,AssignedDate,DateApplied,Country,County,LeadStatus,PreviousEducation,CampusId,CreatedDate,ForeignPhone,ForeignZip,OtherState,AddressStatus,PhoneStatus,LeadGrpId,DependencyTypeId,GeographicTypeId,AdminCriteriaId,InquiryTime,AdvertisementNote,HousingId) ")
    '            .Append(" Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
    '        End With

    '        'Add Parameters To Query

    '        'LeadId
    '        db.AddParameter("@LeadId", leadinfo.LeadMasterID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'First Name
    '        db.AddParameter("@FirstName", leadinfo.FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'Last Name
    '        db.AddParameter("@LastName", leadinfo.LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'MiddleName
    '        If leadinfo.MiddleName = "" Then
    '            db.AddParameter("@MiddleName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@MiddleName", leadinfo.MiddleName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'SSN
    '        If leadinfo.SSN = "" Then
    '            db.AddParameter("@SSN", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@SSN", leadinfo.SSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'HomePhone
    '        If leadinfo.Phone = "" Then
    '            db.AddParameter("@HomePhone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@HomePhone", leadinfo.Phone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If


    '        'HomeEmail
    '        If leadinfo.HomeEmail = "" Then
    '            db.AddParameter("@HomeEmail", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@HomeEmail", leadinfo.HomeEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'Address
    '        If leadinfo.Address1 = "" Then
    '            db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Address1", leadinfo.Address1, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'Address2
    '        If leadinfo.Address2 = "" Then
    '            db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Address2", leadinfo.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'city
    '        If leadinfo.City = "" Then
    '            db.AddParameter("@city", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@city", leadinfo.City, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'StateId
    '        If leadinfo.State = "" Then
    '            db.AddParameter("@State", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@State", leadinfo.State, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'Zip
    '        If leadinfo.Zip = "" Then
    '            db.AddParameter("@zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@zip", leadinfo.Zip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If


    '        'WorkEmail
    '        If leadinfo.WorkEmail = "" Then
    '            db.AddParameter("@WorkEmail", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@WorkEmail", leadinfo.WorkEmail, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'AddressType
    '        If leadinfo.AddressType = "" Then
    '            db.AddParameter("@AddressType", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AddressType", leadinfo.AddressType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Prefix
    '        If leadinfo.Prefix = "" Then
    '            db.AddParameter("@Prefix", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Prefix", leadinfo.Prefix, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'Suffix
    '        If leadinfo.Suffix = "" Then
    '            db.AddParameter("@Suffix", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Suffix", leadinfo.Suffix, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'BirthDate
    '        If leadinfo.BirthDate = "" Then
    '            db.AddParameter("@BirthDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@BirthDate", leadinfo.BirthDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If


    '        'Sponsor
    '        If leadinfo.Sponsor = "" Then
    '            db.AddParameter("@Sponsor", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Sponsor", leadinfo.Sponsor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'AdmissionsRep
    '        If leadinfo.AdmissionsRep = "" Then
    '            db.AddParameter("@AdmissionsRep", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AdmissionsRep", leadinfo.AdmissionsRep, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Gender
    '        If leadinfo.Gender = "" Then
    '            db.AddParameter("@Gender", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Gender", leadinfo.Gender, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'Race
    '        If leadinfo.Race = "" Then
    '            db.AddParameter("@Race", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Race", leadinfo.Race, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'MaritalStatus
    '        If leadinfo.MaritalStatus = "" Then
    '            db.AddParameter("@MaritalStatus", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@MaritalStatus", leadinfo.MaritalStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'FamilyIncome
    '        If leadinfo.FamilyIncome = "" Then
    '            db.AddParameter("@FamilyIncome", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@FamilyIncome", leadinfo.FamilyIncome, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Children
    '        If leadinfo.Children = "" Then
    '            db.AddParameter("@Children", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Children", leadinfo.Children, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'PhoneType
    '        If leadinfo.PhoneType = "" Then
    '            db.AddParameter("@PhoneType", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@PhoneType", leadinfo.PhoneType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'PhoneStatus
    '        'db.AddParameter("@PhoneStatus", strStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        'SourceCatagoryID
    '        If leadinfo.SourceCategory = "" Then
    '            db.AddParameter("@SourceCategoryID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@SourceCategoryID", leadinfo.SourceCategory, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'SourceTypeID
    '        If leadinfo.SourceType = "" Then
    '            db.AddParameter("@SourceTypeID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@SourceTypeID", leadinfo.SourceType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'SourceDate
    '        If leadinfo.SourceDate = "" Then
    '            db.AddParameter("@SourceDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@SourceDate", leadinfo.SourceDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'AreaID
    '        If leadinfo.Area = "" Then
    '            db.AddParameter("@AreaID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AreaID", leadinfo.Area, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If


    '        'ProgramId
    '        If leadinfo.ProgramID = "" Then
    '            db.AddParameter("@ProgramID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@ProgramID", leadinfo.ProgramID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'PrgVerId
    '        If leadinfo.PrgVerId = "" Then
    '            db.AddParameter("@PrgVerID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@PrgVerID", leadinfo.PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If


    '        'ExpectedStart
    '        If leadinfo.ExpectedStart = "" Then
    '            db.AddParameter("@ExpectedStart", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@ExpectedStart", leadinfo.ExpectedStart, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'ShiftID
    '        If leadinfo.ShiftID = "" Then
    '            db.AddParameter("@ShiftID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@ShiftID", leadinfo.ShiftID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'Nationality
    '        If leadinfo.Nationality = "" Then
    '            db.AddParameter("@Nationality", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Nationality", leadinfo.Nationality, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'Citizen
    '        If leadinfo.Citizen = "" Then
    '            db.AddParameter("@Citizen", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Citizen", leadinfo.Citizen, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'DrivLicStateID
    '        If leadinfo.DriverLicState = "" Then
    '            db.AddParameter("@DrivLicStateID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@DrivLicStateID", leadinfo.DriverLicState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'DriverLicNumber
    '        If leadinfo.DriverLicNumber = "" Then
    '            db.AddParameter("@DrivLicNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@DrivLicNumber", leadinfo.DriverLicNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'AlienNumber
    '        If leadinfo.AlienNumber = "" Then
    '            db.AddParameter("@AlienNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AlienNumber", leadinfo.AlienNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Comments
    '        If leadinfo.Notes = "" Then
    '            db.AddParameter("@Comments", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Comments", leadinfo.Notes, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        ''ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        ''ModDate
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        'SourceAdvertisement
    '        If leadinfo.SourceAdvertisement = "" Then
    '            db.AddParameter("@SourceAdvertisement", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@SourceAdvertisement", leadinfo.SourceAdvertisement, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Assigned Date
    '        If leadinfo.AssignmentDate = "" Then
    '            db.AddParameter("@AssignedDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AssignedDate", leadinfo.AssignmentDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Applied Date
    '        If leadinfo.AppliedDate = "" Then
    '            db.AddParameter("@AppliedDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AppliedDate", leadinfo.AppliedDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Country
    '        If leadinfo.Country = "" Then
    '            db.AddParameter("@Country", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Country", leadinfo.Country, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'County
    '        If leadinfo.County = "" Then
    '            db.AddParameter("@County", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@County", leadinfo.County, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'LeadStatus
    '        If leadinfo.Status = "" Then
    '            db.AddParameter("@LeadStatus", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@LeadStatus", leadinfo.Status, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'Previous Education
    '        If leadinfo.PreviousEducation = "" Then
    '            db.AddParameter("@PreviousEducation", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@PreviousEducation", leadinfo.PreviousEducation, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Address Status
    '        ' db.AddParameter("@AddressStatus", leadinfo.AddressStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        'db.AddParameter("@AddressStatus", strStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        'CampusId
    '        If leadinfo.CampusId = Guid.Empty.ToString Or leadinfo.CampusId = "" Then
    '            db.AddParameter("@CampusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@CampusId", leadinfo.CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Created Date
    '        db.AddParameter("@CreatedDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)


    '        'ForeignPhone
    '        db.AddParameter("@ForeignPhone", leadinfo.ForeignPhone, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

    '        'Foreign Zip
    '        db.AddParameter("@ForeignZip", leadinfo.ForeignZip, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

    '        'OtherState
    '        If leadinfo.OtherState = "" Then
    '            db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@OtherState", leadinfo.OtherState, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'Address Status
    '        If leadinfo.AddressStatus = "" Then
    '            db.AddParameter("@AddressStatus", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AddressStatus", leadinfo.AddressStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Phone Status
    '        If leadinfo.PhoneStatus = "" Then
    '            db.AddParameter("@PhoneStatus", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@PhoneStatus", leadinfo.PhoneStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        If leadinfo.LeadGrpId = "" Then
    '            db.AddParameter("@LeadGrpID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@LeadGrpId", leadinfo.LeadGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'DependencyTypeId
    '        If leadinfo.DependencyTypeId = "" Then
    '            db.AddParameter("@DependencyTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@DependencyTypeId", leadinfo.DependencyTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'GeographicTypeId
    '        If leadinfo.GeographicTypeId = "" Then
    '            db.AddParameter("@GeographicTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@GeographicTypeId", leadinfo.GeographicTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'AdminCriteriaId
    '        If leadinfo.AdminCriteriaId = "" Then
    '            db.AddParameter("@AdminCriteriaId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AdminCriteriaId", leadinfo.AdminCriteriaId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        If leadinfo.InquiryTime = "" Then
    '            db.AddParameter("@InquiryTime", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@InquiryTime", leadinfo.InquiryTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        If leadinfo.AdvertisementNote = "" Then
    '            db.AddParameter("@AdvertisementNote", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AdvertisementNote", leadinfo.AdvertisementNote, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'HousingTypeId
    '        If leadinfo.HousingTypeId = "" Then
    '            db.AddParameter("@HousingId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@HousingId", leadinfo.HousingTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Execute The Query
    '        Try
    '            db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '            'Return "The lead " & leadinfo.FirstName & " " & leadinfo.LastName & " was added successfully"
    '            Return ""
    '        Catch ex As OleDbException
    '            Return DALExceptions.BuildErrorMessage(ex)
    '        Finally
    '            db.ClearParameters()
    '            sb.Remove(0, sb.Length)
    '            db.CloseConnection()
    '        End Try

    '        Return ""
    '        'Catch ex As System.Exception
    '        '    'Return an Error To Client
    '        '    Return -1
    '    Catch ex As OleDbException
    '        '   return an error to the client
    '        Return ex.Message
    '    Finally
    '        'Close Connection
    '        'db.CloseConnection()
    '    End Try
    'End Function
    'Public Function UpdateLead(ByVal leadinfo As LeadMasterInfo, ByVal user As String, ByVal strObjective As String) As String
    '    ''Connect To The Database
    '    Dim db As New DataAccess
    '    Dim strStatus As String = "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965"

    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '    'Do an Update
    '    Try
    '        'Build The Query
    '        Dim sb As New StringBuilder
    '        With sb
    '            .Append("Update adLeads set FirstName=?, ")
    '            .Append(" LastName=?,MiddleName=?,SSN=?,Phone=?,HomeEmail=?, ")
    '            .Append(" Address1=?,Address2=?,City=?,StateId=?,Zip=?, ")
    '            .Append(" WorkEmail=?,AddressType=?,Prefix=?,Suffix=?,BirthDate=?, ")
    '            .Append(" Sponsor=?,AdmissionsRep=?,Gender=?,Race=?, ")
    '            .Append(" MaritalStatus=?,FamilyIncome=?,Children=?,PhoneType=?, ")
    '            .Append(" SourceCategoryID=?,SourceTypeID=?,SourceDate=?, ")
    '            .Append(" AreaID=?,ProgramID=?,PrgVerId=?,ExpectedStart=?,ShiftID=?, ")
    '            .Append(" Nationality=?,Citizen=?,DrivLicStateID=?,DrivLicNumber=?,AlienNumber=?, ")
    '            .Append(" Comments=?,ModUser=?,ModDate=?, SourceAdvertisement=?,AssignedDate=?, ")
    '            .Append(" DateApplied=?,Country=?,County=?,LeadStatus=?,PreviousEducation=?, ")
    '            .Append(" ForeignPhone=?,ForeignZip=?,OtherState=?,AddressStatus=?,PhoneStatus=?,LeadGrpId=?, ")
    '            .Append(" DependencyTypeId=?,GeographicTypeId=?,AdminCriteriaId=?,InquiryTime=?,AdvertisementNote=?,HousingId=? ")
    '            .Append(" where LeadID = ? ")
    '        End With

    '        'First Name
    '        db.AddParameter("@FirstName", leadinfo.FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'Last Name
    '        db.AddParameter("@LastName", leadinfo.LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'MiddleName
    '        If leadinfo.MiddleName = "" Then
    '            db.AddParameter("@MiddleName", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@MiddleName", leadinfo.MiddleName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'SSN
    '        If leadinfo.SSN = "" Then
    '            db.AddParameter("@SSN", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@SSN", leadinfo.SSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'HomePhone
    '        If leadinfo.Phone = "" Then
    '            db.AddParameter("@HomePhone", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@HomePhone", leadinfo.Phone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If


    '        'HomeEmail
    '        If leadinfo.HomeEmail = "" Then
    '            db.AddParameter("@HomeEmail", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@HomeEmail", leadinfo.HomeEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'Address
    '        If leadinfo.Address1 = "" Then
    '            db.AddParameter("@Address1", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Address1", leadinfo.Address1, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'Address2
    '        If leadinfo.Address2 = "" Then
    '            db.AddParameter("@Address2", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Address2", leadinfo.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'city
    '        If leadinfo.City = "" Then
    '            db.AddParameter("@city", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@city", leadinfo.City, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'StateId
    '        If leadinfo.State = "" Then
    '            db.AddParameter("@State", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@State", leadinfo.State, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'Zip
    '        If leadinfo.Zip = "" Then
    '            db.AddParameter("@zip", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@zip", leadinfo.Zip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If


    '        'WorkEmail
    '        If leadinfo.WorkEmail = "" Then
    '            db.AddParameter("@WorkEmail", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@WorkEmail", leadinfo.WorkEmail, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'AddressType
    '        If leadinfo.AddressType = "" Then
    '            db.AddParameter("@AddressType", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AddressType", leadinfo.AddressType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Prefix
    '        If leadinfo.Prefix = "" Then
    '            db.AddParameter("@Prefix", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Prefix", leadinfo.Prefix, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'Suffix
    '        If leadinfo.Suffix = "" Then
    '            db.AddParameter("@Suffix", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Suffix", leadinfo.Suffix, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'BirthDate
    '        If leadinfo.BirthDate = "" Then
    '            db.AddParameter("@BirthDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@BirthDate", leadinfo.BirthDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If



    '        'Sponsor
    '        If leadinfo.Sponsor = "" Then
    '            db.AddParameter("@Sponsor", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Sponsor", leadinfo.Sponsor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'AdmissionsRep
    '        If leadinfo.AdmissionsRep = "" Then
    '            db.AddParameter("@AdmissionsRep", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AdmissionsRep", leadinfo.AdmissionsRep, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Gender
    '        If leadinfo.Gender = "" Then
    '            db.AddParameter("@Gender", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Gender", leadinfo.Gender, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'Race
    '        If leadinfo.Race = "" Then
    '            db.AddParameter("@Race", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Race", leadinfo.Race, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'MaritalStatus
    '        If leadinfo.MaritalStatus = "" Then
    '            db.AddParameter("@MaritalStatus", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@MaritalStatus", leadinfo.MaritalStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'FamilyIncome
    '        If leadinfo.FamilyIncome = "" Then
    '            db.AddParameter("@FamilyIncome", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@FamilyIncome", leadinfo.FamilyIncome, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Children
    '        If leadinfo.Children = "" Then
    '            db.AddParameter("@Children", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Children", leadinfo.Children, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'PhoneType
    '        If leadinfo.PhoneType = "" Then
    '            db.AddParameter("@PhoneType", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@PhoneType", leadinfo.PhoneType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'SourceCatagoryID
    '        If leadinfo.SourceCategory = "" Then
    '            db.AddParameter("@SourceCategoryID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@SourceCategoryID", leadinfo.SourceCategory, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'SourceTypeID
    '        If leadinfo.SourceType = "" Then
    '            db.AddParameter("@SourceTypeID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@SourceTypeID", leadinfo.SourceType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'SourceDate
    '        If leadinfo.SourceDate = "" Then
    '            db.AddParameter("@SourceDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@SourceDate", leadinfo.SourceDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'AreaID
    '        If leadinfo.Area = "" Then
    '            db.AddParameter("@AreaID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AreaID", leadinfo.Area, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If


    '        'ProgramId
    '        If leadinfo.ProgramID = "" Then
    '            db.AddParameter("@ProgramID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@ProgramID", leadinfo.ProgramID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'PrgVerId
    '        If leadinfo.PrgVerId = "" Then
    '            db.AddParameter("@PrgVerID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@PrgVerID", leadinfo.PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If


    '        'ExpectedStart
    '        If leadinfo.ExpectedStart = "" Then
    '            db.AddParameter("@ExpectedStart", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@ExpectedStart", leadinfo.ExpectedStart, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'ShiftID
    '        If leadinfo.ShiftID = "" Then
    '            db.AddParameter("@ShiftID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@ShiftID", leadinfo.ShiftID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'Nationality
    '        If leadinfo.Nationality = "" Then
    '            db.AddParameter("@Nationality", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Nationality", leadinfo.Nationality, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'Citizen
    '        If leadinfo.Citizen = "" Then
    '            db.AddParameter("@Citizen", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Citizen", leadinfo.Citizen, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'DrivLicStateID
    '        If leadinfo.DriverLicState = "" Then
    '            db.AddParameter("@DrivLicStateID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@DrivLicStateID", leadinfo.DriverLicState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'DriverLicNumber
    '        If leadinfo.DriverLicNumber = "" Then
    '            db.AddParameter("@DrivLicNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@DrivLicNumber", leadinfo.DriverLicNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'AlienNumber
    '        If leadinfo.AlienNumber = "" Then
    '            db.AddParameter("@AlienNumber", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AlienNumber", leadinfo.AlienNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Comments
    '        If leadinfo.Notes = "" Then
    '            db.AddParameter("@Comments", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Comments", leadinfo.Notes, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        ''ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        ''ModDate
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        'SourceAdvertisement
    '        If leadinfo.SourceAdvertisement = "" Then
    '            db.AddParameter("@SourceAdvertisement", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@SourceAdvertisement", leadinfo.SourceAdvertisement, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Assigned Date
    '        If leadinfo.AssignmentDate = "" Then
    '            db.AddParameter("@AssignedDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AssignedDate", leadinfo.AssignmentDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Applied Date
    '        If leadinfo.AppliedDate = "" Then
    '            db.AddParameter("@AppliedDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AppliedDate", leadinfo.AppliedDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Country
    '        If leadinfo.Country = "" Then
    '            db.AddParameter("@Country", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Country", leadinfo.Country, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'County
    '        If leadinfo.County = "" Then
    '            db.AddParameter("@County", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@County", leadinfo.County, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'LeadStatus
    '        If leadinfo.Status = "" Then
    '            db.AddParameter("@LeadStatus", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@LeadStatus", leadinfo.Status, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'Previous Education
    '        If leadinfo.PreviousEducation = "" Then
    '            db.AddParameter("@PreviousEducation", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@PreviousEducation", leadinfo.PreviousEducation, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'ForeignPhone
    '        db.AddParameter("@ForeignPhone", leadinfo.ForeignPhone, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

    '        'Foreign Zip
    '        db.AddParameter("@ForeignZip", leadinfo.ForeignZip, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

    '        'OtherState
    '        If leadinfo.OtherState = "" Then
    '            db.AddParameter("@OtherState", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@OtherState", leadinfo.OtherState, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'Address Status
    '        If leadinfo.AddressStatus = "" Then
    '            db.AddParameter("@AddressStatus", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AddressStatus", leadinfo.AddressStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Phone Status
    '        If leadinfo.PhoneStatus = "" Then
    '            db.AddParameter("@PhoneStatus", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@PhoneStatus", leadinfo.PhoneStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Lead Group Id
    '        If leadinfo.LeadGrpId = "" Then
    '            db.AddParameter("@LeadGrpID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@LeadGrpId", leadinfo.LeadGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'DependencyTypeId
    '        If leadinfo.DependencyTypeId = "" Then
    '            db.AddParameter("@DependencyTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@DependencyTypeId", leadinfo.DependencyTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'GeographicTypeId
    '        If leadinfo.GeographicTypeId = "" Then
    '            db.AddParameter("@GeographicTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@GeographicTypeId", leadinfo.GeographicTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'AdminCriteriaId
    '        If leadinfo.AdminCriteriaId = "" Then
    '            db.AddParameter("@AdminCriteriaId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AdminCriteriaId", leadinfo.AdminCriteriaId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        If leadinfo.InquiryTime = "" Then
    '            db.AddParameter("@InquiryTime", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@InquiryTime", leadinfo.InquiryTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'Advertisement Note
    '        If leadinfo.AdvertisementNote = "" Then
    '            db.AddParameter("@AdvertisementNote", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@AdvertisementNote", leadinfo.AdvertisementNote, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'HousingTypeId
    '        If leadinfo.HousingTypeId = "" Then
    '            db.AddParameter("@HousingId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@HousingId", leadinfo.HousingTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If


    '        'LeadID
    '        db.AddParameter("@LeadID", leadinfo.LeadMasterID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        Try
    '            db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '            'Return "The modifications made to lead " & leadinfo.FirstName & " " & leadinfo.LastName & " was successfully saved"
    '            Return ""
    '        Catch ex As OleDbException
    '            '   return an error to the client
    '            Return DALExceptions.BuildErrorMessage(ex)
    '        Finally
    '            db.ClearParameters()
    '            sb.Remove(0, sb.Length)
    '            db.CloseConnection()
    '        End Try

    '    Catch ex As OleDbException
    '        '   return an error to the client
    '        Return ex.Message
    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    '    'Catch e As System.Exception
    '    'Finally
    '    'Close Connection

    'End Function

    Public Function AddLead(ByVal leadinfo As LeadMasterInfo, ByVal user As String, ByVal strObjective As String, Optional ByVal useRegent As Boolean = False) As String
        ''Connect To The Database
        Dim db As New DataAccess



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim db1 As New DataAccess
        db1.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            '    'Build The Query
            Dim sb As New StringBuilder
            ' Dim strStatus As String = "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965"

            With sb
                .Append("Insert into adLeads(LeadId,FirstName, ")
                .Append(" LastName,MiddleName,SSN,Phone,HomeEmail, ")
                .Append(" Address1,Address2,City,StateId,Zip,WorkEmail, ")
                .Append("AddressType,Prefix,Suffix,BirthDate,Sponsor,AdmissionsRep,Gender, ")
                .Append("Race,MaritalStatus,FamilyIncome,Children,PhoneType,SourceCategoryID,SourceTypeID,SourceDate, ")
                .Append("AreaID,ProgramID,PrgVerId,ExpectedStart,ShiftID,Nationality,Citizen,DrivLicStateID,DrivLicNumber,AlienNumber,Comments,ModUser,ModDate,SourceAdvertisement,AssignedDate,DateApplied,Country,County,LeadStatus,PreviousEducation,CampusId,CreatedDate,ForeignPhone,ForeignZip,OtherState,AddressStatus,PhoneStatus,LeadGrpId,DependencyTypeId,GeographicTypeId,AdminCriteriaId,InquiryTime,AdvertisementNote,HousingId,Phone2,PhoneType2,PhoneStatus2,ForeignPhone2,DefaultPhone,DegCertSeekingId")
                If useRegent = True Then
                    .Append(",entranceinterviewdate,highschoolprogramcode")
                End If
                .Append(" ) ")
                .Append(" Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ")
                If useRegent = True Then
                    .Append(",?,?")
                End If
                .Append(" ) ")
            End With

            'Add Parameters To Query

            'LeadId
            db.AddParameter("@LeadId", leadinfo.LeadMasterID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'First Name
            db.AddParameter("@FirstName", leadinfo.FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Last Name
            db.AddParameter("@LastName", leadinfo.LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'MiddleName
            If leadinfo.MiddleName = "" Then
                db.AddParameter("@MiddleName", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@MiddleName", leadinfo.MiddleName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'SSN
            If leadinfo.SSN = "" Then
                db.AddParameter("@SSN", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SSN", leadinfo.SSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'HomePhone
            If leadinfo.Phone = "" Then
                db.AddParameter("@HomePhone", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HomePhone", leadinfo.Phone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'HomeEmail
            If leadinfo.HomeEmail = "" Then
                db.AddParameter("@HomeEmail", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HomeEmail", leadinfo.HomeEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Address
            If leadinfo.Address1 = "" Then
                db.AddParameter("@Address1", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", leadinfo.Address1, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Address2
            If leadinfo.Address2 = "" Then
                db.AddParameter("@Address2", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", leadinfo.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'city
            If leadinfo.City = "" Then
                db.AddParameter("@city", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@city", leadinfo.City, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'StateId
            If leadinfo.State = "" Then
                db.AddParameter("@State", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@State", leadinfo.State, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Zip
            If leadinfo.Zip = "" Then
                db.AddParameter("@zip", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@zip", leadinfo.Zip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'WorkEmail
            If leadinfo.WorkEmail = "" Then
                db.AddParameter("@WorkEmail", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@WorkEmail", leadinfo.WorkEmail, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'AddressType
            If leadinfo.AddressType = "" Then
                db.AddParameter("@AddressType", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AddressType", leadinfo.AddressType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Prefix
            If leadinfo.Prefix = "" Then
                db.AddParameter("@Prefix", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Prefix", leadinfo.Prefix, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'Suffix
            If leadinfo.Suffix = "" Then
                db.AddParameter("@Suffix", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Suffix", leadinfo.Suffix, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'BirthDate
            If leadinfo.BirthDate = "" Then
                db.AddParameter("@BirthDate", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@BirthDate", leadinfo.BirthDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If


            'Sponsor
            If leadinfo.Sponsor = "" Then
                db.AddParameter("@Sponsor", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Sponsor", leadinfo.Sponsor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'AdmissionsRep
            If leadinfo.AdmissionsRep = "" Then
                db.AddParameter("@AdmissionsRep", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AdmissionsRep", leadinfo.AdmissionsRep, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Gender
            If leadinfo.Gender = "" Then
                db.AddParameter("@Gender", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Gender", leadinfo.Gender, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'Race
            If leadinfo.Race = "" Then
                db.AddParameter("@Race", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Race", leadinfo.Race, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'MaritalStatus
            If leadinfo.MaritalStatus = "" Then
                db.AddParameter("@MaritalStatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@MaritalStatus", leadinfo.MaritalStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'FamilyIncome
            If String.IsNullOrEmpty(leadinfo.FamilyIncome) Or leadinfo.FamilyIncome = Guid.Empty.ToString() Then
                db.AddParameter("@FamilyIncome", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@FamilyIncome", leadinfo.FamilyIncome, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Children
            If leadinfo.Children = "" Then
                db.AddParameter("@Children", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Children", leadinfo.Children, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'PhoneType
            If leadinfo.PhoneType = "" Then
                db.AddParameter("@PhoneType", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PhoneType", leadinfo.PhoneType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'PhoneStatus
            'db.AddParameter("@PhoneStatus", strStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'SourceCatagoryID
            If leadinfo.SourceCategory = "" Then
                db.AddParameter("@SourceCategoryID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@SourceCategoryID", leadinfo.SourceCategory, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'SourceTypeID
            If leadinfo.SourceType = "" Then
                db.AddParameter("@SourceTypeID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@SourceTypeID", leadinfo.SourceType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'SourceDate
            If leadinfo.SourceDate = "" Then
                db.AddParameter("@SourceDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@SourceDate", leadinfo.SourceDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'AreaID
            If leadinfo.Area = "" Then
                db.AddParameter("@AreaID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@AreaID", leadinfo.Area, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'ProgramId
            If leadinfo.ProgramID = "" Then
                db.AddParameter("@ProgramID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ProgramID", leadinfo.ProgramID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'PrgVerId
            If leadinfo.PrgVerId = "" Then
                db.AddParameter("@PrgVerID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PrgVerID", leadinfo.PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'ExpectedStart
            If leadinfo.ExpectedStart = "" Then
                db.AddParameter("@ExpectedStart", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ExpectedStart", leadinfo.ExpectedStart, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ShiftID
            If leadinfo.ShiftID = "" Then
                db.AddParameter("@ShiftID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@ShiftID", leadinfo.ShiftID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'Nationality
            If leadinfo.Nationality = "" Then
                db.AddParameter("@Nationality", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Nationality", leadinfo.Nationality, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'Citizen
            If leadinfo.Citizen = "" Then
                db.AddParameter("@Citizen", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Citizen", leadinfo.Citizen, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'DrivLicStateID
            If leadinfo.DriverLicState = "" Then
                db.AddParameter("@DrivLicStateID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DrivLicStateID", leadinfo.DriverLicState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'DriverLicNumber
            If leadinfo.DriverLicNumber = "" Then
                db.AddParameter("@DrivLicNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DrivLicNumber", leadinfo.DriverLicNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'AlienNumber
            If leadinfo.AlienNumber = "" Then
                db.AddParameter("@AlienNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AlienNumber", leadinfo.AlienNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Comments
            If leadinfo.Notes = "" Then
                db.AddParameter("@Comments", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Comments", leadinfo.Notes, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'SourceAdvertisement
            If leadinfo.SourceAdvertisement = "" Then
                db.AddParameter("@SourceAdvertisement", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@SourceAdvertisement", leadinfo.SourceAdvertisement, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Assigned Date
            If leadinfo.AssignmentDate = "" Then
                db.AddParameter("@AssignedDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AssignedDate", leadinfo.AssignmentDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Applied Date
            If leadinfo.AppliedDate = "" Then
                db.AddParameter("@AppliedDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AppliedDate", leadinfo.AppliedDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Country
            If leadinfo.Country = "" Then
                db.AddParameter("@Country", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Country", leadinfo.Country, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'County
            If leadinfo.County = "" Then
                db.AddParameter("@County", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@County", leadinfo.County, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'LeadStatus
            If leadinfo.Status = "" Then
                db.AddParameter("@LeadStatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@LeadStatus", leadinfo.Status, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'Previous Education
            If leadinfo.PreviousEducation = "" Then
                db.AddParameter("@PreviousEducation", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PreviousEducation", leadinfo.PreviousEducation, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Address Status
            ' db.AddParameter("@AddressStatus", leadinfo.AddressStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'db.AddParameter("@AddressStatus", strStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'CampusId
            If leadinfo.CampusId = Guid.Empty.ToString Or leadinfo.CampusId = "" Then
                db.AddParameter("@CampusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CampusId", leadinfo.CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Created Date
            db.AddParameter("@CreatedDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)


            'ForeignPhone
            db.AddParameter("@ForeignPhone", leadinfo.ForeignPhone, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'Foreign Zip
            db.AddParameter("@ForeignZip", leadinfo.ForeignZip, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'OtherState
            If leadinfo.OtherState = "" Then
                db.AddParameter("@OtherState", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", leadinfo.OtherState, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Address Status
            If leadinfo.AddressStatus = "" Then
                db.AddParameter("@AddressStatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AddressStatus", leadinfo.AddressStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Phone Status
            If leadinfo.PhoneStatus = "" Then
                db.AddParameter("@PhoneStatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PhoneStatus", leadinfo.PhoneStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            If leadinfo.LeadGrpId = "" Then
                db.AddParameter("@LeadGrpID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@LeadGrpId", leadinfo.LeadGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'DependencyTypeId
            If leadinfo.DependencyTypeId = "" Then
                db.AddParameter("@DependencyTypeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DependencyTypeId", leadinfo.DependencyTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'GeographicTypeId
            If leadinfo.GeographicTypeId = "" Then
                db.AddParameter("@GeographicTypeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@GeographicTypeId", leadinfo.GeographicTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'AdminCriteriaId
            If leadinfo.AdminCriteriaId = "" Then
                db.AddParameter("@AdminCriteriaId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AdminCriteriaId", leadinfo.AdminCriteriaId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            If leadinfo.InquiryTime = "" Then
                db.AddParameter("@InquiryTime", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@InquiryTime", leadinfo.InquiryTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            If leadinfo.AdvertisementNote = "" Then
                db.AddParameter("@AdvertisementNote", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AdvertisementNote", leadinfo.AdvertisementNote, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'HousingTypeId
            If leadinfo.HousingTypeId = "" Then
                db.AddParameter("@HousingId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@HousingId", leadinfo.HousingTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            If leadinfo.Phone2 = "" Then
                db.AddParameter("@HomePhone2", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HomePhone2", leadinfo.Phone2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'PhoneType
            If leadinfo.PhoneType2 = "" Then
                db.AddParameter("@PhoneType2", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PhoneType2", leadinfo.PhoneType2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'Phone Status
            If leadinfo.PhoneStatus2 = "" Then
                db.AddParameter("@PhoneStatus2", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PhoneStatus2", leadinfo.PhoneStatus2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            db.AddParameter("@ForeignPhone2", leadinfo.ForeignPhone2, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@DefaultPhone", leadinfo.DefaultPhone, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            If leadinfo.DegCertSeekingId = "" Then
                db.AddParameter("@DegCertSeekingId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DegCertSeekingId", leadinfo.DegCertSeekingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'If useRegent = True Then
            '    If Not leadinfo.EntranceInterviewDate = "01/01/1900" Then
            '        db.AddParameter("@EntranceInterviewDate", leadinfo.EntranceInterviewDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    Else
            '        db.AddParameter("@EntranceInterviewDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    End If
            '    If leadinfo.HighSchoolProgramCode = "" Then
            '        db.AddParameter("@HighSchoolProgramCode", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    Else
            '        db.AddParameter("@HighSchoolProgramCode", leadinfo.HighSchoolProgramCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    End If
            'End If

            'Execute The Query
            ''18036: QA: The lead conversions total report is not picking up the new lead status.
            ''Added by SAraswathi Lakshmanan on Nov 17 2009
            ''Build the query for StatusChangesTable
            ''When ever a new lead is added an entry is inserted into the LeadStatusesChanges table with orig Status as NUll and New Status as the entered status
            Dim sb1 As New StringBuilder
            With sb1
                .Append("INSERT INTO syLeadStatusesChanges")
                .Append("    (StatusChangeId,LeadId,OrigStatusId,NewStatusId,ModUser,ModDate) ")
                .Append("VALUES(newId(),?,?,?,?,?)")
            End With

            '   Add parameters values to the query
            '   StatusChangeId
            'db1.AddParameter("@StatusChangeId", ,DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   LeadId
            db1.AddParameter("@LeadId", leadinfo.LeadMasterID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   OrigStatusId
            db1.AddParameter("@OrigStatusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            '   NewStatusId
            db1.AddParameter("@NewStatusId", leadinfo.Status, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModUser
            db1.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db1.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)




            Try
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                'Return "The lead " & leadinfo.FirstName & " " & leadinfo.LastName & " was added successfully"
                ''Added by SAraswathi Lakshmanan on NOv 17 2009
                ''When a Lead is added successfully, the Lead is entered into the LeadStatusesChanges table with orig statusId as Null and New status Id as the Lead Status
                db1.RunParamSQLExecuteNoneQuery(sb1.ToString)


                Return ""
            Catch ex As OleDbException
                Return DALExceptions.BuildErrorMessage(ex)
            Finally
                db.ClearParameters()
                sb.Remove(0, sb.Length)
                db.CloseConnection()
                db1.CloseConnection()
            End Try


            'Catch ex As System.Exception
            '    'Return an Error To Client
            '    Return -1
        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            'db.CloseConnection()
        End Try
    End Function


    Public Function UpdateLead(ByVal leadinfo As LeadMasterInfo, ByVal user As String, ByVal strObjective As String, Optional ByVal useRegent As Boolean = False, Optional ByVal bUpdateSSN As Boolean = True) As String
        ''Connect To The Database
        Dim db As New DataAccess
        ' Dim strStatus As String = "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965"


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Update adLeads set FirstName=?, ")
                .Append(" LastName=?,MiddleName=?, ")
                If bUpdateSSN Then
                    .Append(" SSN=?, ")
                End If
                .Append(" Phone=?,Phone2=?,HomeEmail=?, ")
                .Append(" Address1=?,Address2=?,City=?,StateId=?,Zip=?, ")
                .Append(" WorkEmail=?,AddressType=?,Prefix=?,Suffix=?,BirthDate=?, ")
                .Append(" Sponsor=?,AdmissionsRep=?,Gender=?,Race=?, ")
                .Append(" MaritalStatus=?,FamilyIncome=?,Children=?,PhoneType=?,PhoneType2=?, ")
                .Append(" SourceCategoryID=?,SourceTypeID=?,SourceDate=?, ")
                .Append(" AreaID=?,ProgramID=?,PrgVerId=?,ExpectedStart=?,ShiftID=?, ")
                .Append(" Nationality=?,Citizen=?,DrivLicStateID=?,DrivLicNumber=?,AlienNumber=?, ")
                .Append(" Comments=?,ModUser=?,ModDate=?, SourceAdvertisement=?,AssignedDate=?, ")
                .Append(" DateApplied=?,Country=?,County=?,LeadStatus=?,PreviousEducation=?, ")
                .Append(" ForeignPhone=?,ForeignPhone2=?,ForeignZip=?,OtherState=?,AddressStatus=?,PhoneStatus=?,PhoneStatus2=?,DefaultPhone = ?,LeadGrpId=?, ")
                .Append(" DependencyTypeId=?,GeographicTypeId=?,AdminCriteriaId=?,InquiryTime=?,AdvertisementNote=?,HousingId=?,DegCertSeekingId=? ")
                If useRegent = True Then
                    .Append(" ,entranceinterviewdate=?,highschoolprogramcode=? ")
                End If
                '' Code Added by kamalesh Ahuja on 10 June 2010 to Resolve mantis issue id 18411
                .Append(" ,CampusId=? ")
                '''''''''''''
                .Append(" where LeadID = ? ")
            End With

            'First Name
            db.AddParameter("@FirstName", leadinfo.FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Last Name
            db.AddParameter("@LastName", leadinfo.LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'MiddleName
            If leadinfo.MiddleName = "" Then
                db.AddParameter("@MiddleName", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@MiddleName", leadinfo.MiddleName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'SSN
            If bUpdateSSN Then
                If leadinfo.SSN = "" Then
                    db.AddParameter("@SSN", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@SSN", leadinfo.SSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
            End If

            'HomePhone
            If leadinfo.Phone = "" Then
                db.AddParameter("@HomePhone", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HomePhone", leadinfo.Phone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If leadinfo.Phone2 = "" Then
                db.AddParameter("@HomePhone2", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HomePhone2", leadinfo.Phone2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'HomeEmail
            If leadinfo.HomeEmail = "" Then
                db.AddParameter("@HomeEmail", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HomeEmail", leadinfo.HomeEmail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Address
            If leadinfo.Address1 = "" Then
                db.AddParameter("@Address1", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", leadinfo.Address1, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Address2
            If leadinfo.Address2 = "" Then
                db.AddParameter("@Address2", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", leadinfo.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'city
            If leadinfo.City = "" Then
                db.AddParameter("@city", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@city", leadinfo.City, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'StateId
            If leadinfo.State = "" Then
                db.AddParameter("@State", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@State", leadinfo.State, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Zip
            If leadinfo.Zip = "" Then
                db.AddParameter("@zip", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@zip", leadinfo.Zip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'WorkEmail
            If leadinfo.WorkEmail = "" Then
                db.AddParameter("@WorkEmail", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@WorkEmail", leadinfo.WorkEmail, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'AddressType
            If leadinfo.AddressType = "" Then
                db.AddParameter("@AddressType", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AddressType", leadinfo.AddressType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Prefix
            If leadinfo.Prefix = "" Then
                db.AddParameter("@Prefix", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Prefix", leadinfo.Prefix, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'Suffix
            If leadinfo.Suffix = "" Then
                db.AddParameter("@Suffix", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Suffix", leadinfo.Suffix, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'BirthDate
            If leadinfo.BirthDate = "" Then
                db.AddParameter("@BirthDate", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@BirthDate", leadinfo.BirthDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If



            'Sponsor
            If leadinfo.Sponsor = "" Then
                db.AddParameter("@Sponsor", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Sponsor", leadinfo.Sponsor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'AdmissionsRep
            If leadinfo.AdmissionsRep = "" Then
                db.AddParameter("@AdmissionsRep", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AdmissionsRep", leadinfo.AdmissionsRep, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Gender
            If leadinfo.Gender = "" Then
                db.AddParameter("@Gender", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Gender", leadinfo.Gender, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'Race
            If leadinfo.Race = "" Then
                db.AddParameter("@Race", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Race", leadinfo.Race, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'MaritalStatus
            If leadinfo.MaritalStatus = "" Then
                db.AddParameter("@MaritalStatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@MaritalStatus", leadinfo.MaritalStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'FamilyIncome
            If String.IsNullOrEmpty(leadinfo.FamilyIncome) Or leadinfo.FamilyIncome = Guid.Empty.ToString() Then
                db.AddParameter("@FamilyIncome", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@FamilyIncome", leadinfo.FamilyIncome, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Children
            If leadinfo.Children = "" Then
                db.AddParameter("@Children", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Children", leadinfo.Children, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'PhoneType
            If leadinfo.PhoneType = "" Then
                db.AddParameter("@PhoneType", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PhoneType", leadinfo.PhoneType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            If leadinfo.PhoneType2 = "" Then
                db.AddParameter("@PhoneType2", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PhoneType2", leadinfo.PhoneType2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'SourceCatagoryID
            If leadinfo.SourceCategory = "" Then
                db.AddParameter("@SourceCategoryID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@SourceCategoryID", leadinfo.SourceCategory, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'SourceTypeID
            If leadinfo.SourceType = "" Then
                db.AddParameter("@SourceTypeID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@SourceTypeID", leadinfo.SourceType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'SourceDate
            If leadinfo.SourceDate = "" Then
                db.AddParameter("@SourceDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@SourceDate", leadinfo.SourceDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'AreaID
            If leadinfo.Area = "" Then
                db.AddParameter("@AreaID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@AreaID", leadinfo.Area, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'ProgramId
            If leadinfo.ProgramID = "" Then
                db.AddParameter("@ProgramID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ProgramID", leadinfo.ProgramID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'PrgVerId
            If leadinfo.PrgVerId = "" Then
                db.AddParameter("@PrgVerID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PrgVerID", leadinfo.PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'ExpectedStart
            If leadinfo.ExpectedStart = "" Then
                db.AddParameter("@ExpectedStart", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ExpectedStart", leadinfo.ExpectedStart, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ShiftID
            If leadinfo.ShiftID = "" Then
                db.AddParameter("@ShiftID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@ShiftID", leadinfo.ShiftID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'Nationality
            If leadinfo.Nationality = "" Then
                db.AddParameter("@Nationality", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Nationality", leadinfo.Nationality, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'Citizen
            If leadinfo.Citizen = "" Then
                db.AddParameter("@Citizen", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Citizen", leadinfo.Citizen, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'DrivLicStateID
            If leadinfo.DriverLicState = "" Then
                db.AddParameter("@DrivLicStateID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DrivLicStateID", leadinfo.DriverLicState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'DriverLicNumber
            If leadinfo.DriverLicNumber = "" Then
                db.AddParameter("@DrivLicNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DrivLicNumber", leadinfo.DriverLicNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'AlienNumber
            If leadinfo.AlienNumber = "" Then
                db.AddParameter("@AlienNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AlienNumber", leadinfo.AlienNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Comments
            If leadinfo.Notes = "" Then
                db.AddParameter("@Comments", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Comments", leadinfo.Notes, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'SourceAdvertisement
            If leadinfo.SourceAdvertisement = "" Then
                db.AddParameter("@SourceAdvertisement", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@SourceAdvertisement", leadinfo.SourceAdvertisement, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Assigned Date
            If leadinfo.AssignmentDate = "" Then
                db.AddParameter("@AssignedDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AssignedDate", leadinfo.AssignmentDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Applied Date
            If leadinfo.AppliedDate = "" Then
                db.AddParameter("@AppliedDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AppliedDate", leadinfo.AppliedDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Country
            If leadinfo.Country = "" Then
                db.AddParameter("@Country", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Country", leadinfo.Country, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'County
            If leadinfo.County = "" Then
                db.AddParameter("@County", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@County", leadinfo.County, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'LeadStatus
            If leadinfo.Status = "" Then
                db.AddParameter("@LeadStatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@LeadStatus", leadinfo.Status, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'Previous Education
            If leadinfo.PreviousEducation = "" Then
                db.AddParameter("@PreviousEducation", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PreviousEducation", leadinfo.PreviousEducation, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'ForeignPhone
            db.AddParameter("@ForeignPhone", leadinfo.ForeignPhone, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ForeignPhone2", leadinfo.ForeignPhone2, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            'Foreign Zip
            db.AddParameter("@ForeignZip", leadinfo.ForeignZip, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'OtherState
            If leadinfo.OtherState = "" Then
                db.AddParameter("@OtherState", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", leadinfo.OtherState, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Address Status
            If leadinfo.AddressStatus = "" Then
                db.AddParameter("@AddressStatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AddressStatus", leadinfo.AddressStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Phone Status
            If leadinfo.PhoneStatus = "" Then
                db.AddParameter("@PhoneStatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PhoneStatus", leadinfo.PhoneStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If leadinfo.PhoneStatus2 = "" Then
                db.AddParameter("@PhoneStatus2", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PhoneStatus2", leadinfo.PhoneStatus2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@DefaultPhone", leadinfo.DefaultPhone, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            'Lead Group Id
            If leadinfo.LeadGrpId = "" Then
                db.AddParameter("@LeadGrpID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@LeadGrpId", leadinfo.LeadGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'DependencyTypeId
            If leadinfo.DependencyTypeId = "" Then
                db.AddParameter("@DependencyTypeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DependencyTypeId", leadinfo.DependencyTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'GeographicTypeId
            If leadinfo.GeographicTypeId = "" Then
                db.AddParameter("@GeographicTypeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@GeographicTypeId", leadinfo.GeographicTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'AdminCriteriaId
            If leadinfo.AdminCriteriaId = "" Then
                db.AddParameter("@AdminCriteriaId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AdminCriteriaId", leadinfo.AdminCriteriaId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            If leadinfo.InquiryTime = "" Then
                db.AddParameter("@InquiryTime", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@InquiryTime", leadinfo.InquiryTime, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Advertisement Note
            If leadinfo.AdvertisementNote = "" Then
                db.AddParameter("@AdvertisementNote", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AdvertisementNote", leadinfo.AdvertisementNote, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'HousingTypeId
            If leadinfo.HousingTypeId = "" Then
                db.AddParameter("@HousingId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@HousingId", leadinfo.HousingTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Degree Certificate Seeking Id
            If leadinfo.DegCertSeekingId = "" Then
                db.AddParameter("@DegCertSeekingId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DegCertSeekingId", leadinfo.DegCertSeekingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'If useRegent = True Then
            '    If Not leadinfo.EntranceInterviewDate = "01/01/1900" Then
            '        db.AddParameter("@EntranceInterviewDate", leadinfo.EntranceInterviewDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    Else
            '        db.AddParameter("@EntranceInterviewDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    End If
            '    If leadinfo.HighSchoolProgramCode = "" Then
            '        db.AddParameter("@HighSchoolProgramCode", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    Else
            '        db.AddParameter("@HighSchoolProgramCode", leadinfo.HighSchoolProgramCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    End If
            'End If

            '' Code Added by kamalesh Ahuja on 10 June 2010 to Resolve mantis issue id 18411
            'CampusID
            db.AddParameter("@CampusID", leadinfo.CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '''''''''''''

            'LeadID
            db.AddParameter("@LeadID", leadinfo.LeadMasterID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Try
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                'Return "The modifications made to lead " & leadinfo.FirstName & " " & leadinfo.LastName & " was successfully saved"
                Return ""
            Catch ex As OleDbException
                '   return an error to the client
                Return DALExceptions.BuildErrorMessage(ex)
            Finally
                db.ClearParameters()
                sb.Remove(0, sb.Length)
                db.CloseConnection()
            End Try

        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
        'Catch e As System.Exception
        'Finally
        'Close Connection

    End Function

    Public Function DeleteLead(ByVal LeadID As String, ByVal ModDate As DateTime) As String
        'Connect To The Database
        Dim db As New DataAccess



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        'Do an Update
        'Try
        'Build The Query
        Dim sb As New StringBuilder
        Try
            With sb

                .Append("Delete adEntrTestOverRide where LeadID = '" & LeadID & "' ;" & vbCrLf)
                .Append("Delete adLeadByLeadGroups where LeadID = '" & LeadID & "' ;" & vbCrLf)
                .Append("Delete adLeadDocsReceived where LeadID = '" & LeadID & "' ;" & vbCrLf)
                .Append("Delete adLeadEducation where LeadID = '" & LeadID & "' ;" & vbCrLf)
                .Append("Delete adLeadEmployment where LeadID = '" & LeadID & "' ;" & vbCrLf)
                .Append("Delete adLeadEntranceTest where LeadID = '" & LeadID & "' ;" & vbCrLf)
                .Append("Delete adLeadExtraCurriculars where LeadID = '" & LeadID & "' ;" & vbCrLf)
                .Append("Delete adLeadSkills where LeadID = '" & LeadID & "' ;" & vbCrLf)
                .Append("Delete adLeadEducation where LeadID = '" & LeadID & "' ;" & vbCrLf)
                .Append("Delete adLeadByLeadGroups where LeadID = '" & LeadID & "' ;" & vbCrLf)

                .Append("Delete adLeads where LeadID = '" & LeadID & "' AND ModDate = '" & ModDate & "' ;" & vbCrLf)
                .Append("SELECT count(*) FROM adLeads WHERE LeadId = '" & LeadID & "' ;" & vbCrLf)

                ''New code added by Vijay Ramteke On May 19, 2010 to Resolved the Mantis Id 06517
            End With
            '' New Code Added By Vijay Ramteke on May 19, 2010 To resolved Mantis Id 06517
            'LeadID
            db.AddParameter("@LeadID", LeadID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'LeadID
            db.AddParameter("@LeadID", LeadID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'LeadID
            db.AddParameter("@LeadID", LeadID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'LeadID
            db.AddParameter("@LeadID", LeadID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'LeadID
            db.AddParameter("@LeadID", LeadID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'LeadID
            db.AddParameter("@LeadID", LeadID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'LeadID
            db.AddParameter("@LeadID", LeadID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'LeadID
            db.AddParameter("@LeadID", LeadID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'LeadID
            db.AddParameter("@LeadID", LeadID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'LeadID
            db.AddParameter("@LeadID", LeadID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '' New Code Added By Vijay Ramteke on May 19, 2010 To resolved Mantis Id 06517

            'LeadID
            db.AddParameter("@LeadID", LeadID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'LeadID
            db.AddParameter("@LeadID", LeadID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            'If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function UpdateStudentEducation(ByVal EducationInfo As PlacementInfo, ByVal User As String, ByVal Major As String) As String
        'Connect To The Database
        Dim db As New DataAccess



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Update adLeadEducation set  ")
                .Append(" EducationInstType=?, EducationInstId=?, ")
                .Append(" GraduatedDate=?,FinalGrade=?,CertificateId=?,Comments=?,ModUser=?,ModDate=?,Major=? ")
                .Append(" Where LeadEducationID = ? ")
            End With

            'EducationInstType
            db.AddParameter("@EducationInstType", EducationInfo.EducationInstType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'EducationInstId
            db.AddParameter("@EducationInstId", EducationInfo.EducationInstId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Graduated Date
            If EducationInfo.GraduationDate = "" Then
                db.AddParameter("@GraduatedDate", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@GraduatedDate", EducationInfo.GraduationDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            'FinalGrade
            If EducationInfo.FinalGrade = Guid.Empty.ToString Or EducationInfo.FinalGrade = "" Then
                db.AddParameter("@FinalGrade", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@FinalGrade", EducationInfo.FinalGrade, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Certificate
            If EducationInfo.Certificate = Guid.Empty.ToString Or EducationInfo.Certificate = "" Then
                db.AddParameter("@CertificateId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CertificateId", EducationInfo.Certificate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Comments
            'FinalGrade
            db.AddParameter("@Comments", EducationInfo.Comments, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)

            ''ModUser
            db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", EducationInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'Major
            If EducationInfo.Major = Guid.Empty.ToString Or EducationInfo.Major = "" Then
                db.AddParameter("@Major", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Major", EducationInfo.Major, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            ' EducationInstId
            db.AddParameter("@LeadEducationId", EducationInfo.StEmploymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            EducationInfo.IsInDb = True
        Catch ex As OleDbException
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function GetCollegeNamesByLeadID(ByVal EducationInstType As String, ByVal LeadID As String)
        Dim db As New DataAccess



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Select Case EducationInstType
            Case "Schools"
                With sb
                    .Append(" SELECT LeadEducationID,(select HSName from syInstitutions where HsId = EducationInstID and  ")
                    .Append(" EducationInstType = ? ) as CollegeName ")
                    .Append(" FROM  adLeadEducation where EducationInstType= ? and LeadID= ? ")
                    .Append(" Order By CollegeName ")
                End With
            Case Else
                With sb
                    '.Append(" SELECT stuEducationID,(select collegename from adColleges where collegeid = EducationInstID and  ")
                    '.Append(" EducationInstType = ? ) as CollegeName ")
                    '.Append(" FROM  plStudentEducation where EducationInstType= ? ")
                    '.Append(" Order By CollegeName ")
                    .Append(" SELECT LeadEducationID,(select collegename from adColleges where collegeid = EducationInstID and  ")
                    .Append(" EducationInstType = ? ) as CollegeName ")
                    .Append(" FROM  adLeadEducation where EducationInstType= ? and LeadID = ? ")
                    .Append(" Order By CollegeName ")
                End With
        End Select
        db.AddParameter("@EducationInstType", EducationInstType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@EducationInstType", EducationInstType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@LeadID", LeadID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetCollegeNamesByID(ByVal EducationInstType As String, ByVal StudentID As String)
        Dim db As New DataAccess



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Select Case EducationInstType
            Case "Schools"
                With sb
                    .Append(" SELECT stuEducationID,(select HSName from syInstitutions where HsId = EducationInstID and  ")
                    .Append(" EducationInstType = ? ) as CollegeName ")
                    .Append(" FROM  plStudentEducation where EducationInstType= ? and StudentID= ? ")
                    .Append(" Order By CollegeName ")
                End With
            Case Else
                With sb
                    '.Append(" SELECT stuEducationID,(select collegename from adColleges where collegeid = EducationInstID and  ")
                    '.Append(" EducationInstType = ? ) as CollegeName ")
                    '.Append(" FROM  plStudentEducation where EducationInstType= ? ")
                    '.Append(" Order By CollegeName ")
                    .Append(" SELECT LeadEducationID,(select collegename from adColleges where collegeid = EducationInstID and  ")
                    .Append(" EducationInstType = ? ) as CollegeName ")
                    .Append(" FROM  plStudentEducation where EducationInstType= ? and StudentID = ? ")
                    .Append(" Order By CollegeName ")
                End With
        End Select
        db.AddParameter("@EducationInstType", EducationInstType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@EducationInstType", EducationInstType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@StudentID", StudentID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetRequiredDocsByStudent(ByVal EducationInstType As String, ByVal StudentID As String)
        Dim db As New DataAccess



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Select Case EducationInstType
            Case "Schools"
                With sb
                    .Append(" SELECT stuEducationID,(select HSName from syInstitutions where HsId = EducationInstID and  ")
                    .Append(" EducationInstType = ? ) as CollegeName ")
                    .Append(" FROM  plStudentEducation where EducationInstType= ? and StudentID= ? ")
                    .Append(" Order By CollegeName ")
                End With
            Case Else
                With sb
                    '.Append(" SELECT stuEducationID,(select collegename from adColleges where collegeid = EducationInstID and  ")
                    '.Append(" EducationInstType = ? ) as CollegeName ")
                    '.Append(" FROM  plStudentEducation where EducationInstType= ? ")
                    '.Append(" Order By CollegeName ")
                    .Append(" SELECT LeadEducationID,(select collegename from adColleges where collegeid = EducationInstID and  ")
                    .Append(" EducationInstType = ? ) as CollegeName ")
                    .Append(" FROM  plStudentEducation where EducationInstType= ? and StudentID = ? ")
                    .Append(" Order By CollegeName ")
                End With
        End Select
        db.AddParameter("@EducationInstType", EducationInstType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@EducationInstType", EducationInstType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@StudentID", StudentID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetStudentInfo(ByVal LeadId As String, ByVal EducationInstId As String, ByVal InstType As String) As PlacementInfo
        'connect to the database
        Dim db As New DataAccess



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT     B.EducationInstId, ")
            If InstType = "College" Then
                .Append(" (select Distinct CollegeName from adColleges where CollegeId=B.EducationInstId) as EducationInstitution, ")
            Else
                .Append(" (select Distinct HSName from syInstitutions where HSId=B.EducationInstId) as EducationInstitution, ")
            End If
            .Append("    B.GraduatedDate, ")
            .Append("    B.FinalGrade, ")
            .Append("    B.CertificateId, ")
            .Append("    (select Distinct DegreeDescrip from arDegrees where DegreeId=B.CertificateId) as CertificateDescrip, ")
            .Append(" B.Comments, ")
            .Append(" B.major,B.ModDate, ")
            .Append("    B.EducationInstId ")
            .Append("FROM  adLeadEducation B ")
            .Append("Where LeadEducationId = ? ")
        End With


        db.AddParameter("@EducationInstID", EducationInstId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim studentinfo As New PlacementInfo
        While dr.Read()

            'set properties with data from DataReader
            With studentinfo
                .IsInDb = True
                .StudentId = LeadId

                If Not (dr("EducationInstId") Is DBNull.Value) Then .EducationInstId = CType(dr("EducationInstId"), Guid).ToString

                'Get Graduated Date
                If Not (dr("GraduatedDate") Is DBNull.Value) Then .GraduationDate = dr("GraduatedDate") Else .GraduationDate = ""

                'Get FinalGrade
                If Not (dr("FinalGrade") Is DBNull.Value) Then .FinalGrade = dr("FinalGrade") Else .FinalGrade = ""

                'Get Comments
                If Not (dr("Comments") Is DBNull.Value) Then .Comments = CType(dr("Comments"), String) Else .Comments = ""

                'Get Major
                If Not (dr("Major") Is DBNull.Value) Then .Major = CType(dr("Major"), String) Else .Major = ""

                'Get Certificate
                If Not (dr("CertificateId") Is DBNull.Value) Then .Certificate = CType(dr("CertificateId"), Guid).ToString Else .Certificate = Guid.Empty.ToString

                'Get Graduated Date
                If Not (dr("ModDate") Is DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = Date.MinValue

                'Certificate Descrip
                If Not (dr("CertificateDescrip") Is DBNull.Value) Then .CertificateDescrip = CType(dr("CertificateDescrip"), String) Else .CertificateDescrip = ""

                'Education Institution
                If Not (dr("EducationInstitution") Is DBNull.Value) Then .EducationInst = CType(dr("EducationInstitution"), String) Else .EducationInst = ""
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return studentinfo
    End Function

    Public Function CheckDuplicates(ByVal EducationInstId As String, ByVal LeadId As String, ByVal GraduateDate As String, ByVal CertificateId As String) As Integer
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" SELECT Count(*)  as LeadCount ")
            .Append(" from adLeadEducation where LeadId=? and EducationInstId =? and GraduatedDate = ? and CertificateId=?  ")
        End With

        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@EducationInstID", EducationInstId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@GraduateDate", GraduateDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CertificateID", CertificateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim intCheckCount As Integer
        While dr.Read()
            intCheckCount = dr("LeadCount")
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return intCheckCount
    End Function

    Public Function GetStudentsByCampusFASAP(ByVal CampusId As String, Optional ByVal prgVerId As String = "") As ArrayList
        Dim db As New DataAccess

        Dim oleConn As OleDbConnection
        Dim oleCmd As OleDbCommand
        Dim strOutputParam As OleDbParameter

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        oleConn = New OleDbConnection(myAdvAppSettings.AppSettings("ConString"))
        oleConn.Open()

        oleCmd = New OleDbCommand("USP_FASAPByCampusPrgVersion", oleConn)
        oleCmd.CommandType = CommandType.StoredProcedure
        strOutputParam = oleCmd.Parameters.Add("@CampId", OleDbType.Guid)
        strOutputParam.Direction = ParameterDirection.Input
        strOutputParam = oleCmd.Parameters.Add("@ProgVerID", OleDbType.Guid)
        strOutputParam.Direction = ParameterDirection.Input

        oleCmd.Parameters("@CampId").Value = New Guid(CampusId)
        If prgVerId <> String.Empty Then
            oleCmd.Parameters("@ProgVerID").Value = New Guid(prgVerId)
        End If

        Dim dr As OleDbDataReader = oleCmd.ExecuteReader()
        Dim StrStudentEnrollment As New ArrayList
        While dr.Read()
            StrStudentEnrollment.Add(CType(dr("StuEnrollId"), Guid).ToString)
        End While

        oleConn.Close()
        Return StrStudentEnrollment

    End Function
    Public Function GetStudentsByCampus(ByVal CampusId As String, Optional ByVal prgVerId As String = "") As ArrayList
        'connect to the database
        Dim db As New DataAccess



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT StuEnrollId ")
            .Append("FROM arStuEnrollments t1, syStatusCodes t2, sySysStatus t3 ")
            .Append("WHERE CampusId= ?  ")
            .Append("AND t1.StatusCodeId = t2.StatusCodeId ")
            .Append("AND t2.SysStatusId = t3.SysStatusId ")
            .Append("AND t3.SysStatusId IN(9,13,20,22) ")
            .Append("AND t1.StartDate <= ? ")
            'Added for testing
            '.Append("AND t1.StuEnrollId = '1D97FDA6-8AFA-4134-865A-204C16D7C377' ")

            If prgVerId <> "" Then
                .Append("AND t1.PrgVerId = ? ")
            End If

            '.Append("AND t1.StuEnrollId = '73EB25A3-75C9-4701-8302-AC6C56AD3F87' ")
        End With

        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ''ModDate
        db.AddParameter("@sDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        If prgVerId <> "" Then
            db.AddParameter("@prgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim StrStudentEnrollment As New ArrayList
        While dr.Read()
            StrStudentEnrollment.Add(CType(dr("StuEnrollId"), Guid).ToString)
        End While

        If Not dr.IsClosed Then dr.Close()

        Return StrStudentEnrollment
    End Function

    Public Function GetLeadEnrollmentInfo(ByVal leadId As String) As LeadMasterInfo
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With sub-queries
            .Append("SELECT  Distinct    ")
            .Append("       B.LeadId, ")
            .Append("       B.FirstName, ")
            .Append("       B.MiddleName, ")
            .Append("       B.LastName, ")
            .Append("       B.ExpectedStart, ")
            .Append("       B.IsDisabled,  ")
            .Append("       B.EntranceInterviewDate,  ")
            .Append("       B.IsFirstTimeInSchool,  ")
            .Append("       B.IsFirstTimePostSecSchool, ")
            .Append("       B.ShiftID, ")
            .Append("       (CASE WHEN (Select Count(*) from adLeadByLeadGroups LLG inner join adLeadGroups LG on LLG.LeadGrpId = LG.LeadGrpId WHERE (lg.Descrip LIKE 'Int%' OR lg.Descrip LIKE '%International%') and LeadId=B.LeadId) > 0 THEN 1 ELSE 0 END) as InternationalLead, ")
            .Append("       (select Distinct ShiftDescrip from arShifts where ShiftId=B.ShiftId) as ShiftDescrip, ")
            .Append("       B.CampusId, ")
            .Append("       (select Distinct CampDescrip from syCampuses where CampusId=B.CampusId) as CampusDescrip, ")
            .Append("       B.AdmissionsRep, ")
            .Append("       (select Distinct FullName from syUsers where UserId=B.AdmissionsRep) as AdmissionsRepDescrip, ")
            .Append("       B.PreviousEducation, ")
            .Append("       (select Distinct EdLvlDescrip from adEdLvls where EdLvlId=B.PreviousEducation) as EdLvlDescrip, ")
            .Append("       B.PrgVerId, ")
            .Append("       (select Distinct PrgVerDescrip from arPrgVersions where PrgVerId=B.PrgVerId) as PrgVersionDescrip, ")
            .Append(" case when (select ShiftDescrip from arShifts,arPrograms,arPrgVersions where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=arPrgVersions.Progid and arPrgVersions.PrgVerId=B.prgVerId) is null  then ")
            .Append(" (select Distinct PrgVerDescrip from arPrgVersions where PrgVerId=B.PrgVerId) ")
            .Append(" else ")
            .Append(" (select Distinct PrgVerDescrip from arPrgVersions where PrgVerId=B.PrgVerId) + ' (' + (select ShiftDescrip from arShifts,arPrograms,arPrgVersions where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=arPrgVersions.ProgId and arPrgVersions.PrgVerId=B.prgVerId) + ')'  ")
            .Append(" end as PrgVerShiftDescrip,  ")

            .Append("       (Case when PrgVerId is NOT NULL then (select Distinct BillingMethodId from arPrgVersions where PrgVerId=B.PrgVerId)  end) as BillingMethodId, ")
            '.Append("       (Case when PrgVerId is NOT NULL then (select Distinct BillingMethodDescrip from saBillingMethods where BillingMethodId=BillingMethodId) end) as BillingMethodDescrip, ")
            .Append("       (Case when PrgVerId is NOT NULL then (select Distinct BillingMethodDescrip from saBillingMethods where BillingMethodId in (select Distinct BillingMethodId from arPrgVersions where PrgVerId=B.PrgVerId)) end) as BillingMethodDescrip, ")
            .Append(" (select Distinct ProgId from arPrgVersions where PrgVerId=B.PrgVerId) as ProgId,	")
            .Append(" B.Gender, ")
            .Append(" (select Distinct GenderDescrip from adGenders where GenderId=B.Gender) as GenderDescrip, ")
            .Append(" B.Race, ")
            .Append(" (select Distinct EthCodeDescrip from adEthCodes where EthCodeId=B.Race) as EthCodeDescrip, ")
            .Append(" B.MaritalStatus, ")
            .Append(" (select Distinct MaritalStatDescrip from adMaritalStatus where MaritalStatId=B.MaritalStatus) as MaritalStatusDescrip, ")
            .Append(" B.Nationality, ")
            .Append(" (select Distinct NationalityDescrip from adNationalities where NationalityId=B.Nationality) as NationalityDescrip, ")
            .Append(" B.Citizen, ")
            .Append(" (select Distinct CitizenShipDescrip from adCitizenships where CitizenshipId=B.Citizen) as CitizenDescrip, ")
            .Append(" DependencyTypeId, ")
            .Append(" (select Distinct Descrip from adDependencyTypes where DependencyTypeId=B.DependencyTypeId) as DependencyTypeDescrip, ")
            .Append(" GeographicTypeId, ")
            .Append(" (select Distinct Descrip from adGeographicTypes where GeographicTypeId=B.GeographicTypeId) as GeographicTypeDescrip, ")

            'DE7172 2/16/2012 Janet Robinson Add Family Income
            .Append(" FamilyIncome, ")
            .Append(" (select Distinct FamilyIncomeDescrip from syFamilyIncome where FamilyIncomeId=B.FamilyIncome) as FamilyIncomeDescrip, ")

            .Append(" AdminCriteriaId,HousingId, ")
            .Append(" (select Distinct Descrip from adAdminCriteria where AdminCriteriaId=B.AdminCriteriaId) as AdminCriteriaDescrip, ")
            .Append(" (select Distinct Descrip from arHousing where HousingId=B.HousingId) as HousingTypeDescrip, ")
            .Append(" B.StateId, B.ForeignZip, ")
            .Append(" (select Distinct StateDescrip from syStates where StateId=B.StateId) as StateDescrip,B.BirthDate, ")
            .Append(" B.DegCertSeekingId,(SELECT Descrip FROM adDegCertSeeking WHERE DegCertSeekingId=B.DegCertSeekingId) AS DegCertSeekingDescrip,B.SSN  ")
            .Append(" FROM   ")
            .Append("       adLeads B ")
            .Append(" WHERE  ")
            .Append("       LeadId = ? ")
        End With
        db.AddParameter("@LeadID", leadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim leadInfo As New LeadMasterInfo
        While dr.Read()
            'set properties with data from DataReader
            With leadInfo
                'Is Disabled
                Dim isdi = dr("IsDisabled")
                If (isdi Is DBNull.Value) Then
                    .IsDisabled = -1
                Else
                    .IsDisabled = If(isdi, 1, 0)
                End If

                'EntranceInterviewDate
                Dim eid = dr("EntranceInterviewDate")
                If Not (eid Is DBNull.Value) Then .EntranceInterviewDate = eid Else .EntranceInterviewDate = Nothing

                'IsFirstTimeInSchool
                .IsFirstTimeInSchool = dr("IsFirstTimeInSchool")

                'IsFirstTimePostSecSchool
                .IsFirstTimePostSecSchool = dr("IsFirstTimePostSecSchool")

                'FirstName
                If Not (dr("FirstName") Is DBNull.Value) Then .FirstName = dr("FirstName") Else .FirstName = ""

                'Middle Name
                If Not (dr("MiddleName") Is DBNull.Value) Then .MiddleName = dr("MiddleName") Else .MiddleName = ""

                'Last Name
                If Not (dr("LastName") Is DBNull.Value) Then .LastName = dr("LastName") Else .LastName = ""

                'Expected Start Date
                If Not (dr("ExpectedStart") Is DBNull.Value) Then .ExpectedStart = dr("ExpectedStart") Else .ExpectedStart = ""

                'Get Shift
                If Not (dr("ShiftId") Is DBNull.Value) Then .ShiftID = CType(dr("ShiftId"), Guid).ToString Else .ShiftID = ""

                'Shift Descrip
                If Not (dr("ShiftDescrip") Is DBNull.Value) Then .ShiftDescrip = dr("ShiftDescrip") Else .ShiftDescrip = ""

                'Campus
                If Not (dr("CampusId") Is DBNull.Value) Then .CampusId = CType(dr("CampusId"), Guid).ToString Else .CampusId = Guid.Empty.ToString

                'Campus Descrip
                If Not (dr("CampusDescrip") Is DBNull.Value) Then .CampusDescrip = dr("CampusDescrip") Else .CampusDescrip = ""

                'Admissions Rep
                If Not (dr("AdmissionsRep") Is DBNull.Value) Then .AdmissionsRep = CType(dr("AdmissionsRep"), Guid).ToString Else .AdmissionsRep = ""

                'AdmissionsRep Descrip
                If Not (dr("AdmissionsRepDescrip") Is DBNull.Value) Then .AdmissionRepsDescrip = dr("AdmissionsRepDescrip") Else .AdmissionRepsDescrip = ""

                'Grade Level
                If Not (dr("PreviousEducation") Is DBNull.Value) Then .GradeLevel = CType(dr("PreviousEducation"), Guid).ToString Else .GradeLevel = ""

                'Previous Education Level Descrip
                If Not (dr("EdLvlDescrip") Is DBNull.Value) Then .EdLvlDescrip = dr("EdLvlDescrip") Else .EdLvlDescrip = ""

                'PrgVerId
                If Not (dr("PrgVerId") Is DBNull.Value) Then .PrgVerId = CType(dr("PrgVerId"), Guid).ToString Else .PrgVerId = ""

                'Gender
                If Not (dr("Gender") Is DBNull.Value) Then .Gender = CType(dr("Gender"), Guid).ToString Else .Gender = ""

                'Gender Descrip
                If Not (dr("GenderDescrip") Is DBNull.Value) Then .GenderDescrip = dr("GenderDescrip").ToString Else .GenderDescrip = ""
                If Not (dr("ProgId") Is DBNull.Value) Then .ProgramID = CType(dr("ProgId"), Guid).ToString Else .ProgramID = ""
                If .PrgVerId = "" Then
                    .PrgVersionDescrip = ""
                    .ChargingMethod = ""
                    .ChargingMethodDescrip = ""
                Else
                    .PrgVersionDescrip = dr("PrgVersionDescrip")
                    If Not (dr("BillingMethodId") Is DBNull.Value) Then
                        .ChargingMethod = CType(dr("BillingMethodId"), Guid).ToString
                        .ChargingMethodDescrip = dr("BillingMethodDescrip")
                    End If
                End If
                If Not (dr("LeadId") Is DBNull.Value) Then .LeadMasterID = CType(dr("LeadId"), Guid).ToString
                If Not (dr("Gender") Is DBNull.Value) Then .Gender = CType(dr("Gender"), Guid).ToString Else .Gender = ""
                If Not (dr("Race") Is DBNull.Value) Then .Race = CType(dr("Race"), Guid).ToString Else .Race = ""
                If Not (dr("MaritalStatus") Is DBNull.Value) Then .MaritalStatus = CType(dr("MaritalStatus"), Guid).ToString Else .MaritalStatus = ""
                If Not (dr("Nationality") Is DBNull.Value) Then .Nationality = CType(dr("Nationality"), Guid).ToString Else .Nationality = ""
                If Not (dr("Citizen") Is DBNull.Value) Then .Citizen = CType(dr("Citizen"), Guid).ToString Else .Citizen = ""
                If Not (dr("PreviousEducation") Is DBNull.Value) Then .PreviousEducation = CType(dr("PreviousEducation"), Guid).ToString Else .PreviousEducation = ""
                If Not (dr("DependencyTypeId") Is DBNull.Value) Then .DependencyTypeId = CType(dr("DependencyTypeId"), Guid).ToString Else .DependencyTypeId = ""
                If Not (dr("GeographicTypeId") Is DBNull.Value) Then .GeographicTypeId = CType(dr("GeographicTypeId"), Guid).ToString Else .GeographicTypeId = ""
                If Not (dr("AdminCriteriaId") Is DBNull.Value) Then .AdminCriteriaId = CType(dr("AdminCriteriaId"), Guid).ToString Else .AdminCriteriaId = ""
                If Not (dr("StateDescrip") Is DBNull.Value) Then .StateDescrip = CType(dr("StateDescrip"), String).ToString Else .StateDescrip = ""
                If Not (dr("EthCodeDescrip") Is DBNull.Value) Then .EthCodeDescrip = CType(dr("EthCodeDescrip"), String).ToString Else .EthCodeDescrip = ""
                If Not (dr("DependencyTypeDescrip") Is DBNull.Value) Then .DependencyTypeDescrip = CType(dr("DependencyTypeDescrip"), String).ToString Else .DependencyTypeDescrip = ""
                If Not (dr("GeographicTypeDescrip") Is DBNull.Value) Then .GeographicTypeDescrip = CType(dr("GeographicTypeDescrip"), String).ToString Else .GeographicTypeDescrip = ""
                If Not (dr("HousingId") Is DBNull.Value) Then .HousingTypeId = CType(dr("HousingId"), Guid).ToString Else .HousingTypeId = ""
                If Not (dr("AdminCriteriaDescrip") Is DBNull.Value) Then .AdminCriteriaDescrip = dr("AdminCriteriaDescrip").ToString Else .AdminCriteriaDescrip = ""
                If Not (dr("HousingTypeDescrip") Is DBNull.Value) Then .HousingTypeDescrip = dr("HousingTypeDescrip").ToString Else .HousingTypeDescrip = ""
                If Not (dr("stateid") Is DBNull.Value) Then .State = CType(dr("StateId"), Guid).ToString Else .State = ""
                If Not (dr("StateDescrip") Is DBNull.Value) Then .StateDescrip = CType(dr("StateDescrip"), String).ToString Else .StateDescrip = ""
                If Not (dr("BirthDate") Is DBNull.Value) Then .BirthDate = dr("BirthDate") Else .BirthDate = ""
                If Not (dr("DegCertSeekingId") Is DBNull.Value) Then .DegCertSeekingId = CType(dr("DegCertSeekingId"), Guid).ToString Else .DegCertSeekingId = ""
                If Not (dr("DegCertSeekingDescrip") Is DBNull.Value) Then .DegCertSeekingDescrip = dr("DegCertSeekingDescrip").ToString() Else .DegCertSeekingDescrip = ""

                'DE7172 2/16/2012 Janet Robinson Add Family Income
                If Not (dr("FamilyIncome") Is DBNull.Value) Then .FamilyIncome = CType(dr("FamilyIncome"), Guid).ToString Else .FamilyIncome = Guid.Empty.ToString
                If Not (dr("FamilyIncomeDescrip") Is DBNull.Value) Then .FamilyIncomeDescrip = dr("FamilyIncomeDescrip").ToString() Else .FamilyIncomeDescrip = ""
                If Not (dr("SSN") Is DBNull.Value) Then .SSN = dr("SSN").ToString() Else .SSN = ""
                ' DE5156 01/13/2012 Janet Robinson 
                If dr("ForeignZip") = True Then
                    .ForeignZip = 1
                Else
                    .ForeignZip = 0
                End If

                .InternationalLead = dr("InternationalLead")
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return leadInfo
    End Function
    Public Function GetLeadUnEnrollmentInfo(ByVal LeadId As String) As LeadMasterInfo
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT     B.LeadId, ")
            .Append("    B.SSN, ")
            .Append("    B.BirthDate,A.EnrollmentId ")
            .Append(" FROM  adLeads B,arStuEnrollments A ")
            .Append(" Where B.LeadId=A.LeadId and B.LeadId = ? ")
        End With

        db.AddParameter("@LeadID", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim LeadInfo As New LeadMasterInfo
        While dr.Read()
            'set properties with data from DataReader
            With LeadInfo
                'Expected Start Date
                If Not (dr("SSN") Is DBNull.Value) Then .SSN = dr("SSN")
                If Not (dr("BirthDate") Is DBNull.Value) Then .BirthDate = dr("BirthDate")
                If Not (dr("EnrollmentId") Is DBNull.Value) Then .EnrollmentId = dr("EnrollmentId")
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return LeadInfo
    End Function
    Public Function GetReassignLeadStatus(Optional ByVal strActiveOnly As String = "", Optional ByVal campGrpId As String = "") As DataSet
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("SELECT DISTINCT  ")
            .Append("       A.StatusCodeID AS StatusCodeID,A.StatusCodeDescrip AS StatusCodeDescrip ")
            .Append("FROM   syStatusCodes A,sySysStatus B,syStatuses C,syStatusLevels D ")
            .Append("WHERE  A.sysStatusID=B.sysStatusID ")
            .Append("       AND A.StatusID=C.StatusID ")
            .Append("       AND B.StatusLevelId=D.StatusLevelId ")
            .Append("       AND D.StatusLevelId=1 ")
            If strActiveOnly = "" Then
                .Append("       AND C.Status='Active' ")
            End If
            If campGrpId <> "" Then
                .Append("       AND A.CampGrpId=? ")
            End If
            .Append("ORDER BY A.StatusCodeDescrip  ")
        End With

        '   add parameter if needed
        If campGrpId <> "" Then
            db.AddParameter("@CampGrpId", campGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetLeadStatusesByUserAndCampus(ByVal campusId As String, ByVal userId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim IsSAOrDirectorOfAdmissions As Boolean = (New UserSecurityDB).IsSAOrDirectorOfAdmissions(userId)

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        'Modified By Balaji
        With sb
            .Append("	SELECT Distinct GetNewLeadStatusByCampus.StatusCodeId, ")
            .Append("					GetNewLeadStatusByCampus.StatusCodeDescrip, ")
            .Append("					GetNewLeadStatusByCampus.CampGrpId	")
            .Append("	FROM ")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '	This Table Query Returns The Lead Status Based on the Campus and Statuses mapped to "NEW LEAD STATUS"
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            .Append(" (	")
            .Append("   select StatusCodeId,StatusCodeDescrip,CampGrpId from syStatusCodes t1,sySysStatus t2 ")
            .Append("   where t1.SysStatusId = t2.SysStatusId and t2.StatusLevelId=1 and ")
            .Append("	t1.StatusId='" & strActiveGUID & "' and CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" ) GetNewLeadStatusByCampus, ")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '	This Table Query Returns The Lead Status FROM LeadStatusChanges Table Based on the Campus
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            .Append(" (	")
            .Append(" SELECT Distinct LeadStatusChangeId,OrigStatusId,CampGrpId FROM syLeadStatusChanges ")
            .Append(" WHERE CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" ) GetLeadStatusByCampus ")
            If IsSAOrDirectorOfAdmissions = False Then
                .Append(" ,syLeadStatusChangePermissions F,syUsersRolesCampGrps U ")
            End If
            .Append(" WHERE GetNewLeadStatusByCampus.StatusCodeId = GetLeadStatusByCampus.OrigStatusId	")
            .Append(" and GetNewLeadStatusByCampus.CampGrpId = GetLeadStatusByCampus.CampGrpID	")
            If IsSAOrDirectorOfAdmissions = False Then
                .Append(" and GetLeadStatusByCampus.LeadStatusChangeId = F.LeadStatusChangeId and ")
                .Append(" F.RoleId = U.RoleId and UserId='" & userId & "'  and F.StatusId='" & strActiveGUID & "'  ")
            End If
        End With
        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetNewLeadStatus() As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Dim strStatus As String = "Enroll"
        'build the sql query
        With sb
            .Append(" SELECT Distinct  A.StatusCodeID as StatusCodeID,A.StatusCodeDescrip as StatusCodeDescrip FROM  ")
            .Append(" syStatusCodes A,sySysStatus B,syStatuses C,syStatusLevels D ")
            .Append(" where A.sysStatusID = B.sysStatusID And A.StatusID = C.StatusID and B.StatusLevelId = D.StatusLevelId ")
            .Append(" and C.Status = 'Active' and D.StatusLevelId = 1 and ")
            '.Append(" B.SysStatusDescrip not like  + '%' + ? + '%'")
            .Append(" B.SysStatusId <> 6  ")
            .Append(" ORDER BY A.StatusCodeDescrip  ")
        End With

        'db.AddParameter("@Descrip", strStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function IsSAOrDirectorOfAdmissions(ByVal userId As String) As Boolean

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       Count(*) ")
            .Append("FROM ")
            .Append("       syUsers U, ")
            .Append("       syUsersRolesCampGrps URC, ")
            .Append("       syRoles R ")
            .Append("WHERE ")
            .Append("       (U.UserId=URC.UserId ")
            .Append("AND    URC.RoleId=R.RoleId ")
            .Append("AND    (R.SysRoleId=8 OR R.SysRoleId=1) ")
            .Append("AND 	U.UserId = ? ) ")
            .Append("OR	    (U.UserId = ? AND IsAdvantageSuperUser=1) ")
        End With

        ' Add the UserId to the parameter list
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return boolean
        Return CType(db.RunParamSQLScalar(sb.ToString), Boolean)

    End Function

    Public Function IsDirectorofAcademic(ByVal user As String, ByVal CampusId As String) As Boolean

        '   connect to the database
        Dim db As New DataAccess
        Dim intRowCount As Integer
        Dim boolValue As Boolean = False



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT " & vbCrLf)
            .Append("Count(*) as CountRecords " & vbCrLf)
            .Append("FROM  syUsers SU " & vbCrLf)
            .Append("INNER JOIN	syUsersRolesCampGrps SURC ON  SU.UserId=SURC.UserId " & vbCrLf)
            .Append("INNER JOIN	syRoles SR ON  SURC.RoleId=SR.RoleId " & vbCrLf)
            .Append("WHERE SR.SysRoleId=13 AND 	" & vbCrLf)
            .Append("SU.UserName = ? AND " & vbCrLf)
            .Append("SURC.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId = ?)" & vbCrLf)
        End With

        ' Add the UserId to the parameter list
        db.AddParameter("@User", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            intRowCount = db.RunParamSQLScalar(sb.ToString)
            If intRowCount >= 1 Then
                Return True

            End If
        Catch ex As Exception
            intRowCount = 0
        End Try
        Return boolValue
    End Function
    Public Function IsDirectorofAcademic(ByVal user As String) As Boolean

        '   connect to the database
        Dim db As New DataAccess
        Dim intRowCount As Integer
        Dim boolValue As Boolean = False


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       Count(*) as CountRecords ")
            .Append("FROM ")
            .Append("       syUsers U, ")
            .Append("       syUsersRolesCampGrps URC, ")
            .Append("       syRoles R ")
            .Append("WHERE ")
            .Append("       U.UserId=URC.UserId ")
            .Append("AND    URC.RoleId=R.RoleId ")
            .Append("AND    (R.SysRoleId=13) ")
            .Append("AND 	U.UserName = ?  ")
        End With

        ' Add the UserId to the parameter list
        db.AddParameter("@User", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        Try
            intRowCount = db.RunParamSQLScalar(sb.ToString)
            If intRowCount >= 1 Then
                Return True

            End If
        Catch ex As Exception
            intRowCount = 0
        End Try
        Return boolValue
    End Function
    Public Function GetLeadsNotEnroll(ByVal campusId As String, ByVal userId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim isUserSAorDir As Boolean = IsSAOrDirectorOfAdmissions(userId)
        'Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid

        With sb
            .Append("   select Distinct t3.StatusCodeId as StatusCodeId,t3.StatusCodeDescrip as StatusCodeDescrip ")
            .Append("   from syLeadStatusChanges t1,syStatusCodes t3 ")
            If isUserSAorDir = False Then
                .Append(" ,syLeadStatusChangePermissions t2 ")
            End If
            .Append("   where ")
            .Append("   t1.OrigStatusId = t3.StatusCodeId and t1.NewStatusId in (select Distinct StatusCodeId from syStatusCodes where SysStatusId=6) ")
            .Append("   and t1.CampGrpId in (select Distinct CampGrpId from syCmpGrpCmps where CampusId=?) ")
            If isUserSAorDir = False Then
                .Append("   and t1.LeadStatusChangeId = t2.LeadStatusChangeId and RoleId in  ")
                .Append("   (select RoleId from syUsersRolesCampGrps t4,syCmpGrpCmps t5 where t4.CampGrpId=t5.CampGrpId and ")
                .Append("   t4.UserId=? and CampusId=?) ")
            End If
        End With

        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If isUserSAorDir = False Then
            db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetEnrolledLeadStatus(Optional ByVal campusId As String = "") As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append(" SELECT DISTINCT ")
            .Append("       A.StatusCodeID as StatusCodeID,A.StatusCodeDescrip AS StatusCodeDescrip ")
            .Append(" FROM  syStatusCodes A,sySysStatus B,syStatuses C,syStatusLevels D,syCmpGrpCmps G ")
            .Append(" WHERE A.sysStatusID=B.sysStatusID")
            .Append("       AND A.StatusID=C.StatusID AND B.StatusLevelId=D.StatusLevelId")
            .Append("       AND C.Status='Active' AND D.StatusLevelId=1")
            .Append("       AND B.SysStatusId=6")
            If campusId <> "" Then
                .Append("       AND G.CampGrpId=A.CampGrpId")
                .Append("       AND G.CampusId=?")
            End If
            .Append(" ORDER BY A.StatusCodeDescrip")
        End With

        If campusId <> "" Then
            db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetReassignLeadStatusNoEnroll(ByVal campusId As String, ByVal userId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("SELECT	")
            .Append("       L.NewStatusId AS StatusCodeId,")
            .Append("       (SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=L.NewStatusId) AS StatusCodeDescrip ")
            .Append("FROM 	syLeadStatusChanges L,syStatusCodes SC,syStatuses S,syCmpGrpCmps G,syLeadStatusChangePermissions P,syUsersRolesCampGrps U ")
            .Append("WHERE  L.OrigStatusId=SC.StatusCodeId And SC.SysStatusId=6 ")
            .Append("       AND SC.StatusId=S.StatusId AND S.Status='Active' ")
            .Append("       AND G.CampGrpId=L.CampGrpId ")
            .Append("       AND G.CampusId=? ")
            .Append("       AND L.LeadStatusChangeId=P.LeadStatusChangeId ")
            .Append("       AND EXISTS (SELECT * FROM syLeadStatusChangePermissions A,syStatuses B ")
            .Append("                   WHERE LeadStatusChangeId=P.LeadStatusChangeId AND RoleId=P.RoleId ")
            .Append("                   AND A.StatusId=B.StatusId AND B.Status='Active') ")
            .Append("       AND P.RoleId=U.RoleId ")
            .Append("       AND U.UserId=? ")
            .Append("       AND L.CampGrpId=U.CampGrpId ")
            .Append("       AND EXISTS (SELECT * FROM syStatusCodes A,syStatuses B ")
            .Append("                   WHERE A.StatusCodeId=L.NewStatusId ")
            .Append("                   AND A.StatusId=B.StatusId AND B.Status='Active') ")
            .Append("ORDER BY StatusCodeDescrip ")

            ''.Append(" SELECT Distinct  A.StatusCodeID as StatusCodeID,A.StatusCodeDescrip as StatusCodeDescrip FROM    ")
            ''.Append(" syStatusCodes A,sySysStatus B,syStatuses C,syStatusLevels D  ")
            ''.Append(" where A.sysStatusID = B.sysStatusID And A.StatusID = C.StatusID  and B.StatusLevelId = D.StatusLevelId ")
            ''.Append(" and C.Status = 'Active' and D.StatusLevelId = 1 ")
            ''.Append(" and A.StatusCodeId not in ( ")
            ''.Append(" select StatusCodeId from syStatusCodes where SysStatusId in (select SysStatusId from sySysStatus where SysStatusId  =6)) ")
            ''.Append(" ORDER BY A.StatusCodeDescrip  ")
        End With

        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllSourceCatagory() As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("select LT.SourceCatagoryID,LT.SourceCatagoryDescrip ")
            .Append("FROM   adSourceCatagory LT,syStatuses ST ")
            .Append("WHERE    LT.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append(" ORDER BY LT.SourceCatagoryDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllSourceCategoryByCampus(ByVal campusid As String, ByVal LeadId As String) As DataSet
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusid)

        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
            Else
                strCampGrpId = guid.Empty.ToString() & "'"
        End If

        'build the sql query
        With sb
            If Not LeadId = "" Then
                .Append(" Select Distinct LT.SourceCatagoryID as SourceCatagoryID,'(X) ' + LT.SourceCatagoryDescrip as SourceCatagoryDescrip from adLeads L,adSourceCatagory LT ")
                .Append(" where L.SourceCategoryID=LT.SourceCatagoryID AND L.LeadId='" & LeadId & "'")
                .Append(" and LT.SourceCatagoryId not in ")
                .Append(" ( ")
                .Append("Select DISTINCT LT.SourceCatagoryID ")
                .Append("FROM   adSourceCatagory LT ")
                .Append("WHERE    LT.StatusId = '" & strActiveGUID & "' ")
                .Append("AND LT.CampGrpId in ('")
                .Append(strCampGrpId)
                .Append(") ")
                .Append(" ) ")
                .Append(" Union ")
            End If
            .Append("Select DISTINCT LT.SourceCatagoryID as SourceCatagoryID,LT.SourceCatagoryDescrip as SourceCatagoryDescrip ")
            .Append("FROM   adSourceCatagory LT ")
            .Append("WHERE    LT.StatusId = '" & strActiveGUID & "' ")
            .Append("AND LT.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" ORDER BY SourceCatagoryDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllSourceTypeByCampus(ByVal SourceCatagoryID As String, ByVal campusid As String, ByVal LeadId As String, Optional ByVal boolDisplayInActive As Boolean = False) As DataSet
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        ' Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusid)

        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        'build the sql query
        With sb
            If Not LeadId = "" And boolDisplayInActive = True Then
                .Append(" Select DISTINCT LT.SourceTypeID as SourceTypeId,'(X) ' + LT.SourceTypeDescrip as SourceTypeDescrip from adLeads L,adSourceType LT ")
                .Append(" where L.SourceTypeID=LT.SourceTypeID AND L.LeadId='" & LeadId & "' ")
                .Append(" AND L.SourceTypeId not in   ")
                .Append(" (  ")
                .Append(" Select DISTINCT LT.SourceTypeID  ")
                .Append(" FROM   adSourceType LT ")
                .Append(" WHERE  LT.SourceCatagoryID='" & SourceCatagoryID & "' AND ")
                .Append(" LT.StatusId = '" & strActiveGUID & "' ")
                .Append(" AND LT.CampGrpId in ('")
                .Append(strCampGrpId)
                .Append(")  ")
                .Append(" ) ")
                .Append(" Union ")
            End If
            .Append(" Select DISTINCT LT.SourceTypeID as SourceTypeId,LT.SourceTypeDescrip as SourceTypeDescrip ")
            .Append(" FROM   adSourceType LT ")
            .Append(" WHERE  LT.SourceCatagoryID='" & SourceCatagoryID & "' AND ")
            .Append(" LT.StatusId = '" & strActiveGUID & "' ")
            .Append(" AND LT.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(")  ")
            .Append(" ORDER BY SourceTypeDescrip ")
        End With
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllSourceAdvertisementByCampus(ByVal SourceTypeID As String, ByVal campusid As String, ByVal LeadId As String, Optional ByVal boolDisplayInActive As Boolean = False) As DataSet
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusid)

        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        'build the sql query
        With sb
            If Not LeadId = "" And boolDisplayInActive = True Then
                .Append(" Select DISTINCT LT.SourceAdvID as SourceAdvID,'(X) ' + LT.SourceAdvDescrip as SourceAdvDescrip from adLeads L,adSourceAdvertisement LT ")
                .Append(" where L.SourceAdvertisement=LT.SourceAdvID AND L.LeadId='" & LeadId & "' ")
                .Append(" AND  L.SourceAdvertisement NOT IN ")
                .Append(" ( ")
                .Append(" Select DISTINCT LT.SourceAdvID")
                .Append(" FROM   adSourceAdvertisement LT ")
                .Append(" WHERE  LT.SourceTypeID='" & SourceTypeID & "' AND ")
                .Append(" LT.StatusId = '" & strActiveGUID & "' ")
                .Append(" AND LT.CampGrpId in ('")
                .Append(strCampGrpId)
                .Append(")  ")
                .Append(" ) ")
                .Append(" Union ")
            End If
            .Append(" Select DISTINCT LT.SourceAdvID as SourceAdvID,LT.SourceAdvDescrip as SourceAdvDescrip")
            .Append(" FROM   adSourceAdvertisement LT ")
            .Append(" WHERE  LT.SourceTypeID='" & SourceTypeID & "' AND ")
            .Append(" LT.StatusId = '" & strActiveGUID & "' ")
            .Append(" AND LT.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(")  ")
            .Append(" ORDER BY SourceAdvDescrip ")
        End With
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllSourceType(ByVal SourceCatagoryID As String) As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            '.Append("Select Distinct '00000000-0000-0000-0000-000000000000' as SourceTypeId, 'Select' as SourceTypeDescrip ")
            ' .Append(" union ")
            .Append("select LT.SourceTypeID as SourceTypeId,LT.SourceTypeDescrip as SourceTypeDescrip ")
            .Append("FROM   adSourceType LT,syStatuses ST ")
            .Append("WHERE    LT.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' and LT.SourceCatagoryID=?")
            .Append(" ORDER BY LT.SourceTypeDescrip ")
        End With
        db.AddParameter("@SourceCatagoryID", SourceCatagoryID, DataAccess.OleDbDataType.OleDbString, 50)
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetAllArea() As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("select LT.ProgID,LT.ProgDescrip ")
            .Append("FROM   adPrograms LT,syStatuses ST ")
            .Append("WHERE    LT.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append(" ORDER BY LT.ProgDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    'Public Function GetAllProgramIDByArea() As DataSet
    '    'connect to the database
    '    Dim db As New DataAccess

    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder

    '    'build the sql query
    '    With sb
    '        .Append("select LT.advProgTypID,LT.Descrip ")
    '        .Append("FROM   advProgTyps LT ")
    '    End With

    '    'return dataset
    '    Return db.RunSQLDataSet(sb.ToString)
    'End Function
    Public Function GetAllSponsor() As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("select LT.AgencySpID,LT.AgencySpDescrip ")
            .Append("FROM   adAgencySponsors LT,syStatuses ST ")
            .Append("WHERE    LT.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active'")
            .Append(" ORDER BY LT.AgencySpDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllPhoneTypes() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   GE.PhoneTypeId, GE.PhoneTypeDescrip ")
            .Append("FROM     syPhoneType GE,syStatuses ST ")
            .Append("WHERE    GE.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active'")
            .Append(" ORDER BY GE.PhoneTypeDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetDocsStudent(ByVal ModuleID As Integer, ByVal StudentId As String, ByVal CampusId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        'Dim da6 As New OleDbDataAdapter
        Dim ds As DataSet
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select Distinct A.StudentDocId,B.StudentId,B.FirstName,  ")
            '            .Append(" ,B.FirstName + ' ' + B.LastName + ' ' + B.MiddleName + (Select Descrip from adReqs where adReqId = A.DocumentId) as Descrip from plStudentDocs A,arStudent B where A.StudentId=B.StudentId and A.ModuleID= ? ")
            .Append(" B.FirstName + ' ' + B.LastName + ' ' + B.MiddleName + ' - ' + (Select Descrip from adReqs where adReqId = A.DocumentId) as Descrip ")
            .Append(" from plStudentDocs A,arStudent B,arStuEnrollments C where A.StudentId=B.StudentId  and B.StudentId = C.StudentId ")

            If ModuleID >= 1 Then
                .Append(" and A.ModuleId= ? ")
            End If
            If Not StudentId = Guid.Empty.ToString Then
                .Append(" and B.StudentId = ? ")
            End If
            If Not CampusId = "" Then
                .Append(" and C.CampusId=? ")
            End If
            .Append(" order by B.FirstName ")
        End With

        'Add the EmployerContactId the parameter list
        If ModuleID >= 1 Then
            db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        End If

        If Not StudentId = Guid.Empty.ToString Then
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not CampusId = "" Then
            db.AddParameter("@campusid", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds

    End Function
    Public Function GetDocsByStudentAndModule(ByVal ModuleID As Integer, ByVal StudentId As String, ByVal CampusId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        'Dim da6 As New OleDbDataAdapter
        Dim ds As DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim dsGetCmpGrps As DataSet

        'Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)

        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            '.Append(" select Distinct SD.StudentDocId,SD.StudentId,'(X) ' + R.Descrip as DocumentDescription,(Select Distinct DocStatusDescrip from syDocStatuses where DocStatusId=SD.DocStatusId) as DocumentStatus ")
            '.Append(" from ")
            '.Append("  plStudentDocs  SD,(Select Distinct adReqId,Descrip,CampGrpId from adReqs R where StatusId='1AF592A6-8790-48EC-9916-5412C25EF49F') R, ")
            '.Append("  arStudent B,arStuEnrollments C where SD.StudentId=B.StudentId and B.StudentId = C.StudentId ")
            '.Append("  and SD.DocumentId = R.adReqId ")
            .Append(" select Distinct SD.StudentDocId,SD.StudentId,'(X) ' + R.Descrip as DocumentDescription,(Select Distinct DocStatusDescrip from syDocStatuses where DocStatusId=SD.DocStatusId) as DocumentStatus ")
            .Append(" from ")
            .Append("  plStudentDocs  SD,(Select Distinct adReqId,Descrip,CampGrpId from adReqs) R, ")
            .Append("  arStudent B,arStuEnrollments C where SD.StudentId=B.StudentId and B.StudentId = C.StudentId ")
            .Append("  and SD.DocumentId = R.adReqId ")
            .Append("  AND SD.DocumentId not in (Select Distinct adReqId from adReqs WHERE StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ")
            .Append("  CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(")) ")
            If Not ModuleID = 0 Then
                .Append(" and SD.ModuleID= ? ")
            End If
            If Not StudentId = Guid.Empty.ToString Then
                .Append(" and SD.StudentId = ? ")
            End If
            .Append(" Union ")
            .Append(" select Distinct A.StudentDocId,B.StudentId,  ")
            .Append(" R.Descrip as  DocumentDescription, ")
            .Append(" (Select Distinct DocStatusDescrip from syDocStatuses where DocStatusId=A.DocStatusId) as DocumentStatus ")
            .Append(" from plStudentDocs A,arStudent B,arStuEnrollments C, ")
            .Append(" (Select Distinct adReqId,Descrip,CampGrpId from adReqs R where StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965') R ")
            .Append(" where A.StudentId=B.StudentId  and B.StudentId = C.StudentId and A.DocumentId = R.adReqId ")
            If ModuleID >= 1 Then
                .Append(" and A.ModuleId= ? ")
            End If
            If Not StudentId = Guid.Empty.ToString Then
                .Append(" and B.StudentId = ? ")
            End If
            If Not strCampGrpId = "" Then
                .Append("   AND R.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" order by DocumentDescription ")
        End With


        If ModuleID >= 1 Then
            db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        End If

        If Not StudentId = Guid.Empty.ToString Then
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        If ModuleID >= 1 Then
            db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        End If

        If Not StudentId = Guid.Empty.ToString Then
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds

    End Function

    Public Function GetDocsStudentByModuleIdAndStatus(ByVal ModuleID As Integer, ByVal StudentId As String, ByVal CampusId As String, ByVal StatusId As String, ByVal TypeofRequirement As Integer, ByVal DocumentTypeId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        'Dim da6 As New OleDbDataAdapter
        Dim ds As DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        Dim dsGetCmpGrps As DataSet
        'Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)

        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If


        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select Distinct A.StudentDocId,B.StudentId,B.FirstName,  ")
            .Append(" B.FirstName + ' ' + B.LastName   + ' - ' + '(X) ' + R.Descrip as Descrip,ReqforEnrollment ,ReqforFinancialAid ,ReqforGraduation,R.Descrip AS DocDescrip  ")
            .Append(" from plStudentDocs A,arStudent B,arStuEnrollments C, ")
            .Append(" (Select Distinct adReqId,Descrip,CampGrpId,ReqforEnrollment ,ReqforFinancialAid ,ReqforGraduation from adReqs R) R ")
            .Append(" where ")

            .Append(" C.CampusId='" & CampusId & "' AND ")
            If Not StudentId = "" Then
                .Append(" B.StudentId = ?  AND ")
            End If
            .Append(" A.StudentId=B.StudentId and B.StudentId=C.StudentId and A.DocumentId=R.adReqId ")
            .Append("  AND A.DocumentId not in (Select Distinct adReqId from adReqs WHERE StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ")
            .Append("  CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(")) ")
            If ModuleID >= 1 Then
                .Append(" and A.ModuleId= ? ")
            End If
            'If Not strCampGrpId = "" Then
            '    .Append("   AND R.CampGrpId in 	('")
            '    .Append(strCampGrpId)
            '    .Append(") ")
            'End If
            If Not StatusId = "" Then
                .Append(" and A.DocStatusId=? ")
            End If

            If Not string.IsNullOrEmpty(DocumentTypeId) Then
                .Append(" and R.AdReqId ='" & DocumentTypeId & "'") 
            End If

            If TypeofRequirement = 1 Then
                .Append(" and R.reqforEnrollment =1 ")
            ElseIf TypeofRequirement = 2 Then
                .Append(" and R.reqforFinancialAid =1 ")
            ElseIf TypeofRequirement = 3 Then
                .Append(" and R.reqforGraduation =1 ")
            ElseIf TypeofRequirement = 4 Then
                .Append(" and R.reqforEnrollment =0  and R.reqforFinancialAid =0 and R.reqforGraduation =0 ")
            End If


            .Append(" Union ")
            .Append(" select Distinct A.StudentDocId,B.StudentId,B.FirstName,  ")
            .Append(" B.FirstName + ' ' + B.LastName   + ' - ' + R.Descrip as Descrip,ReqforEnrollment ,ReqforFinancialAid ,ReqforGraduation,R.Descrip AS DocDescrip  ")
            .Append(" from plStudentDocs A,arStudent B,arStuEnrollments C, ")
            .Append(" (Select Distinct adReqId,Descrip,CampGrpId,ReqforEnrollment ,ReqforFinancialAid ,ReqforGraduation from adReqs R where StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965') R ")
            .Append(" WHERE ")

            .Append(" C.CampusId='" & CampusId & "' AND ")
            If Not StudentId = "" Then
                .Append(" B.StudentId = ?  AND ")
            End If
            .Append(" A.StudentId=B.StudentId and B.StudentId=C.StudentId and A.DocumentId=R.adReqId and A.DocumentId=R.adReqId ")
            If ModuleID >= 1 Then
                .Append(" and A.ModuleId= ? ")
            End If
            If Not strCampGrpId = "" Then
                .Append("   AND R.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            If Not StatusId = "" Then
                .Append(" and A.DocStatusId=? ")
            End If

            If Not string.IsNullOrEmpty(DocumentTypeId) Then
                .Append(" and R.AdReqId ='" & DocumentTypeId & "'") 
            End If
            If TypeofRequirement = 1 Then
                .Append(" and R.reqforEnrollment =1 ")
            ElseIf TypeofRequirement = 2 Then
                .Append(" and R.reqforFinancialAid =1 ")
            ElseIf TypeofRequirement = 3 Then
                .Append(" and R.reqforGraduation =1 ")
            ElseIf TypeofRequirement = 4 Then
                .Append(" and R.reqforEnrollment =0  and R.reqforFinancialAid =0 and R.reqforGraduation =0 ")
            End If   

            .Append(" order by FirstName,DocDescrip ")
        End With

        If Not StudentId = "" Then
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If ModuleID >= 1 Then
            db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        End If
        If Not StatusId = "" Then
            db.AddParameter("@DocStatusId", StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If Not StudentId = "" Then
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If ModuleID >= 1 Then
            db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        End If
        If Not StatusId = "" Then
            db.AddParameter("@DocStatusId", StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
         
        db.OpenConnection() 

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds

    End Function
    Public Function GetAllDocsStudentByCampus(ByVal CampusId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        'Dim da6 As New OleDbDataAdapter
        Dim ds As DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        Dim dsGetCmpGrps As DataSet
        'Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)

        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select Distinct A.StudentDocId,B.StudentId,B.FirstName,  ")
            .Append(" B.FirstName + ' ' + B.LastName   + ' - ' + '(X) ' + R.Descrip as Descrip,R.Descrip AS DocDescrip ")
            .Append(" from plStudentDocs A,arStudent B,arStuEnrollments C, ")
            .Append(" (Select Distinct adReqId,Descrip,CampGrpId from adReqs R) R ")
            .Append(" where ")
            .Append(" A.StudentId=B.StudentId and B.StudentId=C.StudentId and A.DocumentId=R.adReqId and C.CampusId='" & CampusId & "' ")
            .Append("  AND A.DocumentId not in (Select Distinct adReqId from adReqs WHERE StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ")
            .Append("  CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(")) ")
            .Append(" Union ")
            .Append(" select Distinct A.StudentDocId,B.StudentId,B.FirstName,  ")
            .Append(" B.FirstName + ' ' + B.LastName   + ' - ' + R.Descrip as Descrip,R.Descrip AS DocDescrip  ")
            .Append(" from plStudentDocs A,arStudent B,arStuEnrollments C, ")
            .Append(" (Select Distinct adReqId,Descrip,CampGrpId from adReqs R) R ")
            .Append(" where ")
            .Append(" A.StudentId=B.StudentId and B.StudentId=C.StudentId and A.DocumentId=R.adReqId  and C.CampusId='" & CampusId & "' ")
            .Append(" AND A.DocumentId in (Select Distinct adReqId from adReqs WHERE StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ")
            .Append(" CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(")) ")
            .Append(" Order by  FirstName,DocDescrip ")
        End With

        'Add the EmployerContactId the parameter list
        'If Not CampusId = "" Then
        '    db.AddParameter("@campusid", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'End If
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds

    End Function
    Public Function GetDocsStudentNoModule(ByVal StudentId As String, ByVal CampusId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        'Dim da6 As New OleDbDataAdapter
        Dim ds As DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select A.StudentDocId,B.StudentId,  ")
            '            .Append(" ,B.FirstName + ' ' + B.LastName + ' ' + B.MiddleName + (Select Descrip from adReqs where adReqId = A.DocumentId) as Descrip from plStudentDocs A,arStudent B where A.StudentId=B.StudentId and A.ModuleID= ? ")
            .Append(" B.FirstName + ' ' + B.LastName + ' ' + B.MiddleName + ' - ' + (select descrip from adReqs where adReqId = A.DocumentId) as Descrip ")
            .Append(" from plStudentDocs A,arStudent B where A.StudentId=B.StudentId ")
            If Not StudentId = Guid.Empty.ToString Then
                .Append(" and B.StudentId = ? ")
            End If
            If Not CampusId = "" Then
                .Append(" and CampusId=? ")
            End If
        End With

        'Add the EmployerContactId the parameter list
        If Not StudentId = Guid.Empty.ToString Then
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not CampusId = "" Then
            db.AddParameter("@campusid", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds

    End Function
    Public Function GetDocsStudentByStatus(ByVal DocStatusId As String, ByVal ModuleID As Integer, ByVal StudentId As String, ByVal CampusId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        'Dim da6 As New OleDbDataAdapter
        Dim ds As DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select Distinct A.StudentDocId,B.StudentId,  ")
            .Append(" B.FirstName + ' ' + B.LastName + ' - ' + (Select Descrip from adReqs where adReqId = A.DocumentId) as Descrip ")
            .Append(" from plStudentDocs A,arStudent B,arStuEnrollments C where A.StudentId=B.StudentId and B.StudentId = C.StudentId ")
            If Not ModuleID = 0 Then
                .Append(" and A.ModuleID= ? ")
            End If
            If Not DocStatusId = "" Then
                .Append(" and A.DocStatusId= ?  ")
            End If
            If Not StudentId = Guid.Empty.ToString Then
                .Append(" and B.StudentId = ? ")
            End If
            If Not CampusId = "" Then
                .Append(" and C.CampusId=? ")
            End If
        End With

        'Add the EmployerContactId the parameter list
        If Not ModuleID = 0 Then
            db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        End If
        If Not DocStatusId = "" Then
            db.AddParameter("@DocStatusId", DocStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not StudentId = Guid.Empty.ToString Then
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not CampusId = "" Then
            db.AddParameter("@campusid", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds

    End Function
    Public Function GetDocsStudentByStatusAndModule(ByVal DocStatusId As String, ByVal ModuleID As Integer, ByVal StudentId As String, ByVal CampusId As String, ByVal TypeofRequirement As Integer, ByVal DocumentTypeId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        ' Dim da6 As New OleDbDataAdapter
        Dim ds As DataSet


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        Dim dsGetCmpGrps As DataSet

        'Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select Distinct SD.StudentDocId,SD.StudentId,'(X) ' + R.Descrip as DocumentDescription,(Select Distinct DocStatusDescrip from syDocStatuses where DocStatusId=SD.DocStatusId) as DocumentStatus ")
            .Append(" from ")
            .Append("  plStudentDocs  SD,(Select Distinct adReqId,Descrip,CampGrpId,reqforEnrollment,ReqforFinancialAid,reqforGraduation from adReqs) R, ")
            .Append("  arStudent B,arStuEnrollments C where SD.StudentId=B.StudentId and B.StudentId = C.StudentId ")
            .Append("  and SD.DocumentId = R.adReqId ")
            .Append("  AND SD.DocumentId not in (Select Distinct adReqId from adReqs WHERE StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ")
            .Append("  CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(")) ")
            If Not ModuleID = 0 Then
                .Append(" and SD.ModuleID= ? ")
            End If
            If Not DocStatusId = "" Then
                .Append(" and SD.DocStatusId= ?  ")
            End If
            If Not StudentId = Guid.Empty.ToString Then
                .Append(" and SD.StudentId = ? ")
            End If

            If Not string.IsNullOrEmpty(DocumentTypeId) Then
                .Append(" and R.AdReqId ='" & DocumentTypeId & "'") 
            End If

            If TypeofRequirement = 1 Then
                .Append(" and R.reqforEnrollment =1 ")
            ElseIf TypeofRequirement = 2 Then
                .Append(" and R.reqforFinancialAid =1 ")
            ElseIf TypeofRequirement = 3 Then
                .Append(" and R.reqforGraduation =1 ")
            ElseIf TypeofRequirement = 4 Then
                .Append(" and R.reqforEnrollment =0  and R.reqforFinancialAid =0 and R.reqforGraduation =0 ")
            End If

            'If Not strCampGrpId = "" Then
            '    .Append("   AND R.CampGrpId in 	('")
            '    .Append(strCampGrpId)
            '    .Append(") ")
            'End If
            .Append(" Union ")
            'Brings only Active Documents
            .Append(" select Distinct A.StudentDocId,B.StudentId,  ")
            .Append(" (Select Distinct Descrip from adReqs where adReqId = A.DocumentId) as DocumentDescription,(Select Distinct DocStatusDescrip from syDocStatuses where DocStatusId=A.DocStatusId) as DocumentStatus ")
            .Append(" from plStudentDocs A,arStudent B,arStuEnrollments C, ")
            .Append(" (Select Distinct adReqId,Descrip,CampGrpId,reqforEnrollment,ReqforFinancialAid,reqforGraduation from adReqs where StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965') R ")
            .Append(" where A.StudentId=B.StudentId and B.StudentId = C.StudentId and A.DocumentId=R.adReqId ")
            If Not ModuleID = 0 Then
                .Append(" and A.ModuleID= ? ")
            End If
            If Not DocStatusId = "" Then
                .Append(" and A.DocStatusId= ?  ")
            End If
            If Not StudentId = Guid.Empty.ToString Then
                .Append(" and B.StudentId = ? ")
            End If

            If Not string.IsNullOrEmpty(DocumentTypeId) Then
                .Append(" and R.AdReqId ='" & DocumentTypeId & "'") 
            End If

            If TypeofRequirement = 1 Then
                .Append(" and R.reqforEnrollment =1 ")
            ElseIf TypeofRequirement = 2 Then
                .Append(" and R.reqforFinancialAid =1 ")
            ElseIf TypeofRequirement = 3 Then
                .Append(" and R.reqforGraduation =1 ")
            ElseIf TypeofRequirement = 4 Then
                .Append(" and R.reqforEnrollment =0  and R.reqforFinancialAid =0 and R.reqforGraduation =0 ")
            End If
            If Not strCampGrpId = "" Then
                .Append("   AND R.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" order by DocumentDescription ")
        End With

        'Add the EmployerContactId the parameter list
        If Not ModuleID = 0 Then
            db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        End If
        If Not DocStatusId = "" Then
            db.AddParameter("@DocStatusId", DocStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not StudentId = Guid.Empty.ToString Then
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        'If Not CampusId = "" Then
        '    db.AddParameter("@campusid", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'End If
        If Not ModuleID = 0 Then
            db.AddParameter("@ModuleId", ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        End If
        If Not DocStatusId = "" Then
            db.AddParameter("@DocStatusId", DocStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not StudentId = Guid.Empty.ToString Then
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        'If Not CampusId = "" Then
        '    db.AddParameter("@campusid", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'End If
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds

    End Function

    Public Function GetAllLeadGroups() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   GE.LeadGrpId, GE.Descrip ")
            .Append("FROM     adLeadGroups GE,syStatuses ST ")
            .Append("WHERE    GE.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active'")
            .Append(" ORDER BY GE.Descrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllPhoneStatuses() As DataSet
        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'Build the sql query
        With sb
            .Append("SELECT   GE.PhoneStatusId, GE.PhoneStatusDescrip ")
            .Append("FROM     syPhoneStatuses GE,syStatuses ST ")
            .Append("WHERE    GE.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active'")
            .Append(" ORDER BY GE.PhoneStatusDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllAddressStatuses() As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   GE.AddressStatusId, GE.AddressStatusDescrip ")
            .Append("FROM     syAddressStatuses GE,syStatuses ST ")
            .Append("WHERE    GE.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active'")
            .Append(" ORDER BY GE.AddressStatusDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function

    Public Function GetAllFamilyIncome(Optional ByVal strStatus As String = "Active") As DataSet

        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'Build the sql query
        With sb
            .Append("SELECT   PR.FamilyIncomeId, PR.FamilyIncomeDescrip, ")
            .Append("         PR.ViewOrder, ST.StatusId, ST.Status ")
            .Append("FROM     syFamilyIncome PR, syStatuses ST ")
            .Append("WHERE    PR.StatusId = ST.StatusId ")
            If strStatus = "Active" Then
                .Append(" AND     ST.Status = 'Active'")
            ElseIf strStatus = "Inactive" Then
                .Append(" AND     ST.Status = 'Inactive'")
            End If
            '.Append(" ORDER BY PR.FamilyIncomeDescrip ")
            .Append(" ORDER BY PR.ViewOrder ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllPreviousEducation() As DataSet

        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'Build the sql query
        With sb
            .Append(" Select PR.EdLvlID,PR.EdLvlDescrip ")
            .Append(" FROM     adEdLvls PR, syStatuses ST ")
            .Append(" WHERE    PR.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active'")
            .Append(" ORDER BY PR.EdLvlDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function

    'Public Function GetAllPreviousEducationByProgramVersion(ByVal PrgVerId As String) As DataSet

    '    'connect to the database
    '    Dim db As New DataAccess

    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder

    '    'Build the sql query
    '    With sb
    '        .Append(" Select PR.EdLvlID,PR.EdLvlDescrip ")
    '        .Append(" FROM     adEdLvls PR, syStatuses ST ")
    '        .Append(" WHERE    PR.StatusId = ST.StatusId ")
    '        .Append(" AND     ST.Status = 'Active' and EdLvlId not in ")
    '        .Append(" (select PrevEduId from adPrgVerTest where PrgVerId=? and PrevEduId is not null) ")
    '        .Append(" ORDER BY PR.EdLvlDescrip ")
    '    End With
    '    db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    'return dataset
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
    'Public Function GetAllPreviousEducationByProgramVersion(ByVal PrgVerId As String, ByVal PreviousEducation As String) As DataSet

    '    'connect to the database
    '    Dim db As New DataAccess


    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder

    '    'Build the sql query
    '    With sb
    '        .Append(" Select PR.EdLvlID,PR.EdLvlDescrip ")
    '        .Append(" FROM     adEdLvls PR, syStatuses ST ")
    '        .Append(" WHERE    PR.StatusId = ST.StatusId ")
    '        .Append(" AND     ST.Status = 'Active' and EdLvlId not in ")
    '        .Append(" (select PrevEduId from adPrgVerTest where PrgVerId=? and PrevEduId is not null) ")
    '        .Append(" union ")
    '        .Append(" Select PR.EdLvlID,PR.EdLvlDescrip ")
    '        .Append(" FROM     adEdLvls PR, syStatuses ST ")
    '        .Append(" WHERE    PR.StatusId = ST.StatusId ")
    '        .Append(" AND     ST.Status = 'Active' and EdLvlId = ? ")
    '        .Append(" ORDER BY PR.EdLvlDescrip ")
    '    End With
    '    db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@PreviousEducation", PreviousEducation, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    'return dataset
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
    'Public Function GetAllPreviousEducationByAnyProgramVersion(ByVal PrgVerId As String) As Integer

    '    'connect to the database
    '    Dim db As New DataAccess


    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder
    '    Dim intCount As Integer

    '    'Build the sql query
    '    With sb
    '        .Append(" select Count(*) from adPrgVerTest where PrgVerId=? and PrevEduId is  null ")
    '    End With
    '    db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    intCount = db.RunParamSQLScalar(sb.ToString)

    '    Return intCount
    'End Function
    Public Function GetAllProgramsByGroup() As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Build the sql query
        With sb
            .Append(" Select PR.PrgGrpID,PR.PrgGrpDescrip ")
            .Append(" FROM   arPrgGrp PR, syStatuses ST ")
            .Append(" WHERE  PR.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active'")
            .Append(" ORDER BY PR.PrgGrpDescrip ")
        End With
        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllProgramsByGroupAndCampusId(ByVal campusid As String) As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Build the sql query
        With sb
            .Append(" SELECT PR.PrgGrpID,PR.PrgGrpDescrip ")
            .Append(" FROM   arPrgGrp AS PR ")
            .Append(" INNER JOIN syStatuses AS ST  ON PR.StatusId = ST.StatusId ")
            .Append(" INNER JOIN arprgversions AS V ON PR.PrgGrpID = V.PrgGrpId ")
            .Append(" INNER JOIN syCmpGrpCmps AS CG ON V.CampGrpId = CG.CampGrpID ")
            .Append(" INNER JOIN sycampuses AS C ON CG.CampusId = C.CampusId ")
            .Append(" WHERE   ST.Status = 'Active' and C.campusID = '")
            .Append(campusid)
            .Append("' ")
            .Append(" GROUP BY PR.PrgGrpID,PR.PrgGrpDescrip ")
            .Append(" ORDER BY PR.PrgGrpDescrip ")
        End With
        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllProgramsByGroupAndLead(ByVal LeadId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        'Build the sql query
        With sb
            If Not LeadId = "" Then
                .Append("  Select Distinct L.AreaId as PrgGrpId,'(X) ' + PR.PrgGrpDescrip as PrgGrpDescrip from adLeads L,arPrgGrp PR where L.AreaId=PR.PrgGrpID ")
                .Append("  AND L.LeadId='" & LeadId & "' ")
                .Append("  AND L.AreaId NOT IN ")
                .Append(" ( ")
                .Append(" Select Distinct PR.PrgGrpID ")
                .Append(" FROM   arPrgGrp PR ")
                .Append(" WHERE  PR.StatusId = '" & strActiveGUID & "' ")
                .Append(" ) ")
                .Append(" Union ")
            End If
            .Append(" Select Distinct PR.PrgGrpID as PrgGrpId,PR.PrgGrpDescrip as PrgGrpDescrip ")
            .Append(" FROM   arPrgGrp PR ")
            .Append(" WHERE  PR.StatusId = '" & strActiveGUID & "' ")
            .Append(" ORDER BY PrgGrpDescrip ")
        End With
        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllPrograms(ByVal PrgGrpID As String) As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append(" select s1.ProgId,s1.ProgDescrip from arPrograms S1,syStatuses ST where s1.StatusId = sT.StatusId  ")
            .Append(" and ProgId in (select ProgId from arPrgVersions where PrgGrpId = (select PrgGrpId from arPrgGrp where PrgGrpId=?)) ")
        End With
        'return dataset
        db.AddParameter("@PrgTypId", PrgGrpID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllProgramsByCampusAndUser(ByVal PrgGrpID As String, ByVal campusId As String, ByVal LeadId As String, Optional ByVal boolDisplayInActive As Boolean = False) As DataSet
        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        With sb
            ''Modified by saraswathi lakshmanan on May 20 2009
            ''The Shift is added to the program description

            If Not LeadId = "" And boolDisplayInActive = True Then
                '  .Append(" Select Distinct L.ProgramId as ProgId,'(X) ' + P.ProgDescrip as ProgDescrip ")
                .Append(" Select Distinct L.ProgramId as ProgId,'(X) ' +  ")
                .Append(" case when (select ShiftDescrip from arShifts where arShifts.shiftid=P.shiftid ) is null  then ")
                .Append(" P.ProgDescrip ")
                .Append(" else ")
                .Append(" P.ProgDescrip + ' (' + (select ShiftDescrip from arShifts where arShifts.shiftid=P.shiftid) + ')'  ")
                .Append(" end as ProgDescrip  ")

                .Append("  from adLeads L,arPrograms P where L.ProgramId=P.ProgId ")
                .Append(" AND L.LeadId='" & LeadId & "' ")
                .Append(" AND L.ProgramId NOT IN ")
                .Append(" ( ")
                .Append("   SELECT DISTINCT P.ProgId FROM arPrograms P,arPrgVersions PV,arPrgGrp PG ")
                .Append("   WHERE P.ProgId=PV.ProgId AND PV.PrgGrpId = PG.PrgGrpId AND  ")
                .Append("   PG.PrgGrpId='" & PrgGrpID & "' ")
                .Append("   AND P.StatusId = '" & strActiveGUID & "' ")
                .Append("   AND PV.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
                .Append(" ) ")
                .Append(" Union ")
            End If
            ' .Append("   SELECT DISTINCT P.ProgId as ProgId,P.ProgDescrip as ProgDescrip ")
            .Append("   SELECT DISTINCT P.ProgId as ProgId ")
            .Append(" ,case when (select ShiftDescrip from arShifts where arShifts.shiftid=P.shiftid ) is null  then ")
            .Append(" P.ProgDescrip ")
            .Append(" else ")
            .Append(" P.ProgDescrip + ' (' + (select ShiftDescrip from arShifts where arShifts.shiftid=P.shiftid) + ')'  ")
            .Append(" end as ProgDescrip  ")

            .Append(" FROM arPrograms P,arPrgVersions PV,arPrgGrp PG ")
            .Append("   WHERE P.ProgId=PV.ProgId AND PV.PrgGrpId = PG.PrgGrpId AND  ")
            .Append("   PG.PrgGrpId='" & PrgGrpID & "' ")
            .Append("   AND P.StatusId = '" & strActiveGUID & "' ")
            .Append("   AND PV.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("   ORDER BY ProgDescrip ")
        End With
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllProgramVersionByCampusAndUser(ByVal ProgID As String, ByVal campusId As String, ByVal LeadId As String, Optional ByVal boolDisplayInActive As Boolean = False) As DataSet
        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        With sb
            If Not LeadId = "" And boolDisplayInActive = True Then
                .Append(" Select Distinct L.PrgVerId as PrgVerId,'(X) ' + PV.PrgVerDescrip as PrgVerDescrip,")
                .Append(" case when (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=PV.ProgId) is null  then ")
                .Append(" PV.PrgVerDescrip ")
                .Append(" else ")
                .Append(" PV.PrgVerDescrip + ' (' + (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=PV.ProgId) + ')'  ")
                .Append(" end as PrgVerShiftDescrip  ")
                .Append(" from adLeads L,arPrgVersions PV where L.PrgVerId=PV.PrgVerID ")
                .Append(" AND L.LeadId='" & LeadId & "' ")
                .Append(" AND L.PrgVerId NOT IN ")
                .Append(" ( ")
                .Append("   SELECT DISTINCT PV.PrgVerId FROM arPrgVersions PV ")
                .Append("   WHERE PV.ProgId = '" & ProgID & "'  ")
                .Append("   AND PV.StatusId = '" & strActiveGUID & "' ")
                .Append("   AND PV.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
                .Append(" ) ")
                .Append(" Union ")
            End If
            .Append("   SELECT DISTINCT PV.PrgVerId as PrgVerId,PV.PrgVerDescrip as PrgVerDescrip,")
            .Append(" case when (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=PV.ProgId) is null  then ")
            .Append(" PV.PrgVerDescrip ")
            .Append(" else ")
            .Append(" PV.PrgVerDescrip + ' (' + (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=PV.ProgId) + ')'  ")
            .Append(" end as PrgVerShiftDescrip  ")
            .Append(" FROM arPrgVersions PV ")
            .Append("   WHERE PV.ProgId = '" & ProgID & "'  ")
            .Append("   AND PV.StatusId = '" & strActiveGUID & "' ")
            .Append("   AND PV.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("   ORDER BY PrgVerDescrip ")
        End With
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllSourceAdv(ByVal SourceTypeID As String) As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Build the sql query
        With sb
            '.Append(" Select Distinct '00000000-0000-0000-0000-000000000000' as SourceAdvId, 'Select' as SourceAdvDescrip ")
            '.Append(" union ")
            .Append(" Select PR.sourceadvid,PR.sourceadvdescrip ")
            .Append(" FROM   adSourceAdvertisement PR,syStatuses ST ")
            .Append(" WHERE  PR.StatusId = ST.StatusId and ST.Status='Active' and PR.SourceTypeID = ? ")
            .Append(" ORDER BY PR.sourceadvdescrip ")
        End With

        'return dataset
        db.AddParameter("@SourceTypeID", SourceTypeID.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllLeadTypes() As DataSet
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Build the sql query
        With sb
            .Append(" Select PR.LeadTypId,PR.LeadTypDescrip ")
            .Append(" FROM   adLeadTyps PR, syStatuses ST ")
            .Append(" WHERE  PR.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active'")
            .Append(" ORDER BY PR.LeadTypDescrip ")
        End With
        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllAdmissionReps() As DataSet
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim da6 As OleDbDataAdapter
        Dim ds As New DataSet
        'Build the sql query
        With sb
            .Append(" select hrEmployees.EmpId,LastName,FirstName,MI from hrEmployees,hrEmpRoles,syRoles,syStatuses ")
            .Append(" where hrEmployees.EmpId=hrEmpRoles.EmpId and hrEmpRoles.RoleId = syRoles.RoleId  ")
            .Append(" and syRoles.Role = 'Admissions Reps' and syRoles.StatusId = syStatuses.StatusId ")
            .Append(" AND syStatuses.Status = 'Active'")
            .Append(" ORDER BY lastname ")
        End With
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT")
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetAllAdmissionRepsByCampus(ByVal strCampusId As String, Optional ByVal repID As String = "") As DataSet
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Dim da6 As New OleDbDataAdapter
        'Dim ds As New DataSet
        'Build the sql query
        With sb
            .Append(" select distinct s1.fullname,s1.UserId from ")
            .Append(" syUsers s1,syUsersRolesCampGrps s2,syRoles s3,sySysRoles s4,syCmpGrpCmps s5,syStatuses s6 ")
            If repID = "" Then
                .Append(" where s1.UserId = s2.UserId and s2.RoleId = s3.RoleId ")
            Else
                'Modified by Balaji on 01/09/2006 to address the Time Out Expired error by QA
                .Append(" where (s1.UserId = ? or (s1.UserId = s2.UserId and s2.RoleId = s3.RoleId)) ")
                ''.Append(" where s1.UserId = s2.UserId and s2.RoleId = s3.RoleId and s1.UserId = ?  ")
            End If
            .Append(" and s3.SysRoleId = s4.SysRoleId and s2.CampGrpId = s5.CampGrpId and s4.StatusId = s6.StatusId ")
            .Append(" and s5.CampusId=? and s4.SysRoleId = 3 ")
            .Append(" order by s1.fullname")
            '.Append(" select hrEmployees.EmpId,LastName,FirstName,MI from hrEmployees,hrEmpRoles,syRoles,syStatuses ")
            '.Append(" where hrEmployees.EmpId=hrEmpRoles.EmpId and hrEmpRoles.RoleId = syRoles.RoleId  ")
            '.Append(" and syRoles.Role = 'Admissions Reps' and syRoles.StatusId = syStatuses.StatusId ")
            '.Append(" AND syStatuses.Status = 'Active'")
            '.Append(" ORDER BY lastname ")
        End With
        db.OpenConnection()
        If repID <> "" Then
            db.AddParameter("@RepId", repID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@CampusId", strCampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAllAdmissionRepsByCampusAndUserId(ByVal strCampusId As String, ByVal strUserId As String, Optional ByVal LeadId As String = "") As DataSet
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Dim da6 As New OleDbDataAdapter
        'Dim ds As New DataSet
        'Build the sql query
        With sb
            If Not LeadId = "" Then
                .Append(" Select distinct '(X) ' + U.fullname as FullName,U.UserId as UserId from adLeads L,syUsers U ")
                .Append(" where L.AdmissionsRep=U.UserId And AccountActive=0 ")
                .Append(" AND L.LeadId='" & LeadId & "' ")
                .Append(" Union ")
            End If
            .Append(" select distinct s1.fullname,s1.UserId from ")
            .Append(" syUsers s1,syUsersRolesCampGrps s2,syRoles s3,sySysRoles s4,syCmpGrpCmps s5,syStatuses s6 ")
            .Append(" where s1.UserId = s2.UserId and s2.RoleId = s3.RoleId ")
            .Append(" and s3.SysRoleId = s4.SysRoleId and s2.CampGrpId = s5.CampGrpId and s4.StatusId = s6.StatusId ")
            .Append(" and s5.CampusId=? and s4.SysRoleId = 3 and s1.AccountActive=1 and s6.Status='Active' ")
            .Append(" and s1.UserId=? ")
            .Append(" order by fullname")
        End With
        db.OpenConnection()
        db.AddParameter("@CampusId", strCampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@UserId", strUserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAllAdmissionRepsByCampusAndUserIdAndUserRoles(ByVal strCampusId As String, ByVal strUserId As String, ByVal RoleId As Integer, Optional ByVal LeadId As String = "") As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Dim da6 As New OleDbDataAdapter
        'Dim strInActiveStatus As String = AdvantageCommonValues.InactiveGuid
        'Dim ds As New DataSet
        'Build the sql query
        With sb
            If Not LeadId = "" Then
                .Append(" Select distinct '(X) ' + U.fullname as FullName,U.UserId as UserId from adLeads L,syUsers U ")
                .Append(" where L.AdmissionsRep=U.UserId And AccountActive=0 ")
                .Append(" AND L.LeadId='" & LeadId & "' ")
                .Append(" Union ")
            End If
            .Append(" select distinct s1.fullname as FullName,s1.UserId as UserId from ")
            .Append(" syUsers s1,syUsersRolesCampGrps s2,syRoles s3,sySysRoles s4,syCmpGrpCmps s5,syStatuses s6 ")
            .Append(" where s1.UserId = s2.UserId and s2.RoleId = s3.RoleId ")
            .Append(" and s3.SysRoleId = s4.SysRoleId and s2.CampGrpId = s5.CampGrpId and s4.StatusId = s6.StatusId ")
            .Append(" and s5.CampusId=? and s1.AccountActive=1 and s4.SysRoleId = 3 and s6.Status='Active' ")
            If Not RoleId = 8 Then
                .Append(" and s1.UserId=? ")
            End If
            .Append(" order by fullname")
        End With
        db.OpenConnection()
        db.AddParameter("@CampusId", strCampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If Not RoleId = 8 Then
            db.AddParameter("@UserId", strUserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        Finally
            db.CloseConnection()
        End Try
    End Function

    Public Function GetAllPlacementRepsByCampusAndUserId(ByVal strCampusId As String, ByVal strUserId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Dim da6 As New OleDbDataAdapter
        'Dim ds As New DataSet
        'Build the sql query
        With sb
            .Append(" select distinct s1.fullname,s1.UserId from ")
            .Append(" syUsers s1,syUsersRolesCampGrps s2,syRoles s3,sySysRoles s4,syCmpGrpCmps s5,syStatuses s6 ")
            .Append(" where s1.UserId = s2.UserId and s2.RoleId = s3.RoleId ")
            .Append(" and s3.SysRoleId = s4.SysRoleId and s2.CampGrpId = s5.CampGrpId and s4.StatusId = s6.StatusId ")
            .Append(" and s5.CampusId=? and s4.SysRoleId = 6 and s6.Status='Active'  ")
            '.Append(" and s1.UserId=? ")
            .Append(" order by s1.fullname")
        End With
        db.OpenConnection()
        db.AddParameter("@CampusId", strCampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@UserId", strUserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAllLeadsNoFilter() As DataSet

        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim da6 As OleDbDataAdapter
        Dim ds As New DataSet

        'Build the sql query
        With sb
            .Append(" select leadid,firstname,middlename,lastname from adLeads ")
        End With
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT")
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetAllLeads(ByVal LeadStatus As String) As DataSet

        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim da6 As OleDbDataAdapter
        Dim ds As New DataSet
        'Build the sql query
        With sb
            .Append(" select leadid,firstname,middlename,lastname from adLeads where LeadStatus = ? and ")
            .Append(" LeadId not in (select StudentID from arStudent) ")
            .Append(" order by firstname ")
        End With
        db.AddParameter("@LeadStatus", LeadStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT")
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetAllLeadsByCampus(ByVal LeadStatus As String, ByVal CampusId As String, ByVal userId As String, ByVal FirstName As String, ByVal LastName As String) As DataSet

        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim da6 As OleDbDataAdapter
        Dim ds As New DataSet
        'Build the sql query
        Const andOrOrOperator As String = " AND "
        Dim UserSecurity As New UserSecurityDB
        Dim boolDir As Boolean

        boolDir = UserSecurity.IsSAOrDirectorOfAdmissions(userId)

        With sb
            .Append(" select leadid,firstname,middlename,lastname,ssn from adLeads where  ")
            .Append(" LeadId not in (select StudentID from arStudent) and CampusId=? ")
            .Append(" and LeadStatus in (select Distinct A.StatusCodeId ")
            .Append("                       from syStatusCodes A,syLeadStatusChanges B,syCmpGrpCmps G,syLeadStatusChangePermissions P,syUsersRolesCampGrps U ")
            .Append("                       where sysStatusId <> 6 and A.StatusCodeId=B.OrigStatusId ")
            .Append("                       and B.NewStatusId IN (select X.StatusCodeId ")
            .Append("                                               from syStatusCodes X ")
            .Append("                                               where X.SysStatusId=6)")
            .Append("                       AND G.CampGrpId=B.CampGrpId")
            .Append("                       AND G.CampusId=?")
            If boolDir Then
                'Special case: Director of Admissions and the System Administrator
                .Append("                       AND U.RoleId IN (SELECT RoleId FROM syRoles WHERE SysRoleId=8 OR SysRoleId=1))")
            Else
                'All other users
                .Append("                       AND B.LeadStatusChangeId=P.LeadStatusChangeId")
                .Append("                       AND P.RoleId=U.RoleId")
                .Append("                       and U.UserId=?")
                .Append("                       AND P.StatusId=(SELECT StatusId FROM syStatuses WHERE Status='Active'))")
            End If
            If boolDir = False Then
                .Append(" and AdmissionsRep = ? ")
            End If
            If Not FirstName = "" Then
                .Append(andOrOrOperator)
                .Append(" FirstName like  + '%' + ? + '%'")
            End If
            If Not LastName = "" Then
                .Append(andOrOrOperator)
                .Append(" LastName like  + '%' + ? + '%'")
            End If
            If Not LeadStatus = "" Then
                .Append(andOrOrOperator)
                .Append(" LeadStatus = ? ")
            End If
            .Append(" order by firstname ")
        End With
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If boolDir = False Then
            db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not FirstName = "" Then
            db.AddParameter("@FirstName", FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If Not LastName = "" Then
            db.AddParameter("@LastName", LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If Not LeadStatus = "" Then
            db.AddParameter("@LeadStatus", LeadStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT")
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetAllLeadsByCampusUserAndNameFilters(ByVal LeadStatus As String, ByVal CampusId As String, ByVal userId As String, ByVal FirstName As String, ByVal LastName As String) As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim da6 As OleDbDataAdapter
        Dim ds As New DataSet
        'Build the sql query
        Const andOrOrOperator As String = " AND "
        Dim userSecurity As New UserSecurityDB
        Dim boolDir As Boolean

        boolDir = userSecurity.IsSAOrDirectorOfAdmissions(userId)

        'The Status Code Check has been left out as the Lead Status DDL brings only 
        'the leads based on status

        With sb

            .Append("   SELECT  ")
            .Append("       DISTINCT    ")
            .Append("       LeadId,     ")
            .Append("       FirstName,  ")
            .Append("       MiddleName, ")
            .Append("       LastName,   ")
            .Append("       SSN         ")
            .Append("   FROM            ")
            .Append("       adLeads     ")
            .Append("   WHERE           ")
            .Append("       CampusId = ? ")

            If boolDir = False Then
                .Append(andOrOrOperator)
                .Append("   AdmissionsRep=? ")
            End If
            If Not FirstName = "" Then
                .Append(andOrOrOperator)
                .Append(" FirstName like  + ? + '%'")
            End If
            If Not LastName = "" Then
                .Append(andOrOrOperator)
                .Append(" LastName like  + ? + '%'")
            End If
            If Not LeadStatus = "" Then
                .Append(andOrOrOperator)
                .Append(" LeadStatus = ? ")
            Else
                .Append(andOrOrOperator)
                .Append(" LeadId not in (select Distinct LeadId from arStuEnrollments where LeadId is not null) ")
            End If
            .Append(" ORDER BY  ")
            .Append("   FIRSTNAME ")
        End With
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If boolDir = False Then
            db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not FirstName = "" Then
            db.AddParameter("@FirstName", FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If Not LastName = "" Then
            db.AddParameter("@LastName", LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If Not LeadStatus = "" Then
            db.AddParameter("@LeadStatus", LeadStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT")
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetAllRequirementBydate() As DataSet
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append("select startdate,enddate,adReqId,adReqEffectiveDateId from adReqsEffectiveDates ")
        End With
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllUnEnrolledLeadsByCampus(ByVal LeadId As String, ByVal CampusId As String, ByVal userId As String, ByVal FirstName As String, ByVal LastName As String, ByVal ssn As String, ByVal leadstatus As String) As DataSet

        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim da6 As OleDbDataAdapter
        Dim ds As New DataSet
        'Build the sql query
        Const andOrOrOperator As String = " AND "

        With sb
            .Append(" select leadid,firstname,middlename,lastname,ssn from adLeads where  ")

            'Select Any lead Status That is Not Enrolled
            .Append("  LeadStatus IN (SELECT StatusCodeId FROM dbo.sySysStatus A,syStatusCodes B WHERE A.SysStatusId=B.SysStatusId and StatusLevelID=1 AND A.SysStatusId not IN (6,17,18)) ")
            .Append("  and CampusId=? ")
            .Append("  AND (                                                                         ")
            .Append("        AdmissionsRep = ? ") 'UserID
            .Append("   OR  (SELECT COUNT( DISTINCT syUsersRolesCampGrps.UserID) FROM dbo.syUsersRolesCampGrps  ")
            .Append("         JOIN syRoles ON syRoles.RoleId = syUsersRolesCampGrps.RoleId  ")
            .Append("         JOIN syUsers ON syUsers.UserId = syUsersRolesCampGrps.UserId  ")
            .Append("         WHERE dbo.syRoles.SysRoleId = 8  ") 'Director of Admissions
            .Append("         AND dbo.syUsers.CampusId = ?    ") ' CampusId
            .Append("         AND dbo.syUsersRolesCampGrps.UserId = ?  ") ' UserId
            .Append("         ) > 0 ")
            .Append("     )  ")



            If Not FirstName = "" Then
                .Append(andOrOrOperator)
                .Append(" FirstName like + ? + '%'")
            End If
            If Not LastName = "" Then
                .Append(andOrOrOperator)
                .Append(" LastName like  + ? + '%'")
            End If
            If Not LeadId = "" Then
                .Append(andOrOrOperator)
                .Append(" LeadId = ? ")
            End If
            If Not ssn = "" Then
                .Append(andOrOrOperator)
                .Append(" SSN = ? ")
            End If
            If Not leadstatus = "" Then
                .Append(andOrOrOperator)
                .Append(" LeadStatus = ? ")
            End If
            .Append(" order by firstname ")
        End With
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId2", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@UserId2", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If Not FirstName = "" Then
            db.AddParameter("@FirstName", FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If Not LastName = "" Then
            db.AddParameter("@LastName", LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If Not LeadId = "" Then
            db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not ssn = "" Then
            db.AddParameter("@SSN", ssn, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not leadstatus = "" Then
            db.AddParameter("@LeadStatus", leadstatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT")
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetAllLeadsByAdmissionRep(ByVal AdmissionRepId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim da6 As OleDbDataAdapter
        Dim ds As New DataSet
        'Build the sql query
        With sb
            .Append(" select leadid,firstname,middlename,lastname from adLeads where AdmissionsRep = ? ")
        End With
        db.AddParameter("@AdmissionsRep", AdmissionRepId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT")
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetAllEnrolledLeads(ByVal LeadStatus As String, ByVal StartDate As String, ByVal EndDate As String, ByVal UserId As String, ByVal CampusId As String, Optional ByVal FirstName As String = "", Optional ByVal LastName As String = "") As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim da6 As OleDbDataAdapter
        Dim ds As New DataSet
        Dim userSecurity As New UserSecurityDB
        Dim boolDir As Boolean

        boolDir = userSecurity.IsSAOrDirectorOfAdmissions(UserId)
        'Build the sql query
        With sb
            .Append(" select leadid,firstname,middlename,lastname,ssn from adLeads  ")
            .Append(" where ")
            .Append(" LeadStatus in  (select Distinct StatusCodeId from syStatusCodes t1,sySysStatus t2,syStatusLevels t3 ")
            .Append(" where t1.SysStatusId = t2.SysStatusId and t2.StatusLevelId = t3.StatusLevelId and  ")
            .Append(" t2.SysStatusId=6) ")
            If FirstName <> "" Then
                .Append(" and firstname like  + '' + ? + '%'")
            End If
            If LastName <> "" Then
                .Append(" and lastName like  + '' + ? + '%'")
            End If

            If boolDir = False Then
                .Append(" and AdmissionsRep = ? ")
            End If
            .Append(" and CampusId = ? ")
            If Not StartDate = "" And Not EndDate = "" Then
                .Append(" and LeadId in (select LeadId from arStuEnrollments  where EnrollDate >= ? and EnrollDate <= ?) ")
            End If
            .Append("  AND leadid not IN (SELECT leadid FROM dbo.arStuEnrollments WHERE StuEnrollId IN(SELECT tb1.StuEnrollId ")
            .Append("FROM (SELECT StuEnrollId, COUNT(t.TransCodeId) AS CountFor20 ")
            .Append("FROM dbo.saTransactions t INNER JOIN dbo.saTransCodes c ON t.TransCodeId=c.TransCodeId INNER JOIN dbo.saSysTransCodes sc ON c.SysTransCodeId=sc.SysTransCodeId ")
            .Append("WHERE Voided = 0 AND sc.Description='Applicant Fee' ")
            .Append("GROUP BY StuEnrollId,sc.SysTransCodeId) as tb1 ")
            .Append("INNER Join (SELECT t.StuEnrollId,COUNT(t.StuEnrollId) AS Counttotal ")
            .Append("FROM dbo.saTransactions t INNER JOIN dbo.saTransCodes c ON t.TransCodeId=c.TransCodeId INNER JOIN dbo.saSysTransCodes sc ON c.SysTransCodeId=sc.SysTransCodeId ")
            .Append("WHERE Voided = 0 GROUP BY StuEnrollId	) AS tb2 ON tb1.StuEnrollId=tb2.StuEnrollId ")
            .Append("WHERE tb2.Counttotal > tb1.CountFor20) AND LeadId IS NOT null) ")
            .Append(" AND leadid NOT IN (SELECT leadid FROM dbo.arStuEnrollments WHERE StuEnrollId IN(SELECT StuEnrollId FROM dbo.arResults) AND LeadId IS NOT null) ")
            .Append(" AND leadid NOT IN (SELECT leadid FROM dbo.arStuEnrollments WHERE StuEnrollId IN(SELECT StuEnrollId FROM dbo.arTransferGrades) AND LeadId IS NOT null) ")
            .Append(" AND leadid NOT IN (SELECT leadid FROM dbo.arStuEnrollments WHERE StuEnrollId IN(SELECT StuEnrollId FROM dbo.atClsSectAttendance) AND LeadId IS NOT null) ")
            .Append(" AND leadid NOT IN (SELECT leadid FROM dbo.arStuEnrollments WHERE StuEnrollId IN(SELECT StuEnrollId FROM dbo.arStudentClockAttendance WHERE ActualHours <> 9999.00) AND LeadId IS NOT null) ")
            .Append(" AND leadid IN     (SELECT leadId FROM dbo.arStuEnrollments e JOIN syStatusCodes sc ON e.StatusCodeId = sc.StatusCodeId JOIN sySysStatus sss ON sc.SysStatusId = sss.SysStatusId WHERE InSchool = 1 )")
            .Append(" order by FirstName, LastName ")
        End With
        If FirstName <> "" Then
            db.AddParameter("@FirstName", FirstName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If LastName <> "" Then
            db.AddParameter("@LastName", LastName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        If boolDir = False Then
            db.AddParameter("@userid", UserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@cmpid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If Not StartDate = "" And Not EndDate = "" Then
            db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@EndDate", EndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT")
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        Return ds
    End Function

    Public Function GetLeadsByCampusforSearchCtl(ByVal CampusId As String) As DataSet

        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim da6 As OleDbDataAdapter
        Dim ds As New DataSet
        'Build the sql query
        With sb

            .Append(" Select leadid, coalesce(FirstName,'') + ' ' + coalesce(lastname,'') AS FullName from adLeads where campusid = ? ")
            '.Append(" select leadid,firstname,middlename,lastname from adLeads where campusid = ? ")
        End With
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "Leads")
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        Return ds

    End Function


    Public Function GetLeadInfo(ByVal LeadId As String, Optional ByVal useRegent As Boolean = False) As LeadMasterInfo
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" Select A.LeadId,A.FirstName,A.LastName,A.MiddleName,A.SSN,A.Phone,A.Phone2,A.HomeEmail, ")
            .Append(" A.WorkEmail, ")
            .Append(" A.BirthDate,A.AssignedDate,A.DateApplied,A.Children,  ")
            .Append(" A.SourceCategoryID, A.SourceTypeID, ")
            .Append(" A.SourceDate,A.AreaID,A.ProgramID,A.ExpectedStart, ")
            .Append(" A.CampusId, A.ForeignPhone,A.ForeignPhone2, A.DefaultPhone,A.ForeignZip, A.OtherState, ")
            .Append(" A.ModDate,A.LeadGrpId, ")
            .Append(" A.Address1,A.Address2,A.City,A.Zip, ")
            .Append(" A.StateId, ")
            .Append(" (select Distinct StateDescrip from syStates where StateId=A.StateId) as StateDescrip, ")
            .Append(" A.AddressType, ")
            .Append(" (select Distinct AddressDescrip from plAddressTypes where AddressTypeId=A.AddressType) as AddressTypeDescrip, ")
            .Append(" A.Prefix, ")
            .Append(" (select Distinct PrefixDescrip from syPrefixes where PrefixId=A.Prefix) as PrefixDescrip, ")
            .Append(" A.Suffix, ")
            .Append(" (select Distinct SuffixDescrip from sySuffixes where SuffixId=A.Suffix) as SuffixDescrip, ")
            .Append(" A.AdmissionsRep, ")
            .Append(" (select Distinct FullName from syUsers where UserId=A.AdmissionsRep) as AdmissionsRepDescrip, ")
            .Append(" A.Gender, ")
            .Append(" (select Distinct GenderDescrip from adGenders where GenderId=A.Gender) as GenderDescrip, ")
            .Append(" A.Race, ")
            .Append(" (select Distinct EthCodeDescrip from adEthCodes where EthCodeId=A.Race) as EthCodeDescrip, ")
            .Append(" A.MaritalStatus, ")
            .Append(" (select Distinct MaritalStatDescrip from adMaritalStatus where MaritalStatId=A.MaritalStatus) as MaritalStatusDescrip, ")
            .Append(" A.FamilyIncome, ")
            .Append(" (select Distinct FamilyIncomeDescrip from syFamilyIncome where FamilyIncomeId=A.FamilyIncome) as FamilyIncomeDescrip, ")
            .Append(" A.PhoneType, ")
            .Append(" A.PhoneType2, ")
            .Append(" (select Distinct PhoneTypeDescrip from syPhoneType where PhoneTypeId=A.PhoneType) as PhoneTypeDescrip, ")
            .Append(" (select Distinct PhoneTypeDescrip from syPhoneType where PhoneTypeId=A.PhoneType2) as PhoneTypeDescrip2, ")
            .Append(" A.ShiftID, ")
            .Append(" (select Distinct ShiftDescrip from arShifts where ShiftId=A.ShiftId) as ShiftDescrip, ")
            .Append(" A.Nationality, ")
            .Append(" (select Distinct NationalityDescrip from adNationalities where NationalityId=A.Nationality) as NationalityDescrip, ")
            .Append(" A.Citizen, ")
            .Append(" (select Distinct CitizenShipDescrip from adCitizenships where CitizenshipId=A.Citizen) as CitizenDescrip, ")
            .Append(" A.DrivLicStateID, ")
            .Append(" (select Distinct StateDescrip from syStates where StateId=A.DrivLicStateId) as DriverLicenseStateDescrip, ")
            .Append(" A.DrivLicNumber, A.AlienNumber, ")
            .Append(" A.Comments,A.SourceAdvertisement,A.CampusId,A.PrgVerId, ")
            .Append(" A.Country, ")
            .Append(" (select Distinct CountryDescrip from adCountries where CountryId=A.Country) as CountryDescrip, ")
            .Append(" A.County, ")
            .Append(" (select Distinct CountyDescrip from adCounties where CountyId=A.County) as CountyDescrip, ")
            .Append(" A.PreviousEducation, ")
            .Append(" (select Distinct EdLvlDescrip from adEdLvls where EdLvlId=A.PreviousEducation) as EdLvlDescrip, ")
            .Append(" A.AddressStatus, ")
            .Append(" (select Distinct Status from syStatuses where StatusId=A.AddressStatus) as AddressStatusDescrip, ")
            .Append(" A.PhoneStatus, ")
            .Append(" A.PhoneStatus2, ")
            .Append(" (select Distinct Status from syStatuses where StatusId=A.PhoneStatus) as PhoneStatusDescrip, ")
            .Append(" (select Distinct Status from syStatuses where StatusId=A.PhoneStatus2) as PhoneStatusDescrip2, ")
            .Append(" A.LeadStatus, ")
            .Append(" (select StatusCodeDescrip from syStatusCodes where StatusCodeId = A.LeadStatus) as StatusDescrip, ")
            .Append(" DependencyTypeId, ")
            .Append(" (select Distinct Descrip from adDependencyTypes where DependencyTypeId=A.DependencyTypeId) as DependencyTypeDescrip, ")
            .Append(" GeographicTypeId, ")
            .Append(" (select Distinct Descrip from adGeographicTypes where GeographicTypeId=A.GeographicTypeId) as GeographicTypeDescrip, ")
            .Append(" A.Sponsor, ")
            .Append(" (select Distinct AgencySpDescrip from adAgencySponsors where AgencySpId=A.Sponsor) as SponsorTypeDescrip, ")
            .Append(" AdminCriteriaId,InquiryTime,AdvertisementNote,HousingId, ")
            .Append(" (select Distinct Descrip from adAdminCriteria where AdminCriteriaId=A.AdminCriteriaId) as AdminCriteriaDescrip, ")
            .Append(" (select Distinct Descrip from arHousing where HousingId=A.HousingId) as HousingTypeDescrip, ")
            .Append(" DegCertSeekingId, ")
            .Append(" (select Distinct Descrip from adDegCertSeeking where DegCertSeekingId=A.DegCertSeekingId) as DegCertSeekingDescrip, ")
            .Append(" (select count(*) from syStatusCodes t1,sySysStatus t2 where t1.SysStatusId=t2.SysStatusId and t2.SysStatusDescrip='Enrolled' and t1.StatusCodeId=A.LeadStatus) as EnrolledStatus ")
            If useRegent = True Then
                .Append(" ,entranceinterviewdate,highschoolprogramcode ")
            End If
            .Append(" from adLeads A ")
            .Append(" where A.LeadId = ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim leadInfo As New LeadMasterInfo
        While dr.Read()
            'set properties with data from DataReader
            With leadInfo
                .IsInDB = True
                .LeadMasterID = CType(dr("LeadId"), Guid).ToString()
                If Not (dr("FirstName") Is DBNull.Value) Then .FirstName = CType(dr("FirstName"), String).ToString Else .FirstName = ""
                If Not (dr("LastName") Is DBNull.Value) Then .LastName = CType(dr("LastName"), String).ToString Else .LastName = ""
                If Not (dr("Middlename") Is DBNull.Value) Then .MiddleName = CType(dr("MiddleName"), String).ToString Else .MiddleName = ""
                If Not (dr("SSN") Is DBNull.Value) Then .SSN = CType(dr("SSN"), String).ToString
                If Not (dr("Phone") Is DBNull.Value) Then .Phone = CType(dr("Phone"), String).ToString
                If Not (dr("Phone2") Is DBNull.Value) Then .Phone2 = CType(dr("Phone2"), String).ToString
                If Not (dr("DefaultPhone") Is DBNull.Value) Then .DefaultPhone = dr("DefaultPhone") Else .DefaultPhone = 1
                If Not (dr("HomeEmail") Is DBNull.Value) Then .HomeEmail = CType(dr("HomeEmail"), String).ToString
                If Not (dr("Address1") Is DBNull.Value) Then .Address1 = CType(dr("Address1"), String).ToString
                If Not (dr("Address2") Is DBNull.Value) Then .Address2 = CType(dr("Address2"), String).ToString
                If Not (dr("City") Is DBNull.Value) Then .City = CType(dr("City"), String).ToString
                If Not (dr("stateid") Is DBNull.Value) Then .State = CType(dr("StateId"), Guid).ToString Else .State = ""
                If Not (dr("zip") Is DBNull.Value) Then .Zip = CType(dr("zip"), String).ToString

                If Not (dr("Country") Is DBNull.Value) Then .Country = CType(dr("Country"), Guid).ToString Else .Country = ""
                If Not (dr("County") Is DBNull.Value) Then .County = CType(dr("County"), Guid).ToString Else .County = ""

                If Not (dr("BirthDate") Is DBNull.Value) Then .BirthDate = CType(dr("BirthDate"), String).ToString
                If Not (dr("AssignedDate") Is DBNull.Value) Then .AssignmentDate = CType(dr("AssignedDate"), String).ToString
                If Not (dr("DateApplied") Is DBNull.Value) Then .AppliedDate = CType(dr("DateApplied"), String).ToString
                If Not (dr("SourceDate") Is DBNull.Value) Then .SourceDate = CType(dr("SourceDate"), String).ToString
                If Not (dr("ExpectedStart") Is DBNull.Value) Then .ExpectedStart = CType(dr("ExpectedStart"), String).ToString
                If Not (dr("Comments") Is DBNull.Value) Then .Notes = CType(dr("Comments"), String).ToString
                If Not (dr("Children") Is DBNull.Value) Then .Children = CType(dr("Children"), String).ToString

                If Not (dr("WorkEmail") Is DBNull.Value) Then .WorkEmail = CType(dr("WorkEmail"), String).ToString
                If Not (dr("LeadStatus") Is DBNull.Value) Then .Status = CType(dr("LeadStatus"), Guid).ToString Else .Status = ""
                If Not (dr("AddressType") Is DBNull.Value) Then .AddressType = CType(dr("AddressType"), Guid).ToString Else .AddressType = ""
                If Not (dr("Prefix") Is DBNull.Value) Then .Prefix = CType(dr("Prefix"), Guid).ToString Else .Prefix = ""
                If Not (dr("Suffix") Is DBNull.Value) Then .Suffix = CType(dr("Suffix"), Guid).ToString Else .Suffix = ""
                If Not (dr("Sponsor") Is DBNull.Value) Then .Sponsor = CType(dr("Sponsor"), Guid).ToString Else .Sponsor = ""
                If Not (dr("SponsorTypeDescrip") Is DBNull.Value) Then .SponsorTypeDescrip = dr("SponsorTypeDescrip").ToString Else .SponsorTypeDescrip = ""
                If Not (dr("AdmissionsRep") Is DBNull.Value) Then .AdmissionsRep = CType(dr("AdmissionsRep"), Guid).ToString Else .AdmissionsRep = ""
                If Not (dr("Gender") Is DBNull.Value) Then .Gender = CType(dr("Gender"), Guid).ToString Else .Gender = ""
                If Not (dr("Race") Is DBNull.Value) Then .Race = CType(dr("Race"), Guid).ToString Else .Race = ""
                If Not (dr("MaritalStatus") Is DBNull.Value) Then .MaritalStatus = CType(dr("MaritalStatus"), Guid).ToString Else .MaritalStatus = ""
                If Not (dr("FamilyIncome") Is DBNull.Value) Then .FamilyIncome = CType(dr("FamilyIncome"), Guid).ToString Else .FamilyIncome = Guid.Empty.ToString
                If Not (dr("PhoneType") Is DBNull.Value) Then .PhoneType = CType(dr("PhoneType"), Guid).ToString Else .PhoneType = ""
                If Not (dr("PhoneType2") Is DBNull.Value) Then .PhoneType2 = CType(dr("PhoneType2"), Guid).ToString Else .PhoneType2 = ""
                If Not (dr("SourceCategoryId") Is DBNull.Value) Then .SourceCategory = CType(dr("SourceCategoryId"), Guid).ToString Else .SourceCategory = ""
                If Not (dr("SourceTypeId") Is DBNull.Value) Then .SourceType = CType(dr("SourceTypeId"), Guid).ToString Else .SourceType = ""
                If Not (dr("AreaId") Is DBNull.Value) Then .Area = CType(dr("AreaId"), Guid).ToString Else .Area = ""
                If Not (dr("ProgramId") Is DBNull.Value) Then .ProgramID = CType(dr("ProgramId"), Guid).ToString Else .ProgramID = ""
                If Not (dr("PrgVerId") Is DBNull.Value) Then .PrgVerId = CType(dr("PrgVerId"), Guid).ToString Else .PrgVerId = ""
                If Not (dr("ShiftId") Is DBNull.Value) Then .ShiftID = CType(dr("ShiftId"), Guid).ToString Else .ShiftID = ""
                If Not (dr("Nationality") Is DBNull.Value) Then .Nationality = CType(dr("Nationality"), Guid).ToString Else .Nationality = ""
                If Not (dr("Citizen") Is DBNull.Value) Then .Citizen = CType(dr("Citizen"), Guid).ToString Else .Citizen = ""
                If Not (dr("DrivLicStateId") Is DBNull.Value) Then .DriverLicState = CType(dr("DrivLicStateId"), Guid).ToString Else .DriverLicState = ""
                If Not (dr("DrivLicNumber") Is DBNull.Value) Then .DriverLicNumber = CType(dr("DrivLicNumber"), String).ToString Else .DriverLicNumber = ""
                If Not (dr("AlienNumber") Is DBNull.Value) Then .AlienNumber = CType(dr("AlienNumber"), String).ToString Else .AlienNumber = ""
                If Not (dr("SourceAdvertisement") Is DBNull.Value) Then .SourceAdvertisement = CType(dr("SourceAdvertisement"), Guid).ToString Else .SourceAdvertisement = ""
                If Not (dr("PreviousEducation") Is DBNull.Value) Then .PreviousEducation = CType(dr("PreviousEducation"), Guid).ToString Else .PreviousEducation = ""
                If Not (dr("CampusId") Is DBNull.Value) Then .CampusId = CType(dr("CampusId"), Guid).ToString
                If Not (dr("ForeignPhone") Is DBNull.Value) Then .ForeignPhone = dr("ForeignPhone") Else .ForeignPhone = 0
                If Not (dr("ForeignPhone2") Is DBNull.Value) Then .ForeignPhone2 = dr("ForeignPhone2") Else .ForeignPhone2 = 0
                If Not (dr("ForeignZip") Is DBNull.Value) Then .ForeignZip = dr("ForeignZip") Else .ForeignZip = 0
                If Not (dr("OtherState") Is DBNull.Value) Then .OtherState = CType(dr("OtherState"), String).ToString
                If Not (dr("ModDate") Is DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = Date.MinValue
                If Not (dr("StatusDescrip") Is DBNull.Value) Then .StatusDescrip = dr("StatusDescrip") Else .StatusDescrip = ""
                If Not (dr("AddressStatus") Is DBNull.Value) Then .AddressStatus = CType(dr("AddressStatus"), Guid).ToString Else .AddressStatus = ""
                If Not (dr("PhoneStatus") Is DBNull.Value) Then .PhoneStatus = CType(dr("PhoneStatus"), Guid).ToString Else .PhoneStatus = ""
                If Not (dr("PhoneStatus2") Is DBNull.Value) Then .PhoneStatus2 = CType(dr("PhoneStatus2"), Guid).ToString Else .PhoneStatus2 = ""
                If Not (dr("LeadGrpId") Is DBNull.Value) Then .LeadGrpId = CType(dr("LeadGrpId"), Guid).ToString Else .LeadGrpId = ""

                If Not (dr("DependencyTypeId") Is DBNull.Value) Then .DependencyTypeId = CType(dr("DependencyTypeId"), Guid).ToString Else .DependencyTypeId = ""
                If Not (dr("GeographicTypeId") Is DBNull.Value) Then .GeographicTypeId = CType(dr("GeographicTypeId"), Guid).ToString Else .GeographicTypeId = ""
                If Not (dr("AdminCriteriaId") Is DBNull.Value) Then .AdminCriteriaId = CType(dr("AdminCriteriaId"), Guid).ToString Else .AdminCriteriaId = ""

                If Not (dr("StateDescrip") Is DBNull.Value) Then .StateDescrip = CType(dr("StateDescrip"), String).ToString Else .StateDescrip = ""
                If Not (dr("StateDescrip") Is DBNull.Value) Then .StateDescrip = CType(dr("StateDescrip"), String).ToString Else .StateDescrip = ""
                If Not (dr("AddressTypeDescrip") Is DBNull.Value) Then .AddressTypeDescrip = CType(dr("AddressTypeDescrip"), String).ToString Else .AddressTypeDescrip = ""
                If Not (dr("PrefixDescrip") Is DBNull.Value) Then .PrefixDescrip = CType(dr("PrefixDescrip"), String).ToString Else .PrefixDescrip = ""
                If Not (dr("SuffixDescrip") Is DBNull.Value) Then .SuffixDescrip = CType(dr("SuffixDescrip"), String).ToString Else .SuffixDescrip = ""
                If Not (dr("AdmissionsRepDescrip") Is DBNull.Value) Then .AdmissionRepsDescrip = CType(dr("AdmissionsRepDescrip"), String).ToString Else .AdmissionRepsDescrip = ""
                If Not (dr("GenderDescrip") Is DBNull.Value) Then .GenderDescrip = CType(dr("GenderDescrip"), String).ToString Else .GenderDescrip = ""
                If Not (dr("EthCodeDescrip") Is DBNull.Value) Then .EthCodeDescrip = CType(dr("EthCodeDescrip"), String).ToString Else .EthCodeDescrip = ""
                If Not (dr("MaritalStatusDescrip") Is DBNull.Value) Then .MaritalStatusDescrip = CType(dr("MaritalStatusDescrip"), String).ToString Else .MaritalStatusDescrip = ""
                If Not (dr("FamilyIncomeDescrip") Is DBNull.Value) Then .FamilyIncomeDescrip = CType(dr("FamilyIncomeDescrip"), String).ToString Else .FamilyIncomeDescrip = ""
                If Not (dr("PhoneTypeDescrip") Is DBNull.Value) Then .PhoneTypeDescrip = CType(dr("PhoneTypeDescrip"), String).ToString Else .PhoneTypeDescrip = ""
                If Not (dr("ShiftDescrip") Is DBNull.Value) Then .ShiftDescrip = CType(dr("ShiftDescrip"), String).ToString Else .ShiftDescrip = ""
                If Not (dr("NationalityDescrip") Is DBNull.Value) Then .NationalityDescrip = CType(dr("NationalityDescrip"), String).ToString Else .NationalityDescrip = ""
                If Not (dr("CitizenDescrip") Is DBNull.Value) Then .CitizenDescrip = CType(dr("CitizenDescrip"), String).ToString Else .CitizenDescrip = ""
                If Not (dr("DriverLicenseStateDescrip") Is DBNull.Value) Then .DriverLicenseStateDescrip = CType(dr("DriverLicenseStateDescrip"), String).ToString Else .DriverLicenseStateDescrip = ""
                If Not (dr("CountryDescrip") Is DBNull.Value) Then .CountryDescrip = CType(dr("CountryDescrip"), String).ToString Else .CountryDescrip = ""
                If Not (dr("CountyDescrip") Is DBNull.Value) Then .CountyDescrip = CType(dr("CountyDescrip"), String).ToString Else .CountyDescrip = ""
                If Not (dr("EdLvlDescrip") Is DBNull.Value) Then .EdLvlDescrip = CType(dr("EdLvlDescrip"), String).ToString Else .EdLvlDescrip = ""
                If Not (dr("AddressStatusDescrip") Is DBNull.Value) Then .AddressStatusDescrip = CType(dr("AddressStatusDescrip"), String).ToString Else .AddressStatusDescrip = ""
                If Not (dr("PhoneStatusDescrip") Is DBNull.Value) Then .PhoneStatusDescrip = CType(dr("PhoneStatusDescrip"), String).ToString Else .PhoneStatusDescrip = ""
                If Not (dr("StatusDescrip") Is DBNull.Value) Then .StatusDescrip = CType(dr("StatusDescrip"), String).ToString Else .StatusDescrip = ""
                If Not (dr("DependencyTypeDescrip") Is DBNull.Value) Then .DependencyTypeDescrip = CType(dr("DependencyTypeDescrip"), String).ToString Else .DependencyTypeDescrip = ""
                If Not (dr("GeographicTypeDescrip") Is DBNull.Value) Then .GeographicTypeDescrip = CType(dr("GeographicTypeDescrip"), String).ToString Else .GeographicTypeDescrip = ""
                If Not (dr("InquiryTime") Is DBNull.Value) Then .InquiryTime = CType(dr("InquiryTime"), String).ToString Else .InquiryTime = ""
                If Not (dr("AdvertisementNote") Is DBNull.Value) Then .AdvertisementNote = CType(dr("AdvertisementNote"), String).ToString Else .AdvertisementNote = ""
                If Not (dr("HousingId") Is DBNull.Value) Then .HousingTypeId = CType(dr("HousingId"), Guid).ToString Else .HousingTypeId = ""
                If Not (dr("AdminCriteriaDescrip") Is DBNull.Value) Then .AdminCriteriaDescrip = dr("AdminCriteriaDescrip").ToString Else .AdminCriteriaDescrip = ""
                If Not (dr("HousingTypeDescrip") Is DBNull.Value) Then .HousingTypeDescrip = dr("HousingTypeDescrip").ToString Else .HousingTypeDescrip = ""
                If Not (dr("DegCertSeekingId") Is DBNull.Value) Then .DegCertSeekingId = dr("DegCertSeekingId").ToString Else .DegCertSeekingId = ""
                If Not (dr("DegCertSeekingDescrip") Is DBNull.Value) Then .DegCertSeekingDescrip = dr("DegCertSeekingDescrip").ToString Else .DegCertSeekingDescrip = ""
                If Not (dr("EnrolledStatus") Is DBNull.Value) Then
                    If CType(dr("EnrolledStatus"), Integer) >= 1 Then
                        .IsStatusEnrolled = True
                    Else
                        .IsStatusEnrolled = False
                    End If
                Else
                    .IsStatusEnrolled = False
                End If
                'If useRegent = True Then
                '    If Not (dr("entranceinterviewdate") Is DBNull.Value) Then .EntranceInterviewDate = dr("entranceinterviewdate") Else .EntranceInterviewDate = Date.Now
                '    If Not (dr("highschoolprogramcode") Is DBNull.Value) Then .HighSchoolProgramCode = dr("highschoolprogramcode") Else .HighSchoolProgramCode = ""
                'End If
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return LeadMasterInfo
        Return leadInfo
    End Function
    Public Function GetAllReassignAdmissionReps(ByVal AdmissionRepID As String, ByVal strCampusId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Build the sql query
        With sb
            .Append(" select distinct s1.fullname,s1.UserId from ")
            .Append(" syUsers s1,syUsersRolesCampGrps s2,syRoles s3,sySysRoles s4,syCmpGrpCmps s5,syStatuses s6 ")
            .Append(" where s1.UserId = s2.UserId and s2.RoleId = s3.RoleId ")
            .Append(" and s3.SysRoleId = s4.SysRoleId and s2.CampGrpId = s5.CampGrpId and s4.StatusId = s6.StatusId ")
            .Append(" and s5.CampusId=? and s4.SysRoleId = 3 and s6.Status='Active'")
            .Append(" and s1.UserId not in (?) ")
            .Append(" order by s1.fullname ")

        End With
        db.OpenConnection()
        db.AddParameter("@CampusId", strCampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@AdmissionRepID", AdmissionRepID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GenerateLead(ByVal AdmissionRepID As String, ByVal StartDate As String, ByVal EndDate As String, ByVal LeadStatus As String, ByVal campusId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da6 As OleDbDataAdapter


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("SELECT distinct LeadID,FirstName,MiddleName,LastName,LeadStatus,(select Distinct StatusCodeDescrip from syStatusCodes where StatusCodeId=LeadStatus) as StatusDescrip FROM adLeads ")
            .Append(" WHERE AdmissionsRep = ? and CampusId = ?  ")
            'If Not StartDate = Date.MinValue And Not EndDate = Date.MinValue Then
            '    .Append(" AND (AssignedDate >= ?  AND AssignedDate <= ?) ")
            'End If
            If Not StartDate = "" And Not EndDate = "" Then
                .Append(" AND (AssignedDate >= ?  AND AssignedDate <= ?) ")
            End If
            .Append(" and LeadStatus in (")
            .Append(LeadStatus)
            .Append(")")
            .Append(" ORDER BY LastName ")
        End With

        db.AddParameter("@AdmissionsRep", AdmissionRepID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'If Not StartDate = Date.MinValue And Not EndDate = Date.MinValue Then
        '    db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        '    db.AddParameter("@EndDate", EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        'End If
        If Not StartDate = "" And Not EndDate = "" Then
            db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@EndDate", EndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        ' db.AddParameter("@LeadStatus", LeadStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        ' Try
        da6.Fill(ds, "InstructorDT")

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Catch ex As System.Exception
        '    Throw New System.Exception(ex.InnerException.Message)
        ' Finally
        '    db.CloseConnection()
        '  End Try
        Return ds
    End Function
    Public Function GenerateLeadNoAdmissionRep(ByVal AdmissionRepID As String, ByVal StartDate As String, ByVal EndDate As String, ByVal LeadStatus As String, ByVal CampusId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da6 As OleDbDataAdapter


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("SELECT distinct LeadID,FirstName,MiddleName,LastName,LeadStatus,(select Distinct StatusCodeDescrip from syStatusCodes where StatusCodeId=LeadStatus) as StatusDescrip  FROM adLeads ")
            .Append(" WHERE AdmissionsRep = ?  and CampusId=? ")
            'If Not StartDate = Date.MinValue And Not EndDate = Date.MinValue Then
            '    .Append(" AND (AssignedDate >= ?  AND AssignedDate <= ?) ")
            'End If
            If Not StartDate = "" And Not EndDate = "" Then
                .Append(" AND (AssignedDate >= ?  AND AssignedDate <= ?) ")
            End If
            .Append(" and LeadStatus in (")
            .Append(LeadStatus)
            .Append(")")
            .Append(" ORDER BY LastName ")
        End With

        db.AddParameter("@AdmissionsRep", Guid.Empty.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'If Not StartDate = Date.MinValue And Not EndDate = Date.MinValue Then
        '    db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '    db.AddParameter("@EndDate", EndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'End If
        If Not StartDate = "" And Not EndDate = "" Then
            db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@EndDate", EndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        'db.AddParameter("@LeadStatus", LeadStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT")
            'Catch ex As System.Exception
            '    Throw New System.Exception(ex.InnerException.Message)
        Finally
            db.CloseConnection()
        End Try
        Return ds
    End Function

    Public Function GetAllStudentNames() As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da6 As OleDbDataAdapter

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("SELECT StudentID,FirstName,MiddleName,LastName  FROM arStudent ")
            .Append(" ORDER BY FirstName ")
        End With

        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorStudent")
        Finally
            db.CloseConnection()
        End Try
        Return ds
    End Function
    Public Function GetAllDocumentStudentNames(ByVal campusId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da6 As OleDbDataAdapter


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append(" SELECT StudentID,FirstName,MiddleName,LastName  FROM arStudent ")
            .Append(" where campusId = ? ")
            .Append(" ORDER BY FirstName ")
        End With
        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorStudent")
        Finally
            db.CloseConnection()
        End Try
        Return ds
    End Function
    Public Function PopulateReassignLeads(ByVal LeadID As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da6 As OleDbDataAdapter


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append(" SELECT A.LeadID,(select FirstName from adLeads where LeadID=A.LeadID) as LeadFirstName, ")
            .Append(" (select LastName from adLeads where LeadID=A.LeadID) as LeadLastName, ")
            .Append(" (select MiddleName from adLeads where LeadID=A.LeadID) as LeadMiddleName, ")
            .Append(" A.AdmissionsRep,(select FullName from syUsers where UserID= A.AdmissionsRep) as AdmissionRepFullName ")
            .Append("  FROM adLeads A")
            .Append(" where A.LeadID in ('")
            .Append(LeadID)
            .Append("')")
        End With

        Dim strLead As String
        ''To Concatenate a Single Quote To LeadID
        'strLead = "'" & LeadID
        'db.AddParameter("@LeadID", strLead, DataAccess.OleDbDataType.OleDbString, 500, ParameterDirection.Input)

        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT")
            'Catch ex As System.Exception
            '    Throw New System.Exception(ex.InnerException.Message)
        Finally
            db.CloseConnection()
        End Try
        Return ds
    End Function
    Public Function PopulateAssignLeadsToCampus(ByVal LeadID As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da6 As OleDbDataAdapter



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append(" SELECT A.LeadID,(select FirstName from adLeads where LeadID=A.LeadID) as LeadFirstName, ")
            .Append(" (select LastName from adLeads where LeadID=A.LeadID) as LeadLastName, ")
            .Append(" (select MiddleName from adLeads where LeadID=A.LeadID) as LeadMiddleName, ")
            .Append(" A.CampusId,(select CampDescrip from syCampuses where CampusId =  A.CampusId) as CampusDescrip ")
            .Append("  FROM adLeads A")
            .Append(" where A.LeadID in ('")
            .Append(LeadID)
            .Append("')")
        End With

        ' Dim strLead As String

        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT")
            'Catch ex As System.Exception
            '    Throw New System.Exception(ex.InnerException.Message)
        Finally
            db.CloseConnection()
        End Try
        Return ds
    End Function
    Public Function UpdateAssignmentLead(ByVal selectedLeads() As String, ByVal CurrAdmissionRepID As String, ByVal user As String, ByVal selectedDegrees() As String, ByVal selectedStatus() As String, ByVal StartLeadDate As String, ByVal EndLeaddate As String) As Integer
        '   connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")


        '   Insert one record per each Item in the Selected Group
        'Dim i, j, k As Integer


        Try
            Dim intLeadCount As Integer
            intLeadCount = selectedLeads.Length
            Dim x, y, z As Integer
            While x < intLeadCount
                With sb
                    .Append("Update adLeads set AdmissionsRep = ?,ModUser=?,ModDate=? where LeadID = ? ")
                End With
                'add parameters
                db.AddParameter("@AdmissionRepID", DirectCast(selectedDegrees.GetValue(y), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@LeadID", DirectCast(selectedLeads.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                'Increment value of x and y
                x = x + 1
                y = y + 1
                If y = selectedDegrees.Length Then
                    y = 0
                End If
            End While
            Return 0
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateAssignmentLeadToCampus(ByVal selectedLeads() As String, ByVal CurrAdmissionRepID As String, ByVal selectedCampusId() As String, ByVal user As String) As Integer
        '   connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   Insert one record per each Item in the Selected Group
        Dim i, j, k As Integer
        Dim x, y, z As Integer
        Try
            Dim intLeadCount As Integer
            intLeadCount = selectedLeads.Length
            While x < intLeadCount
                With sb
                    .Append("Update adLeads set CampusId =?,ModUser=?,ModDate=?  ")
                    .Append(" where LeadID = ? ")
                End With
                'add parameters
                db.AddParameter("@CampusId", DirectCast(selectedCampusId.GetValue(y), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@LeadID", DirectCast(selectedLeads.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                'Increment value of x and y
                x = x + 1
                y = y + 1
                If y = selectedCampusId.Length Then
                    y = 0
                End If
            End While
            Return 0

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GenerateLeadByAdmissionRep(ByVal AdmissionRepID As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da6 As OleDbDataAdapter



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("SELECT LeadID,FirstName,MiddleName,LastName  FROM adLeads ")
            .Append(" WHERE AdmissionsRep = ?  ")
            .Append(" ORDER BY LastName ")
        End With

        db.AddParameter("@AdmissionsRep", AdmissionRepID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT2")
        Finally
            db.CloseConnection()
        End Try
        Return ds
    End Function

    Public Function GenerateUnAssignedLeadByAdmissionRep(ByVal AdmissionRepID As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da6 As OleDbDataAdapter



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("SELECT t1.LeadID,t1.FirstName,t1.MiddleName,t1.LastName,(select Distinct CampDescrip from syCampuses where CampusId=t1.CampusId) as CampusDescrip,t1.LeadStatus,(select Distinct StatusCodeDescrip from syStatusCodes where StatusCodeId=t1.LeadStatus) as StatusDescrip  FROM adLeads t1 ")
            .Append(" WHERE  ")
            If Not AdmissionRepID = "" Then
                .Append(" t1.AdmissionsRep = ?  ")
            Else
                .Append(" t1.AdmissionsRep  is null ")
            End If
            .Append(" ORDER BY t1.FirstName,CampusDescrip ")
        End With
        If Not AdmissionRepID = Guid.Empty.ToString Then
            db.AddParameter("@AdmissionsRep", AdmissionRepID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT2")
        Finally
            db.CloseConnection()
        End Try
        Return ds
    End Function
    Public Function GetSAPResults(ByVal CampusId As String, Optional ByVal prgVerId As String = "") As DataSet
        'connect to the database
        Dim db As New DataAccess



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append("select * from ( ")
            .Append("SELECT t1.StuEnrollId, t1.Period, t1.IsMakingSAP,t1.ModDate,t4.FirstName,t4.LastName,t3.PrgVerId,t5.PrgVerDescrip, ")
            .Append("CASE t1.IsMakingSAP when 1 then 'Yes' else 'No' end as MakingSAP,t1.Comments,t2.ProbationCount,  ")
            .Append(" (select isnull(TerminationProbationCnt,0) from arSAP where SAPId=t6.SAPId) as TerminationProbationCount, ")
            .Append(" Case (select isnull(TerminationProbationCnt,0) from arSAP where SAPId=t6.SAPId) when 0 then 0 ")
            .Append(" else case when (t2.ProbationCount-(select isnull(TerminationProbationCnt,0) from arSAP where SAPId=t6.SAPId)) >=0 then 1 ")
            .Append(" else 0   End end as TerminateBool ")
            .Append(",(select top 1 IsMakingSAP from arFASAPChkResults where StuEnrollId=t1.StuEnrollId ")
            .Append(" order by moddate desc) as IsSAP ")
            .Append(" FROM  arFASAPChkResults t1,(SELECT StuEnrollId, MAX(ModDate)As ModDate,count(*) as ProbationCount  ")
            .Append("                          FROM arFASAPChkResults t2   where t2.IsMakingSAP=0 ")
            .Append("                          GROUP BY StuEnrollId) t2, arStuEnrollments t3, arStudent t4, arPrgVersions t5,arSAPDetails t6 ")
            .Append("WHERE t1.StuEnrollId = t2.StuEnrollId ")
            .Append("AND t1.ModDate = t2.ModDate ")
            .Append("AND t1.StuEnrollId = t3.StuEnrollId ")
            .Append("AND t3.StudentId = t4.StudentId ")
            .Append("AND t3.PrgVerId = t5.PrgVerId ")
            .Append("AND t1.IsMakingSAP = 0 and t1.SAPdetailID=t6.SAPDetailID ")
            .Append("AND t3.CampusId = ? ")


            If prgVerId <> "" Then
                .Append("AND t3.PrgVerId = ? ")
            End If
            .Append(" ) SAPChk where IsSAP=0 ")
            '.Append(" ORDER BY TerminateBool desc,ProbationCount desc,PrgVerDescrip,FirstName,ModDate DESC ")
            '.Append(" Order by  CAST(CONVERT(VARCHAR(10), modDate, 111) AS DATETIME) desc ,FirstName ")
            .Append(" Order by  modDate desc ,FirstName ")
        End With
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        If prgVerId <> "" Then
            db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If


        db.OpenConnection()
        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Finally
            db.CloseConnection()
        End Try

    End Function

    Public Function GetSAPResults1(ByVal CampusId As String, ByVal chkdate As String, Optional ByVal prgVerId As String = "") As DataSet
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append("select * from ( ")
            .Append("SELECT t1.StuEnrollId, t1.Period, t1.IsMakingSAP,t1.ModDate,t1.CheckPointDate,RTRIM(LTRIM(t4.FirstName)) AS FirstName, RTRIM(LTRIM(t4.LastName)) AS LastName,t3.PrgVerId,t5.PrgVerDescrip, ")
            .Append("CASE t1.IsMakingSAP when 1 then 'Yes' else 'No' end as MakingSAP,t1.Comments,t2.ProbationCount,  ")
            .Append(" (select isnull(TerminationProbationCnt,0) from arSAP where SAPId=t6.SAPId) as TerminationProbationCount, ")
            .Append(" Case (select isnull(TerminationProbationCnt,0) from arSAP where SAPId=t6.SAPId) when 0 then 0 ")
            .Append(" else case when (t2.ProbationCount-(select isnull(TerminationProbationCnt,0) from arSAP where SAPId=t6.SAPId)) >=0 then 1 ")
            .Append(" else 0   End end as TerminateBool ")
            .Append(",(select top 1 IsMakingSAP from arSAPChkResults where StuEnrollId=t1.StuEnrollId ")
            .Append(" order by moddate desc) as IsSAP ")
            .Append(" ,t6.ConsequenceTypId as ConsequenceTypId, ")

            .Append(" (Select ConseqTypDesc From arConsequenceTyps CT Where CT.ConsequenceTypId=t6.ConsequenceTypId)+' ('+ ")
            .Append(" (case (t6.ConsequenceTypId) when 1 then ")
            .Append(" (SELECT Top 1 A.StatusCodeDescrip AS StatusCodeDescrip  FROM   syStatusCodes A WHERE  A.sysStatusId = 20) ")
            .Append(" when 2 then ")
            .Append("(SELECT top 1 a.StatusCodeDescrip FROM syStatusCodes a, sySysStatus b ")
            .Append("WHERE a.SysStatusId in (12,19) order by a.StatusCodeDescrip)")
            .Append("else (SELECT DISTINCT A.StatusCodeDescrip AS StatusCodeDescrip ")
            .Append(" FROM   syStatusCodes A WHERE  A.StatusCodeId = t3.StatusCodeId) ")
            .Append(" End)+')'  as StudentStatus, ")
            '''''''''''''''''''''''''''''''''''''''''''''''''
            .Append(" (select sum(isnull(TerminationProbationCnt,0))  from arSAP) as SAPTerminationCnt ")
            .Append(" FROM  arSAPChkResults t1,(SELECT StuEnrollId, MAX(ModDate)As ModDate,count(*) as ProbationCount  ")
            .Append("                          FROM arSAPChkResults t2   where t2.IsMakingSAP=0 ")
            .Append("                          GROUP BY StuEnrollId) t2, arStuEnrollments t3, arStudent t4, arPrgVersions t5, arSAPDetails t6, arSAP t7 ")
            .Append("WHERE t1.StuEnrollId = t2.StuEnrollId ")
            .Append("AND t1.ModDate = t2.ModDate ")
            .Append("AND t1.StuEnrollId = t3.StuEnrollId ")
            .Append("AND t3.StudentId = t4.StudentId ")
            .Append("AND t3.PrgVerId = t5.PrgVerId ")
            .Append("AND t1.IsMakingSAP = 0 and t1.SAPdetailID=t6.SAPDetailID  and t6.SAPId = t7.SAPId ")
            .Append("AND t3.CampusId = ? ")
            .Append(" and t1.modDate >= ? ")
            .Append(" and t7.FASAPPolicy=0 ")
            If prgVerId <> "" Then
                .Append("AND t3.PrgVerId = ? ")
            End If
            .Append(" ) SAPChk where IsSAP=0 ")
            '.Append(" ORDER BY TerminateBool desc,ProbationCount desc,PrgVerDescrip,FirstName,ModDate DESC ")
            .Append(" Order by  CAST(CONVERT(VARCHAR(10), modDate, 111) AS DATETIME) desc ,FirstName ")
            '.Append(" Order by  modDate desc ,FirstName ")
        End With
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@modDate", chkdate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If prgVerId <> "" Then
            db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If


        db.OpenConnection()
        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Finally
            db.CloseConnection()
        End Try

    End Function
    Public Function GetFASAPResults1(ByVal CampusId As String, ByVal chkdate As String, Optional ByVal prgVerId As String = "") As DataSet
        'connect to the database
        Dim db As New DataAccess
        'Dim ds As New DataSet
        'Dim da6 As New OleDbDataAdapter

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append("select * from ( ")
            .Append("SELECT t1.StuEnrollId, t1.Period, t1.IsMakingSAP,t1.ModDate,t1.CheckPointDate,RTRIM(LTRIM(t4.FirstName)) AS FirstName, RTRIM(LTRIM(t4.LastName)) AS LastName,t3.PrgVerId,t5.PrgVerDescrip, ")
            .Append("CASE t1.IsMakingSAP when 1 then 'Yes' else 'No' end as MakingSAP,t1.Comments,t2.ProbationCount,  ")
            .Append(" (select isnull(TerminationProbationCnt,0) from arSAP where SAPId=t6.SAPId) as TerminationProbationCount, ")
            .Append(" Case (select isnull(TerminationProbationCnt,0) from arSAP where SAPId=t6.SAPId) when 0 then 0 ")
            .Append(" else case when (t2.ProbationCount-(select isnull(TerminationProbationCnt,0) from arSAP where SAPId=t6.SAPId)) >=0 then 1 ")
            .Append(" else 0   End end as TerminateBool ")
            .Append(",(select top 1 IsMakingSAP from arFASAPChkResults where StuEnrollId=t1.StuEnrollId ")
            .Append(" order by moddate desc) as IsSAP ")
            .Append(" ,t6.ConsequenceTypId as ConsequenceTypId, ")
            .Append(" (Select ConseqTypDesc From arConsequenceTyps CT Where CT.ConsequenceTypId=t6.ConsequenceTypId)+' ('+ ")
            .Append(" (case (t6.ConsequenceTypId) when 1 then ")
            .Append(" (SELECT Top 1 A.StatusCodeDescrip AS StatusCodeDescrip  FROM   syStatusCodes A WHERE  A.sysStatusId = 20) ")
            .Append(" when 2 then ")
            .Append("(SELECT top 1 a.StatusCodeDescrip FROM syStatusCodes a, sySysStatus b ")
            .Append("WHERE a.SysStatusId in (12,19) order by a.StatusCodeDescrip)")
            .Append("else (SELECT DISTINCT A.StatusCodeDescrip AS StatusCodeDescrip ")
            .Append(" FROM   syStatusCodes A WHERE  A.StatusCodeId = t3.StatusCodeId) ")
            .Append(" End)+')'  as StudentStatus, ")
            '''''''''''''''''''''''''''''''''''''''''''''''''
            .Append(" (select sum(isnull(TerminationProbationCnt,0))  from arSAP) as SAPTerminationCnt ")
            .Append(" FROM  arFASAPChkResults t1,(SELECT StuEnrollId, MAX(ModDate)As ModDate,count(*) as ProbationCount  ")
            .Append("                          FROM arFASAPChkResults t2   where t2.IsMakingSAP=0 ")
            .Append("                          GROUP BY StuEnrollId) t2, arStuEnrollments t3, arStudent t4, arPrgVersions t5, arSAPDetails t6 ")
            .Append("WHERE t1.StuEnrollId = t2.StuEnrollId ")
            .Append("AND t1.ModDate = t2.ModDate ")
            .Append("AND t1.StuEnrollId = t3.StuEnrollId ")
            .Append("AND t3.StudentId = t4.StudentId ")
            .Append("AND t3.PrgVerId = t5.PrgVerId ")
            .Append("AND t1.IsMakingSAP = 0 and t1.SAPdetailID=t6.SAPDetailID ")
            .Append("AND t3.CampusId = ? ")
            .Append(" and t1.modDate >= ? ")


            If prgVerId <> "" Then
                .Append("AND t3.PrgVerId = ? ")
            End If
            .Append(" ) SAPChk where IsSAP=0 ")
            .Append(" Order by  CAST(CONVERT(VARCHAR(10), modDate, 111) AS DATETIME) desc ,FirstName ")
        End With
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@modDate", chkdate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If prgVerId <> "" Then
            db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If


        db.OpenConnection()
        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Finally
            db.CloseConnection()
        End Try

    End Function

    Public Function GetFASAPResultsByStudent(ByVal StuEnrollId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim ds As DataSet
        'Dim da6 As New OleDbDataAdapter



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder


        With sb
            .Append("select t1.*, CASE t1.IsMakingSAP when 1 then 'Yes' else 'No' end as MakingSAP, ")
            .Append("t3.FirstName, t3.LastName, t4.PrgVerId, t4.PrgVerDescrip,t5.TrigValue AS TrigValue, t5.TrigOffsetSeq, t6.TrigUnitTypDescrip, t7.TrigOffTypDescrip ")
            .Append("from arFASAPChkResults t1, arStuEnrollments t2, arStudent t3, arPrgVersions t4, arSAPDetails t5, arTrigUnitTyps t6, arTrigOffsetTyps t7 ")
            .Append("where   ")
            .Append("t1.StuEnrollId = ? ")
            .Append("and t1.StuEnrollId = t2.StuEnrollId and t2.StudentId= t3.StudentId and t2.PrgVerId = t4.PrgVerId ")
            .Append("and t1.SAPDetailId = t5.SAPDetailId and  t5.TrigUnitTypId = t6.TrigUnitTypId and t5.TrigOffsetTypId = t7.TrigOffsetTypId and t1.PreviewSapCheck = 0 ")
            .Append("ORDER BY t4.PrgVerDescrip,t3.FirstName,t1.ModDate DESC ")
        End With

        db.AddParameter("@stuenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        db.OpenConnection()
        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Finally
            db.CloseConnection()
        End Try
        Return ds
    End Function

    Public Function GetSAPResultsByStudent(ByVal StuEnrollId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim ds As DataSet
        'Dim da6 As New OleDbDataAdapter



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder


        With sb
            .Append("select t1.*, CASE t1.IsMakingSAP when 1 then 'Yes' else 'No' end as MakingSAP, ")
            .Append("t3.FirstName, t3.LastName, t4.PrgVerId, t4.PrgVerDescrip,t5.TrigValue AS TrigValue, t5.TrigOffsetSeq, t6.TrigUnitTypDescrip, t7.TrigOffTypDescrip ")
            .Append("from arSAPChkResults t1, arStuEnrollments t2, arStudent t3, arPrgVersions t4, arSAPDetails t5, arTrigUnitTyps t6, arTrigOffsetTyps t7 ")
            .Append("where   ")
            .Append("t1.StuEnrollId = ? ")
            .Append("and t1.StuEnrollId = t2.StuEnrollId and t2.StudentId= t3.StudentId and t2.PrgVerId = t4.PrgVerId ")
            .Append("and t1.SAPDetailId = t5.SAPDetailId and  t5.TrigUnitTypId = t6.TrigUnitTypId and t5.TrigOffsetTypId = t7.TrigOffsetTypId and t1.PreviewSapCheck = 0 ")
            .Append("ORDER BY t4.PrgVerDescrip,t3.FirstName,t1.DatePerformed ASC ")
        End With

        db.AddParameter("@stuenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        db.OpenConnection()
        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Finally
            db.CloseConnection()
        End Try
        Return ds
    End Function

    Public Function GetTestExists(ByVal TestId As String) As Integer
        'connect to the database
        Dim db As New DataAccess
        'Dim ds As New DataSet
        'Dim da6 As New OleDbDataAdapter



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append(" select count(*) from adLeadEntranceTest where EntrTestId in (select Distinct EntrTestId from ")
            .Append(" adPrgVerTestDetails where PrgVerTestDetailId=?) ")
        End With

        db.AddParameter("@PrgVerTestDetailId", TestId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()

        Try
            Return db.RunParamSQLScalar(sb.ToString)
        Finally
            db.CloseConnection()
        End Try

    End Function
    Public Function CheckDuplicateLead(ByVal strLastName As String, ByVal strFirstName As String, ByVal strSSN As String, ByVal strDOB As String, ByVal strCampusId As String, ByVal repId As String, ByVal strAddress As String, ByVal strPhone As String, ByVal strPhone2 As String) As DataSet
        'connect to the database
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb

            .Append(" SELECT    ")
            .Append(" S1.FirstName, ")
            .Append(" S1.LastName,S1.BirthDate,S1.SSN,S1.CampusId,S1.Address1,S1.Phone,S1.Phone2,CA.CampDescrip,US.FullName ")
            .Append(" FROM ")
            .Append(" adLeads S1,syCampuses CA, syUsers US ")
            .Append("  where S1.CampusId=CA.CampusId and S1.AdmissionsRep=US.UserId")

            '''''''''''''''''''''''''''''

            'the relationship logic between checkboxes is "OR" instead of "AND". Only the first one is AND
            Dim andOrOrOperator As String = " AND "
            If Not strLastName = "" Or Not strFirstName = "" Or Not strSSN = "" Or Not strDOB = Guid.Empty.ToString Then
                '.Append(" where AdmissionsRep = ? and CampusId = ?  ") Commented out by Troy on 7/21/2006. The search must
                'db.AddParameter("@repid", repId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'be done against all leads for the campus that the user is logged in to.
                ''.Append(" where CampusId = ?  ")

                ''' Commented By Kamalesh Ahuja on 17 July 2010 to resolve mantis issues id 19541
                '.Append(" and S1.CampusId = ?  ")
                'db.AddParameter("@cmpid", strCampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                '''
            End If

            '   check if the selection is "All Campus Groups"

            If Not strLastName = "" Then
                .Append(andOrOrOperator)
                ''.Append(" LastName like  + ? + '%'")
                .Append(" S1.LastName like  + ? + '%'")
                db.AddParameter("@LastName", strLastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strFirstName = "" Then
                .Append(andOrOrOperator)
                ''.Append(" FirstName like  + ? + '%'")
                .Append(" S1.FirstName like  + ? + '%'")
                db.AddParameter("@FirstName", strFirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Not strSSN = "" Then
                .Append(andOrOrOperator)
                ''.Append(" SSN like  + ? + '%'")
                .Append(" S1.SSN like  + ? + '%'")
                Dim SSNWithoutDashes As String = strSSN
                Dim anyOf As Char() = {"-"c, " "c, "_"c, "/"c, "\"c}
                While (True)
                    Dim index As Integer = SSNWithoutDashes.IndexOfAny(anyOf)
                    If index = -1 Then
                        Exit While
                    Else
                        SSNWithoutDashes = SSNWithoutDashes.Remove(index, 1)
                    End If
                End While
                db.AddParameter("@SSN", SSNWithoutDashes, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strDOB = "" Then
                .Append(andOrOrOperator)
                ''.Append(" BirthDate  = ? ")
                .Append(" S1.BirthDate  = ? ")
                '.Append(" BirthDate  like + ? + '%'")
                db.AddParameter("@DOB", strDOB, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Not strAddress = "" Then
                .Append(andOrOrOperator)
                .Append(" S1.Address1 like  + ? + '%'")
                db.AddParameter("@Address", strAddress, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Not strPhone = "" Then
                .Append(andOrOrOperator)
                .Append(" S1.Phone like  + ? + '%'")
                Dim PhoneWithoutDashes As String = strPhone
                Dim anyOf As Char() = {"-"c, " "c, "_"c, "("c, ")"c}
                While (True)
                    Dim index As Integer = PhoneWithoutDashes.IndexOfAny(anyOf)
                    If index = -1 Then
                        Exit While
                    Else
                        PhoneWithoutDashes = PhoneWithoutDashes.Remove(index, 1)
                    End If
                End While

                db.AddParameter("@Phone", PhoneWithoutDashes, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Not strPhone2 = "" Then
                .Append(andOrOrOperator)
                .Append(" S1.Phone2 like  + ? + '%'")
                Dim phone2WithoutDashes As String = strPhone2
                Dim anyOf As Char() = {"-"c, " "c, "_"c, "("c, ")"c}
                While (True)
                    Dim index As Integer = phone2WithoutDashes.IndexOfAny(anyOf)
                    If index = -1 Then
                        Exit While
                    Else
                        phone2WithoutDashes = phone2WithoutDashes.Remove(index, 1)
                    End If
                End While
                db.AddParameter("@Phone2", phone2WithoutDashes, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            .Append(" order by S1.LastName ")
        End With
        '   Execute the query and return Dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function CheckDuplicateStudent(ByVal strLastName As String, ByVal strFirstName As String, ByVal strSSN As String, ByVal strDOB As String, ByVal strCampusId As String, ByVal strUserId As String) As DataSet
        'connect to the database
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append(" SELECT    ")
            .Append(" FirstName, ")
            .Append(" LastName,DOB,SSN ")
            .Append(" FROM ")
            .Append(" arStudent S1 where  CampusId = ?  ")

            db.AddParameter("@cmpid", strCampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'the relationship logic between checkboxes is "OR" instead of "AND". Only the first one is AND
            Dim andOrOrOperator As String = " AND "

            '   check if the selection is "All Campus Groups"
            If Not strLastName = "" Then
                .Append(andOrOrOperator)
                .Append(" LastName like  + ? + '%'")
                db.AddParameter("@LastName", strLastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strFirstName = "" Then
                .Append(andOrOrOperator)
                .Append(" FirstName like  + ? + '%'")
                db.AddParameter("@FirstName", strFirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Not strSSN = "" Then
                .Append(andOrOrOperator)
                .Append(" SSN like  + ? + '%'")
                db.AddParameter("@SSN", strSSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strDOB = "" Then
                .Append(andOrOrOperator)
                .Append(" DOB = ? ")
                db.AddParameter("@DOB", strDOB, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            .Append(" order by LastName ")
        End With
        '   Execute the query and return Dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function CheckCountDuplicateStudent(ByVal strLastName As String, ByVal strFirstName As String, ByVal strSSN As String, ByVal strDOB As String) As Integer
        'connect to the database
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append(" SELECT  Count(*) as DuplicateStudentCount  ")
            .Append(" FROM ")
            .Append(" arStudent S1 ")

            'the relationship logic between checkboxes is "OR" instead of "AND". Only the first one is AND
            Dim andOrOrOperator As String = " AND "
            If Not strLastName = "" Or Not strFirstName = "" Or Not strSSN = "" Or Not strDOB = "" Then
                .Append(" where ")
            End If

            '   check if the selection is "All Campus Groups"
            If Not strLastName = "" Then
                .Append(" LastName like  + ? + '%'")
                db.AddParameter("@LastName", strLastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strFirstName = "" Then
                If Not strLastName = "" Then
                    .Append(andOrOrOperator)
                End If
                .Append(" FirstName like  + ? + '%'")
                db.AddParameter("@FirstName", strFirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If Not strSSN = "" Then
                If Not strLastName = "" Or Not strFirstName = "" Then
                    .Append(andOrOrOperator)
                End If
                .Append(" SSN like  + ? + '%'")
                db.AddParameter("@SSN", strSSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                andOrOrOperator = " AND "
            End If

            If Not strDOB = "" Then
                If Not strLastName = "" Or Not strFirstName = "" Or Not strSSN = "" Then
                    .Append(andOrOrOperator)
                End If
                .Append(" DOB  = ? ")
                db.AddParameter("@DOB", strDOB, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            '.Append(" order by LastName ")
        End With
        '   Execute the query and return Dataset
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim strCount As Integer
        While dr.Read
            If Not (dr("DuplicateStudentCount") Is DBNull.Value) Then strCount = CType(dr("DuplicateStudentCount"), Integer) Else strCount = 0
        End While

        If Not dr.IsClosed Then dr.Close()

        Return strCount
    End Function
    Public Function ContactStudent(ByVal strSearch As String) As DataSet
        'connect to the database
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append(" Select  distinct t1.FirstName,t1.LastName,t1.SSN,t1.HomeEmail as Email,t2.Phone ")
            .Append(" FROM arStudent t1,arStudentPhone t2 where t1.StudentId=t2.StudentId  and ( ")

            'the relationship logic between checkboxes is "OR" instead of "AND". Only the first one is AND
            Dim andOrOrOperator As String = " or "

            '   check if the selection is "All Campus Groups"
            .Append(" FirstName like  + '%' + ? + '%'")
            db.AddParameter("@FirstName", strSearch, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            .Append(andOrOrOperator)
            .Append(" LastName like  +  '%' + ? + '%'")
            db.AddParameter("@LastName", strSearch, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            .Append(andOrOrOperator)
            .Append(" t1.SSN like  + '%' + ? + '%'")
            db.AddParameter("@StudentId", strSearch, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            .Append(" ) ")
            .Append(" order by t1.LastName ")
        End With
        '   Execute the query and return Dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function AddObjective(ByVal StudentId As String, ByVal strObjective As String) As Integer

        ''Connect To The Database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            '    'Build The Query
            Dim sb As New StringBuilder
            Dim sb1 As New StringBuilder
            With sb1
                .Append(" Delete from adResumeObjective where StudentId = ? ")
            End With
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb1.ToString)
            db.ClearParameters()
            sb1.Remove(0, sb1.Length)


            With sb
                .Append("Insert adResumeObjective(ResumeId,StudentId,Objective) values (?,?,?) ")
            End With

            'ResumeId
            db.AddParameter("@ResumeId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'StudentId
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Objective
            db.AddParameter("@Objective", strObjective, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)

            ''Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            'Retun Without Errors
            Return 0
        Catch ex As Exception
            'Return an Error To Client
            Return -1
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function DeleteObjective(ByVal StudentId As String) As Integer

        ''Connect To The Database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            '    'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Delete from adResumeObjective where StudentId=? ")
            End With

            'StudentId
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            'Retun Without Errors
            Return 0
        Catch ex As Exception
            'Return an Error To Client
            Return -1
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetDocumentsByProgramVersionAndStatus(ByVal LeadId As String, ByVal PrgVerId As String) As DataSet

        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append(" select Distinct t1.adReqId as DocumentId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,t2.Descrip as DocumentDescrip, ")
            .Append(" case when t3.IsRequired = 1 then 'Yes' else 'No' end as Required, ")
            .Append(" case when (select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and LeadId='" & LeadId & "' )>=1 then 'Yes' else 'No' end as Submitted, ")
            .Append(" case when (select V1.sysDocStatusId  from sySysDocStatuses V1,syDocStatuses V2,adLeadDocsReceived V3  ")
            .Append(" where V1.SysDocStatusId = V2.SysDocStatusId and V2.DocStatusId = V3.DocStatusId and V3.DocumentId=t1.adReqId and V3.LeadId='" & LeadId & "' ) = 1 then 'Approved' else 'Not Approved' end as DocumentStatus ")
            .Append(" from adPrgVerTestDetails t1,adReqs t2,adReqLeadGroups t3   ")
            .Append(" where t1.adReqId = t2.adReqId and t2.adReqId = t3.adReqId and t1.PrgVerId='" & PrgVerId & "'  ")
            .Append(" and t2.adReqTypeId = 3 and t1.ReqGrpId is null and t3.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "' )  ")
            .Append(" union ")
            .Append(" select t5.adReqId as DocumentId,t4.ReqGrpId,t5.Descrip as DocumentDescrip, ")
            .Append(" case when t6.IsRequired = 1 then 'Yes' else 'No' end as Required, ")
            .Append(" case when (select Count(*) from adLeadDocsReceived where DocumentId=t5.adReqId and LeadId='" & LeadId & "' )>=1 then 'Yes' else 'No' end as Submitted, ")
            .Append(" case when (select V1.sysDocStatusId  from sySysDocStatuses V1,syDocStatuses V2,adLeadDocsReceived V3  ")
            .Append(" where V1.SysDocStatusId = V2.SysDocStatusId and V2.DocStatusId = V3.DocStatusId and V3.DocumentId=t5.adReqId and V3.LeadId='" & LeadId & "' ) = 1  ")
            .Append(" then 'Approved' else 'Not Approved' end as DocumentStatus ")
            .Append(" from  adReqGrpDef t4,adReqs t5,adReqLeadGroups t6   ")
            .Append(" where t4.adReqId = t5.adReqId and t5.adReqId = t6.adReqId and t6.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "' ) ")
            .Append(" and t5.adReqTypeId = 3  ")
            .Append(" and ReqGrpId in (select t1.ReqGrpId from adPrgVerTestDetails t1 where t1.PrgVerId='" & PrgVerId & "'  and ReqGrpId is not null)  ")
            .Append(" union ")
            'Get Documents assigned to lead group but not part of a requirement group or assigned to
            'program version
            .Append(" select Distinct B1.adReqId as DocumentId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,B1.Descrip as DocumentDescrip, ")
            .Append(" case when B2.IsRequired = 1 then 'Yes' else 'No' end as Required, ")
            .Append(" case when (select Count(*) from adLeadDocsReceived where DocumentId=B1.adReqId and LeadId='" & LeadId & "' )>=1 then 'Yes' else 'No' end as Submitted, ")
            .Append(" case when (select V1.sysDocStatusId  from sySysDocStatuses V1,syDocStatuses V2,adLeadDocsReceived V3  ")
            .Append(" where V1.SysDocStatusId = V2.SysDocStatusId and V2.DocStatusId = V3.DocStatusId and V3.DocumentId=B1.adReqId and V3.LeadId='" & LeadId & "' ) = 1  ")
            .Append(" then 'Approved' else 'Not Approved' end as DocumentStatus ")
            .Append(" from adReqs B1,adReqLeadGroups B2 where B1.adReqId = B2.adReqId   ")
            .Append(" and B2.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "' ) ")
            .Append(" and B1.adReqTypeId=3 and B1.adReqId not in  ")
            .Append(" (select adReqId from adPrgVerTestDetails where PrgVerId='" & PrgVerId & "'  and adReqId is not null) ")
            .Append(" and B1.adReqId not in  ")
            .Append(" (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails ")
            .Append(" where PrgVerId='" & PrgVerId & "'  and adReqId is NULL))  ")
            'Get all mandatory requirements
            .Append(" union ")
            .Append(" select Distinct adReqId as DocumentId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,Descrip as DocumentDescrip,'Yes' as Required, ")
            .Append(" case when (select Count(*) from adLeadDocsReceived where DocumentId=adReqId and LeadId='" & LeadId & "' )>=1 then 'Yes' else 'No' end as Submitted, ")
            .Append(" case when (select V1.sysDocStatusId  from sySysDocStatuses V1,syDocStatuses V2,adLeadDocsReceived V3  ")
            .Append(" where V1.SysDocStatusId = V2.SysDocStatusId and V2.DocStatusId = V3.DocStatusId and V3.DocumentId=adReqId and V3.LeadId='" & LeadId & "' ) = 1 ")
            .Append(" then 'Approved' else 'Not Approved' end as DocumentStatus ")
            .Append(" from adReqs where adReqTypeId=3 and AppliesToAll=1  ")
            .Append(" order by Descrip   ")
        End With
        'Execute the query
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function AddExpRoles(ByVal StudentId As String, ByVal strEmployerId As String, ByVal strRoles As String) As Integer

        ''Connect To The Database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Insert adResumeExperience(ResumeId,StudentId,EmployerId,JobRoles) values (?,?,?,?) ")
            End With

            'ResumeId
            db.AddParameter("@ResumeId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'StudentId
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Employer
            db.AddParameter("@EmployerId", strEmployerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Roles
            db.AddParameter("@Roles", strRoles, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)

            ''Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            'Retun Without Errors
            Return 0
        Catch ex As Exception
            'Return an Error To Client
            Return -1
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetObjective(ByVal StudentId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" SELECT Objective from adResumeObjective where StudentId= ?  ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    'Public Function GetDocumentsByPrgVersion(ByVal LeadId As String) As DataSet
    '    'connect to the database
    '    Dim db As New DataAccess

    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

    '    'build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        'With subqueries
    '        .Append(" SELECT PrgVerId from adLeads where LeadId= ?  ")
    '    End With

    '    db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    'Execute the query
    '    Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
    '    Dim strPrgVersion As String
    '    While dr.Read
    '        If Not (dr("PrgVerId") Is DBNull.Value) Then strPrgVersion = CType(dr("PrgVerId"), Guid).ToString Else strPrgVersion = ""
    '    End While

    '    If Not dr.IsClosed Then dr.Close()

    '    db.ClearParameters()
    '    sb.Remove(0, sb.Length)

    '    With sb
    '        .Append(" select DocumentReceivedFromLead = ")
    '        .Append(" Case when (select count(*) from adLeadDocsReceived where LeadId=? ")
    '        .Append(" and DocumentId = t2.DocumentId)>=1 then 'Yes' else 'No' end, t2.DocumentDescrip ")
    '        .Append(" from adPrgVerDocs t1,cmDocuments  t2  where t1.DocumentId = t2.DocumentId and t1.PrgVerId = ? ")
    '        .Append(" and t1.Required=1 ")
    '    End With
    '    db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@PrgVerId", strPrgVersion, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    'Execute the query
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
    'Public Function GetOptionalDocumentsByPrgVersion(ByVal LeadId As String) As DataSet
    '    'connect to the database
    '    Dim db As New DataAccess


    '    db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

    '    'build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        'With subqueries
    '        .Append(" SELECT PrgVerId from adLeads where LeadId= ?  ")
    '    End With

    '    db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    'Execute the query
    '    Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
    '    Dim strPrgVersion As String
    '    While dr.Read
    '        If Not (dr("PrgVerId") Is DBNull.Value) Then strPrgVersion = CType(dr("PrgVerId"), Guid).ToString Else strPrgVersion = ""
    '    End While

    '    If Not dr.IsClosed Then dr.Close()

    '    db.ClearParameters()
    '    sb.Remove(0, sb.Length)

    '    With sb
    '        .Append(" select DocumentReceivedFromLead = ")
    '        .Append(" Case when (select count(*) from adLeadDocsReceived where LeadId=? ")
    '        .Append(" and DocumentId = t2.DocumentId)>=1 then 'Yes' else 'No' end, t2.DocumentDescrip ")
    '        .Append(" from adPrgVerDocs t1,cmDocuments  t2  where t1.DocumentId = t2.DocumentId and t1.PrgVerId = ? ")
    '        .Append(" and t1.Required=0 ")
    '    End With
    '    db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@PrgVerId", strPrgVersion, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    'Execute the query
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
    Public Function CheckIfPrgVersionExists(ByVal LeadId As String) As Integer
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" SELECT Count(*) from adLeads where LeadId= ? and PrgVerId is Null ")
        End With
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim intPrgVerExists As Integer = db.RunParamSQLScalar(sb.ToString)

        Return intPrgVerExists
    End Function
    Public Function GetLeadObjective(ByVal StudentId As String) As String
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" SELECT Objective from arStudent where StudentId= ?  ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim strObjective As String
        While dr.Read
            If Not (dr("Objective") Is DBNull.Value) Then strObjective = dr("Objective").ToString Else strObjective = ""
        End While

        If Not dr.IsClosed Then dr.Close()

        Return strObjective
    End Function
    Public Function GetBillingMethodByPrgVersion(ByVal PrgVerId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            '  .Append(" SELECT BillingMethodId from arPrgVersions where PrgVerId= ?  ")
            .Append("  SELECT P.BillingMethodId,BillingMethodDescrip from arPrgVersions P, dbo.saBillingMethods B WHERE P.BillingMethodId=B.BillingMethodId AND  PrgVerId= ? ")
        End With

        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        'Dim strBillingMethodId As String
        'Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        'While dr.Read
        '    If Not (dr("BillingMethodId") Is System.DBNull.Value) Then strBillingMethodId = dr("BillingMethodId").ToString Else strBillingMethodId = ""
        'End While
        '   Return strBillingMethodId

        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function IsClockHourProg(ByVal PrgVerId As String) As Boolean
        'connect to the database
        Dim db As New DataAccess
        Dim isClockHourprg As Boolean = False


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            ''New Code Added By VIjay Ramteke On June 18, 2010 For Mantis Id 18607
            ''.Append(" Select  CASE(UnitTypeDescrip) When 'Clock Hours'  Then 1 When 'Minutes' Then 1 Else 0 End As UseTimeClock from arPrgVersions PV, arAttUnitType UT where PV.UnitTypeId=UT.UnitTypeId and PV.PrgVerId=?")
            .Append(" SELECT PV.Hours, CASE(SELECT ACDescrip FROM syAcademicCalendars AC WHERE P.ACId=AC.ACId) WHEN 'Clock Hour' THEN (CASE(SELECT UnitTypeDescrip FROM arAttUnitType UT WHERE PV.UnitTypeId=UT.UnitTypeId) WHEN 'Clock Hours' THEN 1 WHEN 'Minutes' THEN 1 ELSE 0 END) ELSE 0 END AS UseTimeClock FROM  arPrgVersions PV , arPrograms P WHERE PV.ProgId=P.ProgId AND PV.PrgVerId=? ")
            ''New Code Added By VIjay Ramteke On June 18, 2010 For Mantis Id 18607
        End With

        db.AddParameter("@StudentId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read
            isClockHourprg = CType(dr("UseTimeClock").ToString, Boolean)
        End While

        If Not dr.IsClosed Then dr.Close()

        Return isClockHourprg
    End Function

    Public Function IsClockHourProgram(ByVal PrgVerId As String) As Boolean
        Dim ds As DataSet
        Dim db As New SQLDataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@prgVerID", New Guid(PrgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_IsProgramVersion_ProgramClockHourType")
        Catch ex As Exception
            Return False
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False

            End If
        Else
            Return False
        End If
    End Function
    Public Function GetResponsibility(ByVal StudentId As String, ByVal EmployerId As String) As String
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" SELECT JobRoles from adResumeExperience where StudentId= ? and EmployerId = ? ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@EmployerId", EmployerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim strResponsibility As String
        While dr.Read
            strResponsibility = dr("JobRoles").ToString
        End While

        If Not dr.IsClosed Then dr.Close()

        Return strResponsibility
    End Function
    Public Function GetJobRole(ByVal StudentId As String, ByVal EmployerId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" SELECT JobRoles from adResumeExperience where StudentId = ? and EmployerId = ? ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@EmployerId", EmployerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function CheckDuplicateFamilyIncome(ByVal ViewOrder As Integer) As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT     FamilyIncomeId ")
            .Append("FROM       syFamilyIncome ")
            .Append("WHERE      ViewOrder = ? ")
            .Append(";SELECT     ViewOrder ")
            .Append("FROM       syFamilyIncome ")
            .Append("ORDER BY   ViewOrder")
        End With

        db.AddParameter("@ViewOrder", ViewOrder, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        'Execute the query
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function IsLeadEnrolled(ByVal leadId As String) As Boolean
        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT COUNT(*) FROM arStuEnrollments WHERE LeadId = ? ")
        End With

        db.AddParameter("@LeadId", leadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim enrollmentCounter As Integer = db.RunParamSQLScalar(sb.ToString)

        If enrollmentCounter = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Function GetLeadDataForHeaderInfo(ByVal leadId As String) As LeadDataForHeaderInfo

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("        L.LastName + ', ' + L.FirstName as Name, ")
            .Append("       (Select StatusCodeDescrip from syStatusCodes where StatusCodeId=L.LeadStatus) as LeadStatus, ")
            .Append("       L.AssignedDate as DateReceived, ")
            .Append("       (select U.FullName from syUsers U where UserId=L.AdmissionsRep) As AdmissionsRep, ")
            .Append("       (select Caption from syFldCaptions where FldId=664) As DateReceivedCaption, ")
            .Append("       (select Caption from syFldCaptions where FldId=607) As AdmissionsRepCaption ")
            .Append("FROM	adLeads L ")
            .Append("WHERE	L.LeadId=? ")
        End With

        ' Add the leadId to the parameter list
        db.AddParameter("@LeadId", leadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim leadDataForHeaderInfo As New LeadDataForHeaderInfo(leadId)

        While dr.Read()
            '   populate fields with data from DataReader
            With leadDataForHeaderInfo
                .Name = dr("Name")
                .LeadStatus = dr("LeadStatus")
                .DateReceived = dr("DateReceived")
                .AdmissionsRep = dr("AdmissionsRep")
                .AdmissionsRepCaption = dr("AdmissionsRepCaption")
                .DateReceivedCaption = dr("DateReceivedCaption")
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return leadDataForHeaderInfo
        Return leadDataForHeaderInfo

    End Function
    Public Function GetAllLeadsForEmail(ByVal statusCodeId As String, ByVal campusId As String, ByVal prgVerId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("Select DISTINCT ")
            .Append("       L.LastName, ")
            .Append("       L.FirstName + (Select Case Len(Coalesce(L.WorkEmail, L.HomeEmail, '')) when 0 then '(*)' else '' end) FirstName, ")
            .Append("       (Select Case Len(Coalesce(L.WorkEmail, L.HomeEmail, '')) when 0 then 0 else 1 end) As HasEmail, ")
            .Append("       Coalesce(L.WorkEmail, L.HomeEmail, '') Email ")
            .Append("FROM   adLeads L, syStatusCodes SC, sySysStatus SS  ")
            .Append("WHERE ")
            '.Append("       (WorkEmail is not null OR HomeEmail Is not null) ")
            .Append("       L.LeadStatus=SC.StatusCodeId ")
            .Append("AND    SC.SysStatusId=SS.SysStatusId ")

            '   check if the selection is "All Statuses"
            If Not statusCodeId = Guid.Empty.ToString Then
                .Append("AND    SC.StatusCodeId=? ")
                db.AddParameter("@StatusCodeId", statusCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   check if the selection is "All Campus Groups"
            If Not campusId = Guid.Empty.ToString Then
                .Append("AND    L.CampusId = ? ")
                db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   check if the selection is "All Programs" (Enrollments)
            If Not prgVerId = Guid.Empty.ToString Then
                .Append("AND    L.PrgVerId = ? ")
                db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   order by lead name
            .Append("ORDER BY L.LastName, FirstName ")

        End With

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        '   add a column with the concatenation of last and first names
        ds.Tables(0).Columns.Add("Name", GetType(String), "LastName + ' ' + FirstName")

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function GetAllEnrollmentsUsedByLeads() As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   Distinct ")
            .Append("         PV.PrgVerId, PV.PrgVerDescrip ")
            .Append("FROM     adLeads L, arPrgVersions PV,  syStatuses ST ")
            .Append("WHERE  ")
            .Append("         L.PrgVerId = PV.PrgVerId ")
            .Append(" AND     PV.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY PV.PrgVerDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function DeleteCampusFromCampusGroups(ByVal CampusId As String) As String

        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        Dim strGetCampGrpWithJustOneCampus As String
        Dim intCommaPosition As Integer
        With sb
            .Append(" select Distinct GetAllCampusGroupsByCampusId.CampGrpId from ")
            .Append(" ( ")
            .Append("       select Distinct CampGrpId from syCmpGrpCmps where ")
            .Append("       CampusId = ? ")
            .Append(" ) GetAllCampusGroupsByCampusId,")
            .Append(" ( ")
            .Append("       select CampGrpId,Count(*) as Count from syCmpGrpCmps ")
            .Append("       group by CampGrpId having count(*) = 1 ")
            .Append(" ) GetAllCampusGroupsWithJustOneCampus ")
            .Append(" where ")
            .Append("   GetAllCampusGroupsByCampusId.CampGrpId = GetAllCampusGroupsWithJustOneCampus.CampGrpId ")
        End With
        db.AddParameter("@campusid", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            strGetCampGrpWithJustOneCampus &= CType(dr("CampGrpId"), Guid).ToString & "','"
        End While

        If Not dr.IsClosed Then dr.Close()

        intCommaPosition = InStrRev(strGetCampGrpWithJustOneCampus, ",")
        strGetCampGrpWithJustOneCampus = "'" & Mid(strGetCampGrpWithJustOneCampus, 1, intCommaPosition - 1)
        Return strGetCampGrpWithJustOneCampus
    End Function
    Public Function GetAllProgramsAJAX(ByVal PrgGrpID As String) As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append(" select s1.ProgId as ValueColumn,s1.ProgDescrip as DisplayColumn from arPrograms S1,syStatuses ST where s1.StatusId = sT.StatusId  ")
            .Append(" and ProgId in (select ProgId from arPrgVersions where PrgGrpId = (select PrgGrpId from arPrgGrp where PrgGrpId=?)) ")
        End With
        'return dataset
        db.AddParameter("@PrgTypId", PrgGrpID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllProgVersionsByProgramIdAJAX(ByVal ProgId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   Distinct PrgVerId as ValueColumn, PrgVerDescrip as DisplayColumn ")
            .Append("FROM     arPrgVersions,syStatuses  ")
            .Append(" where arPrgVersions.StatusId = syStatuses.StatusId and syStatuses.Status='Active' and ProgId = ? ")
            .Append("ORDER BY PrgVerDescrip ")
        End With

        db.AddParameter("@ProgId", ProgId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllSourceTypeAJAX(ByVal SourceCatagoryID As String) As DataSet
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append(" select LT.SourceTypeID as ValueColumn,LT.SourceTypeDescrip as DisplayColumn ")
            .Append(" FROM   adSourceType LT,syStatuses ST ")
            .Append(" WHERE    LT.StatusId = ST.StatusId ")
            .Append(" and LT.SourceCatagoryID=?")
            .Append(" ORDER BY LT.SourceTypeDescrip ")
        End With
        db.AddParameter("@SourceCatagoryID", SourceCatagoryID, DataAccess.OleDbDataType.OleDbString, 50)
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllSourceAdvAJAX(ByVal SourceTypeID As String) As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Build the sql query
        With sb
            .Append(" Select PR.sourceadvid as ValueColumn,PR.sourceadvdescrip as DisplayColumn")
            .Append(" FROM   adSourceAdvertisement PR,syStatuses ST ")
            .Append(" WHERE  PR.StatusId = ST.StatusId and ST.Status='Active' and PR.SourceTypeID = ? ")
            .Append(" ORDER BY PR.sourceadvdescrip ")
        End With

        'return dataset
        db.AddParameter("@SourceTypeID", SourceTypeID.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllDataAJAX(ByVal ValueColumnName As String, ByVal DisplayColumnName As String, ByVal TableName As String, ByVal WhereColumnName As String, ByVal WhereColumnValue As String, ByVal OrderByColumnName As String) As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append(" Select ")
            .Append(ValueColumnName)
            .Append(" as ValueColumn, ")
            .Append(DisplayColumnName)
            .Append(" as DisplayColumn ")
            .Append(" from ")
            .Append(TableName)
            .Append(" where ")
            .Append(WhereColumnName)
            .Append(" = ? ")
            .Append(" Order By ")
            .Append(OrderByColumnName)
        End With
        'return dataset
        db.AddParameter("@WhereColumnName", WhereColumnValue, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetLeadsByAdmissionsRep(ByVal admissionsRepId As String, ByVal leadStatusId As String)
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       LeadId ")
            .Append("FROM	adLeads ")
            .Append("WHERE ")
            .Append("       1=1 ")
            If Not admissionsRepId = "" Then
                .Append("AND    AdmissionsRep = '" + admissionsRepId + "' ")
            End If
            If Not leadStatusId = "" Then
                .Append("AND	LeadStatus = '" + leadStatusId + "' ")
            End If
            .Append("ORDER BY LastName ")
        End With

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetDefaultLeadStatusCodeId() As String
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid()
        Dim strResult As String 
        'Build the sql query
        With sb
            .Append(" select Distinct StatusCodeId from syStatusCodes where IsDefaultLeadStatus=1 and StatusId='" & strActiveGUID & "' ")
        End With

        Try
            strResult = (db.RunParamSQLScalar(sb.ToString)).ToString
        Catch ex As Exception
            strResult = ""
        End Try
        Return strResult
    End Function
    Public Function GetNewDefaultLeadStatusCodeId(ByVal CampusId As String, ByVal UserId As String) As String
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As  DataSet
        Dim dsGetNewLeadStatus As  DataSet
        Dim strStatusCodeId As String

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetNewLeadStatus = (New LeadStatusChangesDB).GetNewLeadStatuses(CampusId, UserId)
        If dsGetNewLeadStatus.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetNewLeadStatus.Tables(0).Rows
                strStatusCodeId &= row("StatusCodeId").ToString & "','"
            Next
            strStatusCodeId = Mid(strStatusCodeId, 1, InStrRev(strStatusCodeId, "'") - 2)
        End If

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        Dim strResult As String = ""
        'Build the sql query
        With sb
            '.Append(" select Distinct StatusCodeId from syStatusCodes where IsDefaultLeadStatus=1 and StatusId='" & strActiveGUID & "' ")

            'Brings in all the statuscodes set up for the campus 
            'gives priority to status set for All campus group
            .Append(" select Distinct Top 1 StatusCodeId,IsAllCampusGrp from ")
            .Append(" syStatusCodes t1,syCampGrps t2 where t1.CampGrpId = t2.CampGrpId and t1.IsDefaultLeadStatus=1 and t1.StatusId='" & strActiveGUID & "' and ")
            .Append(" t2.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and StatusCodeId in ('")
            .Append(strStatusCodeId)
            .Append(") order by IsAllCampusGrp")
        End With

        Try
            strResult = (db.RunParamSQLScalar(sb.ToString)).ToString
        Catch ex As Exception
            strResult = ""
        End Try
        Return strResult
    End Function
    Public Function GetExistingDefaultLeadStatusCodeId(ByVal CampusId As String, ByVal UserId As String, ByVal CurrentStatusId As String, ByVal LeadId As String) As String
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As New DataSet
        Dim dsGetNewLeadStatus As New DataSet
        Dim strStatusCodeId As String

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetNewLeadStatus = (New LeadStatusChangesDB).GetExistingLeadStatuses(CampusId, UserId, CurrentStatusId, LeadId)
        If dsGetNewLeadStatus.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetNewLeadStatus.Tables(0).Rows
                strStatusCodeId &= row("StatusCodeId").ToString & "','"
            Next
            strStatusCodeId = Mid(strStatusCodeId, 1, InStrRev(strStatusCodeId, "'") - 2)
        End If

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        Dim strResult As String = ""
        'Build the sql query
        With sb
            '.Append(" select Distinct StatusCodeId from syStatusCodes where IsDefaultLeadStatus=1 and StatusId='" & strActiveGUID & "' ")

            'Brings in all the statuscodes set up for the campus 
            'gives priority to status set for All campus group
            .Append(" select Distinct Top 1 StatusCodeId,IsAllCampusGrp from ")
            .Append(" syStatusCodes t1,syCampGrps t2 where t1.CampGrpId = t2.CampGrpId and t1.IsDefaultLeadStatus=1 and t1.StatusId='" & strActiveGUID & "' and ")
            .Append(" t2.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" and StatusCodeId in ('")
            .Append(strStatusCodeId)
            .Append(") ")
        End With

        Try
            strResult = (db.RunParamSQLScalar(sb.ToString)).ToString
        Catch ex As Exception
            strResult = ""
        End Try
        Return strResult
    End Function
    Public Function CheckDuplicateLeadsWithSamePhoneNumber(Optional ByVal CampusId As String = "", _
                                                           Optional ByVal Phone1 As String = "", _
                                                           Optional ByVal Phone2 As String = "") As Integer
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        With sb
            .Append(" Select Count(*) from adLeads ")
            If Not CampusId = "" Then
                .Append(" Where ")
            End If
            If Not CampusId = "" Then
                .Append(" CampusId = ? ")
            End If
            If Not Phone1 = "" Then
                If Not CampusId = "" Then
                    .Append(" and ")
                End If
                .Append(" Phone = ? ")
            End If
            If Not Phone2 = "" Then
                If Not Phone1 = "" Or Not CampusId = "" Then
                    .Append(" and ")
                End If
                .Append(" Phone2 = ? ")
            End If
        End With
        If Not CampusId = "" Then
            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not Phone1 = "" Then
            db.AddParameter("@Phone1", Phone1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not Phone2 = "" Then
            db.AddParameter("@Phone2", Phone2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        Try
            Dim intDuplicateLeads As Integer = db.RunParamSQLScalar(sb.ToString)
            Return intDuplicateLeads
        Catch ex As Exception
            Return 0
        End Try
    End Function
    Public Function CheckDuplicateLeadsWithSameEmail(Optional ByVal CampusId As String = "", _
                                                        Optional ByVal WorkEmail As String = "", _
                                                        Optional ByVal HomeEmail As String = "") As Integer
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        With sb
            .Append(" Select Count(*) from adLeads ")
            If Not CampusId = "" Then
                .Append(" Where ")
            End If
            If Not CampusId = "" Then
                .Append(" CampusId = ? ")
            End If
            If Not WorkEmail = "" Then
                If Not CampusId = "" Then
                    .Append(" and ")
                End If
                .Append(" WorkEmail = ? ")
            End If
            If Not HomeEmail = "" Then
                If Not WorkEmail = "" Or Not CampusId = "" Then
                    .Append(" and ")
                End If
                .Append(" HomeEmail = ? ")
            End If
        End With
        If Not CampusId = "" Then
            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not WorkEmail = "" Then
            db.AddParameter("@WorkEmail", WorkEmail, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not HomeEmail = "" Then
            db.AddParameter("@HomeEmail", HomeEmail, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        Try
            Dim intDuplicateLeads As Integer = db.RunParamSQLScalar(sb.ToString)
            Return intDuplicateLeads
        Catch ex As Exception
            Return 0
        End Try

    End Function
    Public Function CheckDuplicateLeadsWithSameAddress(Optional ByVal CampusId As String = "", _
                                                       Optional ByVal Address1 As String = "", _
                                                       Optional ByVal Address2 As String = "", _
                                                       Optional ByVal city As String = "", _
                                                       Optional ByVal state As String = "", _
                                                       Optional ByVal zip As String = "") As Integer
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        With sb
            .Append(" Select Count(*) from adLeads ")
            If Not CampusId = "" Then
                .Append(" Where ")
            End If
            If Not CampusId = "" Then
                .Append(" CampusId = ? ")
            End If
            If Not Address1 = "" Then
                If Not CampusId = "" Then
                    .Append(" and ")
                End If
                .Append(" Address1 = ? ")
            End If
            If Not Address2 = "" Then
                If Not Address1 = "" Or Not CampusId = "" Then
                    .Append(" and ")
                End If
                .Append(" Address2 = ? ")
            End If
            If Not city = "" Then
                If Not Address1 = "" Or Not Address2 = "" Or Not CampusId = "" Then
                    .Append(" and ")
                End If
                .Append(" City = ? ")
            End If
            If Not state = "" Then
                If Not Address1 = "" Or Not Address2 = "" Or Not CampusId = "" Or Not city = "" Then
                    .Append(" and ")
                End If
                .Append(" StateId = ? ")
            End If
            If Not zip = "" Then
                If Not Address1 = "" Or Not Address2 = "" Or Not CampusId = "" Or Not city = "" Or Not state = "" Then
                    .Append(" and ")
                End If
                .Append(" Zip = ? ")
            End If
        End With
        If Not CampusId = "" Then
            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not Address1 = "" Then
            db.AddParameter("@Address1", Address1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not Address2 = "" Then
            db.AddParameter("@Address2", Address2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not city = "" Then
            db.AddParameter("@city", city, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not state = "" Then
            db.AddParameter("@state", state, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not zip = "" Then
            db.AddParameter("@zip", zip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        Try
            Dim intDuplicateLeads As Integer = db.RunParamSQLScalar(sb.ToString)
            Return intDuplicateLeads
        Catch ex As Exception
            Return 0
        End Try
    End Function
    Public Function CheckDuplicateLeads(ByVal FirstName As String, ByVal LastName As String, Optional ByVal SSN As String = "", Optional ByVal DOB As String = "", Optional ByVal MiddleName As String = "", Optional ByVal CampusId As String = "") As Integer
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        With sb
            .Append(" Select Count(*) from adLeads ")
            If Not FirstName = "" Or Not LastName = "" Or Not SSN = "" Or Not DOB = "" Or Not MiddleName = "" Or Not CampusId = "" Then
                .Append(" Where ")
            End If
            If Not FirstName = "" Then
                .Append(" firstname = ? ")
            End If
            If Not LastName = "" Then
                If Not FirstName = "" Then
                    .Append(" and ")
                End If
                .Append(" lastname = ? ")
            End If
            If Not SSN = "" Then
                If Not FirstName = "" Or Not LastName = "" Then
                    .Append(" and ")
                End If
                .Append(" ssn = ? ")
            End If
            If Not DOB = "" Then
                If Not FirstName = "" Or Not LastName = "" Or Not SSN = "" Then
                    .Append(" and ")
                End If
                .Append(" BirthDate = ? ")
            End If
            If Not MiddleName = "" Then
                If Not FirstName = "" Or Not LastName = "" Or Not SSN = "" Or Not DOB = "" Then
                    .Append(" and ")
                End If
                .Append(" MiddleName = ? ")
            End If
            If Not CampusId = "" Then
                If Not FirstName = "" Or Not LastName = "" Or Not SSN = "" Or Not DOB = "" Or Not MiddleName = "" Then
                    .Append(" and ")
                End If
                .Append(" CampusId = ? ")
            End If
        End With
        If Not FirstName = "" Then
            db.AddParameter("@FirstName", FirstName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not LastName = "" Then
            db.AddParameter("@LastName", LastName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not SSN = "" Then
            db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not DOB = "" Then
            db.AddParameter("@DOB", DOB, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not MiddleName = "" Then
            db.AddParameter("@MiddleName", MiddleName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not CampusId = "" Then
            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        Try
            Dim intDuplicateLeads As Integer = db.RunParamSQLScalar(sb.ToString)
            Return intDuplicateLeads
        Catch ex As Exception
            Return 0
        End Try

    End Function

    Public Function CheckDuplicateSSN(ByVal FirstName As String, ByVal LastName As String, ByVal SSN As String) As String

        If String.IsNullOrEmpty(FirstName) Or String.IsNullOrEmpty(LastName) Or String.IsNullOrEmpty(SSN) Then
            Return Nothing
        Else
            Dim db As New SQLDataAccess
            Dim sb As New StringBuilder

            Try

                db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")

                db.AddParameter("@SSN", SSN, SqlDbType.VarChar, 50, ParameterDirection.Input)
                db.AddParameter("@FirstName", FirstName, SqlDbType.VarChar, 250, ParameterDirection.Input)
                db.AddParameter("@LastName", LastName, SqlDbType.VarChar, 250, ParameterDirection.Input)

                Using rs As SqlDataReader = db.RunParamSQLDataReader_SP("dbo.CheckForDuplicateSSN")

                    If rs.HasRows Then
                        sb.Append("SSN already exists for the lead(s) or Student(s) below:" & vbCrLf)
                        Do While rs.Read()
                            With sb
                                .Append(rs("FirstName").ToString() & " " & rs("LastName").ToString())
                                If rs("Status") IsNot DBNull.Value Then
                                    .Append(", Status: " & rs("Status") & vbCrLf)
                                End If
                            End With
                        Loop
                    Else
                        Return Nothing
                    End If
                End Using

                Return sb.ToString()

            Catch ex As Exception
                Return Nothing
            Finally
                db.CloseConnection()
            End Try

        End If

    End Function
    Public Function AddElectronicLead(ByVal selectedLeadId() As String, _
                                ByVal selectedFirstName() As String, _
                                ByVal selectedLastName() As String, _
                                ByVal selectedAddress() As String, _
                                ByVal selectedCity() As String, _
                                ByVal selectedState() As String, _
                                ByVal selectedZip() As String, _
                                ByVal selectedPhone() As String, _
                                ByVal selectedPhoneType() As String, _
                                ByVal selectedEmail() As String, _
                                ByVal selectedEmailType() As String, _
                                ByVal selectedMaritalStatus() As String, _
                                ByVal selectedSourceCategory() As String, _
                                ByVal selectedSourceType() As String, _
                                ByVal selectedSourceAdvertisement() As String, _
                                ByVal selectedArea() As String, _
                                ByVal selectedProgramType() As String, _
                                ByVal selectedProgramVersion() As String, _
                                ByVal CampusId As String, _
                                ByVal UserId As String, _
                                ByVal SubmittedDate As DateTime) As Integer


        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'Dim sb As New StringBuilder
        Dim intArrayCount As Integer = selectedLeadId.Length
        Dim intLoop As Integer
        Dim strSQL, strInsertQuery As String
        Dim strPhoneType, strLeadStatus As String
        Dim strStateId, strPrgGrpId, strProgId, strPrgVerId, strSourceCatagoryId, strSourceTypeId, strSourceAdvertisementId As String
        Dim dsGetAdmissionReps As  DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim dsGetCmpGrps As  DataSet
        Dim getLeadStatus As  DataSet
        'Dim strDateAssigned As Date = Date.Now.ToShortDateString
        Dim intRepIncrement As Integer = 0
        Dim intDuplicateCountIncrement As Integer = 0
        Dim intDuplicateCount As Integer

        getLeadStatus = (New LeadStatusChangesDB).GetNewLeadStatuses(CampusId, UserId)
        If getLeadStatus.Tables(0).Rows.Count >= 1 Then
            Dim row As DataRow = getLeadStatus.Tables(0).Rows(0)
            strLeadStatus = row("StatusCodeId").ToString
        End If


        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            Dim row As DataRow = dsGetCmpGrps.Tables(0).Rows(0)
            strCampGrpId = row("CampGrpId").ToString
        End If

        dsGetAdmissionReps = GetAdmissionReps(CampusId)
        Dim intRepCount As Integer = dsGetAdmissionReps.Tables(0).Rows.Count
        Dim selectedAdmissionsRep() As String = Array.CreateInstance(GetType(String), intRepCount)
        If dsGetAdmissionReps.Tables(0).Rows.Count >= 1 Then
            Dim k As Integer
            For Each row As DataRow In dsGetAdmissionReps.Tables(0).Rows
                selectedAdmissionsRep.SetValue(row("UserId").ToString, intRepIncrement)
                intRepIncrement += 1
            Next
        End If
        Dim intAdmissionRepLoop As Integer = 0

        While intLoop < intArrayCount
            'Get Phone Type or Insert a new Phone type for that campus
            If Not selectedPhoneType.GetValue(intLoop).ToString = "" Then
                strSQL = "select Distinct PhoneTypeId from syPhoneType where PhoneTypeDescrip like '" & selectedPhoneType.GetValue(intLoop).ToString & "%'"
                Try
                    strPhoneType = db.RunParamSQLScalar(strSQL).ToString
                Catch ex As Exception
                    strPhoneType = Guid.NewGuid.ToString
                    strInsertQuery = "insert into syPhoneType(PhoneTypeId,PhoneTypeCode,PhoneTypeDescrip,StatusId,CampGrpId,ModUser,ModDate) values(?,?,?,?,?,?,?)"
                    db.AddParameter("@PhoneTypeId", strPhoneType)
                    db.AddParameter("@PhoneTypeCode", selectedPhoneType.GetValue(intLoop).ToString.Substring(1, 4))
                    db.AddParameter("@PhoneTypeDescrip", selectedPhoneType.GetValue(intLoop).ToString)
                    db.AddParameter("@StatusId", strActiveGUID)
                    db.AddParameter("@CampGrpId", strCampGrpId, DataAccess.OleDbDataType.OleDbString)
                    db.AddParameter("@ModUser", "sa")
                    db.AddParameter("@ModDate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbString)
                    db.RunParamSQLExecuteNoneQuery(strInsertQuery)
                    db.ClearParameters()
                End Try
            Else
                strPhoneType = ""
            End If

            'Get State
            If Not selectedState.GetValue(intLoop).ToString = "" Then
                strSQL = "select Distinct StateId from syStates where StateDescrip like '" & selectedState.GetValue(intLoop).ToString & "%'"
                Try
                    strStateId = db.RunParamSQLScalar(strSQL).ToString
                Catch ex As Exception
                    strStateId = ""
                End Try
            Else
                strStateId = ""
            End If

            'Get Program Group
            If Not selectedArea.GetValue(intLoop).ToString = "" Then
                strSQL = "select Distinct PrgGrpId from arPrgGrp where PrgGrpDescrip like '" & selectedArea.GetValue(intLoop).ToString & "%'"
                Try
                    strPrgGrpId = db.RunParamSQLScalar(strSQL).ToString
                Catch ex As Exception
                    strPrgGrpId = Guid.NewGuid.ToString
                    strInsertQuery = "insert into arPrgGrp(PrgGrpId,PrgGrpCode,PrgGrpDescrip,StatusId,ModUser,ModDate) values(?,?,?,?,?,?)"
                    db.AddParameter("@PhoneTypeId", strPrgGrpId)
                    db.AddParameter("@PhoneTypeCode", selectedArea.GetValue(intLoop).ToString.Substring(1, 4))
                    db.AddParameter("@PhoneTypeDescrip", selectedArea.GetValue(intLoop).ToString)
                    db.AddParameter("@StatusId", strActiveGUID)
                    db.AddParameter("@ModUser", "sa")
                    db.AddParameter("@ModDate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbString)
                    db.RunParamSQLExecuteNoneQuery(strInsertQuery)
                    db.ClearParameters()
                End Try
            Else
                strPrgGrpId = ""
            End If

            If Not selectedProgramType.GetValue(intLoop).ToString = "" Then
                strSQL = "select Distinct ProgId from arPrograms where ProgDescrip like '" & selectedProgramType.GetValue(intLoop).ToString & "%'"
                Try
                    strProgId = db.RunParamSQLScalar(strSQL).ToString
                Catch ex As Exception
                    strProgId = Guid.NewGuid.ToString
                    strInsertQuery = "insert into arPrograms(ProgId,ProgCode,ProgDescrip,StatusId,ModUser,ModDate) values(?,?,?,?,?,?)"
                    db.AddParameter("@PhoneTypeId", strProgId)
                    db.AddParameter("@PhoneTypeCode", selectedProgramType.GetValue(intLoop).ToString.Substring(1, 4))
                    db.AddParameter("@PhoneTypeDescrip", selectedProgramType.GetValue(intLoop).ToString)
                    db.AddParameter("@StatusId", strActiveGUID)
                    db.AddParameter("@ModUser", "sa")
                    db.AddParameter("@ModDate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbString)
                    db.RunParamSQLExecuteNoneQuery(strInsertQuery)
                    db.ClearParameters()
                End Try
            Else
                strProgId = ""
            End If

            If Not selectedProgramVersion.GetValue(intLoop).ToString = "" Then
                strSQL = "select Distinct PrgVerId from arPrgVersions where PrgVerDescrip like '" & selectedProgramVersion.GetValue(intLoop).ToString & "%'"
                Try
                    strPrgVerId = db.RunParamSQLScalar(strSQL).ToString
                    strSQL = ""
                Catch ex As Exception
                    strPrgVerId = Guid.NewGuid.ToString
                    Dim intWeeks As Integer = 0
                    Dim intTerms As Integer = 0
                    Dim decHours As Decimal = 0.0
                    Dim decCredits As Decimal = 0.0
                    Dim strDeptId, strGrdSystemId, strBillingMethodId, strTuitionEarningId, strProgTypId, strUnitTypeId As String

                    'Get Dept
                    strSQL = "Select Top 1 DeptId from arDepartments"
                    Try
                        strDeptId = db.RunParamSQLScalar(strSQL).ToString
                    Catch ex1 As Exception
                        strDeptId = Guid.Empty.ToString
                    End Try
                    strSQL = ""

                    'GrdSystemId
                    strSQL = "select Top 1 GrdSystemId from arGradesystems where CampGrpId in (select Distinct CampGrpId from syCmpGrpCmps where CampusId='" & CampusId & "')"
                    Try
                        strGrdSystemId = db.RunParamSQLScalar(strSQL).ToString
                    Catch ex2 As Exception
                        strGrdSystemId = Guid.Empty.ToString
                    End Try
                    strSQL = ""

                    'BillingMethodId
                    strSQL = "select Top 1 BillingMethodId from saBillingMethods where CampGrpId in (select Distinct CampGrpId from syCmpGrpCmps where CampusId='" & CampusId & "')"
                    Try
                        strBillingMethodId = db.RunParamSQLScalar(strSQL).ToString
                    Catch ex3 As Exception
                        strBillingMethodId = Guid.Empty.ToString
                    End Try
                    strSQL = ""

                    'TuitionEarningId
                    strSQL = "select Top 1 TuitionEarningId from saTuitionEarnings where CampGrpId in (select Distinct CampGrpId from syCmpGrpCmps where CampusId='" & CampusId & "')"
                    Try
                        strTuitionEarningId = db.RunParamSQLScalar(strSQL).ToString
                    Catch ex4 As Exception
                        strTuitionEarningId = Guid.Empty.ToString
                    End Try
                    strSQL = ""

                    'ProgTypId
                    strSQL = "select Top 1 ProgTypId from arProgTypes where CampGrpId in (select Distinct CampGrpId from syCmpGrpCmps where CampusId='" & CampusId & "')"
                    Try
                        strProgTypId = db.RunParamSQLScalar(strSQL).ToString
                    Catch ex5 As Exception
                        strProgTypId = Guid.Empty.ToString
                    End Try
                    strSQL = ""


                    'Unit Types
                    strSQL = "select Top 1 UnitTypeId from arAttUnitType"
                    Try
                        strUnitTypeId = db.RunParamSQLScalar(strSQL).ToString
                    Catch ex6 As Exception
                        strUnitTypeId = Guid.Empty.ToString
                    End Try
                    strSQL = ""

                    strInsertQuery = "insert into arPrgVersions(PrgVerId,PrgVerCode,PrgVerDescrip,ProgId,PrgGrpId,StatusId,CampGrpId,ModUser,ModDate,Weeks,Terms,Hours,Credits,FullTime,DeptId,GrdSystemId,BillingMethodId,TuitionEarningId,ProgTypId,UnitTypeId,TrackTardies) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
                    db.AddParameter("@PhoneTypeId", strPrgVerId)
                    db.AddParameter("@PhoneTypeCode", selectedProgramVersion.GetValue(intLoop).ToString.Substring(1, 4))
                    db.AddParameter("@PhoneTypeDescrip", selectedProgramVersion.GetValue(intLoop).ToString)
                    db.AddParameter("@ProgId", strProgId)
                    db.AddParameter("@PrgGrpId", strPrgGrpId)
                    db.AddParameter("@StatusId", strActiveGUID)
                    db.AddParameter("@CampGrpId", strCampGrpId, DataAccess.OleDbDataType.OleDbString)
                    db.AddParameter("@ModUser", "sa")
                    db.AddParameter("@ModDate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbString)
                    db.AddParameter("@Weeks", intWeeks)
                    db.AddParameter("@Terms", intTerms)
                    db.AddParameter("@Hours", decHours)
                    db.AddParameter("@Credits", decCredits)
                    db.AddParameter("@FullTime", 0)
                    db.AddParameter("@DeptId", strDeptId)
                    db.AddParameter("@GrdSystemId", strGrdSystemId)
                    db.AddParameter("@BillingMethodId", strBillingMethodId)
                    db.AddParameter("@TuitionEarning", strTuitionEarningId)
                    db.AddParameter("@ProgTypeId", strProgTypId)
                    db.AddParameter("@UnitTypeId", strUnitTypeId)
                    db.AddParameter("@TrackTardies", 0)


                    db.RunParamSQLExecuteNoneQuery(strInsertQuery)
                    db.ClearParameters()
                End Try
            Else
                strPrgVerId = ""
            End If

            'Get Source Catagory
            If Not selectedSourceCategory.GetValue(intLoop).ToString = "" Then
                strSQL = "select Distinct SourceCatagoryId from adSourceCatagory where SourceCatagoryDescrip like '" & selectedSourceCategory.GetValue(intLoop).ToString & "%'"
                Try
                    strSourceCatagoryId = db.RunParamSQLScalar(strSQL).ToString
                Catch ex As Exception
                    strSourceCatagoryId = Guid.NewGuid.ToString
                    strInsertQuery = "insert into adSourceCatagory(SourceCatagoryId,SourceCatagoryCode,SourceCatagoryDescrip,StatusId,CampGrpId,ModUser,ModDate) values(?,?,?,?,?,?,?)"
                    db.AddParameter("@PhoneTypeId", strSourceCatagoryId)
                    db.AddParameter("@PhoneTypeCode", selectedSourceCategory.GetValue(intLoop).ToString.Substring(1, 4))
                    db.AddParameter("@PhoneTypeDescrip", selectedSourceCategory.GetValue(intLoop).ToString)
                    db.AddParameter("@StatusId", strActiveGUID)
                    db.AddParameter("@CampGrpId", strCampGrpId, DataAccess.OleDbDataType.OleDbString)
                    db.AddParameter("@ModUser", "sa")
                    db.AddParameter("@ModDate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbString)
                    db.RunParamSQLExecuteNoneQuery(strInsertQuery)
                    db.ClearParameters()
                End Try
            Else
                strSourceCatagoryId = ""
            End If

            If Not selectedSourceType.GetValue(intLoop).ToString = "" Then
                strSQL = "select Distinct SourceTypeId from adSourceType where SourceTypeDescrip like '" & selectedSourceType.GetValue(intLoop).ToString & "%'"
                Try
                    strSourceTypeId = db.RunParamSQLScalar(strSQL).ToString
                Catch ex As Exception
                    strSourceTypeId = Guid.NewGuid.ToString
                    strInsertQuery = "insert into adSourceType(SourceTypeId,SourceTypeCode,SourceTypeDescrip,SourceCatagoryId,StatusId,CampGrpId,ModUser,ModDate) values(?,?,?,?,?,?,?,?)"
                    db.AddParameter("@PhoneTypeId", strSourceTypeId)
                    db.AddParameter("@PhoneTypeCode", selectedSourceType.GetValue(intLoop).ToString.Substring(1, 4))
                    db.AddParameter("@PhoneTypeDescrip", selectedSourceType.GetValue(intLoop).ToString)
                    db.AddParameter("@SourceCatagoryId", strSourceCatagoryId)
                    db.AddParameter("@StatusId", strActiveGUID)
                    db.AddParameter("@CampGrpId", strCampGrpId, DataAccess.OleDbDataType.OleDbString)
                    db.AddParameter("@ModUser", "sa")
                    db.AddParameter("@ModDate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbString)
                    db.RunParamSQLExecuteNoneQuery(strInsertQuery)
                    db.ClearParameters()
                End Try
            Else
                strSourceTypeId = ""
            End If

            If Not selectedSourceAdvertisement.GetValue(intLoop).ToString = "" Then
                strSQL = "select Distinct SourceAdvertisementId from adSourceAdvertisement where SourceAdvertisementDescrip like '" & selectedSourceAdvertisement.GetValue(intLoop).ToString & "%'"
                Try
                    strSourceAdvertisementId = db.RunParamSQLScalar(strSQL).ToString
                Catch ex As Exception
                    strSourceAdvertisementId = Guid.NewGuid.ToString

                    'AdvInterval
                    Dim strAdvIntervalId As String
                    strSQL = "select Top 1 AdvIntervalId from adAdvInterval where CampGrpId in (select Distinct CampGrpId from syCmpGrpCmps where CampusId='" & CampusId & "')"
                    Try
                        strAdvIntervalId = db.RunParamSQLScalar(strSQL).ToString
                    Catch ex5 As Exception
                        strAdvIntervalId = Guid.Empty.ToString
                    End Try
                    strSQL = ""

                    strInsertQuery = "insert into adSourceAdvertisement(SourceAdvId,SourceAdvCode,SourceAdvDescrip,SourceTypeId,StatusId,CampGrpId,ModUser,ModDate,StartDate,EndDate,Cost,AdvIntervalId) values(?,?,?,?,?,?,?,?,?,?,?,?)"
                    db.AddParameter("@PhoneTypeId", strSourceAdvertisementId)
                    db.AddParameter("@PhoneTypeCode", selectedSourceAdvertisement.GetValue(intLoop).ToString.Substring(1, 4))
                    db.AddParameter("@PhoneTypeDescrip", selectedSourceAdvertisement.GetValue(intLoop).ToString)
                    db.AddParameter("@SourceCatagoryId", strSourceTypeId)
                    db.AddParameter("@StatusId", strActiveGUID)
                    db.AddParameter("@CampGrpId", strCampGrpId, DataAccess.OleDbDataType.OleDbString)
                    db.AddParameter("@ModUser", "sa")
                    db.AddParameter("@ModDate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbString)
                    db.AddParameter("@StartDate", "12/1/2006")
                    db.AddParameter("@EndDate", "12/30/2006")
                    db.AddParameter("@Cost", 0.0)
                    db.AddParameter("@AdvIntervalId", strAdvIntervalId)
                    db.RunParamSQLExecuteNoneQuery(strInsertQuery)
                    db.ClearParameters()
                End Try
            Else
                strSourceAdvertisementId = ""
            End If

            Dim strMaritalStatus As String
            If Not selectedMaritalStatus.GetValue(intLoop).ToString = "" Then
                strSQL = "select Distinct MaritalStatId from adMaritalStatus where MaritalStatDescrip like '" & selectedMaritalStatus.GetValue(intLoop).ToString & "%'"
                Try
                    strMaritalStatus = db.RunParamSQLScalar(strSQL).ToString
                Catch ex As Exception
                    strMaritalStatus = Guid.NewGuid.ToString
                    strInsertQuery = "insert into adMaritalStatus(MaritalStatId,MaritalStatCode,MaritalStatDescrip,StatusId,CampGrpId,ModUser,ModDate) values(?,?,?,?,?,?,?)"
                    db.AddParameter("@PhoneTypeId", strMaritalStatus)
                    db.AddParameter("@PhoneTypeCode", selectedMaritalStatus.GetValue(intLoop).ToString.Substring(1, 4))
                    db.AddParameter("@PhoneTypeDescrip", selectedMaritalStatus.GetValue(intLoop).ToString)
                    db.AddParameter("@StatusId", strActiveGUID)
                    db.AddParameter("@CampGrpId", strCampGrpId, DataAccess.OleDbDataType.OleDbString)
                    db.AddParameter("@ModUser", "sa")
                    db.AddParameter("@ModDate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbString)
                    db.RunParamSQLExecuteNoneQuery(strInsertQuery)
                    db.ClearParameters()
                End Try
            Else
                strMaritalStatus = ""
            End If
            Dim sbInsertLead As New StringBuilder
            With sbInsertLead
                .Append(" Insert into adLeads ")
                .Append(" (LeadId,FirstName,LastName,Address1,City,StateId,Zip, ")
                .Append(" Phone,PhoneType,HomeEmail,MaritalStatus, ")
                .Append(" SourceCategoryId,SourceTypeId,SourceAdvertisement, ")
                .Append(" AreaId,ProgramId,PrgVerId, ")
                .Append(" AssignedDate,DateApplied,LeadStatus,CampusId,AddressStatus,PhoneStatus,AdmissionsRep,ForeignPhone,ForeignZip,ModUser,ModDate,Comments) ")
                .Append(" Values ")
                .Append("  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            'LeadId
            db.AddParameter("@LeadId", selectedLeadId.GetValue(intLoop).ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'First Name
            db.AddParameter("@FirstName", selectedFirstName.GetValue(intLoop).ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Last Name
            db.AddParameter("@LastName", selectedLastName.GetValue(intLoop).ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Address
            If selectedAddress.GetValue(intLoop).ToString = "" Then
                db.AddParameter("@Address1", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", selectedAddress.GetValue(intLoop).ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'city
            If selectedCity.GetValue(intLoop).ToString = "" Then
                db.AddParameter("@city", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@city", selectedCity.GetValue(intLoop).ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'StateId
            If strStateId = "" Then
                db.AddParameter("@State", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@State", strStateId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Zip
            If selectedZip.GetValue(intLoop).ToString = "" Then
                db.AddParameter("@zip", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@zip", selectedZip.GetValue(intLoop).ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If



            'HomePhone
            If selectedPhone.GetValue(intLoop).ToString = "" Then
                db.AddParameter("@HomePhone", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HomePhone", selectedPhone.GetValue(intLoop).ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'PhoneType
            If strPhoneType = "" Then
                db.AddParameter("@PhoneType", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PhoneType", strPhoneType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'HomeEmail
            If selectedEmail.GetValue(intLoop).ToString = "" Then
                db.AddParameter("@HomeEmail", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HomeEmail", selectedEmail.GetValue(intLoop).ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If



            'MaritalStatus
            If selectedMaritalStatus.GetValue(intLoop).ToString = "" Then
                db.AddParameter("@MaritalStatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@MaritalStatus", strMaritalStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If





            'SourceCatagoryID
            If selectedSourceCategory.GetValue(intLoop).ToString = "" Then
                db.AddParameter("@SourceCategoryID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@SourceCategoryID", strSourceCatagoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'SourceTypeID
            If selectedSourceType.GetValue(intLoop).ToString = "" Then
                db.AddParameter("@SourceTypeID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@SourceTypeID", strSourceTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'SourceAdvertisement
            If selectedSourceAdvertisement.GetValue(intLoop).ToString = "" Then
                db.AddParameter("@SourceAdvertisement", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@SourceAdvertisement", strSourceAdvertisementId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'AreaID
            If selectedArea.GetValue(intLoop).ToString = "" Then
                db.AddParameter("@AreaID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@AreaID", strPrgGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'ProgramId
            If selectedProgramType.GetValue(intLoop).ToString = "" Then
                db.AddParameter("@ProgramID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ProgramID", strProgId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'PrgVerId
            If selectedProgramVersion.GetValue(intLoop).ToString = "" Then
                db.AddParameter("@PrgVerID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PrgVerID", strPrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Assigned Date

            db.AddParameter("@AssignedDate", SubmittedDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            'Applied Date

            db.AddParameter("@AppliedDate", SubmittedDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)




            'LeadStatus
            If strLeadStatus = "" Then
                db.AddParameter("@LeadStatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@LeadStatus", strLeadStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'CampusId
            If CampusId = Guid.Empty.ToString Or CampusId = "" Then
                db.AddParameter("@CampusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If



            'Address Status
            db.AddParameter("@AddressStatus", strActiveGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            'Phone Status
            db.AddParameter("@PhoneStatus", strActiveGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'AdmissionsRep
            db.AddParameter("@AdmissionsRep", selectedAdmissionsRep.GetValue(intAdmissionRepLoop).ToString, DataAccess.OleDbDataType.OleDbString)

            'ForeignPhone
            db.AddParameter("@ForeignPhone", 0)

            'ForeignZip
            db.AddParameter("@ForeignZip", 0)

            ''ModUser
            db.AddParameter("@ModUser", "sa", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.AddParameter("@Comments", "Import", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute The Query
            Try
                intDuplicateCount = CheckDuplicateImportedLead(selectedFirstName.GetValue(intLoop).ToString, selectedLastName.GetValue(intLoop).ToString, selectedPhone.GetValue(intLoop).ToString)
                If intDuplicateCount >= 1 Then
                    intDuplicateCountIncrement += 1
                    db.ClearParameters()
                    sbInsertLead.Remove(0, sbInsertLead.Length)
                    Exit Try
                End If
                db.RunParamSQLExecuteNoneQuery(sbInsertLead.ToString)
                db.ClearParameters()
                sbInsertLead.Remove(0, sbInsertLead.Length)
            Catch ex As OleDbException
                Return DALExceptions.BuildErrorMessage(ex)
            Finally
            End Try
            intLoop += 1
            intAdmissionRepLoop += 1
            If intAdmissionRepLoop = selectedLeadId.Length Or intAdmissionRepLoop = intRepIncrement Then
                intAdmissionRepLoop = 0
            End If
        End While
        If intDuplicateCountIncrement >= 1 Then
            Return intDuplicateCountIncrement
        Else
            Return 0
        End If


    End Function
    Public Function GetAdmissionReps(ByVal CampusId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append("   select Distinct t1.UserId from syUsers t1,syRoles t2,syUsersRolesCampGrps t3 ")
            .Append("   where t2.SysRoleId = 3 and t2.RoleId = t3.RoleId and t1.UserId=t3.UserId and ")
            .Append("   CampGrpId in (select CampGrpId from syCmpGrpCmps where CampusId='" & CampusId & "') ")
            .Append("   and t1.IsLoggedIn = 1 ")
        End With
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAdmissionRepsAndLeadAssignedCount(ByVal CampusId As String, ByVal SubmittedDate As DateTime) As DataSet
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Dim dtAssignedDate As Date = Date.Now.ToShortDateString
        With sb
            .Append("   select Distinct t1.UserId,t1.FullName, ")
            .Append("   (select Count(*) from adLeads where AdmissionsRep=t1.UserId and Comments='Import' and AssignedDate='" & SubmittedDate & "') as LeadsAssigned ")
            .Append("   from syUsers t1,syRoles t2,syUsersRolesCampGrps t3 ")
            .Append("   where t2.SysRoleId = 3 and t2.RoleId = t3.RoleId and t1.UserId=t3.UserId and ")
            .Append("   CampGrpId in (select CampGrpId from syCmpGrpCmps where CampusId='" & CampusId & "') ")
            .Append("   and t1.IsLoggedIn = 1 Order By t1.FullName ")
        End With
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllImportedLeads(ByVal CampusId As String, ByVal DuplicateCount As String, ByVal SubmittedDate As DateTime) As DataSet
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Dim dtAssignedDate As Date = Date.Now.ToShortDateString
        With sb
            .Append(" Select A.SourceCategoryId, ")
            .Append(" (Select Top 1  SourceCatagoryDescrip from adSourceCatagory where SourceCatagoryId=A.SourceCategoryId) as SourceCategory, ")
            .Append(" Count(*) as SourceCategoryCount ")
            .Append("   from adLeads A  ")
            .Append(" where A.Comments='Import' and A.AssignedDate='" & SubmittedDate & "' ")
            .Append(" and A.CampusId='" & CampusId & "' and A.SourceCategoryId is not null ")
            .Append(" Group by A.SourceCategoryId")
            If Not DuplicateCount = "0" Then
                .Append(" Union ")
                .Append(" Select '00000000-0000-0000-0000-000000000000','Duplicate Leads found', ")
                .Append(DuplicateCount)
                .Append(" Order by SourceCategoryId desc ")
            End If
        End With
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllExistingDocumentStatus(ByVal LeadDocumentId As String, ByVal CampusId As String) As DataSet
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As  DataSet

        'Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)

        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        With sb
            .Append(" select Distinct SD.DocStatusId as DocStatusIdId,'(X) ' + R.DocStatusDescrip as DocStatusDescrip ")
            .Append(" from ")
            .Append(" adLeadDocsReceived  SD,syDocStatuses R ")
            .Append(" WHERE SD.DocStatusId = R.DocStatusId AND SD.StatusId='1AF592A6-8790-48EC-9916-5412C25EF49F' AND SD.LeadDocId='" & LeadDocumentId & "' ")
            .Append(" Union ")
            .Append(" Select SG.DocStatusId, SG.DocStatusDescrip ")
            .Append(" from syDocStatuses SG,syStatuses ST  ")
            .Append(" where SG.StatusId = St.StatusId and ST.Status = 'Active' ")
            .Append("AND SG.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" Order By SG.DocStatusDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function CheckDuplicateImportedLead(ByVal strFirstName As String, ByVal strLastName As String, ByVal strPhone As String) As Integer
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append("Select Count(*) from adLeads where FirstName='" & strFirstName & "' and LastName='" & strLastName & "' and Phone='" & strPhone & "' and Comments='Import'")
        End With
        Dim intDuplicateCount As Integer = db.RunParamSQLScalar(sb.ToString)
        Return intDuplicateCount
    End Function
    Public Function GetLeadGroupsByCampus(ByVal campusId As String) As DataSet
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append("select distinct llg.LeadGrpId,lg.Descrip ")
            .Append("from adLeadByLeadGroups llg, adLeadGroups lg, syCmpGrpCmps cgc ")
            .Append("where llg.LeadGrpId=lg.LeadGrpId ")
            .Append("and cgc.CampGrpId=lg.CampGrpId ")
            .Append("and cgc.CampusId=? ")
            .Append("order by lg.Descrip ")
        End With

        db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetQuickLeadFields() As DataSet
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append(" Select Distinct ")
            .Append(" t1.SectionName,t3.FldName,t3.FldId,t1.SectionId,t4.Caption ")
            .Append(" from adQuickLeadSections t1,adExpQuickLeadSections t2,syFields t3,syFldCaptions t4,adLeadFields t5 ")
            .Append(" where t1.SectionId = t2.SectionId and t2.FldId = t3.FldId and t3.FldId=t4.FldId ")
            .Append(" and t3.FldId=t5.LeadId and t5.LeadField=t1.SectionId ")
            .Append(" Order by t1.SectionId,t3.FldId ")
        End With
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    ''Added By Saraswathi Lakshmanan to fix issue 14385
    ''When entering a new lead information SA, Front Desk and Director
    '' of Admissions should be able to assign them to admission rep 
    ''Used the existing function and Modified the checking condition from Dir of Admn to 
    ''DirOfAdmn and Frontdesk and Username with sa

    Public Function GetAllAdmissionRepsByCampusUserIdUserRoles(ByVal strCampusId As String, ByVal strUserId As String, ByVal userName As String, ByVal isDirOfAdorFDesk As Boolean, Optional ByVal LeadId As String = "") As DataSet
        'connect to the database
        Dim db As New DataAccess



        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        'Dim da6 As New OleDbDataAdapter
        'Dim strInActiveStatus As String = AdvantageCommonValues.InactiveGuid
        'Dim ds As New DataSet
        'Build the sql query
        With sb
            If Not LeadId = "" Then
                .Append(" Select distinct '(X) ' + U.fullname as FullName,U.UserId as UserId from adLeads L,syUsers U ")
                .Append(" where L.AdmissionsRep=U.UserId And AccountActive=0 ")
                .Append(" AND L.LeadId='" & LeadId & "' ")
                .Append(" Union ")
            End If
            .Append(" select distinct s1.fullname as FullName,s1.UserId as UserId from ")
            .Append(" syUsers s1,syUsersRolesCampGrps s2,syRoles s3,sySysRoles s4,syCmpGrpCmps s5,syStatuses s6 ")
            .Append(" where s1.UserId = s2.UserId and s2.RoleId = s3.RoleId ")
            .Append(" and s3.SysRoleId = s4.SysRoleId and s2.CampGrpId = s5.CampGrpId and s4.StatusId = s6.StatusId ")
            .Append(" and s5.CampusId=? and s1.AccountActive=1 and s4.SysRoleId = 3 and s6.Status='Active' ")
            If isDirOfAdorFDesk = False And userName <> "sa" Then
                .Append(" and s1.UserId=? ")
            End If
            .Append(" order by fullname")
        End With
        db.OpenConnection()
        db.AddParameter("@CampusId", strCampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If isDirOfAdorFDesk = False And userName <> "sa" Then
            db.AddParameter("@UserId", strUserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        Try
            Return db.RunParamSQLDataSet(sb.ToString)
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetSuspensionStatus(Optional ByVal StuEnrollId As String = "") As String
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("Select distinct campusid from arStuEnrollments where stuenrollid=?")
        End With
        db.AddParameter("@stuenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim strCampusId As String = ""
        Dim dr1 As OleDbDataReader
        Try
            dr1 = db.RunParamSQLDataReader(sb.ToString)
            While dr1.Read()
                strCampusId = CType(dr1("campusId"), Guid).ToString
            End While
        Catch ex As Exception
            strCampusId = ""
        Finally
            dr1.Close()
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try

        With sb
            .Append(" SELECT Top 1  ")
            .Append("       A.StatusCodeID as StatusCodeID,A.StatusCodeDescrip AS StatusCodeDescrip ")
            .Append(" FROM  syStatusCodes A,sySysStatus B,syStatuses C,syCmpGrpCmps G ")
            .Append(" WHERE A.sysStatusID=B.sysStatusID")
            .Append("       AND A.StatusID=C.StatusID ")
            .Append("       AND C.Status='Active' ")
            .Append("       AND B.SysStatusId=11")
            If strCampusId <> "" Then
                .Append("       AND G.CampGrpId=A.CampGrpId")
                .Append("       AND G.CampusId=?")
            End If
            .Append(" ORDER BY A.StatusCodeDescrip")
        End With

        If strCampusId <> "" Then
            db.AddParameter("@CampusId", strCampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        'return dataset
        Dim dr As OleDbDataReader
        Dim strSuspensionStatus As String = ""
        dr = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            strSuspensionStatus = CType(dr("StatusCodeId"), Guid).ToString
        End While
        dr.Close()
        Return strSuspensionStatus
    End Function
    Public Function GetLOAStatus(Optional ByVal StuEnrollId As String = "") As String
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder


        With sb
            .Append("Select distinct campusid from arStuEnrollments where stuenrollid=?")
        End With
        db.AddParameter("@stuenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim strCampusId As String = ""
        Dim dr1 As OleDbDataReader
        Try
            dr1 = db.RunParamSQLDataReader(sb.ToString)
            While dr1.Read()
                strCampusId = CType(dr1("campusId"), Guid).ToString
            End While
        Catch ex As Exception
            strCampusId = ""
        Finally
            dr1.Close()
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try

        'build the sql query
        With sb
            .Append(" SELECT Top 1  ")
            .Append("       A.StatusCodeID as StatusCodeID,A.StatusCodeDescrip AS StatusCodeDescrip ")
            .Append(" FROM  syStatusCodes A,sySysStatus B,syStatuses C,syCmpGrpCmps G ")
            .Append(" WHERE A.sysStatusID=B.sysStatusID")
            .Append("       AND A.StatusID=C.StatusID ")
            .Append("       AND C.Status='Active' ")
            .Append("       AND B.SysStatusId=10")
            If strCampusId <> "" Then
                .Append("       AND G.CampGrpId=A.CampGrpId")
                .Append("       AND G.CampusId=?")
            End If
            .Append(" ORDER BY A.StatusCodeDescrip")
        End With

        If strCampusId <> "" Then
            db.AddParameter("@CampusId", strCampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        'return dataset
        Dim dr As OleDbDataReader
        Dim strSuspensionStatus As String = ""
        dr = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            strSuspensionStatus = CType(dr("StatusCodeId"), Guid).ToString
        End While
        dr.Close()
        Return strSuspensionStatus
    End Function
    Public Function GetLOAReasons(Optional ByVal strCampusId As String = "") As DataTable
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append(" SELECT Distinct  ")
            .Append("       A.Descrip as Descrip,A.LOAReasonId AS LOAReasonId ")
            .Append(" FROM  arLOAReasons A,syCmpGrpCmps G ")
            .Append(" WHERE ")
            .Append("       A.StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' ")
            If strCampusId <> "" Then
                .Append("       AND A.CampGrpId=G.CampGrpId")
                .Append("       AND G.CampusId=?")
            End If
            .Append(" ORDER BY A.Descrip")
        End With

        If strCampusId <> "" Then
            db.AddParameter("@CampusId", strCampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        'return dataset
        Dim dt As  DataTable
        dt = CType(db.RunParamSQLDataSet(sb.ToString), DataSet).Tables(0)
        Return dt
    End Function
    Public Function GetLeadGroups_SP(ByVal showActiveOnly As Boolean, ByVal campusId As String) As DataTable
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")


        db.AddParameter("@campusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@showActiveOnly", showActiveOnly, SqlDbType.Bit, , ParameterDirection.Input)

        'add cutOff date parameter


        ds = db.RunParamSQLDataSet_SP("dbo.usp_GetLeadGroups")

        Try
            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

        'Return db.RunParamSQLDataSet("dbo.usp_GetLeadGroups", Nothing, "SP").Tables(0)

    End Function

    Public Function GetAllLeadsByCampusUserAndNameFiltersNew(ByVal LeadStatus As String, ByVal CampusId As String, ByVal userId As String, ByVal FirstName As String, ByVal LastName As String, ByVal LeadCampuses As String, Optional ByVal HasEditOtherPerm As Boolean = False) As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim da6 As OleDbDataAdapter
        Dim ds As New DataSet
        'Build the sql query
        Dim andOrOrOperator As String = " AND "
        Dim UserSecurity As New UserSecurityDB
        Dim boolDir As Boolean

        boolDir = UserSecurity.IsSAOrDirectorOfAdmissions(userId)

        'The Status Code Check has been left out as the Lead Status DDL brings only 
        'the leads based on status

        With sb

            .Append("   SELECT  ")
            .Append("       DISTINCT    ")
            .Append("       LeadId,     ")
            .Append("       FirstName,  ")
            .Append("       MiddleName, ")
            .Append("       LastName,   ")
            .Append("       SSN         ")
            .Append("   FROM            ")
            .Append("       adLeads     ")
            .Append("   WHERE           ")

            If Not CampusId = Guid.Empty.ToString Then
                .Append("       CampusId = ? ")
            Else
                If LeadCampuses.ToLower = "ownleads" Then
                    ''' Code change to Resolve Mantis Issue id 19448
                    ''.Append("CampusId = ? ")
                    .Append("CampusId in (SELECT C.CampusId FROM syCampuses C, syStatuses S WHERE C.StatusId = S.StatusId AND S.Status = 'Active' AND CampusId in (Select CampusId from syCmpGrpCmps where CampGrpId in  (select CampgrpId from syUsersRolesCampGrps where UserId = ? ))) ")
                ElseIf LeadCampuses.ToLower = "permissiblecampuses" Then
                    ''''''''''''''
                    .Append("CampusId in (SELECT C.CampusId FROM syCampuses C, syStatuses S WHERE C.StatusId = S.StatusId AND S.Status = 'Active' AND CampusId in (Select CampusId from syCmpGrpCmps where CampGrpId in  (select CampgrpId from syUsersRolesCampGrps where UserId = ? ))) ")
                    ''db.AddParameter("@Userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    .Append("CampusId in (select CampusId from syCampuses ) ")
                End If
            End If



            ''' Code change to Resolve Mantis Issue id 19448
            If (boolDir = False And HasEditOtherPerm = False And Not LeadCampuses.ToLower = "permissiblecampuses" And Not LeadCampuses.ToLower = "allcampuses") Or (boolDir = False And LeadCampuses.ToLower = "ownleads") Then
                '''''
                .Append(andOrOrOperator)
                .Append("   AdmissionsRep=? ")
            End If

            If Not FirstName = "" Then
                .Append(andOrOrOperator)
                .Append(" FirstName like  + ? + '%'")
            End If
            If Not LastName = "" Then
                .Append(andOrOrOperator)
                .Append(" LastName like  + ? + '%'")
            End If
            If Not LeadStatus = "" Then
                .Append(andOrOrOperator)
                .Append(" LeadStatus = ? ")
            Else
                .Append(andOrOrOperator)
                .Append(" LeadId not in (select Distinct LeadId from arStuEnrollments where LeadId is not null) ")
            End If
            .Append(" ORDER BY  ")
            .Append("   FIRSTNAME ")
        End With

        If Not CampusId = Guid.Empty.ToString Then
            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            ''' Code change to Resolve Mantis Issue id 19448
            If LeadCampuses.ToLower = "permissiblecampuses" Or LeadCampuses.ToLower = "ownleads" Then
                '''''
                db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
        End If

        ''' Code change to Resolve Mantis Issue id 19448
        If (boolDir = False And HasEditOtherPerm = False And Not LeadCampuses.ToLower = "permissiblecampuses" And Not LeadCampuses.ToLower = "allcampuses") Or (boolDir = False And LeadCampuses.ToLower = "ownleads") Then
            '''''''''''''''
            db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        If Not FirstName = "" Then
            db.AddParameter("@FirstName", FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If Not LastName = "" Then
            db.AddParameter("@LastName", LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If Not LeadStatus = "" Then
            db.AddParameter("@LeadStatus", LeadStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT")
        Catch ex As Exception
            Throw New Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        Return ds
    End Function
    ''''' Code changes to Fix mantis issue id 19549 by kamalesh Ahuja on 16 Aug 2010 '''
    Public Function CheckLeadUser(ByVal UserId As String, ByVal LeadId As String) As Integer
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" SELECT Count(*)  as LeadCount ")
            .Append(" from adLeads where LeadId=? and AdmissionsRep =? ")
        End With

        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim intCheckCount As Integer = 0
        While dr.Read()
            intCheckCount = dr("LeadCount")
        End While

        Dim UserSecurity As New UserSecurityDB
        Dim boolDir As Boolean

        boolDir = UserSecurity.IsSAOrDirectorOfAdmissions(UserId)

        If boolDir = True Then
            intCheckCount = 1
        End If

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return intCheckCount
    End Function
End Class

