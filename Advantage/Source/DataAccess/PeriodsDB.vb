Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' PeriodsDB.vb
'
' PeriodsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class PeriodsDB
    Private ReadOnly myAdvAppSettings As AdvAppSettings

    Sub New()
        myAdvAppSettings = AdvAppSettings.GetAppSettings()

    End Sub

    Public Function GetAllPeriods(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

       

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      P.PeriodId, ")
            .Append("	      P.StatusId, ")
            .Append("	      P.PeriodCode, ")
            .Append("	      P.PeriodDescrip ")
            .Append("FROM     syPeriods P, syStatuses ST ")
            .Append("WHERE    P.StatusId = ST.StatusId ")
            .Append("AND      P.CampGrpId In (select CGC.CampGrpId from syCmpGrpCmps CGC, syCampGrps CG where CGC.CampusId = ? AND	CGC.CampGrpId=CG.CampGrpId) ")
            .Append("ORDER BY ST.Status,P.PeriodDescrip asc")
        End With

        ' Add the CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllPeriods() As DataSet

        '   connect to the database
        Dim db As New DataAccess

       

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      P.PeriodId, ")
            .Append("	      P.StatusId, ")
            .Append("	      P.PeriodCode, ")
            .Append("	      P.PeriodDescrip ")
            .Append("FROM     syPeriods P, syStatuses ST ")
            .Append("WHERE    P.StatusId = ST.StatusId ")
            .Append("ORDER BY ST.Status,P.PeriodDescrip asc")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetPeriodInfo(ByVal PeriodId As String) As PeriodInfo

        '   connect to the database
        Dim db As New DataAccess

      

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT P.PeriodId, ")
            .Append("    P.PeriodCode, ")
            .Append("    P.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=P.StatusId) As Status, ")
            .Append("    P.PeriodDescrip, ")
            .Append("    P.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=P.CampGrpId) As CampGrpDescrip, ")
            '.Append("    P.MeetDays, ")
            .Append("    (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("    P.StartTimeId, ")
            .Append("    (Select TimeIntervaldescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("    P.EndTimeId, ")
            .Append("    (Select TimeIntervaldescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime, ")
            '.Append("    P.StartTimeAndEndTimeAreFixed, ")
            .Append("    P.ModUser, ")
            .Append("    P.ModDate ")
            .Append("FROM  syPeriods P ")
            .Append("WHERE P.PeriodId= ? ")
        End With

        ' Add the PeriodId to the parameter list
        db.AddParameter("@PeriodId", PeriodId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim PeriodInfo As New PeriodInfo

        While dr.Read()

            '   set properties with data from DataReader
            With PeriodInfo
                .PeriodId = PeriodId
                .IsInDB = True
                .Code = dr("PeriodCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("PeriodDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                If Not (dr("MeetDays") Is System.DBNull.Value) Then .MeetDays = dr("MeetDays")
                If Not (dr("StartTimeId") Is System.DBNull.Value) Then .StartTimeId = CType(dr("StartTimeId"), Guid).ToString
                If Not (dr("EndTimeId") Is System.DBNull.Value) Then .EndTimeId = CType(dr("EndTimeId"), Guid).ToString
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return PeriodInfo
        Return PeriodInfo

    End Function
    Public Function UpdatePeriodInfo(ByVal PeriodInfo As PeriodInfo, ByVal user As String) As String

        If NumberOfPeriodsLikeThisAlreadyRegistered(PeriodInfo.StartTimeId, PeriodInfo.EndTimeId, PeriodInfo.StatusId, PeriodInfo.CampGrpId, PeriodInfo.MeetDays, PeriodInfo.PeriodId) > 0 Then
            Return "This Period already exists."
        End If

        '   Connect to the database
        Dim db As New DataAccess

      

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syPeriods Set PeriodId = ?, PeriodCode = ?, ")
                .Append(" StatusId = ?, PeriodDescrip = ?, CampGrpId = ?, ")
                .Append(" StartTimeId = ?, EndTimeId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE PeriodId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from syPeriods where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   PeriodId
            db.AddParameter("@PeriodId", PeriodInfo.PeriodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   EnrollmentId
            db.AddParameter("@PeriodCode", PeriodInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", PeriodInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   PeriodDescrip
            db.AddParameter("@PeriodDescrip", PeriodInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If PeriodInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", PeriodInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            ''   MeetDays
            'db.AddParameter("@MeetDays", PeriodInfo.MeetDays, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   StartTimeId
            db.AddParameter("@StartTimeId", PeriodInfo.StartTimeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   EndTimeId
            db.AddParameter("@EndTimeId", PeriodInfo.EndTimeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   PeriodId
            db.AddParameter("@PeriodId", PeriodInfo.PeriodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", PeriodInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString, groupTrans)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                'update PeriodsWorkDays Table
                Dim res As String = UpdatePeriodsWorkDays(PeriodInfo.PeriodId, PeriodInfo.MeetDays, groupTrans, db)
                '   return without errors
                If res = String.Empty Then
                    groupTrans.Commit()
                Else
                    '   rollback transaction if there were errors
                    groupTrans.Rollback()
                End If
                Return res
            Else
                '   rollback transaction if there were errors
                groupTrans.Rollback()

                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddPeriodInfo(ByVal periodInfo As PeriodInfo, ByVal user As String) As String

        'check if a period like this is already registered
        If NumberOfPeriodsLikeThisAlreadyRegistered(periodInfo.StartTimeId, periodInfo.EndTimeId, periodInfo.StatusId, periodInfo.CampGrpId, periodInfo.MeetDays, periodInfo.PeriodId) > 0 Then
            Return "This Period already exists."
        End If

        '   Connect to the database
        Dim db As New DataAccess

      

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syPeriods (PeriodId, PeriodCode, StatusId, ")
                .Append("   PeriodDescrip, CampGrpId, ")
                .Append("   StartTimeId, EndTimeId, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   PeriodId
            db.AddParameter("@PeriodId", periodInfo.PeriodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   PeriodCode
            db.AddParameter("@PeriodCode", periodInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", periodInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   PeriodDescrip
            db.AddParameter("@PeriodDescrip", periodInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If periodInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", periodInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            ''   MeetDays
            'db.AddParameter("@MeetDays", PeriodInfo.MeetDays, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   StartTimeId
            db.AddParameter("@StartTimeId", periodInfo.StartTimeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   EndTimeId
            db.AddParameter("@EndTimeId", periodInfo.EndTimeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            'update PeriodsWorkDays Table
            Dim res As String = UpdatePeriodsWorkDays(periodInfo.PeriodId, periodInfo.MeetDays, groupTrans, db)

            '   return without errors
            If res = String.Empty Then
                '   commit transaction 
                groupTrans.Commit()
            Else
                '   rollback transaction if there were errors
                groupTrans.Rollback()
            End If

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Private Function NumberOfPeriodsLikeThisAlreadyRegistered(ByVal startTimeId As String, ByVal endTimeId As String, ByVal statusId As String, ByVal campGrpId As String, ByVal meetDays As Byte, ByVal periodId As String) As Integer

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select ")
            .Append("       count(*) ")
            .Append("from syPeriods P ")
            .Append("where ")
            .Append("       StartTimeId = ? ")
            .Append("and	EndTimeId = ? ")
            .Append("and    StatusId = ? ")
            .Append("and    CampGrpId = ? ")
            .Append("and	? = (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND PWD.WorkDayId=WD.WorkDaysId AND P1.PeriodId=P.PeriodId) ")
            .Append("and    P.PeriodId <> ? ")
        End With

        ' Add the StartTimeId to the parameter list
        db.AddParameter("@StartTimeId", startTimeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the EndTimeId to the parameter list
        db.AddParameter("@EndTimeId", endTimeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the StatusId to the parameter list
        db.AddParameter("@StatusId", statusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the CampGrpId to the parameter list
        db.AddParameter("@CampGrpId", campGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the CampusId to the parameter list
        db.AddParameter("@MeetDays", meetDays, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        ' Add the PeriodId to the parameter list
        db.AddParameter("@PeriodId", periodId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return boolean
        Dim obj = db.RunParamSQLScalar(sb.ToString)
        If Not (obj = Nothing) Then
            Return CType(obj, Integer)
        Else
            Return 0
        End If

    End Function
    Public Function DeletePeriodInfo(ByVal PeriodId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

     

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syPeriods ")
                .Append("WHERE PeriodId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM syPeriods WHERE PeriodId = ? ")
            End With

            '   add parameters values to the query

            '   PeriodId
            db.AddParameter("@PeriodId", PeriodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   PeriodId
            db.AddParameter("@PeriodId", PeriodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString, groupTrans)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                'update PeriodsWorkDays Table
                Dim res As String = UpdatePeriodsWorkDays(PeriodId, 0, groupTrans, db)
                If res = String.Empty Then
                    '   commit transaction 
                    groupTrans.Commit()
                    '   return without errors
                    Return res
                Else
                    '   rollback transaction if there were errors
                    groupTrans.Rollback()

                    '   return without errors
                    Return res
                End If
            Else
                '   rollback transaction if there were errors
                groupTrans.Rollback()

                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetCollectionOfMeetingDates(ByVal startDate As Date, ByVal endDate As Date, ByVal periodId As String, ByVal SelectWeek As Integer) As System.Collections.Generic.List(Of Date)
        Dim pi As PeriodInfo = GetPeriodInfo(periodId)

        'return collection of dates
        Return AdvantageCommonValues.GetCollectionOfDates(startDate, endDate, pi.MeetDays, SelectWeek)
    End Function
    Public Function GetCollectionOfMeetingDatesWithCourseTypeID(ByVal clsSectionId As String, ByVal InstructionTypeID As String, Optional ByVal campusid As String = "") As System.Collections.Generic.List(Of AdvantageClassSectionMeeting)

        'get collection of holidays
        Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New HolidaysDB).GetCollectionOfHolidayDates(campusid)

        '   connect to the database
        Dim db As New DataAccess

       

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("       0 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime, CSM.InstructionTypeID, ACT.InstructionTypeDescrip  ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P , arInstructionType ACT ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.PeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? ")
            .Append(" AND CSM.InstructionTypeID=ACT.InstructionTypeID ")
            .Append(" And CSM.InstructionTypeID= ? ")
            .Append("UNION ALL ")
            .Append("SELECT ")
            .Append("       1 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime, CSM.InstructionTypeID, ACT.InstructionTypeDescrip  ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P, arInstructionType ACT  ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.AltPeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? ")
            .Append(" AND CSM.InstructionTypeID=ACT.InstructionTypeID ")
            .Append(" And CSM.InstructionTypeID= ? ")
            '.Append("SELECT  ")
            '.Append("       0 as Type, ")
            '.Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            '.Append("	   (Select StartDate from arClassSections where ClssectionId=CSM.ClsSectionId) As StartDate,  ")
            '.Append("	   (Select EndDate from arClassSections where ClssectionId=CSM.ClsSectionId) As EndDate, ")
            '.Append("	   (Select POWER(2,((ViewOrder+6)-((ViewOrder+6)/7)*7)) from plWorkDays where WorkDaysId=CSM.WorkDaysId) as MeetDays, ")
            '.Append("	   (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.TimeIntervalId) as StartTime,  ")
            '.Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.EndIntervalId) as EndTime  ")
            '.Append("FROM	arClsSectMeetings CSM ")
            '.Append("WHERE  ")
            '.Append("	    CSM.ClsSectionId = ? ")
        End With

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@InstructionTypeID", InstructionTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@InstructionTypeID", InstructionTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'this is the collection 
        Dim collectionOfClassSectionMeetings As System.Collections.Generic.List(Of AdvantageClassSectionMeeting) = New System.Collections.Generic.List(Of AdvantageClassSectionMeeting)

        While dr.Read()

            'set the type of week to process
            Dim selectWeek As Integer = 0 'all weeks
            If Not dr("IsAltPeriod") Is System.DBNull.Value Then
                If dr("Type") = 0 Then
                    selectWeek = 1  'even weeks
                Else
                    selectWeek = 2  'odd weeks
                End If
            End If

            'generate list of meeting dates
            Dim listOfDates As System.Collections.Generic.List(Of Date) = AdvantageCommonValues.GetCollectionOfDates(dr("StartDate"), dr("EndDate"), dr("MeetDays"), selectWeek)

            For Each [date] As Date In listOfDates
                'parse the start time of the meeting
                Dim meetingDateAndTime As DateTime = Date.Parse([date].Year.ToString + "-" + [date].Month.ToString + "-" + [date].Day.ToString + " " + CType(dr("StartTime"), Date).Hour.ToString + ":" + CType(dr("StartTime"), Date).Minute.ToString)

                'calculate the duration of the meeting
                Dim ts As TimeSpan = CType(dr("EndTime"), Date).Subtract(CType(dr("StartTime"), Date))
                Dim duration As Integer = ts.Hours * 60 + ts.Minutes

                'add the Class Section Meeting to the collection
                Dim acsm As AdvantageClassSectionMeeting = New AdvantageClassSectionMeeting(meetingDateAndTime, duration)

                'add it to the collection only if it is not a holiday
                If Not IsHoliday(acsm, collectionOfHolidays) Then
                    collectionOfClassSectionMeetings.Add(acsm)
                End If
            Next

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return collection of AdvantageClassSectionMeetings. Sort it by date
        collectionOfClassSectionMeetings.Sort()
        Return collectionOfClassSectionMeetings

    End Function

    Public Function NewGetCollectionOfMeetingDatesWithCourseTypeID(ByVal clsSectionId As String, ByVal ClsSectMeetingID As String, Optional ByVal campusid As String = "") As System.Collections.Generic.List(Of AdvantageClassSectionMeeting)

        'get collection of holidays
        Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New HolidaysDB).GetCollectionOfHolidayDates(campusid)

        '   connect to the database
        Dim db As New DataAccess

    

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("       0 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime, CSM.InstructionTypeID, ACT.InstructionTypeDescrip  ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P , arInstructionType ACT ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.PeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? ")
            .Append(" AND CSM.InstructionTypeID=ACT.InstructionTypeID ")
            .Append(" And CSM.ClsSectMeetingID= ? ")
            .Append("UNION ALL ")
            .Append("SELECT ")
            .Append("       1 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime, CSM.InstructionTypeID, ACT.InstructionTypeDescrip  ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P, arInstructionType ACT  ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.AltPeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? ")
            .Append(" AND CSM.InstructionTypeID=ACT.InstructionTypeID ")
            .Append(" And CSM.ClsSectMeetingID= ? ")
            '.Append("SELECT  ")
            '.Append("       0 as Type, ")
            '.Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            '.Append("	   (Select StartDate from arClassSections where ClssectionId=CSM.ClsSectionId) As StartDate,  ")
            '.Append("	   (Select EndDate from arClassSections where ClssectionId=CSM.ClsSectionId) As EndDate, ")
            '.Append("	   (Select POWER(2,((ViewOrder+6)-((ViewOrder+6)/7)*7)) from plWorkDays where WorkDaysId=CSM.WorkDaysId) as MeetDays, ")
            '.Append("	   (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.TimeIntervalId) as StartTime,  ")
            '.Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.EndIntervalId) as EndTime  ")
            '.Append("FROM	arClsSectMeetings CSM ")
            '.Append("WHERE  ")
            '.Append("	    CSM.ClsSectionId = ? ")
        End With

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ClsSectMeetingID", ClsSectMeetingID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ClsSectMeetingID", ClsSectMeetingID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'this is the collection 
        Dim collectionOfClassSectionMeetings As System.Collections.Generic.List(Of AdvantageClassSectionMeeting) = New System.Collections.Generic.List(Of AdvantageClassSectionMeeting)

        While dr.Read()

            'set the type of week to process
            Dim selectWeek As Integer = 0 'all weeks
            If Not dr("IsAltPeriod") Is System.DBNull.Value Then
                If dr("Type") = 0 Then
                    selectWeek = 1  'even weeks
                Else
                    selectWeek = 2  'odd weeks
                End If
            End If

            'generate list of meeting dates
            Dim listOfDates As System.Collections.Generic.List(Of Date) = AdvantageCommonValues.GetCollectionOfDates(dr("StartDate"), dr("EndDate"), dr("MeetDays"), selectWeek)

            For Each [date] As Date In listOfDates
                'parse the start time of the meeting
                Dim meetingDateAndTime As DateTime = Date.Parse([date].Year.ToString + "-" + [date].Month.ToString + "-" + [date].Day.ToString + " " + CType(dr("StartTime"), Date).Hour.ToString + ":" + CType(dr("StartTime"), Date).Minute.ToString)

                'calculate the duration of the meeting
                Dim ts As TimeSpan = CType(dr("EndTime"), Date).Subtract(CType(dr("StartTime"), Date))
                Dim duration As Integer = ts.Hours * 60 + ts.Minutes

                'add the Class Section Meeting to the collection
                Dim acsm As AdvantageClassSectionMeeting = New AdvantageClassSectionMeeting(meetingDateAndTime, duration)

                'add it to the collection only if it is not a holiday
                If Not IsHoliday(acsm, collectionOfHolidays) Then
                    collectionOfClassSectionMeetings.Add(acsm)
                End If
            Next

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return collection of AdvantageClassSectionMeetings. Sort it by date
        collectionOfClassSectionMeetings.Sort()
        Return collectionOfClassSectionMeetings

    End Function


    Public Function GetCollectionOfMeetingDates(ByVal clsSectionId As String, Optional ByVal campusid As String = "") As System.Collections.Generic.List(Of AdvantageClassSectionMeeting)

        'get collection of holidays
        Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New HolidaysDB).GetCollectionOfHolidayDates(campusid)

        '   connect to the database
        Dim db As New DataAccess

       

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("       0 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.PeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? ")
            .Append("UNION ALL ")
            .Append("SELECT ")
            .Append("       1 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime  ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.AltPeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? ")
            '.Append("SELECT  ")
            '.Append("       0 as Type, ")
            '.Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            '.Append("	   (Select StartDate from arClassSections where ClssectionId=CSM.ClsSectionId) As StartDate,  ")
            '.Append("	   (Select EndDate from arClassSections where ClssectionId=CSM.ClsSectionId) As EndDate, ")
            '.Append("	   (Select POWER(2,((ViewOrder+6)-((ViewOrder+6)/7)*7)) from plWorkDays where WorkDaysId=CSM.WorkDaysId) as MeetDays, ")
            '.Append("	   (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.TimeIntervalId) as StartTime,  ")
            '.Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.EndIntervalId) as EndTime  ")
            '.Append("FROM	arClsSectMeetings CSM ")
            '.Append("WHERE  ")
            '.Append("	    CSM.ClsSectionId = ? ")
        End With

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'this is the collection 
        Dim collectionOfClassSectionMeetings As System.Collections.Generic.List(Of AdvantageClassSectionMeeting) = New System.Collections.Generic.List(Of AdvantageClassSectionMeeting)

        While dr.Read()

            'set the type of week to process
            Dim selectWeek As Integer = 0 'all weeks
            If Not dr("IsAltPeriod") Is System.DBNull.Value Then
                If dr("Type") = 0 Then
                    selectWeek = 1  'even weeks
                Else
                    selectWeek = 2  'odd weeks
                End If
            End If

            'generate list of meeting dates
            Dim listOfDates As System.Collections.Generic.List(Of Date) = AdvantageCommonValues.GetCollectionOfDates(dr("StartDate"), dr("EndDate"), dr("MeetDays"), selectWeek)

            For Each [date] As Date In listOfDates
                'parse the start time of the meeting
                Dim meetingDateAndTime As DateTime = Date.Parse([date].Year.ToString + "-" + [date].Month.ToString + "-" + [date].Day.ToString + " " + CType(dr("StartTime"), Date).Hour.ToString + ":" + CType(dr("StartTime"), Date).Minute.ToString)

                'calculate the duration of the meeting
                Dim ts As TimeSpan = CType(dr("EndTime"), Date).Subtract(CType(dr("StartTime"), Date))
                Dim duration As Integer = ts.Hours * 60 + ts.Minutes

                'add the Class Section Meeting to the collection
                Dim acsm As AdvantageClassSectionMeeting = New AdvantageClassSectionMeeting(meetingDateAndTime, duration)

                'add it to the collection only if it is not a holiday
                If Not IsHoliday(acsm, collectionOfHolidays) Then
                    collectionOfClassSectionMeetings.Add(acsm)
                End If
            Next

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return collection of AdvantageClassSectionMeetings. Sort it by date
        collectionOfClassSectionMeetings.Sort()
        Return collectionOfClassSectionMeetings

    End Function
    Public Function GetCollectionOfMeetingDates_New(ByVal clsSectionId As String, Optional ByVal campusid As String = "") As System.Collections.Generic.List(Of AdvantageClassSectionMeeting)

        'get collection of holidays
        Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New HolidaysDB).GetCollectionOfHolidayDates(campusid)

        '   connect to the database
        Dim db As New DataAccess

       

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("       0 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.PeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? ")
            .Append("UNION ALL ")
            .Append("SELECT ")
            .Append("       1 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime  ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.AltPeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? ")
            '.Append("SELECT  ")
            '.Append("       0 as Type, ")
            '.Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            '.Append("	   (Select StartDate from arClassSections where ClssectionId=CSM.ClsSectionId) As StartDate,  ")
            '.Append("	   (Select EndDate from arClassSections where ClssectionId=CSM.ClsSectionId) As EndDate, ")
            '.Append("	   (Select POWER(2,((ViewOrder+6)-((ViewOrder+6)/7)*7)) from plWorkDays where WorkDaysId=CSM.WorkDaysId) as MeetDays, ")
            '.Append("	   (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.TimeIntervalId) as StartTime,  ")
            '.Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.EndIntervalId) as EndTime  ")
            '.Append("FROM	arClsSectMeetings CSM ")
            '.Append("WHERE  ")
            '.Append("	    CSM.ClsSectionId = ? ")
        End With

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'this is the collection 
        Dim collectionOfClassSectionMeetings As System.Collections.Generic.List(Of AdvantageClassSectionMeeting) = New System.Collections.Generic.List(Of AdvantageClassSectionMeeting)

        While dr.Read()

            'set the type of week to process
            Dim selectWeek As Integer = 0 'all weeks
            If Not dr("IsAltPeriod") Is System.DBNull.Value Then
                If dr("Type") = 0 Then
                    selectWeek = 1  'even weeks
                Else
                    selectWeek = 2  'odd weeks
                End If
            End If

            'generate list of meeting dates
            Dim listOfDates As System.Collections.Generic.List(Of Date) = AdvantageCommonValues.GetCollectionOfDates_New(dr("StartDate"), dr("EndDate"), dr("MeetDays"), selectWeek)

            For Each [date] As Date In listOfDates
                'parse the start time of the meeting
                Dim meetingDateAndTime As DateTime = Date.Parse([date].Year.ToString + "-" + [date].Month.ToString + "-" + [date].Day.ToString + " " + CType(dr("StartTime"), Date).Hour.ToString + ":" + CType(dr("StartTime"), Date).Minute.ToString)

                'calculate the duration of the meeting
                Dim ts As TimeSpan = CType(dr("EndTime"), Date).Subtract(CType(dr("StartTime"), Date))
                Dim duration As Integer = ts.Hours * 60 + ts.Minutes

                'add the Class Section Meeting to the collection
                Dim acsm As AdvantageClassSectionMeeting = New AdvantageClassSectionMeeting(meetingDateAndTime, duration)

                'add it to the collection only if it is not a holiday
                If Not IsHoliday(acsm, collectionOfHolidays) Then
                    collectionOfClassSectionMeetings.Add(acsm)
                End If
            Next

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return collection of AdvantageClassSectionMeetings. Sort it by date
        collectionOfClassSectionMeetings.Sort()
        Return collectionOfClassSectionMeetings

    End Function
    Public Function GetCollectionOfMeetingDatesForMeetings(ByVal clsSectionId As String, clsSectmeetingId As String, Optional ByVal campusid As String = "") As System.Collections.Generic.List(Of AdvantageClassSectionMeeting)

        'get collection of holidays
        Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New HolidaysDB).GetCollectionOfHolidayDates(campusid)

        '   connect to the database
        Dim db As New DataAccess

       

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("       0 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.PeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? and CSM.ClsSectMeetingId = ? ")
            .Append("UNION ALL ")
            .Append("SELECT ")
            .Append("       1 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime  ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.AltPeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? and CSM.ClsSectMeetingId = ? ")
            '.Append("SELECT  ")
            '.Append("       0 as Type, ")
            '.Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            '.Append("	   (Select StartDate from arClassSections where ClssectionId=CSM.ClsSectionId) As StartDate,  ")
            '.Append("	   (Select EndDate from arClassSections where ClssectionId=CSM.ClsSectionId) As EndDate, ")
            '.Append("	   (Select POWER(2,((ViewOrder+6)-((ViewOrder+6)/7)*7)) from plWorkDays where WorkDaysId=CSM.WorkDaysId) as MeetDays, ")
            '.Append("	   (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.TimeIntervalId) as StartTime,  ")
            '.Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CSM.EndIntervalId) as EndTime  ")
            '.Append("FROM	arClsSectMeetings CSM ")
            '.Append("WHERE  ")
            '.Append("	    CSM.ClsSectionId = ? ")
        End With

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clsSectmeetingId", clsSectmeetingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@clsSectmeetingId", clsSectmeetingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'this is the collection 
        Dim collectionOfClassSectionMeetings As System.Collections.Generic.List(Of AdvantageClassSectionMeeting) = New System.Collections.Generic.List(Of AdvantageClassSectionMeeting)

        While dr.Read()

            'set the type of week to process
            Dim selectWeek As Integer = 0 'all weeks
            If Not dr("IsAltPeriod") Is System.DBNull.Value Then
                If dr("Type") = 0 Then
                    selectWeek = 1  'even weeks
                Else
                    selectWeek = 2  'odd weeks
                End If
            End If

            'generate list of meeting dates
            Dim listOfDates As System.Collections.Generic.List(Of Date) = AdvantageCommonValues.GetCollectionOfDates_New(dr("StartDate"), dr("EndDate"), dr("MeetDays"), selectWeek)

            For Each [date] As Date In listOfDates
                'parse the start time of the meeting
                Dim meetingDateAndTime As DateTime = Date.Parse([date].Year.ToString + "-" + [date].Month.ToString + "-" + [date].Day.ToString + " " + CType(dr("StartTime"), Date).Hour.ToString + ":" + CType(dr("StartTime"), Date).Minute.ToString)

                'calculate the duration of the meeting
                Dim ts As TimeSpan = CType(dr("EndTime"), Date).Subtract(CType(dr("StartTime"), Date))
                Dim duration As Integer = ts.Hours * 60 + ts.Minutes

                'add the Class Section Meeting to the collection
                Dim acsm As AdvantageClassSectionMeeting = New AdvantageClassSectionMeeting(meetingDateAndTime, duration)

                'add it to the collection only if it is not a holiday
                If Not IsHoliday(acsm, collectionOfHolidays) Then
                    collectionOfClassSectionMeetings.Add(acsm)
                End If
            Next

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return collection of AdvantageClassSectionMeetings. Sort it by date
        collectionOfClassSectionMeetings.Sort()
        Return collectionOfClassSectionMeetings

    End Function
    Public Function GetScheduledHoursForStudent(ByVal stuEnrollId As String, ByVal OffSetDate As String) As Integer
        Dim attDB As New ClsSectAttendanceDB
        Dim attDt As New DataTable
        Dim i As Integer = 0
        Dim TotalDuration As Integer = 0
        attDt = attDB.GetAllClsSectionsForStudent(stuEnrollId)
        If attDt.Rows.Count > 0 Then
            For i = 0 To attDt.Rows.Count - 1
                TotalDuration = TotalDuration + GetCollectionOfMeetingDates(attDt.Rows(i)("ClsSectionId").ToString, stuEnrollId, OffSetDate)
            Next

        End If
        Return TotalDuration

    End Function


    Public Function GetCollectionOfMeetingDates(ByVal clsSectionId As String, ByVal stuEnrollId As String, ByVal OffSetDate As String) As Integer

        'get collection of holidays
        Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New HolidaysDB).GetCollectionOfHolidayDates()
        Dim totalDuration As Integer = 0
        '   connect to the database
        Dim db As New DataAccess

       

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("       0 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.PeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? ")
            .Append(" and  CSM.StartDate>=(select startDate from  arStuEnrollments where StuEnrollId= ? ) ")
            .Append(" and CSM.EndDate<= ? ")
            .Append("UNION ALL ")
            .Append("SELECT ")
            .Append("       1 as Type, ")
            .Append("		Len(CSM.AltPeriodId) as IsAltPeriod, ")
            .Append("		CSM.StartDate, ")
            .Append("       CSM.EndDate, ")
            '.Append("		P.MeetDays, ")
            .Append("       (Select Sum(POWER(2,((WD.ViewOrder+6)-((WD.ViewOrder+6)/7)*7))) FROM syPeriods P1, syPeriodsWorkdays PWD, plWorkDays WD WHERE P1.PeriodId = PWD.PeriodId AND	PWD.WorkDayId=WD.WorkDaysId AND	P1.PeriodId=P.PeriodId) As MeetDays, ")
            .Append("		(Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.StartTimeId) as StartTime, ")
            .Append("       (Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=P.EndTimeId) as EndTime ")
            .Append("FROM	arClassSections CS, arClsSectMeetings CSM, syPeriods P ")
            .Append("WHERE ")
            .Append("       CS.ClsSectionId = CSM.ClsSectionId ")
            .Append("AND	CSM.AltPeriodId=P.PeriodId ")
            .Append("AND    CS.ClsSectionId = ? ")
            .Append(" and  CSM.StartDate>=(select startDate from  arStuEnrollments where StuEnrollId= ? ) ")
            .Append(" and CSM.EndDate<= ? ")
        End With

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@EndDate", Date.Now, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add ClsSectionId to the parameter list
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@stuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@EndDate", Date.Now, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'this is the collection 
        'Dim collectionOfClassSectionMeetings As System.Collections.Generic.List(Of AdvantageClassSectionMeeting) = New System.Collections.Generic.List(Of AdvantageClassSectionMeeting)

        While dr.Read()

            'set the type of week to process
            Dim selectWeek As Integer = 0 'all weeks
            If Not dr("IsAltPeriod") Is System.DBNull.Value Then
                If dr("Type") = 0 Then
                    selectWeek = 1  'even weeks
                Else
                    selectWeek = 2  'odd weeks
                End If
            End If

            'generate list of meeting dates
            Dim listOfDates As System.Collections.Generic.List(Of Date) = AdvantageCommonValues.GetCollectionOfDates(dr("StartDate"), dr("EndDate"), dr("MeetDays"), selectWeek)

            For Each [date] As Date In listOfDates
                'parse the start time of the meeting
                Dim meetingDateAndTime As DateTime = Date.Parse([date].Year.ToString + "-" + [date].Month.ToString + "-" + [date].Day.ToString + " " + CType(dr("StartTime"), Date).Hour.ToString + ":" + CType(dr("StartTime"), Date).Minute.ToString)

                'calculate the duration of the meeting
                Dim ts As TimeSpan = CType(dr("EndTime"), Date).Subtract(CType(dr("StartTime"), Date))
                Dim duration As Integer = ts.Hours * 60 + ts.Minutes

                'add the Class Section Meeting to the collection
                Dim acsm As AdvantageClassSectionMeeting = New AdvantageClassSectionMeeting(meetingDateAndTime, duration)

                'add it to the collection only if it is not a holiday
                If Not IsHoliday(acsm, collectionOfHolidays) Then
                    totalDuration = totalDuration + duration
                    'collectionOfClassSectionMeetings.Add(acsm)
                End If
            Next

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return collection of AdvantageClassSectionMeetings. Sort it by date
        'collectionOfClassSectionMeetings.Sort()
        Return totalDuration

    End Function
    Public Function GetCollectionOfMeetingDates(ByVal stuEnrollId As Guid) As System.Collections.Generic.List(Of AdvantageClassSectionMeeting)

        '   connect to the database
        Dim db As New DataAccess

       

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select  ")
            .Append("		RecordDate, ")
            .Append("		SchedHours, ")
            .Append("		ActualHours  ")
            .Append("from arStudentClockAttendance  ")
            .Append("where ")
            .Append("       StuEnrollId = ? ")
        End With

        ' Add StuEnrollId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbGuid, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'this is the collection 
        Dim collectionOfClassSectionMeetings As System.Collections.Generic.List(Of AdvantageClassSectionMeeting) = New System.Collections.Generic.List(Of AdvantageClassSectionMeeting)

        While dr.Read()

            'add the Class Section Meeting to the collection
            Dim acsm As AdvantageClassSectionMeeting = New AdvantageClassSectionMeeting(dr("RecordDate"), CType(dr("SchedHours"), Decimal) * 60)
            collectionOfClassSectionMeetings.Add(acsm)

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return collection of AdvantageClassSectionMeetings. Sort it by date
        collectionOfClassSectionMeetings.Sort()
        Return collectionOfClassSectionMeetings

    End Function
    Private Function IsHoliday(ByVal classSectionMeeting As AdvantageClassSectionMeeting, ByVal collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday)) As Boolean
        'calculate end of class Section meeting
        Dim endOfClassSectionMeeting As DateTime = classSectionMeeting.MeetingDateAndTime.AddMinutes(classSectionMeeting.Duration)

        For Each holiday As AdvantageHoliday In collectionOfHolidays

            'calculate end of the holiday
            Dim endOfHoliday As DateTime = holiday.HolidayDateAndTime.AddMinutes(holiday.Duration)

            'it is not a holiday if the endtime of the class section meeting is earlier than the start time of the holiday OR
            'the start time of the class section meeting is later that the start time of the holiday.
            If (endOfClassSectionMeeting.CompareTo(holiday.HolidayDateAndTime) > -1) And (classSectionMeeting.MeetingDateAndTime.CompareTo(endOfHoliday) < 1) Then Return True
        Next

        'return false
        Return False

    End Function
    Public Function GetWorkDayIdsAndStartAndEndTimeForPeriod(ByVal periodId As String) As ArrayList
        'get PeriodInfo
        Dim pi As PeriodInfo = GetPeriodInfo(periodId)

        'This arraylist will return the three values
        Dim al As New ArrayList(3)

        'add start time
        al.Add(pi.StartTime)

        'add end time
        al.Add(pi.EndTime)

        'add string of WorkdayIds
        al.Add(AdvantageCommonValues.GetWorkDaysIdsFromMeetDays(pi.MeetDays))

        'return arraylist
        Return al
    End Function
    Private Function UpdatePeriodsWorkDays(ByVal periodId As String, ByVal meetDays As Integer, ByVal groupTrans As OleDbTransaction, ByVal db As DataAccess) As String
        ''   Connect to the database
        'Dim db As New DataAccess
        'db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syPeriodsWorkDays ")
                .Append("WHERE PeriodId = ? ")
            End With

            '   add parameters values to the query
            db.ClearParameters()

            '   PeriodId
            db.AddParameter("@PeriodId", periodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            Dim workDays() As String = AdvantageCommonValues.GetWorkDaysIdsFromMeetDays(meetDays).Split(";")

            If Not workDays(0) = "" Then
                For i As Integer = 0 To workDays.Length - 1
                    '   build the query
                    sb = New StringBuilder()
                    With sb
                        .Append("INSERT syPeriodsWorkDays (PeriodId, WorkDayId) ")
                        .Append("VALUES ('")
                        .Append(periodId + "','")
                        .Append(workDays(i) + "')")
                    End With

                    '   execute the query
                    db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                Next

            End If

            '   return without errors
            Return ""

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close(Connection)
            'db.CloseConnection()
        End Try
    End Function
    Public Function GetPeriodsDescrip() As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

       

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT PeriodId, PeriodDescrip, PeriodCode ")
            .Append("FROM syPeriods t1, cmTimeInterval t2 ")
            .Append("WHERE t1.StartTimeId = t2.TimeIntervalId ")
            .Append("ORDER BY t2.TimeIntervalDescrip ")
        End With

        ds = db.RunSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function


End Class
