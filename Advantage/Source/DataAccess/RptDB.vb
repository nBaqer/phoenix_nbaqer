' ===============================================================================
' RptDB.vb
' DataAccess classes for the Reports project
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' History
' 9/2006 - ThinkTron - added support for AdHoc Reports
' 10/20/06 - ThinkTron - added support for UDF

Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports System.Web
Imports FAME.AdvantageV1.Common.Reports
Imports System.Collections.Generic
Imports FAME.Advantage.Common

Namespace Reports
#Region "General"
    Public Class ReportsDB
        Public Shared Function GetSimpleReportSql(ByVal sqlID As Integer) As DataSet
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim ds As DataSet

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            sb.Append("SELECT SQLStmt,WhereClause,OrderByClause ")
            sb.Append("FROM syRptSQL ")
            sb.Append("WHERE RptSQLId = ?")

            db.AddParameter("@rptsqlid", sqlID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.OpenConnection()
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()
            Return ds
        End Function

        Public Shared Function GetSimpleReportDataSet(ByVal paramInfo As ReportParamInfo) As DataSet
            Dim strSql As String
            Dim strWhere As String
            Dim strOrderBy As String
            'Dim sb As New StringBuilder
            Dim db As New DataAccess
            Dim ds2 As DataSet
            Dim ds As DataSet

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            Try
                'We need to get the SQL statement for this report
                ds2 = GetSimpleReportSql(paramInfo.SqlId)
                strSql = ds2.Tables(0).Rows(0)("SQLStmt").ToString()
                strWhere = ds2.Tables(0).Rows(0)("WhereClause").ToString()
                strOrderBy = ds2.Tables(0).Rows(0)("OrderByClause").ToString()

                If strWhere <> "" Then
                    If paramInfo.FilterList <> "" Then
                        strWhere &= " AND " & paramInfo.FilterList
                    End If
                Else
                    If paramInfo.FilterList <> "" Then
                        strWhere &= "WHERE " & paramInfo.FilterList
                    End If
                End If

                If strWhere <> "" Then
                    If paramInfo.FilterOther <> "" Then
                        strWhere &= " AND " & paramInfo.FilterOther
                    End If
                Else
                    If paramInfo.FilterOther <> "" Then
                        strWhere &= "WHERE " & paramInfo.FilterOther
                    End If
                End If

                If strOrderBy <> "" Then
                    If paramInfo.OrderBy <> "" Then
                        If strOrderBy.StartsWith("ORDER BY") Then
                            strOrderBy &= "," & paramInfo.OrderBy
                        Else
                            'May start with 'GROUP BY'
                            strOrderBy &= " ORDER BY " & paramInfo.OrderBy
                        End If
                    End If
                Else
                    If paramInfo.OrderBy <> "" Then
                        strOrderBy &= "ORDER BY " & paramInfo.OrderBy
                    End If
                End If
                If strWhere <> "" Then
                    strSql &= " " & strWhere
                End If
                If strOrderBy <> "" Then
                    strSql &= " " & strOrderBy
                End If

                db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
                db.OpenConnection()

                ds = db.RunSQLDataSet(strSql)

                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                Return ds


            Catch ex As Exception
                If ex.InnerException Is Nothing Then
                    Throw New Exception(ex.Message)
                Else
                    Throw New Exception(ex.InnerException.ToString)
                End If
            End Try
        End Function

        Public Shared Function GetSimpleReportSqlCount(ByVal sqlID As Integer) As String
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim ds As DataSet

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            sb.Append("SELECT RptCountStmt ")
            sb.Append("FROM syRptSQLCount ")
            sb.Append("WHERE RptSQLId = ?")

            db.AddParameter("@rptsqlid", sqlID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.OpenConnection()
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()
            Return ds.Tables(0).Rows(0)("RptCountStmt")
        End Function

        Public Shared Function GetRecordCount(ByVal sqlID As Integer, Optional ByVal sortParams As String = "",
                Optional ByVal filterListParams As String = "", Optional ByVal filterOtherParams As String = "") As String
            Dim strSql As String
            Dim sb As New StringBuilder
            Dim db As New DataAccess
            'Dim ds As New DataSet
            Dim strRecordCount As String = ""

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            ' Try
            'We need to get the SQL statement for this report
            strSql = GetSimpleReportSqlCount(sqlID)
            sb.Append(strSql)

            If filterListParams <> "" Then
                sb.Append(" AND " & filterListParams)
            End If
            If filterOtherParams <> "" Then
                sb.Append(" AND " & filterOtherParams)
            End If
            'If sortParams <> "" Then
            '    sb.Append(" ORDER BY " & sortParams)
            'End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            'Execute the query
            Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)
            While dr.Read()
                strRecordCount = dr("EmployerCount")
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return strRecordCount
        End Function

        Public Shared Function GetSqlByResource(ByVal resourceID As Integer) As String
            'Dim strSQL As String = ""
            Dim sb As New StringBuilder
            Dim db As New DataAccess
            ' Dim ds As New DataSet
            Dim strSqlidValue As String = ""

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            sb.Append(" Select RptSQLID from syRptProps where ResourceID=? ")
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            db.AddParameter("@ResourceID", resourceID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            While dr.Read()
                strSqlidValue = dr("RptSQLID")
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return strSqlidValue
        End Function

        Public Shared Function GetObjName(ByVal objID As Integer) As String
            Dim sb As New StringBuilder
            Dim db As New DataAccess
            'Dim ds As New DataSet

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            sb.Append(" Select Descrip from syBusObjects where BusObjectId=? ")
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            db.AddParameter("@ResourceID", objID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            Dim s As String
            s = CType(db.RunParamSQLScalar(sb.ToString), String)

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return s
        End Function

        Public Shared Function GetReportInfo(ByVal resourceID As Integer) As ReportInfo
            Dim sb As New StringBuilder
            Dim db As New DataAccess
            'Dim ds As New DataSet
            Dim rptInfo As New ReportInfo

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            sb.Append("SELECT t1.RptSQLId,t1.RptObjId,t2.ResourceURL,t2.Resource ")
            sb.Append("FROM syRptProps t1, syResources t2 ")
            sb.Append("WHERE t1.ResourceID=t2.ResourceId ")
            sb.Append("AND t1.ResourceID=?")

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            db.AddParameter("@ResourceID", resourceID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read
                If dr("RptObjId").ToString = "" Then
                    rptInfo.ObjId = 0
                Else
                    rptInfo.ObjId = dr("RptObjId")
                End If
                If dr("RptSQLId").ToString = "" Then
                    rptInfo.SqlId = 0
                Else
                    rptInfo.SqlId = dr("RptSQLId")
                End If

                rptInfo.ResourceID = resourceID
                rptInfo.Url = dr("ResourceURL")
                rptInfo.Resource = dr("Resource")
            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return rptInfo
        End Function

        Public Shared Function GetPlacementStats(ByVal sqlID As Integer, Optional ByVal sortParams As String = "",
              Optional ByVal filterListParams As String = "", Optional ByVal filterOtherParams As String = "") As String
            Dim sb As New StringBuilder
            Dim db As New DataAccess
            'Dim ds As New DataSet
            Const strRecordCount As String = ""

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            'We need to get the SQL statement for this report
            Dim strSql As String = GetSimpleReportSqlCount(sqlID)

            sb.Append(strSql)
            If filterListParams <> "" Then
                sb.Append(" AND " & filterListParams)
            End If
            If filterOtherParams <> "" Then
                sb.Append(" AND " & filterOtherParams)
            End If
            'If sortParams <> "" Then
            '    sb.Append(" ORDER BY " & sortParams)
            'End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            'Execute the query
            Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)
            While dr.Read()

            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return strRecordCount
        End Function

        Public Shared Function GetSchoolLogo(Optional ByVal useOfficialLogo As Boolean = False) As AdvantageLogoImage ' As Byte()
            Dim sb As New StringBuilder
            Dim db As New DataAccess
            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            sb.Append("SELECT TOP 1 Image, ImgLen, ContentType  ")
            sb.Append("FROM sySchoolLogo ")
            If useOfficialLogo Then
                sb.Append("WHERE OfficialUse=1 ")
            Else
                sb.Append("WHERE OfficialUse=0 ")   'ImgId=1
            End If
            'Retrieve the smallest logo
            sb.Append("ORDER BY ImgLen")

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            'Execute the query
            Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)
            'Return objName
            dr.Read()

            'this is the AdvantageLogoImage object
            Dim advLogoImage As New AdvantageLogoImage
            If dr.HasRows Then
                'populate AdvantageLogoImage
                With advLogoImage
                    .SchoolId = 0
                    .Image = dr("image")
                    .ImageLength = dr("ImgLen")
                    If dr("ContentType") Is DBNull.Value Then .ContentType = "image/jpeg" Else .ContentType = dr("ContentType")
                End With
            End If

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return advLogoImage
        End Function

        Public Shared Function GetSchoolLogo(ByVal schoolId As Integer) As AdvantageLogoImage
            Dim sb As New StringBuilder
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            sb.Append("SELECT Image, ImgLen, ContentType FROM sySchoolLogo WHERE ImgId=?")
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            'add parameter schoolId to the query
            db.AddParameter("@SchoolId", schoolId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'this is the AdvantageLogoImage object
            Dim advLogoImage As New AdvantageLogoImage
            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            dr.Read()

            If dr.HasRows Then
                'populate AdvantageLogoImage
                advLogoImage.SchoolId = schoolId
                advLogoImage.Image = dr("image")
                advLogoImage.ImageLength = dr("ImgLen")
                If dr("ContentType") Is DBNull.Value Then advLogoImage.ContentType = "image/jpeg" Else advLogoImage.ContentType = dr("ContentType")
            End If

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return advLogoImage
        End Function

        ''' <summary>
        ''' Get all the pre-defined and adhoc reports for the given user.
        ''' The moduleid (resourceid) can be specified.
        ''' </summary>
        ''' <param name="UserId"></param>
        ''' <param name="ModuleId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetReportsForUser(ByVal userId As String, ByVal campusId As String, ByVal moduleId As String,
                                        ByVal showPreDef As Boolean, ByVal showAdHoc As Boolean) As DataSet
            ' do some error checking
            If Not showPreDef AndAlso Not showAdHoc Then
                Return Nothing
            End If

            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            If showPreDef Then
                ' get all pre-defined reports
                sb.Append("select " + vbCrLf)
                sb.Append("     X.ResourceId as ReportId, " & vbCrLf)
                sb.Append("     X.Resource as ReportName, " & vbCrLf)
                sb.Append("     0 as IsAdHoc, " & vbCrLf)
                sb.Append("     '00000000-0000-0000-0000-000000000000' as CampGrpId, " + vbCrLf)
                sb.Append("    '' AS CampGrpDescrip  " + vbCrLf)
                sb.Append("from " & vbCrLf)
                sb.Append("     syResources X, syNavigationNodes Y " + vbCrLf)
                sb.Append("where " + vbCrLf)
                sb.Append("     X.ResourceTypeId = 5 and X.ResourceId = Y.ResourceId " + vbCrLf)
                If Not moduleId Is Nothing AndAlso moduleId <> "" Then
                    sb.Append("     and Y.ParentId=(select B.HierarchyId from syResources A,syNavigationNodes B " + vbCrLf)
                    sb.Append("         where A.ResourceId=B.ResourceId and A.ResourceId = ?) " + vbCrLf)
                    db.AddParameter("@ModuleId", moduleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
            End If

            ' add a union if we're showing both pre-def and adhoc reprots
            If showPreDef AndAlso showAdHoc Then
                sb.Append(vbCrLf + "union " + vbCrLf + vbCrLf)
            End If

            If showAdHoc Then
                ' get all adhoc reports
                sb.Append("select DISTINCT " + vbCrLf)
                sb.Append("     UR.ResourceId as ReportId, " + vbCrLf)
                sb.Append("     UR.Resource as ReportName, " + vbCrLf)
                sb.Append("     1 as IsAdHoc, " + vbCrLf)
                sb.Append("     UR.CampGrpId as CampGrpId,(select CampGrpCode from syCampGrps where CampGrpId=UR.CampGrpId) as CampGrpDescrip " + vbCrLf)
                sb.Append("from " + vbCrLf)
                sb.Append("     syUserResources UR, syUserResPermissions UP " + vbCrLf)
                sb.Append("where " + vbCrLf)
                sb.Append("     UR.ResourceId = UP.UserResourceId " + vbCrLf)
                sb.Append("     and UP.Permission = 1 " + vbCrLf)
                ' 11/14/06 (BEN) Mantis 9578 - Add campus level security
                If Not String.IsNullOrEmpty(campusId) Then
                    sb.Append("     and UR.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId = ?)" + vbCrLf)
                    db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                If Not String.IsNullOrEmpty(moduleId) Then
                    sb.Append("     and UP.ResourceId = ? " + vbCrLf)
                    db.AddParameter("@ModuleId", moduleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                If Not String.IsNullOrEmpty(userId) Then
                    sb.Append("     and (UR.CreatedBy = ? or UR.IsPublic = 1) " + vbCrLf)
                    db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    sb.Append("     and UR.IsPublic = 1 " + vbCrLf)
                End If
            End If

            sb.Append("order by ReportName " + vbCrLf)
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
        'New Function Added By Vijay Ramteke on May 05, 2010
        Public Shared Function GetReportsForUserMsg(ByVal userId As String, ByVal moduleId As String, ByVal campusId As String) As DataSet
            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder

            ' get all adhoc reports
            sb.Append("select Distinct " + vbCrLf)
            sb.Append("     UR.ResourceId as ReportId, " + vbCrLf)
            sb.Append("     UR.Resource as ReportName, " + vbCrLf)
            sb.Append("     1 as IsAdHoc, " + vbCrLf)
            sb.Append("     UR.CampGrpId as CampGrpId,(select CampGrpCode from syCampGrps where CampGrpId=UR.CampGrpId) as CampGrpDescrip " + vbCrLf)
            sb.Append("from " + vbCrLf)
            sb.Append("     syUserResources UR, syUserResPermissions UP " + vbCrLf)
            sb.Append("where " + vbCrLf)
            sb.Append("     UR.ResourceId = UP.UserResourceId " + vbCrLf)
            sb.Append("     and UP.Permission = 1 " + vbCrLf)
            If Not String.IsNullOrEmpty(campusId) Then
                sb.Append("     and UR.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId = ?)" + vbCrLf)
                db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If Not String.IsNullOrEmpty(moduleId) Then
                sb.Append("     and UP.ResourceId = ? " + vbCrLf)
                db.AddParameter("@ModuleId", moduleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If Not String.IsNullOrEmpty(userId) Then
                sb.Append("     and (UR.CreatedBy = ? or UR.IsPublic = 1) " + vbCrLf)
                db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                sb.Append("     and UR.IsPublic = 1 " + vbCrLf)
            End If
            sb.Append("order by ReportName " + vbCrLf)
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
        'New Function Added By Vijay Ramteke on May 05, 2010
#Region "Fields Methods"
        ''' <summary>
        ''' Common function to build the sql string to retrieve the list of fields
        ''' from either syRptAdHocFields or syRptParams depending on the value of IsAdHoc
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetReportFieldsSql(ByVal reportId As String) As String
            '   build the sql query
            Dim sb As New StringBuilder
            Dim lang As String = HttpContext.Current.Items("Language")
            If Not lang Is Nothing Then lang = lang.ToUpper() Else lang = "EN-US"

            ' Get all normal fields
            sb.Append("(" + vbCrLf)
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     F.AdHocFieldId as ParamId, " & vbCrLf)
            sb.Append("     F.ResourceId as ReportId, " & vbCrLf)
            sb.Append("     TF.TblId, " & vbCrLf)
            sb.Append("     0 as IsUDF,	" & vbCrLf)
            sb.Append("      CAST(TF.TblFldsId AS VARCHAR(50)) AS TblFldsId, " & vbCrLf)
            sb.Append("     F.SDFId, " + vbCrLf)
            sb.Append("     (select _T.TblName from syTables _T where _T.TblId=TF.TblId) as TableName, " & vbCrLf)
            sb.Append("     TF.FldId, " & vbCrLf)
            sb.Append("     (select _F.FldName from syFields _F where _F.FldId=TF.FldId) as FieldName, " & vbCrLf)
            sb.Append("     C.Caption as Header,  " & vbCrLf)
            sb.Append("     F.ColOrder, " & vbCrLf)
            sb.Append("     F.Visible, " & vbCrLf)
            sb.Append("     F.FormatString, " & vbCrLf)
            sb.Append("     F.Width, " & vbCrLf)
            sb.Append("     F.ShowInGroupBy, " & vbCrLf)
            sb.Append("     F.ShowInSummary, " & vbCrLf)
            sb.Append("     F.SummaryType, " & vbCrLf)
            sb.Append("     F.GroupBy, " & vbCrLf)
            sb.Append("     F.SortBy, " & vbCrLf)
            sb.Append("     F.IsParam, " & vbCrLf)
            sb.Append("     F.IsRequired, " & vbCrLf)
            sb.Append("     SF.DDLId, " + vbCrLf)
            sb.Append("     SF.FldTypeId, " & vbCrLf)
            sb.Append("     (select _FT.FldType from syFieldTypes _FT where _FT.FldTypeId=SF.FldTypeId) as FldType, " + vbCrLf)
            sb.Append("     TF.FKColDescrip, " & vbCrLf)
            sb.Append("     (SELECT CalculationSQL FROM syFieldCalculation WHERE fldid=TF.fldid) as CalculationSQL " & vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     syRptAdHocFields F, syTblFlds TF, syFields SF, syFldCaptions C, syLangs L " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     F.TblFldsId = TF.TblFldsId AND TF.FldId = SF.FldId AND TF.FldId = C.FldId AND C.Caption is not null ")
            sb.Append("     AND C.LangId = L.LangId AND L.LangName = '" + lang + "'" + vbCrLf)
            sb.AppendFormat("       AND F.ResourceId = {0} {1}", reportId, vbCrLf)

            sb.Append(vbCrLf + "UNION" + vbCrLf + vbCrLf)

            ' get all UDFs
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     F.AdHocFieldId as ParamId, " & vbCrLf)
            sb.Append("     F.ResourceId as ReportId, " & vbCrLf)
            sb.Append("     null as TblId, " & vbCrLf)
            sb.Append("     1 as IsUDF,	" & vbCrLf)
            sb.Append("      CAST(F.SDFId AS VARCHAR(50)) AS TblFldsId, " & vbCrLf)
            sb.Append("     F.SDFId, " + vbCrLf)
            sb.Append("     null as TableName, " & vbCrLf)
            sb.Append("     null as FldId, " & vbCrLf)
            sb.Append("     SF.SDFDescrip as FieldName, " & vbCrLf)
            sb.Append("     SF.SDFDescrip as Header,  " & vbCrLf)
            sb.Append("     F.ColOrder, " & vbCrLf)
            sb.Append("     F.Visible, " & vbCrLf)
            sb.Append("     CASE WHEN SF.DTypeId = 3 THEN '{0:d}' ELSE F.FormatString END AS FormatString , " & vbCrLf)
            sb.Append("     F.Width, " & vbCrLf)
            sb.Append("     F.ShowInGroupBy, " & vbCrLf)
            sb.Append("     F.ShowInSummary, " & vbCrLf)
            sb.Append("     F.SummaryType, " & vbCrLf)
            sb.Append("     F.GroupBy, " & vbCrLf)
            sb.Append("     F.SortBy, " & vbCrLf)
            sb.Append("     F.IsParam, " & vbCrLf)
            sb.Append("     F.IsRequired, " & vbCrLf)
            sb.Append("     (SELECT count(SdfListId) FROM sySDFValList WHERE SDFId=SF.SDFId  having COUNT(SdfListId) >0)  AS DDLId , " + vbCrLf)
            'Troy: AD-8300. We have to map the datatypes from the sySdfDtypes table to those in the syFieldTypes table.
            sb.Append("     (CASE SF.DTypeId  " & vbCrLf)
            sb.Append("          WHEN 1 THEN 200 " & vbCrLf)
            sb.Append("          WHEN 2 THEN 131 " & vbCrLf)
            sb.Append("          WHEN 3 THEN 135 " & vbCrLf)
            sb.Append("          WHEN 4 THEN 11 " & vbCrLf)
            sb.Append("      END " & vbCrLf)
            sb.Append("     ) AS FldTypeId, " & vbCrLf)
            sb.Append("     null as FldType, " + vbCrLf)
            sb.Append("     null as FKColDescrip, " & vbCrLf)
            sb.Append("     null as CalculationSQL " & vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     syRptAdHocFields F, sySDF SF " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     F.SDFid = SF.SDFId " + vbCrLf)
            sb.AppendFormat("       AND F.ResourceId = {0} {1}", reportId, vbCrLf)
            sb.Append(")" + vbCrLf)

            Return sb.ToString()
        End Function

        ''' <summary>
        ''' Returns the list of fields for a given ReportId (ResourceId)
        ''' as a DataSet.  Useful for binding to controls.
        ''' </summary>
        ''' <param name="reportId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetReportFieldsDS(ByVal reportId As String, ByVal isAdHoc As Boolean) As DataSet
            ' check for invalid ReportId passed
            If reportId Is Nothing Or reportId = "" Then
                Return Nothing
            End If

            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append(GetReportFieldsSql(reportId))
            sb.Append("ORDER BY ColOrder ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Returns a datatable describing all the fields given a set of TblFldIds
        ''' </summary>
        ''' <param name="tblFldIds"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetDtFromTblFldIds(ByVal tblFldIds() As Integer) As DataTable
            '   build the sql query
            Dim sb As New StringBuilder

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            sb.Append("SELECT " + vbCrLf)
            sb.Append("     TF.TblId, " & vbCrLf)
            sb.Append("     TF.TblFldsId, " & vbCrLf)
            sb.Append("     (select _T.TblName from syTables _T where _T.TblId=TF.TblId) as TableName, " & vbCrLf)
            sb.Append("     TF.FldId, " & vbCrLf)
            sb.Append("     (select _F.FldName from syFields _F where _F.FldId=TF.FldId) as FieldName, " & vbCrLf)
            sb.Append("     SF.FldTypeId, " & vbCrLf)
            sb.Append("     (select _FT.FldType from syFieldTypes _FT where _FT.FldTypeId=SF.FldTypeId) as FldType, " + vbCrLf)
            sb.Append("     TF.FKColDescrip " & vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     syTblFlds TF, syFields SF, syFldCaptions C " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     TF.FldId = SF.FldId AND TF.FldId = C.FldId AND C.Caption is not null " + vbCrLf)
            sb.Append("     AND TF.TblFldsId in (")
            For i As Integer = 0 To tblFldIds.Length - 1
                sb.Append(tblFldIds(i))
                If i < tblFldIds.Length - 1 Then sb.Append(",")
            Next
            sb.Append(")" + vbCrLf)

            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
        End Function

        Public Shared Function GetValuesForBitField() As DataTable
            Dim db As New DataAccess
            Dim ds As DataSet
            Dim sb As New StringBuilder
            sb.Append("select boolValue as valueid,BoolDescrip as Descrip from arBoolean")
            Try
                ds = db.RunParamSQLDataSet(sb.ToString)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw New Exception("Error getting info for DDL - " & ex.Message)
            Finally
                db.Dispose()
                'db = Nothing
            End Try
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="ddlId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetValuesForField(ByVal ddlId As String, Optional ByVal campGrpId As String = "", Optional ByVal strUserId As String = "") As DataTable
            Dim db As New DataAccess
            Dim ds As DataSet
            Dim sb As New StringBuilder
            Dim userId As String = strUserId

            Select Case ddlId
                ' Special case: CampusId (Mantis 9359) - added by Ben on 11/2/06
                Case 5
                    If HttpContext.Current.Session("UserName").ToString = "sa" Then
                        ' Advantage Power User. He/She can see ALL campus groups.
                        sb.Append("SELECT DISTINCT CampusId as ValueId, CampDescrip as Descrip " + vbCrLf)
                        sb.Append("FROM syCampuses " + vbCrLf)
                        sb.Append("ORDER BY CampDescrip" + vbCrLf)
                    Else
                        'Enforce security: need to get list of campus group that user has access to.
                        sb.Append("SELECT DISTINCT C.CampusId as ValueId, C.CampDescrip as Descrip " + vbCrLf)
                        sb.Append("FROM syCampuses C " + vbCrLf)
                        sb.Append("WHERE C.CampusId in " + vbCrLf)
                        sb.Append("     (select CC.CampusId from syCmpGrpCMps CC, syUsersRolesCampGrps URC " + vbCrLf)
                        sb.Append("         where URC.UserId=? " + vbCrLf)
                        sb.Append("         and URC.CampGrpId = CC.CampGrpId) " + vbCrLf)
                        sb.Append("ORDER BY C.CampDescrip")
                        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    End If

                Case 7
                    'Added by Michelle R. Rodriguez on 01/18/2005.
                    'Special case: Campus Group DDL.
                    If HttpContext.Current.Session("UserName").ToString = "sa" Then
                        ' Advantage Power User. He/She can see ALL campus groups.
                        sb.Append("SELECT DISTINCT A.CampGrpId as ValueId, A.CampGrpDescrip as Descrip ")
                        sb.Append("FROM syCampGrps A ")
                        sb.Append("ORDER BY A.CampGrpDescrip")
                    Else
                        'Enforce security: need to get list of campus group that user has access to.
                        sb.Append("SELECT DISTINCT A.CampGrpId as ValueId, B.CampGrpDescrip as Descrip ")
                        sb.Append("FROM syUsersRolesCampGrps A, syCampGrps B ")
                        sb.Append("WHERE A.CampGrpId = B.CampGrpId ")
                        sb.Append("AND A.UserId=? ")
                        sb.Append("ORDER BY B.CampGrpDescrip")
                        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    End If

                Case 81
                    'Added by Michelle R. Rodriguez on 02/10/2005.
                    'Special case: StuEnrollmentStatus DDL.
                    sb.Append("SELECT DISTINCT D.StatusCodeId as ValueId, D.StatusCodeDescrip as Descrip ")
                    sb.Append("FROM sySysStatus A,syStatuses B,syStatusLevels C,syStatusCodes D ")
                    sb.Append("WHERE A.StatusId=B.StatusId AND B.Status='Active' ")
                    sb.Append("AND A.StatusLevelId=C.StatusLevelId AND C.StatusLevelId=2 ")
                    sb.Append("AND D.SysStatusId=A.SysStatusId ")
                    If campGrpId <> "" Then
                        sb.Append(" and (D.CampgrpId='" & campGrpId & "'")
                        sb.Append(" or D.CampgrpId=(select CampGrpId from syCampGrps where CampGrpDescrip='All')) ")
                    End If
                    sb.Append("ORDER BY D.StatusCodeDescrip ")

                    ds = db.RunSQLDataSet(sb.ToString)
                    Return ds.Tables(0)

                Case 89
                    'Added by Michelle R. Rodriguez on 11/30/2005.
                    'Special case: LeadStatus DDL.
                    sb.Append("SELECT DISTINCT D.StatusCodeId as ValueId, D.StatusCodeDescrip as Descrip ")
                    sb.Append("FROM sySysStatus A,syStatuses B,syStatusLevels C,syStatusCodes D ")
                    sb.Append("WHERE A.StatusId=B.StatusId AND B.Status='Active' ")
                    sb.Append("AND A.StatusLevelId=C.StatusLevelId AND C.StatusLevelId=1 ")
                    sb.Append("AND D.SysStatusId=A.SysStatusId ")
                    If campGrpId <> "" Then
                        sb.Append(" and (D.CampgrpId='" & campGrpId & "'")
                        sb.Append(" or D.CampgrpId=(select CampGrpId from syCampGrps where CampGrpDescrip='All')) ")
                    End If
                    sb.Append("ORDER BY D.StatusCodeDescrip ")
                    ds = db.RunSQLDataSet(sb.ToString)
                    Return ds.Tables(0)

                Case 49
                    'Added by Michelle R. Rodriguez on 09/19/2005.
                    'Special case: Term DDL.
                    sb.Append("SELECT     TermId as ValueId, TermDescrip as Descrip ")
                    sb.Append("FROM       arTerm ")
                    If campGrpId <> "" Then
                        sb.Append(" where (arTerm.CampgrpId='" & campGrpId & "'")
                        sb.Append(" or arTerm.CampgrpId=(select CampGrpId from syCampGrps where CampGrpDescrip='All')) ")
                    End If
                    sb.Append("ORDER BY   StartDate,EndDate,TermDescrip ")
                    ds = db.RunSQLDataSet(sb.ToString)
                    Return ds.Tables(0)

                Case 55
                    'JAGG Return all banks in database to filter.
                    sb.Append("SELECT BankAcctId as ValueId, BankAcctDescrip as Descrip ")
                    sb.Append("FROM  saBankAccounts ")
                    sb.Append("ORDER BY   BankAcctDescrip")
                    ds = db.RunSQLDataSet(sb.ToString)
                    Return ds.Tables(0)

                Case 90
                    With sb
                        .Append(" select distinct s1.UserId as ValueId,s1.fullname as Descrip from ")
                        .Append(" syUsers s1,syUsersRolesCampGrps s2,syRoles s3,sySysRoles s4,syCmpGrpCmps s5,syStatuses s6 ")
                        .Append(" where s1.UserId = s2.UserId and s2.RoleId = s3.RoleId ")
                        .Append(" and s3.SysRoleId = s4.SysRoleId and s2.CampGrpId = s5.CampGrpId and s4.StatusId = s6.StatusId ")
                        .Append(" and s4.SysRoleId = 3 ")
                        If campGrpId <> "" Then
                            .Append(" and (s2.CampgrpId='" & campGrpId & "'")
                            .Append(" or s2.CampgrpId=(select CampGrpId from syCampGrps where CampGrpDescrip='All')) ")

                        End If
                        .Append(" order by s1.fullname")
                    End With
                    ds = db.RunSQLDataSet(sb.ToString)
                    Return ds.Tables(0)
                Case 119
                    With sb
                        .Append(" SELECT DISTINCT HSId AS ValueId,HSName AS Descrip FROM syInstitutions AS SI	INNER JOIN adLeads AS AL ON AL.HighSchoolId = SI.HSId ")
                        If campGrpId <> "" Then
                            sb.Append(" where (SI.CampGrpId='" & campGrpId & "'")
                            sb.Append(" or SI.CampGrpId=(select CampGrpId from syCampGrps where CampGrpDescrip='All')) ")
                        End If
                        sb.Append(" ORDER BY   SI.HSName ")
                        ds = db.RunSQLDataSet(sb.ToString)
                        Return ds.Tables(0)
                    End With
                Case 129
                    With sb
                        .Append(" SELECT DISTINCT AL.LeadGrpId AS ValueId,Descrip FROM adLeadGroups AS SI	INNER JOIN adLeadByLeadGroups AS AL ON AL.LeadGrpId = SI.LeadGrpId ")
                        If campGrpId <> "" Then
                            sb.Append(" where  StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND   (SI.CampGrpId='" & campGrpId & "'")
                            sb.Append(" or SI.CampGrpId=(select CampGrpId from syCampGrps where CampGrpDescrip='All')) ")
                        End If
                        sb.Append(" ORDER BY   SI.Descrip ")
                        ds = db.RunSQLDataSet(sb.ToString)
                        Return ds.Tables(0)
                    End With
                Case Else
                    Try
                        'We first need to retrieve the information for the DDLS.
                        Dim dtDDLDefs As DataTable = _GetValuesForField(ddlId)

                        'Only one row is returned so we can point dr to the first row.
                        db.ConnectionString = "ConString"
                        If dtDDLDefs.Rows.Count = 0 Then Return Nothing

                        Dim dr As DataRow = dtDDLDefs.Rows(0)
                        'We can now set up the template

                        Dim sTemplate As String = "SELECT %ValueField% as ValueId, %DisplayField% as Descrip " &
                                      "FROM %TableName% " &
                                      "ORDER BY %DisplayField%"
                        'Replace the TableName token
                        If campGrpId <> "" Then
                            Dim cmpGrpCnt As Integer = GetCampGrpCnt(dr("TblName").ToString())
                            If cmpGrpCnt > 0 Then
                                If dr("TblName").ToString().ToLower = "pljobschedule" Then
                                    sTemplate = "SELECT %ValueField% as ValueId, %DisplayField% as Descrip " &
                                  " FROM %TableName% where " &
                                 "  ( CamgrpId='" & campGrpId & "'" &
                                 " or CamgrpId=(select CampGrpId from syCampGrps where CampGrpDescrip='All')) " &
                                  " ORDER BY %DisplayField%"
                                Else
                                    sTemplate = "SELECT %ValueField% as ValueId, %DisplayField% as Descrip " &
                                  "FROM %TableName% where " &
                                 "  ( CampgrpId='" & campGrpId & "'" &
                                 " or CampgrpId=(select CampGrpId from syCampGrps where CampGrpDescrip='All')) " &
                                  " ORDER BY %DisplayField%"
                                End If

                            End If
                        End If
                        sTemplate = sTemplate.Replace("%TableName%", dr("TblName").ToString())
                        'Replace the DisplayField token
                        sTemplate = sTemplate.Replace("%DisplayField%", dr("DispText").ToString())
                        'Replace the ValueField token
                        sTemplate = sTemplate.Replace("%ValueField%", dr("DispValue").ToString())

                        sb.Append(sTemplate)
                    Catch ex As Exception
                        Throw New Exception("Error getting info for DDL - " & ex.Message)
                    End Try
            End Select
            Try
                ds = db.RunParamSQLDataSet(sb.ToString)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw New Exception("Error getting info for DDL - " & ex.Message)
            Finally
                db.Dispose()
                'db = Nothing
            End Try
        End Function

        Public Shared Function GetValuesForSdfField(ByVal sdfId As String) As DataTable
            Dim ds As DataSet
            Dim db As New SQLDataAccess

            db.AddParameter("SDFId", New Guid(sdfId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetValuesForSDFField")

            Return ds.Tables(0)
        End Function

#End Region
#Region "Helper Function"
        ''' <summary>
        ''' Returns a dt with the DDL info for the requested ddl collection.
        ''' </summary>
        ''' <param name="ddlId">Id of the DDL that info is needed for.</param>
        ''' <returns>DataTable</returns>
        ''' <remarks>This sub relies on the ResourceId passed in the querystring or in the form's params 
        ''' collection when there is a postback.</remarks>
        Protected Shared Function _GetValuesForField(ByVal ddlId As String) As DataTable
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     t3.FldName, " + vbCrLf)
            sb.Append("     t4.DDLId, " + vbCrLf)
            sb.Append("     t5.TblName, " + vbCrLf)
            sb.Append("     t6.FldName as DispText, " + vbCrLf)
            sb.Append("     t7.FldName as DispValue, " + vbCrLf)
            sb.Append("     t4.ResourceId as DDLResourceId " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     syTblFlds t2, syFields t3, syDDLS t4, syTables t5, syFields t6, syFields t7 " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     t2.FldId=t3.FldId " + vbCrLf)
            sb.Append("     AND t3.DDLId=t4.DDLId " + vbCrLf)
            sb.Append("     AND t4.TblId=t5.TblId " + vbCrLf)
            sb.Append("     AND t4.DispFldId=t6.FldId " + vbCrLf)
            sb.Append("     AND t4.ValFldId=t7.FldId " + vbCrLf)
            sb.Append("     AND t4.DDLId = ? " + vbCrLf)

            db.AddParameter("@ddlid", ddlId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
        End Function
        Protected Shared Function GetCampGrpCnt(ByVal tblName As String) As Integer
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim rtn As Integer
            If tblName.ToLower = "pljobschedule" Then
                sb.Append("Select count(COLUMN_NAME) as campGrpIdCnt From Information_Schema.Columns Where Table_Name = '" & tblName & "'")
                sb.Append(" and LOWER (Column_Name) = 'camgrpid' ")
            Else
                sb.Append("Select count(COLUMN_NAME) as campGrpIdCnt From Information_Schema.Columns Where Table_Name = '" & tblName & "'")
                sb.Append(" and LOWER (Column_Name) = 'campgrpid' ")
            End If
            rtn = CInt(db.RunSQLScalar(sb.ToString))
            Return rtn
        End Function

#End Region
#Region "Build SQL Methods"
#End Region
    End Class
#End Region

#Region "Params"
    Public Class RptParamsDB
#Region "Private Methods - Commented"
        ' ''' <summary>
        ' ''' Returns a dt with the DDL info for the requested ddl collection.
        ' ''' </summary>
        ' ''' <param name="ddlId">Id of the DDL that info is needed for.</param>
        ' ''' <returns>DataTable</returns>
        ' ''' <remarks>This sub relies on the ResourceId passed in the querystring or in the form's params 
        ' ''' collection when there is a postback.</remarks>
        'Private Shared Function GetDDLInfo(ByVal ReportId As String, ByVal IsAdHoc As Boolean, ByVal ddlId As Integer) As DataTable
        '    Try
        '        Dim db As New DataAccess

        '        Dim MyAdvAppSettings As AdvAppSettings
        '        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        '        Else
        '            MyAdvAppSettings = New AdvAppSettings
        '        End If

        '        Dim sb As New StringBuilder
        '        sb.Append("SELECT " + vbCrLf)
        '        sb.Append("     t1.RptParamId, " + vbCrLf)
        '        sb.Append("     t3.FldName, " + vbCrLf)
        '        sb.Append("     t4.DDLId, " + vbCrLf)
        '        sb.Append("     t5.TblName, " + vbCrLf)
        '        sb.Append("     t6.FldName as DispText, " + vbCrLf)
        '        sb.Append("     t7.FldName as DispValue, " + vbCrLf)
        '        sb.Append("     t4.ResourceId as DDLResourceId " + vbCrLf)
        '        sb.Append("FROM " + vbCrLf)
        '        sb.Append("     syRptParams t1, syTblFlds t2, syFields t3, syDDLS t4, syTables t5, syFields t6, syFields t7 " + vbCrLf)
        '        sb.Append("WHERE " + vbCrLf)
        '        sb.Append("     t1.TblFldsId = t2.TblFldsId " + vbCrLf)
        '        sb.Append("     AND t2.FldId=t3.FldId " + vbCrLf)
        '        sb.Append("     AND t3.DDLId=t4.DDLId " + vbCrLf)
        '        sb.Append("     AND t4.TblId=t5.TblId " + vbCrLf)
        '        sb.Append("     AND t4.DispFldId=t6.FldId " + vbCrLf)
        '        sb.Append("     AND t4.ValFldId=t7.FldId " + vbCrLf)
        '        sb.Append("     AND t1.ResourceId = ? " + vbCrLf)
        '        sb.Append("     AND t4.DDLId = ? " + vbCrLf)

        '        db.AddParameter("@resid", ReportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '        db.AddParameter("@ddlid", ddlId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        '        Return db.RunParamSQLDataSet(sb.ToString, "RptParamDDL").Tables(0)
        '    Catch ex As System.Exception
        '        Throw New Exception("Error retrieving parameters for report - " & ex.InnerException.Message)
        '    End Try
        'End Function
#End Region

#Region "Public Methods"
        ''' <summary>
        ''' Retrieves the preferences saved for a given report by a specific user.  Set IsAdHoc
        ''' to true to get prefs for an AdHoc report.
        ''' </summary>
        ''' <returns>DataTable containing the preference id and the preference name.</returns>
        ''' <remarks></remarks>
        Public Shared Function GetAll(ByVal userId As String, ByVal reportId As String,
                                                Optional ByVal isAdHoc As Boolean = False) As DataTable
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     PrefId, " + vbCrLf)
            sb.Append("     PrefName " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     syRptUserPrefs " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            If Not isAdHoc Then
                sb.Append("     ResourceId = ? " + vbCrLf)
            Else
                sb.Append("     UserResourceId = ? " + vbCrLf)
            End If
            db.AddParameter("@resid", reportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not userId Is Nothing AndAlso userId <> "" Then
                sb.Append("     AND UserID = ? " + vbCrLf)
                db.AddParameter("@usrname", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            sb.Append("ORDER BY PrefName" + vbCrLf)
            db.ConnectionString = "ConString"
            Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
        End Function

        ''' <summary>
        ''' Loads a UserPrefsInfo object from the database given a prefid
        ''' </summary>
        ''' <param name="id">A prefid</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal id As String) As UserPrefsInfo
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder()
            sb.Append("select " + vbCrLf)
            sb.Append("     UserId, " + vbCrLf)
            sb.Append("     (select UserName from syUsers U where U.UserId = UP.UserId) as UserName, " + vbCrLf)
            sb.Append(" 	isnull(ResourceId, UserResourceId) as ReportId, " + vbCrLf)
            sb.Append("	    case " + vbCrLf)
            sb.Append("         when ResourceId is null then " + vbCrLf)
            sb.Append("             (select Resource from syUserResources UR where UR.ResourceId = UP.UserResourceId) " + vbCrLf)
            sb.Append("		    else (select Resource from syResources UR where UR.ResourceId = UP.ResourceId) " + vbCrLf)
            sb.Append("     end as ReportName, " + vbCrLf)
            sb.Append("     case when ResourceId is null then 1 else 0 end as IsAdHoc, " + vbCrLf)
            sb.Append("     UP.PrefName " + vbCrLf)
            sb.Append("from " + vbCrLf)
            sb.Append("     syRptUserPrefs UP " + vbCrLf)
            sb.Append("where " + vbCrLf)
            sb.Append("     UP.PrefId = ?; " + vbCrLf)

            db.AddParameter("@id", id, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            Try
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                Dim info As New UserPrefsInfo()
                If dr.Read() Then
                    '   set properties with data from DataReader
                    info.PrefId = id
                    info.IsInDB = True
                    If Not (dr("UserId") Is DBNull.Value) Then info.UserId = dr("UserId").ToString()
                    If Not (dr("UserName") Is DBNull.Value) Then info.UserName = dr("UserName").ToString()
                    If Not (dr("ReportId") Is DBNull.Value) Then info.ReportId = dr("ReportId")
                    If Not (dr("ReportName") Is DBNull.Value) Then info.ReportName = dr("ReportName")
                    If Not (dr("IsAdHoc") Is DBNull.Value) Then info.IsAdHoc = CType(dr("IsAdHoc"), Boolean)
                    If Not (dr("PrefName") Is DBNull.Value) Then info.PrefName = dr("PrefName")

                    ' retrieve the filters for this user pref
                    info.Filters = GetFilters(id)
                End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                Return info
            Catch ex As Exception
            End Try
            Return Nothing
        End Function

        ''' <summary>
        ''' Returns all the filters for the given PrefId
        ''' </summary>
        ''' <param name="id">A prefid</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetFiltersDS(ByVal id As String) As DataSet
            ' check for invalid PrefId passed
            If id Is Nothing Or id = "" Then Return Nothing

            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append(GetFiltersSql(id))
            db.AddParameter("@Id", id, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Returns an array of FilterInfo objectes retrieved from the db given a prefid
        ''' </summary>
        ''' <param name="id">A prefid</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetFilters(ByVal id As String) As FilterInfo()
            ' check for invalid PrefId passed
            If id Is Nothing Or id = "" Then Return Nothing

            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append(GetFiltersSql(id))
            db.AddParameter("@Id", id, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            Try
                Dim dt As DataTable = db.RunParamSQLDataSet(sb.ToString).Tables(0)
                ' check if there are any fields defined for this report
                Dim count As Integer = dt.Rows.Count
                If count = 0 Then Return Nothing

                ' iterate through the resulting datatable and populate the values 
                Dim info(count - 1) As FilterInfo
                For i As Integer = 0 To count - 1
                    info(i) = New FilterInfo()
                    Dim dr As DataRow = dt.Rows(i)

                    If Not (dr("FilterId") Is DBNull.Value) Then info(i).FilterId = dr("FilterId").ToString()
                    If Not (dr("PrefId") Is DBNull.Value) Then info(i).PrefId = dr("PrefId").ToString()
                    If Not (dr("RptParamId") Is DBNull.Value) Then info(i).RptParamId = dr("RptParamId").ToString()
                    If Not (dr("OpId") Is DBNull.Value) Then info(i).OpId = CType(dr("OpId"), Integer)
                    If Not (dr("OpValue") Is DBNull.Value) Then info(i).Value = dr("OpValue").ToString()
                    If Not (dr("AdHocFieldId") Is DBNull.Value) Then info(i).AdHocFieldId = dr("AdHocFieldId").ToString()
                Next

                Return info ' return the array of fields
            Catch ex As Exception
            End Try
            Return Nothing
        End Function

        ''' <summary>
        ''' Update a UserPrefsInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Update(ByVal info As UserPrefsInfo, ByVal user As String) As String
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("UPDATE syRptUserPrefs " & vbCrLf)
                sb.Append("SET " & vbCrLf)
                sb.Append(" UserId = ?, " & vbCrLf)
                sb.Append(" ResourceId = ?," & vbCrLf)
                sb.Append(" PrefName = ?, " & vbCrLf)
                sb.Append(" UserResourceId = ? " & vbCrLf)
                sb.Append("WHERE Prefid = ? ")

                '   add parameters values to the query                
                db.AddParameter("@userid", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.IsAdHoc Then
                    db.AddParameter("@resid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@resid", info.ReportId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@prefname", info.PrefName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If Not info.IsAdHoc Then
                    db.AddParameter("@uresid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@uresid", info.ReportId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@prefid", info.PrefId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                ' run the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)

                ' iterate through the resulting datatable and populate the values 
                DeleteAllFilters(info.PrefId)
                If Not info.Filters Is Nothing Then
                    For Each fInfo As FilterInfo In info.Filters
                        AddFilter(fInfo)
                    Next
                End If

                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
        End Function

        ''' <summary>
        ''' Persists a new UserPrefsInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As UserPrefsInfo, ByVal user As String) As String
            '   Connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("INSERT syRptUserPrefs (PrefId, UserId, ResourceId, PrefName, UserResourceId) ")
                sb.Append(" VALUES (?,?,?,?,?) ")

                '   add parameters values to the query
                db.AddParameter("@prefid", info.PrefId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@userid", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.IsAdHoc Then
                    db.AddParameter("@resid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@resid", info.ReportId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@prefname", info.PrefName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If Not info.IsAdHoc Then
                    db.AddParameter("@uresid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@uresid", info.ReportId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()

                ' add all the fields
                If Not info.Filters Is Nothing Then
                    For Each fInfo As FilterInfo In info.Filters
                        fInfo.PrefId = info.PrefId ' Need to set the PrefId
                        AddFilter(fInfo)
                    Next
                End If
                '   return without errors
                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
        End Function

        ''' <summary>
        ''' Deletes all the user preference records for the given prefid
        ''' </summary>
        ''' <param name="id">A prefid</param>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal id As String) As String
            ' Do an error check on the param
            If (String.IsNullOrEmpty(id)) Then Return "Invalid param passed to delete function"

            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            'This section deals with deleting the sort prefs.
            Dim sb As New StringBuilder()
            sb.Append("DELETE FROM syRptSortPrefs WHERE PrefId = ? " + vbCrLf)
            sb.Append("DELETE FROM syRptFilterListPrefs WHERE PrefId = ? " + vbCrLf)
            sb.Append("DELETE FROM syRptFilterOtherPrefs WHERE PrefId = ? " + vbCrLf)
            sb.Append("DELETE FROM syRptUserPrefs WHERE PrefId = ? " + vbCrLf)
            db.AddParameter("@id1", id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@id2", id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@id3", id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@id4", id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString())
            Return ""
        End Function

        ''' <summary>
        ''' Deletes all the user preference records for the given prefid
        ''' </summary>
        ''' <param name="Id">A ReportId</param>
        ''' <remarks></remarks>
        Public Shared Function DeleteByReportId(ByVal id As String) As String
            ' Do an error check on the param
            If (String.IsNullOrEmpty(id)) Then Return "Invalid param passed to delete function"

            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            'This section deals with deleting the sort prefs.
            Dim sb As New StringBuilder()
            ' delete syRptFilterOtherPrefs
            sb.Append("delete syRptFilterOtherPrefs " + vbCrLf)
            sb.Append("from syRptUserPrefs " + vbCrLf)
            sb.Append("where syRptUserPrefs.PrefId = syRptFilterOtherPrefs.PrefId " + vbCrLf)
            sb.Append("     and syRptUserPrefs.UserResourceId = ? " + vbCrLf)
            ' delete syRptUserPrefs
            sb.Append("DELETE FROM syRptUserPrefs WHERE UserResourceId = ? " + vbCrLf)
            db.AddParameter("@id1", id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@id2", id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString())
            Return ""
        End Function

#Region "Private Helper Functions"
        Protected Shared Sub AddFilter(ByVal info As FilterInfo)
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            Dim sb As New StringBuilder
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            sb.Append("INSERT INTO syRptFilterOtherPrefs (FilterOtherPrefId, PrefId, RptParamId, OpId, OpValue, AdHocFieldId) ")
            sb.Append("VALUES(?,?,?,?,?,?)")

            db.AddParameter("@filterid", info.FilterId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@prefid", info.PrefId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@rptparamid", info.RptParamId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@opid", info.OpId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@opvalue", info.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@adhocfieldid", info.AdHocFieldId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
        End Sub

        Protected Shared Sub DeleteAllFilters(ByVal prefId As String)
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            Const sql As String = "delete syRptFilterOtherPrefs where PrefId = ?"
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.AddParameter("@prefid", prefId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sql)
        End Sub
#End Region

#Region "Helper Functions"
        ''' <summary>
        ''' Helper function which returns a sql query to get the list
        ''' of filters for a user pref
        ''' </summary>
        ''' <param name="id"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function GetFiltersSql(ByVal id As String) As String
            Dim sb As New StringBuilder()
            sb.Append(vbCrLf)
            sb.Append("select " + vbCrLf)
            sb.Append("     F.FilterOtherPrefId as FilterId, " + vbCrLf)
            sb.Append("     F.PrefId as PrefId, " + vbCrLf)
            sb.Append("     F.RptParamId as RptParamId, " + vbCrLf)
            sb.Append("     F.OpId as OpId, " + vbCrLf)
            sb.Append("     F.OpValue as OpValue, " + vbCrLf)
            sb.Append("     F.AdHocFieldId as AdHocFieldId " + vbCrLf)
            sb.Append("from " + vbCrLf)
            sb.Append("     syRptFilterOtherPrefs F " + vbCrLf)
            sb.Append("where " + vbCrLf)
            sb.Append("     F.PrefId = ? " + vbCrLf)
            Return sb.ToString()
        End Function

        ''' <summary>
        ''' Helper function to get report parameters
        ''' </summary>
        ''' <param name="reportId"></param>
        ''' <param name="IsAdHoc"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function GetRptParamsSql(ByVal reportId As String, ByVal isAdHoc As Boolean) As String
            Dim sb As New StringBuilder
            'Dim lang As String = HttpContext.Current.Items("Language")
            'If Not lang Is Nothing Then lang = lang.ToUpper() Else lang = "EN-US"
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     DISTINCT t1.RptParamId, " + vbCrLf)
            sb.Append("     t1.RptCaption AS Caption, " + vbCrLf)
            sb.Append("     t1.Required, " + vbCrLf)
            sb.Append("     t1.SortSec, " + vbCrLf)
            sb.Append("     t1.FilterListSec, " + vbCrLf)
            sb.Append("     t1.FilterOtherSec, " + vbCrLf)
            sb.Append("     t2.TblId, " + vbCrLf)
            sb.Append("     t8.TblName, " + vbCrLf)
            sb.Append("     t5.FldName, " + vbCrLf)
            sb.Append("     t5.FldId, " + vbCrLf)
            sb.Append("     t5.DDLId, " + vbCrLf)
            sb.Append("     t5.FldLen, " + vbCrLf)
            sb.Append("     t7.FldType " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     syRptParams t1, syTblFlds t2, syFldCaptions t3,syLangs t4,syFields t5,syFieldTypes t7,syTables t8 " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     t1.TblFldsId=t2.TblFldsId " + vbCrLf)
            sb.Append("     AND t2.TblId=t8.Tblid " + vbCrLf)
            sb.Append("     AND t2.FldId=t3.FldId " + vbCrLf)
            sb.Append("     AND t3.LangId=t4.LangId " + vbCrLf)
            sb.Append("     AND t2.FldId=t5.FldId " + vbCrLf)
            sb.Append("     AND t5.FldTypeId=t7.FldTypeId " + vbCrLf)
            sb.Append("     AND t1.ResourceId = ? " + vbCrLf)
            sb.Append("     AND t4.LangName = ? " + vbCrLf)
            sb.Append("     AND t1.IsAdHoc = ? " + vbCrLf)
            Return sb.ToString()
        End Function


        ''' <summary>
        ''' Retrieves all the necessary info to set up the parameters for the requested report
        ''' </summary>
        ''' <returns>DataSet containing the parameters information</returns>
        ''' <remarks>This sub relies on the ResourceId passed in the querystring or in the form's params 
        ''' collection when there is a postback.</remarks>
        Public Shared Function GetRptParams(ByVal reportId As String, ByVal isAdHoc As Boolean) As DataSet
            Try
                Dim ds As DataSet
                Dim db As New DataAccess
                Dim sb As New StringBuilder

                Dim lang As String = HttpContext.Current.Items("Language")
                If Not lang Is Nothing Then lang = lang.ToUpper() Else lang = "EN-US"

                db.ConnectionString = "ConString"
                sb.Append(GetRptParamsSql(reportId, isAdHoc))

                sb.Append("ORDER BY t1.RptCaption;" + vbCrLf)
                sb.Append("SELECT t.AllowParams, t.AllowDescrip, t.AllowInstruct, t.AllowNotes " + vbCrLf)
                sb.Append("FROM syRptProps t " + vbCrLf)
                sb.Append("WHERE t.ResourceId = ? " + vbCrLf)

                db.AddParameter("@resid", reportId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@langname", lang, DataAccess.OleDbDataType.OleDbString, 5, ParameterDirection.Input)
                db.AddParameter("@isadhoc", isAdHoc, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@resid", reportId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                ds = db.RunParamSQLDataSet(sb.ToString)
                If ds.Tables.Count > 0 Then
                    ds.Tables(0).TableName = "RptParams"
                    If ds.Tables.Count > 1 Then ds.Tables(1).TableName = "RptProps"
                End If
                Return ds
            Catch ex As Exception
                Throw New Exception("Error retrieving parameters for report - " & ex.InnerException.Message)
            End Try
            'Return Nothing
        End Function
#End Region
#End Region
    End Class
#End Region

#Region "AdHoc Reports"
    Public Class AdHocRptDB
#Region "Reports Methods"
        ''' <summary>
        ''' Returns all the adhoc reports defined in the system.
        ''' Adhoc reports can be identified in syUserResources by ResourceTypeId = 9
        ''' 10/27/06 - BEN - Add support for the "All Campus Group"
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAll(ByVal userId As String, ByVal campusId As String, ByVal moduleId As String, ByVal entityId As String,
                                        ByVal showActive As Boolean, ByVal showInactive As Boolean) As DataSet
            ' Get if the user is a sa or administrator reports.
            Dim isAdministrator As Boolean = IsReportAdministrator(userId, campusId)

            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     UR.ResourceId as ReportId, " & vbCrLf)
            sb.Append("     UR.Resource as ReportName, " & vbCrLf)
            sb.Append("     UR.ModUser as UserName, " & vbCrLf) ' retrieved the last user that modified
            sb.Append("     UR.CreatedBy as UserName, " & vbCrLf) ' retrieved the user that create the table
            sb.Append("     UR.IsPublic, " & vbCrLf)
            sb.Append("     UR.Active " & vbCrLf)
            sb.Append("FROM syUserResources UR " & vbCrLf)
            If Not moduleId Is Nothing AndAlso moduleId <> "" Then
                sb.Append("     ,syAdvantageResourceRelations R, syResources E, syResources M " + vbCrLf)
            End If
            sb.Append("WHERE   UR.ResourceTypeId = 9 " & vbCrLf) ' Only get adhoc reports

            ' filter on the campusid
            If Not campusId Is Nothing AndAlso campusId <> "" Then
                sb.Append("     AND UR.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId = ?)" + vbCrLf)
                db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            ' add more tables if ModuleId has been passed
            If Not moduleId Is Nothing AndAlso moduleId <> "" Then
                sb.Append("        and R.ResourceId = E.ResourceId and " + vbCrLf)
                sb.Append("        R.RelatedResourceId = M.ResourceId and " + vbCrLf)
                sb.Append("        E.ResourceTypeId=8 and " + vbCrLf)
                sb.Append("        M.ResourceTypeId=1 and " + vbCrLf)
                sb.Append("	 	   E.ResourceId = UR.EntityId " + vbCrLf)
            End If

            If Not isAdministrator Then
                If Not userId Is Nothing AndAlso userId <> "" Then
                    sb.Append("     AND UR.CreatedBy = ? " & vbCrLf)
                    db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
            End If
            If Not moduleId Is Nothing AndAlso moduleId <> "" Then
                sb.Append("     AND M.ResourceId = ? " & vbCrLf)
                db.AddParameter("@ModuleId", moduleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If Not entityId Is Nothing AndAlso entityId <> "" Then
                sb.Append("     AND UR.EntityId = ? " & vbCrLf)
                db.AddParameter("@EntityId", entityId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If showActive AndAlso Not showInactive Then
                sb.Append("     AND UR.Active = ? " & vbCrLf)
                db.AddParameter("@Active", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            ElseIf Not showActive AndAlso showInactive Then
                sb.Append(" AND UR.Active = ? " & vbCrLf)
                db.AddParameter("@Active", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If
            sb.Append("ORDER BY UR.Resource " & vbCrLf)
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
        Public Shared Function IsReportAdministrator(ByVal userId As String, ByVal campusId As String) As Boolean

            Dim ds As DataSet
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append(" SELECT UR.UserId" + vbCrLf)
            sb.Append("FROM     syUsersRolesCampGrps UR, syRoles R,syCmpGrpCmps CG,sySysRoles SR" + vbCrLf)
            sb.Append(" WHERE    UR.RoleId=R.RoleId and R.SysRoleId=SR.SysRoleId ")
            sb.Append(" AND (SR.Descrip='Report Administrator' OR SR.Descrip ='System Administrator' ) AND CG.CampGrpId=UR.CampGrpId and CG.CampusId='" + campusId + "'" + vbCrLf)
            If Not String.IsNullOrEmpty(userId) Then
                'If Not userId Is Nothing AndAlso userId <> "" Then
                sb.Append("         AND UR.UserId = ? " + vbCrLf)
                db.AddParameter("@userId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            ds = db.RunParamSQLDataSet(sb.ToString)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return True
            End If

            Return False

        End Function
        ''' <summary>
        ''' Returns a list of reports that are valid for the given user, moduleid and entityid
        ''' UserId is mandatory while the others are optional
        ''' 10/27/06 - BEN - Add support for all Campus Group
        ''' </summary>
        ''' <param name="userId"></param>
        ''' <param name="moduleId"></param>
        ''' <param name="entityId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAllForUser(ByVal userId As String, ByVal campusId As String, ByVal moduleId As String, ByVal entityId As String) As DataSet
            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     UR.ResourceId as ReportId, " & vbCrLf)
            sb.Append("     UR.Resource as ReportName, " & vbCrLf)
            sb.Append("     UR.IsPublic, " & vbCrLf)
            sb.Append("     UR.Active " & vbCrLf)
            sb.Append("FROM syUserResources UR " & vbCrLf)
            If Not moduleId Is Nothing AndAlso moduleId <> "" Then
                sb.Append("     ,syAdvantageResourceRelations R, syResources E, syResources M " + vbCrLf)
            End If
            sb.Append("WHERE   UR.ResourceTypeId = 9 " & vbCrLf) ' Only get adhoc reports
            sb.Append("     AND UR.Active = 1 " & vbCrLf)
            sb.Append("     AND (UR.IsPublic = 1 OR UR.ModUser = ?) " & vbCrLf)
            db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            ' filter on the campusid
            If Not campusId Is Nothing AndAlso campusId <> "" Then
                sb.Append("     AND UR.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId = ?)" + vbCrLf)
                db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            ' add more tables if ModuleId has been passed
            If Not moduleId Is Nothing AndAlso moduleId <> "" Then
                sb.Append("        and R.ResourceId = E.ResourceId and " + vbCrLf)
                sb.Append("        R.RelatedResourceId = M.ResourceId and " + vbCrLf)
                sb.Append("        E.ResourceTypeId=8 and " + vbCrLf)
                sb.Append("        M.ResourceTypeId=1 and " + vbCrLf)
                sb.Append("	 	   E.ResourceId = UR.EntityId " + vbCrLf)
            End If
            If Not moduleId Is Nothing AndAlso moduleId <> "" Then
                sb.Append("     AND M.ResourceId = ? " & vbCrLf)
                db.AddParameter("@ModuleId", moduleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If Not entityId Is Nothing AndAlso entityId <> "" Then
                sb.Append("     AND UR.EntityId = ? " & vbCrLf)
                db.AddParameter("@EntityId", entityId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            sb.Append("ORDER BY UR.Resource " & vbCrLf)
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieve an AdHocRptInfo object from the database
        ''' </summary>
        ''' <param name="Id">ReportId / ResourceId in syResources</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal id As String) As AdHocRptInfo
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     UR.ResourceId as ReportId, " & vbCrLf)
            sb.Append("     UR.Resource as ReportName, " & vbCrLf)
            sb.Append("     UR.EntityId as ReportType, " & vbCrLf)
            sb.Append("     (select Resource from syResources where ResourceId=UR.EntityId) as ReportTypeDescrip, " + vbCrLf)
            sb.Append("     UR.CampGrpId as CampGroupId, " & vbCrLf)
            sb.Append("     (select CampGrpDescrip from syCampGrps C where C.CampGrpId=UR.CampGrpid) as CampGrpDescrip, " + vbCrLf)
            sb.Append("     UR.Options, " & vbCrLf)
            sb.Append("     UR.IsPublic, " & vbCrLf)
            sb.Append("     UR.ModUser, " & vbCrLf)
            sb.Append("     UR.ModDate, " & vbCrLf)
            sb.Append("     UR.Active, " & vbCrLf)
            sb.Append("    (SELECT sy.FullName FROM syUsers sy WHERE UserId = UR.CreatedBy) AS CreatedBy, ISNULL(UR.UseLeftJoin,0) AS  UseLeftJoin " & vbCrLf)
            sb.Append("FROM     syUserResources UR " + vbCrLf)
            sb.Append("WHERE   UR.ResourceId = ? ")
            db.AddParameter("@Id", id, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            Try
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                Dim info As New AdHocRptInfo
                If dr.Read() Then
                    '   set properties with data from DataReader
                    info.ReportId = id
                    info.IsInDB = True
                    If Not (dr("ReportName") Is DBNull.Value) Then info.ReportName = dr("ReportName")
                    If Not (dr("ReportType") Is DBNull.Value) Then info.ReportType = CType(dr("ReportType"), Integer)
                    If Not (dr("ReportTypeDescrip") Is DBNull.Value) Then info.ReportTypeDescrip = dr("ReportTypeDescrip")
                    If Not (dr("CampGroupId") Is DBNull.Value) Then info.CampGrpId = CType(dr("CampGroupId"), Guid).ToString()
                    If Not (dr("CampGrpDescrip") Is DBNull.Value) Then info.CampGrpDescrip = dr("CampGrpDescrip")
                    If Not (dr("Options") Is DBNull.Value) Then info.Options = CType(dr("Options"), Integer)
                    If Not (dr("IsPublic") Is DBNull.Value) Then info.IsPublic = CType(dr("IsPublic"), Boolean)
                    If Not (dr("ModUser") Is DBNull.Value) Then info.ModUser = dr("ModUser").ToString()
                    If Not (dr("ModDate") Is DBNull.Value) Then info.ModDate = dr("ModDate")
                    If Not (dr("Active") Is DBNull.Value) Then info.Active = CType(dr("Active"), Boolean)
                    If Not (dr("CreatedBy") Is DBNull.Value) Then info.CreatedBy = CType(dr("CreatedBy"), String)
                    If Not (dr("UseLeftJoin") Is DBNull.Value) Then info.UseLeftJoin = CType(dr("UseLeftJoin"), String)

                    ' retrieve the permissions for this report
                    info.Permissions = GetPermissions(id)

                    ' retrieve the fields for this report
                    info.Fields = GetReportFields(id)
                End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                Return info
            Catch ex As Exception
            End Try
            Return Nothing
        End Function

        ''' <summary>
        ''' Update a AdHocRptInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Update(ByVal info As AdHocRptInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("UPDATE syUserResources " & vbCrLf)
                sb.Append("SET " & vbCrLf)
                sb.Append(" Resource = ?, " & vbCrLf)
                sb.Append(" CampGrpId = ?, " & vbCrLf)
                'sb.Append(" ResourceTypeId = ?, " + vbCrLf)
                sb.Append(" EntityId = ?, " + vbCrLf)
                sb.Append(" Options = ?, " + vbCrLf)
                sb.Append(" IsPublic = ?, " + vbCrLf)
                sb.Append(" ModUser = ?, ModDate = ?, " & vbCrLf)
                sb.Append(" Active = ? " + vbCrLf)
                sb.Append(" , UseLeftJoin = ? " + vbCrLf)
                sb.Append("WHERE ResourceId = ? ")

                '   add parameters values to the query            
                db.AddParameter("@Resource", info.ReportName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.CampGrpId Is Nothing Or info.CampGrpId = Guid.Empty.ToString Then
                    db.AddParameter("@CampGroupId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CampGroupId", info.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@EntityId", info.ReportType, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@Options", info.Options, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsPublic", info.IsPublic, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                If info.ModUser Is Nothing Or info.ModUser = Guid.Empty.ToString Then
                    db.AddParameter("@ModUser", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModUser", info.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@UseLeftJoin", info.UseLeftJoin, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@ResourceId", info.ReportId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                ' run the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)

                ' add all the permissions
                DeleteAllPermissions(info.ReportId) ' start clean, delete all the permissions
                If Not info.Permissions Is Nothing Then
                    For Each permissionInfo As AdHocPermissionInfo In info.Permissions
                        permissionInfo.ReportId = info.ReportId ' Need to set the reportId
                        AddPermission(permissionInfo)
                    Next
                End If

                ' add all the fields
                DeleteAllFields(info.ReportId) ' start clean, delete all the fields
                If Not info.Fields Is Nothing Then
                    For Each fldInfo As AdHocFieldInfo In info.Fields
                        fldInfo.ReportId = info.ReportId
                        AddField(fldInfo)
                    Next
                End If

                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Persists a new AdHocRptInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As AdHocRptInfo, ByVal user As String) As Boolean
            '   Connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("INSERT syUserResources (Resource, CampGrpId, ResourceTypeId, EntityId, Options, IsPublic, ModDate, ModUser, Active, CreatedBy,UseLeftJoin) " + vbCrLf)
                sb.Append(" VALUES (?,?,?,?,?,?,?,?,?,?,?) " + vbCrLf)
                sb.Append("SELECT MAX(ResourceId) from syUserResources")

                '   add parameters values to the query
                'db.AddParameter("@ResourceId", info.ReportId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Resource", info.ReportName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.CampGrpId Is Nothing Or info.CampGrpId = Guid.Empty.ToString Then
                    db.AddParameter("@CampGrpId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CampGrpId", info.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@ResourceTypeId", 9, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@EntityId", info.ReportType, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@Options", info.Options, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsPublic", info.IsPublic, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If info.ModUser Is Nothing Or info.ModUser = Guid.Empty.ToString Then
                    db.AddParameter("@ModUser", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModUser", info.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                ' this store in new report the original user that create the report......
                If info.ModUser Is Nothing Or info.ModUser = Guid.Empty.ToString Then
                    db.AddParameter("@CreatedBy", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CreatedBy", info.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@UseLeftJoin", info.UseLeftJoin, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                '   execute the query, the result is the new ReportId created by SQL Server
                Dim id As Integer = db.RunParamSQLScalar(sb.ToString)
                info.ReportId = id ' Set the Id so the caller has access to it

                db.CloseConnection()

                ' add all the permissions
                If Not info.Permissions Is Nothing Then
                    For Each permissionInfo As AdHocPermissionInfo In info.Permissions
                        permissionInfo.ReportId = id ' Need to set the reportId
                        AddPermission(permissionInfo)
                    Next
                End If

                ' add all the fields
                If Not info.Fields Is Nothing Then
                    For Each fldInfo As AdHocFieldInfo In info.Fields
                        fldInfo.ReportId = id ' Need to set the reportId
                        AddField(fldInfo)
                    Next
                End If

                '   return without errors
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Delete report for the given ReportId
        ''' </summary>
        ''' <param name="ReportId"></param>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal reportId As String, ByVal user As String) As String
            ' Delete all saved params
            RptParamsDB.DeleteByReportId(reportId)
            ' delete report fields, don't know if the db has a constrain that 
            DeleteAllFields(reportId)
            ' delete the permissions associated with this report
            DeleteAllPermissions(reportId)
            ' delete the report
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("DELETE syUserResources " & vbCrLf)
                sb.Append("WHERE ResourceId = ? " & vbCrLf)
                sb.Append("SELECT COUNT(*) FROM syUserResources " & vbCrLf)
                sb.Append("WHERE ResourceId = ? " & vbCrLf)
                db.AddParameter("@ResourceId", reportId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ResourceId", reportId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Dim rowCount As Integer
                '   execute the query
                rowCount = db.RunParamSQLScalar(sb.ToString())
                '   If the row was not deleted then there was a concurrency problem
                If rowCount = 0 Then
                    Return ""   ' return without errors
                Else
                    Return DALExceptions.BuildConcurrencyExceptionMessage()
                End If
                'Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return ""
        End Function
#End Region

#Region "Fields Methods"
        ''' <summary>
        ''' Returns the list of fields defined for the given report.
        ''' This is used in GetInfo.
        ''' </summary>
        ''' <param name="reportId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetReportFields(ByVal reportId As String) As AdHocFieldInfo()
            ' check for invalid ReportId passed
            If reportId Is Nothing Or reportId = "" Then
                Return Nothing
            End If

            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append(ReportsDB.GetReportFieldsSql(reportId))
            sb.Append("ORDER BY ColOrder ")
            '   Execute the query
            Try
                Dim dt As DataTable = db.RunParamSQLDataSet(sb.ToString).Tables(0)
                ' check if there are any fields defined for this report
                Dim count As Integer = dt.Rows.Count
                If count = 0 Then
                    Return Nothing
                End If

                ' iterate through the resulting datatable and populate the values 
                Dim info(count - 1) As AdHocFieldInfo
                For i As Integer = 0 To count - 1
                    info(i) = New AdHocFieldInfo()
                    Dim dr As DataRow = dt.Rows(i)

                    info(i).IsInDB = True
                    info(i).AdHocFieldId = dr("ParamId").ToString()
                    If Not (dr("ReportId") Is DBNull.Value) Then info(i).ReportId = dr("ReportId")
                    If Not (dr("TblFldsId") Is DBNull.Value) Then info(i).TblFldsId = dr("TblFldsId")
                    If Not (dr("SDFId") Is DBNull.Value) Then info(i).SDFId = dr("SDFId").ToString()
                    If Not (dr("TblId") Is DBNull.Value) Then info(i).TblId = CType(dr("TblId"), Integer)
                    If Not (dr("TableName") Is DBNull.Value) Then info(i).TableName = dr("TableName")
                    If Not (dr("FldId") Is DBNull.Value) Then info(i).FldId = CType(dr("FldId"), Integer)
                    If Not (dr("FieldName") Is DBNull.Value) Then info(i).FieldName = dr("FieldName")
                    If Not (dr("ColOrder") Is DBNull.Value) Then info(i).ColOrder = CType(dr("ColOrder"), Integer)
                    If Not (dr("Header") Is DBNull.Value) Then info(i).Header = dr("Header")
                    If Not (dr("Visible") Is DBNull.Value) Then info(i).Visible = CType(dr("Visible"), Boolean)
                    If Not (dr("FormatString") Is DBNull.Value) Then info(i).FormatString = dr("FormatString")
                    If Not (dr("Width") Is DBNull.Value) Then info(i).Width = CType(dr("Width"), Integer)
                    If Not (dr("ShowInGroupBy") Is DBNull.Value) Then info(i).ShowInGroupBy = CType(dr("ShowInGroupBy"), Boolean)
                    If Not (dr("ShowInSummary") Is DBNull.Value) Then info(i).ShowInSummary = CType(dr("ShowInSummary"), Boolean)
                    If Not (dr("SummaryType") Is DBNull.Value) Then info(i).SummaryType = CType(dr("SummaryType"), Integer)
                    If Not (dr("GroupBy") Is DBNull.Value) Then info(i).GroupBy = CType(dr("GroupBy"), Boolean)
                    If Not (dr("SortBy") Is DBNull.Value) Then info(i).SortBy = CType(dr("SortBy"), Boolean)
                    If Not (dr("IsParam") Is DBNull.Value) Then info(i).IsParam = CType(dr("IsParam"), Boolean)
                    If Not (dr("IsRequired") Is DBNull.Value) Then info(i).IsRequired = CType(dr("IsRequired"), Boolean)
                    If Not (dr("FldTypeId") Is DBNull.Value) Then info(i).FldTypeId = CType(dr("FldTypeid"), Integer)
                    If Not (dr("FldType") Is DBNull.Value) Then info(i).FieldType = dr("FldType")
                    If Not (dr("FKColDescrip") Is DBNull.Value) Then info(i).FKColDescrip = dr("FKColDescrip")
                    If Not (dr("CalculationSQL") Is DBNull.Value) Then info(i).CalculationSql = dr("CalculationSQL")
                Next

                Return info ' return the array of fields

            Catch ex As Exception
            End Try
            Return Nothing
        End Function
        Public Shared Function GetReportFieldsSdf(ByVal reportId As String) As AdHocFieldInfo()
            ' check for invalid ReportId passed
            If reportId Is Nothing Or reportId = "" Then
                Return Nothing
            End If

            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append(ReportsDB.GetReportFieldsSql(reportId))
            sb.Append("ORDER BY ColOrder ")
            '   Execute the query
            Try
                Dim dt As DataTable = db.RunParamSQLDataSet(sb.ToString).Tables(0)
                ' check if there are any fields defined for this report
                Dim count As Integer = dt.Rows.Count
                If count = 0 Then
                    Return Nothing
                End If

                ' iterate through the resulting datatable and populate the values 
                Dim info(count - 1) As AdHocFieldInfo
                For i As Integer = 0 To count - 1
                    info(i) = New AdHocFieldInfo()
                    Dim dr As DataRow = dt.Rows(i)

                    info(i).IsInDB = True
                    info(i).AdHocFieldId = dr("ParamId").ToString()
                    If Not (dr("ReportId") Is DBNull.Value) Then info(i).ReportId = dr("ReportId")
                    If Not (dr("TblFldsId") Is DBNull.Value) Then info(i).TblFldsId = CType(dr("TblFldsId"), Integer)
                    If Not (dr("SDFId") Is DBNull.Value) Then
                        info(i).SDFId = dr("SDFId").ToString()
                        info(i).TblFldsId = dr("SDFId").ToString()
                    End If

                    If Not (dr("TblId") Is DBNull.Value) Then info(i).TblId = CType(dr("TblId"), Integer)
                    If Not (dr("TableName") Is DBNull.Value) Then info(i).TableName = dr("TableName")
                    If Not (dr("FldId") Is DBNull.Value) Then info(i).FldId = CType(dr("FldId"), Integer)
                    If Not (dr("FieldName") Is DBNull.Value) Then info(i).FieldName = dr("FieldName")
                    If Not (dr("ColOrder") Is DBNull.Value) Then info(i).ColOrder = CType(dr("ColOrder"), Integer)
                    If Not (dr("Header") Is DBNull.Value) Then info(i).Header = dr("Header")
                    If Not (dr("Visible") Is DBNull.Value) Then info(i).Visible = CType(dr("Visible"), Boolean)
                    If Not (dr("FormatString") Is DBNull.Value) Then info(i).FormatString = dr("FormatString")
                    If Not (dr("Width") Is DBNull.Value) Then info(i).Width = CType(dr("Width"), Integer)
                    If Not (dr("ShowInGroupBy") Is DBNull.Value) Then info(i).ShowInGroupBy = CType(dr("ShowInGroupBy"), Boolean)
                    If Not (dr("ShowInSummary") Is DBNull.Value) Then info(i).ShowInSummary = CType(dr("ShowInSummary"), Boolean)
                    If Not (dr("SummaryType") Is DBNull.Value) Then info(i).SummaryType = CType(dr("SummaryType"), Integer)
                    If Not (dr("GroupBy") Is DBNull.Value) Then info(i).GroupBy = CType(dr("GroupBy"), Boolean)
                    If Not (dr("SortBy") Is DBNull.Value) Then info(i).SortBy = CType(dr("SortBy"), Boolean)
                    If Not (dr("IsParam") Is DBNull.Value) Then info(i).IsParam = CType(dr("IsParam"), Boolean)
                    If Not (dr("IsRequired") Is DBNull.Value) Then info(i).IsRequired = CType(dr("IsRequired"), Boolean)
                    If Not (dr("FldTypeId") Is DBNull.Value) Then info(i).FldTypeId = CType(dr("FldTypeid"), Integer)
                    If Not (dr("FldType") Is DBNull.Value) Then info(i).FieldType = dr("FldType")
                    If Not (dr("FKColDescrip") Is DBNull.Value) Then info(i).FKColDescrip = dr("FKColDescrip")
                Next

                Return info ' return the array of fields

            Catch ex As Exception
            End Try
            Return Nothing
        End Function

        ''' <summary>
        ''' Delete all the fields for the given ReportId
        ''' </summary>
        ''' <param name="ReportId"></param>
        ''' <remarks></remarks>
        Public Shared Sub DeleteAllFields(ByVal reportId As String)
            Dim sql As String = String.Format("DELETE syRptAdHocFields WHERE ResourceId = {0}", reportId)
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.RunParamSQLExecuteNoneQuery(sql)
            db.CloseConnection()
        End Sub

        ''' <summary>
        ''' Adds an AdHocFieldInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function AddField(ByVal info As AdHocFieldInfo) As Boolean
            '   Connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("INSERT syRptAdHocFields (AdHocFieldId, ResourceId, TblFldsId, ColOrder, Header, Visible," + vbCrLf)
                sb.Append("           FormatString, Width, ShowInGroupBy, ShowInSummary, SummaryType, GroupBy, SortBy, IsParam, IsRequired, SDFId) " + vbCrLf)
                sb.Append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")

                '   add parameters values to the query
                db.AddParameter("@AdHocFieldId", info.AdHocFieldId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ResourceId", info.ReportId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.TblFldsId Is Nothing Or info.TblFldsId = "" Then
                    db.AddParameter("@TblFldsid", DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                Else
                    db.AddParameter("@TblFldsid", info.TblFldsId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                End If
                db.AddParameter("@ColOrder", info.ColOrder, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                If info.Header Is Nothing Then
                    db.AddParameter("@Header", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@Header", info.Header, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@Visible", info.Visible, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                If info.FormatString Is Nothing Then
                    db.AddParameter("@FormatString", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@FormatString", info.FormatString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@Width", info.Width, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ShowInGroupBy", info.ShowInGroupBy, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                ' need to modify table to add this one
                ' db.AddParameter("@ShowInOrderBy", info.ShowInOrderBy, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@ShowInSummary", info.ShowInSummary, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@SummaryType", info.SummaryType, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@GroupBy", info.GroupBy, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@SortBy", info.SortBy, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@IsParam", info.IsParam, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@IsRequired", info.IsRequired, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                If info.SDFId Is Nothing Or info.SDFId = "" Then
                    db.AddParameter("@SDFId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@SDFId", info.SDFId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                '   return without errors
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Gets all the categories for the supplied EntityId.
        ''' EntityId is really a resourceId defined in syResources
        ''' </summary>
        ''' <param name="entityId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetCategories(ByVal entityId As String) As DataSet
            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT Distinct ")
            sb.Append(" CategoryId, " & vbCrLf)
            sb.Append(" Descrip " & vbCrLf)
            sb.Append("FROM syFldCategories " & vbCrLf)
            If (Not String.IsNullOrEmpty(entityId)) Then
                sb.Append("WHERE EntityId = ? " + vbCrLf)
                db.AddParameter("@EntityId", entityId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            sb.Append("ORDER BY Descrip " & vbCrLf)
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Gets all the fields for a specified category
        ''' </summary>
        ''' <param name="CategoryId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetFields(ByVal categoryId As String) As DataSet
            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT Distinct ")
            sb.Append("     TF.TblFldsId,  " & vbCrLf)
            sb.Append("     F.FldName, " & vbCrLf)
            sb.Append("     FC.Caption, " & vbCrLf)
            sb.Append("     (convert(varchar(10),TF.TblFldsId) + ',' + " + vbCrLf)
            sb.Append("     FC.Caption + ',' + " + vbCrLf)
            sb.Append("     convert(varchar(10),F.FldTypeId) + ',' + " + vbCrLf)
            sb.Append("     convert(varchar(10),F.FldLen) + ',' +  " + vbCrLf)
            sb.Append("     isnull(F.Mask,'')) + ',syTblFlds' as FieldData " + vbCrLf)
            sb.Append("FROM syFields F, syTblFlds TF, syFldCaptions FC " & vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     TF.FldId=F.FldId and TF.FldId=FC.FldId " + vbCrLf)
            If Not String.IsNullOrEmpty(categoryId) Then
                sb.Append("     AND TF.CategoryId = ? " + vbCrLf)
                db.AddParameter("@CategoryId", categoryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            sb.Append("ORDER BY FC.Caption " & vbCrLf)
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Gets all the User Defined Fields for the given category
        ''' Values for CategoryId are 1=Student, 2=Lender, 3=Employees, 4=Employers, 5=Lenders
        ''' </summary>
        ''' <param name="CategoryId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetUdfFields(ByVal categoryId As String) As DataSet
            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT Distinct ")
            sb.Append("     S.SDFId as TblFldsId,  " & vbCrLf)
            sb.Append("     S.SDFDescrip as FldName, " & vbCrLf)
            sb.Append("     S.SDFDescrip as Caption, " & vbCrLf)
            sb.Append("     (convert(varchar(50),S.SDFId) + ',' + " + vbCrLf)
            sb.Append("     S.SDFDescrip + ',' + " + vbCrLf)
            sb.Append("     convert(varchar(10),S.DTypeId) + ',' + " + vbCrLf)
            sb.Append("     convert(varchar(10),S.Len) + ',' + " + vbCrLf)
            sb.Append("     '' + ',sySDF,') as FieldData " + vbCrLf)
            sb.Append("FROM sySDF S " & vbCrLf)
            sb.Append(" INNER Join syResourceSdf R On S.SDFId = R.sdfID " & vbCrLf)
            sb.Append(" INNER Join syResources E ON R.ResourceId = E.ResourceID " & vbCrLf)
            sb.Append(" INNER Join syTblFlds TF On E.TblFldsId = TF.TblFldsId " & vbCrLf)
            sb.Append(" INNER Join syTables T ON TF.TblId = T.TblId " & vbCrLf)
            sb.Append(" INNER Join syFields F On TF.FldId = F.FldId " & vbCrLf)
            sb.Append(" INNER Join( " & vbCrLf)
            sb.Append(" Select  ResourceId " & vbCrLf)
            sb.Append("       ,RelatedResourceId " & vbCrLf)
            sb.Append(" From syAdvantageResourceRelations " & vbCrLf)
            Select Case categoryId
                Case 1 ' Students / 394
                    sb.AppendFormat("   WHERE     ResourceId = {0} {1}", "394 ", vbCrLf)
                Case 2 ' Leads = 395 in syResources
                    sb.AppendFormat("   WHERE     ResourceId = {0} {1}", "395 ", vbCrLf)
                Case 3 ' Employees = 396 in syResources
                    sb.AppendFormat("   WHERE     ResourceId = {0} {1}", "396 ", vbCrLf)
                Case 4 ' Employers = 397 in syResources
                    sb.AppendFormat("   WHERE     ResourceId = {0} {1}", "397 ", vbCrLf)
                Case 5 ' Lenders = 434 in syResources
                    sb.AppendFormat("   WHERE     ResourceId = {0} {1}", "434 ", vbCrLf)
            End Select
            sb.Append(" ) AR On AR.RelatedResourceId = E.ResourceID " & vbCrLf)
            'sb.Append("     sySDF S, syResourceSDF R, syAdvantageResourceRelations AR, syResources E " + vbCrLf)
            'sb.Append("Where " + vbCrLf)
            'sb.Append("     S.SDFId = R.sdfID " + vbCrLf)
            'sb.Append("     And R.ModuleId = AR.RelatedResourceId " + vbCrLf)
            'sb.Append("     And AR.ResourceId = E.ResourceId And E.ResourceTypeId = 8 " + vbCrLf)
            'Select Case CategoryId
            '    Case 1 ' Students / 394
            '        sb.AppendFormat("   And E.ResourceId = {0} {1}", 394, vbCrLf)
            '    Case 2 ' Leads = 395 in syResources
            '        sb.AppendFormat("   And E.ResourceId = {0} {1}", 395, vbCrLf)
            '    Case 3 ' Employees = 396 in syResources
            '        sb.AppendFormat("   And E.ResourceId = {0} {1}", 396, vbCrLf)
            '    Case 4 ' Employers = 397 in syResources
            '        sb.AppendFormat("   And E.ResourceId = {0} {1}", 397, vbCrLf)
            '    Case 5 ' Lenders = 434 in syResources
            '        sb.AppendFormat("   And E.ResourceId = {0} {1}", 434, vbCrLf)
            'End Select
            sb.Append("ORDER BY S.SDFDescrip " & vbCrLf)
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
#End Region

#Region "Params"
#End Region

#Region "Permissions"
        ''' <summary>
        ''' Returns an array of permission objects for the given report
        ''' </summary>
        ''' <param name="reportId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetPermissions(ByVal reportId) As AdHocPermissionInfo()
            ' check for invalid ReportId passed
            If reportId Is Nothing Or reportId = "" Then
                Return Nothing
            End If

            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Try
                Dim sb As New StringBuilder
                sb.Append("Select " + vbCrLf)
                sb.Append("     UP.ResPermissionId, " + vbCrLf)
                sb.Append("     UP.UserResourceId As ReportId, " + vbCrLf)
                sb.Append("     M.ResourceId As ModuleId, " + vbCrLf)
                sb.Append("(Select R.Resource FROM syResources R WHERE R.ResourceId = M.ResourceId) As ModuleName, " + vbCrLf)
                sb.Append("     ISNULL(UP.Permission, 0) As Permission " + vbCrLf)
                sb.Append("FROM " + vbCrLf)
                sb.Append("     syResources M " + vbCrLf)
                sb.Append("      LEFT JOIN syUserResPermissions UP  On  M.ResourceId = UP.ResourceId " + vbCrLf)
                sb.Append("WHERE " + vbCrLf)
                sb.Append("      M.ResourceTypeId=1 " + vbCrLf)
                sb.Append("     And UP.ResourceId = ? ")
                db.AddParameter("@ReportId", reportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                sb.Append("ORDER BY M.ResourceId ")

                Dim dt As DataTable = db.RunParamSQLDataSet(sb.ToString).Tables(0)
                ' check if there are any fields defined for this report
                Dim count As Integer = dt.Rows.Count
                If count = 0 Then
                    Return Nothing
                End If

                ' iterate through the resulting datatable and populate the values  
                Dim info(count - 1) As AdHocPermissionInfo
                For i As Integer = 0 To count - 1
                    info(i) = New AdHocPermissionInfo()
                    Dim dr As DataRow = dt.Rows(i)

                    info(i).IsInDB = True
                    If Not (dr("ResPermissionId") Is DBNull.Value) Then info(i).ResPermissionId = CType(dr("ResPermissionId"), Guid).ToString()
                    If Not (dr("ReportId") Is DBNull.Value) Then info(i).ReportId = dr("ReportId")
                    If Not (dr("ModuleId") Is DBNull.Value) Then info(i).ModuleId = dr("ModuleId")
                    If Not (dr("ModuleName") Is DBNull.Value) Then info(i).ModuleName = dr("ModuleName")
                    If Not (dr("Permission") Is DBNull.Value) Then info(i).Permission = CType(dr("Permission"), Boolean)
                Next

                Return info ' return the array of fields
            Catch ex As Exception
            End Try
            Return Nothing
        End Function

        ''' <summary>
        ''' Returns the permissions for the specified report
        ''' </summary>
        ''' <param name="ReportId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetPermissionsDS(ByVal userId As String, ByVal reportId As String) As DataSet
            ' if reportid is not passed
            If String.IsNullOrEmpty(reportId) Then
                Dim ds As DataSet = RolesDB.GetValidModulesForUser(userId)
                ds.Tables(0).Columns.Add("Permission", Type.GetType("System.Int16"))
                For Each dr As DataRow In ds.Tables(0).Rows
                    dr("Permission") = 0
                Next
                Return ds
            End If

            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("Select " + vbCrLf)
            sb.Append("     DISTINCT M.ResourceId, " + vbCrLf)
            sb.Append("     M.ResourceId As ModuleId, " + vbCrLf)
            sb.Append("     (Select R.Resource FROM syResources R where R.ResourceId = M.ResourceId) As ModuleName, " + vbCrLf)
            sb.Append("     ISNULL(UP.Permission,0) As Permission " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     syRolesModules RM " + vbCrLf)
            sb.Append("     INNER JOIN syRoles R On RM.RoleId=R.RoleId " + vbCrLf)
            sb.Append("     INNER JOIN syUsersRolesCampGrps UR On UR.RoleId=R.RoleId " + vbCrLf)
            sb.Append("     INNER JOIN syResources M On M.ResourceId = RM.ModuleId " + vbCrLf)
            sb.Append("     LEFT JOIN syUserResPermissions UP On M.ResourceId = UP.ResourceId " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     M.ResourceTypeId=1 " + vbCrLf)
            If Not userId Is Nothing AndAlso userId <> "" Then
                sb.Append("         And UR.UserId = ? " + vbCrLf)
                db.AddParameter("@userId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If Not reportId Is Nothing AndAlso reportId <> "" Then
                sb.Append("     And UP.UserResourceId = ? ")
                db.AddParameter("@ReportId", reportId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            sb.Append("ORDER BY M.ResourceId ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
        Public Shared Function AddPermission(ByVal info As AdHocPermissionInfo) As Boolean
            '   Connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("INSERT syUserResPermissions (ResPermissionId, UserResourceId, ResourceId, Permission) " + vbCrLf)
                sb.Append(" VALUES (?,?,?,?) ")

                '   add parameters values to the query
                db.AddParameter("@ResPermissionId", info.ResPermissionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@UserResourceId", info.ReportId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ResourceId", info.ModuleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Permission", info.Permission, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                '   return without errors
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Deletes all the permissions associated with a report
        ''' </summary>
        ''' <param name="ReportId"></param>
        ''' <remarks></remarks>
        Public Shared Sub DeleteAllPermissions(ByVal reportId As String)
            Dim sql As String = String.Format("DELETE syUserResPermissions WHERE UserResourceId = {0}", reportId)
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.RunParamSQLExecuteNoneQuery(sql)
            db.CloseConnection()
        End Sub
#End Region

#Region "Viewing Reports Methods"
        ''' <summary>
        ''' The main entry point to getting the dataset for a report
        ''' Parma1 - info: Retrieved by calling AdHocRptFacade.GetReportInfo
        ''' Param2 - params: an array of paramters used to fill the where clause
        ''' </summary>
        ''' <param name="info"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetReportDataSet(ByVal info As AdHocRptInfo, Optional ByVal isSummary As Boolean = False) As DataSet
            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            Dim sql As String = _GetReportSql(db, info, isSummary)
            Dim ds As DataSet = db.RunParamSQLDataSet(sql)

            ' apply masks to ssn, phone and zip
            ApplyMasks(ds, sql)

            If info.ShowSummary Then
                db.ClearParameters()
                sql = _GetReportSql(db, info, True)
                Dim ds2 As DataSet = db.RunParamSQLDataSet(sql)
                Dim dt1 As DataTable = ds2.Tables(0).Copy
                dt1.TableName = "Summary1"
                ds.Tables.Add(dt1)

                Dim dt2 As DataTable = ds2.Tables(0).Copy
                dt2.TableName = "Summary2"
                ds.Tables.Add(dt2)
            End If

            ' do a fixup of the dataset by removing any column that started with a "!".
            ' The "!" is added to the sql query within GetColumnsSql to make the query
            ' run ok with order and compute statements.  Now that the query has executed,
            ' we no longer need those columns.
            For Each dt As DataTable In ds.Tables
                For i As Integer = dt.Columns.Count - 1 To 0 Step -1
                    Dim c As DataColumn = dt.Columns(i)
                    If c.ColumnName.IndexOf("!", StringComparison.Ordinal) >= 0 Then
                        dt.Columns.RemoveAt(i)
                    End If
                Next
            Next

            ' apply another fix to the dataset which happens if there is a "group by"
            ' and there are no records returned.  If this happens, we get only 1 table back
            ' representing the count.  This messes up the reporting, so we just want to detect
            ' this case and remove the table.
            If ds.Tables.Count = 1 AndAlso ds.Tables(0).Columns.Count = 1 AndAlso ds.Tables(0).Columns(0).ColumnName = "cnt" Then
                ds.Tables.RemoveAt(0)
            End If

            Return ds
        End Function
        Public Shared Function AddExportReportSummary(ByVal info As AdHocRptInfo, ByVal reportDs As DataSet) As DataSet
            Dim newDataSet As New DataSet
            For tblCnt As Integer = 0 To reportDs.Tables.Count - 1
                '' Function to Add summary (count, Average and Sum) and group row to the dataset.
                Dim dt As New DataTable
                Dim fieldcount As Integer = info.Fields.Length
                Dim repFieldInfo(fieldcount) As AdHocFieldInfo

                Dim dtNewRow As DataRow
                Dim dtBlankRow1 As DataRow
                Dim dtBlankRow2 As DataRow

                repFieldInfo = info.Fields

                '' Create array to save the fields and the summaries required for each of them
                Dim reportFld(fieldcount, 13) As String
                Dim reportSummary(fieldcount, 5) As String

                Dim groupCol As String = ""

                '' Add field and their respective summary required information
                For i As Integer = 0 To fieldcount - 1
                    reportFld(i, 0) = repFieldInfo(i).FieldName
                    reportFld(i, 1) = repFieldInfo(i).SummaryType And 1   ''count
                    reportFld(i, 2) = repFieldInfo(i).SummaryType And 2   ''Sum
                    reportFld(i, 3) = repFieldInfo(i).SummaryType And 4   ''Max
                    reportFld(i, 4) = repFieldInfo(i).SummaryType And 8   ''Min   
                    reportFld(i, 5) = repFieldInfo(i).SummaryType And 16  ''Average
                    reportFld(i, 6) = "0"
                    reportFld(i, 7) = "0"
                    reportFld(i, 8) = "0"
                    reportFld(i, 9) = "0"
                    reportFld(i, 10) = "0"
                    reportFld(i, 11) = repFieldInfo(i).GroupBy  ''  If group by true or false
                    reportFld(i, 12) = repFieldInfo(i).FieldType '' Field Datatype  
                    ' Save in the variable the column name to group the report 
                    If (repFieldInfo(i).GroupBy) Then
                        ''''' Code modified by kamalesh Ahuja on 28th Sept 2010 to resolve mantis issue id 19743
                        ''GroupCol = RepFieldInfo(i).FieldName
                        'If repFieldInfo(i).FKColDescrip <> ""  And repFieldInfo(i).SummaryType Then
                        '    groupCol = repFieldInfo(i).FKColDescrip
                        'Else
                        '    groupCol = repFieldInfo(i).FieldName
                        'End If

                        If reportDs.Tables(tblCnt).Columns.Contains(repFieldInfo(i).FKColDescrip)
                            groupCol = repFieldInfo(i).FKColDescrip
                        else
                            groupCol = repFieldInfo(i).FieldName
                        End If
                        '''''''''''''''''
                    End If

                    reportSummary(i, 0) = "0" '' Count
                    reportSummary(i, 1) = "0" '' Sum
                    reportSummary(i, 2) = "0" '' Max
                    reportSummary(i, 3) = "0" '' Min
                    reportSummary(i, 4) = "0" '' Average

                Next

                '' Add colums to the blank new datatable to add the records and summary rows
                For i As Integer = 0 To fieldcount - 1

                    If info.Fields(i).FKColDescrip <> "" Then

                        'NZ 11/6/2012
                        If info.Fields(i).FKColDescrip = "StateDescrip" AndAlso info.Fields(i).FieldName = "DrivLicStateID" Then
                            dt.Columns.Add("DL" & info.Fields(i).FKColDescrip, Type.GetType("System.String"))
                        Else
                            'B. Shanblatt 12/4/2012
                            'handle duplicate foreign database keys with the column name of Descrip
                            'fallout from US3454
                            If info.Fields(i).FKColDescrip = "Descrip" Or info.Fields(i).FKColDescrip = "FundSourceDescrip" Then
                                dt.Columns.Add(info.Fields(i).FieldName, Type.GetType("System.String"))
                                reportDs.Tables(tblCnt).Columns(i).ColumnName = dt.Columns(i).Caption
                                groupCol = info.Fields(i).FieldName
                            Else
                                dt.Columns.Add(info.Fields(i).FKColDescrip, Type.GetType("System.String"))
                                groupCol = info.Fields(i).FKColDescrip
                            End If
                        End If
                    Else

                        If (dt.Columns.Contains(info.Fields(i).FieldName)) Then
                            Dim duplicateName = True
                            Dim num = 1
                            Dim newName = info.Fields(i).FieldName & num
                            While (duplicateName)
                                If (Not dt.Columns.Contains(newName)) Then
                                    duplicateName = False
                                    dt.Columns.Add(newName, Type.GetType("System.String"))
                                Else
                                    num += 1
                                    newName = info.Fields(i).FieldName & num
                                End If

                            End While

                        Else
                            dt.Columns.Add(info.Fields(i).FieldName, Type.GetType("System.String"))
                        End If
                    End If
                    ''''''''''''
                Next

                '' Variable to save the group name in loop 
                Dim groupText As String = Nothing

                'Dim newDataSet As New DataSet

                Try

                    ' Variable to record the row-number that is being summarized
                    Dim rowcount As Integer = reportDs.Tables(tblCnt).Rows.Count + 1
                    Dim colDataType As String

                    For Each dRow As DataRow In reportDs.Tables(tblCnt).Rows  '' Loop for each datarow in the input dataset

                        rowcount -= 1  '' Update the rowcount

                        If groupCol = "" Then  '' Case to process the records if there is no grouping criteria

                            dt.ImportRow(dRow) '' Import the row in the new datatable

                            '' Loop for each column to save the summaries in the array
                            For j As Integer = 0 To fieldcount - 1

                                reportFld(j, 6) = If(dRow.Item(j).ToString() <> "", (CInt(reportFld(j, 6)) + 1), CInt(reportFld(j, 6)))                         '' Count

                                If reportFld(j, 2) > 0 Then
                                    reportFld(j, 7) = If(dRow.Item(j).ToString() <> "", (CDbl(reportFld(j, 7)) + CDbl(dRow.Item(j).ToString())), CDbl(reportFld(j, 7)))     '' Sum
                                End If

                                If CInt(reportFld(j, 3)) > 0 Then           '' Max
                                    colDataType = reportFld(j, 12)
                                    If colDataType = "Varchar" Then
                                        If (reportFld(j, 8) <> "" And reportFld(j, 8) <> "0") Then
                                            Dim res As Integer
                                            res = String.Compare(reportFld(j, 8), dRow.Item(j).ToString())
                                            If (res = -1) Then
                                                reportFld(j, 8) = dRow.Item(j).ToString()
                                            End If
                                        Else
                                            reportFld(j, 8) = dRow.Item(j).ToString()
                                        End If
                                    ElseIf colDataType = "Datetime" Then
                                        If (dRow.Item(j).ToString() <> Nothing) Then
                                            If (reportFld(j, 8) <> Nothing And reportFld(j, 8) <> "0") Then
                                                If (CDate(dRow.Item(j).ToString()) > CDate(reportFld(j, 8))) Then
                                                    reportFld(j, 8) = dRow.Item(j).ToString()
                                                End If
                                            Else
                                                reportFld(j, 8) = dRow.Item(j).ToString()
                                            End If
                                        End If
                                    ElseIf colDataType = "Decimal" Then
                                        If (dRow.Item(j).ToString() <> Nothing) Then
                                            If (reportFld(j, 8) <> Nothing And reportFld(j, 8) <> "0") Then
                                                If (CDec(dRow.Item(j).ToString()) > CDec(reportFld(j, 8))) Then
                                                    reportFld(j, 8) = dRow.Item(j).ToString()
                                                End If
                                            Else
                                                reportFld(j, 8) = dRow.Item(j).ToString()
                                            End If
                                        End If
                                    End If
                                End If

                                If CInt(reportFld(j, 4)) > 0 Then       '' Min
                                    colDataType = reportFld(j, 12)

                                    If colDataType = "Varchar" Then
                                        If (reportFld(j, 9) <> "" And reportFld(j, 9) <> "0") Then
                                            Dim res As Integer
                                            res = String.Compare(reportFld(j, 9), dRow.Item(j).ToString())
                                            If (res = 1) Then
                                                reportFld(j, 9) = dRow.Item(j).ToString()
                                            End If
                                        Else
                                            reportFld(j, 9) = dRow.Item(j).ToString()
                                        End If
                                    ElseIf colDataType = "Datetime" Then
                                        If (dRow.Item(j).ToString() <> Nothing) Then
                                            If (reportFld(j, 9) <> Nothing And reportFld(j, 9) <> "0") Then
                                                If (CDate(dRow.Item(j).ToString()) < CDate(reportFld(j, 9))) Then
                                                    reportFld(j, 9) = dRow.Item(j).ToString()
                                                End If
                                            Else
                                                reportFld(j, 9) = dRow.Item(j).ToString()
                                            End If
                                        End If
                                    ElseIf colDataType = "Decimal" Then
                                        If (dRow.Item(j).ToString() <> Nothing) Then
                                            If (reportFld(j, 9) <> Nothing And reportFld(j, 9) <> "0") Then
                                                If (CDec(dRow.Item(j).ToString()) < CDec(reportFld(j, 9))) Then
                                                    reportFld(j, 9) = dRow.Item(j).ToString()
                                                End If
                                            Else
                                                reportFld(j, 9) = dRow.Item(j).ToString()
                                            End If
                                        End If
                                    End If
                                End If

                                If reportFld(j, 5) > 0 Then
                                    reportFld(j, 10) = If(dRow.Item(j).ToString() <> "", (CDbl(reportFld(j, 10)) + CDbl(dRow.Item(j).ToString())), CDbl(reportFld(j, 10)))  '' Average 
                                End If
                            Next

                            '' If the last row is being processed Compute and add the summary row to the datatable
                            If rowcount = 1 Then


                                dtNewRow = dt.NewRow()
                                'dtBlankRow1 = dt.NewRow()
                                dtBlankRow2 = dt.NewRow()

                                For k As Integer = 0 To fieldcount - 1

                                    Dim strSummary As String = ""
                                    ' ReSharper disable UnusedVariable
                                    Dim teststr As String = reportFld(k, 10)
                                    ' ReSharper restore UnusedVariable
                                    If reportFld(k, 1) > 0 Then
                                        strSummary = IIf(reportFld(k, 6) <> "0" And reportFld(k, 6) <> Nothing, "Count = " + reportFld(k, 6) & Chr(10), "")  '' count
                                    End If
                                    reportSummary(k, 0) = If(reportFld(k, 6) <> "" And reportFld(k, 6) <> "0", (CInt(reportFld(k, 6)) + CInt(reportSummary(k, 0))), CInt(reportSummary(k, 0)))  '' Count
                                    strSummary = strSummary + IIf(reportFld(k, 7) <> "0" And reportFld(k, 7) <> Nothing, "Sum = " + reportFld(k, 7) & Chr(10), "")  ''Sum 
                                    reportSummary(k, 1) = If(reportFld(k, 7) <> "" And reportFld(k, 7) <> "0", (CDbl(reportFld(k, 7)) + CDbl(reportSummary(k, 1))), CDbl(reportSummary(k, 1)))  '' Sum 
                                    strSummary = strSummary + IIf(reportFld(k, 8) <> "0" And reportFld(k, 8) <> Nothing, "Max = " + reportFld(k, 8) & Chr(10), "")  ''Max 
                                    strSummary = strSummary + IIf(reportFld(k, 9) <> "0" And reportFld(k, 9) <> Nothing, "Min = " + reportFld(k, 9) & Chr(10), "")  '' Min

                                    strSummary = strSummary + IIf(reportFld(k, 10) <> "0" And reportFld(k, 10) <> Nothing, "Average = " + CStr(CDbl(reportFld(k, 10)) / CDbl(reportFld(k, 6))) & Chr(10), "")  '' Average
                                    reportSummary(k, 4) = If(reportFld(k, 10) <> "" And reportFld(k, 10) <> "0", (CDbl(reportFld(k, 10)) + CDbl(reportSummary(k, 4))), CDbl(reportSummary(k, 4)))  '' Average 
                                    dtNewRow(k) = strSummary

                                    '' Add Data to ReportSUMMARY Array

                                    If CInt(reportFld(k, 3)) > 0 Then           '' Max
                                        colDataType = reportFld(k, 12)
                                        If colDataType = "Varchar" Then
                                            If (reportSummary(k, 2) <> "" And reportSummary(k, 2) <> "0") Then
                                                Dim res As Integer
                                                res = String.Compare(reportSummary(k, 2), reportFld(k, 8))
                                                If (res = -1) Then
                                                    reportSummary(k, 2) = reportFld(k, 8)
                                                End If
                                            Else
                                                reportSummary(k, 2) = reportFld(k, 8)
                                            End If
                                        ElseIf colDataType = "Datetime" Then
                                            If (reportSummary(k, 2) <> Nothing) Then
                                                If (reportSummary(k, 2) <> Nothing And reportSummary(k, 2) <> "0") Then
                                                    If (CDate(reportFld(k, 8)) > CDate(reportSummary(k, 2))) Then
                                                        reportSummary(k, 2) = reportFld(k, 8)
                                                    End If
                                                Else
                                                    reportSummary(k, 2) = reportFld(k, 8)
                                                End If
                                            End If
                                        ElseIf colDataType = "Decimal" Then
                                            If (reportSummary(k, 2) <> Nothing) Then
                                                If (reportSummary(k, 2) <> Nothing And reportSummary(k, 2) <> "0") Then
                                                    If (CDec(reportFld(k, 8)) > CDec(reportSummary(k, 2))) Then
                                                        reportSummary(k, 2) = reportFld(k, 8)
                                                    End If
                                                Else
                                                    reportSummary(k, 2) = reportFld(k, 8)
                                                End If
                                            End If
                                        End If
                                    End If

                                    If CInt(reportFld(k, 4)) > 0 Then       '' Min
                                        colDataType = reportFld(k, 12)

                                        If colDataType = "Varchar" Then
                                            If (reportSummary(k, 3) <> "" And reportSummary(k, 3) <> "0") Then
                                                Dim res As Integer
                                                res = String.Compare(reportSummary(k, 3), reportFld(k, 9))
                                                If (res = 1) Then
                                                    reportSummary(k, 3) = reportFld(k, 9)
                                                End If
                                            Else
                                                reportSummary(k, 3) = reportFld(k, 9)
                                            End If
                                        ElseIf colDataType = "Datetime" Then
                                            If (reportSummary(k, 3) <> Nothing) Then
                                                If (reportSummary(k, 3) <> Nothing And reportSummary(k, 3) <> "0") Then
                                                    If (CDate(reportFld(k, 9)) < CDate(reportSummary(k, 3))) Then
                                                        reportSummary(k, 3) = reportFld(k, 9)
                                                    End If
                                                Else
                                                    reportSummary(k, 3) = reportFld(k, 9)
                                                End If
                                            End If
                                        ElseIf colDataType = "Decimal" Then
                                            If (reportSummary(k, 3) <> Nothing) Then
                                                If (reportSummary(k, 3) <> Nothing And reportSummary(k, 3) <> "0") Then
                                                    If (CDec(reportFld(k, 9)) < CDec(reportSummary(k, 3))) Then
                                                        reportSummary(k, 3) = reportFld(k, 9)
                                                    End If
                                                Else
                                                    reportSummary(k, 3) = reportFld(k, 9)
                                                End If
                                            End If
                                        End If
                                    End If
                                    '' Add Data to ReportSUMMARY Array
                                    reportFld(k, 6) = "0"
                                    reportFld(k, 7) = "0"
                                    reportFld(k, 8) = "0"
                                    reportFld(k, 9) = "0"
                                    reportFld(k, 10) = "0"

                                Next
                                'DT.Rows.Add(DtBlankRow1) '' Adding Blank Datarow 
                                '' Add New Code For Add Summary Details as per criteria
                                If info.ShowGroupBySummary = True Then
                                    dt.Rows.Add(dtNewRow) '' Adding Summary Datarow 
                                End If
                                dt.Rows.Add(dtBlankRow2) '' Adding Blank Datarow 
                                '' Add New Code For Add Summary Details as per criteria
                            End If

                        Else '' Case to process the records if there is a grouping criteria in the report

                            If groupText Is Nothing Then
                                ''''' Code modified by kamalesh Ahuja on 30th Sept 2010 to resolve mantis issue id 19743
                                ''GroupText = IIf(IsDBNull(DRow.Item(GroupCol)), "", DRow.Item(GroupCol))
                                groupText = IIf(IsDBNull(dRow.Item(groupCol)), "", dRow.Item(groupCol).ToString())
                                '''''''''''''
                            End If

                            '' If the current group is not he same as the group name for previous Add a summary Row
                            ''''' Code modified by kamalesh Ahuja on 30th Sept 2010 to resolve mantis issue id 19743
                            ''If (GroupText <> IIf(IsDBNull(DRow.Item(GroupCol)), "", DRow.Item(GroupCol))) Then
                            If (groupText <> IIf(IsDBNull(dRow.Item(groupCol)), "", dRow.Item(groupCol).ToString())) Then
                                ''''''''''''

                                If rowcount = 1 Then  '' If the last row is being processed 

                                    dtNewRow = dt.NewRow()
                                    'dtBlankRow1 = dt.NewRow()
                                    dtBlankRow2 = dt.NewRow()

                                    For k As Integer = 0 To fieldcount - 1 '' Loop to add a summary Row for the previous group

                                        Dim strSummary As String = ""
                                        ' ReSharper disable UnusedVariable
                                        Dim teststr As String = reportFld(k, 10)
                                        ' ReSharper restore UnusedVariable

                                        If reportFld(k, 1) > 0 Then
                                            strSummary = IIf(reportFld(k, 6) <> "0" And reportFld(k, 6) <> Nothing, "Count = " + reportFld(k, 6) & Chr(10), "")
                                        End If
                                        reportSummary(k, 0) = If(reportFld(k, 6) <> "" And reportFld(k, 6) <> "0", (CInt(reportFld(k, 6)) + CInt(reportSummary(k, 0))), CInt(reportSummary(k, 0)))  '' Count
                                        strSummary = strSummary + IIf(reportFld(k, 7) <> "0" And reportFld(k, 7) <> Nothing, "Sum = " + reportFld(k, 7) & Chr(10), "")
                                        reportSummary(k, 1) = If(reportFld(k, 7) <> "" And reportFld(k, 7) <> "0", (CDbl(reportFld(k, 7)) + CDbl(reportSummary(k, 1))), CDbl(reportSummary(k, 1)))  '' Sum 

                                        strSummary = strSummary + IIf(reportFld(k, 8) <> "0" And reportFld(k, 8) <> Nothing, "Max = " + reportFld(k, 8) & Chr(10), "")  ''Max 
                                        strSummary = strSummary + IIf(reportFld(k, 9) <> "0" And reportFld(k, 9) <> Nothing, "Min = " + reportFld(k, 9) & Chr(10), "")  '' Min

                                        strSummary = strSummary + IIf(reportFld(k, 10) <> "0" And reportFld(k, 10) <> Nothing, "Average = " + CStr(CDbl(reportFld(k, 10)) / CDbl(reportFld(k, 6))) & Chr(10), "")
                                        reportSummary(k, 4) = If(reportFld(k, 10) <> "" And reportFld(k, 10) <> "0", (CDbl(reportFld(k, 10)) + CDbl(reportSummary(k, 4))), CDbl(reportSummary(k, 4)))  '' Average 

                                        dtNewRow(k) = strSummary
                                        '' Add Data to ReportSUMMARY Array

                                        If CInt(reportFld(k, 3)) > 0 Then           '' Max
                                            colDataType = reportFld(k, 12)
                                            If colDataType = "Varchar" Then
                                                If (reportSummary(k, 2) <> "" And reportSummary(k, 2) <> "0") Then
                                                    Dim res As Integer
                                                    res = String.Compare(reportSummary(k, 2), reportFld(k, 8))
                                                    If (res = -1) Then
                                                        reportSummary(k, 2) = reportFld(k, 8)
                                                    End If
                                                Else
                                                    reportSummary(k, 2) = reportFld(k, 8)
                                                End If
                                            ElseIf colDataType = "Datetime" Then
                                                If (reportSummary(k, 2) <> Nothing) Then
                                                    If (reportSummary(k, 2) <> Nothing And reportSummary(k, 2) <> "0") Then
                                                        If (CDate(reportFld(k, 8)) > CDate(reportSummary(k, 2))) Then
                                                            reportSummary(k, 2) = reportFld(k, 8)
                                                        End If
                                                    Else
                                                        reportSummary(k, 2) = reportFld(k, 8)
                                                    End If
                                                End If
                                            ElseIf colDataType = "Decimal" Then
                                                If (reportSummary(k, 2) <> Nothing) Then
                                                    If (reportSummary(k, 2) <> Nothing And reportSummary(k, 2) <> "0") Then
                                                        If (CDec(reportFld(k, 8)) > CDec(reportSummary(k, 2))) Then
                                                            reportSummary(k, 2) = reportFld(k, 8)
                                                        End If
                                                    Else
                                                        reportSummary(k, 2) = reportFld(k, 8)
                                                    End If
                                                End If
                                            End If
                                        End If

                                        If CInt(reportFld(k, 4)) > 0 Then       '' Min
                                            colDataType = reportFld(k, 12)

                                            If colDataType = "Varchar" Then
                                                If (reportSummary(k, 3) <> "" And reportSummary(k, 3) <> "0") Then
                                                    Dim res As Integer
                                                    res = String.Compare(reportSummary(k, 3), reportFld(k, 9))
                                                    If (res = 1) Then
                                                        reportSummary(k, 3) = reportFld(k, 9)
                                                    End If
                                                Else
                                                    reportSummary(k, 3) = reportFld(k, 9)
                                                End If
                                            ElseIf colDataType = "Datetime" Then
                                                If (reportSummary(k, 3) <> Nothing) Then
                                                    If (reportSummary(k, 3) <> Nothing And reportSummary(k, 3) <> "0") Then
                                                        If (CDate(reportFld(k, 9)) < CDate(reportSummary(k, 3))) Then
                                                            reportSummary(k, 3) = reportFld(k, 9)
                                                        End If
                                                    Else
                                                        reportSummary(k, 3) = reportFld(k, 9)
                                                    End If
                                                End If
                                            ElseIf colDataType = "Decimal" Then
                                                If (reportSummary(k, 3) <> Nothing) Then
                                                    If (reportSummary(k, 3) <> Nothing And reportSummary(k, 3) <> "0") Then
                                                        If (CDec(reportFld(k, 9)) < CDec(reportSummary(k, 3))) Then
                                                            reportSummary(k, 3) = reportFld(k, 9)
                                                        End If
                                                    Else
                                                        reportSummary(k, 3) = reportFld(k, 9)
                                                    End If
                                                End If
                                            End If
                                        End If
                                        '' Add Data to ReportSUMMARY Array
                                        '' Clear the Array after summaries have been added to the DataTable
                                        reportFld(k, 6) = "0"
                                        reportFld(k, 7) = "0"
                                        reportFld(k, 8) = "0"
                                        reportFld(k, 9) = "0"
                                        reportFld(k, 10) = "0"

                                    Next
                                    'DT.Rows.Add(DtBlankRow1) '' Adding Blank Datarow 
                                    '' Add New Code For Add Summary Details as per criteria
                                    If info.ShowGroupBySummary = True Then
                                        dt.Rows.Add(dtNewRow) '' Adding Summary Datarow 
                                    End If
                                    dt.Rows.Add(dtBlankRow2) '' Adding Blank Datarow 
                                    '' Add New Code For Add Summary Details as per criteria
                                    For j As Integer = 0 To fieldcount - 1 '' Add the current row to the datatable And save the totals to the array

                                        reportFld(j, 6) = If(dRow.Item(j).ToString() <> "", (CInt(reportFld(j, 6)) + 1), CInt(reportFld(j, 6)))                         '' Count

                                        If reportFld(j, 2) > 0 Then
                                            reportFld(j, 7) = If(dRow.Item(j).ToString() <> "", (CDbl(reportFld(j, 7)) + CDbl(dRow.Item(j).ToString())), CDbl(reportFld(j, 7)))     '' Sum
                                        End If

                                        If CInt(reportFld(j, 3)) > 0 Then   '' Max
                                            colDataType = reportFld(j, 12)
                                            If colDataType = "Varchar" Then
                                                If (reportFld(j, 8) <> "" And reportFld(j, 8) <> "0") Then
                                                    Dim res As Integer
                                                    res = String.Compare(reportFld(j, 8), dRow.Item(j).ToString())
                                                    If (res = -1) Then
                                                        reportFld(j, 8) = dRow.Item(j).ToString()
                                                    End If
                                                Else
                                                    reportFld(j, 8) = dRow.Item(j).ToString()
                                                End If
                                            ElseIf colDataType = "Datetime" Then
                                                If (dRow.Item(j).ToString() <> Nothing) Then
                                                    If (reportFld(j, 8) <> Nothing And reportFld(j, 8) <> "0") Then
                                                        If (CDate(dRow.Item(j).ToString()) > CDate(reportFld(j, 8))) Then
                                                            reportFld(j, 8) = dRow.Item(j).ToString()
                                                        End If
                                                    Else
                                                        reportFld(j, 8) = dRow.Item(j).ToString()
                                                    End If
                                                End If
                                            ElseIf colDataType = "Decimal" Then
                                                If (dRow.Item(j).ToString() <> Nothing) Then
                                                    If (reportFld(j, 8) <> Nothing And reportFld(j, 8) <> "0") Then
                                                        If (CDec(dRow.Item(j).ToString()) > CDec(reportFld(j, 8))) Then
                                                            reportFld(j, 8) = dRow.Item(j).ToString()
                                                        End If
                                                    Else
                                                        reportFld(j, 8) = dRow.Item(j).ToString()
                                                    End If
                                                End If
                                            End If

                                        End If

                                        If CInt(reportFld(j, 4)) > 0 Then       '' Min
                                            colDataType = reportFld(j, 12)

                                            If colDataType = "Varchar" Then

                                                If (reportFld(j, 9) <> "" And reportFld(j, 9) <> "0") Then
                                                    Dim res As Integer
                                                    res = String.Compare(reportFld(j, 9), dRow.Item(j).ToString())
                                                    If (res = 1) Then
                                                        reportFld(j, 9) = dRow.Item(j).ToString()
                                                    End If
                                                Else
                                                    reportFld(j, 9) = dRow.Item(j).ToString()

                                                End If

                                            ElseIf colDataType = "Datetime" Then
                                                If (dRow.Item(j).ToString() <> Nothing) Then
                                                    If (reportFld(j, 9) <> Nothing And reportFld(j, 9) <> "0") Then
                                                        If (CDate(dRow.Item(j).ToString()) < CDate(reportFld(j, 9))) Then
                                                            reportFld(j, 9) = dRow.Item(j).ToString()
                                                        End If
                                                    Else
                                                        reportFld(j, 9) = dRow.Item(j).ToString()
                                                    End If
                                                End If
                                            ElseIf colDataType = "Decimal" Then
                                                If (dRow.Item(j).ToString() <> Nothing) Then
                                                    If (reportFld(j, 9) <> Nothing And reportFld(j, 9) <> "0") Then
                                                        If (CDec(dRow.Item(j).ToString()) < CDec(reportFld(j, 9))) Then
                                                            reportFld(j, 9) = dRow.Item(j).ToString()
                                                        End If
                                                    Else
                                                        reportFld(j, 9) = dRow.Item(j).ToString()
                                                    End If
                                                End If
                                            End If

                                        End If
                                        If reportFld(j, 5) > 0 Then
                                            reportFld(j, 10) = If(dRow.Item(j).ToString() <> "", (CDbl(reportFld(j, 10)) + CDbl(dRow.Item(j).ToString())), CDbl(reportFld(j, 10)))  '' Average 
                                        End If
                                    Next

                                    dt.ImportRow(dRow)
                                End If      '' End of Is Last Row (if row count =1)

                                dtNewRow = dt.NewRow() '' Add the summary row to the datatable for the current group
                                'dtBlankRow1 = dt.NewRow()
                                dtBlankRow2 = dt.NewRow()
                                For k As Integer = 0 To fieldcount - 1

                                    Dim strSummary As String = ""
                                    ' ReSharper disable UnusedVariable
                                    Dim teststr As String = reportFld(k, 10)
                                    ' ReSharper restore UnusedVariable

                                    If reportFld(k, 1) > 0 Then
                                        strSummary = IIf(reportFld(k, 6) <> "0" And reportFld(k, 6) <> Nothing, "Count = " + reportFld(k, 6) & Chr(10), "")
                                    End If
                                    reportSummary(k, 0) = If(reportFld(k, 6) <> "" And reportFld(k, 6) <> "0", (CInt(reportFld(k, 6)) + CInt(reportSummary(k, 0))), CInt(reportSummary(k, 0)))  '' Count
                                    strSummary = strSummary + IIf(reportFld(k, 7) <> "0" And reportFld(k, 7) <> Nothing, "Sum = " + reportFld(k, 7) & Chr(10), "")
                                    reportSummary(k, 1) = If(reportFld(k, 7) <> "" And reportFld(k, 7) <> "0", (CDbl(reportFld(k, 7)) + CDbl(reportSummary(k, 1))), CDbl(reportSummary(k, 1)))  '' Sum 

                                    strSummary = strSummary + IIf(reportFld(k, 8) <> "0" And reportFld(k, 8) <> Nothing, "Max = " + reportFld(k, 8) & Chr(10), "")  ''Max 
                                    strSummary = strSummary + IIf(reportFld(k, 9) <> "0" And reportFld(k, 9) <> Nothing, "Min = " + reportFld(k, 9) & Chr(10), "")  '' Min

                                    strSummary = strSummary + IIf(reportFld(k, 10) <> "0" And reportFld(k, 10) <> Nothing, "Average = " + CStr(CDbl(reportFld(k, 10)) / CDbl(reportFld(k, 6))) & Chr(10), "")
                                    reportSummary(k, 4) = If(reportFld(k, 10) <> "" And reportFld(k, 10) <> "0", (CDbl(reportFld(k, 10)) + CDbl(reportSummary(k, 4))), CDbl(reportSummary(k, 4)))  '' Average 

                                    dtNewRow(k) = strSummary
                                    '' Add Data to ReportSUMMARY Array

                                    If CInt(reportFld(k, 3)) > 0 Then           '' Max
                                        colDataType = reportFld(k, 12)
                                        If colDataType = "Varchar" Then
                                            If (reportSummary(k, 2) <> "" And reportSummary(k, 2) <> "0") Then
                                                Dim res As Integer
                                                res = String.Compare(reportSummary(k, 2), reportFld(k, 8))
                                                If (res = -1) Then
                                                    reportSummary(k, 2) = reportFld(k, 8)
                                                End If
                                            Else
                                                reportSummary(k, 2) = reportFld(k, 8)
                                            End If
                                        ElseIf colDataType = "Datetime" Then
                                            If (reportSummary(k, 2) <> Nothing) Then
                                                If (reportSummary(k, 2) <> Nothing And reportSummary(k, 2) <> "0") Then
                                                    If (CDate(reportFld(k, 8)) > CDate(reportSummary(k, 2))) Then
                                                        reportSummary(k, 2) = reportFld(k, 8)
                                                    End If
                                                Else
                                                    reportSummary(k, 2) = reportFld(k, 8)
                                                End If
                                            End If
                                        ElseIf colDataType = "Decimal" Then
                                            If (reportSummary(k, 2) <> Nothing) Then
                                                If (reportSummary(k, 2) <> Nothing And reportSummary(k, 2) <> "0") Then
                                                    If (CDec(reportFld(k, 8)) > CDec(reportSummary(k, 2))) Then
                                                        reportSummary(k, 2) = reportFld(k, 8)
                                                    End If
                                                Else
                                                    reportSummary(k, 2) = reportFld(k, 8)
                                                End If
                                            End If
                                        End If
                                    End If

                                    If CInt(reportFld(k, 4)) > 0 Then       '' Min
                                        colDataType = reportFld(k, 12)

                                        If colDataType = "Varchar" Then
                                            If (reportSummary(k, 3) <> "" And reportSummary(k, 3) <> "0") Then
                                                Dim res As Integer
                                                res = String.Compare(reportSummary(k, 3), reportFld(k, 9))
                                                If (res = 1) Then
                                                    reportSummary(k, 3) = reportFld(k, 9)
                                                End If
                                            Else
                                                reportSummary(k, 3) = reportFld(k, 9)
                                            End If
                                        ElseIf colDataType = "Datetime" Then
                                            If (reportSummary(k, 3) <> Nothing) Then
                                                If (reportSummary(k, 3) <> Nothing And reportSummary(k, 3) <> "0") Then
                                                    If (CDate(reportFld(k, 9)) < CDate(reportSummary(k, 3))) Then
                                                        reportSummary(k, 3) = reportFld(k, 9)
                                                    End If
                                                Else
                                                    reportSummary(k, 3) = reportFld(k, 9)
                                                End If
                                            End If
                                        ElseIf colDataType = "Decimal" Then
                                            If (reportSummary(k, 3) <> Nothing) Then
                                                If (reportSummary(k, 3) <> Nothing And reportSummary(k, 3) <> "0") Then
                                                    If (CDec(reportFld(k, 9)) < CDec(reportSummary(k, 3))) Then
                                                        reportSummary(k, 3) = reportFld(k, 9)
                                                    End If
                                                Else
                                                    reportSummary(k, 3) = reportFld(k, 9)
                                                End If
                                            End If
                                        End If
                                    End If
                                    '' Add Data to ReportSUMMARY Array

                                    '' Clear the Array after summaries have been added to the DataTable
                                    reportFld(k, 6) = "0"
                                    reportFld(k, 7) = "0"
                                    reportFld(k, 8) = "0"
                                    reportFld(k, 9) = "0"
                                    reportFld(k, 10) = "0"

                                Next
                                'DT.Rows.Add(DtBlankRow1) '' Adding Blank Datarow 
                                '' Add New Code For Add Summary Details as per criteria
                                If info.ShowGroupBySummary = True Then
                                    dt.Rows.Add(dtNewRow) '' Adding Summary Datarow 
                                End If
                                dt.Rows.Add(dtBlankRow2) '' Adding Blank Datarow 
                                '' Add New Code For Add Summary Details as per criteria

                            Else    '' If group is not changing

                                If rowcount = 1 Then  '' If the last row is being processed 



                                    For j As Integer = 0 To fieldcount - 1 '' Add the current row to the datatable And save the totals to the array

                                        reportFld(j, 6) = If(dRow.Item(j).ToString() <> "", (CInt(reportFld(j, 6)) + 1), CInt(reportFld(j, 6)))                         '' Count

                                        If reportFld(j, 2) > 0 Then
                                            reportFld(j, 7) = If(dRow.Item(j).ToString() <> "", (CDbl(reportFld(j, 7)) + CDbl(dRow.Item(j).ToString())), CDbl(reportFld(j, 7)))     '' Sum
                                        End If

                                        If CInt(reportFld(j, 3)) > 0 Then   '' Max
                                            colDataType = reportFld(j, 12)
                                            If colDataType = "Varchar" Then
                                                If (reportFld(j, 8) <> "" And reportFld(j, 8) <> "0") Then
                                                    Dim res As Integer
                                                    res = String.Compare(reportFld(j, 8), dRow.Item(j).ToString())
                                                    If (res = -1) Then
                                                        reportFld(j, 8) = dRow.Item(j).ToString()
                                                    End If
                                                Else
                                                    reportFld(j, 8) = dRow.Item(j).ToString()
                                                End If
                                            ElseIf colDataType = "Datetime" Then
                                                If (dRow.Item(j).ToString() <> Nothing) Then
                                                    If (reportFld(j, 8) <> Nothing And reportFld(j, 8) <> "0") Then
                                                        If (CDate(dRow.Item(j).ToString()) > CDate(reportFld(j, 8))) Then
                                                            reportFld(j, 8) = dRow.Item(j).ToString()
                                                        End If
                                                    Else
                                                        reportFld(j, 8) = dRow.Item(j).ToString()
                                                    End If
                                                End If
                                            ElseIf colDataType = "Decimal" Then
                                                If (dRow.Item(j).ToString() <> Nothing) Then
                                                    If (reportFld(j, 8) <> Nothing And reportFld(j, 8) <> "0") Then
                                                        If (CDec(dRow.Item(j).ToString()) > CDec(reportFld(j, 8))) Then
                                                            reportFld(j, 8) = dRow.Item(j).ToString()
                                                        End If
                                                    Else
                                                        reportFld(j, 8) = dRow.Item(j).ToString()
                                                    End If
                                                End If
                                            End If

                                        End If

                                        If CInt(reportFld(j, 4)) > 0 Then       '' Min
                                            colDataType = reportFld(j, 12)

                                            If colDataType = "Varchar" Then

                                                If (reportFld(j, 9) <> "" And reportFld(j, 9) <> "0") Then
                                                    Dim res As Integer
                                                    res = String.Compare(reportFld(j, 9), dRow.Item(j).ToString())
                                                    If (res = 1) Then
                                                        reportFld(j, 9) = dRow.Item(j).ToString()
                                                    End If
                                                Else
                                                    reportFld(j, 9) = dRow.Item(j).ToString()
                                                End If

                                            ElseIf colDataType = "Datetime" Then
                                                If (dRow.Item(j).ToString() <> Nothing) Then
                                                    If (reportFld(j, 9) <> Nothing And reportFld(j, 9) <> "0") Then
                                                        If (CDate(dRow.Item(j).ToString()) < CDate(reportFld(j, 9))) Then
                                                            reportFld(j, 9) = dRow.Item(j).ToString()
                                                        End If
                                                    Else
                                                        reportFld(j, 9) = dRow.Item(j).ToString()
                                                    End If
                                                End If
                                            ElseIf colDataType = "Decimal" Then
                                                If (dRow.Item(j).ToString() <> Nothing) Then
                                                    If (reportFld(j, 9) <> Nothing And reportFld(j, 9) <> "0") Then
                                                        If (CDec(dRow.Item(j).ToString()) < CDec(reportFld(j, 9))) Then
                                                            reportFld(j, 9) = dRow.Item(j).ToString()
                                                        End If
                                                    Else
                                                        reportFld(j, 9) = dRow.Item(j).ToString()
                                                    End If
                                                End If
                                            End If

                                        End If
                                        If reportFld(j, 5) > 0 Then
                                            reportFld(j, 10) = If(dRow.Item(j).ToString() <> "", (CDbl(reportFld(j, 10)) + CDbl(dRow.Item(j).ToString())), CDbl(reportFld(j, 10)))  '' Average 
                                        End If
                                    Next
                                    dt.ImportRow(dRow)


                                    dtNewRow = dt.NewRow() '' Add the summary row to the datatable for the current group
                                    'dtBlankRow1 = dt.NewRow()
                                    dtBlankRow2 = dt.NewRow()
                                    For k As Integer = 0 To fieldcount - 1

                                        Dim strSummary As String = ""
                                        ' ReSharper disable UnusedVariable
                                        Dim teststr As String = reportFld(k, 10)
                                        ' ReSharper restore UnusedVariable

                                        If reportFld(k, 1) > 0 Then
                                            strSummary = IIf(reportFld(k, 6) <> "0" And reportFld(k, 6) <> Nothing, "Count = " + reportFld(k, 6) & Chr(10), "")
                                        End If
                                        reportSummary(k, 0) = If(reportFld(k, 6) <> "" And reportFld(k, 6) <> "0", (CInt(reportFld(k, 6)) + CInt(reportSummary(k, 0))), CInt(reportSummary(k, 0)))  '' Count

                                        strSummary = strSummary + IIf(reportFld(k, 7) <> "0" And reportFld(k, 7) <> Nothing, "Sum = " + reportFld(k, 7) & Chr(10), "")
                                        reportSummary(k, 1) = If(reportFld(k, 7) <> "" And reportFld(k, 7) <> "0", (CDbl(reportFld(k, 7)) + CDbl(reportSummary(k, 1))), CDbl(reportSummary(k, 1)))  '' Sum 

                                        strSummary = strSummary + IIf(reportFld(k, 8) <> "0" And reportFld(k, 8) <> Nothing, "Max = " + reportFld(k, 8) & Chr(10), "")  ''Max 
                                        strSummary = strSummary + IIf(reportFld(k, 9) <> "0" And reportFld(k, 9) <> Nothing, "Min = " + reportFld(k, 9) & Chr(10), "")  '' Min

                                        strSummary = strSummary + IIf(reportFld(k, 10) <> "0" And reportFld(k, 10) <> Nothing, "Average = " + CStr(CDbl(reportFld(k, 10)) / CDbl(reportFld(k, 6))) & Chr(10), "")
                                        reportSummary(k, 4) = If(reportFld(k, 10) <> "" And reportFld(k, 10) <> "0", (CDbl(reportFld(k, 10)) + CDbl(reportSummary(k, 4))), CDbl(reportSummary(k, 4)))  '' Average 

                                        dtNewRow(k) = strSummary
                                        '' Add Data to ReportSUMMARY Array

                                        If CInt(reportFld(k, 3)) > 0 Then           '' Max
                                            colDataType = reportFld(k, 12)
                                            If colDataType = "Varchar" Then
                                                If (reportSummary(k, 2) <> "" And reportSummary(k, 2) <> "0") Then
                                                    Dim res As Integer
                                                    res = String.Compare(reportSummary(k, 2), reportFld(k, 8))
                                                    If (res = -1) Then
                                                        reportSummary(k, 2) = reportFld(k, 8)
                                                    End If
                                                Else
                                                    reportSummary(k, 2) = reportFld(k, 8)
                                                End If
                                            ElseIf colDataType = "Datetime" Then
                                                If (reportSummary(k, 2) <> Nothing) Then
                                                    If (reportSummary(k, 2) <> Nothing And reportSummary(k, 2) <> "0") Then
                                                        If (CDate(reportFld(k, 8)) > CDate(reportSummary(k, 2))) Then
                                                            reportSummary(k, 2) = reportFld(k, 8)
                                                        End If
                                                    Else
                                                        reportSummary(k, 2) = reportFld(k, 8)
                                                    End If
                                                End If
                                            ElseIf colDataType = "Decimal" Then
                                                If (reportSummary(k, 2) <> Nothing) Then
                                                    If (reportSummary(k, 2) <> Nothing And reportSummary(k, 2) <> "0") Then
                                                        If (CDec(reportFld(k, 8)) > CDec(reportSummary(k, 2))) Then
                                                            reportSummary(k, 2) = reportFld(k, 8)
                                                        End If
                                                    Else
                                                        reportSummary(k, 2) = reportFld(k, 8)
                                                    End If
                                                End If
                                            End If
                                        End If

                                        If CInt(reportFld(k, 4)) > 0 Then       '' Min
                                            colDataType = reportFld(k, 12)

                                            If colDataType = "Varchar" Then
                                                If (reportSummary(k, 3) <> "" And reportSummary(k, 3) <> "0") Then
                                                    Dim res As Integer
                                                    res = String.Compare(reportSummary(k, 3), reportFld(k, 9))
                                                    If (res = 1) Then
                                                        reportSummary(k, 3) = reportFld(k, 9)
                                                    End If
                                                Else
                                                    reportSummary(k, 3) = reportFld(k, 9)
                                                End If
                                            ElseIf colDataType = "Datetime" Then
                                                If (reportSummary(k, 3) <> Nothing) Then
                                                    If (reportSummary(k, 3) <> Nothing And reportSummary(k, 3) <> "0") Then
                                                        If (CDate(reportFld(k, 9)) < CDate(reportSummary(k, 3))) Then
                                                            reportSummary(k, 3) = reportFld(k, 9)
                                                        End If
                                                    Else
                                                        reportSummary(k, 3) = reportFld(k, 9)
                                                    End If
                                                End If
                                            ElseIf colDataType = "Decimal" Then
                                                If (reportSummary(k, 3) <> Nothing) Then
                                                    If (reportSummary(k, 3) <> Nothing And reportSummary(k, 3) <> "0") Then
                                                        If (CDec(reportFld(k, 9)) < CDec(reportSummary(k, 3))) Then
                                                            reportSummary(k, 3) = reportFld(k, 9)
                                                        End If
                                                    Else
                                                        reportSummary(k, 3) = reportFld(k, 9)
                                                    End If
                                                End If
                                            End If
                                        End If
                                        '' Add Data to ReportSUMMARY Array
                                        '' Clear the Array after summaries have been added to the DataTable
                                        reportFld(k, 6) = "0"
                                        reportFld(k, 7) = "0"
                                        reportFld(k, 8) = "0"
                                        reportFld(k, 9) = "0"
                                        reportFld(k, 10) = "0"

                                    Next
                                    'DT.Rows.Add(DtBlankRow1) '' Adding Blank Datarow 
                                    '' Add New Code For Add Summary Details as per criteria
                                    If info.ShowGroupBySummary = True Then
                                        dt.Rows.Add(dtNewRow) '' Adding Summary Datarow 
                                    End If
                                    dt.Rows.Add(dtBlankRow2) '' Adding Blank Datarow 
                                    '' Add New Code For Add Summary Details as per criteria

                                End If      '' End of Is Last Row (if row count =1)

                            End If   '' End of if Group is changing 


                            If rowcount <> 1 Then '' If the row being processed is not the last row Add the row to the Datatable
                                '' And save the totals to the array  
                                For j As Integer = 0 To fieldcount - 1

                                    reportFld(j, 6) = If(dRow.Item(j).ToString() <> "", (CInt(reportFld(j, 6)) + 1), CInt(reportFld(j, 6)))                         '' Count

                                    If reportFld(j, 2) > 0 Then
                                        reportFld(j, 7) = If(dRow.Item(j).ToString() <> "", (CDbl(reportFld(j, 7)) + CDbl(dRow.Item(j).ToString())), CDbl(reportFld(j, 7)))     '' Sum
                                    End If

                                    If CInt(reportFld(j, 3)) > 0 Then           '' Max
                                        colDataType = reportFld(j, 12)
                                        If colDataType = "Varchar" Then
                                            If (reportFld(j, 8) <> "" And reportFld(j, 8) <> "0") Then
                                                Dim res As Integer
                                                res = String.Compare(reportFld(j, 8), dRow.Item(j).ToString())
                                                If (res = -1) Then
                                                    reportFld(j, 8) = dRow.Item(j).ToString()
                                                End If
                                            Else
                                                reportFld(j, 8) = dRow.Item(j).ToString()
                                            End If
                                        ElseIf colDataType = "Datetime" Then
                                            If (dRow.Item(j).ToString() <> Nothing) Then
                                                If (reportFld(j, 8) <> Nothing And reportFld(j, 8) <> "0") Then
                                                    If (CDate(dRow.Item(j).ToString()) > CDate(reportFld(j, 8))) Then
                                                        reportFld(j, 8) = dRow.Item(j).ToString()
                                                    End If
                                                Else
                                                    reportFld(j, 8) = dRow.Item(j).ToString()
                                                End If
                                            End If
                                        ElseIf colDataType = "Decimal" Then
                                            If (dRow.Item(j).ToString() <> Nothing) Then
                                                If (reportFld(j, 8) <> Nothing And reportFld(j, 8) <> "0") Then
                                                    If (CDec(dRow.Item(j).ToString()) > CDec(reportFld(j, 8))) Then
                                                        reportFld(j, 8) = dRow.Item(j).ToString()
                                                    End If
                                                Else
                                                    reportFld(j, 8) = dRow.Item(j).ToString()
                                                End If
                                            End If
                                        End If

                                        ''ReportFLD(j, 8) = If(DRow.Item(j).ToString() <> "", If(CInt(DRow.Item(j).ToString()) > CInt(ReportFLD(j, 8)), CInt(DRow.Item(j).ToString()), CInt(ReportFLD(j, 8))), CInt(ReportFLD(j, 8)))   '' Max
                                    End If

                                    If CInt(reportFld(j, 4)) > 0 Then

                                        colDataType = reportFld(j, 12)

                                        If colDataType = "Varchar" Then

                                            If (reportFld(j, 9) <> "" And reportFld(j, 9) <> "0") Then
                                                Dim res As Integer
                                                res = String.Compare(reportFld(j, 9), dRow.Item(j).ToString())
                                                If (res = 1) Then
                                                    reportFld(j, 9) = dRow.Item(j).ToString()
                                                End If
                                            Else
                                                reportFld(j, 9) = dRow.Item(j).ToString()
                                            End If

                                        ElseIf colDataType = "Datetime" Then
                                            If (dRow.Item(j).ToString() <> Nothing) Then
                                                If (reportFld(j, 9) <> Nothing And reportFld(j, 9) <> "0") Then
                                                    If (CDate(dRow.Item(j).ToString()) < CDate(reportFld(j, 9))) Then
                                                        reportFld(j, 9) = dRow.Item(j).ToString()
                                                    End If
                                                Else
                                                    reportFld(j, 9) = dRow.Item(j).ToString()
                                                End If
                                            End If
                                        ElseIf colDataType = "Decimal" Then
                                            If (reportFld(j, 9) <> Nothing And reportFld(j, 9) <> "0") Then
                                                If (CDec(dRow.Item(j).ToString()) < CDec(reportFld(j, 9))) Then
                                                    reportFld(j, 9) = dRow.Item(j).ToString()
                                                End If
                                            Else
                                                reportFld(j, 9) = dRow.Item(j).ToString()
                                            End If
                                        End If
                                        'ReportFLD(j, 9) = If(DRow.Item(j).ToString() <> "", If(CInt(DRow.Item(j).ToString()) > CInt(ReportFLD(j, 9)), CInt(DRow.Item(j).ToString()), CInt(ReportFLD(j, 9))), CInt(ReportFLD(j, 9)))   ''Min

                                    End If
                                    If reportFld(j, 5) > 0 Then
                                        reportFld(j, 10) = If(dRow.Item(j).ToString() <> "", (CDbl(reportFld(j, 10)) + CDbl(dRow.Item(j).ToString())), CDbl(reportFld(j, 10)))  '' Average 
                                    End If
                                Next

                                dt.ImportRow(dRow)

                            End If  '' If rowcount not =1 i.e. this is not the last row

                            '' Update the variable with the current group name
                            ''''' Code modified by kamalesh Ahuja on 30th Sept 2010 to resolve mantis issue id 19743
                            ''GroupText = IIf(IsDBNull(DRow.Item(GroupCol)), "", DRow.Item(GroupCol))
                            groupText = IIf(IsDBNull(dRow.Item(groupCol)), "", dRow.Item(groupCol).ToString())
                            ''''''''''
                        End If
                    Next

                    dtNewRow = dt.NewRow()
                    dtBlankRow1 = dt.NewRow()

                    For ks As Integer = 0 To fieldcount - 1
                        Dim strSummary As String = ""
                        ' ReSharper disable UnusedVariable
                        Dim teststr As String = reportFld(ks, 10)
                        ' ReSharper restore UnusedVariable

                        If reportFld(ks, 1) > 0 Then
                            strSummary = IIf(reportSummary(ks, 0) <> "0" And reportSummary(ks, 0) <> Nothing, "Count = " + reportSummary(ks, 0) & Chr(10), "")
                        End If

                        strSummary = strSummary + IIf(reportSummary(ks, 1) <> "0" And reportSummary(ks, 1) <> Nothing, "Sum = " + reportSummary(ks, 1) & Chr(10), "")

                        strSummary = strSummary + IIf(reportSummary(ks, 2) <> "0" And reportSummary(ks, 2) <> Nothing, "Max = " + reportSummary(ks, 2) & Chr(10), "")  ''Max 
                        strSummary = strSummary + IIf(reportSummary(ks, 3) <> "0" And reportSummary(ks, 3) <> Nothing, "Min = " + reportSummary(ks, 3) & Chr(10), "")  '' Min

                        strSummary = strSummary + IIf(reportSummary(ks, 4) <> "0" And reportSummary(ks, 4) <> Nothing, "Average = " + CStr(CDbl(reportSummary(ks, 4)) / CDbl(reportSummary(ks, 0))) & Chr(10), "")

                        dtNewRow(ks) = strSummary
                    Next

                    '' Add New Code For Add Summary Details as per criteria as per criteria
                    If info.ShowSummary = True Then
                        dtBlankRow1(0) = "SUMMARY PAGE"
                        dt.Rows.Add(dtBlankRow1)
                        dtBlankRow1 = dt.NewRow()
                        dtBlankRow1(0) = "Total Records: " + reportDs.Tables(tblCnt).Rows.Count.ToString
                        dt.Rows.Add(dtBlankRow1)
                        dtBlankRow1 = dt.NewRow()
                        For i As Integer = 0 To fieldcount - 1
                            ''DtBlankRow1(i) = info.Fields(i).FieldName
                            ''''' Code modified by kamalesh Ahuja on 30th Sept 2010 to resolve mantis issue id 19743
                            If info.Fields(i).FKColDescrip <> "" Then
                                dtBlankRow1(i) = info.Fields(i).FKColDescrip
                            Else
                                dtBlankRow1(i) = info.Fields(i).FieldName
                            End If
                            ''''''''''''''''
                        Next
                        dt.Rows.Add(dtBlankRow1)
                        dtBlankRow1 = dt.NewRow()
                        dt.Rows.Add(dtBlankRow1)
                        dt.Rows.Add(dtNewRow) '' Adding Summary Datarow 
                    End If
                    '' Add New Code For Add Summary Details as per criteria
                    dt.TableName = "Table" + tblCnt.ToString
                    newDataSet.Tables.Add(dt)
                    ''Create a new dataTable so as to keep only 16300 rows in case there are more
                    '' rows in the return datatable

                    'Dim rowCnt As Integer = 1
                    'Dim deleteDt As DataTable
                    'deleteDt = dt.Clone()
                    'deleteDt.Clear()

                    'For Each dtr As DataRow In dt.Rows

                    '    deleteDt.ImportRow(dtr)
                    '    rowCnt += 1
                    '    If rowCnt = 16300 Then
                    '        Exit For
                    '    End If
                    'Next

                    'newDataSet.Tables.Add(deleteDt)

                Catch ex As Exception
                    Throw ex
                End Try
            Next
            Return newDataSet

        End Function

        ''' <summary>
        ''' Debug function
        ''' </summary>
        ''' <param name="info"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetReportSql(ByVal info As AdHocRptInfo) As String
            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            Dim sql As String = _GetReportSql(db, info, False)
            Return sql
        End Function
        ''New Code Added By Vijay Ramteke On May 06, 2010
        Public Shared Function GetReportDataSetAdHoc(ByVal info As AdHocRptInfo, ByVal entityId As String, Optional ByVal isSummary As Boolean = False) As DataSet
            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            Dim sql As String = _GetReportSql(db, info, isSummary)
            ''Entity Id     Table Description   FieldName
            ''394           arStudent           arStudent.StudentId
            ''395           adLeads             adLeads.LeadId
            ''434           faLenders           faLenders.LenderId
            ''396           hrEmployees         hrEmployees.EmpId
            ''397           plEmployers         plEmployers.EmployerId
            '' Code Changes By Vijay Ramteke On December 31, 2010
            If entityId = "394" Then
                If sql.Contains("arStudent") Then
                    ''sql = "select arStudent.StudentId As RecipientId ,  " + sql.Trim().Substring(6, sql.Trim().Length - 6)
                    sql = "select distinct arStudent.StudentId As RecipientId ,  " + sql.Trim().Substring(15, sql.Trim().Length - 15)
                Else
                    ''sql = "select '00000000-0000-0000-0000-000000000000' As RecipientId ,  " + sql.Trim().Substring(6, sql.Trim().Length - 6)
                    sql = "select distinct '00000000-0000-0000-0000-000000000000' As RecipientId ,  " + sql.Trim().Substring(15, sql.Trim().Length - 15)
                End If
            ElseIf entityId = "395" Then
                If sql.Contains("adLeads") Then
                    ''sql = "select adLeads.LeadId As RecipientId ,  " + sql.Trim().Substring(6, sql.Trim().Length - 6)
                    sql = "select distinct adLeads.LeadId As RecipientId ,  " + sql.Trim().Substring(15, sql.Trim().Length - 15)
                Else
                    ''sql = "select '00000000-0000-0000-0000-000000000000' As RecipientId ,  " + sql.Trim().Substring(6, sql.Trim().Length - 6)
                    sql = "select distinct '00000000-0000-0000-0000-000000000000' As RecipientId ,  " + sql.Trim().Substring(15, sql.Trim().Length - 15)
                End If
            ElseIf entityId = "434" Then
                If sql.Contains("faLenders") Then
                    ''sql = "select faLenders.LenderId As RecipientId ,  " + sql.Trim().Substring(6, sql.Trim().Length - 6)
                    sql = "select distinct faLenders.LenderId As RecipientId ,  " + sql.Trim().Substring(15, sql.Trim().Length - 15)
                Else
                    ''sql = "select '00000000-0000-0000-0000-000000000000' As RecipientId ,  " + sql.Trim().Substring(6, sql.Trim().Length - 6)
                    sql = "select distinct '00000000-0000-0000-0000-000000000000' As RecipientId ,  " + sql.Trim().Substring(15, sql.Trim().Length - 15)
                End If
            ElseIf entityId = "396" Then
                If sql.Contains("hrEmployees") Then
                    ''sql = "select hrEmployees.EmpId As RecipientId ,  " + sql.Trim().Substring(6, sql.Trim().Length - 6)
                    sql = "select distinct hrEmployees.EmpId As RecipientId ,  " + sql.Trim().Substring(15, sql.Trim().Length - 15)
                Else
                    ''sql = "select '00000000-0000-0000-0000-000000000000' As RecipientId ,  " + sql.Trim().Substring(6, sql.Trim().Length - 6)
                    sql = "select distinct '00000000-0000-0000-0000-000000000000' As RecipientId ,  " + sql.Trim().Substring(15, sql.Trim().Length - 15)
                End If
            ElseIf entityId = "397" Then
                If sql.Contains("plEmployers") Then
                    ''sql = "select plEmployers.EmployerId As RecipientId ,  " + sql.Trim().Substring(6, sql.Trim().Length - 6)
                    sql = "select distinct plEmployers.EmployerId As RecipientId ,  " + sql.Trim().Substring(15, sql.Trim().Length - 15)
                Else
                    ''sql = "select '00000000-0000-0000-0000-000000000000' As RecipientId ,  " + sql.Trim().Substring(6, sql.Trim().Length - 6)
                    sql = "select distinct '00000000-0000-0000-0000-000000000000' As RecipientId ,  " + sql.Trim().Substring(15, sql.Trim().Length - 15)
                End If
            End If
            '' Code Changes By Vijay Ramteke On December 31, 2010

            Dim ds As DataSet = db.RunParamSQLDataSet(sql)

            ' apply masks to ssn, phone and zip
            ApplyMasks(ds, sql)

            If info.ShowSummary Then
                db.ClearParameters()
                sql = _GetReportSql(db, info, True)
                Dim ds2 As DataSet = db.RunParamSQLDataSet(sql)
                Dim dt1 As DataTable = ds2.Tables(0).Copy
                dt1.TableName = "Summary1"
                ds.Tables.Add(dt1)

                Dim dt2 As DataTable = ds2.Tables(0).Copy
                dt2.TableName = "Summary2"
                ds.Tables.Add(dt2)
            End If

            ' do a fixup of the dataset by removing any column that started with a "!".
            ' The "!" is added to the sql query within GetColumnsSql to make the query
            ' run ok with order and compute statements.  Now that the query has executed,
            ' we no longer need those columns.
            For Each dt As DataTable In ds.Tables
                For i As Integer = dt.Columns.Count - 1 To 0 Step -1
                    Dim c As DataColumn = dt.Columns(i)
                    If c.ColumnName.IndexOf("!", StringComparison.Ordinal) >= 0 Then
                        dt.Columns.RemoveAt(i)
                    End If
                Next
            Next

            ' apply another fix to the dataset which happens if there is a "group by"
            ' and there are no records returned.  If this happens, we get only 1 table back
            ' representing the count.  This messes up the reporting, so we just want to detect
            ' this case and remove the table.
            If ds.Tables.Count = 1 AndAlso ds.Tables(0).Columns.Count = 1 AndAlso ds.Tables(0).Columns(0).ColumnName = "cnt" Then
                ds.Tables.RemoveAt(0)
            End If

            Return ds
        End Function
        ''New Code Added By Vijay Ramteke On May 06, 2010

#Region "RunReport Helpers"
        ''' <summary>
        ''' Returns the sql statement for the given report
        ''' </summary>
        ''' <param name="info"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function _GetReportSql(ByVal db As DataAccess, ByVal info As AdHocRptInfo, ByVal IsSummary As Boolean) As String
            ' dtFields holds all the field info for this report
            Dim UDFGroupBy As Boolean = False
            Dim dtFields As DataTable = ReportsDB.GetReportFieldsDS(info.ReportId, True).Tables(0)

            ' Build the column list, the froms and the wheres for each table            
            Dim TablesSql As String = GetTablesSql(dtFields)
            Dim dtRelations As DataTable = GetTableRelations(TablesSql)
            
            Dim ColumnsSql As String = GetColumnsSql(dtFields, dtRelations, info.Filter)
            Dim fromSql As String = GetFromSql(db, TablesSql, dtRelations)
            Dim WheresSql As String = GetWheresSql(db, TablesSql, dtRelations) 'BEN: Changed 2nd param to act on string rather that dtFields
            If(WheresSql.Contains("AND arClassSections.LeadGrpId = adLeadGroups.LeadGrpId"))
                Dim newWhere = WheresSql.Replace("AND arClassSections.LeadGrpId = adLeadGroups.LeadGrpId","")
                WheresSql = newWhere
            End If
            Dim CampGroupWhereSql As String = GetCampGroupWheresSql(info, TablesSql, dtRelations) ' Mantis 9144: Restrict where clause so that it only shows current campus group
            Dim OrderBySql As String = GetOrderBySql(dtFields, dtRelations)
            Dim ComputeSql As String
            UDFGroupBy = IsUdfGroupBy(dtFields)

            ' Build the sql string
            Dim sb As New StringBuilder()
            If UDFGroupBy Then sb.Append("Select * from (")

            Dim notDistinct = True
            For Each dr As DataRow In dtFields.Rows
                If dr("summarytype") = 32 Then
                    notDistinct = False
                End If
            Next
            'Troy: Added if statement here to fix DE11153 PRODUCTION: Adhoc Report Not Displaying Disbursements with same amount and date.
            'When the saTransactions table is involved we will want all transactions returned even if they have matching criteria such as
            'when you have two payments for the same amount for the same fund source.
            If(info.UseLeftJoin) 
                If fromSql.ToLower.Contains("satransactions") And notDistinct Then
                    sb.Append("select ")
                ElseIf fromSql.ToLower.Contains("arresults") Then
                    sb.Append("select ")
                Else
                    sb.Append("select distinct ")
                End If
            else
                If TablesSql.ToLower.Contains("satransactions") And notDistinct Then
                    sb.Append("select ")
                ElseIf TablesSql.ToLower.Contains("arresults") Then
                    sb.Append("select ")
                Else
                    sb.Append("select distinct ")
                End If
            End If
            
            sb.Append(ColumnsSql)

            ' add the list of tables
            sb.Append(" from ")
            Dim bNeedAnd As Boolean = False
            If(info.UseLeftJoin) 
                sb.Append("     " + fromSql)
                If  CampGroupWhereSql.Length > 0 Or info.Filter.Length > 0 Then
                    sb.Append("where ")
                End If
            Else 
                sb.Append("     " + TablesSql)
                ' add "WHERE" if there is something going there
                If WheresSql.Length > 0 Or CampGroupWhereSql.Length > 0 Or info.Filter.Length > 0 Then
                    sb.Append("where ")
                End If
                ' add the where clause
                If WheresSql.Length > 0 Then
                    sb.Append(WheresSql)
                    bNeedAnd = True
                End If
            End If
            
            ' BEN: 10/19/06 - Mantis 9144: Restrict where clause so that it only shows current campus group            
            If CampGroupWhereSql.Length > 0 Then
                If bNeedAnd Then sb.Append("    AND ")
                sb.Append(CampGroupWhereSql)
                bNeedAnd = True
            End If
            ' add any additional filter
            If info.Filter.Length > 0 Then
                'checking the money format compare
                If info.Filter.Contains("SELECT FORMAT(SUM(F.TransAmount),'C','en-us')") Then
                    Dim filter = info.Filter.Replace("SELECT FORMAT(SUM(F.TransAmount),'C','en-us')", "SELECT SUM(F.TransAmount)")
                    info.Filter = filter
                End If
                If info.Filter.Contains("FORMAT(SUM(T.TransAmount),'C','en-us')") Then
                    Dim filter = info.Filter.Replace("FORMAT(SUM(T.TransAmount),'C','en-us')", "SUM(T.TransAmount)")
                    info.Filter = filter
                End If
                If info.Filter.Contains("CAST(SUM(ScheduledHours) AS NVARCHAR(50))") Then
                    Dim change = info.Filter.Replace("CAST(SUM(ScheduledHours) AS NVARCHAR(50))", "SUM(ScheduledHours)")
                    info.Filter = change
                End If
                If info.Filter.Contains("CAST(SUM(ActualHours) AS NVARCHAR(50))") Then
                    Dim change1 = info.Filter.Replace("CAST(SUM(ActualHours) AS NVARCHAR(50))", "SUM(ActualHours)")
                    info.Filter = change1
                End If
                If info.Filter.Contains("FORMAT((SUM(ISNULL(GrossAmount,0)) - SUM(ISNULL(LoanFees,0))),'C','en-us')") Then
                    Dim change1 = info.Filter.Replace("FORMAT((SUM(ISNULL(GrossAmount,0)) - SUM(ISNULL(LoanFees,0))),'C','en-us')", "(SUM(ISNULL(GrossAmount,0)) - SUM(ISNULL(LoanFees,0)))")
                    info.Filter = change1
                End If
                If info.Filter.Contains("FORMAT(SUM(ISNULL(NTR.recAmt,0)),'C','en-us')") Then
                    Dim change1 = info.Filter.Replace("FORMAT(SUM(ISNULL(NTR.recAmt,0)),'C','en-us')", "SUM(ISNULL(NTR.recAmt,0))")
                    info.Filter = change1
                End If
                If info.Filter.Contains("FORMAT(ISNULL(SUM(A.recAmt),0),'C','en-us')") Then
                    Dim change1 = info.Filter.Replace("FORMAT(ISNULL(SUM(A.recAmt),0),'C','en-us')", "ISNULL(SUM(A.recAmt),0)")
                    info.Filter = change1
                End If
                If info.Filter.Contains("(SELECT PayPlanEndDate FROM faStudentPaymentPlans WHERE StuEnrollId = arStuEnrollments.StuEnrollId) AS L") Then
                    Dim change1 = info.Filter.Replace("(SELECT PayPlanEndDate FROM faStudentPaymentPlans WHERE StuEnrollId = arStuEnrollments.StuEnrollId) AS L", "(SELECT PayPlanEndDate FROM faStudentPaymentPlans WHERE StuEnrollId = arStuEnrollments.StuEnrollId) ")
                    info.Filter = change1
                End If
                If bNeedAnd Then sb.Append("    AND ")
                sb.Append(info.Filter)
                bNeedAnd = True
            End If

            'For Each dr As DataRow In dtFields.Rows
            '    Dim fldGrade As String = dr("Header").ToString()
            '    If fldGrade.ToLower().Contains("grade") Then
            '        sb.Append(vbCrLf + "UNION " + vbCrLf)
            '        If UDFGroupBy Then sb.Append("Select * from (")
            '        sb.Append("select distinct " + vbCrLf)
            '        sb.Append(Replace(Replace(Replace(ColumnsSql, "arResults.GrdSysDetailId", "arTransferGrades.GrdSysDetailId"), "arClassSections.ClsSection", "NULL"), "arClassSections.TermId", "arTransferGrades.TermId") + vbCrLf)
            '        sb.Append("from arStudent, arStuEnrollments, arGradeSystemDetails, arReqs, arClassSections, arTransferGrades " + vbCrLf)
            '        sb.Append(vbCrLf + " WHERE arStuEnrollments.StudentId = arStudent.StudentId ")
            '        sb.Append(vbCrLf + " AND arClassSections.ReqId = arReqs.ReqId ")
            '        sb.Append(vbCrLf + " AND arTransferGrades.StuEnrollId = arStuEnrollments.StuEnrollId ")
            '        sb.Append(vbCrLf + " AND arTransferGrades.GrdSysDetailId = arGradeSystemDetails.GrdSysDetailId ")
            '        sb.Append(vbCrLf + " AND arTransferGrades.ReqId = arReqs.ReqId ")
            '        If CampGroupWhereSql.Length > 0 Then
            '            If bNeedAnd Then sb.Append(" AND ")
            '            sb.Append(Replace(CampGroupWhereSql, " arClassSections.CampusId", " arStuEnrollments.CampusId") + vbCrLf)
            '            bNeedAnd = True
            '        End If
            '        If info.Filter.Length > 0 Then
            '            If bNeedAnd Then sb.Append("    AND ")
            '            sb.Append(Replace(info.Filter, "arClassSections.TermId", "arTransferGrades.TermId") + vbCrLf)
            '            bNeedAnd = True
            '        End If
            '    End If
            'Next

            If UDFGroupBy Then sb.Append(") UDF ")
            ' only add the ORDER BY and COMPUTE sql if this is not the summary dataset

            If Not IsSummary Then

                If OrderBySql.Length > 0 Then
                    sb.Append(" order by " + OrderBySql)
                End If
            End If


            Return sb.ToString()
        End Function
        ''' <summary>
        ''' Returns a string with the list of columns contained within dtFields.
        ''' dtFields is a datatable obtained from GetFieldsDS().
        ''' The resulting string is a comma-delimitted list of columns that can
        ''' be safely used in a sql SELECT clause.
        ''' </summary>
        ''' <param name="dtFields"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function GetColumnsSql(ByVal dtFields As DataTable, ByVal dtRelations As DataTable, Optional ByVal filter As String = "") As String
            Dim sb As New StringBuilder()

            ' for each report column defined in dtFields, build a list
            ' of comma delimitted columns suitable to be used in a sql query
            For Each dr As DataRow In dtFields.Rows
                Dim colSql As String = GetColumnSqlFromField(dr, dtRelations, filter)
                sb.Append(colSql)
            Next
            If sb.Length > 5 Then
                sb.Remove(sb.Length - 1, 1) ' remove the last comma and cr/lf
            End If
            Return sb.ToString()
        End Function
        ''' <summary>
        ''' Returns the query for a specific column.  If the column is not related to
        ''' any other column, then the return is simply the column.
        ''' If the column is related to anothe column via a FK, then the return
        ''' is a sub-query on the foreign table.  
        ''' </summary>
        ''' <param name="dr"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function GetColumnSqlFromField(ByVal dr As DataRow, ByVal dtRelations As DataTable, Optional ByVal filter As String = "") As String
            Dim tblName As String = dr("TableName").ToString()
            Dim colName As String = dr("FieldName").ToString()
            'Dim header As String = dr("Header")
            'Dim visible As Boolean = dr("Visible")
            Dim fkColDescrip As String = dr("FKColDescrip").ToString()
            Dim isSdf As Boolean = dr("IsUDF")
            'If header Is Nothing Or header = "" Then header = colName
            Dim calculationSql As String = dr("CalculationSql").ToString()

            Dim sql As String = ""
            If isSdf Then
                Dim tmp As String = GetColumnSqlFromSdfField(dr, dtRelations)
                If tmp <> "" Then
                    'sql = String.Format("     {0} as [{1}], {2}", tmp, colName, vbCrLf)
                    sql = String.Format("     {0} as [{1}],", tmp, colName)
                End If
            Else
                'added by Theresa G on July 30 2010 to fix 10044: 1.2.2: 'Age' and 'County' field on Leads section in adhoc report. 
                If tblName = "adLeads" And colName = "Age" Then
                    sql = " case  when MONTH(adLeads.birthdate) > Month(getdate())  Or (Month(adLeads.BirthDate) = Month(GetDate()) And Day(adLeads.BirthDate) > Day(getDate()) And Day(adLeads.BirthDate) <> Day(GetDate())) then	datediff(yy,adLeads.birthdate,GETDATE())-1       Else  DateDiff(yy, adLeads.birthdate, GETDATE()) End as Age "
                    'sql = String.Format("     {0},{1}", sql, vbCrLf)
                    sql = String.Format("     {0},", sql)
                ElseIf tblName = "arRooms" And colName = "Descrip" Then
                    'sql = String.Format("     {0}.{1} as RoomDescrip, {2}", tblName, colName, vbCrLf)
                    sql = String.Format("     {0}.{1} as RoomDescrip,", tblName, colName)
                    'ak added on 1/21/2014 
                ElseIf tblName = "saTransactions" And colName = "TransAmount" And (dr("SummaryType") < 1 Or dr("SummaryType") = 2) Then
                    sql = "CASE WHEN (SUBSTRING(CAST(ISNULL(saTransactions.TransAmount,0) AS VARCHAR),1,1)='-') THEN REPLACE(CAST(CAST(ISNULL(saTransactions.TransAmount,0)AS numeric(19,2))AS varchar),'-','$(')+')' Else '$'+ CAST(CAST(ISNULL(saTransactions.TransAmount,0)AS numeric(19,2))AS varchar) END AS TransAmount"
                    'sql = String.Format("     {0},{1}", sql, vbCrLf)
                    sql = String.Format("     {0},", sql)
                ElseIf Not String.IsNullOrEmpty(calculationSql) Then
                    'sql = String.Format("     {0},{1} ", calculationSql.Trim(), vbCrLf)
                    'adding the logic of filter group by for the aid columns
                    If Not String.IsNullOrEmpty(filter) Then
                        Dim awardTypeIndex = filter.IndexOf("saTransactions.FundSourceId in (")
                        Dim awardTypeIdSub = ""
                        Dim awardTypeIds = ""
                        If awardTypeIndex > -1 Then
                            awardTypeIdSub = filter.Substring(awardTypeIndex + 1, filter.IndexOf(")", awardTypeIndex + 1) - awardTypeIndex)
                            awardTypeIds = awardTypeIdSub.Substring(awardTypeIdSub.IndexOf("(") + 1, awardTypeIdSub.IndexOf(")", awardTypeIdSub.IndexOf("(") + 1) - awardTypeIdSub.IndexOf("(") - 1)
                        End If

                        Dim academicYearIndex = filter.IndexOf("saTransactions.AcademicYearId in (")
                        Dim academicYearIdSub = ""
                        Dim academicYearId = ""
                        If academicYearIndex > -1 Then
                            academicYearIdSub = filter.Substring(academicYearIndex + 1, filter.IndexOf(")", academicYearIndex + 1) - academicYearIndex)
                            academicYearId = academicYearIdSub.Substring(academicYearIdSub.IndexOf("(") + 1, academicYearIdSub.IndexOf(")", academicYearIdSub.IndexOf("(") + 1) - academicYearIdSub.IndexOf("(") - 1)
                        End If
                        Dim filterSql = ""
                        If colName = "TotalAidExpected" Or colName = "TotalAidReceived" Or colName = "TotalAidDue" Then
                            If (awardTypeIds <> "" And academicYearId <> "") Then
                                calculationSql = calculationSql + ","
                                sql = calculationSql.Replace("StuEnrollId = arStuEnrollments.StuEnrollId", " StuEnrollId = arStuEnrollments.StuEnrollId AND AwardTypeId IN ( " + awardTypeIds + " ) AND faStudentAwards.AcademicYearId IN ( " + academicYearId + " ) ")
                            ElseIf (awardTypeIds <> "" And academicYearId.Equals("")) Then
                                calculationSql = calculationSql + ","
                                sql = calculationSql.Replace("StuEnrollId = arStuEnrollments.StuEnrollId", " StuEnrollId = arStuEnrollments.StuEnrollId AND  faStudentAwards.AwardTypeId IN ( " + awardTypeIds + " ) ")
                            ElseIf (academicYearId <> "" And awardTypeIds.Equals("")) Then
                                calculationSql = calculationSql + ","
                                sql = calculationSql.Replace("StuEnrollId = arStuEnrollments.StuEnrollId", " StuEnrollId = arStuEnrollments.StuEnrollId AND faStudentAwards.AcademicYearId IN ( " + academicYearId + " ) ")
                            Else
                                sql = String.Format("     {0},", calculationSql.Trim())
                            End If
                        Else
                            sql = String.Format("     {0},", calculationSql.Trim())
                        End If
                    Else
                        sql = String.Format("     {0},", calculationSql.Trim())
                    End If
                Else
                    'sql = String.Format("     {0}.{1}, {2}", tblName, colName, vbCrLf)
                    sql = String.Format("     {0}.{1},", tblName, colName)
                End If

            End If
            ' check if this column is related to another table and 
            ' if so, create a special sub-query for it
            If fkColDescrip Is Nothing Or fkColDescrip = "" Then
            Else
                Dim tmpSql As String = String.Format("(FK_TABLE='{0}' AND FK_COLUMN='{1}')", tblName, colName)
                Dim drs() As DataRow = dtRelations.Select(tmpSql)
                If drs.Length = 0 Then ' Column is not related to any other table-> Just add it to the select list                        
                Else
                    ' if we are here, then this column requires a sub-query
                    Dim fkTbl As String = drs(0)("PK_TABLE")
                    Dim fkCol As String = drs(0)("PK_COLUMN")
                    ' add the original field to avoid issues when this column is part of the compute and order cluase
                    'sql = String.Format("     {0}.{1} as [!{0}_{1}], {2}", tblName, colName, vbCrLf)
                    sql = String.Format("     {0}.{1} as [!{0}_{1}],", tblName, colName)
                    ' add the sub-query
                    ''New Code Added By Vijay Ramteke On June 29, 2010 For Mantis Id 19136
                    ''sql += String.Format("     (select {0} from {1} where {2}.{3} = {4}.{5}) as {6}, {7}", _
                    ''    fkColDescrip, fkTbl, fkTbl, fkCol, tblName, colName, colName, vbCrLf)

                    ''New Code Added By Vijay Ramteke On June 29, 2010 For Mantis Id 19136

                    If colName = "DrivLicStateID" Then
                        sql += String.Format("     (select {0} from {1} where {2}.{3} = {4}.{5}) as DLStateDescrip,", fkColDescrip, fkTbl, fkTbl, fkCol, tblName, colName)
                    ElseIf colName = "TitleIVStatusId" AndAlso fkColDescrip = "Description" AndAlso tblName = "arFASAPChkResults" Then
                        sql = "(SELECT	TOP 1  Description FROM arFASAPChkResults r INNER JOIN syTitleIVSapStatus ts ON ts.Id = r.TitleIVStatusId WHERE r.StuEnrollId = arStuEnrollments.StuEnrollId ORDER BY r.Period desc) as " + fkColDescrip + ","
                    Else
                        If dr("SummaryType") = 32 And tblName = "saTransactions" And colName = "FundSourceId" And fkColDescrip = "FundSourceCode" Then
                            'This can be added to or changed so that you can merge more than just FundSourceCodes in the future but as of now it is hard coded. 
                            ' sql += String.Format("     (ISNULL(STUFF((SELECT ', ' + {0} from {1}  where {2}.{3} = {4}.{5}   FOR XML PATH('') ), 1, 1, ''), 'None')) AS {6} {7} ", fkColDescrip, fkTbl, fkTbl, fkCol, tblName, colName, fkColDescrip, vbCrLf)
                            'sql = String.Format("     (ISNULL(STUFF((SELECT ', ' + {0} from {1}  where {2}.{3} IN ( SELECT FundSourceId FROM saTransactions WHERE  saTransactions.StuEnrollId = arStuEnrollments.StuEnrollId )  FOR XML PATH('') ), 1, 1, ''), 'None')) AS {4}, {5}", fkColDescrip, fkTbl, fkTbl, fkCol, fkColDescrip, vbCrLf)
                            sql = String.Format("     (ISNULL(STUFF((SELECT ', ' + {0} from {1}  where {2}.{3} IN ( SELECT FundSourceId FROM saTransactions WHERE  saTransactions.StuEnrollId = arStuEnrollments.StuEnrollId )  FOR XML PATH('') ), 1, 1, ''), 'None')) AS {4},", fkColDescrip, fkTbl, fkTbl, fkCol, fkColDescrip)
                        Else
                            'sql += String.Format("     (select {0} from {1} where {2}.{3} = {4}.{5}) as {6}, {7}", fkColDescrip, fkTbl, fkTbl, fkCol, tblName, colName, fkColDescrip, vbCrLf)
                            sql += String.Format("     (select {0} from {1} where {2}.{3} = {4}.{5}) as {6},", fkColDescrip, fkTbl, fkTbl, fkCol, tblName, colName, fkColDescrip)

                        End If
                    End If

                End If
            End If
            'NZ 11/6/2012
            If colName = "EducationInstId" _
                AndAlso fkColDescrip = "EducationInstName" _
                AndAlso tblName = "adLeadEducation" Then
                'sql = String.Format("     {0}.{1} as [!{0}_{1}], {2}", tblName, colName, vbCrLf) 'original field
                sql = String.Format("     (SELECT TOP 1 c.CollegeName FROM {0} a INNER JOIN adColleges c ON c.collegeid = a.{1} AND a.EducationInstId = adLeadEducation.EducationInstId UNION SELECT TOP 1 c.HSName FROM {0} a INNER JOIN syInstitutions c ON c.HSId = a.{1} AND a.EducationInstId = adLeadEducation.EducationInstId) AS EducationInstName,", tblName, colName)

                'AK Added on 1/20/2014
                'ElseIf colName = "LedgerBalance" Then
                '    'calculationSql = ""
                '    sql = " CASE WHEN (SUBSTRING(CAST(ISNULL((SELECT SUM(SL.TransAmount) FROM (SELECT - AP.Amount AS TransAmount FROM saTransactions T ,saAppliedPayments AP WHERE T.StuEnrollId IN (Select StuEnrollId FROM arStuEnrollments b WHERE   b.StudentId = arStuEnrollments.StudentId ) AND T.TransactionId = AP.TransactionId  AND IsPosted = 1 AND T.Voided = 0 UNION ALL SELECT    +( ( SELECT TransAmount FROM SaTransactions WHERE TransactionId = T.TransactionId ) + ( SELECT SUM(Amount) FROM SaAppliedpayments WHERE TransactionId = T.TransactionID) ) AS TransAmount  FROM  saTransactions T , saAppliedPayments AP  WHERE T.StuEnrollId IN (Select StuEnrollId FROM arStuEnrollments b WHERE   b.StudentId = arStuEnrollments.StudentId ) AND T.TransactionId = AP.TransactionId AND IsPosted = 1 AND T.Voided = 0 AND ( ( SELECT  TransAmount FROM SaTransactions WHERE TransactionId = T.TransactionId) + ( SELECT  SUM(Amount) FROM SaAppliedpayments WHERE TransactionId = T.TransactionID ) ) <> 0.00   UNION ALL Select T.TransAmount FROM saTransactions T WHERE     T.StuEnrollId IN (Select StuEnrollId FROM  arStuEnrollments b WHERE   b.StudentId = arStuEnrollments.StudentId ) AND IsPosted = 1 AND T.Voided = 0) SL),0)AS varchar),1,1)='-')  THEN REPLACE (CAST(ISNULL((SELECT SUM(SL.TransAmount) FROM (SELECT - AP.Amount AS TransAmount FROM saTransactions T ,saAppliedPayments AP WHERE T.StuEnrollId IN (Select StuEnrollId FROM arStuEnrollments b WHERE   b.StudentId = arStuEnrollments.StudentId ) AND T.TransactionId = AP.TransactionId  AND IsPosted = 1 AND T.Voided = 0 UNION ALL SELECT    +( ( SELECT TransAmount FROM SaTransactions WHERE TransactionId = T.TransactionId ) + ( SELECT SUM(Amount) FROM SaAppliedpayments WHERE TransactionId = T.TransactionID) ) AS TransAmount  FROM  saTransactions T , saAppliedPayments AP  WHERE T.StuEnrollId IN (Select StuEnrollId FROM arStuEnrollments b WHERE   b.StudentId = arStuEnrollments.StudentId ) AND T.TransactionId = AP.TransactionId AND IsPosted = 1 AND T.Voided = 0 AND ( ( SELECT  TransAmount FROM SaTransactions WHERE TransactionId = T.TransactionId) + ( SELECT  SUM(Amount) FROM SaAppliedpayments WHERE TransactionId = T.TransactionID ) ) <> 0.00   UNION ALL Select T.TransAmount FROM saTransactions T WHERE     T.StuEnrollId IN (Select StuEnrollId FROM  arStuEnrollments b WHERE   b.StudentId = arStuEnrollments.StudentId ) AND IsPosted = 1 AND T.Voided = 0) SL),0)AS varchar),'-','$(')+')' Else '$'+CAST(ISNULL((SELECT SUM(SL.TransAmount) FROM (SELECT - AP.Amount AS TransAmount FROM saTransactions T ,saAppliedPayments AP WHERE T.StuEnrollId IN (Select StuEnrollId FROM arStuEnrollments b WHERE   b.StudentId = arStuEnrollments.StudentId ) AND T.TransactionId = AP.TransactionId  AND IsPosted = 1 AND T.Voided = 0 UNION ALL SELECT    +( ( SELECT TransAmount FROM SaTransactions WHERE TransactionId = T.TransactionId ) + ( SELECT SUM(Amount) FROM SaAppliedpayments WHERE TransactionId = T.TransactionID) ) AS TransAmount  FROM  saTransactions T , saAppliedPayments AP  WHERE T.StuEnrollId IN (Select StuEnrollId FROM arStuEnrollments b WHERE   b.StudentId = arStuEnrollments.StudentId ) AND T.TransactionId = AP.TransactionId AND IsPosted = 1 AND T.Voided = 0 AND ( ( SELECT  TransAmount FROM SaTransactions WHERE TransactionId = T.TransactionId) + ( SELECT  SUM(Amount) FROM SaAppliedpayments WHERE TransactionId = T.TransactionID ) ) <> 0.00   UNION ALL Select T.TransAmount FROM saTransactions T WHERE     T.StuEnrollId IN (Select StuEnrollId FROM  arStuEnrollments b WHERE   b.StudentId = arStuEnrollments.StudentId ) AND IsPosted = 1 AND T.Voided = 0) SL),0)AS varchar) end AS LedgerBalance "
                '    sql = String.Format("     {0},{1}", sql, vbCrLf)

            End If

            Return sql
        End Function

#Region "SDF Support"
        ''' <summary>
        ''' Returns a sql sub-query for a SDF field
        ''' NEW for SDF support in AdHoc        
        ''' </summary>
        ''' <param name="dr"></param>
        ''' <param name="dtRelations"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function GetColumnSqlFromSdfField(ByVal dr As DataRow, ByVal dtRelations As DataTable) As String
            Try
                ' Get table and PK that is associated with this SDF
                Dim reportId As String = dr("ReportId").ToString()
                Dim sdfId As String = dr("SDFId").ToString()
                Dim dtTables As DataTable = GetTableListFromSdf(reportId, sdfId)

                ' return empty string if there are no primary keys associated with this SDF
                If dtTables.Rows.Count = 0 Then Return ""
                If dtTables.Rows(0)(0) = "" Or dtTables.Rows(0)(1) = "" Then Return ""

                Dim sb2 As New StringBuilder()
                ' Mantis 9637: only show the first record so that an error does not ocurr if there are multiple records
                sb2.AppendFormat("(select top 1 SDFValue from sySDFModuleValue V where SDFID='{0}' and (", sdfId)
                For Each drp As DataRow In dtTables.Rows
                    sb2.AppendFormat("PgPKID={0}.{1} or ", drp(0).ToString(), drp(1).ToString())
                Next
                sb2.Remove(sb2.Length - 3, 3)
                sb2.Append("))")

                Return sb2.ToString()
            Catch ex As Exception
            End Try

            Return ""
        End Function
        'Protected Shared Function GetColumnSqlFromSDFField(ByVal dr As DataRow, ByVal dtRelations As DataTable) As String
        '    Try
        '        ' Get table and PK that is associated with this SDF
        '        Dim reportId As String = dr("ReportId").ToString()
        '        Dim sdfId As String = dr("SDFId").ToString()
        '        Dim dtTables As DataTable = GetTableListFromSDF(reportId, sdfId)

        '        ' return empty string if there are no primary keys associated with this SDF
        '        If dtTables.Rows.Count = 0 Then Return ""
        '        If dtTables.Rows(0)(0) = "" Or dtTables.Rows(0)(1) = "" Then Return ""
        '        Dim strSDF As String
        '        Dim sb2 As New StringBuilder()
        '        ' Mantis 9637: only show the first record so that an error does not ocurr if there are multiple records
        '        'sb2.AppendFormat("(select top 1 SDFValue from sySDFModuleValue V where SDFID='{0}' and (", sdfId)
        '        ' For Each drp As DataRow In dtTables.Rows
        '        'sb2.AppendFormat("PgPKID={0}.{1} or ", drp(0).ToString(), drp(1).ToString())
        '        'Next

        '        Dim i As Integer = 0
        '        Dim cnt As Integer = dtTables.Rows.Count
        '        For Each drp As DataRow In dtTables.Rows

        '            If i = cnt - 1 Then
        '                strSDF = strSDF & dtTables.Rows(i)(0).ToString() & "." & dtTables.Rows(i)(1).ToString()
        '            Else
        '                strSDF = strSDF & dtTables.Rows(i)(0).ToString() & "." & dtTables.Rows(i)(1).ToString() & ","
        '            End If
        '            i = i + 1
        '        Next


        '        'sb2.Remove(sb2.Length - 3, 3)


        '        'sb2.Append("))")

        '        Return sb2.AppendFormat(" dbo.GetUDFValueColumn('{0}','{1}')", sdfId, strSDF).ToString()
        '    Catch ex As Exception
        '    End Try

        '    Return ""
        'End Function
        Public Shared Function GetColumnSqlFromSdf(ByVal reportId As String, ByVal sdfId As String) As String
            Try
                ' Get table and PK that is associated with this SDF
                Dim dtTables As DataTable = GetTableListFromSdf(reportId, sdfId)

                ' return empty string if there are no primary keys associated with this SDF
                If dtTables.Rows.Count = 0 Then Return ""
                If dtTables.Rows(0)(0) = "" Or dtTables.Rows(0)(1) = "" Then Return ""

                Dim sb2 As New StringBuilder()
                ' Mantis 9637: only show the first record so that an error does not ocurr if there are multiple records
                'sb2.AppendFormat("(select top 1 SDFValue from sySDFModuleValue V where SDFID='{0}' and (", sdfId)
                'For Each drp As DataRow In dtTables.Rows
                'sb2.AppendFormat("PgPKID={0}.{1} or ", drp(0).ToString(), drp(1).ToString())
                'Next
                'Dim i As Integer = 0
                'For Each drp As DataRow In dtTables.Rows
                '    sb2.AppendFormat("dbo.GetUDFValue('{0}',{1}.{2})$", SDFId, dtTables.Rows(i)(0).ToString(), dtTables.Rows(i)(1).ToString()).ToString()
                '    i = i + 1
                'Next
                'sb2.Remove(sb2.Length - 1, 1)
                ''sb2.Append("))")
                'Return sb2.ToString()

                Return sb2.AppendFormat(" dbo.GetUDFValue('{0}',{1}.{2})", sdfId, dtTables.Rows(0)(0).ToString(), dtTables.Rows(0)(1).ToString()).ToString()
            Catch ex As Exception
            End Try

            Return ""
        End Function
        Public Shared Function GetColumnSqlFromSdfForDate(ByVal reportId As String, ByVal sdfId As String) As String
            Try
                ' Get table and PK that is associated with this SDF
                Dim dtTables As DataTable = GetTableListFromSdf(reportId, sdfId)

                ' return empty string if there are no primary keys associated with this SDF
                If dtTables.Rows.Count = 0 Then Return ""
                If dtTables.Rows(0)(0) = "" Or dtTables.Rows(0)(1) = "" Then Return ""

                Dim sb2 As New StringBuilder()
                ' Mantis 9637: only show the first record so that an error does not ocurr if there are multiple records
                'sb2.AppendFormat("(select top 1 SDFValue from sySDFModuleValue V where SDFID='{0}' and (", sdfId)
                'For Each drp As DataRow In dtTables.Rows
                'sb2.AppendFormat("PgPKID={0}.{1} or ", drp(0).ToString(), drp(1).ToString())
                'Next
                'Dim i As Integer = 0
                'For Each drp As DataRow In dtTables.Rows
                '    sb2.AppendFormat("dbo.GetUDFValueForDate('{0}',{1}.{2})$", SDFId, dtTables.Rows(i)(0).ToString(), dtTables.Rows(i)(1).ToString()).ToString()
                '    i = i + 1
                'Next
                'sb2.Remove(sb2.Length - 1, 1)
                ''sb2.Append("))")
                'Return sb2.ToString()
                sb2.Append("CONVERT(DATE,")
                sb2.AppendFormat(" dbo.GetUDFValue('{0}',{1}.{2})", sdfId, dtTables.Rows(0)(0).ToString(), dtTables.Rows(0)(1).ToString()).ToString()
                sb2.Append(",101)")
                Return sb2.ToString
                'Return sb2.AppendFormat(" dbo.GetUDFValue('{0}',{1}.{2})", SDFId, dtTables.Rows(0)(0).ToString(), dtTables.Rows(0)(1).ToString()).ToString()
            Catch ex As Exception
            End Try

            Return ""
        End Function

        Protected Shared Function GetTableListFromSdf(ByVal reportId As String, ByVal sdfId As String) As DataTable
            Try
                ' Get all the tables that are associated with this SDF
                Dim sb As New StringBuilder()
                Dim sb1 As New StringBuilder
                Dim db As New DataAccess
                Dim intEntityId As Integer

                Dim myAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    myAdvAppSettings = New AdvAppSettings
                End If

                db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
                With sb1
                    .Append(" Select Distinct EntityId from syUserResources where ResourceId=? ")
                End With
                db.AddParameter("@ReportId", reportId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                intEntityId = db.RunParamSQLScalar(sb1.ToString)
                sb1.Remove(0, sb1.Length)
                db.ClearParameters()

                With sb
                    .Append("select ")
                    .Append("     T.TblName as TableName, ")
                    .Append("     F.FldName as ColName ")
                    .Append("from ")
                    .Append(" sySDF s,syResourceSDF RSDF,syResources R, ")
                    .Append(" (select ResourceId,RelatedResourceId from syAdvantageResourceRelations where ResourceId=?) ARR, ")
                    .Append(" syTblFlds TF,syTables T,syFields F,syUserResources UR ")
                    .Append(" where ")
                    .Append(" s.SDFId = RSDF.SDFId and RSDF.ResourceId=R.ResourceId and R.ResourceId = ARR.RelatedResourceId ")
                    .Append(" and R.TblFldsId=TF.TblFldsId and TF.TblId = T.TblId and TF.FldId = F.FldId ")
                    .Append(" and ARR.ResourceId=UR.EntityId ")
                    .Append(" and UR.ResourceId=? ")
                    .Append(" and S.SDFId = ? ")
                End With
                db.AddParameter("@EntityId", intEntityId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ResourceId", reportId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@SDFId", sdfId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Return db.RunParamSQLDataSet(sb.ToString()).Tables(0)
            Catch ex As Exception
            End Try

            Return Nothing
        End Function
#End Region

        ''' <summary>
        ''' Retrieve the list of order by's given a DataTable.  The format of the
        ''' DataTable must contain columns "TableName", "FieldName", "SortBy" and "GroupBy"
        ''' For sql, the order by statement needs all columns that will participate
        ''' in the group by / compute portion of the query.  So, the returned string
        ''' is {All of the group by columns} + {All of the order by columns}
        ''' </summary>
        ''' <param name="dtFields"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetOrderBySql(ByVal dtFields As DataTable, ByVal dtRelations As DataTable) As String
            Dim sb1 As New StringBuilder() ' list of fields in the order by
            Dim sb2 As New StringBuilder() ' list of fields in the group by
            For Each dr As DataRow In dtFields.Rows
                Dim tbl As String = dr("TableName").ToString()
                Dim col As String = dr("FieldName").ToString()
                Dim calcSql As String = dr("CalculationSQL").ToString()


                'jg-08/26/2013 - check if this is a calculated field using "as" in the field name
                'if so, strip out everything before the alias and only use the alias name
                If col.ToLower().Contains(" as ") Then
                    col = col.Substring(col.ToLower().LastIndexOf(" as ", StringComparison.Ordinal) + 4)
                End If

                Dim orderby As Boolean = dr("SortBy")
                Dim groupby As Boolean = dr("GroupBy")
                Dim UDFGroupBy As Boolean = False
                UDFGroupBy = IsUdfGroupBy(dtFields)
                If orderby Then
                    If calcSql <> "" Then
                        If UDFGroupBy Then
                            sb1.Append(String.Format("UDF.[{0}], ", col))
                        Else
                            sb1.Append(String.Format("[{0}], ", col))
                        End If
                        sb1.Replace("[[", "[")
                        sb1.Replace("]]", "]")
                    ElseIf tbl <> "" Then
                        If UDFGroupBy Then
                            sb1.Append(String.Format("UDF.{0}, ", col))
                        Else
                            sb1.Append(String.Format("{0}.{1}, ", tbl, col))
                        End If
                    Else ' this is an SDF column
                        If UDFGroupBy Then
                            sb1.Append(String.Format("UDF.[{0}], ", col))
                        Else
                            sb1.Append(String.Format("[{0}], ", col))
                        End If
                        sb1.Replace("[[", "[")
                        sb1.Replace("]]", "]")
                    End If
                ElseIf groupby Then
                    If tbl <> "" Then
                        sb2.Append(String.Format("{0}.{1}, ", tbl, col))
                    Else
                        sb2.Append(String.Format("[{0}], ", col))
                        sb2.Replace("[[", "[")
                        sb2.Replace("]]", "]")
                    End If
                End If
            Next
            sb2.Append(sb1.ToString())
            If sb2.Length > 2 Then sb2.Chars(sb2.Length - 2) = " " ' remove the last comma
            Return sb2.ToString()
        End Function

        ''' <summary>
        ''' NEW: Return a list of table dependencies that show
        ''' - what tables need to be added given Table 1 and Table 2.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function GetTableDependencies() As DataTable
            Try
                Const sql As String = "select table1, table2, depTable from syTblDependencies"
                '   connect to the database
                Dim db As New DataAccess

                Dim myAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    myAdvAppSettings = New AdvAppSettings
                End If

                db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
                Return db.RunParamSQLDataSet(sql).Tables(0)
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

        ''' <summary>
        ''' Returns a string with the list of distinct tables contained within dtFields.
        ''' The returned value is comma-delimitted suitable to be used in the 
        ''' FROM portion of a sql statement.
        ''' </summary>
        ''' <param name="dtFields"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function GetTablesSql(ByVal dtFields As DataTable) As String
            Dim fromsLookup As New Dictionary(Of String, Boolean)
            Dim b As Boolean
            For Each dr As DataRow In dtFields.Rows
                Dim tblName As String = dr("TableName").ToString()
                If tblName <> "" AndAlso Not fromsLookup.TryGetValue(tblName, b) Then
                    fromsLookup.Add(tblName, True)
                ElseIf dr("SDFId").ToString() <> "" Then
                    ' 10/24/06 - SDF Support - Add list of tables that belong to our SDF's
                    Dim reportId As String = dr("ReportId").ToString()
                    Dim sdfId As String = dr("SDFId").ToString()
                    Dim dtTablesSdf As DataTable = GetTableListFromSdf(reportId, sdfId)
                    If Not dtTablesSdf Is Nothing AndAlso dtTablesSdf.Rows.Count > 0 Then
                        For Each dr2 As DataRow In dtTablesSdf.Rows
                            tblName = dr2("TableName").ToString()
                            If tblName <> "" AndAlso Not fromsLookup.TryGetValue(tblName, b) Then
                                fromsLookup.Add(tblName, True)
                            End If
                        Next
                    End If
                End If
            Next

            ' *** NEW ***
            ' Fix the problem when we have columns from Table A and columns from Table C, but A and C are related by B
            Dim dtDependTables As DataTable = GetTableDependencies()
            Dim fromsLookup2 As New Dictionary(Of String, Boolean)
            If Not dtDependTables Is Nothing Then
                For Each tbl1 As String In fromsLookup.Keys
                    For Each tbl2 As String In fromsLookup.Keys
                        If tbl1 <> "" AndAlso tbl2 <> "" AndAlso tbl1 <> tbl2 Then
                            ' look for all tables that need to be added given table1 and table2
                            Dim sql As String = String.Format("table1 = '{0}' and table2 = '{1}'", tbl1, tbl2)
                            Dim drs() As DataRow = dtDependTables.Select(sql)
                            For Each dr As DataRow In drs
                                Dim depTbl As String = dr("depTable").ToString()
                                ' 9/25 - BEN - only add the table if it is not in the original FromsLookup dictionary.
                                ' This fixes the problem of having a table added twice
                                If Not fromsLookup.TryGetValue(depTbl, b) AndAlso Not fromsLookup2.TryGetValue(depTbl, b) Then
                                    fromsLookup2.Add(depTbl, True)
                                End If
                            Next
                        End If
                    Next
                Next
            End If

            ' *** NEW - Moved this from above so that it can work on the complete list of tables ***
            ' create the comma-delimitted list of tables
            Dim sb As New StringBuilder()
            For Each tbl As String In fromsLookup.Keys
                sb.Append(tbl + ", ")
            Next
            For Each tbl As String In fromsLookup2.Keys
                sb.Append(tbl + ", ")
            Next
            sb.Chars(sb.Length - 2) = " " ' remove the last ","

            Return sb.ToString()
        End Function
        Protected Shared Function GetFromSql(ByVal db As DataAccess, ByVal tableList As String,
                                               ByVal dtRelations As DataTable) As String
            ' Get a distinct list of tables
            Dim tables() As String = tableList.Split(",")
            Dim sbFrom As New StringBuilder()
            sbFrom.Append(tables(0))
            Dim reverseTable = tables(0).Trim()
            ' use all the foreign keys for each table  to build the on clause of the join
            For Each t1 As String In tables
                For Each t2 As String In tables
                    If t1 <> "" AndAlso t2 <> "" AndAlso t1 <> t2 Then
                        Dim tbl1 As String = t1.Trim()
                        Dim tbl2 As String = t2.Trim()
                        ' Find and create a data relation between tbl1 and tbl2
                        Dim tmpSql As String = String.Format("(FK_TABLE='{0}' AND PK_TABLE='{1}')", tbl1, tbl2)
                        Dim drs() As DataRow = dtRelations.Select(tmpSql)
                        If Not drs Is Nothing AndAlso drs.Length > 0 Then
                            Dim col1 As String = drs(0)("FK_COLUMN")
                            Dim col2 As String = drs(0)("PK_COLUMN")
                            Dim tsql As String
                            If reverseTable.Equals(tbl1) Then
                                reverseTable = tbl2
                                If tbl2.Equals("arStuEnrollments") Or tbl2.Equals("arStudent") Then
                                    tsql = String.Format("inner join {0} on  {1}.{2} = {3}.{4} {5}", tbl2, tbl1, col1, tbl2, col2, vbCrLf)
                                Else
                                    tsql = String.Format("left join {0} on  {1}.{2} = {3}.{4} {5}", tbl2, tbl1, col1, tbl2, col2, vbCrLf)
                                End If
                            Else
                                reverseTable = tbl1
                                If tbl1.Equals("arStuEnrollments") Or tbl1.Equals("arStudent") Then
                                    tsql = String.Format("inner join {0} on  {1}.{2} = {3}.{4} {5}", tbl1, tbl1, col1, tbl2, col2, vbCrLf)
                                Else
                                    tsql = String.Format("left join {0} on  {1}.{2} = {3}.{4} {5}", tbl1, tbl1, col1, tbl2, col2, vbCrLf)
                                End If
                            End If
                            sbFrom.Append("    " + tsql)
                        End If
                    End If
                Next
            Next

            'If sbFrom.Length > 3 Then
            '    sbFrom.Remove(sbFrom.Length - 2, 2) ' remove the last comma and cr/lf
            'End If
            Return sbFrom.ToString()
        End Function
        ''' <summary>
        ''' Returns the where clause for the report.
        ''' The first part of this function builds a where clause from the FK relations
        ''' between all the tables used in the report.
        ''' The second part of this function builds the where clause from the list of
        ''' parameters passed to the report.  It is important that the list of parameters
        ''' has the same length as the the actual number of parameters define in the report.  
        ''' Parameters not specified by the user should still be in the params array but with a value
        ''' of nothing.  For ex, param(3) = Nothing would mean the 3rd paramter was not specified by the user.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function GetWheresSql(ByVal db As DataAccess, ByVal tableList As String,
                            ByVal dtRelations As DataTable) As String
            ' Get a distinct list of tables
            Dim tables() As String = tableList.Split(",")
            Dim sbWhere As New StringBuilder()

            ' Get the first part of the where clause which is built by
            ' by using all the foreign keys for each table            
            'Dim baseTbl As String = tables(0)
            For Each t1 As String In tables
                For Each t2 As String In tables
                    If t1 <> "" AndAlso t2 <> "" AndAlso t1 <> t2 Then
                        Dim tbl1 As String = t1.Trim()
                        Dim tbl2 As String = t2.Trim()
                        ' Find and create a data relation between tbl1 and tbl2
                        Dim tmpSql As String = String.Format("(FK_TABLE='{0}' AND PK_TABLE='{1}')", tbl1, tbl2)
                        Dim drs() As DataRow = dtRelations.Select(tmpSql)
                        If Not drs Is Nothing AndAlso drs.Length > 0 Then
                            Dim col1 As String = drs(0)("FK_COLUMN")
                            Dim col2 As String = drs(0)("PK_COLUMN")
                            Dim tsql As String = String.Format("{0}.{1} = {2}.{3} {4}", tbl1, col1, tbl2, col2, vbCrLf)
                            If sbWhere.Length = 0 Then
                                sbWhere.Append("    " + tsql)
                            Else
                                sbWhere.Append("    AND " + tsql)
                            End If
                        End If
                    End If
                Next
            Next

            If sbWhere.Length > 3 Then
                sbWhere.Remove(sbWhere.Length - 2, 2) ' remove the last comma and cr/lf
            End If
            Return sbWhere.ToString()
        End Function

        ''' <summary>
        ''' Returns a where clause with a restriction on the Campus Group
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="tableList"></param>
        ''' <param name="dtRelations"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function GetCampGroupWheresSql(ByVal info As AdHocRptInfo, ByVal tableList As String,
                            ByVal dtRelations As DataTable) As String
            ' Get a distinct list of tables
            Dim tables() As String = tableList.Split(",")
            Dim sbWhere As New StringBuilder()
            ' Get the first part of the where clause which is built by
            ' by using all the foreign keys for each table            
            'Dim baseTbl As String = tables(0)
            For Each t1 As String In tables
                Dim tbl1 As String = t1.Trim()
                Const tbl2 As String = "syCampuses"
                ' Find and create a data relation between tbl1 and tbl2
                Dim tmpSql As String = String.Format("(FK_TABLE='{0}' AND PK_TABLE='{1}')", tbl1, tbl2)
                Dim drs() As DataRow = dtRelations.Select(tmpSql)
                If Not drs Is Nothing AndAlso drs.Length > 0 Then
                    Dim col1 As String = drs(0)("FK_COLUMN")
                    'Dim col2 As String = drs(0)("PK_COLUMN")
                    Dim tsql As String = String.Format("{0}.{1} in (select distinct CG_.CampusId from syCmpGrpCmps CG_, syCampuses C_ where CG_.CampusId=C_.CampusId and CG_.CampGrpId='{2}') {3}", tbl1, col1, info.CampGrpId, vbCrLf)
                    If sbWhere.Length = 0 Then
                        sbWhere.Append("    " + tsql)
                    Else
                        sbWhere.Append("    AND " + tsql)
                    End If
                End If
            Next

            If sbWhere.Length > 3 Then
                sbWhere.Remove(sbWhere.Length - 2, 2) ' remove the last comma and cr/lf
            End If
            Return sbWhere.ToString()
        End Function

        ''' <summary>
        ''' Returns the COMPUTE BY part of the query
        ''' </summary>
        ''' <param name="dtFields"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function GetComputeSql(ByVal dtFields As DataTable) As String
            Dim sb1 As New StringBuilder()
            Dim sb2 As New StringBuilder()
            'Dim tbl1 As String = dtFields.Rows(0)("TableName").ToString()
            'Dim col1 As String = dtFields.Rows(0)("FieldName").ToString()
            For Each dr As DataRow In dtFields.Rows
                Dim tblName As String = dr("TableName").ToString()
                Dim colName As String = dr("FieldName").ToString()
                Dim sqlCalculation As String = dr("CalculationSQL").ToString()
                Dim groupby As Boolean = dr("GroupBy")
                Dim summaryby As Boolean = dr("SummaryType")

                If (String.IsNullOrEmpty(sqlCalculation)) Then
                    'Modified by Balaji to separate compute columns and group by columns
                    If summaryby AndAlso tblName <> "" Then

                        sb1.Append(String.Format("count({0}.{1}), ", tblName, colName))
                    End If
                    'for UDF



                    If groupby AndAlso tblName <> "" Then

                        ' force the count stat to be on the first column to solve
                        ' the problem if the group by column is a sub-query
                        'Modified to bring all columns that were selected to compute by
                        sb2.Append(String.Format("{0}.{1}, ", tblName, colName))
                    End If
                End If

            Next

            Dim res As String = ""
            If sb1.Length > 2 Then
                sb1.Chars(sb1.Length - 2) = " " ' remove the last comma                
                If sb2.Length > 2 Then
                    sb2.Chars(sb2.Length - 2) = " " ' remove the last comma
                    res = "compute " + sb1.ToString() + vbCrLf
                    res += "by " + sb2.ToString()
                End If
            End If
            Return res
        End Function
        'GetComputeSqlForUDF
        Protected Shared Function GetComputeSqlForUdf(ByVal dtFields As DataTable) As String
            Dim sb1 As New StringBuilder()
            Dim sb2 As New StringBuilder()
            'Dim tbl1 As String = dtFields.Rows(0)("TableName").ToString()
            'Dim col1 As String = dtFields.Rows(0)("FieldName").ToString()
            For Each dr As DataRow In dtFields.Rows
                Dim tblName As String = dr("TableName").ToString()
                Dim colName As String = dr("FieldName").ToString()
                Dim groupby As Boolean = dr("GroupBy")
                Dim summaryby As Boolean = dr("SummaryType")
                Dim isUdf As Boolean = dr("IsUDF")

                If summaryby And tblName = "" And isUdf Then

                    sb1.Append(String.Format("count([{0}]), ", colName))
                End If
                If groupby And tblName = "" And isUdf Then

                    ' force the count stat to be on the first column to solve
                    ' the problem if the group by column is a sub-query
                    'Modified to bring all columns that were selected to compute by
                    sb2.Append(String.Format("[{0}], ", colName))
                End If
            Next

            Dim res As String = ""
            If sb1.Length > 2 Then
                sb1.Chars(sb1.Length - 2) = " " ' remove the last comma                
                If sb2.Length > 2 Then
                    sb2.Chars(sb2.Length - 2) = " " ' remove the last comma
                    res = "compute " + sb1.ToString() + vbCrLf
                    res += "by " + sb2.ToString()
                End If
            End If
            Return res
        End Function

        Protected Shared Function IsUdfGroupBy(ByVal dtFields As DataTable) As Boolean

            For Each dr As DataRow In dtFields.Rows
                'Dim tblName As String = dr("TableName").ToString()
                'Dim colName As String = dr("FieldName").ToString()
                Dim groupby As Boolean = dr("GroupBy")
                Dim summaryby As Boolean = dr("SummaryType")
                Dim isUdf As Boolean = dr("IsUDF")
                If isUdf And groupby And summaryby Then Return True

            Next
            Return False

        End Function





        ''' <summary>
        ''' Returns all the foreign keys/columns for all the tables within the passed in comma delimitted tableList
        ''' </summary>
        ''' <param name="tableList"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function GetTableRelations(ByVal tableList As String) As DataTable
            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            Dim tables() As String = tableList.Split(",")
            Dim sbTableList As New StringBuilder()
            For Each table As String In tables
                Dim tmp As String = String.Format("'{0}'", table.Trim())
                If sbTableList.Length = 0 Then
                    sbTableList.Append(tmp)
                Else
                    sbTableList.Append(", " + tmp)
                End If
            Next

            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("	    fk.TABLE_NAME as FK_TABLE, ")
            sb.Append("     fk.COLUMN_NAME as FK_COLUMN, ")
            sb.Append("     pk.TABLE_NAME AS PK_TABLE, ")
            sb.Append("     pk.COLUMN_NAME AS PK_COLUMN ")
            sb.Append("FROM ")
            sb.Append("     INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc INNER JOIN ")
            sb.Append("     INFORMATION_SCHEMA.KEY_COLUMN_USAGE pk ON ")
            sb.Append("     rc.UNIQUE_CONSTRAINT_NAME = pk.CONSTRAINT_NAME INNER JOIN ")
            sb.Append("     INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE fk ON ")
            sb.Append("     rc.CONSTRAINT_NAME = fk.CONSTRAINT_NAME ")
            sb.Append("WHERE ")
            sb.Append("     fk.TABLE_NAME in (" + sbTableList.ToString() + ")")
            'sb.Append(" pk.TABLE_NAME in (" + sbTableList.ToString() + ")" + vbCrLf)
            'sb.Append(" and fk.TABLE_NAME in (" + sbTableList.ToString() + ")" + vbCrLf)

            Dim dsTemp As DataSet = db.RunParamSQLDataSet(sb.ToString)

            Dim sb2 As New StringBuilder
            sb2.Append("SELECT FkTable AS FK_TABLE,FkColumn AS FK_COLUMN,PkTable AS PK_TABLE,PkColumn AS PK_COLUMN FROM syRptAdhocRelations WHERE FkTable IN (")
            sb2.Append(sbTableList.ToString() + ")")
            Dim dsTemp2 As DataSet = db.RunParamSQLDataSet(sb2.ToString)
            dsTemp.Merge(dsTemp2.Tables(0))
            Return dsTemp.Tables(0)
        End Function

        ''' <summary>
        ''' Returns the number of "?" in the sql string
        ''' </summary>
        ''' <param name="s"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function GetNumParams(ByVal s As String) As Integer
            Dim num As Integer = 0
            For Each c As Char In s
                If c = "?" Then
                    num += 1
                End If
            Next
            Return num
        End Function

        ''' <summary>
        ''' Apply masks to phone, ssn and zip
        ''' 11/9/06 (BEN) - Fix to apply masks to all tables
        ''' </summary>
        ''' <param name="ds"></param>
        ''' <param name="sql"></param>
        ''' <remarks></remarks>
        Public Shared Sub ApplyMasks(ByVal ds As DataSet, ByVal sql As String)
            Dim facInputMasksDB As New InputMasksDB
            Dim dr As DataRow
            'Get the mask for phone numbers
            'Item 2 is Phone

            Dim ssnMask As String = facInputMasksDB.GetInputMaskForItem(1)
            Dim phoneMask As String = facInputMasksDB.GetInputMaskForItem(2)
            Dim zipMask As String = facInputMasksDB.GetInputMaskForItem(3)

            For Each dt As DataTable In ds.Tables

                ApplyPhoneMask(dt, "Phone", phoneMask)
                ApplyPhoneMask(dt, "PhoneDefault", phoneMask)

                If Not dt.Columns("SSN") Is Nothing Then
                    For Each dr In dt.Rows
                        If dr("SSN").ToString.Length >= 9 Then
                            dr("SSN") = ApplyMask(ssnMask, dr("SSN"))
                        ElseIf dr("SSN").ToString.Length >= 1 And dr("SSN").ToString.Length < 9 Then
                            dr("SSN") = dr("SSN")
                        Else
                            dr("SSN") = ""
                        End If
                    Next
                End If

                If Not dt.Columns("Zip") Is Nothing Then
                    For Each dr In dt.Rows
                        If dr("Zip").ToString.Length >= 5 Then
                            dr("Zip") = ApplyMask(zipMask, dr("Zip"))
                        ElseIf dr("Zip").ToString.Length >= 1 And dr("Zip").ToString.Length < 5 Then
                            dr("Zip") = dr("Zip")
                        Else
                            dr("Zip") = ""
                        End If
                    Next
                End If
            Next
        End Sub

        ' Apply Phone mask to defined column in parameter.
        Private Shared Sub ApplyPhoneMask(ByRef dt As DataTable, ByVal fieldName As String, ByVal phoneMask As String)
            If Not dt.Columns(fieldName) Is Nothing Then
                For Each dr As DataRow In dt.Rows
                    If dr(fieldName).ToString.Length = 10 Then
                        dr(fieldName) = ApplyMask(phoneMask, dr(fieldName))
                    ElseIf ((dr(fieldName).ToString.Length >= 1 And dr(fieldName).ToString.Length < 10) Or (dr(fieldName).ToString.Length > 10)) Then
                        dr(fieldName) = dr(fieldName)
                    Else
                        dr(fieldName) = ""
                    End If
                Next
            End If
        End Sub

        Public Shared Function ApplyMask(ByVal strMask As String, ByVal strVal As String) As String
            Dim arrMask As New ArrayList
            Dim arrVal As New ArrayList
            Dim chr As Char
            'Dim strMaskChars As String = "#&?AULH\"
            Const strMaskChars As String = "#"
            Dim intCounter As Integer
            Dim intCounter2 As Integer
            Dim strCorrVal As String
            Dim strReturn As String
            Dim strPrev As String = ""

            'Add each character in strMask to arrMask
            'Modified by Balaji on 2/18/2005 (If statement added)
            If Not strMask = "" Then
                For Each chr In strMask
                    arrMask.Add(chr.ToString)
                Next
            End If

            'Add each character in strVal to arrVal
            For Each chr In strVal
                arrVal.Add(chr)
            Next

            If arrVal.Count >= 1 Then
                'We need to loop through the arrMask ArrayList and see if each item is a
                'mask character. If it is, we can use intCounter to get the corresponding
                'value from the arrVal ArrayList. Note that we ignore the \ character unless
                'it was preceded by another \. This means that at the end if we have \\ it
                'should be replaced by a \ and if we have a \ then it should be replaced
                'with an empty space.
                For intCounter2 = 0 To arrMask.Count - 1
                    If arrMask(intCounter2).ToString() = "\" And strPrev <> "\" Then
                        'ignore
                    ElseIf strMaskChars.IndexOf(arrMask(intCounter2).ToString(), StringComparison.Ordinal) <> -1 Then
                        strCorrVal = arrVal(intCounter).ToString()
                        arrMask(intCounter2) = strCorrVal
                        intCounter += 1
                    End If
                    strPrev = arrMask(intCounter2).ToString()
                Next

                Dim strChars(arrMask.Count) As String
                arrMask.CopyTo(strChars)
                strReturn = String.Join("", strChars)


                If strReturn.IndexOf("\\", StringComparison.Ordinal) <> -1 Then
                    strReturn = strReturn.Replace("\\", "\")

                ElseIf strReturn.IndexOf("\", StringComparison.Ordinal) <> -1 Then
                    strReturn = strReturn.Replace("\", "")
                Else
                    Return strReturn
                End If

                Return strReturn
            Else
                Return ""
            End If
        End Function
#End Region
#End Region

        Shared Function GetCalculatedfieldTblFldId() As DataTable
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim ds As DataSet

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            sb.Append("SELECT TblFldsId FROM syTblFlds ")
            sb.Append("JOIN syFields ON syFields.FldId = syTblFlds.FldId ")
            sb.Append("JOIN syFieldCalculation ON syFieldCalculation.FldId = syFields.FldId")
            Try
                db.OpenConnection()
                ds = db.RunParamSQLDataSet(sb.ToString)
                Return ds.Tables(0)
            Finally
                'Close Connection
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
        End Function

    End Class
#End Region
End Namespace

