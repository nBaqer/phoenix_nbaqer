' ===============================================================================
' FacultyDB.vb
' DataAccess classes for Faculty
' ===============================================================================
' Copyright (C) 2006,2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Text
Imports System.Data
Imports FAME.AdvantageV1.Common.AR
Imports FAME.AdvantageV1.Common.FA
Imports FAME.Advantage.Common

Namespace FA

    Public Class FacultyDB
        ''' <summary>
        ''' Get all active terms
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetTerms(ByVal ShowEndedTerms As Boolean) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT   T.TermId as Id, " + vbCrLf)
            sb.Append("         T.TermDescrip as Descrip " + vbCrLf)
            sb.Append("FROM     arTerm T, syStatuses ST " + vbCrLf)
            sb.Append("WHERE    T.StatusId = ST.StatusId " + vbCrLf)
            If Not ShowEndedTerms Then
                sb.Append("         AND T.StartDate <= GetDate() " + vbCrLf)
            End If
            sb.Append("         AND ST.Status = 'Active' " + vbCrLf)
            sb.Append("ORDER BY T.StartDate, T.TermDescrip " + vbCrLf)

            '   return dataset
            Return db.RunSQLDataSet(sb.ToString)
        End Function

        Public Shared Function GetClassSectionsForTerm(ByVal CampusId As String, ByVal TermId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     C.ClsSectionId as ID, " + vbCrLf)
            sb.Append("     C.ReqId, " + vbCrLf)
            sb.Append("     R.Code as ReqCode, " + vbCrLf)
            sb.Append("     C.ClsSection, " + vbCrLf)
            sb.Append("     R.Descrip, " + vbCrLf)
            sb.Append("     R.CourseId, " + vbCrLf)
            sb.Append("     R.Credits, " + vbCrLf)
            sb.Append("     R.Hours, " + vbCrLf)
            sb.Append("     C.StartDate, " + vbCrLf)
            sb.Append("     C.EndDate, " + vbCrLf)
            sb.Append("     C.MaxStud, " + vbCrLf)
            sb.Append("     C.InstructorId, " + vbCrLf)
            sb.Append("     (select FullName from syUsers t where t.UserId=C.InstructorId) as InstructorName, " + vbCrLf)
            sb.Append("     1 as Active " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arClassSections C, arReqs R " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     C.ReqId = R.ReqId " + vbCrLf)
            If CampusId IsNot Nothing AndAlso CampusId <> "" Then
                sb.AppendFormat("   and C.CampusId = '{0}' {1}", CampusId, vbCrLf)
            End If
            If TermId IsNot Nothing AndAlso TermId <> "" Then
                sb.AppendFormat("   and C.TermId = '{0}' {1}", TermId, vbCrLf)
            End If
            sb.Append("ORDER BY R.Descrip, C.StartDate, C.EndDate " + vbCrLf)

            '   return dataset
            Return db.RunSQLDataSet(sb.ToString)
        End Function

        Public Shared Function GetClassSectionsWithLabsForTerm(ByVal CampusId As String, ByVal TermId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     C.ClsSectionId as ID, " + vbCrLf)
            sb.Append("     C.ReqId, " + vbCrLf)
            sb.Append("     R.Code as ReqCode, " + vbCrLf)
            sb.Append("     C.ClsSection, " + vbCrLf)
            ''ClassSection added to the Description
            ''Added by Saraswathi Lakshmanan on Nov 3rd 2008
            sb.Append("    '(' + R.Code + ') ' +  R.Descrip + ' - ' + C.ClsSection as Descrip, " + vbCrLf)
            sb.Append("     R.CourseId, " + vbCrLf)
            sb.Append("     R.Credits, " + vbCrLf)
            sb.Append("     R.Hours, " + vbCrLf)
            sb.Append("     C.StartDate, " + vbCrLf)
            sb.Append("     C.EndDate, " + vbCrLf)
            sb.Append("     C.MaxStud, " + vbCrLf)
            sb.Append("     C.InstructorId, " + vbCrLf)
            sb.Append("     (select FullName from syUsers t where t.UserId=C.InstructorId) as InstructorName, " + vbCrLf)
            sb.Append("     1 as Active,R.Descrip AS descrip1 " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arClassSections C, arReqs R " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     C.ReqId = R.ReqId " + vbCrLf)
            sb.Append("     and (select count(*) " + vbCrLf)
            sb.Append("         from arGrdBkWeights GBW, arGrdBkWgtDetails GBWD, arGrdComponentTypes GDT " + vbCrLf)
            sb.Append("         where " + vbCrLf)
            sb.Append("             C.ReqId = GBW.ReqId " + vbCrLf)
            sb.Append("             and GBW.InstrGrdBkWgtId = GBWD.InstrGrdBkWgtId " + vbCrLf)
            sb.Append("             and GBWD.GrdComponentTypeId = GDT.GrdComponentTypeId  " + vbCrLf)
            sb.Append("             and GBWD.Number > 0 " + vbCrLf)
            sb.Append("             and GDT.SysComponentTypeId in (500,503) " + vbCrLf)
            sb.Append("             and (GBW.EffectiveDate <= getdate() or " + vbCrLf)
            sb.Append("             GBW.EffectiveDate = (select max(t.EffectiveDate) from arGrdBkWeights t where t.ReqId=C.ReqId and t.EffectiveDate <= getdate())) " + vbCrLf)
            sb.Append("         ) > 0 " + vbCrLf)
            If CampusId IsNot Nothing AndAlso CampusId <> "" Then
                sb.AppendFormat("   and C.CampusId = '{0}' {1}", CampusId, vbCrLf)
            End If
            If TermId IsNot Nothing AndAlso TermId <> "" Then
                sb.AppendFormat("   and C.TermId = '{0}' {1}", TermId, vbCrLf)
            End If
            sb.Append("ORDER BY descrip1, R.Code, C.StartDate, C.EndDate " + vbCrLf)

            '   return dataset
            Return db.RunSQLDataSet(sb.ToString)
        End Function
    End Class

    Public Class GrdPostingsDB
        ''' <summary>
        ''' Returns detail information for a specific lab
        ''' </summary>
        ''' <param name="Id"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetLabDetails(ByVal Id As String, ByVal term As String) As DataTable
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     CS.StartDate, " + vbCrLf)
            sb.Append("     CS.EndDate, " + vbCrLf)
            sb.Append("     (select FullName from syUsers t where t.UserId=CS.InstructorId) as Instructor, " + vbCrLf)
            sb.Append("     GBWD.Number, " + vbCrLf)
            sb.Append("     GDT.Descrip, " + vbCrLf)
            sb.Append("     GDT.SysComponentTypeId " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arClassSections CS, " + vbCrLf)
            sb.Append("     arGrdBkWeights GBW, " + vbCrLf)
            sb.Append("     arGrdBkWgtDetails GBWD, " + vbCrLf)
            sb.Append("     arGrdComponentTypes GDT " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     CS.ReqId = GBW.ReqId " + vbCrLf)
            sb.Append("     and GBW.InstrGrdBkWgtId = GBWD.InstrGrdBkWgtId " + vbCrLf)
            sb.Append("     and GBWD.GrdComponentTypeId = GDT.GrdComponentTypeId " + vbCrLf)

            sb.AppendFormat("   and InstrGrdBkWgtDetailId = '{0}' {1}", Id, vbCrLf)
            sb.AppendFormat("   and CS.TermId = '{0}' {1}", term, vbCrLf)

            '   return dataset
            Return db.RunSQLDataSet(sb.ToString).Tables(0)
        End Function

        Public Shared Sub SetIsCourseCompleted(ByVal StuEnrollId As String, ByVal ClassSectionId As String)
            Dim db As New SQLDataAccess
            Dim ds As New DataSet

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                db.OpenConnection()
                db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@ClsSectionId", New Guid(ClassSectionId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery_SP("USP_IsCourseCompleted_ForClinicWorkorLabHours")
            Catch ex As System.Exception

            Finally
                db.CloseConnection()
            End Try
        End Sub

        ''' <summary>
        ''' Returns all the students that are in the given class section
        ''' </summary>
        ''' <param name="ClsSectionId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetStudensForClassSection(ByVal ClsSectionId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     S.StudentId, " + vbCrLf)
            sb.Append("     SE.StuEnrollId, " + vbCrLf)
            sb.Append("     S.FirstName, " + vbCrLf)
            sb.Append("     S.LastName, " + vbCrLf)
            sb.Append("     S.MiddleName, " + vbCrLf)
            sb.Append("     coalesce(Case len(S.SSN) When 9 then '*******' + substring(S.SSN,6,4) else ' ' end, ' ') as SSN, " + vbCrLf)
            sb.Append("     SE.StartDate, " + vbCrLf)
            sb.Append("     (select StatusCodeDescrip from syStatusCodes SC where SE.StatusCodeId=SC.StatusCodeId) as StatusCodeDescrip, " + vbCrLf)
            sb.Append("     PV.PrgVerDescrip " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arResults R, " + vbCrLf)
            sb.Append("     arClassSections CS, " + vbCrLf)
            sb.Append("     arStuEnrollments SE, " + vbCrLf)
            sb.Append("     arStudent S, " + vbCrLf)
            sb.Append("     arPrgVersions PV " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     R.TestId = CS.ClsSectionId " + vbCrLf)
            sb.Append("     and R.StuEnrollId = SE.StuEnrollId " + vbCrLf)
            sb.Append("     and SE.StudentId = S.StudentId " + vbCrLf)
            sb.Append("     and SE.PrgVerId = PV.PrgVerId " + vbCrLf)
            sb.AppendFormat("     and CS.ClsSectionId = '{0}' {1} ", ClsSectionId, vbCrLf)
            sb.Append("ORDER BY S.LastName, S.FirstName, S.MiddleName " + vbCrLf)
            '   return dataset
            Return db.RunSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' returns all labs for the given class section
        ''' </summary>
        ''' <param name="ClsSectionId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ''' 
        Public Shared Function GetRecentDate(ByVal ClsSectionId As String) As String
            Dim db As New DataAccess
            Dim maxDate As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)

            sb.Append("     max(GBW.EffectiveDate) " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arClassSections CS, arGrdBkWeights GBW, arGrdBkWgtDetails GBWD, arGrdComponentTypes GDT " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     CS.ReqId = GBW.ReqId " + vbCrLf)
            sb.Append("     and GBW.InstrGrdBkWgtId = GBWD.InstrGrdBkWgtId " + vbCrLf)
            sb.Append("     and GBWD.GrdComponentTypeId = GDT.GrdComponentTypeId " + vbCrLf)
            sb.Append("     and GBWD.Number > 0 " + vbCrLf)
            sb.AppendFormat("   and GDT.SysComponentTypeId not in ({0},{1}) {2}", _
                            CType(SysGradeBookComponentTypes.LabHours, Integer), _
                            CType(SysGradeBookComponentTypes.LabWork, Integer), _
                            vbCrLf)
            sb.Append("    and GBW.EffectiveDate <= CS.StartDate " + vbCrLf)
            'sb.Append("     and (GBW.EffectiveDate <= getdate() or " + vbCrLf)
            'sb.Append("         GBW.EffectiveDate = (select max(t.EffectiveDate) from arGrdBkWeights t where t.ReqId=CS.ReqId and t.EffectiveDate <= getdate())) " + vbCrLf)
            sb.AppendFormat("   and CS.ClsSectionId = '{0}' {1}", ClsSectionId, vbCrLf)
            'sb.Append("ORDER BY GBWD.Descrip " + vbCrLf)
            '   return dataset
            maxDate = db.RunSQLScalar(sb.ToString).ToString
            If maxDate = "" Then
                sb.Remove(0, sb.Length)
                sb.Append("SELECT " + vbCrLf)

                sb.Append("     max(GBW.EffectiveDate) " + vbCrLf)
                sb.Append("FROM " + vbCrLf)
                sb.Append("     arClassSections CS, arGrdBkWeights GBW, arGrdBkWgtDetails GBWD, arGrdComponentTypes GDT " + vbCrLf)
                sb.Append("WHERE " + vbCrLf)
                sb.Append("     CS.ReqId = GBW.ReqId " + vbCrLf)
                sb.Append("     and GBW.InstrGrdBkWgtId = GBWD.InstrGrdBkWgtId " + vbCrLf)
                sb.Append("     and GBWD.GrdComponentTypeId = GDT.GrdComponentTypeId " + vbCrLf)
                sb.Append("     and GBWD.Number > 0 " + vbCrLf)
                'sb.AppendFormat("   and GDT.SysComponentTypeId not in ({0},{1}) {2}", _
                sb.AppendFormat("   and GDT.SysComponentTypeId  in ({0},{1}) {2}", _
                                CType(SysGradeBookComponentTypes.LabHours, Integer), _
                                CType(SysGradeBookComponentTypes.LabWork, Integer), _
                                vbCrLf)
                sb.Append("     and (GBW.EffectiveDate <= getdate() or " + vbCrLf)
                sb.Append("         GBW.EffectiveDate = (select max(t.EffectiveDate) from arGrdBkWeights t where t.ReqId=CS.ReqId and t.EffectiveDate <= getdate())) " + vbCrLf)
                sb.AppendFormat("   and CS.ClsSectionId = '{0}' {1}", ClsSectionId, vbCrLf)
                maxDate = db.RunSQLScalar(sb.ToString).ToString
                Return maxDate
            Else
                Return maxDate
            End If
            Return maxDate
        End Function
        Public Shared Function GetLabsForClassSection(ByVal ClsSectionId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim maxDate As String = GetRecentDate(ClsSectionId)
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     GBWD.InstrGrdBkWgtDetailId as ID, " + vbCrLf)
            sb.Append("     isnull(GBWD.Descrip, GDT.Descrip) as Descrip, " + vbCrLf)
            sb.Append("     GDT.SysComponentTypeId, " + vbCrLf)
            sb.Append("     GBW.EffectiveDate " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arClassSections CS, arGrdBkWeights GBW, arGrdBkWgtDetails GBWD, arGrdComponentTypes GDT " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     CS.ReqId = GBW.ReqId " + vbCrLf)
            sb.Append("     and GBW.InstrGrdBkWgtId = GBWD.InstrGrdBkWgtId " + vbCrLf)
            sb.Append("     and GBWD.GrdComponentTypeId = GDT.GrdComponentTypeId " + vbCrLf)
            sb.Append("     and GBWD.Number > 0 " + vbCrLf)
            sb.AppendFormat("   and GDT.SysComponentTypeId in ({0},{1}) {2}", _
                            CType(SysGradeBookComponentTypes.LabHours, Integer), _
                            CType(SysGradeBookComponentTypes.LabWork, Integer), _
                            vbCrLf)
            sb.Append("    and GBW.EffectiveDate <= CS.StartDate " + vbCrLf)
            'sb.Append("     and (GBW.EffectiveDate <= getdate() or " + vbCrLf)
            'sb.Append("         GBW.EffectiveDate = (select max(t.EffectiveDate) from arGrdBkWeights t where t.ReqId=CS.ReqId and t.EffectiveDate <= getdate())) " + vbCrLf)
            sb.AppendFormat("   and CS.ClsSectionId = '{0}' {1}", ClsSectionId, vbCrLf)
            sb.Append(" and GBW.EffectiveDate= ? ")

            sb.Append("ORDER BY GBWD.Descrip " + vbCrLf)
            '   return dataset
            db.AddParameter("@EffectiveDate", maxDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Returns all PostDates that have been entered for class / lab
        ''' </summary>
        ''' <param name="ClsSectionId"></param>
        ''' <param name="LabId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetPostDates(ByVal ClsSectionId As String, ByVal LabId As String) As DataTable
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     distinct convert(varchar,GBR.PostDate,1) as PostDate " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arGrdBkResults GBR " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.AppendFormat("     GBR.ClsSectionId = '{0}' {1}", ClsSectionId, vbCrLf)
            sb.AppendFormat("     and GBR.InstrGrdBkWgtDetailId = '{0}' {1}", LabId, vbCrLf)
            sb.Append("ORDER BY PostDate DESC " + vbCrLf)
            '   return dataset
            Return db.RunSQLDataSet(sb.ToString).Tables(0)
        End Function

        ''' <summary>
        ''' Returns the total lab counts for each student
        ''' </summary>
        ''' <param name="ClsSectionId"></param>
        ''' <param name="LabId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetTotalLabCounts(ByVal ClsSectionId As String, ByVal LabId As String) As DataTable
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     GBR.StuEnrollId, " + vbCrLf)
            sb.Append("     sum(GBR.Score) as Total " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arGrdBkResults GBR " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.AppendFormat("     GBR.ClsSectionId = '{0}' {1}", ClsSectionId, vbCrLf)
            sb.AppendFormat("     and GBR.InstrGrdBkWgtDetailId = '{0}' {1}", LabId, vbCrLf)
            sb.Append("GROUP BY GBR.StuEnrollId " + vbCrLf)
            sb.Append("ORDER BY GBR.StuEnrollId " + vbCrLf)
            '   return dataset
            Return db.RunSQLDataSet(sb.ToString).Tables(0)
        End Function

        ''' <summary>
        ''' Returns the labs results for a Class Section and lab
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetLabResults(ByVal ClsSectionId As String, ByVal LabId As String, ByVal PostDate As DateTime) As DataTable
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     GBR.GrdBkResultId, " + vbCrLf)
            sb.Append("     GBR.ClsSectionId, " + vbCrLf)
            sb.Append("     GBR.StuEnrollId, " + vbCrLf)
            sb.Append("     GBR.InstrGrdBkWgtDetailId, " + vbCrLf)
            sb.Append("     GBR.Score, " + vbCrLf)
            sb.Append("     GBR.Comments, " + vbCrLf)
            sb.Append("     GBR.PostDate, " + vbCrLf)
            sb.Append("     GBR.ModUser, " + vbCrLf)
            sb.Append("     GBR.ModDate, " + vbCrLf)
            sb.Append("     GBR.ResNum,	" + vbCrLf)
            sb.Append("     (select sum(t.Score) from arGrdBkResults t where t.ClsSectionId=GBR.ClsSectionId " + vbCrLf)
            sb.Append("         and t.StuEnrollId=GBR.StuEnrollId and t.InstrGrdBkWgtDetailId=GBR.InstrGrdBkWgtDetailId) as Total " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arGrdBkResults GBR " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.AppendFormat("     GBR.ClsSectionId = '{0}' {1}", ClsSectionId, vbCrLf)
            sb.AppendFormat("     and GBR.InstrGrdBkWgtDetailId = '{0}' {1}", LabId, vbCrLf)
            If PostDate <> DateTime.MinValue Then
                sb.AppendFormat("     and Convert(Char(10),GBR.PostDate,101) = '{0}' {1}", PostDate.ToString("MM/dd/yyyy"), vbCrLf)
            End If
            sb.Append("ORDER BY GBR.StuEnrollId, GBR.InstrGrdBkWgtDetailId " + vbCrLf)
            '   return dataset
            Return db.RunSQLDataSet(sb.ToString).Tables(0)
        End Function

        ''' <summary>
        ''' Adds a GrdPostingsInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As GrdPostingsInfo, ByVal user As String) As String
            Try
                Dim db As New DataAccess

                Dim MyAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

                Dim sb As New StringBuilder


                With sb
                    .Append(" select Top 1 t2.Descrip as Descrip from arGrdBkWgtDetails t1,arGrdComponentTypes t2 where t1.GrdComponentTypeId=t2.GrdComponentTypeId ")
                    .Append(" and t1.InstrGrdBkWgtDetailId=? ")
                End With
                db.AddParameter("@InstrGrdBkWgtDetailId", info.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                Dim strCompDescrip As String = ""
                Try
                    strCompDescrip = CType(db.RunParamSQLScalar(sb.ToString), String)
                Catch ex As Exception
                    strCompDescrip = ""
                End Try
                sb.Remove(0, sb.Length)
                db.ClearParameters()

                If info.Comments.ToString.Trim = "" Then
                    info.Comments = strCompDescrip
                End If


                sb.Append("INSERT INTO arGrdBkResults " + vbCrLf)
                sb.Append("     ([GrdBkResultId] " + vbCrLf)
                sb.Append("     ,[ClsSectionId] " + vbCrLf)
                sb.Append("     ,[InstrGrdBkWgtDetailId] " + vbCrLf)
                sb.Append("     ,[Score] " + vbCrLf)
                sb.Append("     ,[Comments] " + vbCrLf)
                sb.Append("     ,[StuEnrollId] " + vbCrLf)
                sb.Append("     ,[ModUser] " + vbCrLf)
                sb.Append("     ,[ModDate] " + vbCrLf)
                sb.Append("     ,[ResNum] " + vbCrLf)
                sb.Append("     ,[PostDate]) " + vbCrLf)

                sb.Append("VALUES( " + vbCrLf)
                sb.AppendFormat("       '{0}', {1} ", info.GrdBkResultId, vbCrLf)
                sb.AppendFormat("       '{0}', {1} ", info.ClsSectId, vbCrLf)
                sb.AppendFormat("       '{0}', {1} ", info.GrdBkWgtDetailId, vbCrLf)
                sb.AppendFormat("       {0}, {1} ", info.Score, vbCrLf)
                sb.AppendFormat("       '{0}', {1} ", info.Comments, vbCrLf)
                sb.AppendFormat("       '{0}', {1} ", info.StuEnrollId, vbCrLf)
                sb.AppendFormat("       '{0}', {1} ", user, vbCrLf)
                sb.AppendFormat("       '{0}', {1} ", Date.Now.ToString(), vbCrLf)
                sb.AppendFormat("       {0}, {1} ", info.ResNum, vbCrLf)
                If info.PostDate = DateTime.MinValue Then
                    sb.AppendFormat("       {0}) {1} ", "null", vbCrLf)
                Else
                    sb.AppendFormat("       '{0}') {1} ", info.PostDate.ToShortDateString(), vbCrLf)
                End If


                db.AddParameter("@ClsSectionId", info.ClsSectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@InstrGrdBkWgtDetailId", info.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@score", info.Score, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@Comments", info.Comments, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@resnum", info.ResNum, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If info.PostDate = DateTime.MinValue Then
                    db.AddParameter("@PostDate", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@PostDate", info.PostDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If

                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                Return "" ' success
            Catch ex As System.Exception
                Return ex.Message
            End Try
        End Function
        Public Shared Function GetCount(ByVal info As GrdPostingsInfo) As Integer
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            Dim intRowCount As Integer = 0
            With sb
                .Append(" Select Count(*) as CountRows from arGrdBkResults where " + vbCrLf)
                sb.Append("     ClsSectionId=? and " + vbCrLf)
                sb.Append("     StuEnrollId=? and  " + vbCrLf)
                sb.Append("     InstrGrdBkWgtDetailId=? " + vbCrLf)
            End With
            db.AddParameter("@ClsSectId", info.ClsSectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@stuenrollid", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@InstrGrdBkWgtDetailId", info.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            Try
                intRowCount = CType(db.RunParamSQLScalar(sb.ToString), Integer)
            Catch ex As System.Exception
                intRowCount = 0
            End Try
            Return intRowCount
        End Function
        ''' <summary>
        ''' Updates a GrdPostingsInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ''' 

        Public Shared Function Update(ByVal info As GrdPostingsInfo, ByVal user As String)
            Try

                Dim MyAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If

                Dim db As New DataAccess
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

                Dim sb As New StringBuilder

                With sb
                    .Append(" select Top 1 t2.Descrip as Descrip from arGrdBkWgtDetails t1,arGrdComponentTypes t2 where t1.GrdComponentTypeId=t2.GrdComponentTypeId ")
                    .Append(" and t1.InstrGrdBkWgtDetailId=? ")
                End With
                db.AddParameter("@InstrGrdBkWgtDetailId", info.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                Dim strCompDescrip As String = ""
                Try
                    strCompDescrip = CType(db.RunParamSQLScalar(sb.ToString), String)
                Catch ex As Exception
                    strCompDescrip = ""
                End Try
                sb.Remove(0, sb.Length)
                db.ClearParameters()

                If info.Comments.ToString.Trim = "" Then
                    info.Comments = strCompDescrip
                End If

                'With sb
                '    .Append(" select Top 1 GrdBkResultId from arGrdBkResults where  ")
                '    sb.Append("     ClsSectionId=? and " + vbCrLf)
                '    sb.Append("     StuEnrollId=? and " + vbCrLf)
                '    sb.Append("     InstrGrdBkWgtDetailId=? " + vbCrLf)
                'End With
                'db.AddParameter("@ClsSectId", info.ClsSectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'db.AddParameter("@stuenrollid", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'db.AddParameter("@bookid", info.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'Dim strGrdBkResultId As String = ""
                'Try
                '    strGrdBkResultId = CType(db.RunParamSQLScalar(sb.ToString), Guid).ToString
                '    If Not strGrdBkResultId = "" And Not strGrdBkResultId = System.Guid.Empty.ToString Then
                '        info.GrdBkResultId = strGrdBkResultId
                '    End If

                'Catch ex As System.Exception
                '    strGrdBkResultId = ""
                'End Try
                'sb.Remove(0, sb.Length)
                'db.ClearParameters()


                sb.Append("UPDATE arGrdBkResults " + vbCrLf)
                sb.Append("SET " + vbCrLf)
                sb.Append("     Score = ?, " + vbCrLf)
                sb.Append("     ResNum = ?, " + vbCrLf)
                sb.Append("     Comments = ?, " + vbCrLf)
                sb.Append("     ModUser=?, " + vbCrLf)
                sb.Append("     ModDate=?, " + vbCrLf)
                sb.Append("     ClsSectionId=?, " + vbCrLf)
                sb.Append("     StuEnrollId=?, " + vbCrLf)
                sb.Append("     InstrGrdBkWgtDetailId=?, " + vbCrLf)
                sb.Append("     PostDate=? " + vbCrLf)
                sb.Append("WHERE " + vbCrLf)
                sb.Append("     GrdBkResultId = ? ")

                db.AddParameter("@score", info.Score, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@resnum", info.ResNum, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@comments", info.Comments, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ClsSectId", info.ClsSectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@stuenrollid", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@bookid", info.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.PostDate = DateTime.MinValue Then
                    db.AddParameter("@PostDate", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@PostDate", info.PostDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                db.AddParameter("@GrdBkResultId", info.GrdBkResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                Return "" ' success
            Catch ex As System.Exception
                Return ex.Message
            End Try
        End Function

        ''' <summary>
        ''' Deletes a GrdPostings record from the database
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal GrdBkResultId As String) As String
            Try

                Dim MyAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If

                Dim db As New DataAccess
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                Dim sb As New StringBuilder
                sb.Append("DELETE FROM arGrdBkResults ")
                sb.Append("WHERE GrdBkResultId = ? ")

                db.AddParameter("@GrdBkResultId", GrdBkResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                Return "" ' success
            Catch ex As System.Exception
                Return ex.Message
            End Try
        End Function
    End Class

End Namespace
