Imports FAME.Advantage.Common

Public Class AttendanceHistoryMultipleDb

#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")

#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region


#Region "Public Methods"

    Public Function GetAttendanceHistory(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""
        Dim strFrom As String = ""
        Dim strStudentId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try


            If paramInfo.FilterList <> "" Then
                strWhere &= " AND " & paramInfo.FilterList

                strWhere = Replace(strWhere, "syCampuses.CampusId", "F.CampusId")
            End If

            'If paramInfo.FilterOther <> "" Then
            '    strWhere &= " AND " & paramInfo.FilterOther
            'End If

            'If paramInfo.OrderBy <> "" Then
            '    strOrderBy &= "," & paramInfo.OrderBy
            'End If

            If StudentIdentifier = "SSN" Then
                strStudentId = "adLeads.SSN AS StudentIdentifier,"
            ElseIf StudentIdentifier = "EnrollmentId" Then
                strStudentId = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
            ElseIf StudentIdentifier = "StudentId" Then
                strStudentId = "adLeads.StudentNumber AS StudentIdentifier,"
            End If

            'Dim FilterOtherParam As String
            'FilterOtherParam = paramInfo.FilterOtherString
            'Dim date1 As String
            'date1 = FilterOtherParam.Substring(FilterOtherParam.IndexOf("(") + 1, (FilterOtherParam.IndexOf(")") - FilterOtherParam.IndexOf("(")) - 1)
            'Dim FromDate As Date
            'Dim ToDate As Date
            'FromDate = CDate(date1.Substring(0, (date1.IndexOf("A")) - 1))
            'ToDate = CDate(date1.Substring(date1.IndexOf("D") + 1, (date1.Length - date1.IndexOf("D") - 1)))

            Dim FilterOtherParam As String
            FilterOtherParam = paramInfo.FilterOtherString
            Dim dates As String()
            Dim Dates1 As String
            Dim FromAndToDate As String()

            dates = Split(FilterOtherParam, "Between")
            Dates1 = dates(1).Substring(dates(1).IndexOf("(") + 1, dates(1).Length - 4)
            FromAndToDate = Split(Dates1, "AND")

            Dim FromDate As Date
            Dim ToDate As Date
            FromDate = CDate(FromAndToDate(0))
            ToDate = CDate(FromAndToDate(1))

            Dim strFromDate As String
            Dim strToDate As String
            FromDate = New Date(FromDate.Year, FromDate.Month, 1)
            strFromDate = Format(FromDate, "MM/dd/yyyy")
            ToDate = ToDate.AddMonths(1)
            ToDate = New Date(ToDate.Year, ToDate.Month, 1)
            ToDate = ToDate.AddDays(-1)
            strToDate = Format(ToDate, "MM/dd/yyyy")

            'Dim Filters As String()
            'Filters = (strWhere.Replace(" AND ", ";")).Split(";")
            'Dim CampusGrpQuery As String
            'Dim CampuspQuery As String
            'If Filters.Length > 0 Then
            '    For i = 0 To Filters.Length - 1
            '        If Filters(i).Contains("syCampGrps") Then
            '            CampusGrpQuery = Filters(i)
            '        ElseIf Filters(i).Contains("CampusId") Then
            '            CampuspQuery = Filters(i)
            '        End If
            '    Next
            'End If



            If MyAdvAppSettings.AppSettings("TrackSapAttendance", paramInfo.CampusId).ToString.ToLower = "byday" Then

                With sb

                    .Append(" Select adLeads.LastName,adLeads.FirstName,adLeads.MiddleName,SSC.StatusCodeDescrip,")
                    .Append(strStudentId)
                    .Append(" B.StuENrollId,Convert(varchar,RecordDate,101) RecordDate,SchedHours,ActualHours,IsNull((Select (ActualHours- SchedHours) from  ")
                    .Append(" arStudentClockAttendance A where A.StuEnrollId=B.StuEnrollId and A.Scheduleid=B.ScheduleId and   A.RecordDate=B.RecordDate and  ")
                    .Append(" ActualHours<>9999.0 and ActualHours<>999.0 and SchedHours is not null and SchedHours<ActualHours ),0.00)MakeUpHours, ")
                    .Append(" IsNull((Select (SchedHours- ActualHours) from  ")
                    .Append(" arStudentClockAttendance A where A.StuEnrollId=B.StuEnrollId and A.Scheduleid=B.ScheduleId and   A.RecordDate=B.RecordDate and  ")
                    .Append(" ActualHours<>9999.0 and ActualHours<>999.0 and SchedHours is not null and SchedHours>ActualHours ),0.00)AbsentHours, ")
                    .Append(" isTardy, (dbo.GetLDA(B.StuEnrollId)) LDA, ")
                    .Append(" '' AS ClassSection ")
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    ''Commented by Saraswathi lakshmanan on jan 27 2010
                    ' .Append(" from arStudentCLockAttendance B,arStudent,arStuEnrollments,arStudentSchedules,SyStatusCodes SSC ")
                    .Append(" from arStudentCLockAttendance B,adLeads,arStuEnrollments,SyStatusCodes SSC ")

                    .Append("   , syCmpGrpCmps E,syCampuses F,syCampGrps ")

                    .Append(" where RecordDate>='" & strFromDate & "' ")
                    .Append(" and RecordDate<='" & strToDate & "' ")
                    .Append(" and SchedHours is not null and ActualHours <>999.0 and ActualHours<>9999.0 ")
                    .Append(" and adLeads.StudentId=arStuEnrollments.StudentId and arStuEnrollments.StuEnrollId=B.StuEnrollId  ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '.Append(" and arStuEnrollments.StuEnrollId=arStudentSchedules.StuEnrollId and arStudentSchedules.ScheduleId=B.ScheduleID ")
                    .Append(" and SSC.StatusCodeId=arStuEnrollments.StatusCodeId ")
                    '.Append(" and arStudentSchedules.Active=1 and SSC.StatusCodeId=arStuEnrollments.StatusCodeId ")

                    .Append("  AND arStuEnrollments.CampusId=F.CampusId  ")
                    .Append("   AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId  ")
                    .Append(strWhere)


                    .Append(" ;Select StuEnrollId,StartDate,EndDate from arStudentLOAs ;")
                    .Append(" Select StuEnrollId,StartDate,EndDate from arStdSuspensions ;")
                    ''Added by Saraswathi lakshmanan on march 03 2010
                    .Append(" SELECT    syCmpGrpCmps.CampusId,  CCT.HolidayCode,  CCT.HolidayStartDate StartDate, ")
                    .Append("     CCT.HolidayEndDate EndDate, ")
                    .Append(" CCT.HolidayDescrip ")
                    .Append(" FROM     syHolidays CCT, syStatuses ST,syCmpGrpCmps ")

                    .Append(" WHERE    CCT.StatusId = ST.StatusId  and ST.Status='Active' and syCmpGrpCmps.CampGrpId=CCT.CampGrpId ")


                    .Append(" ;Select  Distinct arStudentClockAttendance.StuEnrollId ")
                    ''Added by Saraswathi to find the unit type (hours or Days)
                    .Append(" ,PV.UnitTypeId ")
                    .Append(" ,arStuEnrollments.CampusID ")

                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '.Append(" from arStudentClockAttendance,arStudentSchedules ")
                    .Append(" from arStudentClockAttendance ")

                    .Append(" ,arStuEnrollments ")
                    .Append(" , arPrgVersions PV ")
                    .Append("   , syCmpGrpCmps E,syCampuses F,syCampGrps ")

                    .Append(" where RecordDate>='" & strFromDate & "'")
                    .Append(" and RecordDate<='" & strToDate & "'")

                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '.Append(" and arStudentSchedules.ScheduleId=arStudentClockAttendance.ScheduleID")
                    '.Append(" and arStudentClockAttendance.StuEnrollId=arStudentSchedules.StuEnrollId")
                    .Append(" and  arStuEnrollments.StuEnrollid=arStudentClockAttendance.StuEnrollId ")
                    ' .Append(" and arStudentSchedules.Active=1 ")
                    .Append(" and SchedHours is not null and ActualHours <>999.0 and ActualHours<>9999.0")

                    .Append(" and arStuEnrollments.PrgVerId =PV.PrgVerId")

                    .Append("  AND arStuEnrollments.CampusId=F.CampusId  ")
                    .Append("   AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId  ")
                    .Append(strWhere)

                    .Append(" ;Select  arStudentClockAttendance.StuEnrollId,arStudentClockAttendance.ScheduleID, ")
                    .Append(" isNull(sum(SchedHours),0.00) SchedHours,(isNull(Sum(ActualHours),0.00) -  dbo.GetAbsentHoursFromTardyForByDay(arStudentClockAttendance.StuEnrollid,'" & strFromDate & "')) ActualHours")
                    .Append(" ,(isNull((Select Sum(SchedHours - ActualHours) from arStudentClockAttendance A  where  A.StuEnrollId=arStudentClockAttendance.StuEnrollid ")
                    .Append(" and  A.RecordDate<'" & strFromDate & "'")
                    .Append(" and A.Scheduleid=arStudentClockAttendance.ScheduleId and   ActualHours<>9999.0 and ActualHours<>999.0 and ")
                    .Append(" SchedHours is not null and SchedHours>ActualHours ),0.00) +  dbo.GetAbsentHoursFromTardyForByDay(arStudentClockAttendance.StuEnrollid,'" & strFromDate & "')) AbsentHours")
                    .Append(" ,isNull((Select Sum(Actualhours - SchedHours) from arStudentClockAttendance A  where  A.StuEnrollId=arStudentClockAttendance.StuEnrollid ")
                    .Append(" and   A.RecordDate<'" & strFromDate & "'")
                    .Append(" and A.Scheduleid=arStudentClockAttendance.ScheduleId and   ActualHours<>9999.0 and ActualHours<>999.0 and ")
                    .Append(" SchedHours is not null and SchedHours<ActualHours ),0.00)MakeUpHours,")
                    .Append(" dbo.GetAbsentHoursFromTardyForByDay(arStudentClockAttendance.StuEnrollid,'" & strFromDate & "') AbsentHrsFromTardy ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '.Append(" from arStudentClockAttendance,arStudentSchedules ")
                    .Append(" from arStudentClockAttendance ")

                    .Append(" ,arStuEnrollments ")
                    .Append("   , syCmpGrpCmps E,syCampuses F,syCampGrps ")

                    .Append(" where RecordDate< '" & strFromDate & "'")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '.Append(" and arStudentSchedules.ScheduleId=arStudentClockAttendance.ScheduleID")
                    '.Append(" and arStudentClockAttendance.StuEnrollId=arStudentSchedules.StuEnrollId")
                    .Append(" and  arStuEnrollments.StuEnrollid=arStudentClockAttendance.StuEnrollId ")
                    '.Append(" and arStudentSchedules.Active=1 ")
                    .Append(" and SchedHours is not null and ActualHours <>999.0 and ActualHours<>9999.0")

                    .Append("  AND arStuEnrollments.CampusId=F.CampusId  ")
                    .Append("   AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId  ")
                    .Append(strWhere)

                    .Append(" group by arStudentClockAttendance.StuEnrollid,arStudentClockAttendance.ScheduleID")

                    .Append(" ;Select   ")
                    .Append(" B.StuENrollId,DATEADD(dd, 0, DATEDIFF(dd, 0, RecordDate))RecordDate,SchedHours,ActualHours,  ")
                    .Append(" isTardy ,PV.TrackTardies, PV.TardiesMakingAbsence, 0 AS MarkAbsent  ")

                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    ''Commented by Saraswathi lakshmanan on jan 27 2010
                    ' .Append(" from arStudentCLockAttendance B,arStudent,arStuEnrollments,arStudentSchedules,SyStatusCodes SSC ")
                    .Append(" from arStudentCLockAttendance B,adLeads,arStuEnrollments,arPrgVersions PV,SyStatusCodes SSC ")

                    .Append("   , syCmpGrpCmps E,syCampuses F,syCampGrps ")

                    .Append(" where PV.PrgVerId=dbo.arStuEnrollments.PrgVerId      AND isTardy=1 AND TrackTardies=1  AND TardiesMakingAbsence>0   ")

                    .Append(" and SchedHours is not null and ActualHours <>999.0 and ActualHours<>9999.0 ")
                    .Append(" and adLeads.StudentId=arStuEnrollments.StudentId and arStuEnrollments.StuEnrollId=B.StuEnrollId  ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    '.Append(" and arStuEnrollments.StuEnrollId=arStudentSchedules.StuEnrollId and arStudentSchedules.ScheduleId=B.ScheduleID ")
                    .Append(" and SSC.StatusCodeId=arStuEnrollments.StatusCodeId ")
                    '.Append(" and arStudentSchedules.Active=1 and SSC.StatusCodeId=arStuEnrollments.StatusCodeId ")

                    .Append("  AND arStuEnrollments.CampusId=F.CampusId  ")
                    .Append("   AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId  ")
                    .Append(strWhere)
                    ''To check if we have to show the instruction type or not on the report
                    .Append(";SELECT COUNT(InstructionTypeId)  FROM dbo.arInstructionType; ")

                End With

            Else

                'We are using TrackSapAttendance = ByClass app setting here 
                With sb
                    .Append("SELECT B.LastName, B.FirstName, B.MiddleName, B.StatusCodeDescrip, B.StudentIdentifier, B.StuEnrollId, B.RecordDate, ")
                    .Append("SUM(B.SchedHours) AS SchedHours, SUM(B.ActualHours) AS ActualHours, SUM(B.MakeUpHours) AS MakeUpHours, SUM(B.AbsentHours) AS AbsentHours, ")
                    .Append("isTardy, ( dbo.GetLDA(B.StuEnrollId)) AS LDA, B.ClassSection,B.ClsSectMeetingId ,B.StartTime,B.EndTime ")
                    .Append("FROM ")
                    .Append("(")
                    'Student Details Section
                    .Append("SELECT adLeads.LastName,adLeads.FirstName,adLeads.MiddleName,SSC.StatusCodeDescrip, ")
                    .Append("adLeads.SSN AS StudentIdentifier, B.StuENrollId, ")
                    .Append("Convert(varchar,MeetDate,101) RecordDate, Scheduled AS SchedHours, Actual AS ActualHours, ")
                    .Append("IsNull((Select (Actual- Scheduled) from   atClsSectAttendance A  ")
                    .Append("where A.StuEnrollId=B.StuEnrollId  ")
                    .Append("and A.MeetDate=B.MeetDate and Actual<>9999.0 and  ")
                    .Append("Actual<>999.0 and Scheduled is not null and Scheduled<Actual AND A.ClsSectMeetingId = B.ClsSectMeetingId ),0.00) AS MakeUpHours, ")
                    .Append("IsNull((Select (Scheduled- Actual) from atClsSectAttendance A ")
                    .Append("where A.StuEnrollId=B.StuEnrollId ")
                    .Append("and A.MeetDate=B.MeetDate ")
                    .Append("and Actual<>9999.0 and Actual<>999.0 ")
                    .Append("and Scheduled is not null and Scheduled>Actual AND A.ClsSectMeetingId = B.ClsSectMeetingId ),0.00) AS AbsentHours, ")
                    .Append("B.Tardy AS isTardy, (dbo.GetLDA(B.StuEnrollId)) LDA, ")
                    .Append("(SELECT Descrip FROM arreqs WHERE ReqId IN ")
                    .Append("(SELECT ReqId FROM dbo.arClassSections WHERE ClsSectionId = B.ClsSectionId)) + ' (' + ")
                    .Append("(SELECT ClsSection FROM arClassSections WHERE ClsSectionId = B.ClsSectionId) + ') % ' + ")
                    .Append("(SELECT InstructionTypeDescrip FROM arInstructionType WHERE InstructionTypeId =  ")
                    .Append("(SELECT InstructionTypeId FROM arClsSectMeetings WHERE ClsSectMeetingId = ")
                    .Append("(SELECT ClsSectMeetingId FROM atClsSectAttendance WHERE ClsSectAttId = B.ClsSectAttId))) AS ClassSection,B.ClsSectMeetingId ")
                    .Append(" , (SELECT CONVERT(VARCHAR,Table2.TimeIntervalDescrip,108) FROM dbo.arClsSectMeetings Table1,dbo.syPeriods Table3, ")
                    .Append(" dbo.cmTimeInterval Table2 WHERE Table1.ClsSectMeetingId = B.ClsSectMeetingId   ")
                    .Append(" AND Table3.PeriodId=Table1.PeriodId  ")
                    .Append(" AND Table3.StartTimeId=Table2.TimeIntervalId)AS StartTime,  ")
                    .Append("  (SELECT CONVERT(VARCHAR,Table2.TimeIntervalDescrip,108) FROM dbo.arClsSectMeetings Table1,dbo.syPeriods Table3, ")
                    .Append(" dbo.cmTimeInterval Table2 WHERE Table1.ClsSectMeetingId = B.ClsSectMeetingId   ")
                    .Append(" AND Table3.PeriodId=Table1.PeriodId  ")
                    .Append(" AND Table3.EndTimeId=Table2.TimeIntervalId)AS EndTime ")

                    .Append("FROM ")
                    .Append("atClsSectAttendance B,adLeads,arStuEnrollments,SyStatusCodes SSC, syCmpGrpCmps E,syCampuses F,syCampGrps  ")
                    .Append("WHERE ")
                    '.Append("B.MeetDate>='" & strFromDate & "' ")
                    '.Append("and B.MeetDate<='" & strToDate & "' ")
                    .Append("            CAST(CONVERT(VARCHAR, B.MeetDate, 101)as DATE)>= CAST(CONVERT(VARCHAR,'" + strFromDate + "', 101)as DATE) ")
                    .Append("            AND CAST(CONVERT(VARCHAR, B.MeetDate, 101)as DATE)<= CAST(CONVERT(VARCHAR,'" + strToDate + "', 101) as DATE) ")

                    .Append("and B.Scheduled is not null ")
                    .Append("and B.Actual <>999.0 and B.Actual<>9999.0 ")
                    .Append("and adLeads.StudentId=arStuEnrollments.StudentId ")
                    .Append("and arStuEnrollments.StuEnrollId=B.StuEnrollId ")
                    .Append("and SSC.StatusCodeId=arStuEnrollments.StatusCodeId ")
                    .Append("and arStuEnrollments.CampusId=F.CampusId ")
                    .Append("and F.CampusId=E.CampusId ")
                    .Append("AND E.CampGrpId = syCampGrps.CampGrpId ")
                    .Append(strWhere)
                    .Append(") B ")
                    .Append("GROUP BY ")
                    .Append("B.RecordDate, B.LastName, B.FirstName, B.MiddleName, B.StatusCodeDescrip, B.StudentIdentifier, B.StuEnrollId, ")
                    .Append("B.isTardy, B.ClassSection,B.ClsSectMeetingId,B.StartTime,B.EndTime ")

                    'LOA Section
                    .Append(";SELECT StuEnrollId, StartDate, EndDate FROM arStudentLOAs ")

                    'Suspension Section
                    .Append(";SELECT StuEnrollId, StartDate, EndDate FROM arStdSuspensions ")

                    'Holidays Section
                    .Append(";SELECT syCmpGrpCmps.CampusId, CCT.HolidayCode, CCT.HolidayStartDate StartDate, CCT.HolidayEndDate EndDate,  CCT.HolidayDescrip ")
                    .Append("  , CCT.AllDay, (SELECT TimeIntervalDescrip FROM dbo.cmTimeInterval WHERE TimeIntervalId=StartTimeId)StartTime,(SELECT TimeIntervalDescrip FROM dbo.cmTimeInterval WHERE TimeIntervalId=cct.EndTimeId)EndTime  ")
                    .Append("FROM syHolidays CCT, syStatuses ST, syCmpGrpCmps ")
                    .Append("WHERE CCT.StatusId = ST.StatusId ")
                    .Append("and ST.Status='Active' ")
                    .Append("and syCmpGrpCmps.CampGrpId=CCT.CampGrpId ")



                    'Students List Section
                    .Append(";SELECT  Distinct atClsSectAttendance.StuEnrollId  ,PV.UnitTypeId  ,arStuEnrollments.CampusID ")
                    .Append("FROM atClsSectAttendance, arStuEnrollments, arPrgVersions PV, syCmpGrpCmps E, syCampuses F, syCampGrps ")
                    .Append("WHERE ")
                    .Append("        CAST(    CONVERT(VARCHAR, MeetDate, 101)as DATE)>= CAST(CONVERT(VARCHAR,'" + strFromDate + "', 101)as DATE) ")
                    .Append("            AND CAST( CONVERT(VARCHAR, MeetDate, 101) as DATE)<= CAST(CONVERT(VARCHAR,'" + strToDate + "', 101)as DATE) ")

                    '.Append("MeetDate>='" & strFromDate & "' ")
                    '.Append("and MeetDate<='" & strToDate & "' ")
                    .Append("and arStuEnrollments.StuEnrollid=atClsSectAttendance.StuEnrollId ")
                    .Append("and Scheduled is not null ")
                    .Append("and Actual <>999.0 and Actual<>9999.0 ")
                    .Append("and arStuEnrollments.PrgVerId =PV.PrgVerId ")
                    .Append("and arStuEnrollments.CampusId=F.CampusId ")
                    .Append("AND F.CampusId=E.CampusId ")
                    .Append("AND E.CampGrpId = syCampGrps.CampGrpId ")
                    .Append(strWhere)

                    'Student Month Totals Section
                    '.Append(";SELECT atClsSectAttendance.StuEnrollId,'' AS ScheduleId,  isNull(sum(Scheduled),0.00) SchedHours, ")
                    '.Append("isNull(Sum(Actual),0.00)ActualHours, ")
                    '.Append("isNull((Select Sum(Scheduled - Actual) from atClsSectAttendance A ")
                    '.Append("where  A.StuEnrollId=atClsSectAttendance.StuEnrollid ")
                    ' '' and A.MeetDate<'" & strFromDate & "' ")
                    '.Append("            AND CAST( CONVERT(VARCHAR, A.MeetDate, 101) as DATE)< CAST( CONVERT(VARCHAR,'" + strFromDate + "', 101) as DATE) ")

                    '.Append("and Actual<>9999.0 and Actual<>999.0 ")
                    '.Append("and Scheduled is not null ")
                    '.Append("and Scheduled>Actual ),0.00)AbsentHours, ")
                    '.Append("isNull((Select Sum(Actual - Scheduled) from atClsSectAttendance A ")
                    '.Append("where A.StuEnrollId=atClsSectAttendance.StuEnrollid ")
                    ' '' and A.MeetDate<'" & strFromDate & "' ")
                    '.Append("            AND CAST(CONVERT(VARCHAR, A.MeetDate, 101)as DATE)< CAST(CONVERT(VARCHAR,'" + strFromDate + "', 101) as DATE)")
                    '.Append("and Actual<>9999.0 and Actual<>999.0 ")
                    '.Append("and  Scheduled is not null ")
                    '.Append("and Scheduled<Actual ),0.00)MakeUpHours ")
                    '.Append("FROM ")
                    '.Append("atClsSectAttendance, arStuEnrollments, syCmpGrpCmps E, syCampuses F, syCampGrps ")
                    '.Append("WHERE ")
                    ' ''.Append("atClsSectAttendance.MeetDate<='" & strFromDate & "' ")
                    '.Append("          CAST( CONVERT(VARCHAR, atClsSectAttendance.MeetDate, 101) as DATE)<= CAST(CONVERT(VARCHAR,'" + strFromDate + "', 101) as DATE)")
                    '.Append("and arStuEnrollments.StuEnrollid=atClsSectAttendance.StuEnrollId ")
                    '.Append("and atClsSectAttendance.Actual <>999.0 and atClsSectAttendance.Actual<>9999.0 ")
                    '.Append("and arStuEnrollments.CampusId=F.CampusId ")
                    '.Append("and F.CampusId=E.CampusId ")
                    '.Append("and E.CampGrpId = syCampGrps.CampGrpId ")
                    '.Append(strWhere)
                    '.Append(" GROUP BY atClsSectAttendance.StuEnrollid ")


                    .Append(";SELECT ")
                    .Append("  atClsSectAttendance.StuEnrollId , ")
                    .Append(" ISNULL(Scheduled, 0.00) Scheduled , ")
                    .Append(" ISNULL(Actual, 0.00) Actual , ")
                    .Append(" atClsSectAttendance.ClsSectMeetingId, ")
                    .Append(" TardiesMakingAbsence, ")
                    .Append(" Tardy, ")
                    .Append(" MeetDate, ")
                    .Append("(SELECT Descrip FROM arreqs WHERE ReqId IN ")
                    .Append("(SELECT ReqId FROM dbo.arClassSections B WHERE B.ClsSectionId = atClsSectAttendance.ClsSectionId)) + ' (' + ")
                    .Append("(SELECT ClsSection FROM arClassSections B WHERE B.ClsSectionId = atClsSectAttendance.ClsSectionId) + ') % ' + ")
                    .Append("(SELECT InstructionTypeDescrip FROM arInstructionType WHERE InstructionTypeId =  ")
                    .Append("(SELECT InstructionTypeId FROM arClsSectMeetings WHERE ClsSectMeetingId = ")
                    .Append("(SELECT ClsSectMeetingId FROM atClsSectAttendance C WHERE C.ClsSectAttId = atClsSectAttendance.ClsSectAttId))) AS ClassSection  ")
                    .Append("FROM ")
                    .Append("atClsSectAttendance, arStuEnrollments, syCmpGrpCmps E, syCampuses F, syCampGrps,arPrgVersions ")
                    .Append("WHERE ")
                    ''.Append("atClsSectAttendance.MeetDate<='" & strFromDate & "' ")
                    .Append("          CAST( CONVERT(VARCHAR, atClsSectAttendance.MeetDate, 101) as DATE)< CAST(CONVERT(VARCHAR,'" + strFromDate + "', 101) as DATE)")
                    .Append("and arStuEnrollments.StuEnrollid=atClsSectAttendance.StuEnrollId ")
                    .Append(" AND dbo.arStuEnrollments.PrgVerId=dbo.arPrgVersions.PrgVerId ")
                    .Append("and atClsSectAttendance.Actual <>999.0 and atClsSectAttendance.Actual<>9999.0 ")
                    .Append("and arStuEnrollments.CampusId=F.CampusId ")
                    .Append("and F.CampusId=E.CampusId ")
                    .Append("and E.CampGrpId = syCampGrps.CampGrpId ")
                    .Append(strWhere)
                    .Append("  ORDER BY atClsSectAttendance.StuEnrollId,MeetDate ")
                    '.Append(" GROUP BY atClsSectAttendance.StuEnrollid ")


                    'Student Tardy Details Section
                    .Append(";SELECT B.StuENrollId, Convert(varchar,MeetDate,101) RecordDate, Scheduled AS SchedHours, ")
                    .Append("Actual AS ActualHours, Tardy AS isTardy, PV.TrackTardies, ")
                    .Append("PV.TardiesMakingAbsence, 0 AS MarkAbsent,B.ClsSectMeetingId   ")
                    .Append("FROM ")
                    .Append("atClsSectAttendance B, adLeads, arStuEnrollments, arPrgVersions PV, SyStatusCodes SSC, syCmpGrpCmps E,syCampuses F,syCampGrps ")
                    .Append("WHERE ")
                    .Append("PV.PrgVerId=dbo.arStuEnrollments.PrgVerId ")
                    .Append("and Tardy=1 AND TrackTardies=1 ")
                    .Append("and TardiesMakingAbsence>0 ")
                    .Append("and Actual <>999.0 and Actual<>9999.0 ")
                    .Append("and adLeads.StudentId=arStuEnrollments.StudentId ")
                    .Append("and arStuEnrollments.StuEnrollId=B.StuEnrollId ")
                    .Append("and SSC.StatusCodeId=arStuEnrollments.StatusCodeId ")
                    .Append("and arStuEnrollments.CampusId=F.CampusId ")
                    .Append("and F.CampusId=E.CampusId ")
                    .Append("and E.CampGrpId = syCampGrps.CampGrpId ")
                    .Append(strWhere)

                    ''To check if we have to show the instruction type or not on the report
                    .Append("; SELECT COUNT(InstructionTypeId)  FROM dbo.arInstructionType; ")

                End With

            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()
            ds = db.RunParamSQLDataSet(sb.ToString)

        Catch ex As Exception
            Return ds
        End Try
        ' If ds.Tables.Count > 0 Then
        ds.Tables(0).TableName = "Studentdetails"
        ds.Tables(1).TableName = "LOA"
        ds.Tables(2).TableName = "Susp"
        ds.Tables(3).TableName = "Holiday"
        ds.Tables(4).TableName = "Studentslist"
        ds.Tables(5).TableName = "StudentMonthTots"
        ds.Tables(6).TableName = "StudentTardyDetails"
        ds.Tables(7).TableName = "InstructionTypeCount"
        'End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
#End Region

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

End Class
