Imports FAME.Advantage.Common

Public Class InquiryAnalysisByTime

    Public Function GetLeadsCreatedDate(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "ORDER BY " & paramInfo.OrderBy
        End If

        With sb
            .Append("SELECT CONVERT(char(10),SourceDate,101)+ ' ' + ISNULL(InquiryTime, right(SourceDate,7)) as SourceDate FROM adLeads,syCmpGrpCmps,syCampGrps,syCampuses ")
            .Append("WHERE syCmpGrpCmps.CampusId=adLeads.CampusId ")
            .Append("AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            .Append("AND syCampuses.CampusId=adLeads.CampusId ")
            .Append("AND SourceDate IS NOT NULL ") ' and InquiryTime is not null ") - removed the ``and is Not Null`` bc the query has ``ISNULL(InquiryTime,`` logic above which is the desired behavior/logic 
            .Append(strWhere)
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

End Class


