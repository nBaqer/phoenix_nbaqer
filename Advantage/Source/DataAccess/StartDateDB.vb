Imports System.Xml
Imports FAME.Advantage.Common

Public Class StartDateDB

    Public Function GetGradeScales() As DataTable
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        With sb
            .Append("SELECT GrdScaleId,Descrip ")
            .Append("FROM arGradeScales ")
            .Append(" ,syStatuses where arGradeScales.StatusId=syStatuses.StatusId and syStatuses.Status='Active' ")
            .Append(" ORDER BY Descrip ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            ds = db.RunParamSQLDataSet(sb.ToString)
            Return ds.Tables(0)
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        End Try
    End Function

    Public Function GetShifts() As DataTable
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        With sb
            .Append("SELECT ShiftId,ShiftDescrip ")
            .Append("FROM arShifts ")
            .Append("ORDER BY ShiftDescrip ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            ds = db.RunParamSQLDataSet(sb.ToString)
            Return ds.Tables(0)
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        End Try
    End Function

    Public Function GetTermStartDate(ByVal termId) As String
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        With sb
            .Append("SELECT StartDate ")
            .Append("FROM arTerm ")
            .Append("WHERE TermId = ? ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            Return Convert.ToString(db.RunParamSQLScalar(sb.ToString))

        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        End Try
    End Function

    Public Function GetTermEndDate(ByVal termId) As String
        Dim sb As New StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        With sb
            .Append("SELECT EndDate ")
            .Append("FROM arTerm ")
            .Append("WHERE TermId = ? ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            Return Convert.ToString(db.RunParamSQLScalar(sb.ToString))

        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        End Try
    End Function

    Public Function GetRelatedStarts(ByVal termId As String, Optional ByVal ShiftId As String = "") As DataTable
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If



        With sb
            .Append("SELECT t1.TermId,t1.TermDescrip ")
            .Append("FROM arTerm t1 ")
            .Append("WHERE ProgId = (SELECT t2.ProgId ")
            .Append("                FROM arTerm t2 ")
            .Append("                WHERE t2.TermId = ?) ")
            .Append("AND t1.TermId <> ? ")
            .Append("AND ( ")
            .Append("       ( ")
            .Append("           t1.StartDate < (SELECT t3.EndDate ")
            .Append("                           FROM arTerm t3 ")
            .Append("                           WHERE t3.TermId = ?) ")
            .Append("           AND ")
            .Append("           t1.EndDate > (SELECT t4.EndDate ")
            .Append("           FROM arTerm t4 ")
            .Append("           WHERE t4.TermId = ?) ")
            .Append("       ) ")
            .Append("           OR ")
            .Append("       ( ")
            .Append("           t1.StartDate < (SELECT t5.StartDate ")
            .Append("                           FROM arTerm t5 ")
            .Append("                           WHERE t5.TermId = ?) ")
            .Append("           AND ")
            .Append("           t1.EndDate BETWEEN ((SELECT t6.StartDate ")
            .Append("                                FROM arTerm t6 ")
            .Append("                                WHERE t6.TermId = ?)) ")
            .Append("                                       AND ")
            .Append("                               ((SELECT t7.EndDate ")
            .Append("                                 FROM arTerm t7 ")
            .Append("                                 WHERE t7.TermId = ?)) ")
            .Append("       ) ")
            .Append("   ) ")
            If (ShiftId <> "") Then
                .Append(" and ShiftId= ? ")
            End If
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        If (ShiftId <> "") Then
            db.AddParameter("@shiftid", ShiftId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        Try
            ds = db.RunParamSQLDataSet(sb.ToString)
            Return ds.Tables(0)
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        End Try

    End Function

    Public Function GetDatesIfNull(ByVal termId As String, ByVal clsSecId As String) As String()
        Dim db As New DataAccess
        Dim dr As OleDbDataReader
        Dim rtn(2) As String
        Dim sql As String = " Select StartDate,EndDate from arClassSections where ClsSectionId = ? "
        sql &= " and TermId = ? "

        Try
            db.AddParameter("@ClsSectionId", clsSecId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            dr = db.RunParamSQLDataReader(sql)
            If (dr.HasRows) Then
                While dr.Read()
                    rtn(0) = dr("StartDate").ToShortDateString()
                    rtn(1) = dr("EndDate").ToShortDateString()
                    Exit While
                End While

            End If

            If Not dr.IsClosed Then dr.Close()
            
        Catch ex As Exception
            db.CloseConnection()
            Throw ex
        End Try


        Return rtn
    End Function
    Public Function ValidateEndDate(ByVal termId As String) As Object
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim maxEndDate As Object
        With sb
            .Append("SELECT  max(t1.EndDate) ")
            .Append(" FROM arClassSections t1, arClassSectionTerms t2 ")
            .Append(" WHERE t1.ClsSectionId=t2.ClsSectionId ")
            .Append(" AND t2.TermId = '" + termId + "'")
        End With
        'db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.OpenConnection()

        Try
            maxEndDate = db.RunSQLScalar(sb.ToString).ToString
        Catch ex As System.Exception
            db.CloseConnection()
            Throw ex
        End Try
        Return maxEndDate

    End Function


    Public Function GetStartDateClassesAndMeetings(ByVal termId As String) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim daClassSections As New OleDbDataAdapter
        Dim daClsSectMeetings As New OleDbDataAdapter
        Dim daRelatedStarts As New OleDbDataAdapter
        Dim sb As New StringBuilder

        'Classes for this start date
        With sb
            .Append("SELECT t1.ClsSectionId,t1.ReqId,t1.ClsSection,t1.InstructorId,t1.StartDate, ")
            .Append("       t1.EndDate,t1.MaxStud,t1.ModUser,t1.ModDate,t1.CampusId,t1.GrdScaleId,t1.ShiftId,t2.TermId ")
            .Append("FROM arClassSections t1, arClassSectionTerms t2 ")
            .Append("WHERE t1.ClsSectionId=t2.ClsSectionId ")
            .Append("AND t2.TermId = ? ")
            .Append("ORDER BY t1.StartDate ")
        End With

        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.OpenConnection()

        Try
            daClassSections = db.RunParamSQLDataAdapter(sb.ToString)
            daClassSections.Fill(ds, "ClassSections")
        Catch ex As System.Exception
            db.CloseConnection()
            Throw ex
        End Try

        sb.Remove(0, sb.Length)
        db.ClearParameters()

        'Meetings for the classes for this start date
        With sb
            .Append("SELECT csm.ClsSectMeetingId,csm.ClsSectionId,RoomId,PeriodId,AltPeriodId,StartDate,EndDate,ModUser,ModDate ")
            .Append("FROM arClsSectMeetings csm ")
            .Append("WHERE csm.ClsSectionId IN (SELECT t1.ClsSectionId ")
            .Append("                           FROM arClassSections t1, arClassSectionTerms t2 ")
            .Append("                           WHERE t1.ClsSectionId=t2.ClsSectionId ")
            .Append("                           AND t2.TermId = ?) ")
            .Append("ORDER BY StartDate ")
        End With

        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            daClsSectMeetings = db.RunParamSQLDataAdapter(sb.ToString)
            daClsSectMeetings.Fill(ds, "ClassSectionMeetings")
            'Dim dt As DataTable = ds.Tables("ClassSectionMeetings")
            ''dt.Load(daClsSectMeetings)
            'If (dt.Rows.Count > 0) Then
            '    Dim index As Integer = 0
            '    For index = 0 To dt.Rows.Count - 1
            '        If (dt.Rows(index)("StartDate") Is System.DBNull.Value) Then
            '            Dim dat() As String = GetDatesIfNull(termId, dt.Rows(index)("ClsSectionId").ToString())
            '            dt.Rows(index)("StartDate") = dat(0)
            '            dt.Rows(index)("EndDate") = dat(1)
            '        End If
            '    Next

            'End If

        Catch ex As Exception
            db.CloseConnection()
            Throw ex
        End Try

        sb.Remove(0, sb.Length)
        db.ClearParameters()

        'Get the related starts for the classes being offered for this start date.
        With sb
            .Append("SELECT cst.ClsSectTermId,cst.ClsSectionId,cst.TermId,cst.ModDate,cst.ModUser ")
            .Append("FROM arClassSectionTerms cst ")
            .Append("WHERE cst.ClsSectionId IN(SELECT t1.ClsSectionId ")
            .Append("                           FROM arClassSections t1, arClassSectionTerms t2 ")
            .Append("                           WHERE t1.ClsSectionId=t2.ClsSectionId ")
            .Append("                           AND t2.TermId = ?) ")
            .Append("AND cst.TermId <> ? ")
        End With

        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            daRelatedStarts = db.RunParamSQLDataAdapter(sb.ToString)
            daRelatedStarts.Fill(ds, "RelatedStarts")
        Catch ex As Exception
            db.CloseConnection()
            Throw ex
        End Try

        sb.Remove(0, sb.Length)
        db.ClearParameters()

        'Get the Shift and Grade scale associated with this start. Remember that these fields are really tied to a class section.
        'However, all the class sections for a start date will be using the same shift and grade scale so we just need to find  
        'out what these are for any class that has been created for this start.
        With sb
            .Append("SELECT DISTINCT t1.GrdScaleId,t1.ShiftId ")
            .Append("FROM arClassSections t1, arClassSectionTerms t2 ")
            .Append("WHERE t1.ClsSectionId=t2.ClsSectionId ")
            .Append("AND t2.TermId = ? ")
        End With

        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            daRelatedStarts = db.RunParamSQLDataAdapter(sb.ToString)
            daRelatedStarts.Fill(ds, "GradeScaleAndShift")
        Catch ex As Exception
            db.CloseConnection()
            Throw ex
        End Try

        sb.Remove(0, sb.Length)
        db.ClearParameters()


        db.CloseConnection()

        Return ds

    End Function

    Public Function GetOverlappingClassesForStartDate(ByVal termID As String, ByVal shift As String) As DataTable
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        With sb
            .Append("SELECT t1.ClsSectionId,'(' + CONVERT(varchar(12),t4.StartDate,1) + ')' + ")
            .Append(" + '(' + CONVERT(varchar(12),t1.StartDate,1) + '-' + CONVERT(varchar(12),t1.EndDate,1) + ')' + '(' + t3.Code + ') ' + t3.Descrip AS Class ")
            '.Append(",'(' + CONVERT(varchar(12),t1.StartDate,1) + '-' + CONVERT(varchar(12),t1.EndDate,1) + ')'  as SEDate ")
            .Append(" FROM arClassSections t1, arClassSectionTerms t2, arReqs t3, arTerm t4 ")
            .Append("WHERE t1.ClsSectionId=t2.ClsSectionId ")
            .Append("AND t1.ReqId=t3.ReqId ")
            .Append("AND t2.TermId=t4.TermId ")
            .Append("AND t4.ProgId = (SELECT ProgId ")
            .Append("                 FROM arTerm ")
            .Append("                 WHERE TermId = ?) ")
            .Append("AND t2.TermId <> ? ")
            .Append("AND t1.StartDate >= (SELECT StartDate ")
            .Append("                     FROM arTerm ")
            .Append("                     WHERE TermId = ?) ")
            .Append("AND t1.EndDate <= (SELECT EndDate ")
            .Append("                   FROM arTerm ")
            .Append("                   WHERE TermId = ?) ")
            .Append("AND t1.ClsSectionId NOT IN(SELECT cst.ClsSectionId ")
            .Append("                           FROM arClassSectionTerms cst ")
            .Append("                           WHERE cst.TermId = ?) ")
            .Append(" AND t1.ShiftId = ? ")
            .Append("AND t4.TermId = (SELECT TOP 1 TermId ")
            .Append("                   FROM arTerm tm2 ")
            .Append("                   WHERE tm2.ProgId = (SELECT ProgId ")
            .Append("                                       FROM arTerm ")
            .Append("                                       WHERE TermId = ?) ")
            .Append("                   AND tm2.StartDate < (SELECT StartDate FROM arTerm tm3 WHERE TermId = ?) ")
            .Append("                   ORDER BY tm2.StartDate DESC) ")
            .Append("ORDER BY t4.StartDate,t1.StartDate ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        db.AddParameter("@termid", termID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@termid", termID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@termid", termID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@termid", termID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@termid", termID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@shiftid", shift, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@termid", termID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@termid", termID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Try
            ds = db.RunParamSQLDataSet(sb.ToString)
            Return ds.Tables(0)
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        End Try


    End Function

    Public Function AddClassesToStartDate(ByVal arrCS As ArrayList, ByVal termID As String, ByVal user As String) As String
        Dim sb As New StringBuilder
        Dim db As New DataAccess

        Dim groupTrans As OleDbTransaction = db.StartTransaction
        Try
            For Each csID As String In arrCS

                With sb
                    .Append("INSERT INTO arClassSectionTerms(ClsSectTermId,ClsSectionId,TermId,ModDate,ModUser) ")
                    .Append("VALUES(?,?,?,?,?);")
                    '.Append("SELECT COUNT(*) FROM arClassSectionTerms WHERE ModDate=? ")
                End With

                'ClsSectTermId
                db.AddParameter("@ClsSectTermId", System.Guid.NewGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                'ClsSectionId
                db.AddParameter("@ClsSectionId", XmlConvert.ToGuid(csID), DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                'TermId
                db.AddParameter("@TermId", XmlConvert.ToGuid(termID), DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                'ModDate
                Dim strnow As Date
                strnow = Date.Now
                Dim sDate As String = strnow.Date.ToShortDateString
                Dim sHour As String = strnow.Hour.ToString
                Dim sMinute As String = strnow.Minute.ToString
                Dim iSecs As Integer = strnow.Second
                strnow = Convert.ToDateTime(sDate + " " + sHour + ":" + sMinute + ":" + iSecs.ToString)
                db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                'ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.RunParamSQLScalar(sb.ToString, groupTrans)

                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Next

            groupTrans.Commit()
        Catch ex As System.Exception
            'groupTrans.Rollback()
            If ex.InnerException Is Nothing Then
                Return ex.Message
            Else
                Return ex.InnerException.Message
            End If
        End Try

    End Function

    Public Function ProcessStartDateClassesAndMeetings(ByVal dtEntries As DataTable, ByVal dsDBInfo As DataSet, _
            ByVal termID As String, ByVal modUser As String, ByVal campusId As String, ByVal grdScaleId As String, _
            ByVal shiftID As String) As String
        'At this stage the dt passed should not be empty. It is possible that the ds might be empty.
        'We need to loop through each row in the dt:
        '   If the class section does not exist in the ds then we are definitely dealing with an INSERT.
        '   If the class section exists in the ds then:
        '           (1) we could be updating the class section
        '           (2) we could be updating the meetings
        '           (3) we could be adding meetings. 
        Dim dr As DataRow
        Dim drProcessed As DataRow
        Dim drClassSections As DataRow
        Dim db As New DataAccess
        Dim csr As New ClsSectionResultInfo
        Dim dtProcessed As New DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.OpenConnection()
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        'Each time we process a class section from the table of user entries sent to us we will add an entry for it
        'in this table. This is necessary because a class section might have severl meet records which means that 
        'the class section id can appear more than once in the table sent to us.
        dtProcessed.Columns.Add("ClsSectionId", Type.GetType("System.String"))

        Try
            For Each dr In dtEntries.Rows
                'Check if the class section associated with the row has been processed already
                If Not HasClassBeenProcessed(dr("ClsSectionId"), dtProcessed) Then
                    'Check if the class section associated with the row exists in the ds.
                    If Not DoesClassExistsInDS(dr("ClsSectionId"), dsDBInfo) Then
                        'Add the class section. 
                        AddClassSection(BuildCassSectionInfoObject(dr, dtEntries, grdScaleId, campusId, shiftID, termID), modUser, db, groupTrans)
                        'Update the processed databale
                        drProcessed = dtProcessed.NewRow
                        drProcessed("ClsSectionId") = dr("ClsSectionId")
                        dtProcessed.Rows.Add(drProcessed)
                        'Add the class section meetings
                        AddClsSectMeetings(dr, dtEntries, modUser, db, groupTrans)
                        'Add any selected related terms
                        AddRelatedTerms(dr, dtEntries, modUser, db, groupTrans)
                    Else
                        'The class section exists so we need to do an update if there have been any changes to any of the fields for
                        'the class.
                        If HasClassInformationBeenChanged(dr, dtEntries, grdScaleId, shiftID, dsDBInfo) Then
                            UpdateClassSection(BuildCassSectionInfoObject(dr, dtEntries, grdScaleId, campusId, shiftID, termID), modUser, db, groupTrans)
                        End If
                        'Update the processed datatable
                        drProcessed = dtProcessed.NewRow
                        drProcessed("ClsSectionId") = dr("ClsSectionId")
                        dtProcessed.Rows.Add(drProcessed)
                        'Update the class section meetings
                        UpdateClsSectMeetings(dr, dtEntries, dsDBInfo, modUser, db, groupTrans)
                        'Update the related starts
                        UpdateRelatedStarts(dr, dtEntries, dsDBInfo, termID, modUser, db, groupTrans)
                    End If
                End If


            Next

            'We also need to see if there any class sections in the DS but not in the user entries dt
            'If there are then we will need to delete the class sections and associated meetings from the db.
            'Since the class section id can only be in the db once we can simply loop through the rows in the
            'class section dt.
            For Each drClassSections In dsDBInfo.Tables("ClassSections").Rows
                If Not DoesClassExistsInUserEntries(drClassSections, dtEntries) Then
                    'First delete the meetings associated with the class
                    DeleteClsSectMeetings(drClassSections, dsDBInfo, db, groupTrans)
                    'Delete the class relation to the current start
                    DeleteClassSection(drClassSections("ClsSectionId"), XmlConvert.ToGuid(termID), db, groupTrans)
                End If
            Next

            'We also need to see if there are any meetings in the DS but not in the user entries dt.
            'This is important because a class might have several meet date records. The user might just want to delete
            'one of those meetings but not the entire class itself. In that case, the previous code above would not
            'delete the meeting because the class is in both the user entries and the DS. We don't want all the meetings
            'in the DS but not in the user entried dt. The previous code will take care of any meetings that belong to a
            'class that is in the DS but not in the user entries dt. We are interested in the ones that belong to a class
            'that is in both the DS and the user entries dt but the meeting is in the DS but not in the user entries dt.
            For Each drClassSections In dsDBInfo.Tables("ClassSections").Rows
                If DoesClassExistsInUserEntries(drClassSections, dtEntries) Then
                    ProcessClassThatExistsInDSandUserEntries(drClassSections, dsDBInfo, dtEntries, db, groupTrans)
                End If
            Next

            'delete the class section meetings that belong to classes that are no longer
            'related to any term (start)
            DeleteClsSectMtngsForOrphanedClassSections(db, groupTrans)

            'delete any results for the classes that are no longer related to any term
            DeleteResultsForOrphanedClassSections(db, groupTrans)

            'delete the classes that are no longer related to any term (start)
            DeleteOrphanedClassSections(db, groupTrans)

            groupTrans.Commit()
            Return ""
        Catch ex As Exception
            groupTrans.Rollback()
            If ex.InnerException Is Nothing Then
                Return ex.Message
            Else
                Return ex.InnerException.Message
            End If
        Finally
            db.CloseConnection()
        End Try


        Return ""

    End Function

    Private Function HasClassInformationBeenChanged(ByVal dr As DataRow, ByVal dtEntries As DataTable, ByVal grdScaleId As String, _
            ByVal shiftID As String, ByVal dsDBInfo As DataSet) As Boolean
        'We want to compare the information for the class in both the dtEntries table and the dsDBInfo DS.
        Dim drDB() As DataRow
        Dim minDate As String
        Dim maxDate As String

        minDate = dtEntries.Compute("MIN(StartDate)", "ClsSectionId = '" & XmlConvert.ToGuid(dr("ClsSectionId")).ToString & "'")
        maxDate = dtEntries.Compute("MAX(EndDate)", "ClsSectionId = '" & XmlConvert.ToGuid(dr("ClsSectionId")).ToString & "'")
        drDB = dsDBInfo.Tables("ClassSections").Select("ClsSectionId ='" & XmlConvert.ToGuid(dr("ClsSectionId")).ToString & "'")

        If drDB(0)("ReqId").ToString() <> dr("ReqId") Or drDB(0)("MaxStud").ToString() <> dr("MaxStud") Or _
           drDB(0)("InstructorId").ToString() <> dr("InstructorId") Or _
           drDB(0)("StartDate").ToShortDateString() <> minDate Or drDB(0)("EndDate").ToShortDateString() <> maxDate Or _
           drDB(0)("GrdScaleId").ToString() <> grdScaleId Or drDB(0)("ShiftId").ToString() <> shiftID Then
            Return True
        End If

        Return False

    End Function

    Private Function HasMeetingInformationBeenChanged(ByVal drMeeting As DataRow, ByVal dtEntries As DataTable, ByVal dsDBInfo As DataSet) As Boolean
        'We want to compare the information for the meeting in both the dtEntries table and the dsDBInfo DS.
        Dim drDB() As DataRow

        drDB = dsDBInfo.Tables("ClassSectionMeetings").Select("ClsSectMeetingId ='" & XmlConvert.ToGuid(drMeeting("ClsSectMeetingId")).ToString & "'")

        If drDB(0)("PeriodId").ToString() <> drMeeting("PeriodId") Or drDB(0)("AltPeriodId").ToString() <> drMeeting("AltPeriodId") Or _
           drDB(0)("RoomId").ToString() <> drMeeting("RoomId") Or drDB(0)("StartDate").ToShortDateString() <> drMeeting("StartDate") Or _
           drDB(0)("EndDate").ToShortDateString() <> drMeeting("EndDate") Then
            Return True
        End If

        Return False

    End Function

    Private Sub ProcessClassThatExistsInDSandUserEntries(ByVal drClassSections As DataRow, ByVal dsDBInfo As DataSet, _
        ByVal dtEntries As DataTable, ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        Dim filter As String
        Dim arrMeetings() As DataRow

        filter = "ClsSectionId = '" & drClassSections("ClsSectionId").ToString & "'"

        'Get the meetings in the DS that belongs to the class section being processed
        arrMeetings = dsDBInfo.Tables("ClassSectionMeetings").Select(filter)

        If arrMeetings.Length > 0 Then
            'Check if each meeting is in the user entries dt. If not we will delete it.
            For Each drMeeting As DataRow In arrMeetings
                If Not DoesMeetingExistsInUserEntries(drMeeting, dtEntries) Then
                    DeleteOneClsSectMeeting(drMeeting("ClsSectMeetingId"), db, groupTrans)
                End If
            Next
        End If



    End Sub

    Private Function BuildCassSectionInfoObject(ByVal dr As DataRow, ByVal dtEntries As DataTable, ByVal grdScaleId As String, _
                ByVal campusId As String, ByVal shiftID As String, ByVal termID As String) As ClassSectionInfo

        Dim csi As New ClassSectionInfo
        Dim minDate As String
        Dim maxDate As String
        Dim convertedClsSectionId As Guid

        csi.ClsSectId = XmlConvert.ToGuid(dr("ClsSectionId"))
        csi.CampusId = XmlConvert.ToGuid(campusId)
        csi.CourseId = XmlConvert.ToGuid(dr("ReqId"))
        csi.GrdScaleId = XmlConvert.ToGuid(grdScaleId)
        If dr("InstructorId") <> "" Then
            csi.InstructorId = XmlConvert.ToGuid(dr("InstructorId"))
        End If
        csi.ShiftId = XmlConvert.ToGuid(shiftID)

        If dr("MaxStud") <> "" Then
            csi.MaxStud = dr("MaxStud")
        End If

        csi.TermId = XmlConvert.ToGuid(termID)
        csi.Section = "A"

        'A class might have serveral meet date records in the datatable so to get the start and end dates we cannot simply
        'use those fields for the record being processed. We have to get the min and max values for those fields for the
        'class being processed.
        Dim filter As String
        convertedClsSectionId = XmlConvert.ToGuid(dr("ClsSectionId"))
        filter = "ClsSectionId = '" & convertedClsSectionId.ToString & "'"
        minDate = dtEntries.Compute("MIN(StartDate)", filter)
        maxDate = dtEntries.Compute("MAX(EndDate)", filter)
        csi.StartDate = minDate
        csi.EndDate = maxDate



        Return csi

    End Function

    Private Function BuildClassSectMeetingInfoObject(ByVal dr As DataRow) As ClsSectMeetingInfo
        Dim csm As New ClsSectMeetingInfo

        csm.ClsSectMtgId = XmlConvert.ToGuid(dr("ClsSectMeetingId"))
        csm.ClsSectId = XmlConvert.ToGuid(dr("ClsSectionId"))
        csm.StartDate = dr("StartDate")
        csm.EndDate = dr("EndDate")

        If dr("PeriodId") <> "" Then
            csm.PeriodId = XmlConvert.ToGuid(dr("PeriodId"))
        End If

        If dr("AltPeriodId") <> "" Then
            csm.AltPeriodId = XmlConvert.ToGuid(dr("AltPeriodId"))
        End If

        If dr("RoomId") <> "" Then
            csm.Room = XmlConvert.ToGuid(dr("RoomId"))
        End If


        Return csm

    End Function

    Private Sub AddClassSection(ByVal ClassSectionObject As ClassSectionInfo, ByVal user As String, ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        Dim strSQL As New StringBuilder
        Dim count As Integer
        Dim resultInfo As New ClsSectionResultInfo

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ClearParameters()
        '   Validate uniqueness of class section.
        '   Get the number of class section with same values in these fields: ClsSection,TermId,ReqId and CampusId
        count = DoesClsSectionExist(ClassSectionObject, ClassSectionObject.TermId.ToString)

        If count >= 1 Then
            Throw New System.Exception("You cannot create a two or more classes with the same course" + vbCr + "with the same section numbers")
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With strSQL
            .Append("INSERT INTO arClassSections ")
            .Append("(ClsSectionId, TermId, ReqId, InstructorId,ShiftId,CampusId, GrdScaleId, ClsSection, StartDate, EndDate, MaxStud,ModUser,ModDate) ")
            .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?);")
            .Append("SELECT COUNT(*) FROM arClassSections WHERE ModDate=? ")
        End With

        ' Set the DateCreated time to now
        db.AddParameter("@clsSectid", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@termid", ClassSectionObject.TermId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@courseid", ClassSectionObject.CourseId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        If ClassSectionObject.InstructorId.ToString = Guid.Empty.ToString Or ClassSectionObject.InstructorId.ToString = "" Then
            db.AddParameter("@empid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@empid", ClassSectionObject.InstructorId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        If ClassSectionObject.ShiftId.ToString = Guid.Empty.ToString Or ClassSectionObject.ShiftId.ToString = "" Then
            db.AddParameter("@shiftid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@shiftid", ClassSectionObject.ShiftId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        If ClassSectionObject.CampusId.ToString = Guid.Empty.ToString Or ClassSectionObject.CampusId.ToString = "" Then
            db.AddParameter("@campusid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@campusid", ClassSectionObject.CampusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        db.AddParameter("@grdscaleid", ClassSectionObject.GrdScaleId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        db.AddParameter("@section", ClassSectionObject.Section, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@startdate", ClassSectionObject.StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@enddate", ClassSectionObject.EndDate, DataAccess.OleDbDataType.OleDbDateTime, 16, ParameterDirection.Input)
        db.AddParameter("@maxstud", ClassSectionObject.MaxStud, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)

        'ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Dim strnow As Date
        strnow = Date.Now
        Dim sDate As String = strnow.Date.ToShortDateString
        Dim sHour As String = strnow.Hour.ToString
        Dim sMinute As String = strnow.Minute.ToString
        Dim iSecs As Integer = strnow.Second
        strnow = Convert.ToDateTime(sDate + " " + sHour + ":" + sMinute + ":" + iSecs.ToString)

        'ModDate
        db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        'ModDate
        db.AddParameter("@ModDate2", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   execute the query
        Dim rowCount As Integer = db.RunParamSQLScalar(strSQL.ToString, groupTrans)

        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

        With strSQL
            .Append("INSERT INTO arClassSectionTerms(ClsSectTermId,ClsSectionId,TermId,ModDate,ModUser) ")
            .Append("VALUES(?,?,?,?,?);")
            .Append("SELECT COUNT(*) FROM arClassSectionTerms WHERE ModDate=? ")
        End With

        'ClsSectTermId
        db.AddParameter("@ClsSectTermId", System.Guid.NewGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        'ClsSectionId
        db.AddParameter("@ClsSectionId", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        'TermId
        db.AddParameter("@TermId", ClassSectionObject.TermId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        'ModDate
        db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        'ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'ModDate
        db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)


        rowCount = db.RunParamSQLScalar(strSQL.ToString, groupTrans)

        db.ClearParameters()

    End Sub

    Public Sub UpdateClassSection(ByVal ClassSectionObject As ClassSectionInfo, ByVal user As String, ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        Dim strSQL As New StringBuilder
        Dim count As Integer
        Dim resultInfo As New ClsSectionResultInfo
        Dim strnow As Date

        db.ClearParameters()

        With strSQL
            .Append("UPDATE arClassSections SET ")
            .Append("TermId = ?")
            .Append(",ReqId = ?")
            .Append(",InstructorId = ?")
            .Append(",ShiftId = ?")
            .Append(",CampusId = ?")
            .Append(",GrdScaleId = ?")
            .Append(",ClsSection = ?")
            .Append(",StartDate = ?")
            .Append(",EndDate = ?")
            .Append(",MaxStud = ?")
            .Append(",ModUser=? ")
            .Append(",ModDate=? ")
            .Append(" WHERE ClsSectionId = ? ")
            '.Append("AND ModDate = ? ;")
            '.Append("Select count(*) from arClassSections where ModDate = ? ")
        End With

        db.AddParameter("@termid", ClassSectionObject.TermId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@courseid", ClassSectionObject.CourseId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        If ClassSectionObject.InstructorId.ToString = Guid.Empty.ToString Or ClassSectionObject.InstructorId.ToString = "" Then
            db.AddParameter("@empid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@empid", ClassSectionObject.InstructorId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        If ClassSectionObject.ShiftId.ToString = Guid.Empty.ToString Or ClassSectionObject.ShiftId.ToString = "" Then
            db.AddParameter("@shiftid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@shiftid", ClassSectionObject.ShiftId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If

        If ClassSectionObject.CampusId.ToString = Guid.Empty.ToString Or ClassSectionObject.CampusId.ToString = "" Then
            db.AddParameter("@campid", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@campusid", ClassSectionObject.CampusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        db.AddParameter("@grdscaleid", ClassSectionObject.GrdScaleId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        db.AddParameter("@section", ClassSectionObject.Section, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@startdate", ClassSectionObject.StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@enddate", ClassSectionObject.EndDate, DataAccess.OleDbDataType.OleDbDateTime, 16, ParameterDirection.Input)
        db.AddParameter("@maxstud", ClassSectionObject.MaxStud, DataAccess.OleDbDataType.OleDbInteger, 16, ParameterDirection.Input)
        ''ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        strnow = Utilities.GetAdvantageDBDateTime(Date.Now)

        db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        db.AddParameter("@clsSectid", ClassSectionObject.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        '   ModDate
        'db.AddParameter("@Original_ModDate", ClassSectionObject.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   ModDate
        'db.AddParameter("@Updated_ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   execute the query


        Dim rowCount As Integer = db.RunParamSQLScalar(strSQL.ToString, groupTrans)

        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub

    Public Sub DeleteClassSection(ByVal ClsSectID As Guid, ByVal termId As Guid, ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        'Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        Dim rowCount As Integer

        db.ClearParameters()

        With strSQL
            .Append("DELETE FROM arClassSectionTerms ")
            .Append("WHERE ClsSectionId=? AND TermId=? ")
        End With


        db.AddParameter("@ClsSectionId", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(strSQL.ToString, groupTrans)




        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

        'A class can be shared by many starts. We should only delete it if is only be used by one class.
        'If it is being used by just one class then when the code gets to this point there should be no
        'reference to it in the arClassSectionTerms table because of the earlier delete from that table.
        With strSQL
            .Append("DELETE FROM arClassSections WHERE ClsSectionId = ? ")
            .Append("AND 0 = (SELECT COUNT(*) ")
            .Append("FROM arClassSectionTerms ")
            .Append("WHERE ClsSectionId = ?) ")
        End With
        db.AddParameter("@clssectionid", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@clssectionid", ClsSectID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)





        db.RunParamSQLExecuteNoneQuery(strSQL.ToString, groupTrans)


        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)











    End Sub

    Private Sub AddSingleClsSectMeeting(ByVal ClsSectMeeting As ClsSectMeetingInfo, ByVal user As String, ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        Dim strSQL As New StringBuilder
        Dim resinfo As New ClsSectMtgIdObjInfo

        db.ClearParameters()

        With strSQL
            .Append("INSERT INTO arClsSectMeetings ")
            .Append("(ClsSectMeetingId, ClsSectionId, PeriodId,AltPeriodId, RoomId,StartDate,EndDate, ModUser,ModDate) ")
            .Append("VALUES(?,?,?,?,?,?,?,?,?)")
        End With

        db.AddParameter("@csmid", ClsSectMeeting.ClsSectMtgId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@clsSectid", ClsSectMeeting.ClsSectId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        If ClsSectMeeting.PeriodId.ToString = Guid.Empty.ToString Or ClsSectMeeting.PeriodId.ToString = "" Then
            db.AddParameter("@period", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@period", ClsSectMeeting.PeriodId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If

        If ClsSectMeeting.AltPeriodId.ToString = Guid.Empty.ToString Or ClsSectMeeting.AltPeriodId.ToString = "" Then
            db.AddParameter("@altperiod", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@altperiod", ClsSectMeeting.AltPeriodId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If

        If ClsSectMeeting.Room.ToString = Guid.Empty.ToString Or ClsSectMeeting.Room.ToString = "" Then
            db.AddParameter("@Room", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@Room", ClsSectMeeting.Room, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If

        db.AddParameter("@sdate", ClsSectMeeting.StartDate, DataAccess.OleDbDataType.OleDbDateTime, 16, ParameterDirection.Input)
        db.AddParameter("@edate", ClsSectMeeting.EndDate, DataAccess.OleDbDataType.OleDbDateTime, 16, ParameterDirection.Input)
        ''ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Dim strnow As Date
        strnow = Date.Now
        Dim sDate As String = strnow.Date.ToShortDateString
        Dim sHour As String = strnow.Hour.ToString
        Dim sMinute As String = strnow.Minute.ToString
        Dim iSecs As Integer = strnow.Second
        strnow = Convert.ToDateTime(sDate + " " + sHour + ":" + sMinute + ":" + iSecs.ToString)

        'ModDate
        db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)


        db.RunParamSQLExecuteNoneQuery(strSQL.ToString, groupTrans)
        strSQL.Remove(0, strSQL.Length)

    End Sub

    Public Sub UpdateSingleClsSectMtg(ByVal ClsSectMeeting As ClsSectMeetingInfo, ByVal user As String, ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        Dim strSQL As New StringBuilder
        Dim resultInfo As New ClsSectMtgIdObjInfo
        Dim strnow As Date

        db.ClearParameters()

        With strSQL
            .Append("UPDATE arClsSectMeetings SET ")
            .Append("PeriodId = ?")
            .Append(",AltPeriodId =?")
            .Append(",RoomId = ?")
            .Append(",StartDate = ? ")
            .Append(",EndDate = ? ")
            .Append(",ModUser=? ")
            .Append(",ModDate=? ")
            .Append(" WHERE ClsSectMeetingId = ? ")
            '.Append(" AND ModDate = ? ;")
            '.Append("Select count(*) from arClsSectMeetings where ModDate = ? ")
            '.Append("And ClsSectMeetingId = ? ")
        End With

        If ClsSectMeeting.PeriodId.ToString = Guid.Empty.ToString Or ClsSectMeeting.PeriodId.ToString = "" Then
            db.AddParameter("@period", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@period", ClsSectMeeting.PeriodId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If

        If ClsSectMeeting.AltPeriodId.ToString = Guid.Empty.ToString Or ClsSectMeeting.AltPeriodId.ToString = "" Then
            db.AddParameter("@altperiod", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@altperiod", ClsSectMeeting.AltPeriodId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If

        If ClsSectMeeting.Room.ToString = Guid.Empty.ToString Or ClsSectMeeting.Room.ToString = "" Then
            db.AddParameter("@Room", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@Room", ClsSectMeeting.Room, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        db.AddParameter("@sdate", ClsSectMeeting.StartDate, DataAccess.OleDbDataType.OleDbDateTime, 16, ParameterDirection.Input)
        db.AddParameter("@edate", ClsSectMeeting.EndDate, DataAccess.OleDbDataType.OleDbDateTime, 16, ParameterDirection.Input)
        ''ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        strnow = Date.Now

        Dim sDate As String = strnow.Date.ToShortDateString
        Dim sHour As String = strnow.Hour.ToString
        Dim sMinute As String = strnow.Minute.ToString
        Dim iSecs As Integer = strnow.Second
        strnow = Convert.ToDateTime(sDate + " " + sHour + ":" + sMinute + ":" + iSecs.ToString)

        db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        db.AddParameter("@clsSectid", ClsSectMeeting.ClsSectMtgId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        '   ModDate
        'db.AddParameter("@Original_ModDate", ClsSectMeeting.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   ModDate
        'db.AddParameter("@Updated_ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        'db.AddParameter("@clsSectid", ClsSectMeeting.ClsSectMtgId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        '   execute the query

        Dim rowCount As Integer = db.RunParamSQLScalar(strSQL.ToString, groupTrans)
        db.ClearParameters()
        '   If there were no updated rows then there was a concurrency problem

    End Sub

    Public Sub DeleteOneClsSectMeeting(ByVal ClsSectMtgID As Guid, ByVal ClsSectionId As Guid, ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        'Dim db As New DataAccess
        Dim strSQL As New StringBuilder

        db.ClearParameters()
        'A class and its meetings can be shared by more than one starts. We should only delete a meeting if the class that it
        'belongs to, is only being used by one start. The current sequence that we are using when we have to delete a class
        'is to first delete the meetings, followed by the class section terms and then finally the class itself. At this point
        'then we should only delete the meetings if the count of starts referencing its parent class is only one.
        With strSQL
            .Append("DELETE FROM arClsSectMeetings WHERE ClsSectMeetingId = ? ")
            .Append("AND 1 = (SELECT COUNT(*) ")
            .Append("           FROM arClassSectionTerms ")
            .Append("           WHERE ClsSectionId = ?) ")
        End With

        db.AddParameter("@clssectionmtgid", ClsSectMtgID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@csid", ClsSectionId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(strSQL.ToString, groupTrans)
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub

    Public Sub DeleteOneClsSectMeeting(ByVal ClsSectMtgID As Guid, ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        'Dim db As New DataAccess
        Dim strSQL As New StringBuilder

        db.ClearParameters()
        'This function is used to delete a meeting from a class. There is no need to worry if the class is being used by other starts.
        'The other function above is used when a class is being deleted from a specified start. The start could still be used by other
        'starts.
        With strSQL
            .Append("DELETE FROM arClsSectMeetings WHERE ClsSectMeetingId = ? ")
        End With

        db.AddParameter("@clssectionmtgid", ClsSectMtgID, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(strSQL.ToString, groupTrans)
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub

    Private Sub AddClsSectMeetings(ByVal dr As DataRow, ByVal dtEntries As DataTable, ByVal user As String, ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        Dim arrRows() As DataRow
        Dim filter As String
        Dim gClsSectionId As Guid
        Dim drMeeting As DataRow

        'We need to find all the meetings for the class section that we are dealing with
        gClsSectionId = XmlConvert.ToGuid(dr("ClsSectionId"))
        filter = "ClsSectionId = '" & gClsSectionId.ToString & "'"
        arrRows = dtEntries.Select(filter)
        'We should have at least one row because the start date and end date fields are always required
        For Each drMeeting In arrRows
            AddSingleClsSectMeeting(BuildClassSectMeetingInfoObject(drMeeting), user, db, groupTrans)
        Next

    End Sub

    Private Sub UpdateClsSectMeetings(ByVal dr As DataRow, ByVal dtEntries As DataTable, ByVal dsDBInfo As DataSet, ByVal user As String, _
                ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        Dim arrRows() As DataRow
        Dim filter As String
        Dim gClsSectionId As Guid
        Dim drMeeting As DataRow

        'We need to find all the meetings for the class section that we are dealing with
        gClsSectionId = XmlConvert.ToGuid(dr("ClsSectionId"))
        filter = "ClsSectionId = '" & gClsSectionId.ToString & "'"
        arrRows = dtEntries.Select(filter)
        'We should have at least one row because the start date and end date fields are always required
        For Each drMeeting In arrRows
            'We will do an update if the meeting already exists in the DB and any of its fields have changed.
            'Otherwise we will do an insert.
            If DoesMeetingExistsInDB(drMeeting, dsDBInfo) Then
                If HasMeetingInformationBeenChanged(drMeeting, dtEntries, dsDBInfo) Then
                    UpdateSingleClsSectMtg(BuildClassSectMeetingInfoObject(drMeeting), user, db, groupTrans)
                End If

            Else
                AddSingleClsSectMeeting(BuildClassSectMeetingInfoObject(drMeeting), user, db, groupTrans)
            End If

        Next

    End Sub

    Private Sub DeleteClsSectMeetings(ByVal dr As DataRow, ByVal dsDBInfo As DataSet, ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        Dim arrRows() As DataRow
        Dim filter As String
        Dim drMeeting As DataRow

        'We need to find all the meetings for the class section that we are dealing with
        filter = "ClsSectionId = '" & dr("ClsSectionId").ToString & "'"
        arrRows = dsDBInfo.Tables("ClassSectionMeetings").Select(filter)
        For Each drMeeting In arrRows
            DeleteOneClsSectMeeting(drMeeting("ClsSectMeetingId"), dr("ClsSectionId"), db, groupTrans)
        Next

    End Sub

    Private Sub AddRelatedTerms(ByVal dr As DataRow, ByVal dtEntries As DataTable, ByVal modUser As String, _
                ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        Dim relatedStarts() As String
        'Check if there are any related terms
        If dr("RelatedStarts").ToString <> "" Then
            'Get an array of the related starts
            relatedStarts = dr("RelatedStarts").ToString.Split(",")
            For Each start As String In relatedStarts
                If start <> "" Then
                    AddSingleRelatedStart(dr("ClsSectionId"), start, modUser, db, groupTrans)
                End If

            Next
        End If

    End Sub

    Private Sub UpdateRelatedStarts(ByVal dr As DataRow, ByVal dtEntries As DataTable, ByVal dsDBInfo As DataSet, _
            ByVal termId As String, ByVal modUser As String, ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        'For the related starts we only need to perform inserts and deletes
        'If there are any related starts, if it does not exists in the DS then we will do an insert.
        'If there are related starts in the DS for the class being processed but not in the row pass to us
        'then we will need to delete those entries from the database.
        Dim relatedStarts() As String
        Dim arrDBStarts() As DataRow
        Dim filter As String

        'Check if there are any related starts
        If dr("RelatedStarts").ToString <> "" Then
            'Get an array of the related starts
            relatedStarts = dr("RelatedStarts").ToString.Split(",")

            For Each start As String In relatedStarts
                If start <> "" Then
                    'We will only add the related start if it is not already in the DS for the class being processed
                    If Not DoesStartExistsForClass(dr, start, dsDBInfo) Then
                        AddSingleRelatedStart(dr("ClsSectionId"), start, modUser, db, groupTrans)
                    End If
                End If
            Next

        End If

        'If there are any related starts for the class in the DS but not here we will need to delete them.
        'Check if there any related starts for the class being processed in the DS
        filter = "ClsSectionId = '" & XmlConvert.ToGuid(dr("ClsSectionId")).ToString & "' AND TermId <> '" & XmlConvert.ToGuid(termId).ToString & "'"
        If dsDBInfo.Tables("RelatedStarts").Rows.Count > 0 Then
            arrDBStarts = dsDBInfo.Tables("RelatedStarts").Select(filter)
            If arrDBStarts.Length > 0 Then
                'Check if any of the start is not in the current list of starts
                For Each drCSTerm As DataRow In arrDBStarts
                    If Not DoesStartExistsForClassInUserEntries(relatedStarts, drCSTerm("TermId").ToString()) Then
                        DeleteSingleRelatedStart(dr("ClsSectionId"), drCSTerm("TermId").ToString(), modUser, db, groupTrans)
                    End If
                Next
            End If
        End If


    End Sub

    Private Sub AddSingleRelatedStart(ByVal clsSectionId As String, ByVal termId As String, ByVal user As String, _
            ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        Dim sb As New StringBuilder

        db.ClearParameters()

        With sb
            .Append("INSERT INTO arClassSectionTerms(ClsSectTermId,ClsSectionId,TermId,ModDate,ModUser) ")
            .Append("VALUES(?,?,?,?,?);")
            '.Append("SELECT COUNT(*) FROM arClassSectionTerms WHERE ModDate=? ")
        End With

        'ClsSectTermId
        db.AddParameter("@ClsSectTermId", System.Guid.NewGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        'ClsSectionId
        db.AddParameter("@ClsSectionId", XmlConvert.ToGuid(clsSectionId), DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        'TermId
        db.AddParameter("@TermId", XmlConvert.ToGuid(termId), DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        Dim strnow As Date
        strnow = Date.Now
        Dim sDate As String = strnow.Date.ToShortDateString
        Dim sHour As String = strnow.Hour.ToString
        Dim sMinute As String = strnow.Minute.ToString
        Dim iSecs As Integer = strnow.Second
        strnow = Convert.ToDateTime(sDate + " " + sHour + ":" + sMinute + ":" + iSecs.ToString)
        'ModDate
        db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        'ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.RunParamSQLScalar(sb.ToString, groupTrans)

        db.ClearParameters()

    End Sub

    Private Sub DeleteSingleRelatedStart(ByVal clsSectionId As String, ByVal termId As String, ByVal user As String, _
            ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        Dim sb As New StringBuilder

        db.ClearParameters()

        With sb
            .Append("DELETE FROM arClassSectionTerms ")
            .Append("WHERE ClsSectionId = ? ")
            .Append("AND TermId = ? ")
        End With

        'ClsSectionId
        db.AddParameter("@ClsSectionId", XmlConvert.ToGuid(clsSectionId), DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        'TermId
        db.AddParameter("@TermId", XmlConvert.ToGuid(termId), DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

        db.RunParamSQLScalar(sb.ToString, groupTrans)

        db.ClearParameters()

    End Sub

    Private Function DoesMeetingExistsInDB(ByVal drMeeting As DataRow, ByVal dsDBInfo As DataSet) As Boolean
        Dim filter As String
        Dim arrRows() As DataRow

        filter = "ClsSectMeetingId ='" & XmlConvert.ToGuid(drMeeting("ClsSectMeetingId")).ToString & "'"
        If dsDBInfo.Tables("ClassSectionMeetings").Rows.Count = 0 Then
            Return False
        Else
            arrRows = dsDBInfo.Tables("ClassSectionMeetings").Select(filter)
            If arrRows.Length = 0 Then
                Return False
            Else
                Return True
            End If
        End If
    End Function

    Private Function DoesStartExistsForClass(ByVal dr As DataRow, ByVal start As String, ByVal dsDBInfo As DataSet) As Boolean
        Dim filter As String
        Dim arrRows() As DataRow

        filter = "ClsSectionId = '" & XmlConvert.ToGuid(dr("ClsSectionId")).ToString & "' AND TermId = '" & XmlConvert.ToGuid(start).ToString & "'"
        If dsDBInfo.Tables("ClassSections").Rows.Count = 0 Then
            Return False
        Else
            arrRows = dsDBInfo.Tables("RelatedStarts").Select(filter)
            If arrRows.Length = 0 Then
                Return False
            Else
                Return True
            End If
        End If
    End Function

    Private Function DoesStartExistsForClassInUserEntries(ByVal relatedStarts() As String, ByVal termId As String) As Boolean
        If relatedStarts Is Nothing Then
            Return False
        ElseIf relatedStarts.Length = 0 Then
            Return False
        Else
            For Each start As String In relatedStarts
                If start.ToString = termId.ToString Then
                    Return True
                End If
            Next
        End If

        Return False
    End Function

    Private Function DoesClassExistsInUserEntries(ByVal drCS As DataRow, ByVal dtEntries As DataTable) As Boolean
        Dim arrRows() As DataRow
        Dim filter As String
        'If the dt is empty then simply returns false
        If dtEntries.Rows.Count = 0 Then
            Return False
        Else
            'Search the dt
            filter = "ClsSectionId = '" & drCS("clsSectionId").ToString & "'"
            arrRows = dtEntries.Select(filter)
            If arrRows.Length = 0 Then
                Return False
            Else
                Return True
            End If
        End If
    End Function

    Private Function DoesMeetingExistsInUserEntries(ByVal drCM As DataRow, ByVal dtEntries As DataTable) As Boolean
        Dim arrRows() As DataRow
        Dim filter As String
        'If the dt is empty then simply returns false
        If dtEntries.Rows.Count = 0 Then
            Return False
        Else
            'Search the dt
            filter = "ClsSectMeetingId = '" & drCM("ClsSectMeetingId").ToString & "'"
            arrRows = dtEntries.Select(filter)
            If arrRows.Length = 0 Then
                Return False
            Else
                Return True
            End If
        End If
    End Function

    Private Function DoesClassExistsInDS(ByVal clsSectionId As String, ByVal dsDBInfo As DataSet) As Boolean
        Dim arrRows() As DataRow
        Dim convertedClsSectionId As Guid
        Dim filter As String
        'If the dataset is empty then simply returns false
        If dsDBInfo.Tables("ClassSections").Rows.Count = 0 Then
            Return False
        Else
            'Search the dataset
            convertedClsSectionId = XmlConvert.ToGuid(clsSectionId)
            filter = "ClsSectionId = '" & convertedClsSectionId.ToString & "'"
            arrRows = dsDBInfo.Tables("ClassSections").Select(filter)
            If arrRows.Length = 0 Then
                Return False
            Else
                Return True
            End If
        End If
    End Function

    Private Function HasClassBeenProcessed(ByVal clsSectionId As String, ByVal dtProcessed As DataTable) As Boolean
        Dim arrRows() As DataRow
        Dim convertedClsSectionId As Guid
        Dim filter As String
        'If the dataset is empty then simply returns false
        If dtProcessed.Rows.Count = 0 Then
            Return False
        Else
            'Search the DataTable

            convertedClsSectionId = XmlConvert.ToGuid(clsSectionId)
            filter = "ClsSectionId = '" & convertedClsSectionId.ToString & "'"
            arrRows = dtProcessed.Select(filter)
            If arrRows.Length = 0 Then
                Return False
            Else
                Return True
            End If
        End If
    End Function

    Public Function DoesClsSectionExist(ByVal ClsSection As ClassSectionInfo, ByVal Term As String) As Integer
        Dim ds As New DataSet

        Dim da As OleDbDataAdapter

        Dim RowCount As Integer

        Dim sGrdSysDetailId As String



        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New System.Text.StringBuilder

        With sb

            .Append("Select count(*) as Count ")
            .Append("from arClassSections a ")
            .Append("where a.ClsSection = ? and a.TermId = ? ")
            .Append("and a.ReqId = ? ")
            .Append("and a.CampusId = ? ")

        End With

        db.AddParameter("@Section", ClsSection.Section, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@term", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@reqid", ClsSection.CourseId.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@cmpid", ClsSection.CampusId.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()

        RowCount = CInt(db.RunParamSQLScalar(sb.ToString))

        db.CloseConnection()

        'Return the datatable in the dataset
        Return RowCount

    End Function

    Public Function AreStuentsRegisteredForStart(ByVal termId As String) As Boolean
        Dim ds As New DataSet
        Dim RowCount As Integer
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New System.Text.StringBuilder

        With sb

            .Append("SELECT COUNT(*) as Count ")
            .Append("FROM arResults r, arClassSections c, arClassSectionTerms ct ")
            .Append("WHERE r.TestId=c.ClsSectionId ")
            .Append("AND c.ClsSectionId=ct.ClsSectionId ")
            .Append("AND ct.TermId = ? ")
        End With

        db.AddParameter("@termid", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()

        RowCount = CInt(db.RunParamSQLScalar(sb.ToString))

        db.CloseConnection()

        If RowCount > 0 Then
            Return True
        End If


        Return False

    End Function

    Public Sub DeleteClsSectMtngsForOrphanedClassSections(ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        Dim sb As New StringBuilder

        db.ClearParameters()

        With sb
            .Append("delete from arClsSectMeetings ")
            .Append("where exists(select ClsSectionId from arClassSections ")
            .Append("               where arClassSections.ClsSectionId=arClsSectMeetings.ClsSectionId ")
            .Append("               and not exists(select cst.ClsSectionId ")
            .Append("                               from arClassSectionTerms cst ")
            .Append("                               where arClassSections.ClsSectionId=cst.ClsSectionId)) ")
        End With

        db.RunParamSQLScalar(sb.ToString, groupTrans)

        db.ClearParameters()
    End Sub

    Public Sub DeleteResultsForOrphanedClassSections(ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        Dim sb As New StringBuilder

        db.ClearParameters()

        With sb
            .Append("delete * from arResults ")
            .Append("where exists(select ClsSectionId from arClassSections ")
            .Append("               where arClassSections.ClsSectionId=arResults.TestId ")
            .Append("               and not exists(select cst.ClsSectionId ")
            .Append("                               from arClassSectionTerms cst ")
            .Append("                               where arClassSections.ClsSectionId=cst.ClsSectionId)) ")
        End With

        db.RunParamSQLScalar(sb.ToString, groupTrans)

        db.ClearParameters()
    End Sub

    Public Sub DeleteOrphanedClassSections(ByVal db As DataAccess, ByVal groupTrans As OleDbTransaction)
        Dim sb As New StringBuilder

        db.ClearParameters()

        With sb
            .Append("delete from arClassSections ")
            .Append("where not exists(select cst.ClsSectionId ")
            .Append("                   from arClassSectionTerms cst ")
            .Append("                   where arClassSections.ClsSectionId=cst.ClsSectionId) ")
        End With

        db.RunParamSQLScalar(sb.ToString, groupTrans)

        db.ClearParameters()
    End Sub

End Class

