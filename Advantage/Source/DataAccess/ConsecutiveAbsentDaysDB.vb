﻿Imports FAME.Advantage.Common

Public Class ConsecutiveAbsentDaysDB
#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
#End Region

#Region "Public Properties"
    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property
#End Region
#Region "Public Methods"
    Public Function GetConsecutiveAbsentDays(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim strWhere As String
        Dim strTempWhere As String = ""
        Dim db As New SQLDataAccess
        Dim ds As New DataSet
        Dim campGrpId As String = String.Empty
        Dim prgVerId As String = String.Empty

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If
        strTempWhere = strWhere
        strTempWhere = strTempWhere.ToLower.Replace("and", ";")
        strWhere = ""
        Dim strArr As String() = strTempWhere.Remove(strTempWhere.IndexOf(";"), 1).Split(";")

        For i As Integer = 0 To strArr.Length - 1
            strWhere &= " and " & strArr(i)
        Next
        If strArr.Length = 1 Then
            strWhere &= " and "
        End If
        campGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in (")).ToLower.IndexOf(" and ")).Replace("t1.campgrpid in", "").Replace(")", "").Replace("(", "").Replace("'", "")

        'get the prgverid
        If strWhere.ToLower.Contains("arprgversions.prgverid") Then
            If strWhere.ToLower.Contains("arprgversions.prgverid in ") Then
                If strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in ")).ToLower.IndexOf(" and ") >= 0 Then
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in (")).ToLower.IndexOf(" and ")).Replace("arprgversions.prgverid in", "").Replace(")", "").Replace("(", "").Replace("'", "")
                Else
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in (")).Replace("arprgversions.prgverid in", "").Replace(")", "").Replace("(", "").Replace("'", "")
                End If
            Else
                If strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = ")).ToLower.IndexOf(" and ") >= 0 Then
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = "), strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = ")).ToLower.IndexOf(" and ")).Replace("arprgversions.prgverid =", "").Replace(")", "").Replace("(", "").Replace("'", "")
                Else
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = ")).Replace("arprgversions.prgverid =", "").Replace(")", "").Replace("(", "").Replace("'", "")
                End If
            End If
        End If
        'end the prgverid

        db.AddParameter("@CampGrpId", campGrpId.Trim(), SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@ProgramIds", IIf(Not prgVerId = "", prgVerId.Trim(), DBNull.Value), SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@ConsecutiveAbsentDays1", CType(paramInfo.NumberOfConsecutiveAbsentDays, Integer), SqlDbType.Int, , ParameterDirection.Input)
        db.AddParameter("@ConsecutiveAbsentDays2", CType(paramInfo.NumberOfConsecutiveAbsentDays2, Integer), SqlDbType.Int, , ParameterDirection.Input)
        db.AddParameter("@CompOperator", paramInfo.CompOperator, SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@StudentIdentifier", StudentIdentifier, SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@campusId", New Guid(paramInfo.CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet_SP("usp_AR_ConsecutiveAbsentDays", "ConsecutiveAbsentDays")
        Return ds
    End Function
    Public Function GetStudentSchedules(ByVal StuEnrollId As String, ByVal CampusId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim da As System.Data.OleDb.OleDbDataAdapter
        db.OpenConnection()
        With sb
            .Append(" Select PS.ScheduleId, PSD.dw, PSD.total, (Select SUM(PSDI.total) From arProgScheduleDetails PSDI Where PSDI.ScheduleId=PS.ScheduleId) as HoursTotal  from arStuEnrollments SE, arProgSchedules PS, arProgScheduleDetails PSD, arStudentSchedules SS  Where SE.PrgVerId=PS.PrgVerId and SE.StuEnrollId =SS.StuEnrollId and SS.ScheduleId =PS.ScheduleId And PS.ScheduleId=PSD.ScheduleId And PSD.total>0  And SE.StuEnrollId='" & StuEnrollId & "' Order by dw ")
        End With
        da = db.RunParamSQLDataAdapter(sb.ToString)
        da.Fill(ds, "WeekDays")
        sb = sb.Remove(0, sb.Length)
        With sb
            .Append(" Select HolidayStartDate, HolidayEndDate, AllDay  FROM syHolidays H, syCmpGrpCmps t1, syCampGrps t2, syStatuses S ")
            .Append(" where t1.CampGrpId=t2.CampGrpId AND S.StatusId=H.StatusId AND S.Status='Active' ")
            .Append(" and H.CampGrpId=t2.CampGrpId and t1.CampusId='" + CampusId + "' ")
        End With
        da = db.RunParamSQLDataAdapter(sb.ToString)
        da.Fill(ds, "Holidays")

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
#End Region


    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function


End Class
