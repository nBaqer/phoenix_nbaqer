Imports FAME.Advantage.Common

Public Class NewDB
    Public Function AddExtracurricularInfo(ByVal text As String) As Integer

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append("Insert tblNewValue(text) ")
            .Append(" Values(?) ")
        End With

        db.AddParameter("@text", text)


        'Execute The Query
        db.RunParamSQLExecuteNoneQuery(sb.ToString)

        'Retun Without Errors
        Return 0

        db.CloseConnection()
    End Function

End Class
