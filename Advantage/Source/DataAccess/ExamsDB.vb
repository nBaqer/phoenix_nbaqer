
Imports FAME.Advantage.Common
Imports Advantage.Business.Objects



Imports FAME.common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing

Imports System.Text
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects
Imports FAME.Advantage.Common.FAME.Advantage.Common
Imports FAME.Advantage.DataAccess.LINQ.Common
Imports FAME.Advantage.DataAccess.LINQ.PostExams
Public Class ExamsDB

    ''' <summary>
    ''' Store the connection string for this class
    ''' </summary>
    ''' <remarks></remarks>
    Private ReadOnly conString As String

    ''' <summary>
    ''' Application Stting for get some other differents values from config.
    ''' </summary>
    ''' <remarks></remarks>
    Private ReadOnly myAdvAppSettings As AdvAppSettings

    Sub New()
        myAdvAppSettings = AdvAppSettings.GetAppSettings()
        conString = myAdvAppSettings.AppSettings("ConString")
    End Sub

#Region "Post Services"

    Public Function GetClassroomWorkTerm(ByVal stuEnrollId As String) As DataSet
        'Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Return db.RunParamSQLDataSetUsingSP("usp_GetClassroomWorkTerm")
    End Function

    Public Function GetClassroomWorkClass(ByVal stuEnrollId As String, ByVal termId As String) As DataSet
        'Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Return db.RunParamSQLDataSetUsingSP("usp_GetClassroomWorkClass")
    End Function

    Public Function GetClassroomWorkServices(ByVal stuEnrollId As String, ByVal clsSectionId As String, ByVal gbCompType As Integer) As DataSet
        'Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@GradeBookComponentType", gbCompType, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        Return db.RunParamSQLDataSetUsingSP("usp_GetClassroomWorkServices")
    End Function
    Public Function DeleteClassroomWorkServices(ByVal GrdBkResultId As String) As DataSet
        'Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@GrdBkResultId", GrdBkResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Return db.RunParamSQLDataSetUsingSP("USP_Delete_ClassroomWorkServices")
    End Function
    Public Function GetClassroomWorkServicePosts(ByVal stuEnrollId As String, ByVal clsSectionId As String, ByVal gbWeightDetail As String) As DataSet
        'Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@InstrGrdBkWgtDetailId", gbWeightDetail, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Return db.RunParamSQLDataSetUsingSP("usp_GetClassroomWorkServicePosts")
    End Function

    Public Function GetPostedServicesByDay(ByVal stuEnrollId As String, ByVal clsSectionId As String, ByVal instrGrdBkWgtDetailId As String, ByVal postDate As Date) As DataSet
        ' Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@InstrGrdBkWgtDetailId", instrGrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@PostDate", postDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        Return db.RunParamSQLDataSetUsingSP("usp_GetPostedServicesByDay")
    End Function

    Public Function InsertPostedService(ByVal stdGrdObj As StdGrdBkInfo, ByVal user As String) As String
        Try
            'Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.AddParameter("@GrdBkResultId", stdGrdObj.GrdBkResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ClsSectionId", stdGrdObj.ClassId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", stdGrdObj.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@InstrGrdBkWgtDetailId", stdGrdObj.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@Score", stdGrdObj.Score, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
            db.AddParameter("@Comments", stdGrdObj.Comments, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ResNum", stdGrdObj.ResNum, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@IsCompGraded", stdGrdObj.IsCompGraded, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@DateCompleted", DBNull.Value)
            db.AddParameter("@PostDate", stdGrdObj.PostDate.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery("usp_AR_ArGrdBkResults_Insert", Nothing, "SP")
            Return String.Empty
        Catch ex As Exception
            Return "Error inserting service - " & ex.Message
        End Try
    End Function

    Public Function InsertPostedServiceBatch(ByVal stdGrdObj As StdGrdBkInfo, ByVal user As String, ByVal adjustment As Integer) As String
        Try
            'Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.AddParameter("@GrdBkResultId", stdGrdObj.GrdBkResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ClsSectionId", stdGrdObj.ClassId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", stdGrdObj.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@InstrGrdBkWgtDetailId", stdGrdObj.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@Score", stdGrdObj.Score, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
            db.AddParameter("@Comments", stdGrdObj.Comments, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ResNum", stdGrdObj.ResNum, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@IsCompGraded", stdGrdObj.IsCompGraded, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            db.AddParameter("@DateCompleted", DBNull.Value)
            db.AddParameter("@PostDate", stdGrdObj.PostDate.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Adjustment", adjustment, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery("usp_AR_ArGrdBkResults_Insert", Nothing, "SP")
            Return String.Empty
        Catch ex As Exception
            Return "Error inserting service - " & ex.Message
        End Try
    End Function

    Public Function SetIsCourseCompletedForServices(ByVal stuEnrollId As String, ByVal clsSectionId As String) As String
        Try
            'Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery("USP_IsCourseCompleted_ForClinicWorkorLabHours", Nothing, "SP")
            Return String.Empty
        Catch ex As Exception
            Return "Error setting course completed flag - " & ex.Message
        End Try
    End Function

#End Region

    Public Function GetExamsResultsByStudent(ByVal stuEnrollId As String, ByVal sysComponentTypeId As Integer) As DataTable

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = conString ' AdvAppSettings.GetAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select  Distinct ")
            .Append("		GCT.Descrip, ")
            .Append("       (select  top 1 GBWD1.Seq from arGrdComponentTypes GCT1, arGrdBkWgtDetails GBWD1   ")
            .Append("					where GCT1.GrdComponentTypeId = GBWD1.GrdComponentTypeId   ")
            .Append("					and		GCT1.GrdComponentTypeId = GCT.GrdComponentTypeId) As Seq, ")
            .Append("       (select  top 1 GBWD1.Weight from arGrdComponentTypes GCT1, arGrdBkWgtDetails GBWD1   ")
            .Append("					where GCT1.GrdComponentTypeId = GBWD1.GrdComponentTypeId   ")
            .Append("					and		GCT1.GrdComponentTypeId = GCT.GrdComponentTypeId) As Weight, ")
            .Append("		GBCR.Score, ")
            '.Append("		(case when GBCR.Score >= GBCR.MinResult then 1 else 0 end) as IsPass,  ")
            .Append("		(case when GBCR.Score is null then null else (case when GBCR.SCore >= GBCR.MinResult then 1 else 0 end)end) as IsPass, ")
            .Append("		GCT.SysComponentTypeId ")
            .Append("from	arGrdBkConversionResults GBCR, arGrdComponentTypes GCT ")
            .Append("where ")
            .Append("		GBCR.GrdComponentTypeId=GCT.GrdComponentTypeId	 ")
            .Append("and	GBCR.StuEnrollId= ? ")
            .Append("and	GCT.SysComponentTypeId= ? ")
            '.Append("and    GBCR.Score is not null ")
            .Append("union all ")
            .Append("select  ")
            .Append("		GCT.Descrip, ")
            .Append("       GBWD.Seq, ")
            .Append("       GBWD.Weight, ")
            .Append("		GBR.Score, ")
            .Append("		(Select IsPass from arGradeSystemDetails where GrdSysDetailId=(select GrdSysDetailId from arGradeScaleDetails ")
            .Append("			where GrdScaleId = (Select GrdScaleId from arClassSections  ")
            .Append("								where ClsSectionId=(select ClsSectionId from arGrdBkResults where GrdBkResultId=GBR.GrdBkResultId)) 		 ")
            .Append("								and		(GBR.Score >= MinVal)  ")
            .Append("								and		(GBR.Score <= MaxVal))) As IsPass, ")
            .Append("		GCT.SysComponentTypeId  ")
            .Append("from	arGrdBkResults GBR, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT ")
            .Append("where ")
            .Append("		GBR.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
            .Append("and	GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId	 ")
            .Append("and	GBR.StuEnrollId= ? ")
            .Append("and	GCT.SysComponentTypeId= ? ")
            .Append("order by Descrip, Seq ")

        End With

        '   add parameters values to the query
        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   sysComponentTypeId
        db.AddParameter("@SysComponentTypeId", sysComponentTypeId, DataAccess.OleDbDataType.OleDbInteger, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   sysComponentTypeId
        db.AddParameter("@SysComponentTypeId", sysComponentTypeId, DataAccess.OleDbDataType.OleDbInteger, ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function

    Public Function GetGradeBookResultsByStudent_SP(ByVal stuEnrollId As String) As String
        Dim rtn As String = String.Empty
        Dim db As New SQLDataAccess

        db.ConnectionString = conString ' AdvAppSettings.GetAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@stuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            Dim dr As SqlDataReader = db.RunParamSQLDataReader_SP("dbo.USP_GetGradeBookResultsByStudent")
            While dr.Read()
                rtn += dr(0)
            End While
            Return rtn
        Catch ex As Exception
        Finally
            db.CloseConnection()
        End Try
        Return rtn
    End Function

    Public Function GetClinicServicesAndHoursAttemptedByStudent(ByVal stuEnrollId As String, ByVal courseId As String) As Integer
        Dim rtn As Integer = 0
        Dim dtClinicServices As DataTable
        Dim dtClinicHours As DataTable
        dtClinicServices = GetClinicServicesAndHoursByStudent(stuEnrollId, 500, courseId, "")
        If dtClinicServices.Rows.Count > 0 Then
            For Each row As DataRow In dtClinicServices.Rows
                rtn = row("Completed")
                If rtn > 0 Then Return rtn
            Next
        End If
        dtClinicHours = GetClinicServicesAndHoursByStudent(stuEnrollId, 503, courseId, "")
        If dtClinicHours.Rows.Count > 0 Then
            For Each row As DataRow In dtClinicHours.Rows
                rtn = row("Completed")
                If rtn > 0 Then Return rtn
            Next
        End If
        Return rtn
    End Function
    Public Function GetClinicServicesAndHoursAttemptedByStudent_SP(ByVal stuEnrollId As String, ByVal courseId As String) As Integer
        Dim rtn As Integer = 0
        Dim dtClinicServices As DataTable
        Dim dtClinicHours As DataTable
        dtClinicServices = GetClinicServicesAndHoursByStudent_SP(stuEnrollId, 500, courseId)
        If dtClinicServices.Rows.Count > 0 Then
            For Each row As DataRow In dtClinicServices.Rows
                rtn = row("Completed")
                If rtn > 0 Then Return rtn
            Next
        End If
        dtClinicHours = GetClinicServicesAndHoursByStudent_SP(stuEnrollId, 503, courseId)
        If dtClinicHours.Rows.Count > 0 Then
            For Each row As DataRow In dtClinicHours.Rows
                rtn = row("Completed")
                If rtn > 0 Then Return rtn
            Next
        End If
        Return rtn
    End Function
    Public Function GetClinicServicesAndHoursByStudentWithNoRowDeletionCombination(ByVal stuEnrollId As String, ByVal type As Integer, Optional ByVal courseId As String = "", Optional ByVal courses As String = "All") As DataTable

        '   connect to the database
        Dim db As New DataAccess
        'Dim sbGetCourses As New StringBuilder
        Dim dsGetCourses As DataSet
        Dim strCourses As String = ""


        db.ConnectionString = conString 'AdvAppSettings.GetAppSettings.AppSettings("ConString")

        If courses = "All" Then
            dsGetCourses = (New ClinicHoursDB).GetAllCoursesByEnrollment(stuEnrollId)
            'Dim strCampGrpId As String
            If dsGetCourses.Tables(0).Rows.Count >= 1 Then
                For Each row As DataRow In dsGetCourses.Tables(0).Rows
                    strCourses &= row("ReqId").ToString & "','"
                Next
                strCourses = Mid(strCourses, 1, InStrRev(strCourses, "'") - 2)
            End If
        End If


        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select  ")
            .Append("		GBWD.Seq, ")
            .Append("		GCT.GRDComponentTypeId,  ")
            .Append("		isnull(GBWD.Code,GCT.Code) as Code, ")
            .Append("		GCT.Descrip As ClinicService,  ")
            .Append("		IsNULL(GBWD.Number,0) as Required, ")
            .Append("		1 as Type, ")
            .Append("		(Select Top 1 GrdBkResultId  ")
            .Append("			from arGrdBkResults ")
            .Append("			where StuEnrollId= ? ")
            .Append("			and	InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId) as DBId, ")
            .Append("		(Select  IsNULL(sum(Score),0)  ")
            .Append("			from arGrdBkResults    ")
            .Append("			where StuEnrollId= ? ")
            .Append("			and	InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId) as Completed, ")
            .Append("		(Select IsNULL(g.Number,0) - IsNULL(sum(Score),0) ")
            .Append("       from arGrdBkResults r ")
            .Append("	    INNER JOIN arGrdBkWgtDetails g ON r.InstrGrdBkWgtDetailId=g.InstrGrdBkWgtDetailId ")
            .Append("       where StuEnrollId= ? ")
            .Append("       group by Number) as remaining ")
            .Append("		 ")
            .Append("from	arGrdComponentTypes GCT, arGrdBkWgtDetails GBWD,arGrdBkWeights GBW,arClassSections CS,arResults R  ")
            .Append("where  ")
            .Append("		GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId and GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId	and GBW.ReqId=CS.ReqId and CS.StartDate >= GBW.EffectiveDate  ")
            .Append("and		GBWD.Number >= 1  ")
            ''line modified by saraswathi 'and' commented- april 8th 2009
            '' .Append("and		and CS.clsSectionId = R.TestId  and R.StuEnrollId=? ")
            .Append("and CS.clsSectionId = R.TestId  and R.StuEnrollId=? ")
            'MODIFICATION ENDS HERE
            .Append("       and GBWD.InstrGrdBkWgtId  in ")
            .Append(" (select Distinct InstrGrdBkWgtId from arGrdBkWeights where ")
            If courses = "All" Then
                .Append(" ReqId in ('")
                .Append(strCourses)
                .Append(") ")
            Else
                .Append(" ReqId = '" & courseId & "' ")
            End If
            .Append(" ) ")
            .Append("and		GCT.SysComponentTypeId in (499,501,502,533,544)  ")
            .Append("union all ")
            .Append("select  ")
            .Append("		GBWD.Seq, ")
            .Append("		GCT.GRDComponentTypeId, ")
            .Append("		isnull(GBWD.Code,GCT.Code) as Code, ")
            .Append("		GCT.Descrip,   ")
            .Append("		IsNULL(GBWD.Number,0) as Required, ")
            .Append("		0 as Type, ")
            .Append("		(Select Top 1 ConversionResultId  ")
            .Append("			from arGrdBkConversionResults ")
            .Append("			where StuEnrollId= ? ")
            .Append("			and	GrdComponentTypeId=GCT.GrdComponentTypeId) as DBId, ")
            .Append("		(Select Top 1 IsNULL(Score,0)  ")
            .Append("			from arGrdBkConversionResults ")
            .Append("			where StuEnrollId= ? ")
            .Append("			and	GrdComponentTypeId=GCT.GrdComponentTypeId) as Completed, ")
            .Append("		(IsNULL(GBWD.Number,0) -(Select Top 1 IsNULL(Score,0) from arGrdBkConversionResults where StuEnrollId= ? and	GrdComponentTypeId=GCT.GrdComponentTypeId)) As Remaining  ")
            .Append("		 ")
            .Append("from	arGrdComponentTypes GCT, arGrdBkWgtDetails GBWD,arGrdBkWeights GBW,arClassSections CS,arResults R ")
            .Append("where  ")
            .Append("		GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId  and GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId	and GBW.ReqId=CS.ReqId and CS.StartDate >= GBW.EffectiveDate  ")
            .Append("and		GBWD.Number >= 1  ")
            .Append("and		 CS.clsSectionId = R.TestId  and R.StuEnrollId=? ")
            'MODIFICATION ENDS HERE
            .Append("       and GBWD.InstrGrdBkWgtId  in ")
            .Append(" (select Distinct InstrGrdBkWgtId from arGrdBkWeights where ")
            If courses = "All" Then
                .Append(" ReqId in ('")
                .Append(strCourses)
                .Append(") ")
            Else
                .Append(" ReqId = '" & courseId & "' ")
            End If
            .Append(" ) ")
            .Append("and		GCT.SysComponentTypeId in (499,501,502,533,544)  ")
            .Append("and		exists	(Select * from arGrdBkConversionResults where StuEnrollId= ? and GrdComponentTypeId=GCT.GrdComponentTypeId) ")
            .Append("order by GCT.Descrip,GBWD.Seq, GCT.GRDComponentTypeId, Type ")
        End With

        '   add parameters values to the query
        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return datatable
        'Return DeleteDuplicatedRows(db.RunParamSQLDataSet(sb.ToString).Tables(0))
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function

    Public Function GetReqId(ByVal TermId As String, ByVal ClsSectionId As String) As String

        '   connect to the database
        Dim db As New DataAccess

        db.ConnectionString = conString ' AdvAppSettings.GetAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select Top 1  ")
            .Append("		ReqId from arClassSections where TermId=? ")
            .Append("		and ClsSectionId=?  ")
        End With

        '   add parameters values to the query
        '   StuEnrollId
        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@ClsSectionId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        Return CType(db.RunParamSQLScalar(sb.ToString), Guid).ToString

    End Function
    Public Function GetClinicServicesAndHoursByStudentWithNoRowDeletion(ByVal stuEnrollId As String, ByVal type As Integer, Optional ByVal CourseId As String = "", Optional ByVal Courses As String = "All") As DataTable

        '   connect to the database
        Dim db As New DataAccess
        'Dim sbGetCourses As New StringBuilder
        Dim dsGetCourses As DataSet
        Dim strCourses As String = ""


        db.ConnectionString = conString 'AdvAppSettings.GetAppSettings.AppSettings("ConString")

        If Courses = "All" Then
            dsGetCourses = (New ClinicHoursDB).GetAllCoursesByEnrollment(stuEnrollId)
            'Dim strCampGrpId As String
            If dsGetCourses.Tables(0).Rows.Count >= 1 Then
                For Each row As DataRow In dsGetCourses.Tables(0).Rows
                    strCourses &= row("ReqId").ToString & "','"
                Next
                strCourses = Mid(strCourses, 1, InStrRev(strCourses, "'") - 2)
            End If
        End If


        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select  ")
            .Append("		GBWD.Seq, ")
            .Append("		GCT.GRDComponentTypeId,  ")
            .Append("		isnull(GBWD.Code,GCT.Code) as Code, ")
            .Append("		GCT.Descrip As ClinicService,  ")
            .Append("		IsNULL(GBWD.Number,0) as Required, ")
            .Append("		1 as Type, ")
            .Append("		(Select Top 1 GrdBkResultId  ")
            .Append("			from arGrdBkResults ")
            .Append("			where StuEnrollId= ? ")
            .Append("			and	InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId) as DBId, ")
            .Append("		(Select Top 1 IsNULL(sum(Score),0)  ")
            .Append("			from arGrdBkResults    ")
            .Append("			where StuEnrollId= ? ")
            .Append("			and	InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId) as Completed, ")
            .Append("		(Select Top 1 IsNULL(g.Number,0) - IsNULL(sum(Score),0) ")
            .Append("       from arGrdBkResults r ")
            .Append("	    INNER JOIN arGrdBkWgtDetails g ON r.InstrGrdBkWgtDetailId=g.InstrGrdBkWgtDetailId ")
            .Append("       where StuEnrollId= ? ")
            .Append("       group by Number) as remaining ")
            .Append("from	arGrdComponentTypes GCT, arGrdBkWgtDetails GBWD,arGrdBkWeights GBW,arClassSections CS,arResults R  ")
            .Append("where  ")
            .Append("		GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId and GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId	and GBW.ReqId=CS.ReqId and CS.StartDate >= GBW.EffectiveDate  ")
            .Append("and		GBWD.Number >= 1  ")
            ''line modified by saraswathi 'and' commented- april 8th 2009
            '' .Append("and		and CS.clsSectionId = R.TestId  and R.StuEnrollId=? ")
            .Append("and CS.clsSectionId = R.TestId  and R.StuEnrollId=? ")
            'MODIFICATION ENDS HERE
            .Append("       and GBWD.InstrGrdBkWgtId  in ")
            .Append(" (select Distinct InstrGrdBkWgtId from arGrdBkWeights where ")
            If Courses = "All" Then
                .Append(" ReqId in ('")
                .Append(strCourses)
                .Append(") ")
            Else
                .Append(" ReqId = '" & CourseId & "' ")
            End If
            .Append(" ) ")
            .Append("and		GCT.SysComponentTypeId in (?)  ")
            .Append("union all ")
            .Append("select  ")
            .Append("		GBWD.Seq, ")
            .Append("		GCT.GRDComponentTypeId, ")
            .Append("		isnull(GBWD.Code,GCT.Code) as Code, ")
            .Append("		GCT.Descrip,   ")
            .Append("		IsNULL(GBWD.Number,0) as Required, ")
            .Append("		0 as Type, ")
            .Append("		(Select Top 1 ConversionResultId  ")
            .Append("			from arGrdBkConversionResults ")
            .Append("			where StuEnrollId= ? ")
            .Append("			and	GrdComponentTypeId=GCT.GrdComponentTypeId) as DBId, ")
            .Append("		(Select Top 1 IsNULL(Score,0)  ")
            .Append("			from arGrdBkConversionResults ")
            .Append("			where StuEnrollId= ? ")
            .Append("			and	GrdComponentTypeId=GCT.GrdComponentTypeId) as Completed, ")
            .Append("		(IsNULL(GBWD.Number,0) -(Select Top 1 IsNULL(Score,0) from arGrdBkConversionResults where StuEnrollId= ? and	GrdComponentTypeId=GCT.GrdComponentTypeId)) As Remaining  ")
            .Append("		 ")
            .Append("from	arGrdComponentTypes GCT, arGrdBkWgtDetails GBWD,arGrdBkWeights GBW,arClassSections CS,arResults R ")
            .Append("where  ")
            .Append("		GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId  and GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId	and GBW.ReqId=CS.ReqId and CS.StartDate >= GBW.EffectiveDate  ")
            .Append("and		GBWD.Number >= 1  ")
            .Append("and		 CS.clsSectionId = R.TestId  and R.StuEnrollId=? ")
            'MODIFICATION ENDS HERE
            .Append("       and GBWD.InstrGrdBkWgtId  in ")
            .Append(" (select Distinct InstrGrdBkWgtId from arGrdBkWeights where ")
            If Courses = "All" Then
                .Append(" ReqId in ('")
                .Append(strCourses)
                .Append(") ")
            Else
                .Append(" ReqId = '" & CourseId & "' ")
            End If
            .Append(" ) ")
            .Append("and		GCT.SysComponentTypeId in (?)  ")
            .Append("and		exists	(Select * from arGrdBkConversionResults where StuEnrollId= ? and GrdComponentTypeId=GCT.GrdComponentTypeId) ")
            .Append("order by GCT.Descrip,GBWD.Seq, GCT.GRDComponentTypeId, Type ")
        End With

        '   add parameters values to the query
        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   Type
        db.AddParameter("@Type", type, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   Type
        db.AddParameter("@Type", type, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return datatable
        'Return DeleteDuplicatedRows(db.RunParamSQLDataSet(sb.ToString).Tables(0))
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function
    Public Function GetClinicServicesAndHoursByStudentWithNoRowDeletion_SP(ByVal stuEnrollId As String, ByVal type As Integer, ByVal CourseId As String) As DataTable
        Dim ds As DataSet
        Dim db As New SQLDataAccess


        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString") 'AdvAppSettings.GetAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@stuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@type", type, SqlDbType.Int, , ParameterDirection.Input)
        db.AddParameter("@reqId", New Guid(CourseId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet_SP("USP_GetClinicServicesAndHoursByStudent")
        Try
            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Function GetClinicServicesAndHoursByStudentWithNoRowDeletionByTermId(ByVal stuEnrollId As String, ByVal type As Integer, Optional ByVal CourseId As String = "", Optional ByVal Courses As String = "All", Optional ByVal TermId As String = "") As DataTable

        '   connect to the database
        Dim db As New DataAccess
        'Dim sbGetCourses As New StringBuilder
        Dim dsGetCourses As DataSet
        Dim strCourses As String = ""


        db.ConnectionString = conString ' AdvAppSettings.GetAppSettings.AppSettings("ConString")

        If Courses = "All" Then
            dsGetCourses = (New ClinicHoursDB).GetAllCoursesByEnrollmentByTerm(stuEnrollId, TermId)
            'Dim strCampGrpId As String
            If dsGetCourses.Tables(0).Rows.Count >= 1 Then
                For Each row As DataRow In dsGetCourses.Tables(0).Rows
                    strCourses &= row("ReqId").ToString & "','"
                Next
                strCourses = Mid(strCourses, 1, InStrRev(strCourses, "'") - 2)
            End If
        End If

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select  ")
            .Append("		GBWD.Seq, ")
            .Append("		GCT.GRDComponentTypeId,  ")
            .Append("		isnull(GBWD.Code,GCT.Code) as Code, ")
            .Append("		GCT.Descrip As ClinicService,  ")
            .Append("		IsNULL(GBWD.Number,0) as Required, ")
            .Append("		1 as Type, ")
            .Append("		(Select Top 1 GrdBkResultId  ")
            .Append("			from arGrdBkResults ")
            .Append("			where StuEnrollId= ? ")
            .Append("			and	InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId) as DBId, ")
            .Append("		(Select  IsNULL(sum(Score),0)  ")
            .Append("			from arGrdBkResults    ")
            .Append("			where StuEnrollId= ? ")
            .Append("			and	InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId) as Completed, ")
            .Append("		(Select IsNULL(g.Number,0) - IsNULL(sum(Score),0) ")
            .Append("       from arGrdBkResults r ")
            .Append("	    INNER JOIN arGrdBkWgtDetails g ON r.InstrGrdBkWgtDetailId=g.InstrGrdBkWgtDetailId ")
            .Append("       where StuEnrollId= ? ")
            .Append("       group by Number) as remaining ")
            .Append("		 ")
            .Append("from	arGrdComponentTypes GCT, arGrdBkWgtDetails GBWD,arGrdBkWeights GBW,arClassSections CS,arResults R  ")
            .Append("where  ")
            .Append("		GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId and GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId	and GBW.ReqId=CS.ReqId and CS.StartDate >= GBW.EffectiveDate  ")
            .Append("and		GBWD.Number >= 1  ")
            ''line modified by saraswathi 'and' commented- april 8th 2009
            '' .Append("and		and CS.clsSectionId = R.TestId  and R.StuEnrollId=? ")
            .Append("and CS.clsSectionId = R.TestId  and R.StuEnrollId=? ")
            'MODIFICATION ENDS HERE
            .Append("       and GBWD.InstrGrdBkWgtId  in ")
            .Append(" (select Distinct InstrGrdBkWgtId from arGrdBkWeights where ")
            If Courses = "All" Then
                .Append(" ReqId in ('")
                .Append(strCourses)
                .Append(") ")
            Else
                .Append(" ReqId = '" & CourseId & "' ")
            End If
            .Append(" ) ")
            .Append("and		GCT.SysComponentTypeId in (?)  ")
            .Append("union all ")
            .Append("select  ")
            .Append("		GBWD.Seq, ")
            .Append("		GCT.GRDComponentTypeId, ")
            .Append("		isnull(GBWD.Code,GCT.Code) as Code, ")
            .Append("		GCT.Descrip,   ")
            .Append("		IsNULL(GBWD.Number,0) as Required, ")
            .Append("		0 as Type, ")
            .Append("		(Select Top 1 ConversionResultId  ")
            .Append("			from arGrdBkConversionResults ")
            .Append("			where StuEnrollId= ? ")
            .Append("			and	GrdComponentTypeId=GCT.GrdComponentTypeId) as DBId, ")
            .Append("		(Select Top 1 IsNULL(Score,0)  ")
            .Append("			from arGrdBkConversionResults ")
            .Append("			where StuEnrollId= ? ")
            .Append("			and	GrdComponentTypeId=GCT.GrdComponentTypeId) as Completed, ")
            .Append("		(IsNULL(GBWD.Number,0) -(Select Top 1 IsNULL(Score,0) from arGrdBkConversionResults where StuEnrollId= ? and	GrdComponentTypeId=GCT.GrdComponentTypeId)) As Remaining  ")
            .Append("		 ")
            .Append("from	arGrdComponentTypes GCT, arGrdBkWgtDetails GBWD,arGrdBkWeights GBW,arClassSections CS,arResults R ")
            .Append("where  ")
            .Append("		GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId  and GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId	and GBW.ReqId=CS.ReqId and CS.StartDate >= GBW.EffectiveDate  ")
            .Append("and		GBWD.Number >= 1  ")
            .Append("and		 CS.clsSectionId = R.TestId  and R.StuEnrollId=? ")
            'MODIFICATION ENDS HERE
            .Append("       and GBWD.InstrGrdBkWgtId  in ")
            .Append(" (select Distinct InstrGrdBkWgtId from arGrdBkWeights where ")
            If Courses = "All" Then
                .Append(" ReqId in ('")
                .Append(strCourses)
                .Append(") ")
            Else
                .Append(" ReqId = '" & CourseId & "' ")
            End If
            .Append(" ) ")
            .Append("and		GCT.SysComponentTypeId in (?)  ")
            .Append("and		exists	(Select * from arGrdBkConversionResults where StuEnrollId= ? and GrdComponentTypeId=GCT.GrdComponentTypeId) ")
            .Append("order by GCT.Descrip,GBWD.Seq, GCT.GRDComponentTypeId, Type ")
        End With

        '   add parameters values to the query
        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   Type
        db.AddParameter("@Type", type, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   Type
        db.AddParameter("@Type", type, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return datatable
        'Return DeleteDuplicatedRows(db.RunParamSQLDataSet(sb.ToString).Tables(0))
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function
    Public Function GetClinicServicesAndHoursByStudent(ByVal stuEnrollId As String, ByVal type As Integer, Optional ByVal CourseId As String = "", Optional ByVal Courses As String = "All") As DataTable

        '   connect to the database
        Dim db As New DataAccess
        Dim dsGetCourses As DataSet
        Dim strCourses As String = ""


        db.ConnectionString = conString '  AdvAppSettings.GetAppSettings.AppSettings("ConString")

        If Courses = "All" Then
            dsGetCourses = (New ClinicHoursDB).GetAllCoursesByEnrollment(stuEnrollId)
            'Dim strCampGrpId As String
            If dsGetCourses.Tables(0).Rows.Count >= 1 Then
                For Each row As DataRow In dsGetCourses.Tables(0).Rows
                    strCourses &= row("ReqId").ToString & "','"
                Next
                strCourses = Mid(strCourses, 1, InStrRev(strCourses, "'") - 2)
            End If
        End If


        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select  ")
            .Append("		GBWD.Seq, ")
            .Append("		GCT.GRDComponentTypeId,  ")
            .Append("		isnull(GBWD.Code,GCT.Code) as Code, ")
            .Append("		GCT.Descrip As ClinicService,  ")
            .Append("		IsNULL(GBWD.Number,0) as Required, ")
            .Append("		1 as Type, ")
            .Append("		(Select Top 1 GrdBkResultId  ")
            .Append("			from arGrdBkResults ")
            .Append("			where StuEnrollId= ? ")
            .Append("			and	InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId) as DBId, ")
            .Append("		(Select  IsNULL(sum(Score),0)  ")
            .Append("			from arGrdBkResults    ")
            .Append("			where StuEnrollId= ? ")
            .Append("			and	InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId) as Completed, ")
            .Append("		(Select IsNULL(GBWD.Number,0) - IsNULL(sum(Score),0)  ")
            .Append("			from arGrdBkResults    ")
            .Append("			where StuEnrollId= ? ")
            'commented by Theresa G on Nov 2nd 2010 for DE 1187 Coyne - Getting error page while exporting single student progress report.
            ' .Append("			and	InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId group by GBWD.Number) as Remaining ")
            .Append("			and	InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId) as Remaining ")
            .Append("		 ")
            .Append("from	arGrdComponentTypes GCT, arGrdBkWgtDetails GBWD,arGrdBkWeights GBW,arClassSections CS,arResults R  ")
            .Append("where  ")
            .Append("		GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId and GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId	and GBW.ReqId=CS.ReqId and CS.StartDate >= GBW.EffectiveDate  ")
            .Append("and		GBWD.Number >= 1  ")
            ''line modified by saraswathi 'and' commented- april 8th 2009
            '' .Append("and		and CS.clsSectionId = R.TestId  and R.StuEnrollId=? ")
            .Append("and CS.clsSectionId = R.TestId  and R.StuEnrollId=? ")
            'MODIFICATION ENDS HERE
            .Append("       and GBWD.InstrGrdBkWgtId  in ")
            .Append(" (select Distinct InstrGrdBkWgtId from arGrdBkWeights where ")
            If Courses = "All" Then
                .Append(" ReqId in ('")
                .Append(strCourses)
                .Append(") ")
            Else
                .Append(" ReqId = '" & CourseId & "' ")
            End If
            .Append(" ) ")
            .Append("and		GCT.SysComponentTypeId in (?)  ")
            .Append("union all ")
            .Append("select  ")
            .Append("		GBWD.Seq, ")
            .Append("		GCT.GRDComponentTypeId, ")
            .Append("		isnull(GBWD.Code,GCT.Code) as Code, ")
            .Append("		GCT.Descrip,   ")
            .Append("		IsNULL(GBWD.Number,0) as Required, ")
            .Append("		0 as Type, ")
            .Append("		(Select Top 1 ConversionResultId  ")
            .Append("			from arGrdBkConversionResults ")
            .Append("			where StuEnrollId= ? ")
            .Append("			and	GrdComponentTypeId=GCT.GrdComponentTypeId) as DBId, ")
            .Append("		(Select Top 1 IsNULL(Score,0)  ")
            .Append("			from arGrdBkConversionResults ")
            .Append("			where StuEnrollId= ? ")
            .Append("			and	GrdComponentTypeId=GCT.GrdComponentTypeId) as Completed, ")
            .Append("		(IsNULL(GBWD.Number,0) -(Select Top 1 IsNULL(Score,0) from arGrdBkConversionResults where StuEnrollId= ? and	GrdComponentTypeId=GCT.GrdComponentTypeId)) As Remaining  ")
            .Append("		 ")
            .Append("from	arGrdComponentTypes GCT, arGrdBkWgtDetails GBWD,arGrdBkWeights GBW,arClassSections CS,arResults R ")
            .Append("where  ")
            .Append("		GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId  and GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId	and GBW.ReqId=CS.ReqId and CS.StartDate >= GBW.EffectiveDate  ")
            .Append("and		GBWD.Number >= 1  ")
            .Append("and		 CS.clsSectionId = R.TestId  and R.StuEnrollId=? ")
            'MODIFICATION ENDS HERE
            .Append("       and GBWD.InstrGrdBkWgtId  in ")
            .Append(" (select Distinct InstrGrdBkWgtId from arGrdBkWeights where ")
            If Courses = "All" Then
                .Append(" ReqId in ('")
                .Append(strCourses)
                .Append(") ")
            Else
                .Append(" ReqId = '" & CourseId & "' ")
            End If
            .Append(" ) ")
            .Append("and		GCT.SysComponentTypeId in (?)  ")
            .Append("and		exists	(Select * from arGrdBkConversionResults where StuEnrollId= ? and GrdComponentTypeId=GCT.GrdComponentTypeId) ")
            .Append("order by GCT.Descrip,GBWD.Seq, GCT.GRDComponentTypeId, Type ")
        End With

        '   add parameters values to the query
        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   Type
        db.AddParameter("@Type", type, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   Type
        db.AddParameter("@Type", type, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return datatable
        Return DeleteDuplicatedRows(db.RunParamSQLDataSet(sb.ToString).Tables(0))

    End Function
    Public Function GetClinicServicesAndHoursByStudent_SP(ByVal stuEnrollId As String, ByVal type As Integer, ByVal CourseId As String) As DataTable

        Dim ds As DataSet
        Dim db As New SQLDataAccess


        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString") ' AdvAppSettings.GetAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@stuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@type", type, SqlDbType.Int, , ParameterDirection.Input)
        db.AddParameter("@reqId", New Guid(CourseId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet_SP("dbo.USP_GetClinicServicesAndHoursByStudent")
        Try
            Return DeleteDuplicatedRows(ds.Tables(0))
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try





    End Function
    Private Function DeleteDuplicatedRows(ByVal dt As DataTable) As DataTable
        If dt.Rows.Count = 0 Then Return dt
        Dim previous As Guid = Guid.Empty
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i)("GRDComponentTypeId") = previous Or dt.Rows(i)("DBId") Is DBNull.Value Then
                dt.Rows(i).Delete()
            Else
                previous = dt.Rows(i)("GRDComponentTypeId")
            End If
        Next
        dt.AcceptChanges()
        For i As Integer = 0 To dt.Rows.Count - 1

            If dt.Rows(i)("Remaining") < 0 Then dt.Rows(i)("Remaining") = 0
        Next
        Return dt
    End Function
    Public Function isClinicCourseCompletlySatisfied(ByVal stuEnrollId As String, ByVal type As Integer, Optional ByVal CourseId As String = "") As Boolean
        Dim dt As DataTable = GetClinicServicesAndHoursByStudentWithNoRowDeletion(stuEnrollId, type, CourseId, "")
        If dt.Rows.Count = 0 Then Return True
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i)("DBID") Is DBNull.Value Or dt.Rows(i)("Remaining") Is DBNull.Value Then Return False
            If Not dt.Rows(i)("Remaining") Is DBNull.Value Then
                If dt.Rows(i)("Remaining") > 0 Then Return False
            End If
        Next
        Return True
    End Function
    Public Function isClinicCourseCompletlySatisfied_SP(ByVal stuEnrollId As String, ByVal type As Integer, ByVal CourseId As String) As Boolean
        Dim dt As DataTable = GetClinicServicesAndHoursByStudentWithNoRowDeletion_SP(stuEnrollId, type, CourseId)
        If dt.Rows.Count = 0 Then Return True
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i)("DBID") Is DBNull.Value Or dt.Rows(i)("Remaining") Is DBNull.Value Then Return False
            If Not dt.Rows(i)("Remaining") Is DBNull.Value Then
                If dt.Rows(i)("Remaining") > 0 Then Return False
            End If
        Next
        Return True
    End Function
    Public Function isClinicCourseCompletlySatisfiedByTerm(ByVal stuEnrollId As String, ByVal type As Integer, Optional ByVal CourseId As String = "", Optional ByVal Courses As String = "All", Optional ByVal TermId As String = "") As Boolean
        Dim dt As DataTable = GetClinicServicesAndHoursByStudentWithNoRowDeletionByTermId(stuEnrollId, type, "", "All", TermId)
        If dt.Rows.Count = 0 Then Return True
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i)("DBID") Is DBNull.Value Or dt.Rows(i)("Remaining") Is DBNull.Value Then Return False
            If Not dt.Rows(i)("Remaining") Is DBNull.Value Then
                If dt.Rows(i)("Remaining") > 0 Then Return False
            End If
        Next
        Return True
    End Function
    Public Function isClinicCourseCompletlySatisfiedForCombination(ByVal stuEnrollId As String, ByVal type As Integer, Optional ByVal CourseId As String = "") As Boolean
        Dim dt As DataTable = GetClinicServicesAndHoursByStudentWithNoRowDeletionCombination(stuEnrollId, type, CourseId, "")
        If dt.Rows.Count = 0 Then Return True
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i)("DBID") Is DBNull.Value Or dt.Rows(i)("Remaining") Is DBNull.Value Then Return False
            If Not dt.Rows(i)("Remaining") Is DBNull.Value Then
                If dt.Rows(i)("Remaining") > 0 Then Return False
            End If
        Next
        Return True
    End Function
    Public Function IsRemainingLabCount(ByVal stuEnrollId As String, ByVal type As Integer, Optional ByVal CourseId As String = "") As Boolean
        Dim dt As DataTable = GetClinicServicesAndHoursByStudent(stuEnrollId, type, CourseId, "")
        If dt.Rows.Count = 0 Then Return True
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i)("Remaining") > 0 Then Return True
        Next
        Return False
    End Function
    Public Function IsRemainingLabCount_SP(ByVal stuEnrollId As String, ByVal type As Integer, ByVal CourseId As String) As Boolean
        Dim dt As DataTable = GetClinicServicesAndHoursByStudent_SP(stuEnrollId, type, CourseId)
        If dt.Rows.Count = 0 Then Return True
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i)("Remaining") > 0 Then Return True
        Next
        Return False
    End Function

    Public Function GetDataToPostExamsResultsByStudent(ByVal stuEnrollId As String, ByVal type As Integer) As DataTable

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = conString ' AdvAppSettings.GetAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select ROW_NUMBER()    OVER ( order by P.GrpDescrip, P.Subject, P.Seq, P.GRDComponentTypeId, P.Type,")
            .Append(" CASE WHEN P.Postdate Is NULL Then 1 Else 0 End, P.PostDate) AS Row,P.*   from  ")
            .Append(" (select    ")
            .Append("		GBWD.Seq,   ")
            .Append("       GBR.StuEnrollId, ")
            .Append("		GBR.ClsSectionId,  ")
            .Append("		GBWD.InstrGrdBkWgtDetailId,  ")
            .Append("       (Select TermId from arClassSections where ClsSectionId=GBR.ClsSectionId) As TermId, ")
            .Append("		(Select TermDescrip from arTerm where TermId=(Select TermId from arClassSections where ClsSectionId=GBR.ClsSectionId))As GrpDescrip,   ")
            .Append("		(Select StartDate from arTerm where TermId=(Select TermId from arClassSections where ClsSectionId=GBR.ClsSectionId))As StartDate,   ")
            .Append("		(Select EndDate from arTerm where TermId=(Select TermId from arClassSections where ClsSectionId=GBR.ClsSectionId))As EndDate,   ")
            .Append("		(Select Descrip from arReqs where ReqId=(select ReqId from arClassSections where ClsSectionId=GBR.ClsSectionId))As Subject,   ")
            .Append("		GCT.GRDComponentTypeId, ")
            .Append("		isnull(GBWD.Code,GCT.Code) as Code, ")
            .Append("		LTrim(RTRIM(GCT.Descrip)) As Name, ")
            .Append("		1 as Type, ")
            .Append("		GBR.GrdBkResultId as DBId,  ")
            .Append("		GBR.Score,  ")
            .Append("       (select min(MinVal) from arGradeScaleDetails where GrdScaleId=(Select GrdScaleId from arClassSections  where ClsSectionId=GBR.ClsSectionId)) as MinScore, ")
            .Append("  		(select max(MaxVal) from arGradeScaleDetails where GrdScaleId=(Select GrdScaleId from arClassSections  where ClsSectionId=GBR.ClsSectionId)) as MaxScore, ")
            .Append("		GBR.PostDate,GBWD.Number   ")
            .Append("from	arGrdBkResults GBR, arGrdComponentTypes GCT, arGrdBkWgtDetails GBWD    ")
            .Append("where  ")
            .Append("		GBR.StuEnrollId=? ")
            .Append("and	GBR.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
            .Append("and	GBWD.GrdComponentTypeId = GCT.GrdComponentTypeId    ")
            .Append("and	GCT.SysComponentTypeId in (?)    ")
            .Append("union all   ")
            .Append("select    ")
            .Append("		GBWD.Seq,  ")
            .Append("       GBCR.StuEnrollId, ")
            .Append("		null As ClsSectionId,  ")
            .Append("		GBWD.InstrGrdBkWgtDetailId,  ")
            .Append("       GBCR.TermId, ")
            .Append("       (Select TermDescrip from arTerm where TermId=GBCR.TermId) as GrpDescrip,  ")
            .Append("       (Select StartDate from arTerm where TermId=GBCR.TermId) as StartDate,  ")
            .Append("       (Select EndDate from arTerm where TermId=GBCR.TermId) as EndDate,  ")
            .Append("       (Select Descrip from arReqs where ReqId=GBCR.ReqId) as Subject,  ")
            .Append("		GCT.GRDComponentTypeId,   ")
            .Append("		isnull(GBWD.Code,GCT.Code) as Code,   ")
            .Append("		GCT.Descrip, ")
            .Append("		0 as Type,   ")
            .Append("		GBCR.ConversionResultId as DBId,  ")
            .Append("		GBCR.Score,  ")
            .Append("       0 as MinScore, ")
            .Append("       100 as MaxScore, ")
            .Append("		GBCR.PostDate,GBWD.Number 		   ")
            .Append("from	arGrdBkConversionResults GBCR, arGrdComponentTypes GCT, arGrdBkWgtDetails GBWD    ")
            .Append("where   ")
            .Append("		GBCR.StuEnrollId=? ")
            .Append("and	GBCR.GrdComponentTypeId=GCT.GrdComponentTypeId ")
            .Append("and	GCT.GrdComponentTypeId=GBWD.GrdComponentTypeId ")
            .Append("and	GCT.SysComponentTypeId in (?) )P    ")
            '.Append("order by GrpDescrip, Subject, GBWD.Seq, GCT.GRDComponentTypeId, Type,PostDate DESC  ")
            .Append(" order by P.GrpDescrip, P.Subject, P.Seq, P.GRDComponentTypeId, P.Type,CASE WHEN P.Postdate Is NULL Then 1 Else 0 End, P.PostDate ")
        End With

        '   add parameters values to the query
        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   Type
        db.AddParameter("@Type", type, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   Type
        db.AddParameter("@Type", type, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   return datatable
        'Return DeleteDuplicatedRows1(db.RunParamSQLDataSet(sb.ToString).Tables(0))
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function

    'Private Function DeleteDuplicatedRows1(ByVal dt As DataTable) As DataTable
    '    If dt.Rows.Count = 0 Then Return dt
    '    Dim previous As Guid = Guid.Empty
    '    Dim previousclssection As Guid = Guid.Empty
    '    For i As Integer = 0 To dt.Rows.Count - 1
    '        If dt.Rows(i)("clsSectionId") Is DBNull.Value Then
    '            If (dt.Rows(i)("GRDComponentTypeId") = previous Or dt.Rows(i)("GrpDescrip") Is DBNull.Value) Then
    '                dt.Rows(i).Delete()
    '            Else
    '                previous = dt.Rows(i)("GRDComponentTypeId")
    '            End If
    '        Else
    '            If (dt.Rows(i)("GRDComponentTypeId") = previous And dt.Rows(i)("clsSectionId") = previousclssection) Or dt.Rows(i)("GrpDescrip") Is DBNull.Value Then
    '                dt.Rows(i).Delete()
    '            Else
    '                previous = dt.Rows(i)("GRDComponentTypeId")
    '                previousclssection = dt.Rows(i)("clsSectionId")
    '            End If
    '        End If

    '    Next
    '    dt.AcceptChanges()
    '    Return dt
    'End Function




    Public Function PostExamResultsObj(ByVal postExamListFinal As List(Of postExamResultsObject), ByVal stuEnrollId As String, ByVal user As String) As String
        Dim sb As StringBuilder = New StringBuilder
        Dim sbi As New StringBuilder
        Dim sbConv As StringBuilder = New StringBuilder
        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = conString ' AdvAppSettings.GetAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try


            If postExamListFinal.Count() > 0 Then
                Dim now As Date = Date.Now
                With sb
                    .Append("update arGrdBkResults ")
                    .Append("       set Score = ?, ModUser = ?, ModDate = ?, PostDate = ?,Comments = ?, ResNum = ?, IsCompGraded = ?, DateCompleted = ? ")
                    .Append("where GrdBkResultId = ? ")
                End With

                With sbi
                    .Append("insert into arGrdBkResults (GrdBkResultId, ClsSectionId, InstrGrdBkWgtDetailId, Score, StuEnrollId, ModUser, ModDate, PostDate,Comments,resnum,DateCompleted) ")
                    .Append("values (newId(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ")
                End With

                With sbConv
                    .Append("update arGrdBkConversionResults ")
                    .Append("       set Score = ?, ModUser = ?, ModDate = ?, PostDate = ? ")
                    .Append("where ConversionResultId = ? ")
                End With

                For Each post In postExamListFinal
                    db.ClearParameters()

                    If post.dbIdName = "GrdBkResultId" And Not post.dbId = "" Then

                        If Not (post.deleteScore) Then
                            db.AddParameter("@Score", post.score, DataAccess.OleDbDataType.OleDbString)
                        Else
                            db.AddParameter("@Score", DBNull.Value, DataAccess.OleDbDataType.OleDbString)
                        End If
                        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString)
                        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime)
                        If Not (post.deleteDate) Then
                            db.AddParameter("@PostDate", post.postDate, DataAccess.OleDbDataType.OleDbDateTime)
                        Else
                            db.AddParameter("@PostDate", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime)
                        End If
                        db.AddParameter("@Comments", post.componentDescription, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@resNum", post.resnum, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@isCompGraded", 1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        If Not (post.deleteDate) Then
                            db.AddParameter("@DateCompleted", post.postDate, DataAccess.OleDbDataType.OleDbDateTime)
                        Else
                            db.AddParameter("@DateCompleted", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime)
                        End If
                        db.AddParameter("@GrdBkResultId", post.dbId, DataAccess.OleDbDataType.OleDbString)
                        db.RunParamSQLExecuteNoneQuery(sb.ToString(), groupTrans)
                    ElseIf post.dbIdName = "ResultId" And Not post.postDate Is Nothing Then
                        db.AddParameter("@ClsSectionId", post.clssectionId, DataAccess.OleDbDataType.OleDbString)
                        db.AddParameter("@InstrGrdBkWgtDetailId", post.instrGrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString)
                        If Not (post.deleteScore) Then
                            db.AddParameter("@Score", post.score, DataAccess.OleDbDataType.OleDbString)
                        Else
                            db.AddParameter("@Score", DBNull.Value, DataAccess.OleDbDataType.OleDbString)
                        End If
                        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString)
                        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString)
                        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime)
                        If Not (post.deleteDate) Then
                            db.AddParameter("@PostDate", post.postDate, DataAccess.OleDbDataType.OleDbDateTime)
                        Else
                            db.AddParameter("@PostDate", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime)
                        End If
                        db.AddParameter("@Comments", post.componentDescription, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@resNum", post.resnum, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        If Not (post.deleteDate) Then
                            db.AddParameter("@DateCompleted", post.postDate, DataAccess.OleDbDataType.OleDbDateTime)
                        Else
                            db.AddParameter("@DateCompleted", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime)
                        End If
                        db.RunParamSQLExecuteNoneQuery(sbi.ToString(), groupTrans)



                    End If

                    If post.dbIdName = "ConversionResultId" Then

                        db.ClearParameters()

                        If Not (post.deleteScore) Then
                            db.AddParameter("@Score", post.score, DataAccess.OleDbDataType.OleDbString)
                        Else
                            db.AddParameter("@Score", DBNull.Value, DataAccess.OleDbDataType.OleDbString)
                        End If
                        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString)
                        db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime)
                        If Not (post.deleteDate) Then
                            db.AddParameter("@PostDate", post.postDate, DataAccess.OleDbDataType.OleDbDateTime)
                        Else
                            db.AddParameter("@PostDate", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime)
                        End If
                        db.AddParameter("@ConversionResultId", post.dbId, DataAccess.OleDbDataType.OleDbString)
                        db.RunParamSQLExecuteNoneQuery(sbConv.ToString(), groupTrans)

                    End If

                Next

            End If

            'commit transaction
            groupTrans.Commit()

        Catch ex As OleDbException
            groupTrans.Rollback()
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            End If
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            db.CloseConnection()
        End Try

    End Function

    Public Function GetOverallAverageFromTransferGrades(ByVal StuEnrollId As String) As DataTable
        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = conString ' AdvAppSettings.GetAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select sum(Score) as SumOfScores,count(Score) as NumCourses ")
            .Append("from arTransferGrades tg ")
            .Append("where StuEnrollId=? ")
            .Append("and Score is not null ")
        End With

        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return datatable
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function

    Public Function GetOverallAverageFromARResults(ByVal StuEnrollId As String) As DataTable
        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = conString ' AdvAppSettings.GetAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select sum(Score) as SumOfScores,count(Score) as NumCourses ")
            .Append("from arResults rs ")
            .Append("where StuEnrollId=? ")
            .Append("and Score is not null ")
        End With

        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return datatable
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function
    Public Sub UpdateCourseCredited(ByVal tblname As String, ByVal PKValue As String)
        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = conString ' AdvAppSettings.GetAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sbReq As New StringBuilder
        With sbReq
            If tblname = "argrdbkresults" Then
                .Append("Update arGrdBkResults set IsCourseCredited=1 where GrdBkResultId=? ")
            Else
                .Append("Update arGrdBkConversionResults set IsCourseCredited=1 where ConversionResultId=? ")
            End If
        End With
        db.AddParameter("@GrdBkResultId", PKValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(sbReq.ToString)
        Catch ex As Exception
        Finally
            db.CloseConnection()
            db.ClearParameters()
            sbReq.Remove(0, sbReq.Length)
        End Try
    End Sub

End Class
