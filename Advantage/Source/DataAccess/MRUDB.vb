
Imports System.Web
Imports System.Data
Imports System.Xml
Imports FAME.Advantage.Common

Public Class MRUDB

#Region "Private Variables and Objects"
    Private m_Context As HttpContext
    Private m_UserName As String
    Private m_DataSet As DataSet
#End Region

#Region "Public Constructors"
    Public Sub New()
        MyBase.New()
        m_Context = HttpContext.Current
        m_UserName = m_Context.Session("UserName")
    End Sub
#End Region

#Region "Private Methods"
    Private Function GetMRUSQL(ByVal strType As String) As StringBuilder
        Dim sb As New System.Text.StringBuilder

        Select Case strType
            Case "Students"
                With sb
                    .Append("SELECT t1.MRUId,t1.Counter,t1.ChildId,t2.FirstName + ' ' + t2.LastName As ChildName ")
                    .Append("FROM syMRUS t1,arStudent t2 ")
                    .Append("WHERE t1.ChildId = t2.StudentId ")
                    .Append("AND t1.MRUTypeId = 1 ")
                    .Append("AND t1.UserId = ? ")
                    .Append("AND t1.CampusId = ? ")
                    .Append("ORDER BY ChildName")
                End With

            Case "Employers"
                With sb
                    .Append("SELECT t1.MRUId,t1.Counter,t1.ChildId,t2.EmployerDescrip as ChildName ")
                    .Append("FROM syMRUS t1,plEmployers t2 ")
                    .Append("WHERE t1.ChildId = t2.EmployerId ")
                    .Append("AND t1.MRUTypeId = 2 ")
                    .Append("AND t1.UserId = ? ")
                    .Append("AND t1.CampusId = ? ")
                    .Append("ORDER BY ChildName")
                End With

            Case "Employees"
                With sb
                    .Append("SELECT t1.MRUId,t1.Counter,t1.ChildId,t2.FirstName + ' ' + t2.LastName As ChildName ")
                    .Append("FROM syMRUS t1,hrEmployees t2 ")
                    .Append("WHERE t1.ChildId = t2.EmpId ")
                    .Append("AND t1.MRUTypeId = 3 ")
                    .Append("AND t1.UserId = ? ")
                    .Append("AND t1.CampusId = ? ")
                    .Append("ORDER BY ChildName")
                End With
            Case "Leads"
                With sb
                    .Append("SELECT t1.MRUId,t1.Counter,t1.ChildId,t2.FirstName + ' ' + t2.LastName As ChildName ")
                    .Append("FROM syMRUS t1,adLeads t2 ")
                    .Append("WHERE t1.ChildId = t2.LeadId ")
                    .Append("AND t1.MRUTypeId = 4 ")
                    .Append("AND t1.UserId = ? ")
                    .Append("AND t1.CampusId = ? ")
                    .Append("ORDER BY ChildName")
                End With
        End Select

        Return sb
    End Function

    Private Function GetMRUSQLForStudentGroup(ByVal studentGrpId As String) As StringBuilder
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append("SELECT DISTINCT t1.MRUId,t1.Counter,t1.ChildId,t2.FirstName + ' ' + t2.LastName As ChildName ")
            .Append("FROM syMRUS t1,arStudent t2, arStuEnrollments t3, adLeadByLeadGroups t4 ")
            .Append("WHERE t1.ChildId = t2.StudentId ")
            .Append("AND t1.MRUTypeId = 1 ")
            .Append("AND t1.UserId = ? ")
            .Append("AND t1.CampusId = ? ")
            .Append("AND t2.StudentId=t3.StudentId ")
            .Append("AND t3.StuEnrollId=t4.StuEnrollId ")
            .Append("AND t4.LeadGrpId= ? ")
            .Append("ORDER BY ChildName")
        End With

        Return sb

    End Function

    Private Function GetMRUTypeId(ByVal strType As String) As Integer
        Select Case strType
            Case "Students"
                Return 1
            Case "Employers"
                Return 2
            Case "Employees"
                Return 3
            Case "Leads"
                Return 4
        End Select
    End Function
#End Region

#Region "Public Methods"
    Public Function LoadMRU(ByVal strType As String, ByVal userId As String, Optional ByVal campusId As String = Nothing, Optional ByVal studentGrpId As String = "", Optional ByVal restrictSearchByStudentGroup As String = "False") As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sb As StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If studentGrpId = "" Then
            sb = GetMRUSQL(strType)

        Else
            'When we have a studentGrpId and RestrictSearchByStudentGroup is set to true in the Web.Config file it means that we want to load the MRU with the students
            'from that group so we need to first delete all the items from the MRU.
            If restrictSearchByStudentGroup = "True" Then
                DeleteMRUStudents(userId, campusId)

                'We then need to add each student who is a part of the group to the database for the MRU
                AddMRUStudentsForGroup(userId, campusId, studentGrpId)
            End If

            'Now we can get the SQL statement to be executed
            sb = GetMRUSQLForStudentGroup(studentGrpId)

        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@CmpId", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        If studentGrpId <> "" Then
            db.AddParameter("@SGrpId", studentGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        ds = db.RunParamSQLDataSet(sb.ToString, "MRUList")

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Sub DeleteLRUItem(ByVal strType As String, ByVal userId As String, ByVal campusId As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim mruId As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        mruId = GetMRUTypeId(strType)
        With sb
            .Append("DELETE FROM syMRUS ")
            .Append("WHERE Counter = (SELECT MIN(Counter) FROM syMRUS WHERE MRUTypeId = ? AND UserId = ? AND CampusId = ?)")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@mruid", mruId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@user", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
    End Sub

    Public Sub DeleteMRUStudents(ByVal userId As String, ByVal campusId As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("DELETE FROM syMRUS ")
            .Append("WHERE MRUTypeId = 1 ")
            .Append("AND UserId = ? ")
            .Append("AND CampusId = ? ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@user", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
    End Sub

    Public Sub AddMRUItem(ByVal strtype As String, ByVal strId As String, ByVal userId As String, ByVal campusId As String)
        Dim db As New DataAccess
        Dim sb As New Text.StringBuilder
        Dim mruId As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        mruId = GetMRUTypeId(strtype)

        With sb
            .Append("INSERT INTO syMRUS (ChildId,MRUTypeId,UserId,CampusId) VALUES(?,?,?,?)")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@childid", strId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@mruid", mruId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@username", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
    End Sub

    Public Sub AddMRUStudentsForGroup(ByVal userId As String, ByVal campusid As String, ByVal studentGrpId As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim dr As OleDbDataReader
        Dim studentId As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("select distinct se.StudentId ")
            .Append("from adLeadByLeadGroups lbl, arStuEnrollments se ")
            .Append("where lbl.StuEnrollId=se.StuEnrollId ")
            .Append("and lbl.LeadGrpId=? ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@SGRPID", studentGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        dr = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read
            studentId = XmlConvert.ToString(dr("StudentId"))
            AddMRUItem("Students", studentId, userId, campusid)
        End While
        dr.Close()

    End Sub

    Public Function DoesResourceHasMRU(ByVal resourceID As Integer) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim numType As Integer

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        With sb
            .Append("SELECT COUNT(MRUTypeId) As MRUCount ")
            .Append("FROM syResources ")
            .Append("WHERE ResourceId = ?")
        End With
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()
        db.AddParameter("@resid", resourceID, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
        numType = CInt(db.RunParamSQLScalar(sb.ToString))

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        If numType = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function GetResourceMRUTypeId(ByVal resourceID As Integer) As Integer
        Dim db As New DataAccess
        'Dim ds As DataSet
        Dim sb As New StringBuilder
        Dim numType As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT MRUTypeId As MRUCount ")
            .Append("FROM syResources ")
            .Append("WHERE ResourceId = ?")
        End With
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()
        db.AddParameter("@resid", resourceID, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
        numType = CInt(db.RunParamSQLScalar(sb.ToString))

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return numType
    End Function
    ' US3054 4/17/2012 Janet Robinson switch MRU return to match left MRU bar for Students
    Public Function LoadMRUForStudentSearch(ByVal userId As String, ByVal campusId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()
        db.AddParameter("@UserId", userId, SqlDbType.VarChar, 50, ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, SqlDbType.VarChar, 50, ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("dbo.usp_GetStudentMRUForStudentSearch", "MRUList")
            ' ds.Tables(0).TableName = "MRUList"
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try

        Return ds

    End Function
#End Region

    Public Function GetShadowLead(studentGuidStr As String) As Guid
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim conn = myAdvAppSettings.AppSettings("ConnectionString")
        Dim connection = New SqlConnection(conn)
        Dim sb = "SELECT LeadId FROM AdLeads WHERE StudentId = @StudentId"
        Dim comm = New SqlCommand(sb, connection)
        comm.Parameters.AddWithValue("@StudentId", studentGuidStr)
        connection.Open()
        Try
            Dim result = CType(comm.ExecuteScalar(), Guid)
            Return result
        Finally
            connection.Close()
        End Try
    End Function
End Class
