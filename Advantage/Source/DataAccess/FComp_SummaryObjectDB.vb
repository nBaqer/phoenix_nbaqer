Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class FComp_SummaryObjectDB

    'Public Shared Function GetReportDatasetRaw(ByVal RptParamInfo As ReportParamInfoIPEDS) As DataSet

    '    ' get list of students to include, based on report parameters
    '    Dim StudentList As String = IPEDSDB.GetStudentList(RptParamInfo)

    '    ' if there are no students to include, return empty dataset
    '    If StudentList = "" Then
    '        Return New DataSet
    '    End If

    '    Dim sb As New System.Text.StringBuilder
    '    Dim dsRaw As New DataSet

    '    With sb
    '        ' get Students
    '        .Append("SELECT arStudent.StudentId, ")

    '        ' retrieve all other needed columns
    '        .Append("arPrograms.ProgId, arPrograms.ProgDescrip, ")
    '        .Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
    '        .Append("	FROM adEthCodes, ")
    '        .Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
    '        .Append("	WHERE adEthCodes.EthCodeId = syRptAgencySchoolMapping.SchoolDescripId AND ")
    '        .Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
    '        .Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
    '        .Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
    '        .Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
    '        .Append("	      adEthCodes.EthCodeId = arStudent.Race) AS EthCodeDescrip, ")
    '        .Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
    '        .Append("	FROM adGenders, ")
    '        .Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
    '        .Append("	WHERE adGenders.GenderId = syRptAgencySchoolMapping.SchoolDescripId AND ")
    '        .Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
    '        .Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
    '        .Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
    '        .Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
    '        .Append("	      adGenders.GenderId = arStudent.Gender) AS GenderDescrip ")
    '        .Append("FROM ")
    '        .Append("arStudent, arStuEnrollments, arPrgVersions, arPrograms, ")
    '        .Append("syStatusCodes, sySysStatus ")

    '        ' establish necessary relationships
    '        .Append("WHERE ")
    '        .Append("arStudent.StudentId = arStuEnrollments.StudentId AND ")
    '        .Append("arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId AND ")
    '        .Append("arPrgVersions.ProgId = arPrograms.ProgId AND ")
    '        .Append("arStuEnrollments.StatusCodeId = syStatusCodes.StatusCodeId AND ")
    '        .Append("syStatusCodes.SysStatusId = sySysStatus.SysStatusId AND ")

    '        ' only get those students with completed enrollments, within report date range
    '        .Append("sySysStatus.SysStatusDescrip LIKE 'Graduated' AND ")
    '        .Append("arStuEnrollments.ExpGradDate BETWEEN '" & FmtRptDateParam(RptParamInfo.CohortStartDate) & "' AND '" & FmtRptDateParam(RptParamInfo.CohortEndDate) & "' AND ")

    '        ' append list of appropriate students to include
    '        .Append("arStudent.StudentId IN (" & StudentList & ") ")

    '        ' apply sort on program description and ethnic code description
    '        .Append("ORDER BY ")
    '        .Append("arPrograms.ProgDescrip, EthCodeDescrip ")
    '    End With

    '    ' run queries, get raw DataSet
    '    dsRaw = IPEDSDB.DataAccessIPEDS().RunSQLDataSet(sb.ToString)

    '    ' set table names for returned data
    '    dsRaw.Tables(0).TableName = TblNameStudents

    '    ' return the raw DataSet
    '    Return dsRaw

    'End Function
    Public Shared Function GetReportDatasetRaw(ByVal RptParamInfo As ReportParamInfoIPEDS) As DataSet

        ' get list of students to include, based on report parameters
        Dim StudentList As String = IPEDSDB.GetStudentList(RptParamInfo)

        ' if there are no students to include, return empty dataset
        If StudentList = "" Then
            Return New DataSet
        End If

        Dim sb As New System.Text.StringBuilder
        Dim dsRaw As New DataSet

        With sb
            ' get Students
            .Append("SELECT arStudent.StudentId, ")

            ' retrieve all other needed columns
            .Append("arPrograms.ProgId, arPrograms.ProgDescrip,EthCodeDescrip,GenderDescrip ")
            '.Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
            .Append("	FROM adEthCodes,adGenders, ")
            .Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies, ")
            .Append("        arStudent, arStuEnrollments, arPrgVersions, arPrograms, ")
            .Append("syStatusCodes, sySysStatus ")
            .Append("	WHERE adEthCodes.EthCodeId = syRptAgencySchoolMapping.SchoolDescripId AND ")
            .Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
            .Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
            .Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
            .Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
            .Append("	      adEthCodes.EthCodeId = arStudent.Race  ")
            '.Append("(SELECT syRptAgencyFldValues.AgencyDescrip ")
            '.Append("	FROM adGenders, ")
            '.Append("		 syRptAgencySchoolMapping, syRptAgencyFldValues, syRptAgencyFields, syRptAgencies ")
            .Append("	AND adGenders.GenderId = syRptAgencySchoolMapping.SchoolDescripId AND ")
            .Append("	      syRptAgencySchoolMapping.RptAgencyFldValId = syRptAgencyFldValues.RptAgencyFldValId AND ")
            .Append("	      syRptAgencyFldValues.RptAgencyFldId = syRptAgencyFields.RptAgencyFldId AND ")
            .Append("	      syRptAgencyFields.RptAgencyId = syRptAgencies.RptAgencyId AND ")
            .Append("	      syRptAgencies.Descrip LIKE '" & AgencyName & "' AND ")
            .Append("	      adGenders.GenderId = arStudent.Gender  ")


            ' establish necessary relationships
            .Append("AND ")
            .Append("arStudent.StudentId = arStuEnrollments.StudentId AND ")
            .Append("arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId AND ")
            .Append("arPrgVersions.ProgId = arPrograms.ProgId AND ")
            .Append("arStuEnrollments.StatusCodeId = syStatusCodes.StatusCodeId AND ")
            .Append("syStatusCodes.SysStatusId = sySysStatus.SysStatusId AND ")

            ' only get those students with completed enrollments, within report date range
            .Append("sySysStatus.SysStatusDescrip LIKE 'Graduated' AND ")
            .Append("arStuEnrollments.ExpGradDate BETWEEN '" & FmtRptDateParam(RptParamInfo.CohortStartDate) & "' AND '" & FmtRptDateParam(RptParamInfo.CohortEndDate) & "' AND ")

            ' append list of appropriate students to include
            .Append("arStudent.StudentId IN (" & StudentList & ") ")

            ' apply sort on program description and ethnic code description
            .Append("ORDER BY ")
            .Append("arPrograms.ProgDescrip, EthCodeDescrip ")
        End With

        ' run queries, get raw DataSet
        dsRaw = IPEDSDB.DataAccessIPEDS().RunSQLDataSet(sb.ToString)

        ' set table names for returned data
        dsRaw.Tables(0).TableName = TblNameStudents

        ' return the raw DataSet
        Return dsRaw

    End Function

End Class
