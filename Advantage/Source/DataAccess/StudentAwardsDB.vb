Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' StudentAwardsDB.vb
'
' StudentAwardsDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class StudentAwardsDB
    Public Function GetAllStudentAwards(ByVal stuEnrollId As String, Optional ByVal FundSourceId As String = "") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         SA.StudentAwardId, ")
            .Append("         SA.StuEnrollId, ")
            .Append("         SA.AwardTypeId, ")
            .Append("         (SELECT FundSourceDescrip FROM saFundSources WHERE FundSourceId = SA.AwardTypeId) AS AwardTypeDescrip, ")
            .Append("         SA.AcademicYearId, ")
            .Append("         SA.LenderId, ")
            .Append("         (SELECT LenderDescrip FROM faLenders WHERE LenderId = SA.LenderId) AS LenderDescrip, ")
            .Append("         SA.ServicerId, ")
            .Append("         SA.GuarantorId, ")
            .Append("         SA.GrossAmount, ")
            .Append("         SA.LoanFees, ")
            .Append("         SA.AwardStartDate, ")
            .Append("         SA.AwardEndDate, ")
            .Append("         SA.Disbursements, ")
            .Append("         SA.LoanId, ")
            .Append("         SA.ModUser, ")
            .Append("         SA.ModDate ")
            '' New Code Added By Vijay Ramteke On August 25, 2010 For Mantis Id 19621
            ''.Append("FROM     faStudentAwards SA  ")
            .Append("FROM     faStudentAwards SA, saFundSources F  ")
            '' New Code Added By Vijay Ramteke On August 25, 2010 For Mantis Id 19621
            .Append("WHERE    SA.StuEnrollId = ? ")
            '' New Code Added By Vijay Ramteke On August 25, 2010 For Mantis Id 19621
            .Append("And SA.AwardTypeId=F.FundSourceId ")

            ''' Code changes by kamalesh ahuja to resolve mantis issue id 19620
            'If Not CutOffDate = "" Then
            '    .Append("AND (F.CutoffDate > ? Or F.CutoffDate IS NULL) ")
            'End If
            '' New Code Added By Vijay Ramteke On Septemper 27, 2010 For Mantis Id 19767
            If (Not HttpContext.Current.Session("UserName").ToString.ToUpper = "SA" And Not HttpContext.Current.Session("UserName").ToString.ToUpper = "SUPER" And Not HttpContext.Current.Session("UserName").ToString.ToUpper = "SUPPORT") Then
                .Append("AND (F.CutoffDate > SA.AwardStartDate Or F.CutoffDate IS NULL) ")
            End If
            '''''
            '' New Code Added By Vijay Ramteke On Septemper 27, 2010 For Mantis Id 19767
            '''''
            If FundSourceId <> "" Then
                .Append(" AND F.FundSourceId = '")
                .Append(FundSourceId)
                .Append("' ")
            End If


            '' New Code Added By Vijay Ramteke On August 25, 2010 For Mantis Id 19621
            .Append(" ORDER BY AwardStartDate DESC, AwardTypeDescrip")
        End With

        ' Add StudentId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'If FundSourceId <> "" Then
        '    db.AddParameter("@FundSourceId", FundSourceId, DataAccess.OleDbDataType.OleDbString, ParameterDirection.Input)
        'End If
        '' New Code Added By Vijay Ramteke On August 25, 2010 For Mantis Id 19621
        ''' Code Commented by kamalesh ahuja to resolve mantis issue id 19620
        'If Not CutOffDate = "" Then
        '    db.AddParameter("@CutOffDate", CutOffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        'End If
        ''''''''
        '' New Code Added By Vijay Ramteke On August 25, 2010 For Mantis Id 19621
        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    ''Function Added by Kamalesh Ahuja on 31 Aug 2010 to Resolve Mantis Issue Id 19665
    Public Function GetAllStudentAwardsNew(ByVal stuEnrollId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         SA.StudentAwardId, ")
            .Append("         SA.StuEnrollId, ")
            .Append("         SA.AwardTypeId, ")
            .Append("         (SELECT FundSourceDescrip FROM saFundSources WHERE FundSourceId = SA.AwardTypeId) AS AwardTypeDescrip, ")
            .Append("         SA.AcademicYearId, ")
            .Append("         SA.LenderId, ")
            .Append("         (SELECT LenderDescrip FROM faLenders WHERE LenderId = SA.LenderId) AS LenderDescrip, ")
            .Append("         SA.ServicerId, ")
            .Append("         SA.GuarantorId, ")
            .Append("         SA.GrossAmount, ")
            .Append("         SA.LoanFees, ")
            .Append("         SA.AwardStartDate, ")
            .Append("         SA.AwardEndDate, ")
            .Append("         SA.Disbursements, ")
            .Append("         SA.LoanId, ")
            .Append("         SA.ModUser, ")
            .Append("         SA.ModDate ")

            .Append("FROM     faStudentAwards SA  ")

            .Append("WHERE    SA.StuEnrollId = ? ")

            .Append("ORDER BY AwardStartDate DESC, AwardTypeDescrip")
        End With

        ' Add StudentId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    ''
    Public Function GetStudentAwardSchedule(ByVal stuAwardId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   returns the Award Schedules by given stuAwardId

            '''' Code modifed by Kamalesh Ahuja 0n 02 April 2010 to resolve mantis issue id 18769
            ''.Append("select SA.AwardScheduleId,SA.ExpectedDate,PD.Amount from faStudentAwardSchedule SA,saPmtDisbRel PD,saTransactions T where SA.AwardScheduleId = PD.AwardScheduleId and PD.TransactionId = T.TransactionId and T.Voided=0 and SA.StudentAwardId=?")

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Troy: 11/20/2013 Commented out the following code and added modified code to fix DEDE10244
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '.Append("select * from (select SA.AwardScheduleId,SA.ExpectedDate,PD.Amount,")
            '.Append(" (select sum(TransAmount) from saTransactions T, saRefunds R where T.TransactionId=R.TransactionId and R.AwardScheduleId=SA.AwardScheduleId and T.Voided=0 )-PD.Amount as RefundAmount")
            '.Append(" from faStudentAwardSchedule SA,saPmtDisbRel PD,saTransactions T where SA.AwardScheduleId = PD.AwardScheduleId and PD.TransactionId = T.TransactionId and T.Voided=0")
            '.Append("and SA.StudentAwardId=?) R where (RefundAmount < 0 or RefundAmount is null)  order by ExpectedDate ")
            '''''''''''''''''''''''''''''''''''''''''''''''''''
            .Append("SELECT  * ")
            .Append("FROM    ( SELECT    SA.AwardScheduleId , ")
            .Append("                    SA.ExpectedDate , ")
            .Append("                    SA.Amount , ")
            .Append("                    ( SELECT    SUM(TransAmount) ")
            .Append("                      FROM      saTransactions T , saRefunds R ")
            .Append("                      WHERE     T.TransactionId = R.TransactionId ")
            .Append("                      AND R.AwardScheduleId = SA.AwardScheduleId ")
            .Append("                      AND T.Voided = 0 ) - SA.Amount AS RefundAmount ")
            .Append("           FROM      faStudentAwardSchedule SA ")
            .Append("           WHERE     SA.StudentAwardId = ? ")
            .Append("        ) R ")
            .Append("WHERE   ( RefundAmount < 0 OR RefundAmount IS NULL) ")
            .Append("ORDER BY ExpectedDate ")

        End With

        ' Add AwardId to the parameter list
        db.AddParameter("@StuAwardId", stuAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetStudentAwardScheduleForRefunds(ByVal stuAwardId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb

            .Append("SELECT  * ")
            .Append("FROM    ( SELECT    SA.AwardScheduleId , ")
            .Append("                    SA.ExpectedDate , ")
            .Append("                    ((SELECT SUM(Amount) ")
            .Append("                     FROM dbo.saPmtDisbRel PDR, dbo.saTransactions TR ")
            .Append("                     WHERE PDR.AwardScheduleId=SA.AwardScheduleId ")
            .Append("                     AND PDR.TransactionId=TR.TransactionId ")
            .Append("                     AND TR.Voided=0) ")
            .Append("                     - ")
            .Append("                     (SELECT ISNULL(SUM(TransAmount),0) ")
            .Append("                      FROM saTransactions T ,saRefunds R ")
            .Append("                      WHERE T.TransactionId = R.TransactionId ")
            .Append("                      AND R.AwardScheduleId = SA.AwardScheduleId ")
            .Append("                      AND T.Voided = 0)) ")
            .Append("                      AS Amount ")
            .Append("                    ,( SELECT    SUM(TransAmount) ")
            .Append("                      FROM      saTransactions T , saRefunds R ")
            .Append("                      WHERE     T.TransactionId = R.TransactionId ")
            .Append("                      AND R.AwardScheduleId = SA.AwardScheduleId ")
            .Append("                      AND T.Voided = 0 ) - SA.Amount AS RefundAmount ")
            .Append("           FROM      faStudentAwardSchedule SA ")
            .Append("           WHERE     SA.StudentAwardId = ? ")
            .Append("        ) R ")
            '.Append("WHERE   ( RefundAmount < 0 OR RefundAmount IS NULL) ")
            '.Append("         AND R.Amount > 0 ")
            '.Append("         AND R.Amount + ISNULL(R.RefundAmount,0) > 0 ")
            .Append("WHERE R.Amount > 0 ")
            .Append("ORDER BY ExpectedDate DESC ")

        End With

        ' Add AwardId to the parameter list
        db.AddParameter("@StuAwardId", stuAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetAwardsPerStudent(ByVal stuEnrollId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   returns the Fund Sources or Awards received by given stuEnrollId
            .Append("SELECT DISTINCT ")
            .Append("           FS.FundSourceId, FS.FundSourceDescrip ")
            .Append("FROM       faStudentAwards SA, saFundSources FS ")
            .Append("WHERE      SA.StuEnrollId=? AND FS.FundSourceId=SA.AwardTypeId ")
            .Append("ORDER BY   FS.FundSourceDescrip")
        End With

        ' Add StudentId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAvailableAwardsForStudent(ByVal stuEnrollId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   returns the Fund Sources or Awards received by given stuEnrollId
            '.Append("SELECT DISTINCT ")
            '.Append("           F.FundSourceDescrip,F.FundSourceId ")
            '.Append("FROM       faStudentAwards A,faStudentAwardSchedule B,saFundSources F ")
            '.Append("WHERE      A.StuEnrollId=? ")
            '.Append("           AND A.StudentAwardId=B.StudentAwardId ")
            '.Append("           AND NOT EXISTS (SELECT * FROM saPmtDisbRel WHERE AwardScheduleId=B.AwardScheduleId) ")
            '.Append("           AND F.FundSourceId=A.AwardTypeId ")
            .Append("SELECT	DISTINCT ")
            .Append("		T.AwardTypeId As FundSourceId, ")
            .Append("		F.FundSourceDescrip ")
            .Append("FROM ")
            .Append("( ")
            .Append("select ")
            .Append("		SA.AwardTypeId, ")
            .Append("		SAS.AwardScheduleId, ")
            ''.Append("		Amount - Coalesce((Select Sum(Amount) from saPmtDisbrel where AwardScheduleId=SAS.AwardScheduleId),0) as Balance  ")
            '' New code added by Kamalesh on 02 April 2010 to fix mantis issue id 18771 ''
            .Append("		Amount - Coalesce((Select Sum(Amount) from saPmtDisbrel where AwardScheduleId=SAS.AwardScheduleId),0) ")
            .Append("		+ Coalesce((Select Sum(TransAmount) from satransactions TR,saRefunds R where R.AwardScheduleId=SAS.AwardScheduleId and TR.transactionid=R.Transactionid),0) as Balance  ")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            .Append("from  ")
            .Append("		faStudentAwards SA, faStudentAwardSchedule SAS  ")
            .Append("where  ")
            .Append("		SA.StuEnrollId = ? ")
            .Append("and	SA.StudentAwardId=SAS.StudentAwardId ")
            .Append(") T, saFundSources F ")
            .Append("WHERE ")
            .Append("		T.AwardTypeId=F.FundSourceId ")
            .Append("AND	Balance > 0 ")
            .Append("ORDER BY FundSourceDescrip ")
        End With

        ' Add StudentId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    ''Added by Saraswathi lakshmanan on April 15 2010
    ''Converted to stored Procedure
    ''New Code Addded By Vijay Ramteke On June 09, 2010 For Mantis Id 18950
    Public Function GetAvailableAwardsForStudent_Sp(ByVal stuEnrollId As String, ByVal CutoffDate As String) As DataSet

        '   connect to the database
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        ' Add StudentId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        ''New Code Addded By Vijay Ramteke On June 09, 2010 For Mantis Id 18950
        db.AddParameter("@CutoffDate", CutoffDate, SqlDbType.DateTime, , ParameterDirection.Input)
        ''New Code Addded By Vijay Ramteke On June 09, 2010 For Mantis Id 18950
        '   return dataset
        Return db.RunParamSQLDataSet_SP("USP_SA_GetAvailableAwardsForStudent")
    End Function

    'Public Function GetStuAwardInfo(ByVal StudentAwardId As String) As StuAwardInfo

    '    '   connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        '   with subqueries
    '        .Append("SELECT ")
    '        .Append("         SA.StudentAwardId, ")
    '        .Append("         SA.StuEnrollId, ")
    '        .Append("         SA.AwardTypeId, ")
    '        .Append("         (Select AwardTypeDescrip from faAwardTypes where AwardTypeId=SA.AwardTypeId) As AwardType, ")
    '        .Append("         SA.AcademicYearId, ")
    '        .Append("         (Select AcademicYearDescrip from saAcademicYears where AcademicYearId=SA.AcademicYearId) As AcademicYear, ")
    '        .Append("         SA.LenderId, ")
    '        .Append("         (Select LenderDescrip from faLenders where LenderId=SA.LenderId) As Lender, ")
    '        .Append("         SA.ServicerId, ")
    '        .Append("         (Select LenderDescrip from faLenders where LenderId=SA.ServicerId) As Servicer, ")
    '        .Append("         SA.GuarantorId, ")
    '        .Append("         (Select LenderDescrip from faLenders where LenderId=SA.GuarantorId) As Guarantor, ")
    '        .Append("         SA.GrossAmount, ")
    '        .Append("         SA.LoanFees, ")
    '        .Append("         SA.AwardStartDate, ")
    '        .Append("         SA.AwardEndDate, ")
    '        .Append("         SA.Disbursements, ")
    '        .Append("         SA.LoanId, ")
    '        .Append("         SA.ModUser, ")
    '        .Append("         SA.ModDate ")
    '        .Append("FROM  faStudentAwards SA ")
    '        .Append("WHERE SA.StudentAwardId = ? ")
    '    End With

    '    ' Add the StudentAwardId to the parameter list
    '    db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    '   Execute the query
    '    Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

    '    Dim StuAwardInfo As New StuAwardInfo

    '    While dr.Read()

    '        '   set properties with data from DataReader
    '        With StuAwardInfo
    '            .IsInDB = True
    '            .StudentAwardId = StudentAwardId
    '            .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
    '            If Not (dr("AwardTypeId") Is System.DBNull.Value) Then .AwardTypeId = CType(dr("AwardTypeId"), Guid).ToString
    '            If Not (dr("AwardType") Is System.DBNull.Value) Then .AwardType = dr("AwardType")
    '            If Not (dr("AcademicYearId") Is System.DBNull.Value) Then .AcademicYearId = CType(dr("AcademicYearId"), Guid).ToString
    '            If Not (dr("AcademicYear") Is System.DBNull.Value) Then .AcademicYear = dr("AcademicYear")
    '            If Not (dr("LenderId") Is System.DBNull.Value) Then .LenderId = CType(dr("LenderId"), Guid).ToString
    '            If Not (dr("Lender") Is System.DBNull.Value) Then .Lender = dr("Lender")
    '            If Not (dr("ServicerId") Is System.DBNull.Value) Then .ServicerId = CType(dr("ServicerId"), Guid).ToString
    '            If Not (dr("Servicer") Is System.DBNull.Value) Then .Servicer = dr("Servicer")
    '            If Not (dr("GuarantorId") Is System.DBNull.Value) Then .GuarantorId = CType(dr("GuarantorId"), Guid).ToString
    '            If Not (dr("Guarantor") Is System.DBNull.Value) Then .Guarantor = dr("Guarantor")
    '            If Not (dr("GrossAmount") Is System.DBNull.Value) Then .GrossAmount = dr("GrossAmount")
    '            If Not (dr("LoanFees") Is System.DBNull.Value) Then .LoanFees = dr("LoanFees")
    '            .NetLoanAmount = .GrossAmount - .LoanFees
    '            If Not (dr("AwardStartDate") Is System.DBNull.Value) Then .AwardStartDate = dr("AwardStartDate")
    '            If Not (dr("AwardEndDate") Is System.DBNull.Value) Then .AwardEndDate = dr("AwardEndDate")
    '            If Not (dr("Disbursements") Is System.DBNull.Value) Then .Disbursements = dr("Disbursements")
    '            If Not (dr("LoanId") Is System.DBNull.Value) Then .LoanId = dr("LoanId")
    '            If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
    '            If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
    '        End With

    '    End While

    '    If Not dr.IsClosed Then dr.Close()
    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    '   Return StuAwardInfo
    '    Return StuAwardInfo

    'End Function
    Public Function UpdateStuAwardInfo(ByVal StuAwardInfo As StuAwardInfo, ByVal user As String) As ResultInfo
        Dim resInfo As New ResultInfo

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE faStudentAwards ")
                .Append("   Set StudentAwardId = ?, StuEnrollId =?, ")
                .Append("       AwardTypeId = ?,  AcademicYearId = ?, ")
                .Append("       LenderId = ?, ServicerId = ?, GuarantorId = ?, ")
                .Append("       GrossAmount = ?, LoanFees = ?, AwardStartDate = ?, ")
                .Append("       AwardEndDate = ?, Disbursements = ?, ")
                .Append("       LoanId = ?, ModUser = ?, ModDate = ? ")
                .Append("WHERE StudentAwardId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from faStudentAwards where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   StudentAwardId
            db.AddParameter("@StudentAwardId", StuAwardInfo.StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   StuEnrollId
            db.AddParameter("@StuEnrollId", StuAwardInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   AwardTypeId
            If StuAwardInfo.AwardTypeId = Guid.Empty.ToString Then
                db.AddParameter("@AwardTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AwardTypeId", StuAwardInfo.AwardTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   AcademicYearId
            If StuAwardInfo.AcademicYearId = Guid.Empty.ToString Then
                db.AddParameter("@AcademicYearId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AcademicYearId", StuAwardInfo.AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   LenderId
            If StuAwardInfo.LenderId = Guid.Empty.ToString Then
                db.AddParameter("@LenderId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@LenderId", StuAwardInfo.LenderId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ServicerId
            If StuAwardInfo.ServicerId = Guid.Empty.ToString Then
                db.AddParameter("@ServicerId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@ServicerId", StuAwardInfo.ServicerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   GuarantorId
            If StuAwardInfo.GuarantorId = Guid.Empty.ToString Then
                db.AddParameter("@GuarantorId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@GuarantorId", StuAwardInfo.GuarantorId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   GrossAmount
            db.AddParameter("@GrossAmount", StuAwardInfo.GrossAmount, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   LoanFees
            db.AddParameter("@LoanFees", StuAwardInfo.LoanFees, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   AwardStartDate
            db.AddParameter("@AwardStartDate", StuAwardInfo.AwardStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   AwardEndDate
            db.AddParameter("@AwardEndDate", StuAwardInfo.AwardEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   Disbursements
            db.AddParameter("@Disbursements", StuAwardInfo.Disbursements, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   LoanId
            db.AddParameter("@LoanId", StuAwardInfo.LoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   StudentAwardId
            db.AddParameter("@StudentAwardId", StuAwardInfo.StudentAwardId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", StuAwardInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                'Return ""
                StuAwardInfo.ModUser = user
                StuAwardInfo.ModDate = now
                resInfo.UpdatedObject = StuAwardInfo
                Return resInfo
            Else
                resInfo.ErrorString = DALExceptions.BuildConcurrencyExceptionMessage()
                Return resInfo
            End If

        Catch ex As OleDbException

            '   return an error to the client
            'DisplayOleDbErrorCollection(ex)
            resInfo.ErrorString = DALExceptions.BuildErrorMessage(ex)
            Return resInfo

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddStuAwardInfo(ByVal StuAwardInfo As StuAwardInfo, ByVal user As String) As ResultInfo
        Dim resInfo As New ResultInfo

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT faStudentAwards ( ")
                .Append("       StudentAwardId, StuEnrollId, ")
                .Append("       AwardTypeId, AcademicYearId, ")
                .Append("       LenderId, ServicerId, GuarantorId, ")
                .Append("       GrossAmount, LoanFees, ")
                .Append("       AwardStartDate, AwardEndDate, ")
                .Append("       Disbursements, LoanId, ")
                .Append("       ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   StudentAwardId
            db.AddParameter("@StudentAwardId", StuAwardInfo.StudentAwardId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StuEnrollId
            db.AddParameter("@StuEnrollId", StuAwardInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   AwardTypeId
            If StuAwardInfo.AwardTypeId = Guid.Empty.ToString Then
                db.AddParameter("@AwardTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AwardTypeId", StuAwardInfo.AwardTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   AcademicYearId
            If StuAwardInfo.AcademicYearId = Guid.Empty.ToString Then
                db.AddParameter("@AcademicYearId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AcademicYearId", StuAwardInfo.AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   LenderId
            If StuAwardInfo.LenderId = Guid.Empty.ToString Then
                db.AddParameter("@LenderId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@LenderId", StuAwardInfo.LenderId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   ServicerId
            If StuAwardInfo.ServicerId = Guid.Empty.ToString Then
                db.AddParameter("@ServicerId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@ServicerId", StuAwardInfo.ServicerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   GuarantorId
            If StuAwardInfo.GuarantorId = Guid.Empty.ToString Then
                db.AddParameter("@GuarantorId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@GuarantorId", StuAwardInfo.GuarantorId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   GrossAmount
            db.AddParameter("@GrossAmount", StuAwardInfo.GrossAmount, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   LoanFees
            db.AddParameter("@LoanFees", StuAwardInfo.LoanFees, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   AwardStartDate
            db.AddParameter("@AwardStartDate", StuAwardInfo.AwardStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   AwardEndDate
            db.AddParameter("@AwardEndDate", StuAwardInfo.AwardEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   Disbursements
            db.AddParameter("@Disbursements", StuAwardInfo.Disbursements, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   LoanId
            db.AddParameter("@LoanId", StuAwardInfo.LoanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            'Return ""
            StuAwardInfo.IsInDB = True
            StuAwardInfo.ModUser = user
            StuAwardInfo.ModDate = now
            resInfo.UpdatedObject = StuAwardInfo
            Return resInfo

        Catch ex As OleDbException
            '   return an error to the client
            resInfo.ErrorString = DALExceptions.BuildErrorMessage(ex)
            Return resInfo

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteStuAwardInfo(ByVal StudentAwardId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM faStudentAwards ")
                .Append("WHERE StudentAwardId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM faStudentAwards WHERE StudentAwardId = ? ")
            End With

            '   add parameters values to the query

            '   StudentAwardId
            db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   StudentAwardId
            db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetStuAwardsDS(Optional ByVal studentID As String = "") As DataSet

        '   create dataset
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   build the sql query for the StudentAwardSchedules data adapter
        Dim sb As New StringBuilder
        With sb
            '.Append("SELECT SAS.AwardScheduleId, SAS.StudentAwardId, ")
            '.Append("       SAS.ExpectedDate, SAS.Amount, SAS.Reference, ")
            '.Append("       SAS.ModUser, SAS.ModDate, ")
            '.Append("	    WasReceived = (CASE WHEN (SELECT COUNT(*) FROM saPmtDisbRel WHERE AwardScheduleId=SAS.AwardScheduleId) > 0 THEN 'True' ELSE 'False' END) ")
            '.Append("FROM   faStudentAwardSchedule SAS ")
            '.Append("ORDER BY SAS.StudentAwardId, SAS.ExpectedDate  ")
            .Append("SELECT *, ")
            .Append("	   (Case when Balance = 0.00 then 'True' else 'False' end) as WasReceived ")
            .Append("FROM ")
            .Append("	(  ")
            .Append("SELECT SAS.AwardScheduleId, ")
            .Append("	   SAS.StudentAwardId,  ")
            .Append("       SAS.ExpectedDate,  ")
            .Append("	   SAS.Amount,  ")
            .Append("	   SAS.Reference,  ")
            .Append("       SAS.ModUser,  ")
            .Append("	   SAS.ModDate,  ")
            '.Append("	   (SAS.Amount - Coalesce((Select Sum(Amount) from saPmtDisbrel where AwardScheduleId=SAS.AwardScheduleId),0)) as Balance,  ")
            .Append("	   (SAS.Amount + Coalesce((Select Sum(TransAmount) from saPmtDisbRel PDR, saTransactions T where AwardScheduleId=SAS.AwardScheduleId and PDR.TransactionId=T.TransactionId and T.Voided=0),0)) as Balance ")
            ''Code Added by Kamalesh Ahuja on 15 Jan 2010 to Resolve Mantis Issue Id 12038 and 08425
            .Append("	,   ( Coalesce((Select Sum(PDR.Amount) from saPmtDisbRel PDR, saTransactions T where AwardScheduleId=SAS.AwardScheduleId and PDR.TransactionId=T.TransactionId and T.Voided=0),0)) as Received ")
            .Append("  ,isnull((select SUM(RefundAmount) from saRefunds R, saTransactions TR where TR.TransactionId=R.TransactionId and TR.Voided=0 and R.AwardScheduleId = SAS.AwardScheduleId),0) as RefundAmount  ")
            ''''''
            '.Append("	   WasReceived = (CASE WHEN (SELECT COUNT(*) FROM saPmtDisbRel WHERE AwardScheduleId=SAS.AwardScheduleId) > 0 THEN 'True' ELSE 'False' END)  ")
            .Append("FROM   faStudentAwardSchedule SAS  ")
            If studentID <> "" Then
                .Append(" ,faStudentAwards SA where SAS.StudentAwardId=SA.StudentAwardId and SA.StuEnrollId in(select StuEnrollId from arStuEnrollments where StudentId= ? ) ")
            End If
            .Append("	) T ")
            .Append("ORDER BY StudentAwardId, ExpectedDate   ")
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
        If studentID <> "" Then
            sc.Parameters.Add(New OleDbParameter("studentId", studentID))
        End If
        '   Create adapter to handle StudentAwardSchedules table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill StudentAwardSchedules table
        da.Fill(ds, "StudentAwardSchedules")

        '   build select query for the StudentAwards data adapter
        '   build the sql query
        sb = New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("         SA.StudentAwardId, ")
            .Append("         SA.StuEnrollId, ")
            .Append("         SA.AwardTypeId, ")
            .Append("         (SELECT FundSourceDescrip FROM saFundSources WHERE FundSourceId = SA.AwardTypeId) AS AwardTypeDescrip, ")
            .Append("         SA.AcademicYearId, ")
            .Append("         (Select AcademicYearDescrip from saAcademicYears where AcademicYearId=SA.AcademicYearId) As AcademicYear, ")
            .Append("         SA.LenderId, ")
            .Append("         (Select LenderDescrip from faLenders where LenderId=SA.LenderId) As Lender, ")
            .Append("         SA.ServicerId, ")
            .Append("         (Select LenderDescrip from faLenders where LenderId=SA.ServicerId) As Servicer, ")
            .Append("         SA.GuarantorId, ")
            .Append("         (Select LenderDescrip from faLenders where LenderId=SA.GuarantorId) As Guarantor, ")
            .Append("         SA.GrossAmount, ")
            .Append("         SA.LoanFees, ")
            .Append("         SA.AwardStartDate, ")
            .Append("         SA.AwardEndDate, ")
            .Append("         SA.Disbursements, ")
            .Append("         SA.LoanId, ")
            .Append("         SA.ModUser, ")
            .Append("         SA.ModDate,SA.FA_ID,SA.AwardSubCode,SA.AwardCode,SA.AwardStatus ")
            .Append("FROM     faStudentAwards SA ")
            If studentID <> "" Then
                .Append(" where SA.StuEnrollId in(select StuEnrollId from arStuEnrollments where StudentId= ? ) ")
            End If
            .Append(" ORDER BY AwardStartDate DESC, AwardEndDate ASC, AwardTypeDescrip")
        End With

        '   modify select command
        'da.SelectCommand.CommandText = sb.ToString
        Dim sc1 As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
        If studentID <> "" Then
            sc1.Parameters.Add(New OleDbParameter("studentId", studentID))
        End If
        Dim da1 As New OleDbDataAdapter(sc1)

        '   fill faStudentAwards table
        da1.Fill(ds, "StudentAwards")

        '   create primary and foreign key constraints

        '   set primary key for faStudentAwardSchedules table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("StudentAwardSchedules").Columns("AwardScheduleId")
        ds.Tables("StudentAwardSchedules").PrimaryKey = pk0

        '   set primary key for faStudentAwards table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("StudentAwards").Columns("StudentAwardId")
        ds.Tables("StudentAwards").PrimaryKey = pk1

        '   set foreign key column in saStudentAwardSchedules
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("StudentAwardSchedules").Columns("StudentAwardId")

        '   add relationship
        ds.Relations.Add("StudentAwardsStudentAwardSchedules", pk1, fk0)

        '   return dataset
        Return ds

    End Function
    Public Function UpdateStuAwardsDS(ByVal ds As DataSet, Optional ByVal StudentAwardId As String = "", Optional ByVal StudentId As String = "") As String

        '   all updates must be encapsulated as one transaction

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build the sql query for the StudentAwardSchedules data adapter
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT SAS.AwardScheduleId, SAS.StudentAwardId, ")
                .Append("       SAS.ExpectedDate, SAS.Amount, SAS.Reference ")
                .Append("FROM   faStudentAwardSchedule SAS ")
            End With

            '   build select command
            Dim StudentAwardSchedulesSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle StudentAwardSchedules table
            Dim StudentAwardSchedulesDataAdapter As New OleDbDataAdapter(StudentAwardSchedulesSelectCommand)

            '   build insert, update and delete commands for StudentAwardSchedules table
            Dim cb As New OleDbCommandBuilder(StudentAwardSchedulesDataAdapter)

            '   build select query for the StudentAwards data adapter
            sb = New StringBuilder
            With sb
                '   with subqueries
                .Append("SELECT ")
                .Append("         SA.StudentAwardId, ")
                .Append("         SA.StuEnrollId, ")
                .Append("         SA.AwardTypeId, ")
                .Append("         SA.AcademicYearId, ")
                .Append("         SA.LenderId, ")
                .Append("         SA.ServicerId, ")
                .Append("         SA.GuarantorId, ")
                .Append("         SA.GrossAmount, ")
                .Append("         SA.LoanFees, ")
                .Append("         SA.AwardStartDate, ")
                .Append("         SA.AwardEndDate, ")
                .Append("         SA.Disbursements, ")
                .Append("         SA.LoanId, ")
                .Append("         SA.ModUser, ")
                .Append("         SA.ModDate,SA.FA_ID ")
                If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    .Append(" ,SA.awardcode,SA.awardsubcode,SA.AwardStatus ")
                End If
                .Append("FROM     faStudentAwards SA ")
            End With

            '   build select command
            Dim StudentAwardsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle StudentAwards table
            Dim StudentAwardsDataAdapter As New OleDbDataAdapter(StudentAwardsSelectCommand)

            '   build insert, update and delete commands for StudentAwards table
            Dim cb1 As New OleDbCommandBuilder(StudentAwardsDataAdapter)

            '   insert added rows in StudentAwards table
            StudentAwardsDataAdapter.Update(ds.Tables("StudentAwards").Select(Nothing, Nothing, DataViewRowState.Added))

            '   insert added rows in StudentAwardSchedules table
            StudentAwardSchedulesDataAdapter.Update(ds.Tables("StudentAwardSchedules").Select(Nothing, Nothing, DataViewRowState.Added))

            '   delete rows in StudentAwardSchedules table
            StudentAwardSchedulesDataAdapter.Update(ds.Tables("StudentAwardSchedules").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   delete rows in StudentAwards table
            StudentAwardsDataAdapter.Update(ds.Tables("StudentAwards").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   update rows in StudentAwardSchedules table
            StudentAwardSchedulesDataAdapter.Update(ds.Tables("StudentAwardSchedules").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   update rows in StudentAwards table
            StudentAwardsDataAdapter.Update(ds.Tables("StudentAwards").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   everything went fine - commit transaction
            groupTrans.Commit()


            'Insert data into regent xml 
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                Dim regDB As New regentDB
                Dim strFilename As String = AdvantageCommonValues.getStudentBatchFileName()
                regDB.AddAwardXML(StudentId, strFilename, StudentAwardId)
            End If
            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Catch ex As Exception
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return ex.Message

        Finally

            '   close connection
            connection.Close()
        End Try

    End Function
    'Public Function GetAllAwardTypes(ByVal strStatus As String) As DataSet

    '    '   connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("SELECT A.AwardTypeId, A.StatusId, A.AwardTypeDescrip, A.AwardTypeCode ")
    '        .Append("FROM faAwardTypes A, syStatuses ST ")
    '        .Append("WHERE A.StatusId = ST.StatusId ")
    '        If strStatus = "True" Then
    '            .Append("AND    ST.Status = 'Active' ")
    '            .Append("ORDER BY A.AwardTypeDescrip ")
    '        ElseIf strStatus = "False" Then
    '            .Append("AND    ST.Status = 'Inactive' ")
    '            .Append("ORDER BY A.AwardTypeDescrip ")
    '        Else
    '            .Append("ORDER BY ST.Status,A.AwardTypeDescrip ASC")
    '        End If
    '    End With

    '    '   return dataset
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
    'Public Function GetAllAwardYears() As DataSet
    '    '   return dataset
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
    ''Public Function GetAllAwardYears() As DataSet

    'End Function
    '    '   connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   build the sql query
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("SELECT Y.AwardYearId, Y.AwardYearDescrip, Y.AwardYearCode ")
    '        .Append("FROM faAwardYears Y ")
    '        .Append("ORDER BY Y.AwardYearCode ")
    '    End With

    '    '   return dataset
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
    Public Function GetAwardTimeIntervals() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT TI.AwdTimeIntervalId, TI.AwdTimeIntervalDescrip ")
            .Append("FROM faAwardTimeIntervals TI ")
            .Append("ORDER BY TI.AwdTimeIntervalDescrip ")
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetTopTerms(ByVal StartDate As DateTime, ByVal EndDate As DateTime, ByVal DisbNumber As Integer) As DataTable

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder

        '   Retrieve top n terms, including current one.
        '   Current term is such term that contains Start Date between its start date and end date.
        '   Future term is such term that its start date may be greater that Start Date and 
        '   its end date cannot be greated that End Date.
        '   n = DisbNumber
        With sb
            .Append("SELECT TOP " & DisbNumber)
            .Append("       T.StartDate, T.EndDate, T.TermDescrip ")
            .Append("FROM   arTerm T, syStatuses ST  ")
            .Append("WHERE  T.StatusId = ST.StatusId ")
            .Append("       AND ST.Status = 'Active' ")
            .Append("       AND ((T.StartDate <= ? AND ? <= T.EndDate) OR (T.StartDate > ? AND T.StartDate < ?)) ")
            .Append("ORDER BY T.StartDate, T.EndDate;")
        End With

        db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@EndDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@StartDate", EndDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        '   return datatable
        Return ds.Tables(0)
    End Function
    Public Function GetAllPaymentPlans(ByVal stuEnrollId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         PP.PaymentPlanId, ")
            .Append("         PP.StuEnrollId, ")
            .Append("         PP.PayPlanDescrip, ")
            .Append("         PP.AcademicYearId, ")
            .Append("         PP.TotalAmountDue, ")
            .Append("         PP.Disbursements, ")
            .Append("         PP.PayPlanStartDate, ")
            .Append("         PP.PayPlanEndDate, ")
            .Append("         PP.ModUser, ")
            .Append("         PP.ModDate ")
            .Append("FROM     faStudentPaymentPlans PP ")
            .Append("WHERE    PP.StuEnrollId = ? ")
            .Append("ORDER BY PayPlanStartDate DESC")
        End With

        ' Add StudentId to the parameter list
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetPaymentPlanInfo(ByVal PaymentPlanId As String) As PaymentPlanInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("         PP.PaymentPlanId, ")
            .Append("         PP.StuEnrollId, ")
            .Append("         PP.PayPlanDescrip, ")
            .Append("         PP.AcademicYearId, ")
            .Append("         (Select AcademicYearDescrip from saAcademicYears where AcademicYearId=PP.AcademicYearId) as Academic Year, ")
            .Append("         PP.TotalAmountDue, ")
            .Append("         PP.Disbursements, ")
            .Append("         PP.PayPlanStartDate, ")
            .Append("         PP.PayPlanEndDate, ")
            .Append("         PP.ModUser, ")
            .Append("         PP.ModDate ")
            .Append("FROM  faStudentPaymentPlans PP ")
            .Append("WHERE PP.PaymentPlanId = ? ")
        End With

        ' Add the PaymentPlanId to the parameter list
        db.AddParameter("@PaymentPlanId", PaymentPlanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim PaymentPlanInfo As New PaymentPlanInfo

        While dr.Read()

            '   set properties with data from DataReader
            With PaymentPlanInfo
                .IsInDB = True
                .PaymentPlanId = PaymentPlanId
                .StuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
                If Not (dr("PayPlanDescrip") Is System.DBNull.Value) Then .PayPlanDescrip = dr("PayPlanDescrip")
                If Not (dr("AcademicYearId") Is System.DBNull.Value) Then .AcademicYearId = CType(dr("AcademicYearId"), Guid).ToString
                If Not (dr("AcademicYear") Is System.DBNull.Value) Then .AcademicYear = dr("AcademicYear")
                If Not (dr("TotalAmountDue") Is System.DBNull.Value) Then .TotalAmountDue = dr("TotalAmountDue")
                If Not (dr("Disbursements") Is System.DBNull.Value) Then .Disbursements = dr("Disbursements")
                If Not (dr("PayPlanStartDate") Is System.DBNull.Value) Then .PayPlanStartDate = dr("PayPlanStartDate")
                If Not (dr("PayPlanEndDate") Is System.DBNull.Value) Then .PayPlanEndDate = dr("PayPlanEndDate")
                If Not (dr("ModUser") Is System.DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return PaymentPlanInfo
        Return PaymentPlanInfo

    End Function
    Public Function UpdatePaymentPlanInfo(ByVal PaymentPlanInfo As PaymentPlanInfo, ByVal user As String) As ResultInfo
        Dim resInfo As New ResultInfo

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE faStudentPaymentPlans ")
                .Append("   Set PaymentPlanId = ?, StuEnrollId = ?, PayPlanDescrip = ?, ")
                .Append("       AcademicYearId = ?, TotalAmountDue = ?, Disbursements = ?, ")
                .Append("       PayPlanStartDate = ?, PayPlanEndDate = ?, ")
                .Append("       ModUser = ?, ModDate = ? ")
                .Append("WHERE PaymentPlanId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from faStudentPaymentPlans where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   PaymentPlanId
            db.AddParameter("@PaymentPlanId", PaymentPlanInfo.PaymentPlanId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   StuEnrollId
            db.AddParameter("@StuEnrollId", PaymentPlanInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   PayPlanDescrip
            db.AddParameter("@PayPlanDescrip", PaymentPlanInfo.PayPlanDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   AcademicYearId
            If PaymentPlanInfo.AcademicYearId = Guid.Empty.ToString Then
                db.AddParameter("@AcademicYearId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AcademicYearId", PaymentPlanInfo.AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   TotalAmountDue
            db.AddParameter("@TotalAmountDue", PaymentPlanInfo.TotalAmountDue, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   Disbursements
            db.AddParameter("@Disbursements", PaymentPlanInfo.Disbursements, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   PayPlanStartDate
            db.AddParameter("@PayPlanStartDate", PaymentPlanInfo.PayPlanStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   PayPlanEndDate
            db.AddParameter("@PayPlanEndDate", PaymentPlanInfo.PayPlanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   PaymentPlanId
            db.AddParameter("@PaymentPlanId", PaymentPlanInfo.PaymentPlanId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", PaymentPlanInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                'Return ""
                PaymentPlanInfo.ModUser = user
                PaymentPlanInfo.ModDate = now
                resInfo.UpdatedObject = PaymentPlanInfo
                Return resInfo
            Else
                resInfo.ErrorString = DALExceptions.BuildConcurrencyExceptionMessage()
                Return resInfo
            End If

        Catch ex As OleDbException

            '   return an error to the client
            'DisplayOleDbErrorCollection(ex)
            resInfo.ErrorString = DALExceptions.BuildErrorMessage(ex)
            Return resInfo

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddPaymentPlanInfo(ByVal PaymentPlanInfo As PaymentPlanInfo, ByVal user As String) As ResultInfo
        Dim resInfo As New ResultInfo

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT faStudentPaymentPlans ( ")
                .Append("       PaymentPlanId, StuEnrollId, PayPlanDescrip, ")
                .Append("       AcademicYearId, TotalAmountDue, Disbursements, ")
                .Append("       PayPlanStartDate, PayPlanEndDate, ")
                .Append("       ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   PaymentPlanId
            db.AddParameter("@PaymentPlanId", PaymentPlanInfo.PaymentPlanId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StuEnrollId
            db.AddParameter("@StuEnrollId", PaymentPlanInfo.StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   PayPlanDescrip
            db.AddParameter("@PayPlanDescrip", PaymentPlanInfo.PayPlanDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   AcademicYearId
            If PaymentPlanInfo.AcademicYearId = Guid.Empty.ToString Then
                db.AddParameter("@AcademicYearId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AcademicYearId", PaymentPlanInfo.AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   TotalAmountDue
            db.AddParameter("@TotalAmountDue", PaymentPlanInfo.TotalAmountDue, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   Disbursements
            db.AddParameter("@Disbursements", PaymentPlanInfo.Disbursements, DataAccess.OleDbDataType.OleDbMoney, , ParameterDirection.Input)

            '   PayPlanStartDate
            db.AddParameter("@PayPlanStartDate", PaymentPlanInfo.PayPlanStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   PayPlanEndDate
            db.AddParameter("@PayPlanEndDate", PaymentPlanInfo.PayPlanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            'Return ""
            PaymentPlanInfo.IsInDB = True
            PaymentPlanInfo.ModUser = user
            PaymentPlanInfo.ModDate = now
            resInfo.UpdatedObject = PaymentPlanInfo
            Return resInfo

        Catch ex As OleDbException
            '   return an error to the client
            resInfo.ErrorString = DALExceptions.BuildErrorMessage(ex)
            Return resInfo

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeletePaymentPlanInfo(ByVal PaymentPlanId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM faStudentPaymentPlans ")
                .Append("WHERE PaymentPlanId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM faStudentPaymentPlans WHERE PaymentPlanId = ? ")
            End With

            '   add parameters values to the query

            '   PaymentPlanId
            db.AddParameter("@PaymentPlanId", PaymentPlanId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   PaymentPlanId
            db.AddParameter("@PaymentPlanId", PaymentPlanId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return error mesPPge
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetPaymentPlansDS(ByVal paymentPlanId As String, ByVal voided As Boolean?) As DataSet

        '   create dataset
        Dim db As New SQLDataAccess
        Dim ds As New DataSet
        Dim ds2 As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConnectionString")

        Try
            db.OpenConnection()
            If (Not paymentPlanId Is Nothing) Then
                db.AddParameter("@paymentPlanId", New Guid(paymentPlanId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            End If
            If (Not voided Is Nothing) Then
                If (voided.HasValue) Then
                    db.AddParameter("@voided", voided, SqlDbType.Bit, , ParameterDirection.Input)
                End If
            End If
            ds = db.RunParamSQLDataSet_SP("dbo.FA_Advantage_Report_GetStudentPaymentPlan", "PaymentPlans")

            db.ClearParameters()

            If (Not paymentPlanId Is Nothing) Then
                db.AddParameter("@paymentPlanId", New Guid(paymentPlanId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            End If
            If (Not voided Is Nothing) Then
                If (voided.HasValue) Then
                    db.AddParameter("@voided", voided, SqlDbType.Bit, , ParameterDirection.Input)
                End If
            End If
            ds2 = db.RunParamSQLDataSet_SP("dbo.FA_Advantage_Report_GetStudentPaymentPlanSchedules", "PaymentPlanSchedules")


            ds.Tables.Add(ds2.Tables(0).Copy())

            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

    End Function
    Public Function UpdatePaymentPlansDS(ByVal ds As DataSet) As String

        '   all updates must be encapsulated as one tranPPction

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build the sql query for the PaymentPlanSchedule data adapter
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT PPS.PayPlanScheduleId, PPS.PaymentPlanId, ")
                .Append("       PPS.ExpectedDate, PPS.Amount, PPS.Reference ")
                .Append("FROM   faStuPaymentPlanSchedule PPS ")
            End With

            '   build select command
            Dim PaymentPlanSchedulesSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle PaymentPlanSchedules table
            Dim PaymentPlanSchedulesDataAdapter As New OleDbDataAdapter(PaymentPlanSchedulesSelectCommand)

            '   build insert, update and delete commands for PaymentPlanSchedules table
            Dim cb As New OleDbCommandBuilder(PaymentPlanSchedulesDataAdapter)

            '   build select query for the PaymentPlans data adapter
            sb = New StringBuilder
            With sb
                '   with subqueries
                .Append("SELECT ")
                .Append("         PP.PaymentPlanId, ")
                .Append("         PP.StuEnrollId, ")
                .Append("         PP.PayPlanDescrip, ")
                .Append("         PP.AcademicYearId, ")
                .Append("         PP.TotalAmountDue, ")
                .Append("         PP.Disbursements, ")
                .Append("         PP.PayPlanStartDate, ")
                .Append("         PP.PayPlanEndDate, ")
                .Append("         PP.ModUser, ")
                .Append("         PP.ModDate ")
                .Append("FROM     faStudentPaymentPlans PP ")
            End With

            '   build select command
            Dim PaymentPlansSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle PaymentPlans table
            Dim PaymentPlansDataAdapter As New OleDbDataAdapter(PaymentPlansSelectCommand)

            '   build insert, update and delete commands for PaymentPlans table
            Dim cb1 As New OleDbCommandBuilder(PaymentPlansDataAdapter)

            '   insert added rows in PaymentPlans table
            PaymentPlansDataAdapter.Update(ds.Tables("PaymentPlans").Select(Nothing, Nothing, DataViewRowState.Added))

            '   insert added rows in PaymentPlanSchedules table
            PaymentPlanSchedulesDataAdapter.Update(ds.Tables("PaymentPlanSchedules").Select(Nothing, Nothing, DataViewRowState.Added))

            '   delete rows in PaymentPlanSchedules table
            PaymentPlanSchedulesDataAdapter.Update(ds.Tables("PaymentPlanSchedules").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   delete rows in PaymentPlans table
            PaymentPlansDataAdapter.Update(ds.Tables("PaymentPlans").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   update rows in PaymentPlanSchedules table
            PaymentPlanSchedulesDataAdapter.Update(ds.Tables("PaymentPlanSchedules").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   update rows in PaymentPlans table
            PaymentPlansDataAdapter.Update(ds.Tables("PaymentPlans").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   everything went fine - commit tranPPction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback tranPPction
            groupTrans.Rollback()

            '   return error mesPPge
            Return DALExceptions.BuildErrorMessage(ex)

        Catch ex As Exception
            '   something went wrong
            '   rollback tranPPction
            groupTrans.Rollback()

            '   return error mesPPge
            Return ex.Message

        Finally

            '   close connection
            connection.Close()
        End Try

    End Function
    Public Function GetLateFeesToBePostedDS(ByVal cutoffDate As Date, ByVal gracePeriod As Integer, ByVal user As String, ByVal campusid As String, Optional ByVal effectiveDate As String = "1/1/1900") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As StringBuilder = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       T.StuEnrollId, ")
            .Append("       T.PayPlanScheduleId, ")
            .Append("        LastName + ' ' + FirstName  As StudentName, ")
            'include SSN or student identifier
            If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
                .Append("   Coalesce(Case Len(SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') ")
            ElseIf CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToLower = "studentid" Then
                .Append("  Coalesce(StudentNumber, ' ') ")
            Else
                .Append("   Coalesce(" + CType(MyAdvAppSettings.AppSettings("StudentIdentifier"), String).ToString + ", ' ') ")
            End If


            .Append("       As StudentIdentifier, ")
            .Append("		Reference,	 ")
            .Append(" 		ExpectedDate,  ")
            .Append("		DateAdd(day, ? , ExpectedDate) as GraceDate,  ")
            .Append(" 		Amount,   ")
            .Append(" 		Balance   ")
            .Append(" from   ")
            .Append(" (   ")
            .Append(" 		SELECT    ")
            .Append("               PayPlanScheduleId, ")
            .Append(" 				StuEnrollId, ")
            .Append("				Reference, ")
            .Append(" 				ExpectedDate,    ")
            .Append(" 				Amount,    ")
            .Append("		        Amount - Coalesce((Select Sum(Amount) from saPmtDisbRel PDR, saTransactions T where PayPlanScheduleId=PPS.PayPlanScheduleId and PDR.TransactionId=T.TransactionId and T.Voided=0 and T.TransDate <= DateAdd(day, ?, ?) group by PDR.PayPlanScheduleId),0) as Balance  ")
            .Append(" 		 FROM   faStudentPaymentPlans SPP, faStuPaymentPlanSchedule PPS    ")
            .Append(" 		 WHERE     ")
            .Append(" 				SPP.PaymentPlanId=PPS.PaymentPlanId    ")
            .Append(" ) T , arStuEnrollments,arStudent ")
            .Append(" where   ")
            .Append(" 		Balance > 0   ")
            .Append(" and	DateAdd(day, ?, ExpectedDate) <= ? and T.stuenrollId=arStuEnrollments.stuEnrollId and ")
            .Append(" arStuEnrollments.StudentId=arStudent.StudentId and arStuEnrollments.Campusid= ? ")
            .Append(" and ExpectedDate >= ? ")
            .Append(" Order by T.StuEnrollId, ExpectedDate    ")
        End With
        '   add parameters values to the query

        '   Grace Period
        db.AddParameter("@GracePeriod", gracePeriod, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   Grace Period
        db.AddParameter("@GracePeriod", gracePeriod, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   CufoffDate
        db.AddParameter("@CutoffDate", cutoffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   Grace Period
        db.AddParameter("@GracePeriod", gracePeriod, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        '   CufoffDate
        db.AddParameter("@CutoffDate", cutoffDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   CampusId
        db.AddParameter("@CampusId", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Effective Date
        db.AddParameter("@EffectiveDate", effectiveDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function CheckIfDataExistsInPmtDisbRel(ByVal strStudentAwardId As String) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        Dim intDataExists As Integer = 0
        With sb
            .Append(" select Count(*) from saPmtDisbRel where AwardScheduleId in ")
            .Append(" (Select Distinct AwardScheduleId from faStudentAwardSchedule where StudentAwardId=?) ")
        End With
        db.AddParameter("@StudentAwardId", strStudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            intDataExists = db.RunParamSQLScalar(sb.ToString)
            If intDataExists >= 1 Then
                Return "Unable to delete the selected award and disbursement as payment has already been posted for the disbursement"
            Else
                Return ""
            End If
        Catch ex As System.Exception
            Return ex.Message
        End Try
    End Function
    'New Code Added By Vijay Ramteke On June 10, 2010
    Public Function GetAvailableAwardsForStudent_Sp_NoAid(ByVal stuEnrollId As String, ByVal CutoffDate As String) As DataSet

        '   connect to the database
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        ' Add StudentId to the parameter list
        db.AddParameter("@StuEnrollID", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        ''New Code Addded By Vijay Ramteke On June 09, 2010 For Mantis Id 18950
        db.AddParameter("@CutoffDate", CutoffDate, SqlDbType.DateTime, , ParameterDirection.Input)
        ''New Code Addded By Vijay Ramteke On June 09, 2010 For Mantis Id 18950
        '   return dataset
        Return db.RunParamSQLDataSet_SP("USP_SA_GetAvailableAwardsForStudent_NoAid")
    End Function
    'New Code Added By Vijay Ramteke On June 10, 2010

    '' Code Added by Kamalesh Ahuja on July 21 2010 as per discussion with Troy For Revenue Ratio Reports
    Public Function GetAvailableAwardsForStudent_Sp_NoAidNew(ByVal CutoffDate As String, ByVal CampusId As Guid) As DataSet

        '   connect to the database
        Dim db As New SQLDataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@CutoffDate", CutoffDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@CampusId", CampusId, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet_SP("USP_SA_GetAvailableAwardsForStudent_NoAidNew")
    End Function
    '' Code Added by Kamalesh Ahuja on July 21 2010 as per discussion with Troy For Revenue Ratio Reports
    Public Function GetAvailableAwardsForStudent_SpPP(ByVal CutoffDate As String, ByVal CampusId As Guid) As DataSet

        '   connect to the database
        Dim db As New SQLDataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@CutoffDate", CutoffDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@CampusId", CampusId, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        '   return dataset
        Return db.RunParamSQLDataSet_SP("USP_SA_GetAvailableAwardsForStudentPP")
    End Function
    '' Code Added by Kamalesh Ahuja on July 21 2010 as per discussion with Troy For Revenue Ratio Reports
    Public Function GetAvailableAwardsForStudent_SpNew(ByVal CutoffDate As String, ByVal CampusId As Guid) As DataSet

        '   connect to the database
        Dim db As New SQLDataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString


        ''New Code Addded By Vijay Ramteke On June 09, 2010 For Mantis Id 18950
        db.AddParameter("@CutoffDate", CutoffDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@CampusId", CampusId, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        ''New Code Addded By Vijay Ramteke On June 09, 2010 For Mantis Id 18950
        '   return dataset
        Return db.RunParamSQLDataSet_SP("USP_SA_GetAvailableAwardsForStudentNew")
    End Function

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

    Public Function HasPaymentPlanPostedPayments(ByVal paymentPlanId As Guid) As Boolean
        Dim db As New SQLDataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        db.AddParameter("@PaymentPlanId", paymentPlanId, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Dim ds As DataSet = db.RunParamSQLDataSet_SP("USP_SA_HasPaymentPlanPostedPayments")
        If (Not ds Is Nothing) Then
            If (ds.Tables.Count > 0) Then
                If ds.Tables.Item(0).Rows.Count > 0 Then
                    Return CType(Integer.Parse(ds.Tables.Item(0).Rows(0).Item("HasPostedPayment").ToString()), Boolean)
                End If
            End If
        End If
        Return False
    End Function

End Class