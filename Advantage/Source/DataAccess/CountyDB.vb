Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' CountyDB.vb
'
' CountyDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class CountyDB
    Public Function GetAllCounty() As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("select CT.CountyId,CT.CountyCode,CT.CountyDescrip ")
            .Append("FROM     adCounties CT,syStatuses ST ")
            .Append("WHERE    CT.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY CT.CountyDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllCountry() As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'Build the sql query
        With sb
            .Append("select CT.NationalityId as CountryId,CT.NationalityCode as CountryCode,CT.NationalityDescrip as CountryDescrip ")
            .Append("FROM     adNationalities CT,syStatuses ST ")
            .Append("WHERE    CT.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY CT.NationalityDescrip ")
        End With
        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllNationality() As DataSet
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'Build the sql query
        With sb
            .Append("select CT.NationalityId,CT.NationalityCode,CT.NationalityDescrip ")
            .Append("FROM     adNationalities CT,syStatuses ST ")
            .Append("WHERE    CT.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY CT.NationalityDescrip ")
        End With
        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllCitizenShip() As DataSet
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'Build the sql query
        With sb
            .Append("select CT.CitizenshipId,CT.CitizenshipCode,CT.CitizenshipDescrip ")
            .Append("FROM     adCitizenShips CT,syStatuses ST ")
            .Append("WHERE    CT.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY CT.CitizenshipDescrip ")
        End With
        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetDefaultCountry() As String

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        'With sb
        '    .Append("select Distinct Top 1 CT.CountryId ")
        '    .Append("FROM     adCountries CT where CT.IsDefault=1 ")
        'End With

        'Changed as per Troy's request
        ' the Country is read from web.config
        With sb
            .Append("select Distinct Top 1 CT.CountryId ")
            .Append("FROM     adCountries CT where CountryDescrip like '" & GetAdvAppSettings.AppSettings("DefaultCountry") & "%' ")
        End With

        '   return dataset
        Dim strCountryId As Guid = db.RunParamSQLScalar(sb.ToString)
        Return strCountryId.ToString


    End Function
#Region "Get AdvAppsetting for Manage Config entry"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
End Class
