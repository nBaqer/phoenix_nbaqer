' ===============================================================================
' ProgramsDB.vb
' DataAccess classes for Programs
' ===============================================================================
' Copyright (C) 2006,2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports FAME.AdvantageV1.Common.AR
Imports FAME.Advantage.Common

Namespace AR

#Region "Programs"
    Public Class ProgramsDB
        ''' <summary>
        ''' Get all Programs
        ''' </summary>
        ''' <param name="ShowActive"></param>
        ''' <param name="ShowInactive"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAll(ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean, Optional ByVal campusId As String = "") As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     P.ProgId as Id, " & vbCrLf)
            sb.Append("     P.ProgCode as Code, " & vbCrLf)

            ''Modified by saraswathi lakshmanan on May 20 2009
            ''the Shift id added to the program description

            sb.Append(" case when (select ShiftDescrip from arShifts where arShifts.shiftid=P.shiftid ) is null  then ")
            sb.Append(" P.ProgDescrip ")
            sb.Append(" else ")
            sb.Append(" P.ProgDescrip + ' (' + (select ShiftDescrip from arShifts where arShifts.shiftid=P.shiftid) + ')'  ")
            sb.Append(" end as Descrip,  ")


            '  sb.Append("     P.ProgDescrip as Descrip, " & vbCrLf)
            sb.Append("     (case when P.StatusId=(select StatusId from syStatuses where Status='Active') then 1 else 0 end) as Active, " + vbCrLf)
            sb.Append("     P.ModUser, " + vbCrLf)
            sb.Append("     P.ModDate " + vbCrLf)
            sb.Append("FROM arPrograms P where 1=1 " & vbCrLf)
            If ShowActive And Not ShowInactive Then
                sb.Append("and " & vbCrLf)
                sb.Append("     P.StatusId = (select StatusId from syStatuses where Status='Active') " & vbCrLf)
            ElseIf Not ShowActive And ShowInactive Then
                sb.Append("and " & vbCrLf)
                sb.Append("     P.StatusId = (select StatusId from syStatuses where Status='Inactive') " & vbCrLf)
            End If
            If campusId <> "" Then
                sb.Append("AND (P.CampGrpId IN(SELECT CampGrpId ")
                sb.Append("FROM syCmpGrpCmps ")
                sb.Append("WHERE CampusId = ? ")
                sb.Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                sb.Append("OR P.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append("     P.ProgDescrip asc ")
            If campusId <> "" Then
                db.AddParameter("cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieve a ProgramInfo object from the database given a ProgId
        ''' </summary>
        ''' <param name="ProgId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal ProgId As String) As ProgramInfo
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("select " + vbCrLf)
            sb.Append("     P.ProgId, " & vbCrLf)
            sb.Append("     P.ProgCode, " & vbCrLf)
            sb.Append("     (case when P.StatusId = (select StatusId from syStatuses where Status='Active') then 1 else 0 end) as Active, " & vbCrLf)
            sb.Append("     P.ProgDescrip, " & vbCrLf)
            'sb.Append("     P.ProgTypeId, " & vbCrLf)
            'sb.Append("     (select T.Description from arProgTypes T where T.ProgTypId=P.ProgTypeId) as ProgTypeDescrip, " & vbCrLf)
            sb.Append("     P.ModUser, " & vbCrLf)
            sb.Append("     P.ModDate, " & vbCrLf)
            sb.Append("     P.DegreeId, " & vbCrLf)
            sb.Append("     (select D.DegreeDescrip from arDegrees D where D.DegreeId=P.DegreeId) as DegreeDescrip, " & vbCrLf)
            'sb.Append("     P.Weeks, " & vbCrLf)
            'sb.Append("     P.Terms, " & vbCrLf)
            'sb.Append("     P.Hours, " & vbCrLf)
            'sb.Append("     P.Credits, " & vbCrLf)
            sb.Append("     P.CalendarTypeId, " & vbCrLf)
            sb.Append("     'CalendarTypeDescrip' as CalendarTypeDescrip " & vbCrLf)
            sb.Append("from " + vbCrLf)
            sb.Append("     arPrograms P " & vbCrLf)
            sb.Append("where " + vbCrLf)
            sb.AppendFormat("     ProgId = '{0}'", ProgId)

            '   Execute the query
            Try
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                Dim info As New ProgramInfo
                If dr.Read() Then
                    '   set properties with data from DataReader
                    info.ProgId = ProgId
                    info.IsInDB = True
                    If Not (dr("ProgId") Is System.DBNull.Value) Then info.ProgId = dr("ProgId").ToString()
                    If Not (dr("ProgCode") Is System.DBNull.Value) Then info.Code = dr("ProgCode")
                    If Not (dr("Active") Is System.DBNull.Value) Then info.Active = dr("Active")
                    If Not (dr("ProgDescrip") Is System.DBNull.Value) Then info.Descrip = dr("ProgDescrip")
                    'If Not (dr("ProgTypeId") Is System.DBNull.Value) Then info.ProgTypeId = dr("ProgTypeId").ToString()
                    'If Not (dr("ProgTypeDescrip") Is System.DBNull.Value) Then info.ProgTypeDescrip = dr("ProgTypeDescrip")
                    If Not (dr("ModUser") Is System.DBNull.Value) Then info.ModUser = dr("ModUser").ToString()
                    If Not (dr("ModDate") Is System.DBNull.Value) Then info.ModDate = dr("ModDate")
                    If Not (dr("DegreeId") Is System.DBNull.Value) Then info.DegreeId = dr("DegreeId").ToString()
                    If Not (dr("DegreeDescrip") Is System.DBNull.Value) Then info.DegreeDescrip = dr("DegreeDescrip")
                    'If Not (dr("Weeks") Is System.DBNull.Value) Then info.Weeks = dr("Weeks")
                    'If Not (dr("Terms") Is System.DBNull.Value) Then info.Terms = dr("Terms")
                    'If Not (dr("Hours") Is System.DBNull.Value) Then info.Hours = dr("Hours")
                    'If Not (dr("Credits") Is System.DBNull.Value) Then info.Credits = dr("Credits")
                    If Not (dr("CalendarTypeId") Is System.DBNull.Value) Then info.CalendarTypeId = dr("CalendarTypeId").ToString()
                    If Not (dr("CalendarTypeDescrip") Is System.DBNull.Value) Then info.CalendarTypeDescrip = dr("CalendarTypeDescrip")
                End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                Return info
            Catch ex As System.Exception
            End Try
            Return Nothing
        End Function

        ''' <summary>
        ''' Update a ProgramInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Update(ByVal info As ProgramInfo, ByVal user As String) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("update " & vbCrLf)
                sb.Append("     arPrograms " + vbCrLf)
                sb.Append("set " & vbCrLf)
                sb.Append("     ProgCode = ?, " & vbCrLf)
                If info.Active Then
                    sb.Append("     StatusId = (select StatusId from syStatuses where Status='Active'), " + vbCrLf)
                Else
                    sb.Append("     StatusId = (select StatusId from syStatuses where Status='Inactive'), " + vbCrLf)
                End If
                sb.Append("     ProgDescrip = ?, " & vbCrLf)
                'sb.Append("     ProgTypeId = ?, " & vbCrLf)
                sb.Append("     ModUser = ?, " + vbCrLf)
                sb.Append("     ModDate = ?, " & vbCrLf)
                sb.Append("     DegreeId = ?, " & vbCrLf)
                'sb.Append("     Weeks = ?, " & vbCrLf)
                'sb.Append("     Terms = ?, " & vbCrLf)
                'sb.Append("     Hours = ?, " & vbCrLf)
                'sb.Append("     Credits = ?, " & vbCrLf)
                sb.Append("     CalendarTypeId = ? " & vbCrLf)
                sb.Append("where " + vbCrLf)
                sb.Append("     ProgId = ? " + vbCrLf)

                '   add parameters values to the query            
                db.AddParameter("@Code", info.Code, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
                db.AddParameter("@Descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'If info.ProgTypeId Is Nothing Or info.ProgTypeId = Guid.Empty.ToString Then
                'db.AddParameter("@ProgTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'Else
                'db.AddParameter("@ProgTypeId", info.ProgTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'End If
                If info.ModUser Is Nothing Or info.ModUser = Guid.Empty.ToString Then
                    db.AddParameter("@ModUser", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModUser", info.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If info.DegreeId Is Nothing Or info.DegreeId = Guid.Empty.ToString Then
                    db.AddParameter("@DegreeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@DegreeId", info.DegreeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.CalendarTypeId Is Nothing Or info.CalendarTypeId = Guid.Empty.ToString Then
                    db.AddParameter("@CalendarTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CalendarTypeId", info.CalendarTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                db.AddParameter("@ProgId", info.ProgId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                ' run the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return ""
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function

        ''' <summary>
        ''' Persists a new ProgramInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As ProgramInfo, ByVal user As String) As String
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query                
                Dim sb As New StringBuilder
                sb.Append("DECLARE @active as uniqueidentifier " + vbCrLf)
                If info.Active Then
                    sb.Append("SELECT @active=StatusId from syStatuses where Status='Active' " + vbCrLf)
                Else
                    sb.Append("SELECT @active=StatusId from syStatuses where Status='Inactive' " + vbCrLf)
                End If
                sb.Append("INSERT arPrograms (ProgId, ProgCode, StatusId, ProgDescrip, ModUser, ModDate, " + vbCrLf)
                sb.Append("                   DegreeId, CalendarTypeId) ")
                sb.Append("VALUES (?,?,@active,?,?,?,?,?) ")

                '   add parameters values to the query
                db.AddParameter("@ProgId", info.ProgId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Code", info.Code, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
                db.AddParameter("@Descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'If info.ProgTypeId Is Nothing Or info.ProgTypeId = Guid.Empty.ToString Then
                'db.AddParameter("@ProgTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'Else
                'db.AddParameter("@ProgTypeId", info.ProgTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'End If
                If info.ModUser Is Nothing Or info.ModUser = Guid.Empty.ToString Then
                    db.AddParameter("@ModUser", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModUser", info.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If info.DegreeId Is Nothing Or info.DegreeId = Guid.Empty.ToString Then
                    db.AddParameter("@DegreeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@DegreeId", info.DegreeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.CalendarTypeId Is Nothing Or info.CalendarTypeId = Guid.Empty.ToString Then
                    db.AddParameter("@CalendarTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CalendarTypeId", info.CalendarTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                Return "" '   return without errors
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function

        ''' <summary>
        ''' Delete from program from arPrograms given a ProgId
        ''' </summary>
        ''' <param name="ProgId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal ProgId As String, ByVal modDate As DateTime) As String
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                ' check if there are already user tasks with this taskid
                Dim sql As String = String.Format("SELECT COUNT(*) from arPrgVersions WHERE ProgId = '{0}'", ProgId)
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                If rowCount > 0 Then
                    db.CloseConnection()
                    Return "Program cannot be deleted because it is used by one or more program versions."
                Else
                    ' it is safe to delete the results
                    sql = "DELETE arPrograms WHERE ProgId = ? AND ModDate = ? " + vbCrLf
                    sql += "SELECT COUNT(*) from arPrograms where ProgId = ? " + vbCrLf
                    db.AddParameter("@ProgId", ProgId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@ProgId", ProgId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    rowCount = db.RunParamSQLScalar(sql)
                    '   If the row was not deleted then there was a concurrency problem
                    If rowCount = 0 Then
                        Return ""   ' return without errors
                    Else
                        Return DALExceptions.BuildConcurrencyExceptionMessage()
                    End If
                End If
                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function

#Region "Misc"
        ''' <summary>
        ''' Retrieves all degrees for the given campus group
        ''' </summary>
        ''' <param name="CampGrpId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetDegrees(ByVal CampGrpId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     D.DegreeId as ID, " & vbCrLf)
            sb.Append("     D.DegreeCode as Code, " & vbCrLf)
            sb.Append("     D.DegreeDescrip as Descrip, " & vbCrLf)
            sb.Append("     '(' + D.DegreeCode + ') ' + D.DegreeDescrip as Descrip2" + vbCrLf)
            sb.Append("FROM arDegrees D " & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            sb.Append("     D.StatusId = (select StatusId from syStatuses where Status='Active') " & vbCrLf)
            'sb.Append("     and D.CampGrpId = '{0}' " + vbCrLf)            
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append("     D.DegreeDescrip asc ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieves all the Academic Calendars from syAcademicCalendars table
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAcademicCalendars() As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     A.ACId as ID, " & vbCrLf)
            sb.Append("     A.ACDescrip as Descrip " & vbCrLf)
            sb.Append("FROM syAcademicCalendars A " & vbCrLf)
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append("     A.ACId asc ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
#End Region
    End Class
#End Region

#Region "Program Versions"
    Public Class PrgVersionsDB
        ''' <summary>
        ''' Returns the program versions for the given ProgId
        ''' This method is meant to be a quick retrieval to be used to fill up things like a dropdownlist.
        ''' If you need more detailed info on program version, use GetProgramVersionsEx
        ''' </summary>
        ''' <param name="ProgId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAll(ByVal ProgId As String, ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean, Optional ByVal campusId As String = "") As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("select " + vbCrLf)
            sb.Append("     PV.PrgVerId as Id, " & vbCrLf)
            sb.Append("     PV.PrgVerCode as Code, " & vbCrLf)
            sb.Append("     PV.PrgVerDescrip as Descrip, " & vbCrLf)
            sb.Append(" case when (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=PV.ProgId) is null  then ")
            sb.Append(" PV.PrgVerDescrip ")
            sb.Append(" else ")
            sb.Append(" PV.PrgVerDescrip + ' (' + (select ShiftDescrip from arShifts,arPrograms where arShifts.shiftid=arPrograms.shiftid and arPrograms.Progid=PV.ProgId) + ')'  ")
            sb.Append(" end as PrgVerShiftDescrip,  ")
            sb.Append("     (case when PV.StatusId=(select StatusId from syStatuses where Status='Active') then 1 else 0 end) as Active, " + vbCrLf)
            sb.Append("     PV.ModUser, " + vbCrLf)
            sb.Append("     PV.ModDate " + vbCrLf)
            sb.Append("from " + vbCrLf)
            sb.Append("     arPrgVersions PV where 1=1 " & vbCrLf)

            ' format and add the where clause

            If ProgId IsNot Nothing AndAlso ProgId <> "" Then
                sb.AppendFormat(" and    PV.ProgId='{0}' {1}", ProgId, vbCrLf)
            End If
            If ShowActive And Not ShowInactive Then
                sb.Append("  and   PV.StatusId = (select StatusId from syStatuses where Status='Active') " & vbCrLf)
            ElseIf Not ShowActive And ShowInactive Then

                sb.Append(" and PV.StatusId = (select StatusId from syStatuses where Status='Inactive') " & vbCrLf)
            End If
            If campusId <> "" Then
                sb.Append("AND (PV.CampGrpId IN(SELECT CampGrpId ")
                sb.Append("FROM syCmpGrpCmps ")
                sb.Append("WHERE CampusId = ? ")
                sb.Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                sb.Append("OR PV.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If

            sb.Append("order by " & vbCrLf)
            sb.Append("     PV.PrgVerDescrip asc ")
            If campusId <> "" Then
                db.AddParameter("cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Returns a sql string to retrieve detailed information about the supplied PrgVerId
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function GetPrgVersionsSql(ByVal PrgVerId As String) As String
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("select " + vbCrLf)
            sb.Append("     PV.PrgVerId, " & vbCrLf)
            sb.Append("     PV.ProgId, " & vbCrLf)
            sb.Append("     PV.PrgVerCode, " & vbCrLf)
            sb.Append("     PV.PrgVerDescrip, " & vbCrLf)
            sb.Append("     (case when PV.StatusId=(select StatusId from syStatuses where Status='Active') then 1 else 0 end) as Active, " + vbCrLf)
            sb.Append("     PV.CampGrpId, " + vbCrLf)
            sb.Append("     (select CG.CampGrpDescrip from syCampGrps CG where CG.CampGrpId=PV.CampGrpId) as CampGrpDescrip, " + vbCrLf)
            sb.Append("     PV.PrgGrpId, " + vbCrLf)
            sb.Append("     (select PG.PrgGrpDescrip from arPrgGrp PG where PG.PrgGrpId=PV.PrgGrpId) as PrgGrpDescrip, " + vbCrLf)
            sb.Append("     --DegreeId, " + vbCrLf)
            sb.Append("     --(select D.DegreeDescrip from arDegrees D where PV.DegreeId=D.DegreeId) as DegreeDescrip, " + vbCrLf)
            sb.Append("     PV.SAPId, " + vbCrLf)
            sb.Append("     (select SAPDescrip from arSAP S where S.SAPId=PV.SAPId) as SAPDescrip, " + vbCrLf)
            'sb.Append("     PV.ThGrdScaleId, " + vbCrLf)
            'sb.Append("     (select G.Descrip from arGradeScales G where G.GrdScaleId=PV.ThGrdScaleId) as GrdScaleDescrip, " + vbCrLf)
            sb.Append("     PV.TestingModelId, " + vbCrLf)
            sb.Append("     PV.Weeks, " + vbCrLf)
            sb.Append("     PV.Terms, " + vbCrLf)
            sb.Append("     PV.Hours, " + vbCrLf)
            sb.Append("     PV.Credits, " + vbCrLf)
            sb.Append("     PV.LTHalfTime, " + vbCrLf)
            sb.Append("     PV.HalfTime, " + vbCrLf)
            sb.Append("     PV.ThreeQuartTime, " + vbCrLf)
            sb.Append("     PV.FullTime, " + vbCrLf)
            sb.Append("     PV.DeptId, " + vbCrLf)
            sb.Append("     (select DeptDescrip from arDepartments D where D.DeptId=PV.DeptId) as DeptDescrip, " + vbCrLf)
            sb.Append("     PV.GrdSystemId, " + vbCrLf)
            sb.Append("     (select G.Descrip from arGradeSystems G where G.GrdSystemId=PV.GrdSystemId) as GrdSystemDescrip, " + vbCrLf)
            sb.Append("     PV.[Weighted GPA], " + vbCrLf)
            sb.Append("     PV.BillingMethodId, " + vbCrLf)
            sb.Append("     (select B.BillingMethodDescrip from saBillingMethods B where B.BillingMethodId=PV.BillingMethodId) as BillingMethodDescrip, " + vbCrLf)
            sb.Append("     PV.TuitionEarningId, " + vbCrLf)
            sb.Append("     (select T.TuitionEarningsDescrip from saTuitionEarnings T where T.TuitionEarningId=PV.TuitionEarningId) as TuitionEarningsDescrip, " + vbCrLf)
            sb.Append("     PV.ModUser, " + vbCrLf)
            sb.Append("     PV.ModDate, " + vbCrLf)
            sb.Append("     PV.AttendanceLevel, " + vbCrLf)
            sb.Append("     PV.ProgTypId, " + vbCrLf)
            sb.Append("     (select PT.Description from arProgTypes PT where PT.ProgTypId=PV.ProgTypId) as ProgTypeDescrip, " + vbCrLf)
            sb.Append("     PV.IsContinuingEd, " + vbCrLf)
            sb.Append("     PV.UnitTypeId, " + vbCrLf)
            sb.Append("     --(select * from ), " + vbCrLf)
            sb.Append("     PV.TrackTardies, " + vbCrLf)
            sb.Append("     PV.TardiesMakingAbsence, " + vbCrLf)
            sb.Append("     PV.SchedMethodId, " + vbCrLf)
            sb.Append("     (select S.Descrip from sySchedulingMethods S where S.SchedMethodId=PV.SchedMethodId) as SchedMethodDescrip, " + vbCrLf)
            sb.Append("     PV.UseTimeClock " + vbCrLf)
            sb.Append("from " + vbCrLf)
            sb.Append("     arPrgVersions PV " & vbCrLf)
            sb.Append("where " + vbCrLf)
            sb.AppendFormat("       PV.PrgVerId='{0}' {1}", PrgVerId, vbCrLf)
            sb.Append("order by " & vbCrLf)
            sb.Append("     PV.PrgVerDescrip asc ")

            Return sb.ToString()
        End Function

        ''' <summary>
        ''' Loads a PrgVersionInfo object from the database
        ''' </summary>
        ''' <param name="PrgVerId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal PrgVerId As String) As PrgVersionInfo
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sql As String = GetPrgVersionsSql(PrgVerId)
            Dim info As New PrgVersionInfo

            '   Execute the query
            Try
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sql)
                If dr.Read() Then
                    '   set properties with data from DataReader
                    info.IsInDB = True
                    If Not (dr("PrgVerId") Is System.DBNull.Value) Then info.PrgVerId = dr("PrgVerId").ToString()
                    If Not (dr("ProgId") Is System.DBNull.Value) Then info.ProgId = dr("ProgId").ToString()
                    If Not (dr("PrgVerCode") Is System.DBNull.Value) Then info.PrgVerCode = dr("PrgVerCode").ToString()
                    If Not (dr("PrgVerDescrip") Is System.DBNull.Value) Then info.PrgVerDescrip = dr("PrgVerDescrip").ToString()
                    If Not (dr("Active") Is System.DBNull.Value) Then info.Active = dr("Active")
                    If Not (dr("CampGrpId") Is System.DBNull.Value) Then info.CampGrpId = dr("CampGrpId").ToString()
                    If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then info.CampGrpDescrip = dr("CampGrpDescrip").ToString()
                    If Not (dr("PrgGrpId") Is System.DBNull.Value) Then info.PrgGrpId = dr("PrgGrpId").ToString()
                    If Not (dr("PrgGrpDescrip") Is System.DBNull.Value) Then info.PrgGrpDescrip = dr("PrgGrpDescrip").ToString()
                    If Not (dr("SAPId") Is System.DBNull.Value) Then info.SAPId = dr("SAPId").ToString()
                    If Not (dr("SAPDescrip") Is System.DBNull.Value) Then info.SAPDescrip = dr("SAPDescrip").ToString()
                    'If Not (dr("ThGrdScaleId") Is System.DBNull.Value) Then info.GrdScaleId = dr("ThGrdScaleId").ToString()
                    'If Not (dr("GrdScaleDescrip") Is System.DBNull.Value) Then info.GrdScaleDescrip = dr("GrdScaleDescrip").ToString()
                    If Not (dr("TestingModelId") Is System.DBNull.Value) Then info.TestingModelId = dr("TestingModelId").ToString()
                    If Not (dr("Weeks") Is System.DBNull.Value) Then info.Weeks = dr("Weeks")
                    If Not (dr("Terms") Is System.DBNull.Value) Then info.Terms = dr("Terms")
                    If Not (dr("Hours") Is System.DBNull.Value) Then info.Hours = dr("Hours")
                    If Not (dr("Credits") Is System.DBNull.Value) Then info.Credits = dr("Credits")
                    If Not (dr("LTHalfTime") Is System.DBNull.Value) Then info.LTHalfTime = dr("LTHalfTime")
                    If Not (dr("HalfTime") Is System.DBNull.Value) Then info.HalfTime = dr("HalfTime")
                    If Not (dr("ThreeQuartTime") Is System.DBNull.Value) Then info.ThreeQuartTime = dr("ThreeQuartTime")
                    If Not (dr("FullTime") Is System.DBNull.Value) Then info.FullTime = dr("FullTime")
                    If Not (dr("DeptId") Is System.DBNull.Value) Then info.DeptId = dr("DeptId").ToString()
                    If Not (dr("DeptDescrip") Is System.DBNull.Value) Then info.DeptDescrip = dr("DeptDescrip").ToString()
                    If Not (dr("GrdSystemId") Is System.DBNull.Value) Then info.GrdSystemId = dr("GrdSystemId").ToString()
                    If Not (dr("GrdSystemDescrip") Is System.DBNull.Value) Then info.GrdSystemDescrip = dr("GrdSystemDescrip").ToString()
                    If Not (dr("Weighted GPA") Is System.DBNull.Value) Then info.WeightedGPA = dr("Weighted GPA")
                    If Not (dr("BillingMethodId") Is System.DBNull.Value) Then info.BillingMethodId = dr("BillingMethodId").ToString()
                    If Not (dr("BillingMethodDescrip") Is System.DBNull.Value) Then info.BillingMethodDescrip = dr("BillingMethodDescrip").ToString()
                    If Not (dr("TuitionEarningId") Is System.DBNull.Value) Then info.TuitionEarningId = dr("TuitionEarningId").ToString()
                    If Not (dr("TuitionEarningsDescrip") Is System.DBNull.Value) Then info.TuitionEarningsDescrip = dr("TuitionEarningsDescrip").ToString()
                    If Not (dr("ModUser") Is System.DBNull.Value) Then info.ModUser = dr("ModUser").ToString()
                    If Not (dr("ModDate") Is System.DBNull.Value) Then info.ModDate = dr("ModDate")
                    If Not (dr("AttendanceLevel") Is System.DBNull.Value) Then info.AttendanceLevel = dr("AttendanceLevel")
                    If Not (dr("ProgTypId") Is System.DBNull.Value) Then info.ProgTypeId = dr("ProgTypId").ToString()
                    If Not (dr("ProgTypeDescrip") Is System.DBNull.Value) Then info.ProgTypeDescrip = dr("ProgTypeDescrip").ToString()
                    If Not (dr("IsContinuingEd") Is System.DBNull.Value) Then info.IsContinuingEd = dr("IsContinuingEd")
                    If Not (dr("UnitTypeId") Is System.DBNull.Value) Then info.UnitTypeId = dr("UnitTypeId").ToString()
                    If Not (dr("TrackTardies") Is System.DBNull.Value) Then info.TrackTardies = dr("TrackTardies")
                    If Not (dr("TardiesMakingAbsence") Is System.DBNull.Value) Then info.TardiesMakingAbsence = dr("TardiesMakingAbsence")
                    If Not (dr("SchedMethodId") Is System.DBNull.Value) Then info.SchedMethodId = dr("SchedMethodId")
                    If Not (dr("SchedMethodDescrip") Is System.DBNull.Value) Then info.SchedMethodDescrip = dr("SchedMethodDescrip").ToString()
                    If Not (dr("UseTimeClock") Is System.DBNull.Value) Then info.UseTimeClock = dr("UseTimeClock")
                End If
                If Not dr.IsClosed Then dr.Close()
            Catch ex As System.Exception

            End Try
            Return info
        End Function

        ''' <summary>
        ''' Updates a PrgVersionInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Update(ByVal info As PrgVersionInfo, ByVal user As String) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("update " & vbCrLf)
                sb.Append("     arPrgVersions " + vbCrLf)
                sb.Append("set " & vbCrLf)
                sb.Append("     ProgId = ?, " + vbCrLf)
                sb.Append("     PrgVerCode = ?, " & vbCrLf)
                If info.Active Then
                    sb.Append("     StatusId = (select StatusId from syStatuses where Status='Active'), " + vbCrLf)
                Else
                    sb.Append("     StatusId = (select StatusId from syStatuses where Status='Inactive'), " + vbCrLf)
                End If
                sb.Append("     CampGrpId = ?, " + vbCrLf)
                sb.Append("     PrgGrpId = ?, " + vbCrLf)
                ' DegreeId ?
                sb.Append("     PrgVerDescrip = ?, " & vbCrLf)
                sb.Append("     SAPId = ?, " & vbCrLf)
                sb.Append("     TestingModelId = ?, " & vbCrLf)
                sb.Append("     Weeks = ?, " & vbCrLf)
                sb.Append("     Terms = ?, " & vbCrLf)
                sb.Append("     Hours = ?, " & vbCrLf)
                sb.Append("     Credits = ?, " & vbCrLf)
                ' LTHalfTime, HalfTime, ThreeQuartTime, FullTime, 
                sb.Append("     DeptId = ?, " & vbCrLf)
                sb.Append("     GrdSystemId = ?, " & vbCrLf)
                ' Weighted GPA
                sb.Append("     BillingMethodId = ?, " & vbCrLf)
                sb.Append("     TuitionEarningId = ?, " & vbCrLf)
                sb.Append("     ModUser = ?, " + vbCrLf)
                sb.Append("     ModDate = ?, " & vbCrLf)
                ' AttendanceLevel, CustomAttendance
                sb.Append("     ProgTypId = ?, " & vbCrLf)
                ' IsContinuingEd
                sb.Append("     UnitTypeId = ?, " & vbCrLf)
                ' TrackTardies, TardiesMakingAbsense
                sb.Append("     SchedMethodId = ?, " & vbCrLf)
                sb.Append("     UseTimeClock = ? " + vbCrLf)
                sb.Append("where " + vbCrLf)
                sb.Append("     PrgVerId = ? " + vbCrLf)

                '   add parameters values to the query            
                If info.ProgId Is Nothing Or info.ProgId = Guid.Empty.ToString Then
                    db.AddParameter("@ProgId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@ProgId", info.ProgId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                db.AddParameter("@PrgVerCode", info.PrgVerCode, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
                If info.CampGrpId Is Nothing Or info.CampGrpId = Guid.Empty.ToString Then
                    db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@CampGrpId", info.CampGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                If info.PrgGrpId Is Nothing Or info.PrgGrpId = Guid.Empty.ToString Then
                    db.AddParameter("@PrgGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@PrgGrpId", info.PrgGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                ' DegreeId
                db.AddParameter("@PrgVerDescrip", info.PrgVerDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.SAPId Is Nothing Or info.SAPId = Guid.Empty.ToString Then
                    db.AddParameter("@SAPId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@SAPId", info.SAPId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.TestingModelId Is Nothing Or info.TestingModelId = "" Then
                    db.AddParameter("@TestingModelId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@TestingModelId", info.TestingModelId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@Weeks", info.Weeks, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@Terms", info.Terms, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@Hours", info.Hours, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@Credits", info.Credits, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                ' LTHalfTime, HalfTime, ThreeQuartTime, FullTime, 
                If info.DeptId Is Nothing Or info.DeptId = Guid.Empty.ToString Then
                    db.AddParameter("@DeptId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@DeptId", info.DeptId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.GrdSystemId Is Nothing Or info.GrdSystemId = Guid.Empty.ToString Then
                    db.AddParameter("@GrdSystemId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@GrdSystemId", info.GrdSystemId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                ' Weighted GPA
                If info.BillingMethodId Is Nothing Or info.BillingMethodId = Guid.Empty.ToString Then
                    db.AddParameter("@BillingMethodId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@BillingMethodId", info.BillingMethodId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@TuitionEarningId", info.TuitionEarningId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.ModUser Is Nothing Or info.ModUser = Guid.Empty.ToString Then
                    db.AddParameter("@ModUser", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModUser", info.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                ' AttendanceLevel, CustomAttendance
                If info.ProgTypeId Is Nothing Or info.ProgTypeId = Guid.Empty.ToString Then
                    db.AddParameter("@ProgTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ProgTypeId", info.ProgTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                ' IsContinuingEd
                If info.UnitTypeId Is Nothing Then
                    db.AddParameter("@UnitTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@UnitTypeId", info.UnitTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                ' TrackTardies, TardiesMakingAbsense
                db.AddParameter("@SchedMethodId", info.SchedMethodId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@UseTimeClock", info.UseTimeClock, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                db.AddParameter("@PrgVerId", info.PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                ' run the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return ""
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function

        ''' <summary>
        ''' Persists a new program version to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As PrgVersionInfo, ByVal user As String) As String
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query                
                Dim sb As New StringBuilder
                sb.Append("DECLARE @active as uniqueidentifier " + vbCrLf)
                If info.Active Then
                    sb.Append("SELECT @active=StatusId from syStatuses where Status='Active' " + vbCrLf)
                Else
                    sb.Append("SELECT @active=StatusId from syStatuses where Status='Inactive' " + vbCrLf)
                End If
                sb.Append("INSERT arPrgVersions (PrgVerId, ProgId, PrgVerCode, StatusId, " + vbCrLf)
                sb.Append("                     CampGrpId, PrgGrpId, PrgVerDescrip, SAPId, " + vbCrLf)
                sb.Append("                     TestingModelId, " + vbCrLf)
                sb.Append("                     Weeks, Terms, Hours, Credits, " + vbCrLf)
                sb.Append("                     DeptId, GrdSystemId, BillingMethodId, TuitionEarningId, " + vbCrLf)
                sb.Append("                     ModUser, ModDate, AttendanceLevel, CustomAttendance, " + vbCrLf)
                sb.Append("                     ProgTypId, IsContinuingEd, FullTime, " + vbCrLf)
                sb.Append("                     UnitTypeId, TrackTardies, SchedMethodId, UseTimeClock) " + vbCrLf)
                sb.Append("VALUES ( " + vbCrLf)
                sb.AppendFormat("'{0}','{1}','{2}',@active,{3}", info.PrgVerId, info.ProgId, info.PrgVerCode, vbCrLf)
                sb.AppendFormat("'{0}','{1}','{2}','{3}',{4}", info.CampGrpId, info.PrgGrpId, info.PrgVerDescrip, info.SAPId, vbCrLf)
                sb.Append("?," + vbCrLf) ' TestingModelId
                sb.AppendFormat("'{0}','{1}','{2}','{3}',{4}", info.Weeks, info.Terms, info.Hours, info.Credits, vbCrLf)
                sb.AppendFormat("'{0}','{1}','{2}','{3}',{4}", info.DeptId, info.GrdSystemId, info.BillingMethodId, info.TuitionEarningId, vbCrLf)
                sb.AppendFormat("'{0}','{1}',{2},{3},{4}", info.ModUser, Date.Now, "null", "null", vbCrLf)
                sb.AppendFormat("'{0}',{1},0,{2}", info.ProgTypeId, "null", vbCrLf)
                sb.AppendFormat("'{0}',?,{1},?)", info.UnitTypeId, info.SchedMethodId)

                '   add parameters values to the query
                If info.TestingModelId Is Nothing Or info.TestingModelId = "" Then
                    db.AddParameter("@TestingModelId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@TestingModelId", info.TestingModelId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@TrackTardies", info.TrackTardies, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@UseTimeClock", info.UseTimeClock, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                Return "" '   return without errors
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function

        ''' <summary>
        ''' Deletes a program versions from the database
        ''' </summary>
        ''' <param name="PrgVerId"></param>
        ''' <param name="modDate"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal PrgVerId As String, ByVal modDate As DateTime) As String
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                Dim sb As New StringBuilder()
                sb.Append("DELETE saProgramVersionFees WHERE PrgVerId = ?; " + vbCrLf)
                sb.Append("DELETE arProgScheduleDetails FROM arProgSchedules S WHERE S.ScheduleId=arProgScheduleDetails.ScheduleId AND S.PrgVerId = ?; " + vbCrLf)
                sb.Append("DELETE arProgSchedules WHERE PrgVerId = ?; " + vbCrLf)
                sb.Append("DELETE arPrgVersions WHERE PrgVerId = ? AND ModDate = ? " + vbCrLf)
                sb.Append("SELECT COUNT(*) from arPrgVersions where PrgVerId = ? " + vbCrLf)
                db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PrgVerId2", PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PrgVerId3", PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PrgVerId4", PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@PrgVerId3", PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString())
                '   If the row was not deleted then there was a concurrency problem
                If rowCount = 0 Then Return "" ' return without errors
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function

        ''' <summary>
        ''' Returns all active program groups
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetProgramGroups() As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append("     P.PrgGrpId as ID, " & vbCrLf)
            sb.Append("     P.PrgGrpDescrip as Descrip " & vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arPrgGrp P " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     P.StatusId = (select StatusId from syStatuses where Status='Active') " + vbCrLf)
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append("     P.PrgGrpDescrip asc ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        Public Shared Function IsUsingTimeClock(ByVal PrgVerId As String) As Boolean
            If PrgVerId Is Nothing Or PrgVerId = "" Then Return False ' do some argument checking

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append("     ISNULL(P.UseTimeClock,0) " & vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arPrgVersions P " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.AppendFormat("     P.PrgVerid = '{0}' ", PrgVerId)
            Return CType(db.RunParamSQLScalar(sb.ToString), Boolean)
        End Function
    End Class
#End Region

#Region "Fees"
    Public Class PrgVersionFeesDB
        ''' <summary>
        ''' Returns the program version fees for the supplied PrgVerId.  This method is normally used to popuplate
        ''' the LHS datalist on a maintenance page.
        ''' </summary>
        ''' <param name="PrgVerId"></param>
        ''' <param name="ShowActive"></param>
        ''' <param name="ShowInactive"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAll(ByVal PrgVerId As String, ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     PVF.PrgVerFeeId as Id, " + vbCrLf)
            sb.Append("     PVF.TransCodeId, " + vbCrLf)
            sb.Append("     TC.TransCodeCode as Code, " + vbCrLf)
            sb.Append("     TC.TransCodeDescrip as Descrip, " + vbCrLf)
            sb.Append("     (case when PVF.StatusId=(select StatusId from syStatuses where Status='Active') then 1 else 0 end) as Active, " + vbCrLf)
            sb.Append("     PVF.ModUser, " + vbCrLf)
            sb.Append("     PVF.ModDate " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     saProgramVersionFees PVF, saTransCodes TC " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     PVF.TransCodeId = TC.TransCodeId " + vbCrLf)
            If ShowActive And Not ShowInactive Then
                sb.Append("     and PVF.StatusId = (select StatusId from syStatuses where Status='Active') " & vbCrLf)
            ElseIf Not ShowActive And ShowInactive Then
                sb.Append("     and PVF.StatusId = (select StatusId from syStatuses where Status='Inactive') " & vbCrLf)
            End If
            If PrgVerId IsNot Nothing AndAlso PrgVerId <> "" Then
                sb.AppendFormat("     and PVF.PrgVerId = '{0}' {1}", PrgVerId, vbCrLf)
            End If
            sb.Append("ORDER BY " + vbCrLf)
            sb.Append("     TC.TransCodeDescrip ")
            Return db.RunSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Loads a PrgVersionFeeInfo object from the database
        ''' </summary>
        ''' <param name="PrgVerFeeId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal PrgVerFeeId As String) As PrgVersionFeeInfo
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     PVF.PrgVerFeeId, " + vbCrLf)
            sb.Append("     (case when PVF.StatusId=(select StatusId from syStatuses where Status='Active') then 1 else 0 end) as Active, " + vbCrLf)
            sb.Append("     PVF.PrgVerId, " + vbCrLf)
            sb.Append("     (select PrgVerDescrip from arPrgVersions where PrgVerId=PVF.PrgVerId) PrgVerDescrip, " + vbCrLf)
            sb.Append("     (select P.ProgId from arPrograms P, arPrgVersions PV where P.ProgId=PV.ProgId and PV.PrgVerId=PVF.PrgVerId) as ProgId, " + vbCrLf)
            sb.Append("     (select P.ProgDescrip from arPrograms P, arPrgVersions PV where P.ProgId=PV.ProgId and PV.PrgVerId=PVF.PrgVerId) as ProgDescrip, " + vbCrLf)
            sb.Append("     PVF.TransCodeId, " + vbCrLf)
            sb.Append("     (select TransCodeDescrip from saTransCodes where TransCodeId=PVF.TransCodeId) TransCodeDescrip, " + vbCrLf)
            sb.Append("     PVF.TuitionCategoryId, " + vbCrLf)
            sb.Append("     (select TuitionCategoryDescrip from saTuitionCategories where TuitionCategoryId=PVF.TuitionCategoryId) TuitionCategoryDescrip, " + vbCrLf)
            sb.Append("     PVF.Amount, " + vbCrLf)
            sb.Append("     PVF.RateScheduleId, " + vbCrLf)
            sb.Append("     (select RateScheduleDescrip from saRateSchedules where RateScheduleId=PVF.RateScheduleId) RateScheduleDescrip, " + vbCrLf)
            sb.Append("     PVF.UnitId, " + vbCrLf)
            sb.Append("     (select UnitDescrip from syProgramUnitTypes t where t.UnitId=PVF.UnitId) as UnitDescrip, " + vbCrLf)
            sb.Append("     PVF.ModUser, " + vbCrLf)
            sb.Append("     PVF.ModDate " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     saProgramVersionFees PVF " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.AppendFormat("     PVF.PrgVerFeeId = '{0}' ", PrgVerFeeId)

            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            Dim info As New PrgVersionFeeInfo
            If dr.Read() Then
                info.PrgVerFeeId = PrgVerFeeId
                info.IsInDB = True
                info.Active = dr("Active")
                If Not (dr("PrgVerId") Is System.DBNull.Value) Then info.PrgVerId = CType(dr("PrgVerId"), Guid).ToString
                If Not (dr("PrgVerDescrip") Is System.DBNull.Value) Then info.PrgVerDescrip = dr("PrgVerDescrip")
                If Not (dr("ProgId") Is System.DBNull.Value) Then info.ProgId = CType(dr("ProgId"), Guid).ToString
                If Not (dr("ProgDescrip") Is System.DBNull.Value) Then info.ProgDescrip = dr("ProgDescrip")
                If Not (dr("TransCodeId") Is System.DBNull.Value) Then info.TransCodeId = CType(dr("TransCodeId"), Guid).ToString
                If Not (dr("TransCodeDescrip") Is System.DBNull.Value) Then info.TransCodeDescrip = dr("TransCodeDescrip")
                If Not (dr("TuitionCategoryId") Is System.DBNull.Value) Then info.TuitionCategoryId = CType(dr("TuitionCategoryId"), Guid).ToString
                If Not (dr("TuitionCategoryDescrip") Is System.DBNull.Value) Then info.TuitionCategoryDescrip = dr("TuitionCategoryDescrip")
                If Not (dr("Amount") Is System.DBNull.Value) Then info.Amount = dr("Amount")
                If Not (dr("RateScheduleId") Is System.DBNull.Value) Then info.RateScheduleId = CType(dr("RateScheduleId"), Guid).ToString
                If Not (dr("RateScheduleDescrip") Is System.DBNull.Value) Then info.RateScheduleDescrip = dr("RateScheduleDescrip")
                If Not (dr("UnitId") Is System.DBNull.Value) Then info.UnitId = CType(dr("UnitId"), UnitType)
                If Not (dr("UnitDescrip") Is System.DBNull.Value) Then info.UnitDescrip = dr("UnitDescrip")
                If Not (dr("ModUser") Is System.DBNull.Value) Then info.ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then info.ModDate = dr("ModDate")
            End If

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return info
        End Function

        ''' <summary>
        ''' Updates a PrgVersionFeeInfo class to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Update(ByVal info As PrgVersionFeeInfo, ByVal user As String) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("UPDATE " + vbCrLf)
                sb.Append("     saProgramVersionFees " + vbCrLf)
                sb.Append("SET " + vbCrLf)
                If info.Active Then
                    sb.Append("     StatusId = (select StatusId from syStatuses where Status='Active'), " + vbCrLf)
                Else
                    sb.Append("     StatusId = (select StatusId from syStatuses where Status='Inactive'), " + vbCrLf)
                End If
                sb.Append("     PrgVerId = ?, " + vbCrLf)
                sb.Append("     TransCodeId = ?, " + vbCrLf)
                sb.Append("     TuitionCategoryId = ?, " + vbCrLf)
                sb.Append("     Amount = ?, " + vbCrLf)
                sb.Append("     RateScheduleId = ?, " + vbCrLf)
                sb.Append("     UnitId = ?, " + vbCrLf)
                sb.Append("     ModUser = ?, " + vbCrLf)
                sb.Append("     ModDate = ? " + vbCrLf)
                sb.Append("WHERE " + vbCrLf)
                sb.Append("     PrgVerFeeId = ? ;" + vbCrLf)
                'sb.Append("     AND ModDate = ? ;" + vbCrLf)
                sb.Append("select count(*) from saProgramVersionFees where ModDate = ? ")

                '   add parameters values to the query
                If info.PrgVerId = Guid.Empty.ToString Or info.PrgVerId = "" Then
                    db.AddParameter("@PrgVerId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@PrgVerId", info.PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.TransCodeId = Guid.Empty.ToString Or info.TransCodeId = "" Then
                    db.AddParameter("@TransCodeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@TransCodeId", info.TransCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.TuitionCategoryId = Guid.Empty.ToString Or info.TuitionCategoryId = "" Then
                    db.AddParameter("@TuitionCategoryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@TuitionCategoryId", info.TuitionCategoryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@Amount", info.Amount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                If info.RateScheduleId = Guid.Empty.ToString Or info.RateScheduleId = "" Then
                    db.AddParameter("@RateScheduleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@RateScheduleId", info.RateScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@UnitId", info.UnitId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@AdmDepositId", info.PrgVerFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'db.AddParameter("@Original_ModDate", info.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)
                '   If there were no updated rows then there was a concurrency problem
                If rowCount = 1 Then
                    '   return without errors
                    Return ""
                Else
                    Return DALExceptions.BuildConcurrencyExceptionMessage()
                End If

            Catch ex As OleDbException
                Return DALExceptions.BuildErrorMessage(ex)
            Finally
                db.CloseConnection()
            End Try
        End Function

        ''' <summary>
        ''' Adds a PrgVersionFeeInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As PrgVersionFeeInfo, ByVal user As String) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sb As New StringBuilder
                sb.Append("DECLARE @active as uniqueidentifier " + vbCrLf)
                If info.Active Then
                    sb.Append("SELECT @active=StatusId from syStatuses where Status='Active' " + vbCrLf)
                Else
                    sb.Append("SELECT @active=StatusId from syStatuses where Status='Inactive' " + vbCrLf)
                End If
                sb.Append("INSERT saProgramVersionFees (PrgVerFeeId, StatusId, ")
                sb.Append("   PrgVerId, TransCodeId, TuitionCategoryId, ")
                sb.Append("   Amount, RateScheduleId, UnitId, ")
                sb.Append("   ModUser, ModDate) ")
                sb.Append("VALUES (?,@active,?,?,?,?,?,?,?,?) ")

                db.AddParameter("@PrgVerFeeId", info.PrgVerFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.PrgVerId = Guid.Empty.ToString Or info.PrgVerId = "" Then
                    db.AddParameter("@PrgVerId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@PrgVerId", info.PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.TransCodeId = Guid.Empty.ToString Or info.TransCodeId = "" Then
                    db.AddParameter("@TransCodeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@TransCodeId", info.TransCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.TuitionCategoryId = Guid.Empty.ToString Or info.TuitionCategoryId = "" Then
                    db.AddParameter("@TuitionCategoryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@TuitionCategoryId", info.TuitionCategoryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@Amount", info.Amount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                If info.RateScheduleId = Guid.Empty.ToString Or info.RateScheduleId = "" Then
                    db.AddParameter("@RateScheduleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@RateScheduleId", info.RateScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@UnitId", info.UnitId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString) '   execute the query
                Return "" '   return without errors
            Catch ex As OleDbException
                Return DALExceptions.BuildErrorMessage(ex) '   return an error to the client
            Finally
                db.CloseConnection()
            End Try
        End Function

        ''' <summary>
        ''' Deletes a PrgVersionFee record from the database
        ''' </summary>
        ''' <param name="PrgVerFeeId"></param>
        ''' <param name="modDate"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal PrgVerFeeId As String, ByVal modDate As DateTime) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sb As New StringBuilder
                sb.Append("DELETE FROM saProgramVersionFees ")
                sb.Append("WHERE PrgVerFeeId = ? ")
                sb.Append(" AND ModDate = ? ;")
                sb.Append("SELECT count(*) FROM saProgramVersionFees WHERE PrgVerFeeId = ? ")

                db.AddParameter("@PrgVerFeeId", PrgVerFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@PrgVerFeeId", PrgVerFeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)
                '   If the row was not deleted then there was a concurrency problem
                If rowCount = 0 Then
                    Return "" '   return without errors
                Else
                    Return DALExceptions.BuildConcurrencyExceptionMessage()
                End If
            Catch ex As OleDbException
                Return DALExceptions.BuildErrorMessage(ex) '   return an error to the client
            Finally
                db.CloseConnection() 'Close Connection
            End Try
        End Function

        ''' <summary>
        ''' Retrives all active transaction codes
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetTransCodes() As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     TC.TransCodeId as ID, " + vbCrLf)
            sb.Append("     TC.TransCodeDescrip as Descrip, " + vbCrLf)
            sb.Append("     '(' + TC.TransCodeCode + ') ' + TC.TransCodeDescrip as Descrip2" + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     saTransCodes TC " + vbCrLf)
            sb.Append("WHERE ")
            sb.Append("     TC.StatusId = (select StatusId from syStatuses where Status='Active') " + vbCrLf)
            sb.Append("ORDER BY " + vbCrLf)
            sb.Append("     TC.TransCodeDescrip asc ")
            Return db.RunSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Returns all active tution categories
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetTuitionCategories() As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     TC.TuitionCategoryId as ID, " + vbCrLf)
            sb.Append("     TC.TuitionCategoryDescrip as Descrip, " + vbCrLf)
            sb.Append("     '(' + TC.TuitionCategoryCode + ') ' + TC.TuitionCategoryDescrip as Descrip2" + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     saTuitionCategories TC " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     TC.StatusId = (select StatusId from syStatuses where Status='Active') " + vbCrLf)
            sb.Append("ORDER BY " + vbCrLf)
            sb.Append("     TC.TuitionCategoryDescrip asc ")
            Return db.RunSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' returns all active rate schedules
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetRateSchedules() As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     CCT.RateScheduleId as ID, " + vbCrLf)
            sb.Append("     CCT.RateScheduleDescrip as Descrip, ")
            sb.Append("     '(' + CCT.RateScheduleCode + ') ' + CCT.RateScheduleDescrip as Descrip2" + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     saRateSchedules CCT ")
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     CCT.StatusId = (select StatusId from syStatuses where Status='Active') " + vbCrLf)
            sb.Append("ORDER BY " + vbCrLf)
            sb.Append("     CCT.RateScheduleDescrip asc ")
            Return db.RunSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Returns all unit types avaialable for the specified PrgVerid
        ''' </summary>
        ''' <param name="PrgVerId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetUnitTypes(ByVal PrgVerId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     U.UnitId as ID, " + vbCrLf)
            sb.Append("     U.UnitDescrip as Descrip ")
            sb.Append("FROM " + vbCrLf)
            sb.Append("     syProgramUnitTypes U ")
            Return db.RunSQLDataSet(sb.ToString)
        End Function
    End Class
#End Region

#Region "Schedules"
    Public Class SchedulesDB
        ''' <summary>
        ''' Get Schedules for the supplied PrgVerId
        ''' </summary>
        ''' <param name="ShowActive"></param>
        ''' <param name="ShowInactive"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAll(ByVal PrgVerId As String, ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     S.ScheduleId as Id, " & vbCrLf)
            sb.Append("     S.Code, " & vbCrLf)
            sb.Append("     S.Descrip, " & vbCrLf)
            sb.Append("     S.Active, " + vbCrLf)
            sb.Append("     S.ModUser, " + vbCrLf)
            sb.Append("     S.ModDate " + vbCrLf)
            sb.Append("FROM arProgSchedules S " & vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            If PrgVerId IsNot Nothing AndAlso PrgVerId <> "" Then
                sb.AppendFormat("     S.PrgVerId = '{0}' {1}", PrgVerId, vbCrLf)
            End If
            If ShowActive And Not ShowInactive Then
                sb.Append("     AND S.Active = 1 " + vbCrLf)
            ElseIf Not ShowActive And ShowInactive Then
                sb.Append("     AND S.Active = 0 " + vbCrLf)
            End If
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append("     S.Descrip ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieve a ProgramInfo object from the database given a ProgId
        ''' </summary>
        ''' <param name="ScheduleId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal ScheduleId As String) As ScheduleInfo
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            ' db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     S.ScheduleId, " & vbCrLf)
            sb.Append("     S.Code, " & vbCrLf)
            sb.Append("     S.Descrip, " & vbCrLf)
            sb.Append("     S.PrgVerId, " + vbCrLf)
            sb.Append("     P.PrgVerDescrip, " + vbCrLf)
            sb.Append("     P.ProgId, " + vbCrLf)
            sb.Append("     (select ProgDescrip from arPrograms t where t.ProgId=P.ProgId) as ProgDescrip, " + vbCrLf)
            sb.Append("     (select CalendarTypeId from arPrograms t where t.ProgId=P.ProgId) as CalendarTypeId, " + vbCrLf)
            sb.Append("     P.UseTimeClock, " + vbCrLf)
            sb.Append("     S.UseFlexTime, " + vbCrLf)
            sb.Append("     S.Active, " + vbCrLf)
            sb.Append("     S.ModUser, " + vbCrLf)
            sb.Append("     S.ModDate, " + vbCrLf)
            sb.Append("     (select count(*) from arStudentSchedules SS where SS.ScheduleId=S.ScheduleId) as NumStudentsUsing " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arProgSchedules S, " & vbCrLf)
            sb.Append("     arPrgVersions P " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     S.PrgVerId = P.PrgVerId " + vbCrLf)
            sb.AppendFormat("     and S.ScheduleId = '{0}'", ScheduleId)

            '   Execute the query
            Try
                Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString) '.RunParamSQLDataReader(sb.ToString)
                Dim info As New ScheduleInfo
                If dr.Read() Then
                    '   set properties with data from DataReader
                    info.ScheduleId = ScheduleId
                    info.IsInDB = True
                    If Not (dr("ScheduleId") Is System.DBNull.Value) Then info.ScheduleId = dr("ScheduleId").ToString()
                    If Not (dr("Code") Is System.DBNull.Value) Then info.Code = dr("Code")
                    If Not (dr("Descrip") Is System.DBNull.Value) Then info.Descrip = dr("Descrip")
                    If Not (dr("PrgVerId") Is System.DBNull.Value) Then info.PrgVerId = dr("PrgVerId").ToString()
                    If Not (dr("PrgVerDescrip") Is System.DBNull.Value) Then info.PrgVerDescrip = dr("PrgVerDescrip")
                    If Not (dr("ProgId") Is System.DBNull.Value) Then info.ProgId = dr("ProgId").ToString()
                    If Not (dr("ProgDescrip") Is System.DBNull.Value) Then info.ProgDescrip = dr("ProgDescrip")
                    If Not (dr("CalendarTypeId") Is System.DBNull.Value) Then info.AcademicCalendarType = CType(dr("CalendarTypeId"), AcademicCalendarTypes)
                    If Not (dr("UseTimeClock") Is System.DBNull.Value) Then info.UseTimeClock = dr("UseTimeClock")
                    If Not (dr("UseFlexTime") Is System.DBNull.Value) Then info.UseFlexTime = dr("UseFlexTime")
                    If Not (dr("Active") Is System.DBNull.Value) Then info.Active = dr("Active")
                    If Not (dr("ModUser") Is System.DBNull.Value) Then info.ModUser = dr("ModUser").ToString()
                    If Not (dr("ModDate") Is System.DBNull.Value) Then info.ModDate = dr("ModDate")
                    If Not (dr("NumStudentsUsing") Is System.DBNull.Value) Then info.NumStudentsUsing = dr("NumStudentsUsing")

                    ' get a datatable representing the details
                    info.DetailsDT = GetDetailsDT(ScheduleId)

                    ' get an array of ScheduleDetailInfo objects
                    ' This is redundant but each are used in different cases
                    Dim c As Integer = info.DetailsDT.Rows.Count
                    Dim details(c - 1) As ScheduleDetailInfo
                    For dow As Integer = 0 To c - 1
                        Dim drs() As DataRow = info.DetailsDT().Select("dw=" + dow.ToString())
                        If drs.Length > 0 Then
                            details(dow) = GetDetailInfo(drs(0))
                        End If
                    Next
                    info.Details = details
                End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                Return info
            Catch ex As System.Exception
            End Try
            Return Nothing
        End Function

        ''' <summary>
        ''' Fills a ScheduleDetailInfo object from a DataRow
        ''' </summary>
        ''' <param name="dr"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function GetDetailInfo(ByVal dr As DataRow) As ScheduleDetailInfo
            Dim info As New ScheduleDetailInfo
            Try
                info.ScheduleDetailId = dr("ScheduleDetailId").ToString()
                info.ScheduleId = dr("ScheduleId").ToString()
                If Not DBNull.Value.Equals(dr("dw")) Then info.dw = dr("dw")
                If Not DBNull.Value.Equals(dr("total")) Then info.Total = dr("total")
                If Not DBNull.Value.Equals(dr("timein")) Then info.TimeIn = dr("timein")
                If Not DBNull.Value.Equals(dr("lunchout")) Then info.LunchOut = dr("lunchout")
                If Not DBNull.Value.Equals(dr("lunchin")) Then info.LunchIn = dr("lunchin")
                If Not DBNull.Value.Equals(dr("timeout")) Then info.TimeOut = dr("timeout")
                If Not DBNull.Value.Equals(dr("maxnolunch")) Then info.MaxNoLunch = dr("maxnolunch")
                If Not DBNull.Value.Equals(dr("allow_earlyin")) Then info.Allow_EarlyIn = dr("allow_earlyin")
                If Not DBNull.Value.Equals(dr("allow_lateout")) Then info.Allow_LateOut = dr("allow_lateout")
                If Not DBNull.Value.Equals(dr("allow_extrahours")) Then info.Allow_ExtraHours = dr("allow_extrahours")
                If Not DBNull.Value.Equals(dr("check_tardyin")) Then info.Check_TardyIn = dr("check_tardyin")
                If Not DBNull.Value.Equals(dr("max_beforetardy")) Then info.MaxBeforeTardy = dr("max_beforetardy")
                If Not DBNull.Value.Equals(dr("tardy_intime")) Then info.TardyInTime = dr("tardy_intime")
                If Not DBNull.Value.Equals(dr("MinimumHoursToBePresent")) Then info.MinHoursConsideredPresent = dr("MinimumHoursToBePresent")

            Catch ex As System.Exception
            End Try

            Return info
        End Function

        ''' <summary>
        ''' Returns the schedule details in a DataTable
        ''' </summary>
        ''' <param name="ScheduleId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetDetailsDT(ByVal ScheduleId As String) As DataTable
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("select ")
            sb.Append("     SD.ScheduleDetailId, " & vbCrLf)
            sb.Append("     SD.ScheduleId, " + vbCrLf)
            sb.Append("     SD.dw, " + vbCrLf)
            sb.Append("     SD.total, " + vbCrLf)
            sb.Append("     SD.timein, " + vbCrLf)
            sb.Append("     SD.lunchout, " + vbCrLf)
            sb.Append("     SD.lunchin, " + vbCrLf)
            sb.Append("     SD.timeout, " + vbCrLf)
            sb.Append("     SD.maxnolunch, " + vbCrLf)
            sb.Append("     SD.allow_earlyin, " + vbCrLf)
            sb.Append("     SD.allow_lateout, " + vbCrLf)
            sb.Append("     SD.allow_extrahours, " + vbCrLf)
            sb.Append("     SD.check_tardyin, " + vbCrLf)
            sb.Append("     SD.max_beforetardy, " + vbCrLf)
            sb.Append("     SD.tardy_intime, " + vbCrLf)
            sb.Append("     SD.MinimumHoursToBePresent " + vbCrLf)
            sb.Append("from " + vbCrLf)
            sb.Append("     arProgScheduleDetails SD " + vbCrLf)
            sb.Append("where " + vbCrLf)
            sb.AppendFormat("     SD.ScheduleId = '{0}' {1} ", ScheduleId, vbCrLf)
            sb.Append("order by " + vbCrLf)
            sb.Append("     SD.dw asc " + vbCrLf)
            Dim dt As DataTable = db.RunParamSQLDataSet(sb.ToString).Tables(0)

            ' add default rows if nothing has been defined
            If dt.Rows.Count = 0 Then
                For i As Integer = 0 To 6
                    Dim dr As DataRow = dt.NewRow
                    dr("dw") = i
                    dt.Rows.Add(dr)
                Next
            End If

            Return dt
        End Function

        ''' <summary>
        ''' Update a ScheduleInfo object to the database.
        ''' If a student is using a schedule, then they cannot make any changes to the details.
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Update(ByVal info As ScheduleInfo, ByVal user As String) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("update " & vbCrLf)
                sb.Append("     arProgSchedules " + vbCrLf)
                sb.Append("set " & vbCrLf)
                sb.Append("     Code = ?, " & vbCrLf)
                sb.Append("     Descrip = ?, " & vbCrLf)
                sb.Append("     PrgVerid = ?, " & vbCrLf)
                sb.Append("     UseFlexTime = ?, " + vbCrLf)
                sb.Append("     Active = ?, " & vbCrLf)
                sb.Append("     ModUser = ?, " + vbCrLf)
                sb.Append("     ModDate = ? " & vbCrLf)
                sb.Append("where " + vbCrLf)
                sb.Append("     ScheduleId = ? " + vbCrLf)

                '   add parameters values to the query            
                db.AddParameter("@Code", info.Code, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
                db.AddParameter("@Descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.PrgVerId Is Nothing Or info.PrgVerId = Guid.Empty.ToString Then
                    db.AddParameter("@PrgVerId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@PrgVerId", info.PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@UseFlexTime", info.UseFlexTime, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                If info.ModUser Is Nothing Or info.ModUser = Guid.Empty.ToString Then
                    db.AddParameter("@ModUser", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                info.ModDate = Date.Now ' update the info object with the current time
                db.AddParameter("@ModDate", info.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ScheduleId", info.ScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                ' run the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)

                ' Determine if the details can be changed by looking it is is use by a student
                Dim sql As String = String.Format("select count(*) from arStudentSchedules where ScheduleId='{0}'", info.ScheduleId)
                Dim res As Integer = db.RunParamSQLScalar(sql)
                If res > 0 Then
                    Return "Cannot change details for this schedule because it is used by 1 or more students."
                End If

                ' add the new schedule detail information
                DeleteAllDetails(info.ScheduleId)
                For Each sdi As ScheduleDetailInfo In info.Details
                    AddDetail(sdi)
                Next

                Return "" ' return success
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function


        ''' <summary>
        ''' Persists a new ProgramInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As ScheduleInfo, ByVal user As String) As String
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query                
                Dim sb As New StringBuilder
                sb.Append("INSERT arProgSchedules (ScheduleId, Code, Descrip, PrgVerId, UseFlexTime, Active, ModUser, ModDate) " + vbCrLf)
                sb.Append("VALUES (?,?,?,?,?,?,?,?) ")

                '   add parameters values to the query
                db.AddParameter("@ScheduleId", info.ScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Code", info.Code, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
                db.AddParameter("@Descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PrgVerId", info.PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@UseFlexTime", info.UseFlexTime, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                If info.ModUser Is Nothing Or info.ModUser = Guid.Empty.ToString Then
                    db.AddParameter("@ModUser", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModUser", info.ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()

                ' add the new schedule detail information
                DeleteAllDetails(info.ScheduleId)
                For Each sdi As ScheduleDetailInfo In info.Details
                    AddDetail(sdi)
                Next

                Return "" '   return without errors
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function

        ''' <summary>
        ''' Deletes schedule given a scheduleid
        ''' </summary>
        ''' <param name="ScheduleId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal ScheduleId As String, ByVal modDate As DateTime) As String
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                ' First check that it is safe to delete the schedule
                ' it is only safe to delete if no student is using it
                Dim sql As String = String.Format("select count(*) from arStudentSchedules where ScheduleId='{0}'", ScheduleId)
                Dim res As Integer = db.RunParamSQLScalar(sql)
                If res > 0 Then
                    Return "Cannot delete this schedule because it is used by 1 or more students."
                End If

                ' before we can delete the schedule, we must delete the details
                DeleteAllDetails(ScheduleId)

                ' make a sql string to delete the schedule
                sql = "DELETE arProgSchedules WHERE ScheduleId = ? AND ModDate = ? " + vbCrLf
                sql += "SELECT COUNT(*) from arProgSchedules where ScheduleId = ? " + vbCrLf
                db.AddParameter("@ScheduleId", ScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ScheduleId", ScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   If the row was not deleted then there was a concurrency problem
                If db.RunParamSQLScalar(sql) = 0 Then
                    Return ""   ' return without errors
                Else
                    Return DALExceptions.BuildConcurrencyExceptionMessage()
                End If
                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function

        ''' <summary>
        ''' Add one schedule detail record to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function AddDetail(ByVal info As ScheduleDetailInfo) As String
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query                
                Dim sb As New StringBuilder
                sb.Append("INSERT INTO " + vbCrLf)
                sb.Append("     [arProgScheduleDetails] " + vbCrLf)
                sb.Append("           ([ScheduleDetailId],[ScheduleId],[dw],[total],[timein],[lunchin], " + vbCrLf)
                sb.Append("             [lunchout],[timeout],[maxnolunch],[allow_earlyin],[allow_lateout], " + vbCrLf)
                sb.Append("             [allow_extrahours],[check_tardyin],[max_beforetardy],[tardy_intime],[MinimumHoursToBePresent]) " + vbCrLf)
                sb.Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")

                '   add parameters values to the query
                db.AddParameter("@ScheduleDetailId", info.ScheduleDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ScheduleId", info.ScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@dw", info.dw, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                If info.Total = Integer.MinValue Or info.Total = Decimal.MinValue Then
                    db.AddParameter("@total", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                Else
                    db.AddParameter("@total", info.Total, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                End If
                If info.TimeIn = DateTime.MinValue Then
                    db.AddParameter("@timein", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@timein", info.TimeIn, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                If info.LunchIn = DateTime.MinValue Then
                    db.AddParameter("@lunchin", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@lunchin", info.LunchIn, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                If info.LunchOut = DateTime.MinValue Then
                    db.AddParameter("@lunchout", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@lunchout", info.LunchOut, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                If info.TimeOut = DateTime.MinValue Then
                    db.AddParameter("@timeout", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@timeout", info.TimeOut, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                If info.MaxNoLunch = Decimal.MinValue Then
                    db.AddParameter("@maxnolunch", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                Else
                    db.AddParameter("@maxnolunch", info.MaxNoLunch, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                End If
                db.AddParameter("@allow_earlyin", info.Allow_EarlyIn, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@allow_lateout", info.Allow_LateOut, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@allow_extrahours", info.Allow_ExtraHours, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@check_tardyin", info.Check_TardyIn, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                If info.MaxBeforeTardy = DateTime.MinValue Then
                    db.AddParameter("@max_beforetardy", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@max_beforetardy", info.MaxBeforeTardy, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                If info.TardyInTime = DateTime.MinValue Then
                    db.AddParameter("@tardy_intime", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@tardy_intime", info.TardyInTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                If info.MinHoursConsideredPresent = Decimal.MinValue Then
                    db.AddParameter("@MinHoursConsideredPresent", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                Else
                    db.AddParameter("@MinHoursConsideredPresent", info.MinHoursConsideredPresent, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                End If
                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                Return "" '   return without errors
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function

        ''' <summary>
        ''' Removes all schedule details
        ''' </summary>
        ''' <param name="ScheduleId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function DeleteAllDetails(ByVal ScheduleId As String) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                ' check if there are already user tasks with this taskid
                Dim sql As String = String.Format("DELETE arProgScheduleDetails WHERE ScheduleId = '{0}'; {1}", ScheduleId, vbCrLf)
                sql += String.Format("SELECT COUNT(*) from arProgScheduleDetails WHERE ScheduleId = '{0}'; {1}", ScheduleId, vbCrLf)
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                If rowCount > 0 Then
                    db.CloseConnection()
                    Return "Failed to delete schedule details"
                End If
                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function

        ''' <summary>
        ''' Determine if the supplied program version is using a time clock schedule
        ''' </summary>
        ''' <param name="PrgVerId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function IsPrgVerUsingTimeClockSchedule(ByVal PrgVerId As String) As Boolean
            Dim db As New DataAccess
            Dim rtn As Object

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sql As String = String.Format("select UnitTypeId from arPrgVersions where PrgVerId='{0}'", PrgVerId)
                Dim res As String = db.RunParamSQLScalar(sql).ToString()
                If (res = AttendanceUnitTypes.UseTimeClockGuid Or res = AttendanceUnitTypes.PresentAbsentGuid Or res = AttendanceUnitTypes.MinutesGuid) Then
                    'Return True
                    sql = String.Format("select count(*) from arProgSchedules where Active=1 and PrgVerId='{0}'", PrgVerId)
                    rtn = db.RunParamSQLScalar(sql)
                    If (rtn Is System.DBNull.Value) Then
                        Return CType(0, Boolean)
                    Else
                        Return CType(rtn, Boolean)
                    End If

                End If
                ' 3/22/07 - ThinkTron - Return true if UseTimeClock field is 1
                sql = String.Format("select UseTimeClock from arPrgVersions where PrgVerId='{0}'", PrgVerId)
                rtn = db.RunParamSQLScalar(sql)
                If (rtn Is System.DBNull.Value) Then
                    Return CType(0, Boolean)
                Else
                    Return CType(rtn, Boolean)
                End If

            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False ' error if we got here, default to false
        End Function

        Public Shared Function PrgVerAttendanceType(ByVal PrgVerId As String) As String
            Dim db As New DataAccess
            Dim rtn As Object

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sql As String = String.Format("select UnitTypeId from arPrgVersions where PrgVerId='{0}'", PrgVerId)
                Return db.RunParamSQLScalar(sql).ToString()

            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try

        End Function
        Public Shared Function TrackTardy(ByVal PrgVerId As String) As String
            Dim db As New DataAccess
            Dim rtn As Object

            Dim myAdvAppSettings = AdvAppSettings.GetAppSettings()
            Dim connectionString = myAdvAppSettings.AppSettings("ConString")
            db.ConnectionString = connectionString.ToString

            Try
                Dim sql As String = String.Format("select TardiesMakingAbsence from arPrgVersions where PrgVerId='{0}' and TrackTardies=1", PrgVerId)
                Dim result = db.RunParamSQLScalar(sql)
                If result IsNot Nothing Then
                    Return result.ToString
                Else
                    Return "0"
                End If
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
        End Function
        Public Shared Function CheckTardyIn(ByVal PrgVerId As String, ByVal StuEnrollId As String) As DataSet
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim dr As OleDbDataReader
            Dim dsPunch As New DataSet
            Dim arrCheckTardyIn As New ArrayList

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                With sb
                    .Append("   Select Distinct t2.dw,t4.PunchedInTime,t2.DeadLineBeforeConsideredTardy, " + vbCrLf)
                    .Append("   t2.TimeTakenAsPunchIn " + vbCrLf)
                    .Append("   from arProgSchedules t1, " + vbCrLf)
                    .Append("   (select Distinct ScheduleId,dw, " + vbCrLf)
                    .Append("   Convert(char(10),Max_beforeTardy,108) as DeadLineBeforeConsideredTardy, " + vbCrLf)
                    .Append("   Convert(char(10),tardy_intime,108) as TimeTakenAsPunchIn " + vbCrLf)
                    .Append("   from arProgScheduleDetails where Check_TardyIn=1) t2,arStudentSchedules t3, " + vbCrLf)
                    .Append("   (select  Top 1 Convert(char(10),PunchTime,108) as PunchedInTime,StuEnrollId from arStudentTimeClockPunches " + vbCrLf)
                    .Append("   where PunchType=1 order by PunchedInTime) t4 " + vbCrLf)
                    .Append("   where t1.ScheduleId=t2.ScheduleId and t2.ScheduleId=t3.ScheduleId and " + vbCrLf)
                    .Append("   t3.StuEnrollId=t4.StuEnrollId and t1.PrgVerId=? and " + vbCrLf)
                    .Append("   t3.StuEnrollId=? " + vbCrLf)
                    .Append("   and t4.PunchedInTime > t2.DeadLineBeforeConsideredTardy " + vbCrLf)
                    .Append("   Order by t2.dw ")
                End With
                db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                dsPunch = db.RunParamSQLDataSet(sb.ToString)
                Return dsPunch
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return Nothing
            Finally
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
        End Function

        ''' <summary>
        ''' Returns all the schedules assigned to a student
        ''' </summary>
        ''' <param name="StuEnrollId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetStudentSchedules(ByVal StuEnrollId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     SS.StuEnrollId, " + vbCrLf)
            sb.Append("     SS.ScheduleId as Id, " & vbCrLf)
            sb.Append("     PS.Descrip, " & vbCrLf)
            sb.Append("     SS.StartDate, " & vbCrLf)
            sb.Append("     SS.EndDate, " & vbCrLf)
            sb.Append("     SS.Active, " + vbCrLf)
            sb.Append("     (select BadgeNumber from arStuEnrollments SE where SE.StuEnrollId=SS.StuEnrollId) as BadgeNumber, " + vbCrLf)
            sb.Append("     SS.ModUser, " + vbCrLf)
            sb.Append("     SS.ModDate " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arStudentSchedules SS, arProgSchedules PS " & vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     SS.ScheduleId = PS.ScheduleId " + vbCrLf)
            If StuEnrollId IsNot Nothing AndAlso StuEnrollId <> "" Then
                sb.AppendFormat("     AND SS.StuEnrollId = '{0}' {1}", StuEnrollId, vbCrLf)
            End If
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append("     PS.Descrip ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' returns the student's active schedule
        ''' For V1, we are only supporting one schedule per student.
        ''' For V2, we will have to add some code to support getting the real active one.
        ''' </summary>
        ''' <param name="StuEnrollId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetActiveStudentScheduleInfo(ByVal StuEnrollId As String) As StudentScheduleInfo
            Dim info As New StudentScheduleInfo
            Try
                Dim ds As DataSet = GetStudentSchedules(StuEnrollId)
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    '   set properties with data from DataReader
                    Dim dr As DataRow = ds.Tables(0).Rows(0)
                    info.StuEnrollId = StuEnrollId
                    If Not (dr("Id") Is System.DBNull.Value) Then info.ScheduleId = dr("Id").ToString()
                    If Not (dr("Descrip") Is System.DBNull.Value) Then info.Descrip = dr("Descrip")
                    If Not (dr("BadgeNumber") Is System.DBNull.Value) Then info.BadgeNumber = dr("BadgeNumber")
                    If Not (dr("Active") Is System.DBNull.Value) Then info.Active = dr("Active")
                    If Not (dr("ModUser") Is System.DBNull.Value) Then info.ModUser = dr("ModUser").ToString()
                    If Not (dr("ModDate") Is System.DBNull.Value) Then info.ModDate = dr("ModDate")
                End If
            Catch ex As System.Exception
            End Try
            Return info
        End Function

        Public Shared Function AssignStudentScheduleFromSchedulePage(ByVal info As StudentScheduleInfo, ByVal user As String) As Integer
            '   Connect to the database


            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim db As SqlConnection = New SqlConnection(MyAdvAppSettings.AppSettings("ConnectionString").ToString)

            'Dim db As SqlConnection = New SqlConnection(MyAdvAppSettings.AppSettings("ConString"))
            ''   do an insert

            'Try
            '    db.OpenConnection()

            '    Const strStoredProcedureName As String = "UpdateAssignStudentSchedule"

            '    db.AddParameter("@StuEnrollId", info.StuEnrollId, OleDbType.VarChar, 50, ParameterDirection.Input)
            '    db.AddParameter("@ScheduleId", info.ScheduleId, OleDbType.VarChar, 50, ParameterDirection.Input)
            '    db.AddParameter("@Source", "attendance", OleDbType.VarChar, 50, ParameterDirection.Input)
            '    db.AddParameter("@Active", info.Active, OleDbType.Boolean, , ParameterDirection.Input)
            '    db.AddParameter("@ModUser", info.ModUser, OleDbType.VarChar, 50, ParameterDirection.Input)
            '    db.AddParameter("@BadgeNumber", info.BadgeNumber, OleDbType.VarChar, 50, ParameterDirection.Input)


            '    Dim rtnObj As Object = db.RunParamSQLScalar(strStoredProcedureName)
            '    If Not rtnObj Is Nothing Then
            '        If TypeOf rtnObj Is Integer Then
            '            Return DirectCast(rtnObj, Integer)
            '        Else
            '            Return -1
            '        End If
            '    Else
            '        Return -1
            '    End If

            'Catch ex As System.Exception
            '    Return -1
            'Finally
            '    db.CloseConnection()
            'End Try



            Dim command As SqlCommand = New SqlCommand("UpdateAssignStudentSchedule", db)
            command.CommandType = CommandType.StoredProcedure
            command.Parameters.AddWithValue("@StuEnrollId", info.StuEnrollId)
            command.Parameters.AddWithValue("@ScheduleId", info.ScheduleId)
            command.Parameters.AddWithValue("@Source", "attendance")
            command.Parameters.AddWithValue("@Active", info.Active)
            command.Parameters.AddWithValue("@ModUser", info.ModUser)
            command.Parameters.AddWithValue("@BadgeNumber", info.BadgeNumber)
            db.Open()
            Try
                command.ExecuteScalar()
                Return 0
            Catch
                Return -1
            Finally
                db.Close()
            End Try
        End Function


        ''' <summary>
        ''' Adds a new schedule for a student
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function AssignStudentSchedule(ByVal info As StudentScheduleInfo, ByVal user As String) As String
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an insert



            Try
                '   build the query                
                Dim sb As New StringBuilder
                sb.AppendFormat("if (select count(*) from arStudentSchedules where StuEnrollId='{0}') = 0 {1} ", info.StuEnrollId, vbCrLf)
                ' begin of insert into arStudentSchedules code
                sb.Append("begin " + vbCrLf)
                sb.Append("     INSERT INTO [arStudentSchedules] " + vbCrLf)
                sb.Append("           ([StuEnrollId],[ScheduleId],[Source],[StartDate],[EndDate],[Active],[ModUser],[ModDate]) " + vbCrLf)
                sb.Append("     VALUES (?,?,?,?,?,?,?,?) ")
                sb.Append("end " + vbCrLf)

                '   add parameters values to the query
                db.AddParameter("@StuEnrollId", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ScheduleId", info.ScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Source", "attendance", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.StartDate = DateTime.MinValue Then
                    db.AddParameter("@StartDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@StartDate", info.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                If info.EndDate = DateTime.MinValue Then
                    db.AddParameter("@EndDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@EndDate", info.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                ' end of insert into arStudentSchedules code
                sb.Append("else " + vbCrLf)
                sb.Append("begin " + vbCrLf)
                sb.Append("     update arStudentSchedules " + vbCrLf)
                sb.Append("     set " + vbCrLf)
                sb.Append("         ScheduleId = ?, " + vbCrLf)
                sb.Append("         Source = ?, " + vbCrLf)
                sb.Append("         StartDate = ?, " + vbCrLf)
                sb.Append("         EndDate = ?, " + vbCrLf)
                sb.Append("         Active = ?, " + vbCrLf)
                sb.Append("         ModUser = ?, " + vbCrLf)
                sb.Append("         ModDate = ?" + vbCrLf)
                sb.Append("     where StuEnrollId = ? " + vbCrLf)
                db.AddParameter("@ScheduleId", info.ScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Source", "attendance", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.StartDate = DateTime.MinValue Then
                    db.AddParameter("@StartDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@StartDate", info.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                If info.EndDate = DateTime.MinValue Then
                    db.AddParameter("@EndDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@EndDate", info.EndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                sb.Append("end " + vbCrLf)

                ' also, update the enrollment in arStuEnrollments with the badgenumber
                sb.Append(vbCrLf + vbCrLf)
                sb.Append("update arStuEnrollments set BadgeNumber = ? where StuEnrollId = ? " + vbCrLf)
                db.AddParameter("@BadgeNumber", info.BadgeNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                Return "" '   return without errors
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function

        ''' <summary>
        ''' Returns true if the badge number is in use
        ''' </summary>
        ''' <param name="BadgeNumber"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ''' Students who are dropped and Graduated, their badge id's can be reused.
        '''  but, the other students badge number cannot be reused.
        Public Shared Function IsBadgeNumberInUse(ByVal BadgeNumber As String, ByVal StuEnrollId As String, ByVal CampusId As String) As Boolean
            Dim sb As New StringBuilder()
            sb.Append("select count(*) " + vbCrLf)
            sb.Append("from arStuEnrollments SE " + vbCrLf)
            sb.Append("where " + vbCrLf)
            sb.AppendFormat("     SE.BadgeNumber='{0}' {1}", BadgeNumber, vbCrLf)
            sb.AppendFormat("    AND SE.CampusId='{0}' {1}", CampusId, vbCrLf)
            If StuEnrollId IsNot Nothing AndAlso StuEnrollId <> "" Then
                sb.AppendFormat("     and SE.StuEnrollId<>'{0}' {1}", StuEnrollId, vbCrLf)
            End If
            sb.Append("     and SE.StatusCodeId in " + vbCrLf)
            sb.Append("         (select S.StatusCodeId from syStatusCodes S, sySysStatus SS where S.SysStatusId=SS.SysStatusId " + vbCrLf)
            sb.Append("          and SS.SysStatusId not in (12,14,8,19)) " + vbCrLf)
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim res As Integer = db.RunParamSQLScalar(sb.ToString)
                db.CloseConnection()
                Return res > 0 ' Badge is in use if the result > 0                
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return False
            Return True
        End Function

        ''' <summary>
        ''' Deletes all schedules for a student
        ''' </summary>
        ''' <param name="StuEnrollId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DeleteAllStudentSchedules(ByVal StuEnrollId As String) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                ' check if there are already user tasks with this taskid
                Dim sql As String = String.Format("DELETE arStudentSchedules WHERE StuEnrollId = '{0}'; {1}", StuEnrollId, vbCrLf)
                sql += String.Format("SELECT COUNT(*) from arStudentSchedules WHERE ScheduleId = '{0}'; {1}", StuEnrollId, vbCrLf)
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                If rowCount > 0 Then
                    db.CloseConnection()
                    Return "Failed to delete schedule for student"
                End If
                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function

        Public Shared Function GetBadgeNumberforStudentEnrollment(ByVal StuEnrollID As String) As String
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query                
                Dim sb As New StringBuilder
                sb.Append("SELECT BadgeNumber FROM dbo.arStuEnrollments WHERE StuEnrollId=?  " + vbCrLf)

                '   add parameters values to the query
                db.AddParameter("@StuEnrollId", StuEnrollID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute the query
                Dim dt As New DataTable
                dt = db.RunParamSQLDataSet(sb.ToString).Tables(0)

                If dt.Rows.Count > 0 Then
                    Return dt.Rows(0)(0).ToString()
                Else
                    Return ""
                End If

                db.CloseConnection()
                Return "" '   return without errors
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function



    End Class
#End Region

#Region "Grade Book"
    Public Class GradeBookDB

#Region "GradeBook Components"
        ''' <summary>
        ''' Returns all active/inactive grade book components
        ''' </summary>
        ''' <param name="CampGrpId"></param>
        ''' <param name="ShowActive"></param>
        ''' <param name="ShowInactive"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetGradeBookComponents(ByVal CampGrpId As String, ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     G.GrdComponentTypeId as Id, " + vbCrLf)
            sb.Append("     G.Code, " + vbCrLf)
            sb.Append("     G.Descrip, " + vbCrLf)
            sb.Append("     (case when G.StatusId=(select StatusId from syStatuses where Status='Active') then 1 else 0 end) as Active, " + vbCrLf)
            sb.Append("     G.ModDate, " + vbCrLf)
            sb.Append("     G.ModUser " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arGrdComponentTypes G " + vbCrLf)
            sb.Append("WHERE 1=1  " + vbCrLf)
            'Dim bWhere As Boolean = False
            If CampGrpId IsNot Nothing AndAlso CampGrpId <> "" Then
                sb.AppendFormat(" and  G.CampGrpId = '{0}' {1}", CampGrpId, vbCrLf)
                ' bWhere = True
            End If
            If ShowActive And Not ShowInactive Then
                'If bWhere Then sb.Append(" and ")
                sb.Append("  and   G.StatusId = (select StatusId from syStatuses where Status='Active') " & vbCrLf)
            ElseIf Not ShowActive And ShowInactive Then
                'If bWhere Then sb.Append(" and ")
                sb.Append("  and   G.StatusId = (select StatusId from syStatuses where Status='Inactive') " & vbCrLf)
            End If
            sb.Append(" ORDER BY " + vbCrLf)
            sb.Append("     G.Descrip ")
            Return db.RunSQLDataSet(sb.ToString)
        End Function

        Public Shared Function GetGradeBookComponentInfo(ByVal GrdComponentTypeId As String) As GradeBookComponentInfo
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     G.GrdComponentTypeId, " + vbCrLf)
            sb.Append("     G.Code, " + vbCrLf)
            sb.Append("     G.Descrip, " + vbCrLf)
            sb.Append("     (case when G.StatusId=(select StatusId from syStatuses where Status='Active') then 1 else 0 end) as Active, " + vbCrLf)
            sb.Append("     G.CampGrpId, " + vbCrLf)
            sb.Append("     (select CampGrpDescrip from syCampGrps CG where CG.CampGrpId=G.CampGrpId) as CampGrpDescrip, " + vbCrLf)
            sb.Append("     G.ModUser, " + vbCrLf)
            sb.Append("     G.ModDate, " + vbCrLf)
            sb.Append("     G.SysComponentTypeId, " + vbCrLf)
            sb.Append("     (select Resource from syResources where ResourceId=G.SysComponentTypeId) as SysComponenetTypeDescrip " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arGrdComponentTypes G " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.AppendFormat("     G.GrdComponentTypeId = '{0}' ", GrdComponentTypeId)

            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            Dim info As New GradeBookComponentInfo
            If dr.Read() Then
                info.GrdComponentTypeId = GrdComponentTypeId
                info.IsInDB = True
                info.Active = dr("Active")
                If Not (dr("Code") Is System.DBNull.Value) Then info.Code = dr("Code")
                If Not (dr("Descrip") Is System.DBNull.Value) Then info.Descrip = dr("Descrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then info.CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then info.CampGrpDescrip = dr("CampGrpDescrip")
                If Not (dr("ModUser") Is System.DBNull.Value) Then info.ModUser = dr("ModUser")
                If Not (dr("ModDate") Is System.DBNull.Value) Then info.ModDate = dr("ModDate")
                If Not (dr("SysComponentTypeId") Is System.DBNull.Value) Then info.SysComponentTypeId = dr("SysComponentTypeId")
                If Not (dr("SysComponenetTypeDescrip") Is System.DBNull.Value) Then info.SysComponentTypeDescrip = dr("SysComponenetTypeDescrip")
            End If

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return info
        End Function
        ''' <summary>
        ''' Returns system gradebook components that can be matched
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetSysGradeBookComponents() As DataTable
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     R.ResourceId as ID, " + vbCrLf)
            sb.Append("     R.Resource as Descrip " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     syResources R " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     R.ResourceTypeId = 10 " + vbCrLf) ' 10 is the resourcetype for System GradeBook Components
            sb.Append("ORDER BY " + vbCrLf)
            sb.Append("     R.Resource ")
            Return db.RunSQLDataSet(sb.ToString).Tables(0)
        End Function

        Public Shared Function Update(ByVal info As GradeBookComponentInfo, ByVal user As String) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("UPDATE " + vbCrLf)
                sb.Append("     arGrdComponentTypes " + vbCrLf)
                sb.Append("SET " + vbCrLf)
                If info.Active Then
                    sb.Append("     StatusId = (select StatusId from syStatuses where Status='Active'), " + vbCrLf)
                Else
                    sb.Append("     StatusId = (select StatusId from syStatuses where Status='Inactive'), " + vbCrLf)
                End If
                sb.AppendFormat("     Code = '{0}', {1}", info.Code, vbCrLf)
                sb.AppendFormat("     Descrip = '{0}', {1}", info.Descrip, vbCrLf)
                sb.Append("     CampGrpId = ?, " + vbCrLf)
                sb.AppendFormat("     ModUser = '{0}', {1}", user, vbCrLf)
                sb.Append("     ModDate = ?, " + vbCrLf)
                sb.AppendFormat("     SysComponentTypeId = {0} {1}", CType(info.SysComponentTypeId, Integer), vbCrLf)
                sb.Append("WHERE " + vbCrLf)
                sb.Append("     GrdComponentTypeId = ? ;" + vbCrLf)
                sb.Append("select count(*) from arGrdComponentTypes where GrdComponentTypeId = ? and ModDate = ? ")

                '   add parameters values to the query
                If info.CampGrpId = Guid.Empty.ToString Or info.CampGrpId = "" Then
                    db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CampGrpId", info.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@GrdComponentTypeId", info.GrdComponentTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@GrdComponentTypeId", info.GrdComponentTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                ' The code below does not seem to be working right
                Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)
                '   If there were no updated rows then there was a concurrency problem
                If rowCount = 1 Then
                    Return "" ' return without errors
                Else
                    Return DALExceptions.BuildConcurrencyExceptionMessage()
                End If

            Catch ex As OleDbException
                Return DALExceptions.BuildErrorMessage(ex)
            Finally
                db.CloseConnection()
            End Try
        End Function

        ''' <summary>
        ''' Adds a GradeBookComponentInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As GradeBookComponentInfo, ByVal user As String) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sb As New StringBuilder
                sb.Append("DECLARE @active as uniqueidentifier " + vbCrLf)
                If info.Active Then
                    sb.Append("SELECT @active=StatusId from syStatuses where Status='Active' " + vbCrLf)
                Else
                    sb.Append("SELECT @active=StatusId from syStatuses where Status='Inactive' " + vbCrLf)
                End If
                sb.Append("INSERT arGrdComponentTypes (GrdComponentTypeId, Code, Descrip, StatusId, ")
                sb.Append("   CampGrpId, ModDate, ModUser, SysComponentTypeid) ")
                sb.Append("VALUES (?,?,?,@active,?,?,?,?) ")

                db.AddParameter("@GrdComponentTypeId", info.GrdComponentTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Code", info.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.AddParameter("@CampGrpId", info.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@SysComponentTypeid", info.SysComponentTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString) '   execute the query
                Return "" '   return without errors
            Catch ex As OleDbException
                Return DALExceptions.BuildErrorMessage(ex) '   return an error to the client
            Finally
                db.CloseConnection()
            End Try
        End Function

        ''' <summary>
        ''' Deletes a Grade component record from the database
        ''' </summary>
        ''' <param name="GrdComponentTypeId"></param>
        ''' <param name="modDate"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal GrdComponentTypeId As String, ByVal modDate As DateTime) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sb As New StringBuilder
                sb.Append("DELETE FROM arGrdComponentTypes ")
                sb.Append("WHERE GrdComponentTypeId = ? ")
                sb.Append(" AND ModDate = ? ;")
                sb.Append("SELECT count(*) FROM saProgramVersionFees WHERE PrgVerFeeId = ? ")

                db.AddParameter("@GrdComponentTypeId", GrdComponentTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@GrdComponentTypeId", GrdComponentTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)
                '   If the row was not deleted then there was a concurrency problem
                If rowCount = 0 Then
                    Return "" '   return without errors
                Else
                    Return DALExceptions.BuildConcurrencyExceptionMessage()
                End If
            Catch ex As OleDbException
                Return DALExceptions.BuildErrorMessage(ex) '   return an error to the client
            Finally
                db.CloseConnection() 'Close Connection
            End Try
        End Function
#End Region

#Region "GradeBook Weightings"
        ''' <summary>
        ''' Returnes the Gradebook Weightings
        ''' </summary>
        ''' <param name="InstructorId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetGrdBkWgts(ByVal CourseId As String, ByVal InstructorId As String,
                                    ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            sb.Append("select " + vbCrLf)
            sb.Append("     GBW.InstrGrdBkWgtId as ID, " + vbCrLf)
            If CourseId IsNot Nothing AndAlso CourseId <> "" Then
                sb.Append("     convert(varchar(10),GBW.EffectiveDate,101) as Descrip, " + vbCrLf)
            Else

                sb.Append("     GBW.Descrip as Descrip, " + vbCrLf)
            End If

            sb.Append("     (case when GBW.StatusId=(select StatusId from syStatuses where Status='Active') then 1 else 0 end) as Active, " + vbCrLf)
            sb.Append("     GBW.ModDate, " + vbCrLf)
            sb.Append("     GBW.ModUser " + vbCrLf)
            sb.Append("from " + vbCrLf)
            sb.Append("     arGrdBkWeights GBW " + vbCrLf)

            Dim sbWhere As New StringBuilder
            If CourseId IsNot Nothing AndAlso CourseId <> "" Then
                If sbWhere.Length > 0 Then sbWhere.Append("and")
                sbWhere.AppendFormat("     GBW.ReqId = '{0}' {1}", CourseId, vbCrLf)
            End If
            If InstructorId IsNot Nothing AndAlso InstructorId <> "" Then
                If sbWhere.Length > 0 Then sbWhere.Append("and")
                sbWhere.AppendFormat("     GBW.InstructorId = '{0}' {1}", InstructorId, vbCrLf)
            End If
            If ShowActive And Not ShowInactive Then
                If sbWhere.Length > 0 Then sbWhere.Append("and")
                sbWhere.Append("     GBW.StatusId = (select StatusId from syStatuses where Status='Active') " & vbCrLf)
            ElseIf Not ShowActive And ShowInactive Then
                If sbWhere.Length > 0 Then sbWhere.Append("and")
                sbWhere.Append("     GBW.StatusId = (select StatusId from syStatuses where Status='Inactive') " & vbCrLf)
            End If

            ' add the where clause
            If sbWhere.Length > 0 Then
                sb.Append("where " + vbCrLf)
                sb.Append(sbWhere.ToString)
            End If

            sb.Append("order by " + vbCrLf)
            sb.Append("     GBW.Descrip, GBW.EffectiveDate ")

            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieves an info object representing a grade book weighting 
        ''' for the supplied InstrGrdBkWgtId
        ''' </summary>
        ''' <param name="Id"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetGrdBookWgtsInfo(ByVal Id As String) As GrdBkWgtsInfo
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT Distinct" + vbCrLf)
            sb.Append("     GBW.InstrGrdBkWgtId, " + vbCrLf)
            sb.Append("     GBW.InstructorId, " + vbCrLf)
            sb.Append("     GBW.Descrip, " + vbCrLf)
            sb.Append("     (case when GBW.StatusId=(select Distinct StatusId from syStatuses where Status='Active') then 1 else 0 end) as Active, " + vbCrLf)
            sb.Append("     GBW.ReqId, " + vbCrLf)
            sb.Append("     GBW.EffectiveDate, " + vbCrLf)
            sb.Append("     GBW.ModUser, " + vbCrLf)
            sb.Append("     GBW.ModDate " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("       arGrdBkWeights GBW " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.AppendFormat("		GBW.InstrGrdBkWgtId = '{0}' {1}", Id, vbCrLf)

            '   Execute the query
            Try
                Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString) '.RunParamSQLDataReader(sb.ToString)
                Dim info As New GrdBkWgtsInfo
                If dr.Read() Then
                    '   set properties with data from DataReader
                    info.GrdBkWgtId = Id
                    info.IsInDb = True
                    If Not (dr("InstructorId") Is System.DBNull.Value) Then info.InstructorId = dr("InstructorId").ToString()
                    If Not (dr("Descrip") Is System.DBNull.Value) Then info.Descrip = dr("Descrip")
                    If Not (dr("ReqId") Is System.DBNull.Value) Then info.ReqId = dr("ReqId").ToString()
                    If Not (dr("EffectiveDate") Is System.DBNull.Value) Then info.EffectiveDate = dr("EffectiveDate")
                    If Not (dr("Active") Is System.DBNull.Value) Then info.Active = dr("Active")
                    If Not (dr("ModUser") Is System.DBNull.Value) Then info.ModUser = dr("ModUser").ToString()
                    If Not (dr("ModDate") Is System.DBNull.Value) Then info.ModDate = dr("ModDate")
                    info.DetailsDT = GetGrdBkWgtsDetails(Id)
                End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                Return info
            Catch ex As System.Exception
            End Try
            Return Nothing
        End Function
        ''' <summary>
        ''' Returns the details for a GrdBkWgtId
        ''' </summary>
        ''' <param name="ID"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetGrdBkWgtsDetails(ByVal ID As String) As DataTable
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     ISNULL(GD.InstrGrdBkWgtDetailId,newid()) AS ID, " + vbCrLf)
            sb.Append("     ISNULL(GD.Code,GC.Code) AS Code, " + vbCrLf)
            sb.Append("     GC.Descrip, " + vbCrLf)
            sb.Append("     GD.Weight, " + vbCrLf)
            sb.Append("     GD.Parameter, " + vbCrLf)
            sb.Append("     GD.Number, " + vbCrLf)
            sb.Append("     GD.GrdPolicyId, " + vbCrLf)
            sb.Append("     Case when GD.Required=1 then 'True' ELSE 'False' END AS Required, " + vbCrLf)
            sb.Append("     Case when GD.MustPass=1 then 'True' ELSE 'False' END AS MustPass,  " + vbCrLf)
            sb.Append("     GC.GrdComponentTypeId, " + vbCrLf)
            sb.Append("     GC.SysComponentTypeId, " + vbCrLf)
            sb.Append("     GD.ModDate, " + vbCrLf)
            sb.Append("     GD.ModUser, " + vbCrLf)
            sb.Append("     GD.CreditsPerService, " + vbCrLf)
            sb.Append("     SR.Resource AS ComponentType " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arGrdComponentTypes GC  LEFT JOIN arGrdBkWgtDetails GD ON GC.GrdComponentTypeId = GD.GrdComponentTypeId " + vbCrLf)
            sb.Append("     JOIN syresources SR ON SR.ResourceID = GC.SysComponentTypeId " + vbCrLf)
            If ID IsNot Nothing AndAlso ID <> "" Then
                sb.AppendFormat("     WHERE GD.InstrGrdBkWgtId = '{0}' {1}", ID, vbCrLf)
            End If
            sb.Append("ORDER BY " + vbCrLf)
            sb.Append("          GD.Number desc,GD.Weight DESC, GC.Descrip " + vbCrLf)

            Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
        End Function

        ''' <summary>
        ''' Returns an empty grade book weighting by looking at the grade book componenets
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetEmptyGrdBkWgtDetails(ByVal CampusId As String) As DataTable
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            sb.Append("select " + vbCrLf)
            sb.Append("     newid() as ID, " + vbCrLf)
            sb.Append("     GC.Code as Code, " + vbCrLf)
            sb.Append("     GC.Descrip, " + vbCrLf)
            sb.Append("     null as Weight, " + vbCrLf)
            sb.Append("     null as Parameter, " + vbCrLf)
            sb.Append("     null Number, " + vbCrLf)
            sb.Append("     null GrdPolicyId, " + vbCrLf)
            sb.Append("     GC.GrdComponentTypeId, " + vbCrLf)
            sb.Append("     GC.SysComponentTypeId, " + vbCrLf)
            sb.Append("     0 as Required, " + vbCrLf)
            sb.Append("     0 as MustPass, " + vbCrLf)
            sb.Append("     null as ModDate, " + vbCrLf)
            sb.Append("     null ModUser, " + vbCrLf)
            sb.Append("     null as CreditsPerService, " + vbCrLf)
            sb.Append("     R.Resource AS ComponentType " + vbCrLf)
            sb.Append("from " + vbCrLf)
            sb.Append("     arGrdComponentTypes GC, syStatuses S, syResources R " + vbCrLf)
            sb.Append("where " + vbCrLf)
            sb.Append("GC.SysComponentTypeId = R.ResourceID and" + vbCrLf)
            sb.Append("     GC.StatusId=S.StatusId and S.Status='Active' " + vbCrLf)
            sb.Append(" and GC.SysComponentTypeId <> 612 " + vbCrLf)
            sb.AppendFormat("     and GC.CampGrpId in (Select t.CampGrpId from syCmpGrpCmps t where CampusId = '{0}') {1}", CampusId, vbCrLf)
            sb.Append("order by " + vbCrLf)
            sb.Append("     GC.Descrip " + vbCrLf)

            Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
        End Function

        ''' <summary>
        ''' Adds a grade book weight object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function AddGrdBkWgt(ByVal info As GrdBkWgtsInfo, ByVal user As String) As String
            Dim msg As String
            ' check for duplicate
            If IsDuplicateGrdBkWgt(info.GrdBkWgtId, info.EffectiveDate, info.ReqId) Then
                Return "Failed to save grade book weighting because the effective date already exists."
            End If

            Try
                Dim MyAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If

                Dim db As New DataAccess
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

                db.AddParameter("@GrdBkWgtId", info.GrdBkWgtId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@InstructorId", info.InstructorId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 80, ParameterDirection.Input)
                db.AddParameter("@StatusFlag", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                If info.ReqId Is Nothing Or info.ReqId = "" Then
                    db.AddParameter("@ReqId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ReqId", info.ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.EffectiveDate = DateTime.MinValue Then
                    db.AddParameter("@EffectiveDate", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Else
                    db.AddParameter("@EffectiveDate", info.EffectiveDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If

                Dim ds As New DataSet
                ds = db.RunParamSQLDataSetUsingSP("usp_arGrdBkWeights_Insert")

                db.ClearParameters()
                db.CloseConnection()

                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    msg = "Notice : Some existing class sections will use these grade book weights"
                    For Each dr As DataRow In ds.Tables(0).Rows
                        msg &= vbCrLf & "   " & dr.Item("ClsSection").ToString
                    Next
                End If

                For i As Integer = 0 To info.Details().Length - 1
                    ' delete the detail if there was nothing added
                    If info.Details(i).weight = 0.0 AndAlso info.Details(i).Number = 0 AndAlso
                        info.Details(i).Parameter = 0 Then
                        DeleteGrdBkWgtDetail(info.Details(i).GrdBkWgtDetailId)
                    ElseIf info.Details(i).ModUser = "" Then
                        AddGrdBkWgtDetail(info.Details(i), user)
                    Else
                        UpdateGrdBkWgtDetail(info.Details(i), user)
                    End If
                Next

            Catch ex As Exception
                Return "Error inserting grade book weight - " & ex.Message
            End Try

            Return msg

        End Function

        ''' <summary>
        ''' Update a grade book weight object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function UpdateGrdBkWgt(ByVal info As GrdBkWgtsInfo, ByVal user As String) As String
            ' check for duplicate, cannot have dupe effective dates for the given reqid
            If IsDuplicateGrdBkWgt(info.GrdBkWgtId, info.EffectiveDate, info.ReqId) Then
                Return "Failed to save grade book weighting because the effective date already exists."
            End If

            ' check if the grade book weighting is already in use
            If IsGrdBkWgtInUse(info.GrdBkWgtId) And user <> "sa" Then
                Return "Cannot make changes to the grade book weighting because it is in use."
            End If

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sb As New System.Text.StringBuilder
                sb.Append("UPDATE " + vbCrLf)
                sb.Append("     arGrdBkWeights" + vbCrLf)
                sb.Append("SET " + vbCrLf)
                sb.Append("     Descrip = ?," + vbCrLf)
                If info.Active Then
                    sb.Append("     StatusId = (select StatusId from syStatuses where Status='Active'), " + vbCrLf)
                Else
                    sb.Append("     StatusId = (select StatusId from syStatuses where Status='Inactive'), " + vbCrLf)
                End If
                sb.Append("     EffectiveDate=?, " + vbCrLf)
                sb.Append("     InstructorId=?, " + vbCrLf)
                sb.Append("     ModUser=?, " + vbCrLf)
                sb.Append("     ModDate=?, " + vbCrLf)
                sb.Append("     ReqId=? " + vbCrLf)
                sb.Append("WHERE " + vbCrLf)
                sb.Append("     InstrGrdBkWgtId=? ")

                db.AddParameter("@descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@EffectiveDate", info.EffectiveDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.InstructorId Is Nothing Or info.InstructorId = "" Then
                    db.AddParameter("@instructor", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@instructor", info.InstructorId, DataAccess.OleDbDataType.OleDbString, 135, ParameterDirection.Input)
                End If

                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If info.ReqId Is Nothing Or info.ReqId = "" Then
                    db.AddParameter("@reqid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@reqid", info.ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@grdbkwgtid", info.GrdBkWgtId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                ' perform the udpate on the top level grade book weighting
                db.RunParamSQLExecuteNoneQuery(sb.ToString)

                ' Add or update the details
                For i As Integer = 0 To info.Details().Length - 1
                    ' delete the detail if there was nothing added
                    If info.Details(i).weight = 0.0 AndAlso info.Details(i).Number = 0 AndAlso
                        info.Details(i).Parameter = 0 Then
                        DeleteGrdBkWgtDetail(info.Details(i).GrdBkWgtDetailId)

                    ElseIf info.Details(i).ModUser = "" Then
                        AddGrdBkWgtDetail(info.Details(i), user)
                    Else
                        UpdateGrdBkWgtDetail(info.Details(i), user)
                    End If
                Next
                Return "" ' success
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function

        ''' <summary>
        ''' Deletes a grade book weight
        ''' </summary>
        ''' <param name="GrdBkWgtId"></param>
        ''' <param name="modDate"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DeleteGrdBkWgt(ByVal GrdBkWgtId As String, ByVal modDate As DateTime) As String
            ' make sure we don't delete something that is already being used
            If IsGrdBkWgtInUse(GrdBkWgtId) Then
                Return "Cannot delete grade book weighting because it is in use."
            End If

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                ' first delete the details
                Dim res As String = DeleteAllGrdBkWgtDetails(GrdBkWgtId)
                If res <> "" Then Return res ' failed: we couldn't delete the details

                Dim sb As New StringBuilder
                sb.Append("DELETE FROM arGrdBkWeights WHERE InstrGrdBkWgtId =? ;")
                sb.Append("SELECT count(*) FROM arGrdBkWeights WHERE InstrGrdBkWgtId = ? ")
                db.AddParameter("@grdbkwgtid", GrdBkWgtId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@grdbkwgtid2", GrdBkWgtId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)
                '   If the row was not deleted then there was a concurrency problem
                If rowCount = 0 Then
                    '   return without errors
                    Return ""
                Else
                    Return DALExceptions.BuildConcurrencyExceptionMessage()
                End If

            Catch ex As OleDbException
                '   return an error to the client
                Return DALExceptions.BuildErrorMessage(ex)
            Finally
                'Close Connection
                db.CloseConnection()
            End Try
            Return "Unspecified Error"
        End Function

        ''' <summary>
        ''' Delete a specific GrdBook Weight Detail
        ''' </summary>
        ''' <param name="GrdBkWgtDetailId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DeleteGrdBkWgtDetail(ByVal GrdBkWgtDetailId As String) As String
            ' check for a blank param and just return if it is
            If GrdBkWgtDetailId Is Nothing Or GrdBkWgtDetailId = "" Then Return ""

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                ' first delete the details
                Dim sb As New StringBuilder
                sb.AppendFormat("DELETE FROM arGrdBkWgtDetails WHERE InstrGrdBkWgtDetailId = '{0}'", GrdBkWgtDetailId)
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                Return ""
            Catch ex As OleDbException
                '   return an error to the client
                Return DALExceptions.BuildErrorMessage(ex)
            Finally
                'Close Connection
                db.CloseConnection()
            End Try
            Return "Unspecified Error"
        End Function

        ''' <summary>
        ''' Adds a grade book weigth detail object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function AddGrdBkWgtDetail(ByVal info As GrdBkWgtDetailsInfo, ByVal user As String) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            sb.Append("INSERT INTO arGrdBkWgtDetails(InstrGrdBkWgtDetailId,InstrGrdBkWgtId,Code,Descrip," + vbCrLf)
            sb.Append("             Weight,Number,Parameter,Seq,GrdComponentTypeId,GrdPolicyId,Required,MustPass,ModUser,ModDate,CreditsPerService) " + vbCrLf)
            sb.Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")

            db.AddParameter("@grdbkwgtdetid", info.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@GrdBkWgtId", info.GrdBkWgtId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@Code", info.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@Descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'If info.weight = 0 Then
            '    db.AddParameter("@weight", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'Else
            If info.GrdSysCompTypeId = 500 Or info.GrdSysCompTypeId = 503 Then
                db.AddParameter("@weight", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@weight", info.weight, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'End If
            If info.Number = 0 Then
                db.AddParameter("@number", DBNull.Value, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@number", info.Number, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
            End If
            If info.Parameter = 0 Then
                db.AddParameter("@param", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@param", info.Parameter, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            db.AddParameter("@seq", info.Seq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@GrdCompTypId", info.GrdCompTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            If info.GrdPolicyTypeId = 0 Then
                db.AddParameter("@GrdPolicyId", DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Else
                db.AddParameter("@GrdPolicyId", info.GrdPolicyTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If
            db.AddParameter("@Required", info.Required, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@MustPass", info.mustPass, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            If info.CreditsPerService = 0.0 Then
                db.AddParameter("@CreditsPerService", DBNull.Value, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Else
                db.AddParameter("@CreditsPerService", info.CreditsPerService, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            End If
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            Return ""
        End Function

        ''' <summary>
        ''' Updates a Grade Book Weight Detail object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function UpdateGrdBkWgtDetail(ByVal info As GrdBkWgtDetailsInfo, ByVal user As String) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sb As New StringBuilder
                sb.Append("UPDATE arGrdBkWgtDetails " + vbCrLf)
                sb.Append("SET " + vbCrLf)
                sb.Append("     Code = ?, " + vbCrLf)
                sb.Append("     Weight = ?, " + vbCrLf)
                sb.Append("     Number = ?, " + vbCrLf)
                sb.Append("     Parameter = ?, " + vbCrLf)
                sb.Append("     Seq = ?, " + vbCrLf)
                sb.Append("     Descrip = ?, " + vbCrLf)
                sb.Append("     GrdPolicyId = ?, " + vbCrLf)
                sb.Append("     GrdComponentTypeId = ?, Required=?,MustPass=?,ModUser = ?, ModDate = ?,CreditsPerService=? ")
                sb.Append("WHERE  InstrGrdBkWgtDetailId = ?")
                db.AddParameter("@Code", info.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'If info.weight = 0 Then
                '    db.AddParameter("@weight", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'Else
                If info.GrdSysCompTypeId = 500 Or info.GrdSysCompTypeId = 503 Then
                    db.AddParameter("@weight", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@weight", info.weight, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                'End If
                If info.Number = 0 Then
                    db.AddParameter("@number", DBNull.Value, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@number", info.Number, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                End If
                If info.Parameter = 0 Then
                    db.AddParameter("@param", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@param", info.Parameter, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@seq", info.Seq, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@Descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.GrdPolicyTypeId = 0 Then
                    db.AddParameter("@GrdPolicyId", DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                Else
                    db.AddParameter("@GrdPolicyId", info.GrdPolicyTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                End If
                db.AddParameter("@GrdCompTypId", info.GrdCompTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Required", info.Required, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@MustPass", info.mustPass, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If info.CreditsPerService = 0.0 Then
                    db.AddParameter("@CreditsPerService", DBNull.Value, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                Else
                    db.AddParameter("@CreditsPerService", info.CreditsPerService, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                End If
                db.AddParameter("@grdbkwgtdetid", info.GrdBkWgtDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                'Close Connection
                db.CloseConnection()

                Return "" ' success
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function


        ''' <summary>
        ''' Deletes all grade book weightings given an InstrGrdBkWgtId
        ''' </summary>
        ''' <param name="InstrGrdBkWgtId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DeleteAllGrdBkWgtDetails(ByVal InstrGrdBkWgtId As String) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                ' check if there are already user tasks with this taskid
                Dim sql As String = String.Format("DELETE arGrdBkWgtDetails WHERE InstrGrdBkWgtId = '{0}'; {1}", InstrGrdBkWgtId, vbCrLf)
                sql += String.Format("SELECT COUNT(*) from arGrdBkWgtDetails WHERE InstrGrdBkWgtId = '{0}'; {1}", InstrGrdBkWgtId, vbCrLf)
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                If rowCount > 0 Then
                    db.CloseConnection()
                    Return "Failed to delete grade book weight details"
                End If
                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function
        ''' <summary>
        ''' Make sure there are no duplicate grade book weights for
        ''' the suppliced GradBkWgtId, EffectiveId and ReqId
        ''' </summary>
        ''' <param name="GrdBkWgtId"></param>
        ''' <param name="EffectiveDate"></param>
        ''' <param name="ReqId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function IsDuplicateGrdBkWgt(ByVal GrdBkWgtId As String, ByVal EffectiveDate As DateTime, ByVal ReqId As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sb As New StringBuilder
                sb.Append("select count(*) from arGrdBkWeights ")
                sb.Append("where EffectiveDate = ? and ReqId = ? and InstrGrdBkWgtId <> ? ")

                db.AddParameter("@EffectiveDate", EffectiveDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ReqId", ReqId, DataAccess.OleDbDataType.OleDbChar, 50, ParameterDirection.Input)
                db.AddParameter("@GrdBkWgtId", GrdBkWgtId, DataAccess.OleDbDataType.OleDbChar, 50, ParameterDirection.Input)

                '   execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)
                '   If the row was not deleted then there was a concurrency problem
                Return (rowCount > 0)
            Catch ex As System.Exception
                Return True
            Finally
                db.CloseConnection() 'Close Connection
            End Try
        End Function
        ''' <summary>
        ''' Determines if the grade book weights are current in use.
        ''' This can be used to see if we can make any changes
        ''' </summary>
        ''' <param name="GrdBkWgtId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function IsGrdBkWgtInUse(ByVal GrdBkWgtId As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sb As New StringBuilder
                sb.Append("select " + vbCrLf)
                sb.Append("     count(*) " + vbCrLf)
                sb.Append("from " + vbCrLf)
                sb.Append("     arGrdBkResults GBR, arGrdBkWgtDetails GD " + vbCrLf)
                sb.Append("where " + vbCrLf)
                sb.Append("     GBR.InstrGrdBkWgtDetailId = GD.InstrGrdBkWgtDetailId " + vbCrLf)
                sb.AppendFormat("       and GD.InstrGrdBkWgtId = '{0}' {1}", GrdBkWgtId, vbCrLf)

                '   execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)
                '   If the row was not deleted then there was a concurrency problem
                Return (rowCount > 0)
            Catch ex As System.Exception
                Return True
            Finally
                db.CloseConnection() 'Close Connection
            End Try
        End Function
#End Region

    End Class
#End Region

#Region "Courses"
    Public Class CoursesDB
        ''' <summary>
        ''' Get all Courses
        ''' </summary>
        ''' <param name="ShowActive"></param>
        ''' <param name="ShowInactive"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAll(ByVal CampGrpId As String, ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     R.ReqId as Id, " & vbCrLf)
            sb.Append("     R.Code as Code, " & vbCrLf)
            sb.Append("     R.Descrip as Descrip, " & vbCrLf)
            sb.Append("     (case when R.StatusId=(select StatusId from syStatuses where Status='Active') then 1 else 0 end) as Active, " + vbCrLf)
            sb.Append("     R.ModUser, " + vbCrLf)
            sb.Append("     R.ModDate " + vbCrLf)
            sb.Append("FROM arReqs R " & vbCrLf)
            If ShowActive And Not ShowInactive Then
                sb.Append("WHERE " & vbCrLf)
                sb.Append("     R.StatusId = (select StatusId from syStatuses where Status='Active') " & vbCrLf)
            ElseIf Not ShowActive And ShowInactive Then
                sb.Append("WHERE " & vbCrLf)
                sb.Append("     R.StatusId = (select StatusId from syStatuses where Status='Inactive') " & vbCrLf)
            End If
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append("     R.Descrip asc ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieve a CourseInfo object from the database given a ReqId
        ''' </summary>
        ''' <param name="ReqId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal ReqId As String) As CourseInfo
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build the sql query
            Dim sb As New StringBuilder
            sb.Append("select " + vbCrLf)
            sb.Append("     R.ReqId, " & vbCrLf)
            sb.Append("     R.Code, " & vbCrLf)
            sb.Append("     R.Descrip, " & vbCrLf)
            sb.Append("     (case when R.StatusId = (select StatusId from syStatuses where Status='Active') then 1 else 0 end) as Active, " & vbCrLf)
            sb.Append("     R.ModUser, " & vbCrLf)
            sb.Append("     R.ModDate, " & vbCrLf)
            sb.Append("     R.Credits, " & vbCrLf)
            sb.Append("     R.IsExternship " & vbCrLf)
            sb.Append("from " + vbCrLf)
            sb.Append("     arReqs R " & vbCrLf)
            sb.Append("where " + vbCrLf)
            sb.AppendFormat("     ReqId = '{0}'", ReqId)

            '   Execute the query
            Try
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                Dim info As New CourseInfo
                If dr.Read() Then
                    '   set properties with data from DataReader
                    info.ReqId = ReqId
                    info.IsInDB = True
                    If Not (dr("Code") Is System.DBNull.Value) Then info.Code = dr("Code")
                    If Not (dr("Active") Is System.DBNull.Value) Then info.Active = dr("Active")
                    If Not (dr("Descrip") Is System.DBNull.Value) Then info.Descrip = dr("Descrip")
                    If Not (dr("Credits") Is System.DBNull.Value) Then info.Credits = dr("Credits")
                    info.IsExternship = dr("IsExternship")
                    'If Not (dr("ModUser") Is System.DBNull.Value) Then info.ModUser = dr("ModUser").ToString()
                    'If Not (dr("ModDate") Is System.DBNull.Value) Then info.ModDate = dr("ModDate")                    
                End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                Return info
            Catch ex As System.Exception
            End Try
            Return Nothing
        End Function
        Public Function GetCoursesByProgramVersion(ByVal campusId As String, ByVal prgVerId As String, ByVal statusId As String) As DataTable

            Dim ds As DataSet
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            db.AddParameter("@CampusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@PrgVerId", New Guid(prgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@StatusId", New Guid(statusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Try
                Dim dt As New DataTable()
                dt = db.RunParamSQLDataSet_SP("Sp_GetReqsForProgramVersion_Report", "getCoursesByProgramVersion").Tables(0)
                Return dt
            Catch ex As Exception
                Return Nothing
            Finally
                db.CloseConnection()
            End Try
        End Function
        Public Function GetCoursesByProgramVersionAndIgnoreSelectedCourses(ByVal campusId As String, ByVal prgVerId As String, ByVal selectedCourses As String) As DataTable

            Dim ds As DataSet
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            db.AddParameter("@CampusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@PrgVerId", New Guid(prgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@selectedCourses", selectedCourses, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            Try
                Dim dt As New DataTable()
                dt = db.RunParamSQLDataSet_SP("Sp_GetReqsForProgramVersion_Filter", "getCoursesByProgramVersionIgnoreSelectedCourses").Tables(0)
                Return dt
            Catch ex As Exception
                Return Nothing
            Finally
                db.CloseConnection()
            End Try
        End Function
        Public Function GetCoursesByCampus(ByVal campusId As String) As DataTable

            Dim ds As DataSet
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            db.AddParameter("@CampusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Try
                Dim dt As New DataTable()
                dt = db.RunParamSQLDataSet_SP("usp_coursesbycampus_getlist", "getCoursesByCampus").Tables(0)
                Return dt
            Catch ex As Exception
                Return Nothing
            Finally
                db.CloseConnection()
            End Try
        End Function
        Public Function GetStatesByCampus(ByVal campusId As String) As DataTable

            Dim ds As DataSet
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            db.AddParameter("@CampusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Try
                Dim dt As New DataTable()
                dt = db.RunParamSQLDataSet_SP("USP_GetStatesByCampus_List", "getstatesByCampus").Tables(0)
                Return dt
            Catch ex As Exception
                Return Nothing
            Finally
                db.CloseConnection()
            End Try
        End Function
        Public Function GetChargingMethodsByCampus(ByVal campusId As String) As DataTable

            Dim ds As DataSet
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            db.AddParameter("@CampusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Try
                Dim dt As New DataTable()
                dt = db.RunParamSQLDataSet_SP("USP_ChargingMethodByCampus_GetList", "getChargingMethodsByCampus").Tables(0)
                Return dt
            Catch ex As Exception
                Return Nothing
            Finally
                db.CloseConnection()
            End Try
        End Function
        Public Function GetChargingMethodsByProgramVersion(ByVal campusId As String, ByVal prgVerId As String, ByVal statusId As String) As DataTable

            Dim ds As DataSet
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            db.AddParameter("@CampusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@PrgVerId", New Guid(prgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@StatusId", New Guid(statusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Try
                Dim dt As New DataTable()
                dt = db.RunParamSQLDataSet_SP("USP_ChargingMethodByCampusAndProgramVersion_GetList", "getChargingMethodsByProgramVersion").Tables(0)
                Return dt
            Catch ex As Exception
                Return Nothing
            Finally
                db.CloseConnection()
            End Try
        End Function
        Public Function GetChargingMethodsByProgramVersionAndIgnoreSelectedChargingMethods(ByVal campusId As String, ByVal prgVerId As String, ByVal selectedCourses As String) As DataTable

            Dim ds As DataSet
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            db.AddParameter("@CampusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@PrgVerId", New Guid(prgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@selectedCourses", selectedCourses, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            Try
                Dim dt As New DataTable()
                dt = db.RunParamSQLDataSet_SP("Usp_GetChargingMethodsForProgramVersion_Filter", "getChargingMethodsByProgramVersionIgnoreSelectedCourses").Tables(0)
                Return dt
            Catch ex As Exception
                Return Nothing
            Finally
                db.CloseConnection()
            End Try
        End Function
        Public Function GetTermsByCampus(ByVal campusId As String, ByVal showActiveOnly As Boolean) As DataTable

            Dim ds As DataSet
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            db.AddParameter("@showActiveOnly", showActiveOnly, SqlDbType.Bit, , ParameterDirection.Input)
            db.AddParameter("@CampusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Try
                Dim dt As New DataTable()
                dt = db.RunParamSQLDataSet_SP("usp_GetAllTerms", "getTermsbyCampus").Tables(0)
                Return dt
            Catch ex As Exception
                Return Nothing
            Finally
                db.CloseConnection()
            End Try
        End Function
        Public Function GetEnrollmentStatusByCampus(ByVal campusId As String, ByVal showActiveOnly As Boolean) As DataTable

            Dim ds As DataSet
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            db.AddParameter("@showActiveOnly", showActiveOnly, SqlDbType.Bit, , ParameterDirection.Input)
            db.AddParameter("@CampusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Try
                Dim dt As New DataTable()
                dt = db.RunParamSQLDataSet_SP("USP_StatusCode_GET", "getEnrollmentStatusByCampus").Tables(0)
                Return dt
            Catch ex As Exception
                Return Nothing
            Finally
                db.CloseConnection()
            End Try
        End Function
        Public Function GetStudentGroupByCampus(ByVal campusId As String, ByVal showActiveOnly As Boolean) As DataTable

            Dim ds As DataSet
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            db.AddParameter("@showActiveOnly", showActiveOnly, SqlDbType.Bit, , ParameterDirection.Input)
            db.AddParameter("@CampusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Try
                Dim dt As New DataTable()
                dt = db.RunParamSQLDataSet_SP("USP_StudentGroup_GET", "getStudentGroupByCampus").Tables(0)
                Return dt
            Catch ex As Exception
                Return Nothing
            Finally
                db.CloseConnection()
            End Try
        End Function
        Public Function GetAvailableTermsAndIgnoreSelectedTerms(ByVal campusId As String, ByVal selectedTerms As String,
                                                                ByVal showActiveOnly As Boolean) As DataTable
            Dim ds As DataSet
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            db.AddParameter("@CampusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@selectedTerms", selectedTerms, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            db.AddParameter("@showActiveOnly", showActiveOnly, SqlDbType.Bit, , ParameterDirection.Input)
            Try
                Dim dt As New DataTable()
                dt = db.RunParamSQLDataSet_SP("usp_GetAllTerms_IgnoreTerms", "getTermsIgnoreSelectedTerms").Tables(0)
                Return dt
            Catch ex As Exception
                Return Nothing
            Finally
                db.CloseConnection()
            End Try
        End Function
        Public Function GetAvailableEnrollmentStatusAndIgnoreSelectedStatus(ByVal campusId As String, ByVal selectedStatus As String,
                                                                ByVal showActiveOnly As Boolean) As DataTable
            Dim ds As DataSet
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            db.AddParameter("@CampusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@showActiveOnly", showActiveOnly, SqlDbType.Bit, , ParameterDirection.Input)
            db.AddParameter("@selectedStatus", selectedStatus, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            Try
                Dim dt As New DataTable()
                dt = db.RunParamSQLDataSet_SP("USP_StatusCode_GET_IgnoreSelected", "getAllEnrollmentStatusIgnoreSelectedStatus").Tables(0)
                Return dt
            Catch ex As Exception
                Return Nothing
            Finally
                db.CloseConnection()
            End Try
        End Function
        Public Function GetAvailableStudentGroupsAndIgnoreSelectedGroups(ByVal campusId As String, ByVal selectedStudentGroups As String,
                                                                ByVal showActiveOnly As Boolean) As DataTable
            Dim ds As DataSet
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            db.AddParameter("@CampusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@showActiveOnly", showActiveOnly, SqlDbType.Bit, , ParameterDirection.Input)
            db.AddParameter("@selectedStudentGroups", selectedStudentGroups, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            Try
                Dim dt As New DataTable()
                dt = db.RunParamSQLDataSet_SP("USP_StudentGroup_GET_IgnoreSelected", "getAllStudentGroupIgnoreSelectedGroups").Tables(0)
                Return dt
            Catch ex As Exception
                Return Nothing
            Finally
                db.CloseConnection()
            End Try
        End Function
        Public Function GetAvailableStudentsWithGpa(ByVal prgVerId As String, ByVal enrollmentStatusId As String,
                                                               ByVal studentGrpId As String) As DataTable
            Dim ds As DataSet
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
            db.AddParameter("@PrgVerId", prgVerId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            If String.IsNullOrEmpty(enrollmentStatusId) Then
                db.AddParameter("@EnrollmentStatusId", System.DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            Else
                db.AddParameter("@EnrollmentStatusId", enrollmentStatusId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            End If
            If String.IsNullOrEmpty(studentGrpId) Then
                db.AddParameter("@StudentGrpId", System.DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            Else
                db.AddParameter("@StudentGrpId", studentGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            End If

            Try
                Dim dt As New DataTable()
                dt = db.RunParamSQLDataSet_SP("USP_STUDENTGPA_GETDETAILS", "getAllStudentWithGPA").Tables(0)
                Return dt
            Catch ex As Exception
                Return Nothing
            Finally
                db.CloseConnection()
            End Try
        End Function
    End Class

#End Region

#Region "Utilities"
    Public Class UtilitiesDB
        ''' <summary>
        ''' Returns a PrgVerId from a StuEnrollId
        ''' </summary>
        ''' <param name="StuEnrollId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetPrgVerId(ByVal StuEnrollId As String) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sql As String = String.Format("select PrgVerId from arStuEnrollments where StuEnrollId='{0}'", StuEnrollId)
                Dim res As String = db.RunParamSQLScalar(sql).ToString()
                Return res
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return ""
        End Function

        ''' <summary>
        ''' Retrieves all active Campus Groups
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetCampusGroups() As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append("     CG.CampGrpId as ID, " & vbCrLf)
            sb.Append("     CG.CampGrpDescrip as Descrip " & vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     syCampGrps CG ")
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     CG.StatusId = (select StatusId from syStatuses where Status='Active') " & vbCrLf)
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append("     CG.CampGrpDescrip asc ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieves all active departments
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetDepartments() As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     D.DeptId as ID, " & vbCrLf)
            sb.Append("     D.DeptCode as Code, " & vbCrLf)
            sb.Append("     D.DeptDescrip as Descrip, " & vbCrLf)
            sb.Append("     '(' + D.DeptCode + ') ' + D.DeptDescrip as Descrip2" + vbCrLf)
            sb.Append("FROM arDepartments D " & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            sb.Append("     D.StatusId = (select StatusId from syStatuses where Status='Active') " & vbCrLf)
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append("     D.DeptDescrip asc ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieves all active charging methods
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetChargingMethods() As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     BM.BillingMethodId as ID, " & vbCrLf)
            sb.Append("     BM.BillingMethodCode as Code, " & vbCrLf)
            sb.Append("     BM.BillingMethodDescrip as Descrip, " & vbCrLf)
            sb.Append("     '(' + BM.BillingMethodCode + ') ' + BM.BillingMethodDescrip as Descrip2" + vbCrLf)
            sb.Append("FROM saBillingMethods BM " & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            sb.Append("     BM.StatusId = (select StatusId from syStatuses where Status='Active') " & vbCrLf)
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append("     BM.BillingMethodDescrip asc ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieves all active SAP policies
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetSAPPolcies() As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     S.SAPId as ID, " & vbCrLf)
            sb.Append("     S.SAPCode as Code, " & vbCrLf)
            sb.Append("     S.SAPDescrip as Descrip, " & vbCrLf)
            sb.Append("     '(' + S.SAPCode + ') ' + S.SAPDescrip as Descrip2" + vbCrLf)
            sb.Append("FROM arSAP S " & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            sb.Append("     S.StatusId = (select StatusId from syStatuses where Status='Active') " & vbCrLf)
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append("     S.SAPDescrip asc ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieves all active grade systems
        ''' </summary>
        ''' <param name="CampGrpId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetGradeSystems(ByVal CampGrpId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     G.GrdSystemId as ID, " & vbCrLf)
            sb.Append("     G.Descrip as Descrip " & vbCrLf)
            sb.Append("FROM arGradeSystems G " & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            sb.Append("     G.StatusId = (select StatusId from syStatuses where Status='Active') " & vbCrLf)
            If CampGrpId IsNot Nothing AndAlso CampGrpId <> "" Then
                sb.AppendFormat("     AND G.CampGrpId = '{0}' {1}", CampGrpId, vbCrLf)
            End If
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append("     G.Descrip asc ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieves all active program types
        ''' </summary>
        ''' <param name="CampGrpId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetProgramTypes(ByVal CampGrpId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     P.ProgTypId as ID, " & vbCrLf)
            sb.Append("     P.Code as Code, " & vbCrLf)
            sb.Append("     P.Description as Descrip, " & vbCrLf)
            sb.Append("     '(' + P.Code + ') ' + P.Description as Descrip2" + vbCrLf)
            sb.Append("FROM arProgTypes P " & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            sb.Append("     P.StatusId = (select StatusId from syStatuses where Status='Active') " & vbCrLf)
            If CampGrpId IsNot Nothing AndAlso CampGrpId <> "" Then
                sb.AppendFormat("     AND P.CampGrpId = '{0}' {1}", CampGrpId, vbCrLf)
            End If
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append("     P.Description asc ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieves all active tution earnings
        ''' </summary>
        ''' <param name="CampGrpId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetTuitionEarnings(ByVal CampGrpId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     T.TuitionEarningId as ID, " & vbCrLf)
            sb.Append("     T.TuitionEarningsCode as Code, " & vbCrLf)
            sb.Append("     T.TuitionEarningsDescrip as Descrip, " & vbCrLf)
            sb.Append("     '(' + T.TuitionEarningsCode + ') ' + T.TuitionEarningsDescrip as Descrip2" + vbCrLf)
            sb.Append("FROM saTuitionEarnings T " & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            sb.Append("     T.StatusId = (select StatusId from syStatuses where Status='Active') " & vbCrLf)
            If CampGrpId IsNot Nothing AndAlso CampGrpId <> "" Then
                sb.AppendFormat("     AND T.CampGrpId = '{0}' {1}", CampGrpId, vbCrLf)
            End If
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append("     T.TuitionEarningsDescrip asc ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieves all active testing models
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetTestingModels() As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     G.GrdSystemId as ID, " & vbCrLf)
            sb.Append("     G.Descrip as Descrip, " & vbCrLf)
            sb.Append("     '(' + S.SAPCode + ') ' + S.SAPDescrip as Descrip2" + vbCrLf)
            sb.Append("FROM arGradeSystems G " & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            sb.Append("     G.StatusId = (select StatusId from syStatuses where Status='Active') " & vbCrLf)
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append("     G.Descrip asc ")
            Return Nothing
            'Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Returns all the attendance unit types from syAttendanceUnitTypes
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAttendanceUnitTypes() As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append("     A.UnitTypeId as ID, " & vbCrLf)
            sb.Append("     A.Descrip as Descrip, " & vbCrLf)
            sb.Append("     A.Descrip as Descrip2" + vbCrLf)
            sb.Append("FROM syAttendanceUnitTypes A " & vbCrLf)
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append("     A.Descrip asc ")
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
    End Class
#End Region

End Namespace