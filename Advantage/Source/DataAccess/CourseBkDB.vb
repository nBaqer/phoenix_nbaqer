
Imports System.Data
Imports System.Web
Imports FAME.Advantage.Common

Public Class CourseBkDB

    Public Function GetBksNotAssigned(ByVal CourseId As String) As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet

        'Set the connection string


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append("SELECT t1.BkId,t1.BkTitle ")
            .Append("FROM arBooks t1 ")
            .Append("WHERE NOT EXISTS ")
            .Append("(SELECT t2.BkId FROM arCourseBks t2 where t2.CourseId = ? and t2.BkId = t1.BkId)")
            .Append("ORDER BY t1.BkTitle")
        End With
        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@CourseId", CourseId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds.Tables(0)
    End Function
    Public Function GetBksAssgd(ByVal CourseId As String) As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sb As New System.Text.StringBuilder


        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'Build query to obtain course/coursegrpids and course/coursegrpdescrips assgd to a prog ver.
        With sb
            .Append("SELECT t1.BkId,IsReq,t2.BkTitle ")
            .Append("FROM arCourseBks t1, arBooks t2 ")
            .Append("WHERE t1.BkId = t2.BkId AND t1.CourseId = ? ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@CourseId", CourseId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the datatable
        Return ds.Tables(0)
    End Function

    Public Function LoadDS(ByVal CourseId As String) As DataSet

        Dim LoadedDS As New DataSet
        Dim dt1 As New DataTable
        Dim dt2 As New DataTable
        Dim count As Integer


        dt1 = GetBksNotAssigned(CourseId).Copy
        count = dt1.Rows.Count
        dt1.TableName = "AvailBks"
        'Make the ChildId column the primary key
        With dt1
            .PrimaryKey = New DataColumn() {.Columns("BkId")}
        End With
        LoadedDS.Tables.Add(dt1)


        dt2 = GetBksAssgd(CourseId).Copy
        count = dt1.Rows.Count
        dt2.TableName = "SelectBks"
        'Make the ChildId column the primary key
        With dt2
            .PrimaryKey = New DataColumn() {.Columns("BkId")}
        End With
        LoadedDS.Tables.Add(dt2)


        'return dataset to facade
        Return LoadedDS

    End Function

    Public Function InsertCourseBkToDB(ByVal CourseBkObj As CourseBkInfo, ByVal CourseID As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim BookId As String
        Dim StartDate As DateTime
        Dim EndDate As DateTime
        Dim isReq As Boolean


        BookId = CourseBkObj.BookId
        StartDate = CourseBkObj.StartDate
        EndDate = CourseBkObj.EndDate
        isReq = CourseBkObj.IsReq

        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("INSERT INTO arCourseBks(CourseId,BkId,isReq) ")
            .Append("VALUES(?,?,?)")

            db.AddParameter("@courseid", CourseID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@bookid", BookId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@isreq", isReq, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)

        End With

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function

    Public Function UpdateCourseBkInDB(ByVal CourseBkObj As CourseBkInfo, ByVal CourseID As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim childID As String
        Dim childTyp As String
        Dim childSeq As Integer
        Dim req As Integer

       

        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")


        With sb
            .Append("UPDATE arCourseBks Set isReq = ? ")
            .Append("WHERE  BkId = ? and CourseId = ?")


            db.AddParameter("@isreq", CourseBkObj.IsReq, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
            db.AddParameter("@bookid", CourseBkObj.BookId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@courseid", CourseID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        End With

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function

    Public Function DeleteCourseBkFrmDB(ByVal CourseBkObj As CourseBkInfo, ByVal CourseID As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet


        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("DELETE FROM arCourseBks ")
            .Append("WHERE CourseId = ? ")
            .Append("AND BkId = ? ")
        End With

        db.AddParameter("@courseid", CourseID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@transcodeid", CourseBkObj.BookId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function
#Region "Get AdvAppsetting for Manage Config entry"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
End Class
