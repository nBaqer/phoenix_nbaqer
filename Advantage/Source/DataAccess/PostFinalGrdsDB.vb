Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class PostFinalGrdsDB
    Public Function GetAllTerms() As DataSet
        Dim ds As DataSet

        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.TermId,t1.TermDescrip ")
                .Append("FROM arTerm t1  ")
                '.Append("WHERE t2.Status = 'Active' and t1.StatusId = t2.StatusId ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetInstructorsForTerm(ByVal term As String, ByVal campusId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT DISTINCT a.InstructorId, b.FullName ")
            .Append("FROM arClassSections a, syUsers b ")
            .Append("WHERE a.TermId = ? and a.InstructorId = b.UserId ")
            .Append("AND a.CampusId = ? order by b.Fullname ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@TermId", term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "InstructorDT")


        Finally
            'Close Connection
            db.CloseConnection()

        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetClsSects(ByVal Term As String, ByVal Instructor As String, ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        Try
            '   connect to the database

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query

            'End With
            With sb
                .Append("SELECT c.ClsSectionId, a.ReqId, a.ClsSection, '(' + b.code + ') ' +  b.Descrip as Descrip, b.code, b.Descrip AS ShortDesc,a.InstrGrdBkWgtId  ")
                .Append("FROM arClassSections a, arReqs b, arClassSectionTerms c ")
                .Append("WHERE c.TermId = ? and c.ClsSectionId = a.ClsSectionId ")

                If (String.IsNullOrEmpty(Instructor)) Then
                    .Append("and a.ReqId = b.ReqId  ")
                Else
                    .Append("and a.ReqId = b.ReqId and a.InstructorId = ? ")
                End If

                .Append("and a.CampusId = ? ")
                'Code modified to fix issue 15545
                'The class section dropdown in Post Final Grades should not show Externship classes
                'modification starts here
                '.Append(" and b.IsExternship <> 1 ")
                'modification ends here 
                .Append(" group by c.ClsSectionId, a.ReqId, a.ClsSection, '(' + b.code + ') ' +  b.Descrip,b.code, b.Descrip, a.InstrGrdBkWgtId  ")
                .Append(" order by b.Descrip, b.code ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@TermId", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If (Not String.IsNullOrEmpty(Instructor)) Then
                db.AddParameter("@InstrId", Instructor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")



        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'If the course has only lab work and lab hours then remove the course from 
        ' the dataset - Mantis #15200
        Try
            Dim dtCourses As DataTable = ds.Tables(0)
            Dim dtTransferGrade As New TransferGradeDB
            If dtCourses.Rows.Count > 0 Then
                For Each row As DataRow In dtCourses.Rows
                    If (dtTransferGrade.IsCourseALab(row("ReqId").ToString, row("InstrGrdBkWgtId").ToString) = True) Then
                        row.Delete()
                    End If
                Next
            End If
            ds.AcceptChanges()
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetTheoryHours(ByVal reqID As String) As Boolean
        Dim count As Integer
        Dim theoryCount As Integer

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append(" select  count(GBW.InstrGrdBkWgtId) as ID  from arGrdBkWeights GBW   where  GBW.ReqId = ?  and   ")
            .Append("  GBW.StatusId = (select StatusId from syStatuses where Status='Active') ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@ReqId", reqID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        '   Execute the query
        db.OpenConnection()
        Try
            count = db.RunParamSQLScalar(sb.ToString)
            sb.Remove(0, sb.Length)
            db.ClearParameters()


            'Close Connection
            If count > 0 Then
                With sb
                    .Append(" select      count(*) as TheoryCount  from    ")
                    .Append(" arGrdComponentTypes GC, arGrdBkWgtDetails GD  where      GC.GrdComponentTypeId = GD.GrdComponentTypeId  ")
                    '.Append(" and SysComponentTypeid not in(500,503)  
                    .Append(" and number is not null    and weight is not null  and GD.InstrGrdBkWgtId in ")
                    .Append(" (select  GBW.InstrGrdBkWgtId as ID  from arGrdBkWeights GBW   where  GBW.ReqId = ?  ")
                    .Append(" and     GBW.StatusId = (select StatusId from syStatuses where Status='Active') ) ")
                End With
                db.AddParameter("@ReqId", reqID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                theoryCount = db.RunParamSQLScalar(sb.ToString)
                If theoryCount = 0 Then
                    Return False
                Else
                    Return True
                End If
            Else
                Return True
            End If

        Finally
            db.CloseConnection()
        End Try
    End Function

    Public Function GetStudentInfoForFinalGrades(ByVal clsSect As String, ByVal userId As String) As DataSet

        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim ds2 As New DataSet



        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@ClsSectId", clsSect, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSetUsingSP("dbo.GetStudentsForPostFinalGrades")
            db.ClearParameters()

            db.AddParameter("@ClsSectId", clsSect, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ds2 = db.RunParamSQLDataSetUsingSP("dbo.GetGradesForFinalGrades")
            ds.Tables(0).TableName = "ClsSectStds"

            Dim dt As DataTable = ds2.Tables(0)
            ds.Tables.Add(dt.Copy())

            ds.Tables(1).TableName = "ClsSectGrds"
            Return ds

        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try


    End Function



    Public Function GetGradeSystemDetails(ByVal req As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        '   connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT d.Grade,d.GrdSysDetailId ")
            .Append("FROM arClassSections a, arGradeScales b,arGradeSystems c,arGradeSystemDetails d ")
            .Append("WHERE a.ClsSectionId = ? ")
            .Append("AND a.GrdScaleId = b.GrdScaleId ")
            .Append("AND b.GrdSystemId = c.GrdSystemId ")
            .Append("AND c.GrdSystemId = d.GrdSystemId ")
            .Append("ORDER BY d.Grade ")
        End With

        db.AddParameter("@clssectId", req, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "GradeSysDetails")
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetContinuingEDGrades(ByVal prgVerid As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            '   connect to the database
            Dim db As New DataAccess

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            With sb
                .Append(" SELECT d.Grade,d.GrdSysDetailId ")
                .Append(" FROM arPrgVersions a ")
                .Append(" ,arGradeSystems c,arGradeSystemDetails d ")
                .Append(" WHERE a.PrgVerId = ? ")
                .Append(" AND a.GrdSystemId = c.GrdSystemId ")
                .Append(" AND c.GrdSystemId = d.GrdSystemId ")
                .Append(" ORDER BY d.Grade ")
            End With

            db.AddParameter("@prgVerId", prgVerid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "GradeSysDetails")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try
        'Return the datatable in the dataset
        Return ds

    End Function

    Public Sub UpdateFinalGradePerEnrollments(ByVal listOfUpdateOperationsFormatNumeric As List(Of GrdRecordsPoco), ByVal listOfUpdateOperationsFormatNotNumeric As List(Of GrdRecordsPoco))

        'Queries to implement
        Dim sql As StringBuilder = New StringBuilder()

        Dim sqlincom As String = "UPDATE dbo.arResults SET IsInComplete = NULL WHERE TestId = @ClassId1 AND StuEnrollId = @EnrollId1"

        With sql
            .Append("UPDATE  dbo.arResults ")
            .Append("SET     Score = @Score ")
            .Append("       ,IsCourseCompleted = @IsCourseComplete ")
            .Append("      ,isClinicsSatisfied = @isClinicsSatisfied ")
            .Append("	   ,GrdSysDetailId = @GradeId ")
            .Append("	   ,ModUser = @UserName ")
            .Append("	   ,ModDate = GETDATE() ")
            .Append("	   ,IsGradeOverridden = 1 ")
            .Append("	   ,GradeOverriddenBy = @UserName ")
            .Append("	   ,GradeOverriddenDate = GETDATE()
                           ,DateCompleted = @dateCompleted ")
            .Append("WHERE   TestId = @ClassId ")
            .Append("        AND StuEnrollId = @EnrollId ")
        End With

        'Create the connection object
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim sqlconn As SqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString").ToString)

        'Create the command list
        Dim sqlCommandList As List(Of SqlCommand) = New List(Of SqlCommand)()

        For Each poco As GrdRecordsPoco In listOfUpdateOperationsFormatNumeric
            Dim command As SqlCommand = New SqlCommand(sql.ToString(), sqlconn)
            command.Parameters.AddWithValue("@ClassId", poco.ClsSectionId)
            command.Parameters.AddWithValue("@EnrollId", poco.StuEnrollId)
            command.Parameters.AddWithValue("@UserName", poco.UserName)
            command.Parameters.AddWithValue("@Score", If(IsNothing(poco.Score), DBNull.Value, poco.Score))
            command.Parameters.AddWithValue("@GradeId", DBNull.Value)
            command.Parameters.AddWithValue("@IsCourseComplete", Not IsNothing(poco.Score))
            command.Parameters.AddWithValue("@isClinicsSatisfied", If(IsNothing(poco.Score), DBNull.Value, 1))
            command.Parameters.AddWithValue("@dateCompleted", poco.DateCompleted)

            sqlCommandList.Add(command)
        Next

        For Each poco As GrdRecordsPoco In listOfUpdateOperationsFormatNotNumeric
            Dim command As SqlCommand = New SqlCommand(sql.ToString(), sqlconn)
            command.Parameters.AddWithValue("@ClassId", poco.ClsSectionId)
            command.Parameters.AddWithValue("@EnrollId", poco.StuEnrollId)
            command.Parameters.AddWithValue("@UserName", poco.UserName)
            command.Parameters.AddWithValue("@Score", DBNull.Value)
            command.Parameters.AddWithValue("@GradeId", If(String.IsNullOrWhiteSpace(poco.Grade), DBNull.Value, poco.Grade))
            command.Parameters.AddWithValue("@IsCourseComplete", Not String.IsNullOrWhiteSpace(poco.Grade))
            command.Parameters.AddWithValue("@isClinicsSatisfied", If(String.IsNullOrWhiteSpace(poco.Grade), DBNull.Value, 1))
            command.Parameters.AddWithValue("@dateCompleted", poco.DateCompleted)

            sqlCommandList.Add(command)

            If String.IsNullOrWhiteSpace(poco.Grade) Then
                Dim command1 As SqlCommand = New SqlCommand(sqlincom, sqlconn)
                command1.Parameters.AddWithValue("@ClassId1", poco.ClsSectionId)
                command1.Parameters.AddWithValue("@EnrollId1", poco.StuEnrollId)
                sqlCommandList.Add(command1)
            End If
        Next

        'Put command operations in transactions
        Dim sqlTrans As SqlTransaction
        'Open and prepare transaction
        sqlconn.Open()
        sqlTrans = sqlconn.BeginTransaction("UpdateFinalGrade")
        For Each command As SqlCommand In sqlCommandList
            command.Transaction = sqlTrans
        Next

        'Execute Transaction
        Try
            For Each command As SqlCommand In sqlCommandList
                command.ExecuteNonQuery()
            Next
            'Commit Transaction 
            sqlTrans.Commit()

        Catch ex As Exception
            sqlTrans.Rollback()
            Throw
        Finally
            sqlconn.Close()
        End Try
    End Sub


    'Public Function UpdateFinalGrade(ByVal finalGrdObj As FinalGradeInfo, ByVal user As String, Optional ByVal scoreIsNull As Boolean = False, Optional ByVal campusId As String = "") As String
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim clsSectId As String
    '    Dim stuEnrollId As String
    '    Dim grade As String
    '    Dim score As Decimal = 0.0

    '    Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

    '    clsSectId = finalGrdObj.ClsSectId
    '    stuEnrollId = finalGrdObj.StuEnrollId
    '    If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToString.ToLower = "numeric" Then
    '        score = finalGrdObj.Score
    '    Else
    '        grade = finalGrdObj.Grade
    '    End If

    '    'Set the connection string
    '    db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

    '    Try
    '        With sb
    '            .Append("UPDATE arResults Set  ")
    '            .Append(" Score=?, ")
    '            .Append("IsCourseCompleted = ? ,")
    '            If grade = "" Or scoreIsNull Then
    '                .Append("isClinicsSatisfied = null ,")
    '            End If
    '            .Append(" GrdSysDetailId = ?,")
    '            .Append("ModUser=?, ")
    '            .Append("ModDate=?, ")
    '            .Append("IsGradeOverridden = ? ,")
    '            .Append("GradeOverriddenBy= ?, ")
    '            .Append("GradeOverriddenDate= ? ")
    '            .Append("WHERE TestId = ? and StuEnrollId = ? ")

    '            If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToString.ToLower = "numeric" Then
    '                If scoreIsNull Then
    '                    db.AddParameter("Score", DBNull.Value, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '                    db.AddParameter("IsCourseCompleted", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
    '                Else
    '                    db.AddParameter("Score", score, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '                    db.AddParameter("IsCourseCompleted", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
    '                End If
    '                db.AddParameter("@grade1", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            Else
    '                db.AddParameter("Score", DBNull.Value, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '                If grade = "" Then
    '                    db.AddParameter("IsCourseCompleted", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
    '                    db.AddParameter("@grade1", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '                Else
    '                    db.AddParameter("IsCourseCompleted", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
    '                    db.AddParameter("@grade2", grade, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '                End If
    '            End If

    '            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            db.AddParameter("IsGradeOverridden", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
    '            db.AddParameter("GradeOverridenBy", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("GradeOverridenDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            db.AddParameter("@clssectid", clsSectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@stdenrollid", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End With

    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)

    '        '   return without errors
    '        Return ""

    '    Catch ex As OleDbException
    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)

    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try

    'End Function


    '' Get the ClassSections based on the CohortStartDate and instructor and Campus
    Public Function GetClsSectsbyCohortStartDate(ByVal CohortStartDate As String, ByVal Instructor As String, ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            'Commented by BN 07/12/06 to accomodate to take new table arClassSectionTerms into
            'account.
            'With sb
            '    .Append("SELECT a.ClsSectionId, a.ReqId, a.ClsSection, b.Descrip ")
            '    .Append("FROM arClassSections a, arReqs b ")
            '    .Append("WHERE a.TermId = ? and a.ReqId = b.ReqId and a.InstructorId = ? ")
            '    .Append("and a.CampusId = ?")
            'End With
            With sb
                .Append("SELECT c.ClsSectionId, a.ReqId, a.ClsSection, b.Descrip ")
                .Append("FROM arClassSections a, arReqs b, arClassSectionTerms c ")
                .Append("WHERE a.CohortStartDate = ? and a.CohortStartDate is not null and c.ClsSectionId = a.ClsSectionId ")
                .Append("and a.ReqId = b.ReqId and a.InstructorId = ? ")
                .Append("and a.CampusId = ? ")
                .Append(" group by c.ClsSectionId, a.ReqId, a.ClsSection, b.Descrip ")
                '.Append(" having (select      count(*) as TheoryCount ")
                '.Append(" from     arGrdComponentTypes GC, arGrdBkWgtDetails GD ")
                '.Append(" where      GC.GrdComponentTypeId = GD.GrdComponentTypeId and SysComponentTypeid not in(500,503) ")
                '.Append(" and (number is not null    or weight is not null)  and GD.InstrGrdBkWgtId in(select  GBW.InstrGrdBkWgtId as ID ")
                '.Append(" from arGrdBkWeights GBW   where  GBW.ReqId = a.Reqid ")
                '.Append(" and     GBW.StatusId = (select StatusId from syStatuses where Status='Active') ))>0 ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@CohortStartDate", CohortStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@InstrId", Instructor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermClsSects")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    ''Added by Saraswathi lakshmanan to SHow the CohortStart Date for Current and Past terms
    ''Added on october 14 2008
    Public Function GetCohortStartDateforCurrentAndPastTerms(Optional ByVal instructorId As String = "", Optional ByVal campusId As String = "") As DataSet
        Dim ds As DataSet

        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT Distinct Convert(varchar(10),t3.CohortStartDate,101)as CohortStartDate,year(t3.CohortStartDate)  ")
                .Append("FROM arTerm t1, syStatuses t2,arClassSections t3  ")
                .Append("WHERE t1.StartDate <= ?  ")
                .Append("and t2.Status = 'Active' and t1.StatusId = t2.StatusId ")
                .Append("and t1.TermId = t3.TermId and t3.CohortStartDate is not null ")
                If instructorId <> "" Then
                    'this condition will make the query retrieve all current and future terms where the instructor teaches in.
                    .Append("AND EXISTS (SELECT * FROM arClassSections WHERE InstructorId=? AND TermId=t1.TermId) ")
                End If
                If campusId <> "" Then
                    .Append("AND (t1.CampGrpId IN(SELECT CampGrpId ")
                    .Append("FROM syCmpGrpCmps ")

                    .Append("WHERE CampusId = ? ")
                    .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                End If
                .Append("ORDER BY Year(t3.CohortStartDate)")
            End With

            db.AddParameter("@Sdate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            If instructorId <> "" Then
                db.AddParameter("@InstructorId", instructorId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If campusId <> "" Then
                db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

            Return ds

        Catch ex As Exception
            Throw New BaseException(ex.InnerException.Message)
        End Try

        'Return the 
    End Function
    'Added by Saraswathi Lakshmanan to find the Instructror based on the cohort Start Date
    ''Added on October 14th 2008

    Public Function GetInstructorbyCohortStartDate(ByVal CohortStartDate As String, ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT DISTINCT a.InstructorId, b.FullName ")
                .Append("FROM arClassSections a, syUsers b ")
                .Append("WHERE a.CohortStartDate = ? and a.CohortStartDate is not null and a.InstructorId = b.UserId ")
                .Append("AND a.CampusId = ?")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@CohortStartDate", CohortStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "InstructorDT")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function


End Class
