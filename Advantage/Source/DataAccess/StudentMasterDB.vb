
Imports FAME.Advantage.Common

Public Class StudentMasterDB
    Public Function AddStudent(ByVal studentinfo As StudentMasterInfo, ByVal user As String, ByVal strObjective As String, Optional ByVal useRegent As Boolean = False) As String
        ''Connect To The Database
        Dim db As New DataAccess

        db.ConnectionString = GetConnectionString()
        'Do an Update
        Try
            'Build The Query
            Dim sb As New StringBuilder

            With sb
                .Append("Insert into adLeads (StudentId ,FirstName, ")
                .Append(" LastName, MiddleName, SSN, ")
                .Append(" Prefix, Suffix, BirthDate, Sponsor, Gender, ")
                .Append(" Race, MaritalStatus ,FamilyIncome, Children, ")
                .Append(" Nationality ,Citizen ,DrivLicStateID, DrivLicNumber ,AlienNumber, Comments, ")
                .Append(" ModUser ,ModDate ,AssignedDate, StudentStatusId , StudentNumber, ")
                .Append(" DependencyTypeId, GeographicTypeId, AdminCriteriaId, HousingId ")
                If useRegent = True Then
                    .Append(",entranceinterviewdate ")
                End If
                .Append(" ) ")
                .Append(" Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ")
                If useRegent = True Then
                    .Append(",? ")  '                                                                        
                End If
                .Append(" ) ")
            End With

            'Add Parameters To Query
            db.AddParameter("@StudentId", studentinfo.StudentID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'First Name  
            db.AddParameter("@FirstName", studentinfo.FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'Last Name
            db.AddParameter("@LastName", studentinfo.LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'MiddleName
            If studentinfo.MiddleName = "" Then
                db.AddParameter("@MiddleName", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@MiddleName", studentinfo.MiddleName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            'SSN
            If studentinfo.SSN = "" Then
                db.AddParameter("@SSN", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SSN", studentinfo.SSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            'Prefix
            If studentinfo.Prefix = "" Or studentinfo.Prefix = Guid.Empty.ToString Then
                db.AddParameter("@Prefix", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Prefix", studentinfo.Prefix, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'Suffix
            If studentinfo.Suffix = "" Or studentinfo.Suffix = Guid.Empty.ToString Then
                db.AddParameter("@Suffix", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Suffix", studentinfo.Suffix, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'BirthDate
            If studentinfo.BirthDate = "" Then
                db.AddParameter("@BirthDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@BirthDate", studentinfo.BirthDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'Sponsor
            If studentinfo.Sponsor = "" Or studentinfo.Sponsor = Guid.Empty.ToString Then
                db.AddParameter("@Sponsor", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Sponsor", studentinfo.Sponsor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'Gender
            If studentinfo.Gender = "" Or studentinfo.Gender = Guid.Empty.ToString Then
                db.AddParameter("@Gender", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Gender", studentinfo.Gender, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'Race
            If studentinfo.Race = "" Or studentinfo.Race = Guid.Empty.ToString Then
                db.AddParameter("@Race", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Race", studentinfo.Race, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'MaritalStatus
            If studentinfo.MaritalStatus = "" Or studentinfo.MaritalStatus = Guid.Empty.ToString Then
                db.AddParameter("@MaritalStatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@MaritalStatus", studentinfo.MaritalStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'FamilyIncome
            If String.IsNullOrEmpty(studentinfo.FamilyIncome) Or studentinfo.FamilyIncome = Guid.Empty.ToString() Then
                db.AddParameter("@FamilyIncome", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@FamilyIncome", studentinfo.FamilyIncome, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'Children
            If studentinfo.Children = "" Then
                db.AddParameter("@Children", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Children", studentinfo.Children, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'Nationality
            If studentinfo.Nationality = Guid.Empty.ToString Or studentinfo.Nationality = "" Then
                db.AddParameter("@Nationality", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Nationality", studentinfo.Nationality, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'Citizen
            If studentinfo.Citizen = Guid.Empty.ToString Or studentinfo.Citizen = "" Then
                db.AddParameter("@Citizen", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Citizen", studentinfo.Citizen, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'DrivLicStateID
            If studentinfo.DriverLicState = Guid.Empty.ToString Or studentinfo.DriverLicState = "" Then
                db.AddParameter("@DrivLicStateID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DrivLicStateID", studentinfo.DriverLicState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'DriverLicNumber
            If studentinfo.DriverLicNumber = "" Then
                db.AddParameter("@DrivLicNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DrivLicNumber", studentinfo.DriverLicNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'AlienNumber
            If studentinfo.AlienNumber = "" Then
                db.AddParameter("@AlienNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AlienNumber", studentinfo.AlienNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'Comments
            If studentinfo.Notes = "" Then
                db.AddParameter("@Comments", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@Comments", studentinfo.Notes, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If
            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ''ModDate
            db.AddParameter("@ModDate", studentinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'Assigned Date
            If studentinfo.AssignmentDate = "" Then
                db.AddParameter("@AssignedDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AssignedDate", studentinfo.AssignmentDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'Student Status
            If studentinfo.Status = "" Or studentinfo.Status = Guid.Empty.ToString Then
                db.AddParameter("@StudentStatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@StudentStatus", studentinfo.Status, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'StudentNumber
            db.AddParameter("@StudentNumber", studentinfo.StudentNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'DependencyTypeId
            If studentinfo.DependencyTypeId = "" Then
                db.AddParameter("@DependencyTypeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DependencyTypeId", studentinfo.DependencyTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'GeographicTypeId
            If studentinfo.GeographicTypeId = "" Then
                db.AddParameter("@GeographicTypeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@GeographicTypeId", studentinfo.GeographicTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'AdminCriteriaId
            If studentinfo.AdminCriteriaId = "" Then
                db.AddParameter("@AdminCriteriaId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AdminCriteriaId", studentinfo.AdminCriteriaId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'HousingId
            If studentinfo.HousingId = "" Then
                db.AddParameter("@HousingId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@HousingId", studentinfo.HousingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If useRegent = True Then
                If Not studentinfo.EntranceInterviewDate = "01/01/1900" Then
                    db.AddParameter("@EntranceInterviewDate", studentinfo.EntranceInterviewDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@EntranceInterviewDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
            End If
            'Execute The Query
            Try
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
            Catch ex As OleDbException
                db.CloseConnection()
                Return DALExceptions.BuildErrorMessage(ex)
            Finally
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            End Try
            Dim intForeignZip As Integer
            Dim intForeignPhone As Integer
            If studentinfo.ForeignZip = 1 Then
                intForeignZip = 1
            Else
                intForeignZip = 0
            End If
            If studentinfo.ForeignPhone = 1 Then
                intForeignPhone = 1
            Else
                intForeignPhone = 0
            End If
            Dim sb4 As New StringBuilder
            With sb4
                .Append(" Insert into adLeadAddresses (StudentId,Address1,Address2,")
                .Append(" City,StateId,Zip,CountryId,AddressTypeId,ModUser,ModDate,StatusId,default1,ForeignZip,OtherState) ")
                .Append(" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With
            'Add Parameters To Query
            db.AddParameter("@StudentId", studentinfo.StudentID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'Address
            If studentinfo.Address1 = "" Then
                db.AddParameter("@Address1", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address1", studentinfo.Address1, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            'Address2
            If studentinfo.Address2 = "" Then
                db.AddParameter("@Address2", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Address2", studentinfo.Address2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            'city
            If studentinfo.City = "" Then
                db.AddParameter("@city", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@city", studentinfo.City, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            'StateId
            If studentinfo.State = "" Then
                db.AddParameter("@State", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@State", studentinfo.State, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            'Zip
            If studentinfo.Zip = "" Then
                db.AddParameter("@zip", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@zip", studentinfo.Zip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            'Country
            If studentinfo.Country = "" Or studentinfo.Country = Guid.Empty.ToString Then
                db.AddParameter("@Country", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Country", studentinfo.Country, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'AddressType
            If studentinfo.AddressType = "" Or studentinfo.AddressType = Guid.Empty.ToString Then
                db.AddParameter("@AddressTypeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AddressTypeId", studentinfo.AddressType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@StatusId", studentinfo.Status, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@default1", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ForeignZip", intForeignZip, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            'Other State
            If studentinfo.OtherState = "" Then
                db.AddParameter("@OtherState", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@OtherState", studentinfo.OtherState, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            'If Country is Domestic
            If intForeignZip = 0 Then
                If Not studentinfo.Address1 = "" Or Not studentinfo.Address2 = "" Or Not studentinfo.City = "" Or Not studentinfo.Country = Guid.Empty.ToString Or Not studentinfo.State = Guid.Empty.ToString Or Not studentinfo.Zip = "" Or Not studentinfo.AddressStatus = Guid.Empty.ToString Or Not studentinfo.AddressType = Guid.Empty.ToString Then
                    db.RunParamSQLExecuteNoneQuery(sb4.ToString)
                End If
            End If
            'If Country is International
            If intForeignZip = 1 Then
                If Not studentinfo.Address1 = "" Or Not studentinfo.Address2 = "" Or Not studentinfo.City = "" Or Not studentinfo.Country = Guid.Empty.ToString Or Not studentinfo.OtherState = "" Or Not studentinfo.Zip = "" Or Not studentinfo.AddressStatus = Guid.Empty.ToString Or Not studentinfo.AddressType = Guid.Empty.ToString Then
                    db.RunParamSQLExecuteNoneQuery(sb4.ToString)
                End If
            End If
            db.ClearParameters()
            sb4.Remove(0, sb4.Length)

            Dim sb5 As New StringBuilder
            With sb5
                .Append(" Insert into arStudentPhone(StudentId,PhoneTypeId,Phone,")
                .Append(" StatusId,ModUser,ModDate,default1,ForeignPhone) ")
                .Append(" values(?,?,?,?,?,?,?,?) ")
            End With
            db.AddParameter("@StudentId", studentinfo.StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'PhoneType
            If studentinfo.PhoneType = "" Or studentinfo.PhoneType = Guid.Empty.ToString Then
                db.AddParameter("@PhoneType", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PhoneType", studentinfo.PhoneType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'HomePhone
            If studentinfo.Phone = "" Then
                db.AddParameter("@HomePhone", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HomePhone", studentinfo.Phone, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            db.AddParameter("@StatusId", studentinfo.Status, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@default1", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ForeignPhone", intForeignPhone, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            If Not studentinfo.Phone = "" Then
                db.RunParamSQLExecuteNoneQuery(sb5.ToString)
            End If
            db.ClearParameters()
            sb5.Remove(0, sb5.Length)
            Return ""
        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateStudent(ByVal studentinfo As StudentMasterInfo, ByVal user As String, ByVal strObjective As String, Optional ByVal useRegent As Boolean = False, Optional ByVal bUpdateSSN As Boolean = True) As String
        ''Connect To The Database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Dim intForeignZip As Integer
        Dim intForeignPhone As Integer

        If studentinfo.ForeignZip = 1 Then
            intForeignZip = 1
        Else
            intForeignZip = 0
        End If
        If studentinfo.ForeignPhone = 1 Then
            intForeignPhone = 1
        Else
            intForeignPhone = 0
        End If
        Try

            'Build The Query
            Dim sb As New StringBuilder

            With sb
                .Append("Update adLeads set FirstName = ?, ")
                .Append(" LastName = ?, MiddleName = ?, ")
                If bUpdateSSN Then
                    .Append(" SSN = ?, ")
                End If
                .Append(" Prefix = ?, Suffix = ?, BirthDate = ?, Sponsor = ?, Gender = ?, ")
                .Append(" Race = ?, MaritalStatus = ?, FamilyIncome = ?, Children = ?, ")
                .Append(" Nationality = ?, Citizen = ?, DrivLicStateID = ? ,DrivLicNumber = ?, AlienNumber = ?, ")
                .Append(" ModUser = ?, ModDate = ?, AssignedDate = ? , StudentStatusId = ?, StudentNumber = ?, ")
                .Append(" DependencyTypeId = ?, GeographicTypeId = ?, AdminCriteriaId = ?, HousingId = ? ")
                

                If useRegent = True Then
                    .Append(" , EntranceInterviewDate = ? ")  ',highschoolprogramcode=? ")
                End If
                .Append(" where LeadId = ? ; ")
            End With
            'First Name
            db.AddParameter("@FirstName", studentinfo.FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'Last Name
            db.AddParameter("@LastName", studentinfo.LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'MiddleName
            If studentinfo.MiddleName = "" Then
                db.AddParameter("@MiddleName", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@MiddleName", studentinfo.MiddleName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            'SSN
            If bUpdateSSN Then
                If studentinfo.SSN = "" Then
                    db.AddParameter("@SSN", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@SSN", studentinfo.SSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
            End If
            'Prefix
            If studentinfo.Prefix = "" Or studentinfo.Prefix = Guid.Empty.ToString Then
                db.AddParameter("@Prefix", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Prefix", studentinfo.Prefix, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'Suffix
            If studentinfo.Suffix = "" Or studentinfo.Suffix = Guid.Empty.ToString Then
                db.AddParameter("@Suffix", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Suffix", studentinfo.Suffix, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'BirthDate
            If studentinfo.BirthDate = "" Then
                db.AddParameter("@BirthDate", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@BirthDate", DateTime.Parse(studentinfo.BirthDate), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            'Sponsor
            If studentinfo.Sponsor = "" Or studentinfo.Sponsor = Guid.Empty.ToString Then
                db.AddParameter("@Sponsor", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Sponsor", studentinfo.Sponsor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'Gender
            If studentinfo.Gender = "" Or studentinfo.Gender = Guid.Empty.ToString Then
                db.AddParameter("@Gender", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Gender", studentinfo.Gender, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'Race
            If studentinfo.Race = "" Or studentinfo.Race = Guid.Empty.ToString Then
                db.AddParameter("@Race", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Race", studentinfo.Race, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'MaritalStatus
            If studentinfo.MaritalStatus = "" Or studentinfo.MaritalStatus = Guid.Empty.ToString Then
                db.AddParameter("@MaritalStatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@MaritalStatus", studentinfo.MaritalStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'FamilyIncome
            If String.IsNullOrEmpty(studentinfo.FamilyIncome) Or studentinfo.FamilyIncome = Guid.Empty.ToString() Then
                db.AddParameter("@FamilyIncome", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@FamilyIncome", studentinfo.FamilyIncome, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'Children
            If studentinfo.Children = "" Then
                db.AddParameter("@Children", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Children", studentinfo.Children, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'Nationality
            If studentinfo.Nationality = Guid.Empty.ToString Or studentinfo.Nationality = "" Then
                db.AddParameter("@Nationality", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Nationality", studentinfo.Nationality, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'Citizen
            If studentinfo.Citizen = Guid.Empty.ToString Or studentinfo.Citizen = "" Then
                db.AddParameter("@Citizen", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Citizen", studentinfo.Citizen, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'DrivLicStateID
            If studentinfo.DriverLicState = Guid.Empty.ToString Or studentinfo.DriverLicState = "" Then
                db.AddParameter("@DrivLicStateID", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DrivLicStateID", studentinfo.DriverLicState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'DriverLicNumber
            If studentinfo.DriverLicNumber = "" Then
                db.AddParameter("@DrivLicNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DrivLicNumber", studentinfo.DriverLicNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'AlienNumber
            If studentinfo.AlienNumber = "" Then
                db.AddParameter("@AlienNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AlienNumber", studentinfo.AlienNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'ModDate
            db.AddParameter("@ModDate", studentinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'Assigned Date
            If studentinfo.AssignmentDate = "" Then
                db.AddParameter("@AssignedDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AssignedDate", studentinfo.AssignmentDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'Student Status
            If studentinfo.Status = "" Or studentinfo.Status = Guid.Empty.ToString Then
                db.AddParameter("@StudentStatusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@StudentStatusId", studentinfo.Status, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'StudentNumber
            db.AddParameter("@StudentNumber", studentinfo.StudentNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'DependencyTypeId
            If studentinfo.DependencyTypeId = "" Then
                db.AddParameter("@DependencyTypeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DependencyTypeId", studentinfo.DependencyTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'GeographicTypeId
            If studentinfo.GeographicTypeId = "" Then
                db.AddParameter("@GeographicTypeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@GeographicTypeId", studentinfo.GeographicTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'AdminCriteriaId
            If studentinfo.AdminCriteriaId = "" Then
                db.AddParameter("@AdminCriteriaId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AdminCriteriaId", studentinfo.AdminCriteriaId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'HousingId
            If studentinfo.HousingId = "" Then
                db.AddParameter("@HousingId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@HousingId", studentinfo.HousingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
           

            If useRegent = True Then
                If Not studentinfo.EntranceInterviewDate = "01/01/1900" Then
                    db.AddParameter("@EntranceInterviewDate", studentinfo.EntranceInterviewDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@EntranceInterviewDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
            End If
            'LeadId
            db.AddParameter("@LeadId", studentinfo.LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        

            'Execute The Query
            Try
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As OleDbException
                Return DALExceptions.BuildErrorMessage(ex)
            End Try

            'Get The Primary Key Value From arStudentPhone Table(Only Default Phone)
            Dim sb1 As New StringBuilder
            With sb1
                .Append("select LeadPhoneId from adLeadPhone where LeadId = ? And Position = 1 ")
            End With
            'LeadId
            db.AddParameter("@LeadId", studentinfo.LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb1.ToString)
            Dim strLeadPhoneId As String
            While dr.Read()
                If Not (dr("LeadPhoneId") Is DBNull.Value) Then strLeadPhoneId = CType(dr("LeadPhoneId"), Guid).ToString Else strLeadPhoneId = ""
            End While

            If Not dr.IsClosed Then dr.Close()
            db.ClearParameters()
            sb1.Remove(0, sb1.Length)

            'If Default Phone Exist for Student, update phone else insert phone   
            If Not strLeadPhoneId = "" Then
                If studentinfo.Phone = "" Then
                    'Delete record
                    Dim sb5 As New StringBuilder
                    With sb5
                        .Append(" DELETE adLeadPhone ")
                        .Append(" where LeadPhoneId = ? ")
                    End With
                    db.AddParameter("@LeadPhoneId", strLeadPhoneId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'Execute The Query
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb5.ToString)
                        db.ClearParameters()
                        sb5.Remove(0, sb5.Length)
                    Catch ex As OleDbException
                        Return DALExceptions.BuildErrorMessage(ex)
                    End Try
                Else
                    'Update record
                    Dim sb5 As New StringBuilder
                    With sb5
                        .Append(" update adLeadPhone set LeadId = ?, PhoneTypeId = ?, Phone = ?, StatusId = ?, Moduser = ?, ModDate = ?, IsForeignPhone = ? ")
                        .Append(" where LeadPhoneId = ? ")
                    End With
                    'LeadId
                    db.AddParameter("@LeadId", studentinfo.LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'PhoneTypeId
                    db.AddParameter("@PhoneTypeId", studentinfo.PhoneType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'Phone
                    db.AddParameter("@Phone", studentinfo.Phone, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'StatusId
                    db.AddParameter("@StatusId", studentinfo.PhoneStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'ModUser
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'ModDate
                    db.AddParameter("@ModDate", studentinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@IsForeignPhone", Convert.ToBoolean(intForeignPhone), DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                    db.AddParameter("@LeadPhoneId", strLeadPhoneId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'Execute The Query
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb5.ToString)
                        db.ClearParameters()
                        sb5.Remove(0, sb5.Length)
                    Catch ex As OleDbException
                        Return DALExceptions.BuildErrorMessage(ex)
                    End Try
                End If
            ElseIf Not studentinfo.Phone = "" Then
                'insert only if the phone is not blank
                'Insert Record
                Dim sb51 As New StringBuilder
                With sb51
                    .Append(" insert into adLeadPhone (LeadId, PhoneTypeId, Phone, StatusId, Moduser, ModDate ,IsForeignPhone, Position, IsBest ) ")
                    .Append(" values( ?, ?, ?, ?, ?, ?, ?, ?, ?) ")
                End With
                'LeadId
                db.AddParameter("@LeadId", studentinfo.LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'PhoneTypeId
                db.AddParameter("@PhoneTypeId", studentinfo.PhoneType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'Phone
                db.AddParameter("@Phone", studentinfo.Phone, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'StatusId
                db.AddParameter("@StatusId", studentinfo.PhoneStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'ModDate
                db.AddParameter("@ModDate", studentinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                'IsForeignPhone
                db.AddParameter("@IsForeignPhone", Convert.ToBoolean(intForeignPhone), DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'db.AddParameter("@default1", 1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                db.AddParameter("@Position", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsBest", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'Insert Only If Phone is not empty
                Try
                    db.RunParamSQLExecuteNoneQuery(sb51.ToString)
                    db.ClearParameters()
                    sb51.Remove(0, sb51.Length)
                Catch ex As OleDbException
                    Return DALExceptions.BuildErrorMessage(ex)
                End Try

            End If

            'Get The Primary Key Value From arStudentPhone Table(Only Default Phone)
            Dim sb61 As New StringBuilder
            With sb61
                .Append("select adLeadAddressId from adLeadAddresses where LeadId = ? And IsShowOnLeadPage = 1 ")
            End With
            'LeadId
            db.AddParameter("@LeadId", studentinfo.LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            Dim drAddress As OleDbDataReader = db.RunParamSQLDataReader(sb61.ToString)
            'Dim strAddressId As String
            Dim strLeadAddressId As String
            While drAddress.Read()
                'Get Default PhoneValue
                If Not (drAddress("adLeadAddressId") Is DBNull.Value) Then strLeadAddressId = CType(drAddress("adLeadAddressId"), Guid).ToString Else strLeadAddressId = ""
            End While

            If Not drAddress.IsClosed Then drAddress.Close()

            db.ClearParameters()
            sb61.Remove(0, sb61.Length)

            'If Default Phone Exist for Student, update phone else insert phone
            'If Not strAddressId = "" Then
            If Not strLeadAddressId = "" Then
                If studentinfo.Address1 = "" Then
                    'Delete record
                    Dim sb5 As New StringBuilder
                    With sb5
                        .Append(" DELETE adLeadAddresses ")
                        .Append(" where adLeadAddressId = ? ")
                    End With
                    db.AddParameter("@adLeadAddressId", strLeadAddressId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'Execute The Query
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb5.ToString)
                        db.ClearParameters()
                        sb5.Remove(0, sb5.Length)
                    Catch ex As OleDbException
                        Return DALExceptions.BuildErrorMessage(ex)
                    End Try
                Else
                    Dim sb59 As New StringBuilder
                    With sb59
                        .Append(" update adLeadAddresses set LeadId = ?, Address1 = ?, Address2 = ?, City = ?, StateId = ?, ZipCode = ?, CountryId = ?, ")
                        .Append(" CountyId = ?, AddressTypeId = ?, State = ?, ")
                        .Append(" Moduser = ?, ModDate = ?, StatusId = ?, IsInternational = ? ")
                        .Append(" where adLeadAddressId = ? ")
                    End With
                    db.AddParameter("@LeadId", studentinfo.LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'Address1
                    db.AddParameter("@Address1", studentinfo.Address1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'Address2
                    If studentinfo.Address2 = "" Then
                        db.AddParameter("@Address2", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@Address2", studentinfo.Address2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    'City
                    If studentinfo.City = "" Then
                        db.AddParameter("@City", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@City", studentinfo.City, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    'StateId
                    If studentinfo.State = "" Or studentinfo.State = Guid.Empty.ToString Then
                        db.AddParameter("@StateId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@StateId", studentinfo.State, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    'ZipCode
                    If studentinfo.Zip = "" Then
                        db.AddParameter("@ZipCode", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@ZipCode", studentinfo.Zip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    'CountryId
                    If studentinfo.Country = Guid.Empty.ToString Or studentinfo.Country = "" Then
                        db.AddParameter("@CountryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@CountryId", studentinfo.Country, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    'CountyId
                    If studentinfo.County = Guid.Empty.ToString Or studentinfo.County = "" Then
                        db.AddParameter("@CountyId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@CountyId", studentinfo.County, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    'AddressTypeId
                    db.AddParameter("@AddressTypeId", studentinfo.AddressType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                    'OtherState
                    If studentinfo.OtherState = "" Then
                        db.AddParameter("@State", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@State", studentinfo.OtherState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    'ModUser
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'ModDate
                    db.AddParameter("@ModDate", studentinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    'Address Status
                    db.AddParameter("@StatusId", studentinfo.AddressStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'IsInternational
                    db.AddParameter("@IsInternational", studentinfo.IsInternational, DataAccess.OleDbDataType.OleDbBoolean, ParameterDirection.Input)
                    'If String.IsNullOrEmpty(studentinfo.OtherState) Then
                    '    db.AddParameter("@IsInternational", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    'Else
                    '    db.AddParameter("@IsInternational", 0, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    'End If
                    'adLeadAddressId
                    db.AddParameter("@adLeadAddressId", strLeadAddressId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'Execute The Query
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb59.ToString)
                        db.ClearParameters()
                        sb59.Remove(0, sb59.Length)
                    Catch ex As OleDbException
                        Return DALExceptions.BuildErrorMessage(ex)
                    End Try
                End If
            ElseIf Not studentinfo.Address1 = "" Then
                Dim sb52 As New StringBuilder
                With sb52
                    .Append(" insert into adLeadAddresses (LeadId, Address1, Address2, City, StateId, ZipCode, CountryId, ") '  ForeignZip,
                    .Append(" CountyId, AddressTypeId, State, Moduser, ModDate, IsMailingAddress, IsShowOnLeadPage,  ")
                    .Append(" IsInternational, StatusId, ForeignCountyStr, ForeignCountryStr ) ")
                    .Append(" values( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ")
                End With
                'StudentId 
                db.AddParameter("@LeadId", studentinfo.LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                'Address1
                db.AddParameter("@Address1", studentinfo.Address1, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'Address2
                If studentinfo.Address2 = "" Then
                    db.AddParameter("@Address2", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Address2", studentinfo.Address2, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                'City
                If studentinfo.City = "" Then
                    db.AddParameter("@City", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@City", studentinfo.City, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                'StateId
                If studentinfo.State = "" Or studentinfo.State = Guid.Empty.ToString Then
                    db.AddParameter("@StateId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@StateId", studentinfo.State, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                'ZipCode
                If studentinfo.Zip = "" Then
                    db.AddParameter("@ZipCode", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@ZipCode", studentinfo.Zip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                'CountryId
                If studentinfo.Country = Guid.Empty.ToString Or studentinfo.Country = "" Then
                    db.AddParameter("@CountryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@CountryId", studentinfo.Country, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                'CountyId
                If studentinfo.County = Guid.Empty.ToString Or studentinfo.County = "" Then
                    db.AddParameter("@CountyId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@CountyId", studentinfo.County, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                'AddressTypeId
                db.AddParameter("@AddressTypeId", studentinfo.AddressType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'OtherState
                If studentinfo.OtherState = "" Then
                    db.AddParameter("@State", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@State", studentinfo.OtherState, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                'ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'ModDate
                db.AddParameter("@ModDate", studentinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                'IsMailingAddress
                db.AddParameter("@IsMailingAddress", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                'IsShowOnLeadPage
                db.AddParameter("@IsShowOnLeadPage", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                'IsInternational
                If String.IsNullOrEmpty(studentinfo.OtherState) Then
                    db.AddParameter("@IsInternational", 0, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                Else
                    db.AddParameter("@IsInternational", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                End If
                'Address Status
                db.AddParameter("@StatusId", studentinfo.AddressStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ForeignCountyStr", String.Empty, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ForeignCountryStr", String.Empty, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'Execute The Query
                Try
                    db.RunParamSQLExecuteNoneQuery(sb52.ToString)
                    db.ClearParameters()
                    sb52.Remove(0, sb52.Length)
                Catch ex As OleDbException
                    Return DALExceptions.BuildErrorMessage(ex)
                End Try
            End If

            'Get ActiveStatusID from sy
            Dim ActiveStatusCode As String = "A"
            With sb61
                .Append("SELECT  SS.StatusId ")
                .Append("FROM    syStatuses AS SS ")
                .Append("WHERE   SS.StatusCode = ? ")
            End With
            'LeadId
            db.AddParameter("@StatusCode", ActiveStatusCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            Dim drStatuses As OleDbDataReader = db.RunParamSQLDataReader(sb61.ToString)
            'Dim strAddressId As String
            Dim ActiveStatusId As String
            While drStatuses.Read()
                'Get Default PhoneValue
                If Not (drStatuses("StatusId") Is DBNull.Value) Then ActiveStatusId = CType(drStatuses("StatusId"), Guid).ToString Else ActiveStatusId = ""
            End While
            If Not drStatuses.IsClosed Then drStatuses.Close()
            db.ClearParameters()
            sb61.Remove(0, sb61.Length)

            'Get EmailTypeId for Home email type
            Dim EMailTypeCode As String = "Home"
            With sb61
                .Append("SELECT  SETY.EMailTypeId ")
                .Append("FROM    syEmailType AS SETY ")
                .Append("WHERE   SETY.EMailTypeCode = ? ")
            End With
            'LeadId
            db.AddParameter("@EMailTypeCode", EMailTypeCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            Dim drEmailTypeHome As OleDbDataReader = db.RunParamSQLDataReader(sb61.ToString)
            'Dim strAddressId As String
            Dim EMailTypeId As String
            While drEmailTypeHome.Read()
                'Get Default PhoneValue
                If Not (drEmailTypeHome("EMailTypeId") Is DBNull.Value) Then EMailTypeId = CType(drEmailTypeHome("EMailTypeId"), Guid).ToString Else EMailTypeId = ""
            End While
            If Not drEmailTypeHome.IsClosed Then drEmailTypeHome.Close()
            db.ClearParameters()
            sb61.Remove(0, sb61.Length)

            'Get The Primary Key Value From AdLeadEmail Table(Only Default Phone)
            With sb61
                .Append("SELECT TOP 1 ")
                .Append("        ALE.LeadEMailId ")
                .Append("FROM    AdLeadEmail AS ALE ")
                .Append("WHERE   ALE.IsShowOnLeadPage = 1 ")
                .Append("        AND EMailTypeId = ? ")
                .Append("        AND ALE.LeadId = ? ")
            End With
            'LeadId
            db.AddParameter("@EMailTypeId", EMailTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@LeadId", studentinfo.LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            Dim drLeadEmailHome As OleDbDataReader = db.RunParamSQLDataReader(sb61.ToString)
            'Dim strAddressId As String
            Dim strLeadEmailId As String
            While drLeadEmailHome.Read()
                'Get Default PhoneValue
                If Not (drLeadEmailHome("LeadEMailId") Is DBNull.Value) Then strLeadEmailId = CType(drLeadEmailHome("LeadEMailId"), Guid).ToString Else strLeadEmailId = ""
            End While

            If Not drLeadEmailHome.IsClosed Then drLeadEmailHome.Close()

            db.ClearParameters()
            sb61.Remove(0, sb61.Length)

            If Not strLeadEmailId = "" Then
                If studentinfo.HomeEmail = "" Then
                    'Delete record
                    Dim sb5 As New StringBuilder
                    With sb5
                        .Append(" DELETE AdLeadEmail ")
                        .Append(" WHERE LeadEMailId = ? ")
                    End With
                    db.AddParameter("@LeadEMailId", strLeadEmailId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'Execute The Query
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb5.ToString)
                        db.ClearParameters()
                        sb5.Remove(0, sb5.Length)
                    Catch ex As OleDbException
                        Return DALExceptions.BuildErrorMessage(ex)
                    End Try
                Else
                    Dim sb59 As New StringBuilder
                    With sb59
                        .Append(" UPDATE AdLeadEmail ")
                        .Append("    SET LeadId = ?, EMail = ?, EMailTypeId = ?, IsPreferred = ?, ModUser = ?, ModDate = ?, ")
                        .Append("        StatusId = ?, IsShowOnLeadPage = ?  ")
                        .Append(" WHERE LeadEMailId = ? ")
                    End With
                    db.AddParameter("@LeadId", studentinfo.LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'Email
                    db.AddParameter("@HomeEmail", studentinfo.HomeEmail, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'EMailTypeId
                    db.AddParameter("@EMailTypeId", EMailTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@IsPreferred", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                    'ModUser
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'ModDate
                    db.AddParameter("@ModDate", studentinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    'Email Status
                    db.AddParameter("@StatusId", ActiveStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'IsShowOnLeadPage
                    db.AddParameter("@IsShowOnLeadPage", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                    'LeadEMailId
                    db.AddParameter("@LeadEMailId", strLeadEmailId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'Execute The Query
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb59.ToString)
                        db.ClearParameters()
                        sb59.Remove(0, sb59.Length)
                    Catch ex As OleDbException
                        Return DALExceptions.BuildErrorMessage(ex)
                    End Try
                End If
            Else
                If (Not String.IsNullOrEmpty(studentinfo.HomeEmail)) Then
                    Dim sb52 As New StringBuilder
                    With sb52
                        .Append(" INSERT INTO AdLeadEmail ")
                        .Append("             (LeadId, EMail, EMailTypeId, IsPreferred, IsPortalUserName, ModUser, ModDate, ")
                        .Append("              StatusId, IsShowOnLeadPage ) ")
                        .Append(" values( ?, ?, ?, ?, ?, ?, ?, ?, ? ) ")
                    End With
                    db.AddParameter("@LeadId", studentinfo.LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'Email
                    db.AddParameter("@HomeEmail", studentinfo.HomeEmail, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'EMailTypeId
                    db.AddParameter("@EMailTypeId", EMailTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@IsPreferred", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                    db.AddParameter("@IsPortalUserName", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                    'ModUser
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'ModDate
                    db.AddParameter("@ModDate", studentinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    'Email Status
                    db.AddParameter("@StatusId", ActiveStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'IsShowOnLeadPage
                    db.AddParameter("@IsShowOnLeadPage", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    'Execute The Query
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb52.ToString)
                        db.ClearParameters()
                        sb52.Remove(0, sb52.Length)
                    Catch ex As OleDbException
                        Return DALExceptions.BuildErrorMessage(ex)
                    End Try
                End If

            End If

            'Get EmailTypeId for Work email type  (Best Email)
            EMailTypeCode = "Work" 'Best Email
            With sb61
                .Append("SELECT  SETY.EMailTypeId ")
                .Append("FROM    syEmailType AS SETY ")
                .Append("WHERE   SETY.EMailTypeCode = ? ")

            End With
            'StudentId
            'LeadId
            db.AddParameter("@EMailTypeCode", EMailTypeCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            Dim drEmailTypeWork As OleDbDataReader = db.RunParamSQLDataReader(sb61.ToString)
            'Dim strAddressId As String
            EMailTypeId = ""
            While drEmailTypeWork.Read()
                'Get Default PhoneValue
                If Not (drEmailTypeWork("EMailTypeId") Is DBNull.Value) Then EMailTypeId = CType(drEmailTypeWork("EMailTypeId"), Guid).ToString Else EMailTypeId = ""
            End While
            If Not drEmailTypeWork.IsClosed Then drEmailTypeWork.Close()
            db.ClearParameters()
            sb61.Remove(0, sb61.Length)

            'Get The Primary Key Value From AdLeadEmail Table(Only Default Phone)
            With sb61
                .Append("SELECT TOP 1 ")
                .Append("        ALE.LeadEMailId ")
                .Append("FROM    AdLeadEmail AS ALE ")
                .Append("WHERE   ALE.IsShowOnLeadPage = 1 ")
                .Append("        AND EMailTypeId = ? ")
                .Append("        AND ALE.LeadId = ? ")
            End With
            'StudentId
            'LeadId
            db.AddParameter("@EMailTypeId", EMailTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@LeadId", studentinfo.LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            Dim drLeadEmailWork As OleDbDataReader = db.RunParamSQLDataReader(sb61.ToString)
            'Dim strAddressId As String
            strLeadEmailId = ""

            While drLeadEmailWork.HasRows And drLeadEmailWork.Read()
                'Get Default PhoneValue
                If Not (drLeadEmailWork("LeadEMailId") Is DBNull.Value) Then strLeadEmailId = CType(drLeadEmailWork("LeadEMailId"), Guid).ToString Else strLeadEmailId = ""
            End While

            If Not drLeadEmailWork.IsClosed Then drLeadEmailWork.Close()

            db.ClearParameters()
            sb61.Remove(0, sb61.Length)

            If Not strLeadEmailId = "" Then
                If studentinfo.WorkEmail = "" Then
                    'Delete record
                    Dim sb5 As New StringBuilder
                    With sb5
                        .Append(" DELETE AdLeadEmail ")
                        .Append(" WHERE LeadEMailId = ? ")
                    End With
                    db.AddParameter("@LeadEMailId", strLeadEmailId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'Execute The Query
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb5.ToString)
                        db.ClearParameters()
                        sb5.Remove(0, sb5.Length)
                    Catch ex As OleDbException
                        Return DALExceptions.BuildErrorMessage(ex)
                    End Try
                Else
                    Dim sb59 As New StringBuilder
                    With sb59
                        .Append(" UPDATE AdLeadEmail ")
                        .Append("    SET LeadId = ?, EMail = ?, EMailTypeId = ?, IsPreferred = ?, ModUser = ?, ModDate = ?, ")
                        .Append("        StatusId = ?, IsShowOnLeadPage = ?  ")
                        .Append(" WHERE LeadEMailId = ? ")
                    End With
                    db.AddParameter("@LeadId", studentinfo.LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'Email
                    db.AddParameter("@WorkEmail", studentinfo.WorkEmail, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'EMailTypeId
                    db.AddParameter("@EMailTypeId", EMailTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@IsPreferred", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                    'ModUser
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'ModDate
                    db.AddParameter("@ModDate", studentinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    'Email Status
                    db.AddParameter("@StatusId", ActiveStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'IsShowOnLeadPage
                    db.AddParameter("@IsShowOnLeadPage", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                    'LeadEMailId
                    db.AddParameter("@LeadEMailId", strLeadEmailId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'Execute The Query
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb59.ToString)
                        db.ClearParameters()
                        sb59.Remove(0, sb59.Length)
                    Catch ex As OleDbException
                        Return DALExceptions.BuildErrorMessage(ex)
                    End Try
                End If
            ElseIf Not String.IsNullOrEmpty(studentinfo.WorkEmail) Then
                Dim sb52 As New StringBuilder
                With sb52
                    .Append(" INSERT INTO AdLeadEmail ")
                    .Append("             (LeadId, EMail, EMailTypeId, IsPreferred, IsPortalUserName, ModUser, ModDate, ")
                    .Append("              StatusId, IsShowOnLeadPage ) ")
                    .Append(" values( ?, ?, ?, ?, ?, ?, ?, ?, ? ) ")
                End With
                'LeadId
                db.AddParameter("@LeadId", studentinfo.LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'Email
                db.AddParameter("@WorkEmail", studentinfo.WorkEmail, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'EMailTypeId
                db.AddParameter("@EMailTypeId", EMailTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@IsPreferred", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@IsPortalUserName", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'ModDate
                db.AddParameter("@ModDate", studentinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                'Email Status
                db.AddParameter("@StatusId", ActiveStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'IsShowOnLeadPage
                db.AddParameter("@IsShowOnLeadPage", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                'Execute The Query
                Try
                    db.RunParamSQLExecuteNoneQuery(sb52.ToString)
                    db.ClearParameters()
                    sb52.Remove(0, sb52.Length)
                Catch ex As OleDbException
                    Return DALExceptions.BuildErrorMessage(ex)
                End Try
            End If

            'Get Note Comments for Students
            Dim UserId As String
            With sb61
                .Append("SELECT TOP 1  ")
                .Append("        SU.UserId ")
                .Append("FROM    syUsers AS SU ")
                .Append("WHERE   SU.UserName = ?; ")
            End With
            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            Dim drsyUsers = db.RunParamSQLDataReader(sb61.ToString)
            While drsyUsers.Read()
                'Get Default PhoneValue
                If Not (drsyUsers("UserId") Is DBNull.Value) Then UserId = CType(drsyUsers("UserId"), Guid).ToString() Else UserId = ""
            End While
            If Not drsyUsers.IsClosed Then drsyUsers.Close()
            db.ClearParameters()
            sb61.Remove(0, sb61.Length)

            'Get Note Comments for Students
            Dim ModuleCode As String = "AR"  ' Academics 
            Dim NoteType As String = "Comment" ' Comment for Student
            Dim PageFieldId As Integer = 2
            With sb61
                .Append("SELECT ALN.id, AN.NotesId  ")
                .Append("FROM    adLead_Notes AS ALN ")
                .Append("INNER JOIN AllNotes AS AN ON AN.NotesId = ALN.NotesId ")
                .Append("WHERE   AN.ModuleCode = ? ")
                .Append("        AND AN.NoteType = ? ")
                .Append("        AND AN.PageFieldId = ?  ")
                .Append("        AND ALN.LeadId = ? ")
            End With

            db.AddParameter("@ModuleCode", ModuleCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@NoteType", NoteType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PageFieldId", PageFieldId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@LeadId", studentinfo.LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            Dim drAllNotes = db.RunParamSQLDataReader(sb61.ToString)

            Dim NotesId As Integer
            Dim adLead_NotesId As Integer
            While drAllNotes.Read()
                'Get Default PhoneValue
                If Not (drAllNotes("NotesId") Is DBNull.Value) Then NotesId = CType(drAllNotes("NotesId"), Integer) Else NotesId = 0
                If Not (drAllNotes("id") Is DBNull.Value) Then adLead_NotesId = CType(drAllNotes("id"), Integer) Else adLead_NotesId = 0
            End While
            If Not drAllNotes.IsClosed Then drAllNotes.Close()
            db.ClearParameters()
            sb61.Remove(0, sb61.Length)

            If Not NotesId = 0 Then
                If studentinfo.Notes = "" Then
                    'Delete record adLead_Notes
                    Dim sb5 As New StringBuilder
                    With sb5
                        .Append(" DELETE adLead_Notes ")
                        .Append(" WHERE id = ? ")
                    End With
                    db.AddParameter("@id", adLead_NotesId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    'Execute The Query
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb5.ToString)
                        db.ClearParameters()
                        sb5.Remove(0, sb5.Length)
                    Catch ex As OleDbException
                        Return DALExceptions.BuildErrorMessage(ex)
                    End Try

                    'Delete record adLead_Notes
                    Dim sb6 As New StringBuilder
                    With sb6
                        .Append(" DELETE ALLNotes ")
                        .Append(" WHERE NotesId = ? ")
                    End With
                    db.AddParameter("@NotesId", NotesId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    'Execute The Query
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb6.ToString)
                        db.ClearParameters()
                        sb6.Remove(0, sb6.Length)
                    Catch ex As OleDbException
                        Return DALExceptions.BuildErrorMessage(ex)
                    End Try
                Else
                    Dim sb59 As New StringBuilder
                    With sb59
                        .Append(" UPDATE ALLNotes ")
                        .Append("    SET NoteText = ?, ModUser = ?, ModDate = ? ")
                        .Append(" WHERE NotesId = ? ")
                    End With
                    'NoteText
                    db.AddParameter("@NoteText", studentinfo.Notes, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'ModUser
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'ModDate
                    db.AddParameter("@ModDate", studentinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    'NotesId
                    db.AddParameter("@NotesId", NotesId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    'Execute The Query
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb59.ToString)
                        db.ClearParameters()
                        sb59.Remove(0, sb59.Length)
                    Catch ex As OleDbException
                        Return DALExceptions.BuildErrorMessage(ex)
                    End Try
                End If
            Else
                Dim LastNotesId As Integer
                Dim sb52 As New StringBuilder
                With sb52
                    .Append(" DECLARE @OutputTable TABLE ( NotesId  Integer ) ")
                    .Append(" INSERT INTO ALLNotes ")
                    .Append("             (ModuleCode, NoteType, UserId, NoteText, ModUser, ModDate, ")
                    .Append("              PageFieldId ) ")
                    .Append(" OUTPUT INSERTED.NotesId INTO @OutputTable ")
                    .Append(" values( ?, ?, ?, ?, ?, ?, ? ) ")
                    .Append(" SELECT TOP 1 NotesId FROM @OutputTable  ")
                End With
                'ModuleCode
                db.AddParameter("@ModuleCode", ModuleCode, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'NoteType
                db.AddParameter("@NoteType", NoteType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'UserId
                db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'NoteText
                db.AddParameter("@NoteText", studentinfo.Notes, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'ModDate
                db.AddParameter("@ModDate", studentinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                'PageFieldId
                db.AddParameter("@PageFieldId", PageFieldId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                'Execute The Query
                Try
                    Dim drAllNotesLast = db.RunParamSQLDataReader(sb52.ToString)
                    While drAllNotesLast.Read()
                        'Get Default PhoneValue
                        If Not (drAllNotesLast("NotesId") Is DBNull.Value) Then LastNotesId = CType(drAllNotesLast("NotesId"), Integer) Else LastNotesId = 0
                    End While
                    If Not drAllNotesLast.IsClosed Then drAllNotesLast.Close()
                    db.ClearParameters()
                    sb52.Remove(0, sb52.Length)

                Catch ex As OleDbException
                    Return DALExceptions.BuildErrorMessage(ex)
                End Try

                Dim sb53 As New StringBuilder
                With sb53
                    .Append(" INSERT INTO adLead_Notes ")
                    .Append("             (LeadId, NotesId ) ")
                    .Append(" values( ?, ? ) ")
                End With
                'LeadId
                db.AddParameter("@LeadId", studentinfo.LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'NotesId
                db.AddParameter("@NotesId", LastNotesId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                'Execute The Query
                Try
                    db.RunParamSQLExecuteNoneQuery(sb53.ToString)
                    db.ClearParameters()
                    sb53.Remove(0, sb53.Length)
                Catch ex As OleDbException
                    Return DALExceptions.BuildErrorMessage(ex)
                End Try
            End If

            studentinfo.IsInDB = True

            'Insert data into regent xml 
            If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                Dim strEmailType As String = ""
                Dim strEmailCode As String = ""
                If Not studentinfo.HomeEmail = "" Then
                    strEmailType = "HOME"
                    strEmailCode = "HOME"
                End If
                If Not studentinfo.WorkEmail = "" Then
                    strEmailType = "WORK"
                    strEmailCode = "WORK"
                End If

                Dim regDB As New regentDB
                Dim strFilename As String = AdvantageCommonValues.getStudentBatchFileName()
                regDB.AddCommentXML(studentinfo.StudentID, strFilename, "")
                regDB.AddDemographicXML(studentinfo.StudentID, strFilename, "")
                regDB.AddEmailXML(studentinfo.StudentID, strFilename, "", strEmailType, strEmailCode)
            End If

            Return ""
        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function DeleteStudent(ByVal studentId As String, ByVal modDate As DateTime) As String
        'Connect To The Database
        Dim db As New DataAccess
        db.ConnectionString = GetConnectionString()
        'Do an Update
        'Try
        'Build The Query
        Dim sb As New StringBuilder
        Try
            With sb
                .Append("Delete from arStudent where StudentId = ? ")
                .Append(" And ModDate = ? ;")
                .Append("SELECT count(*) FROM arStudent WHERE StudentId = ? ")
            End With
            'StudentId
            db.AddParameter("@StudentID", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'StudentId
            db.AddParameter("@StudentID", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)
            'If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetStudentInfo(ByVal studentId As String, Optional ByVal useRegent As Boolean = False, Optional ByVal enrollmentId As string = Nothing) As StudentMasterInfo
        'connect to the database
        Dim db As New DataAccess
        db.ConnectionString = GetConnectionString()

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" Select A.StudentId, " + vbCrLf)
            .Append(" A.FirstName,A.LastName,A.MiddleName,A.SSN, " + vbCrLf)
            .Append(" (SELECT TOP 1 EMail FROM dbo.AdLeadEmail WHERE LeadId = A.LeadId AND (IsPreferred = 1 OR IsShowOnLeadPage = 1) ORDER BY IsPreferred DESC, IsShowOnLeadPage DESC) As HomeEmail " + vbCrLf)
            .Append(", (Select Address1 from arStudAddresses where StudentId=A.StudentId And default1=1) as Address1, " + vbCrLf)
            .Append(" (Select Address2 from arStudAddresses where StudentId=A.StudentId And default1=1) as Address2, " + vbCrLf)
            .Append(" (Select city from arStudAddresses where StudentId=A.StudentId And default1=1) as City, " + vbCrLf)
            .Append(" (Select County from arStudent where StudentId=A.StudentId) as County, " + vbCrLf)
            .Append(" (select Distinct c.CountyDescrip from arStudent b, adCounties c where b.County=c.CountyId And b.StudentId = A.StudentId) as CountyDescrip, " + vbCrLf)

            .Append(" (SELECT IsInternational FROM AdLeadAddresses ad JOIN AdLeads l ON l.LeadId = ad.LeadId ")
            .Append(" WHERE l.StudentId = A.StudentId And ad.IsMailingAddress=1) as IsInternational, " + vbCrLf)

            .Append(" (Select StateId from arStudAddresses where StudentId=A.StudentId And default1=1) as State, " + vbCrLf)
            .Append(" (Select c.StateDescrip from arStudAddresses b, syStates c where b.StateId = c.StateId And b.StudentId = A.StudentId And b.default1 = 1) as StateDescrip, " + vbCrLf)
            .Append(" (Select Zip from arStudAddresses where StudentId=A.StudentId And default1=1) as Zip, " + vbCrLf)
            .Append(" (Select AddressTypeId from arStudAddresses where StudentId=A.StudentId And default1=1) as AddressType, " + vbCrLf)
            .Append(" (select Distinct c.AddressDescrip from arStudAddresses b,plAddressTypes c where b.AddressTypeId=c.AddressTypeId And b.StudentId = A.StudentId And b.default1 = 1) as AddressTypeDescrip, " + vbCrLf)
            .Append(" (Select CountryId from arStudAddresses where StudentId=A.StudentId And default1=1) as Country, " + vbCrLf)
            .Append(" (select Distinct c.CountryDescrip from arStudAddresses b, adCountries c where b.CountryId=c.CountryId And b.StudentId = A.StudentId And b.default1 = 1) as CountryDescrip, " + vbCrLf)
            .Append(" (Select PhoneTypeId from arStudentPhone where StudentId=A.StudentId And default1=1) as PhoneType, " + vbCrLf)
            .Append(" (select Distinct c.PhoneTypeDescrip from arStudentPhone b, syPhoneType c where b.PhoneTypeId=c.PhoneTypeId And b.StudentId = A.StudentId And b.default1 = 1) as PhoneTypeDescrip, " + vbCrLf)
            .Append(" (Select Phone from arStudentPhone where StudentId=A.StudentId And default1=1) as Phone, " + vbCrLf)
            .Append(" (Select StatusId from arStudAddresses where StudentId=A.StudentId And default1=1) as AddressStatus, " + vbCrLf)
            .Append(" (select Distinct c.Status from arStudAddresses b, syStatuses c where b.StatusId=c.StatusId And b.StudentId = A.StudentId And b.default1 = 1) as AddressStatusDescrip, " + vbCrLf)
            .Append(" (Select StatusId from arStudentPhone where StudentId=A.StudentId And default1=1) as PhoneStatus, " + vbCrLf)
            .Append(" (select Distinct c.Status from arStudentPhone b, syStatuses c where b.StatusId=c.StatusId And b.StudentId = A.StudentId And b.default1 = 1) as PhoneStatusDescrip, " + vbCrLf)
            .Append(" (select ForeignPhone from arStudentPhone where StudentId=A.StudentId And default1=1) as ForeignPhone, " + vbCrLf)
            .Append(" (select ForeignZip from arStudAddresses where StudentId=A.StudentId And default1=1) as ForeignZip, " + vbCrLf)
            .Append(" (select OtherState from arStudAddresses where StudentId=A.StudentId And default1=1) as OtherState, " + vbCrLf)
            .Append(" (select Top 1 LeadId from arStuEnrollments where StudentId=A.StudentId And LeadId Is Not null) as LeadId, " + vbCrLf)
            .Append(" A.StudentStatusId as StudentStatus, " + vbCrLf)
            .Append(" (select Distinct Status from syStatuses where StatusId=A.StudentStatusId) as StudentStatusDescrip, " + vbCrLf)
            .Append(" (SELECT TOP 1 EMail FROM dbo.AdLeadEmail WHERE LeadId = A.LeadId AND (IsPreferred = 0 OR IsShowOnLeadPage = 0) ORDER BY IsPreferred ASC, IsShowOnLeadPage ASC) As WorkEmail " + vbCrLf)
            .Append(" ,A.Prefix, A.Suffix, " + vbCrLf)
            .Append(" (select Distinct PrefixDescrip from syPrefixes where PrefixId=A.Prefix) as PrefixDescrip, " + vbCrLf)
            .Append(" (select Distinct SuffixDescrip from sySuffixes where SuffixId=A.Suffix) as SuffixDescrip, " + vbCrLf)
            .Append(" A.BirthDate as DOB,A.Sponsor,A.AssignedDate, " + vbCrLf)
            .Append(" (select Distinct AgencySpDescrip from adAgencySponsors where AgencySpId=A.Sponsor) as SponsorTypeDescrip, " + vbCrLf)
            .Append(" A.Gender,A.Race,A.MaritalStatus,A.FamilyIncome,A.Children, " + vbCrLf)
            .Append(" A.ExpectedStart,A.ShiftID, " + vbCrLf)
            .Append(" A.Nationality,A.Citizen,A.DrivLicStateID,A.DrivLicNumber,A.AlienNumber, " + vbCrLf)
            .Append(" (SELECT TOP (1) AN.NoteText FROM dbo.AllNotes AN LEFT JOIN dbo.adLead_Notes LN ON LN.NotesId = AN.NotesId
WHERE LN.LeadId = E.leadId AND AN.NoteType = 'Comment' AND LEN(LTRIM(RTRIM(AN.NoteText))) > 0 ORDER BY AN.ModDate DESC ) As Comments , " + vbCrLf)
            .Append(" A.StudentNumber,A.ModDate, " + vbCrLf)
            .Append(" A.DependencyTypeId,A.GeographicTypeId,A.AdminCriteriaId,A.HousingId, " + vbCrLf)
            .Append(" (select Distinct GenderDescrip from adGenders where GenderId=A.Gender) as GenderDescrip, " + vbCrLf)
            .Append(" (select Distinct EthCodeDescrip from adEthCodes where EthCodeId=A.Race) as EthCodeDescrip, " + vbCrLf)
            .Append(" (select Distinct MaritalStatDescrip from adMaritalStatus where MaritalStatId=A.MaritalStatus) as MaritalStatusDescrip, " + vbCrLf)
            .Append(" (select Distinct FamilyIncomeDescrip from syFamilyIncome where FamilyIncomeId=A.FamilyIncome) as FamilyIncomeDescrip, " + vbCrLf)
            .Append(" (select Distinct NationalityDescrip from adNationalities where NationalityId=A.Nationality) as NationalityDescrip, " + vbCrLf)
            .Append(" (select Distinct CitizenShipDescrip from adCitizenships where CitizenshipId=A.Citizen) as CitizenDescrip, " + vbCrLf)
            .Append(" (select Distinct StateDescrip from syStates where StateId=A.DrivLicStateId) as DriverLicenseStateDescrip, " + vbCrLf)
            .Append(" (select Distinct Descrip from adDependencyTypes where DependencyTypeId=A.DependencyTypeId) as DependencyTypeDescrip, " + vbCrLf)
            .Append(" (select Distinct Descrip from adGeographicTypes where GeographicTypeId=A.GeographicTypeId) as GeographicTypeDescrip, " + vbCrLf)
            .Append(" (select Distinct Descrip from adAdminCriteria where AdminCriteriaId=A.AdminCriteriaId) as AdminCriteriaDescrip, " + vbCrLf)
            .Append(" (select Distinct Descrip from arHousing where HousingId=A.HousingId) as HousingDescrip " + vbCrLf)
            If useRegent = True Then
                .Append(" ,entranceinterviewdate,highschoolprogramcode, " + vbCrLf)
           
            End If
            .Append(" from arStuEnrollments E LEFT JOIN  adleads A ON A.LeadId = E.LeadId " + vbCrLf)
            .Append(" where A.StudentId = ? ")

            If enrollmentId IsNot Nothing Then
                .Append(" and E.stuenrollId = ? ")
            End If
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@StudentId", studentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If enrollmentId IsNot Nothing Then
            db.AddParameter("@EnrollmentId", enrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If


        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'Dim strStateValue As String
        Dim studentInfo As New StudentMasterInfo
        While dr.Read()
            'set properties with data from DataReader
            With studentInfo
                .IsInDB = True
                .StudentID = CType(dr("StudentId"), Guid).ToString()
                If Not (dr("FirstName") Is DBNull.Value) Then .FirstName = CType(dr("FirstName"), String).ToString Else .FirstName = ""
                If Not (dr("LastName") Is DBNull.Value) Then .LastName = CType(dr("LastName"), String).ToString Else .LastName = ""
                If Not (dr("MiddleName") Is DBNull.Value) Then .MiddleName = CType(dr("MiddleName"), String).ToString Else .MiddleName = ""
                If Not (dr("SSN") Is DBNull.Value) Then .SSN = CType(dr("SSN"), String).ToString Else .SSN = ""
                If Not (dr("Phone") Is DBNull.Value) Then .Phone = CType(dr("Phone"), String).ToString Else .Phone = ""
                If Not (dr("HomeEmail") Is DBNull.Value) Then .HomeEmail = CType(dr("HomeEmail"), String).ToString.Trim Else .HomeEmail = ""
                If Not (dr("Address1") Is DBNull.Value) Then .Address1 = CType(dr("Address1"), String).ToString Else .Address1 = ""
                If Not (dr("Address2") Is DBNull.Value) Then .Address2 = CType(dr("Address2"), String).ToString Else .Address2 = ""
                If Not (dr("City") Is DBNull.Value) Then .City = CType(dr("City"), String).ToString Else .City = ""
                If Not (dr("state") Is DBNull.Value) Then .State = CType(dr("State"), Guid).ToString Else .State = Guid.Empty.ToString
                If Not (dr("LeadId") Is DBNull.Value) Then .LeadId = CType(dr("LeadId"), Guid).ToString Else .LeadId = ""

                ' Read is international
                If Not (dr("IsInternational") Is DBNull.Value) Then .IsInternational = CType(dr("IsInternational"), Boolean) Else .IsInternational = False


                If Not (dr("zip") Is DBNull.Value) Then .Zip = CType(dr("zip"), String).ToString Else .Zip = ""

                If Not (dr("Country") Is DBNull.Value) Then .Country = CType(dr("Country"), Guid).ToString Else .Country = Guid.Empty.ToString

                If Not (dr("County") Is DBNull.Value) Then .County = CType(dr("County"), Guid).ToString Else .County = Guid.Empty.ToString

                If Not (dr("DOB") Is DBNull.Value) Then .BirthDate = CType(dr("DOB"), String).ToString Else .BirthDate = ""
                If Not (dr("AssignedDate") Is DBNull.Value) Then .AssignmentDate = CType(dr("AssignedDate"), String).ToString Else .AssignmentDate = ""
                If Not (dr("ExpectedStart") Is DBNull.Value) Then .ExpectedStart = CType(dr("ExpectedStart"), String).ToString Else .ExpectedStart = ""
                If Not (dr("Comments") Is DBNull.Value) Then .Notes = CType(dr("Comments"), String).ToString Else .Notes = ""
                If Not (dr("Children") Is DBNull.Value) Then .Children = CType(dr("Children"), String).ToString Else .Children = ""

                If Not (dr("WorkEmail") Is DBNull.Value) Then .WorkEmail = CType(dr("WorkEmail"), String).ToString.Trim Else .WorkEmail = ""
                If Not (dr("StudentStatus") Is DBNull.Value) Then .Status = CType(dr("StudentStatus"), Guid).ToString Else .Status = Guid.Empty.ToString
                If Not (dr("AddressType") Is DBNull.Value) Then .AddressType = CType(dr("AddressType"), Guid).ToString Else .AddressType = Guid.Empty.ToString
                If Not (dr("Prefix") Is DBNull.Value) Then .Prefix = CType(dr("Prefix"), Guid).ToString Else .Prefix = Guid.Empty.ToString
                If Not (dr("Suffix") Is DBNull.Value) Then .Suffix = CType(dr("Suffix"), Guid).ToString Else .Suffix = Guid.Empty.ToString
                If Not (dr("Sponsor") Is DBNull.Value) Then .Sponsor = CType(dr("Sponsor"), Guid).ToString Else .Sponsor = Guid.Empty.ToString
                'If Not (dr("AdmissionsRep") Is System.DBNull.Value) Then .AdmissionsRep = CType(dr("AdmissionsRep"), Guid).ToString Else 
                .AdmissionsRep = Guid.Empty.ToString
                If Not (dr("Gender") Is DBNull.Value) Then .Gender = CType(dr("Gender"), Guid).ToString Else .Gender = Guid.Empty.ToString
                If Not (dr("Race") Is DBNull.Value) Then .Race = CType(dr("Race"), Guid).ToString Else .Race = Guid.Empty.ToString
                If Not (dr("MaritalStatus") Is DBNull.Value) Then .MaritalStatus = CType(dr("MaritalStatus"), Guid).ToString Else .MaritalStatus = Guid.Empty.ToString
                If Not (dr("FamilyIncome") Is DBNull.Value) Then .FamilyIncome = CType(dr("FamilyIncome"), Guid).ToString Else .FamilyIncome = Guid.Empty.ToString
                If Not (dr("PhoneType") Is DBNull.Value) Then .PhoneType = CType(dr("PhoneType"), Guid).ToString Else .PhoneType = Guid.Empty.ToString
                If Not (dr("PhoneStatus") Is DBNull.Value) Then .PhoneStatus = CType(dr("PhoneStatus"), Guid).ToString Else .PhoneStatus = Guid.Empty.ToString
                'If Not (dr("PrgVerId") Is System.DBNull.Value) Then .PrgVerId = CType(dr("PrgVerId"), Guid).ToString Else 
                .PrgVerId = Guid.Empty.ToString
                If Not (dr("ShiftId") Is DBNull.Value) Then .ShiftID = CType(dr("ShiftId"), Guid).ToString Else .ShiftID = Guid.Empty.ToString
                If Not (dr("Nationality") Is DBNull.Value) Then .Nationality = CType(dr("Nationality"), Guid).ToString Else .Nationality = Guid.Empty.ToString
                If Not (dr("Citizen") Is DBNull.Value) Then .Citizen = CType(dr("Citizen"), Guid).ToString Else .Citizen = Guid.Empty.ToString
                If Not (dr("DrivLicStateId") Is DBNull.Value) Then .DriverLicState = CType(dr("DrivLicStateId"), Guid).ToString Else .DriverLicState = Guid.Empty.ToString
                If Not (dr("DrivLicNumber") Is DBNull.Value) Then .DriverLicNumber = CType(dr("DrivLicNumber"), String).ToString Else .DriverLicNumber = ""
                If Not (dr("AlienNumber") Is DBNull.Value) Then .AlienNumber = CType(dr("AlienNumber"), String).ToString Else .AlienNumber = ""


                If Not (dr("AddressType") Is DBNull.Value) Then .AddressType = CType(dr("AddressType"), Guid).ToString Else .AddressType = Guid.Empty.ToString
                If Not (dr("AddressStatus") Is DBNull.Value) Then .AddressStatus = CType(dr("AddressStatus"), Guid).ToString Else .AddressStatus = Guid.Empty.ToString
                If Not (dr("StudentNumber") Is DBNull.Value) Then .StudentNumber = CType(dr("StudentNumber"), String).ToString Else .StudentNumber = ""
                If Not (dr("ForeignPhone") Is DBNull.Value) Then .ForeignPhone = dr("ForeignPhone") Else .ForeignPhone = 0
                If Not (dr("ForeignZip") Is DBNull.Value) Then .ForeignZip = dr("ForeignZip") Else .ForeignZip = 0

                If Not (dr("OtherState") Is DBNull.Value) Then .OtherState = CType(dr("OtherState"), String).ToString Else .OtherState = ""
                If Not (dr("ModDate") Is DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = Date.MinValue
                If Not (dr("DependencyTypeId") Is DBNull.Value) Then .DependencyTypeId = CType(dr("DependencyTypeId"), Guid).ToString Else .DependencyTypeId = ""
                If Not (dr("GeographicTypeId") Is DBNull.Value) Then .GeographicTypeId = CType(dr("GeographicTypeId"), Guid).ToString Else .GeographicTypeId = ""
                If Not (dr("AdminCriteriaId") Is DBNull.Value) Then .AdminCriteriaId = CType(dr("AdminCriteriaId"), Guid).ToString Else .AdminCriteriaId = ""
                If Not (dr("HousingId") Is DBNull.Value) Then .HousingId = CType(dr("HousingId"), Guid).ToString Else .HousingId = ""
                If Not (dr("StateDescrip") Is DBNull.Value) Then .StateDescrip = CType(dr("StateDescrip"), String).ToString Else .StateDescrip = ""
                If Not (dr("AddressTypeDescrip") Is DBNull.Value) Then .AddressTypeDescrip = CType(dr("AddressTypeDescrip"), String).ToString Else .AddressTypeDescrip = ""
                If Not (dr("PrefixDescrip") Is DBNull.Value) Then .PrefixDescrip = CType(dr("PrefixDescrip"), String).ToString Else .PrefixDescrip = ""
                If Not (dr("SuffixDescrip") Is DBNull.Value) Then .SuffixDescrip = CType(dr("SuffixDescrip"), String).ToString Else .SuffixDescrip = ""
                If Not (dr("AdminCriteriaDescrip") Is DBNull.Value) Then .AdminCriteriaDescrip = CType(dr("AdminCriteriaDescrip"), String).ToString Else .AdminCriteriaDescrip = ""
                If Not (dr("GenderDescrip") Is DBNull.Value) Then .GenderDescrip = CType(dr("GenderDescrip"), String).ToString Else .GenderDescrip = ""
                If Not (dr("EthCodeDescrip") Is DBNull.Value) Then .EthCodeDescrip = CType(dr("EthCodeDescrip"), String).ToString Else .EthCodeDescrip = ""
                If Not (dr("MaritalStatusDescrip") Is DBNull.Value) Then .MaritalStatusDescrip = CType(dr("MaritalStatusDescrip"), String).ToString Else .MaritalStatusDescrip = ""
                If Not (dr("FamilyIncomeDescrip") Is DBNull.Value) Then .FamilyIncomeDescrip = CType(dr("FamilyIncomeDescrip"), String).ToString Else .FamilyIncomeDescrip = ""
                If Not (dr("PhoneTypeDescrip") Is DBNull.Value) Then .PhoneTypeDescrip = CType(dr("PhoneTypeDescrip"), String).ToString Else .PhoneTypeDescrip = ""
                If Not (dr("NationalityDescrip") Is DBNull.Value) Then .NationalityDescrip = CType(dr("NationalityDescrip"), String).ToString Else .NationalityDescrip = ""
                If Not (dr("CitizenDescrip") Is DBNull.Value) Then .CitizenDescrip = CType(dr("CitizenDescrip"), String).ToString Else .CitizenDescrip = ""
                If Not (dr("DriverLicenseStateDescrip") Is DBNull.Value) Then .DriverLicenseStateDescrip = CType(dr("DriverLicenseStateDescrip"), String).ToString Else .DriverLicenseStateDescrip = ""



                If Not (dr("CountyDescrip") Is DBNull.Value) Then .CountyDescrip = CType(dr("CountyDescrip"), String).ToString Else .CountyDescrip = ""



                If Not (dr("CountryDescrip") Is DBNull.Value) Then .CountryDescrip = CType(dr("CountryDescrip"), String).ToString Else .CountryDescrip = ""
                If Not (dr("AddressStatusDescrip") Is DBNull.Value) Then .AddressStatusDescrip = CType(dr("AddressStatusDescrip"), String).ToString Else .AddressStatusDescrip = ""
                If Not (dr("PhoneStatusDescrip") Is DBNull.Value) Then .PhoneStatusDescrip = CType(dr("PhoneStatusDescrip"), String).ToString Else .PhoneStatusDescrip = ""
                If Not (dr("StudentStatusDescrip") Is DBNull.Value) Then .StudentStatusDescrip = CType(dr("StudentStatusDescrip"), String).ToString Else .StudentStatusDescrip = ""
                If Not (dr("DependencyTypeDescrip") Is DBNull.Value) Then .DependencyTypeDescrip = CType(dr("DependencyTypeDescrip"), String).ToString Else .DependencyTypeDescrip = ""
                If Not (dr("GeographicTypeDescrip") Is DBNull.Value) Then .GeographicTypeDescrip = CType(dr("GeographicTypeDescrip"), String).ToString Else .GeographicTypeDescrip = ""
                If Not (dr("HousingDescrip") Is DBNull.Value) Then .HousingDescrip = CType(dr("HousingDescrip"), String).ToString Else .HousingDescrip = ""
                If Not (dr("SponsorTypeDescrip") Is DBNull.Value) Then .SponsorTypeDescrip = dr("SponsorTypeDescrip").ToString Else .SponsorTypeDescrip = ""

                'If Not (dr("StudentGrpId") Is System.DBNull.Value) Then .StudentGrpId = CType(dr("StudentGrpId"), Guid).ToString Else 
                .StudentGrpId = ""
                If useRegent = True Then
                    If Not (dr("entranceinterviewdate") Is DBNull.Value) Then .EntranceInterviewDate = dr("entranceinterviewdate") Else .EntranceInterviewDate = Date.Now
                    If Not (dr("highschoolprogramcode") Is DBNull.Value) Then .HighSchoolProgramCode = dr("highschoolprogramcode") Else .HighSchoolProgramCode = ""
                End If
              
                

            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return StudentMasterInfo
        Return studentInfo
    End Function
    Public Function AddStudentIdFormat(ByVal strFormatType As String, ByVal strYearNumber As Integer, ByVal strMonthNumber As Integer, ByVal strDateNumber As Integer, ByVal strLNameNumber As Integer, ByVal strFNameNumber As Integer, ByVal strSeqNumber As Integer, ByVal strStartingSeqNumber As Integer, ByVal user As String) As Integer

        'Connect To The Database
        Dim db As New DataAccess
        db.ConnectionString = GetConnectionString()
        'Do an Update
        'Try
        Dim sb1 As New StringBuilder
        With sb1
            .Append("Truncate table syStudentFormat ")
        End With

        'Get FormatType
        db.AddParameter("@FormatType", strFormatType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'Execute The Query
        db.RunParamSQLExecuteNoneQuery(sb1.ToString)

        'Clear Parameters
        db.ClearParameters()
        sb1.Remove(0, sb1.Length)

        Dim sb As New StringBuilder
        With sb
            .Append("Insert syStudentFormat(FormatType,YearNumber,MonthNumber,DateNumber,LNameNumber,FNameNumber,SeqNumber,SeqStartingNumber,ModUser,ModDate) ")
            .Append(" Values(?,?,?,?,?,?,?,?,?,?) ")
        End With

        'Get FormatType
        db.AddParameter("@FormatType", strFormatType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'Get YearNumber
        If strYearNumber = 0 Then
            db.AddParameter("@YearNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@YearNumber", strYearNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        'Get MonthYear
        If strMonthNumber = 0 Then
            db.AddParameter("@MonthNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@MonthNumber", strMonthNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        'Get DateNumber
        If strDateNumber = 0 Then
            db.AddParameter("@DateNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@DateNumber", strDateNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        'Get LastName
        If strLNameNumber = 0 Then
            db.AddParameter("@LNameNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@LNameNumber", strLNameNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        'Get FirstName
        If strFNameNumber = 0 Then
            db.AddParameter("@FNameNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@FNameNumber", strFNameNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        'Get Sequential Number
        If strSeqNumber = 0 Then
            db.AddParameter("@SeqNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@SeqNumber", strSeqNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        'Get Sequential Number
        If strStartingSeqNumber = 0 Then
            db.AddParameter("@StartSeqNumber", DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@StartSeqNumber", strStartingSeqNumber, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        'ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'ModDate
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        'Execute The Query
        db.RunParamSQLExecuteNoneQuery(sb.ToString)

        'Retun Without Errors
        Return 0

        'Catch ex As System.Exception
        'Return an Error To Client
        '    Return -1

        'Finally
        'Close Connection
        '    db.CloseConnection()
        'End Try
    End Function
    Public Function GetStudentFormat() As StudentMasterInfo
        'connect to the database
        Dim db As New DataAccess
        db.ConnectionString = GetConnectionString()

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select YearNumber,MonthNumber,DateNumber, ")
            .Append(" LNameNumber,FNameNumber,SeqNumber, ")
            .Append(" SeqStartingNumber, FormatType ")
            .Append(" from syStudentFormat ")
        End With


        'Execute the query
        Dim dr As OleDbDataReader = db.RunSQLDataReader(sb.ToString)
        Dim studentInfo As New StudentMasterInfo
        While dr.Read()
            'set properties with data from DataReader
            With studentInfo
                If Not (dr("YearNumber") Is DBNull.Value) Then .YearNumber = CType(dr("YearNumber"), Integer) Else .YearNumber = 0
                If Not (dr("MonthNumber") Is DBNull.Value) Then .MonthNumber = CType(dr("MonthNumber"), Integer) Else .MonthNumber = 0
                If Not (dr("DateNumber") Is DBNull.Value) Then .DateNumber = CType(dr("DateNumber"), Integer) Else .DateNumber = 0
                If Not (dr("LNameNumber") Is DBNull.Value) Then .LNameNumber = CType(dr("LNameNumber"), Integer) Else .LNameNumber = 0
                If Not (dr("FNameNumber") Is DBNull.Value) Then .FNameNumber = CType(dr("FNameNumber"), Integer) Else .FNameNumber = 0
                If Not (dr("SeqNumber") Is DBNull.Value) Then .SeqNumber = CType(dr("SeqNumber"), Integer) Else .SeqNumber = 0
                If Not (dr("FormatType") Is DBNull.Value) Then .FormatType = CType(dr("FormatType"), String) Else .FormatType = ""
                If Not (dr("SeqStartingNumber") Is DBNull.Value) Then .SeqStartNumber = CType(dr("SeqStartingNumber"), Integer) Else .SeqStartNumber = 0
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return StudentMasterInfo
        Return studentInfo
    End Function
    Public Function CheckChildExists(ByVal educationInstId As String) As Integer
        'connect to the database
        Dim db As New DataAccess
        db.ConnectionString = GetConnectionString()

        Dim sb As New StringBuilder
        With sb
            .Append(" select Count(*) as ChildExists from ")
            .Append(" (select Distinct EducationInstId from plStudentEducation where EducationInstId = ? ")
            .Append(" union ")
            .Append(" Select Distinct EducationInstId from adLeadEducation where EducationInstId = ?) ED ")
        End With
        db.AddParameter("@EducationInstId", educationInstId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@EducationInstId", educationInstId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim intCheckChild As Integer = db.RunParamSQLScalar(sb.ToString)
        Return intCheckChild
    End Function

    Public Function CheckDuplicateSSN(ByVal firstName As String, ByVal lastName As String, ByVal ssn As String) As String
        If String.IsNullOrEmpty(firstName) Or String.IsNullOrEmpty(lastName) Or String.IsNullOrEmpty(ssn) Then
            Return Nothing
        Else
            Dim db As New SQLDataAccess
            Dim sb As New StringBuilder
            Try
                db.ConnectionString = GetConnectionString()
                db.AddParameter("@SSN", ssn, SqlDbType.VarChar, 50, ParameterDirection.Input)
                db.AddParameter("@FirstName", firstName, SqlDbType.VarChar, 250, ParameterDirection.Input)
                db.AddParameter("@LastName", lastName, SqlDbType.VarChar, 250, ParameterDirection.Input)

                Using rs As SqlDataReader = db.RunParamSQLDataReader_SP("dbo.CheckForDuplicateSSN")

                    If rs.HasRows Then
                        sb.Append("SSN already exists for the lead(s) Or Student(s) below:" & vbCrLf)
                        Do While rs.Read()
                            With sb
                                .Append(rs("FirstName").ToString() & " " & rs("LastName").ToString())
                                If rs("Status") IsNot DBNull.Value Then
                                    .Append(", Status: " & rs("Status") & vbCrLf)
                                End If
                            End With
                        Loop
                    Else
                        Return Nothing
                    End If
                End Using

                Return sb.ToString()

            Catch ex As Exception
                Return Nothing
            Finally
                db.CloseConnection()
            End Try

        End If
    End Function
    Public Function GetAllDependencyType() As DataSet

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = GetConnectionString()

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   Distinct BC.DependencyTypeId, BC.Descrip ")
            .Append("FROM     adDependencyTypes BC, syStatuses ST ")
            .Append("WHERE    BC.StatusId = ST.StatusId ")
            .Append("And      ST.Status = 'Active' ")
            .Append("ORDER BY BC.Descrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllGeographicType() As DataSet
        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = GetConnectionString()

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT Distinct  BC.GeographicTypeId, BC.Descrip ")
            .Append("FROM     adGeographicTypes BC, syStatuses ST ")
            .Append("WHERE    BC.StatusId = ST.StatusId ")
            .Append("AND      ST.Status = 'Active' ")
            .Append("ORDER BY BC.Descrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllAdminCriteria() As DataSet
        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = GetConnectionString()

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT Distinct  BC.AdminCriteriaId, BC.Descrip ")
            .Append("FROM     adAdminCriteria BC, syStatuses ST ")
            .Append("WHERE    BC.StatusId = ST.StatusId ")
            .Append("AND      ST.Status = 'Active' ")
            .Append("ORDER BY BC.Descrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllHousingType() As DataSet
        Dim db As New DataAccess
        db.ConnectionString = GetConnectionString()

        Dim sb As New StringBuilder
        With sb
            .Append("SELECT Distinct  BC.HousingId, BC.Descrip ")
            .Append("FROM     arHousing BC, syStatuses ST ")
            .Append("WHERE    BC.StatusId = ST.StatusId ")
            .Append("AND      ST.Status = 'Active' ")
            .Append("ORDER BY BC.Descrip ")
        End With
        Return db.RunSQLDataSet(sb.ToString)
    End Function

    ''' <summary>
    ''' Get conection string
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetConnectionString() As String
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connectionString As String = myAdvAppSettings.AppSettings("ConString")
        Return connectionString
    End Function

    Public Function GetStudentImage(studentId As String) As Byte()
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim constring As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim conn = New SqlConnection(constring)

        'Create the query
        Dim sb As New StringBuilder

        sb.Append(" SELECT [Image] FROM doc.LeadImage ")
        sb.Append("	WHERE TypeId = 1 ")
        sb.Append(" AND LeadId = (SELECT TOP 1 LeadId FROM AdLeads WHERE StudentId = @StudentId) ")

        'Enter parameter
        Dim command As SqlCommand = New SqlCommand(sb.ToString(), conn)
        command.Parameters.AddWithValue("@StudentId", studentId)
        conn.Open()
        Try
            Dim imagesbytes = command.ExecuteScalar()
            Return imagesbytes
        Finally
            conn.Close()
        End Try
    End Function

    Public Function GetShadowLead(studentId As String) As String
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim constring As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim conn = New SqlConnection(constring)

        'Create the query
        Dim sb As New StringBuilder

        sb.Append("SELECT LeadId FROM AdLeads WHERE StudentId = @StudentId ")

        'Enter parameter
        Dim command = New SqlCommand(sb.ToString(), conn)
        command.Parameters.AddWithValue("@StudentId", studentId)
        conn.Open()
        Try
            Dim shadow = command.ExecuteScalar()
            Return shadow.ToString()
        Finally
            conn.Close()
        End Try
    End Function
End Class
