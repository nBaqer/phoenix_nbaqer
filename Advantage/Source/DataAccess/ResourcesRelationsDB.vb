Imports FAME.Advantage.Common

Public Class ResourcesRelationsDB
    Public Function GetModules() As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT ResourceID,Resource ")
            .Append("FROM syResources ")
            .Append("WHERE ResourceTypeId = 1 ")
            .Append("ORDER BY Resource")
        End With

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function

    Public Function GetDefaultModules() As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT ResourceID,Resource ")
            .Append("FROM syResources ")
            .Append("WHERE ResourceTypeId = 1 ")
            .Append("AND ResourceID <> 196 ")
            .Append("ORDER BY Resource")
        End With

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function
    Public Function GetModulesByRoleId(ByVal roleId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT ")
            .Append("       ResourceID, ")
            .Append("       Resource ")
            .Append("FROM   syRolesModules RM, syResources R ")
            .Append("WHERE ")
            .Append("       RM.ModuleId=R.ResourceId ")
            .Append("AND    RM.RoleId=? ")
            .Append("ORDER BY Resource")
        End With

        'add roleId parameter
        db.AddParameter("@RoleId", roleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function GetResourceTypes() As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            ' build query to retrieve resource types excluding Module(1), Submodule(2) and System Settings(6)
            .Append("SELECT ResourceTypeID, ResourceType ")
            .Append("FROM syResourceTypes ")
            .Append("WHERE ResourceTypeID NOT IN (1,2,6,7,8,9,10)")
        End With
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds.Tables(0)
    End Function

    Public Function GetResourceTypesForSchlReqFields() As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT ResourceTypeID,ResourceType ")
            .Append("FROM syResourceTypes ")
            .Append("WHERE ResourceTypeID IN(3,4)")
        End With

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function

    Public Function GetResourcesRelations() As DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        With sb
            .Append("SELECT ResourceID, Resource, ResourceTypeID, ResourceURL, AllowSchlReqFlds ")
            .Append("FROM syResources")
        End With

        da = db.RunParamSQLDataAdapter(sb.ToString())
        da.Fill(ds, "Resources")
        db.ClearParameters()
        sb.Remove(0, sb.Length)


        With sb
            .Append("SELECT t1.ResRelId, t1.ResourceId, t1.ParentId, t1.Sequence,t2.Resource,t2.ResourceTypeID,t2.ChildTypeId,t2.AllowSchlReqFlds ")
            .Append("FROM syResourceRelations t1, syResources t2 ")
            .Append("WHERE t1.ResourceId = t2.ResourceID ")
            .Append("ORDER BY t2.Resource")
        End With

        da = db.RunParamSQLDataAdapter(sb.ToString)
        da.Fill(ds, "ResourceRelations")
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Make certain we close the connection
        db.CloseConnection()

        'Add primary key to the Resourced datatable
        With ds.Tables("Resources")
            .PrimaryKey = New DataColumn() {.Columns("ResourceID")}
        End With

        'create a relation between the Resources and the ResourceRelations tables.
        ds.Relations.Add("ResourcesResourceRelations", _
         ds.Tables("Resources").Columns("ResourceID"), _
         ds.Tables("ResourceRelations").Columns("ResourceId"))

        Return ds

    End Function

    'This function excludes fields that are required by Advantage and bit fields
    Public Function GetFieldsForPage(ByVal resourceId As Integer) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        ' DE6156 01/12/2012 Janet Robinson add ipeds indicator
        ' DE7175 02/15/2012 Janet Robinson add DOB as IPEDS Required
        ' DE7172 02/15/2012 Janet Robinson add FamilyIncome as IPEDS Required
        If resourceId = 174 Then
            With sb
                .Append("SELECT t1.ResDefId,t1.SchlReq,t2.TblFldsId,t2.FldId,t3.FldName, ")
                .Append("Caption = CASE ")
                .Append(" WHEN t2.FldId = 222 THEN 'DOB (IPEDS)' ")
                .Append(" WHEN t2.FldId = 81 THEN 'Education Level (IPEDS)' ")
                .Append(" WHEN t2.FldId = 728 THEN 'Family Income (IPEDS)'  ")
                .Append(" WHEN t2.FldId  IN ( SELECT distinct t2.FldId FROM syRptAgencyFields t1, syRptFields t2 WHERE t1.RptFldId = t2.RptFldId AND t1.RptAgencyId=1 AND t2.FldId IS NOT NULL) THEN t4.Caption + ' (IPEDS)' ")
                .Append(" 	ELSE t4.Caption END ")
                .Append("FROM syResTblFlds t1, syTblFlds t2, syFields t3, syFldCaptions t4, syFieldTypes t5 ")
                .Append("WHERE t1.ResourceId = ? ")
                .Append("AND t1.Required = 0 ")
                .Append("AND t1.TblFldsId=t2.TblFldsId ")
                .Append("AND t2.FldId=t3.FldId ")
                .Append("AND t3.FldId=t4.FldId ")
                .Append("AND t3.FldTypeId=t5.FldTypeId ")
                .Append("AND t5.FldTypeId <> 11 ")
                .Append("AND t4.LangId = 1 ")
                .Append("ORDER BY t3.FldName")
            End With
        Else
            With sb
                .Append("SELECT t1.ResDefId,t1.SchlReq,t2.TblFldsId,t2.FldId,t3.FldName, t4.Caption ")
                .Append("FROM syResTblFlds t1, syTblFlds t2, syFields t3, syFldCaptions t4, syFieldTypes t5 ")
                .Append("WHERE t1.ResourceId = ? ")
                .Append("AND t1.Required = 0 ")
                .Append("AND t1.TblFldsId=t2.TblFldsId ")
                .Append("AND t2.FldId=t3.FldId ")
                .Append("AND t3.FldId=t4.FldId ")
                .Append("AND t3.FldTypeId=t5.FldTypeId ")
                .Append("AND t5.FldTypeId <> 11 ")
                .Append("AND t4.LangId = 1 ")
                .Append("ORDER BY t3.FldName")
            End With
        End If

        db.AddParameter("@resid", resourceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function

    Public Function GetSchoolRequiredFldsForResource(ByVal resourceId As Integer) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT TblFldsId ")
            .Append("FROM syInstResFldsReq ")
            .Append("WHERE ResourceId = ?")
        End With

        db.AddParameter("@resid", resourceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function

    Public Sub AddResourceRequiredField(ByVal resReqFldId As String, ByVal resourceId As Integer, ByVal tblFldsId As Integer, ByVal user As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        With sb
            .Append("INSERT INTO syInstResFldsReq(ResReqFldId,ResourceId,TblFldsId,ModUser,ModDate) ")
            .Append("VALUES(?,?,?,?,?)")
        End With

        db.AddParameter("@PKID", resReqFldId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@resid", resourceId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
        db.AddParameter("@fldid", tblFldsId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

        'ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'ModDate
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    End Sub

    Public Sub DeleteResourceRequiredField(ByVal resourceId As Integer, ByVal tblFldsId As Integer)
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        With sb
            .Append("DELETE FROM syInstResFldsReq ")
            .Append("WHERE ResourceId = ? ")
            .Append("AND TblFldsId = ?")
        End With

        db.AddParameter("@resid", resourceId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
        db.AddParameter("@fldid", tblFldsId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    End Sub

    Public Function DoesResourceBelongToASubmodule(ByVal resourceId As Integer) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim numFound As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        With sb
            .Append("SELECT COUNT(*) ")
            .Append("FROM syResourceRelations t1, syResources t2 ")
            .Append("WHERE t1.ParentId=t2.ResourceId ")
            .Append("AND t2.ResourceTypeId = 2 ")
            .Append("AND t1.ResourceId = ? ")
        End With

        db.AddParameter("@resid", resourceId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

        numFound = CInt(db.RunParamSQLScalar(sb.ToString))

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        If numFound = 0 Then
            Return False
        Else
            Return True
        End If


    End Function
End Class
