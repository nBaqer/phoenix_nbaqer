
Imports System.Xml
Imports System.Security.Cryptography
Imports FAME.Advantage.Common

Public Class UserSecurityDB

    Public Function GetAdvAppSettings()
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

    Public Function GetRolesFromDatabase(ByVal UserSecurity As UserSecurityInfo) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT RoleId, CampGrpId ")
            .Append(" FROM syUsersRolesCampGrps ")
            .Append(" WHERE UserId = ? ")
            .Append("and CampgrpId in (select CampgrpId from syCmpGrpCmps where CampusId=(Select CampusId from syUsers where UserId=?))")
        End With

        db.OpenConnection()
        db.AddParameter("@userid", UserSecurity.UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@userid", UserSecurity.UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString, "RoleList")

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds

    End Function
    Public Function GetAllRoles() As DataTable
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        If (HttpContext.Current.Session("UserName") = "Support")
            With sb
                .Append("SELECT RoleID,Role ")
                .Append("FROM syRoles R ")
                .Append("WHERE SysRoleId NOT IN (15,17)") 
                .Append("ORDER BY Role")
            End With
        Else
            With sb
                .Append("SELECT RoleID,Role ")
                .Append("FROM syRoles R ")
                .Append("JOIN sySysRoles SR ON R.SysRoleId = SR.SysRoleId ")
                .Append("JOIN syUserType UT ON SR.RoleTypeId = UT.UserTypeId ")
                .Append("WHERE SR.SysRoleId<> 15 AND UT.Code = 'Advantage' ")  ' AND RoleType Not API
                .Append("ORDER BY Role")
            End With
        End If
        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function GetRoleAccessLevels(ByVal roleID As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString


        With sb
            .Append("SELECT RoleID,ResourceID,AccessLevel ")
            .Append("FROM syRlsResLvls ")
            .Append("WHERE RoleID = ?")
        End With

        db.AddParameter("@roleid", roleID, DataAccess.OleDbDataType.OleDbString)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Sub DeleteRoleResourceAccessLevels(ByVal roleId As String, ByVal resId As Integer)
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        With sb
            .Append("DELETE FROM syRlsResLvls ")
            .Append("WHERE syRlsResLvls.RoleID = ? ")
            .Append("AND syRlsResLvls.ResourceID = ?")
        End With

        db.AddParameter("@roleguid", roleId, DataAccess.OleDbDataType.OleDbString)
        db.AddParameter("@resid", resId, DataAccess.OleDbDataType.OleDbInteger)

        db.RunParamSQLExecuteNoneQuery(sb.ToString())
        'Clear parameters
        db.ClearParameters()
    End Sub

    Public Sub AddRoleResourceAccessLevel(ByVal roleId As String, ByVal resourceId As Integer, ByVal accessLevel As Integer, ByVal user As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        With sb
            .Append("INSERT INTO syRlsResLvls(RoleId,ResourceID,AccessLevel,ModUser,ModDate) ")
            .Append("VALUES(?,?,?,?,?)")
        End With

        db.AddParameter("@roleid", roleId, DataAccess.OleDbDataType.OleDbString)
        db.AddParameter("@resourceid", resourceId, DataAccess.OleDbDataType.OleDbInteger)
        db.AddParameter("@level", accessLevel, DataAccess.OleDbDataType.OleDbInteger)

        'ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'ModDate
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)


        db.RunParamSQLExecuteNoneQuery(sb.ToString())
        'Clear parameters
        db.ClearParameters()
    End Sub

    Public Function VerifyUserNamePassword(ByVal UserSecurityInfo As UserSecurityInfo) As Int32
        '   return code 0 = sucessful login
        '   return code 1 = incorrect username/password combination
        '   return code 2 = users account is not active 
        '   return code 3 = user was found, and validated password, but no roles have been assigned to him/her
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT UserName, Password, Salt, AccountActive ")
            .Append("FROM syUsers ")
            .Append("WHERE UserName = ? ")
            .Append("AND Password = ?")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@userid", UserSecurityInfo.UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''Code Commented and Added by Kamalesh Ahuja on April 20 2010 to resolve Mantis Issue Id 18047
        ''db.AddParameter("@password", UserSecurityInfo.UserPassword, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        If UserSecurityInfo.UserName.ToUpper = "SUPER" Or UserSecurityInfo.UserName.ToUpper = "SUPPORT" Then
            'Password
            db.AddParameter("@password", Encrypt(UserSecurityInfo.UserPassword), DataAccess.OleDbDataType.OleDbString, 40, ParameterDirection.Input)
        Else
            'Password
            db.AddParameter("@password", UserSecurityInfo.UserPassword, DataAccess.OleDbDataType.OleDbString, 40, ParameterDirection.Input)
        End If
        ''''''''''

        Try

            Dim reader As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            If reader.HasRows Then
                reader.Read()
                ' Check to see if the user is active or not
                Dim Active As Boolean = reader.GetValue(3)
                If Active = True Then
                    'The user has successfully logged in and account is active
                    Return 0
                Else
                    'The account is not active - user goes no further unless it is the sa account
                    If UserSecurityInfo.UserName = "sa" Then
                        Return 0
                    Else
                        Return 2
                    End If

                End If
            Else
                'Incorrect username/password
                Return 1
            End If
            If Not reader.IsClosed Then reader.Close()
        Catch ex As Exception
            Throw New Exception("Exception verifying password. " + ex.Message)
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try

    End Function
    Public Function CheckDuplicateUserOrEmail(username As String, email As String) As Boolean
        '   return code 0 = sucessful login
        '   return code 1 = incorrect username/password combination
        '   return code 2 = users account is not active 
        '   return code 3 = user was found, and validated password, but no roles have been assigned to him/her
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT *  ")
            .Append("FROM syUsers ")
            .Append("WHERE UserName = ? ")
            .Append(" OR email = ?")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@userid", username, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@email", email, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ''Code Commented and Added by Kamalesh Ahuja on April 20 2010 to resolve Mantis Issue Id 18047
        ''db.AddParameter("@password", UserSecurityInfo.UserPassword, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        Try

            Dim reader As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            If reader.HasRows Then
                Return True
            Else
                Return False
            End If
            If Not reader.IsClosed Then reader.Close()
        Catch ex As Exception
            Throw New Exception("Exception verifying password. " + ex.Message)
        Finally
            db.CloseConnection()
        End Try
        Return False
    End Function
    Public Function CheckDuplicateUserOrEmail(username As String, email As String, userID As String) As Boolean
        '   return code 0 = sucessful login
        '   return code 1 = incorrect username/password combination
        '   return code 2 = users account is not active 
        '   return code 3 = user was found, and validated password, but no roles have been assigned to him/her
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT *  ")
            .Append("FROM syUsers ")
            .Append("WHERE (UserName = ? ")
            .Append(" OR email = ? ) AND userID <> ? ")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@userid", username, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@email", email, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@userid", userID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ''Code Commented and Added by Kamalesh Ahuja on April 20 2010 to resolve Mantis Issue Id 18047
        ''db.AddParameter("@password", UserSecurityInfo.UserPassword, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        Try

            Dim reader As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            If reader.HasRows Then
                Return True
            Else
                Return False
            End If
            If Not reader.IsClosed Then reader.Close()
        Catch ex As Exception
            Throw New Exception("Exception verifying password. " + ex.Message)
        Finally
            db.CloseConnection()
        End Try
        Return False
    End Function
    Public Function AddNewUser(ByVal UserSecurityInfo As UserSecurityInfo) As Boolean
        ' Write the User record first
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("INSERT INTO syUsers")
            .Append("(Username, Password, Salt, AccountActive, ModDate, ModUser)")
            .Append(" VALUES (?,?,?,?,?,?)")
        End With
        db.AddParameter("@Param1", UserSecurityInfo.UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@Param2", UserSecurityInfo.UserPassword, DataAccess.OleDbDataType.OleDbString, 40, ParameterDirection.Input)
        db.AddParameter("@Param3", UserSecurityInfo.Salt, DataAccess.OleDbDataType.OleDbString, 10, ParameterDirection.Input)
        db.AddParameter("@Param4", UserSecurityInfo.AccountActive, DataAccess.OleDbDataType.OleDbBoolean, 1, ParameterDirection.Input)
        db.AddParameter("@Param5", Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        db.AddParameter("@Param6", UserSecurityInfo.UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex.InnerException)
            Return False
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
        ' Now write Question1 
        Dim strSQL2 As New StringBuilder
        With strSQL2
            .Append("INSERT INTO syUserSpecQuestions")
            .Append("( UserName, UserQuestionId, UserAnswer, ModDate, ModUser)")
            .Append(" VALUES (?,?,?,?,?)")
        End With
        db.AddParameter("@Param1", UserSecurityInfo.UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@Param2", UserSecurityInfo.Question1, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@Param3", UserSecurityInfo.Answer1, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@Param4", Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        db.AddParameter("@Param5", UserSecurityInfo.UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL2.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex.InnerException)
            Return False
        End Try
        db.ClearParameters()
        strSQL2.Remove(0, strSQL.Length)

        ' Now write Question2 
        'Dim strSQL3 As New StringBuilder
        'With strSQL3
        '    .Append("INSERT INTO syUserSpecQuestions")
        '    .Append("( UserName, UserQuestionId, UserAnswer, ModDate, ModUser)")
        '    .Append(" VALUES (?,?,?,?,?)")
        'End With
        'db.AddParameter("@Param1", UserSecurityInfo.UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'db.AddParameter("@Param2", UserSecurityInfo.Question2, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        'db.AddParameter("@Param3", UserSecurityInfo.Answer2, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'db.AddParameter("@Param4", Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        'db.AddParameter("@Param5", UserSecurityInfo.UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'Try
        '    db.RunParamSQLExecuteNoneQuery(strSQL3.ToString)
        'Catch ex As System.Exception
        '    'Redirect to error page.
        '    Throw New Exception(ex.Message, ex.InnerException)
        '    Return False
        'End Try
        'db.ClearParameters()
        'strSQL3.Remove(0, strSQL.Length)
        '' Now write Question3
        'Dim strSQL4 As New StringBuilder
        'With strSQL4
        '    .Append("INSERT INTO syUserSpecQuestions")
        '    .Append("( UserName, UserQuestionId, UserAnswer, ModDate, ModUser)")
        '    .Append(" VALUES (?,?,?,?,?)")
        'End With
        'db.AddParameter("@Param1", UserSecurityInfo.UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'db.AddParameter("@Param2", UserSecurityInfo.Question3, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        'db.AddParameter("@Param3", UserSecurityInfo.Answer3, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'db.AddParameter("@Param4", Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        'db.AddParameter("@Param5", UserSecurityInfo.UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'Try
        '    db.RunParamSQLExecuteNoneQuery(strSQL4.ToString)
        'Catch ex As System.Exception
        '    'Redirect to error page.
        '    Throw New Exception(ex.Message, ex.InnerException)
        '    Return False
        'End Try
        'db.ClearParameters()
        'strSQL4.Remove(0, strSQL.Length)

    End Function
    Public Function GetAccessByRoleResource(ByVal ResourceId As Int32) As DataSet

        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("SELECT DISTINCT RoleID, AccessLevel ")
            .Append("FROM syRlsResLvls ")
            .Append("WHERE ResourceID = ?")
        End With
        db.OpenConnection()
        db.AddParameter("@ResourceId", ResourceId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        Try
            da = db.RunParamSQLDataAdapter(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "ResourceRoleList")

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return ds
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
    End Function
    Public Function GetEmployeeBasedOnNewUser(ByVal UserSecurityInfo As UserSecurityInfo) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder

        With strSQLString
            .Append("SELECT * ")
            .Append(" FROM hrEmployees")
            .Append(" WHERE LastName = ? AND")
            .Append(" FirstName = ? AND")
            .Append(" MI = ? AND")
            .Append(" BirthDate = ?")
        End With
        db.OpenConnection()
        db.AddParameter("@LastName", UserSecurityInfo.LastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@FirstName", UserSecurityInfo.FirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@MI", UserSecurityInfo.MI, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@BirthDate", UserSecurityInfo.BirthDate, DataAccess.OleDbDataType.OleDbDateTime, 4, ParameterDirection.Input)
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "EmployeeList")
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
            Return ds
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try

    End Function
    Public Function LinkNewUserToEmployee(ByVal EmpId As Guid, ByVal UserName As String) As Boolean
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("UPDATE hrEmployees SET ")
            .Append("Username = ?")
            .Append(" WHERE EmpId = ? ")
        End With
        db.AddParameter("@Param2", UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@Param3", EmpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
            Return True
        Catch ex As System.Exception
            'Redirect to error page.
            Return False
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
    End Function
    Public Function GetProfileAnswers(ByVal username As String) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder

        With strSQLString
            .Append("SELECT UserQuestionId, UserAnswer")
            .Append(" FROM syUserSpecQuestions")
            .Append(" WHERE UserName = ?")
        End With
        db.AddParameter("@UserName", username, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "AnswerList")
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
            Return ds
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try

    End Function
    Public Function GetProfileQuestions(ByVal UserName As String) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder

        With strSQLString
            .Append("SELECT b.UserQuestionDescrip")
            .Append(" FROM syUserSpecQuestions a, syUserQuestions b")
            .Append(" WHERE a.UserName = ? AND")
            .Append(" a.UserQuestionId = b.UserQuestionId ")

        End With
        db.AddParameter("@UserName", UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "QuestionList")
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
            Return ds
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
    End Function
    Public Function GetAllProfileQuestions() As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder

        With strSQLString
            .Append("SELECT UserQuestionId, UserQuestionDescrip ")
            .Append(" FROM syUserQuestions")

        End With
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "AllQuestionList")
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
            Return ds
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
    End Function
    Public Function GetUserSecurityInfoRecord(ByVal UserName As String) As UserSecurityInfo

        Dim FoundUserSecurityInfo As New UserSecurityInfo
        Dim passwordMatch As Boolean = False
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        Dim connString As String = MyAdvAppSettings.AppSettings("ConString").ToString
        Dim conn As OleDbConnection = New OleDbConnection(connString)
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("SELECT * FROM syUsers WHERE UserName = ?")
        End With
        Dim cmd As OleDbCommand = New OleDbCommand(strSQL.ToString, conn)
        cmd.CommandType = CommandType.Text
        Dim sqlParam As OleDbParameter = cmd.Parameters.Add("@UserName", OleDbType.VarChar, 50)
        sqlParam.Value = UserName
        Try
            conn.Open()
            Dim reader As OleDbDataReader = cmd.ExecuteReader()
            If reader.HasRows Then
                reader.Read()
                FoundUserSecurityInfo.UserName = reader.GetValue(0)
                FoundUserSecurityInfo.UserPassword = reader.GetValue(1)
                FoundUserSecurityInfo.Salt = reader.GetValue(2)
                FoundUserSecurityInfo.AccountActive = reader.GetValue(3)
                FoundUserSecurityInfo.Question1 = reader.GetValue(6)
                FoundUserSecurityInfo.Question2 = reader.GetValue(7)
                FoundUserSecurityInfo.Question3 = reader.GetValue(8)
                FoundUserSecurityInfo.Answer1 = reader.GetValue(9)
                FoundUserSecurityInfo.Answer2 = reader.GetValue(10)
                FoundUserSecurityInfo.Answer3 = reader.GetValue(11)
                Return FoundUserSecurityInfo
            Else
                'Username was not found
                Return FoundUserSecurityInfo
            End If
            If Not reader.IsClosed Then reader.Close()
        Catch ex As Exception
            Throw New Exception("Exception verifying password. " + ex.Message)
        Finally
            conn.Close()
        End Try
    End Function
    Public Function ResetPassword(ByVal UserName As String, ByVal Password As String, ByVal Salt As String) As Boolean
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("UPDATE syUsers SET ")
            .Append("Password = ?, Salt = ?")
            .Append(" WHERE UserName = ? ")
        End With
        db.AddParameter("@Password", Password, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@Password", Salt, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@UserName", UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
            Return True
        Catch ex As System.Exception
            'Redirect to error page.
            Return False
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
    End Function
    Public Function GetUserEmpId(ByVal UserSecurityInfo As UserSecurityInfo) As DataTable
        Dim db As New DataAccess
        Dim dt As New DataTable
        Dim da As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder

        With strSQLString
            .Append("SELECT EmpId ")
            .Append(" FROM hrEmployees")
            .Append(" WHERE Username = ?")

        End With
        db.AddParameter("@UserName", UserSecurityInfo.UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(dt)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return dt
    End Function

    Public Function GetExistingUsers(Optional ByVal UserName As String = "", Optional ByVal CampusId As String = "") As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        If Not UserName.ToString.Trim = "" And UserName.ToString.Trim.ToLower = "sa" Then
            With sb
                .Append("SELECT UserId,FullName,UserName,AccountActive  ")
                .Append("FROM syUsers ")
                '' New code Added By Vijay Ramteke on May 20, 2010 For Mantis Id 19025
                .Append(" WHERE UserName NOT IN ('Super','Support') ")
                '' New code Added By Vijay Ramteke on May 20, 2010 For Mantis Id 19025
                .Append(" ORDER BY FullName")
            End With
            ''   ElseIf Not UserName.ToString.Trim = "" And Not CampusId.ToString.Trim = "" Then
        ElseIf Not UserName.ToString.Trim = "" Then
            With sb
                .Append("select * from( " + vbCrLf)
                .Append(" select distinct t1.UserId,t5.FullName,t5.UserName,t5.AccountActive  from " + vbCrLf)
                .Append(" syUsersRolesCampGrps t1,syCmpGrpCmps t2,syCampuses t3,syUsers t5 " + vbCrLf)
                .Append(" where t1.CampGrpId=t2.CampGrpId and t2.CampusId=t3.CampusId and " + vbCrLf)
                .Append(" t1.UserId=t5.UserId ")
                ''DE7898 non Sa user can view users from other campuses too -  July 02 2012
                '  .Append("  and t3.CampusId=? " + vbCrLf)
                '' New code Added By Vijay Ramteke on May 20, 2010 For Mantis Id 19025
                .Append(" and t5.UserName not in ('Super','Support') " + vbCrLf)
                '' New code Added By Vijay Ramteke on May 20, 2010 For Mantis Id 19025
                .Append("union " + vbCrLf)
                .Append("select distinct UserId,FullName,UserName,AccountActive  " + vbCrLf)
                .Append("from syUsers u " + vbCrLf)
                .Append("where not exists(select UserId " + vbCrLf)
                .Append("                 from syUsersRolesCampGrps rc " + vbCrLf)
                .Append("                 where u.UserId=rc.UserId) " + vbCrLf)
                ''DE7898 non Sa user can view users from other campuses too- July 02 2012
                '     .Append("and CampusId=? " + vbCrLf)
                '' New code Added By Vijay Ramteke on May 20, 2010 For Mantis Id 19025
                .Append(" and u.UserName not in ('Super','Support') " + vbCrLf)
                '' New code Added By Vijay Ramteke on May 20, 2010 For Mantis Id 19025
                .Append(") R order by FullName " + vbCrLf)
            End With
            'db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds.Tables(0)
    End Function

    Public Function GetUserInfo(ByVal userId As String) As UserSecurityInfo
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("SELECT FullName,UserName,Email,Password,ConfirmPassword,ModuleId,ModDate,ModUser,AccountActive,IsAdvantageSuperUser,IsDefaultAdminRep ")
            .Append("FROM syUsers ")
            .Append("WHERE UserId = ?")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim userSecurit As New UserSecurityInfo

        While dr.Read()

            'Set properties with data from DataReader
            With userSecurit
                .UserId = userId
                .IsInDB = True
                .FullName = dr("FullName")
                .Email = dr("Email").ToString()
                .UserName = dr("UserName")
                .UserPassword = dr("Password")
                .ConfirmUserPassword = dr("ConfirmPassword")
                If Not (dr("AccountActive") Is System.DBNull.Value) Then .AccountActive = dr("AccountActive")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
                .ModuleId = dr("ModuleId")
                If (Not (dr("IsAdvantageSuperUser") Is System.DBNull.Value) And Not dr("IsAdvantageSuperUser") = 0) Then .IsAdvantageSuperUser = True Else .IsAdvantageSuperUser = False
                If Not (dr("IsDefaultAdminRep") Is System.DBNull.Value) Then .IsDefaultAdminRep = dr("IsDefaultAdminRep")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return UserSecurityInfo
        Return userSecurit

    End Function
    Public Function GetUserInfoUsingUserName(ByVal userName As String) As UserSecurityInfo
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("SELECT UserId,FullName,UserName,Email,Password,ConfirmPassword,CampusId,ModuleId,ModDate,ModUser,AccountActive,IsAdvantageSuperUser,IsDefaultAdminRep ")
            .Append("FROM syUsers ")
            .Append("WHERE UserName = ?")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        db.AddParameter("@username", userName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim userSecurit As New UserSecurityInfo

        While dr.Read()

            'Set properties with data from DataReader
            With userSecurit
                .UserId = CType(dr("UserId"), Guid).ToString()
                .IsInDB = True
                .FullName = dr("FullName")
                .Email = dr("Email").ToString()
                .UserName = dr("UserName")
                .UserPassword = dr("Password")
                .ConfirmUserPassword = dr("ConfirmPassword")
                If Not (dr("AccountActive") Is System.DBNull.Value) Then .AccountActive = dr("AccountActive")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
                .CampusId = CType(dr("CampusId"), Guid).ToString
                .ModuleId = dr("ModuleId")
                If Not (dr("IsAdvantageSuperUser") Is System.DBNull.Value) Then .IsAdvantageSuperUser = True Else .IsAdvantageSuperUser = False
                If Not (dr("IsDefaultAdminRep") Is System.DBNull.Value) Then .IsDefaultAdminRep = dr("IsDefaultAdminRep")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return UserSecurityInfo
        Return userSecurit

    End Function
    Public Function GetAllCampuses() As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("SELECT CampusId,CampDescrip ")
            .Append("FROM syCampuses ")
            .Append("ORDER BY CampDescrip")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function UpdateUserSecurityInfo(ByVal userSecurit As UserSecurityInfo, ByVal user As String, Optional ByVal userName As String = "") As String
        'Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        'do an insert
        Try
            'build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syUsers ")
                .Append("SET FullName = ?,Email = ?,UserName = ?,Password = ?,ConfirmPassword=?,AccountActive = ?, ")
                '  .Append("CampusId = ?,ModuleId = ?,IsDefaultAdminRep = ?,ModUser = ?, ModDate = ? ")
                .Append("ModuleId = ?,IsDefaultAdminRep = ?,ModUser = ?, ModDate = ? ")
                .Append("WHERE UserId = ? ")
                .Append("AND CONVERT(VARCHAR,ModDate,120 ) = CONVERT(VARCHAR,CAST(? AS DATEtime),120 ) ;")
                .Append("Select count(*) from syUsers where ModDate = ?; ")
                If userName <> "" And userName <> userSecurit.FullName Then
                    .Append(" Update rptInstructor set InstructorDescrip= ? where InstructorID = ? ;")
                End If
            End With

            'Add parameters values to the query

            'FullName
            db.AddParameter("@fullname", userSecurit.FullName, DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)

            'Email
            db.AddParameter("@email", userSecurit.Email, DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)

            'UserName
            db.AddParameter("@username", userSecurit.UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Password
            db.AddParameter("@password", "No Password", DataAccess.OleDbDataType.OleDbString, 40, ParameterDirection.Input)

            'ConfirmPassword
            db.AddParameter("@confirmpassword", "No Password", DataAccess.OleDbDataType.OleDbString, 40, ParameterDirection.Input)

            'AccountActive
            db.AddParameter("@accountactive", userSecurit.AccountActive, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'CampusId
            '     db.AddParameter("@campusid", userSecurit.CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModuleId
            db.AddParameter("@modid", userSecurit.ModuleId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

            'Default Admin Rep
            db.AddParameter("@defar", userSecurit.IsDefaultAdminRep, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Dim now As Date = Utilities.GetAdvantageDBDateTime(Date.Now)
            Dim now As Date = Date.Now

            'Updated ModDate
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'UserId
            db.AddParameter("@userid", userSecurit.UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Original ModDate
            db.AddParameter("@Original_ModDate", userSecurit.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'Updated ModDate
            db.AddParameter("@ModDate1", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            If userName <> "" And userName <> userSecurit.FullName Then
                db.AddParameter("@fullname", userSecurit.FullName, DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)
                db.AddParameter("@userid", userSecurit.UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            'If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function AddUserSecurityInfo(ByVal userSecurit As UserSecurityInfo, ByVal user As String) As String
        'Connect to the database
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        'do an insert
        Try
            'build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT INTO syUsers (UserId,FullName,Email,UserName,Password,ConfirmPassword,AccountActive, ")
                .Append("ModuleId,IsDefaultAdminRep,ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            'Add parameters values to the query

            'UserId
            db.AddParameter("@userid", userSecurit.UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'FullName
            db.AddParameter("@fullname", userSecurit.FullName, DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)

            'Email
            db.AddParameter("@email", userSecurit.Email, DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)

            'UserName
            db.AddParameter("@username", userSecurit.UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Password
            db.AddParameter("@password", "No Password", DataAccess.OleDbDataType.OleDbString, 40, ParameterDirection.Input)

            'ConfirmPassword
            db.AddParameter("@confirmpassword", "No Password", DataAccess.OleDbDataType.OleDbString, 40, ParameterDirection.Input)

            'AccountActive
            db.AddParameter("@accountactive", userSecurit.AccountActive, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)


            'ModuleId
            db.AddParameter("@modid", userSecurit.ModuleId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

            'Default Admin Rep
            db.AddParameter("@DefAR", userSecurit.IsDefaultAdminRep, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'Execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)


            'Return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function DeleteUserSecurityInfo(ByVal UserId As String, ByVal modDate As DateTime) As String

        'Connect to the database
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        'Do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syUsers ")
                .Append("WHERE UserId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count(*) from syUsers where UserId = ? ")
            End With

            'Add parameters values to the query

            'UserId
            db.AddParameter("@userid", UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'UserId
            db.AddParameter("@userid", UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                'Return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            'Return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function DeleteAdvantageUser(ByVal userId As Guid) As String
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        Dim msg As String
        Dim intReturn As Integer = 0
        Dim intReturn2 As Integer = 0
        Dim inactivateMsg As String = "</br>To make Inactive,</br>uncheck the Active Account checkbox."

        Try
            Using cn As New SqlConnection(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
                Using cmd As New SqlCommand("dbo.DeleteAdvantageUser")

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@UserId", SqlDbType.UniqueIdentifier))
                    cmd.Parameters("@UserId").Value = userId

                    Dim returnparameter As SqlParameter = New SqlParameter("@ResultId", 0)
                    returnparameter.Direction = ParameterDirection.Output
                    cmd.Parameters.Add(returnparameter)

                    Dim returnparameter2 As SqlParameter = New SqlParameter("@ErrorId", 0)
                    returnparameter2.Direction = ParameterDirection.Output
                    cmd.Parameters.Add(returnparameter2)

                    Dim objReturn As Object



                    cn.Open()

                    cmd.Connection = cn
                    cmd.ExecuteNonQuery()

                    Int32.TryParse(cmd.Parameters("@ResultId").Value.ToString(), intReturn)

                    Int32.TryParse(cmd.Parameters("@ErrorId").Value.ToString(), intReturn2)



                    If intReturn2 <> 0 Then
                        msg = "(Error code: " & intReturn2.ToString() & ") Error occured deleting user."
                    Else
                        If intReturn <> 1 Then
                            Select Case intReturn
                                Case 2
                                    msg = "(Error code: " & intReturn.ToString() & ") Could not delete user from Advantage!"
                                Case 3
                                    msg = "(Error code: " & intReturn.ToString() & ") Could not delete Tenant User!"
                                Case 4
                                    msg = "(Error code: " & intReturn.ToString() & ") Could not delete user tenant profile!"
                                Case 5
                                    msg = "(Error code: " & intReturn.ToString() & ") Could not delete user tenant roles!"
                                Case 6
                                    msg = "(Error code: " & intReturn.ToString() & ") Could not delete user tenant personalization!"
                                Case 7
                                    msg = "(Error code: " & intReturn.ToString() & ") Could not delete user tenant membership!"
                                Case 8
                                    msg = "(Error code: " & intReturn.ToString() & ") Could not delete aspnet_user!"
                                Case Else
                                    msg = "(Error code: " & intReturn.ToString() & ") An error occurred attempting to delete user!"
                            End Select
                        Else
                            msg = ""
                        End If
                    End If
               
                    If Not String.IsNullOrEmpty(msg) Then
                        msg += inactivateMsg
                    End If
                End Using
            End Using

            Return msg

        Catch ex As Exception
            msg = "An error occurred attempting to delete user! - " & ex.Message & inactivateMsg
            Return msg
        End Try

    End Function

    Public Function GetUserAssignedRolesCampusGroups(ByVal userId As String, ByVal roleId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("SELECT t1.UserRoleCampGrpId,t1.RoleId,t1.CampGrpId,t2.CampGrpDescrip As Description ")
            .Append("FROM syUsersRolesCampGrps t1, syCampGrps t2, syRoles t3 ")
            .Append("WHERE t1.CampGrpId = t2.CampGrpId ")
            .Append("AND t1.RoleId = t3.RoleId ")
            .Append("AND t1.UserId = ? ")
            .Append("AND t1.RoleId = ? ")
            .Append("ORDER BY Description")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@roleid", roleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function GetUserAvailableRolesCampusGroups(ByVal userId As String, ByVal roleId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("SELECT t1.CampGrpId,t1.CampGrpDescrip ")
            .Append("FROM syCampGrps t1 ")
            .Append("WHERE NOT EXISTS ")
            .Append("(SELECT t2.CampGrpId ")
            .Append("FROM syUsersRolesCampGrps t2 ")
            .Append("WHERE t2.CampGrpId = t1.CampGrpId ")
            .Append("AND t2.UserId = ? ")
            .Append("AND t2.RoleId = ? )")
            .Append("ORDER BY t1.CampGrpDescrip")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@roleid", roleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Sub DeleteUserRoleCampusGroups(ByVal userId As String, ByVal roleid As String)
        Dim sb As New StringBuilder
        Dim db As New DataAccess

        With sb
            .Append("DELETE FROM syUsersRolesCampGrps ")
            .Append("WHERE UserId = ? ")
            .Append("AND RoleId = ?;")
            .Append("Delete FROM    syUsersRolesCampGrps ")
            .Append(" WHERE   UserId = ? ")
            .Append(" AND RoleId = ( SELECT   roleid ")
            .Append(" FROM SyRoles WHERE SysRoleId = 15);")

        End With

        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@roleid", roleid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
    End Sub

    Public Sub AddUserRoleCampusGroup(ByVal userId As String, ByVal roleId As String, ByVal campGrpId As String, ByVal user As String)
        Dim sb As New StringBuilder
        Dim db As New DataAccess

        With sb
            .Append("INSERT INTO syUsersRolesCampGrps(UserId,RoleId,CampGrpId,ModUser,ModDate) ")
            .Append("VALUES(?,?,?,?,?)")
        End With

        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@roleid", roleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campgrpid", campGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'ModDate
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
    End Sub

    Public Function UserHasAccessToDefaultCampus(ByVal userId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim numRows As Integer
        Dim campusId As String

        campusId = GetUserDefaultCampus(userId)

        With sb
            .Append("SELECT COUNT(*) ")
            .Append("FROM syUsersRolesCampGrps t1 ")
            .Append("WHERE UserId = ? ")
            .Append("AND EXISTS (SELECT t2.CampGrpId ")
            .Append("FROM syCmpGrpCmps t2 ")
            .Append("WHERE t1.CampGrpId=t2.CampGrpId ")
            .Append("AND t2.CampusId = ?) ")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campusid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        numRows = db.RunParamSQLScalar(sb.ToString)

        If numRows = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    Public Function GetUserDefaultCampus(ByVal userId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        ''   Dim numRows As Integer

        With sb
            .Append("SELECT CampusId ")
            .Append("FROM syUsers ")
            .Append("WHERE UserId = ?")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return XmlConvert.ToGuid(db.RunParamSQLScalar(sb.ToString).ToString()).ToString

    End Function

    Public Function GetUserDefaultModule(ByVal userId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        '' Dim numRows As Integer

        With sb
            .Append("SELECT ModuleId ")
            .Append("FROM syUsers ")
            .Append("WHERE UserId = ?")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return (db.RunParamSQLScalar(sb.ToString)).ToString()
    End Function
    Public Sub UpdateIsLoggedIn(ByVal UserId As String, ByVal CampusId As String)
        Dim db As New DataAccess
        Dim sb, sb1 As New StringBuilder
        Dim numRows As Integer
        ''Commented this "t2.SysRoleId = 3 and" from the query
        ''modified by Saraswathi lakshmanan
        With sb
            .Append("   select Count(*) from syUsers t1,syRoles t2,syUsersRolesCampGrps t3 ")
            .Append("   where  t2.RoleId = t3.RoleId and t1.UserId=t3.UserId and t1.UserId='" & UserId & "' ")
            .Append("   AND CampGrpId in (select CampGrpId from syCmpGrpCmps where CampusId='" & CampusId & "') ")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        numRows = CInt(db.RunParamSQLScalar(sb.ToString))
        db.ClearParameters()

        If numRows >= 1 Then
            With sb1
                .Append("Update syUsers set IsLoggedIn=1 where UserId='" & UserId & "' ")
            End With
            db.RunParamSQLExecuteNoneQuery(sb1.ToString)
        End If
    End Sub
    Public Sub UpdateWhenUserLogsOut(ByVal UserId As String, ByVal CampusId As String)
        Dim db As New DataAccess
        Dim sb, sb1 As New StringBuilder
        Dim numRows As Integer
        ''Commented by Saraswathi "t2.SysRoleId = 3 and" from the query
        With sb
            .Append("   select Count(*) from syUsers t1,syRoles t2,syUsersRolesCampGrps t3 ")
            .Append("   where  t2.RoleId = t3.RoleId and t1.UserId=t3.UserId and t1.UserId='" & UserId & "' ")
            .Append("   AND CampGrpId in (select CampGrpId from syCmpGrpCmps where CampusId='" & CampusId & "') ")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        numRows = CInt(db.RunParamSQLScalar(sb.ToString))
        db.ClearParameters()

        If numRows >= 1 Then
            With sb1
                .Append("Update syUsers set IsLoggedIn=0 where UserId='" & UserId & "' ")
            End With
            db.RunParamSQLExecuteNoneQuery(sb1.ToString)
        End If
    End Sub
    Public Sub UpdateGradeRounding(ByVal GradeRounding As String)
        Dim db As New DataAccess
        Dim sb, sb1 As New StringBuilder
        ''  Dim numRows As Integer
        ''Commented by Saraswathi "t2.SysRoleId = 3 and" from the query

        With sb1
            .Append("Update syGradeRounding set graderoundingvalue='" & GradeRounding & "'")
        End With
        db.RunParamSQLExecuteNoneQuery(sb1.ToString)

    End Sub
    Public Function GetCampusGroupsWithCampus(ByVal campusId As String) As DataTable
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        '' Dim numRows As Integer

        With sb
            .Append("SELECT DISTINCT t1.CampGrpId,t2.CampGrpDescrip ")
            .Append("FROM syCmpGrpCmps t1, syCampGrps t2 ")
            .Append("WHERE t1.CampGrpId = t2.CampGrpId ")
            .Append("AND t1.CampusId = ?")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@campusid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function GetUserId(ByVal userName As String, ByVal passWord As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim userId As String

        With sb
            .Append("SELECT UserId ")
            .Append("FROM syUsers ")
            .Append("WHERE UserName = ? ")
            .Append("AND Password = ? ")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@username", userName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ''Code Commented and Added by Kamalesh Ahuja on April 20 2010 to resolve Mantis Issue Id 18047
        ''db.AddParameter("@password", passWord, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        If userName.ToUpper = "SUPER" Or userName.ToUpper = "SUPPORT" Then
            'Password
            db.AddParameter("@password", Encrypt(passWord), DataAccess.OleDbDataType.OleDbString, 40, ParameterDirection.Input)
        Else
            'Password
            db.AddParameter("@password", passWord, DataAccess.OleDbDataType.OleDbString, 40, ParameterDirection.Input)
        End If
        '''''''''''''''''''''''''
        userId = XmlConvert.ToGuid(db.RunParamSQLScalar(sb.ToString).ToString()).ToString

        Return userId
    End Function
    ''Code Added by Kamalesh Ahuja on April 20 2010 to resolve Mantis Issue Id 18047
    Private Function Encrypt(ByVal myString As String) As String
        Dim myKey As String = "Advantage"
        Dim cryptDES3 As New TripleDESCryptoServiceProvider()
        Dim cryptMD5Hash As New MD5CryptoServiceProvider()

        cryptDES3.Key = cryptMD5Hash.ComputeHash(ASCIIEncoding.ASCII.GetBytes(myKey))
        cryptDES3.Mode = CipherMode.ECB
        Dim desdencrypt As ICryptoTransform = cryptDES3.CreateEncryptor()
        Dim MyASCIIEncoding = New ASCIIEncoding()
        Dim buff() As Byte = ASCIIEncoding.ASCII.GetBytes(myString)
        Encrypt = Convert.ToBase64String(desdencrypt.TransformFinalBlock(buff, 0, buff.Length))
    End Function
    ''

    Public Function GetFullNameByUserId(ByVal userId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim username As String

        With sb
            .Append("SELECT FullName ")
            .Append("FROM syUsers ")
            .Append("WHERE UserId = ? ")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        username = db.RunParamSQLScalar(sb.ToString)

        Return username
    End Function

    Public Function GetUserRolesForCurrentCampus(ByVal userId As String, ByVal campusId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("SELECT t1.RoleId ")
            .Append("FROM syUsersRolesCampGrps t1 ")
            .Append("WHERE t1.UserId = ? ")
            .Append("AND t1.CampGrpId IN ")
            .Append("(SELECT t2.CampGrpId ")
            .Append("FROM syCmpGrpCmps t2 ")
            .Append("WHERE t2.CampusId = ?)")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campusid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function

    Public Function GetUserRoles(ByVal userId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("SELECT t1.RoleId ")
            .Append("FROM syUsersRolesCampGrps t1 ")
            .Append("WHERE t1.UserId = ? ")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    'Public Function GetResourceRoles(ByVal resourceId As Integer) As DataTable
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim ds As New DataSet

    '    With sb
    '        .Append("SELECT Distinct t1.RoleId, t1.AccessLevel,t2.ResourceURL ")
    '        .Append("FROM syRlsResLvls t1, syResources t2 ")
    '        .Append("WHERE t1.ResourceID = t2.ResourceID ")
    '        .Append("AND t1.ResourceId  = ?")
    '    End With

    '    Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    'db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

    '    db.AddParameter("@resid", resourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

    '    ds = db.RunParamSQLDataSet(sb.ToString)

    '    Return ds.Tables(0)
    'End Function

    Public Function GetResourceRoles(ByVal resourceId As Integer) As DataTable
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@resid", resourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet("dbo.usp_GetResourceRoles", Nothing, "SP")

        Return ds.Tables(0)
    End Function

    Public Function GetUserCampuses(ByVal userId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("SELECT DISTINCT t1.CampusId,t2.CampDescrip ")
            .Append("FROM syCmpGrpCmps t1, syCampuses t2 ")
            .Append("WHERE t1.CampusID = t2.CampusId ")
            .Append("AND t1.CampGrpId IN ")
            .Append("(SELECT t3.CampGrpId ")
            .Append("FROM syUsersRolesCampGrps t3 ")
            .Append("WHERE t3.UserId = ? )")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function ShowDefaultCampusScreenAtLogin(ByVal userId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        ''  Dim blnShow As Boolean

        With sb
            .Append("SELECT ShowDefaultCampus ")
            .Append("FROM syUsers ")
            .Append("WHERE UserId = ? ")
            .Append("and exists (select * from syUsersRolesCampGrps where CampGrpId in (select CampGrpId from syCmpGrpCmps where CampusId=(Select CampusId from syUsers where UserId=?)))")

        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)

        If Not obj Is System.DBNull.Value Then
            Return CType(obj, Boolean)
        Else
            Return False
        End If

        If Not dr.IsClosed Then dr.Close()

    End Function

    Public Sub UpdateUserDefaultCampus(ByVal userId As String, ByVal campusId As String, ByVal showLogin As Boolean)
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        ''   Dim blnShow As Boolean

        With sb
            .Append("UPDATE syUsers ")
            .Append("SET CampusId = ?, ")
            .Append("ShowDefaultCampus = ? ")
            .Append("WHERE UserId = ?")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        db.AddParameter("@campusid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@showlogin", showLogin, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)

    End Sub

    Public Function GetCampusName(ByVal campusId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        ''  Dim userId As String

        With sb
            .Append("SELECT CampDescrip ")
            .Append("FROM syCampuses ")
            .Append("WHERE CampusId = ? ")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@campusid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Return db.RunParamSQLScalar(sb.ToString).ToString()

    End Function

    Public Function GetAssignedCampuses(ByVal UserID As String) As DataTable
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT DISTINCT C.CampusID, C.CampDescrip ")
            .Append("FROM syUsersRolesCampGrps A, syCmpGrpCmps B, syCampuses C ")
            .Append("WHERE A.CampGrpID = B.CampGrpID AND B.CampusID = C.CampusID ")
            .Append("AND A.UserID = ? ")

            ' Following line ensures that only assigned campuses that are the only 
            '	member of their group are returned. Comment out this line to get all 
            '	assigned campuses.
            .Append("AND ((SELECT COUNT(*) FROM dbo.syCmpGrpCmps B1 WHERE B1.CampGrpID = A.CampGrpID) = 1) ")

            .Append("ORDER BY C.CampDescrip")
        End With

        With New DataAccess
            .AddParameter("@userid", UserID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Return .RunParamSQLDataSet(sb.ToString).Tables(0)
        End With

    End Function

    Public Function GetResourceURL(ByVal resourceID As Integer) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        ''   Dim userId As String

        With sb
            .Append("SELECT ResourceURL ")
            .Append("FROM syResources ")
            .Append("WHERE ResourceID = ? ")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@resid", resourceID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        Return db.RunParamSQLScalar(sb.ToString).ToString()

    End Function
    Public Function GetResourcesForSubModule(ByVal subModId As Integer, ByVal resource As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim intModuleResource As Integer

        Select Case resource.ToString.ToLower.Trim
            Case Is = "admissions"
                intModuleResource = 189
            Case Is = "academic records"
                intModuleResource = 26
            Case Is = "placement"
                intModuleResource = 193
            Case Is = "financial aid"
                intModuleResource = 191
            Case Is = "human resources"
                intModuleResource = 192
            Case Is = "student accounts"
                intModuleResource = 194
            Case Is = "system"
                intModuleResource = 195
            Case Is = "faculty"
                intModuleResource = 300
        End Select

        With sb
            '.Append("Select ResourceId, HierarchyIndex from syNavigationNodes where ParentId=( ")
            '.Append("Select ")
            '.Append("       HierarchyId ")
            '.Append("FROM   syNavigationNodes ")
            '.Append("WHERE  HierarchyId in ")
            '.Append("           (select HierarchyId from syNavigationNodes where ResourceId= ?) ")
            '.Append("AND    ParentId=(Select HierarchyId from syNavigationNodes where ResourceId=")
            '.Append("       (select ResourceId from syResources where Resource= ? ))) ")
            '.Append("ORDER BY HierarchyIndex ")

            .Append("Select ResourceId, HierarchyIndex " & vbCrLf)
            .Append("from syNavigationNodes " & vbCrLf)
            .Append("WHERE " & vbCrLf)
            .Append("	ParentId IN (SELECT HierarchyId FROM syNavigationNodes WHERE ResourceId=737 " & vbCrLf)
            .Append("			AND ParentId IN " & vbCrLf)
            .Append("					 (Select HierarchyId FROM   syNavigationNodes " & vbCrLf)
            .Append("					 WHERE  ResourceId=395)" & vbCrLf)
            .Append("			)" & vbCrLf)

            'Modified by Balaji on July 19th 2011
            'Reason: To suit the new menu layout
            '.Append(" SELECT t1.ResourceId,t2.HierarchyIndex FROM ")
            '.Append(" syResources t1 INNER JOIN syNavigationNodes t2 ON t1.ResourceId=t2.ResourceId ")
            '.Append(" INNER JOIN syNavigationNodes t3 ON t2.ParentId = t3.HierarchyId ")
            '.Append(" INNER JOIN syNavigationNodes t4 ON t3.ParentId = t4.HierarchyId ")
            '.Append(" WHERE t4.ResourceId=? AND t4.ParentId IN (SELECT HierarchyId FROM syNavigationNodes WHERE ResourceId=?) ")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        'db.AddParameter("@submodid", subModId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        'db.AddParameter("@Resource", resource, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'Modified by Balaji on July 19th 2011
        'db.AddParameter("@ModuleResourceId", intModuleResource, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        Try
            Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        End Try
    End Function
    Public Function IsSAorAdvantageSuperUser(ByVal userId As String) As Boolean

        '   connect to the database
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       Count(*) ")
            .Append("FROM ")
            .Append("       syUsers U, ")
            .Append("       syUsersRolesCampGrps URC, ")
            .Append("       syRoles R ")
            .Append("WHERE ")
            .Append("       (U.UserId=URC.UserId ")
            .Append("AND    URC.RoleId=R.RoleId ")
            .Append("AND    (R.SysRoleId=1) ")
            .Append("AND 	U.UserId = ? ) ")
            .Append("OR	    (U.UserId = ? AND IsAdvantageSuperUser=1) ")
        End With

        ' Add the UserId to the parameter list
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return boolean
        Return CType(db.RunParamSQLScalar(sb.ToString), Boolean)

    End Function
    Public Function IsSAOrDirectorOfAdmissions(ByVal userId As String) As Boolean

        '   connect to the database
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       Count(*) ")
            .Append("FROM ")
            .Append("       syUsers U, ")
            .Append("       syUsersRolesCampGrps URC, ")
            .Append("       syRoles R ")
            .Append("WHERE ")
            .Append("       (U.UserId=URC.UserId ")
            .Append("AND    URC.RoleId=R.RoleId ")
            .Append("AND    (R.SysRoleId=8 OR R.SysRoleId=1) ")
            .Append("AND 	U.UserId = ? ) ")
            .Append("OR	    (U.UserId = ? AND IsAdvantageSuperUser=1) ")
        End With

        ' Add the UserId to the parameter list
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return boolean
        Return CType(db.RunParamSQLScalar(sb.ToString), Boolean)

    End Function
    Public Function DoesDefaultAdminRepExistForCampus(ByVal ModuleId As Integer, ByVal Campus As String) As String
        '   connect to the database
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       Count(*) ")
            .Append("FROM ")
            .Append("       syUsers U ")
            .Append("WHERE ")
            .Append("       U.ModuleId=? ")
            .Append("AND U.CampusId=? ")
            .Append("AND IsDefaultAdminRep=1")
        End With

        ' Add the UserId to the parameter list
        db.AddParameter("@mod", ModuleId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@camp", Campus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return boolean
        Return CType(db.RunParamSQLScalar(sb.ToString), Boolean)
    End Function
    Public Function GetCampusGroupsByUser(ByVal UserId As String) As DataTable
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim sb As New StringBuilder

        With sb
            .Append("   select Distinct t1.CampGrpId,t1.CampGrpDescrip,t3.StatusId as StatusId,t3.Status as Status from syCampGrps t1,syUsersRolesCampGrps t2,syStatuses t3 where t1.CampGrpId = t2.CampGrpId and t1.StatusId=t3.StatusId and t2.UserId=? order by CampGrpDescrip ")
        End With
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.OpenConnection()
        db.AddParameter("@userid", UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString, "RoleList")
        db.CloseConnection()
        Return ds.Tables(0)
    End Function
    Public Function GetCampusAndRolesByCampusGroups(ByVal UserId As String, ByVal CampGrpId As String) As DataSet
        Dim db As New DataAccess
        Dim sb, sb1 As New StringBuilder
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.OpenConnection()

        With sb
            .Append(" select Distinct CampDescrip from syCampuses t1,syCmpGrpCmps t2 where t1.CampusId = t2.CampusId and t2.CampGrpId='" & CampGrpId & "' Order By CampDescrip ")
        End With
        da = New OleDbDataAdapter(sb.ToString, MyAdvAppSettings.AppSettings("ConString").ToString)
        da.Fill(ds, "Campus")
        da.Dispose()
        db.ClearParameters()

        db.CloseConnection()
        Return ds
    End Function
    Public Function GetRolesByCampusGroups(ByVal UserId As String, ByVal CampGrpId As String) As DataSet
        Dim db As New DataAccess
        Dim sb, sb1 As New StringBuilder
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.OpenConnection()

        With sb1
            .Append(" select Distinct t3.Role as Role from syCampGrps t1,syUsersRolesCampGrps t2,syRoles t3 where t1.CampGrpId = t2.CampGrpId and t2.RoleId=t3.RoleId and t2.UserId='" & UserId & "' and t2.CampGrpId='" & CampGrpId & "' Order by Role ")
        End With
        da = New OleDbDataAdapter(sb1.ToString, MyAdvAppSettings.AppSettings("ConString").ToString)
        da.Fill(ds, "Roles")
        da.Dispose()
        db.ClearParameters()
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetUserRolesForAllCampGrps(ByVal UserId As String) As DataTable
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT t1.RoleId, t3.Role, t1.CampGrpId, t2.CampGrpDescrip ")
            .Append(" FROM syUsersRolesCampGrps t1, syCampGrps t2, syRoles t3 ")
            .Append(" WHERE t1.UserId = ? and t1.CampGrpId = t2.CampGrpId and t1.RoleId = t3.RoleId ")
            .Append("ORDER BY t3.Role ")
        End With

        db.OpenConnection()
        db.AddParameter("@userid", UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString, "RoleList")

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)

    End Function
    Public Function GetInstructors(ByVal campusId As String) As DataTable
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet


        With sb
            .Append("SELECT distinct a.UserId,a.FullName ")
            .Append("FROM syUsers a, syUsersRolesCampGrps b, syRoles c ")
            .Append("WHERE a.UserId = b.UserId ")
            .Append("AND b.RoleId = c.RoleId ")
            .Append("AND c.SysRoleId = 2 ")
            .Append("AND CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            .Append("ORDER BY a.FullName")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@cmpid2", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet(sb.ToString)
            Return ds.Tables(0)
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw ex
            Else
                Throw ex.InnerException
            End If
        End Try


    End Function
    ''Added by Saraswathi Lakshmanan to fix issue 14385
    ''When entering a new lead information SA, Front Desk and 
    ''Director of Admissions should be able to assign them to admission rep 
    Public Function IsSAOrDirectorOfAdmissionsorFrontDesk(ByVal userId As String) As Boolean

        '   connect to the database
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       Count(*) ")
            .Append("FROM ")
            .Append("       syUsers U, ")
            .Append("       syUsersRolesCampGrps URC, ")
            .Append("       syRoles R ")
            .Append("WHERE ")
            .Append("       (U.UserId=URC.UserId ")
            .Append("AND    URC.RoleId=R.RoleId ")
            '.Append("AND    (R.SysRoleId=8 OR R.SysRoleId=1) ")
            '' SysRoleId- 8-DirectorOfAdmissions, 12- FrontDesk, 1-SystemAdmin
            .Append("AND    (R.SysRoleId=8 OR R.SysRoleId=1 OR R.SysRoleId=12) ")

            .Append("AND 	U.UserId = ? ) ")
            .Append("OR	    (U.UserId = ? AND IsAdvantageSuperUser=1) ")
        End With

        ' Add the UserId to the parameter list
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return boolean
        Return CType(db.RunParamSQLScalar(sb.ToString), Boolean)

    End Function

    ''Added by saraswatgi lakshmanan on march 11 2009
    ''To get the username and password based on the userid 
    ''this is used in the intermediate page to go to leads page.
    Public Function GetUserNameandPassword(ByVal userId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("SELECT UserName , Password ")
            .Append("FROM syUsers ")
            .Append("WHERE UserId = ?")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function
    ''Added on may 13 2009
    ''Added by saraswathi Lakshmanan to find the password for the selected Campus
    Public Function GetPasswordforCampus(ByVal CampusId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("SELECT RemoteServerPwd ")
            .Append("FROM SyCampuses ")
            .Append("WHERE CampusID = ?")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@CampusID", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)(0).ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If

    End Function
    ''Added by Kamalesh Ahuja on May 27 2010 to resolve mantis issue id 18652
    Public Function GetPasswordforCampusFL(ByVal CampusId As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        With sb
            .Append("SELECT RemoteServerPwdFL ")
            .Append("FROM SyCampuses ")
            .Append("WHERE CampusID = ?")
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@CampusID", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)(0).ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If

    End Function

    Public Function GetAllInstructors_SP(ByVal showActiveOnly As Boolean, ByVal campusId As String) As DataTable

        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@showActiveOnly", showActiveOnly, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet("dbo.usp_GetAllInstructors", Nothing, "SP").Tables(0)

    End Function

    Public Function GetInstructor_SP() As DataTable
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString


        Return db.RunParamSQLDataSet("dbo.usp_GetInstructor", Nothing, "SP").Tables(0)
    End Function

    Public Function GetInstructorActive_SP() As DataTable
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString


        Return db.RunParamSQLDataSet("dbo.usp_GetInstructorActive", Nothing, "SP").Tables(0)
    End Function

    'US3434 - SSN field on the Student Master page to visible to only certain security roles 
    Public Function GetRolePermissionforSSN(ByVal UserId As String, ByVal campusId As String) As Boolean

        Dim db As New DataAccess
        Dim dt As New DataTable
        db.ClearParameters()

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        dt = db.RunParamSQLDataSet("dbo.GetUserPermissionforSSN", Nothing, "SP").Tables(0)
        Return CBool(dt.Rows(0)(0).ToString)
    End Function

    Public Function UpdateTenantUsers(ByVal userid As String, ByVal schoolname As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Try

            With sb
                .Append("INSERT dbo.TenantUsers  ")
                .Append("( TenantId, UserId ) ")
                .Append("VALUES ")
                .Append("((SELECT TenantId FROM dbo.Tenant WHERE TenantName = '" & schoolname & "'),'" & userid & "')")
            End With

            db.ConnectionString = ConfigurationManager.ConnectionStrings("TenantAuthDBConnection").ToString
            db.RunParamSQLExecuteNoneQuery(sb.ToString(), Nothing, Nothing, "Tenant")

        Catch ex As Exception
            Throw New Exception("Failure updating the TenantUsers table. " + ex.Message)
        Finally
            db.CloseConnection()
        End Try

    End Function

    Public Function DeleteTenantUsers(ByVal userid As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Try

            With sb
                .Append("DELETE FROM dbo.TenantUsers ")
                .Append("WHERE UserID = ?")
            End With

            db.ConnectionString = ConfigurationManager.ConnectionStrings("TenantAuthDBConnection").ToString
            db.AddParameter("@userid", userid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString(), Nothing, Nothing, "Tenant")

        Catch ex As Exception
            Throw New Exception("Failure deleting from the TenantUsers table. " + ex.Message)
        Finally
            db.CloseConnection()
        End Try

    End Function

    Public Function ResetUsers_UserName(ByVal userid As String, ByVal username As String)

        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Try

            With sb
                .Append("UPDATE dbo.aspnet_Users ")
                .Append("SET UserName = ? , LoweredUserName = ? ")
                .Append("WHERE UserID = ? ")
            End With

            db.ConnectionString = ConfigurationManager.ConnectionStrings("TenantAuthDBConnection").ToString

            db.AddParameter("@username", username, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@username", username.ToLower, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@userid", userid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString(), Nothing, Nothing, "Tenant")

        Catch ex As Exception
            Throw New Exception("Failure updating the aspnet_Users table " + ex.Message)
        Finally
            db.CloseConnection()
        End Try

    End Function

    Public Function UpdateTenantTables(ByVal userid As String, ByVal schoolname As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Try

            With sb
                .Append("INSERT dbo.TenantUsers  ")
                .Append("( TenantId, UserId ) ")
                .Append("VALUES ")
                .Append("((SELECT TenantId FROM dbo.Tenant WHERE TenantName = '" & schoolname & "'),'" & userid & "')")
            End With

            db.ConnectionString = ConfigurationManager.ConnectionStrings("TenantAuthDBConnection").ToString
            db.RunParamSQLExecuteNoneQuery(sb.ToString(), Nothing, Nothing, "Tenant")

        Catch ex As Exception
            Throw New Exception("Failure updating the TenantUsers table. " + ex.Message)
        Finally
            db.CloseConnection()
        End Try

    End Function

    Public Function GetDashboardRoleID() As DataSet

        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try

            With sb
                .Append("SELECT ")
                .Append("(SELECT RoleID FROM syRoles WHERE Role = 'Dashboard User') as RoleID, ")
                .Append("(SELECT CampGrpID from syCampGrps where CampGrpCode = 'All') as CampGrpId")
            End With

            ds = db.RunParamSQLDataSet(sb.ToString)


        Catch ex As Exception
            Throw New Exception("Failure getting the roleid for the dashboard user. " + ex.Message)
        Finally
            db.CloseConnection()

        End Try
        Return ds
    End Function

    Public Sub InsertCampusIdforNewUser(ByVal UserId As String, ByVal CampusId As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try


            With sb
                .Append("UPDATE syUsers ")
                .Append("SET CampusID ='" & CampusId & "' ")
                .Append("WHERE UserId='" & UserId & "' ")
            End With

            db.RunParamSQLExecuteNoneQuery(sb.ToString)

        Catch ex As Exception
            Throw New Exception("Failure inserting the campus id for the dashboard user. " + ex.Message)
        Finally
            db.CloseConnection()

        End Try
    End Sub

    Public Function DisplayNameExists(ByVal userdisplayname As String, Optional ByRef userid As String = "") As Boolean

        Dim db As New DataAccess
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT *  ")
            .Append("FROM syUsers ")
            .Append("WHERE UserName = ? ")
            If Not userid = String.Empty Then
                .Append(" AND UserID <> ? ")
            End If
        End With

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        db.AddParameter("@userdisplayname", userdisplayname, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        If Not userid = String.Empty Then
            db.AddParameter("@userid", userid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        Try
            Dim reader As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            If reader.HasRows Then
                Return True
            Else
                Return False
            End If
            If Not reader.IsClosed Then reader.Close()
        Catch ex As Exception
            Throw New Exception("Exception verifying password. " + ex.Message)
        Finally
            db.CloseConnection()
        End Try
        Return False
    End Function

    Public Function DoesUserBelongToRoleForCampus(ByVal userId As String, ByVal sysRoleId As Integer, ByVal campusId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim count As Integer
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString

        With sb
            .Append("SELECT COUNT(*)  ")
            .Append("FROM syUsersRolesCampGrps urgc, dbo.syCampGrps cg, dbo.syCmpGrpCmps cgc, syroles r ")
            .Append("WHERE urgc.CampGrpId=cg.CampGrpId ")
            .Append("AND cg.CampGrpId=cgc.CampGrpId ")
            .Append("AND urgc.RoleId=r.RoleId ")
            .Append("AND urgc.userid = ? ")
            .Append("AND r.SysRoleId = ? ")
            .Append("AND cgc.CampusId = ? ")
        End With

        db.AddParameter("@userId", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@roleId", sysRoleId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        count = CInt(db.RunParamSQLScalar(sb.ToString))

        If count > 0 Then
            Return True
        Else
            Return False
        End If

    End Function
    Public Sub InsertMRUsForFirstTimeUser(ByVal userId As String) 

        Dim db As New SQLDataAccess
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Purpose of the SP: To get the Grade System Component Types
        'Create Procedure SP_GetGradeSystemComponentTypes
        'as
        'select Distinct ResourceId,Resource from syResources where resourcetypeId=10
        'order by Resource
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Dim ds As New DataSet
        Try
            db.OpenConnection()
            db.AddParameter("@UserId", userId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_InsertMRUsForFirstTimeUsers")
        Catch ex As Exception
            Throw New Exception("Failure inserting the mrus for first time user. " + ex.Message)
        Finally
            db.CloseConnection()
        End Try
    End Sub
End Class
