Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common



Public Class ProgDB

    Public Function GetCoursesNotAssigned(ByVal TblSelect As String, ByVal ID As String, ByVal CampusId As String) As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim da As New OleDbDataAdapter
        Dim sb As New System.Text.StringBuilder
        Dim reccount As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        If TblSelect = "PROGVER" Then
            '   build the sql query
            With sb
                .Append("Select count(*) from arProgVerDef")
            End With

            db.OpenConnection()
            reccount = db.RunParamSQLScalar(sb.ToString)
            sb.Remove(0, sb.Length)

            If reccount <> 0 Then

                With sb
                    .Append("SELECT t1.ReqId,t1.Descrip,t1.Hours,t1.Credits,t1.Code, t2.ReqTypeId ")
                    .Append("FROM arReqs t1, arReqTypes t2,syStatuses t3 ")
                    .Append("WHERE t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 0 and ")
                    .Append("t1.ReqId not in (Select ReqId from arProgVerDef t3 where PrgVerId = ?) ")
                    .Append("and t1.StatusId=t3.StatusId AND t3.Status='Active' ")
                    .Append("and t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append("ORDER BY t1.Descrip")
                End With
                ' Add the PrgVerId and ChildId to the parameter list
                db.AddParameter("@PrgVerId", ID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Else
                With sb
                    .Append("SELECT t1.ReqId,t1.Descrip,t1.Hours,t1.Credits,t1.Code,t2.ReqTypeId ")
                    .Append("FROM arReqs t1, arReqTypes t2,syStatuses t3 ")
                    .Append("WHERE t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 0 and t2.ReqTypeId = 1  ")
                    .Append("and t1.StatusId=t3.StatusId AND t3.Status='Active' ")
                    .Append("and t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append("ORDER BY t1.Descrip")
                End With
                db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            End If


        ElseIf TblSelect = "COURSEGRP" Then
            With sb
                .Append("Select count(*) from arReqGrpDef")
            End With

            db.OpenConnection()
            reccount = db.RunParamSQLScalar(sb.ToString)
            sb.Remove(0, sb.Length)

            If reccount <> 0 Then

                With sb
                    .Append("SELECT t1.ReqId,t1.Descrip,t1.Hours,t1.Credits,t1.Code, t2.ReqTypeId ")
                    .Append("FROM arReqs t1, arReqTypes t2,syStatuses t3 ")
                    .Append("WHERE t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 0 and  ")
                    .Append("t1.ReqId not in (Select ReqId from arReqGrpDef t3 where GrpId = ?) ")
                    .Append("and t1.StatusId=t3.StatusId AND t3.Status='Active' ")
                    .Append("and t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append("ORDER BY t1.Descrip")

                End With
                ' Add the PrgVerId and ChildId to the parameter list
                db.AddParameter("@CourseGrpId", ID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Else
                With sb
                    .Append("SELECT t1.ReqId,t1.Descrip,t1.Hours,t1.Credits,t1.Code, t2.ReqTypeId ")
                    .Append("FROM arReqs t1, arReqTypes t2,syStatuses t3 ")
                    .Append("WHERE t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 0 ")
                    .Append("and t1.StatusId=t3.StatusId AND t3.Status='Active' ")
                    .Append("and t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append("ORDER BY t1.Descrip")
                End With
                db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            End If

        End If

        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds
    End Function

    Public Function GetCoursesNotAssigned(ByVal TblSelect As String, ByVal ID As String, ByVal TrackAttendance As Boolean, ByVal CampusId As String) As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim da As New OleDbDataAdapter
        Dim sb As New System.Text.StringBuilder
        Dim reccount As Integer
        Dim attType As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        If TrackAttendance Then
            With sb
                .Append("select UnitTypeId from arPrgVersions where PrgVerId = ? ")
                db.AddParameter("@PrgVerId", ID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End With
            db.OpenConnection()
            attType = db.RunParamSQLScalar(sb.ToString).ToString
            sb.Remove(0, sb.Length)
            db.ClearParameters()
        End If
        If TblSelect = "PROGVER" Then
            '   build the sql query
            With sb
                .Append("Select count(*) from arProgVerDef")
            End With

            db.OpenConnection()
            reccount = db.RunParamSQLScalar(sb.ToString)
            sb.Remove(0, sb.Length)

            If reccount <> 0 Then

                With sb
                    .Append("SELECT t1.ReqId,t1.Descrip,t1.Hours,t1.Credits,t1.Code, t2.ReqTypeId ")
                    .Append("FROM arReqs t1, arReqTypes t2,syStatuses t3 ")
                    .Append("WHERE t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 0 and ")
                    .Append("t1.ReqId not in (Select ReqId from arProgVerDef t3 where PrgVerId = ?) ")
                    .Append("and t1.StatusId=t3.StatusId AND t3.Status='Active' ")
                    .Append("and t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    If TrackAttendance Then
                        .Append(" and t1.UnitTypeId IN (SELECT UnitTypeId FROM arAttUnitType WHERE UnitTypeDescrip NOT LIKE '%Present%' AND unitTypeDescrip NOT LIKE '%Minute%') ")
                        .Append(" Union ")
                        .Append(" SELECT t1.ReqId,t1.Descrip,t1.Hours,t1.Credits,t1.Code, t2.ReqTypeId FROM arReqs t1, arReqTypes t2,syStatuses t3 ")
                        .Append(" WHERE t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 0  and t1.StatusId=t3.StatusId AND t3.Status='Active' and t1.CampGrpId in (Select CampGrpId from ")
                        'modified by Theresa G on 5/18/09
                        '16224: 2.2.0: Can add the same course a second time on the program version defintions page. 
                        .Append(" syCmpGrpCmps where CampusId = ? ) and t1.UnitTypeId = ?  and t1.ReqId not in (Select ReqId from arProgVerDef t3 where PrgVerId = ?) ")

                    End If
                    .Append("ORDER BY t1.Descrip")
                End With
                ' Add the PrgVerId and ChildId to the parameter list
                db.AddParameter("@PrgVerId", ID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If TrackAttendance Then
                    db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@unitTypeId", attType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'added by Theresa G on 5/18/09
                    '16224: 2.2.0: Can add the same course a second time on the program version defintions page. 
                    db.AddParameter("@PrgVerId", ID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
            Else

                With sb
                    .Append("SELECT t1.ReqId,t1.Descrip,t1.Hours,t1.Credits,t1.Code,t2.ReqTypeId ")
                    .Append("FROM arReqs t1, arReqTypes t2,syStatuses t3 ")
                    .Append("WHERE t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 0 and t2.ReqTypeId = 1  ")
                    .Append("and t1.StatusId=t3.StatusId AND t3.Status='Active' ")
                    .Append("and t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    If TrackAttendance Then
                        .Append("and t1.UnitTypeId IN (SELECT UnitTypeId FROM arAttUnitType WHERE UnitTypeDescrip NOT LIKE '%Present%' AND unitTypeDescrip NOT LIKE '%Minute%') ")
                        .Append(" Union ")
                        .Append(" SELECT t1.ReqId,t1.Descrip,t1.Hours,t1.Credits,t1.Code, t2.ReqTypeId FROM arReqs t1, arReqTypes t2,syStatuses t3 ")
                        .Append(" WHERE t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 0  and t1.StatusId=t3.StatusId AND t3.Status='Active' and t1.CampGrpId in (Select CampGrpId from ")
                        'modified by Theresa G on 5/18/09
                        '16224: 2.2.0: Can add the same course a second time on the program version defintions page. 
                        .Append(" syCmpGrpCmps where CampusId = ? ) and t1.UnitTypeId = ?  and t1.ReqId not in (Select ReqId from arProgVerDef t3 where PrgVerId = ?)  ")
                    End If
                    .Append("ORDER BY t1.Descrip")
                End With
                db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If TrackAttendance Then
                    db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@unitTypeId", attType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'added by Theresa G on 5/18/09
                    '16224: 2.2.0: Can add the same course a second time on the program version defintions page. 
                    db.AddParameter("@PrgVerId", ID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

            End If


        ElseIf TblSelect = "COURSEGRP" Then
            With sb
                .Append("Select count(*) from arReqGrpDef")
            End With

            db.OpenConnection()
            reccount = db.RunParamSQLScalar(sb.ToString)
            sb.Remove(0, sb.Length)

            If reccount <> 0 Then

                With sb
                    .Append("SELECT t1.ReqId,t1.Descrip,t1.Hours,t1.Credits,t1.Code, t2.ReqTypeId ")
                    .Append("FROM arReqs t1, arReqTypes t2,syStatuses t3 ")
                    .Append("WHERE t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 0 and  ")
                    .Append("t1.ReqId not in (Select ReqId from arReqGrpDef t3 where GrpId = ?) ")
                    .Append("and t1.StatusId=t3.StatusId AND t3.Status='Active' ")
                    .Append("and t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append("ORDER BY t1.Descrip")

                End With
                ' Add the PrgVerId and ChildId to the parameter list
                db.AddParameter("@CourseGrpId", ID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Else
                With sb
                    .Append("SELECT t1.ReqId,t1.Descrip,t1.Hours,t1.Credits,t1.Code, t2.ReqTypeId ")
                    .Append("FROM arReqs t1, arReqTypes t2,syStatuses t3 ")
                    .Append("WHERE t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 0 ")
                    .Append("and t1.StatusId=t3.StatusId AND t3.Status='Active' ")
                    .Append("and t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append("ORDER BY t1.Descrip")
                End With
                db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            End If

        End If

        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds
    End Function

    Public Function GetCourseGrpsNotAssigned(ByVal TblSelect As String, ByVal ID As String, ByVal CampusId As String) As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim reccount As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        If TblSelect = "PROGVER" Then
            '   build the sql query
            With sb
                .Append("Select count(*) from arProgVerDef")
            End With

            db.OpenConnection()
            reccount = db.RunParamSQLScalar(sb.ToString)
            sb.Remove(0, sb.Length)

            If reccount <> 0 Then

                With sb
                    .Append("SELECT t1.ReqId,t1.Descrip,t1.Hours,t1.Credits, t2.ReqTypeId ")
                    .Append("FROM arReqs t1, arReqTypes t2,syStatuses t3 ")
                    .Append("WHERE t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 1 and t2.ReqTypeId = 2 and ")
                    .Append("t1.ReqId not in (Select ReqId from arProgVerDef t3 where PrgVerId = ?) ")
                    .Append("and t1.StatusId=t3.StatusId AND t3.Status='Active' ")
                    .Append("and t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append("ORDER BY t1.Descrip")
                End With
                ' Add the PrgVerId and ChildId to the parameter list
                db.AddParameter("@PrgVerId", ID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Else
                With sb
                    .Append("SELECT t1.ReqId,t1.Descrip,t1.Hours,t1.Credits, t2.ReqTypeId ")
                    .Append("FROM arReqs t1, arReqTypes t2,syStatuses t3 ")
                    .Append("WHERE t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 1 and t2.ReqTypeId = 2 ")
                    .Append("and t1.StatusId=t3.StatusId AND t3.Status='Active' ")
                    .Append("and t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append("ORDER BY t1.Descrip")
                End With
                db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            End If

        ElseIf TblSelect = "COURSEGRP" Then
            With sb
                .Append("Select count(*) from arReqGrpDef")
            End With

            db.OpenConnection()
            reccount = db.RunParamSQLScalar(sb.ToString)
            sb.Remove(0, sb.Length)

            If reccount <> 0 Then

                With sb
                    .Append("SELECT t1.ReqId,t1.Descrip,t1.Hours,t1.Credits, t2.ReqTypeId ")
                    .Append("FROM arReqs t1, arReqTypes t2,syStatuses t3 ")
                    .Append("WHERE t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 1 and  ")
                    .Append("t1.ReqId not in (Select ReqId from arReqGrpDef t3 where GrpId = ?) ")
                    .Append("and ReqId <> ? ")
                    .Append("and t1.StatusId=t3.StatusId AND t3.Status='Active' ")
                    .Append("and t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append("ORDER BY t1.Descrip")

                End With
                ' Add the PrgVerId and ChildId to the parameter list
                db.AddParameter("@CourseGrpId", ID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CourseGrpId2", ID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Else
                With sb
                    .Append("SELECT t1.ReqId,t1.Descrip,t1.Hours,t1.Credits, t2.ReqTypeId ")
                    .Append("FROM arReqs t1, arReqTypes t2,syStatuses t3 ")
                    .Append("WHERE t1.ReqTypeId = t2.ReqTypeId and t2.IsGroup = 1 ")
                    .Append("and t1.StatusId=t3.StatusId AND t3.Status='Active' ")
                    .Append("and t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
                    .Append("ORDER BY t1.Descrip")
                End With
                db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            End If

        End If

        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds
    End Function

    Public Function GetCoursesCourseGrpsAssgd(ByVal TblSelect As String, ByVal ID As String) As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        If TblSelect = "PROGVER" Then
            'Build query to obtain course/coursegrpids and course/coursegrpdescrips assgd to a prog ver.
            With sb

                .Append("SELECT t1.ReqId,ReqSeq,IsRequired,TrkForCompletion,GrdSysDetailId,t2.Descrip,t2.Code,t2.ReqTypeId,t2.Hours,t2.Credits,t1.ModUser,t1.ModDate, t1.CourseWeight, t1.ProgVerDefId ")
                .Append("FROM arProgVerDef t1, arReqs t2 ")
                .Append("WHERE t1.ReqId = t2.ReqId and t1.PrgVerId = ? and t2.ReqTypeId IN (1,2) ")
                .Append("ORDER BY t1.ReqSeq")

            End With
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@PrgVerId", ID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ElseIf TblSelect = "COURSEGRP" Then
            'Build query to obtain course/coursegrpids and course/coursegrpdescrips assgd to a coursegrp.
            With sb
                .Append("SELECT t1.ReqId,ReqSeq,IsRequired,t2.Descrip,t2.Code,t2.ReqTypeId,t2.Hours,t2.Credits,t1.ModUser,t1.ModDate ")
                .Append("FROM arReqGrpDef t1, arReqs t2 ")
                .Append("WHERE t1.ReqId = t2.ReqId and t1.GrpId = ? ")
                .Append("ORDER BY t1.ReqSeq")

            End With
            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@CourseGrpId", ID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the datatable
        Return ds.Tables(0)
    End Function
    Public Function GetGrdSysDetails(ByVal ID As String) As DataSet
        Dim ds As DataSet

        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.GrdSystemId, t2.GrdSysDetailId,t2.Grade ")
                .Append("FROM arPrgVersions t1, arGradeSystemDetails t2 ")
                .Append("WHERE t1.GrdSystemId = t2.GrdSystemId and t2.IsPass=1 and t2.IsInGPA=1 and  t1.PrgVerId = ? ")
                .Append(" order by t2.Grade ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@PrgVerId", ID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()


        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function InsertChildToDB(ByVal TblSelect As String, ByVal ChildInfoObj As ChildInfo, ByVal ID As String, ByVal user As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim childID As String
        Dim childTyp As String
        Dim childSeq As Integer
        Dim req As Integer
        Dim trk As Integer
        Dim hours As Decimal
        Dim credits As Decimal
        Dim grade As String
        Dim strnow As Date

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        childID = ChildInfoObj.ChildId
        ' childTyp = ChildInfoObj.ChildType
        childSeq = ChildInfoObj.ChildSeq
        req = ChildInfoObj.Req
        hours = ChildInfoObj.Hours
        credits = ChildInfoObj.Credits
        trk = ChildInfoObj.TrkForCompletion
        grade = ChildInfoObj.GradeOverride

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            If (TblSelect = "PROGVER") Then
                With sb
                    .Append("INSERT INTO arProgVerDef(PrgVerId,ReqId,ReqSeq,IsRequired,TrkForCompletion,GrdSysDetailId,Hours,Credits,ModUser,ModDate) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?)")

                    db.AddParameter("@prgverid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@childid", childID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@childseq", childSeq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@req", req, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@trk", trk, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@GradeOverride", grade, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@hours", hours, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@credits", credits, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    '   ModUser
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    strnow = Date.Now

                    '   ModDate
                    db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End With
            ElseIf (TblSelect = "COURSEGRP") Then
                With sb
                    .Append("INSERT INTO arReqGrpDef(GrpId,ReqId,ReqSeq,IsRequired,ModUser,ModDate) ")
                    .Append("VALUES(?,?,?,?,?,?)")
                End With

                db.AddParameter("@coursegrpid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@childid", childID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@childseq", childSeq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@req", req, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                '   ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                strnow = Date.Now
                '   ModDate
                db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            End If


            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try


        'Return the datatable in the dataset
        Return strnow.ToString

    End Function



    Public Function UpdateChildInDB(ByVal TblSelect As String, ByVal ChildInfoObj As ChildInfo, ByVal ID As String, ByVal user As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim childID As String
        Dim childTyp As String
        Dim childSeq As Integer
        Dim req As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        childID = ChildInfoObj.ChildId
        childSeq = ChildInfoObj.ChildSeq

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        If (TblSelect = "PROGVER") Then
            With sb
                .Append("UPDATE arProgVerDef Set ReqSeq = ?, ")
                .Append("ModUser=?, ")
                .Append("ModDate=? ")
                .Append("WHERE  ReqId = ? and PrgVerId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from arProgVerDef where ReqId = ? and PrgVerId = ? and ModDate = ? ")

                db.AddParameter("@childseq", childSeq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                '   ModDate
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@childid", childID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@prgverid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                '   ModDate
                db.AddParameter("@Original_ModDate", ChildInfoObj.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@childid2", childID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@prgverid2", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                '   ModDate
                db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End With
        ElseIf (TblSelect = "COURSEGRP") Then
            With sb
                .Append("UPDATE arReqGrpDef Set ReqSeq = ?, ")
                .Append("ModUser=?, ")
                .Append("ModDate=? ")
                .Append("WHERE  ReqId = ? and GrpId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from arReqGrpDef where ReqId = ? and GrpId = ? and ModDate = ? ")

                db.AddParameter("@childseq", childSeq, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                '   ModDate
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@childid", childID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@coursegrpid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                '   ModDate
                db.AddParameter("@Original_ModDate", ChildInfoObj.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@childid2", childID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@coursegrpid2", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                '   ModDate
                db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End With
        End If


        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function

    Public Function DeleteChildFrmDB(ByVal TblSelect As String, ByVal ChildInfoObj As ChildInfo, ByVal ID As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        If (TblSelect = "PROGVER") Then
            
            With sb
                .AppendLine("DELETE classSectionsTerms")
                .AppendLine(" FROM arClassSectionTerms classSectionsTerms")
                .AppendLine(" INNER JOIN arClassSections classSections ON classSections.ClsSectionId = classSectionsTerms.ClsSectionId")
                .AppendLine(" INNER JOIN arProgVerDef programVersionDefinition on programVersionDefinition.ProgVerDefId = classSections.ProgramVersionDefinitionId")
                .AppendLine(" WHERE programVersionDefinition.PrgVerId = ? ")
                .AppendLine(" AND programVersionDefinition.ReqId = ? ")
            End With

            db.AddParameter("@prgverid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@childid", ChildInfoObj.ChildId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            With sb
                .AppendLine("DELETE classSections")
                .AppendLine(" FROM arClassSections classSections")
                .AppendLine(" INNER JOIN arProgVerDef programVersionDefinition on programVersionDefinition.ProgVerDefId = classSections.ProgramVersionDefinitionId")
                .AppendLine(" WHERE programVersionDefinition.PrgVerId = ? ")
                .AppendLine(" AND programVersionDefinition.ReqId = ? ")
            End With

            db.AddParameter("@prgverid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@childid", ChildInfoObj.ChildId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            With sb
                .Append("DELETE FROM arProgVerDef ")
                .Append("WHERE PrgVerId = ? ")
                .Append("AND ReqId = ? ")
            End With

            db.AddParameter("@prgverid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@childid", ChildInfoObj.ChildId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

        ElseIf (TblSelect = "COURSEGRP") Then
            With sb
                .Append("DELETE FROM arReqGrpDef ")
                .Append("WHERE GrpId = ? ")
                .Append("AND ReqId = ? ")
            End With

            db.AddParameter("@campgrpid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@campusid", ChildInfoObj.ChildId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

        End If



        'Close Connection
        db.CloseConnection()
    End Function



    Public Function GetCourses() As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try
            'Set the connection string
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            'Build query to obtain courseids and coursedescrips from the arCourses table.
            With sb
                .Append("SELECT t1.ReqId,t1.Code,Descrip,Hours,Credits ")
                .Append("FROM arReqs t1 ")
                .Append("WHERE t1.ReqTypeId = 1 ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds.Tables(0)

    End Function
    Public Function GetCoursesForProgVersion(ByVal prgVerId As String) As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try
            'Set the connection string
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            'Build query to obtain courseids and coursedescrips from the arCourses table.
            With sb
                .Append("SELECT t1.ReqId,Descrip,t2.TermNo,t1.Code ")
                .Append("FROM arReqs t1,arProgVerDef t2  ")
                .Append("WHERE t1.ReqId=t2.ReqId and t1.ReqTypeId = 1 and PrgVerId = ? ")
                .Append(" order by TermNo,Descrip ")
            End With

            db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds.Tables(0)

    End Function
    Public Function GetCourseGrps() As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim da As OleDbDataAdapter

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try
            'Set the connection string
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            'Build query to obtain coursegrpids and coursegrpdescrips from the arCourseGrps table.
            With sb
                .Append("SELECT t1.ReqId,t1.Code,Descrip,Hours,Credits ")
                .Append("FROM arReqs t1 ")
                .Append("WHERE t1.ReqTypeId = 2 ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable
        Return ds.Tables(0)
    End Function

    Public Function LoadChildDS(ByVal TblSelect As String, ByVal ID As String) As DataSet

        Dim LoadedChildDS As New DataSet
        Dim dt1 As New DataTable
        Dim dt2 As New DataTable
        Dim dt3 As New DataTable
        Dim row As DataRow
        Dim isreq As Boolean

        dt1 = GetCourses().Copy
        dt1.TableName = "Courses"
        LoadedChildDS.Tables.Add(dt1)


        dt2 = GetCourseGrps().Copy
        dt2.TableName = "CourseGrps"
        LoadedChildDS.Tables.Add(dt2)


        dt3 = GetCoursesCourseGrpsAssgd(TblSelect, ID).Copy
        dt3.TableName = "FldsSelected"
        LoadedChildDS.Tables.Add(dt3)

        'return dataset to facade
        Return LoadedChildDS

    End Function

    Public Function GetMinGradeReqd(ByVal PrgVerId As String) As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Try
            'Set the connection string
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            'Build query to obtain courseids and coursedescrips from the arCourses table.
            With sb
                '.Append("Select t1.Grade from arGradeSystemDetails t1, arPrgVersions t2 ")
                '.Append("where t2.PrgVerId = ? and t1.GrdSystemId = t2.GrdSystemId and ")
                '.Append("t1.ViewOrder = (Select max(ViewOrder) from arGradeSystemDetails where IsPass = 1) ")
                '.Append(" Select t1.Grade from arGradeSystemDetails t1, arPrgVersions t2 ")
                '.Append(" where t2.PrgVerId = ? and t1.GrdSystemId = t2.GrdSystemId and ")
                '.Append(" IsPass = 1 and GPA=(select min(GPA) from arGradeSystemDetails where IsPass = 1) ")
                .Append(" Select top 1 t1.Grade,t1.GrdSysDetailId ")
                .Append(" from arGradeSystemDetails t1, arPrgVersions t2  where t2.PrgVerId = ? ")
                .Append(" and t1.GrdSystemId = t2.GrdSystemId and IsPass = 1 and GPA is not null order by GPA ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            ds = db.RunParamSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds.Tables(0)

    End Function
    Public Function AreStudentsRegFrPrgVer(ByVal ProgramVersion As String) As Integer
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder
        With sb
            '.Append("Select count(*) as Count from arResults a where a.StuEnrollId in (Select b.StuEnrollId from arStuEnrollments b where b.PrgVerId = ?) ")
            .Append("SELECT Count(*) ")
            .Append("FROM 	arResults R, arStuEnrollments SE, arPrgVersions PV ")
            .Append("WHERE ")
            .Append("       R.StuEnrollId=SE.StuEnrollId ")
            .Append("AND	SE.PrgVerId=PV.PrgVerId ")
            .Append("AND	PV.PrgVerId=? ")
        End With

        ' Add the PrgVerId to the parameter list
        db.AddParameter("@PrgVerId", ProgramVersion, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Return db.RunParamSQLScalar(sb.ToString)

    End Function
    Public Function AreStudentsEnrolledInPrgVer(ByVal ProgramVersion As String) As Integer
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder
        With sb
            '.Append("Select count(*) as Count from arResults a where a.StuEnrollId in (Select b.StuEnrollId from arStuEnrollments b where b.PrgVerId = ?) ")
            .Append("SELECT Count(*) ")
            .Append("FROM 	arStuEnrollments SE ")
            .Append("WHERE ")
            .Append("	SE.PrgVerId=? ")
        End With

        ' Add the PrgVerId to the parameter list
        db.AddParameter("@PrgVerId", ProgramVersion, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Return db.RunParamSQLScalar(sb.ToString)

    End Function
    Public Function PrgVersionsWithDifferingUnits(ByVal Program As String, ByVal hours As Decimal, ByVal credits As Decimal, ByVal terms As Integer, ByVal weeks As Integer) As Integer
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder
        With sb
            '.Append("Select count(*) as Count from arResults a where a.StuEnrollId in (Select b.StuEnrollId from arStuEnrollments b where b.PrgVerId = ?) ")
            .Append("SELECT Count(PrgVerId) ")
            .Append("FROM 	arPrgVersions PV ")
            .Append("WHERE ")
            .Append("       ProgId = ? ")
            .Append("AND	PrgVerId in ")
            .Append("(Select PrgVerId from arPrgVersions where Hours <> ? OR Credits <> ? and Terms <> ? and  Weeks <> ?)  ")
        End With

        ' Add the PrgVerId to the parameter list
        db.AddParameter("@PrgVerId", Program, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@hours", hours, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
        db.AddParameter("@credits", credits, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
        db.AddParameter("@terms", terms, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
        db.AddParameter("@weeks", weeks, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)

        'Execute the query
        Return db.RunParamSQLScalar(sb.ToString)

    End Function
    Public Function IsCourseHaveStudents(ByVal ReqId As String, ByVal ProgVerId As String) As Integer
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder
        With sb
            .Append("select count(*) from arResults t1,arClassSections t2,arReqs t3,arStuEnrollments t4 ")
            .Append(" where t1.TestID = t2.ClsSectionId And t2.ReqId = t3.ReqId and t1.StuEnrollId=t4.StuEnrollID ")
            .Append(" and t3.ReqId= ? and t4.PrgVerId= ? ")
        End With
        db.AddParameter("@ReqId", ReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ProgVerId", ProgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLScalar(sb.ToString)
    End Function
    ''Added by Saraswathi lakshmanan ON June 18 2010
    ''To fix the validation when removing courses from the Program version definition

    Public Function IsCourseHaveStudents_SP(ByVal ReqId As String, ByVal PrgVerId As String) As Integer

        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@PrgVerId", New Guid(PrgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@ReqId", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Try
            Return db.RunParamSQLScalar_SP("dbo.USP_AR_AreStudentsRegisteredinCourse")
        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try


    End Function

    Public Sub UpdatePrgVersionTermNo(ByVal termNo As Integer, ByVal reqId As String, ByVal prgverid As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try

            With sb
                .Append(" update arProgVerDef  set TermNo= ? where ReqId= ? and PrgVerId = ? ")

                db.AddParameter("@termNo", termNo, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@reqId", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@prgverid", prgverid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            End With



            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try


        'Return the datatable in the dataset


    End Sub
    ''Added by Saraswathi lakshmanan on may 05 2010 for payment periods

    Public Function GetDefaultChargesForProgVersion_sp(ByVal prgVerId As String) As DataTable

        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@PrgVerId", New Guid(prgVerId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_AR_GetDefaultCharges_GetList")
        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return ds.Tables(0)
    End Function

    Public Function GetAllSysTransCodes_SP(ByVal ShowActive As String) As DataTable

        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")


        db.AddParameter("@ShowActive", ShowActive, SqlDbType.VarChar, 50, ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_AR_GetSystemTransCodesCharges_GetList")
        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return ds.Tables(0)
    End Function

    Public Function GetAllFeeLevels_SP() As DataTable

        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")

        Try
            ds = db.RunParamSQLDataSet_SP("dbo.USP_AR_FeeLevels_GetList")
        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return ds.Tables(0)
    End Function

    Public Sub UpdateDefaultChargesforProgramVersion(ByVal dtDefaultTable As DataTable, ByVal UserName As String)
        Dim db As New SQLDataAccess
        Dim sb As New System.Text.StringBuilder
        Dim dt As New DataTable
        'Set the connection string

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim Insertquery = From DefaultChargeTable In dtDefaultTable.AsEnumerable()
                          Where DefaultChargeTable.Field(Of Integer)("CmdType") = 1 Select DefaultChargeTable

        For Each row In Insertquery
            db.AddParameter("@PrgVerID", New Guid(row("PrgVerID").ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@FeeLevelID", row("FeeLevelID"), SqlDbType.TinyInt, , ParameterDirection.Input)
            db.AddParameter("@SysTransCodeID", row("SysTransCodeID"), SqlDbType.Int, , ParameterDirection.Input)
            db.AddParameter("@PeriodCanChange", row("PeriodCanChange"), SqlDbType.Bit, , ParameterDirection.Input)
            db.AddParameter("@UserName", UserName, SqlDbType.VarChar, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_AR_DefaultCharges_Insert")
            db.ClearParameters()
        Next

        Dim Updatequery = From DefaultChargeTable In dtDefaultTable.AsEnumerable()
                          Where DefaultChargeTable.Field(Of Integer)("CmdType") = 2 Select DefaultChargeTable

        For Each row In Updatequery
            db.AddParameter("@PrgVerPmtId", New Guid(row("PrgVerPmtId").ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@PrgVerID", New Guid(row("PrgVerID").ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@FeeLevelID", row("FeeLevelID"), SqlDbType.TinyInt, , ParameterDirection.Input)
            db.AddParameter("@SysTransCodeID", row("SysTransCodeID"), SqlDbType.Int, , ParameterDirection.Input)
            db.AddParameter("@PeriodCanChange", row("PeriodCanChange"), SqlDbType.Bit, , ParameterDirection.Input)
            db.AddParameter("@UserName", UserName, SqlDbType.VarChar, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_AR_DefaultCharges_Update")
            db.ClearParameters()
        Next

        Dim Deletequery = From DefaultChargeTable In dtDefaultTable.AsEnumerable()
                          Where DefaultChargeTable.Field(Of Integer)("CmdType") = 3 Select DefaultChargeTable

        For Each row In Deletequery
            db.AddParameter("@PrgVerPmtId", New Guid(row("PrgVerPmtId").ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_AR_DefaultCharges_Delete")
            db.ClearParameters()
        Next


        'Try

        '    With sb
        '        .Append(" update arProgVerDef  set TermNo= ? where ReqId= ? and PrgVerId = ? ")

        '        db.AddParameter("@termNo", termNo, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        '        db.AddParameter("@reqId", reqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '        db.AddParameter("@prgverid", prgverid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '        End With



        '    db.RunParamSQLExecuteNoneQuery(sb.ToString)
        '    db.ClearParameters()
        '    sb.Remove(0, sb.Length)

        '        'Close Connection
        '    db.CloseConnection()

        'Catch ex As System.Exception
        '    Throw New BaseException(ex.Message)
        '    End Try


        'Return the datatable in the dataset


    End Sub
End Class

