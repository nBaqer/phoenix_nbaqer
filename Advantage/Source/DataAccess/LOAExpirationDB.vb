﻿Imports FAME.Advantage.Common

Public Class LOAExpirationDB
#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
#End Region

#Region "Public Properties"
    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property
#End Region
#Region "Public Methods"

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function


    Public Function GetLOAExpiration(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim strWhere As String
        Dim strDateRange As String
        Dim strTempWhere As String = ""
        Dim db As New SQLDataAccess
        Dim ds As New DataSet
        Dim campGrpId As String = String.Empty
        Dim prgVerId As String = String.Empty
        Dim StartDate As DateTime = "1/1/2000"
        Dim EndDate As DateTime = "1/1/2099"

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If
        strTempWhere = strWhere
        strTempWhere = strTempWhere.ToLower.Replace("and", ";")
        strWhere = ""
        Dim strArr As String() = strTempWhere.Remove(strTempWhere.IndexOf(";"), 1).Split(";")

        For i As Integer = 0 To strArr.Length - 1
            strWhere &= " and " & strArr(i)
        Next
        If strArr.Length = 1 Then
            strWhere &= " and "
        End If
        campGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in (")).ToLower.IndexOf(" and ")).Replace("t1.campgrpid in", "").Replace(")", "").Replace("(", "").Replace("'", "")

        'get the prgverid
        If strWhere.ToLower.Contains("arprgversions.prgverid") Then
            If strWhere.ToLower.Contains("arprgversions.prgverid in ") Then
                If strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in ")).ToLower.IndexOf(" and ") >= 0 Then
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in (")).ToLower.IndexOf(" and ")).Replace("arprgversions.prgverid in", "").Replace(")", "").Replace("(", "").Replace("'", "")
                Else
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in (")).Replace("arprgversions.prgverid in", "").Replace(")", "").Replace("(", "").Replace("'", "")
                End If
            Else
                If strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = ")).ToLower.IndexOf(" and ") >= 0 Then
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = "), strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = ")).ToLower.IndexOf(" and ")).Replace("arprgversions.prgverid =", "").Replace(")", "").Replace("(", "").Replace("'", "")
                Else
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = ")).Replace("arprgversions.prgverid =", "").Replace(")", "").Replace("(", "").Replace("'", "")
                End If
            End If
        End If
        'end the prgverid

        'get the CohortStartDate
        If paramInfo.FilterOther.ToLower.Contains("arstudentloas.enddate") Then
            strDateRange = paramInfo.FilterOther.ToLower
            strDateRange = strDateRange.Replace("arstudentloas.enddate between ", "")
            StartDate = strDateRange.Substring(1, strDateRange.IndexOf("and") - 3).Trim
            EndDate = strDateRange.Substring(strDateRange.IndexOf("and")).Trim.Replace("and", "").Replace("'", "").Trim
        End If

        'end the CohortStartDate


        db.AddParameter("@CampGrpId", campGrpId.Trim(), SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@ProgramIds", IIf(Not prgVerId = "", prgVerId.Trim(), DBNull.Value), SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@StartDate", StartDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@EndDate", EndDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@StudentIdentifier", StudentIdentifier, SqlDbType.VarChar, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet_SP("usp_AR_LOAExpiration", "PendingLOAExpiration")
        Return ds
    End Function
#End Region


End Class
