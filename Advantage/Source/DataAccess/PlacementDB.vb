Imports FAME.Advantage.Common

Public Class PlacementDB
    Public Function GetAllSkillGroups() As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select SG.SkillGrpId,SG.SkillGrpName ")
            .Append(" from plSkillGroups SG ")
            .Append(" Order By SG.SkillGrpName ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllSkillGroups(ByVal CampusId As String, ByVal LeadId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)

        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        With sb
            .Append(" Select Distinct SG.SkillGrpId as SkillGrpId,'(X) ' + SG.SkillGrpName as SkillGrpName from adLeadSkills L,plSkills S,plSkillGroups SG ")
            .Append(" where L.LeadId='" & LeadId & "' and L.SkillId=S.SkillId and S.SkillGrpId=SG.SkillGrpId and SG.SkillGrpId not in ")
            .Append(" (Select DISTINCT SG.SkillGrpId FROM   plSkillGroups SG WHERE   SG.StatusId = '" & strActiveGUID & "'  ")
            .Append(" AND SG.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(")) ")
            .Append(" Union ")
            .Append(" Select SG.SkillGrpId,SG.SkillGrpName from plSkillGroups SG WHERE   SG.StatusId = '" & strActiveGUID & "' ")
            .Append(" AND SG.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" Order By SkillGrpName ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllAssistance() As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select SG.AssistanceId,SG.AssistanceDescrip ")
            .Append(" from plAssistance SG ")
            .Append(" Order By SG.AssistanceDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllExtraCurrResume(ByVal StudentId As String) As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" select A.ExtracurrGrpDescrip as Descrip,B.ExtraCurrDescrip as Descrip,Null,Null ")
            .Append(" from adExtraCurrGrp A,adExtraCurr B,plStudentExtracurriculars C ")
            .Append(" where A.ExtraCurrGrpID = B.ExtraCurricularGrpID And C.ExtraCurricularID = B.ExtraCurrId ")
            .Append(" and StudentId = ? ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllDegrees() As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select SG.DegreeId,SG.DegreeDescrip ")
            .Append(" from arDegrees SG,syStatuses ST ")
            .Append(" where SG.StatusId = ST.StatusId and ST.Status='Active' ")
            .Append(" Order By SG.DegreeDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllSalaryType() As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select SG.SalaryTypeDescrip,SG.SalaryTypeId ")
            .Append(" from plSalaryType SG,syStatuses ST ")
            .Append(" where SG.StatusId = St.StatusId and St.Status = 'Active'")
            .Append(" Order By SG.SalaryTypeDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllSchoolStatus() As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select SG.Status,SG.StatusId ")
            .Append(" from SyStatuses SG ")
            .Append(" Order By SG.Status ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllFieldStudy() As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select SG.FldStudyDescrip,SG.FldStudyId ")
            .Append(" from plFldStudy SG,syStatuses ST ")
            .Append(" where SG.StatusId = St.StatusId and St.Status = 'Active'")
            .Append(" Order By SG.FldStudyDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllInterview() As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select SG.InterviewDescrip,SG.InterviewId ")
            .Append(" from plInterview SG,syStatuses ST ")
            .Append(" where SG.StatusId = St.StatusId and St.Status = 'Active'")
            .Append(" Order By SG.InterviewDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllTransportation() As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select SG.TransportationDescrip,SG.TransportationId ")
            .Append(" from plTransportation SG,syStatuses ST ")
            .Append(" where SG.StatusId = St.StatusId and St.Status = 'Active'")
            .Append(" Order By SG.TransportationDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllExtracurriculars() As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select SG.ExtraCurrId,SG.ExtraCurrDescrip ")
            .Append(" from adExtraCurr SG ")
            .Append(" Order By SG.ExtraCurrDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllExtracurricularGroups() As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            .Append(" Select EG.ExtraCurrGrpId as ExtraCurricularGrpId,EG.ExtraCurrGrpDescrip as ExtraCurricularGrpName ")
            .Append(" from adExtraCurrGrp EG,SyStatuses SG ")
            .Append(" where EG.StatusId = SG.StatusId ")
            .Append(" and SG.Status = 'Active'")
            .Append(" Order By EG.ExtracurrGrpDescrip ")
        End With
        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllExtracurricularGroups(ByVal LeadId As String, ByVal CampusId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)

        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        With sb
            .Append(" Select Distinct EG.ExtraCurrGrpId as ExtraCurricularGrpId,'(X) ' + EG.ExtraCurrGrpDescrip as ExtraCurricularGrpName ")
            .Append(" from adLeadExtracurriculars L,adExtracurr S,adExtraCurrGrp EG ")
            .Append(" where L.LeadId='" & LeadId & "' and L.ExtracurricularId=S.ExtracurrId and S.ExtracurricularGrpId=EG.ExtraCurrGrpId ")
            .Append(" and EG.ExtracurrGrpId not in ")
            .Append(" (Select DISTINCT SG.ExtracurrGrpId FROM   adExtraCurrGrp SG WHERE  SG.StatusId = '" & strActiveGUID & "'  ")
            .Append(" AND SG.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(")) ")
            .Append(" Union ")
            .Append(" Select DISTINCT SG.ExtracurrGrpId,SG.ExtraCurrGrpDescrip as ExtraCurricularGrpName FROM   adExtraCurrGrp SG WHERE   SG.StatusId = '" & strActiveGUID & "' ")
            .Append(" AND SG.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" Order By ExtraCurricularGrpName ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllExtraCurricularsByGroup(ByVal ExtracurricularGrpId As String, ByVal StudentId As String) As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" select SK.ExtracurrDescrip,SK.ExtracurrId from adExtraCurr SK where ExtracurrId ")
            .Append(" not in (select ExtracurricularId from plStudentExtracurriculars ")
            .Append(" where ExtracurricularGrpId=? ")
            .Append(" and StudentId=? )")
            .Append(" and SK.ExtracurricularGrpId =? ")
            .Append(" and SK.StatusId in ")
            .Append(" (Select StatusId from SyStatuses where Status='Active') ")
        End With

        'Add Parameter
        db.AddParameter("@SkillGrpId", ExtracurricularGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Add Parameter
        db.AddParameter("@SkillGrpId1", ExtracurricularGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

        db.CloseConnection()

    End Function
    Public Function GetAllLeadExtraCurricularsByGroup(ByVal ExtracurricularGrpId As String, ByVal LeadId As String) As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" select SK.ExtracurrDescrip,SK.ExtracurrId from adExtraCurr SK where ExtracurrId ")
            .Append(" not in (select ExtracurricularId from adLeadExtracurriculars ")
            .Append(" where  ")
            .Append(" LeadId=? )")
            .Append(" and SK.ExtracurricularGrpId =? ")
            .Append(" and SK.StatusId in ")
            .Append(" (Select StatusId from SyStatuses where Status='Active') ")
        End With

        'Add Parameter
        'db.AddParameter("@SkillGrpId", ExtracurricularGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Add Parameter
        db.AddParameter("@SkillGrpId1", ExtracurricularGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
        db.CloseConnection()
    End Function
    Public Function GetAllLeadExtraCurricularsByGroup(ByVal ExtracurricularGrpId As String, ByVal LeadId As String, ByVal CampusId As String) As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)

        Dim strCampGrpId As String = ""
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        With sb
            .Append(" Select Distinct S.ExtracurrId as ExtraCurrId,'(X) ' + S.ExtraCurrDescrip as ExtraCurrDescrip  ")
            .Append(" from adLeadExtracurriculars L,adExtraCurr S ")
            .Append(" where L.LeadId='" & LeadId & "' and L.ExtracurricularId=S.ExtracurrId and S.ExtraCurricularGrpId='" & ExtracurricularGrpId & "' and S.ExtraCurrId not in ")
            .Append(" (Select DISTINCT SG.ExtracurrId FROM adExtraCurr SG WHERE  SG.ExtraCurricularGrpId='" & ExtracurricularGrpId & "' AND SG.StatusId = '" & strActiveGUID & "' ")
            .Append(" AND SG.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(")) ")
            .Append(" AND S.ExtraCurrId not in (select Distinct ExtraCurricularId from adLeadExtracurriculars where LeadId='" & LeadId & "') ")
            .Append(" Union ")
            .Append(" select S.ExtracurrId as ExtraCurrId,S.ExtraCurrDescrip as ExtraCurrDescrip ")
            .Append(" from adExtraCurr S where S.ExtraCurricularGrpId='" & ExtracurricularGrpId & "' AND S.StatusId = '" & strActiveGUID & "' AND ")
            .Append(" S.ExtraCurrId not in (select Distinct ExtraCurricularId from adLeadExtracurriculars where LeadId='" & LeadId & "') ")
            .Append(" AND S.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("Order by ExtraCurrDescrip ")
        End With
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
        db.CloseConnection()
    End Function

    Public Function GetAllSkillsByGroup(ByVal SkillGrpId As String, ByVal StudentId As String) As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" select SK.SkillDescrip,SK.SkillId from plSkills SK where SkillId ")
            .Append(" not in (select SkillId from plStudentSkills ")
            .Append(" where  ")
            .Append(" StudentId=? )")
            .Append(" and SK.SkillGrpId =? ")
            .Append(" and SK.StatusId in ")
            .Append(" (Select StatusId from SyStatuses where Status='Active') ")
        End With

        'Add Parameter
        'db.AddParameter("@SkillGrpId", SkillGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Add Parameter
        db.AddParameter("@SkillGrpId1", SkillGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

        db.CloseConnection()

    End Function
    Public Function GetAllLeadSkillsByGroup(ByVal SkillGrpId As String, ByVal LeadId As String) As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" select SK.SkillDescrip,SK.SkillId from plSkills SK where SkillId ")
            .Append(" not in (select SkillId from adLeadSkills ")
            .Append(" where ")
            .Append(" LeadId=? )")
            .Append(" and SK.SkillGrpId =? ")
            .Append(" and SK.StatusId in ")
            .Append(" (Select StatusId from SyStatuses where Status='Active') ")
        End With

        'Add Parameter
        'db.AddParameter("@SkillGrpId", SkillGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.AddParameter("@StudentId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Add Parameter
        db.AddParameter("@SkillGrpId1", SkillGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

        db.CloseConnection()

    End Function
    Public Function GetAllLeadSkillsByGroup(ByVal LeadId As String, ByVal CampusId As String, ByVal strSkillGrpId As String) As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)

        Dim strCampGrpId As String = ""
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        With sb
            .Append(" Select Distinct S.SkillId as SkillId,'(X) ' + S.SkillDescrip as SkillDescrip from adLeadSkills L,plSkills S ")
            .Append(" where L.LeadId='" & LeadId & "' and L.SkillId=S.SkillId and S.SkillGrpId='" & strSkillGrpId & "' and S.SkillId not in ")
            .Append(" (Select DISTINCT SG.SkillId FROM  plSkills SG WHERE  SG.SkillGrpId='" & strSkillGrpId & "' AND SG.StatusId = '" & strActiveGUID & "' ")
            .Append(" AND SG.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(")) ")
            .Append(" AND S.SkillId not in (select Distinct SkillId from adLeadSkills where LeadId='" & LeadId & "') ")
            .Append(" Union ")
            .Append(" select SK.SkillId,SK.SkillDescrip from plSkills SK where SK.SkillGrpId='" & strSkillGrpId & "' AND SK.StatusId = '" & strActiveGUID & "' AND ")
            .Append(" SK.SkillId not in (select Distinct SkillId from adLeadSkills where LeadId='" & LeadId & "') ")
            .Append(" AND SK.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
        End With

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

        db.CloseConnection()

    End Function
    Public Function GetLeadSkillsByGroup(ByVal LeadId As String, ByVal CampusId As String) As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)

        Dim strCampGrpId As String = ""
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        With sb
            .Append(" Select Distinct S.SkillId as SkillId,'(X) ' + S.SkillDescrip as SkillDescrip from adLeadSkills L,plSkills S ")
            .Append(" where L.LeadId='" & LeadId & "' and L.SkillId=S.SkillId and S.SkillId not in ")
            .Append(" (Select DISTINCT SG.SkillId FROM  plSkills SG WHERE  SG.StatusId = '" & strActiveGUID & "' ")
            .Append(" AND SG.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(")) ")
            .Append(" Union ")
            .Append(" select SK.SkillId as SkillId,SK.SkillDescrip as SkillDescrip from adLeadSkills L,plSkills SK where L.LeadId='" & LeadId & "' AND L.SkillId = SK.SkillId AND SK.StatusId = '" & strActiveGUID & "' AND ")
            .Append(" SK.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
        End With

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

        db.CloseConnection()

    End Function
    Public Function UpdateStudentSkills(ByVal StudentId As String, ByVal SkillGrpId As String, ByVal user As String, ByVal selectedDegrees() As String)
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            '   First we have to delete all existing selections
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM plStudentSkills ")
                .Append("WHERE StudentId = ? ")
                .Append("AND SkillId IN (SELECT SkillId FROM plSkills WHERE SkillGrpId = ? ) ")
            End With

            '   delete all selected items
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@SkillGrpId", SkillGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            '   Insert one record per each Item in the Selected Group
            Dim i As Integer
            For i = 0 To selectedDegrees.Length - 1
                '   build query
                With sb
                    .Append("INSERT INTO plStudentSkills (StudentId, SkillId, ModDate, ModUser) ")
                    .Append("VALUES(?,?,?,?)")
                End With

                'add parameters
                db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@SkillId", DirectCast(selectedDegrees.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'db.AddParameter("@SkillGrpId", SkillGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Next

        Catch ex As Exception
            '   return error message
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateLeadSkills(ByVal LeadId As String, ByVal SkillGrpId As String, ByVal user As String, ByVal selectedDegrees() As String)
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   First we have to delete all existing selections
        '   build the query
        '   build query
        Dim sb As New StringBuilder
        With sb
            .Append("DELETE FROM adLeadSkills WHERE LeadId = ? ")
        End With
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        '   Insert one record per each Item in the Selected Group
        Dim i As Integer
        For i = 0 To selectedDegrees.Length - 1
            With sb
                .Append("INSERT INTO adLeadSkills (LeadSkillId,LeadId, SkillId,ModDate, ModUser) ")
                .Append("VALUES(?,?,?,?,?)")
            End With

            'add parameters
            db.AddParameter("@LeadSkillId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@SkillId", DirectCast(selectedDegrees.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        Next
        'Close Connection
        db.CloseConnection()
    End Function
    'Public Function UpdateEduStudentExtracurriculurs(ByVal StudentId As String, ByVal EducationInstId As String, ByVal user As String, ByVal selectedDegrees() As String)
    '    '   connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    '   First we have to delete all existing selections
    '    '   build the query
    '    Dim sb As New StringBuilder
    '    With sb
    '        .Append("DELETE FROM plStEduExtracurricular WHERE StudentId = ? ")
    '        .Append(" and EducationInstId = ? ")
    '    End With

    '    '   delete all selected items
    '    db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@EducationInstId", EducationInstId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '    db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '    db.ClearParameters()
    '    sb.Remove(0, sb.Length)

    '    '   Insert one record per each Item in the Selected Group
    '    Dim i As Integer
    '    Try
    '        For i = 0 To selectedDegrees.Length - 1

    '            '   build query
    '            With sb
    '                .Append("INSERT INTO plStEduExtracurricular(StudentId, ExtracurrId,EducationInstId,ModDate, ModUser) ")
    '                .Append("VALUES(?,?,?,?,?)")
    '            End With
    '            'add parameters
    '            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@ExtracurrId", DirectCast(selectedDegrees.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@EducationInstId", EducationInstId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '            'execute query
    '            db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '            db.ClearParameters()
    '            sb.Remove(0, sb.Length)
    '        Next
    '    Catch e As System.Exception
    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function
    Public Function UpdateStudentExtracurriculars(ByVal StudentId As String, ByVal ExtracurricularGrpId As String, ByVal user As String, ByVal selectedDegrees() As String)
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   First we have to delete all existing selections
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("DELETE FROM plStudentExtracurriculars WHERE StudentId = ? ")
            .Append(" and ExtracurricularGrpId = ? ")
        End With

        '   delete all selected items
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ExtracurricularGrpId", ExtracurricularGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        '   Insert one record per each Item in the Selected Group
        Dim i As Integer
        For i = 0 To selectedDegrees.Length - 1
            Dim strStuExtraCurricularID As String = Guid.NewGuid.ToString
            '   build query
            With sb
                .Append("INSERT INTO plStudentExtracurriculars(StuExtraCurricularID,StudentId, ExtracurricularId, ExtraCurricularGrpId,ModDate, ModUser) ")
                .Append("VALUES(?,?,?,?,?,?)")
            End With

            'add parameters
            db.AddParameter("@StuExtraCurricularID", strStuExtraCurricularID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@SkillId", DirectCast(selectedDegrees.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@SkillGrpId", ExtracurricularGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Add data to regent
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                Dim regDB As New regentDB
                Dim strFilename As String = AdvantageCommonValues.getStudentBatchFileName()
                regDB.AddActivityXML(StudentId, strFilename, strStuExtraCurricularID)
            End If
        Next
        'Close Connection
        db.CloseConnection()
    End Function
    Public Function UpdateLeadExtracurriculars(ByVal LeadId As String, ByVal ExtracurricularGrpId As String, ByVal user As String, ByVal selectedDegrees() As String)
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder

        '   Insert one record per each Item in the Selected Group
        Dim i As Integer
        For i = 0 To selectedDegrees.Length - 1

            With sb
                .Append("DELETE FROM adLeadExtracurriculars WHERE LeadId = ? ")
                .Append(" and ExtracurricularId = ? ")
            End With

            '   delete all selected items
            db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ExtracurricularId", DirectCast(selectedDegrees.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            '   build query
            With sb
                .Append("INSERT INTO adLeadExtracurriculars(LeadExtraCurricularID,LeadId, ExtracurricularId, ModDate, ModUser) ")
                .Append("VALUES(?,?,?,?,?)")
            End With

            'add parameters
            db.AddParameter("@LeadExtraCurricularID", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ExtracurricularId", DirectCast(selectedDegrees.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'db.AddParameter("@ExtracurricularGrpId", ExtracurricularGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        Next

        'Close Connection
        db.CloseConnection()

    End Function
    Public Function GetStudentSkillByGroup(ByVal SkillGrpId As String, ByVal StudentId As String) As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select A.SkillId as SkillId,B.SkillDescrip as SkillDescrip,B.SkillGrpId,C.SkillGrpName")
            .Append(" from plStudentSkills A,plSkills B,plSkillGroups C ")
            .Append(" where A.SkillId = B.SkillId and B.SkillGrpId = C.SkillGrpId ")
            .Append(" and A.StudentId = ? ")
            .Append(" Order By B.SkillDescrip ")
        End With

        'Add Parameter
        'db.AddParameter("@SkillGrpId", SkillGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Add StudentId
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

        db.CloseConnection()

    End Function
    Public Function GetLeadSkillByGroup(ByVal SkillGrpId As String, ByVal LeadId As String) As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select A.SkillId as SkillId,B.SkillDescrip as SkillDescrip")
            .Append(" from adLeadSkills A,plSkills B,plSkillGroups C where ")
            .Append(" A.SkillId = B.SkillId and B.SkillGrpId=C.SkillGrpId ")
            .Append(" and A.LeadId = ? ")
            .Append(" Order By A.SkillId ")
        End With

        'Add Parameter
        'db.AddParameter("@SkillGrpId", SkillGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Add StudentId
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

        db.CloseConnection()

    End Function
    Public Function GetStudentExtracurricularsByGroup(ByVal ExtracurricularGrpId As String, ByVal StudentId As String) As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select A.ExtracurricularId,B.ExtraCurrDescrip ")
            .Append(" from plStudentExtracurriculars A,adExtracurr B where ")
            .Append(" A.ExtracurricularId = B.ExtracurrId and A.ExtracurricularGrpId= ? ")
            .Append(" and A.StudentId = ? ")
            .Append(" Order By A.ExtracurricularId ")
        End With

        'Add Parameter
        db.AddParameter("@ExtracurricularGrpId", ExtracurricularGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Add StudentId
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

        db.CloseConnection()

    End Function
    Public Function GetLeadExtracurricularsByGroup(ByVal ExtracurricularGrpId As String, ByVal LeadId As String) As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select A.ExtracurricularId,B.ExtraCurrDescrip ")
            .Append(" from adLeadExtracurriculars A,adExtracurr B,adExtraCurrGrp C where ")
            .Append(" A.ExtracurricularId = B.ExtracurrId and B.ExtraCurricularGrpId = C.ExtraCurrGrpId and ")
            .Append(" A.LeadId = ? ")
            .Append(" Order By A.ExtracurricularId ")
        End With

        'Add Parameter
        ' db.AddParameter("@ExtracurricularGrpId", ExtracurricularGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Add StudentId
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

        db.CloseConnection()

    End Function
    Public Function GetLeadExtracurricularsByGroup(ByVal ExtracurricularGrpId As String, ByVal LeadId As String, ByVal CampusId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)

        Dim strCampGrpId As String = ""
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        With sb
            .Append(" Select Distinct S.ExtracurrId as ExtraCurrId,'(X) ' + S.ExtraCurrDescrip as ExtraCurrDescrip  ")
            .Append(" from adLeadExtracurriculars L,adExtraCurr S ")
            .Append(" where L.LeadId='" & LeadId & "' and L.ExtracurricularId=S.ExtracurrId and S.ExtraCurrId not in ")
            .Append(" (Select DISTINCT SG.ExtracurrId FROM adExtraCurr SG WHERE  SG.StatusId = '" & strActiveGUID & "' ")
            .Append(" AND SG.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(")) ")
            .Append(" Union ")
            .Append(" select S.ExtracurrId as ExtraCurrId,S.ExtraCurrDescrip as ExtraCurrDescrip ")
            .Append(" from adExtraCurr S,adLeadExtracurriculars L where L.LeadId='" & LeadId & "' and S.ExtraCurrId=L.ExtracurricularId and S.StatusId = '" & strActiveGUID & "'  ")
            .Append(" AND S.CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append("Order by ExtraCurrDescrip ")
        End With
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)






        With sb
            .Append(" Select A.ExtracurricularId,B.ExtraCurrDescrip ")
            .Append(" from adLeadExtracurriculars A,adExtracurr B,adExtraCurrGrp C where ")
            .Append(" A.ExtracurricularId = B.ExtracurrId and B.ExtraCurricularGrpId = C.ExtraCurrGrpId and ")
            .Append(" A.LeadId = ? ")
            .Append(" Order By A.ExtracurricularId ")
        End With

        'Add Parameter
        ' db.AddParameter("@ExtracurricularGrpId", ExtracurricularGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Add StudentId
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

        db.CloseConnection()

    End Function
    Public Function AddExtracurricularInfo(ByVal extracurricularinfo As PlacementInfo, ByVal user As String) As Integer

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        'Try
        '    'Build The Query
        Dim sb As New StringBuilder
        With sb
            .Append("Insert adExtracurr(ExtracurrId,ExtracurrCode, ")
            .Append(" StatusId, ExtracurrDescrip,ModUser,ModDate, ")
            .Append(" CampGrpId,ExtracurricularGrpId) ")
            .Append(" Values(?,?,?,?,?,?,?,?) ")
        End With

        '    'Add Parameters To Query

        '    'EmployerId
        db.AddParameter("@ExtracurricularId", extracurricularinfo.ExtracurricularId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '    'Code
        db.AddParameter("@ExraCurrCode", extracurricularinfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'StatusId
        If extracurricularinfo.StatusId = Guid.Empty.ToString Then
            db.AddParameter("@StatusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@StatusId", extracurricularinfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        'ExtracurrDescrip
        db.AddParameter("@ExtracurrDescrip", extracurricularinfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '    '   ModUser
        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '    '   ModDate
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        'CampGroupId
        If extracurricularinfo.CampGrpid = Guid.Empty.ToString Then
            db.AddParameter("@CampGrpId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@CampGrpId", extracurricularinfo.CampGrpid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        'ExtracurrGrpId
        If extracurricularinfo.ExtracurricularGrpId = Guid.Empty.ToString Then
            db.AddParameter("@ExtracurrGrpId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@ExtracurrGrpId", extracurricularinfo.ExtracurricularGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        '    'Execute The Query
        db.RunParamSQLExecuteNoneQuery(sb.ToString)

        extracurricularinfo.IsInDb = True

        'Retun Without Errors
        Return 0

        'Catch ex As System.Exception
        'Return an Error To Client
        '   Return -1

        '  Finally
        'Close Connection
        db.CloseConnection()
        '     End Try
    End Function

    Public Function UpdateExtracurricularInfo(ByVal Extracurricularinfo As PlacementInfo, ByVal User As String) As Integer

        'Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Update plExtracurriculars set StatusId=?, ")
                .Append(" Code=?, ")
                .Append(" ExtracurrDescrip=?,ModUser=?,ModDate=?, ")
                .Append(" CampGrpId=?,ExtracurrGrpId=? ")
                .Append(" Where ExtracurricularId=? ")
            End With
            '    'Add Parameters To Query

            'StatusId
            If Extracurricularinfo.StatusId = Guid.Empty.ToString Then
                db.AddParameter("@StatusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StatusId", Extracurricularinfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Code
            db.AddParameter("@Code", Extracurricularinfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ExtracurrDescrip
            db.AddParameter("@ExtracurrDescrip", Extracurricularinfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '    '   ModUser
            db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '    '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'CampGroupId
            If Extracurricularinfo.CampGrpid = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", Extracurricularinfo.CampGrpid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ExtracurrGrpId
            If Extracurricularinfo.ExtracurricularGrpId = Guid.Empty.ToString Then
                db.AddParameter("@ExtracurrGrpId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ExtracurrGrpId", Extracurricularinfo.ExtracurricularGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ExtracurricularId
            db.AddParameter("@ExtracurricularId", Extracurricularinfo.ExtracurricularId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            Extracurricularinfo.IsInDb = True
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAllExtracurricularDesc(ByVal showActiveOnly As Boolean) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   BC.ExtracurrDescrip,BC.ExtracurricularId ")
            .Append("FROM     plExtracurriculars BC, syStatuses ST ")
            .Append("WHERE    BC.StatusId = ST.StatusId ")



            '   Conditionally include only Active Items 
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            Else
                .Append(" AND ST.Status = 'Inactive' ")
            End If
            .Append("ORDER BY BC.ExtracurrDescrip ")
        End With

        ' return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetExtracurricularDetails(ByVal ExtracurricularId As String) As PlacementInfo

        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT     B.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=B.StatusId) as Status, ")
            .Append("    B.Code, ")
            .Append("    B.ExtracurrDescrip, ")
            .Append(" B.ExtracurrGrpId, ")
            .Append("    (Select ExtraCurrGrpDescrip from adExtraCurrGrp where ExtracurrGrpId=B.ExtracurrGrpId) As ExtracurricularGrpDescrip, ")
            .Append("    B.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=B.CampGrpId) As CampGrpDescrip ")
            .Append("FROM  plExtracurriculars B ")
            .Append("WHERE B.ExtracurricularId= ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@ExtracurricularId", ExtracurricularId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim Extracurricularinfo As New PlacementInfo

        While dr.Read()

            'set properties with data from DataReader
            With Extracurricularinfo
                .IsInDb = True
                .ExtracurricularId = ExtracurricularId

                If Not (dr("StatusId") Is DBNull.Value) Then .StatusId = CType(dr("StatusId"), Guid).ToString
                If Not (dr("CampGrpId") Is DBNull.Value) Then .CampGrpid = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("ExtracurrGrpId") Is DBNull.Value) Then .ExtracurricularGrpId = CType(dr("ExtracurrGrpId"), Guid).ToString

                'Get Code
                .Code = dr("Code")

                'Get ExtracurrDescrip
                .Description = dr("ExtracurrDescrip")

            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return Extracurricularinfo
    End Function
    Public Function DeleteExtracurriculur(ByVal ExtracurriculurId As String) As Integer

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM plExtraCurriculars ")
                .Append("WHERE ExtraCurricularID = ? ")
            End With

            '   add parameters values to the query

            '   BankId
            db.AddParameter("@ExtraCurricularID", ExtracurriculurId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return 0

        Catch ex As OleDbException
            '   return an error to the client
            Return -1
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetAllSchools(ByVal SchoolType As String, ByVal campusid As String, Optional ByVal LeadId As String = "") As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusid)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        Select Case SchoolType
            Case "College"
                With sb
                    If Not LeadId = "" Then
                        .Append(" Select DISTINCT LT.CollegeID as CollegeId,'(X) ' + LT.CollegeName as CollegeName from adLeadEducation L,adColleges LT ")
                        .Append(" where L.EducationInstID=LT.CollegeID AND L.LeadId='" & LeadId & "' ")
                        .Append(" AND L.EducationInstID not in   ")
                        .Append(" (  ")
                        .Append(" select Distinct CollegeId from adColleges ")
                        .Append(" WHERE   ")
                        .Append(" StatusId = '" & strActiveGUID & "' ")
                        .Append(" AND CampGrpId in ('")
                        .Append(strCampGrpId)
                        .Append(")  ")
                        .Append(" ) ")
                        .Append(" Union ")
                    End If
                    .Append(" select Distinct CollegeId as CollegeId,CollegeName as CollegeName from adColleges ")
                    .Append(" WHERE   ")
                    .Append(" StatusId = '" & strActiveGUID & "' ")
                    .Append(" AND CampGrpId in ('")
                    .Append(strCampGrpId)
                    .Append(")  ")
                    .Append(" ORDER BY CollegeName ")
                End With
            Case "Schools"
                With sb
                    If Not LeadId = "" Then
                        .Append(" Select Distinct LT.HsId as CollegeId,'(X) ' + LT.HsName as CollegeName from adLeadEducation L,syInstitutions LT ")
                        .Append(" where L.EducationInstID=LT.HsID AND L.LeadId='" & LeadId & "' ")
                        .Append(" AND L.EducationInstID not in   ")
                        .Append(" (  ")
                        .Append(" select Distinct HsId from syInstitutions ")
                        .Append(" WHERE   ")
                        .Append(" StatusId = '" & strActiveGUID & "' ")
                        .Append(" AND CampGrpId in ('")
                        .Append(strCampGrpId)
                        .Append(")  ")
                        .Append(" ) ")
                        .Append(" Union ")
                    End If
                    .Append(" select Distinct HsId as CollegeId,HsName as CollegeName from syInstitutions ")
                    .Append(" WHERE   ")
                    .Append(" StatusId = '" & strActiveGUID & "' ")
                    .Append(" AND CampGrpId in ('")
                    .Append(strCampGrpId)
                    .Append(")  ")
                    .Append(" ORDER BY CollegeName ")
                End With
        End Select
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllSchoolsForStudent(ByVal SchoolType As String, ByVal campusid As String, Optional ByVal StudentId As String = "") As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim dsGetCmpGrps As New DataSet

        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusid)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        Select Case SchoolType
            Case "College"
                With sb
                    If Not StudentId = "" Then
                        .Append(" Select DISTINCT LT.CollegeID as CollegeId,'(X) ' + LT.CollegeName as CollegeName from plStudentEducation L,adColleges LT ")
                        .Append(" where L.EducationInstID=LT.CollegeID AND L.StudentId='" & StudentId & "' ")
                        .Append(" AND L.EducationInstID not in   ")
                        .Append(" (  ")
                        .Append(" select Distinct CollegeId from adColleges ")
                        .Append(" WHERE   ")
                        .Append(" StatusId = '" & strActiveGUID & "' ")
                        .Append(" AND CampGrpId in ('")
                        .Append(strCampGrpId)
                        .Append(")  ")
                        .Append(" ) ")
                        .Append(" Union ")
                    End If
                    .Append(" select Distinct CollegeId as CollegeId,CollegeName as CollegeName from adColleges ")
                    .Append(" WHERE   ")
                    .Append(" StatusId = '" & strActiveGUID & "' ")
                    .Append(" AND CampGrpId in ('")
                    .Append(strCampGrpId)
                    .Append(")  ")
                    .Append(" ORDER BY CollegeName ")
                End With
            Case "Schools"
                With sb
                    If Not StudentId = "" Then
                        .Append(" Select Distinct LT.HsId as CollegeId,'(X) ' + LT.HsName as CollegeName from plStudentEducation L,syInstitutions LT ")
                        .Append(" where L.EducationInstID=LT.HsID AND L.StudentId='" & StudentId & "' ")
                        .Append(" AND L.EducationInstID not in   ")
                        .Append(" (  ")
                        .Append(" select Distinct HsId from syInstitutions ")
                        .Append(" WHERE   ")
                        .Append(" StatusId = '" & strActiveGUID & "' ")
                        .Append(" AND CampGrpId in ('")
                        .Append(strCampGrpId)
                        .Append(")  ")
                        .Append(" ) ")
                        .Append(" Union ")
                    End If
                    .Append(" select Distinct HsId as CollegeId,HsName as CollegeName from syInstitutions ")
                    .Append(" WHERE   ")
                    .Append(" StatusId = '" & strActiveGUID & "' ")
                    .Append(" AND CampGrpId in ('")
                    .Append(strCampGrpId)
                    .Append(")  ")
                    .Append(" ORDER BY CollegeName ")
                End With
        End Select
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllSchools(ByVal SchoolType As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Select Case SchoolType
            Case "College"
                With sb
                    .Append(" Select Distinct SG.CollegeId,SG.CollegeName ")
                    .Append(" from adColleges SG ")
                    '.Append(" ,syStatuses ST ")
                    '.Append(" where SG.StatusId = ST.StatusId and ST.Status = 'Active'  ")
                    '.Append(" where SG.StatusId = ST.StatusId ")
                    .Append(" Order By SG.CollegeName ")
                End With
            Case "Schools"
                With sb
                    .Append(" Select Distinct SG.HsId as CollegeId,SG.HsName as CollegeName")
                    .Append(" from syInstitutions SG ")
                    '.Append(" ,syStatuses ST ")
                    '.Append(" where SG.StatusId = ST.StatusId and ST.Status = 'Active'  ")
                    .Append(" Order By SG.HsName ")
                End With
        End Select
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetAllSchoolsNotActive(ByVal SchoolType As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Select Case SchoolType
            Case "College"
                With sb
                    .Append(" Select SG.CollegeId,SG.CollegeName ")
                    .Append(" from adColleges SG,syStatuses ST ")
                    .Append(" where SG.StatusId = ST.StatusId  ")
                    .Append(" Order By SG.CollegeName ")
                End With
            Case "Schools"
                With sb
                    .Append(" Select SG.HsId as CollegeId,SG.HsName as CollegeName")
                    .Append(" from syInstitutions SG,syStatuses ST ")
                    .Append(" where SG.StatusId = ST.StatusId  ")
                    .Append(" Order By SG.HsName ")
                End With
        End Select
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllStudentSchools(ByVal SchoolType As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Select Case SchoolType
            Case "College"
                With sb
                    .Append(" Select SG.CollegeId,SG.CollegeName ")
                    .Append(" from adColleges SG,syStatuses ST ")
                    .Append(" where SG.StatusId = ST.StatusId and ST.Status = 'Active'  ")
                    .Append(" Order By SG.CollegeName ")
                End With
            Case "Schools"
                With sb
                    .Append(" Select SG.HsId as CollegeId,SG.HsName as CollegeName")
                    .Append(" from syInstitutions SG,syStatuses ST ")
                    .Append(" where SG.StatusId = ST.StatusId and ST.Status = 'Active'  ")
                    .Append(" Order By SG.HsName ")
                End With
        End Select
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAddressBySchool(ByVal CollegeId As String, ByVal SchoolType As String) As PlacementInfo
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        Select Case SchoolType
            Case "College"
                With sb
                    .Append(" Select SG.Address1,SG.Address2, ")
                    .Append(" SG.City,SG.Zip, ")
                    .Append(" (Select StateDescrip from syStates where StateId=SG.StateId) as State ")
                    .Append(" from adColleges SG ")
                    .Append(" where SG.CollegeId=? ")
                    .Append(" Order By SG.CollegeName ")
                End With
            Case "Schools"
                With sb
                    .Append(" Select SG.Address1,SG.Address2, ")
                    .Append(" SG.City,SG.Zip, ")
                    .Append(" (Select StateDescrip from syStates where StateId=SG.StateId) as State ")
                    .Append(" from syInstitutions SG ")
                    .Append(" where SG.HsId=? ")
                    .Append(" Order By SG.HSName ")
                End With
        End Select

        db.AddParameter("@CollegeId", CollegeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        '    'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim CollegeAddress As New PlacementInfo

        While dr.Read()

            '        'set properties with data from DataReader
            With CollegeAddress
                If Not (dr("Address1") Is DBNull.Value) Then .Address1 = dr("Address1") Else .Address1 = ""
                If Not (dr("Address2") Is DBNull.Value) Then .Address2 = dr("Address2") Else .Address2 = ""
                If Not (dr("State") Is DBNull.Value) Then .State = dr("State") Else .State = ""
                If Not (dr("zip") Is DBNull.Value) Then .Zip = dr("zip") Else .Zip = ""
                If Not (dr("city") Is DBNull.Value) Then .City = dr("city") Else .City = ""
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        db.Dispose()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return CollegeAddress

    End Function
    Public Function CheckStudentEducation(ByVal EducationInstId As String, ByVal SchoolType As String) As String
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'build the sql query
        Dim sb As New StringBuilder
        Select Case SchoolType
            Case "College"
                With sb
                    .Append(" Select SG.CollegeId,SG.CollegeName,ST.Status as Status ")
                    .Append(" from adColleges SG,syStatuses ST ")
                    .Append(" where SG.StatusId = ST.StatusId and SG.CollegeId = ? ")
                    .Append(" Order By SG.CollegeName ")
                End With
            Case "Schools"
                With sb
                    .Append(" Select SG.HsId as CollegeId,SG.HsName as CollegeName,ST.Status as Status ")
                    .Append(" from syInstitutions SG,syStatuses ST ")
                    .Append(" where SG.StatusId = ST.StatusId and SG.HsId = ? ")
                    .Append(" Order By SG.HsName ")
                End With
        End Select

        'Add the EmployerContactId the parameter list
        db.AddParameter("@EducationInstId", EducationInstId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim studentinfo As New PlacementInfo
        While dr.Read()
            'set properties with data from DataReader
            With studentinfo
                .Reason = dr("Status")
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        If studentinfo.Reason = "Active" Then
            Return "Active"
        Else
            Return "Inactive"
        End If
    End Function
    Public Function GetStartDate(ByVal StudentId As String, ByVal PrgVerId As String) As String
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim strStartDate As String
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" Select StartDate ")
            .Append(" from arStuEnrollments ")
            .Append(" where StuEnrollId=? ")
        End With

        'Add the EmployerContactId the parameter list
        'db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim studentinfo As New PlacementInfo
        While dr.Read()
            'set properties with data from DataReader
            With studentinfo
                strStartDate = dr("StartDate")
            End With
        End While

        If Not dr.IsClosed Then dr.Close()

        Return strStartDate
    End Function
    'Public Function AddStudentEducation(ByVal educationinfo As PlacementInfo, ByVal user As String, ByVal campusId As String) As String
    '    ''Connect To The Database
    '    Dim db As New DataAccess
    '    Dim commonvalues As New AdvantageCommonValues

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    'Do an Update
    '    Try

    '        Dim strEducationInstId As String = Guid.NewGuid.ToString
    '        Dim strStatusId As String = AdvantageCommonValues.ActiveGuid
    '        Dim dsGetCmpGrps As New DataSet
    '        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusId)
    '        Dim strCampGrpId As String = ""
    '        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
    '            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
    '                strCampGrpId = row("CampGrpId").ToString
    '                Exit For
    '            Next
    '        End If

    '        If Not educationinfo.OtherCollegeNotListed = "" And educationinfo.EducationInstType = "College" Then
    '            'insert into adColleges
    '            Dim sb1 As New StringBuilder
    '            With sb1
    '                .Append("Insert adColleges(CollegeId,CollegeCode,StatusId,CollegeName,ModUser,ModDate,CampGrpId) ")
    '                .Append(" Values(?,?,?,?,?,?,?) ")
    '            End With

    '            'CollegeId
    '            db.AddParameter("@CollegeId", strEducationInstId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '            ' CollegeCode
    '            If educationinfo.OtherCollegeNotListed.ToString.Length < 7 Then
    '                db.AddParameter("@CollegeCode", educationinfo.OtherCollegeNotListed.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            Else
    '                db.AddParameter("@CollegeCode", educationinfo.OtherCollegeNotListed.ToString.Substring(0, 7), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            End If

    '            'Mid(1, educationinfo.OtherCollegeNotListed.ToString, 8)
    '            ' StatusId
    '            db.AddParameter("@StatusId", strStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '            'EducationInstType
    '            db.AddParameter("@CollegeName", educationinfo.OtherCollegeNotListed, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '            ''ModUser
    '            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '            ''ModDate
    '            db.AddParameter("@ModDate", educationinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '            If strCampGrpId = "" Then
    '                db.AddParameter("@CampGrpId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            Else
    '                db.AddParameter("@CampGrpId", strCampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            End If

    '            ''Execute The Query
    '            db.RunParamSQLExecuteNoneQuery(sb1.ToString)
    '            db.ClearParameters()
    '            sb1.Remove(0, sb1.Length)

    '        ElseIf Not educationinfo.OtherCollegeNotListed = "" And educationinfo.EducationInstType = "Schools" Then
    '            'insert into syInstitutions
    '            Dim sb2 As New StringBuilder
    '            With sb2
    '                .Append("Insert syInstitutions(HSId,HSCode,StatusId,HSName,ModUser,ModDate,CampGrpId) ")
    '                .Append(" Values(?,?,?,?,?,?,?) ")
    '            End With

    '            'CollegeId
    '            db.AddParameter("@HSId", strEducationInstId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '            ' HSCode
    '            If educationinfo.OtherCollegeNotListed.ToString.Length < 7 Then
    '                db.AddParameter("@HSCode", educationinfo.OtherCollegeNotListed.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            Else
    '                db.AddParameter("@HSCode", educationinfo.OtherCollegeNotListed.ToString.Substring(0, 7), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            End If

    '            ' StatusId
    '            db.AddParameter("@StatusId", strStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '            'EducationInstType
    '            db.AddParameter("@HSName", educationinfo.OtherCollegeNotListed, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '            ''ModUser
    '            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '            ''ModDate
    '            db.AddParameter("@ModDate", educationinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '            If strCampGrpId = "" Then
    '                db.AddParameter("@CampGrpId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            Else
    '                db.AddParameter("@CampGrpId", strCampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            End If


    '            ''Execute The Query
    '            db.RunParamSQLExecuteNoneQuery(sb2.ToString)
    '            db.ClearParameters()
    '            sb2.Remove(0, sb2.Length)
    '        End If

    '        'Build The Query
    '        Dim sb As New StringBuilder
    '        With sb
    '            .Append("Insert plStudentEducation(StuEducationId,StudentId,EducationInstId, ")
    '            .Append(" EducationInstType, ")
    '            .Append(" GraduatedDate,FinalGrade,CertificateId,Comments,ModUser,ModDate,Major) ")
    '            .Append(" Values(?,?,?,?,?,?,?,?,?,?,?) ")
    '        End With

    '        '    'Add Parameters To Query

    '        'Primary Key Value
    '        db.AddParameter("@StuEducationId", educationinfo.StEmploymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        '   StudentId
    '        db.AddParameter("@StudentId", educationinfo.StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        ' EducationInstId
    '        If Not educationinfo.OtherCollegeNotListed = "" Then
    '            db.AddParameter("@EducationInstId", strEducationInstId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@EducationInstId", educationinfo.EducationInstId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If


    '        'EducationInstType
    '        If educationinfo.EducationInstType = "" Then
    '            db.AddParameter("@EducationInstType", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@EducationInstType", educationinfo.EducationInstType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'Graduated Date
    '        If educationinfo.GraduationDate = "" Then
    '            db.AddParameter("@GraduatedDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@GraduatedDate", educationinfo.GraduationDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'FinalGrade
    '        If educationinfo.FinalGrade = "" Then
    '            db.AddParameter("@FinalGrade", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@FinalGrade", educationinfo.FinalGrade, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'Certificate
    '        If educationinfo.Certificate = Guid.Empty.ToString Or educationinfo.Certificate = "" Then
    '            db.AddParameter("@CertificateId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@CertificateId", educationinfo.Certificate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'FinalGrade
    '        db.AddParameter("@Comments", educationinfo.Comments, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)

    '        ''ModUser
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        ''ModDate
    '        db.AddParameter("@ModDate", educationinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        'Major
    '        If educationinfo.Major = "" Then
    '            db.AddParameter("@Major", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Major", educationinfo.Major, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        ''Execute The Query
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)

    '        educationinfo.IsInDb = True

    '        'Retun Without Errors
    '        Return ""
    '    Catch ex As OleDbException
    '        'Return an Error To Client
    '        Return DALExceptions.BuildErrorMessage(ex)
    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function
    'Public Function UpdateStudentEducation(ByVal EducationInfo As PlacementInfo, ByVal User As String, ByVal Major As String) As String

    '    'Connect To The Database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    'Do an Update
    '    Try
    '        'Build The Query
    '        Dim sb As New StringBuilder
    '        With sb
    '            .Append("Update plStudentEducation set  ")
    '            .Append(" EducationInstType=?, EducationInstId=?, ")
    '            .Append(" GraduatedDate=?,FinalGrade=?,CertificateId=?,Comments=?,ModUser=?,ModDate=?,Major=? ")
    '            .Append(" Where StuEducationID = ?")
    '        End With

    '        'EducationInstType
    '        If EducationInfo.EducationInstType = "" Then
    '            db.AddParameter("@EducationInstType", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@EducationInstType", EducationInfo.EducationInstType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'EducationInstId
    '        If EducationInfo.EducationInstId = "" Or EducationInfo.EducationInstId = Guid.Empty.ToString Then
    '            db.AddParameter("@EducationInstId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@EducationInstid", EducationInfo.EducationInstId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'Graduated Date
    '        If EducationInfo.GraduationDate = "" Then
    '            db.AddParameter("@GraduatedDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@GraduatedDate", EducationInfo.GraduationDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        End If

    '        'FinalGrade
    '        If EducationInfo.FinalGrade = "" Then
    '            db.AddParameter("@FinalGrade", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@FinalGrade", EducationInfo.FinalGrade, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'Certificate
    '        If EducationInfo.Certificate = Guid.Empty.ToString Or EducationInfo.Certificate = "" Then
    '            db.AddParameter("@CertificateId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@CertificateId", EducationInfo.Certificate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If

    '        'Comments
    '        'FinalGrade
    '        db.AddParameter("@Comments", EducationInfo.Comments, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)

    '        ''ModUser
    '        db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        ''ModDate
    '        db.AddParameter("@ModDate", EducationInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        'Major
    '        If EducationInfo.Major = "" Then
    '            db.AddParameter("@Major", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        Else
    '            db.AddParameter("@Major", EducationInfo.Major, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        End If


    '        ' EducationInstId
    '        db.AddParameter("@StuEducationId", EducationInfo.StEmploymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


    '        'Execute The Query
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '        EducationInfo.IsInDb = True
    '        Return ""
    '    Catch ex As Exception
    '        Return DALExceptions.BuildErrorMessage(ex)
    '    Finally
    '        'Close Connection
    '        ' db.CloseConnection()
    '    End Try
    'End Function
    Public Function GetStudentInfo(ByVal StudentId As String, ByVal EducationInstId As String) As PlacementInfo
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT     B.EducationInstId, ")
            .Append("    B.GraduatedDate, ")
            .Append("    B.FinalGrade, ")
            .Append("    B.CertificateId, ")
            .Append("    (Select DegreeDescrip from arDegrees where DegreeId=B.CertificateId) As DegreeDescrip, ")
            .Append(" B.Comments, ")
            .Append(" B.major,B.ModDate, ")
            .Append("    B.EducationInstId, ")
            .Append("   Coalesce((Select CollegeName from adColleges where CollegeId=B.EducationInstId), (Select HSName from syInstitutions where HSID=B.EducationInstId)) As EducationInst ")
            .Append("FROM  plStudentEducation B ")
            .Append("Where StuEducationId = ? ")
        End With

        db.AddParameter("@EducationInstID", EducationInstId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim studentinfo As New PlacementInfo
        While dr.Read()

            'set properties with data from DataReader
            With studentinfo
                .IsInDb = True
                .StudentId = StudentId
                If Not (dr("EducationInstId") Is DBNull.Value) Then .EducationInstId = CType(dr("EducationInstId"), Guid).ToString
                If Not (dr("EducationInst") Is DBNull.Value) Then .EducationInst = dr("EducationInst")

                'Get Graduated Date
                If Not (dr("GraduatedDate") Is DBNull.Value) Then .GraduationDate = dr("GraduatedDate") Else .GraduationDate = ""

                'Get FinalGrade
                If Not (dr("FinalGrade") Is DBNull.Value) Then .FinalGrade = dr("FinalGrade") Else .FinalGrade = ""

                'Get Comments
                If Not (dr("Comments") Is DBNull.Value) Then .Comments = dr("Comments") Else .Comments = ""

                'Major
                If Not (dr("Major") Is DBNull.Value) Then .Major = dr("Major") Else .Major = ""

                'Get Certificate
                If Not (dr("CertificateId") Is DBNull.Value) Then .Certificate = CType(dr("CertificateId"), Guid).ToString
                If Not (dr("DegreeDescrip") Is DBNull.Value) Then .CertificateDescrip = dr("DegreeDescrip")

                .ModDate = dr("ModDate")
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return studentinfo
    End Function
    Public Function GetEducationInstAddress(ByVal EducationInstId As String, ByVal EducationInstType As String) As PlacementInfo
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder

        Try
            'build the sql query
            If EducationInstType = "College" Then
                With sb
                    .Append(" select A.Address1,A.Address2,A.City,A.Zip, ")
                    .Append(" (select StateDescrip from syStates where StateId=A.StateId) as StateDescrip ")
                    .Append(" from adColleges A where CollegeId=? ")
                End With
                db.AddParameter("@CollegeId", EducationInstId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                Dim studentinfo As New PlacementInfo
                While dr.Read()

                    'set properties with data from DataReader
                    With studentinfo

                        'Get Address1
                        If Not (dr("Address1") Is DBNull.Value) Then .Address1 = dr("Address1") Else .Address1 = ""

                        'Get Address2
                        If Not (dr("Address2") Is DBNull.Value) Then .Address2 = dr("Address2") Else .Address2 = ""

                        'Get City
                        If Not (dr("City") Is DBNull.Value) Then .City = dr("City") Else .City = ""

                        'Get State
                        If Not (dr("StateDescrip") Is DBNull.Value) Then .State = dr("StateDescrip") Else .State = ""

                        'Get Zip
                        If Not (dr("Zip") Is DBNull.Value) Then .Zip = dr("Zip") Else .Zip = ""
                    End With
                End While

                If Not dr.IsClosed Then dr.Close()

                Return studentinfo
            Else
                With sb
                    .Append(" select A.Address1,A.Address2,A.City,A.Zip, ")
                    .Append(" (select StateDescrip from syStates where StateId=A.StateId) as StateDescrip ")
                    .Append(" from syInstitutions A where HSId=? ")
                End With
                db.AddParameter("@HSId", EducationInstId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                Dim studentinfo As New PlacementInfo
                While dr.Read()

                    'set properties with data from DataReader
                    With studentinfo

                        'Get Address1
                        If Not (dr("Address1") Is DBNull.Value) Then .Address1 = dr("Address1") Else .Address1 = ""

                        'Get Address2
                        If Not (dr("Address2") Is DBNull.Value) Then .Address2 = dr("Address2") Else .Address2 = ""

                        'Get City
                        If Not (dr("City") Is DBNull.Value) Then .City = dr("City") Else .City = ""

                        'Get State
                        If Not (dr("StateDescrip") Is DBNull.Value) Then .State = dr("StateDescrip") Else .State = ""

                        'Get Zip
                        If Not (dr("Zip") Is DBNull.Value) Then .Zip = dr("Zip") Else .Zip = ""
                    End With
                End While

                If Not dr.IsClosed Then dr.Close()

                Return studentinfo
            End If


        Finally
            db.CloseConnection()
        End Try

    End Function
    Public Function GetPlacementDetails(ByVal PlacementId As String) As PlacementInfo
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT  ")
            .Append(" B.PlacementId, ")
            .Append(" B.EmployerJobId, ")
            .Append(" (select EmployerJobTitle from plEmployerJobs where EmployerJobId=B.EmployerJobId) As EmployerJobTitle, ")
            .Append(" (select distinct EmployerId from plEmployerJobs where EmployerJobId=B.EmployerJobId) as EmployerId, ")
            .Append(" (select EmployerDescrip from plEmployers where EmployerId=(Select distinct EmployerId from plEmployerJobs where EmployerJobId=B.EmployerJobId)) as Employer, ")
            .Append(" B.JobDescrip, ")
            .Append(" B.PlacementRep, ")
            .Append(" B.JobStatusId, ")
            .Append(" (Select JobStatusDescrip from plJobStatus where JobStatusId=B.JobStatusId) As JobStatus, ")
            .Append(" B.Supervisor, ")
            .Append(" B.PlacedDate, ")
            .Append(" B.StartDate, ")
            .Append(" B.Fee As FeeId, ")
            .Append(" (Select FeeDescrip from plFee where FeeId=B.Fee) As Fee, ")
            .Append(" B.Reason, ")
            .Append(" B.Salary, ")
            .Append(" B.SalaryTypeId, ")
            .Append(" (Select SalaryTypeDescrip from plSalaryType where SalaryTypeId=B.SalaryTypeId) As SalaryType, ")
            .Append(" B.ScheduleId, ")
            .Append(" (Select JobScheduleDescrip from plJobSchedule where JobScheduleId=B.ScheduleId) As Schedule, ")
            .Append(" B.BenefitsId, ")
            .Append(" (Select JobBenefitDescrip from plJobBenefit where JobBenefitId=B.BenefitsId) As Benefits, ")
            .Append(" B.InterviewId, ")
            .Append(" (Select InterviewDescrip from plInterview where InterviewId=B.InterviewId) As Interview, ")
            .Append(" B.FldStudyId, ")
            .Append(" (Select FldStudyDescrip from plFldStudy where FldStudyId=B.FldStudyId) As FldStudy, ")
            .Append(" B.HowPlacedId, ")
            .Append(" (Select HowPlacedDescrip from plHowPlaced where HowPlacedId=B.HowPlacedId) As HowPlaced, ")
            .Append(" B.TerminationDate, ")
            .Append(" B.TerminationReason, ")
            .Append(" B.StuEnrollId ")
            .Append("FROM  plStudentsPlaced B ")
            .Append(" where B.PlacementId = ? ")
        End With

        'Add the PlacementId the parameter list
        db.AddParameter("@PlacementId", PlacementId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim studentinfo As New PlacementInfo
        While dr.Read()

            'set properties with data from DataReader
            With studentinfo
                .IsInDb = True
                If Not (dr("Salary") Is DBNull.Value) Then .Salary = dr("Salary") Else .Salary = ""
                If Not (dr("JobDescrip") Is DBNull.Value) Then .Description = dr("JobDescrip") Else .Description = ""
                If Not (dr("EmployerId") Is DBNull.Value) Then .EmployerId = CType(dr("EmployerId"), Guid).ToString
                If Not (dr("Employer") Is DBNull.Value) Then .EmployerName = dr("Employer")
                If Not (dr("PlacementRep") Is DBNull.Value) Then .PlacementRep = CType(dr("PlacementRep"), Guid).ToString Else .PlacementRep = ""
                If Not (dr("Supervisor") Is DBNull.Value) Then .Supervisor = dr("Supervisor") Else .Supervisor = ""
                If Not (dr("Placeddate") Is DBNull.Value) Then .PlacedDate = dr("PlacedDate") Else .PlacedDate = ""
                If Not (dr("StartDate") Is DBNull.Value) Then .StartDate = dr("StartDate") Else .StartDate = ""
                If Not (dr("Reason") Is DBNull.Value) Then .Comments = dr("Reason") Else .Comments = ""
                If Not (dr("TerminationDate") Is DBNull.Value) Then .AvailableDate = CType(dr("TerminationDate"), String).ToString Else .AvailableDate = ""
                If Not (dr("TerminationReason") Is DBNull.Value) Then .TerminationReason = dr("TerminationReason") Else .TerminationReason = ""
                If Not (dr("HowPlacedId") Is DBNull.Value) Then .HowPlacedId = CType(dr("HowPlacedId"), Guid).ToString
                If Not (dr("HowPlaced") Is DBNull.Value) Then .HowPlaced = dr("HowPlaced")
                If Not (dr("EmployerJobId") Is DBNull.Value) Then .EmployerJobTitleId = CType(dr("EmployerJobId"), Guid).ToString
                If Not (dr("EmployerJobTitle") Is DBNull.Value) Then .EmployerJobTitle = dr("EmployerJobTitle")
                If Not (dr("JobStatusId") Is DBNull.Value) Then .JobStatusId = CType(dr("JobStatusId"), Guid).ToString
                If Not (dr("JobStatus") Is DBNull.Value) Then .JobStatus = dr("JobStatus")
                If Not (dr("FeeId") Is DBNull.Value) Then .FeeId = CType(dr("FeeId"), Guid).ToString
                If Not (dr("Fee") Is DBNull.Value) Then .Fee = dr("fee")
                If Not (dr("SalaryTypeId") Is DBNull.Value) Then .SalaryTypeId = CType(dr("SalaryTypeId"), Guid).ToString
                If Not (dr("SalaryType") Is DBNull.Value) Then .SalaryType = dr("SalaryType")
                If Not (dr("ScheduleId") Is DBNull.Value) Then .ScheduleId = CType(dr("ScheduleId"), Guid).ToString
                If Not (dr("Schedule") Is DBNull.Value) Then .Schedule = dr("Schedule")
                If Not (dr("BenefitsId") Is DBNull.Value) Then .BenefitsId = CType(dr("BenefitsId"), Guid).ToString
                If Not (dr("Benefits") Is DBNull.Value) Then .Benefits = dr("Benefits")
                If Not (dr("InterviewId") Is DBNull.Value) Then .InterviewId = CType(dr("InterviewId"), Guid).ToString
                If Not (dr("Interview") Is DBNull.Value) Then .Interview = dr("Interview")
                If Not (dr("FldStudyId") Is DBNull.Value) Then .FldStudyId = CType(dr("FldStudyId"), Guid).ToString
                If Not (dr("FldStudy") Is DBNull.Value) Then .FldStudy = dr("FldStudy")
                If Not (dr("StuEnrollId") Is DBNull.Value) Then .EnrollmentId = CType(dr("StuEnrollId"), Guid).ToString Else .EnrollmentId = ""
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return studentinfo
    End Function

    Public Function GetCollegeNames(ByVal StudentId As String, ByVal EducationInstType As String)
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Select Case EducationInstType
            Case "Schools"
                With sb
                    .Append(" select HsId as CollegeId,HsName  as CollegeName from syInstitutions  where HsId in ")
                    .Append(" (Select EducationInstId from plStudentEducation where ")
                    .Append(" Studentid=? and EducationInstType = ?) ")
                    .Append(" Order By HsName ")
                End With
            Case Else
                With sb
                    .Append(" select CollegeId,CollegeName from adColleges  where CollegeId in ")
                    .Append(" (Select EducationInstId from plStudentEducation where ")
                    .Append(" Studentid=? and EducationInstType = ?) ")
                    .Append(" Order By CollegeName ")
                End With
        End Select
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@EducationInstType", EducationInstType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetCollegeNamesByID(ByVal EducationInstType As String, ByVal StudentID As String)
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Select Case EducationInstType
            Case "Schools"
                With sb
                    .Append(" SELECT stuEducationID,(select HSName from syInstitutions where HsId = EducationInstID and  ")
                    .Append(" EducationInstType = ? ) as CollegeName ")
                    .Append(" FROM  plStudentEducation where EducationInstType= ? and StudentID= ? ")
                    .Append(" Order By CollegeName ")
                End With
            Case Else
                With sb
                    '.Append(" SELECT stuEducationID,(select collegename from adColleges where collegeid = EducationInstID and  ")
                    '.Append(" EducationInstType = ? ) as CollegeName ")
                    '.Append(" FROM  plStudentEducation where EducationInstType= ? ")
                    '.Append(" Order By CollegeName ")
                    .Append(" SELECT stuEducationID,(select collegename from adColleges where collegeid = EducationInstID and  ")
                    .Append(" EducationInstType = ? ) as CollegeName ")
                    .Append(" FROM  plStudentEducation where EducationInstType= ? and StudentID = ? ")
                    .Append(" Order By CollegeName ")
                End With
        End Select
        db.AddParameter("@EducationInstType", EducationInstType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@EducationInstType", EducationInstType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@StudentID", StudentID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetEmployerNames(ByVal StudentId As String, ByVal showActiveOnly As String)
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb

            .Append(" select B.EMployerName  as EmployerDescrip,B.StEmploymentId as StEmploymentId,B.EmployerJobTitle from plStudentEmployment B ")
            .Append(" where B.StudentId=?  ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetLeadEmployerNames(ByVal LeadId As String)
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            '.Append(" select A.EmployerDescrip as EmployerDescrip,B.StEmploymentId as StEmploymentId from plEmployers A,adLeadEmployment B ")
            '.Append(" where A.EmployerId = B.EmployerId and B.LeadID =? ")
            .Append(" select B.EmployerName as EmployerDescrip,B.StEmploymentId as StEmploymentId,EmployerJobTitle from adLeadEmployment B where B.LeadId=? ")
        End With

        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ''return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetEducationID(ByVal PrimaryId As String) As String
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb

            .Append(" select EducationInstID from plStudentEducation ")
            .Append(" where StuEducationID = ? ")
        End With

        db.AddParameter("@StuEducationId", PrimaryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '    'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim strEducationInstID As String
        While dr.Read()
            strEducationInstID = dr("EducationInstID").ToString
        End While

        If Not dr.IsClosed Then dr.Close()

        Return strEducationInstID
    End Function
    Public Function GetLeadEducationID(ByVal PrimaryId As String) As String
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb

            .Append(" select EducationInstID from adLeadEducation ")
            .Append(" where LeadEducationID = ? ")
        End With

        db.AddParameter("@LeadEducationId", PrimaryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '    'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim strEducationInstID As String
        While dr.Read()
            strEducationInstID = dr("EducationInstID").ToString
        End While

        If Not dr.IsClosed Then dr.Close()

        Return strEducationInstID
    End Function
    Public Function GetEmployerNamesByEnrollment(ByVal StudentId As String, ByVal EnrollmentId As String)
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        With sb
            '.Append(" select EmployerDescrip,B.PlacementId from plEmployers A,plStudentsPlaced B,plEmployerJobs C where  ")
            '.Append(" B.EmployerJobId = C.EmployerJobId and C.EmployerId = A.EmployerId and  B.StudentId = ? ")

            .Append(" select Distinct EmployerDescrip,B.PlacementId,B.StuEnrollId,C.EmployerId,C.EmployerJobTitle ")
            .Append(" from ")
            .Append(" plEmployers A,plStudentsPlaced B,plEmployerJobs C,arStuEnrollments D ")
            .Append(" where ")
            .Append(" B.EmployerJobId = C.EmployerJobId and C.EmployerId = A.EmployerId and  ")
            .Append(" B.StuEnrollId = D.StuEnrollId and D.StudentId = ? ")
            If EnrollmentId <> "All" Then
                .Append(" and D.StuEnrollId = ?  ")
            End If
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        If EnrollmentId <> "All" Then
            db.AddParameter("@EnrollmentId", EnrollmentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        ''return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    'Public Function GetStudentExtracurr(ByVal StudentId As String, ByVal CollegeId As String) As DataSet
    '    'get the connection to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder

    '    'build the sql query
    '    With sb
    '        .Append("SELECT StudentId, ExtraCurrId ")
    '        .Append("FROM   plStEduExtracurricular ")
    '        .Append("WHERE  StudentId = ? and EducationInstId = ? ")
    '        .Append("and  ExtracurrId is Not Null")
    '    End With

    '    'Add the parameter
    '    db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@CollegeId", CollegeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    'Return the dataset
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
    'Public Function GetValidExtracurr(ByVal StudentId As String, ByVal CollegeId As String) As Integer
    '    'get the connection to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder

    '    'build the sql query
    '    With sb
    '        .Append("SELECT Count(*) ")
    '        .Append("FROM   plStEduExtracurricular ")
    '        .Append("WHERE  StudentId = ? and EducationInstId = ? ")
    '        .Append("and  ExtracurrId is Not Null")
    '    End With

    '    'Add the parameter
    '    db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@CollegeId", CollegeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    Dim JobCount As Integer
    '    'Return the dataset
    '    JobCount = db.RunParamSQLScalar(sb.ToString)
    '    Return JobCount
    'End Function
    'Public Function DeleteStudentEducation(ByVal StuEducationId As String) As String
    '    'Connect to the database
    '    Dim db As New DataAccess

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

    '    'Do an insert
    '    Try
    '        'Build the query
    '        Dim sb As New StringBuilder
    '        With sb
    '            .Append("DELETE FROM plStudentEducation ")
    '            .Append("WHERE StuEducationId = ? ")
    '        End With

    '        'Add parameters values to the query
    '        'StuEducationId
    '        db.AddParameter("@StuEducationId", StuEducationId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '        'execute the query
    '        db.RunParamSQLExecuteNoneQuery(sb.ToString)

    '        'return without errors
    '        Return ""
    '    Catch ex As OleDbException
    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)
    '    Finally
    '        'Close Connection
    '        db.CloseConnection()
    '    End Try
    'End Function
    Public Function DeleteLeadEducation(ByVal StuEducationId As String, ByVal ModDate As DateTime) As String
        'Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'Do an insert
        Try
            'Build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM adLeadEducation ")
                .Append("WHERE LeadEducationId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM adLeadEducation WHERE LeadEducationId = ? ")
            End With

            'Primary Key Field
            db.AddParameter("@StuEducationId", StuEducationId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'Primary Key Field
            db.AddParameter("@StuEducationId", StuEducationId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            'execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            'If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function DeleteStudentEmployment(ByVal StudentId As String) As String
        'Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an insert
        Try
            'Build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM plStudentEmployment ")
                .Append("WHERE StEmploymentId=? ")
            End With

            'Add parameters values to the query
            'BankId
            db.AddParameter("@StEmploymentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            'return without errors
            Return ""
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function DeleteLeadEmployment(ByVal LeadId As String, ByVal moddate As DateTime) As String
        'Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an insert
        Try
            'Build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM adLeadEmployment ")
                .Append("WHERE StEmploymentId=? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM adLeadEmployment WHERE StEmploymentId = ? ")
            End With

            'Add parameters values to the query
            'Primary Key
            db.AddParameter("@StEmploymentId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", moddate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'Primary Key
            db.AddParameter("@StEmploymentId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            'execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            'If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAllEmployers() As DataSet
        'Function To Get All Employers
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select EG.EmployerId,EG.EmployerDescrip ")
            .Append(" from plEmployers EG,syStatuses ST ")
            .Append(" where EG.StatusId = ST.StatusId and ST.Status='Active' ")
            .Append(" Order By EG.EmployerDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllJobStatus() As DataSet
        'Function To Get All Employers
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select JS.JobStatusId,JS.JobStatusDescrip ")
            .Append(" from plJobStatus JS ,syStatuses ST ")
            .Append(" where JS.StatusId = ST.StatusId and ST.Status='Active' ")
            .Append(" Order By JS.JobStatusDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllJobTitles() As DataSet
        'Function To Get All Employers
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select JS.TitleId as JobTitleId,JS.TitleDescrip as JobTitleDescrip ")
            .Append(" from adTitles JS,syStatuses ST ")
            .Append(" where JS.StatusId = ST.StatusId ")
            .Append(" and ST.Status = 'Active' ")
            .Append(" Order By JS.TitleDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllJobTitlesByEmployer(ByVal EmployerId As String) As DataSet
        'Function To Get All Employers
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select JS.TitleId as JobTitleId,JS.TitleDescrip as JobTitleDescrip ")
            .Append(" from adTitles JS,syStatuses ST,plEmployerJobs EJ ")
            .Append(" where JS.StatusId = ST.StatusId and JS.TitleId = EJ.JobTitleId ")
            .Append(" and ST.Status = 'Active' and EJ.EmployerId = ? ")
            .Append(" Order By JS.TitleDescrip ")
        End With

        db.AddParameter("@EmployerId", EmployerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetAddressByEmployer(ByVal EmployerId As String) As PlacementInfo
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select SG.Address1,SG.Address2, ")
            .Append(" SG.City,SG.Zip, ")
            .Append(" (Select StateDescrip from syStates where StateId=SG.StateId) as State ")
            .Append(" from plEmployers SG ")
            .Append(" where SG.EmployerId=? ")
        End With

        db.AddParameter("@EmployerId", EmployerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        '    'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim EmployerAddress As New PlacementInfo

        While dr.Read()

            'set properties with data from DataReader
            With EmployerAddress
                If Not (dr("Address1") Is DBNull.Value) Then .Address1 = dr("Address1")
                If Not (dr("Address2") Is DBNull.Value) Then .Address2 = dr("Address2")
                If Not (dr("State") Is DBNull.Value) Then .State = dr("State")
                If Not (dr("Zip") Is DBNull.Value) Then .Zip = dr("Zip")
                If Not (dr("City") Is DBNull.Value) Then .City = dr("City")
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        db.Dispose()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return EmployerAddress
    End Function
    Public Function AddStudentEmployment(ByVal educationinfo As PlacementInfo, ByVal user As String) As String

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            ''Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Insert plStudentEmployment(StEmploymentID,StudentId,EmployerName,EmployerJobTitle, ")
                .Append(" StartDate,EndDate,JobTitleId,JobStatusId,Comments,ModUser,ModDate,JobResponsibilities) ")
                .Append(" Values(?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            'StEmploymentID
            db.AddParameter("@StEmploymentId", educationinfo.StEmploymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StudentId
            db.AddParameter("@StudentId", educationinfo.StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Employer Name
            If educationinfo.EmployerName = "" Then
                db.AddParameter("@EmployerName", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@EmployerName", educationinfo.EmployerName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Employer Job Title
            If educationinfo.EmployerJobTitle = "" Then
                db.AddParameter("@EmployerJobTitle", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@EmployerJobTitle", educationinfo.EmployerJobTitle, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Start Date
            If educationinfo.StartDate = "" Then
                db.AddParameter("@StartDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@StartDate", educationinfo.StartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'To Date
            If educationinfo.EndDate = "" Then
                db.AddParameter("@ToDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@ToDate", educationinfo.EndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'JobTitleId
            If educationinfo.JobTitleId = Guid.Empty.ToString Or educationinfo.JobTitleId = "" Then
                db.AddParameter("@JobTitleId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@JobTitleId", educationinfo.JobTitleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'JobStatusId
            If educationinfo.JobStatus = Guid.Empty.ToString Or educationinfo.JobStatus = "" Then
                db.AddParameter("@JobStatusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@JobStatusId", educationinfo.JobStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Comments
            If educationinfo.Comments = "" Then
                db.AddParameter("@Comments", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@Comments", educationinfo.Comments, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If


            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", educationinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'JobResponsibilities
            If educationinfo.JobResponsibilities = "" Then
                db.AddParameter("@JobResponsibilities", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@JobResponsibilities", educationinfo.JobResponsibilities, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If

            ''Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            educationinfo.IsInDb = True

            '    'Retun Without Errors
            '    Return 0
            'Catch ex As System.Exception
            '    'Return an Error To Client
            '    Return -1
            'Finally
            '    'Close Connection
            '    db.CloseConnection()
            'End Try
            Return ""
            'Catch ex As System.Exception
            '    'Return an Error To Client
            '    Return -1
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateStudentEmployment(ByVal educationinfo As PlacementInfo, ByVal user As String, ByVal PkValue As String) As String

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            '    'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Update plStudentEmployment set ")
                .Append(" StartDate=?,EndDate=?,JobTitleId=?,JobStatusId=?,Comments=?,ModUser=?,ModDate=?,JobResponsibilities=?,EmployerName=?,EmployerJobTitle=? ")
                .Append(" where StEmploymentId = ? ")
            End With

            'Start Date
            If educationinfo.StartDate = "" Then
                db.AddParameter("@StartDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@StartDate", educationinfo.StartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'To Date
            If educationinfo.EndDate = "" Then
                db.AddParameter("@ToDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@ToDate", educationinfo.EndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'JobTitleId
            If educationinfo.JobTitleId = Guid.Empty.ToString Or educationinfo.JobTitleId = "" Then
                db.AddParameter("@JobTitleId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@JobTitleId", educationinfo.JobTitleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'JobStatusId
            If educationinfo.JobStatus = Guid.Empty.ToString Or educationinfo.JobStatus = "" Then
                db.AddParameter("@JobStatusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@JobStatusId", educationinfo.JobStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Comments
            If educationinfo.Comments = Guid.Empty.ToString Or educationinfo.Comments = "" Then
                db.AddParameter("@Comments", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@Comments", educationinfo.Comments, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If


            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", educationinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'JobResponsibilities
            If educationinfo.JobResponsibilities = "" Then
                db.AddParameter("@JobResponsibilities", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@JobResponsibilities", educationinfo.JobResponsibilities, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If

            'Employer Name
            If educationinfo.EmployerName = "" Then
                db.AddParameter("@EmployerName", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@EmployerName", educationinfo.EmployerName, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If

            'Employer JobTitle
            If educationinfo.EmployerJobTitle = "" Then
                db.AddParameter("@EmployerJobTitle", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@EmployerJobTitle", educationinfo.EmployerJobTitle, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If

            'StEmploymentId 
            db.AddParameter("@StEmploymentId", PkValue, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            ''Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            educationinfo.IsInDb = True

            Return ""
            'Catch ex As System.Exception
            '    'Return an Error To Client
            '    Return -1
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function AddLeadEmployment(ByVal educationinfo As PlacementInfo, ByVal user As String) As String

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            ''Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Insert adLeadEmployment(StEmploymentID,LeadId,EmployerName, ")
                .Append(" StartDate,EndDate,JobTitleId,JobStatusId,Comments,ModUser,ModDate,JobResponsibilities,EmployerJobTitle) ")
                .Append(" Values(?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '    'Add Parameters To Query

            'StEmploymentID
            db.AddParameter("@StEmploymentId", educationinfo.StEmploymentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'LeadId
            db.AddParameter("@LeadId", educationinfo.LeadID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ' Employer Name
            If educationinfo.EmployerName = "" Then
                db.AddParameter("@EmployerName", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@EmployerName", educationinfo.EmployerName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Start Date
            If educationinfo.StartDate = "" Then
                db.AddParameter("@StartDate", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@StartDate", educationinfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            'To Date
            If educationinfo.EndDate = "" Then
                db.AddParameter("@ToDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@ToDate", educationinfo.EndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'JobTitleId
            If educationinfo.JobTitleId = Guid.Empty.ToString Or educationinfo.JobTitleId = "" Then
                db.AddParameter("@JobTitleId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@JobTitleId", educationinfo.JobTitleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'JobStatusId
            If educationinfo.JobStatus = Guid.Empty.ToString Or educationinfo.JobStatus = "" Then
                db.AddParameter("@JobStatusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@JobStatusId", educationinfo.JobStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Comments
            If educationinfo.Comments = "" Then
                db.AddParameter("@Comments", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@Comments", educationinfo.Comments, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If

            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", educationinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'JobResponsibilities
            If educationinfo.JobResponsibilities = "" Then
                db.AddParameter("@JobResponsibilities", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@JobResponsibilities", educationinfo.JobResponsibilities, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If

            ' Employer Job Title
            If educationinfo.EmployerJobTitle = "" Then
                db.AddParameter("@EmployerJobTitle", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@EmployerJobTitle", educationinfo.EmployerJobTitle, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            ''Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            educationinfo.IsInDb = True

            'Return Without Errors
            Return ""
        Catch ex As Exception
            'Return an Error To Client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateLeadEmployment(ByVal educationinfo As PlacementInfo, ByVal user As String, ByVal PkValue As String) As String

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            '    'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Update adLeadEmployment set ")
                .Append(" StartDate=?,EndDate=?,JobTitleId=?,JobStatusId=?,Comments=?,ModUser=?,ModDate=?,JobResponsibilities=?,EmployerName=?,EmployerJobTitle=? ")
                .Append(" where StEmploymentId = ? ")
            End With

            'Start Date
            If educationinfo.StartDate = "" Then
                db.AddParameter("@StartDate", DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@StartDate", educationinfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If


            'To Date
            If educationinfo.EndDate = "" Then
                db.AddParameter("@ToDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@ToDate", educationinfo.EndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'JobTitleId
            If educationinfo.JobTitleId = Guid.Empty.ToString Or educationinfo.JobTitleId = "" Then
                db.AddParameter("@JobTitleId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@JobTitleId", educationinfo.JobTitleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'JobStatusId
            If educationinfo.JobStatus = Guid.Empty.ToString Or educationinfo.JobStatus = "" Then
                db.AddParameter("@JobStatusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@JobStatusId", educationinfo.JobStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Comments
            If educationinfo.Comments = "" Then
                db.AddParameter("@Comments", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@Comments", educationinfo.Comments, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If

            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''ModDate
            db.AddParameter("@ModDate", educationinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'JobResponsibilities
            If educationinfo.JobResponsibilities = "" Then
                db.AddParameter("@JobResponsibilities", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@JobResponsibilities", educationinfo.JobResponsibilities, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If

            'Employer Name
            If educationinfo.EmployerName = "" Then
                db.AddParameter("@EmployerName", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@EmployerName", educationinfo.EmployerName, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If

            'Employer Job Title
            If educationinfo.EmployerJobTitle = "" Then
                db.AddParameter("@EmployerJobTitle", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@EmployerJobTitle", educationinfo.EmployerJobTitle, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If

            'StEmploymentId 
            db.AddParameter("@StEmploymentId", PkValue, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            ''Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            educationinfo.IsInDb = True

            'Retun Without Errors
            Return ""
        Catch ex As Exception
            'Return an Error To Client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetEmploymentInfo(ByVal StudentId As String) As PlacementInfo
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT     B.EmployerName,B.EmployerJobTitle, ")
            .Append("   B.StudentId, ")
            .Append("    B.StartDate, ")
            .Append("    B.EndDate, ")
            .Append("    B.JobTitleId, ")
            .Append(" (select TitleDescrip from adTitles where TitleId = B.JobTitleId) as JobTitleDescrip, ")
            .Append("    B.JobStatusId, ")
            .Append(" (select JobStatusDescrip from plJobStatus where JobStatusId = B.JobStatusId) as JobTitleDescrip, ")
            .Append(" B.Comments,B.JobResponsibilities,B.ModDate ")
            .Append(" FROM  plStudentEmployment B ")
            .Append(" WHERE B.StEmploymentId = ?")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@StEmploymentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim studentinfo As New PlacementInfo
        While dr.Read()

            'set properties with data from DataReader
            With studentinfo
                .IsInDb = True

                If Not (dr("StartDate") Is DBNull.Value) Then .StartDate = dr("StartDate") Else .StartDate = ""
                If Not (dr("EndDate") Is DBNull.Value) Then .EndDate = dr("EndDate").ToString Else .EndDate = ""

                'Get Comments
                If Not (dr("Comments") Is DBNull.Value) Then .Comments = dr("Comments") Else .Comments = ""

                'Get Primary Key Value from the parameter passed
                .StEmploymentId = StudentId

                'Get JobTitleId
                If Not (dr("JobTitleId") Is DBNull.Value) Then .JobTitleId = CType(dr("JobTitleId"), Guid).ToString

                'Get JobStatusId
                If Not (dr("JobStatusId") Is DBNull.Value) Then .JobStatus = CType(dr("JobStatusId"), Guid).ToString

                'Get Employer Name
                If Not (dr("EmployerName") Is DBNull.Value) Then .EmployerName = CType(dr("EmployerName"), String).ToString Else .EmployerName = ""

                'Get Employer Job Title
                If Not (dr("EmployerJobTitle") Is DBNull.Value) Then .EmployerJobTitle = CType(dr("EmployerJobTitle"), String).ToString Else .EmployerJobTitle = ""

                'Get JobResponsibilities
                If Not (dr("JobResponsibilities") Is DBNull.Value) Then .JobResponsibilities = dr("JobResponsibilities").ToString

                .ModDate = dr("ModDate")

            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return studentinfo
    End Function
    Public Function GetLeadEmploymentInfo(ByVal LeadId As String) As PlacementInfo
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT     B.EmployerName,B.EmployerJobTitle, ")
            .Append("   B.LeadId, ")
            .Append("    B.StartDate, ")
            .Append("    B.EndDate, ")
            .Append("    B.JobTitleId, ")
            .Append(" (select Distinct TitleDescrip from adTitles where TitleId = B.JobTitleId) as JobTitleDescrip, ")
            .Append("    B.JobStatusId, ")
            .Append(" (select Distinct JobStatusDescrip from plJobStatus where JobStatusId = B.JobStatusId) as JobStatusDescrip, ")
            .Append(" B.Comments,B.JobResponsibilities,B.ModDate ")
            .Append(" FROM  adLeadEmployment B ")
            .Append(" WHERE B.StEmploymentId = ?")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@StEmploymentId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim studentinfo As New PlacementInfo
        While dr.Read()

            'set properties with data from DataReader
            With studentinfo
                .IsInDb = True
                If Not (dr("StartDate") Is DBNull.Value) Then .StartDate = dr("StartDate") Else .StartDate = ""
                If Not (dr("EndDate") Is DBNull.Value) Then .EndDate = dr("EndDate") Else .EndDate = ""
                'Get Comments
                If Not (dr("Comments") Is DBNull.Value) Then .Comments = dr("Comments") Else .Comments = ""
                'Get Primary Key Value from the parameter passed
                .StEmploymentId = LeadId
                'Get JobTitleId
                If Not (dr("JobTitleId") Is DBNull.Value) Then .JobTitleId = CType(dr("JobTitleId"), Guid).ToString Else .JobTitleId = ""
                'Get JobTitle Descrip
                If Not (dr("JobTitleDescrip") Is DBNull.Value) Then .JobTitleDescrip = dr("JobTitleDescrip") Else .JobTitleDescrip = ""
                'Get JobStatusId
                If Not (dr("JobStatusId") Is DBNull.Value) Then .JobStatus = CType(dr("JobStatusId"), Guid).ToString Else .JobStatus = ""
                'Get Job Status Descrip
                If Not (dr("JobStatusDescrip") Is DBNull.Value) Then .JobStatusDescrip = dr("JobStatusDescrip") Else .JobStatusDescrip = ""
                'Get Employer Name
                If Not (dr("EmployerName") Is DBNull.Value) Then .EmployerName = dr("EmployerName").ToString Else .EmployerName = ""
                'Get Employer JobTitle
                If Not (dr("EmployerJobTitle") Is DBNull.Value) Then .EmployerJobTitle = dr("EmployerJobTitle").ToString Else .EmployerJobTitle = ""
                'Get JobResponsibilities
                If Not (dr("JobResponsibilities") Is DBNull.Value) Then .JobResponsibilities = dr("JobResponsibilities").ToString
                If Not (dr("ModDate") Is DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = ""
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return studentinfo
    End Function
    Public Function GetAllStudentAddressPhone(ByVal StudentId As String) As String
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select A.FirstName, A.LastName, ")
            .Append(" (Select Address1 from arStudAddresses where StudentId=A.StudentId and default1=1) as Address1, ")
            .Append(" (Select Address2 from arStudAddresses where StudentId=A.StudentId and default1=1) as Address2, ")
            .Append(" (Select city from arStudAddresses where StudentId=A.StudentId and default1=1) as City, ")
            .Append(" (select StateDescrip from syStates where StateId=(Select StateId from arStudAddresses where StudentId=A.StudentId and default1=1)) as State, ")
            .Append(" (Select Zip from arStudAddresses where StudentId=A.StudentId and default1=1) as Zip, ")
            .Append(" (Select CountryId from arStudAddresses where StudentId=A.StudentId and default1=1) as Country, ")
            .Append(" (Select Phone from arStudentPhone where StudentId=A.StudentId and default1=1) as HomePhone, ")
            .Append(" A.homeemail ")
            .Append(" from arStudent A where A.studentid=? ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim studentPhone As String
        While dr.Read()
            'set properties with data from DataReader
            If Not (dr("HomePhone") Is DBNull.Value) Then studentPhone = dr("HomePhone") Else studentPhone = ""
        End While

        If Not dr.IsClosed Then dr.Close()

        Return studentPhone

    End Function
    Public Function GetAllStudentAddress(ByVal StudentId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select A.FirstName, A.LastName, ")
            .Append(" (Select Address1 from arStudAddresses where StudentId=A.StudentId and default1=1) as Address1, ")
            .Append(" (Select Address2 from arStudAddresses where StudentId=A.StudentId and default1=1) as Address2, ")
            .Append(" (Select city from arStudAddresses where StudentId=A.StudentId and default1=1) as City, ")
            .Append(" (select StateDescrip from syStates where StateId=(Select StateId from arStudAddresses where StudentId=A.StudentId and default1=1)) as State, ")
            .Append(" (Select Zip from arStudAddresses where StudentId=A.StudentId and default1=1) as Zip, ")
            .Append(" (Select CountryId from arStudAddresses where StudentId=A.StudentId and default1=1) as Country, ")
            .Append(" (Select Phone from arStudentPhone where StudentId=A.StudentId and default1=1) as HomePhone, ")
            .Append(" A.homeemail ")
            .Append(" from arStudent A where A.studentid=? ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString())

    End Function
    Public Function GetAllLeadAddress(ByVal LeadId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select A.FirstName, A.LastName, ")
            .Append(" A.Address1,A.Address2,A.City,A.StateId,(select StateDescrip from syStates where StateId=A.StateId) as StateDescrip,A.Zip,A.phone,A.homeemail ")
            .Append(" from adLeads A where A.LeadId=? ")
        End With

        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString())

    End Function
    Public Function intCheckAddressExists(ByVal StudentId As String) As Integer
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim intAddressCount As Integer
        With sb
            .Append(" select count(*) from arStudAddresses A where StudentId = ? and default1=1 ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            intAddressCount = db.RunParamSQLScalar(sb.ToString)
            Return intAddressCount
        Catch ex As Exception
            intAddressCount = 0
            Return intAddressCount
        End Try
    End Function
    Public Function intCheckPhoneExists(ByVal StudentId As String) As Integer
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim intPhoneCount As Integer
        With sb
            .Append(" select count(*) from arStudentPhone A where StudentId = ? and default1=1 ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            intPhoneCount = db.RunParamSQLScalar(sb.ToString)
            Return intPhoneCount
        Catch ex As Exception
            intPhoneCount = 0
            Return intPhoneCount
        End Try
    End Function
    Public Function GetStudentName(ByVal StudentId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" select distinct firstname,lastname ")
            .Append(" from arStudent  where StudentId = ? ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString())
    End Function
    Public Function GetResumeStudentPhone(ByVal StudentId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select Phone,ForeignPhone from arStudentPhone where StudentId=? and default1=1 ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString())

    End Function
    Public Function GetResumeStudentAddress(ByVal StudentId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" select distinct firstname,lastname,HomeEmail, ")
            .Append(" (select Address1 from arStudAddresses A  ")
            .Append(" where StudentId=t1.StudentId and default1=1) as Address, ")
            .Append(" (select City from arStudAddresses A ")
            .Append(" where StudentId=t1.StudentId and default1=1) as City, ")
            .Append(" (select StateDescrip from arStudAddresses A,syStates C  ")
            .Append(" where StudentId=t1.StudentId and A.StateId = C.StateId and A.default1=1) as State, ")
            .Append(" (select zip from arStudAddresses where StudentId=t1.StudentId and default1=1) as zip,  ")
            .Append(" (Select Phone from arStudentPhone where StudentId=t1.StudentId and default1=1) as Phone ")
            .Append(" from arStudent t1 where t1.StudentId = ? ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString())

    End Function
    Public Function GetAllStudentSkills(ByVal StudentId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" select SkillDescrip,SkillGrpName from ")
            .Append(" plStudentSkills A,plSkills B,plSkillGroups C ")
            .Append(" where A.SkillId = B.SkillId ")
            .Append(" and B.SkillGrpId = C.SkillGrpId ")
            .Append(" and A.StudentId= ? ")
            .Append(" Order By SkillGrpName ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString())

    End Function
    Public Function GetStudentEmploymentHistory(ByVal StudentId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" select B.EmployerName as EmployerDescrip,convert(char(8),B.StartDate,1) as StartDate, ")
            .Append(" convert(char(8),B.EndDate,1) as EndDate,C.TitleDescrip from ")
            .Append(" plStudentEmployment B,adTitles C ")
            .Append(" where  B.JobTitleId = C.TitleId ")
            .Append(" and B.StudentId = ? ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString())

    End Function
    Public Function GetSchoolStuEmpHistory(ByVal StudentId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select (select EmployerDescrip from plEmployers where EmployerId = SP.EmployerId) EmployerDescrip, ")
            .Append("(Select TitleDescrip from adTitles where adTitles.TitleId = SP.JobTitleId) as JobTitleDescrip, ")
            .Append(" Convert(Char(10),StartDate,101) as StartDate,TerminationDate ")
            .Append(" from plStudentsPlaced SP ")
            .Append(" where SP.StudentId = ? ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString())

    End Function
    Public Function GetStudentCollegeHistory(ByVal StudentId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select CollegeName,convert(char(8),GraduatedDate,1) as GraduatedDate,'(' + Major + ')' as Major,DegreeDescrip ")
            .Append(" from adColleges A,plStudentEducation B,arDegrees C ")
            .Append(" where A.CollegeId = B.EducationInstId ")
            .Append(" and B.CertificateId = C.DegreeId ")
            .Append(" and B.StudentId = ? ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString())

    End Function
    Public Function ResumeEducationHistory(ByVal LeadId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select CollegeName,GraduatedDate,Major as Major,DegreeDescrip ")
            .Append(" from adColleges A,adLeadEducation B,arDegrees C ")
            .Append(" where A.CollegeId = B.EducationInstId ")
            .Append(" and B.CertificateId = C.DegreeId ")
            .Append(" and B.LeadID = ? ")
            .Append(" Union")
            .Append(" Select Distinct HsName as CollegeName,GraduatedDate,Null as Major,DegreeDescrip ")
            .Append(" from syInstitutions A,adLeadEducation B,arDegrees C ")
            .Append(" where A.HsId = B.EducationInstId ")
            .Append(" and B.CertificateId = C.DegreeId ")
            .Append(" and B.LeadId = ? ")
        End With

        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString())

    End Function
    Public Function ResumeStudentEducationHistory(ByVal StudentId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select CollegeName,GraduatedDate,Major as Major,(select DegreeDescrip from arDegrees where DegreeId = B.CertificateId) as DegreeDescrip ")
            .Append(" from adColleges A,plStudentEducation B ")
            .Append(" where A.CollegeId = B.EducationInstId ")
            .Append(" and B.StudentID = ? ")
            .Append(" Union")
            .Append(" Select Distinct HsName as CollegeName,GraduatedDate,Null as Major,(select DegreeDescrip from arDegrees where DegreeId = B.CertificateId) as DegreeDescrip ")
            .Append(" from syInstitutions A,plStudentEducation B ")
            .Append(" where A.HsId = B.EducationInstId ")
            .Append(" and B.StudentId = ? ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString())

    End Function
    Public Function GetLeadCollegeHistory(ByVal LeadId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select CollegeName,convert(char(8),GraduatedDate,1) as GraduatedDate,'(' + Major + ')' as Major,(select DegreeDescrip from arDegrees where DegreeId = B.CertificateId) as DegreeDescrip ")
            .Append(" from adColleges A,adLeadEducation B ")
            .Append(" where A.CollegeId = B.EducationInstId ")
            .Append(" and B.LeadID = ? ")
        End With

        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString())

    End Function
    Public Function GetStudentSchoolHistory(ByVal StudentId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select Distinct HsName,convert(char(8),GraduatedDate,1) as GraduatedDate,(select DegreeDescrip from arDegrees where DegreeId = B.CertificateId) as DegreeDescrip ")
            .Append(" from syInstitutions A,plStudentEducation B ")
            .Append(" where A.HsId = B.EducationInstId ")
            .Append(" and B.StudentId = ? ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString())

    End Function
    Public Function GetLeadSchoolHistory(ByVal LeadId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select Distinct HsName,convert(char(8),GraduatedDate,1) as GraduatedDate,(select DegreeDescrip from arDegrees where DegreeId = B.CertificateId) as DegreeDescrip ")
            .Append(" from syInstitutions A,adLeadEducation B ")
            .Append(" where A.HsId = B.EducationInstId ")
            .Append(" and B.LeadId = ? ")
        End With

        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString())

    End Function
    Public Function GetJobTitleByEmployer(ByVal EmployerId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim da6 As New OleDbDataAdapter
        With sb
            .Append(" select Distinct S1.JobTitleId,s1.Code as TitleCode,s2.TitleDescrip,s1.EmployerJobTitle,s1.EmployerJobId ")
            .Append(" from plEmployerJobs s1,adTitles s2,syStatuses s3 where ")
            .Append(" s1.JobTitleId = s2.TitleId and s1.StatusId = s3.StatusId  and s3.Status='Active' and ")
            .Append(" s1.EmployerId = ? ")
        End With
        db.AddParameter("@EmployerId", EmployerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'return dataset
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT")
        Finally
            db.CloseConnection()
        End Try
        Return ds
    End Function
    Public Function SkillGroup(ByVal StudentId As String) As String
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim s3 As String
        Dim forCounter As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '' Grab the Categories and Products table
        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        dadNorthwind = New OleDbDataAdapter("select Distinct SkillGrpName,A.SkillGrpId from plSkillGroups A,plSkills B,plStudentSkills C where  A.SkillGrpId = B.SkillGrpId and B.SkillId = C.SkillId and C.StudentId='" & StudentId & "'", conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "Groups")

        dadNorthwind.SelectCommand = New OleDbCommand("select C.SkillGrpId,A.SkillId,B.SkillDescrip from plStudentSkills A,plSkills B,plSkillGroups C where A.SkillId = B.SkillId And B.SkillGrpId = c.SkillGrpId and A.StudentId='" & StudentId & "'", conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "Skills")
        conNorthwind.Close()

        '' Add Parent/Child Relationship
        dstNorthwind.Relations.Add( _
         "myrelation", _
         dstNorthwind.Tables("Groups").Columns("SkillGrpID"), _
         dstNorthwind.Tables("Skills").Columns("SkillGrpID"))


        ''Display each Skill Group and Child Skills for the Student
        s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
        For Each drowParent In dstNorthwind.Tables("Groups").Rows
            s3 &= "<TR><td class=""resumedescription2""><strong>" & drowParent("SkillGrpName") & "</strong></td></tr>"
            For Each drowChild In drowParent.GetChildRows("myrelation")
                s3 &= "<tr><td class=""resumedescription2""><ul><li>" & drowChild("SkillDescrip") & "</li></ul></td></tr>"
            Next
            forCounter = 1
        Next
        s3 &= "</table>"

        If forCounter = 1 Then
            Return s3.ToString()
        Else
            Return "None"
        End If
    End Function
    Public Function Experience(ByVal StudentId As String) As String
        '    Dim dstNorthwind As DataSet
        '    Dim conNorthwind As New OleDbConnection
        '    Dim dadNorthwind As OleDbDataAdapter
        '    Dim drowParent As DataRow
        '    Dim drowChild As DataRow
        '    Dim s3 As String
        '    Dim forCounter As Integer
        '    Dim strRoles() As String
        '    Dim intRoleLength As Integer
        '    Dim x As Integer

        '    '' Grab the Categories and Products table
        '    dstNorthwind = New DataSet
        '    conNorthwind.ConnectionString = SingletonAppSettings.AppSettings("ConString")

        '    dadNorthwind = New OleDbDataAdapter("select t1.EmployerId,t2.EmployerDescrip,t1.State,t1.City,t3.TitleDescrip,t1.StartDate,t1.EndDate from adLeadEmployment t1,plEmployers t2,adTitles t3 where t1.EmployerId = t2.EmployerId and t1.JobTitleId = t3.TitleId  and t1.LeadId = '" & StudentId & "'", conNorthwind)
        '    conNorthwind.Open()
        '    dadNorthwind.Fill(dstNorthwind, "ExperienceGroup")

        '    dadNorthwind.SelectCommand = New OleDbCommand("select EmployerId,JobResponsibilities from adLeadEmployment where LeadId='" & StudentId & "'", conNorthwind)
        '    dadNorthwind.Fill(dstNorthwind, "ExperienceDetail")
        '    conNorthwind.Close()

        '    '' Add Parent/Child Relationship
        '    Try
        '        dstNorthwind.Relations.Add( _
        '         "myrelation", _
        '         dstNorthwind.Tables("ExperienceGroup").Columns("EmployerID"), _
        '         dstNorthwind.Tables("ExperienceDetail").Columns("EmployerID"))


        '        ''Display each Skill Group and Child Skills for the Student
        '        s3 &= "<TABLE>"
        '        For Each drowParent In dstNorthwind.Tables("ExperienceGroup").Rows
        '            s3 &= "<TR><td nowrap><font face=verdana size=2><b>" & drowParent("EmployerDescrip") & "</b></font></td></tr>"
        '            s3 &= "<TR><td nowrap><font face=verdana size=1>" & drowParent("StartDate") & "-" & drowParent("EndDate") & "</font></td><td nowrap><font face=verdana size=1>" & drowParent("TitleDescrip") & "</font></td><td width=""5%"">&nbsp;</td><td><font face=verdana size=1>" & drowParent("city") & "," & drowParent("State") & "</font></td></tr>"
        '            For Each drowChild In drowParent.GetChildRows("myrelation")
        '                strRoles = drowChild("JobResponsibilities").ToString.Split(".")
        '                intRoleLength = strRoles.Length
        '                While x < intRoleLength
        '                    If strRoles(x).Length >= 1 Then
        '                        s3 &= "<tr><td nowrap><li><font face=verdana size=1>" & strRoles(x) & "</font></td></tr>"
        '                    End If
        '                    x = x + 1

        '                End While
        '                x = 0
        '            Next
        '            s3 &= "<TR></tr>"
        '            forCounter = 1
        '        Next
        '        s3 &= "</table>"
        '        If forCounter = 1 Then
        '            Return s3.ToString()
        '        Else
        '            Return "None"
        '        End If
        '    Catch ex As Exception
        '        Return "None"
        '    End Try
    End Function
    Public Function StudentExperience(ByVal StudentId As String) As String
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim strRoles() As String
        Dim intRoleLength As Integer
        Dim x As Integer
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '' Grab the Categories and Products table
        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'dadNorthwind = New OleDbDataAdapter("select * from plStudentEmployment t1,adTitles t3 where t1.JobTitleId = t3.TitleId  and t1.StudentId = '" & StudentId & "'", conNorthwind)
        'conNorthwind.Open()
        'dadNorthwind.Fill(dstNorthwind, "ExperienceGroup")

        'Return dstNorthwind

        With sb
            .Append(" select t1.StEmploymentId,t1.EmployerName,(select Distinct TitleDescrip from adTitles where TitleId=t1.JobTitleId) as TitleDescrip,t1.StartDate,t1.EndDate,t1.EmployerJobTitle,NULL as JobDescrip from plStudentEmployment t1 where t1.StudentId = '" & StudentId & "' ")
            .Append(" union ")
            .Append(" select ")
            .Append(" t1.PlacementId as StEmployementId, ")
            .Append(" t3.EmployerDescrip as EmployerName, ")
            .Append(" NULL as TitleDescrip, ")
            .Append(" t1.StartDate, ")
            .Append(" NULL as EndDate, ")
            .Append(" t2.EmployerJobTitle as EmployerJobTitle,t1.JobDescrip as JobDescrip ")
            .Append(" from ")
            .Append(" plStudentsPlaced t1, ")
            .Append(" plEmployerJobs t2, ")
            .Append(" plEmployers t3 ")
            .Append(" where ")
            .Append(" t1.EmployerJobId = t2.EmployerJobId and t2.EmployerId = t3.EmployerId and ")
            .Append(" t1.StuEnrollId in (select StuEnrollId from arStuEnrollments  ")
            .Append(" where StudentId='" & StudentId & "') order by StartDate Desc")
        End With


        dadNorthwind = New OleDbDataAdapter(sb.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "ExperienceGroup")

        dadNorthwind.SelectCommand = New OleDbCommand("select StEmploymentId,EmployerName,JobResponsibilities from plStudentEmployment where StudentId='" & StudentId & "'", conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "ExperienceDetail")
        conNorthwind.Close()

        ''' Add Parent/Child Relationship
        Try
            dstNorthwind.Relations.Add( _
             "myrelation", _
             dstNorthwind.Tables("ExperienceGroup").Columns("StEmploymentId"), _
             dstNorthwind.Tables("ExperienceDetail").Columns("StEmploymentId"))


            ''Display each Skill Group and Child Skills for the Student
            s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
            For Each drowParent In dstNorthwind.Tables("ExperienceGroup").Rows
                s3 &= "<TR><td class=""educationdescription"" style=""width: 50%; text-align: left""><strong>" & drowParent("EmployerName") & "</strong></td>"
                s3 &= "<td class=""educationdescription"" style=""width: 25%; text-align: left"">" & drowParent("EmployerJobTitle") & "</td>"
                If Not drowParent("StartDate") Is DBNull.Value Then
                    s3 &= "<td class=""educationdescription"" style=""width: 25%; text-align: left"">" & drowParent("StartDate")
                    If Not drowParent("EndDate") Is DBNull.Value Then
                        s3 &= "-" & drowParent("EndDate") & "</td></tr>"
                    End If
                    If Not drowParent("JobDescrip") Is DBNull.Value Then
                        s3 &= "<tr><td class=""resumedescription"" colspan=""3""><ul><li>" & drowParent("JobDescrip") & "</li></ul></td></tr>"
                    End If
                End If
                For Each drowChild In drowParent.GetChildRows("myrelation")
                    strRoles = drowChild("JobResponsibilities").ToString.Split(".")
                    intRoleLength = strRoles.Length
                    While x < intRoleLength
                        If strRoles(x).Length >= 1 Then
                            s3 &= "<tr><td class=""resumedescription"" colspan=""3""><ul><li>" & strRoles(x) & "</li></ul></td></tr>"
                        End If
                        x = x + 1
                    End While
                    x = 0
                Next
                s3 &= ""
                forCounter = 1
            Next
            s3 &= "</table>"
            If forCounter = 1 Then
                Return s3.ToString()
            Else
                Return "None"
            End If
        Catch ex As Exception
        End Try
    End Function

    Public Function ExtracurricularGroup(ByVal StudentId As String) As String
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim s3 As String
        Dim forCounter As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '' Grab the Categories and Products table
        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        dadNorthwind = New OleDbDataAdapter("select Distinct A.ExtraCurrGrpDescrip,A.ExtracurrGrpId from adExtraCurrGrp A,plStudentExtracurriculars B where A.ExtracurrGrpId = B.ExtracurricularGrpId and B.StudentId='" & StudentId & "'", conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "ExtracurricularGroups")

        dadNorthwind.SelectCommand = New OleDbCommand("select A.ExtracurricularGrpId,A.ExtracurricularId,B.ExtracurrDescrip from plStudentExtracurriculars A,adExtraCurr B,adExtraCurrGrp  C where A.ExtracurricularId = B.ExtracurrId And A.ExtracurricularGrpId = c.ExtracurrGrpId and A.StudentId='" & StudentId & "'", conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "ExtracurricularSkills")
        conNorthwind.Close()

        '' Add Parent/Child Relationship
        dstNorthwind.Relations.Add( _
         "Extracurricular", _
         dstNorthwind.Tables("ExtracurricularGroups").Columns("ExtracurrGrpID"), _
         dstNorthwind.Tables("ExtracurricularSkills").Columns("ExtracurricularGrpId"))


        ''Display each Skill Group and Child Skills for the Student
        s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
        For Each drowParent In dstNorthwind.Tables("ExtracurricularGroups").Rows
            s3 &= "<TR><td class=""resumedescription2""><strong>" & drowParent("ExtracurrGrpDescrip") & "</strong></td></tr>"
            For Each drowChild In drowParent.GetChildRows("Extracurricular")
                s3 &= "<tr><td class=""resumedescription2""><ul><li>" & drowChild("ExtracurrDescrip") & "</li></ul></td></tr>"
            Next
            forCounter = 1
        Next
        s3 &= "</table>"
        If forCounter = 1 Then
            Return s3.ToString()
        Else
            Return "None"
        End If
    End Function
    Public Function AddStudentPlHistory(ByVal StudentInfo As PlacementInfo, ByVal user As String) As String

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            '    'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Insert plStudentsPlaced(PlacementId,EmployerJobId,JobDescrip, ")
                .Append(" PlacementRep,JobStatusId,Supervisor,PlacedDate, ")
                .Append(" StartDate,Fee,Salary,SalaryTypeId,ScheduleId, ")
                .Append(" BenefitsId,HowPlacedId,InterViewId,FldStudyId, ")
                .Append(" TerminationDate,TerminationReason,Reason,ModUser,ModDate,StuEnrollId) ")
                .Append(" Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            ''Add Parameters To Query

            'PlacmentId
            db.AddParameter("@PlacementId", StudentInfo.PlacementId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Job TitleId
            If StudentInfo.EmployerJobTitle = Guid.Empty.ToString Or StudentInfo.EmployerJobTitle = "" Then
                db.AddParameter("@EmployerJobId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@EmployerJobId", StudentInfo.EmployerJobTitle, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Job Descrip
            If StudentInfo.Description = "" Then
                db.AddParameter("@JobDescrip", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
            Else
                db.AddParameter("@JobDescrip", StudentInfo.Description, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
            End If


            'Placement Rep
            If StudentInfo.PlacementRep = Guid.Empty.ToString Or StudentInfo.PlacementRep = "" Then
                db.AddParameter("@PlacementRep", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PlacementRep", StudentInfo.PlacementRep, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Job StatusId
            If StudentInfo.JobStatus = Guid.Empty.ToString Then
                db.AddParameter("@JobStatusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@JobStatusId", StudentInfo.JobStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Supervisor
            If StudentInfo.Supervisor = Guid.Empty.ToString Or StudentInfo.Supervisor = "" Then
                db.AddParameter("@Supervisor", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Supervisor", StudentInfo.Supervisor, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Placed Date
            If StudentInfo.PlacedDate = "" Then
                db.AddParameter("@PlacedDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PlacedDate", StudentInfo.PlacedDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Start Date
            If StudentInfo.StartDate = "" Then
                db.AddParameter("@StartDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@StartDate", StudentInfo.StartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Fee
            If StudentInfo.FeeId = "" Or StudentInfo.FeeId = "00000000-0000-0000-0000-000000000000" Then
                db.AddParameter("@Fee", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Fee", StudentInfo.FeeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Salary
            If StudentInfo.Salary = "" Then
                db.AddParameter("@Salary", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Salary", StudentInfo.Salary, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Salary Type Id
            If StudentInfo.SalaryTypeId = Guid.Empty.ToString Or StudentInfo.SalaryTypeId = "" Then
                db.AddParameter("@SalaryTypeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SalaryTypeId", StudentInfo.SalaryTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ScheduleId
            If StudentInfo.ScheduleId = Guid.Empty.ToString Or StudentInfo.ScheduleId = "" Then
                db.AddParameter("@scheduleid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@scheduleid", StudentInfo.ScheduleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Benefits Id
            If StudentInfo.BenefitsId = "" Or StudentInfo.BenefitsId = Guid.Empty.ToString Then
                db.AddParameter("@BenefitsId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@BenefitsId", StudentInfo.BenefitsId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'HowPlaced
            If StudentInfo.HowPlaced = "" Or StudentInfo.HowPlaced = Guid.Empty.ToString Then
                db.AddParameter("@HowPlacedId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@HowPlacedId", StudentInfo.HowPlaced, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Interview Id
            If StudentInfo.InterviewId = Guid.Empty.ToString Or StudentInfo.InterviewId = "" Then
                db.AddParameter("@InterviewId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@InterviewId", StudentInfo.InterviewId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'FldId
            If StudentInfo.FldStudyId = Guid.Empty.ToString Or StudentInfo.FldStudyId = "" Then
                db.AddParameter("@FldStudyId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@FldStudyId", StudentInfo.FldStudyId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'TerminationDate
            If StudentInfo.EndDate = "" Then
                db.AddParameter("@TerminationDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TerminationDate", StudentInfo.EndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Termination Reason
            If StudentInfo.TerminationReason = "" Then
                db.AddParameter("@TerminationReason", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TerminationReason", StudentInfo.TerminationReason, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Reason
            If StudentInfo.Comments = "" Then
                db.AddParameter("@Reason", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@Reason", StudentInfo.Comments, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If

            'StudentId
            'db.AddParameter("@StudentId", StudentInfo.StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'EnrollmentId
            If StudentInfo.EnrollmentId = Guid.Empty.ToString Then
                db.AddParameter("@StuEnrollId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StuEnrollId", StudentInfo.EnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            StudentInfo.IsInDb = True

            Return ""
            'Catch ex As System.Exception
            '    'Return an Error To Client
            '    Return -1
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateStudentPlHistory(ByVal StudentInfo As PlacementInfo, ByVal user As String) As String

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            '    'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append(" Update plStudentsPlaced set PlacementId=?,EmployerJobId=?,JobDescrip=?, ")
                .Append(" PlacementRep=?,JobStatusId=?,Supervisor=?,PlacedDate=?, ")
                .Append(" StartDate=?,Fee=?,Salary=?,SalaryTypeId=?,ScheduleId=?, ")
                .Append(" BenefitsId=?,HowPlacedId=?,InterViewId=?,FldStudyId=?, ")
                .Append(" TerminationDate=?,TerminationReason=?,Reason=?,ModUser=?,ModDate=?,StuEnrollId=? ")
                .Append(" where PlacementId = ? ")

            End With

            ''Add Parameters To Query

            'PlacementId
            db.AddParameter("@PlacementId", StudentInfo.PlacementId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Job TitleId
            If StudentInfo.EmployerJobTitle = Guid.Empty.ToString Or StudentInfo.EmployerJobTitle = "" Then
                db.AddParameter("@EmployerJobTitle", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@EmployerJobTitle", StudentInfo.EmployerJobTitle, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Job Descrip
            If StudentInfo.Description = "" Then
                db.AddParameter("@JobDescrip", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
            Else
                db.AddParameter("@JobDescrip", StudentInfo.Description, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
            End If


            'Placement Rep
            If StudentInfo.PlacementRep = Guid.Empty.ToString Or StudentInfo.PlacementRep = "" Then
                db.AddParameter("@PlacementRep", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@PlacementRep", StudentInfo.PlacementRep, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Job StatusId
            If StudentInfo.JobStatus = Guid.Empty.ToString Then
                db.AddParameter("@JobStatusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@JobStatusId", StudentInfo.JobStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Supervisor
            If StudentInfo.Supervisor = Guid.Empty.ToString Or StudentInfo.Supervisor = "" Then
                db.AddParameter("@Supervisor", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Supervisor", StudentInfo.Supervisor, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Placed Date
            If StudentInfo.PlacedDate = "" Then
                db.AddParameter("@PlacedDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@PlacedDate", StudentInfo.PlacedDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Start Date
            If StudentInfo.StartDate = "" Then
                db.AddParameter("@StartDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@StartDate", StudentInfo.StartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Fee
            If StudentInfo.FeeId = "" Or StudentInfo.FeeId = Guid.Empty.ToString Then
                db.AddParameter("@Fee", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Fee", StudentInfo.FeeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Salary
            If StudentInfo.Salary = "" Then
                db.AddParameter("@Salary", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@Salary", StudentInfo.Salary, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Salary Type Id
            If StudentInfo.SalaryTypeId = Guid.Empty.ToString Or StudentInfo.SalaryTypeId = "" Then
                db.AddParameter("@SalaryTypeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SalaryTypeId", StudentInfo.SalaryTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ScheduleId
            If StudentInfo.ScheduleId = Guid.Empty.ToString Or StudentInfo.ScheduleId = "" Then
                db.AddParameter("@scheduleid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@scheduleid", StudentInfo.ScheduleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Benefits Id
            If StudentInfo.BenefitsId = "" Or StudentInfo.BenefitsId = Guid.Empty.ToString Then
                db.AddParameter("@BenefitsId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@BenefitsId", StudentInfo.BenefitsId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'HowPlaced
            If StudentInfo.HowPlaced = "" Or StudentInfo.HowPlaced = Guid.Empty.ToString Then
                db.AddParameter("@HowPlacedId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@HowPlacedId", StudentInfo.HowPlaced, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Interview Id
            If StudentInfo.InterviewId = Guid.Empty.ToString Or StudentInfo.InterviewId = "" Then
                db.AddParameter("@InterviewId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@InterviewId", StudentInfo.InterviewId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'FldId
            If StudentInfo.FldStudyId = Guid.Empty.ToString Or StudentInfo.FldStudyId = "" Then
                db.AddParameter("@FldStudyId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@FldStudyId", StudentInfo.FldStudyId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'TerminationDate
            If StudentInfo.EndDate = "" Then
                db.AddParameter("@TerminationDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TerminationDate", StudentInfo.EndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Termination Reason
            If StudentInfo.TerminationReason = "" Then
                db.AddParameter("@TerminationReason", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TerminationReason", StudentInfo.TerminationReason, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'Reason
            If StudentInfo.Comments = "" Then
                db.AddParameter("@Reason", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@Reason", StudentInfo.Comments, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If

            'StudentId
            'db.AddParameter("@StudentId", StudentInfo.StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            If StudentInfo.EnrollmentId = Guid.Empty.ToString Then
                db.AddParameter("@StuEnrollId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StuEnrollId", StudentInfo.EnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            'PlacementId
            db.AddParameter("@PlacmentId", StudentInfo.PlacementId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            StudentInfo.IsInDb = True

            Return ""
            'Catch ex As System.Exception
            '    'Return an Error To Client
            '    Return -1
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function DeletePlacementInfo(ByVal PlacementId As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM plStudentsPlaced ")
                .Append("WHERE PlacementId = ? ")
            End With

            '   add parameters values to the query

            '   BankId
            db.AddParameter("@PlacmentId", PlacementId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetNameByStudentId(ByVal StudentId As String) As PlacementInfo
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" select firstname,middlename,lastname ")
            .Append(" from arStudent ")
            .Append(" where StudentId = ? ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'return dataset
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim studentinfo As New PlacementInfo
        While dr.Read()
            'set properties with data from DataReader
            With studentinfo
                If Not (dr("MiddleName") Is DBNull.Value) Then .MiddleName = dr("MiddleName")
                If Not (dr("FirstName") Is DBNull.Value) Then .firstname = dr("FirstName")
                If Not (dr("LastName") Is DBNull.Value) Then .LastName = dr("LastName")
            End With
        End While

        If Not dr.IsClosed Then dr.Close()

        Return studentinfo
    End Function
    Public Function GetEmploymentDates(ByVal StudentId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select StartDate,TerminationDate, ")
            .Append(" (select EmployerDescrip from plEmployers where EmployerId = SP.EmployerId) as EmployerDescrip, ")
            .Append(" (select TitleDescrip from adTitles where TitleId = SP.JobTitleId) as JobTitleDescrip ")
            .Append(" from plStudentsPlaced SP where StudentId= ? ")
            .Append(" union ")
            .Append(" select StartDate,EndDate as TerminationDate, ")
            .Append(" EmployerName as EmployerDescrip, ")
            .Append(" (select TitleDescrip from adTitles where TitleId = SE.JobTitleId) as JobTitleDescrip ")
            .Append(" from plStudentEmployment SE where StudentId= ? ")
            .Append(" order by StartDate Desc ")
        End With

        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet(sb.ToString())
    End Function
    Public Function GetLeadEmploymentDates(ByVal LeadId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" select StartDate,EndDate as TerminationDate, ")
            .Append(" EmployerName as EmployerDescrip, ")
            .Append(" (select TitleDescrip from adTitles where TitleId = SE.JobTitleId) as JobTitleDescrip ")
            .Append(" from adLeadEmployment SE where LeadId= ? ")
            .Append(" order by StartDate Desc ")
        End With

        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Return db.RunParamSQLDataSet(sb.ToString())
    End Function
    Public Function GetHowPlaced() As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select SG.HowPlacedDescrip,SG.HowPlacedId ")
            .Append(" from plHowPlaced SG,syStatuses ST ")
            .Append(" where SG.StatusId = St.StatusId and St.Status = 'Active'")
            .Append(" Order By SG.HowPlacedDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function AddStudentExitInterview(ByVal StudentInfo As PlacementInfo, ByVal user As String) As String

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            '    'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Insert plExitInterview(ExtInterviewId,EnrollmentId,ScStatusId,WantAssistance,WaiverSigned, ")
                .Append(" Eligible,Reason,AreaId,AvailableDate, ")
                .Append(" AvailableDays,AvailableHours,LowSalary,HighSalary,TransportationId,ModUser,ModDate,ExpertiseLevelId,FullTimeId,InelReasonId) ")
                .Append(" Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With
            '19
            'Primary Key 1
            db.AddParameter("@ExtInterviewId", StudentInfo.InterviewId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'PlacmentId 2
            db.AddParameter("@EnrollmentId", StudentInfo.EnrollmentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ScStatusId 3
            If StudentInfo.SchoolStatusId = Guid.Empty.ToString Or StudentInfo.SchoolStatusId = "" Then
                db.AddParameter("@ScStatusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ScStatusId", StudentInfo.SchoolStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'WantAssistance 4
            If StudentInfo.WantAssistance = "" Then
                db.AddParameter("@WantAssistance", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@WantAssistance", StudentInfo.WantAssistance, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'WaiverSigned 5
            If StudentInfo.WaiverSigned = "" Then
                db.AddParameter("@WaiverSigned", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@WaiverSigned", StudentInfo.WaiverSigned, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Eligible 6
            If StudentInfo.Eligible = "" Then
                db.AddParameter("@Eligible", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Eligible", StudentInfo.Eligible, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Reason 7
            If StudentInfo.Reason = "" Then
                db.AddParameter("@Reason", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Reason", StudentInfo.Reason, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'AreaID 8
            If StudentInfo.AreaId = Guid.Empty.ToString Then
                db.AddParameter("@AreaId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@AreaId", StudentInfo.AreaId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Available Date 9
            If StudentInfo.AvailableDate = "" Then
                db.AddParameter("@AvailableDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
            Else
                db.AddParameter("@AvailableDate", StudentInfo.AvailableDate, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
            End If


            'AvailableDays 10
            If StudentInfo.AvailableDays = "" Then
                db.AddParameter("@AvailableDays", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@AvailableDays", StudentInfo.AvailableDays, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Available Hours 11
            If StudentInfo.AvailableHours = "" Then
                db.AddParameter("@AvailableHours", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@AvailableHours", StudentInfo.AvailableHours, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'LowSalary 12
            If StudentInfo.LowSalary = "" Then
                db.AddParameter("@LowSalary", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@LowSalary", StudentInfo.LowSalary, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'HighSalary 13
            If StudentInfo.HighSalary = "" Then
                db.AddParameter("@HighSalary", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HighSalary", StudentInfo.HighSalary, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'TransporationId 14
            If StudentInfo.TransportationId = Guid.Empty.ToString Then
                db.AddParameter("@TransportationId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TransportationId", StudentInfo.TransportationId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ModUser 15
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'ExpertiseLevelId 16
            If StudentInfo.ExpertiseLevelId = "" Then
                db.AddParameter("@ExpertiseLevelId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ExpertiseLevelId", StudentInfo.ExpertiseLevelId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'FullPartTime Id 17
            If StudentInfo.FullPartTime = "" Then
                db.AddParameter("@FullPartId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@FullPartId", StudentInfo.FullPartTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'InelReasonId Id 18
            If StudentInfo.InelReasonId = "" Then
                db.AddParameter("@InelReasonId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@InelReasonId", StudentInfo.InelReasonId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            StudentInfo.IsInDb = True

            'Retun Without Errors
            Return ""

        Catch ex As OleDbException
            'Return an Error To Client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateStudentExitInterview(ByVal StudentInfo As PlacementInfo, ByVal user As String) As String

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            '    'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Update plExitInterview set ScStatusId=?,WantAssistance=?,WaiverSigned=?, ")
                .Append(" Eligible=?,Reason=?,AreaId=?,AvailableDate=?,  ExitInterviewDate=?, ")
                .Append(" AvailableDays=?,AvailableHours=?,LowSalary=?,HighSalary=?,TransportationId=?,ModUser=?,ModDate=?,ExpertiseLevelId=?,FullTimeId=?, InelReasonId=? ")
                .Append(" Where ExtInterViewId = ? ")
            End With

            'ScStatusId
            If StudentInfo.SchoolStatusId = Guid.Empty.ToString Or StudentInfo.SchoolStatusId = "" Then
                db.AddParameter("@ScStatusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ScStatusId", StudentInfo.SchoolStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'WantAssistance
            If StudentInfo.WantAssistance = "" Then
                db.AddParameter("@WantAssistance", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@WantAssistance", StudentInfo.WantAssistance, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'WaiverSigned
            If StudentInfo.WaiverSigned = "" Then
                db.AddParameter("@WaiverSigned", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@WaiverSigned", StudentInfo.WaiverSigned, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Eligible
            If StudentInfo.Eligible = "" Then
                db.AddParameter("@Eligible", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Eligible", StudentInfo.Eligible, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Reason
            If StudentInfo.Reason = "" Then
                db.AddParameter("@Reason", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Reason", StudentInfo.Reason, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'AreaID
            If StudentInfo.AreaId = Guid.Empty.ToString Then
                db.AddParameter("@AreaId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@AreaId", StudentInfo.AreaId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Available Date
            If StudentInfo.AvailableDate = "" Then
                db.AddParameter("@AvailableDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
            Else
                db.AddParameter("@AvailableDate", StudentInfo.AvailableDate, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
            End If

            'Exit Interview Date
            If StudentInfo.AvailableDate = "" Then
                db.AddParameter("@ExitInterviewDate", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
            Else
                db.AddParameter("@ExitInterviewDate", StudentInfo.ExitInterviewDate, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
            End If


            'AvailableDays
            If StudentInfo.AvailableDays = "" Then
                db.AddParameter("@AvailableDays", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@AvailableDays", StudentInfo.AvailableDays, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Available Hours
            If StudentInfo.AvailableHours = "" Then
                db.AddParameter("@AvailableHours", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@AvailableHours", StudentInfo.AvailableHours, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'LowSalary
            If StudentInfo.LowSalary = "" Then
                db.AddParameter("@LowSalary", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@LowSalary", StudentInfo.LowSalary, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'HighSalary
            If StudentInfo.HighSalary = "" Then
                db.AddParameter("@HighSalary", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HighSalary", StudentInfo.HighSalary, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'TransporationId
            If StudentInfo.TransportationId = Guid.Empty.ToString Then
                db.AddParameter("@TransportationId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TransportationId", StudentInfo.TransportationId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)


            'ExpertiseLevelId
            If StudentInfo.ExpertiseLevelId = "" Then
                db.AddParameter("@ExpertiseLevelId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ExpertiseLevelId", StudentInfo.ExpertiseLevelId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'FullPartTime Id
            If StudentInfo.FullPartTime = "" Then
                db.AddParameter("@FullPartId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@FullPartId", StudentInfo.FullPartTime, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            If StudentInfo.InelReasonId = "" Then
                db.AddParameter("@InelReasonId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@InelReasonId", StudentInfo.InelReasonId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ExitInterViewId
            db.AddParameter("@ExitInterViewId", StudentInfo.InterviewId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            StudentInfo.IsInDb = True


        Catch ex As OleDbException
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetExitInterviewDetails(ByVal ExitInterviewId As String) As PlacementInfo
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT   B.EnrollmentId, ")
            .Append(" B.ScStatusId, ")
            .Append("    B.WantAssistance, ")
            .Append("  B.WaiverSigned, ")
            .Append("    B.Eligible, ")
            .Append("    B.Reason, ")
            .Append("    B.AreaId, ")
            .Append(" (Select CountyDescrip from adCounties where CountyId=B.AreaId) As Area, ")
            .Append("    B.AvailableDate, ")
            .Append("    B.AvailableDays, ")
            .Append(" B.AvailableHours, ")
            .Append(" B.LowSalary, ")
            .Append(" B.HighSalary, ")
            .Append(" B.TransportationId, ")
            .Append(" (Select TransportationDescrip from plTransportation where TransportationId=B.TransportationId) as Transportation, ")
            .Append(" B.ExpertiseLevelId,B.FullTimeId,B.ExtInterviewId, ")
            .Append(" B.InelReasonId, B.ExitInterviewDate ")
            .Append(" FROM  plExitInterview B ")
            .Append(" where B.ExtInterviewId = ? ")
        End With

        'Add the PlacementId the parameter list
        db.AddParameter("@ExitInterviewId", ExitInterviewId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim studentinfo As New PlacementInfo
        While dr.Read()

            'set properties with data from DataReader
            With studentinfo
                .IsInDb = True
                .InterviewId = CType(dr("ExtInterviewId"), Guid).ToString
                If Not (dr("WantAssistance") Is DBNull.Value) Then .WantAssistance = dr("WantAssistance") Else .WantAssistance = "No"
                If Not (dr("WaiverSigned") Is DBNull.Value) Then .WaiverSigned = dr("WaiverSigned") Else .WaiverSigned = "No"
                If Not (dr("Eligible") Is DBNull.Value) Then .Eligible = dr("Eligible") Else .Eligible = "No"
                If Not (dr("Reason") Is DBNull.Value) Then .Reason = dr("Reason") Else .Reason = ""
                If Not (dr("AvailableDate") Is DBNull.Value) Then .AvailableDate = dr("AvailableDate") Else .AvailableDate = ""
                If Not (dr("AvailableDays") Is DBNull.Value) Then .AvailableDays = dr("AvailableDays") Else .AvailableDays = ""
                If Not (dr("AvailableHours") Is DBNull.Value) Then .AvailableHours = dr("AvailableHours") Else .AvailableHours = ""
                If Not (dr("LowSalary") Is DBNull.Value) Then .LowSalary = dr("LowSalary") Else .LowSalary = ""
                If Not (dr("HighSalary") Is DBNull.Value) Then .HighSalary = dr("HighSalary") Else .HighSalary = ""
                If Not (dr("EnrollmentId") Is DBNull.Value) Then .EnrollmentId = CType(dr("EnrollmentId"), Guid).ToString
                If Not (dr("AreaId") Is DBNull.Value) Then .AreaId = CType(dr("AreaId"), Guid).ToString
                If Not (dr("Area") Is DBNull.Value) Then .Area = dr("Area")
                If Not (dr("ScStatusId") Is DBNull.Value) Then .SchoolStatusId = CType(dr("ScStatusId"), Guid).ToString
                If Not (dr("TransportationId") Is DBNull.Value) Then .TransportationId = CType(dr("TransportationId"), Guid).ToString Else .TransportationId = ""
                If Not (dr("Transportation") Is DBNull.Value) Then .Transportation = dr("Transportation")
                If Not (dr("ExpertiseLevelid") Is DBNull.Value) Then .ExpertiseLevelId = CType(dr("ExpertiseLevelId"), Guid).ToString Else .ExpertiseLevelId = ""
                If Not (dr("FullTimeId") Is DBNull.Value) Then .FullPartTime = CType(dr("FullTimeId"), Guid).ToString Else .FullPartTime = ""
                If Not (dr("InelReasonId") Is DBNull.Value) Then .InelReasonId = CType(dr("InelReasonId"), Guid).ToString Else .FullPartTime = ""
                If Not (dr("ExitInterviewDate") Is DBNull.Value) Then .ExitInterviewDate = dr("ExitInterviewDate") Else .ExitInterviewDate = ""
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return studentinfo
    End Function
    'Public Function GetCountExitInterView(ByVal EnrollmentId As String, ByVal StudentId As String, ByVal AvailableDate As String) As Integer
    '    Dim result As Integer
    '    Dim sb As New StringBuilder
    '    Dim db As New DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '    With sb
    '        .Append("Select Count(*) as TotalCount from plExitInterview where EnrollmentId = ? and StudentId= ? and AvailableDate = ? ")
    '    End With
    '    db.AddParameter("@EnrollmentId", EnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@AvailableDate", AvailableDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '    Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
    '    While dr.Read()
    '        result = dr("TotalCount")
    '    End While
    '    db.CloseConnection()
    '    Return result
    'End Function
    Public Function GetEnrollmentDescrip(ByVal EnrollmentId As String) As PlacementInfo
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT  PrgVerDescrip from arPrgVersions where PrgVerId =? ")
        End With

        'Add the PrgVerId
        db.AddParameter("@PrgVerId", EnrollmentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim studentinfo As New PlacementInfo
        While dr.Read()

            'set properties with data from DataReader
            With studentinfo
                .EnrollmentDescrip = dr("PrgVerDescrip")
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return EnrollmentDescrip
        Return studentinfo
    End Function
    Public Function DeleteExitInterview(ByVal ExitInterViewId As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM plExitInterview ")
                .Append("WHERE ExtInterViewId = ? ")
            End With


            'ExitInterViewId
            db.AddParameter("@ExitInterViewId", ExitInterViewId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAllInterviewScheduleByStudent(ByVal StudentId As String) As DataSet

        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            '.Append(" select s1.AvailableDate,s1.ExtInterviewId,s2.PrgVerDescrip ")
            '.Append(" from plExitInterview s1,arPrgVersions s2 where ")
            '.Append(" s1.EnrollmentId = s2.PrgVerId and StudentId= ? ")
            .Append(" select s1.AvailableDate,s1.ExtInterviewId,s4.PrgVerDescrip ")
            .Append(" from plExitInterview s1,arStuEnrollments s2,arStudent s3,arPrgVersions s4 where ")
            .Append(" s1.EnrollmentId = s2.StuEnrollId and s2.StudentId = s3.StudentId and s2.PrgVerId=s4.PrgVerId and ")
            .Append(" s3.StudentId= ? ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetResumeSkills(ByVal StudentId As String)
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" select A.SkillGrpName as SkillGroup,B.SkillDescrip as Descrip ")
            .Append(" from plSkillGroups A,plSkills B,plStudentSkills C ")
            .Append(" where A.SkillGrpID = B.SkillGrpId And B.SkillID = C.SkillId ")
            .Append(" and C.StudentId = ? ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetIneligibilityReasons() As DataSet
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append("SELECT  * from syIneligibilityReasons ORDER BY InelReasonName ")
        End With


        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
End Class
