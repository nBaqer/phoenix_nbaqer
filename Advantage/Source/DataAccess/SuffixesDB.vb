Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' SuffixesDB.vb
'
' SuffixesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class SuffixesDB
    Public Function GetAllSuffixes() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   PR.SuffixId, PR.StatusId, PR.SuffixDescrip ")
            .Append("FROM     sySuffixes PR, syStatuses ST ")
            .Append("WHERE    PR.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY PR.SuffixDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
End Class
