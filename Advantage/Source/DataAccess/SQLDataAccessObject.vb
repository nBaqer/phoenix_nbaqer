﻿Imports System
Imports System.Collections
Imports System.Data.SqlClient
Imports FAME.Advantage.Common
Imports Microsoft.VisualBasic


Public NotInheritable Class SQLDataAccess
    Implements IDisposable

#Region "Private Variables and Objects"
    'ADO.NET Objects needed for data access
    Private ReadOnly myAdvAppSettings As AdvAppSettings
    Private mConnection As SqlConnection
    Private mCommand As SqlCommand
    Private mDataReader As SqlDataReader
    Private mDataAdapter As SqlDataAdapter
    Private mDataSet As DataSet
    Private mObject As Object
    Private mOutputParameters As Dictionary(Of String, SqlParameter)
    Public MPageAddress As String
    'We are going to handle parameters internally and this array is going to hold them
    Private ReadOnly mParameterList As ArrayList = New ArrayList()

    'Connection string variables
    Private mConnectionString As String
    Private mCommandTimeout As Integer
    'Naming, Cleanup and Exception variables
    Private ReadOnly mModuleName As String
    Private mDisposedBoolean As Boolean
    Private Const M_EXCEPTION_MESSAGE As String = "Data Application Error. Detail Error Information can be found in the Application Log"

#End Region

#Region "Public Constructors"
    Public Sub New()
        MyBase.New()
        mModuleName = Me.GetType.ToString
        mCommandTimeout = 600

        ' Get the settings
        myAdvAppSettings = AdvAppSettings.GetAppSettings()
        mConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
    End Sub

#End Region

#Region "Public Properties"
    'ConnectionString
    Public Property ConnectionString() As String
        Get
            Try
                Return mConnection.ConnectionString
            Catch
                Return ""
            End Try
        End Get
        Set(ByVal value As String)

            If value = "ConString" Then
                mConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
            ElseIf value = "ConStringAuditing" Then
                mConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
            ElseIf value = "ConStringMaster" Then
                mConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
            Else
                mConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
            End If
        End Set
    End Property

    Public ReadOnly Property Connection As SqlConnection
        Get
            mConnection = New SqlConnection(mConnectionString)
            Return mConnection
        End Get
    End Property

    Public Property CommandTimeout() As Integer
        Get
            Return mCommandTimeout
        End Get
        Set(ByVal Value As Integer)
            ' only accept legitimate values - 0 is not allowed since we don't want unlimited command
            '	execution time, for security purposes
            If Value > 0 Then
                mCommandTimeout = Value
            End If
        End Set
    End Property

    Public Property OutputParameters() As Dictionary(Of String, SqlParameter)
        Get
            Return mOutputParameters
        End Get
        Set(ByVal Value As Dictionary(Of String, SqlParameter))
            mOutputParameters = Value
        End Set
    End Property
#End Region

#Region "Public Methods"
    Public Function StartTransaction() As SqlTransaction
        mConnection = New SqlConnection(mConnectionString)
        mConnection.Open()
        Return mConnection.BeginTransaction()
    End Function
#End Region

#Region "DataAccess Methods"

#Region "Close connection method"
    Public Sub CloseConnection()
        mConnection.Close()
    End Sub
#End Region

#Region "Open connection method"
    Public Sub OpenConnection()
        mConnection = New SqlConnection(mConnectionString)
        mConnection.Open()
    End Sub
#End Region

#Region "RunSQLScalar - Standard SQL is run with ExecuteScalar"
    'The RunSQLScalar function accepts a SQL statement that is required 
    Public Function RunSQLScalar(ByVal SQL As String) As Object
        'Validate the SQL String to be larger than 10 characters
        ValidateSQLStatement(SQL)

        'We include all called object in the try block to catch any exceptions that could occur ( even in the creation of the connection)
        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it.")
            End If

            'Set a new Connection
            mConnection = New SqlConnection(mConnectionString)

            'Set a new Command that accepts an SQL statement and the connection. 
            'The command.commandtype does not have to be set since it defaults to text
            mCommand = New SqlCommand(SQL, mConnection)

            'ExecuteScalar
            mObject = mCommand.ExecuteScalar
            Return mObject
        Finally
            'Immediately Close the Connection after use to free up resources
            mConnection.Close()
        End Try

    End Function
#End Region

#Region "RunSQLDataSet - Standard SQL is run and returns a DataSet"

    'The runSQLDataSet function accepts a SQL statement that is required and an optional table name
    Public Function RunSQLDataSet(ByVal SQL As String, Optional ByVal TableName As String = Nothing) As DataSet

        'Validate the SQL String to be larger than 10 characters
        ValidateSQLStatement(SQL)

        'We include all called object in the try block to catch any exceptions that could occur ( even in the creation of the connection)
        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it.")
            End If

            'Set a new Connecton
            mConnection = New SqlConnection(mConnectionString)
            'Set a new Command that accepts an SQL statement and the connection. 
            'The command.commandtype does not have to be set since it defaults to text
            mCommand = New SqlCommand(SQL, mConnection)
            'Set a new DataSet
            mDataSet = New DataSet
            'Set a new DataAdapter that will run the SQL statement
            mDataAdapter = New SqlDataAdapter(mCommand)
            'Depending on table name passed, Fill the new DataSet with the returned Data in a table
            If TableName = Nothing Then
                mDataAdapter.Fill(mDataSet)
            Else
                mDataAdapter.Fill(mDataSet, TableName)
            End If

            Return mDataSet

        Catch exceptionObject As Exception
            'Any exception will be logged through our private logexception function
            'LogException(ExceptionObject)
            'The exception is passed back to the calling code, with our custom message and specific exception information
            Throw New Exception(M_EXCEPTION_MESSAGE, exceptionObject)
        Finally
            'Immediately Close the Connection after use to free up resources
            mConnection.Close()
        End Try

    End Function

#End Region

#Region "runSQLDataReader - Standard SQL is run and returns a DataReader"

    'The runSQLDataReader function accepts a SQL statement that is required
    Public Function RunSQLDataReader(ByVal SQL As String) As SqlDataReader

        'Validate the SQL String to be larger than 10 characters
        ValidateSQLStatement(SQL)

        'We include all called object in the try block to catch any exceptions that could occur ( even in the creation of the connection)

        'Check to see if this object has already been disposed
        If mDisposedBoolean = True Then
            Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it.")
        End If

        'Set a new Connection
        mConnection = New SqlConnection(mConnectionString)

        'Set a new Command that accepts an SQL statement and the connection. 
        'The command.commandtype does not have to be set since it defaults to text
        mCommand = New SqlCommand(SQL, mConnection)
        'We need to open the connection for the DataReader explicitly
        mConnection.Open()
        'Run the Execute Reader method of the Command Object
        mDataReader = mCommand.ExecuteReader

        Return mDataReader

    End Function

#End Region

#Region "RunParamSQLScalar - Parameterized SQL is run with ExecuteScalar"
    'The RunParamSQLScalar function accepts a SQL statement that is required
    Public Function RunParamSQLScalar(SQL As String) As Object
        'Validate SQL statement
        ValidateSQLStatement(SQL)

        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As SqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
            End If

            'Set a new connection
            mConnection = New SqlConnection(mConnectionString)

            'Define the command object and set command type to process Stored Procedure
            mCommand = New SqlCommand(SQL, mConnection)

            'Move through the privateParameterList with the help of the enumerator
            Do While (usedEnumerator.MoveNext())
                'Get parameter in privateParameterList
                prUsedParameter = usedEnumerator.Current

                'Convert parameter to SQLParameter
                prParameter = ConvertParameters(prUsedParameter)

                'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                mCommand.Parameters.Add(prParameter)
            Loop

            mConnection.Open()
            mObject = mCommand.ExecuteScalar
            Return mObject
        Finally
            mConnection.Close()
        End Try
    End Function
#End Region

#Region "RunParamSQLDataSet - Parameterized SQL is run and returns a DataSet"
    'The RunParamSQLDataSet function accepts a SQL statement that is required and an optinal table name
    Public Function RunParamSQLDataSet(ByVal SQL As String, Optional ByVal TableName As String = Nothing) As DataSet
        'Validate SQL statement
        ValidateSQLStatement(SQL)

        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As SqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
            End If

            'Set a new connection and DataSet
            mConnection = New SqlConnection(mConnectionString)
            Dim prDataSet As New DataSet

            'Define the command object and set command type to process Stored Procedure
            mCommand = New SqlCommand(SQL, mConnection)

            'Move through the privateParameterList with the help of the enumerator
            Do While (usedEnumerator.MoveNext())
                'prUsedParameter = Nothing
                'Get parameter in privateParameterList
                prUsedParameter = usedEnumerator.Current
                'Convert parameter to SQLParameter
                prParameter = ConvertParameters(prUsedParameter)
                'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                mCommand.Parameters.Add(prParameter)
            Loop
            'Have the DataAdapter run the Stored Procedure
            mDataAdapter = New SqlDataAdapter(mCommand)
            'Depending on table name passed, create the DataSet with or without specifically naming the table
            If TableName = Nothing Then
                mDataAdapter.Fill(prDataSet)
            Else
                mDataAdapter.Fill(prDataSet, TableName)
            End If

            Return prDataSet
        Finally
            'Always close the connection as soon as possible(only then will object be allowed to go out of scope)
            mConnection.Close()
        End Try
    End Function

#End Region

#Region "RunParamSQLDataReader - Parameterized SQL is run and returns a DataReader"
    'The RunParamSQLDataReader function accepts a SQL statement that is required
    Public Function RunParamSQLDataReader(ByVal SQL As String) As SqlDataReader
        'Validate Stored Procedure
        ValidateSQLStatement(SQL)

        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As SqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        'Check to see if this object has already been disposed
        If mDisposedBoolean = True Then
            Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
        End If

        'Set a new connection and DataSet
        mConnection = New SqlConnection(mConnectionString)

        'Define the command object and set command type to process Stored Procedure
        mCommand = New SqlCommand(SQL, mConnection)

        'Move through the privateParameterList with the help of the enumerator
        Do While (usedEnumerator.MoveNext())
            'prUsedParameter = Nothing
            'Get parameter in privateParameterList
            prUsedParameter = usedEnumerator.Current
            'Convert parameter to SQLParameter
            prParameter = ConvertParameters(prUsedParameter)
            'Add converted parameter to the privateCommand object that imports data through the DataAdapter
            mCommand.Parameters.Add(prParameter)
        Loop

        mConnection.Open()

        mDataReader = mCommand.ExecuteReader

        Return mDataReader
    End Function
#End Region

#Region "RunParamSQLDataAdapter - Parameterized SQL is run and returns a DataAdapter"
    'The RunParamSQLDataSet function accepts a SQL statement that is required
    Public Function RunParamSQLDataAdapter(ByVal SQL As String) As SqlDataAdapter
        'Validate SQL statement
        ValidateSQLStatement(SQL)

        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As SqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
            End If

            'This method should be used in conjunction with the constructor that 
            'specifies DataAdapter and opens the connection. At this point the connection
            'should be already opened.

            'Define the command object and set command type to process Stored Procedure
            mCommand = New SqlCommand(SQL, mConnection)

            'Move through the privateParameterList with the help of the enumerator
            Do While (usedEnumerator.MoveNext())
                'prUsedParameter = Nothing
                'Get parameter in privateParameterList
                prUsedParameter = usedEnumerator.Current
                'Convert parameter to SQLParameter
                prParameter = ConvertParameters(prUsedParameter)
                'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                mCommand.Parameters.Add(prParameter)
            Loop
            'Have the DataAdapter run the SQL Statement
            mDataAdapter = New SqlDataAdapter(mCommand)

            Return mDataAdapter

        Finally
            'Always close the connection as soon as possible(only then will object be allowed to go out of scope)
            mConnection.Close()
        End Try
    End Function
#End Region

#Region "Execute a none returning parameterized query"
    Public Sub RunParamSQLExecuteNoneQuery(SQL As String, Optional ByVal tran As SqlTransaction = Nothing)
        'Validate Stored Procedure
        ValidateSQLStatement(SQL)

        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As SqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
            End If

            'Set a new connection and DataSet
            If tran Is Nothing Then
                mConnection = New SqlConnection(mConnectionString)
            End If

            'Define the command object and set command type to process Stored Procedure
            mCommand = New SqlCommand(SQL, mConnection)
            If Not (tran Is Nothing) Then
                mCommand.Transaction = tran
            End If
            'Move through the privateParameterList with the help of the enumerator
            Do While (usedEnumerator.MoveNext())
                'prUsedParameter = Nothing
                'Get parameter in privateParameterList
                prUsedParameter = usedEnumerator.Current
                'Convert parameter to SQLParameter
                prParameter = ConvertParameters(prUsedParameter)
                'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                mCommand.Parameters.Add(prParameter)
            Loop

            If tran Is Nothing Then
                mConnection.Open()
            End If
            mCommand.ExecuteNonQuery()
        Finally
            If tran Is Nothing Then
                mConnection.Close()
            End If
        End Try
    End Sub
#End Region

#Region "RunSchemaInfo - Runs a SQL regular statement and returns a Datatable"
    'The RunSchemaInfoForAuditing function accepts a SQL statement that is required
    Public Function RunSchemaInfo(ByVal SQL As String) As DataTable
        Dim dtSchema As DataTable
        'Validate the SQL String to be larger than 10 characters
        ValidateSQLStatement(SQL)

        'We include all called object in the try block to catch any excepitons that could occur ( even in the creation of the connection)
        'Check to see if this object has already been disposed
        If mDisposedBoolean = True Then
            Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it.")
        End If

        'Set a new Connection.
        mConnection = New SqlConnection(mConnectionString)

        'Set a new Command that accepts an SQL statement and the connection. 
        'The command.commandtype does not have to be set since it defaults to text
        mCommand = New SqlCommand(SQL, mConnection)
        'We need to open the connection for the DataReader explicitly
        mConnection.Open()

        Try
            'Run the Execute Reader method of the Command Object
            mDataReader = mCommand.ExecuteReader(CommandBehavior.KeyInfo Or CommandBehavior.SchemaOnly)
            dtSchema = mDataReader.GetSchemaTable
            Return dtSchema
        Finally
            mConnection.Close()
        End Try

    End Function
#End Region

#Region "Stored Procedure Methods"

#Region "RunParamSQLScalar_SP - Parameterized Stored Procedure is run with ExecuteScalar"
    'The RunParamSQLScalar function accepts a SQL statement that is required
    Public Function RunParamSQLScalar_SP(ByVal SQL As String) As Object


        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As SqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        'Check to see if this object has already been disposed
        If mDisposedBoolean = True Then
            Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
        End If

        'Set a new connection
        mConnection = New SqlConnection(mConnectionString)

        'Define the command object and set command type to process Stored Procedure
        mCommand = New SqlCommand(SQL, mConnection)
        mCommand.CommandType = CommandType.StoredProcedure
        mCommand.CommandTimeout = mCommandTimeout

        'Move through the privateParameterList with the help of the enumerator
        Do While (usedEnumerator.MoveNext())
            'prUsedParameter = Nothing
            'Get parameter in privateParameterList
            prUsedParameter = usedEnumerator.Current
            'Convert paramter to SQLParameter
            prParameter = ConvertParameters(prUsedParameter)
            'Add converted parameter to the privateCommand object that imports data through the DataAdapter
            mCommand.Parameters.Add(prParameter)
        Loop

        mConnection.Open()
        Try
            mObject = mCommand.ExecuteScalar
            Return mObject

        Finally
            mConnection.Close()
        End Try
    End Function
    Public Function RunParamSQLScalar_SP_WithOutput(ByVal SQL As String) As Object


        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As SqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        'Check to see if this object has already been disposed
        If mDisposedBoolean = True Then
            Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
        End If

        'Set a new connection
        mConnection = New SqlConnection(mConnectionString)

        'Define the command object and set command type to process Stored Procedure
        mCommand = New SqlCommand(SQL, mConnection)
        mCommand.CommandType = CommandType.StoredProcedure
        mCommand.CommandTimeout = mCommandTimeout

        'Move through the privateParameterList with the help of the enumerator
        Do While (usedEnumerator.MoveNext())
            'prUsedParameter = Nothing
            'Get parameter in privateParameterList
            prUsedParameter = usedEnumerator.Current
            'Convert paramter to SQLParameter
            prParameter = ConvertParameters(prUsedParameter)
            'Add converted parameter to the privateCommand object that imports data through the DataAdapter
            mCommand.Parameters.Add(prParameter)
        Loop

        mConnection.Open()
        Try
            mObject = mCommand.ExecuteScalar

            Dim outputParams = New Dictionary(Of String, SqlParameter)
            For Each p As SqlParameter In mCommand.Parameters
                If (p.Direction = ParameterDirection.Output) Then
                    outputParams.Add(p.ParameterName,p)
                End If
            Next
            mOutputParameters = outputParams
            Return mObject

        Finally
            mConnection.Close()
        End Try
    End Function

    Public Function RunParamSQLScalar_SP(ByVal SQL As String, ByVal ReturnType As SqlDbType) As Object

        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As SqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        'Check to see if this object has already been disposed
        If mDisposedBoolean = True Then
            Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
        End If

        'Set a new connection
        mConnection = New SqlConnection(mConnectionString)

        'Define the command object and set command type to process Stored Procedure
        mCommand = New SqlCommand(SQL, mConnection)
        mCommand.CommandType = CommandType.StoredProcedure
        mCommand.CommandTimeout = mCommandTimeout

        'Move through the privateParameterList with the help of the enumerator
        Do While (usedEnumerator.MoveNext())
            'Get parameter in privateParameterList
            prUsedParameter = usedEnumerator.Current
            'Convert parameter to SQLParameter
            prParameter = ConvertParameters(prUsedParameter)
            'Add converted parameter to the privateCommand object that imports data through the DataAdapter
            mCommand.Parameters.Add(prParameter)
        Loop

        mConnection.Open()
        Try

            If ReturnType = SqlDbType.Int Then
                mObject = Convert.ToInt64(mCommand.ExecuteScalar)
            Else
                mObject = mCommand.ExecuteScalar
            End If

            Return mObject
        Finally
            mConnection.Close()
        End Try
    End Function
#End Region

#Region "RunParamSQLDataSet_SP - Parameterized SQL is run and returns a DataSet"
    'The RunParamSQLDataSet function accepts a SQL statement that is required and an optinal table name
    Public Function RunParamSQLDataSet_SP(ByVal SQL As String, Optional ByVal TableName As String = Nothing) As DataSet

        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As SqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
            End If

            'Set a new connection and DataSet
            mConnection = New SqlConnection(mConnectionString)
            Dim prDataSet As New DataSet

            'Define the command object and set commandtype to process Stored Procedure
            mCommand = New SqlCommand(SQL, mConnection)
            mCommand.CommandType = CommandType.StoredProcedure
            mCommand.CommandTimeout = mCommandTimeout

            'Move through the privateParameterList with the help of the enumerator
            Do While (usedEnumerator.MoveNext())
                'prUsedParameter = Nothing
                'Get parameter in privateParameterList
                prUsedParameter = usedEnumerator.Current
                'Convert paramter to SQLParameter
                prParameter = ConvertParameters(prUsedParameter)
                'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                mCommand.Parameters.Add(prParameter)
            Loop
            'Have the DataAdapter run the Stored Procedure
            mDataAdapter = New SqlDataAdapter(mCommand)
            'Depending on table name passed, create the DataSet with or without specifically naming the table
            If TableName = Nothing Then
                mDataAdapter.Fill(prDataSet)
            Else
                mDataAdapter.Fill(prDataSet, TableName)
            End If

            Return prDataSet

        Catch exceptionObject As Exception
            'An exception will be logged thorugh our private logexception funciton
            'LogException(ExceptionObject)
            'The exception is passed to the calling code
            Throw New Exception(M_EXCEPTION_MESSAGE, exceptionObject)
        Finally
            'Always close the connection as soon as possible(only then will object be allowed to go out of scope)
            mConnection.Close()
        End Try
    End Function

#End Region

#Region "RunParamSQLDataReader_SP - Parameterized SQL is run and returns a DataReader"
    'The RunParamSQLDataReader function accepts a SQL statement that is requirede
    Public Function RunParamSQLDataReader_SP(ByVal sql As String) As SqlDataReader
        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As SqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        'Check to see if this object has already been disposed
        If mDisposedBoolean = True Then
            Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
        End If

        'Set a new connection and DataSet
        mConnection = New SqlConnection(mConnectionString)

        'Define the command object and set command type to process Stored Procedure
        mCommand = New SqlCommand(sql, mConnection)
        mCommand.CommandType = CommandType.StoredProcedure
        mCommand.CommandTimeout = mCommandTimeout

        'Move through the privateParameterList with the help of the enumerator
        Do While (usedEnumerator.MoveNext())
            'prUsedParameter = Nothing
            'Get parameter in privateParameterList
            prUsedParameter = usedEnumerator.Current
            'Convert parameter to SQLParameter
            prParameter = ConvertParameters(prUsedParameter)
            'Add converted parameter to the privateCommand object that imports data through the DataAdapter
            mCommand.Parameters.Add(prParameter)
        Loop

        mConnection.Open()

        mDataReader = mCommand.ExecuteReader

        Return mDataReader
    End Function
#End Region

#Region "RunParamSQLDataAdapter_SP - Parameterized SQL is run and returns a DataAdapter"
    'The RunParamSQLDataSet function accepts a SQL statement that is required
    Public Function RunParamSQLDataAdapter_SP(ByVal SQL As String) As SqlDataAdapter

        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As SqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
            End If

            'This method should be used in conjunction with the constructor that 
            'specifies DataAdapter and opens the connection. At this point the connection
            'should be already opened.

            'Define the command object and set command type to process Stored Procedure
            mCommand = New SqlCommand(SQL, mConnection)
            mCommand.CommandType = CommandType.StoredProcedure
            mCommand.CommandTimeout = mCommandTimeout

            'Move through the privateParameterList with the help of the enumerator
            Do While (usedEnumerator.MoveNext())
                'Get parameter in privateParameterList
                prUsedParameter = usedEnumerator.Current
                'Convert paramter to SQLParameter
                prParameter = ConvertParameters(prUsedParameter)
                'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                mCommand.Parameters.Add(prParameter)
            Loop
            'Have the DataAdapter run the SQL Statement
            mDataAdapter = New SqlDataAdapter(mCommand)

            Return mDataAdapter

        Catch exceptionObject As Exception
            'An exception will be logged through our private loge exception function
            'LogException(ExceptionObject)
            'The exception is passed to the calling code
            Throw New Exception(M_EXCEPTION_MESSAGE, exceptionObject)
        Finally
            'Always close the connection as soon as possible(only then will object be allowed to go out of scope)
            mConnection.Close()
        End Try
    End Function
#End Region

#Region "Execute a none returning parameterized query"
    Public Sub RunParamSQLExecuteNoneQuery_SP(ByVal SQL As String, Optional ByVal tran As SqlTransaction = Nothing)

        'Setting the objects to handle parameters
        Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
        Dim prParameter As SqlParameter            'will contain the converted SQLParameter
        'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
        Dim usedEnumerator As IEnumerator = mParameterList.GetEnumerator()

        Try
            'Check to see if this object has already been disposed
            If mDisposedBoolean = True Then
                Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it")
            End If

            'Set a new connection and DataSet
            If tran Is Nothing Then
                mConnection = New SqlConnection(mConnectionString)
            End If

            'Define the command object and set commandtype to process Stored Procedure
            mCommand = New SqlCommand(SQL, mConnection)
            mCommand.CommandType = CommandType.StoredProcedure
            mCommand.CommandTimeout = mCommandTimeout
            If Not (tran Is Nothing) Then
                mCommand.Transaction = tran
            End If
            'Move through the privateParameterList wiht the help of the enumerator
            Do While (usedEnumerator.MoveNext())
                'Get parameter in privateParameterList
                prUsedParameter = usedEnumerator.Current
                'Convert paramter to SQLParameter
                prParameter = ConvertParameters(prUsedParameter)
                'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                mCommand.Parameters.Add(prParameter)
            Loop

            If tran Is Nothing Then
                mConnection.Open()
            End If
            mCommand.ExecuteNonQuery()

        Finally
            If tran Is Nothing Then
                mConnection.Close()
            End If
        End Try
    End Sub
#End Region

#Region "RunSchemaInfo_SP - Runs a SQL regular statement and returns a Datatable"
    'The RunSchemaInfoForAuditing function accepts a SQL statement that is required
    Public Function RunSchemaInfo_SP(ByVal SQL As String) As DataTable
        Dim dtSchema As DataTable


        'We include all called object in the try block to catch any exceptions that could occur ( even in the creation of the connection)
        'Check to see if this object has already been disposed
        If mDisposedBoolean = True Then
            Throw New ObjectDisposedException(mModuleName, "This object has already been disposed. You cannot reuse it.")
        End If

        'Set a new Connection.
        mConnection = New SqlConnection(mConnectionString)

        'Set a new Command that accepts an SQL statement and the connection. 
        'The command.commandtype does not have to be set since it defaults to text
        mCommand = New SqlCommand(SQL, mConnection)
        mCommand.CommandType = CommandType.StoredProcedure
        mCommand.CommandTimeout = mCommandTimeout
        'We need to open the connection for the DataReader explicitly
        mConnection.Open()
        Try
            'Run the Execute Reader method of the Command Object
            mDataReader = mCommand.ExecuteReader(CommandBehavior.KeyInfo Or CommandBehavior.SchemaOnly)
            dtSchema = mDataReader.GetSchemaTable
            Return dtSchema
        Finally
            mConnection.Close()
        End Try

    End Function
#End Region

#End Region

#Region "AddParameter Public Method and Support functions"

    Public Sub AddParameter(ByVal parameterName As String,
                            Optional ByVal Value As Object = Nothing,
                            Optional ByVal SQLType As SqlDbType = Nothing,
                            Optional ByVal Size As Integer = Nothing,
                            Optional ByVal Direction As ParameterDirection = ParameterDirection.Input)

        'Dim buildDataType As SqlDBType
        Dim buildParameter As Parameter


        buildParameter = New Parameter(parameterName, Value, SQLType, Size, Direction)
        mParameterList.Add(buildParameter)

    End Sub

    Public Class Parameter
        Public ParameterName As String
        Public ParameterValue As Object
        Public ParameterDataType As SqlDbType
        Public ParameterSize As Integer
        Public ParameterDirectionUsed As ParameterDirection

        Sub New(ByVal passedParameterName As String,
                Optional ByVal passedValue As Object = Nothing,
                Optional ByVal passedSqlType As SqlDbType = Nothing,
                Optional ByVal passedSize As Integer = Nothing,
                Optional ByVal passedDirection As ParameterDirection = ParameterDirection.Input)

            ParameterName = passedParameterName
            ParameterValue = passedValue
            ParameterDataType = passedSqlType
            ParameterSize = passedSize
            ParameterDirectionUsed = passedDirection

        End Sub

    End Class

    Private Function ConvertParameters(passedParameter As Parameter) As SqlParameter

        Dim returnSqlParameter = New SqlParameter

        returnSqlParameter.ParameterName = passedParameter.ParameterName
        returnSqlParameter.Value = passedParameter.ParameterValue
        returnSqlParameter.SqlDbType = passedParameter.ParameterDataType
        returnSqlParameter.Size = passedParameter.ParameterSize
        returnSqlParameter.Direction = passedParameter.ParameterDirectionUsed

        Return returnSqlParameter

    End Function

    Public Sub ClearParameters()
        Try
            mParameterList.Clear()
        Catch parameterException As Exception
            Throw New Exception(M_EXCEPTION_MESSAGE & " Parameter List did not clear", parameterException)
        End Try
    End Sub

#End Region

#Region "Exception Logging"
    'We call our event log by passing it the exception we caught
    'Private Sub LogException(ByRef ExceptionObject As Exception)

    '    Dim EventLogMessage As String           'this is the Message we will pass to the log

    '    Try
    '        'Create the Message to be passed from the exception 
    '        EventLogMessage = "An error occured in the following module: " & mModuleName & _
    '                          " The Source was: " & ExceptionObject.Source & vbCrLf & _
    '                          " With the Message: " & ExceptionObject.Message & vbCrLf & _
    '                          " Stack Tace: " & ExceptionObject.StackTrace & vbCrLf & _
    '                          " Target Site: " & ExceptionObject.TargetSite.ToString
    '        'Define the Eventlog as an Application Log entry
    '        Dim localEventLog As New EventLog("Application")
    '        'Write the entry to the Application Event log, using this Module's name, the message an make it an error message with an ID of 55
    '        EventLog.WriteEntry(mModuleName, EventLogMessage, EventLogEntryType.Error, 55)
    '    Catch EventLogException As Exception
    '        'If the eventlog fails (like forgetting to make the ASPNET user account a member of the debugger group, we pass the error
    '        Throw New Exception(M_EXCEPTION_MESSAGE & " - EventLog Error: " & EventLogException.Message, EventLogException)
    '    End Try

    'End Sub

#End Region

#Region "Validations"
    Private Sub ValidateSQLStatement(ByRef SQLStatement As String)
        'SQL Statement must be at least 10 characters ( "Select * form x" )
        If Len(SQLStatement) < 10 Then
            Throw New Exception(M_EXCEPTION_MESSAGE & " The SQL Statement must be provided and at least 10 characters long")
        End If
    End Sub

#End Region

#Region "Overloaded Dispose and Finalize"
    Public Sub Dispose() Implements IDisposable.Dispose
        If (Not mDisposedBoolean) Then
            'Call cleanup code in Finalize
            finalize()

            'Record that object has been disposed
            mDisposedBoolean = True

            'Finalize does not need to be called.
            GC.SuppressFinalize(Me)
        End If
    End Sub
    Protected Overrides Sub finalize()
        'Perform cleanup code here
        If Not mConnection Is Nothing Then
            'm_Connection.Dispose()
            mConnection = Nothing
        End If
    End Sub
#End Region
#End Region


End Class