Imports FAME.Advantage.Common

Public Class CourseGrpsDB
    Public Function GetCourseGrps() As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT t1.ReqId,t1.Descrip, t2.ReqTypeId, t1.StatusId  ")
            .Append(" ,(select Distinct Status from syStatuses where StatusId=t1.StatusId) as Status ")
            .Append("FROM arReqs t1, arReqTypes t2 ")
            .Append("WHERE(t1.ReqTypeId = t2.ReqTypeId And t2.IsGroup = 1 And t2.ReqTypeId = 2) ")
            .Append("ORDER BY t1.Descrip")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetCourseGrpDetails(ByVal CourseGrpId As String) As CourseGrpInfo

        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select ReqId,Code,Descrip,Credits,Hours,StatusId,CampGrpId,ReqTypeId,ModUser,ModDate,(select Distinct CampGrpDescrip from syCampGrps where CampGrpId=arReqs.CampGrpId) as CampGrpDescrip from arReqs where ReqId=? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@CourseGrpID", CourseGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim CourseGrpObj As New CourseGrpInfo

        While dr.Read()
            With CourseGrpObj
                '.StudentId = dr("StudentId")
                CourseGrpObj.CourseGrpId = dr("ReqId")
                CourseGrpObj.CourseGrpCode = dr("Code").ToString
                CourseGrpObj.CourseGrpDescrip = dr("Descrip").ToString
                If Not (dr("Credits") Is System.DBNull.Value) Then
                    CourseGrpObj.CourseGrpCredits = dr("Credits").ToString
                End If
                If Not (dr("Hours") Is System.DBNull.Value) Then
                    CourseGrpObj.CourseGrpHours = dr("Hours").ToString
                End If
                CourseGrpObj.CampGrpId = dr("CampGrpId")
                CourseGrpObj.StatusId = dr("StatusId")
                CourseGrpObj.ReqTypeId = dr("ReqTypeId")
                CourseGrpObj.CampGrpDescrip = dr("CampGrpDescrip")
                'get ModUser
                If Not (dr("ModDate") Is System.DBNull.Value) Then .ModDate = dr("ModDate") Else .ModDate = Date.MinValue

                .ModUser = dr("ModUser")
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return CourseGrpObj
    End Function

    Public Function InsertCourseGrp(ByVal CourseGrpObj As CourseGrpInfo, ByVal user As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim clssectid As String
        Dim StudentId As String
        Dim GrdBkWgtDetailId As String
        Dim score As Integer
        Dim Comments As String

        'clssectid = StdGrdObj.ClsSectId
        'StudentId = StdGrdObj.StudentId
        'GrdBkWgtDetailId = StdGrdObj.GrdBkWgtDetailId
        'score = StdGrdObj.Score
        'Comments = StdGrdObj.Comments

        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("INSERT INTO arReqs(ReqId,Code,Descrip,Credits,Hours,StatusId,CampGrpId,ReqTypeId,ModUser,ModDate) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?)")

                db.AddParameter("@courseid", CourseGrpObj.CourseGrpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                db.AddParameter("@code", CourseGrpObj.CourseGrpCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@descrip", CourseGrpObj.CourseGrpDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.AddParameter("@credits", CourseGrpObj.CourseGrpCredits, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@hours", CourseGrpObj.CourseGrpHours, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

                db.AddParameter("@statusid", CourseGrpObj.StatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

                db.AddParameter("@CampGrpId", CourseGrpObj.CampGrpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                db.AddParameter("@ReqTypeId", CourseGrpObj.ReqTypeId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)


            End With

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function UpdateCourseGrp(ByVal CourseGrpObj As CourseGrpInfo, ByVal user As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim clssectid As String
        Dim StudentId As String
        Dim GrdBkWgtDetailId As String
        Dim score As Integer
        Dim Comments As String


        'clssectid = StdGrdObj.ClsSectId
        'StudentId = StdGrdObj.StudentId
        'GrdBkWgtDetailId = StdGrdObj.GrdBkWgtDetailId
        'score = StdGrdObj.Score
        'Comments = StdGrdObj.Comments

        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("UPDATE arReqs Set Code = ?,Descrip = ?,Credits = ?,Hours = ?,StatusId = ?,CampGrpId = ?,ReqTypeId = ?,ModUser=?,ModDate=? ")
                .Append("WHERE  ReqId = ? ")

                db.AddParameter("@code", CourseGrpObj.CourseGrpCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@descrip", CourseGrpObj.CourseGrpDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.AddParameter("@credits", CourseGrpObj.CourseGrpCredits, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@hours", CourseGrpObj.CourseGrpHours, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@statusid", CourseGrpObj.StatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

                db.AddParameter("@CampGrpId", CourseGrpObj.CampGrpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
                db.AddParameter("@ReqTypeId", CourseGrpObj.ReqTypeId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@courseid", CourseGrpObj.CourseGrpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

            End With

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function DeleteCourseGrp(ByVal CourseGrpId As String, ByVal modDate As DateTime) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim clssectid As String
        Dim StudentId As String
        Dim GrdBkWgtDetailId As String
        Dim score As Integer
        Dim Comments As String


        'clssectid = StdGrdObj.ClsSectId
        'StudentId = StdGrdObj.StudentId
        'GrdBkWgtDetailId = StdGrdObj.GrdBkWgtDetailId


        'Set the connection string
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("DELETE FROM arReqs ")
                .Append("WHERE ReqId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM arReqs WHERE ReqId = ? ")

            End With

            db.AddParameter("@coursegrpid", CourseGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@coursegrpid2", CourseGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            'db.RunParamSQLExecuteNoneQuery(sb.ToString)
            'db.ClearParameters()
            'sb.Remove(0, sb.Length)

            'Catch ex As OleDbException
            '   return an error to the client
            ' Return DALExceptions.BuildErrorMessage(ex)

            'Finally
            'Close Connection
            'db.CloseConnection()
            'End Try

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function GetCourseGrpName(ByVal courseGrpId As String) As String
        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT Descrip ")
            .Append("FROM arReqs ")
            .Append("WHERE ReqId = ?")
        End With

        db.AddParameter("@CourseGrpID", courseGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Return DirectCast(db.RunParamSQLScalar(sb.ToString), String)
    End Function
#Region "Get AdvAppsetting for Manage Config entry"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
End Class
