Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' CreditCardTypesDB.vb
'
' CreditCardTypesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class CreditCardTypesDB
    Public Function GetAllCreditCardTypes(ByVal showActiveOnly As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   CCT.CreditCardTypeId, CCT.StatusId, CCT.CreditCardTypeCode, CCT.CreditCardTypeDescrip,ST.StatusId,ST.Status ")
            .Append("FROM     saCreditCardTypes CCT, syStatuses ST ")
            .Append("WHERE    CCT.StatusId = ST.StatusId ")
            If showActiveOnly = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                .Append("ORDER BY CCT.CreditCardTypeDescrip ")
            ElseIf showActiveOnly = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                .Append("ORDER BY CCT.CreditCardTypeDescrip ")
            Else
                .Append("ORDER BY ST.Status,CCT.CreditCardTypeDescrip asc")
            End If
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetCreditCardTypeInfo(ByVal CreditCardTypeId As String) As CreditCardTypeInfo

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT CCT.CreditCardTypeId, ")
            .Append("    CCT.CreditCardTypeCode, ")
            .Append("    CCT.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=CCT.StatusId) As Status, ")
            .Append("    CCT.CreditCardTypeDescrip, ")
            .Append("    CCT.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=CCT.CampGrpId) As CampGrpdescrip, ")
            .Append("    CCT.ModUser, ")
            .Append("    CCT.ModDate ")
            .Append("FROM  saCreditCardTypes CCT ")
            .Append("WHERE CCT.CreditCardTypeId= ? ")
        End With

        ' Add the CreditCardTypeId to the parameter list
        db.AddParameter("@CreditCardTypeId", CreditCardTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim creditCardTypeInfo As New CreditCardTypeInfo

        While dr.Read()

            '   set properties with data from DataReader
            With creditCardTypeInfo
                .CreditCardTypeId = CreditCardTypeId
                .IsInDB = True
                .Code = dr("CreditCardTypeCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("CreditCardTypeDescrip")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpdescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return creditCardTypeInfo

    End Function
    Public Function UpdateCreditCardTypeInfo(ByVal CreditCardTypeInfo As CreditCardTypeInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saCreditCardTypes Set CreditCardTypeId = ?, CreditCardTypeCode = ?, ")
                .Append(" StatusId = ?, CreditCardTypeDescrip = ?, CampGrpId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE CreditCardTypeId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from saCreditCardTypes where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   CreditCardTypeId
            db.AddParameter("@CreditCardTypeId", CreditCardTypeInfo.CreditCardTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CreditCardTypeCode
            db.AddParameter("@CreditCardTypeCode", CreditCardTypeInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", CreditCardTypeInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CreditCardTypeDescrip
            db.AddParameter("@CreditCardTypeDescrip", CreditCardTypeInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If CreditCardTypeInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", CreditCardTypeInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   CreditCardTypeId
            db.AddParameter("@AdmDepositId", CreditCardTypeInfo.CreditCardTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", CreditCardTypeInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddCreditCardTypeInfo(ByVal CreditCardTypeInfo As CreditCardTypeInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saCreditCardTypes (CreditCardTypeId, CreditCardTypeCode, StatusId, ")
                .Append("   CreditCardTypeDescrip, CampGrpId, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   CreditCardTypeId
            db.AddParameter("@CreditCardTypeId", CreditCardTypeInfo.CreditCardTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CreditCardTypeCode
            db.AddParameter("@CreditCardTypeCode", CreditCardTypeInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", CreditCardTypeInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CreditCardTypeDescrip
            db.AddParameter("@CreditCardTypeDescrip", CreditCardTypeInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If CreditCardTypeInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", CreditCardTypeInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteCreditCardTypeInfo(ByVal CreditCardTypeId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saCreditCardTypes ")
                .Append("WHERE CreditCardTypeId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("Select count(*) from saCreditCardTypes where CreditCardTypeId = ? ")
            End With

            '   add parameters values to the query

            '   CreditCardTypeId
            db.AddParameter("@CreditCardTypeId", CreditCardTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   CreditCardTypeId
            db.AddParameter("@CreditCardTypeId", CreditCardTypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
#Region "Get AdvAppsetting for Manage Config entry"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
End Class
