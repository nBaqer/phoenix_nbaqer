Imports FAME.Advantage.Common

Public Class LeadStatusChangesDB
    Public Function GetLeadStatusChangesDS() As DataSet

        '   create dataset
        Dim ds As New DataSet

        '   build the sql query for the LeadStatusChanges data adapter
        '   retrieve rows where both OrigStatusId and NewStatusId are active
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        With sb
            .Append("SELECT ")
            .Append("       A.LeadStatusChangeId,")
            .Append("       A.OrigStatusId,")
            .Append("       (SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=A.OrigStatusId) AS OrigStatusDescrip,")
            .Append("       A.NewStatusId,")
            .Append("       (SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=A.NewStatusId) AS NewStatusDescrip,")
            .Append("       A.CampGrpId,")
            .Append("       (SELECT CampGrpDescrip FROM syCampGrps WHERE CampGrpId=A.CampGrpId) AS CampGrpDescrip,")
            .Append("       A.IsReversal,")
            .Append("       A.ModDate,")
            .Append("       A.ModUser ")
            .Append("FROM   syLeadStatusChanges A ")
            '',syStatusCodes B1,syStatusCodes B2,syStatuses C1,syStatuses C2
            ''.Append("WHERE  B1.StatusCodeId=A.OrigStatusId ")
            ''.Append("       AND B2.StatusCodeId=A.NewStatusId ")
            ''If leadStatusId <> "" Then
            ''    .Append("       AND B1.StatusId=C1.StatusId AND (C1.Status='Active' OR A.LeadStatusId=?) ")
            ''    .Append("       AND B2.StatusId=C2.StatusId AND (C2.Status='Active' OR A.LeadStatusId =?) ")
            ''Else
            ''    .Append("       AND B1.StatusId=C1.StatusId AND C1.Status='Active' ")
            ''    .Append("       AND B2.StatusId=C2.StatusId AND C2.Status='Active' ")
            ''End If
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

        ''If leadStatusId <> "" Then
        ''    sc.Parameters.Add(New OleDbParameter("@LeadStatusId", leadStatusId))
        ''End If

        '   Create adapter to handle StudentAwardSchedules table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill StudentAwardSchedules table
        da.Fill(ds, "LeadStatusChanges")



        '   build the sql query for the Roles data adapter
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       A.RoleId,")
            .Append("       A.Role AS RoleDescrip,")
            .Append("       A.Code,")
            .Append("       A.StatusId,")
            .Append("       (SELECT Status FROM syStatuses WHERE StatusId=A.StatusId) AS Status ")
            .Append("FROM   syRoles A,sySysRoles B ")
            .Append("WHERE  A.sysRoleId=B.SysRoleId ")
            .Append("       AND B.Descrip='Admission Reps' ")
            .Append("ORDER BY RoleDescrip")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   Fill Roles table
        da.Fill(ds, "Roles")



        '   build the sql query for the Statuses data adapter
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       StatusId,")
            .Append("       Status  ")
            .Append("FROM   syStatuses ")
            .Append("ORDER BY Status")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   Fill Roles table
        da.Fill(ds, "Statuses")



        '   build select query for the LeadStatusChangePermissions data adapter
        '   build the sql query
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       A.LeadStatusChangePermissionId,")
            .Append("       A.LeadStatusChangeId,")
            .Append("       A.RoleId,")
            .Append("       C.Role AS RoleDescrip,")
            .Append("       A.StatusId,")
            .Append("       (SELECT Status FROM syStatuses WHERE StatusId=A.StatusId) AS Status,")
            .Append("       A.ModDate,")
            .Append("       A.ModUser ")
            .Append("FROM   syLeadStatusChangePermissions A,sySysRoles B,syRoles C ")
            .Append("WHERE  A.RoleId=C.RoleId ")
            .Append("       AND B.Descrip='Admission Reps' ")
            .Append("       AND C.sysRoleId=B.SysRoleId ")
            ''.Append("       AND EXISTS (SELECT * ")
            ''.Append("                   FROM syLeadStatusChanges L,syStatusCodes B1,syStatusCodes B2,syStatuses C1,syStatuses C2 ")
            ''.Append("                   WHERE L.leadStatusChangeId=A.LeadStatusChangeId ")
            ''.Append("                   AND B1.StatusCodeId=L.OrigStatusId AND B2.StatusCodeId=L.NewStatusId ")
            ''If leadStatusId <> "" Then
            ''    .Append("               AND B1.StatusId=C1.StatusId AND (C1.Status='Active' OR L.LeadStatusId=?) ")
            ''    .Append("               AND B2.StatusId=C2.StatusId AND (C2.Status='Active' OR L.LeadStatusId =?)) ")
            ''Else
            ''    .Append("               AND B1.StatusId=C1.StatusId AND C1.Status='Active' ")
            ''    .Append("               AND B2.StatusId=C2.StatusId AND C2.Status='Active') ")
            ''End If

            ''.Append("                   AND B1.StatusId=C1.StatusId AND C1.Status='Active' ")
            ''.Append("                   AND B2.StatusId=C2.StatusId AND C2.Status='Active') ")
            .Append("ORDER BY Status,RoleDescrip")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        ''If leadStatusId <> "" Then
        ''    da.SelectCommand.Parameters.Add(New OleDbParameter("@LeadStatusId", leadStatusId))
        ''End If

        '   fill LeadStatusChangePermissions table
        da.Fill(ds, "LeadStatusChangePermissions")



        '   create primary and foreign key constraints

        '   set primary key for LeadStatusChanges table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("LeadStatusChanges").Columns("LeadStatusChangeId")
        ds.Tables("LeadStatusChanges").PrimaryKey = pk0

        '   set primary key for Roles table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("Roles").Columns("RoleId")
        ds.Tables("Roles").PrimaryKey = pk1

        '   set primary key for Statuses table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("Statuses").Columns("StatusId")
        ds.Tables("Statuses").PrimaryKey = pk2

        '   set primary key for LeadStatusChangePermissions table
        Dim pk3(0) As DataColumn
        pk3(0) = ds.Tables("LeadStatusChangePermissions").Columns("LeadStatusChangePermissionId")
        ds.Tables("LeadStatusChangePermissions").PrimaryKey = pk3

        '   set foreign key column in LeadStatusChangePermissions
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("LeadStatusChangePermissions").Columns("LeadStatusChangeId")

        '   add relationship
        ds.Relations.Add("LeadStatusChangesLeadStatusChangePermissions", pk0, fk0)

        '   set foreign key column in LeadStatusChangePermissions
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("LeadStatusChangePermissions").Columns("RoleId")

        '   add relationship
        ds.Relations.Add("RolesLeadStatusChangePermissions", pk1, fk1)

        '   set foreign key column in LeadStatusChangePermissions
        Dim fk2(0) As DataColumn
        fk2(0) = ds.Tables("LeadStatusChangePermissions").Columns("StatusId")

        '   add relationship
        ds.Relations.Add("StatusesLeadStatusChangePermissions", pk2, fk2)

        '   return dataset
        Return ds
    End Function
    Public Function UpdateLeadStatusChangesDS(ByVal ds As DataSet) As String

        '   all updates must be encapsulated as one transaction

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build the sql query for the adLeadGrpReqGroups data adapter
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       LeadStatusChangePermissionId,")
                .Append("       LeadStatusChangeId,")
                .Append("       RoleId,")
                .Append("       StatusId,")
                .Append("       ModDate,")
                .Append("       ModUser ")
                .Append("FROM   syLeadStatusChangePermissions A ")
            End With

            '   build select command
            Dim LeadStatusChangePermissionsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle LeadGrpReqGroups table
            Dim LeadStatusChangePermissionsDataAdapter As New OleDbDataAdapter(LeadStatusChangePermissionsSelectCommand)

            '   build insert, update and delete commands for LeadGrpReqGroups table
            Dim cb As New OleDbCommandBuilder(LeadStatusChangePermissionsDataAdapter)



            '   build select query for the ReqGroups data adapter
            sb = New StringBuilder
            With sb
                '   with subqueries
                .Append("SELECT ")
                .Append("       LeadStatusChangeId,")
                .Append("       OrigStatusId,")
                .Append("       NewStatusId,")
                .Append("       CampGrpId,")
                .Append("       IsReversal,")
                .Append("       ModDate,")
                .Append("       ModUser ")
                .Append("FROM   syLeadStatusChanges ")
            End With

            '   build select command
            Dim LeadStatusChangesSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle StudentAwards table
            Dim LeadStatusChangesDataAdapter As New OleDbDataAdapter(LeadStatusChangesSelectCommand)

            '   build insert, update and delete commands for ReqGroups table
            Dim cb1 As New OleDbCommandBuilder(LeadStatusChangesDataAdapter)



            '   insert added rows in LeadStatusChanges table
            LeadStatusChangesDataAdapter.Update(ds.Tables("LeadStatusChanges").Select(Nothing, Nothing, DataViewRowState.Added))

            '   insert added rows in LeadGrpReqGrps table
            LeadStatusChangePermissionsDataAdapter.Update(ds.Tables("LeadStatusChangePermissions").Select(Nothing, Nothing, DataViewRowState.Added))

            '   delete rows in LeadGrpReqGrps table
            'start DE13822:QA: User is not able to delete a record in the Set Up Lead Status Sequence & Permissions page.
            'cascade delete
            If (ds.Tables("LeadStatusChangePermissions").Select(Nothing, Nothing, DataViewRowState.Deleted)).Any() Then
                Dim deleteDs = New DataSet()
                LeadStatusChangePermissionsDataAdapter.Fill(deleteDs)
                Dim rowDelete() As DataRow = deleteDs.Tables(0).Select("LeadStatusChangeId=" + "'" + CType(HttpContext.Current.Session("LeadStatusChangeId"), String) + "'")
                For Each r As DataRow In rowDelete
                    r.Delete()
                Next
                'LeadStatusChangePermissionsDataAdapter.Update(ds.Tables("LeadStatusChangePermissions").Select(Nothing, Nothing, DataViewRowState.Deleted))
                LeadStatusChangePermissionsDataAdapter.Update(deleteDs.Tables(0).Select(Nothing, Nothing, DataViewRowState.Deleted))
                '   delete rows in LeadStatusChanges table
                LeadStatusChangesDataAdapter.Update(ds.Tables("LeadStatusChanges").Select(Nothing, Nothing, DataViewRowState.Deleted))
            End If
            'end DE13822:QA: User is not able to delete a record in the Set Up Lead Status Sequence & Permissions page.
            '   update rows in LeadGrpReqGrps table
            LeadStatusChangePermissionsDataAdapter.Update(ds.Tables("LeadStatusChangePermissions").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   update rows in LeadStatusChanges table
            LeadStatusChangesDataAdapter.Update(ds.Tables("LeadStatusChanges").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Catch ex As Exception
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return ex.Message

        Finally

            '   close connection
            connection.Close()
        End Try
    End Function
    Public Function GetLeadStatusForCampGroup(ByVal campGrpId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       A.StatusCodeID as StatusCodeID,")
            .Append("       A.StatusCodeDescrip as StatusCodeDescrip ")
            .Append("FROM   syStatusCodes A,sySysStatus B,syStatuses C,syStatusLevels D ")
            .Append("WHERE  A.sysStatusID=B.sysStatusID ")
            .Append("       AND A.StatusID=C.StatusID ")
            .Append("       AND B.StatusLevelId=D.StatusLevelId ")
            .Append("       AND C.Status='Active' ")
            .Append("       AND D.StatusLevelId=1 ")
            .Append("       AND A.CampGrpId=? ")
            .Append("ORDER BY A.StatusCodeDescrip ")
        End With

        db.AddParameter("@CampGrpId", campGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetNewLeadStatuses(ByVal campusId As String, ByVal userId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        '   Dim IsSAOrDirectorOfAdmissions As Boolean = (New UserSecurityDB).IsSAOrDirectorOfAdmissions(userId)
        Dim dsGetCmpGrps As New DataSet
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        With sb
            .Append("	SELECT Distinct StatusCodeId,StatusCodeDescrip,CampGrpId FROM syStatusCodes WHERE ")
            ' .Append("	SysStatusId=1 and StatusId='" & strActiveGUID & "' and CampGrpId in ('")
            .Append("	SysStatusId in (SELECT SysStatusID FROM dbo.sySysStatus WHERE StatusLevelId=1 AND SysStatusId NOT IN (6,17,18)) and StatusId='" & strActiveGUID & "' and CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
        End With




        ''Modified By Balaji
        'With sb
        '    .Append("	SELECT Distinct GetNewLeadStatusByCampus.StatusCodeId, ")
        '    .Append("					GetNewLeadStatusByCampus.StatusCodeDescrip, ")
        '    .Append("					GetNewLeadStatusByCampus.CampGrpId	")
        '    .Append("	FROM ")
        '    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '    '	This Table Query Returns The Lead Status Based on the Campus and Statuses mapped to "NEW LEAD STATUS"
        '    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '    .Append(" (	")
        '    .Append("	SELECT Distinct StatusCodeId,StatusCodeDescrip,CampGrpId FROM syStatusCodes WHERE ")
        '    ' .Append("	SysStatusId=1 and StatusId='" & strActiveGUID & "' and CampGrpId in ('")
        '    .Append("	SysStatusId in (SELECT SysStatusID FROM dbo.sySysStatus WHERE StatusLevelId=1 AND SysStatusId NOT IN (6,17,18)) and StatusId='" & strActiveGUID & "' and CampGrpId in ('")
        '    .Append(strCampGrpId)
        '    .Append(") ")
        '    .Append(" ) GetNewLeadStatusByCampus, ")
        '    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '    '	This Table Query Returns The Lead Status FROM LeadStatusChanges Table Based on the Campus
        '    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '    .Append(" (	")
        '    .Append(" SELECT Distinct LeadStatusChangeId,OrigStatusId,CampGrpId FROM syLeadStatusChanges ")
        '    .Append(" WHERE CampGrpId in 	('")
        '    .Append(strCampGrpId)
        '    .Append(") ")
        '    .Append(" ) GetLeadStatusByCampus ")
        '    If IsSAOrDirectorOfAdmissions = False Then
        '        .Append(" ,syLeadStatusChangePermissions F,syUsersRolesCampGrps U ")
        '    End If
        '    .Append(" WHERE GetNewLeadStatusByCampus.StatusCodeId = GetLeadStatusByCampus.OrigStatusId	")
        '    .Append(" and GetNewLeadStatusByCampus.CampGrpId = GetLeadStatusByCampus.CampGrpID	")
        '    If IsSAOrDirectorOfAdmissions = False Then
        '        .Append(" and GetLeadStatusByCampus.LeadStatusChangeId = F.LeadStatusChangeId and ")
        '        .Append(" F.RoleId = U.RoleId and UserId='" & userId & "'  and F.StatusId='" & strActiveGUID & "'  ")
        '    End If
        'End With


        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetLeadStatusMappedToEnrolledStatus(ByVal userId As String, ByVal campusid As String) As DataSet
        Dim sb As New StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim IsSAOrDirectorOfAdmissions As Boolean = (New UserSecurityDB).IsSAOrDirectorOfAdmissions(userId)
        Dim dsGetCmpGrps As New DataSet
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusid)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        'Modified By Balaji
        With sb
            .Append("	SELECT Distinct GetNewLeadStatusByCampus.StatusCodeId, ")
            .Append("					GetNewLeadStatusByCampus.StatusCodeDescrip, ")
            .Append("					GetNewLeadStatusByCampus.CampGrpId	")
            .Append("	FROM ")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '	This Table Query Returns The Lead Status Based on the Campus and Statuses mapped to Lead Entity
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            .Append(" (	")
            .Append("	SELECT Distinct StatusCodeId,StatusCodeDescrip,CampGrpId FROM syStatusCodes t1,sySysStatus t2 WHERE ")
            .Append("   t1.SysStatusId=t2.SysStatusId and t2.StatusLevelId=1 and t1.StatusId='" & strActiveGUID & "' and t2.StatusId='" & strActiveGUID & "' and CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" ) GetNewLeadStatusByCampus, ")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '	This Table Query Returns The Lead Status FROM LeadStatusChanges Table Based on the Campus
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            .Append(" (	")
            .Append("   select Distinct t3.LeadStatusChangeId,t3.OrigStatusId,t3.CampGrpId from ")
            .Append("   syLeadStatusChanges t3,syStatusCodes t4 ")
            .Append("   WHERE t3.NewStatusId = t4.StatusCodeId and t4.SysStatusId=6 and ")
            .Append("    t3.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" ) GetLeadStatusByCampus ")
            If IsSAOrDirectorOfAdmissions = False Then
                .Append(" ,syLeadStatusChangePermissions F,syUsersRolesCampGrps U ")
            End If
            .Append(" WHERE GetNewLeadStatusByCampus.StatusCodeId = GetLeadStatusByCampus.OrigStatusId	")
            .Append(" and GetNewLeadStatusByCampus.CampGrpId = GetLeadStatusByCampus.CampGrpID	")
            If IsSAOrDirectorOfAdmissions = False Then
                .Append(" and GetLeadStatusByCampus.LeadStatusChangeId = F.LeadStatusChangeId and ")
                .Append(" F.RoleId = U.RoleId and UserId='" & userId & "'  and F.StatusId='" & strActiveGUID & "'  ")
            End If
        End With
        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetLeadStatusMappedToUnEnrolledStatus(ByVal userId As String, ByVal campusid As String) As DataSet
        Dim sb As New StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim IsSAOrDirectorOfAdmissions As Boolean = (New UserSecurityDB).IsSAOrDirectorOfAdmissions(userId)
        Dim dsGetCmpGrps As New DataSet
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusid)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        'Modified By Balaji
        With sb
            .Append("	SELECT Distinct GetNewLeadStatusByCampus.StatusCodeId, ")
            .Append("					GetNewLeadStatusByCampus.StatusCodeDescrip, ")
            .Append("					GetNewLeadStatusByCampus.CampGrpId	")
            .Append("	FROM ")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '	This Table Query Returns The Lead Status Based on the Campus and Statuses mapped to "ENROLLED" system status
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            .Append(" (	")
            .Append("	SELECT Distinct StatusCodeId,StatusCodeDescrip,CampGrpId FROM syStatusCodes t1,sySysStatus t2 WHERE ")
            .Append("   t1.SysStatusId=t2.SysStatusId and t2.StatusLevelId=1 and t1.StatusId='" & strActiveGUID & "' and t2.StatusId='" & strActiveGUID & "' and CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" ) GetNewLeadStatusByCampus, ")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '	This Table Query Returns The Lead Status FROM LeadStatusChanges Table Based on the Campus
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            .Append(" (	")
            .Append("   select Distinct t3.LeadStatusChangeId,t3.OrigStatusId,t3.NewStatusId,t3.CampGrpId from ")
            .Append("   syLeadStatusChanges t3,syStatusCodes t4 ")
            .Append("   WHERE t3.NewStatusId = t4.StatusCodeId and ")
            .Append("   t3.OrigStatusId in (select Distinct StatusCodeId from syStatusCodes where SysStatusId=6) AND ")
            .Append("   t3.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" ) GetLeadStatusByCampus ")
            If IsSAOrDirectorOfAdmissions = False Then
                .Append(" ,syLeadStatusChangePermissions F,syUsersRolesCampGrps U ")
            End If
            .Append(" WHERE GetNewLeadStatusByCampus.StatusCodeId = GetLeadStatusByCampus.NewStatusId	")
            .Append(" and GetNewLeadStatusByCampus.CampGrpId = GetLeadStatusByCampus.CampGrpID	")
            If IsSAOrDirectorOfAdmissions = False Then
                .Append(" and GetLeadStatusByCampus.LeadStatusChangeId = F.LeadStatusChangeId and ")
                .Append(" F.RoleId = U.RoleId and UserId='" & userId & "'  and F.StatusId='" & strActiveGUID & "'  ")
            End If
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetNewSearchLeadStatuses(ByVal campusId As String, ByVal userId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   get lead statuses that map to 'New Lead' system status.
        '   build the sql query
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       A.OrigStatusId AS StatusCodeId,B.StatusCodeDescrip ")
            .Append("FROM syLeadStatusChanges A,syStatusCodes B,syStatuses D,syCmpGrpCmps G,sySysStatus E,syLeadStatusChangePermissions F,syUsersRolesCampGrps U ")
            .Append("WHERE  A.OrigStatusId = B.StatusCodeId ")
            .Append("       AND B.StatusId = D.StatusId AND D.Status = 'Active' ")
            .Append("       AND G.CampGrpId=A.CampGrpId AND G.CampusId=? ")
            '.Append("       AND E.SysStatusDescrip='New Lead' ")
            .Append("       AND B.SysStatusId=E.SysStatusId ")
            .Append("       AND A.LeadStatusChangeId=F.LeadStatusChangeId ")
            .Append("       AND F.RoleId=U.RoleId AND F.StatusId=D.StatusId ")
            .Append("       AND U.UserId=? ")
            .Append("ORDER BY B.StatusCodeDescrip ")
        End With

        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAvailLeadStatuses(ByVal leadStatusId As String, ByVal campusId As String, ByVal userId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   get current lead status and those statuses that can be changed into, depending on user role.
        '   do NOT bring the Enrolled status (System Status = 6) do user is forced to use the 'Enroll Leads' page.
        '   build the sql query
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       A.StatusCodeId,A.StatusCodeDescrip ")
            .Append("FROM   syStatusCodes A, syStatuses B ")
            .Append("WHERE  A.StatusCodeId=? ")
            .Append("UNION ")
            .Append("SELECT ")
            .Append("       A.NewStatusId AS StatusCodeId,SC.StatusCodeDescrip ")
            .Append("FROM   syLeadStatusChanges A,syStatusCodes SC,syStatuses B,syCmpGrpCmps G,syLeadStatusChangePermissions P,syUsersRolesCampGrps U ")
            .Append("WHERE  SC.StatusCodeId=A.NewStatusId AND SC.SysStatusId <> 6 ")
            .Append("       AND A.OrigStatusId=? AND SC.StatusId = B.StatusId ")
            .Append("       AND B.Status = 'Active' ")
            .Append("       AND G.CampGrpId=A.CampGrpId AND G.CampusId=? ")
            If (New UserSecurityDB).IsSAOrDirectorOfAdmissions(userId) Then
                'Special case: SA or Director of Admission
                'These users do not have entries in syLeadStatusChangePermissions
                .Append("AND U.RoleId IN (SELECT RoleId FROM syRoles WHERE SysRoleId=8 OR SysRoleId=1)")
            Else
                'All other users with permissions defined in syLeadStatusChangePermissions
                .Append("       AND A.LeadStatusChangeId=P.LeadStatusChangeId ")
                .Append("       AND EXISTS (SELECT * FROM syLeadStatusChangePermissions A,syStatuses B ")
                .Append("                   WHERE LeadStatusChangeId=P.LeadStatusChangeId AND RoleId=P.RoleId ")
                .Append("                   AND A.StatusId=B.StatusId AND B.Status='Active') ")
                .Append("       AND P.RoleId=U.RoleId ")
            End If
            .Append("       AND U.UserId=? ")
            .Append("       AND A.CampGrpId=U.CampGrpId ")
            .Append("ORDER BY StatusCodeDescrip ")
        End With

        db.AddParameter("@StatusCodeId", leadStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@OrigStatusId", leadStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function InsertLeadStatusChange(ByVal statusChanged As LeadStatusesInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT INTO syLeadStatusesChanges")
                .Append("    (StatusChangeId,LeadId,OrigStatusId,NewStatusId,ModUser,ModDate) ")
                .Append("VALUES(?,?,?,?,?,?)")
            End With

            '   Add parameters values to the query
            '   StatusChangeId
            db.AddParameter("@StatusChangeId", statusChanged.StatusChangeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   LeadId
            db.AddParameter("@LeadId", statusChanged.LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   OrigStatusId
            If statusChanged.OrigStatusId = System.Guid.Empty.ToString Then
                db.AddParameter("@OrigStatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@OrigStatusId", statusChanged.OrigStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   NewStatusId
            db.AddParameter("@NewStatusId", statusChanged.NewStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            '   Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateLeadStatusChange(ByVal statusChanged As LeadStatusesInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syLeadStatusesChanges ")
                .Append("SET    NewStatusId = ?, ModUser = ?, ModDate = ? ")
                .Append("WHERE  LeadId = ? AND OrigStatusId IS NULL")
            End With

            '   Add parameters values to the query
            '   NewStatusId
            db.AddParameter("@NewStatusId", statusChanged.NewStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   LeadId
            db.AddParameter("@LeadId", statusChanged.LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            '   Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function DeleteLeadStatusChange(ByVal leadId As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            With sb
                .Append("DELETE FROM syLeadStatusesChanges ")
                .Append("WHERE LeadId = ? ")
            End With

            db.AddParameter("@LeadId", leadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            '   Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetLeadStatus(ByVal leadId As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim statusId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            With sb
                .Append("SELECT LeadStatus ")
                .Append("FROM adLeads ")
                .Append("WHERE LeadId = ? ")
            End With

            db.AddParameter("@LeadId", leadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim obj As Object = db.RunParamSQLScalar(sb.ToString)

            If Not (obj Is Nothing) Then
                statusId = obj.ToString
            End If

            '   return without errors
            Return statusId

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            '   Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetExistingLeadStatuses(ByVal campusId As String, ByVal userId As String, ByVal currentstatusid As String, Optional ByVal LeadId As String = "") As DataSet
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        Dim IsSAOrDirectorOfAdmissions As Boolean = (New UserSecurityDB).IsSAOrDirectorOfAdmissions(userId)
        Dim dsGetCmpGrps As New DataSet
        Dim dsGetStatusCodes As New DataSet
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusId)
        Dim strCampGrpId, strStatusCodeId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If
        dsGetStatusCodes = (New CampusGroupsDB).GetStatusCodesExceptEnrolledStatus(strCampGrpId)
        If dsGetStatusCodes.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetStatusCodes.Tables(0).Rows
                strStatusCodeId &= row("StatusCodeId").ToString & "','"
            Next
            strStatusCodeId = Mid(strStatusCodeId, 1, InStrRev(strStatusCodeId, "'") - 2)
        Else
            strStatusCodeId = ""
        End If

        'Modified By Balaji
        With sb
            If Not LeadId = "" Then
                'Get InActive StatusCodes that was assigned to Lead or Status Codes that were moved to another campus
                .Append(" Select distinct LeadStatus as StatusCodeId,'(X) ' + StatusCodeDescrip as StatusCodeDescrip ")
                .Append(" from adLeads L,syStatusCodes U ")
                .Append(" where L.LeadStatus=U.StatusCodeId  ")
                .Append(" AND L.LeadId='" & LeadId & "' ")
                .Append(" AND L.LeadStatus NOT IN ")
                .Append(" ( ")
                .Append("   select Distinct StatusCodeId from syStatusCodes where StatusCodeId=? and ")
                .Append("   StatusId='" & strActiveGUID & "' ")
                .Append("   and CampGrpId in ('")
                .Append(strCampGrpId)
                .Append(") ")
                .Append(" Union ")
                .Append(" SELECT Distinct t2.StatusCodeId ")
                .Append("   FROM ")
                .Append("   syLeadStatusChanges t1,SyStatusCodes t2 ")
                If IsSAOrDirectorOfAdmissions = False Then
                    .Append(" ,syLeadStatusChangePermissions F,syUsersRolesCampGrps U ")
                End If
                .Append("   WHERE ")
                .Append("   t1.NewStatusId = t2.StatusCodeId and ")
                .Append("   t1.OrigStatusId = ?  ")
                If Not strStatusCodeId = "" Then
                    .Append("  and t1.NewStatusId not in ('")
                    .Append(strStatusCodeId)
                    .Append(") ")
                End If
                .Append("   and t1.campGrpId in ('")
                .Append(strCampGrpId)
                .Append(") ")
                If IsSAOrDirectorOfAdmissions = False Then
                    .Append(" and t1.LeadStatusChangeId = F.LeadStatusChangeId and ")
                    .Append(" F.RoleId = U.RoleId and UserId='" & userId & "'  and F.StatusId='" & strActiveGUID & "'  ")
                End If
                .Append(" ) ")
                .Append(" Union ")
            End If
            .Append("   select Distinct StatusCodeId,StatusCodeDescrip from syStatusCodes where StatusCodeId=? and ")
            .Append("   StatusId='" & strActiveGUID & "' ")
            .Append("   and CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" Union ")
            .Append(" SELECT t2.StatusCodeId,t2.StatusCodeDescrip ")
            .Append("   FROM ")
            .Append("   syLeadStatusChanges t1,SyStatusCodes t2 ")
            If IsSAOrDirectorOfAdmissions = False Then
                .Append(" ,syLeadStatusChangePermissions F,syUsersRolesCampGrps U ")
            End If
            .Append("   WHERE ")
            .Append("   t1.NewStatusId = t2.StatusCodeId and ")
            .Append("   t1.OrigStatusId = ?  ")
            If Not strStatusCodeId = "" Then
                .Append("  and t1.NewStatusId not in ('")
                .Append(strStatusCodeId)
                .Append(") ")
            End If
            .Append("   and t1.campGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            If IsSAOrDirectorOfAdmissions = False Then
                .Append(" and t1.LeadStatusChangeId = F.LeadStatusChangeId and ")
                .Append(" F.RoleId = U.RoleId and UserId='" & userId & "'  and F.StatusId='" & strActiveGUID & "'  ")
            End If
            .Append(" Order by StatusCodeDescrip ")
        End With

        If Not LeadId = "" Then
            db.AddParameter("@StatusCodeId", currentstatusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@OrigStatusId", currentstatusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@StatusCodeId", currentstatusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@OrigStatusId", currentstatusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function CheckIfLeadCanBeEnrolledByStatus(ByVal userId As String, ByVal campusid As String, ByVal CurrentStatusId As String) As Integer
        Dim sb As New StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim IsSAOrDirectorOfAdmissions As Boolean = (New UserSecurityDB).IsSAOrDirectorOfAdmissions(userId)
        Dim dsGetCmpGrps As New DataSet
        Dim dsEnrollment As New DataSet
        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusid)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        'Modified By Balaji
        With sb
            .Append("	SELECT Distinct GetNewLeadStatusByCampus.StatusCodeId, ")
            .Append("					GetNewLeadStatusByCampus.StatusCodeDescrip, ")
            .Append("					GetNewLeadStatusByCampus.CampGrpId	")
            .Append("	FROM ")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '	This Table Query Returns The Lead Status Based on the Campus and Statuses mapped to Lead Entity
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            .Append(" (	")
            .Append("	SELECT Distinct StatusCodeId,StatusCodeDescrip,CampGrpId FROM syStatusCodes t1,sySysStatus t2 WHERE ")
            .Append("   t1.SysStatusId=t2.SysStatusId and t2.StatusLevelId=1 and t1.StatusId='" & strActiveGUID & "' and t2.StatusId='" & strActiveGUID & "' and CampGrpId in ('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" ) GetNewLeadStatusByCampus, ")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '	This Table Query Returns The Lead Status FROM LeadStatusChanges Table Based on the Campus
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            .Append(" (	")
            .Append("   select Distinct t3.LeadStatusChangeId,t3.OrigStatusId,t3.CampGrpId from ")
            .Append("   syLeadStatusChanges t3,syStatusCodes t4 ")
            .Append("   WHERE t3.NewStatusId = t4.StatusCodeId and t4.SysStatusId=6 and ")
            .Append("    t3.CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(") ")
            .Append(" ) GetLeadStatusByCampus ")
            If IsSAOrDirectorOfAdmissions = False Then
                .Append(" ,syLeadStatusChangePermissions F,syUsersRolesCampGrps U ")
            End If
            .Append(" WHERE GetNewLeadStatusByCampus.StatusCodeId = GetLeadStatusByCampus.OrigStatusId	")
            .Append(" and GetNewLeadStatusByCampus.CampGrpId = GetLeadStatusByCampus.CampGrpID 	")
            .Append(" and GetNewLeadStatusByCampus.StatusCodeId = '" & CurrentStatusId & "' ")
            If IsSAOrDirectorOfAdmissions = False Then
                .Append(" and GetLeadStatusByCampus.LeadStatusChangeId = F.LeadStatusChangeId and ")
                .Append(" F.RoleId = U.RoleId and UserId='" & userId & "'  and F.StatusId='" & strActiveGUID & "'  ")
            End If
        End With
        '   return dataset
        dsEnrollment = db.RunParamSQLDataSet(sb.ToString)
        Dim intReturnValue As Integer = 0
        Try
            intReturnValue = dsEnrollment.Tables(0).Rows.Count
            Return intReturnValue
        Catch ex As Exception
            Return intReturnValue
        End Try
    End Function
End Class
