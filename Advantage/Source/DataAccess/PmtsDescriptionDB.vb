Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' BanksDB.vb
'
' BanksDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class PmtsDescriptionDB
    Public Function GetAllPaymentDescriptions(ByVal showActiveOnly As String, ByVal campusid As Guid) As DataSet

        Dim scampusid As String = campusid.ToString()

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   PD.PmtDescriptionId,PD.PmtCode,PD.PmtDescription,PD.StatusId,PD.CampGrpId, ")
            .Append("         ST.StatusId,ST.Status ")
            .Append("FROM     saPaymentDescriptions PD, syStatuses ST ")
            .Append("WHERE    PD.StatusId = ST.StatusId ")
            '   Conditionally include only Active Items 
            If showActiveOnly = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                .Append("AND PD.CampGrpId in (SELECT campgrpid  FROM sycmpgrpcmps WHERE campusid = '" & scampusid & "')")
                .Append("ORDER BY PD.PmtDescription ")
            ElseIf showActiveOnly = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                .Append("AND PD.CampGrpId in (SELECT campgrpid  FROM sycmpgrpcmps WHERE campusid = '" & scampusid & "')")
                .Append("ORDER BY PD.PmtDescription ")
            Else
                .Append("AND PD.CampGrpId in (SELECT campgrpid  FROM sycmpgrpcmps WHERE campusid = '" & scampusid & "')")
                .Append("ORDER BY ST.Status,PD.PmtDescription asc")
            End If
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    ''Function Added by Saraswathi Lakshmanan on April 09 2010
    ''To fix mantis issue 15222
    ''New function to bring in only the Charges of SysTransCodeId 14,15and 16 in saSysTransCodes

    Public Function GetAllChargeDescriptions_sp(ByVal showActiveOnly As String, ByVal campusid As Guid) As DataSet

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@ShowActive", showActiveOnly, SqlDbType.NVarChar, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusid, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet_SP("USP_SA_GetAllChargeDescriptions")
    End Function


    Public Function GetAllAdjustmentsDescriptions_SP(ByVal showActiveOnly As String) As DataSet

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@ShowActive", showActiveOnly, SqlDbType.NVarChar, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet_SP("USP_AR_GetSystemTransCodesAdjustments_GetList")
    End Function
    ''Function Added by Saraswathi Lakshmanan on April 09 2010
    ''To fix mantis issue 15222
    ''New function to bring in only the payments of SysTransCodeId 11,12,13 in saSysTransCodes

    Public Function GetAllPaymentDescriptions_sp(ByVal showActiveOnly As String, ByVal campusid As Guid) As DataSet

        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@ShowActive", showActiveOnly, SqlDbType.NVarChar, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusid, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Return db.RunParamSQLDataSet_SP("USP_SA_GetAllPaymentDescriptions")
    End Function


    Public Function GetPaymentInfo(ByVal bankId As String) As BankInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT     B.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=B.StatusId) as Status, ")
            .Append("    B.PmtCode, ")
            .Append("    B.PmtDescription, ")
            .Append("    B.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=B.CampGrpId) As CampGrpDescrip ")
            .Append("FROM  saPaymentDescriptions B ")
            .Append("WHERE B.PmtDescriptionId = ? ")
        End With

        ' Add the bankIdto the parameter list
        db.AddParameter("@BankId", bankId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim bankInfo As New BankInfo

        While dr.Read()

            '   set properties with data from DataReader
            With bankInfo
                .IsInDB = True
                .BankId = bankId
                If Not (dr("PmtCode") Is System.DBNull.Value) Then .Code = dr("PmtCode")
                If Not (dr("StatusId") Is System.DBNull.Value) Then .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Name = dr("PmtDescription")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")

            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return BankInfo
        Return bankInfo

    End Function
    Public Function UpdatePaymentInfo(ByVal bankInfo As BankInfo) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE saPaymentDescriptions Set PmtCode = ?, StatusId = ?, PmtDescription = ?,CampGrpId = ? ")
                .Append("WHERE PmtDescriptionId = ? ")
            End With

            '   add parameters values to the query

            '   Code
            db.AddParameter("@PmtCode", bankInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            If bankInfo.StatusId = Guid.Empty.ToString Then
                db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StatusId", bankInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   bank name
            db.AddParameter("@PmtDescription", bankInfo.Name, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Campus Group Id
            If bankInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", bankInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   BankId
            db.AddParameter("@PmtDescriptionId", bankInfo.BankId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            Return ""


        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddPaymentInfo(ByVal bankInfo As BankInfo) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT saPaymentDescriptions (PmtDescriptionId,PmtCode,PmtDescription,StatusId,CampGrpId) ")
                .Append("VALUES (?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   BankId
            db.AddParameter("@PmtDescriptionId", bankInfo.BankId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Code
            db.AddParameter("@PmtCode", bankInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   bank name
            db.AddParameter("@PmtDescription", bankInfo.Name, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '   StatusId
            If bankInfo.StatusId = Guid.Empty.ToString Then
                db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StatusId", bankInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If



            '   Campus Group Id
            If bankInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", bankInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeletePaymentInfo(ByVal bankId As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM saPaymentDescriptions ")
                .Append("WHERE PmtDescriptionId = ? ")

            End With

            '   add parameters values to the query

            '   BankId
            db.AddParameter("@BankId", bankId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)



            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function




    Public Sub DisplayOleDbErrorCollection(ByVal myException As OleDbException)
        Dim i As Integer, str As String = ""
        For i = 0 To myException.Errors.Count - 1
            str += "Index #" + i.ToString() + ControlChars.Cr _
               + "Message: " + myException.Errors(i).Message + ControlChars.Cr _
               + "Native: " + myException.Errors(i).NativeError.ToString() + ControlChars.Cr _
               + "Source: " + myException.Errors(i).Source + ControlChars.Cr _
               + "SQL: " + myException.Errors(i).SQLState + ControlChars.Cr
        Next i
    End Sub

End Class
