Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' AcademicAdvisorsDB.vb
'
' Data Access Logic for Academic Advisors and Financial Aid Advisors. 
'
' This class will be used to retrieve/update data related to Academic Advisors and Financial Aid Advisor.
' This is done because the logic is exactly the same. The difference is only on the SysRoleId used.
' SysRoleId for Academic Advisor is 4. 
' SysRoleId for Financial Aid Advisor is 7. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class AcademicAdvisorsDB
    '
    '   AcademicAdvisorsDB Class
    '

    Public Function GetAcademicAdvisorsPerCampus(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       U.UserId as AcademicAdvisorId, ")
            .Append("       U.FullName as AcademicAdvisorName ")
            .Append("FROM ")
            .Append("       syUsersRolesCampGrps URCG, syRoles R, syUsers U ")
            .Append("WHERE ")
            .Append("       R.RoleId=URCG.RoleId ")
            .Append("AND	R.SysRoleId=4 ")
            .Append("AND	URCG.UserId=U.UserId ")
            .Append("AND	CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            .Append("ORDER BY U.FullName ")
        End With

        ' Add the CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function GetStudentsAssignedToASpecificAcademicAdvisor(ByVal academicAdvisor As String, ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       SE.StuEnrollId, ")
            .Append("        LastName + ' ' + FirstName + '(' + PV.PrgVerDescrip + ')' As StudentName ")
            .Append("FROM ")
            .Append("       arStuEnrollments SE, syStatusCodes SC, arStudent S, arPrgVersions PV ")
            .Append("WHERE ")
            .Append("       SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND	SC.SysStatusId IN (7,9,13) ")
            .Append("AND	SE.StudentId=S.StudentId ")
            .Append("AND	SE.PrgVerId=PV.PrgVerId ")
            .Append("AND    SE.CampusId = ? ") 'changed to bring in assigned students that only belong to the campus that we're signed into. 
            .Append("AND    SE.AcademicAdvisor = ? ")
            .Append("ORDER BY StudentName ")
        End With

        '   if the AcademicAdvisor is empty replace it by the empty Guid
        If academicAdvisor = "" Then academicAdvisor = Guid.Empty.ToString

        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ' Add the AcademicAdvisorId to the parameter list
        db.AddParameter("@AcademicAdvisor", academicAdvisor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function GetStudentsNotAssignedToAnyAcademicAdvisor(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       SE.StuEnrollId, ")
            .Append("        LastName + ' ' + FirstName + '(' + PV.PrgVerDescrip + ')' As StudentName ")
            .Append("FROM ")
            .Append("       arStuEnrollments SE, syStatusCodes SC, arStudent S, arPrgVersions PV ")
            .Append("WHERE ")
            .Append("       SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND	SC.SysStatusId IN (7,9,13) ")
            .Append("AND	SE.StudentId=S.StudentId ")
            .Append("AND	SE.PrgVerId=PV.PrgVerId ")
            .Append("AND    SE.CampusId = ? ")
            .Append("AND    SE.AcademicAdvisor Is Null ")
            .Append("ORDER BY StudentName ")
        End With

        ' Add the CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function UpdateListOfStudentsAssignedToAnAcademicAdvisor(ByVal campusId As String, ByVal AcademicAdvisor As String, ByVal user As String, ByVal selectedStudents() As String) As String
        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            '   First we have to delete all existing selections in the arStuEnrollments table
            '   build the query
            Dim sb As New System.Text.StringBuilder
            With sb
                .Append("UPDATE arStuEnrollments Set AcademicAdvisor = Null WHERE CampusId = ? and AcademicAdvisor = ? ")
            End With

            '   Add parameter CampusId
            db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Add parameter AcademicAdvisor
            db.AddParameter("@AcademicAdvisor", AcademicAdvisor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            '   Insert one record per each Item in the Selected Group
            Dim now As DateTime = Date.Now
            For i As Integer = 0 To selectedStudents.Length - 1

                '   build the query
                With sb
                    .Append("UPDATE arStuEnrollments ")
                    .Append("       Set AcademicAdvisor = ?, ")
                    .Append("       ModUser = ?, ")
                    .Append("       ModDate = ? ")
                    .Append("WHERE ")
                    .Append("       StuEnrollId = ? ")
                End With

                '   add parameters values to the query
                db.AddParameter("@AcademicAdvisorId", AcademicAdvisor, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", DirectCast(selectedStudents.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Next

            '   commit transaction 
            groupTrans.Commit()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            groupTrans.Rollback()
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
                Return ex.Message

            End If
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function


    '   Finacial Aid Advisors
    Public Function GetFinancialAidAdvisorsPerCampus(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("       U.UserId as AcademicAdvisorId, ")
            .Append("       U.FullName as AcademicAdvisorName ")
            .Append("FROM ")
            .Append("       syUsersRolesCampGrps URCG, syRoles R, syUsers U ")
            .Append("WHERE ")
            .Append("       R.RoleId=URCG.RoleId ")
            .Append("AND	R.SysRoleId=7 ")
            .Append("AND	URCG.UserId=U.UserId ")
            .Append("AND	CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            .Append("ORDER BY U.FullName ")
        End With

        ' Add the CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function GetStudentsAssignedToASpecificFinancialAidAdvisor(ByVal faAdvisor As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       SE.StuEnrollId, ")
            .Append("        LastName + ' ' + FirstName + '(' + PV.PrgVerDescrip + ')' As StudentName ")
            .Append("FROM ")
            .Append("       arStuEnrollments SE, syStatusCodes SC, arStudent S, arPrgVersions PV ")
            .Append("WHERE ")
            .Append("       SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND	SC.SysStatusId IN (7,9,13) ")
            .Append("AND	SE.StudentId=S.StudentId ")
            .Append("AND	SE.PrgVerId=PV.PrgVerId ")
            .Append("AND    SE.FAAdvisorId = ? ")
            .Append("ORDER BY StudentName ")
        End With

        '   if the AcademicAdvisor is empty replace it by the empty Guid
        If faAdvisor = "" Then faAdvisor = Guid.Empty.ToString

        ' Add the AcademicAdvisorId to the parameter list
        db.AddParameter("@FAAdvisorId", faAdvisor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function GetStudentsNotAssignedToAnyFinancialAidAdvisor(ByVal campusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       SE.StuEnrollId, ")
            .Append("        LastName + ' ' + FirstName + '(' + PV.PrgVerDescrip + ')' As StudentName ")
            .Append("FROM ")
            .Append("       arStuEnrollments SE, syStatusCodes SC, arStudent S, arPrgVersions PV ")
            .Append("WHERE ")
            .Append("       SE.StatusCodeId=SC.StatusCodeId ")
            .Append("AND	SC.SysStatusId IN (7,9,13) ")
            .Append("AND	SE.StudentId=S.StudentId ")
            .Append("AND	SE.PrgVerId=PV.PrgVerId ")
            .Append("AND    SE.CampusId = ? ")
            .Append("AND    SE.FAAdvisorId Is Null ")
            .Append("ORDER BY StudentName ")
        End With

        ' Add the CampusId to the parameter list
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString)

        'Close Connection
        db.CloseConnection()

        'Return Dataset
        Return ds

    End Function
    Public Function UpdateListOfStudentsAssignedToAFinancialAidAdvisor(ByVal campusId As String, ByVal FinancialAidAdvisor As String, ByVal user As String, ByVal selectedStudents() As String) As String
        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            '   First we have to delete all existing selections in the arStuEnrollments table
            '   build the query
            Dim sb As New System.Text.StringBuilder
            With sb
                .Append("UPDATE arStuEnrollments Set FAAdvisorId = Null WHERE CampusId = ? and FAAdvisorId = ? ")
            End With

            '   Add parameter CampusId
            db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Add parameter AcademicAdvisor
            db.AddParameter("@FAAdvisorId", FinancialAidAdvisor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            '   Insert one record per each Item in the Selected Group
            Dim now As DateTime = Date.Now
            For i As Integer = 0 To selectedStudents.Length - 1

                '   build the query
                With sb
                    .Append("UPDATE arStuEnrollments ")
                    .Append("       Set FAAdvisorId = ?, ")
                    .Append("       ModUser = ?, ")
                    .Append("       ModDate = ? ")
                    .Append("WHERE ")
                    .Append("       StuEnrollId = ? ")
                End With

                '   add parameters values to the query
                db.AddParameter("@FAAdvisorId", FinancialAidAdvisor, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", DirectCast(selectedStudents.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Next

            '   commit transaction 
            groupTrans.Commit()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            groupTrans.Rollback()
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
                Return ex.Message

            End If
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
End Class

