Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' QuickLinksDB.vb
'
' QuickLinksDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class QuickLinksDB
    Public Function GetAllQuickLinks(ByVal showActiveOnly As String, ByVal ModuleName As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   QL.QuickLinkID, QL.ModuleID, QL.StatusId, ST.Status, ")
            .Append("QL.Description, QL.URL, QL.ModUser, QL.ModDate ")
            .Append("FROM     syQuickLinks QL, syStatuses ST, syModules SM ")
            .Append("WHERE    QL.StatusId = ST.StatusId AND QL.ModuleID = SM.ModuleID ")
            If showActiveOnly = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                .Append("ORDER BY QL.Description ")
            ElseIf showActiveOnly = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                .Append("ORDER BY QL.Description ")
            Else
                .Append("ORDER BY ST.Status,QL.Description asc")
            End If
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetQuickLinkInfo(ByVal QuickLinkID As String) As QuickLinkInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT QL.QuickLinkID, ")
            .Append("    QL.ModuleID, ")
            .Append("    QL.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=QL.StatusId) As Status, ")
            .Append("    QL.Description, ")
            .Append("    QL.URL, ")
            .Append("    QL.ModUser, ")
            .Append("    QL.ModDate ")
            .Append("FROM  syQuickLinks QL ")
            .Append("WHERE QL.QuickLinkID= ? ")
        End With

        ' Add the QuickLinkId to the parameter list
        db.AddParameter("@QuickLinkID", QuickLinkID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim QuickLinkInfo As New QuickLinkInfo

        While dr.Read()

            '   set properties with data from DataReader
            With QuickLinkInfo
                .IsInDB = True
                .QuickLinkID = QuickLinkID
                .ModuleID = dr("ModuleID")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("Description")
                .URL = dr("URL")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return QuickLinkInfo
        Return QuickLinkInfo

    End Function
    Public Function UpdateQuickLinkInfo(ByVal QuickLinkInfo As QuickLinkInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syQuickLinks Set QuickLinkID = ?, ModuleID = ?, ")
                .Append(" StatusId = ?, Description = ?, URL = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE QuickLinkID = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from syQuickLinks where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   QuickLinkID
            db.AddParameter("@QuickLinkID", QuickLinkInfo.QuickLinkID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModuleID
            db.AddParameter("@ModuleID", QuickLinkInfo.ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", QuickLinkInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Description
            db.AddParameter("@Description", QuickLinkInfo.Description, DataAccess.OleDbDataType.OleDbString, 150, ParameterDirection.Input)

            '   URL
            db.AddParameter("@URL", QuickLinkInfo.URL, DataAccess.OleDbDataType.OleDbString, 150, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   QuickLinkId
            db.AddParameter("@AdmDepositId", QuickLinkInfo.QuickLinkID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", QuickLinkInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddQuickLinkInfo(ByVal QuickLinkInfo As QuickLinkInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syQuickLinks (QuickLinkID, ModuleID, StatusId, ")
                .Append("   Description, URL, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   QuickLinkID
            db.AddParameter("@QuickLinkID", QuickLinkInfo.QuickLinkID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModuleID
            db.AddParameter("@ModuleID", QuickLinkInfo.ModuleID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", QuickLinkInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Description
            db.AddParameter("@Description", QuickLinkInfo.Description, DataAccess.OleDbDataType.OleDbString, 150, ParameterDirection.Input)

            '   URL
            db.AddParameter("@URL", QuickLinkInfo.URL, DataAccess.OleDbDataType.OleDbString, 150, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteQuickLinkInfo(ByVal QuickLinkID As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syQuickLinks ")
                .Append("WHERE QuickLinkID = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM syQuickLinks WHERE QuickLinkID = ? ")
            End With

            '   add parameters values to the query

            '   QuickLinkID
            db.AddParameter("@QuickLinkID", QuickLinkID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   QuickLinkId
            db.AddParameter("@QuickLinkID", QuickLinkID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
End Class