Imports FAME.Advantage.Common

Public Class StuListByStartDate

#Region "Private Data Members"

    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")

#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region


#Region "Public Methods"

    '  Public Function GetStudentListByStartDate(ByVal paramInfo As ReportParamInfo) As DataSet
    '      Dim sb As New System.Text.StringBuilder
    '      Dim db As New DataAccess
    '      Dim ds As New DataSet
    '      Dim strWhere As String
    '      Dim strOrderBy As String
    '      Dim strStuId, EnrollmentStartDate, EnrollmentEndDate, strStatusCodes As String
    '      Dim strWhereFilterOther, strSyStudentStatusChanges, strAddToWhere, arStuEnrollments As String
    '      Dim strSelectedStatusCodes, stuEnrollDateRangeClause, syStudentStatusChangesTableName As String
    '      Dim EnrollmentStartDateClause As String


    '      If paramInfo.FilterList <> "" Then
    '          strWhere &= " AND " & paramInfo.FilterList
    '      End If

    '      If paramInfo.FilterOther <> "" Then
    '          'Get the selected Statuses
    '          If InStr(strWhere.ToLower, "arstuenrollments.statuscodeid in") > 0 Then
    '              Dim strArr As String() = strWhere.Remove(strWhere.IndexOf("AND"), 1).Split("AND")
    '              Dim i As Integer
    '              For i = 0 To strArr.Length - 1
    '                  If strArr(i).ToLower.IndexOf("arstuenrollments.statuscodeid") > 0 Then
    '                      arStuEnrollments = Trim(Replace(strArr(i), "ND", ""))
    '                      'Now get the selected statuses
    '                      Dim strStatusesArr As String() = arStuEnrollments.Remove(arStuEnrollments.IndexOf("IN"), 1).Split("IN")
    '                      Dim j As Integer
    '                      For j = 0 To strStatusesArr.Length - 1
    '                          strStatusesArr(j) = Replace(strStatusesArr(j), "d N", "IN")
    '                          If InStr(strStatusesArr(j), "IN") > 0 Then
    '                              strStatusCodes = strStatusesArr(j).ToString
    '                          End If
    '                      Next
    '                  End If
    '              Next
    '          Else
    '              Dim strSingleStatusArr As String() = strWhere.Remove(strWhere.IndexOf("AND"), 1).Split("AND")
    '              Dim i As Integer
    '              For i = 0 To strSingleStatusArr.Length - 1
    '                  If InStr(strSingleStatusArr(i).ToLower, "arstuenrollments.statuscodeid") > 0 Then
    '                      strStatusCodes = Trim(Replace(strSingleStatusArr(i), "ND", ""))
    '                      strStatusCodes = Trim(Mid(strStatusCodes, strStatusCodes.IndexOf("="), Len(strStatusCodes)))
    '                  End If
    '              Next
    '          End If



    '          strWhereFilterOther = paramInfo.FilterOther

    '          If InStr(strWhereFilterOther, "syStudentStatusChanges") > 0 Then
    '              'Strip out the syStudentStatusChanges portion and get the dates
    '              Dim strWherePieces As String() = strWhereFilterOther.Remove(strWhereFilterOther.IndexOf("AND"), 1).Split("AND")
    '              Dim i As Integer
    '              For i = 0 To strWherePieces.Length - 1
    '                  If InStr(strWherePieces(i).ToLower, "systudentstatuschanges") > 0 Then
    '                      strWhereFilterOther = Trim(strWherePieces(i))
    '                  End If
    '              Next
    '              EnrollmentStartDate = Mid(strWhereFilterOther, strWhereFilterOther.IndexOf("BETWEEN") + 8, strWhereFilterOther.IndexOf("ND"))
    '              EnrollmentStartDate = Left(EnrollmentStartDate, EnrollmentStartDate.IndexOf("ND"))
    '              EnrollmentEndDate = Mid(strWhereFilterOther, strWhereFilterOther.IndexOf("ND") + 3, Len(strWhereFilterOther))
    '              'Add the rest to the where clause
    '              strAddToWhere = Replace(paramInfo.FilterOther, Replace(strWhereFilterOther, "ND", "AND"), "")


    '              strWhere &= strAddToWhere

    '              'Strip out the student enrollment isolation from the where because we are going to
    '              'be using the status range below.
    '              'Strip out the syStudentStatusChanges portion and get the dates
    '              Dim strStatusRemoved As String
    '              Dim strStatusPieces As String() = strWhere.Remove(strWhere.IndexOf("AND"), 1).Split("AND")
    '              Dim j As Integer
    '              For j = 0 To strStatusPieces.Length - 1
    '                  If InStr(strStatusPieces(j).ToLower, "arstuenrollments.statuscodeid") > 0 Then
    '                      strStatusRemoved = Trim(strStatusPieces(j))
    '                      strStatusRemoved = Replace(strStatusRemoved, "ND", "AND")

    '                      strWhere = Replace(strWhere, strStatusRemoved, "")
    '                  End If
    '              Next
    '          Else
    '              strWhere &= " AND " & paramInfo.FilterOther
    '          End If

    '          If EnrollmentStartDate <> "" And strStatusCodes <> "" Then
    '              'Create the where clause to include the filtering by student enrollment date ranges
    '              syStudentStatusChangesTableName = ", syStudentStatusChanges "

    '              ''Modified By Saraswathi lakshmanan on Oct 09 2009
    '              ''For mantis issue 16650: ENH: AMC List of Currently Enrolled Students
    '              ''And Cluse for comparing the stuEnrollid is added
    '              ''If the student is in out of School Status, then donot consider that student any more
    '              ''For eg: If the student graduated in sept and the report is run for the month of October for Graduated students
    '              ''Donot bring up that student.
    '              ''but if the student was in any inschool status in Sept  then show that student for the month of October




    '              'EnrollmentStartDateClause = "AND syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId " _
    '              '& " AND syStudentStatusChanges.NewStatusId " & strStatusCodes _
    '              '& " AND ((syStudentStatusChanges.ModDate BETWEEN " & EnrollmentStartDate & " AND " & EnrollmentEndDate & ") " _
    '              '& "OR (syStudentStatusChanges.ModDate = (SELECT MAX(syStudentStatusChanges.ModDate) FROM syStudentStatusChanges WHERE syStudentStatusChanges.ModDate <= " & EnrollmentStartDate & "))) "
    '              EnrollmentStartDateClause = "AND syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId " _
    '              & " AND syStudentStatusChanges.NewStatusId " & strStatusCodes _
    '              & " AND ((syStudentStatusChanges.ModDate BETWEEN " & EnrollmentStartDate & " AND " & EnrollmentEndDate & ") " _
    '              & "OR ((syStudentStatusChanges.ModDate = (SELECT MAX(syStudentStatusChanges.ModDate) FROM syStudentStatusChanges WHERE syStudentStatusChanges.ModDate <= " & EnrollmentStartDate & "  and syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId )   " _
    '              & "           and syStudentStatusChanges.NewStatusID not in  (Select StatusCodeId from sySysStatus,SyStatusCodes where InSchool=0 " _
    '              & " and sySysStatus.SysStatusId=SyStatusCodes.SysStatusId) ))) "
    '          ElseIf EnrollmentStartDate <> "" And strStatusCodes = "" Then
    '              'Dates were selected, but no enrollment status. Return all statuses for that date range.
    '              syStudentStatusChangesTableName = ", syStudentStatusChanges "

    '              'EnrollmentStartDateClause = "AND syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId " _
    '              '& " AND ((syStudentStatusChanges.ModDate BETWEEN " & EnrollmentStartDate & " AND " & EnrollmentEndDate & ") " _
    '              '& "OR (syStudentStatusChanges.ModDate = (SELECT MAX(syStudentStatusChanges.ModDate) FROM syStudentStatusChanges WHERE syStudentStatusChanges.ModDate <= " & EnrollmentStartDate & "))) "

    '              ''Modified By Saraswathi lakshmanan on Oct 09 2009
    '              ''For mantis issue 16650: ENH: AMC List of Currently Enrolled Students
    '              ''And Cluse for comparing the stuEnrollid is added
    '              ''If the student is in out of School Status, then donot consider that student any more
    '              ''For eg: If the student graduated in sept and the report is run for the month of October for Graduated students
    '              ''Donot bring up that student.
    '              ''but if the student was in any inschool status in Sept  then show that student for the month of October
    '              EnrollmentStartDateClause = "AND syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId " _
    '& " AND ((syStudentStatusChanges.ModDate BETWEEN " & EnrollmentStartDate & " AND " & EnrollmentEndDate & ") " _
    '& "OR ( (syStudentStatusChanges.ModDate = (SELECT MAX(syStudentStatusChanges.ModDate) FROM syStudentStatusChanges WHERE syStudentStatusChanges.ModDate <= " & EnrollmentStartDate & "  and syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId )  " _
    '              & "           and syStudentStatusChanges.NewStatusID not in  (Select StatusCodeId from sySysStatus,SyStatusCodes where InSchool=0 " _
    '              & " and sySysStatus.SysStatusId=SyStatusCodes.SysStatusId) ))) "


    '          End If

    '      End If

    '      If paramInfo.OrderBy <> "" Then
    '          strOrderBy &= "," & paramInfo.OrderBy
    '      End If

    '      If StudentIdentifier = "SSN" Then
    '          strStuId = "arStudent.SSN AS StudentIdentifier,"
    '      ElseIf StudentIdentifier = "EnrollmentId" Then
    '          strStuId = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
    '      ElseIf StudentIdentifier = "StudentId" Then
    '          strStuId = "arStudent.StudentNumber AS StudentIdentifier,"
    '      End If

    '      With sb
    '          .Append("SELECT ")
    '          .Append("       arStuEnrollments.StuEnrollId,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuId)
    '          .Append("       arStuEnrollments.PrgVerId,(SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS ProgVerDescrip,")
    '          .Append("       (SELECT PrgVerCode FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS ProgVerCode,")
    '          .Append("       arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE arStuEnrollments.StatusCodeId=StatusCodeId) AS StatusCodeDescrip,")
    '          .Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip,")
    '          .Append("       arStuEnrollments.ShiftId,(SELECT ShiftDescrip FROM arShifts WHERE ShiftId=arStuEnrollments.ShiftId) AS ShiftDescrip,")
    '          .Append("       arStuEnrollments.StartDate,arStuEnrollments.ExpGradDate ")
    '          .Append("FROM   arStudent,arStuEnrollments,syCmpGrpCmps,syCampGrps,syCampuses " & syStudentStatusChangesTableName)
    '          .Append("WHERE  arStudent.StudentId = arStuEnrollments.StudentId ")
    '          .Append("       AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
    '          .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
    '          .Append("       AND syCampuses.CampusId=arStuEnrollments.CampusId ")
    '          .Append(strWhere)
    '          .Append(EnrollmentStartDateClause)
    '          .Append("ORDER BY syCampGrps.CampGrpDescrip,syCmpGrpCmps.CampGrpId,syCampuses.CampDescrip,arStuEnrollments.CampusId,ProgVerDescrip")
    '          .Append(strOrderBy)
    '      End With

    '      db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '      db.OpenConnection()

    '      ds = db.RunParamSQLDataSet(sb.ToString)

    '      If ds.Tables.Count > 0 Then
    '          ds.Tables(0).TableName = "StudentListing"
    '          ' Add new columns.
    '          ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
    '          ds.Tables(0).Columns.Add(New DataColumn("StudentCount", System.Type.GetType("System.Int32")))
    '          ds.Tables(0).Columns.Add(New DataColumn("PrgVerIdStr", System.Type.GetType("System.String")))
    '          ds.Tables(0).Columns.Add(New DataColumn("PrgVerLabel", System.Type.GetType("System.String")))
    '      End If

    '      Return ds
    '  End Function
    Public Function GetStudentListByStartDate(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStuId, EnrollmentStartDate, EnrollmentEndDate, strStatusCodes As String
        Dim strWhereFilterOther, strSyStudentStatusChanges, strAddToWhere, arStuEnrollments As String
        Dim strSelectedStatusCodes, stuEnrollDateRangeClause, syStudentStatusChangesTableName As String
        Dim EnrollmentStartDateClause As String
        Dim EnrollmentStartDateClauseCA As String
        Dim StrESFilterCriteria As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            'Get the selected Statuses
            If InStr(strWhere.ToLower, "arstuenrollments.statuscodeid in") > 0 Then
                Dim strArr As String() = strWhere.Remove(strWhere.IndexOf("AND"), 1).Split("AND")
                Dim i As Integer
                For i = 0 To strArr.Length - 1
                    If strArr(i).ToLower.IndexOf("arstuenrollments.statuscodeid") > 0 Then
                        arStuEnrollments = Trim(Replace(strArr(i), "ND", ""))
                        'Now get the selected statuses
                        Dim strStatusesArr As String() = arStuEnrollments.Remove(arStuEnrollments.IndexOf("IN"), 1).Split("IN")
                        Dim j As Integer
                        For j = 0 To strStatusesArr.Length - 1
                            strStatusesArr(j) = Replace(strStatusesArr(j), "d N", "IN")
                            If InStr(strStatusesArr(j), "IN") > 0 Then
                                strStatusCodes = strStatusesArr(j).ToString
                            End If
                        Next
                    End If
                Next
            Else
                Dim strSingleStatusArr As String() = strWhere.Remove(strWhere.IndexOf("AND"), 1).Split("AND")
                Dim i As Integer
                For i = 0 To strSingleStatusArr.Length - 1
                    If InStr(strSingleStatusArr(i).ToLower, "arstuenrollments.statuscodeid") > 0 Then
                        strStatusCodes = Trim(Replace(strSingleStatusArr(i), "ND", ""))
                        strStatusCodes = Trim(Mid(strStatusCodes, strStatusCodes.IndexOf("="), Len(strStatusCodes)))
                    End If
                Next
            End If

        End If

        strWhereFilterOther = paramInfo.FilterOther

        If InStr(strWhereFilterOther, "syStudentStatusChanges") > 0 Then
            'Strip out the syStudentStatusChanges portion and get the dates
            Dim strWherePieces As String() = strWhereFilterOther.Remove(strWhereFilterOther.IndexOf("AND"), 1).Split("AND")
            Dim i As Integer
            For i = 0 To strWherePieces.Length - 1
                If InStr(strWherePieces(i).ToLower, "systudentstatuschanges") > 0 Then
                    strWhereFilterOther = Trim(strWherePieces(i))
                End If
            Next
            EnrollmentStartDate = Mid(strWhereFilterOther, strWhereFilterOther.IndexOf("BETWEEN") + 8, strWhereFilterOther.IndexOf("ND"))
            EnrollmentStartDate = Left(EnrollmentStartDate, EnrollmentStartDate.IndexOf("ND"))
            EnrollmentEndDate = Mid(strWhereFilterOther, strWhereFilterOther.IndexOf("ND") + 3, Len(strWhereFilterOther))
            'Add the rest to the where clause
            strAddToWhere = Replace(paramInfo.FilterOther, Replace(strWhereFilterOther, "ND", "AND"), "")
            strWhere &= strAddToWhere

            '''' Code added on 11th October by kamalesh Ahuja to fix mantis issue id 19762
            Dim strWherePiecestemp As String() = paramInfo.FilterOther.Split("AND")

            Dim k As Integer
            For k = 0 To strWherePiecestemp.Length - 1
                If InStr(strWherePiecestemp(k).ToLower, "systudentstatuschanges") > 0 Then

                    If InStr(strWherePiecestemp(k).ToLower, "between") > 0 Then
                        StrESFilterCriteria = " Between "
                    ElseIf InStr(strWherePiecestemp(k).ToLower, "not between") > 0 Then
                        StrESFilterCriteria = " Not Between "
                    ElseIf InStr(strWherePiecestemp(k).ToLower, ">") > 0 Then
                        StrESFilterCriteria = " > "
                    ElseIf InStr(strWherePiecestemp(k).ToLower, "<") > 0 Then
                        StrESFilterCriteria = " < "
                    End If

                End If
            Next
            '''''''''''''''''''''

            'Strip out the student enrollment isolation from the where because we are going to
            'be using the status range below.
            'Strip out the syStudentStatusChanges portion and get the dates
            Dim strStatusRemoved As String
            Dim strStatusPieces As String() = strWhere.Remove(strWhere.IndexOf("AND"), 1).Split("AND")
            Dim j As Integer
            For j = 0 To strStatusPieces.Length - 1
                If InStr(strStatusPieces(j).ToLower, "arstuenrollments.statuscodeid") > 0 Then
                    strStatusRemoved = Trim(strStatusPieces(j))
                    strStatusRemoved = Replace(strStatusRemoved, "ND", "AND")

                    strWhere = Replace(strWhere, strStatusRemoved, "")
                End If
            Next
        Else
            '' New Code Added By Vijay Ramteke On January 25, 2011 For Rally Id DE4340
            ''strWhere &= " AND " & paramInfo.FilterOther
            If Not paramInfo.FilterOther = "" Then
                strWhere &= " AND " & paramInfo.FilterOther
            End If
            '' New Code Added By Vijay Ramteke On January 25, 2011 For Rally Id DE4340
        End If

        '''' Code modified to fix mantis issue id 19762 on 11 th october 2010 ''''


        If EnrollmentStartDate <> "" And strStatusCodes <> "" Then
            'Create the where clause to include the filtering by student enrollment date ranges
            syStudentStatusChangesTableName = ", syStudentStatusChanges "

            ''Modified By Saraswathi lakshmanan on Oct 09 2009
            ''For mantis issue 16650: ENH: AMC List of Currently Enrolled Students
            ''And Cluse for comparing the stuEnrollid is added
            ''If the student is in out of School Status, then donot consider that student any more
            ''For eg: If the student graduated in sept and the report is run for the month of October for Graduated students
            ''Donot bring up that student.
            ''but if the student was in any inschool status in Sept  then show that student for the month of October




            'EnrollmentStartDateClause = "AND syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId " _
            '& " AND syStudentStatusChanges.NewStatusId " & strStatusCodes _
            '& " AND ((syStudentStatusChanges.ModDate BETWEEN " & EnrollmentStartDate & " AND " & EnrollmentEndDate & ") " _
            '& "OR (syStudentStatusChanges.ModDate = (SELECT MAX(syStudentStatusChanges.ModDate) FROM syStudentStatusChanges WHERE syStudentStatusChanges.ModDate <= " & EnrollmentStartDate & "))) "

            'EnrollmentStartDateClause = "AND syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId " _
            '& " AND syStudentStatusChanges.NewStatusId " & strStatusCodes _
            '& " AND ((syStudentStatusChanges.ModDate BETWEEN " & EnrollmentStartDate & " AND " & EnrollmentEndDate & ") " _
            '& "OR ((syStudentStatusChanges.ModDate = (SELECT MAX(syStudentStatusChanges.ModDate) FROM syStudentStatusChanges WHERE syStudentStatusChanges.ModDate <= " & EnrollmentStartDate & "  and syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId )   " _
            '& "           and syStudentStatusChanges.NewStatusID not in  (Select StatusCodeId from sySysStatus,SyStatusCodes where InSchool=0 " _
            '& " and sySysStatus.SysStatusId=SyStatusCodes.SysStatusId) )) "

            EnrollmentStartDateClause = "AND syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId " _
            & " AND syStudentStatusChanges.NewStatusId " & strStatusCodes _
            & " AND ((syStudentStatusChanges.DateOfChange BETWEEN " & EnrollmentStartDate & " AND " & EnrollmentEndDate & ") " _
            & "OR ((syStudentStatusChanges.DateOfChange = (SELECT MAX(syStudentStatusChanges.DateOfChange) FROM syStudentStatusChanges WHERE syStudentStatusChanges.DateOfChange <= " & EnrollmentStartDate & "  and syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId )   " _
            & "           and syStudentStatusChanges.NewStatusID not in  (Select StatusCodeId from sySysStatus,SyStatusCodes where InSchool=0 " _
            & " and sySysStatus.SysStatusId=SyStatusCodes.SysStatusId) )) "
        ElseIf EnrollmentStartDate <> "" And strStatusCodes = "" Then
            'Dates were selected, but no enrollment status. Return all statuses for that date range.
            syStudentStatusChangesTableName = ", syStudentStatusChanges "

            'EnrollmentStartDateClause = "AND syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId " _
            '& " AND ((syStudentStatusChanges.ModDate BETWEEN " & EnrollmentStartDate & " AND " & EnrollmentEndDate & ") " _
            '& "OR (syStudentStatusChanges.ModDate = (SELECT MAX(syStudentStatusChanges.ModDate) FROM syStudentStatusChanges WHERE syStudentStatusChanges.ModDate <= " & EnrollmentStartDate & "))) "

            ''Modified By Saraswathi lakshmanan on Oct 09 2009
            ''For mantis issue 16650: ENH: AMC List of Currently Enrolled Students
            ''And Cluse for comparing the stuEnrollid is added
            ''If the student is in out of School Status, then donot consider that student any more
            ''For eg: If the student graduated in sept and the report is run for the month of October for Graduated students
            ''Donot bring up that student.
            ''but if the student was in any inschool status in Sept  then show that student for the month of October

            'EnrollmentStartDateClause = "AND syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId " _
            '& " AND ((syStudentStatusChanges.ModDate BETWEEN " & EnrollmentStartDate & " AND " & EnrollmentEndDate & ") " _
            '& "OR ( (syStudentStatusChanges.ModDate = (SELECT MAX(syStudentStatusChanges.ModDate) FROM syStudentStatusChanges WHERE syStudentStatusChanges.ModDate <= " & EnrollmentStartDate & "  and syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId )  " _
            '& "           and syStudentStatusChanges.NewStatusID not in  (Select StatusCodeId from sySysStatus,SyStatusCodes where InSchool=0 " _
            '& " and sySysStatus.SysStatusId=SyStatusCodes.SysStatusId) )) "

            EnrollmentStartDateClause = "AND syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId " _
            & " AND ((syStudentStatusChanges.DateOfChange BETWEEN " & EnrollmentStartDate & " AND " & EnrollmentEndDate & ") " _
            & "OR ( (syStudentStatusChanges.DateOfChange = (SELECT MAX(syStudentStatusChanges.DateOfChange) FROM syStudentStatusChanges WHERE syStudentStatusChanges.DateOfChange <= " & EnrollmentStartDate & "  and syStudentStatusChanges.StuEnrollId = arStuEnrollments.StuEnrollId )  " _
            & "           and syStudentStatusChanges.NewStatusID not in  (Select StatusCodeId from sySysStatus,SyStatusCodes where InSchool=0 " _
            & " and sySysStatus.SysStatusId=SyStatusCodes.SysStatusId) )) "

        End If

        '''' Code changes by kamalesh ahuja on 6th december 2010 to resolve arlly issue id DE1265

        If strStatusCodes <> "" And EnrollmentStartDate <> "" Then

            If GetSysStatus(strStatusCodes, 10) = True Then

                EnrollmentStartDateClause = EnrollmentStartDateClause & " or arStuEnrollments.StuEnrollId in (select LOA.StuEnrollId from arStudentLOAs LOA,arStuEnrollments " _
                    & " where LOA.StuEnrollId=arStuEnrollments.StuEnrollId  and LOA.StartDate<=" & EnrollmentStartDate & " and LOA.EndDate>=" & EnrollmentStartDate & ")"
            End If
            If GetSysStatus(strStatusCodes, 21) = True Or GetSysStatus(strStatusCodes, 11) = True Then

                EnrollmentStartDateClause = EnrollmentStartDateClause & " or arStuEnrollments.StuEnrollId in (select SS.StuEnrollId from arStdSuspensions SS,arStuEnrollments " _
                    & " where SS.StuEnrollId=arStuEnrollments.StuEnrollId  and SS.StartDate<=" & EnrollmentStartDate & " and SS.EndDate>=" & EnrollmentStartDate & ")"
            End If

            If GetSysStatus(strStatusCodes, 20) = True Then

                EnrollmentStartDateClause = EnrollmentStartDateClause & " or arStuEnrollments.StuEnrollId in (select PW.StuEnrollId from arStuProbWarnings PW,arStuEnrollments " _
                    & " where PW.StuEnrollId=arStuEnrollments.StuEnrollId and PW.StartDate<=" & EnrollmentStartDate & " and PW.EndDate>=" & EnrollmentStartDate & ")"
            End If

        End If

        '' New Code Added By Vijay Ramteke On January 25, 2011 For Rally Id DE4340
        ''EnrollmentStartDateClause = EnrollmentStartDateClause & " ) "
        ''strWhere &= " AND " & paramInfo.FilterOther
        If Not EnrollmentStartDateClause = "" Then
            EnrollmentStartDateClause = EnrollmentStartDateClause & " ) "
        End If
        '' New Code Added By Vijay Ramteke On January 25, 2011 For Rally Id DE4340
        ''''''''''''''''''''''''''


        If paramInfo.BaseCAOnRegClasses = True Then

            If IsDate(EnrollmentStartDate.Trim.Substring(1, Len(EnrollmentStartDate.Trim) - 2)) And IsDate(EnrollmentEndDate.Trim.Substring(1, Len(EnrollmentEndDate.Trim) - 2)) Then

                If InStr(StrESFilterCriteria, "Between") > 0 Then

                    EnrollmentStartDateClauseCA = "and arStuEnrollments.StuEnrollId in( " _
                        & " select distinct arStuEnrollments.StuEnrollId from arStudent st, arStuEnrollments , syStatusCodes sc, syCampuses cm, adEthCodes ec,syCmpGrpCmps,syCampGrps,syCampuses , syStudentStatusChanges " _
                       & " where st.StudentId=arStuEnrollments.StudentId and arStuEnrollments.StatusCodeId=sc.StatusCodeId and st.Race=ec.EthCodeId  " _
                       & " AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId AND syCampuses.CampusId=arStuEnrollments.CampusId " _
                       & " and ((arStuEnrollments.LDA between " & EnrollmentStartDate & " and " & EnrollmentEndDate & ") or arStuEnrollments.LDA is null " _
                        & " or ((select max(MeetDate)from atClsSectAttendance csa where csa.StuEnrollId=arStuEnrollments.StuEnrollId) between " & EnrollmentStartDate & " and " & EnrollmentEndDate & ")) " _
                        & " and arStuEnrollments.startdate between " & EnrollmentStartDate & " and " & EnrollmentEndDate & " and arStuEnrollments.ExpGradDate between " & EnrollmentStartDate & " and " & EnrollmentEndDate & " and sc.SysStatusId not in (19,8) " _
                        & " and exists(select * from arResults rs, arClassSections cs, arGradeSystemDetails gsd " _
                        & " where rs.TestId=cs.ClsSectionId and rs.StuEnrollId=arStuEnrollments.StuEnrollId and rs.GrdSysDetailId=gsd.GrdSysDetailId " _
                        & " and (cs.StartDate between " & EnrollmentStartDate & " and " & EnrollmentEndDate & " and cs.EndDate between " & EnrollmentStartDate & " and " & EnrollmentEndDate & ")) "
                    EnrollmentStartDateClauseCA &= strWhere & " )"

                ElseIf InStr(StrESFilterCriteria, "Not Between") > 0 Then

                    EnrollmentStartDateClauseCA = "and arStuEnrollments.StuEnrollId in( " _
                           & " select distinct arStuEnrollments.StuEnrollId from arStudent st, arStuEnrollments , syStatusCodes sc, syCampuses cm, adEthCodes ec,syCmpGrpCmps,syCampGrps,syCampuses , syStudentStatusChanges " _
                          & " where st.StudentId=arStuEnrollments.StudentId and arStuEnrollments.StatusCodeId=sc.StatusCodeId and st.Race=ec.EthCodeId  " _
                          & " AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId AND syCampuses.CampusId=arStuEnrollments.CampusId " _
                          & " and ((arStuEnrollments.LDA Not Between " & EnrollmentStartDate & " and " & EnrollmentEndDate & ") or arStuEnrollments.LDA is null " _
                           & " or ((select max(MeetDate)from atClsSectAttendance csa where csa.StuEnrollId=arStuEnrollments.StuEnrollId) Not Between " & EnrollmentStartDate & " and " & EnrollmentEndDate & ")) " _
                           & " and arStuEnrollments.startdate Not Between " & EnrollmentStartDate & " and " & EnrollmentEndDate & " and arStuEnrollments.ExpGradDate Not Between " & EnrollmentStartDate & " and " & EnrollmentEndDate & " and sc.SysStatusId not in (19,8) " _
                           & " and exists(select * from arResults rs, arClassSections cs, arGradeSystemDetails gsd " _
                           & " where rs.TestId=cs.ClsSectionId and rs.StuEnrollId=arStuEnrollments.StuEnrollId and rs.GrdSysDetailId=gsd.GrdSysDetailId " _
                           & " and (cs.StartDate Not Between " & EnrollmentStartDate & " and " & EnrollmentEndDate & " and cs.EndDate Not Between " & EnrollmentStartDate & " and " & EnrollmentEndDate & ")) "
                    EnrollmentStartDateClauseCA &= strWhere & " )"

                End If

            ElseIf IsDate(EnrollmentStartDate.Trim.Substring(1, Len(EnrollmentStartDate.Trim) - 2)) And IsDate(EnrollmentEndDate.Trim.Substring(1, Len(EnrollmentEndDate.Trim) - 2)) = False Then

                If InStr(StrESFilterCriteria, ">") > 0 Then
                    EnrollmentStartDateClauseCA = "and arStuEnrollments.StuEnrollId in( " _
                        & " select distinct arStuEnrollments.StuEnrollId from arStudent st, arStuEnrollments, syStatusCodes sc, syCampuses cm, adEthCodes ec " _
                       & " where st.StudentId=arStuEnrollments.StudentId and arStuEnrollments.StatusCodeId=sc.StatusCodeId and st.Race=ec.EthCodeId  " _
                       & " AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId AND syCampuses.CampusId=arStuEnrollments.CampusId " _
                       & " and ((arStuEnrollments.LDA >= " & EnrollmentStartDate & " or arStuEnrollments.LDA is null )" _
                        & " or ((select max(MeetDate)from atClsSectAttendance csa where csa.StuEnrollId=arStuEnrollments.StuEnrollId) >= " & EnrollmentStartDate & " )) " _
                        & " and arStuEnrollments.startdate >= " & EnrollmentStartDate & " and arStuEnrollments.ExpGradDate >= " & EnrollmentStartDate & " and sc.SysStatusId not in (19,8) " _
                        & " and exists(select * from arResults rs, arClassSections cs, arGradeSystemDetails gsd " _
                        & " where rs.TestId=cs.ClsSectionId and rs.StuEnrollId=arStuEnrollments.StuEnrollId and rs.GrdSysDetailId=gsd.GrdSysDetailId " _
                        & " and (cs.StartDate >= " & EnrollmentStartDate & " and cs.EndDate >= " & EnrollmentStartDate & " )) "
                    EnrollmentStartDateClauseCA &= strWhere & " )"

                ElseIf InStr(StrESFilterCriteria, "<") > 0 Then

                    EnrollmentStartDateClauseCA = "and arStuEnrollments.StuEnrollId in( " _
                       & " select distinct arStuEnrollments.StuEnrollId from arStudent st, arStuEnrollments , syStatusCodes sc, syCampuses cm, adEthCodes ec " _
                       & " where st.StudentId=arStuEnrollments.StudentId and arStuEnrollments.StatusCodeId=sc.StatusCodeId and st.Race=ec.EthCodeId  " _
                       & " AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId AND syCampuses.CampusId=arStuEnrollments.CampusId " _
                      & " and ((arStuEnrollments.LDA <= " & EnrollmentStartDate & " or arStuEnrollments.LDA is null )" _
                       & " or ((select max(MeetDate)from atClsSectAttendance csa where csa.StuEnrollId=arStuEnrollments.StuEnrollId) <= " & EnrollmentStartDate & " )) " _
                       & " and arStuEnrollments.startdate <= " & EnrollmentStartDate & " and arStuEnrollments.ExpGradDate <= " & EnrollmentStartDate & " and sc.SysStatusId in not (19,8) " _
                       & " and exists(select * from arResults rs, arClassSections cs, arGradeSystemDetails gsd " _
                       & " where rs.TestId=cs.ClsSectionId and rs.StuEnrollId=arStuEnrollments.StuEnrollId and rs.GrdSysDetailId=gsd.GrdSysDetailId " _
                       & " and (cs.StartDate <= " & EnrollmentStartDate & " and cs.EndDate <= " & EnrollmentStartDate & " ))"
                    EnrollmentStartDateClauseCA &= strWhere & " )"

                End If

            End If
        End If


        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        If StudentIdentifier = "SSN" Then
            strStuId = "arStudent.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuId = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuId = "arStudent.StudentNumber AS StudentIdentifier,"
        End If

        With sb
            'Modified for 2.9 Sp1
            .Append("SELECT Distinct  ")
            .Append("       arStuEnrollments.StuEnrollId,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuId)
            .Append("       arStuEnrollments.PrgVerId,(SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS ProgVerDescrip,")
            .Append("       (SELECT PrgVerCode FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS ProgVerCode,")
            .Append("       arStuEnrollments.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE arStuEnrollments.StatusCodeId=StatusCodeId) AS StatusCodeDescrip,")
            .Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip,")
            .Append("       arStuEnrollments.ShiftId,(SELECT ShiftDescrip FROM arShifts WHERE ShiftId=arStuEnrollments.ShiftId) AS ShiftDescrip,")
            If paramInfo.FiltersByStudentGroup
                .Append(" adLeadByLeadGroups.LeadGrpId AS StudentGroupId, adLeadGroups.Descrip AS StudentGroupDescription, ")
            End If
            .Append("       arStuEnrollments.StartDate,arStuEnrollments.ExpGradDate,arStudent.StudentId ")
            .Append("FROM   arStudent,arStuEnrollments,syCmpGrpCmps,syCampGrps,syCampuses " & syStudentStatusChangesTableName)
            If paramInfo.FiltersByStudentGroup
            .Append(", adLeadByLeadGroups, adLeadGroups ")
            End If
            .Append("WHERE  arStudent.StudentId = arStuEnrollments.StudentId ")
            .Append("       AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            .Append("       AND syCampuses.CampusId=arStuEnrollments.CampusId ")
            If paramInfo.FiltersByStudentGroup
                .Append("       AND adLeadByLeadGroups.StuEnrollId = arStuEnrollments.StuEnrollId ")
                .Append("       AND adLeadByLeadGroups.LeadGrpId = adLeadGroups.LeadGrpId ")
            End If
            .Append(strWhere)

            If paramInfo.BaseCAOnRegClasses = True Then
                .Append(EnrollmentStartDateClauseCA)
            End If

            .Append(EnrollmentStartDateClause)
            .Append("ORDER BY syCampGrps.CampGrpDescrip,syCmpGrpCmps.CampGrpId,syCampuses.CampDescrip,arStuEnrollments.CampusId,ProgVerDescrip")
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "StudentListing"
            ' Add empty columns if query did not include to filter by student group
            If Not paramInfo.FiltersByStudentGroup Then
                ds.Tables(0).Columns.Add(New DataColumn("StudentGroupId", System.Type.GetType("System.Guid")))
                ds.Tables(0).Columns.Add(New DataColumn("StudentGroupDescription", System.Type.GetType("System.String")))
            End If
            ' Add new columns.
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("StudentCount", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("PrgVerIdStr", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("PrgVerLabel", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("UniqueStudentCount", System.Type.GetType("System.Int32")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    '''' Function Added by kamalesh ahuja on 6th december 2010 to resolve arlly issue id DE1265
    Public Function GetSysStatus(ByVal StrStatusCodes As String, ByVal SysStatusId As String) As Boolean

        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        With sb
            .Append("SELECT * from syStatusCodes where StatusCodeId " & StrStatusCodes & " and SysStatusId=" & SysStatusId)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables(0).Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    End Function
    Public Function GetNoShowStudents(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStuID As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        If StudentIdentifier = "SSN" Then
            strStuID = "arStudent.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuID = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuID = "arStudent.StudentNumber AS StudentIdentifier,"
        End If

        With sb
            .Append("SELECT ")
            .Append("       arStuEnrollments.StuEnrollId,arStuEnrollments.StudentId,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuID)
            .Append("       arStuEnrollments.ExpStartDate,arStuEnrollments.PrgVerId,arPrgVersions.PrgVerDescrip,arStuEnrollments.StatusCodeId,B.StatusCodeDescrip,")
            .Append("       arTerm.TermId,arTerm.TermDescrip,arTerm.StartDate,arTerm.EndDate,")
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,syCampuses.CampDescrip ")
            .Append("FROM   arStuEnrollments ,syStatusCodes B,sySysStatus C,arStudent,arPrgVersions,arTerm,syCampGrps,syCmpGrpCmps,syCampuses ")
            .Append("WHERE  arStuEnrollments.StatusCodeId=B.StatusCodeId And B.SysStatusId=C.SysStatusId ")
            .Append("       AND C.SysStatusId=8 AND C.StatusLevelId=2 AND arStudent.StudentId=arStuEnrollments.StudentId ")
            .Append("       AND arPrgVersions.PrgVerId=arStuEnrollments.PrgVerId AND arStuEnrollments.ExpStartDate>=arTerm.StartDate AND arStuEnrollments.ExpStartDate<=arTerm.EndDate ")
            .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=arStuEnrollments.CampusId ")
            .Append(strWhere)
            .Append("ORDER BY syCampGrps.CampGrpDescrip,syCampGrps.CampGrpId,syCampuses.CampDescrip,syCampuses.CampusId,")
            .Append("arTerm.StartDate,arTerm.EndDate,arTerm.TermDescrip,arPrgVersions.PrgVerDescrip ")
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "NoShowStudents"
            ' Add new columns.
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("StudentCount", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("TermIdStr", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("PrgVerIdStr", System.Type.GetType("System.String")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function


    Public Function GetStudentsNotScheduled(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStuID As String
        Dim strTerm As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        If paramInfo.FilterList <> "" Then
            Dim temp As String = paramInfo.FilterList
            Dim idx As Integer = temp.IndexOf("arTerm.TermId")
            strTerm = temp.Substring(idx)       '.Replace("arTerm.TermId", "arClassSectionTerms.TermId")
            strWhere &= " AND " & temp
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        If StudentIdentifier = "SSN" Then
            strStuID = "arStudent.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStuID = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStuID = "arStudent.StudentNumber AS StudentIdentifier,"
        End If

        With sb
            '   build query to get all students with 'Currently Attending' and 'Future Start' statuses
            .Append("SELECT")
            .Append("       arStuEnrollments.StuEnrollId,arStuEnrollments.StudentId,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuID)
            .Append("       arStuEnrollments.PrgVerId,arPrgVersions.PrgVerDescrip,")
            .Append("       arStuEnrollments.StatusCodeId,syStatusCodes.StatusCodeDescrip,")
            .Append("       arStuEnrollments.ExpGradDate,arTerm.TermId,arTerm.TermDescrip,arTerm.StartDate,arTerm.EndDate,")
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip ")
            .Append("FROM   arStuEnrollments,arStudent,syCampGrps,syCmpGrpCmps,syCampuses,arPrograms,arPrgVersions,syStatusCodes,arTerm ")
            .Append("WHERE  arStuEnrollments.StudentId=arStudent.StudentId")
            .Append("       AND arStuEnrollments.StatusCodeId=syStatusCodes.StatusCodeId ")
            .Append("       AND syStatusCodes.sysStatusId IN (9, 7) ")
            .Append("       AND arStuEnrollments.PrgVerId=arPrgVersions.PrgVerId ")
            '.Append("       AND arPrgVersions.CampGrpId=syCampGrps.CampGrpId")
            .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
            .Append("       AND syCampuses.CampusId=arStuEnrollments.CampusId ")
            .Append("       AND NOT EXISTS (SELECT * FROM arResults R,arClassSections A ")
            .Append("                       WHERE    R.StuEnrollId=arStuEnrollments.StuEnrollId AND A.ClsSectionId=R.TestId")
            .Append("                                AND A.TermId=arTerm.TermId) ")
            .Append("       AND (arPrgVersions.ProgId=arTerm.ProgId OR arTerm.ProgId is NULL) ")
            .Append(" AND arPrograms.ProgId=arPrgVersions.ProgID and arTerm.StartDate >=arStuEnrollments.ExpStartDate ")
            .Append(strWhere)
            .Append(" ORDER BY syCampGrps.CampGrpDescrip,syCampGrps.CampGrpId,syCampuses.CampDescrip,arStuEnrollments.CampusId")
            .Append(",arTerm.StartDate,arTerm.EndDate,arTerm.TermDescrip,arPrgVersions.PrgVerDescrip")
            .Append(strOrderBy)

            ''.Append("SELECT ")
            ''.Append("       arStuEnrollments.StuEnrollId,arStuEnrollments.StudentId,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName," & strStuID)
            ''.Append("       arStuEnrollments.PrgVerId,arPrgVersions.PrgVerDescrip,")
            ''.Append("       arStuEnrollments.StatusCodeId,syStatusCodes.StatusCodeDescrip,")
            ''.Append("       arStuEnrollments.ExpGradDate,arTerm.TermId,arTerm.TermDescrip,arTerm.StartDate,arTerm.EndDate,")
            ''.Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip ")
            ''.Append("FROM   arStuEnrollments,arStudent,arPrgVersions P,syCampGrps,syCmpGrpCmps,syCampuses,arTerm,arPrgVersions,syStatusCodes ")
            ''.Append("WHERE  NOT EXISTS (SELECT * FROM arResults R ")
            ''.Append("                   WHERE R.StuEnrollId=arStuEnrollments.StuE(R.GrdSysDetailId is null or R.GrdSysDetailId in(select GrdSysDetailId from arGradeSystemDetails where IsPass=1))nrollId")
            ''.Append("                   AND EXISTS (SELECT * FROM arClassSections,arClassSectionTerms")
            ''.Append("                               WHERE arClassSections.ClsSectionId=R.TestId")
            ''.Append("                               AND arClassSections.ClsSectionId=arClassSectionTerms.ClsSectionId")
            ''.Append("                               AND " & strTerm & "))")
            ''.Append("       AND arStuEnrollments.StudentId=arStudent.StudentId ")
            ''.Append("       AND arStuEnrollments.StatusCodeId=syStatusCodes.StatusCodeId ")
            ''.Append("       AND syStatusCodes.sysStatusId IN (9, 7) ")
            ''.Append("       AND arStuEnrollments.PrgVerId=P.PrgVerId ")
            ''.Append("       AND arStuEnrollments.PrgVerId=arPrgVersions.PrgVerId AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            ''.Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=arStuEnrollments.CampusId ")
            ''.Append("       AND arTerm.CampGrpId=syCampGrps.CampGrpId")
            ''.Append(strWhere)
            ''.Append("ORDER BY syCampGrps.CampGrpDescrip,syCampGrps.CampGrpId,syCampuses.CampDescrip,arStuEnrollments.CampusId,")
            ''.Append("arTerm.StartDate,arTerm.EndDate,arTerm.TermDescrip,arPrgVersions.PrgVerDescrip")
            ''.Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "StudentsNotScheduled"
            ' Add new columns.
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("StudentCount", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("PrgVerIdStr", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("TermIdStr", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("UniqueStudentCount", System.Type.GetType("System.String")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

#End Region

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

End Class
