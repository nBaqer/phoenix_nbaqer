Imports System.Data.OleDb
Imports FAME.Advantage.Common

Public Class PostAverageForCourse
    Public Function PostAverage(ByVal Resultid As String, _
                                ByVal source As String, _
                                Optional ByVal user As String = "") As String
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder
        Dim decAverage As Decimal = 0.0
        Dim strTransferId As String = ""
        Dim sb2 As New StringBuilder
        Dim strStuEnrollId, strClsSectionId As String
        Dim grdPolicyId As Integer = 0
        Dim strTestId As String
        Dim strReqId As String
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        Dim dr As OleDbDataReader

        If source = "conversion" Then
            'update arTransferGrades table
            Dim sb29 As New StringBuilder
            With sb29
                .Append(" Select Distinct ReqId,StuEnrollId from arGrdBkConversionResults where ConversionresultId='" & Resultid & "' ")
            End With
            Try
                dr = db.RunSQLDataReader(sb29.ToString)
                While dr.Read()
                    strReqId = CType(dr("ReqId"), Guid).ToString
                    strStuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
                End While
                dr.Close()
            Catch ex As System.Exception
                strReqId = ""
                strStuEnrollId = ""
            Finally
                sb.Remove(0, sb.Length)
            End Try
            With sb
                '.Append("   select Sum(t1.Score*GBWD.Weight)/Sum(GBWD.Weight) as Average ")
                '.Append("   from arGrdBkConversionResults t1,arGrdBkConversionResults t2, ")
                '.Append("   arGrdBkWgtDetails GBWD, arGrdBkWeights GBW where ")
                '.Append("   t1.ReqId=t2.ReqId and t1.StuEnrollId=t2.StuEnrollId and ")
                '.Append("   t1.ConversionresultId='" & Resultid & "' and ")
                '.Append("   GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId And ")
                '.Append("   GBWD.GrdComponentTypeId = t1.GrdComponentTypeId ")
                '.Append("   and t1.ReqId=GBW.ReqId and GBWD.Weight is not null and t1.Score is not null ")
                '.Append("    select Sum(t1.Score*GBWD.Weight)/Sum(GBWD.Weight) as Average " + vbCrLf)
                '.Append("    from " + vbCrLf)
                '.Append("    (select Distinct t2.StuEnrollId,t2.ReqId,t2.Score,t2.GrdComponentTypeId " + vbCrLf)
                '.Append("    from arGrdBkConversionResults t1,arGrdBkConversionResults t2 " + vbCrLf)
                '.Append("    where t1.StuEnrollId=t2.StuEnrollId and t1.ReqId=t2.ReqId and " + vbCrLf)
                '.Append("    t1.ConversionresultId='" & Resultid & "') t1, " + vbCrLf)
                '.Append("    arGrdBkWgtDetails GBWD, arGrdBkWeights GBW,arGrdComponentTypes GCT  " + vbCrLf)
                '.Append("    where " + vbCrLf)
                '.Append("    GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId And " + vbCrLf)
                '.Append("    GBWD.GrdComponentTypeId = t1.GrdComponentTypeId " + vbCrLf)
                '.Append("    and GBWD.GrdComponentTypeId = GCT.GrdComponentTypeId and GCT.SysComponentTypeId not in (500,503,544) " + vbCrLf)
                '.Append("    and t1.ReqId=GBW.ReqId and GBWD.Weight is not null and " + vbCrLf)
                '.Append("    t1.Score is not null " + vbCrLf)
                .Append("    Select Sum(Score*Weight)/Sum(Weight)  as Average from ( ")
                .Append("		Select Distinct GBCR.ConversionResultId as GrdBkResultId,   ")
                .Append("		(select RTRIM(Descrip) from arGrdComponentTypes where GrdComponentTypeId=GBCR.GrdComponentTypeId) as Comments,  ")
                .Append("		GBCR.Score,  ")
                .Append("		GBCR.PostDate,  ")
                .Append("		GBCR.MinResult as PassingGrade,  ")
                .Append("	    (select Weight from arGrdBkWgtDetails GBWD, arGrdBkWeights GBW where GBWD.InstrGrdBkWgtId=GBW.InstrGrdBkWgtId and GBWD.GrdComponentTypeId=GBCR.GrdComponentTypeId and GBCR.ReqId=GBW.ReqId and GBWD.Weight is not null and GBW.EffectiveDate = (Select max(EffectiveDate) from arGrdBkWeights where ReqId=GBCR.ReqId and EffectiveDate <= (select StartDate from arTerm where TermId=GBCR.TermId)) ) As Weight,  ")
                .Append("	    (select Required from arGrdBkWgtDetails GBWD, arGrdBkWeights GBW where GBWD.InstrGrdBkWgtId=GBW.InstrGrdBkWgtId and GBWD.GrdComponentTypeId=GBCR.GrdComponentTypeId and GBCR.ReqId=GBW.ReqId and GBW.EffectiveDate = (Select max(EffectiveDate) from arGrdBkWeights where ReqId=GBCR.ReqId and EffectiveDate <= (select StartDate from arTerm where TermId=GBCR.TermId)) ) As Required,  ")
                .Append("	    (select MustPass from arGrdBkWgtDetails GBWD, arGrdBkWeights GBW where GBWD.InstrGrdBkWgtId=GBW.InstrGrdBkWgtId and GBWD.GrdComponentTypeId=GBCR.GrdComponentTypeId and GBCR.ReqId=GBW.ReqId and GBW.EffectiveDate = (Select max(EffectiveDate) from arGrdBkWeights where ReqId=GBCR.ReqId and EffectiveDate <= (select StartDate from arTerm where TermId=GBCR.TermId)) ) As MustPass,  ")
                .Append("	    (select SysComponentTypeId from arGrdComponentTypes where SysComponentTypeId not in (500,503,544) and SysComponentTypeId is not null and GrdComponentTypeId=GBCR.GrdComponentTypeId) as SysComponentTypeId, ")
                .Append("	    (select Number from arGrdBkWgtDetails GBWD, arGrdBkWeights GBW where GBWD.InstrGrdBkWgtId=GBW.InstrGrdBkWgtId and GBWD.GrdComponentTypeId=GBCR.GrdComponentTypeId and GBCR.ReqId=GBW.ReqId and GBW.EffectiveDate = (Select max(EffectiveDate) from arGrdBkWeights where ReqId=GBCR.ReqId and EffectiveDate <= (select StartDate from arTerm where TermId=GBCR.TermId)) ) As HoursRequired,  ")
                .Append("		Coalesce(GBCR.Score,0) as HoursCompleted ")
                .Append("from	arGrdBkConversionResults GBCR  ")
                .Append("where   ")
                .Append("		GBCR.StuEnrollId='" & strStuEnrollId & "'   ")
                .Append("and	GBCR.ReqId='" & strReqId & "' and GBCR.Score is not null ")
                .Append(" ) R1 ")
            End With
            Try
                dr = db.RunParamSQLDataReader(sb.ToString)
                While dr.Read()
                    decAverage = CType(dr("Average"), Decimal)
                    decAverage = Math.Round(decAverage, 2)
                End While
            Catch ex As System.Exception
            Finally
                dr.Close()
                sb.Remove(0, sb.Length)
            End Try

            With sb
                .Append(" update arTransferGrades set Score=?,ModUser=?,ModDate=? where TransferId in ")
                .Append(" (Select distinct t1.TransferId from arTransferGrades t1,arGrdBkConversionResults t2 ")
                .Append(" where t1.StuEnrollId=t2.StuEnrollId and t1.ReqId=t2.ReqId and t2.ConversionresultId='" & Resultid & "') ")
            End With
            db.ClearParameters()
            If decAverage = "0.00" Then
                db.AddParameter("@Score", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Else
                db.AddParameter("@Score", decAverage, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            End If

            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return ""
            Catch ex As System.Exception
                Return ex.Message
            Finally
                sb.Remove(0, sb.Length)
                db.CloseConnection()
            End Try
        Else
            'update or insert arResults table
            With sb
                .Append(" Select Distinct T.ClsSectionId,T.StuEnrollId,")
                '.Append(" (select Grdpolicyid from arGrdBkWgtDetails where InstrGrdBkWgtDetailId=T.InstrGrdBkWgtDetailId) as grdPolicyId ")
                .Append("  (select isnull(max(Grdpolicyid),0) from arGrdBkWgtDetails,arGrdBkWeights where  ")
                .Append(" arGrdBkWeights.InstrGrdBkWgtId = arGrdBkWgtDetails.InstrGrdBkWgtId And arGrdBkWeights.ReqId = CS.ReqId  ) as grdPolicyId ")
                .Append(" from arGrdBkResults T,arClassSections CS where T.ClsSectionId=CS.ClsSectionId and T.GrdBkResultId='" & Resultid & "' ")
            End With
            Try
                dr = db.RunSQLDataReader(sb.ToString)
                While dr.Read()
                    strClsSectionId = CType(dr("ClsSectionId"), Guid).ToString
                    strStuEnrollId = CType(dr("StuEnrollId"), Guid).ToString
                    If Not dr("GrdPolicyId") Is System.DBNull.Value Then grdPolicyId = dr("Grdpolicyid")
                End While
                dr.Close()
            Catch ex As System.Exception
                strClsSectionId = ""
                strStuEnrollId = ""
            Finally
                sb.Remove(0, sb.Length)
            End Try


            'If grdPolicyId = 0 Then

            '    With sb
            '        '.Append(" select Sum(t1.Score*t2.Weight)/Sum(Weight) as Average from arGrdBkResults t1,arGrdBkWgtDetails t2,arGrdComponentTypes t3 where ")
            '        '.Append(" t1.InstrGrdBkWgtDetailId=t2.InstrGrdBkWgtDetailId and t2.GrdComponentTypeId = t3.GrdComponentTypeId  ")
            '        '.Append(" and t3.SysComponentTypeId not in (500,503,544) ")
            '        '.Append(" and t2.Weight is not null and t1.Score is not null and ")
            '        '.Append(" ClsSectionId='" & strClsSectionId & "' and StuEnrollId='" & strStuEnrollId & "' ")
            '        .Append("    Select Sum(Score*Weight)/Sum(Weight)  as Average from ( ")
            '        .Append(" Select	Distinct GBR.GrdBkResultId,     ")
            '        .Append(" GBR.Comments,   GBR.Score,   ")
            '        .Append("		(select Min(MinVal) from arGradeScaleDetails GCD, arGradeSystemDetails GSD where GCD.GrdSysDetailId=GSD.GrdSysDetailId and GSD.IsPass=1) as PassingGrade,   ")
            '        .Append("		(select Weight from arGrdBkWgtDetails where InstrGrdBkWgtDetailId=GBR.InstrGrdBkWgtDetailId and Weight is not null) as Weight,   ")
            '        .Append(" 		(select Required from arGrdBkWgtDetails where InstrGrdBkWgtDetailId=GBR.InstrGrdBkWgtDetailId) as Required,   ")
            '        .Append("		(select MustPass from arGrdBkWgtDetails where InstrGrdBkWgtDetailId=GBR.InstrGrdBkWgtDetailId) as MustPass,  ")
            '        .Append("		(select SysComponentTypeId from arGrdComponentTypes where SysComponentTypeId not in (500,503,544) and SysComponentTypeId is not null and GrdComponentTypeId=(select GrdComponentTypeId from arGrdBkWgtDetails where InstrGrdBkWgtDetailId=GBR.InstrGrdBkWgtDetailId)) As SysComponentTypeId,  ")
            '        .Append("		(select Number from arGrdBkWgtDetails where InstrGrdBkWgtDetailId=GBR.InstrGrdBkWgtDetailId) as HoursRequired  ")
            '        .Append("from	arGrdBkResults GBR   ,arGrdBkWgtDetails,arGrdComponentTypes    ")
            '        .Append(" where ")
            '        .Append(" GBR.InstrGrdBkWgtDetailId=arGrdBkWgtDetails.InstrGrdBkWgtDetailId and ")
            '        .Append(" arGrdBkWgtDetails.GrdComponentTypeId=arGrdComponentTypes.GrdComponentTypeId and ")
            '        .Append(" arGrdComponentTypes.SysComponentTypeId not in(500,503,544) and ")
            '        .Append(" GBR.StuEnrollId='" & strStuEnrollId & "' ")
            '        .Append(" and GBR.ClsSectionId = '" & strClsSectionId & "' and GBR.Score is not null ")
            '        .Append(" ) R1 ")
            '    End With
            '    Try
            '        dr = db.RunSQLDataReader(sb.ToString)
            '        While dr.Read()
            '            decAverage = CType(dr("Average"), Decimal)
            '            decAverage = Math.Round(decAverage, 2)
            '        End While
            '        dr.Close()
            '    Catch ex As System.Exception
            '    Finally
            '        sb.Remove(0, sb.Length)
            '    End Try
            'Else
            decAverage = getAverage(strStuEnrollId, strClsSectionId)
            'End If
            With sb
                .Append(" update arResults set Score=?,ModUser=?,ModDate=? where TestId='" & strClsSectionId & "' and StuEnrollId='" & strStuEnrollId & "' ")
            End With
            db.ClearParameters()
            If decAverage = "0.00" Then
                db.AddParameter("@Score", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Else
                db.AddParameter("@Score", decAverage, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            End If
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return ""
            Catch ex As System.Exception
                Return ex.Message
            Finally
                sb.Remove(0, sb.Length)
                db.CloseConnection()
            End Try
        End If
    End Function
    Private Function getAverage(ByVal strStuEnrollId As String, ByVal clsSectionId As String) As Decimal
        Dim dtGBWResults As DataTable
        Dim oldGBWDetailId As String = ""
        Dim weight As Decimal
        Dim grdPolicyId As Integer
        Dim param As Integer
        Dim gbwUtil As GrdBkWgtUtility
        Dim arrRows() As DataRow
        Dim accumWgt As Decimal = 0.0
        Dim ttlWgt As Decimal = 0.0
        Dim intFinalScore As Decimal = 0.0
        Dim intWeight As Decimal
        dtGBWResults = (New TransferGradeDB).GetGBWResultsForCourseLevelForExams(strStuEnrollId, clsSectionId, True)



        'Modified by balaji on 04/30/2007 to fix Mantis 11012
        'the total weight and accumWgt needs to be reset for every student otherwise 
        'its keeps adding those weights for all students registered in that
        'class and the final score is miscalculated.
        ttlWgt = 0
        accumWgt = 0
        For Each row As DataRow In dtGBWResults.Rows
            If oldGBWDetailId = "" Or oldGBWDetailId <> row("InstrGrdBkWgtDetailId").ToString Then
                oldGBWDetailId = row("InstrGrdBkWgtDetailId").ToString
                If row.IsNull("Weight") Then
                    weight = 0
                Else
                    weight = row("Weight")
                End If

                If row.IsNull("GrdPolicyId") Then
                    grdPolicyId = 0
                Else
                    grdPolicyId = row("GrdPolicyId")
                End If
                If row.IsNull("Parameter") Then
                    param = 0
                Else
                    param = row("Parameter")
                End If

                'Get results for Grading Component Type
                arrRows = dtGBWResults.Select("InstrGrdBkWgtDetailId='" & oldGBWDetailId & "'")
                '
                If arrRows.GetLength(0) > 0 Then
                    ttlWgt += weight
                    'Compute score using Strategy pattern
                    If Not (grdPolicyId = 0) Then
                        gbwUtil = New GrdBkWgtUtility(arrRows, weight, grdPolicyId, param)
                    Else
                        gbwUtil = New GrdBkWgtUtility(arrRows, weight)
                    End If
                    'Use gbwUtil.Weight property
                    accumWgt += gbwUtil.Weight
                End If
            End If
        Next

        If ttlWgt > 0 Then
            intFinalScore = System.Math.Round((accumWgt / ttlWgt) * 100, 2)
            intWeight = ttlWgt
        End If
        Return intFinalScore
    End Function
    Public Function UpdateScoreForComponent(ByVal stuEnrollId As String, Optional ByVal ClsSectionId As String = "", Optional ByVal User As String = "sa") As String
        Dim db As New DataAccess
        Dim sbGetCourses As New StringBuilder
        Dim dsGetCourses As New DataSet
        Dim strCourses As String = ""
        Dim dt As DataTable
        Dim strComponentTypeId As String = ""
        Dim CourseId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sbgetCourse As New StringBuilder
        Dim drgetCourse As OleDbDataReader
        With sbgetCourse
            .Append(" select Top 1 ReqId from arClassSections where ClsSectionId=? ")
        End With
        db.AddParameter("@TestId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            drgetCourse = db.RunParamSQLDataReader(sbgetCourse.ToString)
            While drgetCourse.Read
                If Not drgetCourse("ReqId") Is System.DBNull.Value Then CourseId = CType(drgetCourse("ReqId"), Guid).ToString Else CourseId = ""
            End While
        Catch ex As Exception
        Finally
            db.ClearParameters()
            sbgetCourse.Remove(0, sbgetCourse.Length)
            drgetCourse.Close()
        End Try


        dt = GetAllComponentsByCourse(stuEnrollId, CourseId, User)
        If dt.Rows.Count >= 1 Then
            For Each row As DataRow In dt.Rows
                strComponentTypeId &= row("GRDComponentTypeId").ToString & "','"
            Next
            strComponentTypeId = Mid(strComponentTypeId, 1, InStrRev(strComponentTypeId, "'") - 2)
        End If

        'Validate if all grade book components has been graded
        Dim strSQLValidate As New StringBuilder
        Dim intValidateConversion As Integer = 0
        Dim intValidateAdvantage As Integer = 0
        Dim intValidateIfConversionDataExists As Integer = 0
        Dim intValidateIfAdvantageDataExists As Integer = 0
        Dim strMessage As String = ""

        With strSQLValidate
            'Query Modified by Saraswathi Lakshmanan
            .Append(" select count(*) from arGrdBkConversionResults where ")
            '.Append(" ReqId in (Select Distinct ReqId from arReqs where Descrip='" & CourseId & "') ")
            .Append(" ReqId ='" & CourseId & "' ")

            If Not strComponentTypeId = "" Then
                .Append(" and GrdComponentTypeId in ('")
                .Append(strComponentTypeId)
                .Append(") ")
            End If
            .Append(" and StuEnrollId=? ")
        End With
        db.ClearParameters()
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            intValidateIfConversionDataExists = db.RunParamSQLScalar(strSQLValidate.ToString)
        Catch ex As System.Exception
            intValidateIfConversionDataExists = 0
        Finally
            strSQLValidate.Remove(0, strSQLValidate.Length)
            db.ClearParameters()
        End Try


        With strSQLValidate

            '''' Code Modified by kamalesh On 14th Sept 2010 to resolve mantis issue id 17833

            ''.Append(" select count(*) from arGrdBkResults where ")
            .Append(" select count(*) from arGrdBkResults GBR,arGrdBkWgtDetails GBWD where GBR.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")

            If Not strComponentTypeId = "" Then
                .Append(" and GrdComponentTypeId in ('")
                .Append(strComponentTypeId)
                .Append(") ")
                ''.Append(") and ")
            End If

            ''.Append(" StuEnrollId=? ")
            .Append("and StuEnrollId=? ")

            ''''''''''''''''''''''''''''
            ''Added by Saraswathi Lakshmanan
            .Append(" and ClsSectionId=? ")

        End With
        db.ClearParameters()
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@TestId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            intValidateIfAdvantageDataExists = db.RunParamSQLScalar(strSQLValidate.ToString)
        Catch ex As System.Exception
            intValidateIfAdvantageDataExists = 0
        Finally
            strSQLValidate.Remove(0, strSQLValidate.Length)
            db.ClearParameters()
        End Try


        If intValidateIfConversionDataExists >= 1 Then
            With strSQLValidate
                .Append(" select count(*) from arGrdBkConversionResults where Score is NULL and ")
                ' .Append(" ReqId in (Select Distinct ReqId from arReqs where Descrip='" & CourseId & "') ")

                ''Query modified by Saraswathi Lakshmanan
                .Append(" ReqId ='" & CourseId & "' ")

                If Not strComponentTypeId = "" Then
                    .Append(" and GrdComponentTypeId in ('")
                    .Append(strComponentTypeId)
                    .Append(") ")
                End If
                .Append(" and StuEnrollId=? ")

            End With
            db.ClearParameters()
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Try
                intValidateConversion = db.RunParamSQLScalar(strSQLValidate.ToString)
            Catch ex As System.Exception
                intValidateConversion = 0
            Finally
                strSQLValidate.Remove(0, strSQLValidate.Length)
                db.ClearParameters()
            End Try

            If intValidateConversion = 0 Then
                strMessage = "Cannot remove course"
            End If
        End If

        'Check Conversion Table for grade posted
        If intValidateIfAdvantageDataExists >= 1 Then
            With strSQLValidate
                ''MOdified by Saraswathi Lakshmanan
                '.Append(" select count(*) from arGrdBkResults where Score is NULL  ")
                '''' Code Modified by kamalesh On 14th Sept 2010 to resolve mantis issue id 17833
                ''.Append(" select count(*) from arGrdBkResults where Score is not NULL  ")
                .Append(" select count(*) from arGrdBkResults GBR,arGrdBkWgtDetails GBWD where GBR.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId and Score is not NULL ")

                If Not strComponentTypeId = "" Then
                    .Append(" and GrdComponentTypeId in ('")
                    .Append(strComponentTypeId)
                    .Append(") ")
                End If
                '''''''''''''''''''''''''
                .Append(" and StuEnrollId=? ")
                ''Added by Saraswathi Lakshmanan
                .Append(" and ClsSectionId=? ")
            End With
            db.ClearParameters()
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TestId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Try
                intValidateAdvantage = db.RunParamSQLScalar(strSQLValidate.ToString)
            Catch ex As System.Exception
                intValidateAdvantage = 0
            Finally
                strSQLValidate.Remove(0, strSQLValidate.Length)
                db.ClearParameters()
            End Try
            ''MOdified by Saraswathi Lakshmanan
            'If intValidateAdvantage = 0 Then
            If intValidateAdvantage > 0 Then
                strMessage = "Cannot remove course"
            End If
        End If
        If Not strMessage = "" Then
            '''' Code Modified by kamalesh On 14th Sept 2010 to resolve mantis issue id 17833
            ''Return "The class cannot be removed as score has been posted for all the grade components"
            Return "The class cannot be removed as scores have been posted for one or more grade components"
            ''''''''''
            Exit Function
        End If

        '   add parameters values to the query
        If Not strComponentTypeId = "" Then
            Dim strSQL As New StringBuilder
            'update arGrdBkConversionResults table
            With strSQL
                .Append(" Update arGrdBkConversionResults set Score=NULL,ModUser=?,ModDate=? where  ")
                '.Append(" ReqId in (Select Distinct ReqId from arReqs where Descrip='" & CourseId & "') ")
                'Query MOdified by Saraswathi lakshmanan
                .Append(" ReqId ='" & CourseId & "' ")

                If Not strComponentTypeId = "" Then
                    .Append(" and GrdComponentTypeId in ('")
                    .Append(strComponentTypeId)
                    .Append(") ")
                End If
                .Append(" and StuEnrollId=? ")
            End With
            db.ClearParameters()
            db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
            Catch ex As System.Exception
            Finally
                strSQL.Remove(0, strSQL.Length)
                db.ClearParameters()
            End Try

            'Update arGrdBkResults table
            With strSQL
                '''' Code Modified by kamalesh On 14th Sept 2010 to resolve mantis issue id 17833

                '' .Append(" update arGrdBkResults set Score=NULL,ModUser=?,ModDate=? where StuEnrollId=? ")
                .Append(" update arGrdBkResults set Score=NULL,ModUser=?,ModDate=? where StuEnrollId=? and InstrGrdBkWgtDetailId in ( select InstrGrdBkWgtDetailId from arGrdBkWgtDetails where ")

                If Not strComponentTypeId = "" Then
                    .Append(" GrdComponentTypeId in ('")
                    .Append(strComponentTypeId)
                    .Append(")) ")
                End If
                '''''''''''''''''''''

                '''' Code Added by kamalesh On 13th Sept 2010 to resolve mantis issue id 17833
                .Append(" and ClsSectionId=? ")
                ''''''''''''''
            End With
            db.ClearParameters()
            db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '''' Code Added by kamalesh On 13th Sept 2010 to resolve mantis issue id 17833
            db.AddParameter("@TestId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ''''

            Try
                db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
            Catch ex As System.Exception
            Finally
                strSQL.Remove(0, strSQL.Length)
                db.ClearParameters()
            End Try
        End If
        Return ""
    End Function
    Public Function GetAllComponentsByCourse(ByVal stuEnrollId As String, Optional ByVal CourseId As String = "", Optional ByVal User As String = "sa") As DataTable
        '   connect to the database
        Dim db As New DataAccess
        Dim sbGetCourses As New StringBuilder
        Dim dsGetCourses As New DataSet
        Dim strCourses As String = ""
        Dim dt As DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select Distinct ")
            .Append("		GBWD.Seq, ")
            .Append("		GCT.GRDComponentTypeId,  ")
            .Append("		isnull(GBWD.Code,GCT.Code) as Code, ")
            .Append("		GCT.Descrip As ClinicService,'advantage' as source  ")
            .Append("from	arGrdComponentTypes GCT, arGrdBkWgtDetails GBWD  ")
            .Append("where  ")
            .Append("		GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId  ")
            .Append("       and GBWD.InstrGrdBkWgtId  in ")
            .Append(" (select Distinct InstrGrdBkWgtId from arGrdBkWeights where ")
            If Not CourseId = "" Then
                ''Query Modified by Saraswathi lakshmanan
                '.Append(" ReqId in (Select Distinct ReqId from arReqs where Descrip='" & CourseId & "') ")

                .Append(" ReqId ='" & CourseId & "' ")
            End If
            .Append(" ) ")
            .Append(" and	GBWD.Number >= 1  ")
            .Append("union ")
            .Append("select Distinct ")
            .Append("		GBWD.Seq, ")
            .Append("		GCT.GRDComponentTypeId, ")
            .Append("		isnull(GBWD.Code,GCT.Code) as Code, ")
            .Append("		GCT.Descrip,'conversion' as source  ")
            .Append("from	arGrdComponentTypes GCT, arGrdBkWgtDetails GBWD  ")
            .Append("where  ")
            .Append("		GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId  ")
            .Append("       and GBWD.InstrGrdBkWgtId  in ")
            .Append(" (select Distinct InstrGrdBkWgtId from arGrdBkWeights where ")
            If Not CourseId = "" Then
                ''Query Modified by Saraswathi lakshmanan
                '.Append(" ReqId in (Select Distinct ReqId from arReqs where Descrip='" & CourseId & "') ")
                .Append(" ReqId ='" & CourseId & "' ")
            End If
            .Append(" ) ")
            .Append("and		GBWD.Number >= 1  ")
            .Append("and		exists	(Select * from arGrdBkConversionResults where StuEnrollId= ? and GrdComponentTypeId=GCT.GrdComponentTypeId) ")
        End With
        '   StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '   return datatable
        dt = db.RunParamSQLDataSet(sb.ToString).Tables(0)
        Return dt
    End Function
End Class
Public Class GrdBkWgtUtility
    Private m_score As Decimal
    Private m_weight As Decimal
    Private m_WGT As Decimal
    Private m_resultsDT As DataTable

    Public Sub New(ByVal arrRows() As DataRow, ByVal weight As Decimal, ByVal grdPolicyId As Integer, ByVal param As Integer)
        MyBase.New()

        m_WGT = weight
        Dim dt As DataTable = BuildScoreTable(arrRows)

        Dim strategyObj As GradingPolicyStrategyPattern
        strategyObj = GradingPolicyFactory.CreateGradingPolicyObj(grdPolicyId)
        m_resultsDT = strategyObj.GetResultsSet(dt, param)

        m_score = ComputeScore()
        m_weight = ComputeWeight()
    End Sub

    Public Sub New(ByVal arrRows() As DataRow, ByVal weight As Decimal)
        MyBase.New()

        m_WGT = weight

        'No Grading Policy, so there is no call to the Strategy Pattern
        m_resultsDT = BuildScoreTable(arrRows)

        m_score = ComputeScore()
        m_weight = ComputeWeight()
    End Sub

    Public ReadOnly Property Score() As Decimal
        Get
            Return System.Math.Round(m_score, 2)
        End Get
    End Property
    Public ReadOnly Property Weight() As Decimal
        Get
            Return m_weight 'System.Math.Round(m_weight, 0)
        End Get
    End Property

    Private Function BuildScoreTable(ByVal arrRows() As DataRow) As DataTable
        Dim dt As New DataTable
        Dim row As DataRow

        dt.Columns.Add("Score", GetType(System.Decimal))
        dt.Columns.Add("PostDate", GetType(System.DateTime))

        For Each dr As DataRow In arrRows
            row = dt.NewRow
            row("Score") = dr("Score")
            row("PostDate") = dr("PostDate")
            dt.Rows.Add(row)
        Next

        Return dt
    End Function
    Private Function ComputeScore() As Decimal
        Dim sum As Decimal = 0
        Dim score As Decimal = 0
        Dim int_resultsDT As Integer = 0

        If m_resultsDT.Rows.Count > 0 Then
            int_resultsDT = m_resultsDT.Rows.Count
        End If

        For Each dr As DataRow In m_resultsDT.Rows
            If dr("Score") Is System.DBNull.Value Then
                'there may be duplicate scores and while calculating average leave out the 
                'row with null score
                sum += 0
                int_resultsDT = int_resultsDT - 1
            Else
                sum += dr("Score")
            End If
        Next
        If int_resultsDT > 0 Then
            score = sum / int_resultsDT
        End If
        Return score
    End Function
    Private Function ComputeWeight() As Decimal
        Return (m_score * m_WGT) / 100
    End Function
End Class
Public Class GradingPolicyFactory

    Public Shared Function CreateGradingPolicyObj(ByVal grdPolicy As GradingPolicyEnum) As GradingPolicyStrategyPattern
        Select Case grdPolicy
            Case GradingPolicyEnum.DropLowest
                Return New DropLowest
            Case GradingPolicyEnum.Best
                Return New Best
            Case GradingPolicyEnum.Latest
                Return New Latest
        End Select
    End Function
End Class
Public Interface GradingPolicyStrategyPattern
    Function GetResultsSet(ByVal resultSet As DataTable, Optional ByVal Param As Integer = 0) As DataTable
End Interface
Public Class DropLowest
    Implements GradingPolicyStrategyPattern
    Public Function GetResultsSet(ByVal resultSet As DataTable, Optional ByVal Param As Integer = 0) As DataTable Implements GradingPolicyStrategyPattern.GetResultsSet
        Dim db As New TerminateStudent
        Dim LowestScore As Integer
        Dim dr As DataRow
        Dim i As Integer
        Dim aRows As DataRow()



        If resultSet.Rows.Count > 0 Then
            'Check to see if the number of rows in the datatable is atleast as many as the Params.
            'Else just send back the dataset w/out altering any rows.
            If resultSet.Rows.Count <= Param Then
                Return resultSet
            End If


            'If Param = 0 Then
            '    'Drop lowest score of student (always in the first row of table)
            '    LowestScore = resultSet.Compute("MIN(Score)", "")
            '    dr = resultSet.Rows.Find(LowestScore)
            '    resultSet.Rows.Remove(dr)
            '    resultSet.AcceptChanges()
            '    Return resultSet
            'End If


            For i = 0 To Param - 1
                LowestScore = resultSet.Compute("MIN(Score)", "")
                aRows = resultSet.Select("Score = '" & LowestScore & "'")
                dr = aRows(0)
                resultSet.Rows.Remove(dr)
            Next

        End If
        resultSet.AcceptChanges()

        'Drop lowest score of student 
        Return resultSet
    End Function
End Class
Public Class Best
    Implements GradingPolicyStrategyPattern
    Public Function GetResultsSet(ByVal resultSet As DataTable, Optional ByVal Param As Integer = 0) As DataTable Implements GradingPolicyStrategyPattern.GetResultsSet
        Dim db As New TerminateStudent
        'Dim LowestScore As Integer
        'Dim dr As DataRow
        Dim i As Integer
        Dim dt As New DataTable
        Dim row As DataRow
        Dim FinalDT As New DataTable


        ''Add a column called TblName and another called TblPK to the dtTables datatable
        Dim col2 As DataColumn = FinalDT.Columns.Add("Score", GetType(Integer))

        'Check to see if the number of rows in the datatable is atleast as many as the Params.
        'Else just send back the dataset w/out altering any rows.
        If resultSet.Rows.Count <= Param Then
            Return resultSet
        End If



        If resultSet.Rows.Count > 0 Then
            Dim dv As DataView = New DataView(resultSet)
            With dv
                .Sort = "Score DESC"
                .RowStateFilter = DataViewRowState.CurrentRows
                .RowFilter = ""
            End With

            For i = 0 To Param - 1

                'Copy the numreqs datatable structure to a new datatable to
                'have the consolidated data for each lead group.           
                FinalDT.TableName = "FinalDT"

                row = FinalDT.NewRow()

                row("Score") = dv(i)("Score")

                FinalDT.Rows.Add(row)
            Next

        End If

        'Drop lowest score of student 
        Return FinalDT
    End Function
End Class
Public Class Latest
    Implements GradingPolicyStrategyPattern
    Public Function GetResultsSet(ByVal resultSet As DataTable, Optional ByVal Param As Integer = 0) As DataTable Implements GradingPolicyStrategyPattern.GetResultsSet
        Dim db As New TerminateStudent
        'Dim LowestScore As Integer
        'Dim dr As DataRow
        Dim i As Integer
        Dim dt As New DataTable
        Dim row As DataRow
        Dim FinalDT As New DataTable


        ''Add a column called TblName and another called TblPK to the dtTables datatable
        Dim col2 As DataColumn = FinalDT.Columns.Add("Score", GetType(Integer))

        'Check to see if the number of rows in the datatable is atleast as many as the Params.
        'Else just send back the dataset w/out altering any rows.
        If resultSet.Rows.Count <= Param Then
            Return resultSet
        End If



        If resultSet.Rows.Count > 0 Then
            Dim dv As DataView = New DataView(resultSet)
            With dv
                .Sort = "PostDate DESC"
                .RowStateFilter = DataViewRowState.CurrentRows
                .RowFilter = ""
            End With

            For i = 0 To Param - 1

                'Copy the numreqs datatable structure to a new datatable to
                'have the consolidated data for each lead group.           
                FinalDT.TableName = "FinalDT"

                row = FinalDT.NewRow()

                row("Score") = dv(i)("Score")

                FinalDT.Rows.Add(row)
            Next

        End If

        'Drop lowest score of student 
        Return FinalDT
    End Function
End Class

