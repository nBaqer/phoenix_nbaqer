Imports FAME.Advantage.Common

Public Class AidReceivedDB

#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
    Private m_AgingDates As AgingBalance

#End Region

#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

    Public Property AgingDates() As AgingBalance
        Get
            Return m_AgingDates
        End Get
        Set(ByVal Value As AgingBalance)
            m_AgingDates = Value
        End Set
    End Property

#End Region

#Region "Public Methods"

    Public Function GetAidReceivedByStudentDate(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStudentId As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        If StudentIdentifier = "SSN" Then
            strStudentId = "G.SSN AS StudentIdentifier "
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStudentId = "F.EnrollmentId AS StudentIdentifier "
        ElseIf StudentIdentifier = "StudentId" Then
            strStudentId = "G.StudentNumber AS StudentIdentifier "
        End If
        With sb
            'Modified by Michelle R. Rodriguez on 03/30/2005
            'Use of new tables: faStudentAwards, faStudentAwardSchedule and saPmtDisbRel
            .Append("SELECT ")
            .Append("       syCampGrps.CampGrpId,REPLACE(syCampGrps.CampGrpDescrip,'''','') as CampGrpDescrip,syCampuses.CampusId,REPLACE(syCampuses.CampDescrip,'''','') as CampDescrip, ")
            .Append("       C.AwardTypeId,saFundSources.FundSourceDescrip AS AwardTypeDescrip,'Aid Disb' AS TransactionType,")
            .Append("       saTransactions.TransDate,saTransactions.TransReference,A.Amount AS TransAmount,")
            .Append("       saTransactions.TransDescrip,G.LastName,G.FirstName,G.MiddleName,")
            .Append(strStudentId)
            .Append("FROM   saTransactions,saPmtDisbRel A,faStudentAwardSchedule B,faStudentAwards C,")
            .Append("       saFundSources,arStuEnrollments F,arStudent G,syCampGrps,syCmpGrpCmps,syCampuses ")
            .Append("WHERE  A.TransactionId=saTransactions.TransactionId AND A.AwardScheduleId=B.AwardScheduleId ")
            .Append("       AND C.StudentAwardId=B.StudentAwardId AND C.AwardTypeId=saFundSources.FundSourceId ")
            .Append("       AND saTransactions.StuEnrollId=F.StuEnrollId AND saTransactions.IsPosted=1 ")
            .Append("       AND F.StudentId=G.StudentId ")
            .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=F.CampusId ")
            .Append("       AND saTransactions.Voided=0 ")
            .Append(strWhere)
            .Append(" UNION ALL ")
            .Append("SELECT ")
            .Append("       syCampGrps.CampGrpId,REPLACE(syCampGrps.CampGrpDescrip,'''','') as CampGrpDescrip,syCampuses.CampusId,REPLACE(syCampuses.CampDescrip,'''','') as CampDescrip,")
            .Append("       C.AwardTypeId,saFundSources.FundSourceDescrip AS AwardTypeDescrip,'Aid Refund' AS TransactionType,")
            .Append("       saTransactions.TransDate,saTransactions.TransReference,(A.RefundAmount * - 1) AS TransAmount,")
            .Append("       saTransactions.TransDescrip,G.LastName,G.FirstName,G.MiddleName,")
            .Append(strStudentId)
            .Append("FROM   saRefunds A,saTransactions,faStudentAwards C,saFundSources,")
            .Append("       arStuEnrollments F, arStudent G,syCampGrps,syCmpGrpCmps,syCampuses,faStudentAwardSchedule ")
            .Append("WHERE  A.RefundTypeId=1 AND A.TransactionId=saTransactions.TransactionId ")
            .Append("       AND saTransactions.IsPosted=1 ")
            .Append("       AND A.FundSourceId=C.AwardTypeId ")
            .Append("       AND C.AwardTypeId=saFundSources.FundSourceId ")
            .Append("       AND saTransactions.StuEnrollId=F.StuEnrollId ")
            .Append("       AND saTransactions.StuEnrollId=C.StuEnrollId ")
            .Append("       AND F.StudentId=G.StudentId AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=F.CampusId ")
            .Append("       AND saTransactions.Voided=0 ")
            .Append("       AND C.StudentAwardId=faStudentAwardSchedule.StudentAwardId ")
            .Append("       AND A.AwardScheduleId=dbo.faStudentAwardSchedule.AwardScheduleId ")
            .Append(strWhere)
            .Append(" ORDER BY REPLACE(syCampGrps.CampGrpDescrip, '''', ''),REPLACE(syCampuses.CampDescrip, '''', ''),saFundSources.FundSourceDescrip,")
            .Append("       TransDate,TransactionType,G.LastName,G.FirstName,G.MiddleName,TransDescrip")
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "AidReceivedByStudentDate"
            'Add new columns.
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("Year", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("Month", System.Type.GetType("System.Int32")))
            '
            If ds.Tables(0).Rows.Count > 0 Then
                'Create table to store Total Amounts per Campus Groups and Campuses.
                Dim dt As New DataTable("TotalAmounts")
                Dim dr2 As DataRow
                dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("CampDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("CampGrpTotal", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("CampusTotal", System.Type.GetType("System.Decimal")))
                ds.Tables.Add(dt)   'Add TotalAmounts table to ds dataset.

                'Create table to store Total by Month.
                Dim dt2 As New DataTable("MonthTotal")
                Dim row As DataRow
                dt2.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
                dt2.Columns.Add(New DataColumn("CampDescrip", System.Type.GetType("System.String")))
                dt2.Columns.Add(New DataColumn("AwardTypeDescrip", System.Type.GetType("System.String")))
                dt2.Columns.Add(New DataColumn("Year", System.Type.GetType("System.Int32")))
                dt2.Columns.Add(New DataColumn("Month", System.Type.GetType("System.Int32")))
                dt2.Columns.Add(New DataColumn("Label", System.Type.GetType("System.String")))
                dt2.Columns.Add(New DataColumn("MonthTotal", System.Type.GetType("System.Decimal")))
                ds.Tables.Add(dt2)  'Add MonthTotal table to ds dataset.
            End If
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Shared Function GetMonthTotal(ByVal CampusGrpId As String, ByVal CampusId As String, _
                                        ByVal AidPrg As String, ByVal TransDate As DateTime) As Decimal
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim dFrame As New DateFrame(TransDate)
        Dim SumTotal As Decimal = 0

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            'Modified by Michelle R. Rodriguez on 03/30/2005
            'Use of new tables: faStudentAwards, faStudentAwardSchedule and saPmtDisbRel
            .Append("SELECT DISTINCT")
            .Append("       B.TransactionId,B.TransAmount AS MonthTotal ")
            .Append("FROM   saTransactions B,faStudentAwards C,saFundSources D,arStuEnrollments F,")
            .Append("       arStudent G,syCampGrps,syCmpGrpCmps,syCampuses,saPmtDisbRel PD, faStudentAwardSchedule SAS ")
            .Append("WHERE  PD.TransactionId=B.TransactionId AND B.IsPosted=1 ")
            '.Append("WHERE EXISTS (SELECT * FROM saPmtDisbRel PD ")
            '.Append("               WHERE PD.TransactionId = B.TransactionId ")
            '.Append("               AND SAS.AwardScheduleId=PD.AwardScheduleId) ")
            .Append("       AND SAS.StudentAwardId=C.StudentAwardId AND SAS.AwardScheduleId=PD.AwardScheduleId ")
            .Append("       AND C.AwardTypeId=D.FundSourceId AND B.StuEnrollId=F.StuEnrollId ")
            .Append("       AND F.StudentId=G.StudentId AND syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
            .Append("       AND syCampuses.CampusId=F.CampusId AND ? <= B.TransDate AND B.TransDate <= ? ")
            .Append("       AND syCmpGrpCmps.CampGrpId = ? AND syCmpGrpCmps.CampusId = ? AND C.AwardTypeId = ? ")
            .Append("       AND B.Voided=0 ")
            .Append("UNION ALL ")
            .Append("SELECT DISTINCT")
            .Append("       B.TransactionId,B.TransAmount AS MonthTotal ")
            .Append("FROM   saRefunds A,saTransactions B,faStudentAwards C,saFundSources D,arStuEnrollments F, arStudent G,syCampGrps,syCmpGrpCmps,syCampuses ")
            .Append("WHERE  A.RefundTypeId=1 AND A.TransactionId=B.TransactionId AND B.IsPosted=1 ")
            .Append("       AND A.FundSourceId=C.AwardTypeId AND C.AwardTypeId=D.FundSourceId ")
            .Append("       AND B.StuEnrollId=F.StuEnrollId ")
            .Append("       AND B.StuEnrollId=C.StuEnrollId ")
            .Append("       AND F.StudentId=G.StudentId AND syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=F.CampusId ")
            .Append("       AND ? <= B.TransDate AND B.TransDate <= ? AND syCmpGrpCmps.CampGrpId = ? ")
            .Append("       AND syCmpGrpCmps.CampusId = ? AND C.AwardTypeId = ? ")
            .Append("       AND B.Voided=0 ")
        End With

        ' Add parameters to Parameter list
        db.AddParameter("@TransDate", dFrame.StartDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", dFrame.EndDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@CampGrpId", CampusGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@AwardTypeId", AidPrg, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", dFrame.StartDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TransDate", dFrame.EndDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@CampGrpId", CampusGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@AwardTypeId", AidPrg, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim objSum As Object
                Try
                    objSum = ds.Tables(0).Compute("Sum(MonthTotal)", "")
                    If Not (objSum Is System.DBNull.Value) Then
                        SumTotal = objSum * (-1)
                    End If

                Catch ex As Exception
                    If ex.InnerException Is Nothing Then
                        Throw New Exception("Error building report dataset - " & ex.Message)
                    Else
                        Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
                    End If
                End Try
            End If
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return SumTotal
    End Function

    Public Function GetAidReceivedBySingleStudent(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStudentId As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "ORDER BY " & paramInfo.OrderBy
        End If

        'If StudentIdentifier = "SSN" Then
        '    strStudentId = "G.SSN AS StudentIdentifier "
        'ElseIf StudentIdentifier = "EnrollmentId" Then
        '    strStudentId = "F.EnrollmentId AS StudentIdentifier "
        'ElseIf StudentIdentifier = "StudentId" Then
        '    strStudentId = "G.StudentNumber AS StudentIdentifier "
        'End If
        With sb
            .Append("SELECT ")
            .Append("       SUM(SAS.Amount) AS AmountReceived,SA.StudentAwardId,SA.AwardTypeId,FS.FundSourceDescrip,")
            .Append("       (SA.GrossAmount - SA.LoanFees) AS NetLoanAmount,SA.AwardStartDate,SA.AwardEndDate,")
            .Append("       SA.AcademicYearId,AY.AcademicYearDescrip ")
            .Append("FROM   saTransactions T,arStuEnrollments,faStudentAwardSchedule SAS,saPmtDisbRel PD,faStudentAwards SA,saFundSources FS,saAcademicYears AY ")
            .Append("WHERE  T.StuEnrollId=arStuEnrollments.StuEnrollId AND T.TransactionId=PD.TransactionId AND T.IsPosted=1 ")
            .Append("       AND PD.AwardScheduleId=SAS.AwardScheduleId AND SAS.StudentAwardId=SA.StudentAwardId ")
            .Append("       AND SA.AwardTypeId=FS.FundSourceId AND SA.AcademicYearId=AY.AcademicYearId ")
            .Append("       AND T.Voided=0 ")
            .Append(strWhere)
            .Append("GROUP BY SA.StudentAwardId,SA.AwardTypeId,FS.FundSourceDescrip,SA.GrossAmount,SA.LoanFees,SA.AwardStartDate,SA.AwardEndDate,")
            .Append("       SA.AcademicYearId,AY.AcademicYearDescrip ")
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "AidReceivedSingleStudent"
            ds.Tables(0).Columns.Add(New DataColumn("DatePeriod", System.Type.GetType("System.String")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function


    Public Function GetStuBalanceProjectionNet(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStudentId As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        If StudentIdentifier = "SSN" Then
            strStudentId = "S.SSN"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStudentId = "arStuEnrollments.EnrollmentId"
        ElseIf StudentIdentifier = "StudentId" Then
            strStudentId = "S.StudentNumber"
        End If
        With sb
            '   query with nested queries
            .Append("SELECT ")
            .Append("       T.StuEnrollId,S.LastName,S.FirstName,S.MiddleName," & strStudentId & " AS StudentIdentifier, ")

            '' Code Added by Kamalesh Ahuja on 23 Jan 2010 to resolve Mantis Issue Id 17903 '''
            .Append("       arStuEnrollments.PrgVerId,arPrgVersions.PrgVerDescrip,isnull(SUM(T.TransAmount),0) AS Balance,")
            '.Append("       arStuEnrollments.PrgVerId,arPrgVersions.PrgVerDescrip,SUM(T.TransAmount) AS Balance,")

            '' Code Added by Kamalesh Ahuja on 23 Jan 2010 to resolve Mantis Issue Id 17903 '''
            .Append("       isnull((SELECT SUM(SAS.Amount) FROM faStudentAwards SA,faStudentAwardSchedule SAS ")
            ''.Append("       (SELECT SUM(SAS.Amount) FROM faStudentAwards SA,faStudentAwardSchedule SAS ")

            .Append("       WHERE SA.StudentAwardId=SAS.StudentAwardId AND SA.StuEnrollId=T.StuEnrollId ")

            '' Code Added by Kamalesh Ahuja on 23 Jan 2010 to resolve Mantis Issue Id 17903 '''
            .Append("             AND NOT EXISTS (SELECT * FROM saPmtDisbRel PD WHERE PD.AwardScheduleID=SAS.AwardScheduleID)),0) ")
            '.Append("             AND NOT EXISTS (SELECT * FROM saPmtDisbRel PD WHERE PD.AwardScheduleID=SAS.AwardScheduleID))")

            .Append("       AS ProjectedAwardDisb,")
            .Append(" (SELECT SUM(ISNULL(SAS.Amount-PD.Amount,0)) FROM ")
            .Append("  faStudentAwards SA,faStudentAwardSchedule SAS,(select AwardScheduleId,SUM(Amount) as Amount from saPmtDisbRel Group by  AwardScheduleId) PD  ")
            .Append("  WHERE SA.StudentAwardId = SAS.StudentAwardId And SA.StuEnrollId = T.StuEnrollId ")
            .Append(" AND PD.AwardScheduleID=SAS.AwardScheduleID and PD.Amount < SAS.Amount) ")
            .Append(" as ProjectedAwardDisbRemaining, ")
            .Append("       (SELECT SUM(PPS.Amount) FROM faStudentPaymentPlans PP, faStuPaymentPlanSchedule PPS ")
            .Append("       WHERE PP.PaymentPlanId=PPS.PaymentPlanId AND PP.StuEnrollId=T.StuEnrollId ")
            .Append("             AND NOT EXISTS (SELECT * FROM saPmtDisbRel PD WHERE PD.PayPlanScheduleID=PPS.PayPlanScheduleID))")
            .Append("       AS ProjectedPaymentDisb,")
            .Append("  (SELECT SUM(ISNULL(PPS.Amount-PD.Amount,0)) FROM ")
            .Append("  faStudentPaymentPlans PP, faStuPaymentPlanSchedule PPS,(select PayPlanScheduleID,SUM(Amount) as Amount from saPmtDisbRel Group by  PayPlanScheduleID) PD ")
            .Append("  WHERE PP.PaymentPlanId=PPS.PaymentPlanId AND PP.StuEnrollId=T.StuEnrollId   and ")
            .Append("  PD.PayPlanScheduleID=PPS.PayPlanScheduleID and PD.Amount < PPS.Amount) ")
            .Append("  AS ProjectedPaymentDisbRemaining, ")
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId,syCampuses.CampDescrip,arStuEnrollments.ExpGradDate,")
            .Append("       (SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip ")
            .Append("FROM   saTransactions T,arStuEnrollments,arStudent S,arPrgVersions,syCampGrps,syCmpGrpCmps,syCampuses ")
            .Append("WHERE  T.StuEnrollId=arStuEnrollments.StuEnrollId AND T.IsPosted=1 AND arStuEnrollments.StudentId=S.StudentId ")
            .Append("       AND arStuEnrollments.PrgVerId=arPrgVersions.PrgVerId AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=arStuEnrollments.CampusId ")
            .Append("       AND T.Voided=0 ")
            .Append(strWhere)
            .Append("GROUP BY syCampGrps.CampGrpDescrip,syCampGrps.CampGrpId,syCampuses.CampDescrip,arStuEnrollments.CampusId,T.StuEnrollId,")
            .Append("       S.LastName,S.FirstName,S.MiddleName," & strStudentId & ",arStuEnrollments.PrgVerId,arPrgVersions.PrgVerDescrip,arStuEnrollments.ExpGradDate,arStuEnrollments.StatusCodeId ")
            .Append("ORDER BY syCampGrps.CampGrpDescrip,syCampGrps.CampGrpId,syCampuses.CampDescrip,")
            .Append("       arStuEnrollments.CampusId,S.LastName,S.FirstName,S.MiddleName,StudentIdentifier,arPrgVersions.PrgVerDescrip,StatusCodeDescrip")
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "StuBalanceProjectedNet"

            '   Add columns to table StuBalanceProjectedNet.
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("StudentCount", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("Net", System.Type.GetType("System.Decimal")))

            '   Compute Net as Balance - ProjectedAwardDisb - ProjectedAwardDisb
            '   Delete all rows where (Balance - ProjectedAwardDisb - ProjectedAwardDisb) <= 0
            For Each dr As DataRow In ds.Tables(0).Rows
                If Not (dr("Balance") Is System.DBNull.Value) Then
                    '' Code Added by Kamalesh Ahuja on 23 Jan 2010 to resolve Mantis Issue Id 17903 '''
                    ''' If the user wants to See the records where projections exceed balance then
                    ''' dont delete rows with net less than 0
                    If paramInfo.ShowRptProjExceedBal = True Then
                        If (Decimal.Parse(dr("Balance")) >= 0) Or (Decimal.Parse(dr("ProjectedAwardDisb")) >= 0) Then
                            dr("Net") = Decimal.Parse(dr("Balance"))

                            If Not (dr("ProjectedAwardDisb") Is System.DBNull.Value) Then
                                dr("Net") -= Decimal.Parse(dr("ProjectedAwardDisb"))
                            End If
                            If Not (dr("ProjectedAwardDisb") Is System.DBNull.Value) And Not (dr("ProjectedAwardDisbRemaining") Is System.DBNull.Value) Then
                                dr("ProjectedAwardDisb") += Decimal.Parse(dr("ProjectedAwardDisbRemaining"))
                            End If

                            If Not (dr("ProjectedPaymentDisb") Is System.DBNull.Value) Then
                                dr("Net") -= Decimal.Parse(dr("ProjectedPaymentDisb"))
                            End If
                            If Not (dr("ProjectedPaymentDisb") Is System.DBNull.Value) And Not (dr("ProjectedPaymentDisbRemaining") Is System.DBNull.Value) Then
                                dr("ProjectedPaymentDisb") += Decimal.Parse(dr("ProjectedPaymentDisbRemaining"))
                            End If
                            If Decimal.Parse(dr("Net")) = 0 Then
                                ''If Decimal.Parse(dr("Net")) <= 0 Then
                                dr.Delete()
                            End If
                        Else
                            dr.Delete() '   Mark for delete.
                        End If
                    Else
                        If Decimal.Parse(dr("Balance")) >= 0 Then
                            dr("Net") = Decimal.Parse(dr("Balance"))

                            If Not (dr("ProjectedAwardDisb") Is System.DBNull.Value) Then
                                dr("Net") -= Decimal.Parse(dr("ProjectedAwardDisb"))
                            End If
                            If Not (dr("ProjectedAwardDisb") Is System.DBNull.Value) And Not (dr("ProjectedAwardDisbRemaining") Is System.DBNull.Value) Then
                                dr("ProjectedAwardDisb") += Decimal.Parse(dr("ProjectedAwardDisbRemaining"))
                            End If

                            If Not (dr("ProjectedPaymentDisb") Is System.DBNull.Value) Then
                                dr("Net") -= Decimal.Parse(dr("ProjectedPaymentDisb"))
                            End If
                            If Not (dr("ProjectedPaymentDisb") Is System.DBNull.Value) And Not (dr("ProjectedPaymentDisbRemaining") Is System.DBNull.Value) Then
                                dr("ProjectedPaymentDisb") += Decimal.Parse(dr("ProjectedPaymentDisbRemaining"))
                            End If
                            If Decimal.Parse(dr("Net")) <= 0 Then
                                dr.Delete()
                            End If
                        Else
                            dr.Delete() '   Mark for delete.
                        End If
                    End If
                Else
                    dr.Delete() '   Mark for delete.
                End If
            Next

            '   Commit changes (deletions).
            ds.Tables(0).AcceptChanges()

            Dim dt As New DataTable("CampusTotals")
            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CStuCount", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("CBalance", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CFAProj", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CPPProj", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CNet", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)

            dt = New DataTable("CampusGroupTotals")
            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CGStuCount", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("CGBalance", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CGFAProj", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CGPPProj", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New DataColumn("CGNet", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function


    Public Function GetListOfEndingLoans(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStudentId As String
        Dim arrDates() As String
        Dim startDate As String
        Dim endDate As String
        Dim dateFilter As String
        Dim temp As String
        Dim idx As Integer
        Dim idx2 As Integer
        Dim myCulture As New System.Globalization.CultureInfo("en-US", True)
        Dim dtEndDate As Date

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            temp = paramInfo.FilterOther.Trim
            'Parse FilterOther in order to get End Date and date filter criteria
            idx = temp.IndexOf("BETWEEN")
            If idx <> -1 Then
                idx2 = temp.IndexOf("AM")
                If idx2 <> -1 Then
                    '   endDate = startDate
                    endDate = temp.Substring(idx + 9, idx2 + 2 - (idx + 8))
                    Dim tempDate() As String = endDate.Split(" ")
                    If tempDate.GetLength(0) > 0 Then
                        endDate = tempDate(0)
                    End If
                    dtEndDate = Date.Parse(endDate, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
                    dateFilter = "<= '" & dtEndDate & "'"        'Convert.ToDateTime(endDate).ToShortDateString
                Else
                    '   endDate <> startDate
                    dateFilter = temp.Substring(idx)
                    arrDates = temp.Substring(idx + 8).Replace("'", "").Split(" ")
                    If arrDates.Length >= 3 Then
                        'startDate = arrDates(0).Substring(1, arrDates(0).Length - 2)
                        startDate = arrDates(0)
                        endDate = arrDates(2)
                        ''endDate = arrDates(2).Substring(1, arrDates(0).Length - 2)
                        'If arrDates(2).Length = 9 And arrDates(0).Length = 9 Then
                        '    endDate = arrDates(2).Substring(1, arrDates(0).Length - 2)
                        'ElseIf arrDates(2).Length = 9 And arrDates(0).Length > 9 Then
                        '    endDate = arrDates(2).Substring(1, arrDates(2).Length - 2)
                        'Else
                        '    endDate = arrDates(2).Substring(1, arrDates(0).Length - 1)
                        'End If
                    End If
                End If
                'endDate = Convert.ToDateTime(endDate).ToShortDateString
                dtEndDate = Date.Parse(endDate, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
                endDate = dtEndDate.ToShortDateString
            End If
            'strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        If StudentIdentifier = "SSN" Then
            strStudentId = "S.SSN"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStudentId = "E.EnrollmentId"
        ElseIf StudentIdentifier = "StudentId" Then
            strStudentId = "S.StudentNumber"
        End If
        With sb
            .Append("SELECT ")
            .Append("       saFundSources.FundSourceDescrip,SA.AwardStartDate,SA.AwardEndDate,SA.StuEnrollId,")
            .Append("       S.LastName,S.FirstName,S.MiddleName," & strStudentId & " AS StudentIdentifier,E.PrgVerId,arPrgVersions.PrgVerDescrip,")
            .Append("       syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,E.ExpGradDate ")
            .Append("FROM   faStudentAwards SA,arStuEnrollments E,arPrgVersions,arStudent S,saFundSources,syCampGrps,syCmpGrpCmps,syCampuses ")
            .Append("WHERE  SA.AwardEndDate " & dateFilter)
            .Append("       AND SA.StuEnrollId=E.StuEnrollId AND E.PrgVerId=arPrgVersions.PrgVerId ")
            .Append("       AND E.StudentId=S.StudentId AND SA.AwardTypeId=saFundSources.FundSourceId ")
            .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=E.CampusId ")
            .Append("       AND E.ExpGradDate > '" & endDate & "' ")
            .Append(strWhere)

            If strWhere.IndexOf("saFundSources") > -1 Then
                'Do not bring records from faStudentPaymentPlans if a Fund Source was used to filter by.
                .Append("ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,S.LastName,S.FirstName,S.MiddleName")
                .Append(strOrderBy)
            Else
                'Bring records from faStudentPaymentPlans if not Fund Source was used to filter by.
                .Append("UNION ALL ")
                .Append("SELECT ")
                .Append("       PP.PayPlanDescrip,PP.PayPlanStartDate,PP.PayPlanEndDate,PP.StuEnrollId,")
                .Append("       S.LastName,S.FirstName,S.MiddleName," & strStudentId & " AS StudentIdentifier,E.PrgVerId,arPrgVersions.PrgVerDescrip,")
                .Append("       syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,E.ExpGradDate ")
                .Append("FROM   faStudentPaymentPlans PP,arStuEnrollments E,arPrgVersions,arStudent S,syCampGrps,syCmpGrpCmps,syCampuses ")
                .Append("WHERE  PP.PayPlanEndDate " & dateFilter)
                .Append("       AND PP.StuEnrollId=E.StuEnrollId ")
                .Append("       AND E.PrgVerId=arPrgVersions.PrgVerId AND E.StudentId=S.StudentId ")
                .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
                .Append("       AND syCampuses.CampusId=E.CampusId ")
                .Append("       AND E.ExpGradDate > '" & endDate & "' ")
                .Append(strWhere)
                .Append("ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,S.LastName,S.FirstName,S.MiddleName")
                .Append(strOrderBy)
            End If
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "ListOfLoanEndings"               '"ListOfEndingLoans"
            '   Add columns to table ListOfEnding Loans.
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("StudentCount", System.Type.GetType("System.Int32")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function


    Public Function GetListOfPastDueDisb(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStudentId As String
        Dim strFundSourceId As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        If StudentIdentifier = "SSN" Then
            strStudentId = "S.SSN"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStudentId = "E.EnrollmentId"
        ElseIf StudentIdentifier = "StudentId" Then
            strStudentId = "S.StudentNumber"
        End If

        'If the where clause has fund sources then we need to get the guid for the fund source that is mapped to the
        'the Advantage system fund source of Student Payment Plan (Cash Payment)
        If strWhere.IndexOf("saFundSources") > -1 Then
            strFundSourceId = GetFundSourceIdForPaymentPlan()
        Else
            strFundSourceId = "Student Payment Plan Not Selected"
        End If


        With sb
            'Awards
            .Append("SELECT ")
            .Append("       saFundSources.FundSourceDescrip AS Source,SA.AwardStartDate AS StartDate,SA.AwardEndDate AS EndDate,faStudentAwardSchedule.ExpectedDate,faStudentAwardSchedule.Amount,")
            .Append("       SA.StuEnrollId,S.LastName,S.FirstName,S.MiddleName,S.SSN AS StudentIdentifier,E.PrgVerId,arPrgVersions.PrgVerDescrip,")
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,E.CampusId, ")
            .Append("       (select (case when (IsNull(arPrograms.ACId,0)=5) ")
            .Append("                   then   IsNull((select sum(Actual)/60  from atClsSectAttendance where stuenrollid=E.StuEnrollId and Actual not in (9999,999,99)),0) + ")
            .Append("                       	IsNull((select sum(ActualHours) from arStudentClockAttendance where stuenrollid=E.StuEnrollId and ActualHours not in (9999)),0) ")
            .Append("                   else null ")
            .Append("                   end)) as ActualHours, ")
            .Append("       (select (case when (IsNull(arPrograms.ACId,0)=5) ")
            .Append("                   then   IsNull((select sum(Scheduled)/60  from atClsSectAttendance where stuenrollid=E.StuEnrollId and Actual not in (9999,999,99)),0) + ")
            .Append("                       	IsNull((select sum(SchedHours) from arStudentClockAttendance where stuenrollid=E.StuEnrollId and ActualHours not in (9999)),0) ")
            .Append("                   else null ")
            .Append("                   end)) as ScheduledHours ")
            .Append("FROM   faStudentAwards SA,faStudentAwardSchedule,arStuEnrollments E,arStudent S,arPrgVersions,saFundSources,syCampGrps,syCmpGrpCmps,syCampuses,arPrograms ")
            .Append("WHERE  SA.StudentAwardId=faStudentAwardSchedule.StudentAwardId AND faStudentAwardSchedule.ExpectedDate <= '" & Date.Today & "' ")
            .Append("       AND NOT EXISTS (SELECT * FROM saPmtDisbRel pdr, saTransactions tr WHERE pdr.TransactionId = tr.TransactionId and tr.Voided = 0 and pdr.AwardScheduleId=faStudentAwardSchedule.AwardScheduleId) ")
            .Append("       AND SA.StuEnrollId=E.StuEnrollId AND E.StudentId=S.StudentId ")
            .Append("       AND E.PrgVerId=arPrgVersions.PrgVerId AND SA.AwardTypeId=saFundSources.FundSourceId ")
            .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=E.CampusId ")
            .Append("       AND arPrograms.ProgId=arPrgVersions.ProgId ")
            .Append(strWhere)

            'Include payment plans if no fund source is specified OR if the fund source(s) include the fund source mapped to 
            'the Advantage system fund source of Student Payment Plan
            If (strWhere.IndexOf("saFundSources") <= -1 Or strWhere.Contains(strFundSourceId)) Then
                .Append("UNION ALL ")
                .Append("SELECT ")
                .Append("       PP.PayPlanDescrip AS Source,PP.PayPlanStartDate AS StartDate,PP.PayPlanEndDate AS EndDate,faStuPaymentPlanSchedule.ExpectedDate,faStuPaymentPlanSchedule.Amount,")
                .Append("       PP.StuEnrollId,S.LastName,S.FirstName,S.MiddleName,S.SSN AS StudentIdentifier,E.PrgVerId,arPrgVersions.PrgVerDescrip,")
                .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,E.CampusId, ")
                .Append("       (select (case when (IsNull(arPrograms.ACId,0)=5) ")
                .Append("                   then   IsNull((select sum(Actual)/60  from atClsSectAttendance where stuenrollid=E.StuEnrollId and Actual not in (9999,999,99)),0) + ")
                .Append("                       	IsNull((select sum(ActualHours) from arStudentClockAttendance where stuenrollid=E.StuEnrollId and ActualHours not in (9999)),0) ")
                .Append("                   else null ")
                .Append("                   end)) as ActualHours, ")
                .Append("       (select (case when (IsNull(arPrograms.ACId,0)=5) ")
                .Append("                   then   IsNull((select sum(Scheduled)/60  from atClsSectAttendance where stuenrollid=E.StuEnrollId and Actual not in (9999,999,99)),0) + ")
                .Append("                       	IsNull((select sum(SchedHours) from arStudentClockAttendance where stuenrollid=E.StuEnrollId and ActualHours not in (9999)),0) ")
                .Append("                   else null ")
                .Append("                   end)) as ScheduledHours ")
                .Append("FROM   faStudentPaymentPlans PP,faStuPaymentPlanSchedule,arStuEnrollments E,arStudent S,arPrgVersions,syCampGrps,syCmpGrpCmps,syCampuses,arPrograms ")
                .Append("WHERE  PP.PaymentPlanId=faStuPaymentPlanSchedule.PaymentPlanId AND faStuPaymentPlanSchedule.ExpectedDate <= '" & Date.Today & "' ")
                .Append("       AND NOT EXISTS (SELECT * FROM saPmtDisbRel pdr, saTransactions tr WHERE pdr.TransactionId = tr.TransactionId and tr.Voided = 0 and pdr.PayPlanScheduleId=faStuPaymentPlanSchedule.PayPlanScheduleId) ")
                .Append("       AND PP.StuEnrollId=E.StuEnrollId AND E.StudentId=S.StudentId AND E.PrgVerId=arPrgVersions.PrgVerId ")
                .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
                .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=E.CampusId ")
                .Append("       AND arPrograms.ProgId=arPrgVersions.ProgId ")
                'We have to modify the reference to saFundSources in it
                .Append(GetModifiedStrWhereForPaymentPlans(strWhere))
            End If
            

            'Order by Clause
            .Append("ORDER BY syCampGrps.CampGrpDescrip,syCampGrps.CampGrpId,syCampuses.CampDescrip,E.CampusId,S.LastName,S.FirstName,S.MiddleName,")
            .Append("       StudentIdentifier,arPrgVersions.PrgVerDescrip,StartDate,EndDate,ExpectedDate")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "ListOfPastDueDisb"

            '   Add columns to table ListOfPastDueDisb.
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("StudentCount", System.Type.GetType("System.Int32")))

            Dim dt As New DataTable("CampusTotals")
            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CampusDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CStuCount", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("CTotalAmount", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)

            dt = New DataTable("CampusGroupTotals")
            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("CGStuCount", System.Type.GetType("System.Int32")))
            dt.Columns.Add(New DataColumn("CGTotalAmount", System.Type.GetType("System.Decimal")))
            ds.Tables.Add(dt)
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Function GetFundSourceIdForPaymentPlan() As String
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim FundSourceId As String

        With sb
            .Append("SELECT TOP 1 FundSourceId ")
            .Append("FROM dbo.saFundSources ")
            .Append("WHERE AdvFundSourceId=23 --Student Payment Plan (Cash Payment)")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        If IsDBNull(ds.Tables(0).Rows(0)(0)) Then
            Return ""
        Else
            Return ds.Tables(0).Rows(0)(0).ToString
        End If


    End Function

    Private Function GetModifiedStrWhereForPaymentPlans(strWhere As String) As String
        'We want to get rid of the reference to saFundSources
        Dim returnString As String
        Dim intStartPosition As Integer
        Dim intEndPosition As Integer
        Dim intNextAndPosition As Integer
        Dim strBeforeAndSaFundSources As String
        Dim strAfterSaFundSourcesClause As String

        'If the string does not contain saFundSources we can simply return it as it is
        If strWhere.IndexOf("saFundSources") <= -1 Then
            returnString = strWhere
        Else
            'Get rid of the reference to saFundSources
            'Find the start of the phrase "And saFundSources"
            intStartPosition = InStr(strWhere, "AND saFundSources")

            'Check if there is another AND after the AND saFundSources
            'Using an offset of 10 from the start of AND saFundSources should be enough.
            'It doesn't have to be precise.
            intNextAndPosition = InStr(intStartPosition + 10, strWhere, "AND")

            'If there is no AND after the start of AND saFundSources then we can simply take the string 
            'up to just before the start of it.
            If intNextAndPosition = 0 Then
                returnString = Mid(strWhere, 1, intStartPosition - 1)
            Else
                'There is an AND after the AND saFundSources so we want the string before the AND saFundSources
                'and the string from the next AND
                strBeforeAndSaFundSources = Mid(strWhere, 1, intStartPosition - 1)

                strAfterSaFundSourcesClause = Mid(strWhere, intNextAndPosition)

                returnString = strBeforeAndSaFundSources & strAfterSaFundSourcesClause
            End If

        End If




        Return returnString

    End Function

    Public Function GetListStuWithoutProjections(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStudentId As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If

        If StudentIdentifier = "SSN" Then
            strStudentId = "arStudent.SSN"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStudentId = "arStuEnrollments.EnrollmentId"
        ElseIf StudentIdentifier = "StudentId" Then
            strStudentId = "arStudent.StudentNumber"
        End If
        With sb
            '   query with subqueries
            .Append("SELECT ")
            .Append("       arStuEnrollments.StuEnrollId,arStuEnrollments.StudentId,arStuEnrollments.PrgVerId,")
            .Append("       arPrgVersions.PrgVerDescrip,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
            .Append("       arStudent.SSN AS StudentIdentifier,arStuEnrollments.ExpGradDate,")
            .Append("       syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,arStuEnrollments.StatusCodeId,")
            .Append("       (SELECT StatusCodeDescrip FROM syStatusCodes WHERE arStuEnrollments.StatusCodeId=StatusCodeId) AS StatusCodeDescrip ")
            .Append("FROM   arStuEnrollments,arStudent,arPrgVersions,syCampGrps,syCmpGrpCmps,syCampuses ")
            .Append("WHERE  arStuEnrollments.StudentId=arStudent.StudentId AND arStuEnrollments.PrgVerId=arPrgVersions.PrgVerId ")
            .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId AND syCmpGrpCmps.CampusId=syCampuses.CampusId ")
            .Append("       AND syCampuses.CampusId=arStuEnrollments.CampusId ")
            .Append("       AND (SELECT SUM(ProjectedAmount) AS TotalProjectedAmount ")
            .Append("               FROM	(SELECT COALESCE(SUM(Amount), 0) AS ProjectedAmount ")
            .Append("                           FROM faStudentAwards SA, faStudentAwardSchedule SAS ")
            .Append("                           WHERE SA.StudentAwardId=SAS.StudentAwardId AND SA.StuEnrollId=arStuEnrollments.StuEnrollId ")
            .Append("                           AND NOT EXISTS (SELECT * FROM saPmtDisbRel WHERE AwardScheduleId=SAS.AwardScheduleId) ")
            .Append("                        UNION ")
            .Append("                        SELECT COALESCE(SUM(Amount), 0) AS ProjectedAmount ")
            .Append("                           FROM faStudentPaymentPlans SPP, faStuPaymentPlanSchedule PPS ")
            .Append("                           WHERE SPP.PaymentPlanId = PPS.PaymentPlanId And SPP.StuEnrollId = arStuEnrollments.StuEnrollId ")
            .Append("                           AND NOT EXISTS (SELECT * FROM saPmtDisbRel WHERE PayPlanScheduleId=PPS.PayPlanScheduleId)) X) = 0 ")
            .Append(strWhere)
            .Append("ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip")
            .Append(strOrderBy)
            ''.Append("SELECT ")
            ''.Append("       arStuEnrollments.StuEnrollId,arStuEnrollments.StudentId,arStuEnrollments.PrgVerId,")
            ''.Append("       arPrgVersions.PrgVerDescrip,arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,")
            ''.Append("       " & strStudentId & " AS StudentIdentifier,arStuEnrollments.ExpGradDate,")
            ''.Append("       syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,arStuEnrollments.StatusCodeId,")
            ''.Append("       (SELECT StatusCodeDescrip FROM syStatusCodes WHERE arStuEnrollments.StatusCodeId=StatusCodeId) AS StatusCodeDescrip ")
            ''.Append("FROM   arStuEnrollments,arStudent,arPrgVersions,syCampGrps,syCmpGrpCmps,syCampuses ")
            ''.Append("WHERE  NOT EXISTS (SELECT * FROM faStudentAwards WHERE StuEnrollId=arStuEnrollments.StuEnrollId) ")
            ''.Append("       AND NOT EXISTS (SELECT * FROM faStudentPaymentPlans WHERE StuEnrollId=arStuEnrollments.StuEnrollId) ")
            ''.Append("       AND arStuEnrollments.StudentId=arStudent.StudentId AND arStuEnrollments.PrgVerId=arPrgVersions.PrgVerId ")
            ''.Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId ")
            ''.Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=arStuEnrollments.CampusId ")
            ''.Append(strWhere)
            ''.Append("ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip")
            ''.Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "ListStuWithoutProjections"

            '   Add columns to table StuBalanceProjectedNet.
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("StudentCount", System.Type.GetType("System.Int32")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    'Public Function GetRevenueRatio(ByVal paramInfo As ReportParamInfo) As DataSet
    '    Dim sb As New System.Text.StringBuilder
    '    Dim db As New DataAccess
    '    Dim ds As New DataSet
    '    Dim strWhere As String
    '    Dim strOrderBy As String
    '    Dim strStudentId As String
    '    Dim myCulture As New System.Globalization.CultureInfo("en-US", True)
    '    Dim fiscalStart As String = SingletonAppSettings.AppSettings("FiscalYearStart")
    '    Dim fiscalEnd As String = SingletonAppSettings.AppSettings("FiscalYearEnd")
    '    Dim dtStartDate As Date
    '    Dim dtEndDate As Date
    '    Dim strYear As String = "/"
    '    Dim yrCopy As String

    '    If paramInfo.FilterList <> "" Then
    '        strWhere &= " AND " & paramInfo.FilterList
    '    End If

    '    If paramInfo.FilterOther <> "" Then
    '        'strWhere &= " AND " & paramInfo.FilterOther
    '        Dim idx As Integer = paramInfo.FilterOther.IndexOf("BETWEEN")
    '        If idx > -1 Then
    '            Dim str As String = paramInfo.FilterOther.Substring(idx + 9)
    '            Dim lidx As Integer = Str.IndexOf("'")
    '            dtStartDate = Date.Parse(str.Substring(0, lidx), myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
    '            idx = str.IndexOf("AND") + 5
    '            lidx = str.LastIndexOf("'")
    '            dtEndDate = Date.Parse(str.Substring(idx, lidx - idx), myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
    '            'Else

    '            '    yrCopy = paramInfo.FilterOther.Substring(7)
    '            '    strYear &= paramInfo.FilterOther.Substring(7)
    '        Else
    '            yrCopy = Today.Year
    '            strYear &= Today.Year
    '            dtStartDate = Date.Parse(fiscalStart & strYear, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
    '            dtEndDate = Date.Parse(fiscalEnd & strYear, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
    '        End If
    '    End If


    '    If Date.Compare(dtEndDate, dtStartDate) < 0 Then
    '        strYear = "/" & (Integer.Parse(yrCopy) + 1)
    '        dtEndDate = Date.Parse(fiscalEnd & strYear, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
    '    End If
    '    dtEndDate = dtEndDate.AddDays(1)


    '    'If paramInfo.OrderBy <> "" Then
    '    '    strOrderBy &= "," & paramInfo.OrderBy
    '    'End If

    '    If StudentIdentifier = "StudentId" Then
    '        strStudentId = "S.StudentNumber"
    '    ElseIf StudentIdentifier = "EnrollmentId" Then
    '        strStudentId = "SE.EnrollmentId"
    '    Else
    '        strStudentId = "S.SSN"
    '    End If

    '    With sb
    '        '.Append("--this is the amount of payments received from TitleIV ")
    '        .Append("select  ")
    '        .Append("		SA.AwardTypeId, ")
    '        .Append("		FS.FundSourceDescrip, ")
    '        .Append("		FS.TitleIV, ")
    '        .Append("		T1.StuEnrollId, ")
    '        .Append("		S.LastName, ")
    '        .Append("		S.FirstName, ")
    '        .Append("		S.MiddleName, ")
    '        .Append("   " & strStudentId & " AS StudentIdentifier, ")
    '        .Append("		(SELECT LenderDescrip FROM faLenders WHERE LenderId=SA.LenderId) AS LenderDescrip, ")
    '        .Append("		CGC.CampGrpId, ")
    '        .Append("		syCampGrps.CampGrpDescrip, ")
    '        .Append("		SE.CampusId, ")
    '        .Append("		C.CampDescrip, ")
    '        .Append("       (case when PDR.Amount > -T1.TransAmount then -T1.TransAmount else PDR.Amount end) as Received, ")
    '        .Append("       coalesce((select sum(TransAmount) from saTransactions T, saRefunds R ")
    '        .Append("            where T.TransactionId=R.TransactionId and R.RefundTypeId=1 and R.StudentAwardId=SA.StudentAwardId and T.TransDate >='" & dtStartDate.ToShortDateString & "' and T.TransDate <='" & dtEndDate.ToShortDateString & "'), 0.00) ")
    '        .Append("            as Refunded, ")
    '        .Append("		(select sum(TB.TransAmount) from saTransactions TA, saAppliedPayments AP1, saTransactions TB, saTransCodes TC1 ")
    '        .Append("				where	TA.TransactionId=AP1.TransactionId ")
    '        .Append("				and		AP1.ApplyToTransId=TB.TransactionId ")
    '        .Append("				and		TB.Voided=0 ")
    '        .Append("				and		TB.TransCodeId=TC1.TransCodeId ")
    '        .Append("				and		TC1.IsInstCharge=1 ")
    '        .Append("				and		TA.TransactionId=T1.TransactionId) As InstCharges, ")
    '        .Append("		0 as PmtPlanCount ")
    '        .Append("from	saTransactions T1, saPayments P, saPmtDisbRel PDR, faStudentAwardSchedule SAS, faStudentAwards SA, saFundSources FS, arStuEnrollments SE, arStudent S, syCmpGrpCmps CGC, syCampGrps, syCampuses C ")
    '        .Append("where	T1.voided=0 ")
    '        .Append("and		T1.TransactionId=P.TransactionId ")
    '        .Append("and		T1.TransactionId=PDR.TransactionId ")
    '        .Append("and		PDR.AwardScheduleId=SAS.AwardScheduleId ")
    '        .Append("and		SAS.StudentAwardId=SA.StudentAwardId ")
    '        .Append("and		SA.AwardTypeId=FS.FundSourceId ")
    '        .Append("and		FS.TitleIV=1 ")
    '        .Append("and		T1.StuEnrollId=SE.StuEnrollId ")
    '        .Append("and		SE.StudentId=S.StudentId ")
    '        .Append("and		CGC.CampusId=SE.CampusId ")
    '        .Append("and		syCampGrps.CampGrpId=CGC.CampGrpId ")
    '        .Append("and		C.CampusId=SE.CampusId ")
    '        .Append("and		T1.Voided=0  ")
    '        .Append(strWhere)
    '        .Append("and        T1.TransDate>='" & dtStartDate.ToShortDateString & "'")
    '        .Append("and        T1.TransDate<='" & dtEndDate.ToShortDateString & "' ")
    '        .Append("union ")
    '        '.Append("--this is the amount of payments received from non TitleIV sources ")
    '        .Append("select  ")
    '        .Append("		coalesce((select top 1 SA.AwardTypeId from saPmtDisbRel PDR, faStudentAwardSchedule SAS, faStudentAwards SA, saFundSources FS ")
    '        .Append("							where	T1.TransactionId=PDR.TransactionId ")
    '        .Append("							and		PDR.AwardScheduleId=SAS.AwardScheduleId ")
    '        .Append("							and		SAS.StudentAwardId=SA.StudentAwardId ")
    '        .Append("							and		SA.AwardTypeId=FS.FundSourceId ")
    '        .Append("							and		FS.TitleIV=0), '00000000-0000-0000-0000-000000000000') as AwardTypeId, ")
    '        .Append("		coalesce((select top 1 FS.FundSourceDescrip from saPmtDisbRel PDR, faStudentAwardSchedule SAS, faStudentAwards SA, saFundSources FS ")
    '        .Append("							where	T1.TransactionId=PDR.TransactionId ")
    '        .Append("							and		PDR.AwardScheduleId=SAS.AwardScheduleId ")
    '        .Append("							and		SAS.StudentAwardId=SA.StudentAwardId ")
    '        .Append("							and		SA.AwardTypeId=FS.FundSourceId ")
    '        .Append("							and		FS.TitleIV=0), 'Fund Source: Cash') as FundSourceDescrip, ")
    '        .Append("		0 as TitleIV, ")
    '        .Append("		T1.StuEnrollId, ")
    '        .Append("		S.LastName, ")
    '        .Append("		S.FirstName, ")
    '        .Append("		S.MiddleName, ")
    '        .Append("   " & strStudentId & " AS StudentIdentifier, ")
    '        .Append("		(select LenderDescrip from faLenders where LenderId=(Select top 1 LenderId from saPmtDisbRel PDR, faStudentAwardSchedule SAS, faStudentAwards SA ")
    '        .Append("							where	T1.TransactionId=PDR.TransactionId ")
    '        .Append("							and		PDR.AwardScheduleId=SAS.AwardScheduleId ")
    '        .Append("							and		SAS.StudentAwardId=SA.StudentAwardId)) as LenderDescrip, ")
    '        .Append("		CGC.CampGrpId, ")
    '        .Append("		syCampGrps.CampGrpDescrip, ")
    '        .Append("		SE.CampusId, ")
    '        .Append("		C.CampDescrip, ")
    '        .Append("       (case when AP.Amount > T2.TransAmount then T2.TransAmount else AP.Amount end) as Received, ")
    '        .Append("       0.00 as Refunded, ")
    '        .Append("       (case when AP.Amount > T2.TransAmount then T2.TransAmount else AP.Amount end) as InstCharges, ")
    '        .Append("		0 as PmtPlanCount ")
    '        .Append("from	saTransactions T1, saAppliedPayments AP, saTransactions T2, saTransCodes TC, arStuEnrollments SE, arStudent S, syCmpGrpCmps CGC, syCampGrps, syCampuses C ")
    '        .Append("where	T1.voided=0 ")
    '        .Append("and		T1.TransactionId=AP.TransactionId ")
    '        .Append("and		AP.ApplyToTransId=T2.TransactionId ")
    '        .Append("and		T2.TransCodeId=TC.TransCodeId ")
    '        .Append("and		TC.IsInstCharge=1 ")
    '        .Append("and		not exists ( ")
    '        .Append("					select * from saPmtDisbRel PDR, faStudentAwardSchedule SAS, faStudentAwards SA, saFundSources FS ")
    '        .Append("					where	T1.TransactionId=PDR.TransactionId ")
    '        .Append("					and		PDR.AwardScheduleId=SAS.AwardScheduleId ")
    '        .Append("					and		SAS.StudentAwardId=SA.StudentAwardId ")
    '        .Append("					and		SA.AwardTypeId=FS.FundSourceId ")
    '        .Append("					and		FS.TitleIV=1 ")
    '        .Append("					) ")
    '        .Append("and		T1.StuEnrollId=SE.StuEnrollId ")
    '        .Append("and		SE.StudentId=S.StudentId ")
    '        .Append("and		CGC.CampusId=SE.CampusId ")
    '        .Append("and		syCampGrps.CampGrpId=CGC.CampGrpId ")
    '        .Append("and		C.CampusId=SE.CampusId ")
    '        .Append(strWhere)
    '        .Append("and        T1.TransDate>='" & dtStartDate.ToShortDateString & "'")
    '        .Append("and        T1.TransDate<='" & dtEndDate.ToShortDateString & "' ")
    '        .Append("union ")
    '        .Append("select ")
    '        .Append("		'00000000-0000-0000-0000-000000000000' as AwardTypeId, ")
    '        .Append(" 		'Fund Source: Cash'	as FundSourceDescrip, ")
    '        .Append(" 		0 as TitleIV, ")
    '        .Append(" 		T1.StuEnrollId, ")
    '        .Append(" 		S.LastName, ")
    '        .Append(" 		S.FirstName, ")
    '        .Append(" 		S.MiddleName, ")
    '        .Append("   " & strStudentId & " AS StudentIdentifier, ")
    '        .Append("		'' as LenderDescrip, ")
    '        .Append(" 		CGC.CampGrpId, ")
    '        .Append(" 		syCampGrps.CampGrpDescrip, ")
    '        .Append(" 		SE.CampusId, ")
    '        .Append(" 		C.CampDescrip, ")
    '        .Append(" 		0.00 as Received, ")
    '        .Append("       T1.TransAmount as Refunded, ")
    '        .Append(" 		0.00 as InstCharges, ")
    '        .Append(" 		0 as PmtPlanCount  ")
    '        .Append("from	saTransactions T1, saRefunds R, arStuEnrollments SE, arStudent S, syCmpGrpCmps CGC, syCampGrps, syCampuses C  ")
    '        .Append("where	T1.voided=0  ")
    '        .Append("and		T1.TransactionId=R.TransactionId  ")
    '        .Append("and		T1.StuEnrollId=SE.StuEnrollId  ")
    '        .Append("and		R.RefundTypeId=0 ")
    '        .Append("and		R.StudentAwardId is null ")
    '        .Append("and        R.IsInstCharge = 1 ")
    '        .Append("and		SE.StudentId=S.StudentId  ")
    '        .Append("and		CGC.CampusId=SE.CampusId  ")
    '        .Append("and		syCampGrps.CampGrpId=CGC.CampGrpId  ")
    '        .Append("and		C.CampusId=SE.CampusId   ")
    '        .Append(strWhere)
    '        .Append("and        T1.TransDate>='" & dtStartDate.ToShortDateString & "'")
    '        .Append("and        T1.TransDate<='" & dtEndDate.ToShortDateString & "' ")
    '        .Append("ORDER BY syCampGrps.CampGrpDescrip, C.CampDescrip, ")
    '        .Append("       FundSourceDescrip, S.LastName, S.FirstName, S.MiddleName ")
    '    End With

    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '    db.OpenConnection()

    '    db.CommandTimeout = 60
    '    ds = db.RunParamSQLDataSet(sb.ToString)

    '    If ds.Tables.Count > 0 Then
    '        ds.Tables(0).TableName = "RevenueRatioDetail"
    '        'Add new columns.
    '        ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
    '        ds.Tables(0).Columns.Add(New DataColumn("NetReceived", System.Type.GetType("System.Decimal")))
    '        ds.Tables(0).Columns.Add(New DataColumn("Stipends", System.Type.GetType("System.Decimal")))
    '        ds.Tables(0).Columns.Add(New DataColumn("NetReceivedMinusStipends", System.Type.GetType("System.Decimal")))
    '        '
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            'Create table to store Totals per Campus Groups.
    '            Dim dt As New DataTable("CampGrpTotals")
    '            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
    '            dt.Columns.Add(New DataColumn("Received", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("Refunded", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("NetReceived", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("Stipends", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("NetReceivedMinusStipends", System.Type.GetType("System.Decimal")))
    '            ds.Tables.Add(dt)   'Add CampGrpTotals table to ds dataset.

    '            'Create table to store Totals per Campus Groups and Campuses.
    '            dt = New DataTable("CampusTotals")
    '            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
    '            dt.Columns.Add(New DataColumn("CampDescrip", System.Type.GetType("System.String")))
    '            dt.Columns.Add(New DataColumn("Received", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("Refunded", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("NetReceived", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("Stipends", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("NetReceivedMinusStipends", System.Type.GetType("System.Decimal")))
    '            ds.Tables.Add(dt)   'Add CampusTotals table to ds dataset.

    '            'Create table to store Totals per Campus Groups,Campuses and Fund Sources.
    '            dt = New DataTable("FSTotals")
    '            dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
    '            dt.Columns.Add(New DataColumn("CampDescrip", System.Type.GetType("System.String")))
    '            dt.Columns.Add(New DataColumn("FundSourceDescrip", System.Type.GetType("System.String")))
    '            dt.Columns.Add(New DataColumn("Received", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("Refunded", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("NetReceived", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("Stipends", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("NetReceivedMinusStipends", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("TitleIV", System.Type.GetType("System.Boolean")))
    '            ds.Tables.Add(dt)   'Add FSTotals table to ds dataset.

    '            'Create table to store TitleIV Totals.
    '            dt = New DataTable("TitleIVTotals")
    '            dt.Columns.Add(New DataColumn("Received", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("Refunded", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("NetReceived", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("Stipends", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("NetReceivedMinusStipends", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("RevenuePercentage", System.Type.GetType("System.Decimal")))
    '            dt.Columns.Add(New DataColumn("Ratio", System.Type.GetType("System.String")))
    '            ds.Tables.Add(dt)   'Add CampusTotals table to ds dataset.
    '        End If
    '    End If

    '    Return ds
    'End Function
    Public Function GetRevenueRatio(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        ''Dim db As New DataAccess
        Dim db As New SQLDataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStudentId As String
        Dim myCulture As New System.Globalization.CultureInfo("en-US", True)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim fiscalStart As String = MyAdvAppSettings.AppSettings("FiscalYearStart")
        Dim fiscalEnd As String = MyAdvAppSettings.AppSettings("FiscalYearEnd")
        Dim dtStartDate As Date
        Dim dtEndDate As Date
        Dim strYear As String = "/"
        Dim yrCopy As String

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            'strWhere &= " AND " & paramInfo.FilterOther
            Dim idx As Integer = paramInfo.FilterOther.IndexOf("BETWEEN")
            If idx > -1 Then
                Dim str As String = paramInfo.FilterOther.Substring(idx + 9)
                Dim lidx As Integer = str.IndexOf("'")
                dtStartDate = Date.Parse(str.Substring(0, lidx), myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
                idx = str.IndexOf("AND") + 5
                lidx = str.LastIndexOf("'")
                dtEndDate = Date.Parse(str.Substring(idx, lidx - idx), myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
                'Else

                '    yrCopy = paramInfo.FilterOther.Substring(7)
                '    strYear &= paramInfo.FilterOther.Substring(7)
            Else
                yrCopy = Today.Year
                strYear &= Today.Year
                dtStartDate = Date.Parse(fiscalStart & strYear, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
                dtEndDate = Date.Parse(fiscalEnd & strYear, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
            End If
        End If


        If Date.Compare(dtEndDate, dtStartDate) < 0 Then
            strYear = "/" & (Integer.Parse(yrCopy) + 1)
            dtEndDate = Date.Parse(fiscalEnd & strYear, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
        End If
        dtEndDate = dtEndDate.AddDays(1)


        'If paramInfo.OrderBy <> "" Then
        '    strOrderBy &= "," & paramInfo.OrderBy
        'End If

        If StudentIdentifier = "StudentId" Then
            strStudentId = "S.StudentNumber"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStudentId = "SE.EnrollmentId"
        Else
            strStudentId = "S.SSN"
        End If

        'With sb
        '    '.Append("--this is the amount of payments received from TitleIV ")
        '    .Append("select  ")
        '    .Append("		SA.AwardTypeId, ")
        '    .Append("		FS.FundSourceDescrip, ")
        '    .Append("		FS.TitleIV, ")
        '    .Append("		T1.StuEnrollId, ")
        '    .Append("		S.LastName, ")
        '    .Append("		S.FirstName, ")
        '    .Append("		S.MiddleName, ")
        '    .Append("   " & strStudentId & " AS StudentIdentifier, ")
        '    .Append("		(SELECT LenderDescrip FROM faLenders WHERE LenderId=SA.LenderId) AS LenderDescrip, ")
        '    .Append("		CGC.CampGrpId, ")
        '    .Append("		syCampGrps.CampGrpDescrip, ")
        '    .Append("		SE.CampusId, ")
        '    .Append("		C.CampDescrip, ")
        '    .Append("       (case when PDR.Amount > -T1.TransAmount then -T1.TransAmount else PDR.Amount end) as Received, ")
        '    .Append("       coalesce((select sum(TransAmount) from saTransactions T, saRefunds R ")
        '    .Append("            where T.TransactionId=R.TransactionId and R.RefundTypeId=1 and R.StudentAwardId=SA.StudentAwardId and T.TransDate >='" & dtStartDate.ToShortDateString & "' and T.TransDate <='" & dtEndDate.ToShortDateString & "'), 0.00) ")
        '    .Append("            as Refunded, ")
        '    .Append("		(select sum(TB.TransAmount) from saTransactions TA, saAppliedPayments AP1, saTransactions TB, saTransCodes TC1 ")
        '    .Append("				where	TA.TransactionId=AP1.TransactionId ")
        '    .Append("				and		AP1.ApplyToTransId=TB.TransactionId ")
        '    .Append("				and		TB.Voided=0 ")
        '    .Append("				and		TB.TransCodeId=TC1.TransCodeId ")
        '    .Append("				and		TC1.IsInstCharge=1 ")
        '    .Append("				and		TA.TransactionId=T1.TransactionId) As InstCharges, ")
        '    .Append("		0 as PmtPlanCount ")
        '    .Append("from	saTransactions T1, saPayments P, saPmtDisbRel PDR, faStudentAwardSchedule SAS, faStudentAwards SA, saFundSources FS, arStuEnrollments SE, arStudent S, syCmpGrpCmps CGC, syCampGrps, syCampuses C ")
        '    .Append("where	T1.voided=0 ")
        '    .Append("and		T1.TransactionId=P.TransactionId ")
        '    .Append("and		T1.TransactionId=PDR.TransactionId ")
        '    .Append("and		PDR.AwardScheduleId=SAS.AwardScheduleId ")
        '    .Append("and		SAS.StudentAwardId=SA.StudentAwardId ")
        '    .Append("and		SA.AwardTypeId=FS.FundSourceId ")
        '    .Append("and		FS.TitleIV=1 ")
        '    .Append("and		T1.StuEnrollId=SE.StuEnrollId ")
        '    .Append("and		SE.StudentId=S.StudentId ")
        '    .Append("and		CGC.CampusId=SE.CampusId ")
        '    .Append("and		syCampGrps.CampGrpId=CGC.CampGrpId ")
        '    .Append("and		C.CampusId=SE.CampusId ")
        '    .Append("and		T1.Voided=0  ")
        '    .Append(strWhere)
        '    .Append("and        T1.TransDate>='" & dtStartDate.ToShortDateString & "'")
        '    .Append("and        T1.TransDate<='" & dtEndDate.ToShortDateString & "' ")
        '    .Append("union ")
        '    '.Append("--this is the amount of payments received from non TitleIV sources ")
        '    .Append("select  ")
        '    .Append("		coalesce((select top 1 SA.AwardTypeId from saPmtDisbRel PDR, faStudentAwardSchedule SAS, faStudentAwards SA, saFundSources FS ")
        '    .Append("							where	T1.TransactionId=PDR.TransactionId ")
        '    .Append("							and		PDR.AwardScheduleId=SAS.AwardScheduleId ")
        '    .Append("							and		SAS.StudentAwardId=SA.StudentAwardId ")
        '    .Append("							and		SA.AwardTypeId=FS.FundSourceId ")
        '    .Append("							and		FS.TitleIV=0), '00000000-0000-0000-0000-000000000000') as AwardTypeId, ")
        '    .Append("		coalesce((select top 1 FS.FundSourceDescrip from saPmtDisbRel PDR, faStudentAwardSchedule SAS, faStudentAwards SA, saFundSources FS ")
        '    .Append("							where	T1.TransactionId=PDR.TransactionId ")
        '    .Append("							and		PDR.AwardScheduleId=SAS.AwardScheduleId ")
        '    .Append("							and		SAS.StudentAwardId=SA.StudentAwardId ")
        '    .Append("							and		SA.AwardTypeId=FS.FundSourceId ")
        '    .Append("							and		FS.TitleIV=0), 'Fund Source: Cash') as FundSourceDescrip, ")
        '    .Append("		0 as TitleIV, ")
        '    .Append("		T1.StuEnrollId, ")
        '    .Append("		S.LastName, ")
        '    .Append("		S.FirstName, ")
        '    .Append("		S.MiddleName, ")
        '    .Append("   " & strStudentId & " AS StudentIdentifier, ")
        '    .Append("		(select LenderDescrip from faLenders where LenderId=(Select top 1 LenderId from saPmtDisbRel PDR, faStudentAwardSchedule SAS, faStudentAwards SA ")
        '    .Append("							where	T1.TransactionId=PDR.TransactionId ")
        '    .Append("							and		PDR.AwardScheduleId=SAS.AwardScheduleId ")
        '    .Append("							and		SAS.StudentAwardId=SA.StudentAwardId)) as LenderDescrip, ")
        '    .Append("		CGC.CampGrpId, ")
        '    .Append("		syCampGrps.CampGrpDescrip, ")
        '    .Append("		SE.CampusId, ")
        '    .Append("		C.CampDescrip, ")
        '    .Append("       (case when AP.Amount > T2.TransAmount then T2.TransAmount else AP.Amount end) as Received, ")
        '    .Append("       0.00 as Refunded, ")
        '    .Append("       (case when AP.Amount > T2.TransAmount then T2.TransAmount else AP.Amount end) as InstCharges, ")
        '    .Append("		0 as PmtPlanCount ")
        '    .Append("from	saTransactions T1, saAppliedPayments AP, saTransactions T2, saTransCodes TC, arStuEnrollments SE, arStudent S, syCmpGrpCmps CGC, syCampGrps, syCampuses C ")
        '    .Append("where	T1.voided=0 ")
        '    .Append("and		T1.TransactionId=AP.TransactionId ")
        '    .Append("and		AP.ApplyToTransId=T2.TransactionId ")
        '    .Append("and		T2.TransCodeId=TC.TransCodeId ")
        '    .Append("and		TC.IsInstCharge=1 ")
        '    .Append("and		not exists ( ")
        '    .Append("					select * from saPmtDisbRel PDR, faStudentAwardSchedule SAS, faStudentAwards SA, saFundSources FS ")
        '    .Append("					where	T1.TransactionId=PDR.TransactionId ")
        '    .Append("					and		PDR.AwardScheduleId=SAS.AwardScheduleId ")
        '    .Append("					and		SAS.StudentAwardId=SA.StudentAwardId ")
        '    .Append("					and		SA.AwardTypeId=FS.FundSourceId ")
        '    .Append("					and		FS.TitleIV=1 ")
        '    .Append("					) ")
        '    .Append("and		T1.StuEnrollId=SE.StuEnrollId ")
        '    .Append("and		SE.StudentId=S.StudentId ")
        '    .Append("and		CGC.CampusId=SE.CampusId ")
        '    .Append("and		syCampGrps.CampGrpId=CGC.CampGrpId ")
        '    .Append("and		C.CampusId=SE.CampusId ")
        '    .Append(strWhere)
        '    .Append("and        T1.TransDate>='" & dtStartDate.ToShortDateString & "'")
        '    .Append("and        T1.TransDate<='" & dtEndDate.ToShortDateString & "' ")
        '    .Append("union ")
        '    .Append("select ")
        '    .Append("		'00000000-0000-0000-0000-000000000000' as AwardTypeId, ")
        '    .Append(" 		'Fund Source: Cash'	as FundSourceDescrip, ")
        '    .Append(" 		0 as TitleIV, ")
        '    .Append(" 		T1.StuEnrollId, ")
        '    .Append(" 		S.LastName, ")
        '    .Append(" 		S.FirstName, ")
        '    .Append(" 		S.MiddleName, ")
        '    .Append("   " & strStudentId & " AS StudentIdentifier, ")
        '    .Append("		'' as LenderDescrip, ")
        '    .Append(" 		CGC.CampGrpId, ")
        '    .Append(" 		syCampGrps.CampGrpDescrip, ")
        '    .Append(" 		SE.CampusId, ")
        '    .Append(" 		C.CampDescrip, ")
        '    .Append(" 		0.00 as Received, ")
        '    .Append("       T1.TransAmount as Refunded, ")
        '    .Append(" 		0.00 as InstCharges, ")
        '    .Append(" 		0 as PmtPlanCount  ")
        '    .Append("from	saTransactions T1, saRefunds R, arStuEnrollments SE, arStudent S, syCmpGrpCmps CGC, syCampGrps, syCampuses C  ")
        '    .Append("where	T1.voided=0  ")
        '    .Append("and		T1.TransactionId=R.TransactionId  ")
        '    .Append("and		T1.StuEnrollId=SE.StuEnrollId  ")
        '    .Append("and		R.RefundTypeId=0 ")
        '    .Append("and		R.StudentAwardId is null ")
        '    .Append("and        R.IsInstCharge = 1 ")
        '    .Append("and		SE.StudentId=S.StudentId  ")
        '    .Append("and		CGC.CampusId=SE.CampusId  ")
        '    .Append("and		syCampGrps.CampGrpId=CGC.CampGrpId  ")
        '    .Append("and		C.CampusId=SE.CampusId   ")
        '    .Append(strWhere)
        '    .Append("and        T1.TransDate>='" & dtStartDate.ToShortDateString & "'")
        '    .Append("and        T1.TransDate<='" & dtEndDate.ToShortDateString & "' ")
        '    .Append("ORDER BY syCampGrps.CampGrpDescrip, C.CampDescrip, ")
        '    .Append("       FundSourceDescrip, S.LastName, S.FirstName, S.MiddleName ")
        'End With

        'With sb
        '    '.Append("--this is the amount of payments received from TitleIV ")
        '    .Append(" select tt.*  ")
        '    .Append("		,coalesce((select sum(TransAmount) from saTransactions T, saRefunds R  where T.TransactionId=R.TransactionId ")
        '    .Append("		and R.RefundTypeId=1 and R.StudentAwardId=tt.StudentAwardId and T.TransDate >='" & dtStartDate.ToShortDateString & "' and T.TransDate <='" & dtEndDate.ToShortDateString & "'), 0.00) as Refunded ")
        '    .Append("		from (select  AwardTypeId,StudentAwardId, FundSourceDescrip, TitleIV, StuEnrollId,LastName,FirstName, MiddleName, ")
        '    .Append("		StudentIdentifier,LenderDescrip,CampGrpId,CampGrpDescrip, CampusId,CampDescrip,sum(Received) as Received, ")
        '    .Append("		InstCharges,PmtPlanCount from ( ")
        '    .Append("		select SA.AwardTypeId,SA.StudentAwardId, FS.FundSourceDescrip, FS.TitleIV, T1.StuEnrollId, S.LastName, S.FirstName, S.MiddleName, ")
        '    .Append("		" & strStudentId & " AS StudentIdentifier, (SELECT LenderDescrip FROM faLenders WHERE LenderId=SA.LenderId) AS LenderDescrip, ")
        '    .Append("       CGC.CampGrpId, syCampGrps.CampGrpDescrip, SE.CampusId, C.CampDescrip,  ")
        '    .Append("		(case when PDR.Amount > -T1.TransAmount then -T1.TransAmount else PDR.Amount end) as Received, ")
        '    .Append("		0.00 as InstCharges, 0 as PmtPlanCount ")
        '    .Append("		from saTransactions T1, saPayments P, saPmtDisbRel PDR, faStudentAwardSchedule SAS, ")
        '    .Append("		faStudentAwards SA, saFundSources FS, arStuEnrollments SE, arStudent S, syCmpGrpCmps CGC, syCampGrps, syCampuses C ")
        '    .Append("		where	T1.voided=0 and	 T1.TransactionId=P.TransactionId and T1.TransactionId=PDR.TransactionId and PDR.AwardScheduleId=SAS.AwardScheduleId ")
        '    .Append("       and SAS.StudentAwardId=SA.StudentAwardId and SA.AwardTypeId=FS.FundSourceId and	FS.TitleIV=1 and T1.StuEnrollId=SE.StuEnrollId  ")
        '    .Append("       and SE.StudentId=S.StudentId and CGC.CampusId=SE.CampusId and syCampGrps.CampGrpId=CGC.CampGrpId and C.CampusId=SE.CampusId and T1.Voided=0 ")
        '    .Append("       and ((T1.TransCodeId in (select TransCodeId from saTransCodes TC where TC.TransCodeId=T1.TransCodeId and TC.IsInstCharge=1 ))or (T1.TransCodeId in (select T2.TransCodeId from saTransCodes TC,saTransactions T2,saAppliedPayments AP1 where AP1.TransactionId=T1.TransactionId and AP1.AppliedPmtId=T2.TransactionId and TC.TransCodeId=T2.TransCodeId and TC.IsInstCharge=1 )))  ")
        '    .Append(strWhere)
        '    .Append("       and  T1.TransDate >='" & dtStartDate.ToShortDateString & "' and T1.TransDate <='" & dtEndDate.ToShortDateString & "' ) t ")
        '    .Append("		group by AwardTypeId,StudentAwardId, FundSourceDescrip, TitleIV, StuEnrollId,LastName,FirstName, MiddleName, ")
        '    .Append("		StudentIdentifier,LenderDescrip,CampGrpId,CampGrpDescrip, CampusId,CampDescrip, InstCharges,PmtPlanCount) tt ")
        '    .Append("union ")
        '    '.Append("--this is the amount of payments received from non TitleIV sources ")

        '    .Append(" select tt.*  ")
        '    .Append("		,coalesce((select sum(TransAmount) from saTransactions T, saRefunds R  where T.TransactionId=R.TransactionId ")
        '    .Append("		and R.RefundTypeId=1 and R.StudentAwardId=tt.StudentAwardId and T.TransDate >='" & dtStartDate.ToShortDateString & "' and T.TransDate <='" & dtEndDate.ToShortDateString & "'), 0.00) as Refunded  ")
        '    .Append("		from (select  AwardTypeId,StudentAwardId, FundSourceDescrip, TitleIV, StuEnrollId,LastName,FirstName, MiddleName, ")
        '    .Append("		StudentIdentifier,LenderDescrip,CampGrpId,CampGrpDescrip, CampusId,CampDescrip,sum(Received) as Received, ")
        '    .Append("		InstCharges,PmtPlanCount from ( ")
        '    .Append("		select SA.AwardTypeId,SA.StudentAwardId, FS.FundSourceDescrip, FS.TitleIV, T1.StuEnrollId, S.LastName, S.FirstName, S.MiddleName, ")
        '    .Append("		" & strStudentId & " AS StudentIdentifier, (SELECT LenderDescrip FROM faLenders WHERE LenderId=SA.LenderId) AS LenderDescrip, ")
        '    .Append("		CGC.CampGrpId, syCampGrps.CampGrpDescrip, SE.CampusId, C.CampDescrip, ")
        '    .Append("		(case when PDR.Amount > -T1.TransAmount then -T1.TransAmount else PDR.Amount end) as Received, ")

        '    .Append("		0.00 as InstCharges,0 as PmtPlanCount")
        '    .Append("		from saTransactions T1, saPayments P, saPmtDisbRel PDR, faStudentAwardSchedule SAS, ")
        '    .Append("		faStudentAwards SA, saFundSources FS, arStuEnrollments SE, arStudent S, syCmpGrpCmps CGC, syCampGrps, syCampuses C ")
        '    .Append("		where	T1.voided=0 and	 T1.TransactionId=P.TransactionId and T1.TransactionId=PDR.TransactionId and PDR.AwardScheduleId=SAS.AwardScheduleId ")
        '    .Append("		and SAS.StudentAwardId=SA.StudentAwardId and SA.AwardTypeId=FS.FundSourceId and	FS.TitleIV=0 and T1.StuEnrollId=SE.StuEnrollId ")
        '    .Append("		and SE.StudentId=S.StudentId and CGC.CampusId=SE.CampusId and syCampGrps.CampGrpId=CGC.CampGrpId and C.CampusId=SE.CampusId and T1.Voided=0 ")
        '    .Append("       and ((T1.TransCodeId in (select TransCodeId from saTransCodes TC where TC.TransCodeId=T1.TransCodeId and TC.IsInstCharge=1 ))or (T1.TransCodeId in (select T2.TransCodeId from saTransCodes TC,saTransactions T2,saAppliedPayments AP1 where AP1.TransactionId=T1.TransactionId and AP1.AppliedPmtId=T2.TransactionId and TC.TransCodeId=T2.TransCodeId and TC.IsInstCharge=1 )))  ")
        '    .Append(strWhere)
        '    .Append("		and  T1.TransDate >='" & dtStartDate.ToShortDateString & "' and T1.TransDate <='" & dtEndDate.ToShortDateString & "' ) t")
        '    .Append("       group by AwardTypeId,StudentAwardId, FundSourceDescrip, TitleIV, StuEnrollId,LastName,FirstName, MiddleName, ")
        '    .Append("		StudentIdentifier,LenderDescrip,CampGrpId,CampGrpDescrip, CampusId,CampDescrip, ")
        '    .Append("		InstCharges,PmtPlanCount) tt ")

        '    .Append("union ")
        '    '.Append("--this is the amount of Refunds against any institutional charge")
        '    .Append(" select  AwardTypeId,StudentAwardId, FundSourceDescrip, TitleIV, StuEnrollId,LastName,FirstName, MiddleName,  ")
        '    .Append("		StudentIdentifier,LenderDescrip,CampGrpId,CampGrpDescrip, CampusId,CampDescrip,sum(Received) as Received, ")
        '    .Append("		InstCharges,PmtPlanCount,SUM(Refunded) as Refunded  from (  ")
        '    .Append("		select  '00000000-0000-0000-0000-000000000000' as AwardTypeId,'00000000-0000-0000-0000-000000000000' as StudentAwardId, 'Fund Source: Cash'	as FundSourceDescrip, ")
        '    .Append("		0 as TitleIV, T1.StuEnrollId, S.LastName, S.FirstName, S.MiddleName, " & strStudentId & " AS StudentIdentifier, ")
        '    .Append("		'' as LenderDescrip, CGC.CampGrpId, syCampGrps.CampGrpDescrip, SE.CampusId, C.CampDescrip, 0.00 as Received, ")
        '    .Append("		T1.TransAmount as Refunded,0.00 as InstCharges, 0 as PmtPlanCount  from	saTransactions T1, saRefunds R, ")
        '    .Append("		arStuEnrollments SE, arStudent S, syCmpGrpCmps CGC, syCampGrps, syCampuses C,saTransCodes TC  where	T1.voided=0 ")
        '    .Append("		and T1.TransactionId=R.TransactionId  and T1.StuEnrollId=SE.StuEnrollId  and	 R.RefundTypeId=0  ")
        '    .Append("		and R.StudentAwardId is null and	 SE.StudentId=S.StudentId  and CGC.CampusId=SE.CampusId ")
        '    .Append("		and syCampGrps.CampGrpId=CGC.CampGrpId  and	 C.CampusId=SE.CampusId")
        '    .Append("		and TC.TransCodeId=T1.TransCodeId  and TC.IsInstCharge=1 ")
        '    .Append(strWhere)
        '    .Append("		and  T1.TransDate >='" & dtStartDate.ToShortDateString & "' and T1.TransDate <='" & dtEndDate.ToShortDateString & "' ) t")
        '    .Append("       group by AwardTypeId,StudentAwardId, FundSourceDescrip, TitleIV, StuEnrollId,LastName,FirstName, MiddleName, ")
        '    .Append("		StudentIdentifier,LenderDescrip,CampGrpId,CampGrpDescrip, CampusId,CampDescrip, ")
        '    .Append("		PmtPlanCount,InstCharges ")

        '    .Append("union ")
        '    '.Append("--this is the amount of Student Cash Payments against any institutional charge")
        '    .Append(" select  AwardTypeId,StudentAwardId, FundSourceDescrip, TitleIV, StuEnrollId,LastName,FirstName, MiddleName,  ")
        '    .Append("		StudentIdentifier,LenderDescrip,CampGrpId,CampGrpDescrip, CampusId,CampDescrip,sum(Received) as Received, ")
        '    .Append("		InstCharges, PmtPlanCount,0.00 as Refunded from ( ")
        '    .Append("		select  '00000000-0000-0000-0000-000000000000' as AwardTypeId,'00000000-0000-0000-0000-000000000000' as StudentAwardId, 'Fund Source: Cash'	as FundSourceDescrip, ")
        '    .Append("		0 as TitleIV, T1.StuEnrollId, S.LastName, S.FirstName, S.MiddleName, S.SSN AS StudentIdentifier,  ")
        '    .Append("		'' as LenderDescrip, CGC.CampGrpId, syCampGrps.CampGrpDescrip, SE.CampusId, C.CampDescrip, -T1.TransAmount as Received, ")
        '    .Append("		0.00 as Refunded,0.00 as InstCharges, 0 as PmtPlanCount  from	saTransactions T1,  ")
        '    .Append("		arStuEnrollments SE, arStudent S, syCmpGrpCmps CGC, syCampGrps, syCampuses C,saTransCodes TC,saPayments P  where	T1.voided=0 ")
        '    .Append("		and T1.StuEnrollId=SE.StuEnrollId and SE.StudentId=S.StudentId  and CGC.CampusId=SE.CampusId  ")
        '    .Append("		and syCampGrps.CampGrpId=CGC.CampGrpId  and	 C.CampusId=SE.CampusId ")
        '    .Append("		and TC.TransCodeId=T1.TransCodeId  and TC.IsInstCharge=1")
        '    .Append("		and T1.TransactionId=P.TransactionId and P.ScheduledPayment=0 ")
        '    .Append(strWhere)
        '    .Append("		and  T1.TransDate >='" & dtStartDate.ToShortDateString & "' and T1.TransDate <='" & dtEndDate.ToShortDateString & "' ) t")
        '    .Append("       group by AwardTypeId,StudentAwardId, FundSourceDescrip, TitleIV, StuEnrollId,LastName,FirstName, MiddleName, ")
        '    .Append("		StudentIdentifier,LenderDescrip,CampGrpId,CampGrpDescrip, CampusId,CampDescrip, ")
        '    .Append("		PmtPlanCount,InstCharges ")


        'End With

        'db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
        'db.OpenConnection()

        ''db.CommandTimeout = 600
        ''ds = db.RunParamSQLDataSet(sb.ToString)

        Dim campGrpId As String = String.Empty
        campGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in (")).ToLower.Replace("t1.campgrpid in", "").Replace(")", "").Replace("(", "").Replace(" ", "").Replace("'", "")

        db.AddParameter("@StartDate", dtStartDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@EndDate", dtEndDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@campGrpId", campGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        db.AddParameter("@strStudentId", StudentIdentifier, SqlDbType.VarChar, , ParameterDirection.Input)
        ''''''''''''''''''
        db.CommandTimeout = 0
        ds = db.RunParamSQLDataSet_SP("dbo.USP_FA_GetRevenueRatioReport")


        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "RevenueRatioDetail"
            'Add new columns.
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("NetReceived", System.Type.GetType("System.Decimal")))
            ds.Tables(0).Columns.Add(New DataColumn("Stipends", System.Type.GetType("System.Decimal")))
            ds.Tables(0).Columns.Add(New DataColumn("NetReceivedMinusStipends", System.Type.GetType("System.Decimal")))
            '
            If ds.Tables(0).Rows.Count > 0 Then
                'Create table to store Totals per Campus Groups.
                Dim dt As New DataTable("CampGrpTotals")
                dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Received", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Refunded", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("NetReceived", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Stipends", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("NetReceivedMinusStipends", System.Type.GetType("System.Decimal")))
                ds.Tables.Add(dt)   'Add CampGrpTotals table to ds dataset.

                'Create table to store Totals per Campus Groups and Campuses.
                dt = New DataTable("CampusTotals")
                dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("CampDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Received", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Refunded", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("NetReceived", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Stipends", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("NetReceivedMinusStipends", System.Type.GetType("System.Decimal")))
                ds.Tables.Add(dt)   'Add CampusTotals table to ds dataset.

                'Create table to store Totals per Campus Groups,Campuses and Fund Sources.
                dt = New DataTable("FSTotals")
                dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("CampDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("FundSourceDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Received", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Refunded", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("NetReceived", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Stipends", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("NetReceivedMinusStipends", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("TitleIV", System.Type.GetType("System.Boolean")))
                ds.Tables.Add(dt)   'Add FSTotals table to ds dataset.

                'Create table to store TitleIV Totals.
                dt = New DataTable("TitleIVTotals")
                dt.Columns.Add(New DataColumn("Received", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Refunded", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("NetReceived", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Stipends", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("NetReceivedMinusStipends", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("RevenuePercentage", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Ratio", System.Type.GetType("System.String")))
                ds.Tables.Add(dt)   'Add CampusTotals table to ds dataset.
            End If
        End If

        Return ds
    End Function
    Public Function GetRevenueRatioSummary(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New SQLDataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStudentId As String
        Dim myCulture As New System.Globalization.CultureInfo("en-US", True)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim fiscalStart As String = MyAdvAppSettings.AppSettings("FiscalYearStart")
        Dim fiscalEnd As String = MyAdvAppSettings.AppSettings("FiscalYearEnd")
        Dim dtStartDate As Date
        Dim dtEndDate As Date
        Dim strYear As String = "/"
        Dim yrCopy As String

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            'strWhere &= " AND " & paramInfo.FilterOther
            Dim idx As Integer = paramInfo.FilterOther.IndexOf("BETWEEN")
            If idx > -1 Then
                Dim str As String = paramInfo.FilterOther.Substring(idx + 9)
                Dim lidx As Integer = str.IndexOf("'")
                dtStartDate = Date.Parse(str.Substring(0, lidx), myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
                idx = str.IndexOf("AND") + 5
                lidx = str.LastIndexOf("'")
                dtEndDate = Date.Parse(str.Substring(idx, lidx - idx), myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
                'Else

                '    yrCopy = paramInfo.FilterOther.Substring(7)
                '    strYear &= paramInfo.FilterOther.Substring(7)
            Else
                yrCopy = Today.Year
                strYear &= Today.Year
                dtStartDate = Date.Parse(fiscalStart & strYear, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
                dtEndDate = Date.Parse(fiscalEnd & strYear, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
            End If
        End If


        If Date.Compare(dtEndDate, dtStartDate) < 0 Then
            strYear = "/" & (Integer.Parse(yrCopy) + 1)
            dtEndDate = Date.Parse(fiscalEnd & strYear, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
        End If
        dtEndDate = dtEndDate.AddDays(1)


        'If paramInfo.OrderBy <> "" Then
        '    strOrderBy &= "," & paramInfo.OrderBy
        'End If
        Dim campGrpId As String = String.Empty
        campGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in (")).ToLower.Replace("t1.campgrpid in", "").Replace(")", "").Replace("(", "").Replace(" ", "").Replace("'", "")

        If StudentIdentifier = "StudentId" Then
            strStudentId = "S.StudentNumber"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStudentId = "SE.EnrollmentId"
        Else
            strStudentId = "S.SSN"
        End If


        db.AddParameter("@StartDate", dtStartDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@EndDate", dtEndDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@campGrpId", campGrpId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        db.AddParameter("@strStudentId", strStudentId, SqlDbType.VarChar, , ParameterDirection.Input)
        ''''''''''''''''''
        db.CommandTimeout = 0
        ds = db.RunParamSQLDataSet_SP("dbo.USP_FA_GetRevenueRatioSummaryReport")

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "RevenueRatioDetail"
            'Add new columns.
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("NetReceived", System.Type.GetType("System.Decimal")))
            ds.Tables(0).Columns.Add(New DataColumn("Stipends", System.Type.GetType("System.Decimal")))
            ds.Tables(0).Columns.Add(New DataColumn("NetReceivedMinusStipends", System.Type.GetType("System.Decimal")))
            '
            If ds.Tables(0).Rows.Count > 0 Then
                'Create table to store Totals per Campus Groups.
                Dim dt As New DataTable("CampGrpTotals")
                dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Received", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Refunded", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("NetReceived", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Stipends", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("NetReceivedMinusStipends", System.Type.GetType("System.Decimal")))
                ds.Tables.Add(dt)   'Add CampGrpTotals table to ds dataset.

                'Create table to store Totals per Campus Groups and Campuses.
                dt = New DataTable("CampusTotals")
                dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("CampDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Received", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Refunded", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("NetReceived", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Stipends", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("NetReceivedMinusStipends", System.Type.GetType("System.Decimal")))
                ds.Tables.Add(dt)   'Add CampusTotals table to ds dataset.

                'Create table to store Totals per Campus Groups,Campuses and Fund Sources.
                dt = New DataTable("FSTotals")
                dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("CampDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("FundSourceDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Received", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Refunded", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("NetReceived", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Stipends", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("NetReceivedMinusStipends", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("TitleIV", System.Type.GetType("System.Boolean")))
                ds.Tables.Add(dt)   'Add FSTotals table to ds dataset.

                'Create table to store TitleIV Totals.
                dt = New DataTable("TitleIVTotals")
                dt.Columns.Add(New DataColumn("Received", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Refunded", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("NetReceived", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Stipends", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("NetReceivedMinusStipends", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("RevenuePercentage", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Ratio", System.Type.GetType("System.String")))
                ds.Tables.Add(dt)   'Add CampusTotals table to ds dataset.
            End If
        End If

        Return ds
    End Function
    Public Function GetProjectedCashFlow(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStudentId As String
        Dim myCulture As New System.Globalization.CultureInfo("en-US", True)
        Dim refDate As String
        Dim idx As Integer
        Dim idx2 As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            'Parse FilterOther in order to get Reference Date
            idx = paramInfo.FilterOther.IndexOf("BETWEEN")
            If idx <> -1 Then
                idx2 = paramInfo.FilterOther.IndexOf("AM")
                If idx2 <> -1 Then
                    refDate = paramInfo.FilterOther.Substring(idx + 9, idx2 + 2 - (idx + 8))
                Else
                    refDate = paramInfo.FilterOther.Substring(idx + 8)
                End If
                Dim tempDate() As String = refDate.Split(" ")
                If tempDate.GetLength(0) > 0 Then
                    refDate = tempDate(0)
                End If
            End If
        End If

        Dim dtRefDate As Date = Date.Parse(refDate, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
        Dim agingDates As AgingBalance = GetAgingDates(dtRefDate)

        'If paramInfo.OrderBy <> "" Then
        '    strOrderBy &= "," & paramInfo.OrderBy
        'End If

        If StudentIdentifier = "StudentId" Then
            strStudentId = "S.StudentNumber"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStudentId = "arStuEnrollments.EnrollmentId"
        Else
            strStudentId = "S.SSN"
        End If

        With sb
            .Append("SELECT ")
            .Append("       syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,syCampuses.CampDescrip,")
            .Append("       arStuEnrollments.StuEnrollId,S.LastName,S.FirstName,S.MiddleName," & strStudentId & " AS StudentIdentifier,")
            .Append("       (SELECT PrgVerDescrip FROM arPrgVersions WHERE PrgVerId=arStuEnrollments.PrgVerId) AS PrgVerDescrip,")
            .Append("       (SELECT StatusCodeDescrip FROM syStatusCodes WHERE StatusCodeId=arStuEnrollments.StatusCodeId) AS StatusCodeDescrip,")
            .Append("       X.Amount, X.TimeFrame ")
            .Append("FROM   arStuEnrollments,arStudent S,syCampGrps,syCmpGrpCmps,syCampuses,")
            '               -- Projected Cash expected within the next 30 days
            .Append("		(SELECT PP.StuEnrollId,PPS.Amount,30 AS TimeFrame")
            .Append("		FROM faStuPaymentPlanSchedule PPS,faStudentPaymentPlans PP")
            .Append("       WHERE PP.PaymentPlanId=PPS.PaymentPlanId")
            .Append("		AND PPS.ExpectedDate>='" & agingDates.CurrentStart & "' AND PPS.ExpectedDate<='" & agingDates.CurrentEnd & "'")
            .Append("       UNION")
            .Append("		SELECT SA.StuEnrollId,SAS.Amount,30 AS TimeFrame")
            .Append("		FROM faStudentAwardSchedule SAS,faStudentAwards SA")
            .Append("       WHERE SA.StudentAwardId=SAS.StudentAwardId")
            .Append("		AND SAS.ExpectedDate>='" & agingDates.CurrentStart & "' AND SAS.ExpectedDate<='" & agingDates.CurrentEnd & "'")
            .Append("       UNION")
            '               -- Projected Cash expected within the next 31-60 days
            .Append("		SELECT PP.StuEnrollId,PPS.Amount,60 AS TimeFrame")
            .Append("		FROM faStuPaymentPlanSchedule PPS,faStudentPaymentPlans PP")
            .Append("       WHERE PP.PaymentPlanId=PPS.PaymentPlanId")
            .Append("		AND PPS.ExpectedDate>='" & agingDates.Start31To60DayPeriod & "' AND PPS.ExpectedDate<='" & agingDates.End31To60DayPeriod & "'")
            .Append("       UNION")
            .Append("		SELECT SA.StuEnrollId,SAS.Amount,60 AS TimeFrame")
            .Append("		FROM faStudentAwardSchedule SAS,faStudentAwards SA")
            .Append("       WHERE SA.StudentAwardId=SAS.StudentAwardId")
            .Append("		AND SAS.ExpectedDate>='" & agingDates.Start31To60DayPeriod & "' AND SAS.ExpectedDate<='" & agingDates.End31To60DayPeriod & "'")
            .Append("       UNION")
            '               -- Projected Cash expected within the next 61-90 days
            .Append("		SELECT PP.StuEnrollId,PPS.Amount,90 AS TimeFrame")
            .Append("		FROM faStuPaymentPlanSchedule PPS,faStudentPaymentPlans PP")
            .Append("       WHERE PP.PaymentPlanId=PPS.PaymentPlanId")
            .Append("		AND PPS.ExpectedDate>='" & agingDates.Start61To90DayPeriod & "' AND PPS.ExpectedDate<='" & agingDates.End61To90DayPeriod & "'")
            .Append("       UNION")
            .Append("		SELECT SA.StuEnrollId,SAS.Amount,90 AS TimeFrame")
            .Append("		FROM faStudentAwardSchedule SAS,faStudentAwards SA")
            .Append("       WHERE SA.StudentAwardId=SAS.StudentAwardId")
            .Append("		AND SAS.ExpectedDate>='" & agingDates.Start61To90DayPeriod & "' AND SAS.ExpectedDate<='" & agingDates.End61To90DayPeriod & "'")
            .Append("       UNION")
            '               -- Projected Cash expected within the next 91-120 days
            .Append("		SELECT PP.StuEnrollId,PPS.Amount,120 AS TimeFrame")
            .Append("		FROM faStuPaymentPlanSchedule PPS,faStudentPaymentPlans PP")
            .Append("       WHERE PP.PaymentPlanId=PPS.PaymentPlanId")
            .Append("		AND PPS.ExpectedDate>='" & agingDates.Start91To120Period & "' AND PPS.ExpectedDate<='" & agingDates.End91To120Period & "'")
            .Append("       UNION")
            .Append("		SELECT SA.StuEnrollId,SAS.Amount,120 AS TimeFrame")
            .Append("		FROM faStudentAwardSchedule SAS,faStudentAwards SA")
            .Append("       WHERE SA.StudentAwardId=SAS.StudentAwardId")
            .Append("		AND SAS.ExpectedDate>='" & agingDates.Start91To120Period & "' AND SAS.ExpectedDate<='" & agingDates.End91To120Period & "'")
            .Append("       UNION")
            '               -- Projected Cash expected over 120 days
            .Append("		SELECT PP.StuEnrollId,PPS.Amount,121 AS TimeFrame")
            .Append("		FROM faStuPaymentPlanSchedule PPS,faStudentPaymentPlans PP")
            .Append("       WHERE PP.PaymentPlanId=PPS.PaymentPlanId")
            .Append("		AND PPS.ExpectedDate>='" & agingDates.Over120DayPeriod & "'")
            .Append("       UNION")
            .Append("		SELECT SA.StuEnrollId,SAS.Amount,121 AS TimeFrame")
            .Append("		FROM faStudentAwardSchedule SAS,faStudentAwards SA")
            .Append("       WHERE SA.StudentAwardId=SAS.StudentAwardId")
            .Append("		AND SAS.ExpectedDate>='" & agingDates.Over120DayPeriod & "') X ")
            .Append("WHERE  arStuEnrollments.StuEnrollId=X.StuEnrollId AND arStuEnrollments.StudentId=S.StudentId")
            .Append("       AND syCmpGrpCmps.CampGrpId=syCampGrps.CampGrpId")
            .Append("       AND syCmpGrpCmps.CampusId=syCampuses.CampusId")
            .Append("       AND syCampuses.CampusId=arStuEnrollments.CampusId ")
            .Append(strWhere)
            .Append("ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,PrgVerDescrip,S.LastName,S.FirstName,S.MiddleName,arStuEnrollments.StuEnrollId,X.TimeFrame")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "ProjectionsDT"
            '
            If ds.Tables(0).Rows.Count > 0 Then
                'Create table to store Totals per Campus Groups.
                Dim dt As New DataTable("CampGrpTotals")
                dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Days30", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Days3160", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Days6190", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Days91120", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Over120Days", System.Type.GetType("System.Decimal")))
                ds.Tables.Add(dt)   'Add CampGrpTotals table to ds dataset.

                'Create table to store Totals per Campus Groups and Campuses.
                dt = New DataTable("CampusTotals")
                dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("CampDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Days30", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Days3160", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Days6190", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Days91120", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Over120Days", System.Type.GetType("System.Decimal")))
                ds.Tables.Add(dt)   'Add CampusTotals table to ds dataset.

                'Create table to store Totals per Campus Groups, Campuses and Program Versions.
                dt = New DataTable("PrgVerTotals")
                dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("CampDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("PrgVerDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Days30", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Days3160", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Days6190", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Days91120", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Over120Days", System.Type.GetType("System.Decimal")))
                ds.Tables.Add(dt)   'Add CampusTotals table to ds dataset.

                'Create table to store Grand Totals.
                dt = New DataTable("GrandTotals")
                dt.Columns.Add(New DataColumn("Days30", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Days3160", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Days6190", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Days91120", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Over120Days", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("StudentCount", System.Type.GetType("System.String")))
                ds.Tables.Add(dt)   'Add GrandTotals table to ds dataset.
            End If
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    ''New Code Added By Vijay Ramteke On July 23, 2010
    Public Function GetRevenueRatioDetailForAStudent(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        ''Dim db As New DataAccess
        Dim db As New SQLDataAccess
        Dim ds As New DataSet
        ''New Code Added By Vijay Ramteke On September 05, 2010
        Dim dtRevenueRatioDetailForStudent As DataTable
        ''New Code Added By Vijay Ramteke On September 05, 2010

        ''New Code Added By Vijay Ramteke On November 15, 2010 For Rally Id DE1290
        Dim dtRevenueRatioDetailCreditBalanceForStudent As DataTable
        ''New Code Added By Vijay Ramteke On November 15, 2010 For Rally Id DE1290

        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStudentId As String
        Dim myCulture As New System.Globalization.CultureInfo("en-US", True)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim fiscalStart As String = MyAdvAppSettings.AppSettings("FiscalYearStart")
        Dim fiscalEnd As String = MyAdvAppSettings.AppSettings("FiscalYearEnd")
        Dim dtStartDate As Date
        Dim dtEndDate As Date
        Dim strYear As String = "/"
        Dim yrCopy As String

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            Dim idx As Integer = paramInfo.FilterOther.IndexOf("BETWEEN")
            If idx > -1 Then
                Dim str As String = paramInfo.FilterOther.Substring(idx + 9)
                Dim lidx As Integer = str.IndexOf("'")
                dtStartDate = Date.Parse(str.Substring(0, lidx), myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
                idx = str.IndexOf("AND") + 5
                lidx = str.LastIndexOf("'")
                dtEndDate = Date.Parse(str.Substring(idx, lidx - idx), myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)

            End If
        End If
        db.ClearParameters()
        db.AddParameter("@StartDate", dtStartDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@EndDate", dtEndDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@StudentId", paramInfo.StudentId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        If Not paramInfo.StuEnrollId = Guid.Empty.ToString Then
            db.AddParameter("@StuEnrollId", paramInfo.StuEnrollId, SqlDbType.VarChar, , ParameterDirection.Input)
        Else
            db.AddParameter("@StuEnrollId", DBNull.Value, SqlDbType.VarChar, , ParameterDirection.Input)
        End If
        db.AddParameter("@StudentIdentifier", StudentIdentifier, SqlDbType.VarChar, , ParameterDirection.Input)
        ''''''''''''''''''
        db.CommandTimeout = 0
        ds = db.RunParamSQLDataSet_SP("USP_FA_GetDetailedReportForAStudent9010", "RevenueRatioDetailForStudent")

        If ds.Tables.Count > 0 Then
            'Add new columns.
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
        End If

        '' New Code Added By Vijay Ramteke On September 05, 2010
        dtRevenueRatioDetailForStudent = ds.Tables("RevenueRatioDetailForStudent")
        ds.Tables.RemoveAt(0)
        db.ClearParameters()
        db.AddParameter("@EndDate", dtStartDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@StudentId", paramInfo.StudentId, SqlDbType.VarChar, 8000, ParameterDirection.Input)
        If Not paramInfo.StuEnrollId = Guid.Empty.ToString Then
            db.AddParameter("@StuEnrollId", paramInfo.StuEnrollId, SqlDbType.VarChar, , ParameterDirection.Input)
        Else
            db.AddParameter("@StuEnrollId", DBNull.Value, SqlDbType.VarChar, , ParameterDirection.Input)
        End If
        db.AddParameter("@StudentIdentifier", StudentIdentifier, SqlDbType.VarChar, , ParameterDirection.Input)
        ''''''''''''''''''
        db.CommandTimeout = 0
        ds = db.RunParamSQLDataSet_SP("USP_FA_GetDetailedReportCreditBalanceForAStudent9010", "RevenueRatioDetailCreditBalanceForStudent")

        ''New Code Added By Vijay Ramteke On November 15, 2010 For Rally Id DE1290
        ''ds.Tables.Add(dtRevenueRatioDetailForStudent)

        dtRevenueRatioDetailCreditBalanceForStudent = ds.Tables("RevenueRatioDetailCreditBalanceForStudent")
        ds.Tables.RemoveAt(0)

        ds.Tables.Add(dtRevenueRatioDetailForStudent)
        ds.Tables.Add(dtRevenueRatioDetailCreditBalanceForStudent)
        ''New Code Added By Vijay Ramteke On November 15, 2010 For Rally Id DE1290

        '' New Code Added By Vijay Ramteke On September 05, 2010

        Return ds
    End Function
    ''New Code Added By Vijay Ramteke On July 23, 2010
    Public Function GetSummaryReportByStudent9010(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New SQLDataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strTempWhere As String
        Dim strOrderBy As String
        Dim strStudentId As String
        Dim myCulture As New System.Globalization.CultureInfo("en-US", True)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim fiscalStart As String = MyAdvAppSettings.AppSettings("FiscalYearStart")
        Dim fiscalEnd As String = MyAdvAppSettings.AppSettings("FiscalYearEnd")
        Dim dtStartDate As Date
        Dim dtEndDate As Date
        Dim strYear As String = "/"
        Dim yrCopy As String

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            'strWhere &= " AND " & paramInfo.FilterOther
            Dim idx As Integer = paramInfo.FilterOther.IndexOf("BETWEEN")
            If idx > -1 Then
                Dim str As String = paramInfo.FilterOther.Substring(idx + 9)
                Dim lidx As Integer = str.IndexOf("'")
                dtStartDate = Date.Parse(str.Substring(0, lidx), myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
                idx = str.IndexOf("AND") + 5
                lidx = str.LastIndexOf("'")
                dtEndDate = Date.Parse(str.Substring(idx, lidx - idx), myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)


            End If
        End If


        'If paramInfo.OrderBy <> "" Then
        '    strOrderBy &= "," & paramInfo.OrderBy
        'End If
        'Dim campGrpId As String = String.Empty
        'campGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in (")).ToLower.Replace("t1.campgrpid in", "").Replace(")", "").Replace("(", "").Replace(" ", "").Replace("'", "")

        Dim campGrpId As String = String.Empty
        Dim prgVerId As String = String.Empty

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If
        strTempWhere = strWhere
        strTempWhere = strTempWhere.ToLower.Replace("and", ";")
        strWhere = ""
        Dim strArr As String() = strTempWhere.Remove(strTempWhere.IndexOf(";"), 1).Split(";")

        For i As Integer = 0 To strArr.Length - 1
            strWhere &= " and " & strArr(i)
        Next
        If strArr.Length = 1 Then
            strWhere &= " and "
        End If
        campGrpId = strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("t1.campgrpid in (")).ToLower.IndexOf(" and ")).Replace("t1.campgrpid in", "").Replace(")", "").Replace("(", "").Replace("'", "")

        'get the prgverid
        If strWhere.ToLower.Contains("arprgversions.prgverid") Then
            If strWhere.ToLower.Contains("arprgversions.prgverid in ") Then
                If strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in ")).ToLower.IndexOf(" and ") >= 0 Then
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in (")).ToLower.IndexOf(" and ")).Replace("arprgversions.prgverid in", "").Replace(")", "").Replace("(", "").Replace("'", "")
                Else
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid in (")).Replace("arprgversions.prgverid in", "").Replace(")", "").Replace("(", "").Replace("'", "")
                End If
            Else
                If strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = ")).ToLower.IndexOf(" and ") >= 0 Then
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = "), strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = ")).ToLower.IndexOf(" and ")).Replace("arprgversions.prgverid =", "").Replace(")", "").Replace("(", "").Replace("'", "")
                Else
                    prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arprgversions.prgverid = ")).Replace("arprgversions.prgverid =", "").Replace(")", "").Replace("(", "").Replace("'", "")
                End If
            End If
        End If
        'end the prgverid


        If StudentIdentifier = "StudentId" Then
            strStudentId = "S.StudentNumber"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStudentId = "SE.EnrollmentId"
        Else
            strStudentId = "S.SSN"
        End If


        db.AddParameter("@StartDate", dtStartDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@EndDate", dtEndDate, SqlDbType.DateTime, , ParameterDirection.Input)
        db.AddParameter("@campGrpId", campGrpId.Trim(), SqlDbType.VarChar, 8000, ParameterDirection.Input)
        db.AddParameter("@strStudentId", strStudentId, SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", IIf(Not prgVerId = "", prgVerId.Trim(), DBNull.Value), SqlDbType.VarChar, , ParameterDirection.Input)

        ''''''''''''''''''
        db.CommandTimeout = 0
        ds = db.RunParamSQLDataSet_SP("dbo.USP_FA_SummaryReportByStudent9010")


        If ds.Tables.Count > 0 Then
            ds.Tables(0).TableName = "RevenueRatioDetail"
            'Add new columns.
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("NetReceived", System.Type.GetType("System.Decimal")))
            ds.Tables(0).Columns.Add(New DataColumn("Stipends", System.Type.GetType("System.Decimal")))
            ds.Tables(0).Columns.Add(New DataColumn("NetReceivedMinusStipends", System.Type.GetType("System.Decimal")))
            '
            If ds.Tables(0).Rows.Count > 0 Then

                Dim dt As New DataTable("CampGrpTotals")
                dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Numerator", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Denominator", System.Type.GetType("System.Decimal")))
                ds.Tables.Add(dt)   'Add CampGrpTotals table to ds dataset.


                dt = New DataTable("CampusTotals")
                dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("CampDescrip", System.Type.GetType("System.String")))
                dt.Columns.Add(New DataColumn("Numerator", System.Type.GetType("System.Decimal")))
                dt.Columns.Add(New DataColumn("Denominator", System.Type.GetType("System.Decimal")))

                ds.Tables.Add(dt)   'Add CampusTotals table to ds dataset.


            End If
        End If

        Return ds
    End Function

#End Region



#Region "Private Methods"

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

    Private Function GetAgingDates(ByVal ReferenceDate As DateTime) As AgingBalance
        Dim aDates As New AgingBalance
        aDates.ReferenceDate = ReferenceDate
        aDates.GoForward = True

        'set class property
        AgingDates = aDates

        Return AgingDates
    End Function

#End Region

End Class




