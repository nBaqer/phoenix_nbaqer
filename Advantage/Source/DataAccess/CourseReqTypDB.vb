
Imports System.Data
Imports System.Web
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class CourseReqTypDB
    Public Function GetAllCourseReqTyps() As DataSet
        Dim ds As DataSet

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.CourseReqTypId,Descrip ")
                .Append("FROM arCourseReqTypes t1 ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
End Class
