﻿Imports FAME.Advantage.Common

Public Class ExpectedDisbursementDB
#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
#End Region
#Region "Public Methods"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
    Public Function GetExpectedDisbursement(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""
        Dim CampusId As String = ""
        Dim db As New DataAccess
        Dim ds As New DataSet

        CampusId = paramInfo.CampusId

        If paramInfo.FilterList <> "" Then
            strWhere = " Where " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " And " & paramInfo.FilterOther
        End If

        strOrderBy &= " ORDER BY  syCampGrps.CampGrpDescrip,syCampuses.CampDescrip, arStudent.LastName,arStudent.FirstName, faStudentAwardSchedule.ExpectedDate"
        ''Code Changed by Vijay Ramteke Removed Fields on April 20, 2010 1.PrgVerDescrip 2.saFundSources.FundSourceDescrip  


        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If
        Try
            If paramInfo.ShowRptDisbNotBeenPaid = False Then
                With sb
                    .Append("SELECT syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,syCampuses.CampDescrip, ")
                    .Append(" arStuEnrollments.StuEnrollId,  arStudent.LastName,arStudent.FirstName,arStudent.MiddleName, ")
                    ' New code added by Vijay Ramteke on April 20, 2010 Mantis Id 18833
                    .Append(" arStudent.StudentId,  ")
                    ' New code added by Vijay Ramteke on April 20, 2010 Mantis Id 18833
                    If StudentIdentifier = "SSN" Then
                        .Append(" arStudent.SSN AS StudentIdentifier,")
                    ElseIf StudentIdentifier = "EnrollmentId" Then
                        .Append(" arStuEnrollments.EnrollmentId AS StudentIdentifier,")
                    ElseIf StudentIdentifier = "StudentId" Then
                        .Append(" arStudent.StudentNumber AS StudentIdentifier,")
                    End If
                    .Append(" faStudentAwardSchedule.ExpectedDate,faStudentAwardSchedule.Amount AS ExpectedAmount, arPrgVersions.PrgVerDescrip," + If(paramInfo.ShowRptGroupByEnrollment = True, " syStatusCodes.StatusCodeDescrip ", " '' as StatusCodeDescrip") + " ,saFundSources.FundSourceDescrip as FundSource, ")
                    .Append(" (select Sum( TR.TransAmount) from saTransactions TR, saPmtDisbRel PDR where Voided=0 and ")
                    .Append(" TR.TransactionId=PDR.TransactionId and PDR.AwardScheduleId=faStudentAwardSchedule.AwardScheduleId) AS AmountPaid, ")
                    .Append(" (select Max( TR.TransDate) from saTransactions TR, saPmtDisbRel PDR where Voided=0 and ")
                    .Append(" TR.TransactionId=PDR.TransactionId and PDR.AwardScheduleId=faStudentAwardSchedule.AwardScheduleId) AS DatePaid, ")
                    .Append(" IsNull((select Sum(TransAmount) from saTransactions t where IsPosted=1 and Voided=0 and StuEnrollId in (select StuEnrollId from arStuEnrollments where StudentId=arStudent.StudentId)),0) as LedgerBalance, ")
                    .Append(IIf(paramInfo.ShowRptGroupByEnrollment = True, " 1 AS ShowStudentGroup ", " 0 AS ShowStudentGroup "))

                    .Append(" FROM syCampGrps INNER JOIN syCmpGrpCmps ON syCampGrps.CampGrpId = syCmpGrpCmps.CampGrpId INNER JOIN   ")
                    .Append(" syCampuses ON syCmpGrpCmps.CampusId = syCampuses.CampusId INNER JOIN arStudent INNER JOIN arStuEnrollments ON arStudent.StudentId = arStuEnrollments.StudentId INNER JOIN    ")
                    .Append(" faStudentAwards INNER JOIN faStudentAwardSchedule ON faStudentAwards.StudentAwardId = faStudentAwardSchedule.StudentAwardId ON    ")
                    .Append("  arStuEnrollments.StuEnrollId = faStudentAwards.StuEnrollId INNER JOIN arPrgVersions ON arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId INNER JOIN ")
                    .Append(" syStatusCodes ON arStuEnrollments.StatusCodeId = syStatusCodes.StatusCodeId INNER JOIN saFundSources ON faStudentAwards.AwardTypeId = saFundSources.FundSourceId ON  syCampuses.CampusId = arStuEnrollments.CampusId   ")
                    .Append(strWhere)
                    .Append(strOrderBy)
                End With
            Else
                With sb
                    .Append("SELECT syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,syCampuses.CampusId,syCampuses.CampDescrip, ")
                    .Append(" arStuEnrollments.StuEnrollId,  arStudent.LastName,arStudent.FirstName,arStudent.MiddleName, ")
                    ' New code added by Vijay Ramteke on April 20, 2010 Mantis Id 18833
                    .Append(" arStudent.StudentId,  ")
                    ' New code added by Vijay Ramteke on April 20, 2010 Mantis Id 18833
                    If StudentIdentifier = "SSN" Then
                        .Append(" arStudent.SSN AS StudentIdentifier,")
                    ElseIf StudentIdentifier = "EnrollmentId" Then
                        .Append(" arStuEnrollments.EnrollmentId AS StudentIdentifier,")
                    ElseIf StudentIdentifier = "StudentId" Then
                        .Append(" arStudent.StudentNumber AS StudentIdentifier,")
                    End If
                    .Append(" faStudentAwardSchedule.ExpectedDate,faStudentAwardSchedule.Amount AS ExpectedAmount, arPrgVersions.PrgVerDescrip," + If(paramInfo.ShowRptGroupByEnrollment = True, " syStatusCodes.StatusCodeDescrip ", " '' as StatusCodeDescrip") + " ,saFundSources.FundSourceDescrip as FundSource, ")
                    .Append(" null AS AmountPaid, ")
                    .Append(" null AS DatePaid, ")
                    .Append(" null as LedgerBalance, ")
                    .Append(IIf(paramInfo.ShowRptGroupByEnrollment = True, " 1 AS ShowStudentGroup ", " 0 AS ShowStudentGroup "))

                    .Append(" FROM syCampGrps INNER JOIN syCmpGrpCmps ON syCampGrps.CampGrpId = syCmpGrpCmps.CampGrpId INNER JOIN   ")
                    .Append(" syCampuses ON syCmpGrpCmps.CampusId = syCampuses.CampusId INNER JOIN arStudent INNER JOIN arStuEnrollments ON arStudent.StudentId = arStuEnrollments.StudentId INNER JOIN    ")
                    .Append(" faStudentAwards INNER JOIN faStudentAwardSchedule ON faStudentAwards.StudentAwardId = faStudentAwardSchedule.StudentAwardId ON    ")
                    .Append("  arStuEnrollments.StuEnrollId = faStudentAwards.StuEnrollId INNER JOIN arPrgVersions ON arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId INNER JOIN ")
                    .Append(" syStatusCodes ON arStuEnrollments.StatusCodeId = syStatusCodes.StatusCodeId INNER JOIN saFundSources ON faStudentAwards.AwardTypeId = saFundSources.FundSourceId ON  syCampuses.CampusId = arStuEnrollments.CampusId   ")

                    .Append(strWhere)
                    .Append(" and faStudentAwardSchedule.AwardScheduleId not in (Select PDR.AwardScheduleId From saPmtDisbRel PDR ,saTransactions TR Where PDR.TransactionId=TR.TransactionId and  PDR.AwardScheduleId=faStudentAwardSchedule.AwardScheduleId and TR.Voided=0) ")
                    .Append(strOrderBy)
                End With
            End If
            ds = db.RunParamSQLDataSet(sb.ToString)
        Catch ex As Exception

        End Try

        Return ds
    End Function
#End Region
#Region "Public Properties"
    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region
End Class
