' ===============================================================================
' TMDB.vb
' DataAccess classes for the TM project
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' 6/14/06 - Delete returns an error message if there are DI issues
' 11/2/06 - (BEN) Added support for StartGap to tmResultTasks

Imports System.Text
Imports System.Data.OleDb
Imports FAME.AdvantageV1.Common.TM
Imports FAME.Advantage.Common

Namespace TM

#Region "Categories"
    Public Class CategoriesDB
        ''' <summary>
        ''' Returns all active categories from the database
        ''' </summary>
        ''' <param name="campusId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAll(ByVal campusId As String, ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ' build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append(" CategoryId, " & vbCrLf)
            sb.Append(" Code, " & vbCrLf)
            sb.Append(" Descrip, " & vbCrLf)
            sb.Append(" CampGroupId, " & vbCrLf)
            sb.Append(" ModDate, " & vbCrLf)
            sb.Append(" ModUser, " & vbCrLf)
            sb.Append(" Active " & vbCrLf)
            sb.Append("FROM tmCategories " & vbCrLf)
            If ShowActive And Not ShowInactive Then
                sb.Append("WHERE " & vbCrLf)
                sb.Append("Active = ? " & vbCrLf)
                db.AddParameter("@Active", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            ElseIf Not ShowActive And ShowInactive Then
                sb.Append("WHERE " & vbCrLf)
                sb.Append("Active = ? " & vbCrLf)
                db.AddParameter("@Active", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If
            sb.Append("ORDER BY Descrip")
            ' return the result
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieves a CategoryInfo object from the database
        ''' </summary>
        ''' <param name="CategoryId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal CategoryId As String) As CategoryInfo
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ' build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append(" CategoryId, " & vbCrLf)
            sb.Append(" Code, " & vbCrLf)
            sb.Append(" Descrip, " & vbCrLf)
            sb.Append(" CampGroupId, " & vbCrLf)
            sb.Append(" ModDate, " & vbCrLf)
            sb.Append(" ModUser, " & vbCrLf)
            sb.Append(" Active " & vbCrLf)
            sb.Append("FROM tmCategories " & vbCrLf)
            sb.Append("WHERE CategoryId = ? " & vbCrLf)

            ' Add the parameter list
            db.AddParameter("Id", CategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ' Execute the query, build the info class and return the results
            Try
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                Dim info As New CategoryInfo
                If dr.Read() Then
                    '   set properties with data from DataReader
                    info.CategoryId = CategoryId
                    info.IsInDB = True
                    If Not (dr("CategoryId") Is System.DBNull.Value) Then info.CategoryId = CType(dr("CategoryId"), Guid).ToString
                    If Not (dr("Code") Is System.DBNull.Value) Then info.Code = dr("Code")
                    If Not (dr("Descrip") Is System.DBNull.Value) Then info.Descrip = dr("Descrip")
                    If Not (dr("CampGroupId") Is System.DBNull.Value) Then info.CampGroupId = CType(dr("CampGroupId"), Guid).ToString
                    If Not (dr("ModDate") Is System.DBNull.Value) Then info.ModDate = dr("ModDate")
                    If Not (dr("ModUser") Is System.DBNull.Value) Then info.ModUser = dr("ModUser").ToString()
                    If Not (dr("Active") Is System.DBNull.Value) Then info.Active = CType(dr("Active"), Integer)
                End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                Return info
            Catch ex As System.Exception
            End Try
            Return Nothing ' Error
        End Function

        ''' <summary>
        ''' Updates the database given a CategoryInfo object
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Update(ByVal info As CategoryInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an update
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("UPDATE tmCategories " & vbCrLf)
                sb.Append("SET " & vbCrLf)
                sb.Append(" Code = ?, " & vbCrLf)
                sb.Append(" Descrip = ?, " & vbCrLf)
                sb.Append(" CampGroupId = ?, " & vbCrLf)
                sb.Append(" ModDate = ?, " & vbCrLf)
                sb.Append(" ModUser = ?, " & vbCrLf)
                sb.Append(" Active = ? " & vbCrLf)
                sb.Append(" WHERE CategoryId = ? ")

                '   add parameters values to the query                            
                db.AddParameter("@Code", info.Code, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
                db.AddParameter("@Descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.CampGroupId = Guid.Empty.ToString Then
                    db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CampGrpId", info.CampGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                If info.CategoryId = Guid.Empty.ToString Then
                    db.AddParameter("@CategoryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CategoryId", info.CategoryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                ' run the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return False
            End Try
        End Function

        ''' <summary>
        ''' Persists a CategoryInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As CategoryInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("INSERT tmCategories (CategoryId, Code, Descrip, CampGroupId, ModDate, ModUser, Active) ")
                sb.Append(" VALUES (?,?,?,?,?,?,?) ")

                '   add parameters values to the query
                If info.CategoryId = Guid.Empty.ToString Then
                    db.AddParameter("@Id", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@Id", info.CategoryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@Code", info.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.CampGroupId = Guid.Empty.ToString Then
                    db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CampGrpId", info.CampGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                '   return without errors
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return False
            End Try
        End Function

        ''' <summary>
        ''' Deletes a category from the tmCategories table
        ''' </summary>
        ''' <param name="Id"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal Id As String, ByVal modDate As DateTime) As String
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                ' check if there are already user tasks with this taskid
                Dim sql As String = String.Format("SELECT COUNT(*) from tmTasks WHERE CategoryId = '{0}'", Id)
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                If rowCount > 0 Then
                    db.CloseConnection()
                    Return "Category cannot be deleted.  Category is used by Tasks."
                Else
                    ' no usertasks with this taskid, we can safely delete the task and supporting tables
                    sql = "DELETE tmCategories WHERE CategoryId = ? AND ModDate = ? " + vbCrLf
                    sql += "SELECT COUNT(*) from tmCategories WHERE CategoryId = ? " + vbCrLf

                    db.AddParameter("@Id", Id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@Id", Id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    '   execute the query
                    rowCount = db.RunParamSQLScalar(sql)
                    '   If the row was not deleted then there was a concurrency problem
                    If rowCount = 0 Then
                        Return ""   '   return without errors
                    Else
                        Return DALExceptions.BuildConcurrencyExceptionMessage()
                    End If
                End If
                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function
    End Class
#End Region

#Region "Permissions"
    Public Class PermissionsDB
        ''' <summary>
        ''' Get all users that are valid for the given userid
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAll(ByVal UserId As String, ByVal CampGrpId As String) As Data.DataSet
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append(" U.UserId, " & vbCrLf)
            sb.Append(" U.FullName, " & vbCrLf)
            sb.Append(" P.RoleId, " & vbCrLf)
            sb.Append(" R.Role " & vbCrLf)
            sb.Append("FROM " & vbCrLf)
            sb.Append("  tmPermissions as P INNER JOIN " & vbCrLf)
            sb.Append("  syRoles AS R ON P.RoleId = R.RoleId INNER JOIN " & vbCrLf)
            sb.Append("  syUsers AS U ON P.UserID = U.UserId " & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            sb.Append("  P.UserId = ? " & vbCrLf)
            db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            If Not CampGrpId Is Nothing AndAlso CampGrpId <> "" Then
                sb.Append("AND P.CampGrpId = ? " & vbCrLf)
                db.AddParameter("@CampGrpId", CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            ' return the result
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        Public Shared Function GetInfo(ByVal userId As String) As PermissionsInfo
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT " & vbCrLf)
                .Append(" SID, " & vbCrLf)
                .Append(" UserId, " & vbCrLf)
                .Append(" RoleId, " & vbCrLf)
                .Append(" CampGrpId " & vbCrLf)
                .Append("FROM " & vbCrLf)
                .Append(" tmPermissions" & vbCrLf)
                .Append("WHERE " & vbCrLf)
                .Append("UserId = ? " & vbCrLf)
            End With

            db.AddParameter("@UserId", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            Dim info As New PermissionsInfo
            Try
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                If dr.Read() Then
                    '   set properties with data from DataReader
                    info.UserId = userId
                    info.IsInDB = True
                    If Not (dr("SID") Is System.DBNull.Value) Then info.SID = CType(dr("SID"), Guid).ToString
                    If Not (dr("RoleId") Is System.DBNull.Value) Then info.RoleId = CType(dr("RoleId"), Guid).ToString
                    If Not (dr("CampGrpId") Is System.DBNull.Value) Then info.CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                Return info
            Catch ex As System.Exception
            End Try
            Return Nothing ' Error

        End Function

        Public Shared Function Update(ByVal info As PermissionsInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("UPDATE tmPermissions " & vbCrLf)
                sb.Append("SET UserId=?, RoleId=?, CampGrpId=? " & vbCrLf)
                sb.Append("WHERE " & vbCrLf)
                sb.Append("SID = ?" & vbCrLf)

                '   add parameters values to the query
                If info.UserId = "" Or info.UserId = Guid.Empty.ToString Then
                    db.AddParameter("@UserID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@UserID", info.UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.RoleId = "" Or info.RoleId = Guid.Empty.ToString Then
                    db.AddParameter("@RoleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@RoleId", info.RoleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                If info.CampGrpId = "" Or info.CampGrpId = Guid.Empty.ToString Then
                    db.AddParameter("@CamGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CampGrpId", info.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                db.AddParameter("@SID", info.SID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                Return False
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return True
        End Function

        Public Shared Function Add(ByVal info As PermissionsInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("INSERT tmPermissions (SID, UserID, RoleID, CampGrpId) ")
                sb.Append(" VALUES (?,?,?,?) ")

                '   add parameters values to the query
                If info.SID = "" Or info.SID = Guid.Empty.ToString Then
                    db.AddParameter("@SID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@SID", info.SID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.UserId = "" Or info.UserId = Guid.Empty.ToString Then
                    db.AddParameter("@UserID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@UserID", info.UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.RoleId = "" Or info.RoleId = Guid.Empty.ToString Then
                    db.AddParameter("@RoleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@RoleId", info.RoleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                If info.CampGrpId = "" Or info.CampGrpId = Guid.Empty.ToString Then
                    db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CampGrpId", info.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                '   return without errors
                Return True
            Catch ex As OleDbException
                If db.Connection.State = Data.ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function

        Public Shared Function Delete(ByVal SID As String) As Integer
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                '   build the query
                Dim sql As String = "DELETE FROM tmPermissions WHERE SID = ? "

                '   add parameters values to the query
                db.AddParameter("@SID", SID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                '   execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                db.CloseConnection()
                '   If the row was not deleted then there was a concurrency problem
                Return rowCount
            Catch ex As OleDbException
                If db.Connection.State = Data.ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.ErrorCode
            End Try
        End Function

        Public Shared Function DeleteByUser(ByVal UserId As String) As Integer
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                '   build the query
                Dim sql As String = "DELETE FROM tmPermissions WHERE UserID = ? " ' don' know if we need campus id here too

                '   add parameters values to the query
                db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                '   execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                db.CloseConnection()
                '   If the row was not deleted then there was a concurrency problem
                Return rowCount
            Catch ex As OleDbException
                If db.Connection.State = Data.ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.ErrorCode
            End Try
        End Function

        Public Shared Function GetSystemUsers(ByVal CampGrpId As String, ByVal ShowActive As Boolean, ByVal ShowInActive As Boolean) As Data.DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append(" U.UserId, " & vbCrLf)
            sb.Append(" U.FullName, " & vbCrLf)
            sb.Append(" U.AccountActive " & vbCrLf)
            sb.Append("FROM syUsers U " & vbCrLf)
            'sb.Append("WHERE " & vbCrLf)
            'sb.Append(" U.CampusId in (select CampGrpId ? " & vbCrLf)
            'db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            If ShowActive And Not ShowInActive Then
                sb.Append("WHERE " & vbCrLf)
                sb.Append("U.AccountActive = ? " & vbCrLf)
                db.AddParameter("@Active", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            ElseIf Not ShowActive And ShowInActive Then
                sb.Append("WHERE " & vbCrLf)
                sb.Append("U.AccountActive = ? " & vbCrLf)
                db.AddParameter("@Active", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If
            ' Else there no need to do anything else because if ShowActive and ShowInActive are both true, then 
            ' there is no filter

            sb.Append("ORDER BY U.FullName " + vbCrLf)
            ' return the result
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Get all the roles defined in the system
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetSystemRoles() As Data.DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " + vbCrLf)
            sb.Append(" distinct R.RoleId, " + vbCrLf)
            sb.Append(" R.Role " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append(" syRoles R" + vbCrLf)
            sb.Append("ORDER BY R.Role")
            ' return the result
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Get all the campus groups defined in the system
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetCampusGroups() As Data.DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append(" C.CampGrpId, " & vbCrLf)
            sb.Append(" C.CampGrpDescrip " & vbCrLf)
            sb.Append("FROM syCampGrps C " & vbCrLf)
            sb.Append("ORDER BY C.CampGrpDescrip")
            ' return the result
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
        Public Shared Function GetCampusGroupsExceptAll() As Data.DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append(" C.CampGrpId, " & vbCrLf)
            sb.Append(" C.CampGrpDescrip " & vbCrLf)
            sb.Append("FROM syCampGrps C  where IsAllCampusGrp=0 " & vbCrLf)
            sb.Append("ORDER BY C.CampGrpDescrip")
            ' return the result
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        Public Shared Function GetAuthorizedAssignedToUsers(ByVal UserId As String, ByVal CampusId As String)
            If UserId Is Nothing Or UserId = "" Or CampusId Is Nothing Or CampusId = "" Then
                Return Nothing
            End If

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append("  DISTINCT UR.UserId, " & vbCrLf)
            sb.Append("  U.FullName " & vbCrLf)
            sb.Append("FROM " & vbCrLf)
            sb.Append("  syUsersRolesCampGrps AS UR INNER JOIN " & vbCrLf)
            sb.Append("  syCmpGrpCmps AS CG ON UR.CampGrpId = CG.CampGrpId INNER JOIN " & vbCrLf)
            sb.Append("  syCmpGrpCmps CC on CG.CampGrpId = CC.CampGrpId INNER JOIN " + vbCrLf)
            sb.Append("  syRoles AS R ON UR.RoleId = R.RoleId INNER JOIN " & vbCrLf)
            sb.Append("  syUsers AS U ON UR.UserID = U.UserId " & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            sb.Append("  CC.CampusId = ? " & vbCrLf)
            sb.Append("AND R.RoleId in " & vbCrLf)
            sb.Append("    (SELECT P.RoleId " & vbCrLf)
            sb.Append("     FROM " & vbCrLf)
            sb.Append("       tmPermissions as P INNER JOIN " & vbCrLf)
            sb.Append("       syRoles AS R ON P.RoleId = R.RoleId INNER JOIN " & vbCrLf)
            sb.Append("       syUsers AS U ON P.UserID = U.UserId " & vbCrLf)
            sb.Append("     WHERE " & vbCrLf)
            sb.Append("       P.UserId = ? AND U.CampusId = ?) " & vbCrLf)
            sb.Append("     GROUP BY UR.UserId, U.FullName " & vbCrLf)
            sb.Append("     ORDER BY U.FullName " & vbCrLf)
            ' return the result
            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@CampusId2", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
    End Class
#End Region

#Region "Results"
    Public Class ResultsDB

        ''' <summary>
        ''' Retrieve results from tmResults table
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAll(ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As Data.DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append(" R.ResultId, " & vbCrLf)
            sb.Append(" R.Code, " & vbCrLf)
            sb.Append(" R.Descrip, " & vbCrLf)
            sb.Append(" R.ResultActionId, " & vbCrLf)
            sb.Append(" (select TableName from tmResultActions where ResultActionId = R.ResultActionId) as ResultActionTableName, " & vbCrLf)
            sb.Append(" (select ColumnName from tmResultActions where ResultActionId = R.ResultActionId) as ResultActionColumnName, " & vbCrLf)
            sb.Append(" (select RowGuid from tmResultActions where ResultActionId = R.ResultActionId) as ResultActionRowGuid, " & vbCrLf)
            sb.Append(" R.ResultActionValue, " & vbCrLf)
            sb.Append(" R.ModDate, " & vbCrLf)
            sb.Append(" R.ModUser, " & vbCrLf)
            sb.Append(" R.Active " & vbCrLf)
            sb.Append("FROM tmResults R " & vbCrLf)
            If ShowActive And Not ShowInactive Then
                sb.Append("WHERE " & vbCrLf)
                sb.Append("R.Active = ? " & vbCrLf)
                db.AddParameter("@Active", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            ElseIf Not ShowActive And ShowInactive Then
                sb.Append("WHERE " & vbCrLf)
                sb.Append("R.Active = ? " & vbCrLf)
                db.AddParameter("@Active", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If
            sb.Append("ORDER BY R.Descrip")

            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieve results from tmResultActions table
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetResultActions() As Data.DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append(" RA.ResultActionId, " & vbCrLf)
            sb.Append(" RA.Descrip, " & vbCrLf)
            sb.Append(" RA.TableName, " & vbCrLf)
            sb.Append(" RA.ColumnName, " & vbCrLf)
            sb.Append(" RA.Values_Query " & vbCrLf)
            sb.Append("FROM tmResultActions RA " & vbCrLf)
            sb.Append("ORDER BY RA.Descrip")

            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieve results from tmResultActions table
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetResultActionValues(ByVal ResultActionId As String) As Data.DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append(" RA.Values_Query " & vbCrLf)
            sb.Append("FROM tmResultActions RA " & vbCrLf)
            sb.Append("WHERE RA.ResultActionId = ?")
            db.AddParameter("ResultActionId", ResultActionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Dim sql As String = db.RunParamSQLScalar(sb.ToString)

            Return db.RunParamSQLDataSet(sql)
        End Function

        ''' <summary>
        ''' Get a ResultInfo object given a ResultsId
        ''' </summary>
        ''' <param name="ResultId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal ResultId As String) As ResultInfo
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append(" R.Descrip, " & vbCrLf)
            sb.Append(" R.Code, " & vbCrLf)
            sb.Append(" R.ResultActionId, " & vbCrLf)
            sb.Append(" (select TableName from tmResultActions where ResultActionId = R.ResultActionId) as ResultActionTableName, " & vbCrLf)
            sb.Append(" (select ColumnName from tmResultActions where ResultActionId = R.ResultActionId) as ResultActionColumnName, " & vbCrLf)
            sb.Append(" (select RowGuid from tmResultActions where ResultActionId = R.ResultActionId) as ResultActionRowGuid, " & vbCrLf)
            sb.Append(" R.ResultActionValue, " & vbCrLf)
            sb.Append(" R.ModDate, " & vbCrLf)
            sb.Append(" R.ModUser, " & vbCrLf)
            sb.Append(" R.Active " & vbCrLf)
            sb.Append("FROM tmResults R " & vbCrLf)
            sb.Append("WHERE R.ResultId = ? " & vbCrLf)

            db.AddParameter("ResultId", ResultId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            Try
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                Dim info As New ResultInfo
                If dr.Read() Then
                    '   set properties with data from DataReader
                    info.ResultId = ResultId
                    info.IsInDB = True
                    If Not (dr("Code") Is System.DBNull.Value) Then info.Code = dr("Code")
                    If Not (dr("Descrip") Is System.DBNull.Value) Then info.Descrip = dr("Descrip")
                    If Not (dr("ResultActionId") Is System.DBNull.Value) Then info.ResultActionId = dr("ResultActionId").ToString()
                    If Not (dr("ResultActionTableName") Is System.DBNull.Value) Then info.ResultActionTableName = dr("ResultActionTableName")
                    If Not (dr("ResultActionColumnName") Is System.DBNull.Value) Then info.ResultActionColumnName = dr("ResultActionColumnName")
                    If Not (dr("ResultActionRowGuid") Is System.DBNull.Value) Then info.ResultActionRowGuid = dr("ResultActionRowGuid")
                    If Not (dr("ResultActionValue") Is System.DBNull.Value) Then info.ResultActionValue = dr("ResultActionValue")
                    If Not (dr("ModDate") Is System.DBNull.Value) Then info.ModDate = dr("ModDate")
                    If Not (dr("ModUser") Is System.DBNull.Value) Then info.ModUser = dr("ModUser").ToString()
                    If Not (dr("Active") Is System.DBNull.Value) Then info.Active = CType(dr("Active"), Boolean)
                End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                Return info
            Catch ex As System.Exception
            End Try
            Return Nothing ' Error
        End Function

        ''' <summary>
        ''' Updates the database given a ResultInfo object
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Update(ByVal info As ResultInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an update
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("UPDATE tmResults " & vbCrLf)
                sb.Append("SET " & vbCrLf)
                sb.Append(" Code = ?, " & vbCrLf)
                sb.Append(" Descrip = ?, " & vbCrLf)
                sb.Append(" ResultActionId = ?, " & vbCrLf)
                sb.Append(" ResultActionValue = ?, " & vbCrLf)
                sb.Append(" ModDate = ?, " & vbCrLf)
                sb.Append(" ModUser = ?, " & vbCrLf)
                sb.Append(" Active = ? " & vbCrLf)
                sb.Append(" WHERE ResultId = ? ")

                '   add parameters values to the query                            
                db.AddParameter("@Code", info.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.ResultActionId = "" Or info.ResultActionId Is Nothing Or info.ResultActionId = Guid.Empty.ToString Then
                    db.AddParameter("@ResultActionId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ResultActionId", info.ResultActionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@ResultActionValue", info.ResultActionValue, DataAccess.OleDbDataType.OleDbString, 256, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                db.AddParameter("@ResultId", info.ResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                ' run the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return True
            Catch ex As OleDbException
                If db.Connection.State = Data.ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Persists a single ResultInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As ResultInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("INSERT tmResults (ResultId, Code, Descrip, ResultActionId, ResultActionValue, Active, ModDate, ModUser) ")
                sb.Append(" VALUES (?,?,?,?,?,?,?,?) ")

                '   add parameters values to the query
                If info.ResultId = "" Or info.ResultId = Guid.Empty.ToString Then
                    db.AddParameter("@ResultId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ResultId", info.ResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@Code", info.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Descrip", info.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.ResultActionId = "" Or info.ResultActionId Is Nothing Or info.ResultActionId = Guid.Empty.ToString Then
                    db.AddParameter("@ResultActionId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ResultActionId", info.ResultActionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@ResultActionValue", info.ResultActionValue, DataAccess.OleDbDataType.OleDbString, 256, ParameterDirection.Input)
                db.AddParameter("@Active", info.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                '   return without errors
                Return True
            Catch ex As OleDbException
                If db.Connection.State = Data.ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Deletes a result from the tmResults table given a ResultId
        ''' </summary>
        ''' <param name="Id"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal Id As String, ByVal modDate As DateTime) As String
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                ' check if there are already user tasks with this taskid
                Dim sql As String = String.Format("SELECT COUNT(*) from tmTaskResults WHERE ResultId = '{0}'", Id)
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                If rowCount > 0 Then
                    db.CloseConnection()
                    Return "Result cannot be deleted.  Result is used by Tasks."
                Else
                    ' it is safe to delete the results
                    sql = "DELETE tmResults WHERE ResultId = ? AND ModDate = ? " + vbCrLf
                    sql += "SELECT COUNT(*) from tmResults WHERE ResultId = ? " + vbCrLf

                    db.AddParameter("@Id", Id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@Id", Id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    '   execute the query
                    rowCount = db.RunParamSQLScalar(sql)
                    '   If the row was not deleted then there was a concurrency problem
                    If rowCount = 0 Then
                        '   return without errors
                        Return ""
                    Else
                        Return DALExceptions.BuildConcurrencyExceptionMessage()
                    End If
                End If
                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function
    End Class
#End Region

#Region "ResultTasks"
    Public Class ResultTasksDB
        ''' <summary>
        ''' Retrieve all the of the next tasks to be created given a tmResults.ResultId
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAll(ByVal ResultId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append(" RT.ResultTaskId, " & vbCrLf)
            sb.Append(" RT.ResultId, " & vbCrLf)
            sb.Append(" RT.TaskId, " & vbCrLf)
            sb.Append(" T.Descrip as TaskDescrip, " & vbCrLf)
            sb.Append(" RT.TaskOrder, " & vbCrLf)
            sb.Append(" isnull(RT.StartGap,0) as StartGap, " + vbCrLf)
            sb.Append(" isnull(RT.StartGapUnit,'day') as StartGapUnit, " + vbCrLf)
            sb.Append(" R.Descrip as ResultDescrip, " & vbCrLf)
            sb.Append(" R.ResultActionId, " & vbCrLf)
            sb.Append(" (select TableName from tmResultActions where ResultActionId = R.ResultActionId) as ResultActionTableName, " & vbCrLf)
            sb.Append(" (select ColumnName from tmResultActions where ResultActionId = R.ResultActionId) as ResultActionColumnName, " & vbCrLf)
            sb.Append(" R.ResultActionValue, " & vbCrLf)
            sb.Append(" R.Active " & vbCrLf)
            sb.Append("FROM tmResultTasks RT, tmTasks T, tmResults R " & vbCrLf)
            sb.Append("WHERE RT.ResultId=R.ResultID AND RT.TaskId=T.TaskId " & vbCrLf)
            If Not ResultId Is Nothing Then
                sb.Append("AND R.ResultId = ? " & vbCrLf)
                db.AddParameter("ResultId", ResultId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            sb.Append("ORDER BY RT.ResultId, RT.TaskOrder, T.Descrip")

            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Get a ResultTaskInfo object given a ResultTaskId
        ''' </summary>
        ''' <param name="ResultTaskId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal ResultTaskId As String) As ResultTaskInfo
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append(" RT.ResultTaskId, " & vbCrLf)
            sb.Append(" RT.ResultId, " & vbCrLf)
            sb.Append(" RT.TaskId, " & vbCrLf)
            sb.Append(" T.Descrip as TaskDescrip, " & vbCrLf)
            sb.Append(" RT.TaskOrder, " & vbCrLf)
            sb.Append(" isnull(RT.StartGap,0) as StartGap, " + vbCrLf)
            sb.Append(" isnull(RT.StartGapUnit,'day') as StartGapUnit, " + vbCrLf)
            sb.Append(" R.Descrip as ResultDescrip, " & vbCrLf)
            sb.Append(" R.ResultActionId, " & vbCrLf)
            sb.Append(" (select TableName from tmResultActions where ResultActionId = R.ResultActionId) as ResultActionTableName, " & vbCrLf)
            sb.Append(" (select ColumnName from tmResultActions where ResultActionId = R.ResultActionId) as ResultActionColumnName, " & vbCrLf)
            sb.Append(" R.ResultActionValue, " & vbCrLf)
            sb.Append(" R.Active " & vbCrLf)
            sb.Append("FROM tmResultTasks RT, tmTasks T, tmResults R " & vbCrLf)
            sb.Append("WHERE RT.ResultId=R.ResultID AND RT.TaskId=T.TaskId " & vbCrLf)
            sb.Append("AND RT.ResultTaskId = ? " & vbCrLf)

            db.AddParameter("ResultTaskId", ResultTaskId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            Try
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                Dim info As New ResultTaskInfo
                If dr.Read() Then
                    '   set properties with data from DataReader
                    info.ResultTaskId = ResultTaskId
                    info.IsInDB = True
                    If Not (dr("ResultTaskId") Is System.DBNull.Value) Then info.ResultTaskId = CType(dr("ResultTaskId"), Guid).ToString
                    If Not (dr("ResultId") Is System.DBNull.Value) Then info.ResultId = CType(dr("ResultId"), Guid).ToString
                    If Not (dr("TaskId") Is System.DBNull.Value) Then info.TaskId = CType(dr("TaskId"), Guid).ToString
                    If Not (dr("TaskDescrip") Is System.DBNull.Value) Then info.TaskDescrip = dr("TaskDescrip")
                    If Not (dr("TaskOrder") Is System.DBNull.Value) Then info.TaskOrder = dr("TaskOrder")
                    If Not (dr("StartGap") Is System.DBNull.Value) Then info.StartGap = dr("StartGap")
                    If Not (dr("StartGapUnit") Is System.DBNull.Value) Then info.StartGapUnit = dr("StartGapUnit")
                    If Not (dr("ResultDescrip") Is System.DBNull.Value) Then info.ResultDescrip = dr("ResultDescrip")
                    If Not (dr("ResultActionId") Is System.DBNull.Value) Then info.ResultActionId = dr("ResultActionId")
                    If Not (dr("ResultActionTableName") Is System.DBNull.Value) Then info.ResultActionTableName = dr("ResultActionTableName")
                    If Not (dr("ResultActionColumnName") Is System.DBNull.Value) Then info.ResultActionColumnName = dr("ResultActionColumnName")
                    If Not (dr("ResultActionValue") Is System.DBNull.Value) Then info.ResultActionValue = dr("ResultActionValue")
                    If Not (dr("Active") Is System.DBNull.Value) Then info.Active = CType(dr("Active"), Boolean)
                End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                Return info
            Catch ex As System.Exception
            End Try
            Return Nothing ' Error
        End Function

        ''' <summary>
        ''' Updates the database given a ResultTaskInfo object
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Update(ByVal info As ResultTaskInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an update
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("UPDATE tmResultTasks " & vbCrLf)
                sb.Append("SET " & vbCrLf)
                sb.Append(" ResultId = ?, " & vbCrLf)
                sb.Append(" TaskId = ?, " & vbCrLf)
                sb.Append(" TaskOrder = ?, " & vbCrLf)
                sb.Append(" StartGap = ?, " & vbCrLf)
                sb.Append(" StartGapUnit = ? " & vbCrLf)
                sb.Append(" WHERE ResultTaskId = ? ")

                '   add parameters values to the query                            
                If info.ResultId = "" Or info.ResultId = Guid.Empty.ToString Then
                    db.AddParameter("@ResultId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ResultId", info.ResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.TaskId = "" Or info.TaskId = Guid.Empty.ToString Then
                    db.AddParameter("@TaskId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@TaskId", info.TaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@TaskOrder", info.TaskOrder, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@StartGap", info.StartGap, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@StartGapUnit", info.StartGapUnit, DataAccess.OleDbDataType.OleDbString, 10, ParameterDirection.Input)
                db.AddParameter("@ResultTaskId", info.ResultTaskId, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                ' run the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return False
            End Try
        End Function

        ''' <summary>
        ''' Persists a single ResultTaskInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As ResultTaskInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("INSERT tmResultTasks (ResultTaskId, ResultId, TaskId, TaskOrder, StartGap, StartGapUnit) ")
                sb.Append(" VALUES (?,?,?,?,?,?) ")

                '   add parameters values to the query
                If info.ResultTaskId = "" Or info.ResultTaskId = Guid.Empty.ToString Then
                    db.AddParameter("@ResultTaskId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ResultTaskId", info.ResultTaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.ResultId = "" Or info.ResultId = Guid.Empty.ToString Then
                    db.AddParameter("@ResultId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ResultId", info.ResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.TaskId = "" Or info.TaskId = Guid.Empty.ToString Then
                    db.AddParameter("@TaskId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@TaskId", info.TaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@TaskOrder", info.TaskOrder, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@StartGap", info.StartGap, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@StartGapUnit", info.StartGapUnit, DataAccess.OleDbDataType.OleDbString, 10, ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                '   return without errors
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return False
            End Try
        End Function

        ''' <summary>
        ''' Deletes a ResultTask from the tmResultTasks table given a ResultTaskId
        ''' </summary>
        ''' <param name="ResultTaskId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal ResultTaskId As String) As Boolean
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                '   build the query
                Dim sql As String = "DELETE FROM tmResultTasks WHERE ResultTaskId = ? "

                '   add parameters values to the query
                db.AddParameter("@ResultTaskId", ResultTaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                '   execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                db.CloseConnection()
                '   If the row was not deleted then there was a concurrency problem
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return False
            End Try
        End Function

        ''' <summary>
        ''' Deletes all TaskResult for a specifc Task
        ''' </summary>
        ''' <param name="ResultId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DeleteAllByResultId(ByVal ResultId As String) As Boolean
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                '   build the query
                Dim sql As String = "DELETE FROM tmResultTasks WHERE ResultId = ? "

                '   add parameters values to the query
                db.AddParameter("@ResultId", ResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                '   execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                db.CloseConnection()
                '   If the row was not deleted then there was a concurrency problem
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return False
            End Try
        End Function
    End Class
#End Region

#Region "TaskResults"
    Public Class TaskResultsDB
        ''' <summary>
        ''' Retrieve all the possible results for a given task
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAll(ByVal TaskId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append(" TR.TaskResultId, " & vbCrLf)
            sb.Append(" TR.TaskId, " & vbCrLf)
            sb.Append(" TR.ResultId, " & vbCrLf)
            sb.Append(" R.Descrip, " & vbCrLf)
            sb.Append(" R.ResultActionId, " & vbCrLf)
            sb.Append(" R.ResultActionValue, " & vbCrLf)
            sb.Append(" R.Active " & vbCrLf)
            sb.Append("FROM tmTaskResults TR, tmResults R " & vbCrLf)
            sb.Append("WHERE TR.ResultID = R.ResultID AND Active=1" & vbCrLf)
            If Not TaskId Is Nothing Then
                sb.Append("AND TR.TaskId = ? " & vbCrLf)
                db.AddParameter("TaskId", TaskId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            sb.Append("ORDER BY R.Descrip")

            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Get a TaskResultInfo object given a TaskResultId
        ''' </summary>
        ''' <param name="TaskResultId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal TaskResultId As String) As TaskResultInfo
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append(" TR.TaskResultId, " & vbCrLf)
            sb.Append(" TR.TaskId, " & vbCrLf)
            sb.Append(" TR.ResultId, " & vbCrLf)
            sb.Append(" R.Descrip, " & vbCrLf)
            sb.Append(" R.ResultActionId, " & vbCrLf)
            sb.Append(" R.ResultActionValue, " & vbCrLf)
            sb.Append(" R.Active " & vbCrLf)
            sb.Append("FROM tmTaskResults TR LEFT JOIN tmResults R ON TR.ResultID = R.ResultID " & vbCrLf)
            sb.Append("WHERE TR.TaskResultId = ? " & vbCrLf)
            sb.Append("AND R.Active=1 " & vbCrLf)
            sb.Append("ORDER BY R.Descrip")

            db.AddParameter("TaskResultId", TaskResultId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            Try
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                Dim info As New TaskResultInfo
                If dr.Read() Then
                    '   set properties with data from DataReader
                    info.TaskResultId = TaskResultId
                    info.IsInDB = True
                    If Not (dr("TaskResultId") Is System.DBNull.Value) Then info.TaskResultId = CType(dr("TaskResulId"), Guid).ToString
                    If Not (dr("TaskId") Is System.DBNull.Value) Then info.TaskId = CType(dr("TaskId"), Guid).ToString
                    If Not (dr("Descrip") Is System.DBNull.Value) Then info.Descrip = dr("Descrip")
                    If Not (dr("ResultActionId") Is System.DBNull.Value) Then info.ResultActionId = dr("ResultActionId")
                    If Not (dr("ResultActionValue") Is System.DBNull.Value) Then info.ResultActionValue = dr("ResultActionValue")
                    If Not (dr("Active") Is System.DBNull.Value) Then info.Active = CType(dr("Active"), Boolean)
                End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                Return info
            Catch ex As System.Exception
            End Try
            Return Nothing ' Error
        End Function

        ''' <summary>
        ''' Updates the database given a TaskResultInfo object
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Update(ByVal info As TaskResultInfo, ByVal user As String) As Integer
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an update
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("UPDATE tmTaskResults " & vbCrLf)
                sb.Append("SET " & vbCrLf)
                sb.Append(" TaskId = ?, " & vbCrLf)
                sb.Append(" ResultId = ? " & vbCrLf)
                sb.Append(" WHERE TaskResultId = ? ")

                '   add parameters values to the query                            
                If info.TaskId = "" Or info.TaskId = Guid.Empty.ToString Then
                    db.AddParameter("@TaskId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@TaskId", info.TaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.ResultId = "" Or info.ResultId = Guid.Empty.ToString Then
                    db.AddParameter("@ResultId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ResultId", info.ResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                db.AddParameter("@TaskResultId", info.TaskResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                ' run the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return 0
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.ErrorCode  'DALExceptions.BuildErrorMessage(ex)                
            End Try
        End Function

        ''' <summary>
        ''' Persists a single TaskResultInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As TaskResultInfo, ByVal user As String) As Integer
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("INSERT tmTaskResults (TaskResultId, TaskId, ResultId) ")
                sb.Append(" VALUES (?,?,?) ")

                '   add parameters values to the query
                If info.TaskResultId = "" Or info.TaskResultId = Guid.Empty.ToString Then
                    db.AddParameter("@TaskResultId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@TaskResultId", info.TaskResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.TaskId = "" Or info.TaskId = Guid.Empty.ToString Then
                    db.AddParameter("@TaskId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@TaskId", info.TaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.ResultId = "" Or info.ResultId = Guid.Empty.ToString Then
                    db.AddParameter("@ResultId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ResultId", info.ResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                '   return without errors
                Return 0
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.ErrorCode  'DALExceptions.BuildErrorMessage(ex)                
            End Try
        End Function

        ''' <summary>
        ''' Deletes a taskresult from the tmTaskResults table given a TaskResultId
        ''' </summary>
        ''' <param name="TaskResultId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal TaskResultId As String) As Integer
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                '   build the query
                Dim sql As String = "DELETE FROM tmTaskResults WHERE TaskResultId = ? "

                '   add parameters values to the query
                db.AddParameter("@TaskResultId", TaskResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                '   execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                db.CloseConnection()
                '   If the row was not deleted then there was a concurrency problem
                Return rowCount
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.ErrorCode
            End Try
        End Function

        ''' <summary>
        ''' Deletes all TaskResult for a specifc Task
        ''' </summary>
        ''' <param name="TaskId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DeleteResultsByTask(ByVal TaskId As String) As Integer
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                '   build the query
                Dim sql As String = "DELETE FROM tmTaskResults WHERE TaskId = ? "

                '   add parameters values to the query
                db.AddParameter("@TaskId", TaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                '   execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                db.CloseConnection()
                '   If the row was not deleted then there was a concurrency problem
                Return rowCount
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.ErrorCode
            End Try
        End Function
        Public Shared Function GetTaskIDFromStatusCode(ByVal statuscode As String, Optional ByVal campusId As String = "", Optional ByVal UserId As String = "") As DataTable
            Dim db As New DataAccess
            Dim rtn As DataTable

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sqlStr As String
                Dim dr As OleDbDataReader
                Dim sb As New StringBuilder
                Dim intTaskCount As Integer
                Dim strCategoryId As String
                Dim strAllCampusGroupId As String

                'Modified by balaji on 04/20/2007

                'A new task needs to be created when a new lead is added, but if no tasks are 
                'mapped to the Lead Status, the new lead task is not added and creates issue in
                'new lead page.

                'To solve this issue, if no tasks are mapped to the lead status the a new task
                'with code 'AL-.... is created and mapped to the selected Lead Status.

                'With sb
                '    .Append(" select count(*) from tmTasks where ModuleEntityId=1 and StatusCodeId=? ")
                '    If campusId <> "" Then
                '        .Append(" AND (CampGroupId IN (SELECT CampGrpId ")
                '        .Append("FROM syCmpGrpCmps ")
                '        .Append("WHERE CampusId = '")
                '        .Append(campusId)
                '        .Append("' ")
                '        .Append("AND CampGroupId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                '        .Append("OR CampGroupId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                '    End If
                'End With
                'db.AddParameter("@StatusCodeId", statuscode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'intTaskCount = db.RunParamSQLScalar(sb.ToString)
                'db.ClearParameters()
                'sb.Remove(0, sb.Length)

                'With sb
                '    .Append("select top 1 CategoryId from tmCategories where Descrip='Lead' ")
                'End With
                'strCategoryId = db.RunParamSQLScalar(sb.ToString).ToString
                'db.ClearParameters()
                'sb.Remove(0, sb.Length)

                'With sb
                '    .Append("select top 1 CampGrpId from syCampGrps where CampGrpDescrip='All' ")
                'End With
                'strAllCampusGroupId = db.RunParamSQLScalar(sb.ToString).ToString
                'db.ClearParameters()
                'sb.Remove(0, sb.Length)

                'Dim statusCodeDescrip As String
                'With sb
                '    .Append("select top 1 StatusCodeDescrip from syStatusCodes where StatusCodeId=? ")
                'End With
                'db.AddParameter("@StatusCodeId", statuscode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'statusCodeDescrip = db.RunParamSQLScalar(sb.ToString).ToString
                'db.ClearParameters()
                'sb.Remove(0, sb.Length)

                'If intTaskCount >= 1 Then
                sb.Append("select * from tmTasks where statusCodeid= ? ")
                If campusId <> "" Then
                    sb.Append(" AND (CampGroupId IN (SELECT CampGrpId ")
                    sb.Append("FROM syCmpGrpCmps ")
                    sb.Append("WHERE CampusId = '")
                    sb.Append(campusId)
                    sb.Append("' ")
                    sb.Append("AND CampGroupId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    sb.Append("OR CampGroupId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                End If
                db.AddParameter("@StatusCodeId", statuscode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                dr = db.RunParamSQLDataReader(sb.ToString)
                If (dr.HasRows) Then
                    rtn = New DataTable()
                    rtn.Load(dr)
                End If
                'Else
                'Dim tInfo As New TaskInfo
                'Dim intResult As Integer
                'tInfo.TaskId = Guid.NewGuid.ToString
                'tInfo.ModuleEntityId = 1
                'tInfo.CategoryId = strCategoryId
                'tInfo.StatusId = statuscode
                'tInfo.CampGroupId = strAllCampusGroupId
                'tInfo.Active = True
                'tInfo.Code = "AL-" + Mid(statusCodeDescrip, 1, 4)
                'tInfo.Descrip = "AddLead-" + statusCodeDescrip
                'intResult = TasksDB.Add(tInfo, UserId)

                'sb.Append("select * from tmTasks where statusCodeid= ? ")
                'If campusId <> "" Then
                '    sb.Append(" AND (CampGroupId IN(SELECT CampGrpId ")

                '    sb.Append("FROM syCmpGrpCmps ")

                '    sb.Append("WHERE CampusId = '")
                '    sb.Append(campusId)
                '    sb.Append("' ")
                '    sb.Append("AND CampGroupId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                '    sb.Append("OR CampGroupId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                'End If
                'db.AddParameter("@StatusCodeId", statuscode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'dr = db.RunParamSQLDataReader(sb.ToString)
                'If (dr.HasRows) Then
                '    rtn = New DataTable()
                '    rtn.Load(dr)
                'End If
                'End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Catch ex As Exception

            End Try
            Return rtn
        End Function



#Region "Gets UserID using Roles for sending the tasks for Terminate,Suspend,LOA"

        Public Shared Function GetOwnerIDByRoles() As DataTable


            Dim db As New DataAccess
            Dim rtn As DataTable

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sqlStr As String
                Dim dr As OleDbDataReader
                sqlStr = "SELECT DISTINCT us.UserId FROM syUsers us, syUsersRolesCampGrps rg, syRoles sr "
                sqlStr = sqlStr & " WHERE(us.UserId = rg.UserId)AND rg.RoleId=sr.RoleId AND sr.SysRoleId IN(9,10)"


                dr = db.RunParamSQLDataReader(sqlStr)
                If (dr.HasRows) Then
                    rtn = New DataTable()
                    rtn.Load(dr)
                End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Catch ex As Exception

            End Try
            Return rtn
        End Function


        Public Shared Function GetStudentIDfromEnrollID(ByVal enrollId As String) As String
            Dim db As New DataAccess
            Dim rtn As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sqlStr As String
                Dim dr As OleDbDataReader
                sqlStr = " select StudentId from dbo.arStuEnrollments where StuEnrollID= ? "

                db.AddParameter("@StuEnrollID", enrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                rtn = db.RunParamSQLScalar(sqlStr).ToString
                db.CloseConnection()
            Catch ex As Exception

            End Try
            Return rtn
        End Function
#End Region


    End Class
#End Region

#Region "Tasks"
    Public Class TasksDB
        ''' <summary>
        ''' Retrieve tasks from tmTasks table
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAll(ByVal CampGroupId As String, ByVal ModuleId As String, ByVal EntityId As String, _
                                        ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append(" distinct TM.TaskId as TaskId, " & vbCrLf)
            sb.Append(" TM.Code, " & vbCrLf)
            sb.Append(" TM.Descrip as Descrip, " & vbCrLf)
            sb.Append(" TM.CampGroupId, " & vbCrLf)
            sb.Append(" TM.CategoryId as CategoryId, " & vbCrLf)
            sb.Append(" C.Descrip as CategoryDescrip, " & vbCrLf)
            sb.Append(" TM.ModuleEntityId, " & vbCrLf)
            sb.Append(" M.ResourceId as ModuleId, " & vbCrLf)
            sb.Append(" M.Resource as ModuleName, " & vbCrLf)
            sb.Append(" E.ResourceId as EntityId, " & vbCrLf)
            sb.Append(" E.Resource as EntityName, " & vbCrLf)
            sb.Append(" TM.ModDate, " & vbCrLf)
            sb.Append(" TM.ModUser, " & vbCrLf)
            sb.Append(" TM.Active " & vbCrLf)
            sb.Append("FROM " & vbCrLf)
            sb.Append(" tmTasks TM, tmCategories C, " & vbCrLf)
            sb.Append(" syAdvantageResourceRelations R, syResources E, syResources M " + vbCrLf)

            ' Add the where clause
            sb.Append("WHERE   C.CategoryId=TM.CategoryId and " & vbCrLf)
            sb.Append("        R.ResourceId = E.ResourceId and " + vbCrLf)
            sb.Append("        R.RelatedResourceId = M.ResourceId and " + vbCrLf)
            sb.Append("        E.ResourceTypeId=8 and " + vbCrLf)
            sb.Append("        M.ResourceTypeId=1 and " + vbCrLf)
            sb.Append("	 	   R.ResRelId = TM.ModuleEntityId " + vbCrLf)

            If Not CampGroupId Is Nothing AndAlso CampGroupId <> "" Then
                sb.Append(" AND TM.CampGroupId = ? " & vbCrLf)
                db.AddParameter("@CampusId", CampGroupId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If Not EntityId Is Nothing AndAlso EntityId <> "" Then
                sb.Append(" AND TM.ModuleEntityId = ? " & vbCrLf)
                db.AddParameter("@ModuleEntityId", EntityId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If ShowActive And Not ShowInactive Then
                sb.Append(" AND TM.Active = ? " & vbCrLf)
                db.AddParameter("@Active", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            ElseIf Not ShowActive And ShowInactive Then
                sb.Append(" AND TM.Active = ? " & vbCrLf)
                db.AddParameter("@Active", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If
            sb.Append("ORDER BY TM.Descrip")

            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        Public Shared Function GetFilter(ByVal CampGroupId As String, ByVal CategoryId As String, ByVal ModuleId As String, _
                                        ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append(" distinct TM.TaskId as TaskId, " & vbCrLf)
            sb.Append(" TM.Code, " & vbCrLf)
            sb.Append(" TM.Descrip as Descrip, " & vbCrLf)
            sb.Append(" TM.CampGroupId, " & vbCrLf)
            sb.Append(" TM.CategoryId as CategoryId, " & vbCrLf)
            sb.Append(" C.Descrip as CategoryDescrip, " & vbCrLf)
            sb.Append(" TM.ModuleEntityId, " & vbCrLf)
            sb.Append(" M.ResourceId as ModuleId, " & vbCrLf)
            sb.Append(" M.Resource as ModuleName, " & vbCrLf)
            sb.Append(" E.ResourceId as EntityId, " & vbCrLf)
            sb.Append(" E.Resource as EntityName, " & vbCrLf)
            sb.Append(" TM.ModDate, " & vbCrLf)
            sb.Append(" TM.ModUser, " & vbCrLf)
            sb.Append(" TM.Active " & vbCrLf)
            sb.Append("FROM " & vbCrLf)
            sb.Append(" tmTasks TM, tmCategories C, " & vbCrLf)
            sb.Append(" syAdvantageResourceRelations R, syResources E, syResources M " + vbCrLf)

            ' Add the where clause
            sb.Append("WHERE   C.CategoryId=TM.CategoryId and " & vbCrLf)
            sb.Append("        R.ResourceId = E.ResourceId and " + vbCrLf)
            sb.Append("        R.RelatedResourceId = M.ResourceId and " + vbCrLf)
            sb.Append("        E.ResourceTypeId=8 and " + vbCrLf)
            sb.Append("        M.ResourceTypeId=1 and " + vbCrLf)
            sb.Append("	 	   R.ResRelId = TM.ModuleEntityId " + vbCrLf)

            If Not CampGroupId Is Nothing AndAlso CampGroupId <> "" Then
                sb.Append(" AND TM.CampGroupId = ? " & vbCrLf)
                db.AddParameter("@CampusId", CampGroupId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If Not CategoryId Is Nothing AndAlso CategoryId <> "" Then
                sb.Append(" AND TM.CategoryId = ? " & vbCrLf)
                db.AddParameter("@CategoryId", CategoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If Not ModuleId Is Nothing AndAlso ModuleId <> "" Then
                sb.Append(" AND M.ResourceId = ? " & vbCrLf)
                db.AddParameter("@ModuleId", ModuleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            If ShowActive And Not ShowInactive Then
                sb.Append(" AND TM.Active = ? " & vbCrLf)
                db.AddParameter("@Active", True, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            ElseIf Not ShowActive And ShowInactive Then
                sb.Append(" AND TM.Active = ? " & vbCrLf)
                db.AddParameter("@Active", False, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            End If
            sb.Append("ORDER BY TM.Descrip")

            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieve tasks from tmTasks table for a specific user
        ''' </summary>
        ''' <param name="UserId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAllForUser(ByVal UserId As String, ByVal CampusId As String, ByVal ModuleId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     distinct TM.TaskId as TaskId, " & vbCrLf)
            sb.Append("     TM.Code, " & vbCrLf)
            sb.Append("     TM.Descrip as Descrip, " & vbCrLf)
            sb.Append("     TM.CampGroupId, " & vbCrLf)
            sb.Append("     TM.CategoryId as CategoryId, " & vbCrLf)
            sb.Append("     (select Descrip from tmCategories where CategoryId=TM.CategoryId) as CategoryDescrip, " & vbCrLf)
            sb.Append("     TM.ModuleEntityId, " & vbCrLf)
            sb.Append("     M.ResourceId as ModuleId, " & vbCrLf)
            sb.Append("     M.Resource as ModuleName, " & vbCrLf)
            sb.Append("     E.ResourceId as EntityId, " & vbCrLf)
            sb.Append("     E.Resource as EntityName, " & vbCrLf)
            sb.Append("     TM.ModDate, " & vbCrLf)
            sb.Append("     TM.ModUser, " & vbCrLf)
            sb.Append("     TM.Active " & vbCrLf)
            sb.Append("FROM " & vbCrLf)
            sb.Append("     tmTasks TM, syUsersRolesCampGrps UR, syRolesModules RM, " & vbCrLf)
            sb.Append("     syAdvantageResourceRelations R, syResources E, syResources M " + vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            sb.Append("     RM.RoleId=UR.RoleId AND TM.Active=1 " + vbCrLf)

            ' only add where clause for campus if campus id was provided
            ' BEN: 10/27/06 - add support for "All Campus Group"
            If Not CampusId Is Nothing AndAlso CampusId <> "" Then
                sb.Append("     AND TM.CampGroupId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId = ?)" + vbCrLf)
                sb.Append("     AND UR.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId = ?)" + vbCrLf)
                db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                sb.Append(" AND TM.CampGroupId = UR.CampGrpId " + vbCrLf)
            End If
            sb.Append("     AND RM.ModuleId = M.ResourceId " + vbCrLf)
            sb.Append("     AND R.ResourceId = E.ResourceId " + vbCrLf)
            sb.Append("     AND R.RelatedResourceId = M.ResourceId " + vbCrLf)
            sb.Append("     AND E.ResourceTypeId=8 " + vbCrLf)
            sb.Append("     AND M.ResourceTypeId=1 " + vbCrLf)
            sb.Append("     AND R.ResRelId = TM.ModuleEntityId " + vbCrLf)

            ' add where clause for userid
            sb.Append("     AND UR.UserId = ? " + vbCrLf)
            db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            ' Add where clause for moduleid
            If Not ModuleId Is Nothing AndAlso ModuleId <> "" Then
                sb.Append("     AND M.ResourceId = ? " + vbCrLf)
                db.AddParameter("@ModuleId", ModuleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            sb.Append("ORDER BY TM.Descrip")

            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Get a TaskInfo object given a TaskId
        ''' </summary>
        ''' <param name="TaskId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal TaskId As String) As TaskInfo
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append(" TM.Descrip, " & vbCrLf)
            sb.Append(" TM.CampGroupId, " & vbCrLf)
            sb.Append(" TM.CategoryId, " & vbCrLf)
            sb.Append(" TM.Code, " & vbCrLf)
            sb.Append(" (select Descrip from tmCategories where CategoryId=TM.CategoryId) as CategoryDescrip, " & vbCrLf)
            sb.Append(" TM.ModuleEntityId, " & vbCrLf)
            sb.Append(" M.ResourceId as ModuleId, " & vbCrLf)
            sb.Append(" M.Resource as ModuleName, " & vbCrLf)
            sb.Append(" E.ResourceId as EntityId, " & vbCrLf)
            sb.Append(" E.Resource as EntityName, " & vbCrLf)
            sb.Append(" TM.ModDate, " & vbCrLf)
            sb.Append(" TM.ModUser, " & vbCrLf)
            sb.Append(" TM.Active, " & vbCrLf)
            sb.Append(" TM.StatusCodeId  " & vbCrLf)
            sb.Append("FROM tmTasks TM, syAdvantageResourceRelations R, syResources E, syResources M  " + vbCrLf)
            sb.Append("WHERE TM.TaskId = ? AND ")
            sb.Append(" R.ResourceId = E.ResourceId and " + vbCrLf)
            sb.Append(" R.RelatedResourceId = M.ResourceId and " + vbCrLf)
            sb.Append(" E.ResourceTypeId=8 and " + vbCrLf)
            sb.Append(" M.ResourceTypeId=1 and " + vbCrLf)
            sb.Append(" R.ResRelId = TM.ModuleEntityId " + vbCrLf)


            db.AddParameter("TaskId", TaskId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            Try
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                Dim info As New TaskInfo
                If dr.Read() Then
                    '   set properties with data from DataReader
                    info.TaskId = TaskId
                    info.IsInDB = True
                    If Not (dr("Descrip") Is System.DBNull.Value) Then info.Descrip = dr("Descrip")
                    If Not (dr("Code") Is System.DBNull.Value) Then info.Code = dr("Code")
                    If Not (dr("CampGroupId") Is System.DBNull.Value) Then info.CampGroupId = CType(dr("CampGroupid"), Guid).ToString
                    If Not (dr("CategoryId") Is System.DBNull.Value) Then info.CategoryId = CType(dr("CategoryId"), Guid).ToString
                    If Not (dr("CategoryDescrip") Is System.DBNull.Value) Then info.CategoryDescrip = dr("CategoryDescrip")
                    If Not (dr("ModuleEntityId") Is System.DBNull.Value) Then info.ModuleEntityId = dr("ModuleEntityId").ToString
                    If Not (dr("ModuleId") Is System.DBNull.Value) Then info.ModuleId = CType(dr("ModuleId"), Integer)
                    If Not (dr("ModuleName") Is System.DBNull.Value) Then info.ModuleName = dr("ModuleName")
                    If Not (dr("EntityId") Is System.DBNull.Value) Then info.EntityId = dr("EntityId").ToString
                    If Not (dr("EntityName") Is System.DBNull.Value) Then info.EntityName = dr("EntityName")
                    If Not (dr("ModUser") Is System.DBNull.Value) Then info.ModUser = CType(dr("ModUser"), Guid).ToString
                    If Not (dr("ModDate") Is System.DBNull.Value) Then info.ModDate = dr("ModDate")
                    If Not (dr("Active") Is System.DBNull.Value) Then info.Active = CType(dr("Active"), Boolean)
                    If Not (dr("StatusCodeId") Is System.DBNull.Value) Then info.StatusId = CType(dr("StatusCodeId"), Guid).ToString

                End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                Return info
            Catch ex As System.Exception
            End Try
            Return Nothing ' Error
        End Function

        ''' <summary>
        ''' Updates the database given a TaskInfo object
        ''' </summary>
        ''' <param name="tInfo"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Update(ByVal tInfo As TaskInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an update

            '   build the query
            Dim sb As New StringBuilder
            sb.Append("UPDATE tmTasks " & vbCrLf)
            sb.Append("SET " & vbCrLf)
            sb.Append(" Code = ?, " & vbCrLf)
            sb.Append(" Descrip = ?, " & vbCrLf)
            sb.Append(" CampGroupid = ?, " & vbCrLf)
            sb.Append(" CategoryId = ?, " & vbCrLf)
            sb.Append(" ModuleEntityId = ?, " & vbCrLf)
            sb.Append(" ModUser = ?, ModDate = ?, " & vbCrLf)
            sb.Append(" Active = ?, " & vbCrLf)
            sb.Append(" StatusCodeId = ? " & vbCrLf)
            sb.Append(" WHERE TaskId = ? ")
            Try
                '   add parameters values to the query                            
                db.AddParameter("@Code", tInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Descrip", tInfo.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If tInfo.CampGroupId = "" Or tInfo.CampGroupId = Guid.Empty.ToString Then
                    db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CampGrpId", tInfo.CampGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If tInfo.CategoryId = "" Or tInfo.CategoryId = Guid.Empty.ToString Then
                    db.AddParameter("@CategoryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CategoryId", tInfo.CategoryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If tInfo.ModuleEntityId = "" Then
                    db.AddParameter("@ModuleEntityId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModuleEntityId", tInfo.ModuleEntityId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If user = "" Or user = Guid.Empty.ToString Then
                    db.AddParameter("@ModUser", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Active", tInfo.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                If (tInfo.StatusId = "" Or tInfo.StatusId = Guid.Empty.ToString) Then
                    db.AddParameter("@StatusCodeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@StatusCodeId", tInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                db.AddParameter("@TaskId", tInfo.TaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                ' run the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return False  'DALExceptions.BuildErrorMessage(ex)                
            End Try
        End Function

        ''' <summary>
        ''' Persists a single TaskInfo object to the database
        ''' </summary>
        ''' <param name="tInfo"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal tInfo As TaskInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do an insert
            Try
                '   build the query
                Dim sb As New StringBuilder
                sb.Append("INSERT tmTasks (TaskId, Code, Descrip, CampGroupId, CategoryId, ModuleEntityId, ModDate, ModUser, Active,StatusCodeId) ")
                sb.Append(" VALUES (?,?,?,?,?,?,?,?,?,?) ")

                '   add parameters values to the query
                If tInfo.TaskId = "" Or tInfo.TaskId = Guid.Empty.ToString Then
                    db.AddParameter("@TaskId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@TaskId", tInfo.TaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@Code", tInfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Descrip", tInfo.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If tInfo.CampGroupId = "" Or tInfo.CampGroupId = Guid.Empty.ToString Then
                    db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CampGrpId", tInfo.CampGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If tInfo.CategoryId = "" Or tInfo.CategoryId = Guid.Empty.ToString Then
                    db.AddParameter("@CategoryId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CategoryId", tInfo.CategoryId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If tInfo.ModuleEntityId Is Nothing Or tInfo.ModuleEntityId = "" Then
                    db.AddParameter("@ModuleEntityId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModuleEntityId", tInfo.ModuleEntityId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If user = "" Or user = Guid.Empty.ToString Then
                    db.AddParameter("@ModUser", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@Active", tInfo.Active, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                If (tInfo.StatusId = "" Or tInfo.StatusId = Guid.Empty.ToString) Then
                    db.AddParameter("@StatusCodeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@StatusCodeId", tInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                '   return without errors
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return False  'DALExceptions.BuildErrorMessage(ex)                
            End Try
        End Function

        ''' <summary>
        ''' Deletes a task from the tmTable given a TaskId
        ''' </summary>
        ''' <param name="Id"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal Id As String, ByVal modDate As DateTime) As String
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                ' check if there are already user tasks with this taskid
                Dim sql As String = String.Format("SELECT COUNT(*) from tmUserTasks WHERE TaskId = '{0}'", Id)
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                If rowCount > 0 Then
                    ' there are usertasks with this taskid which means we cannot delete it
                    db.CloseConnection()
                    Return "Task cannot be deleted.  Task has been used in task assignments."
                Else
                    ' no usertasks with this taskid, we can safely delete the task and supporting tables
                    sql = "DELETE tmTaskResults WHERE TaskId = ? " + vbCrLf
                    sql += "DELETE tmResultTasks WHERE TaskId = ? " + vbCrLf
                    sql += "DELETE tmTasks WHERE TaskId = ? AND ModDate = ? " + vbCrLf
                    sql += "SELECT COUNT(*) from tmTasks WHERE TaskId = ? " + vbCrLf

                    db.AddParameter("@Id", Id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@Id", Id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@Id", Id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@Id", Id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    '   execute the query
                    rowCount = db.RunParamSQLScalar(sql)
                    '   If the row was not deleted then there was a concurrency problem
                    If rowCount = 0 Then
                        Return ""   '   return without errors
                    Else
                        Return DALExceptions.BuildConcurrencyExceptionMessage()
                    End If
                End If
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function
    End Class
#End Region

#Region "UserTasks"
    Public Class UserTasksDB
        ''' <summary>
        ''' This is a helper function for the other GetAll__ methods
        ''' This builds the base SELECT statement without any where conditions.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function GetAllSql(ByVal bDistinct As Boolean) As String
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            If bDistinct Then
                sb.Append("     DISTINCT UT.UserTaskId, " & vbCrLf)
            Else
                sb.Append("     UT.UserTaskId, " & vbCrLf)
            End If

            sb.Append("     UT.TaskId, " & vbCrLf)
            sb.Append("     T.Descrip as TaskDescrip, " & vbCrLf)
            sb.Append("     UT.StartDate, " & vbCrLf)
            sb.Append("     UT.EndDate, " & vbCrLf)
            sb.Append("     UT.Priority as Priority, " & vbCrLf)
            sb.Append("     UT.Message, " & vbCrLf)
            sb.Append("     UT.ReId, " & vbCrLf)
            sb.Append("     ((select FirstName + ' ' + LastName from arStudent S where S.StudentID=UT.ReId) union  " + vbCrLf)
            sb.Append("     (select FirstName +' '+ LastName from adLeads where LeadId=UT.ReId) union " + vbCrLf)
            sb.Append("     (select FirstName +' '+ LastName from hrEmployees where EmpId=UT.ReId) union " + vbCrLf)
            sb.Append("     (select EmployerDescrip from plEmployers where EmployerId=UT.ReId) union " + vbCrLf)
            sb.Append("     (select LenderDescrip from faLenders where LenderId=UT.ReId)) as ReName, " + vbCrLf)

            'Get Campus Description Mantis #15217 Change starts here
            ' As Task manager deals with Lead, Student, Employer, Employees, and Lenders Entity
            ' All these data should be combined using union operator to get the campus id
            sb.Append(" ((select Top 1 C.CampDescrip from arStudent S,arStuEnrollments SE,syCampuses C where " + vbCrLf)
            sb.Append(" S.StudentId=SE.StudentId and SE.CampusId=C.CampusId and S.StudentID=UT.ReId) union " + vbCrLf)
            sb.Append(" (select Top 1 C.CampDescrip from adLeads L,syCampuses C where L.CampusId=C.CampusId and L.LeadId=UT.ReId) union " + vbCrLf)
            sb.Append(" (select Top 1 C.CampDescrip from hrEmployees E,syCampuses C where E.CampusId=C.CampusId and E.EmpId=UT.ReId) union " + vbCrLf)
            sb.Append(" (select Top 1 C.CampDescrip from plEmployers E,syCampuses C,syCmpGrpCmps CGC where " + vbCrLf)
            sb.Append(" E.CampGrpId=CGC.CampGrpId and C.CampusId=CGC.CampusId and E.EmployerId=UT.ReId) union " + vbCrLf)
            sb.Append(" (select Top 1 C.CampDescrip from faLenders L,syCampuses C,syCmpGrpCmps CGC where " + vbCrLf)
            sb.Append(" L.CampGrpId=CGC.CampGrpId and C.CampusId=CGC.CampusId and L.LenderId=UT.ReId)) as CampusDescrip, " + vbCrLf)
            'Get Campus Description Mantis #15217 Change Ends Here

            sb.Append("     ((select Phone from arStudentPhone S where S.StudentID=UT.ReId and default1=1) union  " + vbCrLf)
            sb.Append("     (select Phone from adLeads where LeadId=UT.ReId) union " + vbCrLf)
            sb.Append("     (select HomePhone from hrEmpContactInfo where EmpId=UT.ReId) union " + vbCrLf)
            sb.Append("     (select Phone from plEmployers where EmployerId=UT.ReId) union " + vbCrLf)
            sb.Append("     (select CustService from faLenders where LenderId=UT.ReId)) as Phone, " + vbCrLf)
            sb.Append("     ((select HomeEmail from arStudent S where S.StudentID=UT.ReId) union  " + vbCrLf)
            sb.Append("     (select HomeEmail from adLeads where LeadId=UT.ReId) union  " + vbCrLf)
            sb.Append("     (select HomeEmail from hrEmpContactInfo where EmpId=UT.ReId) union  " + vbCrLf)
            sb.Append("     (select Email from plEmployers where EmployerId=UT.ReId) union  " + vbCrLf)
            sb.Append("     (select Email from faLenders where LenderId=UT.ReId)) as Email, " + vbCrLf)
            sb.Append("     UT.OwnerId, " & vbCrLf)
            sb.Append("     (select FullName from syUsers where UserId=UT.OwnerId) as OwnerName," & vbCrLf)
            sb.Append("     UT.AssignedById, " & vbCrLf)
            sb.Append("     (select FullName from syUsers where UserId=UT.AssignedById) as AssignedByName, " & vbCrLf)
            sb.Append("     UT.PrevUserTaskId, " & vbCrLf)
            sb.Append("     (select T2.TaskId from tmUserTasks UT2, tmTasks T2 where UT2.TaskId=T2.TaskId and UT2.UserTaskId=UT.PrevUserTaskId) as PrevTaskId, " + vbCrLf)
            sb.Append("     (select T2.Descrip from tmUserTasks UT2, tmTasks T2 where UT2.TaskId=T2.TaskId and UT2.UserTaskId=UT.PrevUserTaskId) as PrevTaskDescrip, " + vbCrLf)
            sb.Append("     (select UT2.ResultId from tmUserTasks UT2, tmTasks T2 where UT2.TaskId=T2.TaskId and UT2.UserTaskId=UT.PrevUserTaskId) as PrevTaskResultId, " + vbCrLf)
            sb.Append("     (select R.Descrip from tmUserTasks UT2, tmTasks T2, tmResults R where UT2.TaskId=T2.TaskId and UT2.UserTaskId=UT.PrevUserTaskId and UT2.ResultId=R.ResultId) as PrevTaskResultDescrip, " + vbCrLf)
            sb.Append("     UT.ResultId, " & vbCrLf)
            sb.Append("     (select R.Descrip from tmResults R where UT.ResultId = R.ResultId) as ResultDescrip, " & vbCrLf)
            sb.Append("     T.CategoryId, " & vbCrLf)
            sb.Append("     (select Descrip from tmCategories where CategoryId=T.CategoryId) as CategoryDescrip, " & vbCrLf)
            sb.Append("     T.ModuleEntityId, " & vbCrLf)
            sb.Append("     M.ResourceId as ModuleId, " & vbCrLf)
            sb.Append("     M.Resource as ModuleName, " & vbCrLf)
            sb.Append("     E.ResourceId as EntityId, " & vbCrLf)
            sb.Append("     E.Resource as EntityName, " & vbCrLf)
            sb.Append("     UT.Status, " & vbCrLf)
            sb.Append("     UT.ModDate, " & vbCrLf)
            sb.Append("     UT.ModUser, " & vbCrLf)
            sb.Append("     (case when UT.StartDate is null then 1 else 0 end)" + vbCrLf)
            sb.Append("FROM " & vbCrLf)
            sb.Append("     tmUserTasks UT, tmTasks T, syCmpGrpCmps CC,  " & vbCrLf)
            sb.Append("     syAdvantageResourceRelations R, syResources E, syResources M " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     UT.TaskId = T.TaskId AND " + vbCrLf)
            sb.Append("     CC.CampGrpId = T.CampGroupId AND " + vbCrLf)
            sb.Append("     R.ResourceId = E.ResourceId and " + vbCrLf)
            sb.Append("     R.RelatedResourceId = M.ResourceId and " + vbCrLf)
            sb.Append("     E.ResourceTypeId=8 and " + vbCrLf)
            sb.Append("     M.ResourceTypeId=1 and " + vbCrLf)
            sb.Append("     R.ResRelId = T.ModuleEntityId " + vbCrLf)
            Return sb.ToString()
        End Function

        ''' <summary>
        ''' Retrieve all user tasks for the given user and campus
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAll(ByVal CampusId As String, _
                                        ByVal AssignedById As String, _
                                        ByVal OwnerId As String, _
                                        ByVal ReId As String, _
                                        ByVal Priority As String, _
                                        ByVal Status As TaskStatus, _
                                        ByVal DueDateMin As String, _
                                        ByVal DueDateMax As String, _
                                        Optional ByVal FilterOthers As String = "", _
                                        Optional ByVal OwnerIdForAssignedToMe As String = "") As DataSet
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder(GetAllSql(True))

            ' add where for campusid
            If Not CampusId Is Nothing AndAlso CampusId <> "" Then
                sb.Append("AND CC.CampusId = ?" & vbCrLf)
                db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            ' add optional param for OwnerId
            If Not AssignedById Is Nothing AndAlso AssignedById <> "" Then
                sb.Append("AND UT.AssignedById = ?" & vbCrLf)
                db.AddParameter("@AssignedById", AssignedById, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            ' add optional param for OwnerId
            If Not OwnerId Is Nothing AndAlso OwnerId <> "" Then
                sb.Append("AND UT.OwnerId = ?" & vbCrLf)
                db.AddParameter("@OwnerId", OwnerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            ' add optional param for ReId
            If Not ReId Is Nothing AndAlso ReId <> "" Then
                sb.Append("AND UT.ReId = ?" & vbCrLf)
                db.AddParameter("@ReId", ReId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            ' add optional param for Priority
            If Not Priority Is Nothing AndAlso Priority <> "" Then
                sb.Append("AND UT.Priority = ?" & vbCrLf)
                db.AddParameter("@Priority", Priority, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            ' add optional param for Status
            If Status <> TaskStatus.None Then
                sb.Append("AND UT.Status = ?" & vbCrLf)
                db.AddParameter("@Status", Status, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If
            ' add optional param for the min DueDate
            If Not DueDateMin Is Nothing AndAlso DueDateMin <> "" Then
                sb.Append("AND UT.EndDate >= ?" & vbCrLf)
                db.AddParameter("@DueDateMin", DueDateMin, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            ' add optional param for the max DueDate
            If Not DueDateMax Is Nothing AndAlso DueDateMax <> "" Then
                sb.Append("AND UT.EndDate <= ?" & vbCrLf)
                db.AddParameter("@DueDateMax", DueDateMax, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'If Assigned By Me to Others Bring only the task assigned to Others
            If FilterOthers = "others" Then
                sb.Append(" And UT.OwnerID <> UT.AssignedById " & vbCrLf)
                If Not OwnerIdForAssignedToMe Is Nothing AndAlso OwnerIdForAssignedToMe <> "" Then
                    sb.Append("AND UT.OwnerId <> ?" & vbCrLf)
                    db.AddParameter("@OwnerIdForAssignedToMe", OwnerIdForAssignedToMe, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
            End If

            ' add the order by
            sb.Append("ORDER BY (case when UT.StartDate is null then 1 else 0 end), UT.StartDate, T.Descrip")

            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Retrieve all user tasks for the given user and campus
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetAllWorkflows(ByVal CampusId As String, _
                                        ByVal OwnerId As String, _
                                        ByVal ReId As String, _
                                        ByVal Priority As String, _
                                        ByVal Status As TaskStatus, _
                                        ByVal DueDateMin As String, _
                                        ByVal DueDateMax As String) As DataSet
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ' only want ones that are part of a workflow
            Dim sb As New StringBuilder(GetAllSql(True))
            sb.Append("AND UT.PrevUserTaskId is not null " + vbCrLf)

            ' add where for campusid
            If Not CampusId Is Nothing AndAlso CampusId <> "" Then
                sb.Append("AND CC.CampusId = ?" & vbCrLf)
                db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            ' add optional param for OwnerId
            If Not OwnerId Is Nothing AndAlso OwnerId <> "" Then
                sb.Append("AND UT.OwnerId = ?" & vbCrLf)
                db.AddParameter("@OwnerId", OwnerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            ' add optional param for Priority
            If Not Priority Is Nothing AndAlso Priority <> "" Then
                sb.Append("AND UT.Priority = ?" & vbCrLf)
                db.AddParameter("@Priority", Priority, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            ' add optional param for Status
            If Status <> TaskStatus.None Then
                sb.Append("AND UT.Status = ?" & vbCrLf)
                db.AddParameter("@Status", Status, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If
            ' add optional param for the min DueDate
            If Not DueDateMin Is Nothing AndAlso DueDateMin <> "" Then
                sb.Append("AND UT.EndDate >= ?" & vbCrLf)
                db.AddParameter("@DueDateMin", DueDateMin, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            ' add optional param for the max DueDate
            If Not DueDateMax Is Nothing AndAlso DueDateMax <> "" Then
                sb.Append("AND UT.EndDate <= ?" & vbCrLf)
                db.AddParameter("@DueDateMax", DueDateMax, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            ' add the order by
            sb.Append("ORDER BY (case when UT.StartDate is null then 1 else 0 end), UT.StartDate, T.Descrip")

            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        ''' <summary>
        ''' Returns the contact name given an id.
        ''' Id can be a studentid, leadid or lenderid
        ''' </summary>
        ''' <param name="id"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetContactName(ByVal id As String) As String
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder()
            sb.Append("select " + vbCrLf)
            sb.Append("     ((select FirstName + ' ' + LastName from arStudent S where S.StudentID = ?) union  " + vbCrLf)
            sb.Append("     (select FirstName +' '+ LastName from adLeads where LeadId = ?) union " + vbCrLf)
            sb.Append("     (select EmployerDescrip from plEmployers where EmployerId=UT.ReId) union " + vbCrLf)
            sb.Append("     (select LenderDescrip from faLenders where LenderId = ?)) as ReName " + vbCrLf)

            Try
                db.AddParameter("@reid", id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@reid2", id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@reid3", id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@reid4", id, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Return db.RunParamSQLScalar(sb.ToString).ToString()
            Catch ex As System.Exception
            End Try
            Return ""
        End Function

        ''' <summary>
        ''' Get UserTaskInfo give a UserTaskId
        ''' </summary>
        ''' <param name="userTaskId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetInfo(ByVal UserTaskId As String, Optional ByVal strMask As String = "") As UserTaskInfo
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder(GetAllSql(True))
            sb.Append("AND UserTaskId = ?" & vbCrLf)
            db.AddParameter("@UserTaskId", UserTaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ' create the new object 
            Dim info As New UserTaskInfo
            Try
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                If dr.Read() Then
                    '   set properties with data from DataReader
                    info.UserTaskId = UserTaskId
                    info.IsInDB = True
                    If Not (dr("TaskId") Is System.DBNull.Value) Then info.TaskId = CType(dr("TaskId"), Guid).ToString
                    If Not (dr("TaskDescrip") Is System.DBNull.Value) Then info.TaskDescrip = dr("TaskDescrip")
                    If Not (dr("StartDate") Is System.DBNull.Value) Then info.StartDate = dr("StartDate").ToString()
                    If Not (dr("EndDate") Is System.DBNull.Value) Then info.EndDate = dr("EndDate").ToString()
                    If Not (dr("Priority") Is System.DBNull.Value) Then info.Priority = dr("Priority")
                    If Not (dr("Message") Is System.DBNull.Value) Then info.Message = dr("Message")
                    If Not (dr("ReId") Is System.DBNull.Value) Then info.ReId = CType(dr("ReId"), Guid).ToString
                    If Not (dr("ReName") Is System.DBNull.Value) Then info.ReName = dr("ReName")

                    If Not dr("Phone") Is System.DBNull.Value Then
                        Dim strPhone As String
                        Try
                            strPhone = ApplyMask(strMask, dr("Phone"))
                        Catch ex As Exception
                            strPhone = dr("Phone")
                        End Try

                        info.ReName = info.ReName & " - " & strPhone
                        If Not dr("Email") Is System.DBNull.Value Then
                            info.ReName = info.ReName & " - " & dr("Email")
                        End If
                    Else
                        If Not dr("Email") Is System.DBNull.Value Then
                            info.ReName = info.ReName & " - " & dr("Email")
                        End If
                    End If
                    If Not (dr("OwnerId") Is System.DBNull.Value) Then info.OwnerId = CType(dr("OwnerId"), Guid).ToString
                    If Not (dr("OwnerName") Is System.DBNull.Value) Then info.OwnerName = dr("OwnerName")
                    If Not (dr("AssignedById") Is System.DBNull.Value) Then info.AssignedById = CType(dr("AssignedById"), Guid).ToString
                    If Not (dr("AssignedByName") Is System.DBNull.Value) Then info.AssignedByName = dr("AssignedByName")
                    If Not (dr("PrevUserTaskId") Is System.DBNull.Value) Then info.PrevUserTaskId = CType(dr("PrevUserTaskId"), Guid).ToString
                    If Not (dr("ResultId") Is System.DBNull.Value) Then info.ResultId = CType(dr("ResultId"), Guid).ToString
                    If Not (dr("ResultDescrip") Is System.DBNull.Value) Then info.ResultDescrip = dr("ResultDescrip")
                    If Not (dr("CategoryId") Is System.DBNull.Value) Then info.CategoryId = CType(dr("CategoryId"), Guid).ToString
                    If Not (dr("CategoryDescrip") Is System.DBNull.Value) Then info.CategoryDescrip = dr("CategoryDescrip")
                    If Not (dr("Status") Is System.DBNull.Value) Then info.Status = dr("Status")
                    info.PrevStatus = info.Status
                    If Not (dr("ModuleEntityId") Is System.DBNull.Value) Then info.ModuleEntityId = dr("ModuleEntityId").ToString()
                    If Not (dr("ModuleId") Is System.DBNull.Value) Then info.ModuleId = dr("ModuleId")
                    If Not (dr("ModuleName") Is System.DBNull.Value) Then info.ModuleName = dr("ModuleName")
                    If Not (dr("EntityId") Is System.DBNull.Value) Then info.EntityId = dr("EntityId").ToString
                    If Not (dr("EntityName") Is System.DBNull.Value) Then info.EntityName = dr("EntityName")
                    If Not (dr("ModDate") Is System.DBNull.Value) Then info.ModDate = CType(dr("ModDate"), DateTime)
                    If Not (dr("ModUser") Is System.DBNull.Value) Then info.ModUser = CType(dr("ModUser"), Guid).ToString
                End If

                If Not dr.IsClosed Then dr.Close()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

                Return info
            Catch ex As System.Exception
            End Try
            Return Nothing ' Error
        End Function
        Private Shared Function ApplyMask(ByVal strMask As String, ByVal strVal As String) As String
            Dim arrMask As New ArrayList
            Dim arrVal As New ArrayList
            Dim chr As Char
            'Dim strMaskChars As String = "#&?AULH\"
            Dim strMaskChars As String = "#"
            Dim intCounter As Integer
            Dim intCounter2 As Integer
            Dim strCorrVal As String
            Dim strReturn As String = ""
            Dim strPrev As String = ""

            'Add each character in strMask to arrMask
            'Modified by Balaji on 2/18/2005 (If statement added)
            If Not strMask = "" Then
                For Each chr In strMask
                    arrMask.Add(chr.ToString)
                Next
            End If

            'Add each character in strVal to arrVal
            For Each chr In strVal
                arrVal.Add(chr)
            Next

            If arrVal.Count >= 1 Then
                'We need to loop through the arrMask ArrayList and see if each item is a
                'mask character. If it is, we can use intCounter to get the corresponding
                'value from the arrVal ArrayList. Note that we ignore the \ character unless
                'it was preceded by another \. This means that at the end if we have \\ it
                'should be replaced by a \ and if we have a \ then it should be replaced
                'with an empty space.
                For intCounter2 = 0 To arrMask.Count - 1
                    If arrMask(intCounter2).ToString() = "\" And strPrev <> "\" Then
                        'ignore
                    ElseIf strMaskChars.IndexOf(arrMask(intCounter2).ToString()) <> -1 Then
                        strCorrVal = arrVal(intCounter).ToString()
                        arrMask(intCounter2) = strCorrVal
                        intCounter += 1
                    End If
                    strPrev = arrMask(intCounter2).ToString()
                Next

                Dim strChars(arrMask.Count) As String
                arrMask.CopyTo(strChars)
                strReturn = String.Join("", strChars)


                If strReturn.IndexOf("\\") <> -1 Then
                    strReturn = strReturn.Replace("\\", "\")

                ElseIf strReturn.IndexOf("\") <> -1 Then
                    strReturn = strReturn.Replace("\", "")
                Else
                    Return strReturn
                End If

                Return strReturn
            Else
                Return ""
            End If
        End Function
        ''' <summary>
        ''' Updates tmUserTasks with a UserTaskInfo object
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Update(ByVal info As UserTaskInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            Try
                sb.Append("UPDATE tmUserTasks " & vbCrLf)
                sb.Append("SET " & vbCrLf)
                sb.Append(" TaskId = ?, " & vbCrLf)
                sb.Append(" StartDate = ?, " & vbCrLf)
                sb.Append(" EndDate = ?, " & vbCrLf)
                sb.Append(" Priority = ?, " & vbCrLf)
                sb.Append(" Message = ?, " & vbCrLf)
                sb.Append(" ReId = ?, " & vbCrLf)
                sb.Append(" OwnerId = ?, " & vbCrLf)
                sb.Append(" AssignedById = ?, " & vbCrLf)
                sb.Append(" PrevUserTaskId = ?, " & vbCrLf)
                sb.Append(" ResultId = ?, " & vbCrLf)
                sb.Append(" Status = ?, " & vbCrLf)
                sb.Append(" ModDate = ?, " & vbCrLf)
                sb.Append(" ModUser = ? " & vbCrLf)
                sb.Append("WHERE UserTaskId = ? ")

                If info.TaskId = "" Or info.TaskId = Guid.Empty.ToString Then
                    db.AddParameter("@TaskId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("TaskId", info.TaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.StartDate = "" Or info.StartDate Is Nothing Then
                    db.AddParameter("@StartDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@StartDate", info.StartDate, DataAccess.OleDbDataType.OleDbDateTime, 0, ParameterDirection.Input)
                End If
                If info.EndDate = "" Or info.EndDate Is Nothing Then
                    db.AddParameter("@EndDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@EndDate", info.EndDate, DataAccess.OleDbDataType.OleDbDateTime, 0, ParameterDirection.Input)
                End If
                db.AddParameter("@Priority", info.Priority, DataAccess.OleDbDataType.OleDbInteger, 0, ParameterDirection.Input)
                db.AddParameter("@Message", info.Message, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.ReId = "" Or info.ReId = Guid.Empty.ToString Then
                    db.AddParameter("@ReId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ReId", info.ReId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.OwnerId = "" Or info.OwnerId = Guid.Empty.ToString Then
                    db.AddParameter("@OwnerId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@OwnerId", info.OwnerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.AssignedById = "" Or info.AssignedById = Guid.Empty.ToString Then
                    db.AddParameter("@AssigenedById", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@AssigenedById", info.AssignedById, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.PrevUserTaskId = "" Or info.PrevUserTaskId = Guid.Empty.ToString Then
                    db.AddParameter("@PrevUserTaskId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@PrevUserTaskId", info.PrevUserTaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.ResultId = "" Or info.ResultId = Guid.Empty.ToString Then
                    db.AddParameter("@ResultId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ResultId", info.ResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@Status", info.Status, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
                If user = "" Or user = Guid.Empty.ToString Then
                    db.AddParameter("@ModUser", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                db.AddParameter("@UserTaskId", info.UserTaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                ' run the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return False
            End Try
        End Function

        ''' <summary>
        ''' Persists a UserTaskInfo object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Add(ByVal info As UserTaskInfo, ByVal user As String) As Boolean
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder

            Try
                sb.Append("INSERT tmUserTasks (UserTaskId, TaskId, StartDate, EndDate, Priority, Message, ReId, OwnerId, AssignedById, " & vbCrLf)
                sb.Append("                   PrevUserTaskId, ResultId, Status, ModDate, ModUser) " & vbCrLf)
                sb.Append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)" & vbCrLf)

                ' this one is mandatory
                db.AddParameter("@UserTaskId", info.UserTaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@TaskId", info.TaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If info.StartDate = "" Or info.StartDate Is Nothing Then
                    db.AddParameter("@StartDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@StartDate", info.StartDate, DataAccess.OleDbDataType.OleDbDateTime, 0, ParameterDirection.Input)
                End If
                If info.EndDate = "" Or info.EndDate Is Nothing Then
                    db.AddParameter("@EndDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@EndDate", info.EndDate, DataAccess.OleDbDataType.OleDbDateTime, 0, ParameterDirection.Input)
                End If
                db.AddParameter("@Priority", info.Priority, DataAccess.OleDbDataType.OleDbInteger, 0, ParameterDirection.Input)
                db.AddParameter("@Message", info.Message, DataAccess.OleDbDataType.OleDbString, 1024, ParameterDirection.Input)
                If info.ReId = "" Or info.ReId = Guid.Empty.ToString Then
                    db.AddParameter("@ReId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ReId", info.ReId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.OwnerId = "" Or info.OwnerId = Guid.Empty.ToString Then
                    db.AddParameter("@OwnerId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@OwnerId", info.OwnerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.AssignedById = "" Or info.AssignedById = Guid.Empty.ToString Then
                    db.AddParameter("@AssigenedById", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@AssigenedById", info.AssignedById, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.PrevUserTaskId = "" Or info.PrevUserTaskId = Guid.Empty.ToString Then
                    db.AddParameter("@PrevUserTaskId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@PrevUserTaskId", info.PrevUserTaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If info.ResultId = "" Or info.ResultId = Guid.Empty.ToString Then
                    db.AddParameter("@ResultId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ResultId", info.ResultId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@Status", info.Status, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ModDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
                If user = "" Or user = Guid.Empty.ToString Then
                    db.AddParameter("@ModUser", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                ' run the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return True
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return False
            End Try
        End Function

        ''' <summary>
        ''' Deletes a UserTask
        ''' </summary>
        ''' <param name="UserTaskId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Delete(ByVal UserTaskId As String) As Integer
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   do a delete
            Try
                '   build the query
                Dim sql As String = "DELETE FROM tmUserTasks WHERE UserTaskId = ? "
                '   add parameters values to the query
                db.AddParameter("@UserTaskId", UserTaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                '   execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(sql)
                db.CloseConnection()
                '   If the row was not deleted then there was a concurrency problem
                Return rowCount
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.ErrorCode
            End Try
        End Function
    End Class
#End Region

#Region "Workflow"
    ''' <summary>
    ''' DAL for tmWorkflow
    ''' </summary>
    ''' <remarks></remarks>
    Public Class WorkflowDB
        'Public Shared Function GetAll(ByVal cmpId As String) As DataSet
        Public Shared Function GetAll() As DataSet
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT " & vbCrLf)
                .Append(" WorkFlowId, " & vbCrLf)
                .Append(" Name, " & vbCrLf)
                .Append(" Descrip as Description, " & vbCrLf)
                .Append(" CmpGroupId as CampusGroupId, " & vbCrLf)
                .Append(" ModuleEntityId, " & vbCrLf)
                .Append(" StartTaskId, " & vbCrLf)
                .Append(" ModDate, " & vbCrLf)
                .Append(" ModUser, " & vbCrLf)
                .Append(" IsActive " & vbCrLf)
                .Append("FROM " & vbCrLf)
                .Append(" tmWorkflow " & vbCrLf)
                .Append("WHERE " & vbCrLf)
                '                .Append("CmpGroupId = ?" & vbCrLf)
                ' Add the parameter list
                '                db.AddParameter("CmpGroupId", cmpId, FAME.Advantage.DataAccess.DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                ' return the result
                Return db.RunParamSQLDataSet(sb.ToString)
            End With
            Return Nothing
        End Function

        Public Shared Function GetWorkflow(ByRef oObj As WorkflowInfo) As Integer
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("WorkFlowId, ")
                .Append(" Name, ")
                .Append(" Descrip as Description, ")
                .Append(" CmpGroupId as CampusGroupId, ")
                .Append(" ModuleEntityId, ")
                .Append(" StartTaskId, ")
                .Append(" ModDate, ")
                .Append(" ModUser, ")
                .Append(" IsActive ")
                .Append("FROM ")
                .Append(" tmWorkflow")
                .Append("WHERE ")
                .Append("WorkflowId='" + oObj.WorkflowId + "'")
            End With
            Return Nothing
        End Function

        Public Shared Function Delete(ByVal strId As String) As Integer
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM tmWorkflow ")
                .Append("WHERE")
                .Append(" WorkFlowId = ?")
            End With
            db.AddParameter("@WorkFlowId", strId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString())
                db.CloseConnection()
                '   If the row was not deleted then there was a concurrency problem
                Return rowCount
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.ErrorCode
            End Try
        End Function

        Public Shared Function Update(ByVal oObj As WorkflowInfo, ByVal user As String) As Integer
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE tmWorkflow SET " & vbCrLf)
                .Append(" Name = ?," & vbCrLf)
                .Append(" Descrip = ?," & vbCrLf)
                .Append(" CmpGroupId = ?," & vbCrLf)
                .Append(" ModuleEntityId = ?," & vbCrLf)
                .Append(" StartTaskId = ?," & vbCrLf)
                .Append(" ModDate= ?," & vbCrLf)
                .Append(" ModUser= ?," & vbCrLf)
                .Append(" IsActive=?" & vbCrLf)
                .Append(" WHERE ")
                .Append("WorkFlowId= ?" & vbCrLf)
            End With
            Try
                db.AddParameter("@Name", oObj.Name, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Descript", oObj.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If oObj.CampusGroupId = "" Or oObj.CampusGroupId = Guid.Empty.ToString Then
                    db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CampGrpId", oObj.CampusGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If oObj.ModuleEntityId = "" Or oObj.ModuleEntityId = Guid.Empty.ToString Then
                    db.AddParameter("@ModuleEntityId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModuleEntityId", oObj.ModuleEntityId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If oObj.StartTaskId = "" Or oObj.StartTaskId = Guid.Empty.ToString Then
                    db.AddParameter("@StartTaskId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@StartTaskId", oObj.StartTaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@IsActive", oObj.IsActive, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return 0
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.ErrorCode  'DALExceptions.BuildErrorMessage(ex)                
            End Try
        End Function

        Public Shared Function Add(ByVal oObj As WorkflowInfo, ByVal user As String) As Integer
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT tmWorkflow " & vbCrLf)
                .Append("(WorkflowId,Name,Descrip,CmpGroupId," & vbCrLf)
                .Append("ModuleEntityId,StartTaskId,ModDate," & vbCrLf)
                .Append("ModUser,IsActive) VALUES " & vbCrLf)
                .Append("(?,?,?,?,?,?,?,?,?)")
            End With
            Try
                db.AddParameter("@Name", oObj.Name, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Descript", oObj.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If oObj.CampusGroupId = "" Or oObj.CampusGroupId = Guid.Empty.ToString Then
                    db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CampGrpId", oObj.CampusGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If oObj.ModuleEntityId = "" Or oObj.ModuleEntityId = Guid.Empty.ToString Then
                    db.AddParameter("@ModuleEntityId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ModuleEntityId", oObj.ModuleEntityId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If oObj.StartTaskId = "" Or oObj.StartTaskId = Guid.Empty.ToString Then
                    db.AddParameter("@StartTaskId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@StartTaskId", oObj.StartTaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@IsActive", oObj.IsActive, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                '   execute the query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()
                '   return without errors
                Return 0
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.ErrorCode  'DALExceptions.BuildErrorMessage(ex)                
            End Try
            Return 0
        End Function

    End Class
#End Region

#Region "Modules"
    Public Class ModulesDB
        Public Shared Function GetModules(ByVal UserId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ' build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append("			ResourceId as ModuleId, " & vbCrLf)
            sb.Append("			Resource as ModuleName " & vbCrLf)
            sb.Append("FROM		syResources M " & vbCrLf)
            sb.Append("WHERE	M.ResourceTypeId = 1 and " & vbCrLf)
            ' 7/29/06: Ben - Only bring in modules that have more than 1 entity associated with it
            sb.Append("     (select count(*) from syAdvantageResourceRelations R, syResources E " + vbCrLf)
            sb.Append("         where R.ResourceId = E.ResourceId and R.RelatedResourceId = M.ResourceId and E.ResourceTypeId=8) > 0 " + vbCrLf)
            sb.Append("ORDER BY	M.Resource ")

            ' (Future) UserId not used right now
            If Not UserId Is Nothing And UserId <> "" Then
            End If
            ' return the result
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        Public Shared Function GetModuleEntities(ByVal ModuleId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ' build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append("		   R.ResRelId as ModuleEntityId,		" & vbCrLf)
            sb.Append("        E.ResourceId as EntityId," & vbCrLf)
            sb.Append("        E.Resource as Descrip" & vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("        syAdvantageResourceRelations R," + vbCrLf)
            sb.Append("        syResources E," + vbCrLf)
            sb.Append("        syResources M " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("        R.ResourceId = E.ResourceId and " + vbCrLf)
            sb.Append("        R.RelatedResourceId = M.ResourceId and " + vbCrLf)
            sb.Append("        E.ResourceTypeId=8 and " + vbCrLf)
            sb.Append("        M.ResourceTypeId=1 and " + vbCrLf)
            sb.Append("	 	   M.ResourceId = ? " + vbCrLf)
            db.AddParameter("Id", ModuleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
        Public Shared Function GetEntityStatus(ByVal EntityID As String, Optional ByVal campusId As String = "") As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ' build the sql query
            Dim sb As New StringBuilder(1000)

            Dim dsGetCmpGrps As New DataSet
            Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
            Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
            dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusId)
            Dim strCampGrpId As String
            If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
                For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                    strCampGrpId &= row("CampGrpId").ToString & "','"
                Next
                strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
            End If

            With sb
                .Append("   select StatusCodeId,StatusCodeDescrip from SySysStatus a ,SyStatusCodes b ")
                .Append("   where a.SysStatusId = b.SysStatusId And a.StatusLevelID = ? ")
                If EntityID = 1 Then
                    .Append(" and  a.SysStatusDescrip='New Lead' ")
                End If
                .Append("   AND b.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End With
            db.AddParameter("StatusLevelID", EntityID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

    End Class
#End Region

#Region "Campus Groups"
    Public Class CampGroupsDB
        Public Shared Function GetCampusGroups() As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ' build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append(" CampGrpId, " & vbCrLf)
            sb.Append(" CampGrpDescrip " & vbCrLf)
            sb.Append("FROM syCampGrps")
            ' return the result
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function

        Public Shared Function GetCampuses(ByVal CampGroupId As String) As DataSet
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ' build the sql query
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append(" C.CampusId, " & vbCrLf)
            sb.Append(" C.CampDescrip " & vbCrLf)
            sb.Append("FROM syCampuses C, syCmpGrpCmps CC " + vbCrLf)
            sb.Append("WHERE C.CampusId = CC.CampusId " + vbCrLf)
            If Not CampGroupId Is Nothing AndAlso CampGroupId <> "" Then
                sb.Append("AND CC.CampGrpId = ? " + vbCrLf)
                db.AddParameter("@CampGroupId", CampGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            ' return the result
            Return db.RunParamSQLDataSet(sb.ToString)
        End Function
    End Class
#End Region

#Region "Input Masks"
    Public Class InputMasksDB
        Private Function GetSQL(ByVal tableName As String, ByVal zipLen As Integer) As String
            Dim strTemplate As String

            If tableName <> "faLenders" Then
                strTemplate = "SELECT COUNT(*) " & _
                                      "FROM %TableName% " & _
                                      "WHERE LEN(Zip) = %ZipLen% " & _
                                      "AND ForeignZip = 0"

            Else
                strTemplate = "SELECT COUNT(*) " & _
                              "FROM %TableName% " & _
                              "WHERE LEN(PayZip) = %ZipLen% " & _
                              "OR LEN(Zip) = %ZipLen% " & _
                              "AND ForeignAddress = 0 " & _
                              "OR ForeignPayAddress = 0"
            End If

            'Replace the tokens with the values passed in
            strTemplate = strTemplate.Replace("%TableName%", tableName)
            strTemplate = strTemplate.Replace("%ZipLen%", zipLen)

            Return strTemplate

        End Function

        Public Function GetInputMasks() As DataTable
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim ds As New DataSet

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            With sb
                .Append("SELECT InputMaskId, Item, Mask, ModUser, ModDate ")
                .Append("FROM syInputMasks ")
            End With

            ds = db.RunParamSQLDataSet(sb.ToString)

            Return ds.Tables(0)
        End Function

        Public Function GetInputMaskForItem(ByVal itemId As Integer) As String
            Dim db As New DataAccess
            Dim sb As New StringBuilder

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            With sb
                .Append("SELECT Mask ")
                .Append("FROM syInputMasks ")
                .Append("WHERE InputMaskId = ?")
            End With

            db.AddParameter("@itemid", itemId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

            Dim s As String
            s = CType(db.RunParamSQLScalar(sb.ToString), String)

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return s

        End Function

        Public Sub UpdateInputMask(ByVal inputMaskId As Integer, ByVal mask As String)
            Dim db As New DataAccess
            Dim sb As New StringBuilder

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            With sb
                .Append("UPDATE syInputMasks ")
                .Append("SET Mask = ? ")
                .Append("WHERE InputMaskId = ?")
            End With

            db.AddParameter("@mask", mask, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@inputmaskid", inputMaskId, DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        End Sub

        Public Function UpdateInputMaskInfo(ByVal iMaskInfo As InputMaskInfo, ByVal user As String) As String
            Dim db As New DataAccess
            Dim sb As New StringBuilder

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()

            Try
                With sb
                    .Append("UPDATE syInputMasks ")
                    .Append("SET Mask = ?, ModUser = ?, ModDate = ? ")
                    .Append("WHERE InputMaskId = ? ")
                    .Append("AND ModDate = ? ;")
                    .Append("SELECT COUNT(*) FROM syInputMasks WHERE ModDate = ? and InputMaskId = ? ")
                End With

                db.AddParameter("@Mask", iMaskInfo.Mask, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   ModUser
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   ModDate
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   InputMaskId
                db.AddParameter("@InputMaskId", iMaskInfo.InputMaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   Original ModDate
                db.AddParameter("@Original_ModDate", iMaskInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   ModDate
                db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   InputMaskId
                db.AddParameter("@InputMaskId", iMaskInfo.InputMaskId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute the query
                Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

                'db.RunParamSQLExecuteNoneQuery(sb.ToString)
                'db.ClearParameters()

                '   If there were no updated rows then there was a concurrency problem
                If rowCount = 1 Then
                    '   return without errors
                    Return ""
                Else
                    'Return DALExceptions.BuildConcurrencyExceptionMessage()
                End If

            Catch ex As OleDbException
                '   return an error to the client
                'Return DALExceptions.BuildErrorMessage(ex)

            Finally
                'Close Connection
                db.CloseConnection()
            End Try
            Return Nothing
        End Function

        Public Function UpdateInputMaskDS(ByVal ds As DataSet) As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   all updates must be encapsulated as one transaction
            Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
            connection.Open()

            Try
                '   build the sql query for the StudentAwardSchedules data adapter
                Dim sb As New StringBuilder
                With sb
                    .Append("SELECT InputMaskId, Item, Mask, ModUser, ModDate FROM syInputMasks")
                End With

                '   build select command
                Dim InputMaskCommand As New OleDbCommand(sb.ToString, connection)

                '   Create adapter to handle syInputMasks table
                Dim InputMaskDataAdapter As New OleDbDataAdapter(InputMaskCommand)

                '   build insert, update and delete commands for syInputMasks table
                Dim cb As New OleDbCommandBuilder(InputMaskDataAdapter)

                '   insert added rows in syInputMasks table
                InputMaskDataAdapter.Update(ds.Tables("InputMasks").Select(Nothing, Nothing, DataViewRowState.Added))

                '   delete rows in syInputMasks table
                InputMaskDataAdapter.Update(ds.Tables("InputMasks").Select(Nothing, Nothing, DataViewRowState.Deleted))

                '   update rows in syInputMasks table
                InputMaskDataAdapter.Update(ds.Tables("InputMasks").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

                '   return no errors
                Return ""

            Catch ex As OleDb.OleDbException
                '   something went wrong
                '   return error message
                'Return DALExceptions.BuildErrorMessage(ex)

            Catch ex As System.Exception
                '   something went wrong
                '   return error message
                Return ex.Message

            Finally

                '   close connection
                connection.Close()
            End Try
            Return Nothing
        End Function

        Public Function GetCountOfRecordsWithSpecifiedZipLength(ByVal tableName As String, ByVal zipLen As Integer) As Integer
            Dim strSQL As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            strSQL = GetSQL(tableName, zipLen)

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Return CInt(db.RunParamSQLScalar(strSQL))

        End Function

    End Class
#End Region



End Namespace
