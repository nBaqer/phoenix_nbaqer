Imports FAME.Advantage.Common

Public Class RegistrationProcessDB

    Public Function RegisterStudentForCourse(ByVal clsSectionId As String, ByVal stuEnrollId As String, ByVal user As String) As String
        Try
            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@User", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery("usp_AR_RegisterStudentByCourse", Nothing, "SP")
            Return String.Empty
        Catch ex As Exception
            Return "Error registering student for course - " & ex.Message
        End Try
    End Function

    Public Function TransferCompletedComponents(ByVal newClsSectionId As String, ByVal oldClsSectionId As String, _
                                                ByVal stuEnrollId As String, ByVal user As String) As String
        Try
            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.AddParameter("@NewClsSectionId", newClsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@OldClsSectionId", oldClsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@User", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery("usp_AR_TransferCompletedComponentsByCourse", Nothing, "SP")
        Catch ex As Exception
            Return "Error transferring student course components - " & ex.Message
        End Try

    End Function

    Public Function ArchiveStudentCourse(ByVal clsSectionId As String, ByVal stuEnrollId As String, ByVal user As String) As String
        Try
            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@User", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery("usp_AR_ArchiveStudentCourse", Nothing, "SP")
            Return String.Empty
        Catch ex As Exception
            Return "Error archiving student course - " & ex.Message
        End Try
    End Function

    Public Function ArchiveStudentCourseComponents(ByVal clsSectionId As String, ByVal stuEnrollId As String, ByVal user As String) As String
        Try
            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            db.AddParameter("@RClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@User", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery("usp_AR_ArchiveStudentCourseComponents", Nothing, "SP")
            Return String.Empty
        Catch ex As Exception
            Return "Error archiving student course components - " & ex.Message
        End Try
    End Function

End Class

