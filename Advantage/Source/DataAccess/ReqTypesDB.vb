Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class ReqTypesDB
    Public Function GetAllStatuses() As DataSet
        Dim ds As DataSet

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.StatusId,Status ")
                .Append("FROM syStatuses t1 ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetAllCampGrps() As DataSet
        Dim ds As DataSet

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.CampGrpId,CampGrpDescrip ")
                .Append("FROM syCampGrps t1 ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetChildTypes() As DataSet
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append("SELECT t1.ReqTypeId,t1.Descrip ")
            .Append("FROM arReqTypes t1 ")
            .Append("WHERE t1.IsGroup = 0  ")
            .Append("ORDER BY t1.Descrip")
        End With


        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds
    End Function


    Public Function AddReqType(ByVal RequirementType As RequirementTypeInfo) As String

        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            With sb
                .Append("INSERT INTO arReqTypes ")
                .Append("(ReqTypeId, Code, Descrip, StatusId, CampGrpId, TrkGrade, TrkHours, TrkCount, IsGroup, ChildType) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?)")
            End With
            db.AddParameter("@ReqTypeId", Convert.ToInt16(RequirementType.ReqTypeId), DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
            db.AddParameter("@Code", RequirementType.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@Descrip", RequirementType.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StatusId", RequirementType.StatusId, DataAccess.OleDbDataType.OleDbGuid, 50, ParameterDirection.Input)
            db.AddParameter("@CampGrpId", RequirementType.CampGrpId, DataAccess.OleDbDataType.OleDbGuid, 50, ParameterDirection.Input)
            db.AddParameter("@TrkGrade", Convert.ToBoolean(RequirementType.TrkGrade), DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
            db.AddParameter("@TrkHours", Convert.ToBoolean(RequirementType.TrkHours), DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
            db.AddParameter("@TrkCount", Convert.ToBoolean(RequirementType.TrkCount), DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
            db.AddParameter("@IsGroup", Convert.ToBoolean(RequirementType.IsGroup), DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
            db.AddParameter("@ChildType", Convert.ToInt16(RequirementType.ChildType), DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function UpdateReqType(ByVal RequirementType As RequirementTypeInfo) As String
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        'Set the connection string

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            With strSQL
                .Append("UPDATE arReqTypes SET ")
                .Append("Code = ?")
                .Append(",Descrip = ?")
                .Append(",StatusId = ?")
                .Append(",CampGrpId = ?")
                .Append(",TrkGrade = ?")
                .Append(",TrkHours = ?")
                .Append(",TrkCount = ?")
                .Append(",IsGroup = ?")
                .Append(",ChildType = ?")
                .Append(" WHERE ReqTypeId = ? ")
            End With
            db.AddParameter("@Code", RequirementType.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@Descrip", RequirementType.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StatusId", RequirementType.StatusId, DataAccess.OleDbDataType.OleDbGuid, 50, ParameterDirection.Input)
            db.AddParameter("@CampGrpId", RequirementType.CampGrpId, DataAccess.OleDbDataType.OleDbGuid, 50, ParameterDirection.Input)
            db.AddParameter("@TrkGrade", Convert.ToBoolean(RequirementType.TrkGrade), DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
            db.AddParameter("@TrkHours", Convert.ToBoolean(RequirementType.TrkHours), DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
            db.AddParameter("@TrkCount", Convert.ToBoolean(RequirementType.TrkCount), DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
            db.AddParameter("@IsGroup", Convert.ToBoolean(RequirementType.IsGroup), DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
            db.AddParameter("@ChildType", Convert.ToInt16(RequirementType.ChildType), DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)
            db.AddParameter("@ReqTypeId", Convert.ToInt16(RequirementType.ReqTypeId), DataAccess.OleDbDataType.OleDbInteger, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
            db.ClearParameters()
            strSQL.Remove(0, strSQL.Length)
            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Sub DeleteReqType(ByVal ReqTypeId As Integer)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("DELETE FROM arReqTypes WHERE ReqTypeId = ?")
        End With
        db.AddParameter("@ReqTypeId", ReqTypeId, DataAccess.OleDbDataType.OleDbInteger, 16, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub

    Public Function GetReqTypId() As Integer
        Dim ds As DataSet
        Dim maxindex As Integer

        Try
            '   connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder
            Dim obj As New Object

            '   build the sql query
            With sb
                .Append("SELECT MAX(ReqTypeId) ")
                .Append("FROM arReqTypes")
            End With

            '   Execute the query
            maxindex = db.RunParamSQLScalar(sb.ToString)

            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return maxindex

    End Function

    Public Function GetReqTypeInfo(ByVal ReqTypeId As Integer) As DataSet
        Dim ds As New DataSet
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        With sb
            .Append("Select * from arReqTypes")
            .Append(" where ReqTypeId = ? ")
        End With

        db.AddParameter("@ReqTypeId", ReqTypeId, DataAccess.OleDbDataType.OleDbInteger, 16, ParameterDirection.Input)

        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        Return ds
    End Function


End Class
