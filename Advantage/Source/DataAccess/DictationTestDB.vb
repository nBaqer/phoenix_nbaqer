﻿
Imports System.Data
Imports FAME.Advantage.Common

Public Class DictationTestDB
    Public Function GetDictationSpeedTestDetailsForReport(ByVal paramInfo As ReportParamInfo, _
                                                          ByVal ssnMask As String, _
                                                          ByVal phoneMask As String, _
                                                          ByVal zipMask As String) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim db As New SQLDataAccess
        Dim ds As New DataSet
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            db.AddParameter("@termid", paramInfo.TermId, SqlDbType.VarChar, , ParameterDirection.Input)
            db.AddParameter("@testtype", paramInfo.TestType, SqlDbType.VarChar, , ParameterDirection.Input)
            db.AddParameter("@campgrpid", paramInfo.CampGrpId, SqlDbType.VarChar, , ParameterDirection.Input)
            db.AddParameter("@prgverid", paramInfo.PrgVerId, SqlDbType.VarChar, , ParameterDirection.Input)
            db.AddParameter("@statuscodeid", paramInfo.StatusCodeId, SqlDbType.VarChar, , ParameterDirection.Input)
            ''New Code Added By Kamaleah Ahuja On June29, 2010 For Mantis ID 19264
            ''db.AddParameter("@studentid", paramInfo.StudentId, SqlDbType.Decimal, , ParameterDirection.Input)
            db.AddParameter("@studentid", paramInfo.StudentId, SqlDbType.VarChar, , ParameterDirection.Input)
            ''New Code Added By Kamaleah Ahuja On June29, 2010 For Mantis ID 19264
            ''New Code Added By Vijay Ramteke On July 01, 2010 For Mantis Id 19264
            db.AddParameter("@stuenrollid", paramInfo.StuEnrollId, SqlDbType.VarChar, , ParameterDirection.Input)
            ''New Code Added By Vijay Ramteke On July 01, 2010 For Mantis Id 19264
            ds = db.RunParamSQLDataSet_SP("usp_dictationspeedtestreport_getlist", Nothing)
            ds = db.RunParamSQLDataSet_SP("usp_dictationspeedtestreport_getlist", Nothing)

            If (Not paramInfo.Speed Is Nothing AndAlso Not paramInfo.Speed.Trim = "") Or (Not paramInfo.Accuracy Is Nothing AndAlso Not paramInfo.Accuracy.Trim = "") Then 'only when speed or accuracy filter is selected
                Dim drRows() As DataRow
                Dim selectExpression As String

                If Not paramInfo.Speed.Trim = "" Then
                    selectExpression = paramInfo.Speed
                End If

                If Not paramInfo.Accuracy.Trim = "" Then
                    If Not paramInfo.Speed.Trim = "" Then
                        selectExpression &= " and "
                    End If
                    selectExpression &= paramInfo.Accuracy
                End If

                drRows = ds.Tables(0).Select(selectExpression)


                Dim dtScores As DataTable
                Dim dsScores As New DataSet
                Dim drScore As DataRow
                dtScores = ds.Tables(0).Clone
                For Each dr As DataRow In drRows
                    drScore = dtScores.NewRow
                    If Not dr("PhoneNumber") Is System.DBNull.Value AndAlso (dr("ForeignPhone") = 0 Or dr("ForeignPhone") = False) Then
                        drScore("PhoneNumber") = ApplyMask(phoneMask, dr("PhoneNumber"))
                    End If
                    If Not dr("Zip") Is System.DBNull.Value AndAlso (dr("ForeignZip") = 0 Or dr("ForeignZip") = False) Then
                        drScore("Zip") = ApplyMask(zipMask, dr("Zip"))
                    End If
                    If MyAdvAppSettings.AppSettings("StudentIdentifier").ToString.ToLower = "ssn" AndAlso Not dr("SSN") Is System.DBNull.Value Then
                        drScore("SSN") = "***-**-" & Mid(dr("SSN"), 6, 4)
                    ElseIf MyAdvAppSettings.AppSettings("StudentIdentifier").ToString.ToLower = "studentid" AndAlso _
                        Not dr("StudentNumber") Is System.DBNull.Value Then
                        drScore("SSN") = dr("StudentNumber").ToString
                    ElseIf MyAdvAppSettings.AppSettings("StudentIdentifier").ToString.ToLower = "enrollmentid" AndAlso _
                        Not dr("enrollmentid") Is System.DBNull.Value Then
                        drScore("SSN") = dr("enrollmentid").ToString
                    End If
                    drScore("TermDescrip") = dr("TermDescrip")
                    drScore("StartDate") = dr("StartDate")
                    drScore("EndDate") = dr("EndDate")
                    drScore("StudentId") = dr("StudentId")
                    drScore("FirstName") = dr("FirstName")
                    drScore("LastName") = dr("LastName")
                    drScore("MiddleName") = dr("MiddleName")
                    drScore("StudentNumber") = dr("StudentNumber")
                    drScore("TermDescrip") = dr("TermDescrip")
                    drScore("StartDate") = dr("StartDate")
                    drScore("EndDate") = dr("EndDate")
                    drScore("StudentId") = dr("StudentId")
                    drScore("EnrollmentId") = dr("EnrollmentId")
                    drScore("Descrip") = dr("Descrip")
                    drScore("Speed") = dr("Speed")
                    drScore("Grade") = dr("Grade")
                    drScore("Accuracy") = dr("Accuracy")
                    drScore("DatePassed") = dr("DatePassed")
                    drScore("InstructorName") = dr("InstructorName")
                    drScore("PrgVerDescrip") = dr("PrgVerDescrip")
                    drScore("campusdescrip") = dr("campusdescrip")
                    drScore("campgrpdescrip") = dr("campgrpdescrip")
                    drScore("StudentName") = dr("StudentName")
                    drScore("StudentIdentifier") = dr("StudentIdentifier")
                    drScore("StudentIdentifierCaption") = dr("StudentIdentifierCaption")
                    drScore("Address1") = dr("Address1")

                    Dim strCityStateZip As String = ""
                    If Not dr("City") Is System.DBNull.Value AndAlso Not dr("City") = "" Then
                        strCityStateZip = dr("City")
                    End If
                    If Not dr("StateDescrip") Is System.DBNull.Value AndAlso Not dr("StateDescrip") = "" Then
                        If Not dr("City") Is System.DBNull.Value AndAlso Not dr("City") = "" Then
                            strCityStateZip &= " ,"
                        End If
                        strCityStateZip &= dr("StateDescrip")
                    End If
                    If Not dr("Zip") Is System.DBNull.Value AndAlso Not dr("Zip") = "" Then
                        If Not dr("StateDescrip") Is System.DBNull.Value AndAlso Not dr("StateDescrip") = "" Then
                            strCityStateZip &= " ,"
                        End If
                        strCityStateZip &= dr("Zip")
                    End If

                    'The City field will hold the city, state and address info
                    drScore("City") = strCityStateZip 'dr("City")
                    drScore("StateDescrip") = dr("StateDescrip")
                    drScore("PhoneNumber") = dr("PhoneNumber")
                    drScore("Zip") = dr("Zip")
                    drScore("ForeignPhone") = dr("ForeignPhone")
                    drScore("ForeignZip") = dr("ForeignZip")
                    drScore("GrdComponentTypeId") = dr("GrdComponentTypeId")
                    ' New Code Added By Vijay Ramteke On May 28, 2010
                    drScore("istestmentorproctored") = dr("istestmentorproctored")
                    ' New Code Added By Vijay Ramteke On May 28, 2010
                    dtScores.Rows.Add(drScore)
                Next
                ds = Nothing
                dsScores.Tables.Add(dtScores)
                Return dsScores
            Else
                For Each dr As DataRow In ds.Tables(0).Rows
                    If Not dr("PhoneNumber") Is System.DBNull.Value AndAlso (dr("ForeignPhone") = 0 Or dr("ForeignPhone") = False) Then
                        dr("PhoneNumber") = ApplyMask(phoneMask, dr("PhoneNumber"))
                    End If
                    If Not dr("Zip") Is System.DBNull.Value AndAlso (dr("ForeignZip") = 0 Or dr("ForeignZip") = False) Then
                        dr("Zip") = ApplyMask(zipMask, dr("Zip"))
                    End If
                    If MyAdvAppSettings.AppSettings("StudentIdentifier").ToString.ToLower = "ssn" AndAlso Not dr("SSN") Is System.DBNull.Value Then
                        dr("SSN") = "***-**-" & Mid(dr("SSN"), 6, 4)
                    ElseIf MyAdvAppSettings.AppSettings("StudentIdentifier").ToString.ToLower = "studentid" AndAlso Not dr("StudentNumber") Is System.DBNull.Value Then
                        dr("SSN") = dr("StudentNumber").ToString
                    ElseIf MyAdvAppSettings.AppSettings("StudentIdentifier").ToString.ToLower = "enrollmentid" AndAlso Not dr("enrollmentid") Is System.DBNull.Value Then
                        dr("SSN") = dr("enrollmentid").ToString
                    End If
                    Dim strCityStateZip As String = ""
                    If Not dr("City") Is System.DBNull.Value AndAlso Not dr("City") = "" Then
                        strCityStateZip = dr("City")
                    End If
                    If Not dr("StateDescrip") Is System.DBNull.Value AndAlso Not dr("StateDescrip") = "" Then
                        If Not dr("City") Is System.DBNull.Value AndAlso Not dr("City") = "" Then
                            strCityStateZip &= " ,"
                        End If
                        strCityStateZip &= dr("StateDescrip")
                    End If
                    If Not dr("Zip") Is System.DBNull.Value AndAlso Not dr("Zip") = "" Then
                        If Not dr("StateDescrip") Is System.DBNull.Value AndAlso Not dr("StateDescrip") = "" Then
                            strCityStateZip &= " ,"
                        End If
                        strCityStateZip &= dr("Zip")
                    End If
                    'The City field will hold the city, state and address info
                    dr("City") = strCityStateZip
                Next
                Return ds
            End If
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Function ApplyMask(ByVal strMask As String, ByVal strVal As String) As String
        Dim arrMask As New ArrayList
        Dim arrVal As New ArrayList
        Dim chr As Char
        'Dim strMaskChars As String = "#&?AULH\"
        Dim strMaskChars As String = "#"
        Dim intCounter As Integer
        Dim intCounter2 As Integer
        Dim strCorrVal As String
        Dim strReturn As String = ""
        Dim strPrev As String

        'Add each character in strMask to arrMask
        'Modified by Balaji on 2/18/2005 (If statement added)
        If Not strMask = "" Then
            For Each chr In strMask
                arrMask.Add(chr.ToString)
            Next
        End If

        'Add each character in strVal to arrVal
        For Each chr In strVal
            arrVal.Add(chr)
        Next

        If arrVal.Count >= 1 Then
            'We need to loop through the arrMask ArrayList and see if each item is a
            'mask character. If it is, we can use intCounter to get the corresponding
            'value from the arrVal ArrayList. Note that we ignore the \ character unless
            'it was preceded by another \. This means that at the end if we have \\ it
            'should be replaced by a \ and if we have a \ then it should be replaced
            'with an empty space.
            For intCounter2 = 0 To arrMask.Count - 1
                If arrMask(intCounter2).ToString() = "\" And strPrev <> "\" Then
                    'ignore
                ElseIf strMaskChars.IndexOf(arrMask(intCounter2).ToString()) <> -1 Then
                    strCorrVal = arrVal(intCounter).ToString()
                    arrMask(intCounter2) = strCorrVal
                    intCounter += 1
                End If
                strPrev = arrMask(intCounter2).ToString()
            Next

            Dim strChars(arrMask.Count) As String
            arrMask.CopyTo(strChars)
            strReturn = String.Join("", strChars)


            If strReturn.IndexOf("\\") <> -1 Then
                strReturn = strReturn.Replace("\\", "\")

            ElseIf strReturn.IndexOf("\") <> -1 Then
                strReturn = strReturn.Replace("\", "")
            Else
                Return strReturn
            End If

            Return strReturn
        Else
            Return ""
        End If
    End Function

    Public Function IsStudentRegisteredInDictationTestCourse(ByVal StudentId As String) As Boolean
        Dim db As New SQLDataAccess
        Dim ds As New DataSet
        Dim intRowCount As Integer
        Dim boolIsStudentRegistered As Boolean = False

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@StudentId", New Guid(StudentId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            intRowCount = db.RunParamSQLScalar_SP("usp_StudentRegisteredInDictationTestCourse_getcount")
            If intRowCount >= 1 Then
                boolIsStudentRegistered = True
            Else
                boolIsStudentRegistered = False
            End If
            Return boolIsStudentRegistered
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Create Procedure usp_StudentRegisteredInDictationTestCourse_getcount
        '@studentid uniqueidentifier
        'as
        'select Count(*) as isRegistered
        'from 
        '	arStudent t1 inner join arStuEnrollments t2 on t1.StudentId = t2.StudentId 
        '	inner join arResults t3 on t2.StuEnrollId = t3.StuEnrollId 
        '	inner join arClassSections t4 on t3.TestId = t4.ClsSectionId 
        '	inner join arBridge_GradeComponentTypes_Courses t5 on t4.ReqId=t5.ReqId
        'where 
        '	t1.studentid = @studentid
        'go
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Function

    Public Function IsStudentRegisteredInDictationTestCourseByEnrollment(ByVal StudentId As String, _
                                                                         ByVal StuEnrollId As String _
                                                                         ) As Boolean
        Dim db As New SQLDataAccess
        Dim ds As New DataSet
        Dim intRowCount As Integer
        Dim boolIsStudentRegistered As Boolean = False

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@StudentId", New Guid(StudentId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            intRowCount = db.RunParamSQLScalar_SP("usp_StudentRegisteredInDictationTestCourseByEnrollment_getcount")
            If intRowCount >= 1 Then
                boolIsStudentRegistered = True
            Else
                boolIsStudentRegistered = False
            End If
            Return boolIsStudentRegistered
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Create Procedure usp_StudentRegisteredInDictationTestCourseByEnrollment_getcount
        '@studentid uniqueidentifier,
        '@stuenrollid uniqueidentifier
        'as
        'select Count(*) as isRegistered
        'from 
        '	arStudent t1 inner join arStuEnrollments t2 on t1.StudentId = t2.StudentId 
        '	inner join arResults t3 on t2.StuEnrollId = t3.StuEnrollId 
        '	inner join arClassSections t4 on t3.TestId = t4.ClsSectionId 
        '	inner join arBridge_GradeComponentTypes_Courses t5 on t4.ReqId=t5.ReqId
        'where 
        '	t1.studentid = @studentid and t2.stuenrollid=@stuenrollid
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Function




End Class
