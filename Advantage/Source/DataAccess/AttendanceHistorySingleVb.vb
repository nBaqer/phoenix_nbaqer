Imports FAME.Advantage.Common

Public Class AttendanceHistorySingleVb


#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")

#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region


#Region "Public Methods"

    Public Function GetAttendanceHistory(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String = ""
        Dim strOrderBy As String = ""
        Dim strFrom As String = ""
        Dim strStudentId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        ''"arStuEnrollments.StuEnrollId = '207c8d2d-daec-48a7-9a16-9935e010569c' AND arStudentClockAttendance.recorddate BETWEEN '02/01/2009' AND '03/01/2009 11:59:59 PM' "

        Try



            Dim str1 As String()
            str1 = Split(paramInfo.FilterOther, "AND")
            strWhere &= " AND " & str1(0)

            'If paramInfo.FilterList <> "" Then
            '    strWhere &= " AND " & paramInfo.FilterList
            'End If

            'If paramInfo.FilterOther <> "" Then
            '    strWhere &= " AND " & paramInfo.FilterOther
            'End If

            'If paramInfo.OrderBy <> "" Then
            '    strOrderBy &= "," & paramInfo.OrderBy
            'End If

            If StudentIdentifier = "SSN" Then
                strStudentId = "arStudent.SSN AS StudentIdentifier,"
            ElseIf StudentIdentifier = "EnrollmentId" Then
                strStudentId = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
            ElseIf StudentIdentifier = "StudentId" Then
                strStudentId = "arStudent.StudentNumber AS StudentIdentifier,"
            End If

            Dim FilterOtherParam As String
            FilterOtherParam = paramInfo.FilterOtherString
            Dim dates As String()
            Dim Dates1 As String
            Dim FromAndToDate As String()

            dates = Split(FilterOtherParam, "Between")
            Dates1 = dates(1).Substring(dates(1).IndexOf("(") + 1, dates(1).Length - 4)
            FromAndToDate = Split(Dates1, "AND")


            'Dim date1 As String
            'date1 = FilterOtherParam.Substring(FilterOtherParam.IndexOf("(") + 1, (FilterOtherParam.IndexOf(")") - FilterOtherParam.IndexOf("(")) - 1)
            Dim FromDate As Date
            Dim ToDate As Date
            'FromDate = CDate(date1.Substring(0, (date1.IndexOf("A")) - 1))
            'ToDate = CDate(date1.Substring(date1.IndexOf("D") + 1, (date1.Length - date1.IndexOf("D") - 1)))

            FromDate = CDate(FromAndToDate(0))
            ToDate = CDate(FromAndToDate(1))

            Dim strFromDate As String
            Dim strToDate As String
            FromDate = New Date(FromDate.Year, FromDate.Month, 1)
            strFromDate = Format(FromDate, "MM/dd/yyyy")
            ToDate = ToDate.AddMonths(1)
            ToDate = New Date(ToDate.Year, ToDate.Month, 1)
            ToDate = ToDate.AddDays(-1)
            strToDate = Format(ToDate, "MM/dd/yyyy")

            If MyAdvAppSettings.AppSettings("TrackSapAttendance", paramInfo.CampusId).ToString.ToLower = "byday" Then



                With sb

                    .Append(" Select arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,SSC.StatusCodeDescrip,")
                    .Append(strStudentId)
                    .Append(" B.StuENrollId,Convert(varchar,RecordDate,101) RecordDate,SchedHours,ActualHours,IsNull((Select (ActualHours- SchedHours) from  ")
                    .Append(" arStudentClockAttendance A where A.StuEnrollId=B.StuEnrollId and A.Scheduleid=B.ScheduleId and   A.RecordDate=B.RecordDate and  ")
                    .Append(" ActualHours<>9999.0 and ActualHours<>999.0 and SchedHours is not null and SchedHours<ActualHours ),0.00)MakeUpHours, ")
                    .Append(" IsNull((Select (SchedHours- ActualHours) from  ")
                    .Append(" arStudentClockAttendance A where A.StuEnrollId=B.StuEnrollId and A.Scheduleid=B.ScheduleId and   A.RecordDate=B.RecordDate and  ")
                    .Append(" ActualHours<>9999.0 and ActualHours<>999.0 and SchedHours is not null and SchedHours>ActualHours ),0.00)AbsentHours, ")
                    .Append(" isTardy, (dbo.GetLDA(B.StuEnrollId)) LDA, ")
                    .Append(" '' AS ClassSection ")
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    ''Commented by Saraswathi lakshmanan on jan 27 2010
                    ' .Append(" from arStudentCLockAttendance B,arStudent,arStuEnrollments,arStudentSchedules,SyStatusCodes SSC ")

                    .Append(" from arStudentCLockAttendance B,arStudent,arStuEnrollments,SyStatusCodes SSC ")

                    ''   .Append("   , syCmpGrpCmps E,syCampuses F,syCampGrps ")

                    .Append(" where RecordDate>='" & strFromDate & "' ")
                    .Append(" and RecordDate<='" & strToDate & "' ")
                    .Append(" and SchedHours is not null and ActualHours <>999.0 and ActualHours<>9999.0 ")
                    .Append(" and arStudent.StudentId=arStuEnrollments.StudentId and arStuEnrollments.StuEnrollId=B.StuEnrollId  ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed


                    ' .Append(" and arStuEnrollments.StuEnrollId=arStudentSchedules.StuEnrollId and arStudentSchedules.ScheduleId=B.ScheduleID ")
                    '.Append(" and arStudentSchedules.Active=1 and SSC.StatusCodeId=arStuEnrollments.StatusCodeId ")
                    .Append(" and SSC.StatusCodeId=arStuEnrollments.StatusCodeId ")


                    ''  .Append("  AND arStuEnrollments.CampusId=F.CampusId  ")
                    '' .Append("   AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId  ")
                    .Append(strWhere)


                    .Append(" ;Select StuEnrollId,StartDate,EndDate,ISNULL(LOAReturnDate, DateAdd(DAY,1,EndDate)) as LOAReturnDate from arStudentLOAs ;")
                    .Append(" Select StuEnrollId,StartDate,EndDate from arStdSuspensions ;")
                    .Append(" SELECT  DISTINCT    CCT.HolidayCode,  CCT.HolidayStartDate StartDate, ")
                    .Append("     CCT.HolidayEndDate EndDate, ")
                    .Append(" CCT.HolidayDescrip ")
                    .Append("  , CCT.AllDay, (SELECT TimeIntervalDescrip FROM dbo.cmTimeInterval WHERE TimeIntervalId=StartTimeId)StartTime,(SELECT TimeIntervalDescrip FROM dbo.cmTimeInterval WHERE TimeIntervalId=cct.EndTimeId)EndTime  ")
                    .Append(" FROM     syHolidays CCT, syStatuses ST ")
                    .Append(" WHERE    CCT.StatusId = ST.StatusId  and ST.Status='Active'")
                    ''Added by Saraswathi Lakshmanan on MArxh 3 rd 2010
                    .Append(" and CCT.CampGrpId in (Select syCmpGrpCmps.CampGrpId  from  arStuEnrollments,syCmpGrpCmps where  (syCmpGrpCmps.CampusId=arStuEnrollments.CampusId   ")
                    .Append(strWhere)
                    .Append("  )) OR CCT.CampGrpId IN (SELECT CampGrpId FROM dbo.syCampGrps WHERE CampGrpCode LIKE 'All') ")
                    .Append(" ;Select  Distinct arStudentClockAttendance.StuEnrollId ")
                    ''Added by Saraswathi to find the unit type (hours or Days)
                    .Append(" ,PV.UnitTypeId ")

                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    ''Commented by Saraswathi lakshmanan on jan 27 2010
                    '.Append("  from arStudentClockAttendance,arStudentSchedules ")
                    .Append("  from arStudentClockAttendance ")

                    .Append(" ,arStuEnrollments ")
                    .Append(" , arPrgVersions PV ")

                    .Append(" where RecordDate>='" & strFromDate & "'")
                    .Append(" and RecordDate<='" & strToDate & "'")
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    ''Commented by Saraswathi lakshmanan on jan 27 2010
                    '.Append(" and arStudentSchedules.ScheduleId=arStudentClockAttendance.ScheduleID")
                    '.Append(" and arStudentClockAttendance.StuEnrollId=arStudentSchedules.StuEnrollId")
                    .Append(" and  arStuEnrollments.StuEnrollid=arStudentClockAttendance.StuEnrollId ")
                    ' .Append(" and arStudentSchedules.Active=1 ")
                    .Append(" and SchedHours is not null and ActualHours <>999.0 and ActualHours<>9999.0")

                    .Append(" and arStuEnrollments.PrgVerId =PV.PrgVerId")

                    ''  .Append("  AND arStuEnrollments.CampusId=F.CampusId  ")
                    '' .Append("   AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId  ")
                    .Append(strWhere)

                    .Append(" ;Select  arStudentClockAttendance.StuEnrollId, ")
                    .Append(Guid.Empty.ToString())
                    .Append(" as ScheduleId,")
                    .Append(" isNull(sum(SchedHours),0.00) SchedHours,(isNull(Sum(ActualHours),0.00) -  dbo.GetAbsentHoursFromTardyForByDay(arStudentClockAttendance.StuEnrollid,'" & strFromDate & "')) ActualHours")
                    .Append(" ,(isNull((Select Sum(SchedHours - ActualHours) from arStudentClockAttendance A  where  A.StuEnrollId=arStudentClockAttendance.StuEnrollid ")
                    .Append(" and  A.RecordDate<'" & strFromDate & "'")
                    .Append(" and ActualHours<>9999.0 and ActualHours<>999.0 and ")
                    .Append(" SchedHours is not null and SchedHours>ActualHours ),0.00) +  dbo.GetAbsentHoursFromTardyForByDay(arStudentClockAttendance.StuEnrollid,'" & strFromDate & "')) AbsentHours")
                    .Append(" ,isNull((Select Sum(Actualhours - SchedHours) from arStudentClockAttendance A  where  A.StuEnrollId=arStudentClockAttendance.StuEnrollid ")
                    .Append(" and   A.RecordDate<'" & strFromDate & "'")
                    .Append(" and ActualHours<>9999.0 and ActualHours<>999.0 and ")
                    .Append(" SchedHours is not null and SchedHours<ActualHours ),0.00)MakeUpHours,")
                    .Append(" dbo.GetAbsentHoursFromTardyForByDay(arStudentClockAttendance.StuEnrollid,'" & strFromDate & "') AbsentHrsFromTardy ")
                    '.Append(" from arStudentClockAttendance,arStudentSchedules ")
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    ''Commented by Saraswathi lakshmanan on jan 27 2010
                    .Append(" from arStudentClockAttendance ")

                    .Append(" ,arStuEnrollments ")
                    '' .Append("   , syCmpGrpCmps E,syCampuses F,syCampGrps ")

                    .Append(" where RecordDate<'" & strFromDate & "'")
                    ' .Append(" and arStudentSchedules.ScheduleId=arStudentClockAttendance.ScheduleID")
                    ' .Append(" and arStudentClockAttendance.StuEnrollId=arStudentSchedules.StuEnrollId")
                    .Append(" and  arStuEnrollments.StuEnrollid=arStudentClockAttendance.StuEnrollId ")
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    ''Commented by Saraswathi lakshmanan on jan 27 2010
                    '.Append(" and arStudentSchedules.Active=1 ")
                    .Append(" and SchedHours is not null and ActualHours <>999.0 and ActualHours<>9999.0")

                    ''.Append("  AND arStuEnrollments.CampusId=F.CampusId  ")
                    ''.Append("   AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId  ")
                    .Append(strWhere)

                    .Append(" group by arStudentClockAttendance.StuEnrollid")

                    .Append(" ;Select ")
                    .Append(" B.StuENrollId,DATEADD(dd, 0, DATEDIFF(dd, 0, RecordDate))RecordDate,SchedHours,ActualHours,  ")
                    .Append(" isTardy ,PV.TrackTardies, PV.TardiesMakingAbsence, 0 AS MarkAbsent  ")
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed
                    ''Commented by Saraswathi lakshmanan on jan 27 2010
                    ' .Append(" from arStudentCLockAttendance B,arStudent,arStuEnrollments,arStudentSchedules,SyStatusCodes SSC ")

                    .Append(" from arStudentCLockAttendance B,arStudent,arStuEnrollments,arPrgVersions PV,SyStatusCodes SSC ")

                    ''   .Append("   , syCmpGrpCmps E,syCampuses F,syCampGrps ")

                    .Append(" where  PV.PrgVerId=dbo.arStuEnrollments.PrgVerId      AND isTardy=1 AND TrackTardies=1  AND TardiesMakingAbsence>0    ")
                    .Append(" and SchedHours is not null and ActualHours <>999.0 and ActualHours<>9999.0 ")
                    .Append(" and arStudent.StudentId=arStuEnrollments.StudentId and arStuEnrollments.StuEnrollId=B.StuEnrollId  ")
                    ''Commented  by Saraswathi lakshmanan on Jan 27 2010
                    ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
                    ''The attendance details related to the previous schedules are also displayed


                    ' .Append(" and arStuEnrollments.StuEnrollId=arStudentSchedules.StuEnrollId and arStudentSchedules.ScheduleId=B.ScheduleID ")
                    '.Append(" and arStudentSchedules.Active=1 and SSC.StatusCodeId=arStuEnrollments.StatusCodeId ")
                    .Append(" and SSC.StatusCodeId=arStuEnrollments.StatusCodeId ")


                    ''  .Append("  AND arStuEnrollments.CampusId=F.CampusId  ")
                    '' .Append("   AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId  ")
                    .Append(strWhere)

                    ''To check if we have to show the instruction type or not on the report
                    .Append("; SELECT COUNT(InstructionTypeId)  FROM dbo.arInstructionType; ")
                End With

            Else
                'We are using TrackSapAttendance = ByClass app setting here 
                With sb
                    .Append("SELECT B.LastName, B.FirstName, B.MiddleName, B.StatusCodeDescrip, B.StudentIdentifier, B.StuEnrollId, B.RecordDate, ")
                    .Append("SUM(B.SchedHours) AS SchedHours, SUM(B.ActualHours) AS ActualHours, SUM(B.MakeUpHours) AS MakeUpHours, SUM(B.AbsentHours) AS AbsentHours, ")
                    .Append("isTardy, ( dbo.GetLDA(B.StuEnrollId)) AS LDA, B.ClassSection,B.ClsSectMeetingId,B.StartTime,B.EndTime  ")
                    .Append("FROM ")
                    .Append("(")
                    'Student Details Section
                    .Append("SELECT arStudent.LastName,arStudent.FirstName,arStudent.MiddleName,SSC.StatusCodeDescrip, ")
                    .Append("arStudent.SSN AS StudentIdentifier, B.StuENrollId, ")
                    .Append("Convert(varchar,MeetDate,101) RecordDate, Scheduled AS SchedHours, Actual AS ActualHours, ")
                    .Append("IsNull((Select (Actual- Scheduled) from   atClsSectAttendance A  ")
                    .Append("where A.StuEnrollId=B.StuEnrollId  ")
                    .Append("and A.MeetDate=B.MeetDate and Actual<>9999.0 and  ")
                    .Append("Actual<>999.0 and Scheduled is not null and Scheduled<Actual AND A.ClsSectMeetingId = B.ClsSectMeetingId),0.00) AS MakeUpHours, ")
                    .Append("IsNull((Select (Scheduled- Actual) from atClsSectAttendance A ")
                    .Append("where A.StuEnrollId=B.StuEnrollId ")
                    .Append("and A.MeetDate=B.MeetDate ")
                    .Append("and Actual<>9999.0 and Actual<>999.0 ")
                    .Append("and Scheduled is not null and Scheduled>Actual AND A.ClsSectMeetingId = B.ClsSectMeetingId),0.00) AS AbsentHours, ")
                    .Append("B.Tardy AS isTardy, (dbo.GetLDA(B.StuEnrollId)) LDA, ")
                    .Append("(SELECT Descrip FROM arreqs WHERE ReqId IN ")
                    .Append("(SELECT ReqId FROM dbo.arClassSections WHERE ClsSectionId = B.ClsSectionId)) + ' (' + ")
                    .Append("(SELECT ClsSection FROM arClassSections WHERE ClsSectionId = B.ClsSectionId) + ') % ' + ")
                    .Append("(SELECT InstructionTypeDescrip FROM arInstructionType WHERE InstructionTypeId =  ")
                    .Append("(SELECT InstructionTypeId FROM arClsSectMeetings WHERE ClsSectMeetingId = ")
                    .Append("(SELECT ClsSectMeetingId FROM atClsSectAttendance WHERE ClsSectAttId = B.ClsSectAttId))) AS ClassSection,B.ClsSectMeetingId  ")
                    .Append(" , (SELECT CONVERT(VARCHAR,Table2.TimeIntervalDescrip,108) FROM dbo.arClsSectMeetings Table1,dbo.syPeriods Table3, ")
                    .Append(" dbo.cmTimeInterval Table2 WHERE Table1.ClsSectMeetingId = B.ClsSectMeetingId   ")
                    .Append(" AND Table3.PeriodId=Table1.PeriodId  ")
                    .Append(" AND Table3.StartTimeId=Table2.TimeIntervalId)AS StartTime,  ")
                    .Append("  (SELECT CONVERT(VARCHAR,Table2.TimeIntervalDescrip,108) FROM dbo.arClsSectMeetings Table1,dbo.syPeriods Table3, ")
                    .Append(" dbo.cmTimeInterval Table2 WHERE Table1.ClsSectMeetingId = B.ClsSectMeetingId   ")
                    .Append(" AND Table3.PeriodId=Table1.PeriodId  ")
                    .Append(" AND Table3.EndTimeId=Table2.TimeIntervalId)AS EndTime ")

                    .Append("FROM ")
                    .Append("atClsSectAttendance B,arStudent,arStuEnrollments,SyStatusCodes SSC ")
                    '.Append("   , syCmpGrpCmps E,syCampuses F,syCampGrps ")
                    .Append("WHERE ")
                    .Append("            CAST( CONVERT(VARCHAR, B.MeetDate, 101)as DATE)>= CAST( CONVERT(VARCHAR,'" + strFromDate + "', 101) as DATE) ")
                    ' .Append("B.MeetDate>='" & strFromDate & "' ")
                    .Append("            AND CAST( CONVERT(VARCHAR, B.MeetDate, 101) as DATE)<= CAST(CONVERT(VARCHAR,'" + strToDate + "', 101) as DATE)")
                    '   .Append("and B.MeetDate<='" & strToDate & "' ")
                    .Append("and B.Scheduled is not null ")
                    .Append("and B.Actual <>999.0 and B.Actual<>9999.0 ")
                    .Append("and arStudent.StudentId=arStuEnrollments.StudentId ")
                    .Append("and arStuEnrollments.StuEnrollId=B.StuEnrollId ")
                    .Append("and SSC.StatusCodeId=arStuEnrollments.StatusCodeId ")
                    '.Append("and arStuEnrollments.CampusId=F.CampusId ")
                    '.Append("and F.CampusId=E.CampusId ")
                    '.Append("AND E.CampGrpId = syCampGrps.CampGrpId ")
                    .Append(strWhere)
                    .Append(") B ")
                    .Append("GROUP BY ")
                    .Append("B.RecordDate, B.LastName, B.FirstName, B.MiddleName, B.StatusCodeDescrip, B.StudentIdentifier, B.StuEnrollId, ")
                    .Append("B.isTardy, B.ClassSection,B.ClsSectMeetingId,B.StartTime,B.EndTime ")

                    'LOA Section
                    .Append(";SELECT StuEnrollId, StartDate, EndDate FROM arStudentLOAs ")

                    'Suspension Section
                    .Append(";SELECT StuEnrollId, StartDate, EndDate FROM arStdSuspensions ")

                    'Holidays Section
                    .Append(";SELECT Distinct '' AS CampusId, CCT.HolidayCode, CCT.HolidayStartDate StartDate, CCT.HolidayEndDate EndDate,  CCT.HolidayDescrip ")
                    .Append("  , CCT.AllDay, (SELECT TimeIntervalDescrip FROM dbo.cmTimeInterval WHERE TimeIntervalId=StartTimeId)StartTime,(SELECT TimeIntervalDescrip FROM dbo.cmTimeInterval WHERE TimeIntervalId=cct.EndTimeId)EndTime  ")
                    .Append("FROM syHolidays CCT, syStatuses ST ")
                    '.Append(", syCampGrps ")
                    .Append("WHERE CCT.StatusId = ST.StatusId ")
                    .Append("and ST.Status='Active' ")
                    '.Append("and syCampGrps.CampGrpId=CCT.CampGrpId ")
                    .Append(" and CCT.CampGrpId in (Select syCmpGrpCmps.CampGrpId  from  arStuEnrollments,syCmpGrpCmps where  (syCmpGrpCmps.CampusId=arStuEnrollments.CampusId   ")
                    .Append(strWhere)
                    .Append("  ) ) OR CCT.CampGrpId IN (SELECT CampGrpId FROM dbo.syCampGrps WHERE CampGrpCode LIKE 'All') ")

                    'Students List Section
                    .Append(";SELECT  Distinct atClsSectAttendance.StuEnrollId, PV.UnitTypeId, '' AS CampusId  ")
                    '.Append(" ,arStuEnrollments.CampusID ")
                    .Append("FROM atClsSectAttendance, arStuEnrollments, arPrgVersions PV ")
                    '.Append(" , syCmpGrpCmps E, syCampuses F, syCampGrps ")
                    .Append("WHERE ")
                    ' .Append("MeetDate>='" & strFromDate & "' ")
                    .Append("           CAST( CONVERT(VARCHAR, MeetDate, 101) as DATE)>= CAST(CONVERT(VARCHAR,'" + strFromDate + "', 101)as DATE) ")
                    .Append("            AND CAST( CONVERT(VARCHAR, MeetDate, 101) as DATE)<= CAST(CONVERT(VARCHAR,'" + strToDate + "', 101) as DATE)")
                    ' .Append("and MeetDate<='" & strToDate & "' ")
                    .Append("and arStuEnrollments.StuEnrollid=atClsSectAttendance.StuEnrollId ")
                    .Append("and Scheduled is not null ")
                    .Append("and Actual <>999.0 and Actual<>9999.0 ")
                    .Append("and arStuEnrollments.PrgVerId =PV.PrgVerId ")
                    '.Append("and arStuEnrollments.CampusId=F.CampusId ")
                    '.Append("AND F.CampusId=E.CampusId ")
                    '.Append("AND E.CampGrpId = syCampGrps.CampGrpId ")
                    .Append(strWhere)

                    'Student Month Totals Section
                    '.Append(";SELECT atClsSectAttendance.StuEnrollId,'' AS ScheduleId,  isNull(sum(Scheduled),0.00) SchedHours, ")
                    '.Append("isNull(Sum(Actual),0.00)ActualHours, ")
                    '.Append("isNull((Select Sum(Scheduled - Actual) from atClsSectAttendance A ")
                    '.Append("where  A.StuEnrollId=atClsSectAttendance.StuEnrollid ")
                    ' ''and A.MeetDate<'" & strFromDate & "' ")
                    '.Append("            AND CAST( CONVERT(VARCHAR, A.MeetDate, 101) as DATE)< CAST(CONVERT(VARCHAR,'" + strFromDate + "', 101)as DATE) ")

                    '.Append("and Actual<>9999.0 and Actual<>999.0 ")
                    '.Append("and Scheduled is not null ")
                    '.Append("and Scheduled>Actual  AND A.ClsSectMeetingId=atClsSectAttendance.ClsSectMeetingId),0.00)AbsentHours, ")
                    '.Append("isNull((Select Sum(Actual - Scheduled) from atClsSectAttendance A ")
                    '.Append("where A.StuEnrollId=atClsSectAttendance.StuEnrollid ")
                    ' ''and A.MeetDate<'" & strFromDate & "' ")
                    '.Append("            AND CAST( CONVERT(VARCHAR, A.MeetDate, 101) as DATE)< CAST(CONVERT(VARCHAR,'" + strFromDate + "', 101) as DATE)")

                    '.Append("and Actual<>9999.0 and Actual<>999.0 ")
                    '.Append("and  Scheduled is not null ")
                    '.Append("and Scheduled<Actual   AND A.ClsSectMeetingId=atClsSectAttendance.ClsSectMeetingId),0.00)MakeUpHours,atClsSectAttendance.ClsSectMeetingId ")
                    '.Append("FROM ")
                    '.Append("atClsSectAttendance, arStuEnrollments ")
                    ''.Append(" , syCmpGrpCmps E, syCampuses F, syCampGrps ")
                    '.Append("WHERE ")
                    '' .Append("atClsSectAttendance.MeetDate<='" & strFromDate & "' ")

                    '.Append("          CAST(CONVERT(VARCHAR, atClsSectAttendance.MeetDate, 101) AS DATE)< CAST(CONVERT(VARCHAR,'" + strFromDate + "', 101) AS DATE) ")

                    ''.Append("and arStuEnrollments.StuEnrollid=atClsSectAttendance.StuEnrollId ")
                    '.Append("and atClsSectAttendance.Actual <>999.0 and atClsSectAttendance.Actual<>9999.0 ")
                    '.Append(" AND atClsSectAttendance.StuEnrollId=arStuEnrollments.StuEnrollId ")
                    ''.Append("and arStuEnrollments.CampusId=F.CampusId ")
                    ''.Append("and F.CampusId=E.CampusId ")
                    ''.Append("and E.CampGrpId = syCampGrps.CampGrpId ")
                    '.Append(strWhere)
                    '.Append(" GROUP BY atClsSectAttendance.StuEnrollid,atClsSectAttendance.ClsSectMeetingId ")


                    .Append(";SELECT atClsSectAttendance.StuEnrollId,'' AS ScheduleId,  isNull(Scheduled,0.00) Scheduled, ")
                    .Append("isNull(Actual,0.00)Actual, ")
                    .Append("0 as AbsentHours, ")
                    .Append("0 as MakeUpHours,")
                    .Append(" atClsSectAttendance.ClsSectMeetingId,TardiesMakingAbsence,Tardy,MeetDate, ")

                    .Append("(SELECT Descrip FROM arreqs WHERE ReqId IN ")
                    .Append("(SELECT ReqId FROM dbo.arClassSections B WHERE B.ClsSectionId = atClsSectAttendance.ClsSectionId)) + ' (' + ")
                    .Append("(SELECT ClsSection FROM arClassSections B WHERE B.ClsSectionId = atClsSectAttendance.ClsSectionId) + ') % ' + ")
                    .Append("(SELECT InstructionTypeDescrip FROM arInstructionType WHERE InstructionTypeId =  ")
                    .Append("(SELECT InstructionTypeId FROM arClsSectMeetings WHERE ClsSectMeetingId = ")
                    .Append("(SELECT ClsSectMeetingId FROM atClsSectAttendance C WHERE C.ClsSectAttId = atClsSectAttendance.ClsSectAttId))) AS ClassSection  ")
                    .Append("FROM ")
                    .Append("atClsSectAttendance , arStuEnrollments,arPrgVersions ")
                    '.Append(" , syCmpGrpCmps E, syCampuses F, syCampGrps ")
                    .Append("WHERE ")
                    ' .Append("atClsSectAttendance.MeetDate<='" & strFromDate & "' ")

                    .Append("          CAST(CONVERT(VARCHAR, atClsSectAttendance.MeetDate, 101) AS DATE)< CAST(CONVERT(VARCHAR,'" + strFromDate + "', 101) AS DATE) ")

                    '.Append("and arStuEnrollments.StuEnrollid=atClsSectAttendance.StuEnrollId ")
                    .Append("and atClsSectAttendance.Actual <>999.0 and atClsSectAttendance.Actual<>9999.0 ")
                    .Append(" AND atClsSectAttendance.StuEnrollId=arStuEnrollments.StuEnrollId ")
                    .Append(" and arStuEnrollments.PrgverId=arPrgVersions.PrgVerid ")
                    '.Append("and arStuEnrollments.CampusId=F.CampusId ")
                    '.Append("and F.CampusId=E.CampusId ")
                    '.Append("and E.CampGrpId = syCampGrps.CampGrpId ")
                    .Append(strWhere)
                    .Append(" order by meetdate ")
                    '.Append(" GROUP BY atClsSectAttendance.StuEnrollid,atClsSectAttendance.ClsSectMeetingId ")


                    'Student Tardy Details Section
                    .Append(";SELECT B.StuENrollId, Convert(varchar,MeetDate,101) RecordDate, Scheduled AS SchedHours, ")
                    .Append("Actual AS ActualHours, Tardy AS isTardy, PV.TrackTardies, ")
                    .Append("PV.TardiesMakingAbsence, 0 AS MarkAbsent,B.ClsSectMeetingId  ")


                    .Append("FROM ")
                    .Append("atClsSectAttendance B, arStudent, arStuEnrollments, arPrgVersions PV, SyStatusCodes SSC ")
                    '.Append(", syCmpGrpCmps E,syCampuses F,syCampGrps ")
                    .Append("WHERE ")
                    .Append("PV.PrgVerId=dbo.arStuEnrollments.PrgVerId ")
                    .Append("and Tardy=1 AND TrackTardies=1 ")
                    .Append("and TardiesMakingAbsence>0 ")
                    .Append("and Scheduled is not null ")
                    .Append("and Actual <>999.0 and Actual<>9999.0 ")
                    .Append("and arStudent.StudentId=arStuEnrollments.StudentId ")
                    .Append("and arStuEnrollments.StuEnrollId=B.StuEnrollId ")
                    .Append("and SSC.StatusCodeId=arStuEnrollments.StatusCodeId ")
                    '.Append("and arStuEnrollments.CampusId=F.CampusId ")
                    '.Append("and F.CampusId=E.CampusId ")
                    '.Append("and E.CampGrpId = syCampGrps.CampGrpId ")
                    .Append(strWhere)
                    .Append(" ORDER BY RecordDate ; ")
                    ''To check if we have to show the instruction type or not on the report
                    .Append("SELECT COUNT(InstructionTypeId)  FROM dbo.arInstructionType; ")

                End With


            End If




            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()
            ds = db.RunParamSQLDataSet(sb.ToString)

        Catch ex As Exception
            Return ds
        End Try

        ds.Tables(0).TableName = "Studentdetails"
        ds.Tables(1).TableName = "LOA"
        ds.Tables(2).TableName = "Susp"
        ds.Tables(3).TableName = "Holiday"
        ds.Tables(4).TableName = "Studentslist"
        ds.Tables(5).TableName = "StudentMonthTots"
        ds.Tables(6).TableName = "StudentTardyDetails"
        ds.Tables(7).TableName = "InstructionTypeCount"

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
#End Region

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

End Class
