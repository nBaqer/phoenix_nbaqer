Imports FAME.Advantage.Common

Public Class LeadMasterDB

    Public Function GetLeadSummaryList(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= " ORDER BY " & paramInfo.OrderBy
        End If

        With sb
            .Append("SELECT DISTINCT adLeads.LastName,adLeads.FirstName,adLeads.MiddleName,")
            .Append(" ( SELECT TOP 1 ALP.Phone FROM     adLeadPhone AS ALP WHERE    ALP.IsBest = 1 AND ALP.Position = 1 AND ALP.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' ")
            .Append(" AND ALP.LeadId = adLeads.LeadId ORDER BY ALP.ModDate DESC ) AS Phone ,")
            .Append("( SELECT   TOP 1 ALP.IsForeignPhone FROM     adLeadPhone AS ALP WHERE    ALP.IsBest = 1 AND ALP.Position = 1 AND ALP.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' ")
            .Append(" AND ALP.LeadId = adLeads.LeadId ORDER BY ALP.ModDate DESC ) AS ForeignPhone , ")
            .Append("adLeads.LeadStatus,syStatusCodes.StatusCodeDescrip,adLeads.ProgramID,")
            .Append("(SELECT ProgDescrip FROM arPrograms WHERE ProgId=adLeads.ProgramID) AS ProgramDescrip,")
            .Append("adLeads.ExpectedStart,syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,")
            .Append("adLeads.CampusId,syCampuses.CampDescrip,adLeads.AdmissionsRep,")
            .Append("(SELECT FullName FROM syUsers WHERE UserId=adLeads.AdmissionsRep) AS AdmissionsRepName ")
            .Append("FROM adLeads,syStatusCodes,syCmpGrpCmps,syCampGrps,syCampuses ")
            .Append("WHERE adLeads.LeadStatus = syStatusCodes.StatusCodeId ")
            .Append("AND syCmpGrpCmps.CampusId = adLeads.CampusId ")
            .Append("AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            .Append("AND syCampuses.CampusId=adLeads.CampusId ")
            .Append(strWhere)
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function


    Public Function GetLeadList(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        If paramInfo.FilterList <> "" Then
            If strWhere = "" Then
                strWhere = " WHERE "
            Else
                strWhere &= " AND "
            End If
            strWhere &= paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            If strWhere = "" Then
                strWhere = " WHERE "
            Else
                strWhere &= " AND "
            End If
            strWhere &= paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= " ORDER BY " & paramInfo.OrderBy
        End If

        With sb
            .Append("SELECT DISTINCT ")
            .Append("  adLeads.LastName, adLeads.FirstName, adLeads.MiddleName, ") ' adLeadAddresses.Address1, adLeadAddresses.Address2 ")
            .Append(" ( SELECT   TOP 1 ALA.Address1 FROM     adLeadAddresses AS ALA WHERE    ALA.IsShowOnLeadPage = 1 ")
            .Append(" AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ALA.LeadId = adLeads.LeadId ORDER BY ALA.ModDate DESC ) AS Address1 , ")
            .Append(" ( SELECT   TOP 1 ALA.Address2 FROM     adLeadAddresses AS ALA WHERE    ALA.IsShowOnLeadPage = 1 ")
            .Append(" AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ALA.LeadId = adLeads.LeadId ORDER BY ALA.ModDate DESC ) AS Address2 , ")
            .Append(" ( SELECT TOP 1 ALA.City FROM  adLeadAddresses AS ALA WHERE    ALA.IsShowOnLeadPage = 1 ")
            .Append(" AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ALA.LeadId = adLeads.LeadId ORDER BY ALA.ModDate DESC ) AS City , ")
            .Append(" ( SELECT TOP 1 ALA.StateId FROM   adLeadAddresses AS ALA WHERE  ALA.IsShowOnLeadPage = 1 ")
            .Append(" AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ALA.LeadId = adLeads.LeadId ORDER BY ALA.ModDate DESC ) AS StateId , ")
            .Append(" ( SELECT StateDescrip FROM syStates   WHERE StateId = ( SELECT   TOP 1 ALA.StateId FROM     adLeadAddresses AS ALA ")
            .Append(" WHERE    ALA.IsShowOnLeadPage = 1  AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ALA.LeadId = adLeads.LeadId ")
            .Append(" ORDER BY ALA.ModDate DESC)  ) AS StateDescrip , ")
            .Append(" ( SELECT   TOP 1 ALA.ZipCode  FROM     adLeadAddresses AS ALA  WHERE    ALA.IsShowOnLeadPage = 1 ")
            .Append(" And ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ALA.LeadId = adLeads.LeadId ORDER BY ALA.ModDate DESC ) AS  Zip , ")
            .Append(" adLeads.SourceDate , ")
            .Append(" (SELECT CountryDescrip FROM adCountries WHERE CountryId = (SELECT   TOP 1 ALA.CountryId  FROM     adLeadAddresses AS ALA ")
            .Append(" WHERE    ALA.IsShowOnLeadPage = 1  AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ALA.LeadId = adLeads.LeadId ")
            .Append(" ORDER BY ALA.ModDate DESC)) AS CountryDescrip , ")
            .Append(" ( SELECT   TOP 1 ALA.IsInternational FROM     adLeadAddresses AS ALA WHERE    ALA.IsShowOnLeadPage = 1 ")
            .Append(" AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ALA.LeadId = adLeads.LeadId ORDER BY ALA.ModDate DESC  ) AS ForeignZip , ")
            .Append(" ( SELECT   TOP 1 ALA.State FROM     adLeadAddresses AS ALA WHERE    ALA.IsShowOnLeadPage = 1 ")
            .Append(" AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ALA.LeadId = adLeads.LeadId ORDER BY ALA.ModDate DESC ) AS OtherState , ")
            .Append(" ( SELECT   TOP 1 ALP.Phone FROM     adLeadPhone AS ALP WHERE    ALP.IsBest = 1 AND ALP.Position = 1 ")
            .Append(" AND ALP.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ALP.LeadId = adLeads.LeadId ORDER BY ALP.ModDate DESC  ) AS Phone , ")
            .Append(" ( SELECT   TOP 1 ALP.IsForeignPhone FROM     adLeadPhone AS ALP WHERE    ALP.IsBest = 1 AND ALP.Position = 1 ")
            .Append(" AND ALP.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ALP.LeadId = adLeads.LeadId ORDER BY ALP.ModDate DESC ) AS ForeignPhone ")
            .Append(", adLeads.LeadStatus, syStatusCodes.StatusCodeDescrip ")
            .Append(", adLeads.ExpectedStart ")
            .Append(", adLeads.ProgramID, arPrograms.ProgDescrip  AS Program ")
            .Append(", adLeads.SourceCategoryID, adSourceCatagory.SourceCatagoryDescrip AS SourceCateg ")
            .Append(", adLeads.SourceTypeID, adSourceType.SourceTypeDescrip AS SourceType ")
            .Append(", adLeads.SourceAdvertisement, adSourceAdvertisement.sourceadvdescrip AS SourceAdvert ")
            .Append(", adLeads.AdvertisementNote, adLeads.AdmissionsRep ")
            .Append(", syUsers.FullName AS AdmissionsRepName ")
            .Append(", syCmpGrpCmps.CampGrpId, syCampGrps.CampGrpDescrip ")
            .Append(", adLeads.CampusId, syCampuses.CampDescrip ")
            .Append("FROM adLeads ")
            .Append("    LEFT OUTER JOIN syStatusCodes ")
            .Append("        ON adLeads.LeadStatus = syStatusCodes.StatusCodeId ")
            .Append("    LEFT OUTER JOIN syCampuses ")
            .Append("        ON adLeads.CampusId = syCampuses.CampusId ")
            .Append("    LEFT OUTER JOIN arPrograms ")
            .Append("        ON adLeads.ProgramID = arPrograms.ProgId ")
            .Append("    LEFT OUTER JOIN adSourceCatagory ")
            .Append("        ON adLeads.SourceCategoryID = adSourceCatagory.SourceCatagoryID ")
            .Append("    LEFT OUTER JOIN adSourceAdvertisement ")
            .Append("        ON adLeads.SourceAdvertisement = adSourceAdvertisement.SourceAdvId ")
            .Append("    LEFT OUTER JOIN syUsers ")
            .Append("        ON adLeads.AdmissionsRep = syUsers.UserID ")
            .Append("    LEFT OUTER JOIN adSourceType ")
            .Append("        ON adLeads.SourceTypeID = adSourceType.SourceTypeID ")
            .Append("    LEFT OUTER JOIN syCmpGrpCmps ")
            .Append("        ON adLeads.CampusId = syCmpGrpCmps.CampusId ")
            .Append("    LEFT OUTER JOIN syCampGrps ")
            .Append("        ON syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId")
            .Append("    LEFT OUTER JOIN adLeadAddresses ")
            .Append("        ON adLeadAddresses.LeadId = adLeads.LeadId")
            .Append(strWhere)
            .Append(strOrderBy)
        End With

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.CommandTimeout = 0      ' Special case: this query might take a long time.
            db.OpenConnection()

        Try
            ds = db.RunParamSQLDataSet(sb.ToString)
            Return ds
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw New System.Exception(ex.Message)
            Else
                Throw New System.Exception(ex.InnerException.Message)
            End If
        Finally
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try

    End Function

    Public Function GetLeadZipSummary(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        'If paramInfo.OrderBy <> "" Then
        '    strOrderBy &= "," & paramInfo.OrderBy
        'End If

        With sb
            .Append("SELECT ")
            .Append("       adLeadAddresses.ZipCode AS Zip,adLeadAddresses.IsInternational AS ForeignZip,COUNT(*) AS Inquiries,")
            .Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,adLeads.CampusId,syCampuses.CampDescrip ")
            .Append("FROM   adLeads,syCmpGrpCmps,syCampGrps,syCampuses,adLeadAddresses ")
            .Append("WHERE  adLeads.Zip Is Not NULL ")
            .Append("       AND syCmpGrpCmps.CampusId=adLeads.CampusId ")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            .Append("       AND syCampuses.CampusId=adLeads.CampusId ")
            .Append("       AND adLeadAddresses.LeadId = adLeads.LeadId ")
            .Append(strWhere)
            .Append("GROUP BY syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,adLeads.CampusId,syCampuses.CampDescrip,adLeadAddresses.ZipCode,adLeadAddresses.IsInternational ")
            .Append("ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,adLeadAddresses.ZipCode,adLeadAddresses.IsInternational;")
            .Append("SELECT ")
            .Append("       adLeadAddresses.ZipCode AS Zip,adLeadAddresses.IsInternational AS ForeignZip,COUNT(*) AS CountByInquiryDate,SourceDate AS InquiryDate,")
            .Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,adLeads.CampusId,syCampuses.CampDescrip ")
            .Append("FROM   adLeads,syCmpGrpCmps,syCampGrps,syCampuses,adLeadAddresses ")
            .Append("WHERE  adLeadAddresses.ZipCode Is Not NULL ")
            .Append("       AND syCmpGrpCmps.CampusId=adLeads.CampusId ")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            .Append("       AND syCampuses.CampusId=adLeads.CampusId ")
            .Append("       AND adLeadAddresses.LeadId = adLeads.LeadId ")
            .Append(strWhere)
            .Append("GROUP BY syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,adLeads.CampusId,syCampuses.CampDescrip,adLeadAddresses.ZipCode,adLeadAddresses.IsInternational,adLeads.SourceDate ")
            .Append("ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,adLeadAddresses.ZipCode,adLeadAddresses.IsInternational,adLeads.SourceDate")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count = 2 Then
            ds.Tables(0).TableName = "ZipCodeSummary"
            ds.Tables(1).TableName = "ZipAndInquiryDate"
            '   add new columns
            ds.Tables(0).Columns.Add(New DataColumn("Jan", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("Feb", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("Mar", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("Apr", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("May", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("Jun", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("Jul", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("Aug", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("Sep", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("Oct", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("Nov", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("Dec", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("ZipCount", System.Type.GetType("System.Int32")))

            ''''Dim dt As New DataTable("CampusGroupTotals")
            ''''dt.Columns.Add(New DataColumn("CampusGrpDescrip", System.Type.GetType("System.String")))
            ''''dt.Columns.Add(New DataColumn("CampusGrpTotal", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CGJan", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CGFeb", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CGMar", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CGApr", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CGMay", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CGJun", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CGJul", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CGAug", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CGSep", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CGOct", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CGNov", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CGDec", System.Type.GetType("System.Int32")))
            ''''ds.Tables.Add(dt)

            ''''dt = New DataTable("CampusTotals")
            ''''dt.Columns.Add(New DataColumn("CampusGrpDescrip", System.Type.GetType("System.String")))
            ''''dt.Columns.Add(New DataColumn("CampusDescrip", System.Type.GetType("System.String")))
            ''''dt.Columns.Add(New DataColumn("CampusTotal", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CJan", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CFeb", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CMar", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CApr", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CMay", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CJun", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CJul", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CAug", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CSep", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("COct", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CNov", System.Type.GetType("System.Int32")))
            ''''dt.Columns.Add(New DataColumn("CDec", System.Type.GetType("System.Int32")))
            ''''ds.Tables.Add(dt)
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Function GetLeadZipDetail(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        'If paramInfo.OrderBy <> "" Then
        '    strOrderBy &= "," & paramInfo.OrderBy
        'End If

        With sb
            .Append("SELECT adLeadAddresses.ZipCode AS Zip,adLeadAddresses.IsInternational AS ForeignZip,adLeads.SourceDate,adLeads.LastName,adLeads.FirstName,adLeads.MiddleName,")
            .Append("       adLeads.LeadStatus,syStatusCodes.StatusCodeDescrip,")
            .Append("       adLeads.SourceAdvertisement AS AdvertisementId,")
            .Append("       (SELECT sourceadvdescrip FROM adSourceAdvertisement ")
            .Append("       WHERE  sourceadvid=adLeads.SourceAdvertisement) AS AdvertisementDescrip,")
            .Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,")
            .Append("       adLeads.CampusId,syCampuses.CampDescrip ")
            .Append("FROM   adLeads, syStatusCodes, syCmpGrpCmps, syCampGrps, syCampuses,adLeadAddresses ")
            .Append("WHERE  adLeads.LeadStatus = syStatusCodes.StatusCodeId ")
            .Append("       AND syCmpGrpCmps.CampusId = adLeads.CampusId ")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            .Append("       AND syCampuses.CampusId=adLeads.CampusId ")
            .Append("       AND adLeadAddresses.LeadId = adLeads.LeadId ")
            .Append("       AND adLeadAddresses.ZipCode IS NOT NULL ")
            .Append(strWhere)
            .Append("GROUP BY syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,")
            .Append("       adLeads.CampusId,syCampuses.CampDescrip,adLeadAddresses.ZipCode,adLeadAddresses.IsInternational,adLeads.SourceDate,")
            .Append("       adLeads.LastName,adLeads.FirstName,adLeads.MiddleName,")
            .Append("       adLeads.LeadStatus,syStatusCodes.StatusCodeDescrip,adLeads.SourceAdvertisement ")
            .Append("ORDER BY adLeadAddresses.ZipCode,adLeadAddresses.IsInternational,adLeads.SourceDate,adLeads.LastName,adLeads.FirstName,adLeads.MiddleName,syStatusCodes.StatusCodeDescrip;")
            .Append("SELECT adLeadAddresses.ZipCode AS Zip, adLeadAddresses.IsInternational AS ForeignZip,COUNT(*) AS Inquiries,")
            .Append("       syCmpGrpCmps.CampGrpId, syCampGrps.CampGrpDescrip, adLeads.CampusId, syCampuses.CampDescrip ")
            .Append("FROM   adLeads, syCmpGrpCmps, syCampGrps, syCampuses,adLeadAddresses ")
            .Append("WHERE  adLeadAddresses.ZipCode Is Not NULL ")
            .Append("       AND syCmpGrpCmps.CampusId = adLeads.CampusId ")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            .Append("       AND syCampuses.CampusId=adLeads.CampusId ")
            .Append("       AND adLeads.LeadId=adLeadAddresses.LeadId ")
            .Append(strWhere)
            .Append("GROUP BY syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,adLeads.CampusId,syCampuses.CampDescrip,adLeadAddresses.ZipCode,adLeadAddresses.IsInternational ")
            .Append("ORDER BY syCampGrps.CampGrpDescrip,syCampuses.CampDescrip,adLeadAddresses.ZipCode,adLeadAddresses.IsInternational ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count = 2 Then
            ds.Tables(0).TableName = "ZipCodeDetail"
            ds.Tables(1).TableName = "ZipCodeSummary"
            'add new columns
            ds.Tables(0).Columns.Add(New DataColumn("LeadName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("ZipCount", System.Type.GetType("System.Int32")))
            ds.Tables(1).Columns.Add(New DataColumn("Percentage", System.Type.GetType("System.Decimal")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function

    Public Function GetLeadAdvertisementDetail(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        If paramInfo.OrderBy <> "" Then
            Dim temp As String = paramInfo.OrderBy
            If temp.IndexOf("adLeads.ProgramId") > -1 Then
                temp = temp.Replace("adLeads.ProgramId", "Program")
            End If
            If temp.IndexOf("adLeads.AdmissionsRep") > -1 Then
                temp = temp.Replace("adLeads.AdmissionsRep", "AdmissionsRepName")
            End If
            strOrderBy &= "," & temp
        Else
            strOrderBy &= ",LastName,FirstName,MiddleName"
        End If

        'build query to retrieve leads by campus group, campus, category, source and advertisement
        With sb
            .Append("SELECT ")
            .Append("       syCmpGrpCmps.CampGrpId,syCampGrps.CampGrpDescrip,adLeads.CampusId,syCampuses.CampDescrip,")
            .Append("       adLeads.SourceCategoryId,(SELECT SourceCatagoryDescrip FROM adSourceCatagory WHERE SourceCatagoryID=adLeads.SourceCategoryID) AS Category,")
            .Append("       adLeads.SourceTypeId,(SELECT SourceTypeDescrip FROM adSourceType WHERE SourceTypeID=adLeads.SourceTypeID) AS Source,")
            .Append("       SourceAdvertisement,(SELECT SourceAdvDescrip FROM adSourceAdvertisement WHERE SourceAdvId=adLeads.SourceAdvertisement) AS Advertisement,")
            .Append("       (SELECT StartDate FROM adSourceAdvertisement WHERE SourceAdvId=adLeads.SourceAdvertisement) AS AdvStartDate,")
            .Append("       (SELECT EndDate FROM adSourceAdvertisement WHERE SourceAdvId=adLeads.SourceAdvertisement) AS AdvEndDate,")
            .Append("       FirstName,LastName,MiddleName,LeadStatus,syStatusCodes.StatusCodeDescrip,")
            .Append("       AdmissionsRep,(SELECT FullName FROM syUsers WHERE UserId=adLeads.AdmissionsRep) AS AdmissionsRepName,")
            .Append("       ProgramId,(SELECT ProgDescrip FROM arPrograms WHERE ProgId=adLeads.ProgramID) AS ProgDescrip,")
            .Append("       SourceDate AS InquiryDate,ExpectedStart ")
            .Append("FROM   adLeads,syStatusCodes,syCmpGrpCmps,syCampGrps,syCampuses ")
            .Append("WHERE  adLeads.LeadStatus=syStatusCodes.StatusCodeId")
            .Append("       AND syCmpGrpCmps.CampusId=adLeads.CampusId")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId")
            .Append("       AND syCampuses.CampusId=adLeads.CampusId ")
            .Append(strWhere)
            .Append("ORDER BY syCampGrps.CampGrpDescrip,syCmpGrpCmps.CampGrpId,syCampuses.CampDescrip,")
            .Append("adLeads.CampusId,Category,Source,Advertisement")
            .Append(strOrderBy)
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count = 1 Then
            ds.Tables(0).TableName = "LeadAdDetail"
            'add new columns
            ds.Tables(0).Columns.Add(New DataColumn("LeadName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("RecCount", System.Type.GetType("System.Int32")))
            ds.Tables(0).Columns.Add(New DataColumn("ElapsedTime", System.Type.GetType("System.Int32")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function


    Public Function GetLeadAdvertisementSummary(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String
        Dim strOrderBy As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If

        'If paramInfo.OrderBy <> "" Then
        '    strOrderBy &= "ORDER BY " & paramInfo.OrderBy
        'End If

        'build query to retrieve leads by campus group, campus, category, source and advertisement
        With sb
            .Append("SELECT ")
            .Append("       adLeads.SourceCategoryId,(SELECT SourceCatagoryDescrip FROM adSourceCatagory WHERE SourceCatagoryID=adLeads.SourceCategoryID) AS Category,")
            .Append("       adLeads.SourceTypeId,(SELECT SourceTypeDescrip FROM adSourceType WHERE SourceTypeID=adLeads.SourceTypeID) AS Source,")
            .Append("       SourceAdvertisement,(SELECT SourceAdvDescrip FROM adSourceAdvertisement WHERE SourceAdvId=adLeads.SourceAdvertisement) AS Advertisement,")
            .Append("       (SELECT StartDate FROM adSourceAdvertisement WHERE SourceAdvId=adLeads.SourceAdvertisement) AS AdvStartDate,")
            .Append("       (SELECT EndDate FROM adSourceAdvertisement WHERE SourceAdvId=adLeads.SourceAdvertisement) AS AdvEndDate,")
            .Append("       COUNT(*) AS NumberOfInquiries ")
            .Append("FROM   adLeads,syCmpGrpCmps,syCampGrps,syCampuses ")
            .Append("WHERE  syCmpGrpCmps.CampusId=adLeads.CampusId")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId")
            .Append("       AND syCampuses.CampusId=adLeads.CampusId ")
            .Append(strWhere)
            .Append("GROUP BY adLeads.SourceCategoryId,adLeads.SourceTypeId,SourceAdvertisement ")
            .Append("ORDER BY Category,Source,Advertisement")
            .Append(";")
            .Append("SELECT ")
            .Append("       adLeads.SourceCategoryId,(SELECT SourceCatagoryDescrip FROM adSourceCatagory WHERE SourceCatagoryID=adLeads.SourceCategoryID) AS Category,")
            .Append("       adLeads.SourceTypeId,(SELECT SourceTypeDescrip FROM adSourceType WHERE SourceTypeID=adLeads.SourceTypeID) AS Source,")
            .Append("       COUNT(*) AS NumberOfInquiries ")
            .Append("FROM   adLeads,syCmpGrpCmps,syCampGrps,syCampuses ")
            .Append("WHERE  syCmpGrpCmps.CampusId=adLeads.CampusId")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId")
            .Append("       AND syCampuses.CampusId=adLeads.CampusId ")
            .Append(strWhere)
            .Append("GROUP BY adLeads.SourceCategoryId,adLeads.SourceTypeId ")
            .Append("ORDER BY Category,Source")
            .Append(";")
            .Append("SELECT ")
            .Append("       adLeads.SourceCategoryId,(SELECT SourceCatagoryDescrip FROM adSourceCatagory WHERE SourceCatagoryID=adLeads.SourceCategoryID) AS Category,")
            .Append("       COUNT(*) AS NumberOfInquiries ")
            .Append("FROM   adLeads,syCmpGrpCmps,syCampGrps,syCampuses ")
            .Append("WHERE  syCmpGrpCmps.CampusId=adLeads.CampusId")
            .Append("       AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId")
            .Append("       AND syCampuses.CampusId=adLeads.CampusId ")
            .Append(strWhere)
            .Append("GROUP BY adLeads.SourceCategoryId ")
            .Append("ORDER BY Category")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count = 3 Then
            ds.Tables(0).TableName = "LeadAdSummary"
            ds.Tables(1).TableName = "CategorySourceTotals"
            ds.Tables(2).TableName = "CategoryTotals"
            'add new column
            ds.Tables(0).Columns.Add(New DataColumn("Percentage", System.Type.GetType("System.Decimal")))
            ds.Tables(1).Columns.Add(New DataColumn("Percentage", System.Type.GetType("System.Decimal")))
            ds.Tables(2).Columns.Add(New DataColumn("Percentage", System.Type.GetType("System.Decimal")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    Public Sub MakeATransactionVoid(ByVal TransactionId As String)
        Dim db As New SQLDataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@TransactionId", TransactionId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_VoidTransaction")
        Catch ex As System.Exception

        Finally
            db.CloseConnection()
        End Try
    End Sub
    Public Sub UpdateReversalReason(ByVal TransactionId As String, _
                                    ByVal ReversalReason As String, _
                                   ByVal strLeadId As String, ByVal strTransCodeId As String, _
                                   ByVal strTransReference As String, ByVal strTransDescrip As String, _
                                   ByVal strTransAmount As String, _
                                   ByVal dtTransDate As Date, _
                                   ByVal strCampusId As String, ByVal intTransTypeId As Integer, _
                                   ByVal boolsIsEnrolled As Boolean, ByVal boolVoided As Boolean, _
                                   ByVal strModUser As String, ByVal dtModDate As Date)
        Dim db As New SQLDataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@TransactionId", New Guid(TransactionId), SqlDbType.UniqueIdentifier, 50, ParameterDirection.Input)
            db.AddParameter("@ReversalReason", ReversalReason, SqlDbType.VarChar, 250, ParameterDirection.Input)
            db.AddParameter("@LeadId", New Guid(strLeadId), SqlDbType.UniqueIdentifier, 250, ParameterDirection.Input)
            db.AddParameter("@TransCodeId", New Guid(strTransCodeId), SqlDbType.UniqueIdentifier, 250, ParameterDirection.Input)
            db.AddParameter("@TransReference", strTransReference, SqlDbType.VarChar, 250, ParameterDirection.Input)
            db.AddParameter("@TransDescrip", strTransDescrip, SqlDbType.VarChar, 250, ParameterDirection.Input)
            db.AddParameter("@TransAmount", strTransAmount, SqlDbType.VarChar, 250, ParameterDirection.Input)
            'db.AddParameter("@TransDate", dtTransDate, SqlDbType.Date, 250, ParameterDirection.Input)
            db.AddParameter("@CampusId", New Guid(strCampusId), SqlDbType.UniqueIdentifier, 250, ParameterDirection.Input)
            db.AddParameter("@TransTypeId", intTransTypeId, SqlDbType.Int, 250, ParameterDirection.Input)
            db.AddParameter("@IsEnrolled", boolsIsEnrolled, SqlDbType.Bit, 1, ParameterDirection.Input)
            db.AddParameter("@Voided", boolVoided, SqlDbType.Bit, 1, ParameterDirection.Input)
            db.AddParameter("@ModUser", strModUser, SqlDbType.VarChar, 250, ParameterDirection.Input)
            'db.AddParameter("@ModDate", dtModDate, SqlDbType.VarChar, 250, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_UpdateReversalReason")
        Catch ex As System.Exception
            Throw New Exception(ex.InnerException.ToString)
        Finally
            db.CloseConnection()
        End Try
    End Sub
    Public Function getTransactionsforApplicantLedger(ByVal LeadId As String) As DataSet

        Dim db As New SQLDataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@LeadId", LeadId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("dbo.USP_ApplicantLedger_GetList", "ApplicantLedger")
            Return ds
        Catch ex As System.Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try
    End Function
    Public Sub insertTransactionsforApplicantLedger(ByVal LeadId As String, _
                                                         ByVal TransCodeId As String, _
                                                         ByVal TransReference As String, _
                                                         ByVal TransDescrip As String,
                                                         ByVal TransAmount As Decimal, _
                                                         ByVal TransDate As DateTime, _
                                                         ByVal ModUser As String, _
                                                         ByVal ModDate As DateTime, _
                                                         ByVal CampusId As String, _
                                                         ByVal MaptoTransactionId As String)

        Dim db As New SQLDataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConnectionString")
        Try
            db.OpenConnection()
            db.AddParameter("@LeadId", New Guid(LeadId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@TransCodeId", New Guid(TransCodeId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@TransReference", TransReference, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.AddParameter("@TransDescrip", TransDescrip, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.AddParameter("@TransAmount", TransAmount, SqlDbType.Decimal, , ParameterDirection.Input)
            db.AddParameter("@TransDate", TransDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@ModUser", ModUser, SqlDbType.VarChar, 50, ParameterDirection.Input)
            db.AddParameter("@ModDate", ModDate, SqlDbType.DateTime, , ParameterDirection.Input)
            db.AddParameter("@CampusId", New Guid(CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            db.AddParameter("@MaptoTransactionId", New Guid(MaptoTransactionId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery_SP("USP_LeadTransactions_Insert")
        Catch ex As System.Exception
            Throw New Exception(ex.InnerException.ToString)
        Finally
            db.CloseConnection()
        End Try
    End Sub
End Class
