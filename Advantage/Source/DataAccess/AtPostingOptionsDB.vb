'Option Strict On

Public Class AtPostingOptionsDB
    Public Function GetCampusList(ByVal StatusId As Guid) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strSQLString As New StringBuilder
        Dim da As New OleDbDataAdapter
        db.OpenConnection()
        With strSQLString
            .Append("SELECT CampusId, CampDescrip ")
            .Append("FROM syCampuses ")
            .Append("WHERE StatusId = ? ")
        End With
        db.AddParameter("@StatusId", StatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "CampusList")
        Catch ex As System.Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetProgramList(ByVal CampusId As Guid, ByVal StatusId As Guid) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strSQLString As New StringBuilder
        Dim da As New OleDbDataAdapter
        db.OpenConnection()
        Dim a As String = CampusId.ToString
        Dim b As String = StatusId.ToString
        With strSQLString
            .Append("SELECT a.PrgVerId, a.PrgVerDescrip FROM arPrgVersions a, syCmpGrpCmps c ")
            .Append("WHERE  ")
            .Append("a.StatusId = ? AND ")
            .Append("a.CampGrpId = c.CampGrpId AND ")
            .Append("c.CampusId = ?")
        End With
        db.AddParameter("@StatusId", StatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "ProgramList")
        Catch ex As System.Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetAllStudentList(ByVal CampusId As Guid) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strSQLString As New StringBuilder
        Dim da As New OleDbDataAdapter
        db.OpenConnection()
        With strSQLString
            .Append("SELECT Distinct a.StuEnrollId, b.FirstName, b.MiddleName, b.LastName ")
            .Append("FROM arStuEnrollments a, arStudent b ")
            .Append("WHERE a.StudentId = b.StudentId AND ")
            .Append("a.CampusId = ?")
        End With
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "StudentList")
        Catch ex As System.Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetClassSectionList(ByVal CampusId As Guid, ByVal EmpId As Guid) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strSQLString As New StringBuilder
        Dim da As New OleDbDataAdapter
        db.OpenConnection()
        With strSQLString
            .Append("SELECT ClsSectionId, ClsSection ")
            .Append("FROM arClassSections ")
            .Append("WHERE CampusId = ? AND ")
            .Append("EmpId = ?")
        End With
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@EmpId", EmpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "ClassSectionList")
        Catch ex As System.Exception

        End Try
        strSQLString.Remove(0, strSQLString.Length)
        db.CloseConnection()
        Return ds
    End Function
    'Public Function GetHolidays(ByVal StartDate As DateTime, Optional ByVal SelectStatement As String = "") As DataSet
    '    Dim db As New DataAccess
    '    Dim ds As New DataSet
    '    Dim strSQLString As New StringBuilder
    '    Dim da As New OleDbDataAdapter
    '    db.OpenConnection()
    '    If SelectStatement = "" Then
    '        With strSQLString
    '            .Append("SELECT CalId, CalDescrip AS Description, StartDate AS 'Start Date', StartTime AS 'Start Time', EndDate AS 'End Date' , EndTime AS 'End Time' FROM arCalendar ")
    '            .Append("WHERE ? >= StartDate AND ? <= EndDate")
    '        End With
    '        db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '        db.AddParameter("@StartDate2", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '    Else
    '        With strSQLString
    '            .Append("SELECT CalId, CalDescrip AS Description, StartDate AS 'Start Date', StartTime AS 'Start Time', EndDate AS 'End Date' , EndTime AS 'End Time' FROM arCalendar ")
    '            .Append("WHERE ? >= StartDate AND ? <= EndDate")
    '            .Append(" UNION ")
    '            .Append("SELECT CalId, CalDescrip AS Description, StartDate AS 'Start Date', StartTime AS 'Start Time', EndDate AS 'End Date' , EndTime AS 'End Time' FROM arCalendar ")
    '            .Append("WHERE ? >= StartDate AND ? <= EndDate")
    '            .Append(" UNION ")
    '            .Append("SELECT CalId, CalDescrip AS Description, StartDate AS 'Start Date', StartTime AS 'Start Time', EndDate AS 'End Date' , EndTime AS 'End Time' FROM arCalendar ")
    '            .Append("WHERE ? >= StartDate AND ? <= EndDate")
    '            .Append(" UNION ")
    '            .Append("SELECT CalId, CalDescrip AS Description, StartDate AS 'Start Date', StartTime AS 'Start Time', EndDate AS 'End Date' , EndTime AS 'End Time' FROM arCalendar ")
    '            .Append("WHERE ? >= StartDate AND ? <= EndDate")
    '            .Append(" UNION ")
    '            .Append("SELECT CalId, CalDescrip AS Description, StartDate AS 'Start Date', StartTime AS 'Start Time', EndDate AS 'End Date' , EndTime AS 'End Time' FROM arCalendar ")
    '            .Append("WHERE ? >= StartDate AND ? <= EndDate")
    '            .Append(" UNION ")
    '            .Append("SELECT CalId, CalDescrip AS Description, StartDate AS 'Start Date', StartTime AS 'Start Time', EndDate AS 'End Date' , EndTime AS 'End Time' FROM arCalendar ")
    '            .Append("WHERE ? >= StartDate AND ? <= EndDate")
    '            .Append(" UNION ")
    '            .Append("SELECT CalId, CalDescrip AS Description, StartDate AS 'Start Date', StartTime AS 'Start Time', EndDate AS 'End Date' , EndTime AS 'End Time' FROM arCalendar ")
    '            .Append("WHERE ? >= StartDate AND ? <= EndDate")
    '        End With
    '        db.AddParameter("@StartDate1", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '        db.AddParameter("@StartDate2", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '        db.AddParameter("@StartDate3", StartDate.AddDays(1), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '        db.AddParameter("@StartDate4", StartDate.AddDays(1), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '        db.AddParameter("@StartDate5", StartDate.AddDays(2), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '        db.AddParameter("@StartDate6", StartDate.AddDays(2), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '        db.AddParameter("@StartDate7", StartDate.AddDays(3), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '        db.AddParameter("@StartDate8", StartDate.AddDays(3), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '        db.AddParameter("@StartDate9", StartDate.AddDays(4), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '        db.AddParameter("@StartDate10", StartDate.AddDays(4), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '        db.AddParameter("@StartDate11", StartDate.AddDays(5), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '        db.AddParameter("@StartDate12", StartDate.AddDays(5), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '        db.AddParameter("@StartDate13", StartDate.AddDays(6), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '        db.AddParameter("@StartDate14", StartDate.AddDays(6), DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '    End If
    '    Try
    '        da = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da.Fill(ds, "HolidayList")
    '    Catch ex As System.Exception
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Dim a As Int32 = ds.Tables("HolidayList").Rows.Count
    '    strSQLString.Remove(0, strSQLString.Length)
    '    db.ClearParameters()

    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    Return ds
    'End Function
    Public Function GetProgramClassGroupList(ByVal CampusId As Guid, ByVal StatusId As Guid) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strSQLString As New StringBuilder
        Dim da As New OleDbDataAdapter
        db.OpenConnection()
        With strSQLString
            ' Get the Program Versions for the Campus Selected
            .Append("SELECT PrgVerId, PrgVerDescrip from arPrgVersions ")
            .Append("WHERE CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps ")
            .Append("WHERE CampusId = ? AND ")
            .Append("StatusId = ?)")

        End With
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@StatusId", StatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "ProgramList")
        Catch ex As System.Exception
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)
        db.ClearParameters()
        ' Get the Class Sections for the Campus Selected
        With strSQLString
            .Append("SELECT ClsSectionId, ClsSection FROM arClassSections ")
            .Append("WHERE ReqId IN ")
            .Append("(SELECT ReqId FROM arReqs WHERE CampGrpId IN ")
            .Append("(SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId = ? ))")
        End With
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "ClassList")
        Catch ex As System.Exception
            Throw New Exception(ex.Message, ex)
        End Try

        strSQLString.Remove(0, strSQLString.Length)
        db.ClearParameters()
        ' Get the Student List for the Campus Selected
        With strSQLString
            .Append("SELECT StudentId, FirstName, MiddleName, LastName FROM arStudent ")
            .Append("WHERE CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId = ?)")
        End With
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "StudentList")
        Catch ex As System.Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Dim a As Int32 = ds.Tables("StudentList").Rows.Count
        strSQLString.Remove(0, strSQLString.Length)
        db.CloseConnection()
        Return ds
    End Function
    'Public Function GetAllSelectedStudentsVertical(ByVal PostingOptions As AtPostingOptionsInfo, ByVal QueryString As String, ByVal studentlisting As DataTable) As DataSet
    '    ' Get the attendance records for the students
    '    Dim db As New DataAccess
    '    Dim ds As New DataSet
    '    Dim strSQLString As New StringBuilder
    '    Dim da As New OleDbDataAdapter
    '    db.OpenConnection()
    '    With strSQLString
    '        .Append("SELECT a.AttendanceId, a.Actual, a.AttendanceDate, b.stuEnrollId, b.EnrollmentId, c.SSN,  c.StudentId, c.FirstName, c.MiddleName, c.LastName FROM atAttendance a, arStuEnrollments b, arStudent c ")
    '        .Append("WHERE b.stuEnrollId IN ( ")
    '        .Append(QueryString)
    '        .Append(") AND ")
    '        .Append("a.EnrollId = b.stuEnrollId AND ")
    '        .Append("b.StudentId = c.StudentId AND ")
    '        .Append("a.AttendanceTypeId = ? AND ")
    '        .Append("a.AttendanceDate IN (")
    '        .Append(PostingOptions.PostingPeriod)
    '        .Append(") ")
    '        If PostingOptions.AttendanceType <> "Student" Then
    '            .Append("AND ")
    '        End If
    '    End With
    '    Select Case PostingOptions.AttendanceType
    '        Case "Program"
    '            With strSQLString
    '                .Append("a.AttendanceType = 'P'")
    '            End With
    '        Case "Class Section"
    '            With strSQLString
    '                .Append("a.AttendanceType = 'C'")
    '            End With
    '        Case "Student"
    '            ' Do not restrict what types of record -- we have to go by the students schedule to post.
    '    End Select
    '    Dim a As String = strSQLString.ToString
    '    db.AddParameter("@SelectedGuid", PostingOptions.SelectedGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    Try
    '        da = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da.Fill(ds, "StudentAttendanceList")
    '    Catch ex As System.Exception
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    ' add the student listing table to the vertical results dataset, you will need it later to
    '    ' create dummy records.
    '    ds.Tables.Add(studentlisting)
    '    strSQLString.Remove(0, strSQLString.Length)
    '    db.ClearParameters()

    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    Return ds
    'End Function
    'Public Function GetAllStudents(ByVal PostingOptions As AtPostingOptionsInfo) As DataSet
    '    Dim db As New DataAccess
    '    Dim dt As New DataTable
    '    Dim strSQLString As New StringBuilder
    '    Dim da As New OleDbDataAdapter
    '    db.OpenConnection()
    '    Select Case PostingOptions.AttendanceType
    '        Case "Program"
    '            With strSQLString
    '                .Append("SELECT a.StuEnrollId, a.EnrollmentId, a.StudentId, b.FirstName, b.MiddleName, b.LastName, b.SSN, c.PrgVerId FROM arStuEnrollments a, arStudent b, arPrgVersions c ")
    '                .Append("WHERE a.StudentId = b.StudentId AND ")
    '                .Append("a.PrgVerId = ? AND ")
    '                .Append("a.PrgVerId = c.PrgVerId  AND ")
    '                .Append("b.StudentStatus = ? AND ")
    '                .Append("c.AttendanceLevel = 0")
    '            End With
    '            db.AddParameter("@PrgVersionId", PostingOptions.SelectedGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '            db.AddParameter("@StudentStatus", PostingOptions.ActiveStudentStatus, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)

    '        Case "ClassSection"
    '            With strSQLString
    '                .Append("SELECT DISTINCT a.StudentId, b.StudentStatus, a.PrgVerId, e.ClsSectionId, a.CampusId, a.StuEnrollId, a.EnrollmentId, b.FirstName, b.LastName, b.MiddleName, b.SSN ")
    '                .Append("FROM arStuEnrollments a, arStudent b, arPrgVersions c, arProgVerDef d, arClassSections e ")
    '                .Append("WHERE a.StudentId = b.StudentId AND ")
    '                .Append("b.StudentStatus = ? AND ")
    '                .Append("a.PrgVerId = c.PrgVerId AND ")
    '                .Append("c.PrgVerId = d.PrgVerId AND ")
    '                .Append("c.AttendanceLevel = 1 AND ")
    '                .Append("d.ReqId = e.ReqId AND ")
    '                .Append("e.ClsSectionId = ? ")
    '            End With
    '            db.AddParameter("@StudentStatus", PostingOptions.ActiveStudentStatus, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '            db.AddParameter("@ClsSectionId", PostingOptions.SelectedGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '        Case "Student"
    '            With strSQLString
    '                .Append("SELECT StuEnrollId FROM arStuEnrollments WHERE ")
    '                .Append("studentid = ?")
    '            End With
    '            db.AddParameter("@StudentId", PostingOptions.SelectedGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    End Select
    '    Dim ConcatenatedStudents As String
    '    Try
    '        da = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da.Fill(dt)
    '    Catch ex As System.Exception
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Dim bb As Int32 = dt.Rows.Count
    '    dt.TableName = "StudentListing"
    '    ConcatenatedStudents = CreateStudentList(dt)

    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    ' Check to see if the user selected to Include Inactive Students
    '    Dim ds2 As New DataSet
    '    If ConcatenatedStudents <> "" Then
    '        ' Now that we have the list of students, we now need to get their schedules
    '        ' Now that we have the schedules, we need to get the actual attendance records
    '        ds2 = GetAllSelectedStudentsVertical(PostingOptions, ConcatenatedStudents, dt)
    '        Return ds2
    '    Else
    '        ds2 = Nothing
    '        Return ds2
    '    End If
    'End Function

    Public Function GetClassSections(ByVal CampusId As Guid, ByVal StartDate As DateTime) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strSQLString As New StringBuilder
        Dim da As New OleDbDataAdapter
        db.OpenConnection()
        With strSQLString
            .Append("SELECT ClsSectionId, ClsSection FROM arClassSections ")
            .Append("WHERE ")
            .Append("ReqId IN ")
            .Append("(SELECT ReqId FROM arReqs WHERE CampGrpId IN ")
            .Append("(SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId = ? ))")
            If StartDate > #1/1/1900# Then
                .Append(" AND StartDate = ?")
            End If
        End With
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        If StartDate > #1/1/1900# Then
            db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        End If
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "ClassList")
        Catch ex As System.Exception
            Throw New Exception(ex.Message, ex)
        End Try

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    Public Function CreateStudentList(ByVal dt As DataTable) As String
        Dim row As DataRow
        Dim querystring As New StringBuilder
        For Each row In dt.Rows
            With querystring
                .Append("'")
                .Append(row("StuEnrollId"))
                .Append("',")
            End With
        Next
        If dt.Rows.Count > 0 Then
            querystring.Replace(",", " ", querystring.Length - 1, 1)
        Else
            'return nothing
        End If
        Return querystring.ToString
    End Function

End Class
