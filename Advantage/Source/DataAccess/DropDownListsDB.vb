Imports FAME.Advantage.Common

Public Class DropDownListsDB
#Region "Private Variables"
    Private connectionName As String = "ConnectionString"
#End Region
#Region "Public Methods"
    Public Function GetDropDownLists(ByVal ddlList As System.Collections.Generic.List(Of AdvantageDropDownListMetadata)) As DataSet

        'get an async connection to the SQL server
        Using sqlConnection As New System.Data.SqlClient.SqlConnection(ChangeConnectionStringMode(GetSqlConnectionString(connectionName), True))
            'open SQL connection
            sqlConnection.Open()

            'if SQL server supports MARS then use Async methods, else use standard sync method
            'This line at times throws "Failed to establish a MARS session in preparation to send the request to the server" error
            'Currently disabling MARS 6/8/2012
            'If SQLServerSupportsMARS(sqlConnection.ServerVersion) Then
            '    Return GetDropDownListsAsynchrosnously(ddlList, sqlConnection)
            'Else
            '    Return GetDropDownListsSynchrosnously(ddlList, sqlConnection)
            'End If

            Return GetDropDownListsSynchrosnously(ddlList, sqlConnection)
        End Using

    End Function
#End Region
#Region "Private Methods"
    Private Function BuildDatasetFromDataReader(ByVal advDropDownListMetadatas() As AdvantageDropDownListMetadata, ByVal dataReader As System.Data.SqlClient.SqlDataReader) As DataSet
        Dim ds As New DataSet("DropDownLists")

        For i As Integer = 0 To advDropDownListMetadatas.Length - 1

            'create table and assign name
            Dim dt As New DataTable(advDropDownListMetadatas(i).DropDownListName)

            'add columns ValueField and TextField
            dt.Columns.Add()
            dt.Columns.Add()
            dt.Columns.Add()

            'populate table
            While dataReader.Read
                Dim row As DataRow = dt.NewRow()
                row(0) = dataReader.Item(0)
                row(1) = dataReader.Item(1)
                row(2) = dataReader.Item(2)
                dt.Rows.Add(row)
            End While
            
            'ignore duplicated tables
            If ds.Tables.Contains(dt.TableName) = False Then
                ds.Tables.Add(dt)
            End If
            'Try
            'Catch ex As Exception

            'End Try
            dataReader.NextResult()
        Next
        dataReader.Close()

        'return dataset
        Return ds
    End Function
    Private Function GetDropDownListsAsynchrosnously(ByVal ddlList As System.Collections.Generic.List(Of AdvantageDropDownListMetadata), ByVal sqlConnection As System.Data.SqlClient.SqlConnection) As DataSet
        Dim ds As New DataSet("DropDownLists")

        'send all SQL queries to the server 
        Dim iAsResults(ddlList.Count - 1) As IAsyncResult
        Dim sqlCommands(ddlList.Count - 1) As System.Data.SqlClient.SqlCommand
        Dim waitHandles(ddlList.Count - 1) As System.Threading.WaitHandle
        Dim advDropDownListMetadatas(ddlList.Count - 1) As AdvantageDropDownListMetadata
        Dim i As Integer = 0
        For Each item As AdvantageDropDownListMetadata In ddlList
            advDropDownListMetadatas(i) = item
            sqlCommands(i) = New System.Data.SqlClient.SqlCommand(BuildSQLCommand(advDropDownListMetadatas(i)), sqlConnection)
            'execute sql command
            iAsResults(i) = sqlCommands(i).BeginExecuteReader()
            waitHandles(i) = iAsResults(i).AsyncWaitHandle
            i += 1
        Next

        'wait until all sql statements are completed
        System.Threading.WaitHandle.WaitAll(waitHandles)

        'build output dataset
        For j As Integer = 0 To iAsResults.Length - 1
            'wait until SQL statement ends
            Dim dr As System.Data.SqlClient.SqlDataReader = sqlCommands(j).EndExecuteReader(iAsResults(j))
            Try
                'ignore duplicated tables
                ds.Tables.Add(BuildTableFromDataReader(advDropDownListMetadatas(j), dr))
            Catch ex As Exception

            End Try
        Next
        'return dataset
        Return ds
    End Function
    Private Function GetDropDownListsSynchrosnously(ByVal ddlList As System.Collections.Generic.List(Of AdvantageDropDownListMetadata), ByVal sqlConnection As System.Data.SqlClient.SqlConnection) As DataSet

        'build one SQL statement with all queries 
        Dim advDropDownListMetadatas(ddlList.Count - 1) As AdvantageDropDownListMetadata
        Dim i As Integer = 0
        Dim sb As New StringBuilder
        For Each item As AdvantageDropDownListMetadata In ddlList
            advDropDownListMetadatas(i) = item
            sb.Append(BuildSQLCommand(advDropDownListMetadatas(i)))
            sb.Append(";")
            i += 1
        Next

        'this is the sql command
        Dim sqlCommand As New System.Data.SqlClient.SqlCommand(sb.ToString, sqlConnection)

        'Return output dataset
        Return BuildDatasetFromDataReader(advDropDownListMetadatas, sqlCommand.ExecuteReader())

    End Function
    Private Function SQLServerSupportsMARS(ByVal serverVersion As String) As Boolean
        'only SQL 2005 or later supports MARS (Sql version 09.xx.xxxx)
        If Integer.Parse(serverVersion.Substring(0, 2)) >= 9 Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Function BuildSQLCommand(ByVal advDropDownListMetadata As AdvantageDropDownListMetadata) As String
        'Dim ddlName As String = advDropDownListMetadata.DropDownListName
        'return Override SQL Statement
        Dim strSql As String = advDropDownListMetadata.OverrideSqlStatement
        If strSql IsNot Nothing Then Return strSql

        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append(advDropDownListMetadata.ValueField)
            .Append(",")
            .Append(advDropDownListMetadata.TextField)
            .Append(",")
            .Append("(Case S.Status when 'Active' then 1 else 0 end) As Status ")
            .Append(" FROM ")
            .Append(advDropDownListMetadata.TableName)
            If advDropDownListMetadata.PullActiveItemsOnly Then
                .Append(" T, syStatuses S WHERE T.StatusId=S.StatusId AND S.Status='Active' ")
                If Not advDropDownListMetadata.CampusId Is Nothing Then
                    .Append("AND CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='")
                    .Append(advDropDownListMetadata.CampusId)
                    .Append("')")
                End If
            Else
                .Append(" T, syStatuses S WHERE T.StatusId=S.StatusId ")
                If Not advDropDownListMetadata.CampusId Is Nothing Then
                    .Append("AND CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='")
                    .Append(advDropDownListMetadata.CampusId)
                    .Append("') ")
                End If
            End If
            .Append(" ORDER BY Status, ")
            .Append(advDropDownListMetadata.TextField)
        End With

        'return sql command
        Return sb.ToString
    End Function
    Private Function GetSqlConnectionString(ByVal ConnectionName As String) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Return MyAdvAppSettings.AppSettings(ConnectionName).ToString
    End Function

    Private Function BuildTableFromDataReader(ByVal advDropDownListMetadata As AdvantageDropDownListMetadata, ByVal dataReader As System.Data.SqlClient.SqlDataReader) As DataTable
        'create table and assign name
        Dim dt As New DataTable(advDropDownListMetadata.DropDownListName)

        dt.Columns.Add()
        dt.Columns.Add()
        dt.Columns.Add()

        'populate table
        While dataReader.Read
            Dim row As DataRow = dt.NewRow()
            row(0) = dataReader.Item(0)
            row(1) = dataReader.Item(1)
            row(2) = dataReader.Item(2)
            If LCase(Trim(row(0))) = "aa4f6ff8-491d-49b4-8902-4e7a59d20d42" Then 'If Race is NonResident Alien do not add to table
            Else
                dt.Rows.Add(row)
            End If
        End While
        'close datareader
        dataReader.Close()

        'return table
        Return dt
    End Function
    Private Function ChangeConnectionStringMode(ByVal connectionString As String, ByVal Sync As Boolean) As String

        'set Async key to "false" in the connection string
        connectionString = AddOrReplaceKeyInConnectionString(connectionString, "Async", Sync)

        'set MultipleActiveResultSets key to "false" in the connection string
        connectionString = AddOrReplaceKeyInConnectionString(connectionString, "MultipleActiveResultSets", Sync)

        'return updated connection string
        Return connectionString

    End Function
    Private Function AddOrReplaceKeyInConnectionString(ByVal connectionString As String, ByVal key As String, ByVal value As Boolean) As String
        Dim startPosition As Integer = connectionString.IndexOf(key, StringComparison.InvariantCultureIgnoreCase)
        Dim endPosition As Integer = connectionString.Length

        'if key exists then do a replace
        If startPosition >= 0 Then
            'find first ";" delimiter after the key
            If connectionString.IndexOf(";"c, startPosition) >= 0 Then endPosition = connectionString.IndexOf(";"c, startPosition)

            'remove existing key and value
            connectionString = connectionString.Remove(startPosition, endPosition - startPosition + 1)

        Else
            'if key doesn't exist then do an insert at the begining the string
            startPosition = 0
        End If

        'insert new key with value
        connectionString = connectionString.Insert(startPosition, key + "=" + value.ToString + ";")

        'return updated connection string
        Return connectionString
    End Function
#End Region
End Class


