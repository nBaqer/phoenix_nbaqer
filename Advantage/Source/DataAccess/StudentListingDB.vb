Imports System.XML

Public Class StudentListingDB
    Public Function GetRoleList(ByVal EmpId As Guid) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strSQL As String
        Dim strMessageListString As New StringBuilder
        'strSQL = GetPhoneList()
        With strMessageListString
            .Append("SELECT a.RoleId, b.Role, a.EmpId ")
            .Append(strSQL)
            .Append(" from hrEmpRoles a, syRoles b ")
            .Append(" where a.RoleId = b.RoleId")
            .Append(" AND a.EmpId = ?")
        End With
        db.AddParameter("@EmpId", EmpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.OpenConnection()

        Try
            da = db.RunParamSQLDataAdapter(strMessageListString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
        End Try
        da.Fill(ds, "RoleList")
        db.CloseConnection()
        db.ClearParameters()
        Return ds
    End Function
    Public Function GetPhoneListForStudent(ByVal StudentId As Guid) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strSQL As New StringBuilder

        With strSQL
            .Append("SELECT a.phone, b.PhoneTypeDescrip, b.Sequence FROM arStudentPhone a, syPhoneType b")
            .Append(" WHERE a.PhoneTypeId = b.PhoneTypeId")
            .Append(" and a.StudentId = ?")
        End With
        db.AddParameter("@studentId", StudentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
        End Try
        da.Fill(ds, "PhoneList")
        db.CloseConnection()
        db.ClearParameters()
        Return ds
    End Function

    Public Function GetStudentList(ByVal EmpId As Guid, Optional ByVal RoleId As String = Nothing, Optional ByVal ds As DataSet = Nothing) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim ds2 As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strMessageListString As New StringBuilder
        If RoleId = Nothing Then
            With strMessageListString
                .Append("SELECT DISTINCT a.StudentId, b.FirstName, b.LastName from cmRoleEmpStudent a, arStudent b")
                .Append(" WHERE a.StudentId = b.StudentId")
                .Append(" AND a.EmpId = ?")
            End With
            db.AddParameter("@EmpId", EmpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Else
            With strMessageListString
                .Append("SELECT a.StudentId, b.FirstName, b.LastName from cmRoleEmpStudent a, arStudent b")
                .Append(" WHERE a.StudentId = b.StudentId")
                .Append(" AND a.RoleId = ?")
                .Append(" AND a.EmpId = ?")
            End With
            db.AddParameter("@RoleId", XmlConvert.ToGuid(RoleId), DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            db.AddParameter("@EmpId", EmpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        End If
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strMessageListString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
        End Try
        If ds Is Nothing Then
            da.Fill(ds2, "StudentList")
            db.CloseConnection()
            db.ClearParameters()
            Return ds2
        Else
            da.Fill(ds, "StudentList")
            db.CloseConnection()
            db.ClearParameters()
            Return ds
        End If
    End Function
End Class
