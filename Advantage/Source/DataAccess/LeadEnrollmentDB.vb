
Imports System.IO
Imports FAME.Advantage.Common

Public Class LeadEnrollmentDB
    Public Function GetLeads(LeadID As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim ds As DataSet
        'Dim da6 As New OleDbDataAdapter

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append(" Select B.EnrollmentId,A.StudentID,A.FirstName,A.MiddleName,A.LastName, ")
            .Append(" B.PrgVerId,(Select PrgVerDescrip from arPrgVersions where PrgVerId=B.PrgVerId) as ProgramDescrip ")
            .Append(" FROM arStudent A,arStuEnrollments B  ")
            .Append(" where A.StudentId = B.StudentId ")
            .Append(" and A.StudentID in ('")
            .Append(LeadID)
            .Append("')")
        End With
        ''  Dim strLead As String
        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    'Public Function InsertLeadsToStudent(selectedLead() As String, EnrollmentId() As String, User As String) As Integer
    '    'connect to the database
    '    Dim db As New DataAccess
    '    'Dim ds As New DataSet
    '    'Dim da6 As New OleDbDataAdapter
    '    Dim intLeadCount As Integer
    '    Dim x As Integer
    '    Dim sb1 As New StringBuilder
    '    Dim leadinfo As New LeadMasterInfo

    '    Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
    '    intLeadCount = selectedLead.Length()

    '    db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
    '    Dim strStudentStatus As String = "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965"

    '    Dim sb As New StringBuilder
    '    Dim sb2 As New StringBuilder
    '    'Dim sb3 As New StringBuilder

    '    'build the sql query
    '    With sb
    '        'With subqueries
    '        .Append(" select Distinct StatusCodeId from syStatusCodes where SysStatusId in ")
    '        .Append(" (Select SysStatusId from sySysStatus where SysStatusDescrip  like 'Enroll%')   ")
    '    End With

    '    'Execute the query
    '    Dim dr45 As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
    '    Dim strEnrollmentStatus As String
    '    While dr45.Read()
    '        'set properties with data from DataReader
    '        strEnrollmentStatus = CType(dr45("StatusCodeId"), Guid).ToString
    '    End While

    '    If Not dr45.IsClosed Then dr45.Close()


    '    db.ClearParameters()
    '    sb.Remove(0, sb.Length)

    '    'build the sql query
    '    While x < intLeadCount
    '        With sb

    '            .Append(" Insert Into  arStudent(StudentID,firstname,lastname,middlename,StudentStatus,ModUser,ModDate,EnrollmentId) ")
    '            .Append(" Select LeadId,firstname,lastname,middlename,?,?,?,? from adLeads ")
    '            .Append(" where LeadID = ? ")

    '            db.AddParameter("@StudentStatus", strStudentStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@EnrollmentId", DirectCast(EnrollmentId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@LeadID", DirectCast(selectedLead.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '            db.ClearParameters()
    '            sb.Remove(0, sb.Length)

    '            '  Dim sb1 As New StringBuilder
    '            With sb1
    '                .Append(" Insert into arStuEnrollments(StuEnrollId,StudentId,EnrollDate,PrgVerId,StartDate,ExpStartDate,")
    '                .Append(" MidPtDate,ExpGradDate,TransferDate,ShiftId,GrdLvlId,BillingMethodId,CampusId,StatusCodeId, ")
    '                .Append(" EnrollmentId,AdmissionsRep,AcademicAdvisor,TuitionCategoryId,CohortStartDate) ")
    '                .Append(" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
    '            End With
    '            db.AddParameter("@StuEnrollId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@StudentID", DirectCast(selectedLead.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@EnrollDate", leadinfo.EnrollmentDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            db.AddParameter("@PrgVerId", leadinfo.PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            If leadinfo.StartDate = "" Then
    '                db.AddParameter("@StartDate", leadinfo.ExpectedStart, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            Else
    '                db.AddParameter("@StartDate", leadinfo.StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            End If
    '            db.AddParameter("@ExpStartDate", leadinfo.ExpectedStart, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            db.AddParameter("@MidPtDate", leadinfo.MidPointDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            db.AddParameter("@ExpGradDate", leadinfo.ExpGradDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            db.AddParameter("@TransferDate", leadinfo.TransferDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            db.AddParameter("@ShiftId", leadinfo.ShiftID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@GrdLvlId", leadinfo.GradeLevel, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@BillingMethodId", leadinfo.ChargingMethod, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@CampusId", leadinfo.CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@StatusCodeId", leadinfo.StatusCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@EnrollmentId", DirectCast(EnrollmentId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@AdmissionsRep", leadinfo.AdmissionsRep, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@AcademicAdvisor", leadinfo.AcademicAdvisor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@TuitionCategoryId", leadinfo.TuitionCategory, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@CohortStartDate", leadinfo.ExpectedStart, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '            db.RunParamSQLExecuteNoneQuery(sb1.ToString)
    '            db.ClearParameters()
    '            sb1.Remove(0, sb1.Length)

    '            Dim sb4 As New StringBuilder
    '            With sb4
    '                .Append(" Insert into arStudAddresses(StudentId,Address1,Address2,")
    '                .Append(" City,StateId,Zip,CountryId,AddressTypeId,ModUser,ModDate,StatusId) ")
    '                .Append(" select LeadId,Address1,Address2,City,State,Zip,Country,AddressType,?,?,? from adLeads ")
    '                .Append(" where LeadId = ? ")
    '            End With
    '            'db.AddParameter("@StdAddressId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            db.AddParameter("@StatusId", strStudentStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@LeadID", DirectCast(selectedLead.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '            db.RunParamSQLExecuteNoneQuery(sb4.ToString)
    '            db.ClearParameters()
    '            sb4.Remove(0, sb4.Length)


    '            Dim sb5 As New StringBuilder
    '            With sb5
    '                .Append(" Insert into arStudentPhone(StudentId,PhoneTypeId,Phone,")
    '                .Append(" StatusId,ModUser,ModDate) ")
    '                .Append(" select LeadId,PhoneType,Phone,?,?,? from adLeads where LeadId=? ")
    '            End With
    '            db.AddParameter("@StatusId", strStudentStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            db.AddParameter("@LeadID", DirectCast(selectedLead.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.RunParamSQLExecuteNoneQuery(sb5.ToString)
    '            db.ClearParameters()
    '            sb5.Remove(0, sb5.Length)

    '            Dim sb6 As New StringBuilder
    '            With sb6
    '                .Append(" Insert into plStudentEducation(StudentId,EducationInstId,EducationInstType,GraduatedDate,FinalGrade,CertificateId,Comments,Major,ModUser,ModDate) ")
    '                .Append(" Select LeadId,EducationInstId,EducationInstType,GraduatedDate,FinalGrade,CertificateId,Comments,Major,?,? from adLeadEducation ")
    '                .Append(" where LeadId = ? ")
    '            End With
    '            db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            db.AddParameter("@LeadID", DirectCast(selectedLead.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.RunParamSQLExecuteNoneQuery(sb6.ToString)
    '            db.ClearParameters()
    '            sb6.Remove(0, sb6.Length)

    '            Dim sb7 As New StringBuilder
    '            Dim strGuid = Guid.NewGuid.ToString
    '            With sb7
    '                .Append(" Insert into plStudentEmployment(StudentId,EmployerId,JobTitleId,JobStatusId,StartDate,EndDate,Comments,ModUser,ModDate) ")
    '                .Append(" Select LeadId,EmployerId,JobTitleId,JobStatusId,StartDate,EndDate,Comments,?,? from adLeadEmployment ")
    '                .Append(" where LeadId = ? ")
    '            End With
    '            'db.AddParameter("@StEmploymentId", strGuid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            db.AddParameter("@LeadID", DirectCast(selectedLead.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.RunParamSQLExecuteNoneQuery(sb7.ToString)
    '            db.ClearParameters()
    '            sb7.Remove(0, sb7.Length)


    '            With sb2
    '                .Append("Update adLeads set LeadStatus=? where LeadId=? ")
    '            End With
    '            db.AddParameter("@LeadStatus", strEnrollmentStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '            db.AddParameter("@LeadID", DirectCast(selectedLead.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '            db.RunParamSQLExecuteNoneQuery(sb2.ToString)
    '            db.ClearParameters()
    '            sb2.Remove(0, sb2.Length)
    '        End With
    '        x = x + 1
    '    End While
    '    Return 0
    'End Function
    Public Function GenerateEnrollmentID(strLastName As String, strFirstName As String) As String
        Dim sequence As Singleton
        Dim i, f, g, seqdiff, k, No_Seq As String
        Dim l, j, difference As Integer
        Dim lLl, l_F, l_YY, l_MM, l_DD, l_Sequence As String

        'For Demo
        'l_LL = Mid(strLastName, 1, txtFormatLast.Text).ToUpper
        'l_F = Mid(strFirstName, 1, txtFormatFirst.Text).ToUpper


        lLl = Mid(strLastName, 1, 2).ToUpper
        l_F = Mid(strFirstName, 1, 1).ToUpper

        l_YY = CDate(Now()).Year
        l_YY = Mid(l_YY, 1, 4)


        l_MM = CDate(Now()).Month
        l_MM = Mid(l_YY, 1, 1)

        l_DD = CDate(Now()).Day
        l_DD = Mid(l_YY, 1, 1)

        l_Sequence = 2

        SyncLock sequence
            i = sequence.GetStudentSequence()
            g = Len(i) 'Returns Length Of Sequence Number
            seqdiff = CInt(l_Sequence) - CInt(g)  'Get The Difference Between Length Of Sequence Number and Sequence Number Format
            If seqdiff > 0 Then
                For l = 1 To seqdiff
                    k += "0"
                Next
                i = k + i
            End If
            'For Demo Purpose 
            difference = 2
            f = Mid(i, 1, l_Sequence)
            If difference >= 1 Then
                For j = 1 To difference
                    No_Seq += "0"
                Next
                f = No_Seq & f
            End If
            'Changed For Demo By Balaji on 08/04/2004
            Dim enrollmentId As String
            enrollmentId = StudentIDFormat(lLl, l_F, l_YY, l_MM, l_DD, f)
            Return enrollmentId
        End SyncLock
    End Function

    Public Function StudentIDFormat(Optional ByVal LL As String = "Null", Optional ByVal F As String = "Null", Optional ByVal YY As String = "Null", Optional ByVal MM As String = "Null", Optional ByVal DD As String = "Null", Optional ByVal SequenceID As String = "Null") As String
        'LL ---- First Two Letters Of Students LastName
        'F  ---- First Letter Of Students FirstName
        'YY ---- First Two Digits Of Lead Entry Year
        'MM ---- Month Of Lead Entry Year
        'DD ---- Day Of Lead Entry
        'IncrNUmber ---- Last Four Digits Of The System Generated Sequential Number
        'Dim StudentID As String
        'Dim StudentLastName As String
        'Dim StudentFirstName As String
        'Set Values For Demo
        Return YY & MM & DD & LL & F & SequenceID
    End Function
    Public Function GetStudentName(StudentID As String) As String

        'connect to the database
        Dim db As New DataAccess
        Dim strFirstName, strLastName As String

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With sub queries
            .Append("SELECT    FirstName,LastName from arStudent where StudentId = ? ")

        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@StudentId", StudentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        ''''Dim Extracurricularinfo As New PlacementInfo

        While dr.Read()

            'set properties with data from DataReader
            strFirstName = dr("FirstName").ToString()
            strLastName = dr("LastName").ToString()

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return strFirstName & " " & strLastName
    End Function

    Public Function UpdateLead(leadinfo As LeadMasterInfo, user As String) As String

        'Build The Query
        Dim sb As New StringBuilder
        With sb
            .Append("Update adLeads SET  ")
            .Append(" StateId=@StateId, ")
            .Append(" BirthDate=@BirthDate, ")
            .Append(" Gender=@Gender, Race=@Race, ")
            .Append(" MaritalStatus=@MaritalStatus, ")
            .Append(" Nationality=@Nationality, Citizen=@Citizen, ")
            .Append(" ModUser=@ModUser, ModDate=@ModDate, ")
            .Append(" DependencyTypeId= @DependencyTypeId, GeographicTypeId=@GeographicTypeId, AdminCriteriaId=@AdminCriteriaId, HousingId=@HousingId, ")
            .Append(" DegCertSeekingId=@DegCertSeekingId, SSN=@SSN, FamilyIncome=@FamilyIncome, ")
            .Append(" IsDisabled=@IsDisabled,  ")
            .Append(" EntranceInterviewDate= @EntranceInterviewDate,  ")
            .Append(" IsFirstTimeInSchool= @IsFirstTimeInSchool,  ")
            .Append(" IsFirstTimePostSecSchool=@IsFirstTimePostSecSchool  ")
            .Append(" WHERE LeadID = @LeadID ")
        End With

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim constr As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim conn = New SqlConnection(constr)
        Dim command = New SqlCommand(sb.ToString(), conn)
        command.Parameters.AddWithValue("@StateId", If(leadinfo.State = String.Empty, DBNull.Value, leadinfo.State))
        command.Parameters.AddWithValue("@BirthDate", If(leadinfo.BirthDate = String.Empty, DBNull.Value, leadinfo.BirthDate))
        command.Parameters.AddWithValue("@Gender", If(leadinfo.Gender = String.Empty, DBNull.Value, leadinfo.Gender))
        command.Parameters.AddWithValue("@Race", If(leadinfo.Race = String.Empty, DBNull.Value, leadinfo.Race))
        command.Parameters.AddWithValue("@MaritalStatus", If(leadinfo.MaritalStatus = String.Empty, DBNull.Value, leadinfo.MaritalStatus))
        command.Parameters.AddWithValue("@Nationality", If(leadinfo.Nationality = String.Empty, DBNull.Value, leadinfo.Nationality))
        command.Parameters.AddWithValue("@Citizen", If(leadinfo.Citizen = String.Empty, DBNull.Value, leadinfo.Citizen))
        command.Parameters.AddWithValue("@ModUser", user)
        command.Parameters.AddWithValue("@ModDate", Date.Now)
        command.Parameters.AddWithValue("@DependencyTypeId", If(leadinfo.DependencyTypeId = String.Empty, DBNull.Value, leadinfo.DependencyTypeId))
        command.Parameters.AddWithValue("@GeographicTypeId", If(leadinfo.GeographicTypeId = String.Empty, DBNull.Value, leadinfo.GeographicTypeId))
        command.Parameters.AddWithValue("@AdminCriteriaId", If(leadinfo.AdminCriteriaId = String.Empty, DBNull.Value, leadinfo.AdminCriteriaId))
        command.Parameters.AddWithValue("@HousingId", If(leadinfo.HousingTypeId = String.Empty, DBNull.Value, leadinfo.HousingTypeId))
        command.Parameters.AddWithValue("@DegCertSeekingId", If(leadinfo.DegCertSeekingId = String.Empty, DBNull.Value, leadinfo.DegCertSeekingId))
        command.Parameters.AddWithValue("@SSN", If(leadinfo.SSN = String.Empty, DBNull.Value, leadinfo.SSN))

        command.Parameters.AddWithValue("@FamilyIncome", If(String.IsNullOrEmpty(leadinfo.FamilyIncome) Or leadinfo.FamilyIncome = (Guid.Empty).ToString(), DBNull.Value, leadinfo.FamilyIncome))
        command.Parameters.AddWithValue("@LeadID", leadinfo.LeadMasterID)

        command.Parameters.AddWithValue("@IsDisabled", If(leadinfo.IsDisabled = -1, DBNull.Value, leadinfo.IsDisabled))
        command.Parameters.AddWithValue("@EntranceInterviewDate", If(leadinfo.EntranceInterviewDate Is Nothing, DBNull.Value, leadinfo.EntranceInterviewDate))

        command.Parameters.AddWithValue("@IsFirstTimeInSchool", leadinfo.IsFirstTimeInSchool)
        command.Parameters.AddWithValue("@IsFirstTimePostSecSchool", leadinfo.IsFirstTimePostSecSchool)
        conn.Open()
        Try
            command.ExecuteNonQuery()
            Return String.Empty

        Finally
            conn.Close()
        End Try
    End Function



    ''' <summary>
    ''' Migrate the lead to enrollment
    ''' </summary>
    ''' <param name="leadinfo"></param>
    ''' <param name="user"></param>
    ''' <param name="stuEnrollmentId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Public Function AddLeadStudentEnrollment(leadinfo As LeadMasterInfo, user As String, Optional ByVal stuEnrollmentId As String = "") As String

    '    ''Connect To The Database
    '    Dim db As New DataAccess
    '    'Dim studentNotesDB As New StudentNotesDB

    '    Dim myTrans As OleDbTransaction
    '    Dim mycmd As OleDbCommand

    '    Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
    '    Dim myconn As New OleDbConnection(myAdvAppSettings.AppSettings("ConString"))
    '    myconn.Open()
    '    db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
    '    db.OpenConnection()

    '    Dim strStudentStatus As String = "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965"

    '    myTrans = myconn.BeginTransaction()
    '    Dim myTransDate As DateTime = Date.Now
    '    Try
    '        'Build The Query
    '        Dim sb As New StringBuilder

    '        'Update the lead fields...............................
    '        UpdateLead(leadinfo, user)

    '        ' Insert the Students Record.........................
    '        With sb
    '            .Append(" Insert into arStudent(StudentId,FirstName,LastName,MiddleName,ModUser,ModDate,StudentStatus,SSN,HomeEmail,Gender,DOB,Race,MaritalStatus,Children,FamilyIncome,Sponsor,AssignedDate,Nationality,Citizen,WorkEmail,Prefix,Suffix,EdLvlId,AlienNumber,DrivLicStateId,DrivLicNumber,Comments,DependencyTypeId,GeographicTypeId,admincriteriaid,housingid,County) ")
    '            .Append(" Select ?,FirstName,LastName,MiddleName,?,?,?,?,HomeEmail,Gender,BirthDate,Race,MaritalStatus,Children,?,Sponsor,AssignedDate,Nationality,Citizen,WorkEmail,Prefix,Suffix,PreviousEducation,AlienNumber,DrivLicStateId,DrivLicNumber,Comments,DependencyTypeId,GeographicTypeId,admincriteriaid,housingid,County from adLeads where LeadId=? ")
    '        End With
    '        mycmd = New OleDbCommand(sb.ToString, myconn, myTrans)

    '        mycmd.Parameters.AddWithValue("@StudentId", leadinfo.StudentId)
    '        mycmd.Parameters.AddWithValue("@ModUser", user)
    '        mycmd.Parameters.AddWithValue("@ModDate", myTransDate)
    '        mycmd.Parameters.AddWithValue("@StudentStatus", strStudentStatus)
    '        mycmd.Parameters.AddWithValue("@SSN", leadinfo.SSN)
    '        If String.IsNullOrEmpty(leadinfo.FamilyIncome) Or leadinfo.FamilyIncome = Guid.Empty.ToString() Then
    '            mycmd.Parameters.AddWithValue("@FamilyIncome", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@FamilyIncome", leadinfo.FamilyIncome)
    '        End If
    '        mycmd.Parameters.AddWithValue("@LeadId", leadinfo.LeadMasterID)
    '        mycmd.ExecuteNonQuery()
    '        mycmd.Parameters.Clear()
    '        sb.Remove(0, sb.Length)


    '        'Update Student Number (Student Id)
    '        Dim sb101 As New StringBuilder
    '        With sb101
    '            .Append(" update arStudent set StudentNumber=? where StudentId = ? ")
    '        End With
    '        mycmd.CommandText = sb101.ToString
    '        mycmd.Parameters.AddWithValue("@StudentNumber", leadinfo.StudentNumber)
    '        mycmd.Parameters.AddWithValue("@StudentId", leadinfo.StudentId)
    '        mycmd.ExecuteNonQuery()
    '        mycmd.Parameters.Clear()

    '        Dim sbStatusCode As New StringBuilder
    '        Dim strStuEnrollmentsStatus As String

    '        'we have used Top 1 in the query to avoid multiple future start statuscodes from being populated
    '        'the Top 1 will be removed once the validation has been added to StatusCodes page
    '        With sbStatusCode
    '            .Append(" SELECT Distinct  A.StatusCodeID as StatusCodeID,A.StatusCodeDescrip as StatusCodeDescrip FROM ")
    '            .Append(" syStatusCodes A,sySysStatus B,syStatuses C,syStatusLevels D ")
    '            .Append(" where A.sysStatusID = B.sysStatusID And A.StatusID = C.StatusID And B.StatusLevelId = D.StatusLevelId ")
    '            .Append(" and C.Status = 'Active' and D.StatusLevelId = 2 and B.sysStatusId = 7 ")
    '            .Append(" ORDER BY A.StatusCodeDescrip  ")
    '        End With
    '        Dim drStatusCode As OleDbDataReader = db.RunParamSQLDataReader(sbStatusCode.ToString)
    '        While drStatusCode.Read()
    '            If Not (drStatusCode("StatusCodeId") Is DBNull.Value) Then strStuEnrollmentsStatus = CType(drStatusCode("StatusCodeId"), Guid).ToString Else strStuEnrollmentsStatus = ""
    '        End While

    '        If Not drStatusCode.IsClosed Then drStatusCode.Close()

    '        db.ClearParameters()
    '        sbStatusCode.Remove(0, sbStatusCode.Length)

    '        ''TransferHours 
    '        ''
    '        ' Insert the new enrollment record in ArStuEnrollment.....................................
    '        Dim sb102 As New StringBuilder
    '        With sb102
    '            .Append(" Insert into arStuEnrollments(StuEnrollId,StudentId,EnrollDate,PrgVerId,ExpStartDate,StartDate,")
    '            .Append(" MidPtDate,ExpGradDate,TransferDate,ShiftId,EdLvlId,BillingMethodId,CampusId,StatusCodeId, ")
    '            .Append(" EnrollmentId,AdmissionsRep,AcademicAdvisor,LeadId,TuitionCategoryId,ModDate,ModUser,CohortStartDate,ContractedGradDate,TransferHours,FAAdvisorId,attendtypeid,degcertseekingid,BadgeNumber, PrgVersionTypeId, ")
    '            .Append(" IsDisabled, EntranceInterviewDate, IsFirstTimeInSchool, IsFirstTimePostSecSchool ) ")
    '            .Append(" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
    '            '''' 
    '        End With

    '        Dim stuEnrollId As String
    '        If stuEnrollmentId = "" Then
    '            stuEnrollId = Guid.NewGuid.ToString
    '        Else
    '            stuEnrollId = stuEnrollmentId
    '        End If

    '        mycmd.CommandText = sb102.ToString
    '        mycmd.Parameters.AddWithValue("@StuEnrollId", stuEnrollId)
    '        mycmd.Parameters.AddWithValue("@StudentId", leadinfo.StudentId)
    '        If leadinfo.EnrollmentDate = "" Then
    '            mycmd.Parameters.AddWithValue("@EnrollDate", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@EnrollDate", leadinfo.EnrollmentDate)
    '        End If
    '        If leadinfo.PrgVerId = "" Or leadinfo.PrgVerId = Guid.Empty.ToString Then
    '            mycmd.Parameters.AddWithValue("@PrgVerId", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@PrgVerId", leadinfo.PrgVerId)
    '        End If
    '        If leadinfo.ExpectedStart = "" Then
    '            mycmd.Parameters.AddWithValue("@ExpStartDate", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@ExpStartDate", leadinfo.ExpectedStart)
    '        End If
    '        If leadinfo.StartDate = "" Then
    '            mycmd.Parameters.AddWithValue("@StartDate", leadinfo.ExpectedStart)
    '        Else
    '            mycmd.Parameters.AddWithValue("@StartDate", leadinfo.StartDate)
    '        End If
    '        If leadinfo.MidPointDate = "" Then
    '            mycmd.Parameters.AddWithValue("@MidPtDate", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@MidPtDate", leadinfo.MidPointDate)
    '        End If
    '        If leadinfo.ExpGradDate = "" Then
    '            mycmd.Parameters.AddWithValue("@ExpGradDate", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@ExpGradDate", leadinfo.ExpGradDate)
    '        End If
    '        If leadinfo.TransferDate = "" Then
    '            mycmd.Parameters.AddWithValue("@TransferDate", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@TransferDate", leadinfo.TransferDate)
    '        End If
    '        If leadinfo.ShiftID = Guid.Empty.ToString Or leadinfo.ShiftID = "" Then
    '            mycmd.Parameters.AddWithValue("@ShiftId", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@ShiftId", leadinfo.ShiftID)
    '        End If
    '        If leadinfo.GradeLevel = Guid.Empty.ToString Or leadinfo.GradeLevel = "" Then
    '            mycmd.Parameters.AddWithValue("@EdLvlId", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@EdLvlId", leadinfo.GradeLevel)
    '        End If
    '        If leadinfo.ChargingMethod = Guid.Empty.ToString Or leadinfo.ChargingMethod = "" Then
    '            mycmd.Parameters.AddWithValue("@BillingMethodId", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@BillingMethodId", leadinfo.ChargingMethod)
    '        End If
    '        If leadinfo.CampusId = Guid.Empty.ToString Or leadinfo.CampusId = "" Then
    '            mycmd.Parameters.AddWithValue("@CampusId", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@CampusId", leadinfo.CampusId)
    '        End If

    '        'We want the enrollment status to be "Future Start". However, it is possible for the school
    '        'to have more than one Future Start codes that map to our Future Start. The school will
    '        'therefore have to specify a default Future Start code that we can use in the enrollment
    '        'process.
    '        'For testing purposes we are hardcoding the value of the school's future start code
    '        'Dim strEnrollmentStatus As String = "C92F89E0-6EA7-4176-9825-65720FC103F6"

    '        mycmd.Parameters.AddWithValue("@StatusCodeId", strStuEnrollmentsStatus)

    '        If leadinfo.EnrollmentId = Guid.Empty.ToString Or leadinfo.EnrollmentId = "" Then
    '            mycmd.Parameters.AddWithValue("@EnrollmentId", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@EnrollmentId", leadinfo.EnrollmentId)
    '        End If
    '        If leadinfo.AdmissionsRep = Guid.Empty.ToString Or leadinfo.AdmissionsRep = "" Then
    '            mycmd.Parameters.AddWithValue("@AdmissionsRep", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@AdmissionsRep", leadinfo.AdmissionsRep)
    '        End If
    '        If leadinfo.AcademicAdvisor = Guid.Empty.ToString Or leadinfo.AcademicAdvisor = "" Then
    '            mycmd.Parameters.AddWithValue("@AcademicAdvisor", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@AcademicAdvisor", leadinfo.AcademicAdvisor)
    '        End If
    '        mycmd.Parameters.AddWithValue("@LeadId", leadinfo.LeadMasterID)

    '        If leadinfo.TuitionCategory = Guid.Empty.ToString Or leadinfo.TuitionCategory = "" Then
    '            mycmd.Parameters.AddWithValue("@TuitionCategory", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@TuitionCategory", leadinfo.TuitionCategory)
    '        End If
    '        mycmd.Parameters.AddWithValue("@moddate", myTransDate)
    '        mycmd.Parameters.AddWithValue("@moduser", user)
    '        If leadinfo.ExpectedStart = "" Then
    '            mycmd.Parameters.AddWithValue("@CohortStartDate", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@CohortStartDate", leadinfo.ExpectedStart)
    '        End If
    '        If leadinfo.ExpGradDate = "" Then
    '            mycmd.Parameters.AddWithValue("@ContractedGradDate", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@ContractedGradDate", leadinfo.ExpGradDate)
    '        End If
    '        mycmd.Parameters.AddWithValue("@TransferHours", leadinfo.TransferHours)

    '        If leadinfo.FAAdvisorId = "" Then
    '            mycmd.Parameters.AddWithValue("@FAAdvisorId", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@FAAdvisorId", leadinfo.FAAdvisorId)
    '        End If
    '        If leadinfo.Attendtypeid = "" Then
    '            mycmd.Parameters.AddWithValue("@Attendtypeid", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@Attendtypeid", leadinfo.Attendtypeid)
    '        End If
    '        If leadinfo.DegCertSeekingId = "" Then
    '            mycmd.Parameters.AddWithValue("@Degcertseekingid", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@Degcertseekingid", leadinfo.DegCertSeekingId)
    '        End If


    '        If leadinfo.BadgeNumber = "" Then
    '            mycmd.Parameters.AddWithValue("@BadgeNumber", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@BadgeNumber", leadinfo.BadgeNumber)
    '        End If

    '        mycmd.Parameters.AddWithValue("@PrgVersionTypeId", leadinfo.ProgramVersionType)

    '        'Add the new 4 fields .........
    '        If leadinfo.IsDisabled = -1 Then
    '            mycmd.Parameters.AddWithValue("@IsDisabled", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@IsDisabled", leadinfo.IsDisabled)
    '        End If

    '        If leadinfo.EntranceInterviewDate Is Nothing Then
    '            mycmd.Parameters.AddWithValue("@EntranceInterviewDate", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@EntranceInterviewDate", leadinfo.EntranceInterviewDate)
    '        End If

    '        mycmd.Parameters.AddWithValue("@IsFirstTimeInSchool", leadinfo.IsFirstTimeInSchool)
    '        mycmd.Parameters.AddWithValue("@IsFirstTimePostSecSchool", leadinfo.IsFirstTimePostSecSchool)

    '        ''''''''''''

    '        mycmd.ExecuteNonQuery()
    '        mycmd.Parameters.Clear()

    '        'Insert syStudentStatusChanges for a new enrollment
    '        Dim sbStatusChange As New StringBuilder

    '        'Need to get the LeadStatus before enrolling
    '        Dim origStatusId As String = (New LeadStatusChangesDB).GetLeadStatus(leadinfo.LeadMasterID.ToString())

    '        With sbStatusChange
    '            .Append("INSERT INTO syStudentStatusChanges ")
    '            .Append("(StudentStatusChangeId, StuEnrollId, OrigStatusId, NewStatusId, CampusId, ModDate, ModUser, IsReversal, DropReasonId, DateOfChange, Lda) ")
    '            .Append("VALUES (NEWID(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ")
    '        End With
    '        mycmd.CommandText = sbStatusChange.ToString
    '        mycmd.Parameters.AddWithValue("@StuEnrollId", stuEnrollId)

    '        mycmd.Parameters.AddWithValue("@OrigStatusId", origStatusId)
    '        mycmd.Parameters.AddWithValue("@NewStatusId", strStuEnrollmentsStatus)
    '        If leadinfo.CampusId = Guid.Empty.ToString Or leadinfo.CampusId = "" Then
    '            mycmd.Parameters.AddWithValue("@CampusId", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@CampusId", leadinfo.CampusId)
    '        End If
    '        mycmd.Parameters.AddWithValue("@ModDate", myTransDate)
    '        mycmd.Parameters.AddWithValue("@ModUser", user)
    '        mycmd.Parameters.AddWithValue("@IsReversal", False)
    '        mycmd.Parameters.AddWithValue("@DropReasonId", DBNull.Value)
    '        If leadinfo.EnrollmentDate = "" Then
    '            mycmd.Parameters.AddWithValue("@DateOfChange", DBNull.Value)
    '        Else
    '            mycmd.Parameters.AddWithValue("@DateOfChange", leadinfo.EnrollmentDate)
    '        End If
    '        mycmd.Parameters.AddWithValue("@Lda", DBNull.Value)
    '        mycmd.ExecuteNonQuery()
    '        mycmd.Parameters.Clear()

    '        'Check If Address1 and Address2 Exist for this Lead
    '        Dim sbAddr As New StringBuilder
    '        With sbAddr
    '            .Append(" select Address1,Address2,City,StateId,Zip,Country,AddressType,Phone,Phone2,OtherState,ForeignZip,county from adLeads where LeadId=? ")
    '        End With
    '        db.AddParameter("@LeadId", leadinfo.LeadMasterID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sbAddr.ToString)

    '        Dim strAddress1, strAddress2, strCity, strState, strZip, strCountry, strAddressType, strPhone, strPhone2, strOtherState, strCounty As String
    '        '' Dim intForeignZip As Integer
    '        Dim boolForeignZip = False

    '        While dr.Read()
    '            If Not (dr("Address1") Is DBNull.Value) Then strAddress1 = dr("Address1") Else strAddress1 = ""
    '            If Not (dr("Address2") Is DBNull.Value) Then strAddress2 = dr("Address2") Else strAddress2 = ""
    '            If Not (dr("City") Is DBNull.Value) Then strCity = dr("City") Else strCity = ""
    '            If Not (dr("Zip") Is DBNull.Value) Then strZip = dr("Zip") Else strZip = ""
    '            If Not (dr("StateId") Is DBNull.Value) Then strState = CType(dr("StateId"), Guid).ToString Else strState = ""
    '            If Not (dr("Country") Is DBNull.Value) Then strCountry = CType(dr("Country"), Guid).ToString Else strCountry = ""
    '            If Not (dr("AddressType") Is DBNull.Value) Then strAddressType = CType(dr("AddressType"), Guid).ToString Else strAddressType = ""
    '            If Not (dr("County") Is DBNull.Value) Then strCounty = CType(dr("County"), Guid).ToString Else strCounty = ""
    '            If Not (dr("OtherState") Is DBNull.Value) Then strOtherState = dr("OtherState") Else strOtherState = ""
    '            If Not (dr("Phone") Is DBNull.Value) Then strPhone = CType(dr("Phone"), String) Else strPhone = ""
    '            If Not (dr("Phone2") Is DBNull.Value) Then strPhone2 = CType(dr("Phone2"), String) Else strPhone2 = ""
    '            If Not (dr("ForeignZip") Is DBNull.Value) Then boolForeignZip = dr("ForeignZip") Else boolForeignZip = False
    '        End While

    '        If Not dr.IsClosed Then dr.Close()

    '        db.ClearParameters()
    '        sbAddr.Remove(0, sbAddr.Length)

    '        Dim sb103 As New StringBuilder
    '        With sb103
    '            .Append(" Insert into arStudAddresses(StdAddressId,StudentId,Address1,Address2,")
    '            .Append(" City,StateId,Zip,CountryId,AddressTypeId,ModUser,ModDate,StatusId,default1,ForeignZip,OtherState) ")
    '            .Append(" select ?,?,Address1,Address2,City,StateId,Zip,Country,AddressType,?,?,?,1,ForeignZip,OtherState from adLeads ")
    '            .Append(" where LeadId = ? ")
    '        End With
    '        mycmd.CommandText = sb103.ToString
    '        mycmd.Parameters.AddWithValue("@StdAddressId", Guid.NewGuid.ToString)
    '        mycmd.Parameters.AddWithValue("@StudentId", leadinfo.StudentId)
    '        mycmd.Parameters.AddWithValue("@ModUser", user)
    '        mycmd.Parameters.AddWithValue("@ModDate", myTransDate)
    '        mycmd.Parameters.AddWithValue("@StatusId", strStudentStatus)
    '        mycmd.Parameters.AddWithValue("@LeadId", leadinfo.LeadMasterID)
    '        mycmd.ExecuteNonQuery()
    '        mycmd.Parameters.Clear()

    '        Dim sb104 As New StringBuilder
    '        With sb104
    '            .Append(" Insert into arStudentPhone(StudentPhoneId,StudentId,PhoneTypeId,Phone,")
    '            .Append(" StatusId,ModUser,ModDate,default1,ForeignPhone) ")
    '            .Append(" select ?,?,PhoneType,Phone,?,?,?,")
    '            .Append("(case when defaultphone=1 then 1 else 0 end) as defaultPhone ")
    '            .Append(" ,ForeignPhone from adLeads where LeadId=? ")
    '        End With
    '        mycmd.CommandText = sb104.ToString

    '        mycmd.Parameters.AddWithValue("@StudentPhoneId", Guid.NewGuid.ToString)
    '        mycmd.Parameters.AddWithValue("@StudentId", leadinfo.StudentId)
    '        mycmd.Parameters.AddWithValue("@StatusId", strStudentStatus)
    '        mycmd.Parameters.AddWithValue("@ModUser", user)
    '        mycmd.Parameters.AddWithValue("@ModDate", myTransDate)
    '        mycmd.Parameters.AddWithValue("@LeadId", leadinfo.LeadMasterID)

    '        If Not strPhone = "" Then
    '            mycmd.ExecuteNonQuery()
    '        End If

    '        mycmd.Parameters.Clear()


    '        Dim sb104_2 As New StringBuilder
    '        With sb104_2
    '            .Append(" Insert into arStudentPhone(StudentPhoneId,StudentId,PhoneTypeId,Phone,")
    '            .Append(" StatusId,ModUser,ModDate,default1,ForeignPhone) ")
    '            .Append(" select ?,?,PhoneType2,Phone2,?,?,?,")
    '            .Append("(case when defaultphone=2 then 1 else 0 end) as defaultPhone ")
    '            .Append(",ForeignPhone2 from adLeads where LeadId=? ")
    '        End With
    '        mycmd.CommandText = sb104_2.ToString

    '        mycmd.Parameters.AddWithValue("@StudentPhoneId", Guid.NewGuid.ToString)
    '        mycmd.Parameters.AddWithValue("@StudentId", leadinfo.StudentId)
    '        mycmd.Parameters.AddWithValue("@StatusId", strStudentStatus)
    '        mycmd.Parameters.AddWithValue("@ModUser", user)
    '        mycmd.Parameters.AddWithValue("@ModDate", myTransDate)
    '        mycmd.Parameters.AddWithValue("@LeadId", leadinfo.LeadMasterID)


    '        If Not strPhone2 = "" Then
    '            ' db.RunParamSQLExecuteNoneQuery(sb5.ToString)
    '            mycmd.ExecuteNonQuery()
    '        End If

    '        mycmd.Parameters.Clear()


    '        Dim sb105 As New StringBuilder
    '        With sb105
    '            .Append(" Insert into plStudentEducation(StudentId,EducationInstId,EducationInstType,GraduatedDate,FinalGrade,CertificateId,Comments,Major,ModUser,ModDate) ")
    '            .Append(" Select ?,EducationInstId,EducationInstType,GraduatedDate,FinalGrade,CertificateId,Comments,Major,?,? from adLeadEducation ")
    '            .Append(" where LeadId = ? ")
    '        End With
    '        mycmd.CommandText = sb105.ToString

    '        mycmd.Parameters.AddWithValue("@StudentId", leadinfo.StudentId)
    '        mycmd.Parameters.AddWithValue("@ModUser", user)
    '        mycmd.Parameters.AddWithValue("@ModDate", myTransDate)
    '        mycmd.Parameters.AddWithValue("@LeadId", leadinfo.LeadMasterID)
    '        mycmd.ExecuteNonQuery()
    '        mycmd.Parameters.Clear()


    '        Dim sb106 As New StringBuilder
    '        With sb106
    '            .Append(" Insert into plStudentEmployment(StEmploymentId,StudentId,EmployerName,EmployerJobTitle,JobTitleId,JobStatusId,StartDate,EndDate,Comments,ModUser,ModDate) ")
    '            .Append(" Select newid(),?,EmployerName,EmployerJobTitle,JobTitleId,JobStatusId,StartDate,EndDate,Comments,?,? from adLeadEmployment ")
    '            .Append(" where LeadId = ? ")
    '        End With
    '        mycmd.CommandText = sb106.ToString
    '        'mycmd.Parameters.AddWithValue("@StEmploymentId", Guid.NewGuid.ToString)
    '        mycmd.Parameters.AddWithValue("@StudentId", leadinfo.StudentId)
    '        mycmd.Parameters.AddWithValue("@ModUser", user)
    '        mycmd.Parameters.AddWithValue("@ModDate", myTransDate)
    '        mycmd.Parameters.AddWithValue("@LeadId", leadinfo.LeadMasterID)
    '        mycmd.ExecuteNonQuery()
    '        mycmd.Parameters.Clear()


    '        Dim sbSkills As New StringBuilder
    '        With sbSkills
    '            .Append(" Insert into plStudentSkills(SkillId,StudentId,ModUser,ModDate) ")
    '            .Append(" Select SkillId,?,?,? from adLeadSkills ")
    '            .Append(" where LeadId = ? ")
    '        End With
    '        mycmd.CommandText = sbSkills.ToString
    '        mycmd.Parameters.AddWithValue("@StudentId", leadinfo.StudentId)
    '        mycmd.Parameters.AddWithValue("@ModUser", user)
    '        mycmd.Parameters.AddWithValue("@ModDate", myTransDate)
    '        mycmd.Parameters.AddWithValue("@LeadId", leadinfo.LeadMasterID)
    '        mycmd.ExecuteNonQuery()

    '        mycmd.Parameters.Clear()

    '        Dim sbExtraCurr As New StringBuilder
    '        With sbExtraCurr
    '            .Append(" Insert into plStudentExtracurriculars(ExtraCurricularID,StudentID,ModUser,ModDate) ")
    '            .Append(" Select ExtraCurricularID,?,?,? from adLeadExtraCurriculars ")
    '            .Append(" where LeadId = ? ")
    '        End With
    '        mycmd.CommandText = sbExtraCurr.ToString

    '        mycmd.Parameters.AddWithValue("@StudentId", leadinfo.StudentId)
    '        mycmd.Parameters.AddWithValue("@ModUser", user) ', DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        mycmd.Parameters.AddWithValue("@ModDate", myTransDate) ', DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        mycmd.Parameters.AddWithValue("@LeadId", leadinfo.LeadMasterID) ', DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        mycmd.ExecuteNonQuery()
    '        mycmd.Parameters.Clear()

    '        'db.RunParamSQLExecuteNoneQuery(sbExtraCurr.ToString)
    '        'db.ClearParameters()
    '        'sbExtraCurr.Remove(0, sbExtraCurr.Length)


    '        Dim sbDocument As New StringBuilder
    '        With sbDocument
    '            .Append(" Insert into plStudentDocs(StudentID,DocumentId,DocStatusId,ReceiveDate,ModUser,ModDate,ModuleId,ScannedId ")
    '            If myAdvAppSettings.AppSettings("REGENT").ToString.ToLower = "yes" Then
    '                .Append(",trackcode,DueDate,NotifiedDate,CompletedDate,Notificationcode,Description")
    '            End If
    '            .Append(" ) ")
    '            .Append(" Select ?,DocumentID,DocStatusId,ReceiveDate,?,?,(select Distinct ModuleId from adReqs where adReqId=adLeadDocsReceived.DocumentId),1  ")
    '            If myAdvAppSettings.AppSettings("REGENT").ToString.ToLower = "yes" Then
    '                .Append(",trackcode,DueDate,NotifiedDate,CompletedDate,Notificationcode,Description")
    '            End If
    '            .Append(" from adLeadDocsReceived ")
    '            .Append(" where LeadId = ? ")
    '        End With
    '        mycmd.CommandText = sbDocument.ToString

    '        mycmd.Parameters.AddWithValue("@StudentId", leadinfo.StudentId) ', DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        mycmd.Parameters.AddWithValue("@ModUser", user) ', DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        mycmd.Parameters.AddWithValue("@ModDate", myTransDate) ', DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        mycmd.Parameters.AddWithValue("@LeadId", leadinfo.LeadMasterID) ', DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        mycmd.ExecuteNonQuery()
    '        mycmd.Parameters.Clear()
    '        'db.RunParamSQLExecuteNoneQuery(sbDocument.ToString)
    '        'db.ClearParameters()
    '        sbDocument.Remove(0, sbDocument.Length)


    '        Dim sbDocumentHistory As New StringBuilder
    '        With sbDocumentHistory
    '            .Append(" Update syDocumentHistory set StudentId=?,ModUser=?,ModDate=? where LeadId=? ")
    '        End With
    '        mycmd.CommandText = sbDocumentHistory.ToString

    '        mycmd.Parameters.AddWithValue("@StudentId", leadinfo.StudentId) ', DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        mycmd.Parameters.AddWithValue("@ModUser", user) ', DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        mycmd.Parameters.AddWithValue("@ModDate", myTransDate) ', DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        mycmd.Parameters.AddWithValue("@LeadId", leadinfo.LeadMasterID) ', DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        mycmd.ExecuteNonQuery()
    '        mycmd.Parameters.Clear()

    '        'db.RunParamSQLExecuteNoneQuery(sbDocumentHistory.ToString)
    '        'db.ClearParameters()
    '        sbDocumentHistory.Remove(0, sbDocumentHistory.Length)

    '        Dim sbDocumentHistory1 As New StringBuilder
    '        With sbDocumentHistory1
    '            .Append(" Update adLeadByLeadGroups set StuEnrollId=?,ModUser=?,ModDate=? where LeadId=? ")
    '        End With
    '        mycmd.CommandText = sbDocumentHistory1.ToString
    '        mycmd.Parameters.AddWithValue("@StuEnrollId", stuEnrollId)
    '        mycmd.Parameters.AddWithValue("@ModUser", user) ', DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        mycmd.Parameters.AddWithValue("@ModDate", myTransDate) ', DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        mycmd.Parameters.AddWithValue("@LeadId", leadinfo.LeadMasterID) ', DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        mycmd.ExecuteNonQuery()
    '        mycmd.Parameters.Clear()

    '        'db.RunParamSQLExecuteNoneQuery(sbDocumentHistory.ToString)
    '        'db.ClearParameters()
    '        sbDocumentHistory.Remove(0, sbDocumentHistory.Length)

    '        Dim sb1907 As New StringBuilder
    '        With sb1907
    '            .Append(" update adLeadEntranceTest set StudentId=? where LeadId = ? ")
    '        End With
    '        mycmd.CommandText = sb1907.ToString
    '        mycmd.Parameters.AddWithValue("@StudentId", leadinfo.StudentId)
    '        mycmd.Parameters.AddWithValue("@LeadId", leadinfo.LeadMasterID)
    '        mycmd.ExecuteNonQuery()
    '        mycmd.Parameters.Clear()

    '        Dim sb1908 As New StringBuilder
    '        With sb1908
    '            .Append(" update adEntrTestOverRide set StudentId=? where LeadId = ? ")
    '        End With
    '        mycmd.CommandText = sb1908.ToString
    '        mycmd.Parameters.AddWithValue("@StudentId", leadinfo.StudentId)
    '        mycmd.Parameters.AddWithValue("@LeadId", leadinfo.LeadMasterID)
    '        mycmd.ExecuteNonQuery()
    '        mycmd.Parameters.Clear()

    '        Dim sb2 As New StringBuilder
    '        With sb2
    '            .Append(" Update adLeads set LeadStatus=? where LeadId=? ")
    '        End With
    '        mycmd.CommandText = sb2.ToString
    '        mycmd.Parameters.AddWithValue("@LeadStatus", leadinfo.StatusCodeId) ', DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        mycmd.Parameters.AddWithValue("@LeadId", leadinfo.LeadMasterID) ', DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        mycmd.ExecuteNonQuery()
    '        mycmd.Parameters.Clear()

    '        'db.RunParamSQLExecuteNoneQuery(sb2.ToString)
    '        'db.ClearParameters()
    '        'sb2.Remove(0, sb2.Length)

    '        Dim sb98 As New StringBuilder
    '        With sb98
    '            .Append(" select count(*) from sySDFModuleValue where PgPKID=? ")
    '        End With
    '        db.AddParameter("@LeadId", leadinfo.LeadMasterID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Dim intSDFCount As Integer = db.RunParamSQLScalar(sb98.ToString)
    '        db.ClearParameters()
    '        'sb98.Remove(0, sb98.Length)

    '        If intSDFCount >= 1 Then
    '            'Migrate The SchoolDefined Field Data
    '            Dim sdfval As New StringBuilder
    '            With sdfval
    '                .Append(" Insert into sySDFModuleValue(PgPKID,SDFID,SDFValue) ")
    '                .Append(" Select ?,SDFID,SDFValue from sySDFModuleValue ")
    '                .Append(" where PgPKID = ? ")
    '            End With
    '            mycmd.CommandText = sdfval.ToString
    '            mycmd.Parameters.AddWithValue("@StudentId", leadinfo.StudentId) ', DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            mycmd.Parameters.AddWithValue("@PgPKID", leadinfo.LeadMasterID) ', DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            mycmd.ExecuteNonQuery()
    '            mycmd.Parameters.Clear()
    '        End If

    '        ' DE 5538 Janet Robinson 05/09/2011 Copy Lead Notes from adLeadnotes to syStudentNotes so all notes will be in the same table
    '        mycmd.Parameters.Clear()
    '        db.ClearParameters()
    '        Dim sbNotes As New StringBuilder
    '        With sbNotes
    '            .Append(" select LeadNoteId, StatusId, LeadNoteDescrip, ModuleCode, UserId from adLeadNotes where LeadId = ? ORDER BY CreatedDate ")
    '        End With
    '        db.AddParameter("@LeadId", leadinfo.LeadMasterID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Dim dr1 As OleDbDataReader = db.RunParamSQLDataReader(sbNotes.ToString)
    '        Dim g As String = Guid.NewGuid.ToString
    '        sbNotes.Clear()
    '        With sbNotes
    '            .Append(" INSERT syStudentNotes (StudentNoteId, StatusId, StudentId,  ")
    '            .Append("   StudentNoteDescrip, ModuleCode, UserId, CreatedDate, ModUser, ModDate) ")
    '            .Append(" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ? ) ")
    '        End With
    '        While dr1.Read()
    '            mycmd.CommandText = sbNotes.ToString
    '            mycmd.Parameters.AddWithValue("@StudentNoteId", Guid.NewGuid.ToString)
    '            mycmd.Parameters.AddWithValue("@StatusId", dr1("StatusId"))
    '            mycmd.Parameters.AddWithValue("@StudentId", leadinfo.StudentId)
    '            mycmd.Parameters.AddWithValue("@StudentNoteDescrip", dr1("LeadNoteDescrip"))
    '            mycmd.Parameters.AddWithValue("@ModuleCode", dr1("ModuleCode"))
    '            mycmd.Parameters.AddWithValue("@UserId", dr1("UserId"))
    '            'Dim now As Date = Date.Parse(Date.Now)
    '            mycmd.Parameters.AddWithValue("@CreatedDate", myTransDate)
    '            mycmd.Parameters.Item("@CreatedDate").Value = myTransDate
    '            mycmd.Parameters.AddWithValue("@ModUser", user)
    '            mycmd.Parameters.AddWithValue("@ModDate", myTransDate)
    '            mycmd.ExecuteNonQuery()
    '            mycmd.Parameters.Clear()
    '        End While

    '        If Not dr1.IsClosed Then dr1.Close()

    '        db.ClearParameters()
    '        ' DE 5538 End

    '        ' US3282 11/28/2012 Janet Robinson Add logic to copy Lead Transactions/Payments when the Student is enrolled
    '        mycmd.Parameters.Clear()
    '        With sb
    '            .Append(" USP_AD_CopyLeadTransactions ")
    '        End With
    '        mycmd = New OleDbCommand(sb.ToString, myconn, myTrans)
    '        mycmd.CommandType = CommandType.StoredProcedure
    '        mycmd.Parameters.AddWithValue("@LeadId", leadinfo.LeadMasterID)
    '        mycmd.Parameters.AddWithValue("@StuEnrollId", stuEnrollId)
    '        mycmd.Parameters.AddWithValue("@CampusId", leadinfo.CampusId)
    '        mycmd.ExecuteNonQuery()
    '        db.ClearParameters()
    '        ' US3282 end

    '        myTrans.Commit()

    '        ''To calculate the program version fees when enrolling a  student
    '        ''Review automated posting of program version fees for clock hour programs when a lead is enrolled
    '        ''Step1: Find Out if the prg is a clock hour program
    '        ''Step 2: If it is a clock hours program and if the fee calculation is based on Flat amount then it will post the charge to the satransaction table.
    '        ''This will be performed for any school whose program is a clock hour program.

    '        Dim SbQuery1 As New StringBuilder
    '        With SbQuery1
    '            .Append(" Select Count(P.ACID) from arprograms P,arPrgVersions PV,arStuEnrollments SE ")
    '            .Append(" where PV.PrgVerId=SE.PrgVerId and SE.StuEnrollId=? ")
    '            .Append(" and P.ProgId=PV.ProgId and ACId=5 ")
    '        End With
    '        db.AddParameter("@StuEnrollID", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Dim acCalendarID As Integer
    '        acCalendarID = db.RunParamSQLScalar(SbQuery1.ToString)
    '        db.ClearParameters()

    '        If acCalendarID >= 1 Then
    '            Dim result1 As String = (New TransactionsDB).ApplyFeesByProgramVersionForCLockHourPrograms(stuEnrollId, user, leadinfo.CampusId)

    '        End If

    '        '   charge Program Fees if applicable.
    '        Dim result2 As String = (New TransactionsDB).ApplyFeesByProgramVersion(stuEnrollId, user, leadinfo.CampusId, Date.Today)


    '        'Call Regent Code
    '        'Check if school uses regent (a new web.config entry needs to be added)
    '        If myAdvAppSettings.AppSettings("REGENT").ToString.Trim.ToLower = "yes" Then
    '            Dim regDB As New regentDB
    '            Dim strFilename As String = AdvantageCommonValues.getStudentBatchFileName()
    '            If Not (File.Exists(strFilename)) Then
    '                regDB.GenerateXMLForStudentCreation(leadinfo.StudentId, leadinfo.CampusId)
    '            Else
    '                regDB.UpdateXMLForStudentCreation(leadinfo.StudentId, strFilename, leadinfo.CampusId)
    '            End If
    '        End If


    '        Return ""
    '    Catch ex As OleDbException
    '        myTrans.Rollback()

    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)
    '    Finally
    '        'Close Connection
    '        myconn.Close()

    '        db.CloseConnection()
    '    End Try
    'End Function

    Public Function UnEnrollLeadBySP(LeadId As String, status As String) As Integer
        Dim unEnrollLeadTransaction As OleDbTransaction
        '  Dim intReturnValue As Integer
        ' Dim returnoutputparam As OleDbParameter
        Dim strConn As OleDbConnection
        Dim strCmd As OleDbCommand
        Dim strOutputParam As OleDbParameter

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If


        Try
            strConn = New OleDbConnection(myAdvAppSettings.AppSettings("ConString"))
            strConn.Open()

            unEnrollLeadTransaction = strConn.BeginTransaction
            strCmd = New OleDbCommand("Sp_UnenrollLeads", strConn, unEnrollLeadTransaction)
            strCmd.CommandType = CommandType.StoredProcedure
            strCmd.CommandTimeout = 600
            strOutputParam = strCmd.Parameters.AddWithValue("@LeadId", OleDbType.Guid.ToString())
            strOutputParam.Direction = ParameterDirection.Input
            strOutputParam = strCmd.Parameters.AddWithValue("@Status", OleDbType.Guid.ToString())
            strOutputParam.Direction = ParameterDirection.Input
            strCmd.Parameters("@LeadId").Value = LeadId
            strCmd.Parameters("@Status").Value = status
            strCmd.ExecuteNonQuery()
            unEnrollLeadTransaction.Commit()
            Return 0
        Catch ex As Exception
            unEnrollLeadTransaction.Rollback()
            Return 1
        Finally
            strConn.Close()
        End Try
    End Function
    'Public Function UnEnrollLead(LeadId As String, strEnrollmentStatus As String) As String
    '    'connect to the database
    '    Dim db As New DataAccess
    '    Dim sb1 As New StringBuilder
    '    Dim myTrans As OleDbTransaction

    '    Dim myAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        myAdvAppSettings = New AdvAppSettings
    '    End If


    '    Dim myconn As New OleDbConnection(myAdvAppSettings.AppSettings("ConString"))
    '    myconn.Open()


    '    db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
    '    ' db.OpenConnection()

    '    'Dim strStudentStatus As String = "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965"
    '    'Dim sb As New StringBuilder
    '    'Dim sb2 As New StringBuilder
    '    'Dim sb3 As New StringBuilder
    '    Dim sb4 As New StringBuilder
    '    Dim sb19 As New StringBuilder
    '    With sb4
    '        .Append(" Select Distinct StudentId from arStuEnrollments where LeadId = ?")
    '    End With
    '    'Execute the query
    '    db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb4.ToString)
    '    Dim strStudent As String
    '    Dim intRowCount As Integer
    '    While dr.Read()
    '        strStudent = CType(dr("StudentId"), Guid).ToString
    '    End While

    '    If Not dr.IsClosed Then dr.Close()

    '    db.ClearParameters()
    '    sb4.Remove(0, sb4.Length)

    '    If Not strStudent = "" Then
    '        With sb19
    '            .Append(" Select Count(*) as Count from arResults where StuEnrollId in (select StuEnrollId from arStuEnrollments where StudentId = ?) ")
    '        End With
    '        'Execute the query
    '        db.AddParameter("@StudentId", strStudent, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Try
    '            intRowCount = db.RunParamSQLScalar(sb19.ToString)
    '            db.ClearParameters()
    '            sb19.Remove(0, sb19.Length)
    '        Catch ex As Exception
    '            intRowCount = 0
    '        End Try

    '        If intRowCount >= 1 Then
    '            Dim sMessage As String
    '            sMessage = " The lead cannot be enrolled as the lead has already been registered for a classsection" & vbLf
    '            Return sMessage
    '            Exit Function
    '        End If
    '    End If

    '    Dim mycmd As OleDbCommand
    '    Try
    '        myTrans = myconn.BeginTransaction()

    '        If Not strStudent = "" Then
    '            Dim sb67 As New StringBuilder
    '            With sb67
    '                .Append(" Update adLeadByLeadGroups set StuEnrollId=Null where LeadId='" & LeadId & "' ")
    '            End With
    '            mycmd = New OleDbCommand(sb67.ToString, myconn, myTrans)
    '            mycmd.ExecuteNonQuery()

    '            With sb1
    '                .Append(" Delete from PlStudentsPlaced where StuEnrollId in (select StuEnrollId from arStuEnrollments where StudentId = '" & strStudent & "') ")
    '            End With
    '            mycmd.CommandText = sb1.ToString
    '            mycmd.ExecuteNonQuery()
    '            sb1.Remove(0, sb1.Length)

    '            'With sb1
    '            '    .Append(" Update saTransactions set StuEnrollId=NULL where StuEnrollId in (select StuEnrollId from arStuEnrollments where StudentId = '" & strStudent & "') ")
    '            'End With
    '            'mycmd.CommandText = sb1.ToString
    '            'mycmd.ExecuteNonQuery()
    '            'sb1.Remove(0, sb1.Length)

    '            With sb1
    '                .Append(" Delete from arStuEnrollments where StudentId = '" & strStudent & "' ")
    '            End With
    '            mycmd.CommandText = sb1.ToString
    '            mycmd.ExecuteNonQuery()
    '            sb1.Remove(0, sb1.Length)

    '            Dim sb12 As New StringBuilder
    '            With sb12
    '                .Append(" Delete from arStudAddresses where StudentId = '" & strStudent & "' ")
    '            End With
    '            mycmd.CommandText = sb12.ToString
    '            mycmd.ExecuteNonQuery()


    '            Dim sb5 As New StringBuilder
    '            With sb5
    '                .Append(" Delete from arStudentPhone where StudentId = '" & strStudent & "' ")
    '            End With
    '            mycmd.CommandText = sb5.ToString
    '            mycmd.ExecuteNonQuery()


    '            Dim sb6 As New StringBuilder
    '            With sb6
    '                .Append(" Delete from plStudentEducation where StudentId = '" & strStudent & "' ")
    '            End With
    '            mycmd.CommandText = sb6.ToString
    '            mycmd.ExecuteNonQuery()

    '            Dim sb7 As New StringBuilder
    '            'Dim strGuid = Guid.NewGuid.ToString
    '            With sb7
    '                .Append(" Delete from plStudentEmployment where StudentId = '" & strStudent & "' ")
    '            End With
    '            mycmd.CommandText = sb7.ToString
    '            mycmd.ExecuteNonQuery()

    '            Dim sb87 As New StringBuilder
    '            With sb87
    '                .Append(" Delete from plStudentDocs where StudentId = '" & strStudent & "' ")
    '            End With
    '            mycmd.CommandText = sb87.ToString
    '            mycmd.ExecuteNonQuery()

    '            Dim sb88 As New StringBuilder
    '            With sb88
    '                .Append(" Delete from syDocumentHistory where StudentId = '" & strStudent & "' ")
    '            End With
    '            mycmd.CommandText = sb88.ToString
    '            mycmd.ExecuteNonQuery()

    '            Dim sb188 As New StringBuilder
    '            With sb188
    '                .Append(" Delete from plStudentExtracurriculars where StudentId = '" & strStudent & "' ")
    '            End With
    '            mycmd.CommandText = sb188.ToString
    '            mycmd.ExecuteNonQuery()

    '            Dim sb189 As New StringBuilder
    '            With sb189
    '                .Append(" Delete from plStudentSkills where StudentId = '" & strStudent & "' ")
    '            End With
    '            mycmd.CommandText = sb189.ToString
    '            mycmd.ExecuteNonQuery()

    '            Dim sb89 As New StringBuilder
    '            With sb89
    '                .Append(" Delete from arStudent where StudentId = '" & strStudent & "' ")
    '            End With
    '            mycmd.CommandText = sb89.ToString
    '            mycmd.ExecuteNonQuery()


    '        End If

    '        Dim sb93 As New StringBuilder
    '        With sb93
    '            .Append(" Delete from syDocumentHistory where LeadId = '" & LeadId & "' ")
    '        End With
    '        If strStudent = "" Then
    '            mycmd = New OleDbCommand(sb93.ToString, myconn, myTrans)
    '        Else
    '            mycmd.CommandText = sb93.ToString
    '        End If
    '        mycmd.ExecuteNonQuery()

    '        Dim sb90 As New StringBuilder
    '        With sb90
    '            .Append("Update adLeads set LeadStatus='" & strEnrollmentStatus & "' where LeadId='" & LeadId & "' ")
    '        End With
    '        mycmd.CommandText = sb90.ToString
    '        mycmd.ExecuteNonQuery()
    '        myTrans.Commit()
    '    Catch ex As OleDbException
    '        myTrans.Rollback()
    '        '   return an error to the client
    '        Return DALExceptions.BuildErrorMessage(ex)
    '    Finally
    '        myconn.Close()
    '        'db.CloseConnection()
    '    End Try

    'End Function
    Public Function CheckLeadTakenTest(LeadId As String) As Integer
        'connect to the database
        Dim db As New DataAccess
        Dim intLeadExistCount As Integer

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select Count(*) as LeadExist from adLeadEntranceTest  ")
            .Append(" where LeadId = ? ")
        End With
        db.AddParameter("@LeadID", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            intLeadExistCount = dr("LeadExist")
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return intLeadExistCount
    End Function
    Public Function CheckLeadTakenDocs(LeadId As String) As Integer
        'connect to the database
        Dim db As New DataAccess
        Dim intLeadExistCount As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select Count(*) as LeadExist from adLeadDocsReceived  ")
            .Append(" where LeadId = ? ")
        End With
        db.AddParameter("@LeadID", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            intLeadExistCount = dr("LeadExist")
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return intLeadExistCount
    End Function
    Public Function CheckIfLeadHasTakenTest(LeadId As String) As Integer
        'connect to the database
        Dim db As New DataAccess
        Dim intLeadExistCount As Integer

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select Count(*) as LeadExist from adLeadEntranceTest  ")
            .Append(" where LeadId = ? ")
        End With
        db.AddParameter("@LeadID", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            intLeadExistCount = dr("LeadExist")
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return intLeadExistCount
    End Function

    Public Function CheckLeadHasNotApprovedDocs(LeadId As String) As Integer
        'connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            'Get only the list of documents not approved and not OverRiden
            .Append(" select Count(R1.Descrip) from ")
            .Append(" (select t4.Descrip from ")
            .Append(" adLeadDocsReceived t1,adReqLeadGroups  t2,adLeads t3,adReqs t4,syDocStatuses t5,sySysDocStatuses t6 ")
            .Append(" where t1.DocumentId = t2.adReqId And t2.LeadGrpId = t3.LeadGrpId ")
            .Append(" and t2.IsRequired = 1 and t3.LeadId='" & LeadId & "' ")
            .Append(" and t2.adReqId = t4.adReqId and t4.adReqTypeId=3 and t1.DocStatusId = t5.DocStatusId  ")
            .Append(" and t5.SysDocStatusId = t6.SysDocStatusId and t6.SysDocStatusId = 2 and ")
            .Append(" t1.DocumentId not in (select EntrTestId from adEntrTestOverRide where  ")
            .Append(" LeadId='" & LeadId & "' and OverRide='Yes') ")
            .Append(" union ")
            'Get list of documents not taken
            .Append(" select Distinct t1.Descrip from adReqs t1,adReqLeadGroups t2,adLeads t3 where ")
            .Append(" t1.adReqId = t2.adReqId And t2.LeadGrpId = t3.LeadGrpId And t2.IsRequired = 1 ")
            .Append(" and t1.adReqTypeId = 3 and t3.LeadId ='" & LeadId & "' ")
            .Append(" and t1.adReqId not in (select DocumentId from adLeadDocsReceived where LeadId='" & LeadId & "') ")
            .Append(" and ")
            .Append(" t1.DocumentId not in (select EntrTestId from adEntrTestOverRide where  ")
            .Append(" LeadId='" & LeadId & "' and OverRide='Yes') ")
            .Append(" union ")
            'Get the list of documents that are mandatory (applies to all) but not part of any requirements
            'or any groups
            .Append(" select Distinct Descrip from adReqs where adReqTypeId=3 and AppliesToAll=1 ")
            .Append(" and adReqId not in (select adReqId from adReqLeadGroups) and adReqId not in (select Distinct DocumentId from adLeadDocsReceived  ")
            .Append(" where LeadId='" & LeadId & "') ")
            .Append(" and ")
            .Append(" t1.DocumentId not in (select EntrTestId from adEntrTestOverRide where  ")
            .Append(" LeadId='" & LeadId & "' and OverRide='Yes') ")
            .Append(" ) R1 ")
            '.Append(" union ")
            '.Append(" select t1.Descrip as Descrip from adReqs t1,adReqLeadGroups t2,adLeads t3 where ")
            '.Append(" t1.adReqId = t2.adReqId And t2.LeadGrpId = t3.LeadGrpId And t2.IsRequired = 1 ")
            '.Append(" and t1.adReqTypeId = 3 and t3.LeadId ='" & LeadId & "' ")
            '.Append(" and t1.adReqId not in (select DocumentId from adLeadDocsReceived) ")
            '.Append(" union ")
            '.Append(" select Descrip from adReqs where adReqTypeId=3 and AppliesToAll=1  ")
            '.Append(" and adReqId not in ")
            '.Append(" (select adReqId from adReqLeadGroups)

        End With

        'Execute the query
        Try
            Dim intRequiredDocsNotApprovedCount As Integer = db.RunParamSQLScalar(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            Return intRequiredDocsNotApprovedCount
        Catch e As Exception
            Return 0
        End Try
    End Function
    Public Function CheckIfLeadHasPassedTest(LeadId As String) As Integer
        'connect to the database
        Dim db As New DataAccess
        'Dim strLeadPass As String

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With sub queries
            .Append(" select Count(*) as RequiredTestNotPassedCount from ")
            .Append(" adLeadEntranceTest t1,adReqLeadGroups  t2,adLeads t3,adReqs t4,syDocStatuses t5,sySysDocStatuses t6 ")
            .Append(" where t1.DocumentId = t2.adReqId And t2.LeadGrpId = t3.LeadGrpId ")
            .Append(" and t2.IsRequired = 1 and t3.LeadId='" & LeadId & "' ")
            .Append(" and t2.adReqId = t4.adReqId and t4.adReqTypeId=3 and t1.DocStatusId = t5.DocStatusId  ")
            .Append(" and t5.SysDocStatusId = t6.SysDocStatusId and t6.SysDocStatusId = 2 ")
        End With

        'Execute the query
        Try
            Dim intRequiredDocsNotApprovedCount As Integer = db.RunParamSQLScalar(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            Return intRequiredDocsNotApprovedCount
        Catch e As Exception
            Return 0
        End Try
    End Function
    Public Function CheckLeadHasRequiredDocs(LeadId As String) As Integer
        'connect to the database
        Dim db As New DataAccess
        'Dim strLeadPass As String

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select Count(*) as LeadRequiredDocsCount from adReqLeadGroups  t2,adLeads t3,adReqs t4 ")
            .Append(" where t2.LeadGrpId = t3.LeadGrpId ")
            .Append(" and t2.IsRequired=1 and t3.LeadId='" & LeadId & "' ")
            .Append(" and t2.adReqId = t4.adReqId and t4.adReqTypeId=3  ")
        End With

        'Execute the query
        Try
            Dim intRequiredDocsCount As Integer = db.RunParamSQLScalar(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            Return intRequiredDocsCount
        Catch e As Exception
            Return 0
        End Try
    End Function
    Public Function CheckIfLeadHasRequiredDocs(LeadId As String, PrgVerId As String) As Integer
        'connect to the database
        Dim db As New DataAccess
        Dim strLeadPass As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select count(*) as RequiredDocuments from ")
            .Append(" ( ")
            .Append(" select t1.adReqId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,t2.Descrip, ")
            .Append(" t3.IsRequired as Required ")
            .Append(" from adPrgVerTestDetails t1,adReqs t2,adReqLeadGroups t3 ")
            .Append(" where t1.adReqId = t2.adReqId and  t2.adReqTypeId=3 and t2.adReqId = t3.adReqId and t1.PrgVerId='" & PrgVerId & "' ")
            .Append(" and t1.ReqGrpId is null and t3.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
            .Append(" union ")
            .Append(" select t5.adReqId as ReqId,t4.ReqGrpId,t5.Descrip,t6.IsRequired as Required ")
            .Append(" from  adReqGrpDef t4,adReqs t5,adReqLeadGroups t6 ")
            .Append(" where t4.adReqId = t5.adReqId and t5.adReqTypeId=3 and t5.adReqId = t6.adReqId and t6.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
            .Append(" and ReqGrpId in ")
            .Append(" (select t1.ReqGrpId from adPrgVerTestDetails t1 where t1.PrgVerId='" & PrgVerId & "' and ReqGrpId is not null) ")
            .Append(" union ")
            ' Get Requirements that are assigned to lead group and not part of 
            ' requirements or requirement group
            .Append(" select Distinct B1.adReqId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,B1.Descrip, ")
            .Append(" B2.IsRequired as Required  from adReqs B1,adReqLeadGroups B2 where B1.adReqId = B2.adReqId ")
            .Append(" and B2.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
            .Append(" and B2.IsRequired=1  and B1.adReqTypeId=3 and B1.adReqId not in ")
            .Append(" (select adReqId from adPrgVerTestDetails where PrgVerId='" & PrgVerId & "' and adReqId is not null) ")
            .Append(" and B1.adReqId not in (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails  ")
            .Append(" where PrgVerId='" & PrgVerId & "' and adReqId is NULL)) ")
            .Append(" union ")
            .Append(" select '00000000-0000-0000-0000-000000000000' as adReqId,t1.ReqGrpId,NULL,0 as Required ")
            .Append(" from ")
            .Append(" adPrgVerTestDetails t1,adReqGroups t2 where t1.ReqGrpId = t2.ReqGrpId and ")
            .Append(" t2.ReqGrpId not in (select Distinct ReqGrpId from adReqGrpDef) and t1.PrgVerId='" & PrgVerId & "' and t1.ReqGrpId is not null ")
            'Modified By Balaji on 08/10/2005
            .Append(" union ")
            .Append(" select adReqId as adReqId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,Descrip as Descrip,1 as Required from adReqs where AppliesToAll=1 and adReqTypeId=3 ")
            .Append(" ) R1 where R1.Required = 1 ")
        End With

        'Execute the query
        Try
            Dim intRequiredDocsCount As Integer = db.RunParamSQLScalar(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            Return intRequiredDocsCount
        Catch e As Exception
            Return 0
        End Try
    End Function
    Public Function CheckIfLeadHasRequiredTest(LeadId As String, PrgVerId As String) As Integer
        'connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select count(*) as RequiredTest from ")
            .Append(" ( ")
            .Append(" select Distinct t1.adReqId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,t2.Descrip, ")
            .Append(" t3.IsRequired as Required ")
            .Append(" from adPrgVerTestDetails t1,adReqs t2,adReqLeadGroups t3 ")
            .Append(" where t1.adReqId = t2.adReqId and t2.adReqId = t3.adReqId and t1.PrgVerId='" & PrgVerId & "' ")
            .Append(" and t1.ReqGrpId is null and t2.adReqTypeId=1 and t3.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
            .Append(" union ")
            .Append(" select Distinct t5.adReqId as ReqId,t4.ReqGrpId,t5.Descrip,t6.IsRequired as Required ")
            .Append(" from  adReqGrpDef t4,adReqs t5,adReqLeadGroups t6 ")
            .Append(" where t4.adReqId = t5.adReqId and t5.adReqTypeId=1 and t5.adReqId = t6.adReqId and t6.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
            .Append(" and ReqGrpId in ")
            .Append(" (select t1.ReqGrpId from adPrgVerTestDetails t1 where t1.PrgVerId='" & PrgVerId & "' and ReqGrpId is not null) ")
            .Append(" union ")
            .Append(" select Distinct '00000000-0000-0000-0000-000000000000' as adReqId,t1.ReqGrpId,NULL,0 as Required ")
            .Append(" from ")
            .Append(" adPrgVerTestDetails t1,adReqGroups t2 where t1.ReqGrpId = t2.ReqGrpId and ")
            .Append(" t2.ReqGrpId not in (select Distinct ReqGrpId from adReqGrpDef) and t1.PrgVerId='" & PrgVerId & "' and t1.ReqGrpId is not null ")
            .Append(" union ")
            'Get Requirements that are applied to a leadgroup that are not part of a requirement 
            'group and not assigned to program version
            .Append(" select Distinct B1.adReqId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,B1.Descrip, ")
            .Append(" B2.IsRequired as Required   from adReqs B1,adReqLeadGroups B2 where B1.adReqId = B2.adReqId ")
            .Append(" and B2.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
            .Append(" and B2.IsRequired=1  and B1.adReqTypeId=1 and B1.adReqId not in ")
            .Append(" (select adReqId from adPrgVerTestDetails where PrgVerId='" & PrgVerId & "' and adReqId is not null) ")
            .Append(" and B1.adReqId not in ")
            .Append(" (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails ")
            .Append(" where PrgVerId='" & PrgVerId & "' and adReqId is NULL)) ")
            'Modified By Balaji on 08/10/2005
            'Get all mandatory requirements
            .Append(" union ")
            .Append(" select adReqId as adReqId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,Descrip,1 as Required from adReqs where AppliesToAll=1 and adReqTypeId=1 ")
            .Append(" ) R1 where R1.Required = 1 ")
        End With

        'Execute the query
        Try
            Dim intRequiredDocsCount As Integer = db.RunParamSQLScalar(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            Return intRequiredDocsCount
        Catch e As Exception
            Return 0
        End Try
    End Function
    Public Function CheckIfLeadHasRequiredTestNoProgramVersion(LeadId As String) As Integer
        'connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select count(*) as RequiredTest from ")
            .Append(" ( ")
            'Get Requirements that are applied to a leadgroup that are not part of a requirement 
            'group and not assigned to program version
            .Append(" select Distinct B1.adReqId,B1.Descrip, ")
            .Append(" B2.IsRequired as Required   from adReqs B1,adReqLeadGroups B2 where B1.adReqId = B2.adReqId ")
            .Append(" and B2.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
            .Append(" and B2.IsRequired=1  and B1.adReqTypeId=1 and B1.adReqId not in ")
            .Append(" (select Distinct adReqId from adPrgVerTestDetails where adReqId is not null) ")
            .Append(" and B1.adReqId not in ")
            .Append(" (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails ")
            .Append(" where adReqId is NULL)) ")
            'Modified By Balaji on 08/10/2005
            'Get all mandatory requirements
            .Append(" union ")
            .Append(" select adReqId as adReqId,Descrip,1 as Required from adReqs where AppliesToAll=1 and adReqTypeId=1 ")
            .Append(" ) R1 where R1.Required = 1 ")
        End With

        'Execute the query
        Try
            Dim intRequiredDocsCount As Integer = db.RunParamSQLScalar(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            Return intRequiredDocsCount
        Catch e As Exception
            Return 0
        End Try
    End Function
    Public Function CheckIfLeadHasRequiredDocsNoProgramVersion(LeadId As String) As Integer
        'connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select count(*) as RequiredDocs from ")
            .Append(" ( ")
            'Get Requirements that are applied to a leadgroup that are not part of a requirement 
            'group and not assigned to program version
            .Append(" select Distinct B1.adReqId,B1.Descrip, ")
            .Append(" B2.IsRequired as Required   from adReqs B1,adReqLeadGroups B2 where B1.adReqId = B2.adReqId ")
            .Append(" and B2.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
            .Append(" and B2.IsRequired=1  and B1.adReqTypeId=3 and B1.adReqId not in ")
            .Append(" (select Distinct adReqId from adPrgVerTestDetails where adReqId is not null) ")
            .Append(" and B1.adReqId not in ")
            .Append(" (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails ")
            .Append(" where adReqId is NULL)) ")
            'Modified By Balaji on 08/10/2005
            'Get all mandatory requirements
            .Append(" union ")
            .Append(" select adReqId as adReqId,Descrip,1 as Required from adReqs where AppliesToAll=1 and adReqTypeId=3 ")
            .Append(" ) R1 where R1.Required = 1 ")
        End With

        'Execute the query
        Try
            Dim intRequiredDocsCount As Integer = db.RunParamSQLScalar(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            Return intRequiredDocsCount
        Catch e As Exception
            Return 0
        End Try
    End Function
    Public Function CheckIfLeadHasNotPassedTest(LeadId As String) As Integer
        'connect to the database
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select Count(R1.Descrip) from   ")
            .Append(" ( ")
            'else get the name of test that was failed and was not overriden
            .Append("  select Distinct t2.Descrip as Descrip from adLeadEntranceTest t1,adReqs t2 ")
            .Append("  where t1.EntrTestId = t2.adReqId And t1.Required = 1 ")
            .Append("  and t1.Pass = 0 and t1.LeadId='" & LeadId & "'  and ")
            .Append(" t1.EntrTestId not in (select EntrTestId from adEntrTestOverRide where LeadId='" & LeadId & "' and OverRide='Yes')  ")
            .Append(" union ")
            'Get a list of new tests that have not been taken by the lead
            .Append(" select t1.Descrip from adReqs t1,adReqLeadGroups t2,adLeads t3 where ")
            .Append(" t1.adReqId = t2.adReqId And t2.LeadGrpId = t3.LeadGrpId And t2.IsRequired = 1 ")
            .Append(" and t1.adReqTypeId = 1 and t3.LeadId = '" & LeadId & "' and t1.adReqId not in ")
            .Append(" (select Distinct EntrTestId from adLeadEntranceTest where LeadId='" & LeadId & "') and  ")
            .Append(" t1.adReqId not in (select EntrTestId from adEntrTestOverRide where LeadId='" & LeadId & "' and OverRide='Yes')  ")
            .Append(" union ")
            'Get the list of tests that are mandatory (applies to all)
            .Append(" select Distinct Descrip as Descrip from adReqs where adReqTypeId=1 and AppliesToAll=1 ")
            .Append(" and adReqId not in (select adReqId from adReqLeadGroups)  ")
            .Append(" and adReqId not in (select Distinct EntrTestId from adLeadEntranceTest    ")
            .Append(" where LeadId='" & LeadId & "') and adReqId not in (select EntrTestId from adEntrTestOverRide where LeadId='" & LeadId & "' and OverRide='Yes')  ")
            .Append(" ) R1 ")
        End With

        'Execute the query
        Try
            Dim intLeadFailedTestCount As Integer = db.RunParamSQLScalar(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            Return intLeadFailedTestCount
        Catch e As Exception
            Return 0
        End Try
    End Function


    Public Sub InsertPrgVersionForLead(LeadId As String, PrgVerId As String)

        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        With sb
            .Append(" select count(*) from adLeads where LeadId=? and PrgVerId=? ")
        End With
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim intPrgVerCount As Integer = db.RunParamSQLScalar(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        If intPrgVerCount >= 1 Then
        Else
            With sb
                .Append(" select PrgVerId,PrgGrpId,ProgId from arPrgVersions where PrgVerId=? ")
            End With
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Dim dr5 As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            Dim strPrgVerId, strPrgGrpId, strProgTypeId As String
            While dr5.Read()
                strPrgVerId = CType(dr5("PrgVerId"), Guid).ToString
                strPrgGrpId = CType(dr5("PrgGrpId"), Guid).ToString
                strProgTypeId = CType(dr5("ProgId"), Guid).ToString
            End While

            If Not dr5.IsClosed Then dr5.Close()

            db.ClearParameters()
            sb.Remove(0, sb.Length)

            With sb
                .Append(" update adLeads set PrgVerId=?,AreaId=?,ProgramId=? where LeadId=? ")
            End With
            db.AddParameter("@PrgVerId", strPrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PrgGrpId", strPrgGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ProgTypeId", strProgTypeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

        End If

    End Sub
    Public Sub UpdateExpectedDateForLead(LeadId As String, ExpectedStartDate As String)

        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        With sb
            .Append(" select count(*) from adLeads where LeadId=? and ExpectedStart is not null  ")
        End With
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Dim intExpDtCount As Integer = db.RunParamSQLScalar(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        If intExpDtCount >= 1 Then
        Else

            With sb
                .Append(" update adLeads set ExpectedStart=? where LeadId=? ")
            End With

            db.AddParameter("@ExpectedStartDate", ExpectedStartDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

        End If

    End Sub

    Public Function CheckLeadHasGender(LeadId As String) As Integer
        Dim db As New DataAccess
        Dim sbEnroll As New StringBuilder
        With sbEnroll
            .Append(" select Count(*) from adLeads where LeadId='" & LeadId & "' and (Gender is null or ")
            .Append(" Gender in (select GenderId from adGenders where GenderCode='U')) ")
        End With
        Dim intLeadGenderUnknown As Integer = db.RunParamSQLScalar(sbEnroll.ToString)
        Return intLeadGenderUnknown
    End Function
    Public Function CheckLeadExists(FirstName As String, LastName As String, SSN As String) As Integer
        Dim db As New DataAccess
        Dim sbEnroll As New StringBuilder
        With sbEnroll
            .Append("select count(*) from adLeads where firstname='" & FirstName & "' and LastName ='" & LastName & "' and SSN=? ")
        End With
        db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim intLeadExists As Integer = db.RunParamSQLScalar(sbEnroll.ToString)
        Return intLeadExists
    End Function
    Public Function CheckIfLeadWasEnrolled(LeadId As String) As Boolean
        Dim db As New DataAccess
        Dim sbEnroll As New StringBuilder
        With sbEnroll
            .Append(" select Count(*) from adLeads where LeadId=? and LeadStatus in ")
            .Append(" (select Distinct StatusCodeId from syStatusCodes where SysStatusId=6) ")
        End With
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Dim intLeadExists As Integer = db.RunParamSQLScalar(sbEnroll.ToString)
        If intLeadExists >= 1 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function GetClassesWithStudentStartDate(StuEnrollId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim ds As DataSet

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("Select t1.ClsSectionId, t2.StartDate ")
            .Append("from aClassSections t1, arStuEnrollments t2 ")
            .Append("where t1.StartDate = t2.StartDate and t2.StuEnrollId = ? ")
        End With
        db.OpenConnection()
        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    Public Function RegisterStudentForClassesWithSameStartDate(StuEnrollId As String, StartDate As String, user As String, Optional ByVal shiftId As String = "") As String
        Dim db As New DataAccess

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")


            'Insert row into syCampGrps table
            Dim sb As New StringBuilder
            With sb
                .Append("insert into arResults(StuEnrollId,ModUser,ModDate,TestId) ")

                .Append("Select ?, ?, ?, t1.ClsSectionId from arClassSectionTerms t1,arTerm t2,arClassSections t3 where   ")

                .Append(" t1.TermId = t2.TermId and t2.StartDate = ? and t1.ClsSectionId=t3.ClsSectionId and t2.ProgId in ( ")

                .Append("   Select ProgId from arPrgVersions where PrgVerId in ( ")

                .Append("   Select PrgVerId from arStuEnrollments where StuEnrollId = ? ")

                .Append("       ) ")

                .Append("       ) ")

                If shiftId <> "" Then
                    .Append("and t3.ShiftId = ? ")
                Else
                    'We don't want to register the students for any class if there is no shift
                    'specified.
                    .Append("and 1 = 2 ")
                End If


            End With

            db.AddParameter("@stuenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ''ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ''ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'db.AddParameter("@StartDate2", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@stuenrollid2", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            If shiftId <> "" Then
                db.AddParameter("@shiftid", shiftId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   commit transaction
            groupTrans.Commit()

            '   return with no errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   do not report sql lost connection
            If Not groupTrans.Connection Is Nothing Then
                ' Report the error
            Else
                '   report an error to the client
                DALExceptions.BuildErrorMessage(ex)
            End If

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function

    Public Function RegisterLeadForInitialCourses(PrgVerId As String, StuEnrollId As String, StartDate As String, user As String, shiftId As String) As String
        Dim rightNow As Date = Date.Now
        'Dim result As String = RegisterLeadForInitialCoursesOnly(PrgVerId, StuEnrollId, StartDate, user, shiftId, rightNow)
        Dim result As String = RegisterLeadForClassSections(PrgVerId, StuEnrollId, StartDate, user, shiftId, rightNow)
        If result.Length > 0 Then
            Return result
        Else
            'NEED TO REGISTER THE COMPONENTS ONLY FOR ROSS SCHOOL
            'If SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "true" Or _
            '   SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "yes" Then
            Return RegisterLeadForGradeBookComponents(StuEnrollId, user, rightNow)
            'End If
        End If
    End Function

    Public Function RegisterLeadForInitialCoursesOnly(PrgVerId As String, StuEnrollId As String, StartDate As String, user As String, shiftId As String, rightNow As Date) As String
        Dim db As New DataAccess

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            With sb
                .Append("insert into arResults (ResultId, TestId, StuEnrollId, ModUser, ModDate) ")
                .Append("Select  ")
                .Append("		newId(), ")
                .Append("		(case  ")
                .Append("			when exists (select * from arCourseReqs where ReqId=CS.ReqId and (CourseReqTypId=1 or CourseReqTypId=2)) ")
                .Append("               then	(select ClsSectionId ")
                .Append("                    from arClassSections CS1, arReqs RQ1 ")
                .Append("					where StartDate = (Select min(StartDate) from arClassSections where ReqId=RQ1.ReqId and StartDate >= ?) ")
                .Append("					and CS1.ReqId=RQ1.ReqId ")
                .Append("               		and (CS1.ReqId=(select PreCoReqId from arCourseReqs where ReqId=CS.ReqId))) ")
                .Append("				else	ClsSectionId ")
                .Append("			end) as ClsSectionId, ")
                .Append("		?, ")
                .Append("		?, ")
                .Append("		? ")
                .Append("from arClassSections CS, arReqs RQ ")
                .Append("where	CS.ReqId In ")
                .Append("           (select ")
                .Append("	            RGD.ReqId  ")
                .Append("           from arProgVerDef PVD, arReqs RQ, arReqGrpDef RGD ")
                .Append("           where ")
                .Append("	            PVD.ReqId=RQ.ReqId ")
                .Append("           and	RQ.ReqTypeId=2 ")
                .Append("           and	RQ.ReqId=RGD.GrpId ")
                .Append("           and	PVD.PrgVerId= ? ")
                .Append("           union all ")
                .Append("           select ")
                .Append("	            RQ.ReqId ")
                .Append("           from arProgVerDef PVD, arReqs RQ  ")
                .Append("           where	PVD.ReqId=RQ.ReqId  ")
                .Append("           and	RQ.ReqTypeId=1  ")
                .Append("           and	PVD.PrgVerId= ? ) ")
                .Append("and    CS.ReqId=RQ.ReqId ")
                .Append("and    CS.StartDate = (Select min(StartDate) from arClassSections where ReqId=RQ.ReqId and StartDate >= ?) ")
                .Append("and	CS.TermId in (Select distinct TermId from arTerm where ProgId=(Select ProgId from arPrgVersions where PrgVerId=(Select PrgVerId from arStuEnrollments where StuEnrollId=?))) ")
                .Append("and	((CS.ShiftId = (Select ShiftId from arStuEnrollments where StuEnrollId=?)) Or (CS.ShiftId is null and (Select ShiftId from arStuEnrollments where StuEnrollId=?) is null)) ")
                .Append("union all ")
                .Append("Select  ")
                .Append("		newId(), ")
                .Append("		ClsSectionId, ")
                .Append("		?, ")
                .Append("		?, ")
                .Append("		? ")
                .Append("from arClassSections CS, arCourseReqs CR, arReqs RQ ")
                .Append("where	CS.ReqId in ")
                .Append("           (select ")
                .Append("	            RGD.ReqId  ")
                .Append("           from arProgVerDef PVD, arReqs RQ, arReqGrpDef RGD ")
                .Append("           where ")
                .Append("	            PVD.ReqId=RQ.ReqId ")
                .Append("           and	RQ.ReqTypeId=2 ")
                .Append("           and	RQ.ReqId=RGD.GrpId ")
                .Append("           and	PVD.PrgVerId= ? ")
                .Append("           union all ")
                .Append("           select ")
                .Append("	            RQ.ReqId ")
                .Append("           from arProgVerDef PVD, arReqs RQ  ")
                .Append("           where	PVD.ReqId=RQ.ReqId  ")
                .Append("           and	RQ.ReqTypeId=1  ")
                .Append("           and	PVD.PrgVerId= ? ) ")
                .Append("and    CS.ReqId=RQ.ReqId ")
                .Append("and    CS.StartDate = (Select min(StartDate) from arClassSections where ReqId=RQ.ReqId and StartDate >= ? ) ")
                .Append("and	CS.ReqId=CR.ReqId  ")
                .Append("and	CR.CourseReqTypId=2 ")
                .Append("and	CS.TermId in (Select distinct TermId from arTerm where ProgId=(Select ProgId from arPrgVersions where PrgVerId=(Select PrgVerId from arStuEnrollments where StuEnrollId=?))) ")
                .Append("and	((CS.ShiftId = (Select ShiftId from arStuEnrollments where StuEnrollId=?)) Or (CS.ShiftId is null and (Select ShiftId from arStuEnrollments where StuEnrollId=?) is null)) ")
            End With
            'StartDate
            db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'StuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'PrgVerId
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'PrgVerId
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'StartDate
            db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'StuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'StuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'StuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'StuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'PrgVerId
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'PrgVerId
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'StartDate
            db.AddParameter("@StartDate", StartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'StuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'StuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'StuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   commit transaction
            groupTrans.Commit()

            '   return with no errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            '   do not report sql lost connection
            'If Not groupTrans.Connection Is Nothing Then
            '   report an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
            'End If

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function RegisterLeadForClassSections(PrgVerId As String, StuEnrollId As String, StartDate As String, user As String, shiftId As String, rightNow As Date) As String
        Dim ds As New DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        Dim sb As New StringBuilder
        With sb
            .Append("select ")
            .Append("		RGD.ReqId, ")
            .Append("		PVD.ReqSeq  ")
            .Append("from arProgVerDef PVD, arReqs RQ, arReqGrpDef RGD ")
            .Append("where ")
            .Append("		PVD.ReqId=RQ.ReqId ")
            .Append("and	RQ.ReqTypeId=2 ")
            .Append("and	RQ.ReqId=RGD.GrpId ")
            .Append("and	PVD.PrgVerId= ? ")
            .Append("union all ")
            .Append("select ")
            .Append("		RQ.ReqId, ")
            .Append("		PVD.ReqSeq  ")
            .Append("from arProgVerDef PVD, arReqs RQ  ")
            .Append("where	PVD.ReqId=RQ.ReqId  ")
            .Append("and	RQ.ReqTypeId=1  ")
            .Append("and	PVD.PrgVerId= ? ")
            .Append("Order by PVD.ReqSeq ")
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(myAdvAppSettings.AppSettings("ConString")))
        'PrgVerId
        sc.Parameters.Add(New OleDbParameter("PrgVerId", PrgVerId))
        'PrgVerId
        sc.Parameters.Add(New OleDbParameter("PrgVerId", PrgVerId))

        '   Create adapter to handle Reqs table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill GradeSystems table
        da.Fill(ds, "Reqs")

        sb = New StringBuilder()
        With sb
            .Append("Select  ")
            .Append("		CS.ClsSectionId, ")
            .Append("		CS.ReqId, ")
            .Append("		CS.StartDate, ")
            .Append("       CS.EndDate ")
            .Append("from arClassSections CS, arReqs RQ,arTerm T  ")
            .Append("where	CS.ReqId=RQ.ReqId and CS.TermId=T.TermId ")
            .Append("and		CS.StartDate >= ? and T.StartDate >= ? ")
            .Append("and		CS.TermId in (Select distinct TermId from arTerm where (Progid is null) or ProgId=(Select ProgId from arPrgVersions where PrgVerId=(Select PrgVerId from arStuEnrollments where StuEnrollId=?))) ")
            .Append("and        (CS.CampusId = (Select CampusId from arStuEnrollments where StuEnrollId=?)) ")
            .Append("and		((CS.ShiftId = (Select ShiftId from arStuEnrollments where StuEnrollId=?)) Or (CS.ShiftId is null and (Select ShiftId from arStuEnrollments where StuEnrollId=?) is null)) ")
            .Append("and		CS.ReqId in ")
            .Append("					( ")
            .Append("						select ")
            .Append("								RGD.ReqId ")
            .Append("						from arProgVerDef PVD, arReqs RQ, arReqGrpDef RGD ")
            .Append("						where ")
            .Append("								PVD.ReqId=RQ.ReqId ")
            .Append("						and	RQ.ReqTypeId=2 ")
            .Append("						and	RQ.ReqId=RGD.GrpId ")
            .Append("						and	PVD.PrgVerId= ? ")
            .Append("						union all ")
            .Append("						select ")
            .Append("								RQ.ReqId ")
            .Append("						from arProgVerDef PVD, arReqs RQ  ")
            .Append("						where	PVD.ReqId=RQ.ReqId  ")
            .Append("						and	RQ.ReqTypeId=1  ")
            .Append("						and	PVD.PrgVerId= ? ")
            .Append("					) ")
            .Append("Order by CS.StartDate ")
        End With

        '   build select command
        Dim sd As New OleDbCommand(sb.ToString, New OleDbConnection(myAdvAppSettings.AppSettings("ConString")))

        'StartDate
        sd.Parameters.Add(New OleDbParameter("StartDate", StartDate))
        'StartDate
        sd.Parameters.Add(New OleDbParameter("StartDate", StartDate))
        'StuEnrollId
        sd.Parameters.Add(New OleDbParameter("StuEnrollId", StuEnrollId))
        'StuEnrollId
        sd.Parameters.Add(New OleDbParameter("StuEnrollId", StuEnrollId))
        'StuEnrollId
        sd.Parameters.Add(New OleDbParameter("StuEnrollId", StuEnrollId))
        'StuEnrollId
        sd.Parameters.Add(New OleDbParameter("StuEnrollId", StuEnrollId))
        'PrgVerId
        sd.Parameters.Add(New OleDbParameter("PrgVerId", PrgVerId))
        'PrgVerId
        sd.Parameters.Add(New OleDbParameter("PrgVerId", PrgVerId))

        '   Create adapter to handle ClassSectionss table
        Dim db As New OleDbDataAdapter(sd)

        '   Fill GradeSystems table
        db.Fill(ds, "ClassSections")

        sb = New StringBuilder()
        With sb
            .Append("select  ")
            .Append("		ReqId, ")
            .Append("		PreCoReqId, ")
            .Append("		CourseReqTypId ")
            .Append("from	arCourseReqs ")
        End With

        '   build select command
        Dim se As New OleDbCommand(sb.ToString, New OleDbConnection(myAdvAppSettings.AppSettings("ConString")))

        '   Create adapter to handle ClassSectionss table
        Dim dc As New OleDbDataAdapter(se)

        '   Fill GradeSystems table
        dc.Fill(ds, "PreRequisites")

        Dim reqsTable As DataTable = ds.Tables("Reqs")
        reqsTable.Columns.Add("EndDate", GetType(Date))
        reqsTable.Columns.Add("ClsSectionId", GetType(Guid))
        Dim classSectionsTable As DataTable = ds.Tables("ClassSections")
        Dim preReqsTable As DataTable = ds.Tables("PreRequisites")

        'For i As Integer = 0 To reqsTable.Rows.Count - 1
        '    Dim row As DataRow = reqsTable.Rows(i)
        'Next
        Dim scheduledReqs As Integer = 0
        If reqsTable.Rows.Count > 1 Then
            While reqsTable.Rows.Count > scheduledReqs
                For Each row As DataRow In reqsTable.Rows
                    'if Req has not been scheduled try to schedule it
                    If row("ClsSectionId") Is DBNull.Value Then
                        If Not HasPreReqsNotScheduled(row, reqsTable, classSectionsTable, preReqsTable) Then
                            ScheduleReq(row, reqsTable, classSectionsTable, preReqsTable)
                            scheduledReqs += 1
                        End If
                    End If
                Next
            End While
        Else
            For Each row As DataRow In reqsTable.Rows
                If row("ClsSectionId") Is DBNull.Value Then
                    row("ClsSectionId") = GetEarliestClassSectionForReqId(row("ReqId"), classSectionsTable)
                    row("EndDate") = GetEarliestDateForReqId(row("ReqId"), classSectionsTable)
                End If
            Next
        End If

        Dim arResults As New DataTable()
        arResults.Columns.Add("ResultId", GetType(Guid))
        arResults.Columns.Add("TestId", GetType(Guid))
        arResults.Columns.Add("StuEnrollId", GetType(Guid))
        arResults.Columns.Add("ModUser", GetType(String))
        arResults.Columns.Add("ModDate", GetType(DateTime))

        Dim df As New OleDbDataAdapter("Select ResultId, TestId, StuEnrollId, ModUser, ModDate from arResults", New OleDbConnection(myAdvAppSettings.AppSettings("ConString")))
        Try
            '   build insert, update and delete commands for GradeSystems table
            Dim cb As New OleDbCommandBuilder(df)
            For i As Integer = 0 To reqsTable.Rows.Count - 1
                Dim row As DataRow = reqsTable.Rows(i)
                If Not (row("ClsSectionId") = Guid.Empty) Then
                    Dim newRow As DataRow = arResults.NewRow()
                    newRow("ResultId") = Guid.NewGuid()
                    newRow("TestId") = row("ClsSectionId")
                    newRow("StuEnrollId") = New Guid(StuEnrollId)
                    newRow("ModUser") = user
                    newRow("ModDate") = rightNow
                    arResults.Rows.Add(newRow)
                End If
            Next
            df.Update(arResults)
            Return ""
        Catch ex As OleDbException
            '   report an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        End Try
    End Function
    Private Function HasPreReqsNotScheduled(row As DataRow, reqsTable As DataTable, classSectionsTable As DataTable, preReqsTable As DataTable) As Boolean
        'get preRequisites
        Dim preReqs As DataRow() = preReqsTable.Select("ReqId='" + CType(row("ReqId"), Guid).ToString() + "'", Nothing)
        'check if there are no preReqs 
        If preReqs.Length = 0 Then Return False
        For i As Integer = 0 To preReqs.Length - 1
            If Not ReqIsScheduled(preReqs(i)("PreCoreqId"), reqsTable) Then Return True
        Next
        Return False
    End Function

    Private Function ReqIsScheduled(reqId As Guid, reqsTable As DataTable) As Boolean
        Dim pr As DataRow() = reqsTable.Select("ReqId='" + reqId.ToString() + "'", Nothing)
        If pr(0)("ClsSectionId") Is DBNull.Value Then Return False Else Return True
    End Function

    Private Sub ScheduleReq(row As DataRow, reqsTable As DataTable, classSectionsTable As DataTable, preReqsTable As DataTable)
        'get preRequisites
        Dim preReqs As DataRow() = preReqsTable.Select("ReqId='" + CType(row("ReqId"), Guid).ToString() + "'", Nothing)
        'if there are preRequisites then get the latest startdate
        If preReqs.Length > 0 Then
            Dim latestDate As Date = Date.MinValue
            ' get prerequisites 
            For j As Integer = 0 To preReqs.Length - 1
                Dim reqId As Guid = preReqs(j)("PreCoReqId")
                Dim pr As DataRow() = reqsTable.Select("ReqId='" + reqId.ToString() + "'", Nothing)
                Dim reqIdDate As Date = pr(0)("EndDate")
                If reqIdDate > latestDate Then
                    latestDate = reqIdDate
                End If
            Next
            'select earliest class section later than latest prerequisite start
            row("ClsSectionId") = GetEarliestClassSectionForReqId(row("ReqId"), latestDate, classSectionsTable)
            row("EndDate") = GetEndDateForClassSectionId(row("ClsSectionId"), classSectionsTable)
        Else
            'there are no prerequisites... then assign the earliest date
            row("ClsSectionId") = GetEarliestClassSectionForReqId(row("ReqId"), classSectionsTable)
            row("EndDate") = GetEarliestDateForReqId(row("ReqId"), classSectionsTable)
        End If

    End Sub

    Private Function GetEarliestDateForReqId(reqId As Guid, classSectionsTable As DataTable) As Date
        Dim rows As DataRow() = classSectionsTable.Select("ReqId='" + reqId.ToString + "'", "StartDate asc")
        If rows.Length > 0 Then
            Return rows(0)("EndDate")
        Else
            Return Date.MaxValue
        End If
    End Function
    Private Function GetEarliestClassSectionForReqId(reqId As Guid, classSectionsTable As DataTable) As Guid
        Dim rows As DataRow() = classSectionsTable.Select("ReqId='" + reqId.ToString + "'", "StartDate asc")
        If rows.Length > 0 Then
            Return CType(rows(0)("ClsSectionId"), Guid)
        Else
            Return Guid.Empty
        End If
    End Function
    Private Function GetEarliestClassSectionForReqId(reqId As Guid, earliestDate As Date, classSectionsTable As DataTable) As Guid
        Dim rows As DataRow() = classSectionsTable.Select("ReqId='" + reqId.ToString + "' and StartDate > '" + earliestDate.ToShortDateString + "'", "StartDate asc")
        If rows.Length > 0 Then
            Return CType(rows(0)("ClsSectionId"), Guid)
        Else
            Return Guid.Empty
        End If
    End Function
    Private Function GetEndDateForClassSectionId(classSectionId As Guid, classSectionsTable As DataTable) As Date
        If classSectionId = Guid.Empty Then Return Date.MaxValue
        Dim rows As DataRow() = classSectionsTable.Select("ClsSectionId='" + classSectionId.ToString + "'", Nothing)
        If rows.Length > 0 Then
            Return CType(rows(0)("EndDate"), Date)
        Else
            Return Date.MaxValue
        End If
    End Function
    Public Function RegisterLeadForGradeBookComponents(StuEnrollId As String, user As String, rightNow As Date) As String
        Dim db As New DataAccess

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder

            'Modified by Balaji on 04/03/2009 to fix issue 13655
            'When a student is registered for a class, records were inserted in to arResults table for all schools
            'For Ross Type School (defined by ShowRossOnlyTabs entry in web.config), in addition to inserting records in to arresults, records were inserted into arGrdBkResults table
            'It was realized later that for non-ross type schools, if student is registered in an Externship class then record needs to be inserted into 
            'arGrdBkResults table, otherwise records will not show up in Post Externship Attendance and Attendance cannot be posted for the externship hours attended by the student
            If myAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "true" Or _
              myAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "yes" Then
                With sb
                    .Append("insert into arGrdBkResults (GrdBkResultId, ClsSectionId, InstrGrdBkWgtDetailId, Comments, StuEnrollId, ModUser, ModDate) ")
                    .Append("select ")
                    .Append("		NewId() as GrdBkResultId,  ")
                    .Append("		CS.ClsSectionId, ")
                    .Append("		GBWD.InstrGrdBkWgtDetailId, ")
                    .Append("       GCT.Descrip, ")
                    .Append("		R.StuEnrollId, ")
                    .Append("		?, ")
                    .Append("		? ")
                    .Append("from	arResults R, arClassSections CS,arterm T, arGrdBkWeights GBW, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT  ")
                    .Append("where	R.StuEnrollId=? ")
                    .Append("and		R.TestId=CS.ClsSectionId  ")
                    .Append("and		CS.ReqId=GBW.ReqId ")
                    ''Added by Saraswathi lakshmanan on April 2nd 2009
                    ''To fix mantis issue 15642 Auto registration is selecting inactive terms
                    .Append(" and CS.TermId=T.Termid and T.StatusId in(Select StatusId from Systatuses where Status like 'Active' ) ")
                    .Append("and		GBW.InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId  ")
                    .Append("and		GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId  ")
                    .Append("and		GBW.EffectiveDate = (Select max(EffectiveDate) from arGrdBkWeights where ReqId=CS.ReqId and EffectiveDate <= CS.StartDate) ")
                    .Append("and		not exists (select *  ")
                    .Append("					from arGrdBkResults  ")
                    .Append("					where ClsSectionId=CS.ClsSectionId ")
                    .Append("					and	  InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
                    .Append("					and	  StuEnrollId=?) ")
                End With
            Else
                With sb
                    .Append("insert into arGrdBkResults (GrdBkResultId, ClsSectionId, InstrGrdBkWgtDetailId, Comments, StuEnrollId, ModUser, ModDate) ")
                    .Append("select ")
                    .Append("		NewId() as GrdBkResultId,  ")
                    .Append("		CS.ClsSectionId, ")
                    .Append("		GBWD.InstrGrdBkWgtDetailId, ")
                    .Append("       GCT.Descrip, ")
                    .Append("		R.StuEnrollId, ")
                    .Append("		?, ")
                    .Append("		? ")
                    .Append("from	arResults R, arClassSections CS,arterm T, arGrdBkWeights GBW, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT,arReqs RQ   ")
                    .Append("where	R.StuEnrollId=? ")
                    .Append("and		R.TestId=CS.ClsSectionId  ")
                    .Append("and		CS.ReqId=GBW.ReqId ")
                    ''Added by Saraswathi lakshmanan on April 2nd 2009
                    ''To fix mantis issue 15642 Auto registration is selecting inactive terms
                    .Append(" and CS.TermId=T.Termid and T.StatusId in(Select StatusId from Systatuses where Status like 'Active' ) ")
                    .Append("and		GBW.InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId  ")
                    .Append("and		GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId  and CS.ReqId=RQ.ReqId and RQ.IsExternShip=1   and GCT.SysComponentTypeid=544 and R.score is null and R.GrdSysDetailid is null ")
                    .Append("and		GBW.EffectiveDate = (Select max(EffectiveDate) from arGrdBkWeights where ReqId=CS.ReqId and EffectiveDate <= CS.StartDate) ")
                    .Append("and		not exists (select *  ")
                    .Append("					from arGrdBkResults  ")
                    .Append("					where ClsSectionId=CS.ClsSectionId ")
                    .Append("					and	  InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
                    .Append("					and	  StuEnrollId=?) ")
                End With
            End If
            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'ModDate
            db.AddParameter("@ModDate", rightNow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'stuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'stuEnrollId
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute query
            db.CommandTimeout = 90
            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            '   commit transaction
            groupTrans.Commit()

            '   return with no errors
            Return ""

        Catch ex As OleDbException
            '   rollback transaction if there were errors
            groupTrans.Rollback()

            ''   do not report sql lost connection
            'If Not groupTrans.Connection Is Nothing Then
            '    '   report an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
            'End If

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function CheckIfLeadStatusMapsToEnrollment(CampusId As String, UserId As String, strCurrentLeadStatus As String) As String
        'connect to the database
        Dim db As New DataAccess
        'Dim ds As New DataSet
        'Dim da6 As New OleDbDataAdapter

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" select Count(*) from syLeadStatusChanges t1,syLeadStatusChangePermissions t2,syUsersRolesCampGrps t3,syCmpGrpCmps t4 ")
            .Append(" where ")
            .Append(" t1.LeadStatusChangeId = t2.LeadStatusChangeId and t1.OrigStatusId=? and ")
            .Append(" t1.NewStatusId in (select Distinct StatusCodeId from syStatusCodes where SysStatusId=6) and ")
            .Append(" t2.RoleId = t3.RoleId and ")
            .Append(" t3.UserId=? and ")
            .Append(" t3.CampGrpId = t4.CampGrpId and t4.CampusId=? ")
        End With
        db.AddParameter("@OrigStatusId", strCurrentLeadStatus, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim intLeadEligibleForEnrollment As Integer = db.RunParamSQLScalar(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        If intLeadEligibleForEnrollment >= 1 Then
            Return ""
        Else
            Return "Not Eligible To Enroll"
        End If
    End Function
    Public Function GetExpectedGradDateForModuleStart(prgVerId As String, expStartDate As String, campusId As String, shiftId As String) As String
        'connect to the database
        Dim db As New DataAccess
        Dim maxDate As String
        Dim sb As New StringBuilder

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT MAX(t2.EndDate) ")
            .Append("FROM arClassSectionTerms t1, arClassSections t2, arTerm t3 ")
            .Append("WHERE t1.ClsSectionId=t2.ClsSectionId ")
            .Append("AND t1.TermId=t3.TermId ")
            .Append("AND t3.ProgId=(SELECT ProgId ")
            .Append("               FROM arPrgVersions ")
            .Append("               WHERE PrgVerId = ?) ")
            .Append("AND t3.StartDate = ? ")
            .Append("AND t2.CampusId =? ")
            .Append("AND t2.ShiftId=? ")
        End With

        db.AddParameter("@prgverid", prgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@sdate", expStartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@shiftid", shiftId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            maxDate = (db.RunParamSQLScalar(sb.ToString)).ToShortDateString
        Catch ex As Exception
            maxDate = ""
        End Try

        Return maxDate

    End Function
    Public Function CheckIfStatusMappedToFutureStart() As String
        Dim sbStatusCode As New StringBuilder
        Dim strStuEnrollmentsStatus As String
        Dim strSysStatusDescrip As String = ""
        Dim db As New DataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        'we have used Top 1 in the query to avoid multiple future start statuscodes from being populated
        'the Top 1 will be removed once the validation has been added to StatusCodes page
        With sbStatusCode
            .Append(" SELECT Distinct  A.StatusCodeID as StatusCodeID,A.StatusCodeDescrip as StatusCodeDescrip FROM ")
            .Append(" syStatusCodes A,sySysStatus B,syStatuses C,syStatusLevels D ")
            .Append(" where A.sysStatusID = B.sysStatusID And A.StatusID = C.StatusID And B.StatusLevelId = D.StatusLevelId ")
            .Append(" and C.Status = 'Active' and D.StatusLevelId = 2 and B.sysStatusId = 7 ")
            .Append(" ORDER BY A.StatusCodeDescrip  ")
        End With
        Dim drStatusCode As OleDbDataReader
        Try
            drStatusCode = db.RunParamSQLDataReader(sbStatusCode.ToString)
            While drStatusCode.Read()
                If Not (drStatusCode("StatusCodeId") Is DBNull.Value) Then strStuEnrollmentsStatus = CType(drStatusCode("StatusCodeId"), Guid).ToString Else strStuEnrollmentsStatus = ""
            End While
        Catch ex As Exception
            strStuEnrollmentsStatus = ""
        Finally
            db.ClearParameters()
            sbStatusCode.Remove(0, sbStatusCode.Length)
            drStatusCode.Close()
        End Try

        If strStuEnrollmentsStatus = "" Then
            With sbStatusCode
                .Append(" SELECT Distinct  B.SysStatusDescrip FROM ")
                .Append(" syStatusCodes A,sySysStatus B,syStatuses C,syStatusLevels D ")
                .Append(" where A.sysStatusID = B.sysStatusID And A.StatusID = C.StatusID And B.StatusLevelId = D.StatusLevelId ")
                .Append(" and C.Status = 'Active' and D.StatusLevelId = 2 and A.StatusCodeDescrip like 'Future%' ")

            End With
            Try
                drStatusCode = db.RunParamSQLDataReader(sbStatusCode.ToString)
                While drStatusCode.Read()
                    If Not (drStatusCode("SysStatusDescrip") Is DBNull.Value) Then strSysStatusDescrip = drStatusCode("SysStatusDescrip").ToString Else strSysStatusDescrip = ""
                End While
            Catch ex As Exception
                strSysStatusDescrip = ""
            Finally
                db.ClearParameters()
                sbStatusCode.Remove(0, sbStatusCode.Length)
                drStatusCode.Close()
            End Try
            If Not strSysStatusDescrip = "" Then
                Return strSysStatusDescrip
            End If
        Else
            Return ""
        End If
    End Function

    Private Function CreateRegisterCoursesTable() As DataTable
        Dim dtRegisterCourses As New DataTable()
        dtRegisterCourses.Columns.Add("ResultId", GetType(String))
        dtRegisterCourses.Columns.Add("ReqId", GetType(String))
        dtRegisterCourses.Columns.Add("TermId", GetType(String))
        dtRegisterCourses.Columns.Add("TermStartDate", GetType(DateTime))
        dtRegisterCourses.Columns.Add("TestId", GetType(String))
        dtRegisterCourses.Columns.Add("StuEnrollId", GetType(String))
        dtRegisterCourses.Columns.Add("ModUser", GetType(String))
        dtRegisterCourses.Columns.Add("ModDate", GetType(DateTime))
        Return dtRegisterCourses
    End Function
    Private Function CreateExternshipTable() As DataTable
        Dim dtExternship As New DataTable
        dtExternship.Columns.Add("ResultId", GetType(String))
        dtExternship.Columns.Add("ReqId", GetType(String))
        dtExternship.Columns.Add("TermId", GetType(String))
        dtExternship.Columns.Add("TermStartDate", GetType(DateTime))
        dtExternship.Columns.Add("TestId", GetType(String))
        dtExternship.Columns.Add("StuEnrollId", GetType(String))
        dtExternship.Columns.Add("ModUser", GetType(String))
        dtExternship.Columns.Add("ModDate", GetType(DateTime))
        Return dtExternship
    End Function
    Private Function CreatePreReqTable() As DataTable
        Dim dtGetPreReq As New DataTable
        dtGetPreReq.Columns.Add("ResultId", GetType(String))
        dtGetPreReq.Columns.Add("ReqId", GetType(String))
        dtGetPreReq.Columns.Add("TermId", GetType(String))
        dtGetPreReq.Columns.Add("TermStartDate", GetType(DateTime))
        dtGetPreReq.Columns.Add("TestId", GetType(String))
        dtGetPreReq.Columns.Add("StuEnrollId", GetType(String))
        dtGetPreReq.Columns.Add("ModUser", GetType(String))
        dtGetPreReq.Columns.Add("ModDate", GetType(DateTime))
        Return dtGetPreReq
    End Function
    Public Function DoesTermExist(ExpStartDate As Date) As Boolean
        Dim db As New DataAccess
        'Dim intCounter As Integer = 0
        Dim sbGetCourses As New StringBuilder
        Dim strTermId As String = ""

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        With sbGetCourses
            .Append(" Select Distinct Top 1 TermId  from " + vbCrLf)
            .Append(" arTerm where StartDate=? " + vbCrLf)
        End With
        db.AddParameter("@StartDate", ExpStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        Try
            strTermId = db.RunParamSQLScalar(sbGetCourses.ToString).ToString
        Catch ex As Exception
            strTermId = ""
        Finally
            db.ClearParameters()
            sbGetCourses.Remove(0, sbGetCourses.Length)
        End Try
        If strTermId = "" Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Function GetShiftId(StuEnrollId As String) As String
        Dim db As New DataAccess
        'Dim intCounter As Integer = 0
        Dim sbShift As New StringBuilder
        Dim strShiftId As String = ""

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        With sbShift
            .Append("Select distinct ShiftId from arStuEnrollments where StuEnrollId=? ")
        End With
        db.ClearParameters()
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strShiftId = db.RunParamSQLScalar(sbShift.ToString).ToString
        Catch ex As Exception
            strShiftId = ""
        Finally
            db.ClearParameters()
            sbShift.Remove(0, sbShift.Length)
        End Try
        Return strShiftId
    End Function
    Public Function GetAllCourses(StuEnrollId As String, _
                                  Externship As Boolean, _
                                  PrgVerId As String, _
                                  ExpStartDate As Date) As DataSet
        Dim db As New DataAccess
        'Dim intCounter As Integer = 0
        Dim dsGetCourses As DataSet
        Dim sbGetAllTermsAndClasses As New StringBuilder

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        With sbGetAllTermsAndClasses
            .Append(" Select Distinct t1.ClsSectionId,t2.ReqId,t2.Code,t2.Descrip,t3.TermId,t3.TermDescrip," + vbCrLf)
            .Append(" t3.StartDate, t3.EndDate,t2.IsExternship as Externship,t1.ShiftId,t1.LeadGrpId " + vbCrLf)
            .Append(" from arClassSections t1,arReqs t2, ")
            .Append(" (select * from arTerm where ProgId is NOT NULL) t3,arProgVerDef t4,arPrgVersions t5  " + vbCrLf)
            .Append(" where t1.ReqId = t2.ReqId And t1.TermId = t3.TermId and " + vbCrLf)
            .Append(" t1.ReqId = t4.ReqId and t4.PrgVerId=t5.PrgVerId and t5.ProgId=t3.ProgId and " + vbCrLf)
            .Append(" t4.PrgVerId=? and t3.StartDate>=? " + vbCrLf)
            If Externship = True Then
                .Append(" and t2.IsExternship=1 " & vbCrLf)
                .Append(" and t3.TermId not in " & vbCrLf)
                .Append(" (select Distinct t2.TermId from arResults t1,arClassSections t2 " & vbCrLf)
                .Append(" where t1.TestId=t2.ClsSectionId and t1.StuEnrollId=?) " & vbCrLf)
                .Append(" and t3.StartDate > (select Distinct max(t3.StartDate) from arResults t1,arClassSections t2,arTerm t3 " & vbCrLf)
                .Append(" where t1.TestId = t2.ClsSectionId And t2.TermId = t3.TermId and t1.StuEnrollId=?) " & vbCrLf)
            Else
                .Append(" and t2.IsExternship=0 " & vbCrLf)
            End If
            .Append(" Union ")
            .Append(" Select Distinct t1.ClsSectionId,t2.ReqId,t2.Code,t2.Descrip,t3.TermId,t3.TermDescrip," + vbCrLf)
            .Append(" t3.StartDate, t3.EndDate,t2.IsExternship as Externship,t1.ShiftId,t1.LeadGrpId " + vbCrLf)
            .Append(" from arClassSections t1,arReqs t2,(select * from arTerm where ProgId is NULL) t3,arProgVerDef t4 " + vbCrLf)
            .Append(" where t1.ReqId = t2.ReqId And t1.TermId = t3.TermId And " + vbCrLf)
            .Append(" t1.ReqId = t4.ReqId and t4.PrgVerId=? and t3.StartDate>=? " + vbCrLf)
            If Externship = True Then
                .Append(" and t2.IsExternship=1 " & vbCrLf)
                .Append(" and t3.TermId not in " & vbCrLf)
                .Append(" (select Distinct t2.TermId from arResults t1,arClassSections t2 " & vbCrLf)
                .Append(" where t1.TestId=t2.ClsSectionId and t1.StuEnrollId=?) " & vbCrLf)
                .Append(" and t3.StartDate > (select Distinct max(t3.StartDate) from arResults t1,arClassSections t2,arTerm t3 " & vbCrLf)
                .Append(" where t1.TestId = t2.ClsSectionId And t2.TermId = t3.TermId and t1.StuEnrollId=?) " & vbCrLf)
            Else
                .Append(" and t2.IsExternship=0 " & vbCrLf)
            End If
            .Append(" order by t3.StartDate,t3.EndDate,t3.termdescrip,t2.Code ")
        End With
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StartDate", ExpStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        If Externship = True Then
            db.AddParameter("@StuEnrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StartDate", ExpStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        If Externship = True Then
            db.AddParameter("@StuEnrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        Try
            dsGetCourses = db.RunParamSQLDataSet(sbGetAllTermsAndClasses.ToString)
            Return dsGetCourses
        Catch ex As Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sbGetAllTermsAndClasses.Remove(0, sbGetAllTermsAndClasses.Length)
        End Try
    End Function
    Private Function isPreReqAlreadyRegistered(dtGetAllPreReqsByCourse As DataTable, dtRegisterCourses As DataTable) As Boolean
        For Each drPreReq As DataRow In dtGetAllPreReqsByCourse.Rows
            For Each drGetCourses As DataRow In dtRegisterCourses.Rows
                If drGetCourses("ReqId").ToString.Trim = drPreReq("ReqId").ToString.Trim Then
                    Return True

                End If
            Next
        Next
        Return False
    End Function
    Private Function IsPrereqRegisteredInSameterm(dtGetAllPreReqsByCourse As DataTable, _
                                                  dtRegisterCourses As DataTable, strTermId As String) As Boolean
        For Each drPreReq As DataRow In dtGetAllPreReqsByCourse.Rows
            For Each drGetCourses As DataRow In dtRegisterCourses.Rows
                If drGetCourses("ReqId").ToString.Trim = drPreReq("ReqId").ToString.Trim Then
                    If drGetCourses("TermId").ToString.Trim = strTermId.ToString.Trim Then
                        'If the prereq is registered in the same term  as the input course
                        ' then skip this course.The rule is the course and its prereq
                        'should not be registered in the same term
                        Return True
                        Exit Function
                    End If
                End If
            Next
        Next
        Return False
    End Function
    Private Function IsStudentAlreadyAddedToRegisterDT(dtRegisterCourses As DataTable, strCourseId As String) As Boolean
        For Each drRow As DataRow In dtRegisterCourses.Rows
            If drRow("ReqId").ToString.Trim = strCourseId.ToString.Trim Then
                Return True

            End If
        Next
        Return False
    End Function
    Private Function DoesStudentGroupMatchTheClassStudentGroup(dtGetStudentGrp As DataTable, strClassLeadGrpId As String) As Boolean
        If dtGetStudentGrp Is Nothing Then
            Return False

        End If
        For Each drStudGrp As DataRow In dtGetStudentGrp.Rows
            If drStudGrp("LeadGrpId").ToString.Trim = strClassLeadGrpId.ToString.Trim Then
                Return True

            End If
        Next
        Return False
    End Function

    Private Function AddToRegisterCoursesDT(dsGetCourses As DataSet, _
                                            ByRef dtRegisterCourses As DataTable, _
                                            strShiftId As String, _
                                            StuEnrollId As String, _
                                            user As String, _
                                            dtGetStudentGrp As DataTable) As DataTable

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


        For Each dr As DataRow In dsGetCourses.Tables(0).Rows
            'First Validation : check if course is not externship
            'Second Validation: check if the course has prereqs and check if the student has registered for 
            'all the prereq.
            Dim strCourseId As String = dr("ReqId").ToString
            Dim strClsSectionId As String = dr("ClsSectionId").ToString
            Dim strTermId As String = dr("TermId").ToString
            Dim strTermStartDate As Date = CDate(dr("StartDate"))
            Dim strClassShiftId As String = ""
            Dim strClassLeadGrpId As String = ""

            If Not dr("LeadGrpId") Is DBNull.Value Then strClassLeadGrpId = dr("LeadGrpId").ToString Else strClassLeadGrpId = ""
            If Not dr("ShiftId") Is DBNull.Value Then strClassShiftId = dr("ShiftId").ToString Else strClassShiftId = ""

            'Check if student  shift matches the class section shift
            If Not strShiftId = "" And Not strClassShiftId = "" And strClassShiftId.ToString.Trim = strShiftId.ToString.Trim Then
                'If CType(dr("Externship"), Integer) = 0 Or CType(dr("Externship"), Boolean) = False Then
                Dim boolCourseHavePreReq As Boolean = False
                Dim boolPreReqAlreadyRegistered As Boolean = False
                'Dim strPreReq As String = ""
                If myAdvAppSettings.AppSettings("IgnorePrereqsForRunSchedules").ToString.ToLower = "false" Then
                    'validate for prereqs
                    boolCourseHavePreReq = DoesCourseHavePreReq(strCourseId)
                    If boolCourseHavePreReq = True Then
                        Dim dtGetAllPreReqsByCourse As DataTable = GetAllPreReqsForACourse(strCourseId)
                        Try
                            If dtGetAllPreReqsByCourse.Rows.Count >= 1 And dtRegisterCourses.Rows.Count >= 1 Then
                                boolPreReqAlreadyRegistered = isPreReqAlreadyRegistered(dtGetAllPreReqsByCourse, dtRegisterCourses)
                            Else
                                'works for externships
                                boolPreReqAlreadyRegistered = True
                            End If
                        Catch ex As Exception
                            boolPreReqAlreadyRegistered = False
                        End Try

                        'Dim dtIsAllPreReqsAlreadyRegistered As DataTable = IsAllPreReqsAlreadyRegistered(strCourseId, StuEnrollId)
                        If boolPreReqAlreadyRegistered = False Then
                            'There are some prereqs thats still not registered for this course
                            'skip those prereqs, it will be registered in coming terms
                        Else
                            'Register this course as there are no prereqs
                            Try
                                'Check if the prereq has been registered for the same term
                                'if so exit 
                                If dtGetAllPreReqsByCourse.Rows.Count >= 1 And dtRegisterCourses.Rows.Count >= 1 Then
                                    Dim boolIsPrereqRegisteredInSameTerm As Boolean = IsPrereqRegisteredInSameterm(dtGetAllPreReqsByCourse, dtRegisterCourses, strTermId)
                                    If boolIsPrereqRegisteredInSameTerm = True Then
                                        Exit Try
                                    End If
                                End If
                                'Dim boolIsStudentAlreadyRegistered As Boolean = False
                                Dim boolStudentAlreadyAddedToDT As Boolean = False
                                If dtRegisterCourses.Rows.Count >= 1 Then
                                    boolStudentAlreadyAddedToDT = IsStudentAlreadyAddedToRegisterDT(dtRegisterCourses, strCourseId)
                                End If
                                If boolStudentAlreadyAddedToDT = False Then
                                    Dim boolStudentGrpMatches As Boolean = False
                                    'check if the student's group matches the student group 
                                    'of the class, if there is no student group on class then register.
                                    'if there are student groups check if it matches to class student group
                                    If Not strClassLeadGrpId.Trim = "" Then
                                        boolStudentGrpMatches = DoesStudentGroupMatchTheClassStudentGroup(dtGetStudentGrp, strClassLeadGrpId)
                                    Else
                                        boolStudentGrpMatches = True
                                    End If
                                    If boolStudentGrpMatches = True Then
                                        Dim newRow As DataRow = dtRegisterCourses.NewRow()
                                        newRow("ResultId") = Guid.NewGuid.ToString
                                        newRow("ReqId") = strCourseId
                                        newRow("TermId") = strTermId
                                        newRow("TestId") = strClsSectionId.ToString
                                        newRow("StuEnrollId") = StuEnrollId.ToString
                                        newRow("TermStartDate") = strTermStartDate
                                        newRow("ModUser") = user
                                        newRow("ModDate") = Date.Now
                                        dtRegisterCourses.Rows.Add(newRow)
                                    End If
                                End If
                            Catch ex As Exception

                            End Try
                        End If
                    Else
                        'Register this course as there are no prereqs
                        'Dim boolIsStudentAlreadyRegistered As Boolean = False
                        Dim boolStudentAlreadyAddedToDT As Boolean = False
                        If dtRegisterCourses.Rows.Count >= 1 Then
                            boolStudentAlreadyAddedToDT = IsStudentAlreadyAddedToRegisterDT(dtRegisterCourses, strCourseId)
                        End If
                        If boolStudentAlreadyAddedToDT = False Then
                            Dim boolStudentGrpMatches As Boolean = False
                            'check if the student's group matches the student group 
                            'of the class, if there is no student group on class then register.

                            'if there are student groups check if it matches to class student group
                            If Not strClassLeadGrpId.Trim = "" Then
                                boolStudentGrpMatches = DoesStudentGroupMatchTheClassStudentGroup(dtGetStudentGrp, strClassLeadGrpId)
                            Else
                                boolStudentGrpMatches = True
                            End If
                            If boolStudentGrpMatches = True Then
                                Dim newRow As DataRow = dtRegisterCourses.NewRow()
                                newRow("ResultId") = Guid.NewGuid.ToString
                                newRow("ReqId") = strCourseId
                                newRow("TermId") = strTermId
                                newRow("TestId") = strClsSectionId.ToString
                                newRow("StuEnrollId") = StuEnrollId.ToString
                                newRow("TermStartDate") = strTermStartDate
                                newRow("ModUser") = user
                                newRow("ModDate") = Date.Now
                                dtRegisterCourses.Rows.Add(newRow)
                            End If
                        End If
                    End If
                Else
                    'as the web config entry ignores prereqs 
                    'the student can be registered in a course even if there are prereqs
                    'Dim boolIsStudentAlreadyRegistered As Boolean = False
                    Dim boolStudentAlreadyAddedToDT As Boolean = False
                    If dtRegisterCourses.Rows.Count >= 1 Then
                        boolStudentAlreadyAddedToDT = IsStudentAlreadyAddedToRegisterDT(dtRegisterCourses, strCourseId)
                    End If
                    If boolStudentAlreadyAddedToDT = False Then
                        Dim boolStudentGrpMatches As Boolean = False
                        'check if the student's group matches the student group 
                        'of the class, if there is no student group on class then register.
                        'if there are student groups check if it matches to class student group
                        If Not strClassLeadGrpId.Trim = "" Then
                            boolStudentGrpMatches = DoesStudentGroupMatchTheClassStudentGroup(dtGetStudentGrp, strClassLeadGrpId)
                        Else
                            boolStudentGrpMatches = True
                        End If
                        If boolStudentGrpMatches = True Then
                            Dim newRow As DataRow = dtRegisterCourses.NewRow()
                            newRow("ResultId") = Guid.NewGuid.ToString
                            newRow("ReqId") = strCourseId
                            newRow("TermId") = strTermId
                            newRow("TestId") = strClsSectionId.ToString
                            newRow("StuEnrollId") = StuEnrollId.ToString
                            newRow("TermStartDate") = strTermStartDate
                            newRow("ModUser") = user
                            newRow("ModDate") = Date.Now
                            dtRegisterCourses.Rows.Add(newRow)
                        End If
                    End If
                End If
            Else

            End If
        Next
        Return dtRegisterCourses
    End Function
    Public Function AutoRegisterForRegularTraditionalSchools(ExpStartDate As Date, _
                                                              PrgVerId As String, _
                                                              StuEnrollId As String, _
                                                              user As String) As String
        'Dim sbGetAllTermsAndClasses As New StringBuilder
        'Dim strStuEnrollmentsStatus As String
        'Dim strSysStatusDescrip As String = ""
        'Dim intTermCount As Integer = 0
        'Dim strIsCourseAPreReq As New StringBuilder
        Dim db As New DataAccess
        'Dim intdsCount As Integer = 0
        'Dim intCounter As Integer = 0
        'Dim strLastTermId As String = ""
        'Dim intCourseRegistered As Integer = 0
        'To get courses that have no prereqs and not marked as externship
        Dim dsGetCourses As DataSet
        Dim boolTerm As Boolean = False
        Dim strShiftId As String = ""
        'Dim strStudentGrpId As String = ""
        Dim dtGetStudentGrp As DataTable
        Dim dtRegisterCourses As DataTable = CreateRegisterCoursesTable()
        Dim dtExternship As DataTable = CreateExternshipTable()
        Dim strResultMessage As String = ""

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        boolTerm = DoesTermExist(ExpStartDate)
        If boolTerm = False Then
            Return "Lead was not registered in the courses because the expected start date does not match any of the term's start date"

        End If
        strShiftId = GetShiftId(StuEnrollId)
        Try
            dtGetStudentGrp = GetStudentGroup(StuEnrollId)
        Catch ex As Exception
        Finally
            db.ClearParameters()
        End Try

        Try
            dsGetCourses = GetAllCourses(StuEnrollId, False, PrgVerId, ExpStartDate)
            If Not dsGetCourses Is Nothing Then
                dtRegisterCourses = AddToRegisterCoursesDT(dsGetCourses, dtRegisterCourses, strShiftId, StuEnrollId, user, dtGetStudentGrp)
                strResultMessage = RegisterClasses(dtRegisterCourses)
            Else
                'the student's shift is empty or no shift assigned while creating a class
            End If
        Catch ex As Exception
        Finally
            dsGetCourses = Nothing
            dtRegisterCourses.Clear()
        End Try

        Try
            dsGetCourses = GetAllCourses(StuEnrollId, True, PrgVerId, ExpStartDate)
            If Not dsGetCourses Is Nothing Then
                dtRegisterCourses = AddToRegisterCoursesDT(dsGetCourses, dtRegisterCourses, strShiftId, StuEnrollId, user, dtGetStudentGrp)
                strResultMessage = RegisterClasses(dtRegisterCourses)
            Else
                'the student's shift is empty or no shift assigned while creating a class
            End If
        Catch ex As Exception
        Finally
            dsGetCourses = Nothing
            dtRegisterCourses.Clear()
        End Try

        'Insert data into regent xml 
        If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            Dim regDB As New regentDB
            Dim sb2 As New StringBuilder
            Dim studentId As String = ""
            With sb2
                .Append("select distinct studentid from arStuENrollments where StuEnrollId=?")
            End With
            db.ClearParameters()
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Try
                studentId = db.RunParamSQLScalar(sb2.ToString).ToString
            Catch ex As Exception
            End Try
            If Not studentId = "" Then
                Dim strFilename As String = AdvantageCommonValues.getStudentBatchFileName()
                regDB.AddAcademicXML(studentId, strFilename, "")
            End If
        End If

        Return strResultMessage

    End Function
    Public Function GetCourseCount(PrgVerId As String, ExpStartDate As Date) As Integer
        Dim db As New DataAccess
        Dim intCounter As Integer = 0
        Dim sbGetCourses As New StringBuilder

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        With sbGetCourses
            .Append(" Select Count(*) as CourseCount from ( " + vbCrLf)
            .Append(" Select Distinct t2.ReqId " + vbCrLf)
            .Append(" from arClassSections t1,arReqs t2, " + vbCrLf)
            .Append(" (select * from arTerm where ProgId is NOT NULL) t3,arProgVerDef t4,arPrgVersions t5  " + vbCrLf)
            .Append(" where t1.ReqId = t2.ReqId And t1.TermId = t3.TermId and " + vbCrLf)
            .Append(" t1.ReqId = t4.ReqId and t4.PrgVerId=t5.PrgVerId and t5.ProgId=t3.ProgId and " + vbCrLf)
            .Append(" t4.PrgVerId=? and t3.StartDate>=? " + vbCrLf)
            .Append(" Union ")
            .Append(" Select Distinct t2.ReqId " + vbCrLf)
            .Append(" from arClassSections t1,arReqs t2,(select * from arTerm where ProgId is NULL) t3,arProgVerDef t4 " + vbCrLf)
            .Append(" where t1.ReqId = t2.ReqId And t1.TermId = t3.TermId And " + vbCrLf)
            .Append(" t1.ReqId = t4.ReqId and t4.PrgVerId=? and t3.StartDate>=? " + vbCrLf)
            .Append(" ) GetCourses ")
        End With
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StartDate", ExpStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StartDate", ExpStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        Try
            intCounter = CType(db.RunParamSQLScalar(sbGetCourses.ToString), Integer)
        Catch ex As Exception
            intCounter = 0
        Finally
            db.ClearParameters()
            sbGetCourses.Remove(0, sbGetCourses.Length)
        End Try
        Return intCounter
    End Function
    Public Function GetStudentGroup(StuEnrollID As String) As DataTable
        Dim db As New DataAccess
        'Dim intCounter As Integer = 0
        Dim sbGetCourses As New StringBuilder
        Dim ds As DataSet

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        With sbGetCourses
            .Append(" Select Distinct LeadGrpId from adLeadByLeadGroups where StuEnrollId=?")
        End With
        db.ClearParameters()
        db.AddParameter("@StuEnrollId", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            ds = db.RunParamSQLDataSet(sbGetCourses.ToString)
            If ds.Tables(0).Rows.Count >= 1 Then
                Return ds.Tables(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sbGetCourses.Remove(0, sbGetCourses.Length)
        End Try
    End Function

    Public Function RegisterClasses(dtRegisterCourses As DataTable) As String
        'Dim db As New DataAccess
        'Dim intCount As Integer = 0
        Dim strErrorMessage As String = ""
        Dim intCounter As Integer = 0
        'Dim strTermId As String = ""
        Dim intregCount As Integer = 0
        intregCount = dtRegisterCourses.Rows.Count
        'Register all courses with no prereqs

        Dim dtView As New DataView(dtRegisterCourses)
        dtView.Sort = "TermStartDate Desc"

        For Each dr As DataRow In dtRegisterCourses.Rows
            intCounter += 1
            strErrorMessage &= InsertInToResults(dr, False, "")
            strErrorMessage &= vbCrLf
        Next
        Return strErrorMessage
    End Function
    Private Function InsertInToResults(dr As DataRow, IsExternship As Boolean, _
                                        Optional ByVal strLastTermId As String = "") As String
        Dim db As New DataAccess
        Dim strIsCourseAPreReq As New StringBuilder
        ' Dim intCount As Integer = 0
        ' Dim drReader As OleDbDataReader
        Dim sb As New StringBuilder
        Dim strClsSectionId As String = ""

        'If the course is an externship student needs to be registered in the class that belongs to 
        'last term
        'If no class was created for the last term then ignore registering the student for that course
        If IsExternship = True Then
            With sb
                .Append(" Select Distinct Top 1 ClsSectionId from arClassSections where " + vbCrLf)
                .Append(" ReqId=? and TermId in " & vbCrLf)
                .Append(" (select Distinct TermId from arTerm where StartDate > (select Distinct StartDate from arTerm where " + vbCrLf)
                .Append(" TermId=?)) ")
            End With
            db.ClearParameters()
            db.AddParameter("@ReqId", dr("ReqId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not strLastTermId.ToString.Trim = "" Then
                db.AddParameter("@TermId", strLastTermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@TermId", dr("TermId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            Try
                strClsSectionId = db.RunParamSQLScalar(sb.ToString).ToString
            Catch ex As Exception
            Finally
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            End Try
        Else
            'If course is not externship
            strClsSectionId = dr("TestId").ToString
        End If
        If Not strClsSectionId.ToString.Trim = "" Then
            With strIsCourseAPreReq
                .Append(" insert into arResults(ResultId,TestId,StuEnrollId,ModUser,ModDate) ")
                .Append(" values(?,?,?,?,?) ")
            End With
            db.AddParameter("@ResultId", dr("ResultId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TestId", strClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", dr("StuEnrollId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModUser", dr("ModUser"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModDate", dr("ModDate"), DataAccess.OleDbDataType.OleDbDateTime, ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(strIsCourseAPreReq.ToString)
                Return ""
            Catch ex As Exception
                db.ClearParameters()
                strIsCourseAPreReq.Remove(0, strIsCourseAPreReq.Length)
                db.CloseConnection()
                Return ex.Message
            End Try
        End If
    End Function
    Private Function DoesCourseHavePreReq(CourseId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim intCount As Integer = 0

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        With sb
            .Append(" Select Count(*) from arCourseReqs where ReqId=? ")
        End With
        db.AddParameter("@ReqId", CourseId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            intCount = db.RunParamSQLScalar(sb.ToString)
        Catch ex As Exception
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            db.CloseConnection()
        End Try
        If intCount = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    Private Function IsAllPreReqsAlreadyRegistered(strCourseId As String, StuEnrollId As String) As DataTable
        Dim db As New DataAccess
        Dim strIsCourseAPreReq As New StringBuilder
        Dim intCount As Integer = 0
        Dim drReader As OleDbDataReader

        Dim dtPreReqs As New DataTable()
        dtPreReqs.Columns.Add("Descrip", GetType(String))

        With strIsCourseAPreReq
            .Append(" Select Distinct t5.PreCoReqId,t6.Descrip from arCourseReqs t5,arReqs t6 where " + vbCrLf)
            .Append(" t5.PreCoReqId=t6.ReqId and t5.ReqId=? " + vbCrLf)
            .Append(" and t5.PreCoReqId not in (select Distinct t3.ReqId from arResults t1, " + vbCrLf)
            .Append(" arClassSections t2,arReqs t3 where t1.StuEnrollId=? and " + vbCrLf)
            .Append(" t1.TestId=t2.ClsSectionId and t2.reqId=t3.ReqId) " + vbCrLf)
        End With
        db.AddParameter("@ReqId", strCourseId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            drReader = db.RunParamSQLDataReader(strIsCourseAPreReq.ToString)
            While drReader.Read
                Dim newRow As DataRow = dtPreReqs.NewRow()
                newRow("Descrip") = drReader("Descrip")
                dtPreReqs.Rows.Add(newRow)
            End While

            If Not drReader.IsClosed Then drReader.Close()

        Catch ex As Exception
            db.ClearParameters()
            strIsCourseAPreReq.Remove(0, strIsCourseAPreReq.Length)
        End Try
        If dtPreReqs.Rows.Count >= 1 Then
            Return dtPreReqs
        Else
            Return Nothing
        End If
    End Function
    Private Function GetAllPreReqsForACourse(strCourseId As String) As DataTable
        Dim db As New DataAccess
        Dim strIsCourseAPreReq As New StringBuilder
        Dim intCount As Integer = 0
        Dim drReader As OleDbDataReader

        Dim dtPreReqs As New DataTable()
        dtPreReqs.Columns.Add("ReqId", GetType(String))
        dtPreReqs.Columns.Add("Descrip", GetType(String))

        With strIsCourseAPreReq
            .Append(" Select Distinct t5.PreCoReqId,t6.Descrip from arCourseReqs t5,arReqs t6 where " + vbCrLf)
            .Append(" t5.PreCoReqId=t6.ReqId and t5.ReqId=? " + vbCrLf)
        End With
        db.AddParameter("@ReqId", strCourseId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            drReader = db.RunParamSQLDataReader(strIsCourseAPreReq.ToString)
            While drReader.Read
                Dim newRow As DataRow = dtPreReqs.NewRow()
                newRow("ReqId") = drReader("PreCoReqId")
                newRow("Descrip") = drReader("Descrip")
                dtPreReqs.Rows.Add(newRow)
            End While

            If Not drReader.IsClosed Then drReader.Close()

        Catch ex As Exception
            db.ClearParameters()
            strIsCourseAPreReq.Remove(0, strIsCourseAPreReq.Length)
        End Try
        If dtPreReqs.Rows.Count >= 1 Then
            Return dtPreReqs
        Else
            Return Nothing
        End If
    End Function
    'Private Function IsStudentAlreadyRegisteredForCourse(ByVal StuEnrollId As String, ByVal CourseId As String) As Boolean
    '    Dim db As New DataAccess
    '    Dim sb As New StringBuilder
    '    Dim intCount As Integer = 0

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If


    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    With sb
    '        .Append(" Select Count(*) from arResults where StuEnrollId=? and TestId in ")
    '        .Append(" (select distinct ClsSectionId from arClassSections where ReqId=?) ")
    '    End With
    '    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    db.AddParameter("@ReqId", CourseId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    Try
    '        intCount = db.RunParamSQLScalar(sb.ToString)
    '    Catch ex As Exception
    '        db.ClearParameters()
    '        sb.Remove(0, sb.Length)
    '        db.CloseConnection()
    '    End Try
    '    If intCount = 0 Then
    '        Return False
    '    Else
    '        Return True
    '    End If
    'End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
    Public Function GetCampusGroup() As String
        Dim db As New DataAccess
        'Dim intCounter As Integer = 0
        Dim sbGetCourses As New StringBuilder
        'Dim ds As New DataSet
        Dim strCampGrpId As String = ""

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        With sbGetCourses
            .Append(" select Top 1 CampGrpId from syCampGrps where CampGrpDescrip='All'")
        End With
        Try
            strCampGrpId = db.RunParamSQLScalar(sbGetCourses.ToString).ToString
        Catch ex As Exception
        Finally
            db.ClearParameters()
            sbGetCourses.Remove(0, sbGetCourses.Length)
        End Try
        Return strCampGrpId
    End Function
    Public Function GetProgramGroup(PrgGrpDescrip As String) As String
        Dim db As New DataAccess
        'Dim intCounter As Integer = 0
        Dim sbGetCourses As New StringBuilder
        'Dim ds As New DataSet
        Dim strPrgGrpId As String = ""

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        If PrgGrpDescrip = "" Then
            Return ""

        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        With sbGetCourses
            .Append(" select Top 1 PrgGrpId from arPrgGrp where  " + vbCrLf)
            .Append(" PrgGrpDescrip like ? + '%'")
        End With
        db.ClearParameters()
        db.AddParameter("@PrgGrpDescrip", PrgGrpDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strPrgGrpId = db.RunParamSQLScalar(sbGetCourses.ToString).ToString
        Catch ex As Exception
            strPrgGrpId = ""
        Finally
            db.ClearParameters()
            sbGetCourses.Remove(0, sbGetCourses.Length)
        End Try
        If strPrgGrpId.Trim = "" Then
            Dim intSpace As Integer = 0
            intSpace = InStrRev(strPrgGrpId.Trim, " ")
            PrgGrpDescrip = Mid(PrgGrpDescrip, 1, intSpace - 1)
            With sbGetCourses
                .Append(" select Top 1 PrgGrpId from arPrgGrp where  " + vbCrLf)
                .Append(" PrgGrpDescrip like ? + '%'")
            End With
            db.ClearParameters()
            db.AddParameter("@PrgGrpDescrip", PrgGrpDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                strPrgGrpId = db.RunParamSQLScalar(sbGetCourses.ToString).ToString
            Catch ex As Exception
                strPrgGrpId = ""
            Finally
                db.ClearParameters()
                sbGetCourses.Remove(0, sbGetCourses.Length)
            End Try
        End If
        Return strPrgGrpId
    End Function

    Public Function GetState(StateDescrip As String) As String
        Dim db As New DataAccess
        'Dim intCounter As Integer = 0
        Dim sbGetCourses As New StringBuilder
        ' Dim ds As New DataSet
        Dim strStateId As String = ""

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        With sbGetCourses
            .Append(" select Top 1 StateId from syStates where StateDescrip=@State")
        End With
        db.ClearParameters()
        db.AddParameter("@State", StateDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strStateId = db.RunParamSQLScalar(sbGetCourses.ToString).ToString
        Catch ex As Exception
        Finally
            db.ClearParameters()
            sbGetCourses.Remove(0, sbGetCourses.Length)
        End Try
        Return strStateId
    End Function
    Public Function GetStatusCodes() As String
        Dim db As New DataAccess
        'Dim intCounter As Integer = 0
        Dim sbGetCourses As New StringBuilder
        'Dim ds As New DataSet
        Dim strStateId As String = ""

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        With sbGetCourses
            '.Append(" select Top 1 StatusCodeId from syStatusCodes where StatusCode='NEWLEAD'")
            .Append(" select Top 1 StatusCodeId from syStatusCodes where SysStatusId=1 ")
        End With
        db.ClearParameters()
        Try
            strStateId = db.RunParamSQLScalar(sbGetCourses.ToString).ToString
        Catch ex As Exception
        Finally
            db.ClearParameters()
            sbGetCourses.Remove(0, sbGetCourses.Length)
        End Try
        Return strStateId
    End Function
    Public Function GetSourceCatagory(SourceCatagoryDescrip As String, UserName As String, CampgrpId As String) As String
        Dim db As New DataAccess
        ' Dim intCounter As Integer = 0
        Dim sbGetCourses As New StringBuilder
        ' Dim ds As New DataSet
        Dim strSourceCatagoryId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        If SourceCatagoryDescrip.Trim = "" Then
            Return ""
            Exit Function
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sbGetCourses
            .Append(" select Top 1 SourceCatagoryId from adSourceCatagory " + vbCrLf)
            .Append(" where SourceCatagoryDescrip like  + ? + '%'")
        End With
        db.ClearParameters()
        db.AddParameter("@SourceCatagoryDescrip", SourceCatagoryDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strSourceCatagoryId = db.RunParamSQLScalar(sbGetCourses.ToString).ToString
        Catch ex As Exception
        Finally
            db.ClearParameters()
            sbGetCourses.Remove(0, sbGetCourses.Length)
        End Try
        If Not strSourceCatagoryId = "" Then
            Return strSourceCatagoryId
            Exit Function
        End If
        If strSourceCatagoryId = "" Then
            strSourceCatagoryId = Guid.NewGuid.ToString
            With sbGetCourses
                .Append("insert into adSourceCatagory(SourceCatagoryDescrip,StatusID,SourceCatagoryCode, " + vbCrLf)
                .Append("SourceCatagoryId,ModUser,ModDate,CampGrpId) values(?,?,?,?,?,?,?) ")
            End With
            db.ClearParameters()
            db.AddParameter("@SourceCatagoryDescrip", SourceCatagoryDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StatusId", "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@SourceCatagoryCode", Mid(SourceCatagoryDescrip, 1, 5), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@SourceCatagoryId", strSourceCatagoryId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModUser", UserName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@CampGrpId", CampgrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sbGetCourses.ToString)
            Catch ex As Exception
            Finally
                db.ClearParameters()
                sbGetCourses.Remove(0, sbGetCourses.Length)
            End Try
            Return strSourceCatagoryId
        End If
    End Function
    Public Function GetPreviousEducation(PreviousEducationDescrip As String, UserName As String, CampGrpId As String) As String
        Dim db As New DataAccess
        ' Dim intCounter As Integer = 0
        Dim sbGetCourses As New StringBuilder
        ' Dim ds As New DataSet
        Dim strPreviousEducationId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        If PreviousEducationDescrip.Trim = "" Then
            Return ""
            Exit Function
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sbGetCourses
            .Append(" select Top 1 EdLvlId from adEdLvls " + vbCrLf)
            .Append(" where EdLvlDescrip like  + ? + '%'")
        End With
        db.ClearParameters()
        db.AddParameter("@PreviousEducationDescrip", PreviousEducationDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strPreviousEducationId = db.RunParamSQLScalar(sbGetCourses.ToString).ToString
        Catch ex As Exception
        Finally
            db.ClearParameters()
            sbGetCourses.Remove(0, sbGetCourses.Length)
        End Try
        If Not strPreviousEducationId = "" Then
            Return strPreviousEducationId
            Exit Function
        End If
        If strPreviousEducationId = "" Then
            strPreviousEducationId = Guid.NewGuid.ToString
            With sbGetCourses
                .Append("insert into adEdLvls(EdLvlDescrip,StatusID,EdLvlCode,EdLvlId,ModUser,ModDate,CampGrpId) " + vbCrLf)
                .Append(" values(?,?,?,?,?,?,?) ")
            End With
            db.ClearParameters()
            db.AddParameter("@EdLvlDescrip", PreviousEducationDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StatusId", "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@EdLvlCode", Mid(PreviousEducationDescrip, 1, 5), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@EdLvlId", strPreviousEducationId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModUser", UserName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@CampGrpId", CampGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sbGetCourses.ToString)
            Catch ex As Exception
            Finally
                db.ClearParameters()
                sbGetCourses.Remove(0, sbGetCourses.Length)
            End Try
            Return strPreviousEducationId
        End If
    End Function
    Public Function ImportAllLeads(FileName As String, UserName As String, _
                                  CampusId As String, UserId As String, _
                                  DistributeLeads As Boolean, _
                                  randNumber As String) As String
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim queryBuilder As New StringBuilder
        Dim dtCreatedDate As DateTime
        Dim dtCreatedTime As DateTime
        Dim CampusDescrip As String
        Dim ProgramOfInterest As String
        Dim strAllCampGrpId As String = ""
        Dim FirstName, LastName, Address1, City, State, Zip, Phone1, Phone2, BestTime, Email As String
        Dim BeginStudy, Age, PreviousEducation, SourceCategory, Comments, field1, field2 As String

        Dim OverallComments As String
        Dim dsLeadsList As New DataSet
        Dim dtNewLead As New DataTable()
        dtNewLead.Columns.Add("LeadId", GetType(String))
        dtNewLead.Columns.Add("RecruitmentOffice", GetType(String))
        dtNewLead.Columns.Add("FirstName", GetType(String))
        dtNewLead.Columns.Add("LastName", GetType(String))
        dtNewLead.Columns.Add("Address1", GetType(String))
        dtNewLead.Columns.Add("City", GetType(String))
        dtNewLead.Columns.Add("StateId", GetType(String))
        dtNewLead.Columns.Add("Zip", GetType(String))
        dtNewLead.Columns.Add("CampusId", GetType(String))
        dtNewLead.Columns.Add("HomeEmail", GetType(String))
        dtNewLead.Columns.Add("Phone", GetType(String))
        dtNewLead.Columns.Add("PreviousEducation", GetType(String))
        dtNewLead.Columns.Add("Age", GetType(String))
        dtNewLead.Columns.Add("DateApplied", GetType(DateTime))
        dtNewLead.Columns.Add("AreaId", GetType(String))
        dtNewLead.Columns.Add("SourceCategoryId", GetType(String))
        dtNewLead.Columns.Add("Comments", GetType(String))
        dtNewLead.Columns.Add("LeadStatus", GetType(String))
        dtNewLead.Columns.Add("AdmissionsRep", GetType(String))
        dtNewLead.Columns.Add("ModUser", GetType(String))
        dtNewLead.Columns.Add("ModDate", GetType(DateTime))

        With queryBuilder
            .Append(" BULK INSERT ImportLeadsForMTTI FROM  '" + FileName + "'" + vbCrLf)
            .Append(" WITH " + vbCrLf)
            .Append(" ( " + vbCrLf)
            .Append(" FIELDTERMINATOR = ',',ROWTERMINATOR = '\n' " + vbCrLf)
            .Append(" )" + vbCrLf)
        End With
        Try
            db.RunParamSQLExecuteNoneQuery(queryBuilder.ToString)
        Catch ex As Exception
        Finally
            db.ClearParameters()
            queryBuilder.Remove(0, queryBuilder.Length)
        End Try

        strAllCampGrpId = GetCampusGroup()


        With queryBuilder
            .Append("Select * from ImportLeadsForMTTI")
        End With

        Dim dr As OleDbDataReader
        Try
            dr = db.RunParamSQLDataReader(queryBuilder.ToString)
            While dr.Read()
                dtCreatedDate = dr("CreatedDate")
                dtCreatedTime = dr("CreatedTime")
                CampusDescrip = dr("CampusDescrip")
                If Not dr("ProgramOfInterest") Is DBNull.Value Then ProgramOfInterest = dr("ProgramOfInterest") Else ProgramOfInterest = ""
                FirstName = dr("FirstName")
                LastName = dr("LastName")
                If Not dr("Address1") Is DBNull.Value Then Address1 = dr("Address1") Else Address1 = ""
                If Not dr("City") Is DBNull.Value Then City = dr("City") Else City = ""
                If Not dr("State") Is DBNull.Value Then State = dr("State") Else State = ""
                If Not dr("Zip") Is DBNull.Value Then Zip = dr("Zip") Else Zip = ""
                If Not dr("Phone1") Is DBNull.Value Then Phone1 = dr("Phone1") Else Phone1 = ""
                If Not dr("Phone2") Is DBNull.Value Then Phone2 = dr("Phone2") Else Phone2 = ""
                If Not dr("BestTime") Is DBNull.Value Then BestTime = dr("BestTime") Else BestTime = ""
                If Not dr("Email") Is DBNull.Value Then Email = dr("Email") Else Email = ""
                If Not dr("BeginStudy") Is DBNull.Value Then BeginStudy = dr("BeginStudy") Else BeginStudy = ""
                If Not dr("Age") Is DBNull.Value Then Age = dr("Age") Else Age = ""
                If Not dr("PreviousEducation") Is DBNull.Value Then PreviousEducation = dr("PreviousEducation") Else PreviousEducation = ""
                If Not dr("SourceCategory") Is DBNull.Value Then SourceCategory = dr("SourceCategory") Else SourceCategory = ""
                If Not dr("Comments") Is DBNull.Value Then Comments = dr("Comments") Else Comments = ""
                If Not dr("field1") Is DBNull.Value Then field1 = dr("field1") Else field1 = ""
                If Not dr("field2") Is DBNull.Value Then field2 = dr("field2") Else field2 = ""

                Dim stateId As String = ""
                Dim StatusCodeId As String = ""
                Dim PrgGrpId As String = ""
                Dim SourceCatagoryId As String = ""
                Dim PreviousEducationId As String = ""

                Try
                    stateId = GetState(State)
                Catch ex As Exception

                End Try

                Try
                    StatusCodeId = GetStatusCodes()
                Catch ex As Exception

                End Try
                Try
                    PrgGrpId = GetProgramGroup(ProgramOfInterest)
                Catch ex As Exception

                End Try
                Try
                    SourceCatagoryId = GetSourceCatagory(SourceCategory, UserName, strAllCampGrpId)
                Catch ex As Exception
                End Try
                Try
                    PreviousEducationId = GetPreviousEducation(PreviousEducation, UserName, strAllCampGrpId)
                Catch ex As Exception
                End Try


                Dim newRow As DataRow = dtNewLead.NewRow()
                newRow("LeadId") = Guid.NewGuid.ToString
                newRow("RecruitmentOffice") = randNumber
                newRow("FirstName") = FirstName
                newRow("LastName") = LastName
                newRow("Address1") = Address1.ToString
                newRow("City") = City.ToString
                newRow("StateId") = stateId
                newRow("Zip") = Zip
                newRow("CampusId") = CampusId
                newRow("HomeEmail") = Email
                newRow("Phone") = Phone1
                newRow("PreviousEducation") = PreviousEducationId
                newRow("Age") = Age
                Try
                    newRow("DateApplied") = dtCreatedDate + dtCreatedTime
                Catch ex As Exception
                    newRow("DateApplied") = Date.Now
                End Try
                newRow("AreaId") = PrgGrpId
                newRow("SourceCategoryId") = SourceCatagoryId
                OverallComments = OverallComments & " \n BestTimetoCall:  " & BestTime
                OverallComments = OverallComments & " \n Plan On Attending:  " & BeginStudy
                OverallComments = OverallComments & " \n InnerCode:  " & field2
                OverallComments = OverallComments & " \n Comments:  " & Comments

                newRow("Comments") = OverallComments
                OverallComments = ""
                newRow("LeadStatus") = StatusCodeId
                newRow("AdmissionsRep") = UserId
                newRow("ModUser") = UserName
                newRow("ModDate") = Date.Now
                dtNewLead.Rows.Add(newRow)
            End While

        Catch ex As Exception
        Finally
            db.ClearParameters()
            dr.Close()
        End Try
        Dim strResultMessage As String = ""
        If dtNewLead.Rows.Count >= 1 Then
            strResultMessage = ImportLead(dtNewLead, CampusId, DistributeLeads)
        End If
        Dim sbDeleteTable As New StringBuilder
        With sbDeleteTable
            .Append(" Insert into ImportLeadsArchive ")
            .Append(" Select * from  ImportLeadsForMTTI ")
        End With
        Try
            db.RunParamSQLExecuteNoneQuery(sbDeleteTable.ToString)
        Catch ex As Exception
        Finally
            db.ClearParameters()
            sbDeleteTable.Remove(0, sbDeleteTable.Length)
        End Try
        With sbDeleteTable
            .Append(" Truncate table ImportLeadsForMTTI ")
        End With
        Try
            db.RunParamSQLExecuteNoneQuery(sbDeleteTable.ToString)
        Catch ex As Exception
        Finally
            db.ClearParameters()
            sbDeleteTable.Remove(0, sbDeleteTable.Length)
        End Try
        Return strResultMessage
    End Function

    Public Function InsertLeadsforUnitek(dt As DataTable, CampusId As String, DistributeLeads As Boolean) As String
        Dim db As New DataAccess
        Dim strLead As New StringBuilder
        Dim intLeadCount As Integer = 0
        Dim intLeadAdded As Integer = 0
        Dim intLeadNotAdded As Integer = 0
        Dim intTotalLeads As Integer = 0
        Dim strAdmissionsRepId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'get list of currently logged in Admission Reps
        Dim dtAdmissionsRep As New DataSet
        dtAdmissionsRep = (New LeadDB).GetAdmissionReps(CampusId)

        ''Added by Saraswathi Lakshmanan on Nov 5th 2008
        Dim intAdmRepCount As Integer
        intAdmRepCount = dtAdmissionsRep.Tables(0).Rows.Count
        Dim intAdmRepi As Integer = 0


        For Each dr As DataRow In dt.Rows
            intTotalLeads += 1
            With strLead
                .Append(" Select Count(*) from adLeads where firstname=? and lastname=? and ")
                .Append(" campusid=? and phone1=? ")
            End With
            db.AddParameter("@firstname", dr("FirstName"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@lastname", dr("LastName"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@campusid", dr("CampusId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Phone1", dr("Phone1"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                intLeadCount = db.RunParamSQLScalar(strLead.ToString)
            Catch ex As Exception
            Finally
                db.ClearParameters()
                strLead.Remove(0, strLead.Length)
            End Try

            If intLeadCount = 0 Then
                With strLead
                    .Append(" insert into adLeads(LeadId,FirstName,LastName,MiddleName,Address1,City,StateId,Zip,SSN," + vbCrLf)
                    .Append(" CampusId,HomeEmail,Phone,Phone2,PhoneType,PhoneType2,Gender,Race,MaritalStatus,FamilyIncome, " + vbCrLf)
                    .Append(" DependencyTypeId,GeographicTypeId,HousingId,DateApplied,PreviousEducation,Children,Citizen,BirthDate, " + vbCrLf)
                    .Append(" LeadStatus,AdmissionsRep,modUser,ModDate,Comments) " + vbCrLf)
                    .Append(" values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                End With
                db.AddParameter("@LeadId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@FirstName", dr("FirstName"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@LastName", dr("LastName"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@MiddleName", dr("MiddleName"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@Address1", dr("Address1"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@City", dr("City"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If dr("StateId").ToString.Trim = "" Then
                    db.AddParameter("@StateId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@StateId", dr("StateId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                If dr("Zip").ToString.Trim = "" Then
                    db.AddParameter("@Zip", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Zip", dr("Zip"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                If InStr(dr("SSN"), "-") >= 1 Then
                    dr("SSN") = Replace(dr("SSN"), "-", "")
                End If
                db.AddParameter("@SSN", dr("SSN"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If dr("CampusId").ToString.Trim = "" Then
                    db.AddParameter("@CampusId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@CampusId", dr("CampusId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                db.AddParameter("@HomeEmail", dr("HomeEmail"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@Phone", dr("Phone1"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@Phone2", dr("Phone2"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If Not dr("PhoneType1") Is DBNull.Value Or Not dr("PhoneType1") = Guid.Empty.ToString Then
                    db.AddParameter("@PhoneType", dr("PhoneType1"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@PhoneType", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                If Not dr("PhoneType2") Is DBNull.Value Or Not dr("PhoneType2") = Guid.Empty.ToString Then
                    db.AddParameter("@PhoneType2", dr("PhoneType2"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@PhoneType2", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                If dr("Sex").ToString.Trim = "" Or dr("Sex").ToString = Guid.Empty.ToString Then
                    db.AddParameter("@Gender", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Gender", dr("Sex").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                If dr("Race").ToString.Trim = "" Or dr("Race").ToString = Guid.Empty.ToString Then
                    db.AddParameter("@Race", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Race", dr("Race").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                If dr("MaritalStatus").ToString.Trim = "" Or dr("MaritalStatus").ToString = Guid.Empty.ToString Then
                    db.AddParameter("@MaritalStatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@MaritalStatus", dr("MaritalStatus").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                If dr("YearlyIncome").ToString.Trim = "" Or dr("YearlyIncome").ToString = Guid.Empty.ToString Then
                    db.AddParameter("@FamilyIncome", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@FamilyIncome", dr("YearlyIncome").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                If dr("Dependency").ToString.Trim = "" Or dr("Dependency").ToString = Guid.Empty.ToString Then
                    db.AddParameter("@DependencyTypeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@DependencyTypeId", dr("Dependency").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                If dr("UrbanType").ToString.Trim = "" Or dr("UrbanType").ToString = Guid.Empty.ToString Then
                    db.AddParameter("@GeographicTypeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@GeographicTypeId", dr("UrbanType").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                If dr("HousingType").ToString.Trim = "" Or dr("HousingType").ToString = Guid.Empty.ToString Then
                    db.AddParameter("@HousingId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@HousingId", dr("HousingType").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                db.AddParameter("@DateApplied", dr("DateApplied"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If dr("EducationLevel").ToString.Trim = "" Or dr("EducationLevel").ToString = Guid.Empty.ToString Then
                    db.AddParameter("@PreviousEducation", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@PreviousEducation", dr("EducationLevel").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                db.AddParameter("@Children", dr("NumofDeps"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If dr("CitizenType").ToString.Trim = "" Or dr("CitizenType").ToString = Guid.Empty.ToString Then
                    db.AddParameter("@Citizen", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@Citizen", dr("CitizenType").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                db.AddParameter("@BirthDate", dr("BirthDate"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If dr("LeadStatus").ToString.Trim = "" Or dr("LeadStatus").ToString = Guid.Empty.ToString Then
                    db.AddParameter("@LeadStatus", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@LeadStatus", dr("LeadStatus").ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                Dim strAdmissionRep As String = ""
                If dr("AdmissionsRep") = "" Then
                    strAdmissionRep = GetAdmissionRep("sa").ToString
                Else
                    strAdmissionRep = dr("AdmissionsRep").ToString
                End If

                If DistributeLeads = False Then
                    db.AddParameter("@AdmissionsRep", strAdmissionRep, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    ''MOdified by Saraswathi Lakshmanan  on Nov 5th 2008
                    ''The leads are assigned to admission reps
                    ''If there are 3 leads and 2 reps
                    ''first lead to first rep, second lead to second rep and Third lead to first rep, in a circular manner.

                    '  strAdmissionsRepId = dtAdmissionsRep.Tables(0).Rows(intAdmRepi)("UserId").ToString
                    If strAdmissionsRepId.Trim = "" Or strAdmissionsRepId = Guid.Empty.ToString Then
                        db.AddParameter("@AdmissionsRep", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@AdmissionsRep", strAdmissionRep, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    If intAdmRepi = (intAdmRepCount - 1) Then
                        intAdmRepi = 0
                    Else
                        intAdmRepi = intAdmRepi + 1
                    End If

                    '    If intTotalLeads <= dtAdmissionsRep.Tables(0).Rows.Count Then
                    '        strAdmissionsRepId = dtAdmissionsRep.Tables(0).Rows(intTotalLeads)("UserId").ToString
                    '    End If
                    '    db.AddParameter("@AdmissionsRep", strAdmissionsRepId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                db.AddParameter("@ModUser", dr("ModUser"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", dr("ModDate"), DataAccess.OleDbDataType.OleDbDateTime, ParameterDirection.Input)
                db.AddParameter("@Comments", "Import", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Try
                    db.RunParamSQLExecuteNoneQuery(strLead.ToString)
                    intLeadAdded += 1
                Catch ex As Exception
                    intLeadNotAdded += 1
                Finally
                    db.ClearParameters()
                    strLead.Remove(0, strLead.Length)
                End Try
            Else
                intLeadNotAdded += 1
            End If
        Next
        Dim strSummary As String = ""
        strSummary = "Total number of leads imported from file : " & intTotalLeads.ToString & vbCrLf
        strSummary &= "Number of leads successfully added to advantage:" & intLeadAdded.ToString & vbCrLf
        strSummary &= "Number of leads not added to advantage:" & intLeadNotAdded.ToString & vbCrLf
        Return strSummary.ToString
        'db.CloseConnection()
    End Function
    Public Function ImportLead(dt As DataTable, CampusId As String, DistributeLeads As Boolean) As String
        Dim db As New DataAccess
        Dim strLead As New StringBuilder
        Dim intLeadCount As Integer = 0
        Dim intLeadAdded As Integer = 0
        Dim intLeadNotAdded As Integer = 0
        Dim intTotalLeads As Integer = 0
        Dim strAdmissionsRepId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'get list of currently logged in Admission Reps
        Dim dtAdmissionsRep As New DataSet
        dtAdmissionsRep = (New LeadDB).GetAdmissionReps(CampusId)

        ''Added by Saraswathi Lakshmanan on Nov 5th 2008
        Dim intAdmRepCount As Integer
        intAdmRepCount = dtAdmissionsRep.Tables(0).Rows.Count
        Dim intAdmRepi As Integer = 0


        For Each dr As DataRow In dt.Rows
            intTotalLeads += 1
            With strLead
                .Append(" Select Count(*) from adLeads where firstname=? and lastname=? and ")
                .Append(" campusid=? and phone=? ")
            End With
            db.AddParameter("@firstname", dr("FirstName"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@lastname", dr("LastName"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@campusid", dr("CampusId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Phone", dr("Phone"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                intLeadCount = db.RunParamSQLScalar(strLead.ToString)
            Catch ex As Exception
            Finally
                db.ClearParameters()
                strLead.Remove(0, strLead.Length)
            End Try

            If intLeadCount = 0 Then
                With strLead
                    .Append(" insert into adLeads(LeadId,FirstName,LastName,Address1,City,StateId,Zip," + vbCrLf)
                    .Append(" CampusId,HomeEmail,Phone,PreviousEducation,Age,DateApplied, " + vbCrLf)
                    .Append(" AreaId,SourceCategoryId,Comments,LeadStatus,AdmissionsRep,RecruitmentOffice,modUser,ModDate) " + vbCrLf)
                    .Append(" values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                End With
                db.AddParameter("@LeadId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@FirstName", dr("FirstName"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@LastName", dr("LastName"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@Address1", dr("Address1"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@City", dr("City"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If dr("StateId").ToString.Trim = "" Then
                    db.AddParameter("@StateId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@StateId", dr("StateId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                db.AddParameter("@Zip", dr("Zip"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CampusId", dr("CampusId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@HomeEmail", dr("HomeEmail"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@Phone", dr("Phone"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If dr("PreviousEducation").ToString.Trim = "" Then
                    db.AddParameter("@PreviousEducation", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@PreviousEducation", dr("PreviousEducation"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                db.AddParameter("@Age", dr("Age"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@DateApplied", dr("DateApplied"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If dr("AreaId").ToString.Trim = "" Then
                    db.AddParameter("@AreaId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@AreaId", dr("AreaId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                If dr("SourceCategoryId").ToString.Trim = "" Then
                    db.AddParameter("@SourceCategoryId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@SourceCategoryId", dr("SourceCategoryId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                db.AddParameter("@Comments", dr("Comments"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@LeadStatus", dr("LeadStatus"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If DistributeLeads = False Then
                    db.AddParameter("@AdmissionsRep", dr("AdmissionsRep"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    ''MOdified by Saraswathi Lakshmanan  on Nov 5th 2008
                    ''The leads are assigned to admission reps
                    ''If there are 3 leads and 2 reps
                    ''first lead to first rep, second lead to second rep and Third lead to first rep, in a circular manner.

                    strAdmissionsRepId = dtAdmissionsRep.Tables(0).Rows(intAdmRepi)("UserId").ToString
                    db.AddParameter("@AdmissionsRep", strAdmissionsRepId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    If intAdmRepi = (intAdmRepCount - 1) Then
                        intAdmRepi = 0
                    Else
                        intAdmRepi = intAdmRepi + 1
                    End If

                    '    If intTotalLeads <= dtAdmissionsRep.Tables(0).Rows.Count Then
                    '        strAdmissionsRepId = dtAdmissionsRep.Tables(0).Rows(intTotalLeads)("UserId").ToString
                    '    End If
                    '    db.AddParameter("@AdmissionsRep", strAdmissionsRepId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                db.AddParameter("@RecruitmentOffice", dr("RecruitmentOffice"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModUser", dr("ModUser"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", dr("ModDate"), DataAccess.OleDbDataType.OleDbDateTime, ParameterDirection.Input)
                Try
                    db.RunParamSQLExecuteNoneQuery(strLead.ToString)
                    intLeadAdded += 1
                Catch ex As Exception
                    intLeadNotAdded += 1
                Finally
                    db.ClearParameters()
                    strLead.Remove(0, strLead.Length)
                End Try
            Else
                intLeadNotAdded += 1
            End If
        Next
        Dim strSummary As String = ""
        strSummary = "Total number of leads imported from file : " & intTotalLeads.ToString & vbCrLf
        strSummary &= "Number of leads successfully added to advantage:" & intLeadAdded.ToString & vbCrLf
        strSummary &= "Number of leads not added to advantage:" & intLeadNotAdded.ToString & vbCrLf
        Return strSummary.ToString
        db.CloseConnection()
    End Function
    Public Function GetLeadSummaryByAdmissionsRep(randNumber As String) As DataTable
        Dim db As New DataAccess
        Dim intCounter As Integer = 0
        Dim sbShift As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sbShift
            .Append(" select AdmissionsRep,(select Distinct FullName from syUsers where UserId=t1.AdmissionsRep) as FullName, " + vbCrLf)
            .Append(" Count(*) ImportedLeadCount " + vbCrLf)
            .Append(" from adLeads t1 where RecruitmentOffice=? group by AdmissionsRep ")
        End With
        db.ClearParameters()
        db.AddParameter("@RecruitmentOffice", randNumber, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            Dim dt As DataTable = db.RunParamSQLDataSet(sbShift.ToString).Tables(0)
            If dt.Rows.Count = 0 Then
                db.ClearParameters()
                sbShift.Remove(0, sbShift.Length)
                With sbShift
                    .Append(" select AdmissionsRep,(select Distinct FullName from syUsers where UserId=t1.AdmissionsRep) as FullName, " + vbCrLf)
                    .Append(" Count(*) ImportedLeadCount " + vbCrLf)
                    .Append(" from adLeads t1 where Comments='Import' and ModDate=?  group by AdmissionsRep ")
                End With
                db.ClearParameters()
                db.AddParameter("@ModDate", DateTime.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Try
                    dt = db.RunParamSQLDataSet(sbShift.ToString).Tables(0)
                    Return dt
                Catch ex As Exception
                    Return Nothing
                End Try
            End If
            Return dt
        Catch ex As Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sbShift.Remove(0, sbShift.Length)
        End Try
    End Function
    Public Function GetonlineAdmissionRepsCount(CampusId As String) As Integer
        Dim db As New DataAccess
        Dim intCounter As Integer = 0
        Dim sbAdRepCount As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sbAdRepCount
            .Append("   select Count(Distinct t1.UserId) from syUsers t1,syRoles t2,syUsersRolesCampGrps t3 ")
            .Append("   where t2.SysRoleId = 3 and t2.RoleId = t3.RoleId and t1.UserId=t3.UserId and ")
            .Append("   CampGrpId in (select CampGrpId from syCmpGrpCmps where CampusId='" & CampusId & "') ")
            .Append("   and t1.IsLoggedIn = 1 ")
        End With
        Try
            intCounter = CType(db.RunParamSQLScalar(sbAdRepCount.ToString), Integer)
        Catch ex As Exception
            intCounter = 0
        Finally
            db.ClearParameters()
            sbAdRepCount.Remove(0, sbAdRepCount.Length)
        End Try
        Return intCounter
    End Function

    Public Function ImportLeadsForUnitek(FileName As String, UserName As String, _
                                  CampusId As String, UserId As String, _
                                  DistributeLeads As Boolean, _
                                  randNumber As String) As String
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim queryBuilder As New StringBuilder
        'Dim dtCreatedDate As DateTime
        'Dim dtCreatedTime As DateTime
        'Dim CampusDescrip As String
        'Dim ProgramOfInterest As String
        Dim strAllCampGrpId As String = ""
        Dim FirstName, LastName, Address1, City, State, Zip, Phone1, Phone2, BestTime, Email, PhoneType1, PhoneType2, middlename As String
        'Dim BeginStudy, Age, PreviousEducation, SourceCategory, Comments, field1, field2 As String
        Dim sex, urbantype, race, dependency, educationlevel, yearlyincome, citizentype, housingtype, ssn, maritalstatus As String
        ' Dim BirthDate As DateTime
        'Dim dsAddLeads As New DataSet
        ' Dim OverallComments As String

        Dim dtNewLead As New DataTable("NewLead")
        dtNewLead.Columns.Add("LeadId", GetType(String))
        dtNewLead.Columns.Add("FirstName", GetType(String))
        dtNewLead.Columns.Add("LastName", GetType(String))
        dtNewLead.Columns.Add("Address1", GetType(String))
        dtNewLead.Columns.Add("City", GetType(String))
        dtNewLead.Columns.Add("StateId", GetType(String))
        dtNewLead.Columns.Add("Zip", GetType(String))
        dtNewLead.Columns.Add("SSN", GetType(String))
        dtNewLead.Columns.Add("Phone1", GetType(String))
        dtNewLead.Columns.Add("Phone2", GetType(String))
        dtNewLead.Columns.Add("PhoneType1", GetType(String))
        dtNewLead.Columns.Add("PhoneType2", GetType(String))
        dtNewLead.Columns.Add("MiddleName", GetType(String))
        dtNewLead.Columns.Add("CampusId", GetType(String))
        dtNewLead.Columns.Add("HomeEmail", GetType(String))
        dtNewLead.Columns.Add("Sex", GetType(String))
        dtNewLead.Columns.Add("UrbanType", GetType(String))
        dtNewLead.Columns.Add("Race", GetType(String))
        dtNewLead.Columns.Add("Dependency", GetType(String))
        dtNewLead.Columns.Add("EducationLevel", GetType(String))
        dtNewLead.Columns.Add("MaritalStatus", GetType(String))
        dtNewLead.Columns.Add("NumofDeps", GetType(String))
        dtNewLead.Columns.Add("YearlyIncome", GetType(String))
        dtNewLead.Columns.Add("CitizenType", GetType(String))
        dtNewLead.Columns.Add("HousingType", GetType(String))
        dtNewLead.Columns.Add("BirthDate", GetType(DateTime))
        dtNewLead.Columns.Add("DateApplied", GetType(DateTime))
        dtNewLead.Columns.Add("LeadStatus", GetType(String))
        dtNewLead.Columns.Add("AdmissionsRep", GetType(String))
        dtNewLead.Columns.Add("ModUser", GetType(String))
        dtNewLead.Columns.Add("ModDate", GetType(DateTime))




        With queryBuilder
            .Append(" BULK INSERT adLeadImports  FROM  '" + FileName + "'" + vbCrLf)
            .Append(" WITH " + vbCrLf)
            .Append(" ( " + vbCrLf)
            .Append(" FIELDTERMINATOR = ',',ROWTERMINATOR = '\n' " + vbCrLf)
            .Append(" )" + vbCrLf)
        End With
        Try
            db.RunParamSQLExecuteNoneQuery(queryBuilder.ToString)
        Catch ex As Exception
        Finally
            db.ClearParameters()
            queryBuilder.Remove(0, queryBuilder.Length)
        End Try

        strAllCampGrpId = GetCampusGroup()


        With queryBuilder
            '.Append("Select * from adImportLeadsForFreedomSchools")
            .Append("Select * from adLeadImports")
        End With

        Dim dr As OleDbDataReader
        Dim strResultMessage As String = ""
        Try
            dr = db.RunParamSQLDataReader(queryBuilder.ToString)
            While dr.Read()

                Try
                    If dr("RecNum").Trim.ToString.ToLower = "recnum" Then 'At times import process insert the columnheading as the first row and that should not be added to advantage
                        Exit Try
                    End If

                    FirstName = dr("FirstName")
                    LastName = dr("LastName")
                    middlename = dr("MiddleName")
                    If Not dr("SSN") Is DBNull.Value Then ssn = dr("SSN") Else ssn = ""
                    If Not dr("AddressLine1") Is DBNull.Value Then Address1 = dr("AddressLine1") Else Address1 = ""
                    If Not dr("AddressCity") Is DBNull.Value Then City = dr("AddressCity") Else City = ""
                    If Not dr("AddressState") Is DBNull.Value Then State = dr("AddressState") Else State = ""
                    If Not dr("Zip") Is DBNull.Value Then Zip = dr("Zip") Else Zip = ""

                    Dim strPhone1 As String = ""
                    If Not dr("PhoneNumAreaCode1") Is DBNull.Value Then
                        strPhone1 = dr("PhoneNumAreaCode1").ToString.Trim
                        If Not dr("PhoneNumPrefix1") Is DBNull.Value Then
                            strPhone1 &= dr("PhoneNumPrefix1").ToString.Trim
                        End If
                        If Not dr("PhoneNumBody1") Is DBNull.Value Then
                            strPhone1 &= dr("PhoneNumBody1").ToString.Trim
                        End If
                    End If

                    Dim strPhone2 As String = ""
                    If Not dr("PhoneNumAreaCode2") Is DBNull.Value Then
                        strPhone2 = dr("PhoneNumAreaCode2").ToString.Trim
                        If Not dr("PhoneNumPrefix2") Is DBNull.Value Then
                            strPhone2 &= dr("PhoneNumPrefix2").ToString.Trim
                        End If
                        If Not dr("PhoneNumBody2") Is DBNull.Value Then
                            strPhone2 &= dr("PhoneNumBody2").ToString.Trim
                        End If
                    End If

                    If Not strPhone1 = "" Then Phone1 = strPhone1 Else Phone1 = ""
                    If Not strPhone2 = "" Then Phone2 = strPhone2 Else Phone2 = ""

                    If Not dr("PhoneType1") Is DBNull.Value Then PhoneType1 = GetPhoneTypes((New FreedomStudentCodes).PhoneType(dr("PhoneType1"))) Else PhoneType1 = ""
                    If Not dr("PhoneType2") Is DBNull.Value Then PhoneType2 = GetPhoneTypes((New FreedomStudentCodes).PhoneType(dr("PhoneType2"))) Else PhoneType2 = ""
                    If Not dr("HomeEmail") Is DBNull.Value Then Email = dr("HomeEmail") Else Email = ""


                    If Not dr("sex") Is DBNull.Value Then sex = GetGender((New FreedomStudentCodes).Sex(dr("sex"))) Else sex = ""
                    If Not dr("urbantype") Is DBNull.Value Then urbantype = GetGeographicTypes((New FreedomStudentCodes).GeographicTypes(dr("urbantype"))) Else urbantype = ""
                    If Not dr("race") Is DBNull.Value Then race = GetRace((New FreedomStudentCodes).RaceTypes(dr("race"))) Else race = ""
                    If Not dr("dependency") Is DBNull.Value Then dependency = GetDependencyTypes((New FreedomStudentCodes).DependencyTypes(dr("dependency"))) Else dependency = ""
                    If Not dr("maritalstatus") Is DBNull.Value Then maritalstatus = GetMaritalStatus((New FreedomStudentCodes).MaritalStatus(dr("maritalstatus"))) Else maritalstatus = ""
                    If Not dr("educationlevel") Is DBNull.Value Then educationlevel = GetDependencyTypes((New FreedomStudentCodes).EducationLevel(dr("educationlevel"))) Else educationlevel = ""
                    'If Not dr("yearlyincome") Is System.DBNull.Value Then yearlyincome = GetFamilyIncome((New FreedomStudentCodes).YearlyIncome(dr("YearlyIncome"))) Else yearlyincome = ""
                    If Not dr("citizentype") Is DBNull.Value Then citizentype = GetCitizenShip((New FreedomStudentCodes).Citizenship(dr("CitizenType"))) Else citizentype = ""

                    'If Not dr("SourceCategory") Is System.DBNull.Value Then SourceCategory = dr("SourceCategory") Else SourceCategory = ""
                    'If Not dr("Comments") Is System.DBNull.Value Then Comments = dr("Comments") Else Comments = ""
                    'If Not dr("field1") Is System.DBNull.Value Then field1 = dr("field1") Else field1 = ""
                    'If Not dr("field2") Is System.DBNull.Value Then field2 = dr("field2") Else field2 = ""

                    Dim stateId As String = ""
                    Dim StatusCodeId As String = ""
                    'Dim PrgGrpId As String = ""
                    'Dim SourceCatagoryId As String = ""
                    'Dim PreviousEducationId As String = ""

                    Try
                        stateId = GetState(State)
                    Catch ex As Exception
                    End Try

                    Try
                        StatusCodeId = GetStatusCodes()
                    Catch ex As Exception
                    End Try


                    Dim newRow As DataRow = dtNewLead.NewRow()
                    newRow("LeadId") = Guid.NewGuid.ToString
                    newRow("FirstName") = FirstName
                    newRow("LastName") = LastName
                    newRow("Address1") = Address1.ToString
                    newRow("City") = City.ToString
                    newRow("StateId") = stateId
                    newRow("Zip") = Zip
                    newRow("SSN") = ssn
                    newRow("Phone1") = Phone1
                    newRow("Phone2") = Phone2
                    newRow("PhoneType1") = PhoneType1
                    newRow("PhoneType2") = PhoneType2
                    newRow("MiddleName") = middlename
                    newRow("CampusId") = CampusId.ToString
                    newRow("HomeEmail") = Email
                    newRow("Sex") = sex
                    newRow("UrbanType") = urbantype
                    newRow("Race") = race
                    newRow("Dependency") = dependency
                    newRow("EducationLevel") = educationlevel
                    newRow("NumofDeps") = dr("NumofDeps")
                    newRow("YearlyIncome") = yearlyincome
                    newRow("CitizenType") = citizentype
                    newRow("HousingType") = housingtype
                    newRow("BirthDate") = dr("BirthDate")
                    newRow("DateApplied") = Date.Now.ToShortDateString
                    newRow("LeadStatus") = StatusCodeId
                    newRow("AdmissionsRep") = ""
                    newRow("ModUser") = "sa"
                    newRow("ModDate") = Date.Now.ToShortDateString
                    dtNewLead.Rows.Add(newRow)

                Catch ex As Exception

                End Try
            End While
            ' dsAddLeads.Tables.Add(dtNewLead)
        Catch ex As Exception
        Finally
            db.ClearParameters()
            dr.Close()
        End Try

        'If Not dsAddLeads Is Nothing Then
        '    For Each lcol As DataColumn In dsAddLeads.Tables(0).Columns
        '        lcol.ColumnMapping = System.Data.MappingType.Attribute
        '    Next
        '    Dim strXML As String = dsAddLeads.GetXml
        'End If

        If dtNewLead.Rows.Count >= 1 Then
            strResultMessage = InsertLeadsforUnitek(dtNewLead, CampusId, DistributeLeads)
        End If
        Dim sbDeleteTable As New StringBuilder
        With sbDeleteTable
            .Append(" Insert into adImportLeadsForFreedomSchoolsArchive ")
            ' .Append(" Select * from  adImportLeadsForFreedomSchools ")
            .Append(" Select * from  adLeadImports ")
        End With
        Try
            db.RunParamSQLExecuteNoneQuery(sbDeleteTable.ToString)
        Catch ex As Exception
        Finally
            db.ClearParameters()
            sbDeleteTable.Remove(0, sbDeleteTable.Length)
        End Try
        With sbDeleteTable
            '  .Append(" Truncate table adImportLeadsForFreedomSchools ")
            .Append(" Truncate table adSampleLeadsImport1 ")
        End With
        Try
            db.RunParamSQLExecuteNoneQuery(sbDeleteTable.ToString)
        Catch ex As Exception
        Finally
            db.ClearParameters()
            sbDeleteTable.Remove(0, sbDeleteTable.Length)
        End Try
        Return strResultMessage
    End Function
    Private Function GetGender(Descrip As String) As String
        'Dim ds As New DataSet
        Dim db As New SQLDataAccess
        Dim strId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@GenderDescrip", Descrip, SqlDbType.VarChar, , ParameterDirection.Input)
            strId = CType(db.RunParamSQLScalar_SP("usp_Gender_GetList", Nothing), Guid).ToString
            Return strId
        Catch ex As Exception
            Return strId
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Private Function GetPhoneTypes(Descrip As String) As String
        'Dim ds As New DataSet
        Dim db As New SQLDataAccess
        Dim strId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@PhoneTypeDescrip", Descrip, SqlDbType.VarChar, , ParameterDirection.Input)
            strId = CType(db.RunParamSQLScalar_SP("usp_PhoneType_GetList", Nothing), Guid).ToString
            Return strId
        Catch ex As Exception
            Return strId
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Private Function GetGeographicTypes(Descrip As String) As String
        'Dim ds As New DataSet
        Dim db As New SQLDataAccess
        Dim strId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@GeographicTypeDescrip", Descrip, SqlDbType.VarChar, , ParameterDirection.Input)
            strId = CType(db.RunParamSQLScalar_SP("usp_GeographicTypes_GetList", Nothing), Guid).ToString
            Return strId
        Catch ex As Exception
            Return strId
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Private Function GetRace(Descrip As String) As String
        Dim ds As New DataSet
        Dim db As New SQLDataAccess
        Dim strId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@EthCodeDescrip", Descrip, SqlDbType.VarChar, , ParameterDirection.Input)
            strId = CType(db.RunParamSQLScalar_SP("usp_Race_GetList", Nothing), Guid).ToString
            Return strId
        Catch ex As Exception
            Return strId
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Private Function GetDependencyTypes(Descrip As String) As String
        Dim ds As New DataSet
        Dim db As New SQLDataAccess
        Dim strId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@Descrip", Descrip, SqlDbType.VarChar, , ParameterDirection.Input)
            strId = CType(db.RunParamSQLScalar_SP("usp_DependencyTypes_GetList", Nothing), Guid).ToString
            Return strId
        Catch ex As Exception
            Return strId
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Private Function GetMaritalStatus(Descrip As String) As String
        ' Dim ds As New DataSet
        Dim db As New SQLDataAccess
        Dim strId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@Descrip", Descrip, SqlDbType.VarChar, , ParameterDirection.Input)
            strId = CType(db.RunParamSQLScalar_SP("usp_MaritalStatus_GetList", Nothing), Guid).ToString
            Return strId
        Catch ex As Exception
            Return strId
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Private Function GetAdmissionRep(UserName As String) As String
        ' Dim ds As New DataSet
        Dim db As New SQLDataAccess
        Dim strId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@UserName", UserName, SqlDbType.VarChar, , ParameterDirection.Input)
            strId = CType(db.RunParamSQLScalar_SP("usp_AdmissionReps_GetList", Nothing), Guid).ToString
            Return strId
        Catch ex As Exception
            Return strId
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    'Private Function GetEducation(ByVal Descrip As String) As String
    '    Dim ds As New DataSet
    '    Dim db As New SQLDataAccess
    '    Dim strId As String = ""

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If


    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    Try
    '        'Call the procedure to insert more than one record all at once
    '        db.AddParameter("@Descrip", Descrip, SqlDbType.VarChar, , ParameterDirection.Input)
    '        strId = CType(db.RunParamSQLScalar_SP("usp_Education_GetList", Nothing), Guid).ToString
    '        Return strId
    '    Catch ex As Exception
    '        Return strId
    '    Finally
    '        db.ClearParameters()
    '        db.CloseConnection()
    '    End Try
    'End Function

    'Private Function GetFamilyIncome(ByVal Descrip As String) As String
    '    Dim ds As New DataSet
    '    Dim db As New SQLDataAccess
    '    Dim strId As String = ""

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If


    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    Try
    '        'Call the procedure to insert more than one record all at once
    '        db.AddParameter("@Descrip", Descrip, SqlDbType.VarChar, , ParameterDirection.Input)
    '        strId = CType(db.RunParamSQLScalar_SP("usp_FamilyIncome_GetList", Nothing), Guid).ToString
    '        Return strId
    '    Catch ex As Exception
    '        Return strId
    '    Finally
    '        db.ClearParameters()
    '        db.CloseConnection()
    '    End Try
    'End Function

    Private Function GetCitizenShip(Descrip As String) As String
        Dim ds As New DataSet
        Dim db As New SQLDataAccess
        Dim strId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@Descrip", Descrip, SqlDbType.VarChar, , ParameterDirection.Input)
            strId = CType(db.RunParamSQLScalar_SP("usp_CitizenType_GetList", Nothing), Guid).ToString
            Return strId
        Catch ex As Exception
            Return strId
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    'Private Function GetHousingTypes(ByVal Descrip As String) As String
    '    Dim ds As New DataSet
    '    Dim db As New SQLDataAccess
    '    Dim strId As String = ""

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If


    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
    '    Try
    '        'Call the procedure to insert more than one record all at once
    '        db.AddParameter("@Descrip", Descrip, SqlDbType.VarChar, , ParameterDirection.Input)
    '        strId = CType(db.RunParamSQLScalar_SP("usp_HousingType_GetList", Nothing), Guid).ToString
    '        Return strId
    '    Catch ex As Exception
    '        Return strId
    '    Finally
    '        db.ClearParameters()
    '        db.CloseConnection()
    '    End Try
    'End Function
    Public Function GetLeadIdForEnrollment(stuEnrollId As String) As String
        Dim db As New SQLDataAccess
        Dim strId As String

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Try

            db.AddParameter("@StuEnrollId", stuEnrollId, SqlDbType.VarChar, , ParameterDirection.Input)
            Dim result = db.RunParamSQLScalar_SP("usp_AD_LeadEnrollments_GetLeadIdForStuEnrollId", Nothing)
            strId = If(result Is DBNull.Value, String.Empty, result.ToString())
            Return strId
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function

    Public Function GetImportLeadsFoldersPaths(CampusId As String, Optional ByRef ImportPath As String = "", Optional ByRef ArchivePath As String = "", Optional ByRef ExceptionPath As String = "", Optional ByRef RemoteUserName As String = "", Optional ByRef RemotePasswd As String = "")
        Dim db As New DataAccess
        Dim ds As DataSet

        Try

            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSetUsingSP("USP_IL_GetImportLeadsFoldersPaths")

            If ds.Tables(0).Rows.Count > 0 Then
                ImportPath = ds.Tables(0).Rows(0)("ILSourcePath").ToString()
                ArchivePath = ds.Tables(0).Rows(0)("ILArchivePath").ToString()
                ExceptionPath = ds.Tables(0).Rows(0)("ILExceptionPath").ToString()
                RemoteUserName = ds.Tables(0).Rows(0)("RemoteServerUserNameIL").ToString()
                RemotePasswd = ds.Tables(0).Rows(0)("RemoteServerPasswordIL").ToString()
                Return ""
            End If

        Catch ex As Exception
            Return ""
        End Try

    End Function

    Public Function CheckImportLeadsFoldersPaths(CampusId As String)
        Dim db As New DataAccess
        Dim ds As DataSet

        Try

            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSetUsingSP("USP_IL_CheckImportLeadsFoldersPaths")
            Return ds

        Catch ex As Exception
            Return ""
        End Try

    End Function

    Public Function CheckIsRemoteServer(CampusId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim dr As OleDbDataReader
        Dim isRemoteServer As Boolean = False

        Try

            With sb
                .Append(" Select IsRemoteServerIL from syCampuses where CampusId=? ")

            End With
            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            dr = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read
                isRemoteServer = CType(dr("IsRemoteServerIL"), Boolean).ToString
                Return isRemoteServer
                Exit Function
            End While

            If Not dr.IsClosed Then dr.Close()

        Catch ex As Exception
            Return isRemoteServer
        End Try

    End Function

    Public Function GetAdvImportLeadsFields() As DataTable
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Try
            ds = db.RunParamSQLDataSet("USP_IL_GetAdvImportLeadsFields", "dtAdvImportLeadsFields")
            Return ds.Tables("dtAdvImportLeadsFields")
        Catch Ex As Exception
            If Not Ex.InnerException Is Nothing Then
                Throw New Exception("Problem occurred executing SP to get Advantage Import Leads Fields:" & Ex.InnerException.ToString)
            Else
                Throw New Exception("Problem occurred executing SP to get Advantage Import Leads Fields:" & Ex.Message.ToString)
            End If
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function

    ''' <summary>
    ''' Detect duplicates by first name last name
    ''' </summary>
    ''' <param name="firstName"></param>
    ''' <param name="lastName"></param>
    ''' <param name="ssn"></param>
    ''' <param name="birthDate"></param>
    ''' <param name="phone"></param>
    ''' <param name="homeEmail"></param>
    ''' <returns></returns>
    ''' <remarks>
    ''' This code only work with the parameters first name and last name
    ''' the other are not more longer working!! 
    ''' </remarks>
    Public Function GetCountForImportLeadRecord(firstName As String, lastName As String) As Integer

        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim intResult As Integer

        With sb
            .Append("select count(*) ")
            .Append("from adLeads ")
            .Append("where FirstName = ? ")
            .Append("and LastName = ? ")

            'If ssn <> "" Then
            '    .Append("and SSN = ? ")
            'End If

            'If birthDate <> "" Then
            '    .Append("and BirthDate = ? ")
            'End If

            'If phone <> "" Then
            '    .Append("and Phone = ? ")
            'End If

            'If homeEmail <> "" Then
            '    .Append("and HomeEmail = ? ")
            'End If
        End With

        db.ClearParameters()
        db.AddParameter("@fname", Trim(firstName), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@lname", Trim(lastName), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'If ssn <> "" Then
        '    'Remove any special characters
        '    ssn = Replace(Replace(Replace(Replace(Replace(Trim(ssn), "-", ""), "/", ""), "\", ""), "(", ""), ")", "")
        '    db.AddParameter("@ssn", ssn, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'End If

        'If birthDate <> "" Then
        '    db.AddParameter("@bd", birthDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'End If

        'If phone <> "" Then
        '    'Remove any special characters
        '    Dim unModifiedPhone As String

        '    unModifiedPhone = Trim(phone)
        '    phone = Replace(Replace(Replace(Replace(Replace(Trim(phone), "-", ""), "/", ""), "\", ""), "(", ""), ")", "")
        '    'A regular number will have 10 digits at this point. If not we have to treat the number as in an international
        '    'format and pass it in the way it is.
        '    If Len(phone) = 10 Then
        '        db.AddParameter("@phone", phone, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '    Else
        '        db.AddParameter("@phone", unModifiedPhone, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '    End If

        'End If

        'If homeEmail <> "" Then
        '    db.AddParameter("@he", homeEmail, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'End If

        Try
            intResult = CInt(db.RunParamSQLScalar(sb.ToString))
            Return intResult
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Return "Error getting count of records for lead import record " & ex.Message.ToString
            Else
                Return "Error getting count of records for lead import record " & ex.InnerException.Message.ToString
            End If
        End Try



    End Function

    Public Function GetImportLeadFieldsWithLookups() As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As DataSet

        With sb
            .Append("select * from dbo.adImportLeadsFields ")
            .Append("where ddlname is not null ")
            .Append("order by Caption ")
        End With

        ds = db.RunParamSQLDataSet(sb.ToString, "LookupTable")
        Return ds.Tables("LookupTable")

    End Function
    Public Function ProcessImportLeadFile(dtLeads As DataTable, strMapName As String, userName As String, dtFileAdvFldMappings As DataTable, dtLookupsMappedValues As DataTable) As String
        Dim db As New SQLDataAccess
        Dim adapter As New SqlDataAdapter()
        'Dim adapterAdvFileFldMappings As New SqlDataAdapter
        Dim connString As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        connString = MyAdvAppSettings.AppSettings("ConnectionString")
        Dim conn As New SqlConnection(connString & "Connect Timeout=1000;") 'DE9132
        conn.Open()

        'disable triggers
        db.AddParameter("@EnableTriggers", False, SqlDbType.Bit, , ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery_SP("usp_IL_ProcessImportLeadFile_Setup") 'DE9132
        db.ClearParameters()

        Dim groupTrans As SqlTransaction = conn.BeginTransaction()
        'Save the mapping data if a mapping name is provided
        If strMapName <> "" Then
            'Save parent record to the adImportLeadsMappings table. Note that the SP only adds a new
            'record if the map name does not already exist in the database.
            Dim mappingId As Guid
            mappingId = Guid.NewGuid

            db.AddParameter("@MappingId", mappingId, SqlDbType.UniqueIdentifier, 50, ParameterDirection.Input)
            db.AddParameter("@MapName", strMapName, SqlDbType.VarChar, 200, ParameterDirection.Input)
            db.AddParameter("@User", userName, SqlDbType.VarChar, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery_SP("USP_IL_AddImportLeadsMappingInfo")
            db.ClearParameters()

            'Save data to the adImportLeadsAdvFldMappings table.
            'The dtFileAdvFldMappings datatable does not have any values for the MappingId field at this point
            'so we need to fill those out based on the mappingid value used to save the parent record.
            For Each dr As DataRow In dtFileAdvFldMappings.Rows
                dr("MappingId") = mappingId
            Next

            dtFileAdvFldMappings.AcceptChanges()
            For Each dr2 As DataRow In dtFileAdvFldMappings.Rows
                db.AddParameter("@MappingId", dr2("MappingId"), SqlDbType.VarChar, 50, ParameterDirection.Input)
                db.AddParameter("@FieldNumber", dr2("FileFldNumber"), SqlDbType.Int, 50, ParameterDirection.Input)
                db.AddParameter("@ILFieldId", dr2("AdvILFieldId"), SqlDbType.Int, 50, ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery_SP("USP_IL_AddImportLeadsAdvFileFldMapping")
                db.ClearParameters()
            Next


            'Save the data to the adImportLeadsLookupValsMap table.
            For Each dr3 As DataRow In dtLookupsMappedValues.Rows
                db.AddParameter("@MappingId", mappingId.ToString, SqlDbType.VarChar, 50, ParameterDirection.Input)
                db.AddParameter("@ILFieldId", dr3("ILFieldId"), SqlDbType.Int, 50, ParameterDirection.Input)
                db.AddParameter("@FileValue", dr3("FileValue"), SqlDbType.VarChar, 50, ParameterDirection.Input)
                db.AddParameter("@AdvValue", dr3("AdvMappedValue"), SqlDbType.VarChar, 50, ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery_SP("USP_IL_AddImportLeadsLookupValsMapping")
                db.ClearParameters()
            Next


        End If


        Dim m_Command As SqlCommand = New SqlCommand("usp_IL_ProcessImportLeadFile", conn, groupTrans)
        m_Command.CommandTimeout = 0
        m_Command.CommandType = CommandType.StoredProcedure
        m_Command.UpdatedRowSource = UpdateRowSource.None
        adapter.InsertCommand = m_Command

        adapter.InsertCommand.Parameters.Add("@LeadId", SqlDbType.VarChar, 50, "LeadId")
        adapter.InsertCommand.Parameters.Add("@FirstName", SqlDbType.VarChar, 50, "FirstName")
        adapter.InsertCommand.Parameters.Add("@LastName", SqlDbType.VarChar, 50, "LastName")
        adapter.InsertCommand.Parameters.Add("@MiddleName", SqlDbType.VarChar, 50, "MiddleName")
        adapter.InsertCommand.Parameters.Add("@SSN", SqlDbType.VarChar, 50, "SSN")
        adapter.InsertCommand.Parameters.Add("@ModUser", SqlDbType.VarChar, 50, "ModUser")
        adapter.InsertCommand.Parameters.Add("@ModDate", SqlDbType.VarChar, 50, "ModDate")
        adapter.InsertCommand.Parameters.Add("@Phone", SqlDbType.VarChar, 50, "Phone")
        adapter.InsertCommand.Parameters.Add("@HomeEmail", SqlDbType.VarChar, 50, "HomeEmail")
        adapter.InsertCommand.Parameters.Add("@Address1", SqlDbType.VarChar, 50, "Address1")
        adapter.InsertCommand.Parameters.Add("@Address2", SqlDbType.VarChar, 50, "Address2")
        adapter.InsertCommand.Parameters.Add("@City", SqlDbType.VarChar, 50, "City")
        adapter.InsertCommand.Parameters.Add("@StateId", SqlDbType.VarChar, 50, "StateId")
        adapter.InsertCommand.Parameters.Add("@Zip", SqlDbType.VarChar, 50, "Zip")
        adapter.InsertCommand.Parameters.Add("@LeadStatus", SqlDbType.VarChar, 50, "LeadStatus")
        adapter.InsertCommand.Parameters.Add("@WorkEmail", SqlDbType.VarChar, 50, "WorkEmail")
        adapter.InsertCommand.Parameters.Add("@AddressType", SqlDbType.VarChar, 50, "AddressType")
        adapter.InsertCommand.Parameters.Add("@Prefix", SqlDbType.VarChar, 50, "Prefix")
        adapter.InsertCommand.Parameters.Add("@Suffix", SqlDbType.VarChar, 50, "Suffix")
        adapter.InsertCommand.Parameters.Add("@BirthDate", SqlDbType.VarChar, 50, "BirthDate")
        adapter.InsertCommand.Parameters.Add("@Sponsor", SqlDbType.VarChar, 50, "Sponsor")
        adapter.InsertCommand.Parameters.Add("@AdmissionsRep", SqlDbType.VarChar, 50, "AdmissionsRep")
        adapter.InsertCommand.Parameters.Add("@AssignedDate", SqlDbType.VarChar, 50, "AssignedDate")
        adapter.InsertCommand.Parameters.Add("@Gender", SqlDbType.VarChar, 50, "Gender")
        adapter.InsertCommand.Parameters.Add("@Race", SqlDbType.VarChar, 50, "Race")
        adapter.InsertCommand.Parameters.Add("@MaritalStatus", SqlDbType.VarChar, 50, "MaritalStatus")
        adapter.InsertCommand.Parameters.Add("@FamilyIncome", SqlDbType.VarChar, 50, "FamilyIncome")
        adapter.InsertCommand.Parameters.Add("@Children", SqlDbType.VarChar, 50, "Children")
        adapter.InsertCommand.Parameters.Add("@PhoneType", SqlDbType.VarChar, 50, "PhoneType")
        adapter.InsertCommand.Parameters.Add("@PhoneStatus", SqlDbType.VarChar, 50, "PhoneStatus")
        adapter.InsertCommand.Parameters.Add("@SourceCategoryId", SqlDbType.VarChar, 50, "SourceCategoryId")
        adapter.InsertCommand.Parameters.Add("@SourceTypeId", SqlDbType.VarChar, 50, "SourceTypeId")
        adapter.InsertCommand.Parameters.Add("@SourceDate", SqlDbType.VarChar, 50, "SourceDate")
        adapter.InsertCommand.Parameters.Add("@AreaId", SqlDbType.VarChar, 50, "AreaID")
        adapter.InsertCommand.Parameters.Add("@ProgramID", SqlDbType.VarChar, 50, "ProgramID")
        adapter.InsertCommand.Parameters.Add("@ExpectedStart", SqlDbType.VarChar, 50, "ExpectedStart")
        adapter.InsertCommand.Parameters.Add("@ShiftId", SqlDbType.VarChar, 50, "ShiftId")
        adapter.InsertCommand.Parameters.Add("@Nationality", SqlDbType.VarChar, 50, "Nationality")
        adapter.InsertCommand.Parameters.Add("@Citizen", SqlDbType.VarChar, 50, "Citizen")
        adapter.InsertCommand.Parameters.Add("@DrivLicStateId", SqlDbType.VarChar, 50, "DrivLicStateId")
        adapter.InsertCommand.Parameters.Add("@DrivLicNumber", SqlDbType.VarChar, 50, "DrivLicNumber")
        adapter.InsertCommand.Parameters.Add("@AlienNumber", SqlDbType.VarChar, 50, "AlienNumber")
        adapter.InsertCommand.Parameters.Add("@Comments", SqlDbType.VarChar, 50, "Comments")
        adapter.InsertCommand.Parameters.Add("@SourceAdvertisement", SqlDbType.VarChar, 50, "SourceAdvertisement")
        adapter.InsertCommand.Parameters.Add("@CampusId", SqlDbType.VarChar, 50, "CampusId")
        adapter.InsertCommand.Parameters.Add("@PrgVerId", SqlDbType.VarChar, 50, "PrgVerId")
        adapter.InsertCommand.Parameters.Add("@Country", SqlDbType.VarChar, 50, "Country")
        adapter.InsertCommand.Parameters.Add("@County", SqlDbType.VarChar, 50, "County")
        adapter.InsertCommand.Parameters.Add("@PreviousEducation", SqlDbType.VarChar, 50, "PreviousEducation")
        adapter.InsertCommand.Parameters.Add("@AddressStatus", SqlDbType.VarChar, 50, "AddressStatus")
        adapter.InsertCommand.Parameters.Add("@CreatedDate", SqlDbType.VarChar, 50, "CreatedDate")
        adapter.InsertCommand.Parameters.Add("@ForeignPhone", SqlDbType.VarChar, 50, "ForeignPhone")
        adapter.InsertCommand.Parameters.Add("@ForeignZip", SqlDbType.VarChar, 50, "ForeignZip")
        adapter.InsertCommand.Parameters.Add("@LeadGrpId", SqlDbType.VarChar, 50, "LeadGrpId")
        adapter.InsertCommand.Parameters.Add("@DependencyTypeId", SqlDbType.VarChar, 50, "DependencyTypeId")
        adapter.InsertCommand.Parameters.Add("@DegCertSeekingId", SqlDbType.VarChar, 50, "DegCertSeekingId")
        adapter.InsertCommand.Parameters.Add("@GeographicTypeId", SqlDbType.VarChar, 50, "GeographicTypeId")
        adapter.InsertCommand.Parameters.Add("@HousingId", SqlDbType.VarChar, 50, "HousingId")
        adapter.InsertCommand.Parameters.Add("@AdminCriteriaId", SqlDbType.VarChar, 50, "AdminCriteriaId")
        adapter.InsertCommand.Parameters.Add("@DateApplied", SqlDbType.VarChar, 50, "DateApplied")
        adapter.InsertCommand.Parameters.Add("@AdvertisementNote", SqlDbType.VarChar, 50, "AdvertisementNote")
        adapter.InsertCommand.Parameters.Add("@Phone2", SqlDbType.VarChar, 50, "Phone2")
        adapter.InsertCommand.Parameters.Add("@PhoneType2", SqlDbType.VarChar, 50, "PhoneType2")
        adapter.InsertCommand.Parameters.Add("@PhoneStatus2", SqlDbType.VarChar, 50, "PhoneStatus2")
        adapter.InsertCommand.Parameters.Add("@ForeignPhone2", SqlDbType.VarChar, 50, "ForeignPhone2")
        adapter.InsertCommand.Parameters.Add("@DefaultPhone", SqlDbType.VarChar, 50, "DefaultPhone")


        Try
            ' Set the batch size.
            adapter.UpdateBatchSize = 100

            ' Execute the update.
            adapter.Update(dtLeads)

            db.ClearParameters()

            'Commit the transaction
            groupTrans.Commit()
        Catch ex As Exception
            groupTrans.Rollback()
            'If Not groupTrans.Connection Is Nothing Then
            ' Report the error
            If Not ex.InnerException Is Nothing Then
                Return ex.InnerException.Message.ToString
            Else
                Return ex.Message.ToString
            End If

            'End If
        Finally
            'disable triggers
            Try
                db.AddParameter("@EnableTriggers", True, SqlDbType.Bit, , ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery_SP("usp_IL_ProcessImportLeadFile_Setup") 'DE9132
                db.ClearParameters()
            Catch ex As Exception
                'Do nothing. We could have a referential integrity exception here because of data issue but we
                'don't want to stop the import process because of that.
            End Try

            'Close Connection
            conn.Close()
        End Try

        Return ""


    End Function


    Public Function GetSavedMappingsWithNumberOfFields(numFields As Integer, campusId As String) As DataTable
        Dim ds As New DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            db.AddParameter("@NumberOfFields", numFields, SqlDbType.Int, , ParameterDirection.Input)
            db.AddParameter("@CampusId", campusId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("USP_IL_GetSavedMappingsWithNumberOfFields", "dtSavedMappings")
            Return ds.Tables("dtSavedMappings")
        Catch Ex As Exception
            If Not Ex.InnerException Is Nothing Then
                Throw New Exception("Problem occurred executing SP to get Advantage Import Leads Fields:" & Ex.InnerException.ToString)
            Else
                Throw New Exception("Problem occurred executing SP to get Advantage Import Leads Fields:" & Ex.Message.ToString)
            End If
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function

    Public Function GetAdvFileFldMappings(mappingId As String) As DataTable
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            db.AddParameter("@MappingId", mappingId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("USP_IL_GetAdvFileFldMappings", "dtAdvFileFldMappings")
            Return ds.Tables("dtAdvFileFldMappings")
        Catch Ex As Exception
            If Not Ex.InnerException Is Nothing Then
                Throw New Exception("Problem occurred executing SP to get the Advantage File Field Mappings:" & Ex.InnerException.ToString)
            Else
                Throw New Exception("Problem occurred executing SP to get Advantage File Field Mappings:" & Ex.Message.ToString)
            End If
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function

    Public Function GetLookupFldValsMappings(mappingId As String) As DataTable
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            db.AddParameter("@MappingId", mappingId, SqlDbType.VarChar, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSet_SP("USP_IL_GetLookupFldValsMappings", "dtLookupFldValsMappings")
            Return ds.Tables("dtLookupFldValsMappings")
        Catch Ex As Exception
            If Not Ex.InnerException Is Nothing Then
                Throw New Exception("Problem occurred executing SP to get the Lookup Field Mappings:" & Ex.InnerException.ToString)
            Else
                Throw New Exception("Problem occurred executing SP to get Lookup File Field Mappings:" & Ex.Message.ToString)
            End If
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function



End Class
Public Class FreedomStudentCodes
    Dim id As Integer
    Public Shared ReadOnly Property PhoneType(Id As String) As String
        Get
            Select Case Id
                Case 1
                    Return "Home"
                Case 2
                    Return "Work"
                Case 3
                    Return "Mobile"
                Case 4
                    Return "Fax"
                Case 5
                    Return "Pager"
                Case 6
                    Return "Other"
                Case Else
                    Return "Other"
            End Select
        End Get
    End Property
    Public Shared ReadOnly Property Sex(id As String) As String
        Get
            Select Case id
                Case 1
                    Return "Male"
                Case 2
                    Return "Female"
                Case Else
                    Return "Unknown"
            End Select
        End Get
    End Property
    Public Shared ReadOnly Property GeographicTypes(Id As String) As String
        Get
            Select Case Id
                Case 1
                    Return "Urban (over 100,000)"
                Case 2
                    Return "Suburban (25,000 - 100,000)"
                Case 3
                    Return "Rural (under 25,000)"
            End Select
        End Get
    End Property
    Public Shared ReadOnly Property RaceTypes(Id As String) As String
        Get
            Select Case Id
                Case 1
                    Return "Asian/Pacific Islander"
                Case 2
                    Return "Black, non-Hispanic"
                Case 3
                    Return "White, non-Hispanic"
                Case 4
                    Return "Hispanic"
                Case 5
                    Return "American Indian/Alaska native"
                Case 6
                    Return "Race/ethnicity unknown"
            End Select
        End Get
    End Property
    Public Shared ReadOnly Property DependencyTypes(id As String) As String
        Get
            Select Case id
                Case 1
                    Return "Independent"
                Case 2
                    Return "Dependent"
            End Select
        End Get
    End Property
    Public Shared ReadOnly Property EducationLevel(Id As String) As String
        Get
            Select Case Id
                Case 1
                    Return "Postsecondary"
                Case 2
                    Return "High school"
                Case 3
                    Return "Graduate school"
            End Select
        End Get
    End Property
    Public Shared ReadOnly Property MaritalStatus(Id As String) As String
        Get
            Select Case Id
                Case 1
                    Return "Single"
                Case 2
                    Return "Married"
                Case 3
                    Return "Divorced"
                Case 5
                    Return "Separated"
                Case 6
                    Return "Widowed"
            End Select
        End Get
    End Property
    Public Shared ReadOnly Property Citizenship(Id As String) As String
        Get
            Select Case Id
                Case 1
                    Return "US citizen"
                Case 2
                    Return "Eligible non citizen"
                Case 3
                    Return "Non citizen"
            End Select
        End Get
    End Property
    Public Shared ReadOnly Property Housing(Id As String) As String
        Get
            Select Case Id
                Case 1
                    Return "On campus"
                Case 2
                    Return "Off campus"
                Case 3
                    Return "With parents"
                Case 4
                    Return "Incarcerated"
            End Select
        End Get
    End Property
End Class
