Imports System.Data.Common
Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' HolidaysDB.vb
'
' HolidaysDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class PortalHolidaysDB
    Public Function GetAllHolidays(Optional ByVal campusid As String = "") As DataSet

        '   connect to the database
        Dim db As New MyPortalDataAccess

        Dim myAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            myAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ClientConnectionStringOleDB")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      CCT.HolidayId, ")
            .Append("	      CCT.StatusId, ")
            .Append("	      CCT.HolidayCode, ")
            .Append("         CCT.HolidayStartDate, ")
            .Append("         CCT.HolidayEndDate, ")
            .Append("	      CCT.HolidayDescrip ")
            .Append("   ,CCT.AllDay,(SELECT CONVERT(VARCHAR,TimeIntervalDescrip,108) FROM dbo.cmTimeInterval WHERE TimeIntervalId=CCT.StartTimeID) as StartTime,")
            .Append(" (SELECT CONVERT(VARCHAR,TimeIntervalDescrip,108) FROM dbo.cmTimeInterval WHERE TimeIntervalId=CCT.EndTimeID) as EndTime ")
            .Append("FROM     syHolidays CCT, syStatuses ST ")
            .Append("WHERE    CCT.StatusId = ST.StatusId ")
            If campusid <> "" Then
                sb.Append("AND (CCT.CampGrpId IN(SELECT CampGrpId ")
                sb.Append("FROM syCmpGrpCmps ")
                sb.Append("WHERE CampusId = ? ")
                sb.Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                sb.Append("OR CCT.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            .Append("ORDER BY ST.Status,CCT.HolidayDescrip asc")
        End With
        If Not campusid = "" Then
            db.AddParameter("@campusid", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function


    Public Function GetAllHolidays_ByClass(Optional ByVal campusid As String = "", Optional ByVal clsSectMeetingId As String = "") As DataSet

        '   connect to the database
        Dim db As New MyPortalDataAccess

        Dim myAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            myAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ClientConnectionStringOleDB")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      CCT.HolidayId, ")
            .Append("	      CCT.StatusId, ")
            .Append("	      CCT.HolidayCode, ")
            .Append("         CCT.HolidayStartDate, ")
            .Append("         CCT.HolidayEndDate, ")
            .Append("	      CCT.HolidayDescrip ")
            .Append("   ,CCT.AllDay,(SELECT CONVERT(VARCHAR,TimeIntervalDescrip,108) FROM dbo.cmTimeInterval WHERE TimeIntervalId=CCT.StartTimeID) as StartTime,")
            .Append(" (SELECT CONVERT(VARCHAR,TimeIntervalDescrip,108) FROM dbo.cmTimeInterval WHERE TimeIntervalId=CCT.EndTimeID) as EndTime ")
            .Append("FROM     syHolidays CCT, syStatuses ST ")
            .Append("WHERE    CCT.StatusId = ST.StatusId ")
            If campusid <> "" Then
                sb.Append("AND (CCT.CampGrpId IN(SELECT CampGrpId ")
                sb.Append("FROM syCmpGrpCmps ")
                sb.Append("WHERE CampusId = ? ")
                sb.Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                sb.Append("OR CCT.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            .Append("ORDER BY ST.Status,CCT.HolidayDescrip asc; ")
            If Not clsSectMeetingId = "" Then
                .Append("   SELECT  ( SELECT    TimeIntervalDescrip    FROM      dbo.cmTimeInterval  WHERE     TimeIntervalId = sp.StartTimeID   ) AS StartTime ,   ")
                .Append("  ( SELECT    TimeIntervalDescrip   FROM      dbo.cmTimeInterval  WHERE     TimeIntervalId = sp.endTimeID    ) AS EndTime  ")
                .Append("  FROM    dbo.arClsSectMeetings CSM , dbo.syPeriods SP  WHERE   CSm.PeriodId = SP.PeriodId  ")
                .Append("  AND Csm.ClsSectMeetingId = ?; ")
            End If

        End With
        If Not campusid = "" Then
            db.AddParameter("@campusid", campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        If Not clsSectMeetingId = "" Then
            db.AddParameter("@ClsSectMeetingId", clsSectMeetingId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    Public Function GetHolidayInfo(ByVal holidayId As String) As HolidayInfo

        '   connect to the database
        Dim db As New MyPortalDataAccess

        Dim myAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            myAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ClientConnectionStringOleDB")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT CCT.HolidayId, ")
            .Append("    CCT.HolidayCode, ")
            .Append("    CCT.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=CCT.StatusId) As Status, ")
            .Append("    CCT.HolidayDescrip, ")
            .Append("    CCT.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=CCT.CampGrpId) As CampGrpDescrip, ")
            .Append("    CCT.HolidayStartDate, ")
            .Append("    CCT.HolidayEndDate, ")
            .Append("    CCT.AllDay, ")
            .Append("    CCT.StartTimeId, ")
            .Append("    CCT.EndTimeId, ")
            .Append("    CCT.ModUser, ")
            .Append("    CCT.ModDate ")
            .Append("FROM  syHolidays CCT ")
            .Append("WHERE CCT.HolidayId= ? ")
        End With

        ' Add the HolidayId to the parameter list
        db.AddParameter("@HolidayId", holidayId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim holidayInfo As New HolidayInfo

        While dr.Read()

            '   set properties with data from DataReader
            With holidayInfo
                .HolidayId = holidayId
                .IsInDB = True
                .Code = dr("HolidayCode")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .Status = dr("Status")
                .Description = dr("HolidayDescrip")
                If Not (dr("CampGrpId") Is DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                If Not (dr("CampGrpDescrip") Is DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                .HolidayStartDate = dr("HolidayStartDate")
                If Not dr("HolidayEndDate") Is DBNull.Value Then .HolidayEndDate = dr("HolidayEndDate") Else .HolidayEndDate = .HolidayStartDate
                .AllDay = dr("AllDay")
                If Not (dr("StartTimeId") Is DBNull.Value) Then .StartTimeId = CType(dr("StartTimeId"), Guid).ToString
                If Not (dr("EndTimeId") Is DBNull.Value) Then .EndTimeId = CType(dr("EndTimeId"), Guid).ToString
                If Not (dr("ModUser") Is DBNull.Value) Then .ModUser = dr("ModUser")
                If Not (dr("ModDate") Is DBNull.Value) Then .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return HolidayInfo
        Return holidayInfo

    End Function
    Public Function UpdateHolidayInfo(ByVal holidayInfo As HolidayInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New MyPortalDataAccess

        Dim myAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            myAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ClientConnectionStringOleDB")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syHolidays Set HolidayId = ?, HolidayCode = ?, ")
                .Append(" StatusId = ?, HolidayDescrip = ?, CampGrpId = ?, ")
                .Append(" HolidayStartDate = ?, HolidayEndDate = ?, AllDay = ?, StartTimeId = ?, EndTimeId = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE HolidayId = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from syHolidays where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   HolidayId
            db.AddParameter("@HolidayId", holidayInfo.HolidayId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   EnrollmentId
            db.AddParameter("@HolidayCode", holidayInfo.Code, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", holidayInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   HolidayDescrip
            db.AddParameter("@HolidayDescrip", holidayInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If holidayInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", holidayInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   HolidayStartDate
            db.AddParameter("@HolidayStartDate", holidayInfo.HolidayStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   HolidayEndDate
            db.AddParameter("@HolidayEndDate", holidayInfo.HolidayEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   AllDay
            db.AddParameter("@AllDay", holidayInfo.AllDay, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   StartTimeId
            If holidayInfo.StartTimeId = Guid.Empty.ToString Then
                db.AddParameter("@StartTimeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StartTimeId", holidayInfo.StartTimeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   EndTimeId
            If holidayInfo.EndTimeId = Guid.Empty.ToString Then
                db.AddParameter("@EndTimeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@EndTimeId", holidayInfo.EndTimeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            'Dim strnow As Date = Utilities.GetAdvantageDBDateTime(Date.Now)
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   HolidayId
            db.AddParameter("@HolidayId", holidayInfo.HolidayId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", holidayInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem

            'Update the arClockAttendance and arStudentClockAttendance
            Dim sb1 As New StringBuilder
            With sb1
                '.Append("Update atConversionAttendance set Schedule=0.0 where MeetDate >= ? and MeetDate <= ? ")
                ''modified by Saraswathi lakshmanan on April 12 2010
                ''To fix Ross Snow DAys Issue 18806
                .Append("Update atConversionAttendance set Schedule=0  ")
                .Append("from ")
                .Append(" atConversionAttendance,arStuEnrollments SE , syCmpGrpCmps E,syCampuses F,syCampGrps    ")
                .Append(" Where")
                .Append(" MeetDate >= ? and MeetDate <= ? ")
                .Append(" and SE.StuEnrollId=atConversionAttendance.StuEnrollId")
                .Append(" AND SE.CampusId=F.CampusId ")
                .Append(" AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId ")
                .Append(" and syCampGrps.CampGrpId=? ")


            End With
            db.ClearParameters()
            db.AddParameter("@HolidayStartDate", holidayInfo.HolidayStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@HolidayEndDate", holidayInfo.HolidayEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@CampGrpId", holidayInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb1.ToString)


            Dim sb2 As New StringBuilder
            With sb2
                ''  .Append("Update arStudentClockAttendance set SchedHours=0.0 where RecordDate >= ? and RecordDate <= ? ")

                .Append("Update arStudentClockAttendance set SchedHours=0.0  ")
                .Append("from ")
                .Append("arStudentClockAttendance,arStuEnrollments SE , syCmpGrpCmps E,syCampuses F,syCampGrps    ")
                .Append("Where ")
                .Append(" RecordDate >= ? and RecordDate <= ?  ")
                .Append(" and SE.StuEnrollId=arStudentClockAttendance.StuEnrollId ")
                .Append(" AND SE.CampusId=F.CampusId  ")
                .Append(" AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId ")
                .Append(" and syCampGrps.CampGrpId=? ")

            End With
            db.ClearParameters()
            db.AddParameter("@HolidayStartDate", holidayInfo.HolidayStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@HolidayEndDate", holidayInfo.HolidayEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@CampGrpId", holidayInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            db.RunParamSQLExecuteNoneQuery(sb2.ToString)
            db.ClearParameters()



            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)


        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddHolidayInfo(ByVal holidayInfo As HolidayInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New MyPortalDataAccess

        Dim myAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            myAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ClientConnectionStringOleDB")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syHolidays (HolidayId, HolidayCode, StatusId, ")
                .Append("   HolidayDescrip, CampGrpId, HolidayStartDate, HolidayEndDate, AllDay, ")
                .Append("   StartTimeId, EndTimeId, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   HolidayId
            db.AddParameter("@HolidayId", holidayInfo.HolidayId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   HolidayCode
            db.AddParameter("@HolidayCode", holidayInfo.Code, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", holidayInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   HolidayDescrip
            db.AddParameter("@HolidayDescrip", holidayInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If holidayInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", holidayInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   HolidayStartDate
            db.AddParameter("@HolidayStartDate", holidayInfo.HolidayStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   HolidayEndDate
            db.AddParameter("@HolidayEndDate", holidayInfo.HolidayEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   AllDay
            db.AddParameter("@AllDay", holidayInfo.AllDay, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   StartTimeId
            If holidayInfo.StartTimeId = Guid.Empty.ToString Then
                db.AddParameter("@StartTimeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@StartTimeId", holidayInfo.StartTimeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   EndTimeId
            If holidayInfo.EndTimeId = Guid.Empty.ToString Then
                db.AddParameter("@EndTimeId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@EndTimeId", holidayInfo.EndTimeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim strnow As Date = Utilities.GetAdvantageDBDateTime(Date.Now)
            db.AddParameter("@ModDate", strnow, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)


            'Update the arClockAttendance and arStudentClockAttendance
            Dim sb1 As New StringBuilder
            With sb1
                '.Append("Update atConversionAttendance set Schedule=0.0 where MeetDate >= ? and MeetDate <= ? ")
                ''modified by Saraswathi lakshmanan on April 12 2010
                ''To fix Ross Snow DAys Issue 18806
                .Append("Update atConversionAttendance set Schedule=0  ")
                .Append("from ")
                .Append(" atConversionAttendance,arStuEnrollments SE , syCmpGrpCmps E,syCampuses F,syCampGrps    ")
                .Append(" Where")
                .Append(" MeetDate >= ? and MeetDate <= ? ")
                .Append(" and SE.StuEnrollId=atConversionAttendance.StuEnrollId")
                .Append(" AND SE.CampusId=F.CampusId ")
                .Append(" AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId ")
                .Append(" and syCampGrps.CampGrpId=? ")


            End With
            db.ClearParameters()
            db.AddParameter("@HolidayStartDate", holidayInfo.HolidayStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@HolidayEndDate", holidayInfo.HolidayEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@CampGrpId", holidayInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb1.ToString)


            Dim sb2 As New StringBuilder
            With sb2
                ''  .Append("Update arStudentClockAttendance set SchedHours=0.0 where RecordDate >= ? and RecordDate <= ? ")

                .Append("Update arStudentClockAttendance set SchedHours=0.0  ")
                .Append("from ")
                .Append("arStudentClockAttendance,arStuEnrollments SE , syCmpGrpCmps E,syCampuses F,syCampGrps    ")
                .Append("Where ")
                .Append(" RecordDate >= ? and RecordDate <= ?  ")
                .Append(" and SE.StuEnrollId=arStudentClockAttendance.StuEnrollId ")
                .Append(" AND SE.CampusId=F.CampusId  ")
                .Append(" AND F.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId ")
                .Append(" and syCampGrps.CampGrpId=? ")

            End With
            db.ClearParameters()
            db.AddParameter("@HolidayStartDate", holidayInfo.HolidayStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@HolidayEndDate", holidayInfo.HolidayEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@CampGrpId", holidayInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb2.ToString)
            db.ClearParameters()


            '   return without errors
            Return ""

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteHolidayInfo(ByVal holidayId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New MyPortalDataAccess

        Dim myAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            myAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ClientConnectionStringOleDB")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            Dim sb1 As New StringBuilder
            Dim sb2 As New StringBuilder
            Dim sb3 As New StringBuilder
            Dim dr As OleDbDataReader
            Dim dtHolidayStartDate, dtHolidayEndDate As Date
            With sb1
                .Append("Select Distinct HolidayStartDate,HolidayEndDate from syHolidays where HolidayId=?")
            End With
            db.ClearParameters()
            db.AddParameter("@HolidayId", holidayId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            dr = db.RunParamSQLDataReader(sb1.ToString)
            Try
                While dr.Read
                    dtHolidayStartDate = CType(dr("HolidayStartDate"), Date)
                    dtHolidayEndDate = CType(dr("HolidayEndDate"), Date)
                End While
            Catch ex As Exception
            Finally
                dr.Close()
            End Try

            'Update the arClockAttendance and arStudentClockAttendance
            With sb2
                .Append("Update atConversionAttendance set Schedule=1.0 where MeetDate >= ? and MeetDate <= ? ")
            End With
            db.ClearParameters()
            db.AddParameter("@HolidayStartDate", dtHolidayStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@HolidayEndDate", dtHolidayEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            Try
                db.RunParamSQLExecuteNoneQuery(sb2.ToString)
            Catch ex As Exception
            End Try

            With sb3
                .Append("Update arStudentClockAttendance set SchedHours=0.0 where RecordDate >= ? and RecordDate <= ? ")
            End With
            db.ClearParameters()
            db.AddParameter("@HolidayStartDate", dtHolidayStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@HolidayEndDate", dtHolidayEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            Try
                db.RunParamSQLExecuteNoneQuery(sb3.ToString)
            Catch ex As Exception
            End Try

            With sb
                .Append("DELETE FROM syHolidays ")
                .Append("WHERE HolidayId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM syHolidays WHERE HolidayId = ? ")
            End With
            db.ClearParameters()
            '   add parameters values to the query

            '   HolidayId
            db.AddParameter("@HolidayId", holidayId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   HolidayId
            db.AddParameter("@HolidayId", holidayId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function IsHoliday(ByVal [date] As Date) As Boolean
        '   connect to the database
        Dim db As New MyPortalDataAccess

        Dim myAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            myAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ClientConnectionStringOleDB")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         Coalesce(count(*), 0) ")
            .Append("FROM     syHolidays CCT, syStatuses ST ")
            .Append("WHERE ")
            .Append("           (CCT.StatusId = ST.StatusId) ")
            .Append("AND		ST.Status = 'Active' ")
            .Append("AND		(? >= HolidayStartDate) ")
            .Append("AND		(? <= HolidayEndDate) ")
        End With

        ' Add the [date] to the parameter list
        db.AddParameter("@Date", [date], DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        ' Add the [date] to the parameter list
        db.AddParameter("@Date", [date], DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        '   return boolean
        Return db.RunParamSQLScalar(sb.ToString)

    End Function
    Public Function GetCollectionOfHolidayDates(Optional ByVal campusId As String = "") As List(Of AdvantageHoliday)
        '   connect to the database
        Dim db As New MyPortalDataAccess

        Dim myAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            myAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ClientConnectionStringOleDB")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("	      CCT.HolidayId, ")
            .Append("	      CCT.StatusId, ")
            .Append("	      CCT.HolidayCode, ")
            .Append("         CCT.HolidayStartDate, ")
            .Append("         Coalesce(CCT.HolidayEndDate, CCT.HolidayStartDate) As HolidayEndDate, ")
            .Append("         Coalesce((Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CCT.StartTimeId), '1899-12-30 00:00:00 AM') as StartTime, ")
            .Append("         Coalesce((Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CCT.EndTimeId), '1899-12-30 11:59:59 PM') as EndTime, ")
            .Append("	      CCT.HolidayDescrip ")
            .Append("FROM     syHolidays CCT, syStatuses ST ")
            .Append("WHERE    CCT.StatusId = ST.StatusId ")
            If campusId <> "" Then
                .Append("AND (CCT.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = '")
                .Append(campusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR     CCT.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            .Append("ORDER BY ST.Status,CCT.HolidayDescrip asc")
        End With

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        'instantiate collection of Holiday Dates 
        Dim collectionOfHolidayDates As List(Of AdvantageHoliday) = New List(Of AdvantageHoliday)

        While dr.Read()

            'generate list of holiday dates for the current record
            Dim listOfHolidayDates As List(Of AdvantageHoliday) = ConvertHolidayToCollectionOfAdvantageHoliday(dr("HolidayStartDate"), dr("HolidayEndDate"), dr("StartTime"), dr("EndTime"))

            'add each holiday in the list of holiday dates to the holidays collection
            For Each holiday As AdvantageHoliday In listOfHolidayDates
                collectionOfHolidayDates.Add(holiday)
            Next

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return collection of AdvantageClassSectionMeetings. Sort it by date
        collectionOfHolidayDates.Sort()
        Return collectionOfHolidayDates

    End Function
    Private Function ConvertHolidayToCollectionOfAdvantageHoliday(ByVal startDate As Date, ByVal endDate As Date, ByVal startTime As DateTime, ByVal endTime As DateTime) As List(Of AdvantageHoliday)

        Dim collectionOfAdvantageHolidayDates As New List(Of AdvantageHoliday)()

        'loop throughout all date range
        Dim currentDate As Date = Date.Parse(startDate.ToShortDateString + " 12:00:00 AM")
        While currentDate <= endDate

            'parse start time of the holiday
            Dim holidayDateAndTime As DateTime = Date.Parse(currentDate.Year.ToString + "-" + currentDate.Month.ToString + "-" + currentDate.Day.ToString + " " + startTime.Hour.ToString + ":" + startTime.Minute.ToString)

            'calculate the duration of the holiday
            Dim ts As TimeSpan = endTime.Subtract(startTime)
            Dim duration As Integer = ts.Hours * 60 + ts.Minutes

            'add Class Section Meeting to the collection
            Dim ah As AdvantageHoliday = New AdvantageHoliday(holidayDateAndTime, duration)
            collectionOfAdvantageHolidayDates.Add(ah)

            'point to next day
            currentDate = currentDate.AddDays(1)
        End While

        'return collection of dates
        Return collectionOfAdvantageHolidayDates

    End Function

    Function GetHolidaysDays(startDate As DateTime, endDate As DateTime, Optional campusId As String = "") As IList(Of HolidayDay)

        '   connect to the database
        Dim db As New MyPortalDataAccess

        Dim myAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            myAdvAppSettings = New PortalAdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ClientConnectionStringOleDB")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT ")
            '.Append("	      CCT.HolidayId, ")
            '.Append("	      CCT.StatusId, ")
            '.Append("	      CCT.HolidayCode, ")
            .Append("         CCT.HolidayStartDate, ")
            .Append("         Coalesce(CCT.HolidayEndDate, CCT.HolidayStartDate) As HolidayEndDate, ")
            .Append("         Coalesce((Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CCT.StartTimeId), '1899-12-30 00:00:00 AM') as StartTime, ")
            .Append("         Coalesce((Select TimeIntervalDescrip from cmTimeInterval where TimeIntervalId=CCT.EndTimeId), '1899-12-30 11:59:59 PM') as EndTime, ")
            .Append("	      CCT.HolidayDescrip, ")
            .Append("	      CCT.AllDay, ")
            .Append("	      CCT.CampGrpId ")
            .Append("FROM     syHolidays CCT, syStatuses ST ")
            .Append("WHERE    CCT.StatusId = ST.StatusId ")
            If campusId <> String.Empty Then
                .Append("AND (CCT.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId = '")
                .Append(campusId)
                .Append("' ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR     CCT.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            End If
            .Append("AND CCT.HolidayStartDate >= ? ")
            .Append("AND CCT.HolidayEndDate <= ? ")
            .Append("AND ST.Status = 'Active' ")
            .Append("ORDER BY ST.Status,CCT.HolidayDescrip asc")
        End With

        db.AddParameter("@HolidayStartDate", startDate, DataAccess.OleDbDataType.OleDbDateTime)
        db.AddParameter("@HolidayEndDate", endDate, DataAccess.OleDbDataType.OleDbDateTime)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Try
            'instantiate collection of Holiday Dates 
            Dim collectionOfHolidayDates As List(Of HolidayDay) = New List(Of HolidayDay)
            While dr.Read()

                'generate list of holiday dates for the current record
                Dim hf = New HolidayDay
                'hf.StatusOption = dr.GetString(0)
                hf.StartDate = dr.GetDateTime(0)
                hf.EndDate = dr.GetDateTime(1)
                hf.StartTimeInterval = dr.GetDateTime(2)
                hf.EndTimeInterval = dr.GetDateTime(3)
                hf.Description = dr.GetString(4)
                hf.AllDay = dr.GetBoolean(5)
                hf.CampusGroupId = dr.GetGuid(6)

                collectionOfHolidayDates.Add(hf)
            End While
            Return collectionOfHolidayDates
        Finally
            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
        End Try

    End Function

End Class