Imports FAME.Advantage.Common

Public Class AdReqsDB
    Public Function GetReqGroups(ByVal campusId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT ")
            .Append("       A.ReqGrpId,A.Code,A.Descrip ")
            .Append("FROM   adReqGroups A,syStatuses B,syCmpGrpCmps C ")
            .Append("WHERE  A.StatusId = B.StatusId AND B.Status = 'Active' ")
            .Append("       AND C.CampusId = ? ")
            .Append("ORDER BY A.Descrip")
        End With

        db.AddParameter("cmpid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function
    Public Function GetLeadGroupCount() As Integer
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        Dim intLeadGrpCount As Integer
        With sb
            'With subqueries
            .Append(" Select Count(t1.Descrip) from adLeadGroups t1,syStatuses t2 where t1.StatusId = t2.StatusId and t2.Status='Active'")
        End With

        'Execute the query
        intLeadGrpCount = db.RunParamSQLScalar(sb.ToString)

        Return intLeadGrpCount
    End Function
    'Public Function GetAllLeadGroups() As Integer
    '    'connect to the database
    '    Dim db As New DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

    '    'build the sql query
    '    Dim sb As New StringBuilder
    '    Dim intLeadGrpCount As Integer
    '    With sb
    '        'Get Queries
    '        .Append(" Select * from adLeadGroups")
    '    End With

    '    'Execute the query
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
    Public Function GetReqGroupsDS() As DataSet

        '   create dataset
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   build the sql query for the ReqGroups data adapter
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       A.ReqGrpId,")
            .Append("       A.Code,")
            .Append("       A.Descrip,")
            .Append("       A.StatusId,")
            .Append("       (CASE B.Status WHEN 'Active' THEN 1 ELSE 0 END) As Status,")
            .Append("       A.CampGrpId,")
            .Append("       A.IsMandatoryReqGrp,")
            .Append("       A.ModDate,")
            .Append("       A.ModUser ")
            .Append("FROM   adReqGroups A,syStatuses B ")
            .Append("WHERE  A.StatusId = B.StatusId ")
            .Append("ORDER BY A.Descrip")
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))

        '   Create adapter to handle StudentAwardSchedules table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill StudentAwardSchedules table
        da.Fill(ds, "ReqGroups")



        '   build the sql query for the LeadGroups data adapter
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       A.LeadGrpId,")
            .Append("       A.Descrip AS LeadGrpDescrip ")
            .Append("FROM   adLeadGroups A,syStatuses B ")
            .Append("WHERE  A.StatusId = B.StatusId AND B.Status = 'Active' ")
            .Append("ORDER BY A.Descrip")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   Fill LeadGroups table
        da.Fill(ds, "LeadGroups")



        '   build select query for the LeadGrpReqGrps data adapter
        '   build the sql query
        sb = New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       A.LeadGrpReqGrpId,")
            .Append("       A.ReqGrpId,")
            .Append("       B.Descrip AS ReqGrpDescrip,")
            .Append("       A.LeadGrpId,")
            .Append("       C.Descrip AS LeadGrpDescrip, ")
            .Append("       A.NumReqs, ")
            .Append("       A.ModDate, ")
            .Append("       A.ModUser ")
            .Append("FROM   adLeadGrpReqGroups A, adReqGroups B, adLeadGroups C ")
            .Append("WHERE  A.ReqGrpId = B.ReqGrpId AND A.LeadGrpId = C.LeadGrpId ")
            .Append("ORDER BY ReqGrpDescrip, LeadGrpDescrip")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   fill LeadGrpReqGrps table
        da.Fill(ds, "LeadGrpReqGroups")



        '   create primary and foreign key constraints

        '   set primary key for adReqGroups table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("ReqGroups").Columns("ReqGrpId")
        ds.Tables("ReqGroups").PrimaryKey = pk0

        '   set primary key for adLeadGroups table
        Dim pk1(0) As DataColumn
        pk1(0) = ds.Tables("LeadGroups").Columns("LeadGrpId")
        ds.Tables("LeadGroups").PrimaryKey = pk1

        '   set primary key for adLeadGrpReqGroups table
        Dim pk2(0) As DataColumn
        pk2(0) = ds.Tables("LeadGrpReqGroups").Columns("LeadGrpReqGrpId")
        ds.Tables("LeadGrpReqGroups").PrimaryKey = pk2

        '   set foreign key column in LeadGrpReqGrps
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("LeadGrpReqGroups").Columns("ReqGrpId")

        '   add relationship
        ds.Relations.Add("ReqGroupsLeadGrpReqGrp", pk0, fk0)

        '   set foreign key column in LeadGrpReqGrps
        Dim fk1(0) As DataColumn
        fk1(0) = ds.Tables("LeadGrpReqGroups").Columns("LeadGrpId")

        '   add relationship
        ds.Relations.Add("LeadGroupsLeadGrpReqGrp", pk1, fk1)

        '   return dataset
        Return ds

    End Function
    Public Function GetRequirementsAndEffectiveDatesDS() As DataSet

        '   create dataset
        Dim ds As New DataSet
        Dim strEnrollDate As Date = Date.Now.ToShortDateString
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        '   build the sql query for the ReqGroups data adapter
        Dim sb As New StringBuilder
        With sb

            '' Mantis Id 17676
            .Append(" select Distinct R3.adReqId as adReqId,R3.Code,R3.Descrip,R3.StatusId,R3.CampGrpId,R3.ModuleId,R3.adReqTypeId,R3.ModUser, ")
            .Append(" R3.ModDate,R3.Status, ")
            ''Added by Saraswathi Lakshmanan on Sept 20 2010
            ''For Document Tracking
            .Append(" R3.ReqforEnrollment,R3.ReqforFinancialAid,R3.ReqforGraduation , ")
            .Append(" Case when (select MandatoryRequirement from adReqsEffectiveDates where adReqId=R3.adReqId and ")
            .Append(" R4.CurrentDate >= StartDate and (R4.CurrentDate <= EndDate or EndDate is NULL)) is NULL then 0 ")
            .Append(" else ")
            .Append(" (select MandatoryRequirement from adReqsEffectiveDates where adReqId=R3.adReqId and  ")
            .Append(" R4.CurrentDate >= StartDate and (R4.CurrentDate <= EndDate or EndDate is NULL))  ")
            .Append(" end as MandatoryRequirement ")
            .Append(" from ")
            .Append(" ( ")
            .Append(" select A.adReqId,A.Code,A.Descrip,A.StatusId,A.CampGrpId,A.ModuleId,A.adReqTypeId, ")
            .Append(" A.ModUser,A.ModDate,(CASE B.Status WHEN 'Active' THEN 1 ELSE 0 END) As Status ")
            ''For Document Tracking
            .Append(" ,A.ReqforEnrollment,A.ReqforFinancialAid,A.ReqforGraduation ")
            .Append(" from ")
            .Append(" adReqs A,syStatuses B ")
            .Append(" where ")
            .Append(" A.StatusId = B.StatusId ")
            .Append(" ) R3, ")
            .Append(" ( ")
            .Append(" select R1.adReqId,R1.StartDate,R1.EndDate,'" & strEnrollDate & "' as CurrentDate ")
            .Append(" from ")
            .Append(" ( ")
            .Append(" select ")
            .Append(" A2.adReqId,getdate() as CurrentDate,A3.StartDate,A3.EndDate  ")
            .Append(" from ")
            .Append(" adReqs A2,adReqsEffectiveDates A3  ")
            .Append(" where ")
            .Append(" A2.adReqId = A3.adReqId and  A2.adreqTypeId in (1,3,4,5,6,7) ")
            .Append(" )  ")
            .Append(" R1  ")
            .Append(" ) R4 ")
            .Append(" where R3.adReqId = R4.adReqId ")
            .Append(" union ")
            .Append(" select adReqId,Code,Descrip,A.StatusId,CampGrpId,ModuleId,adReqTypeId, ModUser,ModDate, ")
            .Append(" (CASE B.Status WHEN 'Active' THEN 1 ELSE 0 END) As Status,")
            ''For Document Tracking
            .Append("  A.ReqforEnrollment,A.ReqforFinancialAid,A.ReqforGraduation, ")
            .Append(" 0 from adReqs A,syStatuses B where ")
            .Append(" A.StatusId = B.StatusId and adReqId not in (select Distinct adReqId from adReqsEffectiveDates) ")
        End With

        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(myAdvAppSettings.AppSettings("ConString")))

        '   Create adapter 
        Dim da As New OleDbDataAdapter(sc)

        '   Fill Requirements Table table
        da.Fill(ds, "Requirements")

        '   build the sql query for the Effective Dates
        sb = New StringBuilder
        With sb
            .Append(" select adReqEffectiveDateId,adReqId,StartDate,EndDate,MinScore,ValidDays,MandatoryRequirement,ModUser,ModDate from adReqsEffectiveDates where adReqId in ")
            .Append(" (select Distinct R3.adReqId  ")
            .Append(" from ")
            .Append(" ( ")
            .Append(" select A.adReqId,A.Code,A.Descrip,A.StatusId,A.CampGrpId,A.ModuleId,A.adReqTypeId, ")
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            ''.Append(" A.MinScore,A.ModUser,A.ModDate,(CASE B.Status WHEN 'Active' THEN 1 ELSE 0 END) As Status ")
            .Append(" A.ModUser,A.ModDate,(CASE B.Status WHEN 'Active' THEN 1 ELSE 0 END) As Status ")
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            .Append(" from ")
            .Append(" adReqs A,syStatuses B ")
            .Append(" where ")
            .Append(" A.StatusId = B.StatusId ")
            .Append(" ) R3, ")
            .Append(" ( ")
            .Append(" select R1.adReqId,R1.StartDate,R1.EndDate,'" & strEnrollDate & "' as CurrentDate ")
            .Append(" from ")
            .Append(" ( ")
            .Append(" select ")
            .Append(" A2.adReqId,getdate() as CurrentDate,A3.StartDate,A3.EndDate  ")
            .Append(" from ")
            .Append(" adReqs A2,adReqsEffectiveDates A3  ")
            .Append(" where ")
            .Append(" A2.adReqId = A3.adReqId and  A2.adreqTypeId in (1,3,4,5,6,7) ")
            .Append(" )  ")
            .Append(" R1  ")
            .Append(" ) R4 ")
            .Append(" where R3.adReqId = R4.adReqId) ")
        End With

        '   modify select command
        da.SelectCommand.CommandText = sb.ToString

        '   Fill LeadGroups table
        da.Fill(ds, "EffectiveDates")

        '   set primary key for Requirements table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("Requirements").Columns("adReqId")
        ds.Tables("Requirements").PrimaryKey = pk0

        '   set foreign key column in LeadGrpReqGrps
        Dim fk0(0) As DataColumn
        fk0(0) = ds.Tables("EffectiveDates").Columns("adReqId")

        '   add relationship
        ds.Relations.Add("RequirementsAndEffectiveDates", pk0, fk0)

        '   return dataset
        Return ds

    End Function
    Public Function UpdateReqGroupsDS(ByVal ds As DataSet) As String


        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '   all updates must be encapsulated as one transaction
        Dim connection As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build the sql query for the adLeadGrpReqGroups data adapter
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       LeadGrpReqGrpId,")
                .Append("       ReqGrpId,")
                .Append("       LeadGrpId, ")
                .Append("       NumReqs, ")
                .Append("       ModDate, ")
                .Append("       ModUser ")
                .Append("FROM   adLeadGrpReqGroups ")
            End With

            '   build select command
            Dim LeadGrpReqGroupsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle LeadGrpReqGroups table
            Dim LeadGrpReqGroupsDataAdapter As New OleDbDataAdapter(LeadGrpReqGroupsSelectCommand)

            '   build insert, update and delete commands for LeadGrpReqGroups table
            Dim cb As New OleDbCommandBuilder(LeadGrpReqGroupsDataAdapter)



            '   build select query for the ReqGroups data adapter
            sb = New StringBuilder
            With sb
                '   with subqueries
                .Append("SELECT ")
                .Append("       ReqGrpId,")
                .Append("       Code,")
                .Append("       Descrip,")
                .Append("       StatusId,")
                .Append("       CampGrpId,")
                .Append("       IsMandatoryReqGrp,")
                .Append("       ModDate,")
                .Append("       ModUser ")
                .Append("FROM   adReqGroups ")
            End With

            '   build select command
            Dim ReqGroupsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle StudentAwards table
            Dim ReqGroupsDataAdapter As New OleDbDataAdapter(ReqGroupsSelectCommand)

            '   build insert, update and delete commands for ReqGroups table
            Dim cb1 As New OleDbCommandBuilder(ReqGroupsDataAdapter)



            '   insert added rows in ReqGroups table
            ReqGroupsDataAdapter.Update(ds.Tables("ReqGroups").Select(Nothing, Nothing, DataViewRowState.Added))

            '   insert added rows in LeadGrpReqGrps table
            LeadGrpReqGroupsDataAdapter.Update(ds.Tables("LeadGrpReqGroups").Select(Nothing, Nothing, DataViewRowState.Added))

            '   delete rows in LeadGrpReqGrps table
            LeadGrpReqGroupsDataAdapter.Update(ds.Tables("LeadGrpReqGroups").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   delete rows in ReqGroups table
            ReqGroupsDataAdapter.Update(ds.Tables("ReqGroups").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   update rows in LeadGrpReqGrps table
            LeadGrpReqGroupsDataAdapter.Update(ds.Tables("LeadGrpReqGroups").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   update rows in ReqGroups table
            ReqGroupsDataAdapter.Update(ds.Tables("ReqGroups").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))



            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message`
            Return DALExceptions.BuildErrorMessage(ex)

        Catch ex As Exception
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return ex.Message

        Finally

            '   close connection
            connection.Close()
        End Try

    End Function
    Public Function UpdateRequirementsandEffectiveDatesDS(ByVal ds As DataSet) As String

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        '   all updates must be encapsulated as one transaction
        Dim connection As New OleDbConnection(myAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            '   build the sql query for the adLeadGrpReqGroups data adapter
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT adReqEffectiveDateId,")
                .Append("       adReqId,")
                .Append("       StartDate,")
                .Append("       EndDate, ")
                .Append("       MinScore,")
                .Append("       ValidDays,")
                .Append("       MandatoryRequirement,")
                .Append("       ModDate, ")
                .Append("       ModUser ")
                .Append("FROM   adReqsEffectiveDates ")
            End With

            '   build select command
            Dim LeadGrpReqGroupsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle LeadGrpReqGroups table
            Dim LeadGrpReqGroupsDataAdapter As New OleDbDataAdapter(LeadGrpReqGroupsSelectCommand)

            '   build insert, update and delete commands for LeadGrpReqGroups table
            Dim cb As New OleDbCommandBuilder(LeadGrpReqGroupsDataAdapter)

            '   build select query for the ReqGroups data adapter
            sb = New StringBuilder
            With sb
                '   with subqueries
                .Append(" Select adReqId,Code,Descrip,StatusId,CampGrpId,ModuleId,adReqTypeId, ")
                ''Modified by Saraswathi lakshmanan on Sept 20 2010
                ''For document tracking
                .Append(" ModUser,ModDate,ReqforEnrollment,ReqforFinancialAid,ReqforGraduation  from adReqs ")
            End With

            '   build select command
            Dim ReqGroupsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle StudentAwards table
            Dim ReqGroupsDataAdapter As New OleDbDataAdapter(ReqGroupsSelectCommand)

            '   build insert, update and delete commands for ReqGroups table
            Dim cb1 As New OleDbCommandBuilder(ReqGroupsDataAdapter)


            '   build select query for the ReqGroups data adapter
            'sb = New StringBuilder
            'With sb
            '    '   with subqueries
            '    .Append(" select ReqLeadGrpId,LeadGrpId,IsRequired,adReqEffectiveDateId from adReqLeadGroups ")

            'End With

            ''   build select command
            'Dim ReqLeadGroupsSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            ''   Create adapter to handle StudentAwards table
            'Dim ReqLeadGroupsDataAdapter As New OleDbDataAdapter(ReqLeadGroupsSelectCommand)

            ''   build insert, update and delete commands for ReqGroups table
            'Dim cbLeadGroups As New OleDbCommandBuilder(ReqLeadGroupsDataAdapter)

            '   insert added rows in adReqs table
            ReqGroupsDataAdapter.Update(ds.Tables("Requirements").Select(Nothing, Nothing, DataViewRowState.Added))

            '   insert added rows in adReqEffectiveDates table
            LeadGrpReqGroupsDataAdapter.Update(ds.Tables("EffectiveDates").Select(Nothing, Nothing, DataViewRowState.Added))

            '   Insert rows in LeadGrpReqGrps table
            '  ReqLeadGroupsDataAdapter.Update(ds.Tables("LeadGroupsByEffectiveDates").Select(Nothing, Nothing, DataViewRowState.Added))

            '   Delete rows in LeadGrpReqGrps table
            '  ReqLeadGroupsDataAdapter.Update(ds.Tables("LeadGroupsByEffectiveDates").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   delete rows in ReqGroups table
            LeadGrpReqGroupsDataAdapter.Update(ds.Tables("EffectiveDates").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   delete rows in LeadGrpReqGrps table
            ReqGroupsDataAdapter.Update(ds.Tables("Requirements").Select(Nothing, Nothing, DataViewRowState.Deleted))


            '   Delete rows in LeadGrpReqGrps table
            'ReqLeadGroupsDataAdapter.Update(ds.Tables("LeadGroupsByEffectiveDates").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   update rows in ReqGroups table
            LeadGrpReqGroupsDataAdapter.Update(ds.Tables("EffectiveDates").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))

            '   update rows in LeadGrpReqGrps table
            ReqGroupsDataAdapter.Update(ds.Tables("Requirements").Select(Nothing, Nothing, DataViewRowState.ModifiedCurrent))


            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message`
            Return DALExceptions.BuildErrorMessage(ex)

        Catch ex As Exception
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return ex.Message

        Finally

            '   close connection
            connection.Close()
        End Try

    End Function
    Public Function ValidateMandatoryReqGroup(ByVal ReqGrpId As String) As String

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT     ReqGrpId ")
            .Append("FROM       adReqGroups ")
            .Append("WHERE      IsMandatoryReqGrp = 1")
        End With

        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)
        Dim result As String = ""

        '   return string
        If Not obj Is Nothing Then
            If Convert.ToString(obj) <> ReqGrpId Then
                result = "The Mandatory Requirement Group already exists."
            End If
        End If
        If result = "" Then result = DeleteReqGrpDef(ReqGrpId)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return result
    End Function
    Public Function DeleteReqGrpDef(ByVal ReqGrpId As String) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            With sb
                .Append("DELETE FROM adReqGrpDef ")
                .Append("WHERE ReqGrpId = ? ")
            End With
            db.AddParameter("@ReqGrpId", ReqGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException

            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAllLeadGroups(ByVal CampusId As String, Optional ByVal strStatus As String = "Active") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         L.LeadGrpId, ")
            .Append("         L.StatusId, ")
            .Append("         ST.Status AS StatusDescrip, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         L.Code, ")
            .Append("         L.Descrip ")
            .Append("FROM     adLeadGroups L, syStatuses ST ")
            .Append("WHERE    L.StatusId = ST.StatusId ")
            If strStatus = "Active" Then
                .Append(" and ST.Status='Active' ")
            End If
            .Append("AND L.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("ORDER BY StatusDescrip, L.Descrip ")
        End With
        db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllLeadGroupsForExistingLeads(ByVal CampusId As String, Optional ByVal LeadId As String = "") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("         L.LeadGrpId, ")
            .Append("         L.StatusId, ")
            .Append("         ST.Status AS StatusDescrip, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         L.Code, ")
            .Append("         L.Descrip ")
            .Append("FROM     adLeadGroups L, syStatuses ST ")
            .Append("WHERE    L.StatusId = ST.StatusId ")
            .Append("AND L.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("AND ST.Status='Active' ")
            If Not LeadId = "" Then
                .Append("Union ")
                .Append("Select DISTINCT LL.LeadGrpId, ")
                .Append("         L.StatusId, ")
                .Append("         ST.Status AS StatusDescrip, ")
                .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
                .Append("         L.Code, ")
                .Append("         L.Descrip ")
                .Append("FROM     adLeadGroups L, syStatuses ST,adLeadByLeadGroups LL ")
                .Append("WHERE    L.StatusId = ST.StatusId and L.LeadGrpId=LL.LeadGrpId ")
                .Append("AND LL.LeadId=? and ST.Status <> 'Active' ")
            End If
            .Append("ORDER BY StatusDescrip, L.Descrip ")
        End With

        db.AddParameter("campId", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'optional lead id parameter
        If Not LeadId = "" Then
            db.AddParameter("leadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllLeadGroupsForEnrollment(ByVal CampusId As String, Optional ByVal StuEnrollId As String = "") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT DISTINCT ")
            .Append("         L.LeadGrpId, ")
            .Append("         L.StatusId, ")
            .Append("         ST.Status AS StatusDescrip, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         L.Code, ")
            .Append("         L.Descrip ")
            .Append("FROM     adLeadGroups L, syStatuses ST ")
            .Append("WHERE    L.StatusId = ST.StatusId ")
            .Append("AND L.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("AND ST.Status='Active' ")
            If Not StuEnrollId = "" Then
                .Append("Union ")
                .Append("Select DISTINCT LL.LeadGrpId, ")
                .Append("         L.StatusId, ")
                .Append("         ST.Status AS StatusDescrip, ")
                .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
                .Append("         L.Code, ")
                .Append("         L.Descrip ")
                .Append("FROM     adLeadGroups L, syStatuses ST,adLeadByLeadGroups LL ")
                .Append("WHERE    L.StatusId = ST.StatusId and L.LeadGrpId=LL.LeadGrpId ")
                .Append("AND LL.StuEnrollId=? and ST.Status <> 'Active' ")
            End If
            .Append("ORDER BY StatusDescrip, L.Descrip ")
        End With
        db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllLeadGroups() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         L.LeadGrpId, ")
            .Append("         L.StatusId, ")
            .Append("         ST.Status AS StatusDescrip, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         L.Code, ")
            .Append("         L.Descrip ")
            .Append("FROM     adLeadGroups L, syStatuses ST ")
            .Append("WHERE    L.StatusId = ST.StatusId ")
            .Append("ORDER BY StatusDescrip, L.Descrip ")
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetUseScheduleLeadGroups() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("         L.LeadGrpId, ")
            .Append("         L.StatusId, ")
            .Append("         ST.Status AS StatusDescrip, ")
            .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
            .Append("         L.Code, ")
            .Append("         L.Descrip ")
            .Append("FROM     adLeadGroups L, syStatuses ST ")
            .Append("WHERE    L.StatusId = ST.StatusId AND L.UseForScheduling = 1 ")
            .Append("ORDER BY StatusDescrip, L.Descrip ")
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllRequiredReqsByLeadGroup(ByVal adReqId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select LeadGrpId,IsRequired from adReqLeadGroups where adReqId='" & adReqId & "' ")
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllRequiredReqsCountByLeadGroup(ByVal adReqId As String) As Integer

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        Dim intReqCount As Integer
        With sb
            .Append(" select count(*) from adReqLeadGroups where adReqId='" & adReqId & "' ")
        End With

        '   return dataset
        intReqCount = db.RunParamSQLScalar(sb.ToString)
        Return intReqCount
    End Function
    Public Function AddAdRequirementInfo(ByVal adReqInfo As AdRequirementInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT adReqs ( ")
                .Append("       adReqId, Code, Descrip, StatusId, CampGrpId, ")
                .Append("       ModuleId, adReqTypeId, AppliesToAll, MinScore, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            '   Add parameters values to the query
            '   adReqId
            db.AddParameter("@adReqId", adReqInfo.ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Code
            db.AddParameter("@Code", adReqInfo.Code, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Descrip
            db.AddParameter("@Descrip", adReqInfo.Descrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", adReqInfo.StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   CampGrpId
            db.AddParameter("@CampGrpId", adReqInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModuleId
            db.AddParameter("@ModuleId", adReqInfo.ModuleId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   adReqTypeId
            db.AddParameter("@adReqTypeId", adReqInfo.ReqTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   AppliesToAll
            db.AddParameter("@AppliesToAll", adReqInfo.AppliesToAll, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   MinScore
            db.AddParameter("@MinScore", adReqInfo.MinScore, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            '   Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function UpdateAdRequirementInfo(ByVal adReqInfo As AdRequirementInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE adReqs ")
                .Append("   SET adReqId = ?, Code =?, Descrip = ?, StatusId = ?, CampGrpId = ?, ")
                .Append("       ModuleId = ?, adReqTypeId = ?, AppliesToAll = ?, MinScore = ?, ")
                .Append("       ModUser = ?, ModDate = ? ")
                .Append("WHERE adReqId = ? ")
                '.Append("AND ModDate = ? ;")
                '.Append("SELECT COUNT(*) FROM adReqs WHERE ModDate = ? ")
            End With

            '   Add parameters values to the query
            '   adReqId
            db.AddParameter("@adReqId", adReqInfo.ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   Code
            db.AddParameter("@Code", adReqInfo.Code, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Descrip
            db.AddParameter("@Descrip", adReqInfo.Descrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", adReqInfo.StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   CampGrpId
            db.AddParameter("@CampGrpId", adReqInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModuleId
            db.AddParameter("@ModuleId", adReqInfo.ModuleId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   adReqTypeId
            db.AddParameter("@adReqTypeId", adReqInfo.ReqTypeId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   adReqTypeId
            db.AddParameter("@AppliesToAll", adReqInfo.AppliesToAll, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   MinScore
            db.AddParameter("@MinScore", adReqInfo.MinScore, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   adReqId
            db.AddParameter("@adReqId", adReqInfo.ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ''   ModDate
            'db.AddParameter("@Original_ModDate", adReqInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            ''   ModDate
            'db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            'Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            'If rowCount = 1 Then
            '   return without errors
            Return ""
            'Else
            'Return DALExceptions.BuildConcurrencyExceptionMessage()
            'End If

        Catch ex As OleDbException

            '   return an error to the client
            'DisplayOleDbErrorCollection(ex)
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteAdRequirement(ByVal adReqId As String, ByVal modDate As DateTime) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try
            '   delete from child table first: adReqLeadGroups
            With sb
                .Append("DELETE FROM adReqLeadGroups ")
                .Append("WHERE adReqId = ? ")
            End With
            db.AddParameter("@adReqId", adReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

            db.ClearParameters()
            sb.Remove(0, sb.Length)


            '   delete from parent table: adReqs
            With sb
                .Append("DELETE FROM adReqs ")
                .Append("WHERE adReqId = ? ")
            End With
            db.AddParameter("@adReqId", adReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
            '   commit transaction 
            groupTrans.Commit()

            '   return without errors
            Return ""

        Catch ex As OleDbException
            groupTrans.Rollback()

            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function InsertReqLeadGroups(ByVal adReqLeadGrpId As AdReqLeadGrpInfo) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT adReqLeadGroups ( ")
                .Append("       ReqLeadGrpId, adReqId, LeadGrpId, ")
                .Append("       IsRequired, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   ReqLeadGrpId
            db.AddParameter("@ReqLeadGrpId", adReqLeadGrpId.ReqLeadGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ReqId
            db.AddParameter("@ReqId", adReqLeadGrpId.ReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   LeadGrpId
            db.AddParameter("@LeadGrpId", adReqLeadGrpId.LeadGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   IsRequired
            db.AddParameter("@IsRequired", adReqLeadGrpId.IsRequired, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", adReqLeadGrpId.ModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            '   Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function UpdateReqLeadGroups(ByVal adReqLeadGrpId As AdReqLeadGrpInfo) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE adReqLeadGroups ")
                .Append("   SET IsRequired = ?, ModUser = ?, ModDate = ? ")
                .Append("WHERE adReqId = ? AND LeadGrpId =? ")
                '.Append("AND ModDate = ? ;")
                '.Append("SELECT COUNT(*) FROM adReqLeadGroups WHERE ModDate = ? ")
            End With

            '   Add parameters values to the query
            '   IsRequired
            db.AddParameter("@IsRequired", adReqLeadGrpId.IsRequired, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", adReqLeadGrpId.ModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   adReqId
            db.AddParameter("@adReqId", adReqLeadGrpId.ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   LeadGrpId
            db.AddParameter("@LeadGrpId", adReqLeadGrpId.LeadGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            'Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            'If rowCount = 1 Then
            '   return without errors
            Return ""
            'Else
            'Return DALExceptions.BuildConcurrencyExceptionMessage()
            'End If

        Catch ex As OleDbException

            '   return an error to the client
            'DisplayOleDbErrorCollection(ex)
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Sub DeleteReqLeadGroups(ByVal adReqId As String, ByVal leadGrpId As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("DELETE FROM adReqLeadGroups ")
            .Append("WHERE AdReqId = ? AND LeadGrpId = ? ")
        End With

        db.AddParameter("@adReqId", adReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@leadGrpId", leadGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)

    End Sub
    Public Sub DeleteReqLeadGroups(ByVal adReqId As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("DELETE FROM adReqLeadGroups WHERE AdReqId = ? ")
        End With

        db.AddParameter("@adReqId", adReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)

    End Sub
    'Public Function GetAllLeadGroups() As DataSet
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim da As OleDbDataAdapter
    '    Dim strSQL As String
    '    Dim ds As New DataSet

    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")

    '    '   build the query
    '    With sb
    '        '   obtain Lead Groups already assigned to adReqId
    '        .Append(" select LeadGrpId,Descrip from adLeadGroups t1,SyStatuses t2 ")
    '        .Append(" where t1.StatusId = t2.StatusId and t2.Status='Active' ")
    '    End With

    '    db.OpenConnection()
    '    Return db.RunParamSQLDataSet(sb.ToString)
    'End Function
    Public Function GetLeadGroups(ByVal adReqId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim da As OleDbDataAdapter
        ''    Dim strSQL As String
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the query
        With sb
            '   obtain Lead Groups already assigned to adReqId
            .Append("SELECT ")
            .Append("       A.LeadGrpId,B.Descrip AS LeadGrpDescrip,A.IsRequired ")
            .Append("FROM   adReqLeadGroups A,adLeadGroups B ")
            .Append("WHERE  A.LeadGrpId = B.LeadGrpId AND A.adReqId = ? ")
            .Append("ORDER BY B.Descrip")
        End With

        db.AddParameter("@adReqId", adReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(sb.ToString)
        da.Fill(ds, "SelLeadGrps")
        db.ClearParameters()


        sb = New System.Text.StringBuilder
        With sb
            'Obtain Lead Groups not yet assigned to adReqId   
            .Append("SELECT ")
            .Append("       A.LeadGrpId,A.Descrip ")
            .Append("FROM   adLeadGroups A ")
            .Append("WHERE  NOT EXISTS (SELECT * FROM adReqLeadGroups B ")
            .Append("                  WHERE A.LeadGrpId = B.LeadGrpId AND B.adReqId = ?)")
        End With

        db.AddParameter("@adReqId", adReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        da = db.RunParamSQLDataAdapter(sb.ToString)
        da.Fill(ds, "AvailLeadGrps")

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    Public Function ReqByGrp(ByVal LeadId As String, ByVal PrgVerId As String) As String
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            '.Append(" select Distinct t1.ReqGrpId,t2.Descrip,t3.NumReqs from adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3 ")
            '.Append(" where t1.ReqGrpId = t2.ReqGrpId and  t2.ReqGrpId = t3.ReqGrpId and t1.PrgVerId='" & PrgVerId & "' ")
            '.Append(" Union ")
            '.Append(" select '00000000-0000-0000-0000-000000000000' as None,NULL as Miscellaneous,NULL from adLeadGroups ")
            '.Append(" order by Descrip ")
            '.Append(" select R1.ReqGrpId,R1.Descrip,R1.NumReqs,R2.TestCount+R2.DocumentCount as AttemptedReqs from ")
            '.Append(" (select Distinct t1.ReqGrpId,t2.Descrip,t3.NumReqs from adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3 ")
            '.Append(" where t1.ReqGrpId = t2.ReqGrpId and t2.ReqGrpId = t3.ReqGrpId and t1.PrgVerId='" & PrgVerId & "' and t3.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "')) R1, ")

            '.Append(" (select Distinct A4.Descrip as ReqGroup,A5.Descrip as Requirement,A8.LeadId,A5.adReqId,A3.ReqGrpId, ")
            '.Append(" (select count(*) from adLeadEntranceTest where EntrTestId=A5.adReqId and LeadId=A8.LeadId) as TestCount, ")
            '.Append(" (select count(*) from adPrgVerDocs where DocumentId=A5.adReqId and LeadId=A8.LeadId) as DocumentCount ")
            '.Append(" from ")
            '.Append(" adReqGrpDef A3,adReqGroups A4,adReqs A5,adReqLeadGroups A6,adPrgVerTestDetails A7,adLeads A8 ")
            '.Append(" where A3.ReqGrpId = A4.ReqGrpId And A3.adReqId = A5.adReqId And A5.adReqId = A6.adReqId  ")
            '.Append(" and A7.ReqGrpId = A3.ReqGrpId and A7.ReqGrpId is not Null ")
            '.Append(" and A7.PrgVerId='" & PrgVerId & "' and A8.PrgVerId = A7.PrgVerId ")
            '.Append(" and LeadId='" & LeadId & "') R2 ")
            '.Append(" where R1.ReqGrpId = R2.ReqGrpId ")
            '.Append(" Union ")
            '.Append(" select '00000000-0000-0000-0000-000000000000' as RepGrpId,NULL as Miscellaneous,NULL,NULL as AttemptedReqs from adLeadGroups ")
            '.Append(" order by Descrip  ")


            .Append(" select R1.ReqGrpId,R1.Descrip,R1.NumReqs,R1.TestAttempted + R1.DocsAttempted as AttemptedReqs ")
            .Append(" from ")
            .Append(" ( ")
            .Append(" Select  Distinct t1.ReqGrpId,t2.Descrip,t3.NumReqs, ")

            'Get The Number Of Tests Attempted By Lead In A Particular Group
            .Append(" (select Count(*) from adReqGrpDef A1,adReqLeadGroups  A2,adReqs A3,adLeadEntranceTest A4,adLeads A5 ")
            .Append(" where A1.ReqGrpId=t1.ReqGrpId and A1.adReqId = A2.adReqId and A2.LeadGrpId=(select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') and A2.adReqId = A3.adReqId and ")
            .Append(" A3.adReqId = A4.EntrTestId and A4.LeadId = A5.LeadId and A5.LeadId='" & LeadId & "' ")
            .Append(" ) as TestAttempted, ")

            'Get The Number Of Documents Attempted By Lead In A Particular Group
            .Append(" (select Count(*) from adReqGrpDef S1,adReqLeadGroups  s2,adReqs s3,adLeadDocsReceived s4,adLeads s5 ")
            .Append(" where s1.ReqGrpId=t1.ReqGrpId and s1.adReqId = s2.adReqId and ")
            .Append(" s2.LeadGrpId=(select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') and s2.adReqId = s3.adReqId and ")
            .Append(" s3.adReqId = s4.DocumentId and s4.LeadId = s5.LeadId and s5.LeadId='" & LeadId & "' ")
            .Append(" ) as DocsAttempted ")

            .Append(" from adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3 ")
            .Append(" where  ")
            .Append(" t1.ReqGrpId = t2.ReqGrpId and t2.ReqGrpId = t3.ReqGrpId and t1.PrgVerId='" & PrgVerId & "' ")
            .Append(" and t3.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")

            .Append(" union ")
            .Append(" select ReqGrpId,Descrip,(select count(*) from adReqs where AppliesToAll=1) as NumReqs, ")
            .Append(" (select count(*) from adLeadEntranceTest A7 where A7.EntrTestId  in (select adReqId from adReqs where AppliesToAll=1 and adReqTypeId=1) ")
            .Append(" and A7.LeadId='" & LeadId & "')as TestAttempted, ")
            .Append(" (select count(*) from adLeadDocsReceived A9 where A9.DocumentId  in (select adReqId from adReqs where AppliesToAll=1 and adReqTypeId=3) ")
            .Append(" and A9.LeadId='" & LeadId & "')as DocsAttempted ")
            .Append(" from adReqGroups where IsMandatoryReqGrp=1  ")
            .Append(" ) R1 ")
            .Append(" Union  ")
            .Append(" select '00000000-0000-0000-0000-000000000000' as RepGrpId,NULL as Miscellaneous,NULL,NULL as AttemptedReqs from adLeadGroups  ")
            ' .Append(" union ")
            '.Append(" select ReqGrpId,Descrip,(select count(*) from adReqs where AppliesToAll=1) as NumReqs,NULL as AttemptedReqs from adReqGroups where IsMandatoryReqGrp=1 ")
            .Append(" order by ReqGrpId desc  ")
        End With

        With sb1
            'Get All Requirements That Are Not Part Of Any Group
            .Append(" select t1.adReqId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,t2.Descrip, ")
            .Append(" t3.IsRequired as Required, ")
            .Append(" (select Count(*) from adLeadEntranceTest where EntrTestId = t1.adReqId and LeadId='" & LeadId & "') as TestTakenCount, ")
            .Append(" (select ActualScore from adLeadENtranceTest where EntrTestId=t1.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
            '.Append(" (select MinScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId='" & LeadId & "') as MinScore, ")
            .Append(" t2.MinScore as MinScore, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId = t1.adReqId and LeadId='" & LeadId & "') as DocSubmittedCount, ")
            .Append(" (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and  LeadId='" & LeadId & "') as EntrTestOverRide ")
            .Append(" from adPrgVerTestDetails t1,adReqs t2,adReqLeadGroups t3 ")
            .Append(" where t1.adReqId = t2.adReqId and t2.adReqId = t3.adReqId and t1.PrgVerId='" & PrgVerId & "' ")
            .Append(" and t1.ReqGrpId is null and t3.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
            .Append(" union ")

            'Get All Requirements That Are Part Of Group and Get The Requirements Based on Lead Group Level
            .Append(" select t5.adReqId as ReqId,t4.ReqGrpId,t5.Descrip,t6.IsRequired as Required, ")
            .Append(" (select Count(*) from adLeadEntranceTest where EntrTestId = t5.adReqId and LeadId='" & LeadId & "') as TestTakenCount, ")
            .Append(" (select ActualScore from adLeadENtranceTest where EntrTestId=t5.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
            '.Append(" (select MinScore from adLeadEntranceTest where EntrTestId=t5.adReqId and LeadId='" & LeadId & "') as MinScore, ")
            .Append(" t5.MinScore as MinScore, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and s3.LeadId='" & LeadId & "' and s3.DocumentId = t5.adReqId) as DocStatusDescrip, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId = t5.adReqId and LeadId='" & LeadId & "') as DocSubmittedCount, ")
            .Append(" (select OverRide from adEntrTestOverRide where EntrTestId=t5.adReqId and  LeadId='" & LeadId & "') as EntrTestOverRide ")
            .Append("  from  adReqGrpDef t4,adReqs t5,adReqLeadGroups t6 ")
            .Append(" where t4.adReqId = t5.adReqId and t5.adReqId = t6.adReqId and t6.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
            .Append(" and ReqGrpId in ")
            .Append(" (select t1.ReqGrpId from adPrgVerTestDetails t1 where t1.PrgVerId='" & PrgVerId & "' and ReqGrpId is not null) ")
            .Append(" union ")

            'Get Requirement Group That Has Been Assigned To Program Version But Has No Requirement
            .Append(" select '00000000-0000-0000-0000-000000000000' as adReqId,t1.ReqGrpId,NULL,0 as Required,0 as TestTakenCount, ")
            .Append(" (select ActualScore from adLeadENtranceTest where EntrTestId=t1.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
            '.Append(" (select MinScore from adLeadEntranceTest where EntrTestId=t1.adReqId and LeadId='" & LeadId & "') as MinScore, ")
            .Append(" 0 as MinScore,NULL,NULL,NULL as OverRide ")
            .Append(" from ")
            .Append(" adPrgVerTestDetails t1,adReqGroups t2 where t1.ReqGrpId = t2.ReqGrpId and ")
            .Append(" t2.ReqGrpId not in (select Distinct ReqGrpId from adReqGrpDef) and t1.PrgVerId='" & PrgVerId & "' and t1.ReqGrpId is not null ")
            .Append(" union ")

            'Get requirements assigned to a lead group but not assigned to program version
            'and not part of any requirement group
            .Append(" select Distinct B1.adReqId as EntrTestId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,B1.Descrip, ")
            .Append(" B2.IsRequired as Required,(select Count(*) from adLeadEntranceTest where EntrTestId = B1.adReqId and LeadId='" & LeadId & "') as TestTakenCount,  ")
            .Append(" (select ActualScore from adLeadENtranceTest where EntrTestId=B1.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
            .Append(" MinScore, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and s3.LeadId='" & LeadId & "' and s3.DocumentId = B1.adReqId) as DocStatusDescrip, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId = B1.adReqId and LeadId='" & LeadId & "') as DocSubmittedCount, ")
            .Append(" (select OverRide from adEntrTestOverRide where EntrTestId=B1.adReqId and  LeadId='" & LeadId & "') as EntrTestOverRide ")
            .Append(" from adReqs B1,adReqLeadGroups B2 where B1.adReqId = B2.adReqId ")
            .Append(" and B2.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
            .Append(" and B1.adReqTypeId in (1,3,4,5,6)  and B1.adReqId not in ")
            .Append(" (select adReqId from adPrgVerTestDetails where PrgVerId='" & PrgVerId & "' and adReqId is not null) ")
            .Append(" and B1.adReqId not in ")
            .Append(" (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails  ")
            .Append(" where PrgVerId='" & PrgVerId & "' and adReqId is NULL)) ")

            .Append(" union ")
            'Get Requirements that are mandatory
            .Append(" select adReqId,case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then (select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
            .Append(" else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId,Descrip,1 as Required, ")
            .Append(" (select Count(*) from adLeadEntranceTest where EntrTestId = adReqId and LeadId='" & LeadId & "') as TestTakenCount,   ")
            .Append(" (select ActualScore from adLeadENtranceTest where EntrTestId=adReqId and LeadId='" & LeadId & "') as ActualScore, ")
            .Append(" MinScore, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and s3.LeadId='" & LeadId & "' and s3.DocumentId = adReqId) as DocStatusDescrip, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId = adReqId and LeadId='" & LeadId & "') as DocSubmittedCount, ")
            .Append(" (select OverRide from adEntrTestOverRide where EntrTestId=adReqId and  LeadId='" & LeadId & "') as EntrTestOverRide ")
            .Append(" from adReqs where AppliesToAll=1 ")
            .Append(" order by Descrip ")
        End With

        '' Grab the Categories and Products table
        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        dadNorthwind = New OleDbDataAdapter(sb.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "Parent")

        dadNorthwind.SelectCommand = New OleDbCommand(sb1.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "Child")
        conNorthwind.Close()

        '' Add Parent/Child Relationship
        dstNorthwind.Relations.Add(
         "myrelation",
         dstNorthwind.Tables("Parent").Columns("ReqGrpId"),
         dstNorthwind.Tables("Child").Columns("ReqGrpId"))


        ''Display each Skill Group and Child Skills for the Student
        s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
        For Each drowParent In dstNorthwind.Tables("Parent").Rows
            If Not drowParent("Descrip") Is System.DBNull.Value Then
                s3 &= "<TR>"
                s3 &= "<td nowrap><font face=""verdana"" size=""2""><strong>" & drowParent("Descrip") & "</strong>&nbsp;</font>"
                If Not drowParent("NumReqs") Is System.DBNull.Value Then
                    If drowParent("NumReqs") >= 1 Then
                        s3 &= "<span style=""font: normal 12px arial; padding: 16px"">[Min Req:" & drowParent("NumReqs") & "]"
                        s3 &= "[Min Attempted:" & drowParent("AttemptedReqs") & "]</span>"
                    End If
                End If
                s3 &= "</td></tr>"
            Else
                s3 &= "<TR><td><font face=""verdana"" size=""2""><strong>Miscellaneous</strong></font></td></tr>"
            End If
            For Each drowChild In drowParent.GetChildRows("myrelation")
                If Not drowChild("Descrip") Is System.DBNull.Value Then
                    s3 &= "<tr>"
                    If Not drowChild("TestTakenCount") Is System.DBNull.Value And drowChild("TestTakenCount") >= 1 Then
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    ElseIf Not drowChild("TestTakenCount") Is System.DBNull.Value And drowChild("DocSubmittedCount") >= 1 Then
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    Else
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled></li>"
                    End If
                    s3 &= "<span style=""font:  normal 12px arial; padding: 16px"">" & drowChild("Descrip")
                    If drowChild("Required") = 1 Then
                        s3 &= "<span style=""color:red; padding-left: 3px;font: normal 12px arial;""><strong>(Required)</strong></span>"
                    End If

                    'Check If the Lead Has Passed The Test
                    If Not drowChild("ActualScore") Is System.DBNull.Value Then
                        If drowChild("MinScore") Is System.DBNull.Value Then
                            s3 &= ""
                        Else
                            If drowChild("ActualScore") < drowChild("MinScore") Then
                                s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Fail)</strong></span>"
                            Else
                                s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Pass)</strong></span>"
                            End If
                        End If
                    End If

                    'Check If Document was Approved
                    If Not drowChild("DocStatusDescrip") Is System.DBNull.Value Then
                        If drowChild("DocStatusDescrip") = "Approved" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Approved)</strong></span>"
                        Else
                            s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Not Approved)</strong></span>"
                        End If
                    End If

                    'Check If the requirement was overridden
                    If Not drowChild("EntrTestOverRide") Is System.DBNull.Value Then
                        If drowChild("EntrTestOverRide") = "Yes" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Overriden)</strong></span>"
                        End If
                    End If

                    s3 &= "</span></ul></td></tr>"
                End If
            Next
            s3 &= "<tr height=10><td>&nbsp;</td></tr>"
            forCounter = 1
        Next
        s3 &= "</table>"

        If forCounter = 1 Then
            Return s3.ToString()
        Else
            Return "None"
        End If
    End Function
    Public Function StandardReqByGrp(ByVal LeadId As String) As String
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append(" select R1.ReqGrpId,R1.Descrip,R1.NumReqs,R1.TestAttempted + R1.DocsAttempted as AttemptedReqs ")
            .Append(" from ")
            .Append(" ( ")
            .Append(" Select  Distinct t2.ReqGrpId,t2.Descrip,t3.NumReqs, ")

            'Get The Number Of Tests Attempted By Lead In A Particular Group
            .Append(" (select Count(*) from adReqGrpDef A1,adReqLeadGroups  A2,adReqs A3,adLeadEntranceTest A4,adLeads A5 ")
            .Append(" where A1.ReqGrpId=t2.ReqGrpId and A1.adReqId = A2.adReqId and A2.LeadGrpId=(select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') and A2.adReqId = A3.adReqId and ")
            .Append(" A3.adReqId = A4.EntrTestId and A4.LeadId = A5.LeadId and A5.LeadId='" & LeadId & "' ")
            .Append(" ) as TestAttempted, ")

            'Get The Number Of Documents Attempted By Lead In A Particular Group
            .Append(" (select Count(*) from adReqGrpDef S1,adReqLeadGroups  s2,adReqs s3,adLeadDocsReceived s4,adLeads s5 ")
            .Append(" where s1.ReqGrpId=t2.ReqGrpId and s1.adReqId = s2.adReqId and ")
            .Append(" s2.LeadGrpId=(select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') and s2.adReqId = s3.adReqId and ")
            .Append(" s3.adReqId = s4.DocumentId and s4.LeadId = s5.LeadId and s5.LeadId='" & LeadId & "' ")
            .Append(" ) as DocsAttempted ")
            .Append(" from adReqGroups t2,adLeadGrpReqGroups t3 ")
            .Append(" where  ")
            .Append(" t2.ReqGrpId = t2.ReqGrpId and t2.ReqGrpId = t3.ReqGrpId  ")
            .Append(" and t3.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")

            .Append(" union ")
            .Append(" select ReqGrpId,Descrip,(select count(*) from adReqs where AppliesToAll=1) as NumReqs, ")
            .Append(" (select count(*) from adLeadEntranceTest A7 where A7.EntrTestId  in (select adReqId from adReqs where AppliesToAll=1 and adReqTypeId=1) ")
            .Append(" and A7.LeadId='" & LeadId & "')as TestAttempted, ")
            .Append(" (select count(*) from adLeadDocsReceived A9 where A9.DocumentId  in (select adReqId from adReqs where AppliesToAll=1 and adReqTypeId=3) ")
            .Append(" and A9.LeadId='" & LeadId & "')as DocsAttempted ")
            .Append(" from adReqGroups where IsMandatoryReqGrp=1  ")
            .Append(" ) R1 ")
            .Append(" Union  ")
            .Append(" select '00000000-0000-0000-0000-000000000000' as RepGrpId,NULL as Miscellaneous,NULL,NULL as AttemptedReqs from adLeadGroups  ")
            ' .Append(" union ")
            '.Append(" select ReqGrpId,Descrip,(select count(*) from adReqs where AppliesToAll=1) as NumReqs,NULL as AttemptedReqs from adReqGroups where IsMandatoryReqGrp=1 ")
            .Append(" order by ReqGrpId desc  ")
        End With

        With sb1
            ' The Groups were removed for requirements 
            ' with out program version because if the requirement belongs to two groups
            ' then select statement will crash
            .Append(" select Distinct t1.adReqId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,t1.Descrip,t2.IsRequired as Required, ")
            .Append(" (select Count(*) from adLeadEntranceTest where EntrTestId = t1.adReqId and LeadId='" & LeadId & "') as TestTakenCount,  ")
            .Append(" (select ActualScore from adLeadENtranceTest where EntrTestId=t1.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
            .Append(" MinScore, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId = t1.adReqId and  ")
            .Append(" LeadId='" & LeadId & "') as DocSubmittedCount, ")
            .Append(" (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and  LeadId='" & LeadId & "') as EntrTestOverRide ")
            .Append(" from adReqs t1,adReqLeadGroups t2,adLeads t3 ")
            .Append(" where t1.adReqId = t2.adReqId And t2.LeadGrpId = t3.LeadGrpId ")
            .Append(" and t3.LeadId='" & LeadId & "' and t1.adReqTypeId in (1,3,4,5,6) ")
            .Append(" and t1.adReqId not in ")
            .Append(" (select adReqId from adPrgVerTestDetails where adReqId is not null) ")
            .Append(" and t1.adReqId not in ")
            .Append(" (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails ")
            .Append(" where adReqId is NULL)) and t1.adReqId not in (select Distinct adReqId from adReqGrpDef) ")
            .Append(" union ")
            .Append(" select Distinct t1.adReqId,t4.ReqGrpId,t1.Descrip,t2.IsRequired as Required, ")
            .Append(" (select Count(*) from adLeadEntranceTest where EntrTestId = t1.adReqId and LeadId='" & LeadId & "') as TestTakenCount,  ")
            .Append(" (select ActualScore from adLeadENtranceTest where EntrTestId=t1.adReqId and LeadId='" & LeadId & "') as ActualScore, ")
            .Append(" MinScore, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId = t1.adReqId and  ")
            .Append(" LeadId='" & LeadId & "') as DocSubmittedCount, ")
            .Append(" (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and  LeadId='" & LeadId & "') as EntrTestOverRide ")
            .Append(" from adReqs t1,adReqLeadGroups t2,adLeads t3,adReqGrpDef t4,adLeadGrpReqGroups t5 ")
            .Append(" where t1.adReqId = t2.adReqId And t2.LeadGrpId = t3.LeadGrpId and t1.adReqId = t4.adReqId ")
            .Append(" and t4.ReqGrpId = t5.ReqGrpId and t3.LeadGrpId = t5.LeadgrpId ")
            .Append(" and t3.LeadId='" & LeadId & "' and t1.adReqTypeId in (1,3,4,5,6) ")
            .Append(" and t1.adReqId not in ")
            .Append(" (select adReqId from adPrgVerTestDetails where adReqId is not null) ")
            .Append(" and t1.adReqId not in ")
            .Append(" (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails ")
            .Append(" where adReqId is NULL))  ")
            'Get all requirements that are assigned to lead group but are not part of any requirement group or any program version
            .Append(" union ")
            .Append(" select s1.adReqId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,s1.Descrip,  ")
            .Append(" s2.IsRequired as Required, ")
            .Append(" (select Count(*) from adLeadEntranceTest where EntrTestId = s1.adReqId and LeadId='5506048A-54DA-4E0C-9866-F0F0D70BC901') as TestTakenCount, ")
            .Append(" (select ActualScore from adLeadENtranceTest where EntrTestId=s1.adReqId and LeadId='5506048A-54DA-4E0C-9866-F0F0D70BC901') as ActualScore, ")
            .Append(" MinScore, ")
            .Append(" (select Distinct A1.DocStatusDescrip from sySysDocStatuses A1,syDocStatuses A2,adLeadDocsReceived A3 ")
            .Append(" where  A1.SysDocStatusId = A2.sysDocStatusId and A2.DocStatusId = A3.DocStatusId and ")
            .Append(" A3.LeadId='" & LeadId & "' and A3.DocumentId = s1.adReqId) as DocStatusDescrip, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId = s1.adReqId and  ")
            .Append(" LeadId='" & LeadId & "') as DocSubmittedCount, ")
            .Append(" (select OverRide from adEntrTestOverRide where EntrTestId=s1.adReqId and  LeadId='" & LeadId & "') as EntrTestOverRide ")
            .Append(" from adReqs s1,adReqLeadGroups s2,adLeads s3 ")
            .Append(" where s1.adReqId = s2.adReqId And s2.LeadgrpId = s3.LeadGrpId ")
            .Append(" and s3.LeadId='" & LeadId & "' and adReqTypeId=1 and s1.adReqId not in ")
            .Append(" (select adReqId from adPrgVerTestDetails where adReqId is not null) ")
            .Append(" and s1.adReqId not in (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails ")
            .Append(" where adReqId is NULL)) ")
            .Append(" union ")
            'Get Requirements that are mandatory
            .Append(" select adReqId,case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then (select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
            .Append(" else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId,Descrip,1 as Required, ")
            .Append(" (select Count(*) from adLeadEntranceTest where EntrTestId = adReqId and LeadId='" & LeadId & "') as TestTakenCount,  ")
            .Append(" (select ActualScore from adLeadENtranceTest where EntrTestId=adReqId and LeadId='" & LeadId & "') as ActualScore, ")
            .Append(" MinScore, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and s3.LeadId='" & LeadId & "' and s3.DocumentId = adReqId) as DocStatusDescrip, ")
            .Append(" (select Count(*) from adLeadDocsReceived where DocumentId = adReqId and LeadId='" & LeadId & "') as DocSubmittedCount, ")
            .Append(" (select OverRide from adEntrTestOverRide where EntrTestId=adReqId and  LeadId='" & LeadId & "') as EntrTestOverRide ")
            .Append(" from adReqs where AppliesToAll=1 ")
            .Append(" order by Descrip ")
        End With

        '' Grab the Categories and Products table
        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        dadNorthwind = New OleDbDataAdapter(sb.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "Parent")

        dadNorthwind.SelectCommand = New OleDbCommand(sb1.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "Child")
        conNorthwind.Close()

        ' Add Parent/Child Relationship
        Try
            dstNorthwind.Relations.Add(
             "myrelation",
             dstNorthwind.Tables("Parent").Columns("ReqGrpId"),
             dstNorthwind.Tables("Child").Columns("ReqGrpId"))
        Catch ex As System.Exception
            Return "None"
            Exit Function
        End Try

        ''Display each Skill Group and Child Skills for the Student
        s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
        For Each drowParent In dstNorthwind.Tables("Parent").Rows
            If Not drowParent("Descrip") Is System.DBNull.Value Then
                s3 &= "<TR>"
                s3 &= "<td nowrap><font face=""verdana"" size=""2""><strong>" & drowParent("Descrip") & "</strong>&nbsp;</font>"
                If Not drowParent("NumReqs") Is System.DBNull.Value Then
                    If drowParent("NumReqs") >= 1 Then
                        s3 &= "<span style=""font: normal 12px arial; padding: 16px"">[Min Req:" & drowParent("NumReqs") & "]"
                        s3 &= "[Min Attempted:" & drowParent("AttemptedReqs") & "]</span>"
                    End If
                End If
                s3 &= "</td></tr>"
            Else
                s3 &= "<TR><td><font face=""verdana"" size=""2""><strong>Miscellaneous</strong></font></td></tr>"
            End If
            For Each drowChild In drowParent.GetChildRows("myrelation")
                If Not drowChild("Descrip") Is System.DBNull.Value Then
                    s3 &= "<tr>"
                    If Not drowChild("TestTakenCount") Is System.DBNull.Value And drowChild("TestTakenCount") >= 1 Then
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    ElseIf Not drowChild("TestTakenCount") Is System.DBNull.Value And drowChild("DocSubmittedCount") >= 1 Then
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    Else
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled></li>"
                    End If
                    s3 &= "<span style=""font:  normal 12px arial; padding: 16px"">" & drowChild("Descrip")
                    If drowChild("Required") = 1 Then
                        s3 &= "<span style=""color:red; padding-left: 3px;font: normal 12px arial;""><strong>(Required)</strong></span>"
                    End If

                    'Check If the Lead Has Passed The Test
                    If Not drowChild("ActualScore") Is System.DBNull.Value Then
                        If drowChild("MinScore") Is System.DBNull.Value Then
                            s3 &= ""
                        Else
                            If drowChild("ActualScore") < drowChild("MinScore") Then
                                s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Fail)</strong></span>"
                            Else
                                s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Pass)</strong></span>"
                            End If
                        End If
                    End If

                    'Check If Document was Approved
                    If Not drowChild("DocStatusDescrip") Is System.DBNull.Value Then
                        If drowChild("DocStatusDescrip") = "Approved" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Approved)</strong></span>"
                        Else
                            s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Not Approved)</strong></span>"
                        End If
                    End If

                    'Check If the requirement was overridden
                    If Not drowChild("EntrTestOverRide") Is System.DBNull.Value Then
                        If drowChild("EntrTestOverRide") = "Yes" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Overriden)</strong></span>"
                        End If
                    End If

                    s3 &= "</span></ul></td></tr>"
                End If
            Next
            s3 &= "<tr height=10><td>&nbsp;</td></tr>"
            forCounter = 1
        Next
        s3 &= "</table>"

        If forCounter = 1 Then
            Return s3.ToString()
        Else
            Return "None"
        End If
    End Function
    Public Function InsertReqsByLeadGrp(ByVal adReqId As String, ByVal LeadGrpId() As String, ByVal Required() As String, ByVal Selected() As String, ByVal User As String) As Integer
        '   connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb1 As New StringBuilder
        With sb1
            .Append("Delete from adReqLeadGroups where adReqId = ? ")
        End With
        db.AddParameter("@adReqId", adReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'execute query
        db.RunParamSQLExecuteNoneQuery(sb1.ToString)
        db.ClearParameters()
        sb1.Remove(0, sb1.Length)

        '   Insert one record per each Item in the Selected Group
        ''   Dim i, j, k As Integer
        Dim x As Integer
        ''  Dim y, z As Integer
        Dim intRequired As Integer
        Try
            Dim intLeadCount As Integer
            ''  Dim strRequiredField As String

            intLeadCount = LeadGrpId.Length
            While x < intLeadCount
                If DirectCast(Selected.GetValue(x), String) = "Yes" Then
                    With sb
                        .Append("INSERT INTO adReqLeadGroups(ReqLeadGrpId,adReqId,LeadGrpId,IsRequired,ModUser,ModDate) ")
                        .Append("VALUES(?,?,?,?,?,?)")
                    End With
                    'add parameters
                    ''    Dim strPass As Integer

                    If DirectCast(Required.GetValue(x), String) = "Yes" Then
                        intRequired = 1
                    Else
                        intRequired = 0
                    End If


                    db.AddParameter("@ReqLeadGrpId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@adReqId", adReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@LeadGrpId", DirectCast(LeadGrpId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@IsRequired", intRequired, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                    'execute query
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)
                End If
                x = x + 1
            End While
            Return 0
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAllLeadGroupsByRequirement(ByVal adReqId As String) As DataSet
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append(" select Descrip,LeadGrpId, ")
            .Append("   case when (select count(*) from adReqLeadGroups where LeadGrpId=t1.LeadGrpId ")
            .Append(" and adReqEffectiveDateId='" & adReqId & "') >=1 then 'True' else 'False' end ")
            .Append(" as LeadGroupSelected, ")
            .Append(" case when (select count(*) from adReqLeadGroups where LeadGrpId=t1.LeadGrpId  ")
            .Append(" and adReqEffectiveDateId='" & adReqId & "' and IsRequired=1) >=1 then 'True' else 'False' end as Required ")
            .Append(" from adLeadGroups t1  ")
            .Append(" join syStatuses st on t1.StatusId = st.StatusId and st.StatusCode = 'A' ")
        End With

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds
    End Function
    Public Function GetReqDefByLeadGrp(ByVal ReqGrpId As String, ByVal LeadGrpId As String) As AdReqGrpInfo
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim objValue As New AdReqGrpInfo

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        With sb
            .Append(" select ReqGrpId,LeadgrpId,Numreqs, A.StatusId from adLeadGrpReqGroups A, syStatuses B where ReqGrpId = '" & ReqGrpId & "' and LeadGrpId = '" & LeadGrpId & "'  and  A.StatusId = B.StatusId  ")
        End With
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            If Not (dr("ReqGrpId") Is System.DBNull.Value) Then objValue.ReqGrpId = CType(dr("ReqGrpId"), Guid).ToString Else objValue.ReqGrpId = ""
            If Not (dr("LeadGrpId") Is System.DBNull.Value) Then objValue.LeadgrpId = CType(dr("LeadGrpId"), Guid).ToString Else objValue.LeadgrpId = ""
            If Not (dr("NumReqs") Is System.DBNull.Value) Then objValue.NumReqs = CInt(dr("NumReqs")) Else objValue.NumReqs = 0
            If Not (dr("StatusId") Is System.DBNull.Value) Then objValue.StatusId = CType(dr("StatusId"), Guid).ToString
        End While
        Return objValue
        dr.Close()
        db.ClearParameters()
        sb.Remove(0, sb.Length)
    End Function
    Public Function InsertLeadGroupsByRequirement(ByVal adReqEffectiveDateId As String, ByVal LeadGrpId() As String, ByVal Required() As String, ByVal user As String) As Integer
        '   connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb1 As New StringBuilder
        With sb1
            .Append("Delete from adReqLeadGroups where adReqEffectiveDateId = ? ")
        End With
        db.AddParameter("@adReqEffectiveDateId ", adReqEffectiveDateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'execute query
        db.RunParamSQLExecuteNoneQuery(sb1.ToString)
        db.ClearParameters()
        sb1.Remove(0, sb1.Length)

        '   Insert one record per each Item in the Selected Group
        ''    Dim i, j, k As Integer
        Dim x As Integer
        ''  Dim y, z As Integer
        Dim intRequired As Integer
        Try
            Dim intLeadCount As Integer
            ''   Dim strRequiredField As String

            intLeadCount = LeadGrpId.Length
            While x < intLeadCount
                With sb
                    .Append("INSERT INTO adReqLeadGroups(ReqLeadGrpId,adReqEffectiveDateId,LeadGrpId,IsRequired,ModUser,ModDate) ")
                    .Append("VALUES(?,?,?,?,?,?)")
                End With
                'add parameters
                ''   Dim strPass As Integer

                If DirectCast(Required.GetValue(x), String) = "Yes" Then
                    intRequired = 1
                Else
                    intRequired = 0
                End If


                db.AddParameter("@ReqLeadGrpId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@adReqEffectiveDateId ", adReqEffectiveDateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@LeadGrpId", DirectCast(LeadGrpId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@IsRequired", intRequired, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                'execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
                x = x + 1
            End While
            Return 0
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function InsertEntranceTest(ByVal LeadId As String, ByVal selectedEntranceTest() As String, ByVal selectedRequired() As String, ByVal selectedPass() As String, ByVal selectedTestTaken() As String, ByVal selectedActualScore() As String, ByVal selectedMinScore() As String, ByVal selectedComments() As String, ByVal selectedOverRide() As String, ByVal User As String) As Integer
        '   connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb1 As New StringBuilder
        With sb1
            .Append("Delete from adLeadEntranceTest where LeadId = ? ")
        End With
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'execute query
        db.RunParamSQLExecuteNoneQuery(sb1.ToString)
        db.ClearParameters()
        sb1.Remove(0, sb1.Length)

        '   Insert one record per each Item in the Selected Group
        'Dim i, j, k As Integer
        '', y, z
        Dim x As Integer
        Dim intRequired As Integer
        ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
        ''Dim intMinScore, intActualScore, intPass, intOverRide As Integer
        Dim intPass, intOverRide As Integer
        Dim intMinScore, intActualScore As Decimal
        ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
        Try
            Dim intLeadCount As Integer
            'Dim strRequiredField As String


            intLeadCount = selectedEntranceTest.Length
            While x < intLeadCount
                With sb
                    ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                    '.Append("INSERT INTO adLeadEntranceTest(LeadEntrTestId,LeadId,EntrTestId,TestTaken,ActualScore,MinScore,Comments,Required,Pass,ModUser,ModDate) ")
                    '.Append("VALUES(?,?,?,?,?,?,?,?,?,?,?)")
                    .Append("INSERT INTO adLeadEntranceTest(LeadEntrTestId,LeadId,EntrTestId,TestTaken,ActualScore,Comments,Required,Pass,ModUser,ModDate) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?)")
                    ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                End With

                If DirectCast(selectedRequired.GetValue(x), String) = "Yes" Then
                    intRequired = 1
                Else
                    intRequired = 0
                End If

                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                'If Not DirectCast(selectedActualScore.GetValue(x), String) = "" Then
                '    intActualScore = CInt(DirectCast(selectedActualScore.GetValue(x), String))
                'Else
                '    intActualScore = 0
                'End If
                If Not DirectCast(selectedActualScore.GetValue(x), String) = "" Then
                    intActualScore = CDbl(DirectCast(selectedActualScore.GetValue(x), String))
                Else
                    intActualScore = 0.0
                End If
                intMinScore = CDbl(DirectCast(selectedMinScore.GetValue(x), String))
                ''intMinScore = CInt(DirectCast(selectedMinScore.GetValue(x), String))
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767


                If intActualScore >= intMinScore Then
                    intPass = 1
                ElseIf intActualScore < intMinScore Then
                    intPass = 0
                Else
                    intPass = 2
                End If

                If DirectCast(selectedOverRide.GetValue(x), String) = "Yes" Then
                    intOverRide = 1
                Else
                    intOverRide = 0
                End If

                db.AddParameter("@LeadEntrTestId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@EntrTestId", DirectCast(selectedEntranceTest.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@TestTaken", DirectCast(selectedTestTaken.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                 If Not DirectCast(selectedActualScore.GetValue(x), String) = "" Then
                    db.AddParameter("@ActualScore", intActualScore, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else 
                    db.AddParameter("@ActualScore", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                ''db.AddParameter("@MinScore", DirectCast(selectedMinScore.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                db.AddParameter("@Comments", DirectCast(selectedComments.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@IsRequired", intRequired, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                If intPass = 2 Then
                    db.AddParameter("@IsPass", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                Else
                    db.AddParameter("@IsPass", intPass, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                End If
                'db.AddParameter("@IsOverRide", intOverRide, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                'execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                With sb
                    .Append("delete from adEntrTestOverRide where LeadId=? and EntrTestId=? ")
                End With
                db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@EntrTestId", DirectCast(selectedEntranceTest.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)


                With sb
                    .Append(" Insert into adEntrTestOverRide(EntrTestOverRideId,LeadId,EntrTestId,OverRide,ModUser,ModDate) ")
                    .Append(" values(?,?,?,?,?,?) ")
                End With
                db.AddParameter("@LeadEntrTestId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@EntrTestId", DirectCast(selectedEntranceTest.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@OverRide", intOverRide, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                'execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                x = x + 1
            End While
            Return 0
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateOverRide(ByVal LeadId As String, ByVal selectedEntranceTest() As String, ByVal selectedPass() As String, ByVal selectedDocumentStatus() As String, ByVal selectedReqType() As String, ByVal selectedOverRide() As String, ByVal User As String) As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   Insert one record per each Item in the Selected Group
        '' Dim i, j, k As Integer y, z
        Dim x As Integer
        ''Dim intRequired As Integer
        Dim intMinScore, intActualScore, intPass, intOverRide As Integer
        Try
            Dim intLeadCount As Integer
            ''   Dim strRequiredField As String

            intLeadCount = selectedEntranceTest.Length
            While x < intLeadCount

                'If DirectCast(selectedRequired.GetValue(x), String) = "Yes" Then
                '    intRequired = 1
                'Else
                '    intRequired = 0
                'End If

                If intActualScore >= intMinScore Then
                    intPass = 1
                Else
                    intPass = 0
                End If

                If DirectCast(selectedOverRide.GetValue(x), String) = "Yes" Then
                    intOverRide = 1
                Else
                    intOverRide = 0
                End If

                With sb
                    .Append("delete from adEntrTestOverRide where LeadId=? and EntrTestId=? ")
                End With
                db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@EntrTestId", DirectCast(selectedEntranceTest.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)


                With sb
                    .Append(" Insert into adEntrTestOverRide(EntrTestOverRideId,LeadId,EntrTestId,OverRide,ModUser,ModDate) ")
                    .Append(" values(?,?,?,?,?,?) ")
                End With
                db.AddParameter("@LeadEntrTestId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@EntrTestId", DirectCast(selectedEntranceTest.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@OverRide", intOverRide, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                'execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                x = x + 1
            End While
            Return 0
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function StandardReqByGrpAndEffectiveDates(ByVal LeadId As String, ByVal strEnrollDate As Date) As String
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            '.Append(" select t1.ReqGrpId,t1.Descrip,t2.NumReqs ")
            '.Append(" from adReqGroups t1,adLeadGrpReqGroups t2  ")
            '.Append(" where t1.ReqGrpId = t2.ReqGrpId ")
            '.Append(" and t2.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') ")
            '.Append(" union   ")
            '.Append(" select ReqGrpId,Descrip,NULL as NumReqs  ")
            '.Append(" from adReqGroups where IsMandatoryReqGrp=1   ")
            '.Append(" union  ")
            '.Append(" select '00000000-0000-0000-0000-000000000000' as RepGrpId,NULL as Miscellaneous,NULL from adLeadGroups   ")
            '.Append(" order by t1.ReqGrpId desc  ")

            .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,R5.testAttempted+R5.DocsAttempted as AttemptedReqs ")
            .Append(" from ( ")
            .Append(" select t1.ReqGrpId,t1.Descrip,t2.NumReqs, ")
            .Append(" ( ")
            .Append(" select Count(*) as TestAttempted ")
            .Append(" from ")
            .Append(" ( ")
            .Append(" select A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate from  ")
            .Append(" adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where  ")
            .Append(" A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and  ")
            .Append(" A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (1) ")
            .Append(" ) ")
            .Append(" R1,adLeadEntranceTest R2 where R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "' ")
            .Append(" and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) as TestAttempted, ")
            .Append(" ( ")
            .Append(" select Count(*) as DocsAttempted ")
            .Append(" from ")
            .Append(" ( ")
            .Append("	select A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate from ")
            .Append("	adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("	where  ")
            .Append("	A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append("	A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            .Append("	ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (3) ")
            .Append("	) ")
            .Append("	R1,adLeadDocsReceived R2 where R1.adReqId = R2.DocumentId and R2.LeadId='" & LeadId & "' ")
            .Append("	and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) as DocsAttempted ")
            .Append(" from adReqGroups t1,adLeadGrpReqGroups t2  ")
            .Append(" where t1.ReqGrpId = t2.ReqGrpId ")
            .Append(" and t2.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
            .Append(" LeadId='" & LeadId & "') ")
            .Append(" union   ")
            .Append(" select t7.ReqGrpId,t7.Descrip, ")
            .Append(" ( ")
            .Append(" select Count(*) as NumReqs ")
            .Append(" from ")
            .Append("  ( ")
            .Append("	select A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate from ")
            .Append("	adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3 ")
            .Append("	where ")
            .Append("	A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and ")
            .Append("	A1.ReqGrpId = t7.ReqGrpId  and A2.adreqTypeId in (1) ")
            .Append("	) ")
            .Append("	R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("  )  ")
            .Append(" as NumReqs, ")
            .Append(" ( ")
            .Append(" select Count(*) as TestAttempted ")
            .Append(" from ")
            .Append(" ( ")
            .Append(" select A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate from  ")
            .Append(" adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where  ")
            .Append(" A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and  ")
            .Append(" A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            .Append(" ReqGrpId = t7.ReqGrpId and A2.adreqTypeId in (1) ")
            .Append(" ) ")
            .Append(" R1,adLeadEntranceTest R2 where R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "' ")
            .Append(" and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) as TestAttempted, ")
            .Append(" ( ")
            .Append(" select Count(*) as DocsAttempted ")
            .Append(" from ")
            .Append(" ( ")
            .Append("	select A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate from ")
            .Append("	adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("	where  ")
            .Append("	A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append("	A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  and ")
            .Append("	ReqGrpId = t7.ReqGrpId and A2.adreqTypeId in (3) ")
            .Append("	) ")
            .Append("	R1,adLeadDocsReceived R2 where R1.adReqId = R2.DocumentId and R2.LeadId='" & LeadId & "' ")
            .Append("	and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) as DocsAttempted ")
            .Append(" from adReqGroups t7 where IsMandatoryReqGrp=1 ")
            .Append(" ) R5 ")
            .Append(" union  ")
            .Append(" select '00000000-0000-0000-0000-000000000000' as RepGrpId,NULL as Miscellaneous,NULL as NumReqs,Null as AttemptedReqs from adLeadGroups   ")
            .Append(" order by ReqGrpId desc  ")


        End With

        With sb1
            .Append(" select ")
            .Append(" adReqId,")
            .Append(" Descrip, ")
            .Append(" ReqGrpId, ")
            .Append(" StartDate, ")
            .Append("  EndDate, ")
            .Append(" ActualScore, ")
            .Append(" TestTaken, ")
            .Append(" Comments, ")
            .Append(" Case when OverRide=1 then 'True' else 'False' end as OverRide, ")
            .Append(" Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount, ")
            .Append(" DocSubmittedCount, ")
            .Append(" Minscore, ")
            .Append(" Required, ")
            .Append(" DocStatusDescrip ")
            .Append(" from ")
            .Append(" ( ")
            .Append(" select  ")
            .Append(" adReqId, ")
            .Append(" Descrip, ")
            .Append(" ReqGrpId, ")
            .Append(" StartDate, ")
            .Append(" EndDate, ")
            .Append(" ActualScore, ")
            .Append(" TestTaken, ")
            .Append(" Comments, ")
            .Append(" OverRide, ")
            .Append(" Minscore, ")
            .Append(" Required, ")
            .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
            .Append(" DocSubmittedCount, ")
            .Append(" DocStatusDescrip ")
            .Append(" from  ")
            .Append(" ( ")
            ' Get Requirement Group and requirements of mandatory requirement
            .Append(" select  ")
            .Append(" 	t1.adReqId, ")
            .Append(" 	t1.Descrip, ")
            .Append(" 	case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
            .Append(" 		(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
            .Append(" 	else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId,  ")
            .Append(" 	'" & strEnrollDate & "' as CurrentDate,  ")
            .Append(" 	t2.StartDate, ")
            .Append(" 	t2.EndDate,  ")
            .Append(" 	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append(" 		LeadId='" & LeadId & "' ) as ActualScore,  ")
            .Append(" 	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append(" 		LeadId='" & LeadId & "' ) as TestTaken,  ")
            .Append(" 	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append(" 		LeadId='" & LeadId & "' ) as Comments,  ")
            .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("                    LeadId='" & LeadId & "') ")
            .Append("            as override, ")
            .Append(" 	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            .Append(" 		LeadId='" & LeadId & "' ) as DocSubmittedCount, ")
            .Append(" 	t2.Minscore, ")
            .Append(" 	1 as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append(" from ")
            .Append(" 	adReqs t1, ")
            .Append(" 	adReqsEffectiveDates t2 ")
            .Append(" where  ")
            .Append(" 	t1.adReqId = t2.adReqId and  ")
            .Append(" 	t2.MandatoryRequirement=1 and ")
            .Append(" 	t1.adReqTypeId in (1,3,4,5,6) ")
            .Append(" ) ")
            .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" union ")
            ' Get Requirements assigned to a lead groups,but not assigned directly to a program version
            ' or not assigned to a requirement group 
            .Append(" select  ")
            .Append(" adReqId, ")
            .Append(" Descrip, ")
            .Append(" ReqGrpId, ")
            .Append(" StartDate, ")
            .Append(" EndDate, ")
            .Append(" ActualScore, ")
            .Append(" TestTaken, ")
            .Append(" Comments, ")
            .Append(" OverRide, ")
            .Append(" Minscore, ")
            .Append(" Required, ")
            .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
            .Append(" DocSubmittedCount, ")
            .Append(" DocStatusDescrip ")
            .Append(" from  ")
            .Append(" ( ")
            ' Get Requirement Group and requirements of mandatory requirement
            .Append(" select  ")
            .Append(" 	t1.adReqId, ")
            .Append(" 	t1.Descrip, ")
            .Append(" 	'00000000-0000-0000-0000-000000000000' as ReqGrpId,  ")
            .Append(" 	'" & strEnrollDate & "' as CurrentDate,  ")
            .Append(" 	t2.StartDate, ")
            .Append(" 	t2.EndDate,  ")
            .Append(" 	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append(" 		LeadId='" & LeadId & "' ) as ActualScore,  ")
            .Append(" 	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append(" 		LeadId='" & LeadId & "' ) as TestTaken,  ")
            .Append(" 	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append(" 		LeadId='" & LeadId & "' ) as Comments,  ")
            .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("                    LeadId='" & LeadId & "') ")
            .Append("            as override, ")
            .Append(" 	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            .Append(" 		LeadId='" & LeadId & "' ) as DocSubmittedCount, ")
            .Append(" 	t2.MinScore, ")
            .Append(" 	t3.IsRequired as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append(" 	from  ")
            .Append(" 		adReqs t1, ")
            .Append(" 		adReqsEffectiveDates t2, ")
            .Append(" 		adReqLeadGroups t3 ")
            .Append(" 	where  ")
            .Append(" 		t1.adReqId = t2.adReqId and  ")
            .Append(" 		t1.adreqTypeId in (1,3,4,5,6) and  ")
            .Append(" 		t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and  ")
            .Append("  t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
            .Append(" 		t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and  ")
            .Append(" 		t1.adReqId not in (select Distinct adReqId from adReqGrpDef) ")
            .Append(" ) ")
            .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" union ")
            ' Get Requirements assigned to a lead groups,but not assigned directly to a program version
            ' or not assigned to a requirement group that is part of a program version
            .Append(" select  ")
            .Append(" adReqId, ")
            .Append(" Descrip, ")
            .Append(" ReqGrpId, ")
            .Append(" StartDate, ")
            .Append(" EndDate, ")
            .Append(" ActualScore, ")
            .Append(" TestTaken, ")
            .Append(" Comments, ")
            .Append(" OverRide, ")
            .Append(" Minscore, ")
            .Append(" Required, ")
            .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
            .Append(" DocSubmittedCount, ")
            .Append(" DocStatusDescrip ")
            .Append(" from  ")
            .Append(" ( ")
            ' Get Requirement Group and requirements of mandatory requirement
            .Append(" select  ")
            .Append(" 	t1.adReqId, ")
            .Append(" 	t1.Descrip, ")
            .Append(" 	t5.ReqGrpId as ReqgrpId,  ")
            .Append(" 	'" & strEnrollDate & "' as CurrentDate,  ")
            .Append(" 	t2.StartDate, ")
            .Append(" 	t2.EndDate,  ")
            .Append(" 	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append(" 		LeadId='" & LeadId & "' ) as ActualScore,  ")
            .Append(" 	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append(" 		LeadId='" & LeadId & "' ) as TestTaken,  ")
            .Append(" 	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append(" 		LeadId='" & LeadId & "' ) as Comments,  ")
            .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("                    LeadId='" & LeadId & "') ")
            .Append("            as override, ")
            .Append(" 	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and ")
            .Append(" 		LeadId='" & LeadId & "' ) as DocSubmittedCount, ")
            .Append(" 	t2.MinScore, ")
            .Append(" 	t3.IsRequired as Required, ")
            .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append(" 	from  ")
            .Append(" 		adReqs t1, ")
            .Append(" 		adReqsEffectiveDates t2, ")
            .Append(" adReqLeadGroups t3,adReqGrpDef t5 ")
            .Append(" where ")
            .Append("	t1.adReqId = t2.adReqId and  ")
            .Append("	t1.adreqTypeId in (1,3,4,5,6) and  ")
            .Append("	t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t1.adReqId = t5.adReqId and  ")
            .Append("  t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
            .Append("	t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null)  ")
            '.Append("	and t5.ReqGrpId not in (select Distinct s1.ReqGrpId from adReqGrpDef s1,adPrgVerTestDetails s2 ")
            '.append("			     where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null) ")
            .Append(" ) ")
            .Append("	R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	")
            .Append("	) R2 ")
        End With

        '' Grab the Categories and Products table
        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        dadNorthwind = New OleDbDataAdapter(sb.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "Parent")

        dadNorthwind.SelectCommand = New OleDbCommand(sb1.ToString, conNorthwind)
        dadNorthwind.Fill(dstNorthwind, "Child")
        conNorthwind.Close()

        ' Add Parent/Child Relationship
        Try
            dstNorthwind.Relations.Add(
             "myrelation",
             dstNorthwind.Tables("Parent").Columns("ReqGrpId"),
             dstNorthwind.Tables("Child").Columns("ReqGrpId"))
        Catch ex As System.Exception
            Return "None"
            Exit Function
        End Try

        ''Display each Skill Group and Child Skills for the Student
        s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
        For Each drowParent In dstNorthwind.Tables("Parent").Rows
            If Not drowParent("Descrip") Is System.DBNull.Value Then
                s3 &= "<TR>"
                s3 &= "<td nowrap><font face=""verdana"" size=""2""><strong>" & drowParent("Descrip") & "</strong>&nbsp;</font>"
                If Not drowParent("NumReqs") Is System.DBNull.Value Then
                    If drowParent("NumReqs") >= 1 Then
                        s3 &= "<span style=""font: normal 12px arial; padding: 16px"">[Min Req:" & drowParent("NumReqs") & "]"
                        s3 &= "[Min Attempted:" & drowParent("AttemptedReqs") & "]</span>"
                    End If
                End If
                s3 &= "</td></tr>"
            Else
                s3 &= "<TR><td><font face=""verdana"" size=""2""><strong>Miscellaneous</strong></font></td></tr>"
            End If
            For Each drowChild In drowParent.GetChildRows("myrelation")
                If Not drowChild("Descrip") Is System.DBNull.Value Then
                    s3 &= "<tr>"
                    If Not drowChild("TestTakenCount") Is System.DBNull.Value And drowChild("TestTakenCount") >= 1 Then
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    ElseIf Not drowChild("TestTakenCount") Is System.DBNull.Value And drowChild("DocSubmittedCount") >= 1 Then
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    Else
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled></li>"
                    End If
                    s3 &= "<span style=""font:  normal 12px arial; padding: 16px"">" & drowChild("Descrip")
                    If drowChild("Required") = 1 Then
                        s3 &= "<span style=""color:red; padding-left: 3px;font: normal 12px arial;""><strong>(Required)</strong></span>"
                    End If

                    'Check If the Lead Has Passed The Test
                    If Not drowChild("ActualScore") Is System.DBNull.Value Then
                        If drowChild("MinScore") Is System.DBNull.Value Then
                            s3 &= ""
                        Else
                            If drowChild("ActualScore") >= 1 Then
                                If drowChild("ActualScore") < drowChild("MinScore") Then
                                    s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Fail)</strong></span>"
                                Else
                                    s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Pass)</strong></span>"
                                End If
                            End If
                        End If
                    End If

                    'Check If Document was Approved
                    If Not drowChild("DocStatusDescrip") Is System.DBNull.Value Then
                        If drowChild("DocStatusDescrip") = "Approved" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Approved)</strong></span>"
                        Else
                            s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Not Approved)</strong></span>"
                        End If
                    End If

                    'Check If the test was overridden
                    'If Not drowChild("TestOverRide") Is System.DBNull.Value Then
                    '    If drowChild("TestOverRide") = "Yes" Then
                    '        s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Overriden)</strong></span>"
                    '    End If
                    'End If

                    ''Check If the test was overridden
                    'If Not drowChild("DocumentOverRide") Is System.DBNull.Value Then
                    '    If drowChild("DocumentOverRide") = "Yes" Then
                    '        s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Overriden)</strong></span>"
                    '    End If
                    'End If

                    If Not drowChild("OverRide") Is System.DBNull.Value Then
                        If drowChild("OverRide") = "True" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Overriden)</strong></span>"
                        End If
                    End If


                    s3 &= "</span></ul></td></tr>"
                End If
            Next
            s3 &= "<tr height=10><td>&nbsp;</td></tr>"
            forCounter = 1
        Next
        s3 &= "</table>"

        If forCounter = 1 Then
            Return s3.ToString()
        Else
            Return "None"
        End If
    End Function
    Public Function ReqByGrpandEffectiveDates(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date) As String
        Dim dstNorthwind As DataSet
        Dim conNorthwind As New OleDbConnection
        Dim dadNorthwind As OleDbDataAdapter
        Dim drowParent As DataRow
        Dim drowChild As DataRow
        Dim s3 As String
        Dim forCounter As Integer
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim strCurrentDate As Date = strEnrollDate


        With sb

            '.Append(" select R1.ReqGrpId,R1.Descrip ")
            '.Append(" from ")
            '.Append(" ( ")
            '.Append(" Select  Distinct t1.ReqGrpId,t2.Descrip ")

            '.Append(" from adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3 ")
            '.Append(" where  ")
            '.Append(" t1.ReqGrpId = t2.ReqGrpId and t2.ReqGrpId = t3.ReqGrpId and t1.PrgVerId='" & PrgVerId & "' ")
            '.Append(" and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') ")
            '.Append(" union ")
            '.Append("  Select  Distinct t2.ReqGrpId,t2.Descrip  ")
            '.Append(" from adReqGroups t2,adLeadGrpReqGroups t3  ")
            '.Append(" where ")
            '.Append(" t2.ReqGrpId = t3.ReqGrpId and t3.ReqGrpId not in (select Distinct ReqGrpId from adPrgVerTestDetails where PrgVerId='" & PrgVerId & "' and ReqGrpId is not null) ")
            '.Append(" and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "')  ")
            '.Append(" union ")
            '.Append(" select ReqGrpId,Descrip ")
            '.Append(" from adReqGroups where IsMandatoryReqGrp=1  ")
            '.Append(" ) R1 ")
            '.Append(" Union  ")
            '.Append(" select '00000000-0000-0000-0000-000000000000' as RepGrpId,NULL as Miscellaneous from adLeadGroups  ")
            '.Append(" order by ReqGrpId desc  ")

            .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,R5.TestAttempted+R5.DocsAttempted as AttemptedReqs ")
            .Append("           from ")
            .Append("           (  ")
            .Append("           Select  Distinct t1.ReqGrpId,t2.Descrip,t3.NumReqs as Numreqs, ")
            .Append("	    ( ")
            .Append("	select Count(*) as TestAttempted ")
            .Append("		from ")
            .Append("		( ")
            .Append("			select A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate from ")
            .Append("		adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("			where ")
            .Append("			A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append("		A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' )  and ")
            .Append("		ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (1) ")
            .Append("	) ")
            .Append("	R1,adLeadEntranceTest R2 where R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "' ")
            .Append("	and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("    ) as TestAttempted, ")
            .Append("    ( ")
            .Append("	select Count(*) as DocsAttempted ")
            .Append("	from ")
            .Append("		( ")
            .Append("			select A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate from ")
            .Append("		adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("			where ")
            .Append("			A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append("		A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' )  and ")
            .Append("			ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (3) ")
            .Append("			) ")
            .Append("			R1,adLeadDocsReceived R2 where R1.adReqId = R2.DocumentId and R2.LeadId='" & LeadId & "' ")
            .Append("			and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("	) as DocsAttempted  ")
            .Append("    from adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3  ")
            .Append("    where  ")
            .Append("      t1.ReqGrpId = t2.ReqGrpId and t2.ReqGrpId = t3.ReqGrpId and t1.PrgVerId='" & PrgVerId & "'  ")
            .Append("       and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' ) ")
            .Append(" union ")
            .Append("           Select  Distinct t2.ReqGrpId,t2.Descrip,t3.NumReqs as NumReqs, ")
            .Append("	( ")
            .Append("		select Count(*) as TestAttempted ")
            .Append("		from ")
            .Append("		( ")
            .Append("		select A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate from  ")
            .Append("		adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("		where  ")
            .Append("		A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and  ")
            .Append("		A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' )  and ")
            .Append("		ReqGrpId = t2.ReqGrpId and A2.adreqTypeId in (1) ")
            .Append("		) ")
            .Append("		R1,adLeadEntranceTest R2 where R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "'  ")
            .Append("		and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("	) as TestAttempted, ")
            .Append("	( ")
            .Append("		select Count(*) as DocsAttempted ")
            .Append("		from ")
            .Append("		( ")
            .Append("			select A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate from  ")
            .Append("			adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("			where ")
            .Append("			A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append("			A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' )  and ")
            .Append("			ReqGrpId = t2.ReqGrpId and A2.adreqTypeId in (3) ")
            .Append("			) ")
            .Append("			R1,adLeadDocsReceived R2 where R1.adReqId = R2.DocumentId and R2.LeadId='" & LeadId & "' ")
            .Append("			and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("		) as DocsAttempted  ")
            .Append("    from adReqGroups t2,adLeadGrpReqGroups t3  ")
            .Append("     where ")
            .Append("     t2.ReqGrpId = t3.ReqGrpId and t3.ReqGrpId not in (select Distinct ReqGrpId from adPrgVerTestDetails where PrgVerId='" & PrgVerId & "'  and ReqGrpId is not null) ")
            .Append("     and t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' )  ")
            .Append("  union ")
            .Append("    select ")
            .Append("  	t7.ReqGrpId,t7.Descrip, ")
            .Append("  		( ")
            .Append("  		select Count(*) as NumReqs ")
            .Append("  		from ")
            .Append("  	  ( ")
            .Append("  		select A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate from ")
            .Append("  		adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3 ")
            .Append("  		where ")
            .Append("  	A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.MandatoryRequirement=1 and  ")
            .Append("  	A1.ReqGrpId = t7.ReqGrpId  and A2.adreqTypeId in (1) ")
            .Append("  	) ")
            .Append("  	R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("    ) ")
            .Append("  as NumReqs, ")
            .Append("  ( ")
            .Append("  select Count(*) as TestAttempted ")
            .Append("  from ")
            .Append("  ( ")
            .Append("  select A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate from ")
            .Append("  adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("  where ")
            .Append("  A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and  ")
            .Append("  A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' )  and ")
            .Append("  ReqGrpId = t7.ReqGrpId and A2.adreqTypeId in (1) ")
            .Append("  ) ")
            .Append("  R1,adLeadEntranceTest R2 where R1.adReqId = R2.EntrTestId and R2.LeadId='" & LeadId & "' ")
            .Append("  and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("  ) ")
            .Append("  as TestAttempted, ")
            .Append("  ( ")
            .Append("  select Count(*) as DocsAttempted ")
            .Append("  from ")
            .Append("  ( ")
            .Append("  select A2.adReqId,'" & strEnrollDate & "' as CurrentDate,A3.StartDate,A3.EndDate from  ")
            .Append("  adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("  where ")
            .Append("  A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId and ")
            .Append("  A4.LeadGrpId in (select LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' )  and ")
            .Append("  ReqGrpId = t7.ReqGrpId and A2.adreqTypeId in (3) ")
            .Append("  ) ")
            .Append("  R1,adLeadDocsReceived R2 where R1.adReqId = R2.DocumentId and R2.LeadId='" & LeadId & "'  ")
            .Append("  and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("  ) as DocsAttempted ")
            .Append("  from adReqGroups t7 where IsMandatoryReqGrp=1   ")
            .Append("   ) R5  ")
            .Append("  Union  ")
            .Append("  select '00000000-0000-0000-0000-000000000000' as RepGrpId,NULL as Miscellaneous,NULL as NumReqs,NULL as AttemptedReqs from adLeadGroups  ")
            .Append("  order by ReqGrpId desc  ")


        End With

        'With sb1
        '    'Get All Requirements That Are Not Part Of Any Group
        '    '
        '    .Append(" select ")
        '    .Append(" adReqId, ")
        '    .Append(" Descrip, ")
        '    .Append(" ReqGrpId, ")
        '    .Append(" StartDate, ")
        '    .Append(" EndDate, ")
        '    .Append(" ActualScore, ")
        '    .Append(" TestTaken, ")
        '    .Append(" Comments, ")
        '    .Append(" Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
        '    .Append(" Minscore, ")
        '    .Append(" Required, ")
        '    .Append(" DocSubmittedCount, ")
        '    .Append(" DocStatusDescrip, ")
        '    .Append(" Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount ")
        '    .Append(" from  ")
        '    .Append(" ( ")
        '    .Append(" select  ")
        '    .Append(" adReqId, ")
        '    .Append(" Descrip, ")
        '    .Append(" ReqGrpId, ")
        '    .Append(" StartDate, ")
        '    .Append(" EndDate, ")
        '    .Append(" ActualScore, ")
        '    .Append(" TestTaken, ")
        '    .Append(" Comments, ")
        '    .Append(" OverRide, ")
        '    .Append(" Minscore, ")
        '    .Append(" Required, ")
        '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
        '    .Append(" DocSubmittedCount, ")
        '    .Append(" DocStatusDescrip ")
        '    .Append(" from  ")
        '    .Append(" ( ")
        '    ' Get Requirement Group and requirements of mandatory requirement
        '    .Append(" select ")
        '    .Append("	t1.adReqId, ")
        '    .Append("	t1.Descrip, ")
        '    .Append("	case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
        '    .Append("		(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
        '    .Append("		else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
        '    .Append("	'" & strEnrollDate & "' as CurrentDate,  ")
        '    .Append("	t2.StartDate, ")
        '    .Append("	t2.EndDate, ")
        '    .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
        '    .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
        '    .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
        '    .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
        '    .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
        '    .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
        '    .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
        '    .Append("                    LeadId='" & LeadId & "') ")
        '    .Append("            as override, ")
        '    .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
        '    .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
        '    .Append("	t2.Minscore, ")
        '    .Append("	1 as Required, ")
        '    .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
        '    .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
        '    .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
        '    .Append(" from  ")
        '    .Append("	adReqs t1, ")
        '    .Append("	adReqsEffectiveDates t2 ")
        '    .Append(" where  ")
        '    .Append("	t1.adReqId = t2.adReqId and ")
        '    .Append("	t2.MandatoryRequirement=1 and ")
        '    .Append("	t1.adReqTypeId in (1,3) ")
        '    .Append(" ) ")
        '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
        '    .Append(" union ")
        '    ' Get Requirements assigned to a lead groups,but not assigned directly to a program version
        '    ' or not assigned to a requirement group 
        '    .Append(" select  ")
        '    .Append(" adReqId, ")
        '    .Append(" Descrip, ")
        '    .Append(" ReqGrpId, ")
        '    .Append(" StartDate, ")
        '    .Append(" EndDate, ")
        '    .Append(" ActualScore, ")
        '    .Append(" TestTaken, ")
        '    .Append(" Comments, ")
        '    .Append(" OverRide, ")
        '    .Append(" Minscore, ")
        '    .Append(" Required, ")
        '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
        '    .Append(" DocSubmittedCount, ")
        '    .Append(" DocStatusDescrip ")
        '    .Append(" from  ")
        '    .Append(" ( ")
        '    ' Get Requirement Group and requirements of mandatory requirement
        '    .Append(" select ")
        '    .Append("	t1.adReqId, ")
        '    .Append("	t1.Descrip, ")
        '    .Append("	'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
        '    .Append("	'" & strEnrollDate & "' as CurrentDate,  ")
        '    .Append("	t2.StartDate, ")
        '    .Append("	t2.EndDate, ")
        '    .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
        '    .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
        '    .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
        '    .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
        '    .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
        '    .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
        '    .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
        '    .Append("                    LeadId='" & LeadId & "') ")
        '    .Append("            as override, ")
        '    .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
        '    .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
        '    .Append("	t2.Minscore, ")
        '    .Append("	t3.Isrequired as Required, ")
        '    .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
        '    .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
        '    .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
        '    .Append(" from  ")
        '    .Append("	adReqs t1, ")
        '    .Append("		adReqsEffectiveDates t2, ")
        '    .Append("		adReqLeadGroups t3 ")
        '    .Append("	where ")
        '    .Append("		t1.adReqId = t2.adReqId and ")
        '    .Append("		t1.adreqTypeId in (1,3) and ")
        '    .Append("		t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  and ")
        '    .Append("  t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
        '    .Append("		t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and  ")
        '    .Append("		t1.adReqId not in (select Distinct adReqId from adReqGrpDef) ")
        '    .Append(" ) ")
        '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
        '    .Append(" union ")

        '    ' Requirements assigned to a program version and not part of any requirement group
        '    .Append(" select  ")
        '    .Append(" adReqId, ")
        '    .Append(" Descrip, ")
        '    .Append(" ReqGrpId, ")
        '    .Append(" StartDate, ")
        '    .Append(" EndDate, ")
        '    .Append(" ActualScore, ")
        '    .Append(" TestTaken, ")
        '    .Append(" Comments, ")
        '    .Append(" OverRide, ")
        '    .Append(" Minscore, ")
        '    .Append(" Required, ")
        '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
        '    .Append(" DocSubmittedCount, ")
        '    .Append(" DocStatusDescrip ")
        '    .Append(" from  ")
        '    .Append(" ( ")
        '    ' Get Requirement Group and requirements of mandatory requirement
        '    .Append(" select ")
        '    .Append("	t1.adReqId, ")
        '    .Append("	t1.Descrip, ")
        '    .Append(" case when t5.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t5.ReqGrpId End as reqGrpId,  ")
        '    .Append("	'" & strEnrollDate & "' as CurrentDate,  ")
        '    .Append("	t2.StartDate, ")
        '    .Append("	t2.EndDate, ")
        '    .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
        '    .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
        '    .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
        '    .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
        '    .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
        '    .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
        '    .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
        '    .Append("                    LeadId='" & LeadId & "') ")
        '    .Append("            as override, ")
        '    .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
        '    .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
        '    .Append("	t2.Minscore, ")
        '    .Append("	t3.IsRequired as Required, ")
        '    .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
        '    .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
        '    .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
        '    .Append(" from  ")
        '    .Append("	adReqs t1, ")
        '    .Append("		adReqsEffectiveDates t2, ")
        '    .Append("		adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
        '    .Append("	where ")
        '    .Append("		t1.adReqId = t2.adReqId and ")
        '    .Append("		t1.adreqTypeId in (1,3) and ")
        '    .Append("		t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
        '    .Append("		and t1.adReqId = t5.adReqId and t5.PrgVerId='" & PrgVerId & "'  and ")
        '    .Append("  t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
        '    .Append("		t1.adReqId not in (select Distinct adReqId from adReqGrpDef) ")
        '    .Append(" ) ")
        '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
        '    .Append(" union ")
        '    ' Get Requirements assigned to a lead groups,but not assigned directly to a program version
        '    ' or not assigned to a requirement group that is part of a program version
        '    .Append(" select  ")
        '    .Append(" adReqId, ")
        '    .Append(" Descrip, ")
        '    .Append(" ReqGrpId, ")
        '    .Append(" StartDate, ")
        '    .Append(" EndDate, ")
        '    .Append(" ActualScore, ")
        '    .Append(" TestTaken, ")
        '    .Append(" Comments, ")
        '    .Append(" OverRide, ")
        '    .Append(" Minscore, ")
        '    .Append(" Required, ")
        '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
        '    .Append(" DocSubmittedCount, ")
        '    .Append(" DocStatusDescrip ")
        '    .Append(" from  ")
        '    .Append(" ( ")
        '    ' Get Requirement Group and requirements of mandatory requirement
        '    .Append(" select ")
        '    .Append("	t1.adReqId, ")
        '    .Append("	t1.Descrip, ")
        '    .Append(" case when t5.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t5.ReqGrpId End as reqGrpId,  ")
        '    .Append("	'" & strEnrollDate & "' as CurrentDate,  ")
        '    .Append("	t2.StartDate, ")
        '    .Append("	t2.EndDate, ")
        '    .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
        '    .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
        '    .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
        '    .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
        '    .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
        '    .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
        '    .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
        '    .Append("                    LeadId='" & LeadId & "') ")
        '    .Append("            as override, ")
        '    .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
        '    .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
        '    .Append("	t2.Minscore, ")
        '    .Append("	t3.IsRequired as Required, ")
        '    .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
        '    .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
        '    .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
        '    .Append(" from  ")
        '    .Append("	adReqs t1, ")
        '    .Append("		adReqsEffectiveDates t2, ")
        '    .Append("		adReqLeadGroups t3,adReqGrpDef t5 ")
        '    .Append(" where ")
        '    .Append("	t1.adReqId = t2.adReqId and ")
        '    .Append("	t1.adreqTypeId in (1,3) and ")
        '    .Append("	t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t1.adReqId = t5.adReqId and ")
        '    .Append("  t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
        '    .Append("	t1.adReqId = t5.adReqId and ")
        '    .Append("	t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null) and ")
        '    .Append("	t5.adReqId not in (select Distinct s1.ReqGrpId from adReqGrpDef s1,adPrgVerTestDetails s2 ")
        '    .Append("			     where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null) ")
        '    .Append("	) ")
        '    .Append(" R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
        '    ' requirements part of a requirement group assigned to program version 
        '    .Append(" union ")
        '    .Append(" select  ")
        '    .Append(" adReqId, ")
        '    .Append(" Descrip, ")
        '    .Append(" ReqGrpId, ")
        '    .Append(" StartDate, ")
        '    .Append(" EndDate, ")
        '    .Append(" ActualScore, ")
        '    .Append(" TestTaken, ")
        '    .Append(" Comments, ")
        '    .Append(" OverRide, ")
        '    .Append(" Minscore, ")
        '    .Append(" Required, ")
        '    .Append(" Case  when ActualScore >= Minscore            then 'True' else 'False' End as Pass, ")
        '    .Append(" DocSubmittedCount,DocStatusDescrip ")
        '    .Append(" from  ")
        '    .Append(" ( ")
        '    '' Get Requirement Group and requirements of mandatory requirement
        '    .Append(" select ")
        '    .Append("	t1.adReqId, ")
        '    .Append("	t1.Descrip, ")
        '    .Append(" case when t5.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t5.ReqGrpId End as reqGrpId,  ")
        '    .Append("	'" & strEnrollDate & "' as CurrentDate,  ")
        '    .Append("	t2.StartDate, ")
        '    .Append("	t2.EndDate, ")
        '    .Append("	(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
        '    .Append("		LeadId='" & LeadId & "' ) as ActualScore,  ")
        '    .Append("	(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
        '    .Append("		LeadId='" & LeadId & "' ) as TestTaken,  ")
        '    .Append("	(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
        '    .Append("		LeadId='" & LeadId & "' ) as Comments,  ")
        '    .Append("                   (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
        '    .Append("                    LeadId='" & LeadId & "') ")
        '    .Append("            as override, ")
        '    .Append("	(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
        '    .Append("		LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
        '    .Append("	t2.Minscore, ")
        '    .Append("	t3.IsRequired as Required, ")
        '    .Append(" (select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
        '    .Append(" where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
        '    .Append(" s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
        '    .Append(" from  ")
        '    .Append("	adReqs t1, ")
        '    .Append("		adReqsEffectiveDates t2, ")
        '    .Append("		adReqLeadGroups t3,adReqGrpDef t5  ")
        '    .Append("	where  ")
        '    .Append("		t1.adReqId = t2.adReqId and  ")
        '    .Append("		t1.adreqTypeId in (1,3) and   ")
        '    .Append("		t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t1.adReqId = t5.adReqId and  ")
        '    .Append("  t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "') and ")
        '    .Append("		t5.ReqGrpId in (select Distinct s1.ReqGrpId from adReqGrpDef s1,adPrgVerTestDetails s2  ")
        '    .Append("				     where s1.ReqGrpId = s2.ReqGrpId and s1.adReqId is null)  ")
        '    .Append("	)  ")
        '    .Append("	R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)  ")
        '    .Append("	) R2 ")
        'End With

        With sb1

            .Append(" select  ")
            .Append(" adReqId, ")
            .Append(" Descrip, ")
            .Append(" ReqGrpId, ")
            .Append(" StartDate,  ")
            .Append(" EndDate, ")
            .Append(" ActualScore, ")
            .Append(" TestTaken, ")
            .Append(" Comments, ")
            .Append(" Case when OverRide>=1 then 'True' else 'False' end as OverRide, ")
            .Append(" Case when (select count(*) from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') >=1 then ")
            .Append(" (select MinScore from adPrgVerTestDetails where adReqId=R2.adReqId and PrgVerId='" & PrgVerId & "') ")
            .Append(" else MinScore end as MinScore, ")
            .Append(" Required, ")
            .Append(" DocSubmittedCount, ")
            .Append(" DocStatusDescrip, ")
            .Append(" Case when (TestTaken is not null and ActualScore >=1) then 1 else 0 end as TestTakenCount ")
            .Append(" from  ")
            .Append(" ( ")
            ' Get Requirement Group and requirements of mandatory requirement
            .Append(" 	select  ")
            .Append(" 		adReqId, ")
            .Append("		Descrip, ")
            .Append("		ReqGrpId, ")
            .Append("		StartDate, ")
            .Append("		EndDate, ")
            .Append("		ActualScore, ")
            .Append("		TestTaken, ")
            .Append("		Comments, ")
            .Append("		OverRide, ")
            .Append("		Minscore, ")
            .Append("		Required, ")
            .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("		DocSubmittedCount, ")
            .Append("		DocStatusDescrip ")
            .Append("	from  ")
            .Append("		( ")
            .Append("		select ")
            .Append("		t1.adReqId, ")
            .Append("				t1.Descrip, ")
            .Append("				case when (select Count(*) from adReqGroups where IsMandatoryReqGrp=1) >=1 then ")
            .Append("					(select ReqGrpId from adReqGroups where IsMandatoryReqGrp=1) ")
            .Append("					else '00000000-0000-0000-0000-000000000000' end  as ReqGrpId, ")
            .Append("				'" & strCurrentDate & "' as CurrentDate, ")
            .Append("				t2.StartDate, ")
            .Append("				t2.EndDate, ")
            .Append("				(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("					LeadId='" & LeadId & "' ) as ActualScore, ")
            .Append("				(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("					LeadId='" & LeadId & "' ) as TestTaken, ")
            .Append("				(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("					LeadId='" & LeadId & "' ) as Comments, ")
            .Append("				       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("				        LeadId='" & LeadId & "' ) as override, ")
            .Append("				(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("					LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append("				t2.Minscore, ")
            .Append("				1 as Required, ")
            .Append("				(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("				where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("				s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("				from  ")
            .Append("				adReqs t1, ")
            .Append("				adReqsEffectiveDates t2 ")
            .Append("				where  ")
            .Append("				t1.adReqId = t2.adReqId and ")
            .Append("				t2.MandatoryRequirement=1 and ")
            .Append("				t1.adReqTypeId in (1,3,4,5,6) ")
            .Append("				) ")
            .Append("				R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("		union ")
            ' Get Requirements assigned to a lead groups,but not assigned 
            '  directly to a program version
            ' or not assigned to a requirement group 
            .Append(" 	select  ")
            .Append(" 		adReqId, ")
            .Append("		Descrip, ")
            .Append("		ReqGrpId, ")
            .Append("		StartDate, ")
            .Append("		EndDate, ")
            .Append("		ActualScore, ")
            .Append("		TestTaken, ")
            .Append("		Comments, ")
            .Append("		OverRide, ")
            .Append("		Minscore, ")
            .Append("		Required, ")
            .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("		DocSubmittedCount, ")
            .Append("		DocStatusDescrip ")
            .Append("		from  ")
            .Append("				( ")
            .Append("						select ")
            .Append("							t1.adReqId, ")
            .Append("							t1.Descrip, ")
            .Append("							'00000000-0000-0000-0000-000000000000' as ReqGrpId, ")
            .Append("							'" & strCurrentDate & "' as CurrentDate, ")
            .Append("							t2.StartDate, ")
            .Append("							t2.EndDate, ")
            .Append("							(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("								LeadId='" & LeadId & "' ) as ActualScore, ")
            .Append("							(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("								LeadId='" & LeadId & "' ) as TestTaken, ")
            .Append("							(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("								LeadId='" & LeadId & "' ) as Comments, ")
            .Append("							       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("							        LeadId='" & LeadId & "' ) ")
            .Append("							as override, ")
            .Append("							(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("								LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append("							t2.Minscore, ")
            .Append("							t3.Isrequired as Required, ")
            .Append("							(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("							s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("						from  ")
            .Append("							adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3 ")
            .Append("						where ")
            .Append("							t1.adReqId = t2.adReqId and ")
            .Append("							t1.adreqTypeId in (1,3,4,5,6) and ")
            .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  and ")
            .Append("						       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' ) ")
            .Append("							and t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId = '" & PrgVerId & "' ) and  ")
            .Append("							t1.adReqId not in (select Distinct adReqId from adReqGrpDef f1,adLeadByLeadGroups f2 ")
            .Append("							where f1.LeadGrpId = f2.LeadGrpId and f2.LeadId='" & LeadId & "' ) ")
            .Append("					) ")
            .Append("					R1 where R1.CurrentDate >= R1.StartDate and ")
            .Append("					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("				union ")

            '' Requirements assigned to a lead group and part of requirement group but not assigned to a program version
            '' balaji 09/13/2005


            .Append(" 	select  ")
            .Append(" 		adReqId, ")
            .Append("		Descrip, ")
            .Append("		ReqGrpId, ")
            .Append("		StartDate, ")
            .Append("		EndDate, ")
            .Append("		ActualScore, ")
            .Append("		TestTaken, ")
            .Append("		Comments, ")
            .Append("		OverRide, ")
            .Append("		Minscore, ")
            .Append("		Required, ")
            .Append("		Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("		DocSubmittedCount, ")
            .Append("		DocStatusDescrip ")
            .Append("		from  ")
            .Append("				( ")
            .Append("						select ")
            .Append("							t1.adReqId, ")
            .Append("							t1.Descrip, ")
            .Append("							case when t4.ReqGrpId is NULL then '00000000-0000-0000-0000-000000000000' else t4.ReqGrpId end as ReqGrpId, ")
            .Append("							'" & strCurrentDate & "' as CurrentDate, ")
            .Append("							t2.StartDate, ")
            .Append("							t2.EndDate, ")
            .Append("							(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("								LeadId='" & LeadId & "' ) as ActualScore, ")
            .Append("							(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("								LeadId='" & LeadId & "' ) as TestTaken, ")
            .Append("							(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("								LeadId='" & LeadId & "' ) as Comments, ")
            .Append("							       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("							        LeadId='" & LeadId & "' ) ")
            .Append("							as override, ")
            .Append("							(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("								LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append("							t2.Minscore, ")
            .Append("							t3.Isrequired as Required, ")
            .Append("							(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("							s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("						from  ")
            .Append("							adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3,adReqGrpDef t4 ")
            .Append("						where ")
            .Append("							t1.adReqId = t2.adReqId and ")
            .Append("							t1.adreqTypeId in (1,3,4,5,6) and ")
            .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId  and ")
            .Append("                           t1.adReqId = t4.adReqId and t3.LeadGrpId = t4.LeadGrpId and ")
            .Append("						    t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where LeadId='" & LeadId & "' ) and ")
            .Append("						    t1.adReqId not in (select adReqId from adPrgVerTestDetails where adReqId is not null and PrgVerId = '" & PrgVerId & "') and  ")
            .Append("						    t1.adReqId not in (select Distinct f1.adReqId from adReqGrpDef f1,adLeadByLeadGroups f2,adPrgVerTestDetails f3 ")
            .Append("						                    where f1.LeadGrpId = f2.LeadGrpId and f1.reqGrpId=f3.ReqGrpId and f2.LeadId='" & LeadId & "' ")
            .Append("					                        and f3.prgVerId='" & PrgVerId & "' and f3.ReqGrpId is not null) ")
            .Append("					) ")
            .Append("					R1 where R1.CurrentDate >= R1.StartDate and ")
            .Append("					(R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("				union ")


            '' Requirements assigned to a program version
            .Append("			select  ")
            .Append("					adReqId, ")
            .Append("					Descrip, ")
            .Append("					ReqGrpId, ")
            .Append("					StartDate, ")
            .Append("					EndDate, ")
            .Append("					ActualScore, ")
            .Append("					TestTaken, ")
            .Append("						Comments, ")
            .Append("						OverRide, ")
            .Append("						Minscore, ")
            .Append("						Required, ")
            .Append("						Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("						DocSubmittedCount, ")
            .Append("						DocStatusDescrip ")
            .Append("				from  ")
            .Append("					( ")
            .Append("						select ")
            .Append("							t1.adReqId, ")
            .Append("							t1.Descrip, ")
            .Append("						'00000000-0000-0000-0000-000000000000' as reqGrpId, ")
            .Append("						'" & strCurrentDate & "' as CurrentDate, ")
            .Append("						t2.StartDate, ")
            .Append("						t2.EndDate, ")
            .Append("						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("							LeadId='" & LeadId & "' ) as ActualScore, ")
            .Append("					(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("							LeadId='" & LeadId & "' ) as TestTaken, ")
            .Append("						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("							LeadId='" & LeadId & "' ) as Comments, ")
            .Append("					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("						        LeadId='" & LeadId & "' ) ")
            .Append("						as override, ")
            .Append("						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("							LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append("					t2.Minscore, ")
            .Append("						t3.IsRequired as Required, ")
            .Append("						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("						s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("					from  ")
            .Append("							adReqs t1, ")
            .Append("							adReqsEffectiveDates t2, ")
            .Append("							adReqLeadGroups t3,adLeads t4,adPrgVerTestDetails t5 ")
            .Append("						where ")
            .Append("						t1.adReqId = t2.adReqId and ")
            .Append("							t1.adreqTypeId in (1,3,4,5,6) and ")
            .Append("							t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t4.PrgVerId = t5.PrgVerId ")
            .Append("							and t1.adReqId = t5.adReqId and t5.PrgVerId = '" & PrgVerId & "'   and ")
            .Append("					       t3.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups where ")
            .Append("						LeadId='" & LeadId & "' ) ")
            .Append("							and t5.adReqId is not null ")
            .Append("						) ")
            .Append("						R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append("					union ")

            'Get Requirement groups assigned to a program version and all requirements part of group

            .Append("			select  ")
            .Append("					adReqId, ")
            .Append("					Descrip, ")
            .Append("					ReqGrpId, ")
            .Append("					StartDate, ")
            .Append("					EndDate, ")
            .Append("					ActualScore, ")
            .Append("					TestTaken, ")
            .Append("						Comments, ")
            .Append("						OverRide, ")
            .Append("						Minscore, ")
            .Append("						Required, ")
            .Append("						Case  when ActualScore >= Minscore then 'True' else 'False' End as Pass, ")
            .Append("						DocSubmittedCount, ")
            .Append("						DocStatusDescrip ")
            .Append("				from  ")
            .Append("					( ")
            .Append("						select ")
            .Append("							t1.adReqId, ")
            .Append("							t1.Descrip, ")
            .Append("						t5.ReqGrpId as reqGrpId, ")
            .Append("						'" & strCurrentDate & "' as CurrentDate, ")
            .Append("						t2.StartDate, ")
            .Append("						t2.EndDate, ")
            .Append("						(select ActualScore from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("							LeadId='" & LeadId & "' ) as ActualScore, ")
            .Append("					(select TestTaken from adLeadEntranceTest where EntrTestId=t1.adReqId and ")
            .Append("							LeadId='" & LeadId & "' ) as TestTaken, ")
            .Append("						(select Comments from adLeadEntranceTest where EntrTestId=t1.adReqId and  ")
            .Append("							LeadId='" & LeadId & "' ) as Comments, ")
            .Append("					       (select OverRide from adEntrTestOverRide where EntrTestId=t1.adReqId and ")
            .Append("						        LeadId='" & LeadId & "' ) ")
            .Append("						as override, ")
            .Append("						(select Count(*) from adLeadDocsReceived where DocumentId=t1.adReqId and  ")
            .Append("							LeadId='" & LeadId & "'  ) as DocSubmittedCount, ")
            .Append("					t2.Minscore, ")
            .Append("						t3.IsRequired as Required, ")
            .Append("						(select Distinct s1.DocStatusDescrip from sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 ")
            .Append("							where  s1.SysDocStatusId = s2.sysDocStatusId and s2.DocStatusId = s3.DocStatusId and ")
            .Append("						s3.LeadId='" & LeadId & "' and s3.DocumentId = t1.adReqId) as DocStatusDescrip ")
            .Append("					from  ")
            .Append("						adReqs t1,adReqsEffectiveDates t2,adReqLeadGroups t3,adPrgVerTestDetails t5,adReqGrpDef t6,adLeadByLeadGroups t7 ")
            .Append("					where ")
            .Append("						t1.adReqId = t2.adReqId and ")
            .Append("					t1.adreqTypeId in (1,3,4,5,6) and ")
            .Append("					t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and t5.ReqGrpId = t6.ReqGrpId and  t3.LeadGrpId = t7.LeadGrpId and ")
            .Append("					t6.LeadGrpId = t7.LeadGrpId and  t1.adReqId = t6.adReqId and t5.ReqGrpId is not null ")
            .Append("					and t7.LeadId='" & LeadId & "'  and t5.PrgVerId = '" & PrgVerId & "'  ")
            .Append("		) R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) ")
            .Append(" ) ")
            .Append(" R2 ")
        End With



        '' Grab the Categories and Products table
        dstNorthwind = New DataSet
        conNorthwind.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        dadNorthwind = New OleDbDataAdapter(sb.ToString, conNorthwind)
        conNorthwind.Open()
        dadNorthwind.Fill(dstNorthwind, "Parent")

        dadNorthwind.SelectCommand = New OleDbCommand(sb1.ToString, conNorthwind)
        'dadNorthwind.SelectCommand.Parameters.Add("@strEnrollDate", OleDbType.Date)
        'dadNorthwind.SelectCommand.Parameters("@strEnrolldate").Value = strEnrollDate
        'dadNorthwind.SelectCommand.Parameters.Add("@strEnrollDate", OleDbType.Date)
        'dadNorthwind.SelectCommand.Parameters("@strEnrolldate").Value = strEnrollDate
        'dadNorthwind.SelectCommand.Parameters.Add("@strEnrollDate", OleDbType.Date)
        'dadNorthwind.SelectCommand.Parameters("@strEnrolldate").Value = strEnrollDate
        'dadNorthwind.SelectCommand.Parameters.Add("@strEnrollDate", OleDbType.Date)
        'dadNorthwind.SelectCommand.Parameters("@strEnrolldate").Value = strEnrollDate
        'dadNorthwind.SelectCommand.Parameters.Add("@strEnrollDate", OleDbType.Date)
        'dadNorthwind.SelectCommand.Parameters("@strEnrolldate").Value = strEnrollDate


        dadNorthwind.Fill(dstNorthwind, "Child")
        conNorthwind.Close()

        '' Add Parent/Child Relationship
        dstNorthwind.Relations.Add(
         "myrelation",
         dstNorthwind.Tables("Parent").Columns("ReqGrpId"),
         dstNorthwind.Tables("Child").Columns("ReqGrpId"))


        ''Display each Skill Group and Child Skills for the Student
        s3 &= "<TABLE width=""100%"" cellpadding=""0"" cellspacing=""0"">"
        For Each drowParent In dstNorthwind.Tables("Parent").Rows
            If Not drowParent("Descrip") Is System.DBNull.Value Then
                s3 &= "<TR>"
                s3 &= "<td nowrap><font face=""verdana"" size=""2""><strong>" & drowParent("Descrip") & "</strong>&nbsp;</font>"
                If Not drowParent("NumReqs") Is System.DBNull.Value Then
                    If drowParent("NumReqs") >= 1 Then
                        s3 &= "<span style=""font: normal 12px arial; padding: 16px"">[Min Req:" & drowParent("NumReqs") & "]"
                        s3 &= "[Min Attempted:" & drowParent("AttemptedReqs") & "]</span>"
                    End If
                End If
                s3 &= "</td></tr>"
            Else
                s3 &= "<TR><td><font face=""verdana"" size=""2""><strong>Miscellaneous</strong></font></td></tr>"
            End If
            For Each drowChild In drowParent.GetChildRows("myrelation")
                If Not drowChild("Descrip") Is System.DBNull.Value Then
                    s3 &= "<tr>"
                    If Not drowChild("TestTakenCount") Is System.DBNull.Value And drowChild("TestTakenCount") >= 1 Then
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    ElseIf Not drowChild("TestTakenCount") Is System.DBNull.Value And drowChild("DocSubmittedCount") >= 1 Then
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled Checked></li>"
                    Else
                        s3 &= "<td style=""font-size: 12px""><ul><li><input type=""checkbox"" id=""chkTaken"" disabled></li>"
                    End If
                    s3 &= "<span style=""font:  normal 12px arial; padding: 16px"">" & drowChild("Descrip")
                    If drowChild("Required") = 1 Then
                        s3 &= "<span style=""color:red; padding-left: 3px;font: normal 12px arial;""><strong>(Required)</strong></span>"
                    End If

                    'Check If the Lead Has Passed The Test
                    If Not drowChild("ActualScore") Is System.DBNull.Value Then
                        If drowChild("MinScore") Is System.DBNull.Value Then
                            s3 &= ""
                        Else
                            If drowChild("ActualScore") < drowChild("MinScore") Then
                                s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Fail)</strong></span>"
                            Else
                                s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Pass)</strong></span>"
                            End If
                        End If
                    End If

                    'Check If Document was Approved
                    If Not drowChild("DocStatusDescrip") Is System.DBNull.Value Then
                        If drowChild("DocStatusDescrip") = "Approved" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Approved)</strong></span>"
                        Else
                            s3 &= "<span style=""color:red; padding-left: 3px""><strong>(Not Approved)</strong></span>"
                        End If
                    End If


                    If Not drowChild("OverRide") Is System.DBNull.Value Then
                        If drowChild("OverRide") = "True" Then
                            s3 &= "<span style=""color:black; padding-left: 3px""><strong>(Overriden)</strong></span>"
                        End If
                    End If

                    s3 &= "</span></ul></td></tr>"
                End If
            Next
            s3 &= "<tr height=10><td>&nbsp;</td></tr>"
            forCounter = 1
        Next
        s3 &= "</table>"

        If forCounter = 1 Then
            Return s3.ToString()
        Else
            Return "None"
        End If
    End Function
    Public Function UpdateLeadGroups(ByVal LeadId As String, ByVal user As String, ByVal selectedLeadGroups() As String) As Integer
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   First we have to delete all existing selections
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("DELETE FROM adLeadByLeadGroups WHERE LeadId = ?")
        End With

        '   delete all selected items
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        '   Insert one record per each Item in the Selected Group
        Dim i As Integer
        For i = 0 To selectedLeadGroups.Length - 1

            '   build query
            With sb
                .Append("INSERT INTO adLeadByLeadGroups(LeadGrpLeadId, LeadId,LeadGrpId, ModDate, ModUser) ")
                .Append("VALUES(?,?,?,?,?)")
            End With

            '   add parameters
            db.AddParameter("@LeadGrpLeadId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@LeadgrpId", DirectCast(selectedLeadGroups.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        Next

        'Close Connection
        db.CloseConnection()

    End Function
    Public Function UpdateLeadGroupsAndStudent(ByVal LeadId As String, ByVal user As String, ByVal selectedLeadGroups() As String, ByVal StudentId As String) As Integer
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   First we have to delete all existing selections
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("DELETE FROM adLeadByLeadGroups WHERE StudentId = ?")
        End With

        '   delete all selected items
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        '   Insert one record per each Item in the Selected Group
        Dim i As Integer
        For i = 0 To selectedLeadGroups.Length - 1

            '   build query
            With sb
                .Append("INSERT INTO adLeadByLeadGroups(LeadGrpLeadId, StudentId,LeadGrpId, ModDate, ModUser,LeadId) ")
                .Append("VALUES(?,?,?,?,?,?)")
            End With

            '   add parameters
            db.AddParameter("@LeadGrpLeadId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@LeadgrpId", DirectCast(selectedLeadGroups.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            If Not Trim(LeadId) = Guid.Empty.ToString Then
                db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@LeadId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        Next

        'Close Connection
        db.CloseConnection()

    End Function
    Public Function GetLeadgroupsSelected(ByVal LeadId As String) As DataSet

        '   get the connection to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append(" select LeadGrpLeadId,LeadId,LeadgrpId from adLeadByLeadGroups where LeadId=? ")
        End With

        ' Add the parameter
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return the dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetLeadGroupCount(ByVal LeadId As String) As Integer
        '   get the connection to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder



        '   build the sql query
        With sb
            .Append("SELECT Count(*) ")
            .Append("FROM   adLeadByLeadGroups ")
            .Append("WHERE  LeadId = ? ")
        End With

        ' Add the parameter
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim LeadgrpCount As Integer

        '   Return the dataset
        LeadgrpCount = db.RunParamSQLScalar(sb.ToString)
        Return LeadgrpCount
    End Function
    Public Function GetsReqsByLeadGrp(ByVal LeadGrpId As String, ByVal ReqGrpId As String, ByVal strCurrentDate As Date, Optional ByVal campusid As String = "") As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusid)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        '   build the sql query
        With sb
            .Append("  select  Distinct ")
            .Append("  adReqId,  ")
            .Append("  Descrip,   ")
            .Append("  StartDate, ")
            .Append("  EndDate,  ")
            .Append("  Required, ")
            .Append("  ReqTypeDescrip, ")
            .Append("  ReqSelected,CampGrpId ")
            .Append(" from ")
            .Append(" ( ")
            .Append("  select Distinct ")
            .Append("  adReqId,  ")
            .Append("  Descrip,   ")
            .Append("   StartDate, ")
            .Append("   EndDate,  ")
            '.Append("  Case when Required=1 then 'Yes' else 'No' end as Required, ")
            .Append("  Required, ")
            .Append("  ReqTypeDescrip, ")
            .Append("  Case when ReqGrpDefCount >=1 then 'True' else 'False' end as ReqSelected,CampGrpId ")
            .Append("         from ")
            .Append("   (   ")
            .Append("        select  Distinct ")
            .Append("            t1.adReqId, ")
            .Append("            t1.Descrip, ")
            .Append("           '" & strCurrentDate & "' as CurrentDate, ")
            .Append("           t2.StartDate,  ")
            .Append("          t2.EndDate, ")
            '.Append("         t3.IsRequired as Required, ")
            .Append("    (select Distinct Top 1 IsRequired from adReqGrpDef where adReqId=t1.adReqId and  ReqGrpId='" & ReqGrpId & "' and LeadgrpId='" & LeadGrpId & "') as Required, ")
            .Append("    t4.Descrip as ReqTypeDescrip, ")
            .Append("    (select count(*) from adReqGrpDef where adReqId=t1.adReqId and  ReqGrpId='" & ReqGrpId & "' and LeadgrpId='" & LeadGrpId & "' ) as ReqGrpDefCount,t1.CampGrpId ")
            .Append("     from ")
            .Append("          adReqs t1, ")
            .Append("          adReqsEffectiveDates t2, ")
            .Append("         adReqLeadGroups t3, ")
            .Append("    adReqTypes t4 ")
            .Append("   where ")
            .Append("         t1.adReqId = t2.adReqId and ")
            .Append("         t1.adreqTypeId in (1,3,4,5,6,7) and  ")
            .Append("         t2.adReqEffectiveDateId = t3.adReqEffectiveDateId and ")
            .Append("         t3.LeadGrpId ='" & LeadGrpId & "' and  ")
            .Append("   t1.adReqTypeId = t4.adReqTypeId And t2.MandatoryRequirement <> 1 and t3.IsRequired=0 AND t1.ReqForEnrollment=1 ")
            .Append("   )  ")
            .Append("   R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	 ")
            .Append(" ) R2 ")
            If Not strCampGrpId = "" Then
                .Append("  where R2.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" order by Required desc,Descrip asc ")
        End With

        '   Return the dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetsMandatoryRequirementsByLeadGrp(ByVal strCurrentDate As Date) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("  select  ")
            .Append("  adReqId,  ")
            .Append("  Descrip,   ")
            .Append("   StartDate, ")
            .Append("   EndDate,  ")
            .Append("  Case when Required=1 then 'Yes' else 'No' end as Required, ")
            .Append("  ReqTypeDescrip, ")
            .Append("  'True' as ReqSelected ")
            .Append("         from ")
            .Append("   (   ")
            .Append("        select  ")
            .Append("            t1.adReqId, ")
            .Append("            t1.Descrip, ")
            .Append("           '" & strCurrentDate & "' as CurrentDate, ")
            .Append("           t2.StartDate,  ")
            .Append("          t2.EndDate, ")
            .Append("         1 as Required, ")
            .Append("    t3.Descrip as ReqTypeDescrip ")
            .Append("     from ")
            .Append("          adReqs t1, ")
            .Append("          adReqsEffectiveDates t2,adReqTypes t3 ")
            .Append("   where ")
            .Append("         t1.adReqId = t2.adReqId and ")
            .Append("         t1.adreqTypeId in (1,3) and t1.adReqTypeId = t3.adReqTypeId and t2.MandatoryRequirement = 1 ")
            .Append("   )  ")
            .Append("   R1 where R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)	 ")

        End With


        '   Return the dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetRequirementGroupByLeadGroups(ByVal ReqGrpId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append(" select Distinct t1.LeadGrpId,t1.Descrip from ")
                .Append(" adLeadGroups t1,adReqGrpDef t2,adLeadGrpReqGroups t3, syStatuses t4 where ")
                .Append(" t1.LeadGrpId = t2.LeadGrpId and t2.LeadGrpId = t3.LeadGrpId and t2.ReqGrpId = t3.ReqGrpId and t2.ReqGrpId='" & ReqGrpId & "' and ")
                .Append(" t3.StatusId = t4.StatusId")
                .Append(" order by t1.Descrip ")
            End With
            Return db.RunParamSQLDataSet(sb.ToString)
        Finally
        End Try
    End Function
    Public Function GetRequirementGroupByLeadGroups(ByVal ReqGrpId As String, ByVal Status As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append(" select Distinct t1.LeadGrpId,t1.Descrip, t3.StatusId,(CASE t4.Status WHEN 'Active' THEN 1 ELSE 0 END) As Status  from ")
                .Append(" adLeadGroups t1,adReqGrpDef t2,adLeadGrpReqGroups t3, syStatuses t4 where ")
                .Append(" t1.LeadGrpId = t2.LeadGrpId and t2.LeadGrpId = t3.LeadGrpId and t2.ReqGrpId = t3.ReqGrpId and t2.ReqGrpId='" & ReqGrpId & "' and ")
                .Append(" t3.StatusId = t4.StatusId")
                If Status = "Active" Then
                    .Append(" and t4.Status = 'Active' ")
                ElseIf Status = "Inactive" Then
                    .Append(" and t4.Status = 'Inactive' ")
                End If
                .Append(" order by t1.Descrip ")
            End With
            Return db.RunParamSQLDataSet(sb.ToString)
        Finally
        End Try
    End Function

    Public Function InsertReqGrpDef(ByVal ReqGrpId As String, ByVal minNumReqs As Integer, ByVal LeadGrpId As String, ByVal selectedRequirements() As String, ByVal User As String, ByVal selectedMandatory() As Boolean, ByVal StatusId As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            Dim intReqGrpLeadGrpRelation As Integer = 0

            With sb
                .Append("Select Count(*) from adLeadGrpReqGroups A, syStatuses B where A.StatusId = B.StatusId and B.Status= 'Active' and A.ReqGrpId=? and A.LeadGrpId=? ")
            End With
            db.AddParameter("@ReqGrpId", ReqGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@LeadGrpId", LeadGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Try
                intReqGrpLeadGrpRelation = db.RunParamSQLScalar(sb.ToString)
            Catch ex As System.Exception
                intReqGrpLeadGrpRelation = 0
            Finally
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            End Try

            If intReqGrpLeadGrpRelation = 0 Then
                With sb
                    .Append(" INSERT adLeadGrpReqGroups( ")
                    .Append(" LeadGrpReqGrpId,ReqGrpId,LeadGrpId,NumReqs,ModUser,ModDate, StatusId) ")
                    .Append(" VALUES (?,?,?,?,?,?,?) ")
                End With

                '   add parameters values to the query

                '   ReqLeadGrpId
                db.AddParameter("@LeadGrpReqGrpId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   ReqId
                db.AddParameter("@ReqGrpId", ReqGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                '   LeadGrpId
                db.AddParameter("@LeadGrpId", LeadGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                '   IsRequired
                db.AddParameter("@NumReqs", minNumReqs, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                '   ModUser
                db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                '   ModDate
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   StatusId
                db.AddParameter("@StatusId", StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                Try
                    '   execute the query
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As System.Exception
                    Return "Unable to add minimum requirements to the requirement group for the selected lead group"
                    Exit Function
                End Try
            Else
                With sb
                    .Append(" Update adLeadGrpReqGroups ")
                    .Append(" set NumReqs=?,ModUser=?,ModDate=?, StatusId=? where ReqGrpId=? and LeadGrpId=? ")
                End With

                '   IsRequired
                db.AddParameter("@NumReqs", minNumReqs, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                '   ModUser
                db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                '   ModDate
                Dim now As Date = Date.Now
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                '   StatusId
                db.AddParameter("@StatusId", StatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                '   ReqGrpId
                db.AddParameter("@ReqGrpId", ReqGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                '   LeadGrpId
                db.AddParameter("@LeadGrpId", LeadGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                Try
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As System.Exception
                    Return "Unable to update minimum requirements set for the requirement group and selected lead group"
                    Exit Function
                End Try
            End If

            Dim i As Integer
            For i = 0 To selectedRequirements.Length - 1
                With sb
                    .Append(" Delete from adReqGrpDef where ReqGrpId=? and adReqId=? and LeadGrpId=? ")
                End With
                db.AddParameter("@ReqGrpId", ReqGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@adReqId", DirectCast(selectedRequirements.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@LeadGrpId", LeadGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                Try
                    db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Catch ex As System.Exception
                Finally
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)
                End Try

                '   build query
                With sb
                    .Append("INSERT INTO adReqGrpDef(ReqGrpDefId,ReqGrpId,adReqId,LeadGrpId,IsRequired,ModDate, ModUser) ")
                    .Append("VALUES(?,?,?,?,?,?,?)")
                End With

                '   add parameters
                db.AddParameter("@ReqGrpDefId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ReqGrpId", ReqGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@adReqId", DirectCast(selectedRequirements.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@LeadGrpId", LeadGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@IsRequired", DirectCast(selectedMandatory.GetValue(i), Boolean), DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute query

                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Next

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            '   Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function UpdateReqGrpDef(ByVal ReqGrpId As String, ByVal minNumReqs As Integer, ByVal LeadGrpId As String, ByVal selectedRequirements() As String, ByVal User As String, ByVal selectedMandatory() As Boolean, ByVal statusId As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append(" update adLeadGrpReqGroups set NumReqs=?,LeadGrpId=?,ModUser=?,ModDate=?, StatusID=? where ReqGrpId=? and LeadGrpId=? ")
            End With

            '   add parameters values to the query

            '   ReqLeadGrpId
            db.AddParameter("@NumReqs", minNumReqs, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            '   LeadGrpId
            db.AddParameter("@LeadGrpId", LeadGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", statusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   adReqGrpId
            db.AddParameter("@ReqGrpId", ReqGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   IsRequired
            db.AddParameter("@LeadgrpId", LeadGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            db.ClearParameters()
            sb.Remove(0, sb.Length)


            If selectedRequirements.Length >= 1 Then
                With sb
                    .Append(" delete from adReqGrpDef where ReqGrpId=? and LeadGrpId=? ")
                End With
                db.AddParameter("@ReqGrpId", ReqGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@LeadGrpId", LeadGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.RunParamSQLDataSet(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            End If

            Dim i As Integer
            For i = 0 To selectedRequirements.Length - 1
                '   build query
                With sb
                    .Append("INSERT INTO adReqGrpDef(ReqGrpDefId,ReqGrpId,adReqId,LeadGrpId,IsRequired,ModDate, ModUser) ")
                    .Append("VALUES(?,?,?,?,?,?,?)")
                End With

                '   add parameters
                db.AddParameter("@ReqGrpDefId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ReqGrpId", ReqGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@adReqId", DirectCast(selectedRequirements.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@LeadGrpId", LeadGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@IsRequired", DirectCast(selectedMandatory.GetValue(i), Boolean), DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                '   execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Next

            '   return without errors
            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            '   Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetLeadgroupsSelectedByStudent(ByVal StudentId As String) As DataSet

        '   get the connection to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append(" select LeadGrpLeadId,LeadId,LeadgrpId from adLeadByLeadGroups where StudentId=? ")
        End With

        ' Add the parameter
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return the dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetLeadGroupCountByStudent(ByVal StudentId As String) As Integer
        '   get the connection to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder



        '   build the sql query
        With sb
            .Append("SELECT Count(*) ")
            .Append("FROM   adLeadByLeadGroups ")
            .Append("WHERE  StudentId = ? ")
        End With

        ' Add the parameter
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim LeadgrpCount As Integer

        '   Return the dataset
        LeadgrpCount = db.RunParamSQLScalar(sb.ToString)
        Return LeadgrpCount
    End Function
    Public Function GetStudentListByAdReqs(ByVal campusId As String, ByVal prgVerId As String,
                                            ByVal studentGrpId As String,
                                            ByVal shiftId As String,
                                            ByVal statusCodeId As String) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '' Dim strStuId As String

        With sb
            '   Student list
            .Append("SELECT ")
            .Append("       E.StuEnrollId,S.StudentId,S.LastName,S.FirstName,S.MiddleName,S.SSN,E.PrgVerId,E.StartDate,E.ExpGradDate,")
            .Append("       (SELECT PrgVerDescrip FROM arPrgVersions WHERE E.PrgVerId=PrgVerId) AS PrgVerDescrip,")
            .Append("       (SELECT PrgVerCode FROM arPrgVersions WHERE E.PrgVerId=PrgVerId) AS PrgVerCode,")
            .Append("       E.StatusCodeId,(SELECT StatusCodeDescrip FROM syStatusCodes WHERE E.StatusCodeId=StatusCodeId) AS StatusCodeDescrip,")
            .Append("       E.ShiftId,(SELECT ShiftDescrip FROM arShifts WHERE ShiftId=E.ShiftId) AS ShiftDescrip,")
            .Append("       L.LeadGrpId AS StudentGrpId,E.EdLvlId,(SELECT EdLvlDescrip FROM adEdLvls WHERE EdLvlId=E.EdLvlId) AS EdLvlDescrip ")
            .Append("FROM   arStudent S,arStuEnrollments E,adLeadByLeadGroups L ")
            .Append("WHERE  S.StudentId=E.StudentId AND E.CampusId=? ")         'AND E.PrgVerId=? ")
            .Append("       AND EXISTS (SELECT *  ")
            .Append("                   FROM arPrgVersions PV,arPrograms P  ")
            .Append("                   WHERE PV.ProgId=P.ProgId ")
            .Append("		            AND P.ProgId=? ")
            .Append("		            AND PV.PrgVerId=E.PrgVerId ) ")
            .Append("       AND L.StuEnrollId=E.StuEnrollId AND L.LeadGrpId=? ")
            If shiftId <> System.Guid.Empty.ToString Then
                .Append("       AND E.ShiftId=? ")
            End If
            If statusCodeId <> System.Guid.Empty.ToString Then
                .Append("       AND E.StatusCodeId=? ")
            End If
            .Append("ORDER BY PrgVerDescrip,S.LastName,S.FirstName,S.MiddleName")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ' Add parameters
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StudentGrpId", studentGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If shiftId <> System.Guid.Empty.ToString Then
            db.AddParameter("@ShiftId", shiftId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If statusCodeId <> System.Guid.Empty.ToString Then
            db.AddParameter("@StatusCodeId", statusCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            ds.Tables(0).Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function
    Public Function GetRequirementsByProgramVersionAndMandatories(ByVal campusId As String, ByVal prgVerId As String, ByVal studentGrpId As String) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            '   Requirements (Documents and Tests) for a specific Program Version and Student Group
            .Append("SELECT ")
            .Append("       T.adReqId,R.Descrip AS Descrip,R.Code,RT.Descrip AS ReqType ")
            .Append("FROM   adPrgVerTestDetails T,adReqs R,adReqTypes RT,syStatuses S,adReqsEffectiveDates E,adReqLeadGroups L ")
            .Append("WHERE  T.adReqId=R.adReqId AND R.adReqTypeId=RT.adReqTypeId ")
            .Append("       AND R.StatusId=S.StatusId AND S.Status='Active' ")
            '.Append("       AND T.PrgVerId=? ")
            .Append("       AND EXISTS (SELECT *  ")
            .Append("                   FROM arPrgVersions PV,arPrograms P  ")
            .Append("                   WHERE PV.ProgId=P.ProgId ")
            .Append("		            AND P.ProgId=? ")
            .Append("		            AND PV.PrgVerId=T.PrgVerId ) ")
            .Append("       AND R.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps,syCampuses WHERE syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=?) AND R.adReqId=E.adReqId ")
            .Append("       AND E.StartDate<='" & Date.Now & "' AND (E.EndDate IS NULL OR E.EndDate>='" & Date.Now & "') ")
            .Append("       AND E.adReqEffectiveDateId=L.adReqEffectiveDateId ")
            .Append("       AND L.LeadGrpId=? ")
            .Append("UNION ")
            '   Mandatory Requirements (Documents and Tests)
            .Append("SELECT ")
            .Append("       T.adReqId,R.Descrip AS Descrip,R.Code,RT.Descrip AS ReqType ")
            .Append("FROM   adReqs R,adReqsEffectiveDates T,adReqTypes RT,syStatuses S ")
            .Append("WHERE  R.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps,syCampuses WHERE syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=?) AND T.adReqId=R.adReqId AND R.adReqTypeId=RT.adReqTypeId ")
            .Append("       AND R.StatusId=S.StatusId ")
            .Append("       AND S.Status='Active' AND T.MandatoryRequirement=1")
            .Append("       AND T.StartDate<='" & Date.Now & "' AND (T.EndDate IS NULL OR T.EndDate>='" & Date.Now & "') ")
            .Append("UNION ")
            '   Required Requirements for selected Student Group
            .Append("SELECT ")
            .Append("       R.adReqId,R.Descrip,R.Code,RT.Descrip AS ReqType ")
            .Append("FROM   adReqs R, adReqTypes RT, syStatuses S, adReqsEffectiveDates E, adReqLeadGroups L ")
            .Append("WHERE  R.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps,syCampuses WHERE syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=?) AND R.adReqTypeId=RT.adReqTypeId ")    'AND RT.Descrip LIKE 'Document%'
            .Append("       AND R.StatusId=S.StatusId AND S.Status='Active' AND R.adReqId=E.adReqId")
            .Append("       AND E.StartDate<='" & Date.Now & "' AND (E.EndDate IS NULL OR E.EndDate>='" & Date.Now & "') ")
            .Append("       AND E.adReqEffectiveDateId=L.adReqEffectiveDateId ")
            .Append("       AND L.LeadGrpId=? AND L.IsRequired=1 ")
            .Append("ORDER BY R.Descrip")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ' Add parameters
        db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampGrpId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@LeadGrpId", studentGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampGrpId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampGrpId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@LeadGrpId", studentGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function
    Public Function GetReqGroupsByProgramVersion(ByVal campusId As String, ByVal prgVerId As String, ByVal studentGrpId As String) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            '   Requirement Groups for a specific Program Version and Student Group
            .Append("SELECT DISTINCT ")
            .Append("       T.ReqGrpId,RG.Descrip,RG.Code,L.NumReqs ")
            .Append("FROM   adPrgVerTestDetails T,adReqGroups RG,syStatuses S,adLeadGrpReqGroups L ")
            .Append("WHERE  ")
            .Append("       EXISTS (SELECT *  ")
            .Append("               FROM arPrgVersions PV,arPrograms P  ")
            .Append("               WHERE PV.ProgId=P.ProgId ")
            .Append("		        AND P.ProgId=? ")
            .Append("		        AND PV.PrgVerId=T.PrgVerId ) ")
            .Append("       AND RG.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps,syCampuses WHERE syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=?) ")
            .Append("       AND T.ReqGrpId=RG.ReqGrpId AND RG.StatusId=S.StatusId AND S.Status='Active' ")
            .Append("       AND RG.ReqGrpId=L.ReqGrpId AND L.LeadGrpId=? ")
            .Append("ORDER BY Descrip")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ' Add parameters
        db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampGrpId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@LeadGrpId", studentGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function
    Public Function GetStudentRequirements(ByVal campusId As String, ByVal prgVerId As String,
                                            ByVal studentGrpId As String,
                                            ByVal shiftId As String,
                                            ByVal statusCodeId As String) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            '   Student Requirements (Documents)
            .Append("SELECT ")
            .Append("       D.StudentId,R.adReqId,R.Descrip AS ReqDescrip,D.ReceiveDate,")
            .Append("       D.DocStatusId,DS.SysDocStatusId,SDS.DocStatusDescrip ")
            .Append("FROM   plStudentDocs D,adReqs R,syDocStatuses DS,sySysDocStatuses SDS ")
            .Append("WHERE  D.DocumentId=R.adReqId ")
            .Append("       AND R.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps,syCampuses WHERE syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=?) ")
            .Append("       AND D.DocStatusId=DS.DocStatusId ")
            .Append("       AND DS.SysDocStatusId=SDS.SysDocStatusId ")
            .Append("       AND EXISTS (SELECT * FROM (")
            .Append("                           SELECT T.adReqId,R.Descrip AS Descrip,R.Code ")
            .Append("                           FROM   adPrgVerTestDetails T,adReqs R,adReqTypes RT,syStatuses S,adReqsEffectiveDates E,adReqLeadGroups L ")
            .Append("                           WHERE  T.adReqId=R.adReqId AND R.adReqTypeId=RT.adReqTypeId ")      'AND RT.Descrip LIKE 'Document%' 
            .Append("                           AND R.StatusId=S.StatusId AND S.Status='Active' ")
            '.Append("                           AND T.PrgVerId=? ")
            .Append("                           AND EXISTS (SELECT *  ")
            .Append("                                       FROM arPrgVersions PV,arPrograms P  ")
            .Append("                                       WHERE PV.ProgId=P.ProgId ")
            .Append("		                                AND P.ProgId=? ")
            .Append("		                                AND PV.PrgVerId=T.PrgVerId ) ")
            .Append("                           AND R.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps,syCampuses WHERE syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=?) AND R.adReqId=E.adReqId ")
            .Append("                           AND E.StartDate<='" & Date.Now & "' AND (E.EndDate IS NULL OR E.EndDate>='" & Date.Now & "') ")
            .Append("                           AND E.adReqEffectiveDateId=L.adReqEffectiveDateId ")
            .Append("                           AND L.LeadGrpId=? ")
            .Append("                           UNION ")
            .Append("                           SELECT T.adReqId,R.Descrip AS Descrip,R.Code ")
            .Append("                           FROM   adReqs R,adReqsEffectiveDates T,adReqTypes RT,syStatuses S ")
            .Append("                           WHERE  R.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps,syCampuses WHERE syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=?) AND T.adReqId=R.adReqId AND R.adReqTypeId=RT.adReqTypeId ")
            .Append("                           AND R.StatusId=S.StatusId ")        'AND RT.Descrip LIKE 'Document%' 
            .Append("                           AND S.Status='Active' AND T.MandatoryRequirement=1")
            .Append("                           AND T.StartDate<='" & Date.Now & "' AND (T.EndDate IS NULL OR T.EndDate>='" & Date.Now & "') ")
            .Append("                           UNION ")
            .Append("                           SELECT R.adReqId,R.Descrip,R.Code ")
            .Append("                           FROM   adReqs R, adReqTypes RT, syStatuses S, adReqsEffectiveDates E, adReqLeadGroups L ")
            .Append("                           WHERE  R.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps,syCampuses WHERE syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=?) AND R.adReqTypeId=RT.adReqTypeId ") 'AND RT.Descrip LIKE 'Document%'
            .Append("                           AND R.StatusId=S.StatusId AND S.Status='Active' AND R.adReqId=E.adReqId")
            .Append("                           AND E.StartDate<='" & Date.Now & "' AND (E.EndDate IS NULL OR E.EndDate>='" & Date.Now & "') ")
            .Append("                           AND E.adReqEffectiveDateId=L.adReqEffectiveDateId ")
            .Append("                           AND L.LeadGrpId=? AND L.IsRequired=1) P ")
            .Append("                   WHERE P.adReqId=D.DocumentId) ")
            .Append("       AND EXISTS (SELECT * ")
            .Append("                   FROM    arStudent S,arStuEnrollments E,adLeadByLeadGroups L ")
            .Append("                   WHERE  S.StudentId=E.StudentId AND L.StuEnrollId=E.StuEnrollId ")
            If shiftId <> System.Guid.Empty.ToString Then
                .Append("                   AND E.ShiftId=? ")
            End If
            If statusCodeId <> System.Guid.Empty.ToString Then
                .Append("                   AND E.StatusCodeId=? ")
            End If
            '.Append("                   AND E.PrgVerId=? ")
            .Append("                   AND EXISTS (SELECT *  ")
            .Append("                               FROM arPrgVersions PV,arPrograms P  ")
            .Append("                               WHERE PV.ProgId=P.ProgId ")
            .Append("		                        AND P.ProgId=? ")
            .Append("		                        AND PV.PrgVerId=E.PrgVerId ) ")
            .Append("                   AND E.CampusId=? AND L.LeadGrpId=? AND D.StudentId=S.StudentId)")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ' Add parameters
        db.AddParameter("@CampGrpId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampGrpId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@LeadGrpId", studentGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampGrpId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampGrpId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@LeadGrpId", studentGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If shiftId <> System.Guid.Empty.ToString Then
            db.AddParameter("@ShiftId", shiftId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If statusCodeId <> System.Guid.Empty.ToString Then
            db.AddParameter("@StatusCodeId", statusCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StudentGrpId", studentGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function
    Public Function GetStudentTestRequirements(ByVal campusId As String, ByVal prgVerId As String,
                                        ByVal studentGrpId As String,
                                        ByVal shiftId As String,
                                        ByVal statusCodeId As String) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            '   Student Requirements (Documents)
            .Append("SELECT ")
            .Append("       E.StudentId,R.adReqId,R.Descrip AS ReqDescrip,")
            .Append("		T.ActualScore,T.MinScore,T.Pass ")
            .Append("FROM   adLeadEntranceTest T,adReqs R,arStuEnrollments E,arStudent S,adLeadByLeadGroups L ")
            .Append("WHERE  T.EntrTestId=R.adReqId ")
            .Append("       AND R.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps,syCampuses WHERE syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=?)")
            .Append("       AND T.LeadId=E.LeadId AND E.StudentId=S.StudentId AND L.StuEnrollId=E.StuEnrollId")
            .Append("       AND E.CampusId=? AND L.LeadGrpId=?")
            If shiftId <> System.Guid.Empty.ToString Then
                .Append("                   AND E.ShiftId=? ")
            End If
            If statusCodeId <> System.Guid.Empty.ToString Then
                .Append("                   AND E.StatusCodeId=? ")
            End If
            .Append("       AND EXISTS (SELECT * ")
            .Append("                   FROM    arPrgVersions PV,arPrograms P ")
            .Append("                   WHERE   PV.ProgId= P.ProgId")
            .Append("			                AND P.ProgId=? AND PV.PrgVerId=E.PrgVerId)")
            .Append("       AND EXISTS (SELECT * FROM ( ")
            .Append("					            SELECT  T.adReqId,R.Descrip AS Descrip,R.Code ")
            .Append("                               FROM    adPrgVerTestDetails T,adReqs R,adReqTypes RT,syStatuses S,adReqsEffectiveDates E,adReqLeadGroups L ")
            .Append("                               WHERE   T.adReqId = R.adReqId And R.adReqTypeId = RT.adReqTypeId")
            .Append("					                    AND R.StatusId=S.StatusId AND S.Status='Active'")
            .Append("					                    AND EXISTS (SELECT * FROM arPrgVersions PV,arPrograms P")
            .Append("                                           WHERE PV.ProgId=P.ProgId AND P.ProgId=? AND PV.PrgVerId=T.PrgVerId)")
            .Append("								        AND R.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps,syCampuses WHERE syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=?)")
            .Append("								        AND R.adReqId=E.adReqId")
            .Append("								        AND E.StartDate<='" & Date.Now & "'")
            .Append("								        AND (E.EndDate IS NULL OR E.EndDate>='" & Date.Now & "')")
            .Append("								        AND E.adReqEffectiveDateId=L.adReqEffectiveDateId")
            .Append("								        AND L.LeadGrpId=?")
            .Append("                               UNION")
            .Append("								SELECT  T.adReqId,R.Descrip AS Descrip,R.Code")
            .Append("								FROM    adReqs R,adReqsEffectiveDates T,adReqTypes RT,syStatuses S  ")
            .Append("								WHERE   R.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps,syCampuses WHERE syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=?)")
            .Append("								        AND T.adReqId=R.adReqId AND R.adReqTypeId=RT.adReqTypeId")
            .Append("								        AND R.StatusId=S.StatusId")
            .Append("								        AND S.Status='Active' AND T.MandatoryRequirement=1")
            .Append("								        AND T.StartDate<='" & Date.Now & "'")
            .Append("								        AND (T.EndDate IS NULL OR T.EndDate>='" & Date.Now & "')")
            .Append("                               UNION")
            .Append("								SELECT  R.adReqId,R.Descrip,R.Code")
            .Append("								FROM    adReqs R, adReqTypes RT, syStatuses S, adReqsEffectiveDates E, adReqLeadGroups L")
            .Append("								WHERE   R.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps,syCampuses WHERE syCmpGrpCmps.CampusId=syCampuses.CampusId AND syCampuses.CampusId=?)")
            .Append("								        AND R.adReqTypeId=RT.adReqTypeId")
            .Append("								        AND R.StatusId=S.StatusId AND S.Status='Active' AND R.adReqId=E.adReqId")
            .Append("								        AND E.StartDate<='" & Date.Now & "'")
            .Append("								        AND (E.EndDate IS NULL OR E.EndDate>='" & Date.Now & "')")
            .Append("								        AND E.adReqEffectiveDateId=L.adReqEffectiveDateId ")
            .Append("								        AND L.LeadGrpId=? AND L.IsRequired=1) P")
            .Append("					WHERE P.adReqId=T.EntrTestId)")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ' Add parameters
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@LeadGrpId", studentGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If shiftId <> System.Guid.Empty.ToString Then
            db.AddParameter("@ShiftId", shiftId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If statusCodeId <> System.Guid.Empty.ToString Then
            db.AddParameter("@StatusCodeId", statusCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@LeadGrpId", studentGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@LeadGrpId", studentGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function
    Public Function AreReqGroupsMet(ByVal stuEnrollId As String, ByVal reqGrpId As String) As Boolean
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim result As Boolean

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            '   Student Requirement Groups (Documents)
            .Append("SELECT ")
            .Append("       t1.ReqGrpId,t2.Descrip,t3.NumReqs,( ")
            .Append("       SELECT COUNT(*) AS DocsAttempted ")
            .Append("       FROM (	SELECT 	A2.adReqId,? AS CurrentDate,A3.StartDate,A3.EndDate ")
            .Append("               FROM 	adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append("               WHERE   A1.adReqId=A2.adReqId ")
            .Append("                       AND A2.adReqId=A3.adReqId ")
            .Append("                       AND A3.adReqEffectiveDateId=A4.adReqEffectiveDateId ")
            .Append("                       AND A4.LeadGrpId IN (SELECT LeadGrpId from adLeadByLeadGroups L, arStuEnrollments E WHERE E.StuEnrollId=? AND E.StuEnrollId=L.StuEnrollId) ")
            .Append("                       AND ReqGrpId=t1.ReqGrpId and A2.adReqTypeId=3 ) R1, plStudentDocs R2 ")
            .Append("       WHERE   R1.adReqId = R2.DocumentId ")
            .Append("   	        AND R2.StudentId IN (SELECT StudentId from arStuEnrollments WHERE StuEnrollId=? ) ")
            .Append("   			AND R1.CurrentDate >= R1.StartDate ")
            .Append("   			AND (R1.CurrentDate <= R1.EndDate or R1.EndDate IS NULL)")
            .Append("   	) AS DocsAttempted ")
            .Append("FROM	adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3 ")
            .Append("WHERE  t1.ReqGrpId=? AND t1.ReqGrpId=t2.ReqGrpId")
            .Append("       AND t2.ReqGrpId=t3.ReqGrpId ")
            .Append("       AND t1.PrgVerId=(SELECT PrgVerId FROM arStuEnrollments WHERE StuEnrollId=?) ")
            .Append("       AND t3.LeadGrpId=(SELECT LeadGrpId FROM adLeadByLeadGroups L,arStuEnrollments E WHERE E.StuEnrollId=? AND E.StuEnrollId=L.StuEnrollId ) ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ' Add parameters
        db.AddParameter("@CurrentDate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ReqGrpId", reqGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)

        If ds.Tables.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                If Not dr.IsNull("NumReqs") Then
                    If Not dr.IsNull("DocsAttempted") Then
                        result = (dr("DocsAttempted") >= dr("NumReqs"))
                    End If
                Else
                    If Not dr.IsNull("DocsAttempted") Then
                        result = True
                    End If
                End If
            Next
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return result
    End Function
    Public Function GetAttemptedReqsFromReqGroupByStudentId(ByVal campusId As String, ByVal progId As String,
                                        ByVal studentGrpId As String,
                                        ByVal shiftId As String,
                                        ByVal statusCodeId As String,
                                        ByVal reqGrpId As String) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '' Dim result As Boolean

        With sb
            '   Fulfilled Requirement Groups (Documents and Tests)
            .Append("SELECT	DISTINCT ")
            .Append("	    E.StudentId,t1.ReqGrpId,t2.Descrip,t3.NumReqs ")
            .Append("FROM   adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3,arStuEnrollments E,adLeadByLeadGroups L ")
            .Append("WHERE  t1.ReqGrpId=? AND t1.ReqGrpId=t2.ReqGrpId")
            .Append("       AND t2.ReqGrpId=t3.ReqGrpId")
            .Append("       AND t1.PrgVerId IN (SELECT PrgVerId FROM arPrgVersions WHERE ProgId=?)")
            .Append("       AND t3.LeadGrpId=? AND t3.LeadGrpId=L.LeadGrpId")
            .Append("       AND L.StuEnrollId=E.StuEnrollId AND t1.PrgVerId=E.PrgVerId")
            .Append("       AND E.CampusId=?")
            .Append("       AND EXISTS (SELECT * FROM arPrgVersions PV,arPrograms P")
            .Append("                   WHERE PV.ProgId=P.ProgId")
            .Append("                   AND P.ProgId=? AND PV.PrgVerId=E.PrgVerId)")
            If Not (shiftId = System.Guid.Empty.ToString) Then
                .Append("       AND E.ShiftId=?")
            End If
            If Not (statusCodeId = System.Guid.Empty.ToString) Then
                .Append("       AND E.StatusCodeId=?")
            End If
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ' Add parameters
        db.AddParameter("@ReqGrpId", reqGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ProgId", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@LeadGrpId", studentGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ProgId", progId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If Not (shiftId = System.Guid.Empty.ToString) Then
            db.AddParameter("@ShiftId", shiftId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Not (statusCodeId = System.Guid.Empty.ToString) Then
            db.AddParameter("@StatusCodeId", statusCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        ds = db.RunParamSQLDataSet(sb.ToString)

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function
    Public Function GetDocTestFromReqGrpAttempted(ByVal studentGrpId As String,
                                    ByVal reqGrpId As String,
                                    ByVal studentIdList As String) As DataTable
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        '' Dim result As Boolean

        With sb
            '   Fulfilled Requirement Groups (Documents and Tests)
            .Append("SELECT	")
            .Append("       R3.StudentId,SUM(Attempted) AS ReqsFromRGAttempted ")
            .Append("FROM ( ")
            .Append("       SELECT R2.StudentId,COUNT(*) AS Attempted ")
            .Append("       FROM (SELECT DISTINCT")
            .Append("                   A2.adReqId,A2.Descrip,? AS CurrentDate,A3.StartDate,A3.EndDate")
            .Append("             FROM 	adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4")
            .Append("             WHERE A1.adReqId=A2.adReqId AND A2.adReqId=A3.adReqId")
            .Append("                   AND A3.adReqEffectiveDateId=A4.adReqEffectiveDateId")
            .Append("                   AND A4.LeadGrpId=? AND A1.ReqGrpId=? AND A2.adReqTypeId=3")
            .Append("             )R1, plStudentDocs R2 ")
            .Append("       WHERE R1.adReqId= R2.DocumentId")
            .Append("             AND R2.StudentId IN (" & studentIdList & ")")
            .Append("             AND R1.CurrentDate>=R1.StartDate")
            .Append("             AND (R1.CurrentDate<=R1.EndDate OR R1.EndDate IS NULL)")
            .Append("             AND R2.DocStatusId IN (SELECT DocStatusId FROM syDocStatuses D,sySysDocStatuses S WHERE D.SysDocStatusId=S.SysDocStatusId AND S.DocStatusDescrip='Approved')")
            .Append("       GROUP BY R2.StudentId,R2.DocumentId")
            .Append("       UNION ALL ")
            .Append("       SELECT EE.StudentId,COUNT(*) AS Attempted ")
            .Append("       FROM (SELECT DISTINCT")
            .Append("               	A2.adReqId,A2.Descrip,? AS CurrentDate,A3.StartDate,A3.EndDate")
            .Append("             FROM 	adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4")
            .Append("             WHERE A1.adReqId=A2.adReqId AND A2.adReqId=A3.adReqId")
            .Append("                   AND A3.adReqEffectiveDateId=A4.adReqEffectiveDateId")
            .Append("                   AND A4.LeadGrpId=? AND A1.ReqGrpId=?")
            .Append("                   AND A2.adReqTypeId=1")
            .Append("             ) R1, adLeadEntranceTest R2,arStuEnrollments EE")
            .Append("       WHERE R1.adReqId=R2.EntrTestId AND R2.LeadId=EE.LeadId")
            .Append("             AND EE.StudentId IN (" & studentIdList & ")")
            .Append("             AND R1.CurrentDate>=R1.StartDate")
            .Append("             AND (R1.CurrentDate<=R1.EndDate OR R1.EndDate IS NULL)")
            .Append("             AND R2.Pass=1")
            .Append("       GROUP BY EE.StudentId")
            .Append("       ) R3 ")
            .Append("GROUP BY StudentId")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.OpenConnection()

        ' Add parameters
        db.AddParameter("@currentDate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@LeadGrpId", studentGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ReqGrpId", reqGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@currentDate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@LeadGrpId", studentGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ReqGrpId", reqGrpId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)


        If ds.Tables(0).Rows.Count > 0 Then

            ds.Tables(0).TableName = "DocTestFromReqGrpAttempted"

            '   set primary key for DocTestFromReqGrpAttempted table
            Dim pk0(0) As DataColumn
            pk0(0) = ds.Tables("DocTestFromReqGrpAttempted").Columns("StudentId")
            ds.Tables("DocTestFromReqGrpAttempted").PrimaryKey = pk0
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds.Tables(0)
    End Function
    Public Function GetMissingItemsDropDownLists(ByVal campusId As String) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim sb As New StringBuilder

        db.OpenConnection()

        '   Get all Programs
        With sb
            .Append("SELECT ")
            .Append("       P.ProgId,P.ProgCode,P.ProgDescrip,P.StatusId, ")
            .Append(" case when (select ShiftDescrip from arShifts where shiftid=P.shiftid) is null  then ")
            .Append(" P.ProgDescrip ")
            .Append(" else ")
            .Append("P.ProgDescrip + ' (' + (select ShiftDescrip from arShifts where shiftid=P.shiftid) + ')'  ")
            .Append(" end as ShiftDescrip  ")
            .Append("FROM   arPrograms P, syStatuses ST ")
            .Append("WHERE  P.StatusId=ST.StatusId AND ST.Status='Active' ")
            .Append("ORDER BY P.ProgDescrip ")
        End With

        Dim da As New OleDbDataAdapter
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "ProgramDT")
        Catch ex As System.Exception

        End Try


        '   Get all Program Versions
        With sb
            .Append("SELECT     DISTINCT PrgVerId, PrgVerDescrip ")
            .Append("FROM       arPrgVersions,syStatuses ")
            .Append("WHERE      arPrgVersions.StatusId=syStatuses.StatusId and syStatuses.Status='Active' ")
            .Append("ORDER BY   PrgVerDescrip ")
        End With

        Dim da2 As New OleDbDataAdapter
        da2 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da2.Fill(ds, "ProgramVersionDT")
        Catch ex As System.Exception

        End Try


        sb.Remove(0, sb.Length)
        '   Get Shifts
        With sb
            .Append("SELECT     ShiftId, ShiftDescrip ")
            .Append("FROM       arShifts ")
            .Append("ORDER BY   ShiftDescrip ")
        End With

        Dim da4 As New OleDbDataAdapter
        da4 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da4.Fill(ds, "ShiftDT")
        Catch ex As System.Exception

        End Try



        sb.Remove(0, sb.Length)
        '   Get Student Groups
        With sb
            .Append("SELECT     LG.LeadGrpId,LG.StatusId,LG.Descrip ")
            .Append("FROM       adLeadGroups LG, syStatuses ST ")
            .Append("WHERE      LG.StatusId=ST.StatusId AND ST.Status='Active' ")
            .Append("ORDER BY   Descrip ")
        End With

        Dim da5 As New OleDbDataAdapter
        da5 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da5.Fill(ds, "StudentGroupDT")
        Catch ex As System.Exception

        End Try


        sb.Remove(0, sb.Length)
        '   Get Status Codes
        With sb
            .Append("SELECT DISTINCT")
            .Append("           A.StatusCodeID AS StatusCodeID,A.StatusCodeDescrip AS StatusCodeDescrip ")
            .Append("FROM       syStatusCodes A,sySysStatus B,syStatuses C,syStatusLevels D ")
            .Append("WHERE      A.sysStatusID=B.sysStatusID")
            .Append("           AND A.StatusID=C.StatusID")
            .Append("           AND B.StatusLevelId=D.StatusLevelId")
            .Append("           AND C.Status='Active'")
            .Append("           AND D.StatusLevelId=2 ")
            .Append("ORDER BY   A.StatusCodeDescrip")
        End With

        Dim da6 As New OleDbDataAdapter
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "StatusCodeDT")
        Catch ex As System.Exception

        End Try

        ''Added by Saraswathi lakshmanan on Jan 27 2011
        ''To fix rally case De 4941 Add req type on the missing items page.

        sb.Remove(0, sb.Length)
        '   Get Status Codes
        With sb
            .Append("SELECT TypeOfReq    FROM dbo.SyTypeofRequirement")

        End With

        Dim da7 As New OleDbDataAdapter
        da7 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da7.Fill(ds, "ReqTypesDT")
        Catch ex As System.Exception

        End Try

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds
    End Function
    Public Function InsertStudentEntranceTest(ByVal StudentId As String, ByVal selectedEntranceTest() As String, ByVal selectedRequired() As String, ByVal selectedPass() As String, ByVal selectedTestTaken() As String, ByVal selectedActualScore() As String, ByVal selectedMinScore() As String, ByVal selectedComments() As String, ByVal selectedOverRide() As String, ByVal User As String) As Integer
        '   connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb1 As New StringBuilder
        With sb1
            .Append("Delete from adLeadEntranceTest where StudentId = ? ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'execute query
        db.RunParamSQLExecuteNoneQuery(sb1.ToString)
        db.ClearParameters()
        sb1.Remove(0, sb1.Length)


        Dim sb6 As New StringBuilder
        With sb6
            'With subqueries
            .Append(" Select LeadId from arStuEnrollments where StudentId=? ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb6.ToString)
        Dim strLeadId As String
        While dr.Read()
            If Not (dr("LeadId") Is System.DBNull.Value) Then strLeadId = CType(dr("LeadId"), Guid).ToString Else strLeadId = ""
        End While

        If Not dr.IsClosed Then dr.Close()

        db.ClearParameters()
        sb6.Remove(0, sb6.Length)

        '   Insert one record per each Item in the Selected Group
        ''  Dim i, j, k As Integer  y, z 
        Dim x As Integer
        Dim intRequired As Integer
        ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
        ''Dim intMinScore, intActualScore, intPass, intOverRide As Integer
        Dim intPass, intOverRide As Integer
        Dim intMinScore, intActualScore As Decimal
        ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
        Try
            Dim intLeadCount As Integer
            ''   Dim strRequiredField As String

            intLeadCount = selectedEntranceTest.Length
            While x < intLeadCount
                With sb
                    ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                    '.Append("INSERT INTO adLeadEntranceTest(LeadEntrTestId,LeadId,StudentId,EntrTestId,TestTaken,ActualScore,MinScore,Comments,Required,Pass,ModUser,ModDate) ")
                    '.Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?)")
                    .Append("INSERT INTO adLeadEntranceTest(LeadEntrTestId,LeadId,StudentId,EntrTestId,TestTaken,ActualScore,Comments,Required,Pass,ModUser,ModDate) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?)")
                    ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                End With

                If DirectCast(selectedRequired.GetValue(x), String) = "Yes" Then
                    intRequired = 1
                Else
                    intRequired = 0
                End If

                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                'If Not DirectCast(selectedActualScore.GetValue(x), String) = "" Then
                '    intActualScore = CInt(DirectCast(selectedActualScore.GetValue(x), String))
                'Else
                '    intActualScore = 0
                'End If
                If Not DirectCast(selectedActualScore.GetValue(x), String) = "" Then
                    intActualScore = CDbl(DirectCast(selectedActualScore.GetValue(x), String))
                Else
                    intActualScore = 0.0
                End If
                ''intMinScore = CInt(DirectCast(selectedMinScore.GetValue(x), String))
                intMinScore = CDbl(DirectCast(selectedMinScore.GetValue(x), String))
                ''New Code Added By Vijay Ramteke On September 20, 2010 For Mantis Id 17767


                If intActualScore >= intMinScore Then
                    intPass = 1
                ElseIf intActualScore < intMinScore Then
                    intPass = 0
                Else
                    intPass = 2
                End If

                If DirectCast(selectedOverRide.GetValue(x), String) = "Yes" Then
                    intOverRide = 1
                Else
                    intOverRide = 0
                End If

                db.AddParameter("@LeadEntrTestId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If Not strLeadId = "" Then
                    db.AddParameter("@LeadId", strLeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@LeadId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@EntrTestId", DirectCast(selectedEntranceTest.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@TestTaken", DirectCast(selectedTestTaken.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If Not DirectCast(selectedActualScore.GetValue(x), String) = "" Then
                    db.AddParameter("@ActualScore", intActualScore, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else 
                    db.AddParameter("@ActualScore", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                
                '' New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                ''db.AddParameter("@MinScore", DirectCast(selectedMinScore.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                '' New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                db.AddParameter("@Comments", DirectCast(selectedComments.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@IsRequired", intRequired, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                If intPass = 2 Then
                    db.AddParameter("@IsPass", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                Else
                    db.AddParameter("@IsPass", intPass, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                End If
                'db.AddParameter("@IsOverRide", intOverRide, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                'execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                With sb
                    .Append("delete from adEntrTestOverRide where StudentId=? and EntrTestId=? ")
                End With
                db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@EntrTestId", DirectCast(selectedEntranceTest.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)


                With sb
                    .Append(" Insert into adEntrTestOverRide(EntrTestOverRideId,LeadId,StudentId,EntrTestId,OverRide,ModUser,ModDate) ")
                    .Append(" values(?,?,?,?,?,?,?) ")
                End With
                db.AddParameter("@LeadEntrTestId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If Not strLeadId = "" Then
                    db.AddParameter("@LeadId", strLeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@LeadId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@EntrTestId", DirectCast(selectedEntranceTest.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@OverRide", intOverRide, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                'execute query
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                x = x + 1
            End While
            Return 0
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function DeleteLeadGroups(ByVal RefEffectiveDateId As String) As Integer

        '   connect to the database
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb1 As New StringBuilder
        With sb1
            .Append("Delete from adReqLeadGroups where adReqEffectiveDateId = ? ")
        End With
        db.AddParameter("@adReqEffectiveDateId", RefEffectiveDateId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'execute query
        db.RunParamSQLExecuteNoneQuery(sb1.ToString)
        db.ClearParameters()
        sb1.Remove(0, sb1.Length)
        Return 0
    End Function
    Public Function GetAllOutofSchoolStatus(ByVal CampusId As String, Optional ByVal strStatus As String = "Active") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select Distinct t1.StatusCodeId,t1.StatusCodeDescrip " + vbCrLf)
            .Append(" from syStatusCodes t1,sySysStatus t2,arStuEnrollments t3,syStatuses t4 " + vbCrLf)
            .Append(" where t1.SysStatusId=t2.SysStatusId and t1.StatusCodeId=t3.StatusCodeId and t2.InSchool = 0 and t1.StatusId=t4.StatusId " + vbCrLf)
            If strStatus = "Active" Then
                .Append(" and t4.Status='Active' ")
            ElseIf strStatus = "Inactive" Then
                .Append(" and t4.Status='Inactive' ")
            End If
            .Append("AND t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("ORDER BY t1.StatusCodeDescrip ")
        End With
        db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllInSchoolStatus(ByVal CampusId As String, Optional ByVal strStatus As String = "Active") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select Distinct t1.StatusCodeId,t1.StatusCodeDescrip " + vbCrLf)
            .Append(" from syStatusCodes t1,sySysStatus t2,arStuEnrollments t3,syStatuses t4 " + vbCrLf)
            .Append(" where t1.SysStatusId=t2.SysStatusId and t1.StatusCodeId=t3.StatusCodeId and t2.InSchool = 1 and t1.StatusId=t4.StatusId " + vbCrLf)
            If strStatus = "Active" Then
                .Append(" and t4.Status='Active' ")
            ElseIf strStatus = "Inactive" Then
                .Append(" and t4.Status='Inactive' ")
            End If
            .Append("AND t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("ORDER BY t1.StatusCodeDescrip ")
        End With
        db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetStatusCodes(ByVal CampusId As String, Optional ByVal strStatus As String = "Active", Optional ByVal StudentEnrollmentStatus As String = "") As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select Distinct t1.StatusCodeId,t1.StatusCodeDescrip " + vbCrLf)
            .Append(" from syStatusCodes t1,sySysStatus t2,arStuEnrollments t3,syStatuses t4 " + vbCrLf)
            .Append(" where t1.SysStatusId=t2.SysStatusId and t1.StatusCodeId=t3.StatusCodeId and ")
            If StudentEnrollmentStatus = "InSchool" Then
                .Append(" t2.InSchool = 1 and ")
            ElseIf StudentEnrollmentStatus = "OutofSchool" Then
                .Append(" t2.InSchool = 0 and ")
            End If
            .Append(" t1.StatusId=t4.StatusId " + vbCrLf)
            If strStatus = "Active" Then
                .Append(" and t4.Status='Active' ")
            ElseIf strStatus = "Inactive" Then
                .Append(" and t4.Status='Inactive' ")
            End If
            .Append("AND t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            .Append("ORDER BY t1.StatusCodeDescrip ")
        End With
        db.AddParameter("@campid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
End Class
