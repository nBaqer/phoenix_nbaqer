Imports FAME.Advantage.Common

Public Class TimeClockExceptionVb

#Region "Private Data Members"
    Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")

#End Region


#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region


#Region "Public Methods"

    Public Function GetTimeClockExceptions(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim sb As New System.Text.StringBuilder
        Dim sbSub As New System.Text.StringBuilder
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strWhere As String = "  AND sc.SysStatusId NOT IN (14)
        AND CONVERT(VARCHAR, PunchTime, 101) < ExpGradDate "
        Dim strOrderBy As String = ""
        Dim strFrom As String = ""
        Dim strStudentId As String = ""

        strFrom = "FROM arStudenttimeClockPunches STC ,arstudent,arStuEnrollments, syCmpGrpCmps E,syCampuses ,syCampGrps ,     syStatusCodes SC "

        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
            'If paramInfo.FilterList.IndexOf("FundSourceId") > -1 Then            

        End If

        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther 
        End If

        If paramInfo.OrderBy <> "" Then
            strOrderBy &= "," & paramInfo.OrderBy
        End If



        If StudentIdentifier = "SSN" Then
            strStudentId = "arStudent.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStudentId = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStudentId = "arStudent.StudentNumber AS StudentIdentifier,"
        End If
        Dim sqlquery As String
        Try

            ' DE6854 12/5/2011 Janet Robinson - Remove Students from report with status 5 not in selected campus 
            ' subquery added campusid from arStuEnrollments to make determination
            With sbSub

                .Append("( SELECT ID,")
                .Append("  arStuEnrollments.StuEnrollId, arStuEnrollments.CampusId, arStudent.LastName,arStudent.FirstName,")
                .Append(" arStudent.MiddleName,")
                .Append(strStudentId)
                .Append("  arStuEnrollments.BadgeNumber,BadgeId,Status,ClsSectMeetingId, ")
                .Append(" convert(varchar,PunchTime,101) Date,(Select distinct convert(varchar,PunchTime,108) ")
                .Append(" from arStudenttimeClockPunches where  PunchType=1 and PunchTime=STC.PunchTime ")
                .Append(" and  BadgeId=STC.badgeId  and stuEnrollid=STC.StuEnrollId) as TimeIn,  (Select distinct convert(varchar,PunchTime,108) ")
                .Append(" from arStudenttimeClockPunches where  PunchType=2 and PunchTime=STC.PunchTime ")
                .Append(" and BadgeId=STC.badgeId and stuEnrollid=STC.StuEnrollId) as TimeOut ,  syCampGrps.CampGrpDescrip,SyCampuses.CampDescrip  ")
                .Append(strFrom)
                ''Added by saraswathi on march 31 2009 for Student GRoup in Filter section
                If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                    .Append(",adLeadByLeadGroups, adLeadGroups ")
                End If

                '.Append(" ,((Select distinct convert(varchar,PunchTime,101) pntime,BadgeId BdgID from arStudentTimeClockPunches)")
                '.Append(" except ")
                '.Append(" (Select distinct convert(varchar,PunchTime,101) pntime,BadgeId BdgID  from arStudentTimeClockPunches")
                '.Append(" where status=1)) as T3 ")
                .Append(" Where ")

                '.Append("    where(t3.PnTime = Convert(varchar, STC.PunchTime, 101))")
                '.Append(" and STC.BadgeId=t3.BdgID and ")
                .Append(" arstudent.Studentid = arStuEnrollments.StudentId ")
                .Append(" and arSTuEnrollments.StuEnrollId=STC.StuEnrollId  ")
                .Append(" and Convert(int,arStuEnrollments.BadgeNumber)=Convert(int,STC.BadgeId) ")
                .Append("    AND STC.Status IN (2,6,7,9,10)  ")
                .Append(" AND arStuEnrollments.CampusId=SyCampuses.CampusId AND sc.StatusCodeId = dbo.arStuEnrollments.StatusCodeId ")
                .Append("  AND SyCampuses.CampusId=E.CampusId AND    E.CampGrpId = syCampGrps.CampGrpId  ")
                ''Added by saraswathi on march 31 2009 for Student GRoup in Filter section
                If paramInfo.FilterList.Contains("adLeadGroups.LeadGrpId") Then
                    .Append("AND adLeadByLeadGroups.LeadGrpId=adLeadGroups.LeadGrpId ")
                    .Append("AND adLeadByLeadGroups.StuEnrollId=arStuEnrollments.StuEnrollId ")
                End If

                .Append(strWhere)
                .Append(" union all ")
                .Append(" select ID,")
                ''Modified by saraswathi on May 29 2009
                ''To show the student name for those with no student name mapped to the badgeID
                .Append("isNull((Select arStuEnrollments.StuEnrollId from arStuENrollments where Convert(int,arStuEnrollments.BadgeNumber)=Convert(int,STC.BadgeID)  AND arStuEnrollments.CampusId = '")
                .Append(paramInfo.CampusId)
                .Append("' ),null)StuEnrollId, ")
                .Append("isNull((Select arStuEnrollments.CampusId from arStuENrollments where Convert(int,arStuEnrollments.BadgeNumber)=Convert(int,STC.BadgeID) AND arStuEnrollments.CampusId = '")
                .Append(paramInfo.CampusId)
                .Append("'),null)CampusId, ")
                .Append(" isNull((Select arstudent.LastName from arStudent,arStuEnrollments where arStudent.StudentId=arStuEnrollments.StudentID And Convert(int,arStuEnrollments.BadgeNumber)= Convert(int,STC.BadgeID) AND arStuEnrollments.CampusId = '")
                .Append(paramInfo.CampusId)
                .Append("'),'')Lastname, ")
                .Append(" isNull((Select arstudent.FirstName from arStudent,arStuEnrollments where arStudent.StudentId=arStuEnrollments.StudentID and Convert(int,arStuEnrollments.BadgeNumber)= Convert(int,STC.BadgeID) AND arStuEnrollments.CampusId = '")
                .Append(paramInfo.CampusId)
                .Append("'),'')FirstName, ")
                .Append("isNull( (Select arstudent.MiddleName from arStudent,arStuEnrollments where arStudent.StudentId=arStuEnrollments.StudentID and Convert(int,arStuEnrollments.BadgeNumber)= Convert(int,STC.BadgeID) AND arStuEnrollments.CampusId = '")
                .Append(paramInfo.CampusId)
                .Append("' ),'') MiddleName, ")


                If StudentIdentifier = "SSN" Then
                    .Append("IsNull( (Select arstudent.SSN from arStudent,arStuEnrollments where arStudent.StudentId=arStuEnrollments.StudentID and Convert(int,arStuEnrollments.BadgeNumber)= Convert(int,STC.BadgeID) AND arStuEnrollments.CampusId = '")
                    .Append(paramInfo.CampusId)
                    .Append("' ),'')StudentIdentifier, ")
                ElseIf StudentIdentifier = "EnrollmentId" Then
                    strStudentId = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
                    .Append(" IsNull((Select arStuEnrollments.EnrollmentId from arStuEnrollments where Convert(int,arStuEnrollments.BadgeNumber)= Convert(int,STC.BadgeID) AND arStuEnrollments.CampusId = '")
                    .Append(paramInfo.CampusId)
                    .Append("'),'')StudentIdentifier, ")
                ElseIf StudentIdentifier = "StudentId" Then
                    .Append("IsNull( (Select arstudent.StudentNumber from arStudent,arStuEnrollments where arStudent.StudentId=arStuEnrollments.StudentID and Convert(int,arStuEnrollments.BadgeNumber)= Convert(int,STC.BadgeID) AND arStuEnrollments.CampusId = '")
                    .Append(paramInfo.CampusId)
                    .Append("'),'')StudentIdentifier, ")
                End If

                .Append("isNull( (Select arStuEnrollments.BadgeNumber from arStuEnrollments where  Convert(int,arStuEnrollments.BadgeNumber)= Convert(int,STC.BadgeID) AND arStuEnrollments.CampusId = '")
                .Append(paramInfo.CampusId)
                .Append("'),null) BadgeNumber, ")
                .Append(" BadgeId,Status,ClsSectMeetingId, ")
                .Append(" convert(varchar,PunchTime,101) Date,(Select  distinct convert(varchar,PunchTime,108) ")
                .Append(" from arStudenttimeClockPunches where  PunchType=1 and PunchTime=STC.PunchTime ")
                ''  .Append(" and  BadgeId=STC.badgeId and stuEnrollid=STC.StuEnrollId) as TimeIn,  (Select distinct convert(varchar,PunchTime,108) ")
                .Append(" and  BadgeId=STC.badgeId ) as TimeIn,  (Select distinct convert(varchar,PunchTime,108) ")
                .Append(" from arStudenttimeClockPunches where  PunchType=2 and PunchTime=STC.PunchTime ")
                ''  .Append(" and BadgeId=STC.badgeId and stuEnrollid=STC.StuEnrollId) as TimeOut,'','' ")
                .Append(" and BadgeId=STC.badgeId ) as TimeOut,'','' ")
                .Append(" from arStudenttimeClockPunches STC")
                ''Modified on June 12 2009
                ''Status in 5 or 8, i.e from inserted or from Duplicate entry
                .Append(" where StuEnrollId is Null and Status in (5,8,9) ) ")

            End With

            With sb

                .Append(" SELECT a.BadgeId, a.StuEnrollId, a.CampusId, a.LastName, a.FirstName, ")
                .Append(" a.MiddleName, a.StudentIdentifier, a.BadgeNumber, a.Status, a.ClsSectMeetingId, Date, ")
                .Append(" TimeIn, TimeOut, a.CampGrpDescrip, a.CampDescrip from " + sbSub.ToString + " a ")
                .Append(" WHERE CampusId IS NULL OR CampusId = '")
                .Append(paramInfo.CampusId)
                .Append("' ")
                .Append(" order by Lastname,FirstName, Id  ")
                sqlquery = sbSub.ToString

                .Append("; Select Distinct Date,BadgeId from " + sqlquery + " as Table1 ")
                .Append(" WHERE CampusId IS NULL OR CampusId = '")
                .Append(paramInfo.CampusId)
                .Append("' ; ")
            End With

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.OpenConnection()
            ds = db.RunParamSQLDataSet(sb.ToString)
        Catch ex As Exception
            Return ds
        End Try
        '  Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        Dim ds1 As New DataSet
        Dim dt As New DataTable("StudentDetails")
        Dim drpunch As DataRow()


        If ds.Tables.Count > 0 Then
            dt.Columns.Add(New DataColumn("StuEnrollId", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("StudentIdentifier", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("BadgeNumber", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("BadgeId", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Status", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("Date", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("TimeIn", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("TimeOut", System.Type.GetType("System.String")))
            dt.Columns.Add(New DataColumn("SuppressDate", System.Type.GetType("System.String")))

            Dim stuName As String
            Dim row As DataRow
            Dim i As Integer
            Dim j As Integer
            Dim IncrementTheCounter As Boolean = False

            For i = 0 To ds.Tables(1).Rows.Count - 1

                Dim sql As String = String.Format("BadgeId='{0}' and Date='{1}' ", _
                                                                ds.Tables(1).Rows(i)("BadgeId"), ds.Tables(1).Rows(i)("Date"))
                drpunch = ds.Tables(0).Select(sql)

                If drpunch.Length > 1 Then


                    'For j = 0 To drpunch.Length - 1
                    '    row = dt.NewRow()
                    '    row("StuEnrollId") = drpunch(j)("StuEnrollId").ToString
                    '    row("StudentIdentifier") = drpunch(j)("StudentIdentifier").ToString

                    '    stuName = drpunch(j)("LastName")
                    '    If Not (drpunch(j)("FirstName") Is System.DBNull.Value) Then
                    '        If drpunch(j)("FirstName") <> "" Then
                    '            stuName &= ", " & drpunch(j)("FirstName")
                    '        End If
                    '    End If
                    '    If Not (drpunch(j)("MiddleName") Is System.DBNull.Value) Then
                    '        If drpunch(j)("MiddleName") <> "" Then
                    '            stuName &= " " & drpunch(j)("MiddleName") & "."
                    '        End If
                    '    End If

                    '    row("StudentName") = stuName
                    '    row("BadgeId") = drpunch(j)("BadgeId")
                    '    row("Status") = drpunch(j)("Status")
                    '    row("Date") = drpunch(j)("Date")

                    '    If drpunch(j)("TimeIn") Is System.DBNull.Value Then
                    '        If j = drpunch.Length - 1 Then
                    '            row("TimeIn") = drpunch(j)("TimeIn")
                    '        Else
                    '            If Not drpunch(j + 1)("TimeIn") Is System.DBNull.Value Then
                    '                If (drpunch(j + 1)("TimeIn") < drpunch(j)("TimeOut")) Then
                    '                    row("TimeIn") = drpunch(j + 1)("TimeIn")
                    '                    IncrementTheCounter = True
                    '                Else
                    '                    row("TimeIn") = drpunch(j)("TimeIn")
                    '                End If

                    '            Else
                    '                row("TimeIn") = drpunch(j)("TimeIn")
                    '            End If
                    '        End If
                    '    Else
                    '        row("TimeIn") = drpunch(j)("TimeIn")
                    '    End If



                    '    If drpunch(j)("TimeOut") Is System.DBNull.Value Then
                    '        If j = drpunch.Length - 1 Then
                    '            row("TimeOut") = drpunch(j)("TimeOut")
                    '        Else
                    '            If Not drpunch(j + 1)("TimeOut") Is System.DBNull.Value Then
                    '                If (drpunch(j)("TimeIn") < drpunch(j + 1)("TimeOut")) Then
                    '                    row("TimeOut") = drpunch(j + 1)("TimeOut")
                    '                    IncrementTheCounter = True
                    '                Else
                    '                    row("TimeOut") = drpunch(j)("TimeOut")
                    '                End If
                    '            Else
                    '                row("TimeOut") = drpunch(j)("TimeOut")
                    '            End If
                    '        End If
                    '    Else
                    '        row("TimeOut") = drpunch(j)("TimeOut")
                    '    End If
                    '    dt.Rows.Add(row)
                    '    If IncrementTheCounter = True Then
                    '        IncrementTheCounter = False
                    '        j = j + 1
                    '    End If
                    'Next j
                    For j = 0 To drpunch.Length - 1
                        row = dt.NewRow()
                        row("StuEnrollId") = drpunch(j)("StuEnrollId").ToString
                        row("BadgeNumber") = drpunch(j)("BadgeNumber").ToString
                        row("StudentIdentifier") = drpunch(j)("StudentIdentifier").ToString

                        stuName = drpunch(j)("LastName")
                        If Not (drpunch(j)("FirstName") Is System.DBNull.Value) Then
                            If drpunch(j)("FirstName") <> "" Then
                                stuName &= ", " & drpunch(j)("FirstName")
                            End If
                        End If
                        If Not (drpunch(j)("MiddleName") Is System.DBNull.Value) Then
                            If drpunch(j)("MiddleName") <> "" Then
                                stuName &= " " & drpunch(j)("MiddleName") & "."
                            End If
                        End If

                        row("StudentName") = stuName
                        row("BadgeId") = drpunch(j)("BadgeId")
                        row("Status") = drpunch(j)("Status")
                        row("Date") = drpunch(j)("Date")

                        Try
                            row("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                        Catch ex As System.Exception
                            row("SuppressDate") = "no"
                        End Try

                        If drpunch(j)("Status") = 7 Then
                            If drpunch(j)("TimeIn") Is System.DBNull.Value Then
                                row("TimeOut") = drpunch(j)("TimeOut")
                            End If
                            If drpunch(j)("TimeOut") Is System.DBNull.Value Then
                                row("TimeIn") = drpunch(j)("TimeIn")
                                If j + 1 < drpunch.Length - 1 Then
                                    If drpunch(j + 1)("TimeIn") Is System.DBNull.Value Then
                                        row("TimeOut") = drpunch(j + 1)("TimeOut")
                                        IncrementTheCounter = True
                                    End If
                                End If
                            End If
                        Else

                            If drpunch(j)("TimeIn") Is System.DBNull.Value Then
                                If j = drpunch.Length - 1 Then
                                    row("TimeIn") = drpunch(j)("TimeIn")
                                Else
                                    If Not drpunch(j + 1)("TimeIn") Is System.DBNull.Value Then
                                        If (drpunch(j + 1)("TimeIn") < drpunch(j)("TimeOut")) Then
                                            row("TimeIn") = drpunch(j + 1)("TimeIn")
                                            IncrementTheCounter = True
                                        Else
                                            row("TimeIn") = drpunch(j)("TimeIn")
                                        End If

                                    Else
                                        row("TimeIn") = drpunch(j)("TimeIn")
                                    End If
                                End If
                            Else
                                row("TimeIn") = drpunch(j)("TimeIn")
                            End If



                            If drpunch(j)("TimeOut") Is System.DBNull.Value Then
                                If j = drpunch.Length - 1 Then
                                    row("TimeOut") = drpunch(j)("TimeOut")
                                Else
                                    If Not drpunch(j + 1)("TimeOut") Is System.DBNull.Value Then
                                        If (drpunch(j)("TimeIn") < drpunch(j + 1)("TimeOut")) Then
                                            row("TimeOut") = drpunch(j + 1)("TimeOut")
                                            IncrementTheCounter = True
                                        Else
                                            row("TimeOut") = drpunch(j)("TimeOut")
                                        End If
                                    Else
                                        row("TimeOut") = drpunch(j)("TimeOut")
                                    End If
                                End If
                            Else
                                row("TimeOut") = drpunch(j)("TimeOut")
                            End If
                        End If

                        dt.Rows.Add(row)
                        If IncrementTheCounter = True Then
                            IncrementTheCounter = False
                            j = j + 1
                        End If
                    Next j

                Else

                    row = dt.NewRow()
                    row("StuEnrollId") = drpunch(0)("StuEnrollId").ToString
                    row("StudentIdentifier") = drpunch(0)("StudentIdentifier").ToString

                    stuName = drpunch(0)("LastName")
                    If Not (drpunch(0)("FirstName") Is System.DBNull.Value) Then
                        If drpunch(0)("FirstName") <> "" Then
                            stuName &= ", " & drpunch(0)("FirstName")
                        End If
                    End If
                    If Not (drpunch(0)("MiddleName") Is System.DBNull.Value) Then
                        If drpunch(0)("MiddleName") <> "" Then
                            stuName &= " " & drpunch(0)("MiddleName") & "."
                        End If
                    End If

                    row("StudentName") = stuName
                    row("BadgeId") = drpunch(0)("BadgeId")
                    row("Status") = drpunch(0)("Status")
                    row("Date") = drpunch(0)("Date")
                    row("TimeIn") = drpunch(0)("TimeIn")
                    row("TimeOut") = drpunch(0)("TimeOut")
                    Try
                        row("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                    Catch ex As System.Exception
                        row("SuppressDate") = "no"
                    End Try

                    dt.Rows.Add(row)
                End If
            Next i

            ds1.Tables.Add(dt)
        End If

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ds1
    End Function
#End Region

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

End Class
