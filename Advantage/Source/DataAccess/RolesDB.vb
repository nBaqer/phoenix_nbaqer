' ===============================================================================
' RolesDB.vb
' DataAccess classes for the managing Roles
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Text
Imports System.Data.OleDb
Imports FAME.AdvantageV1.Common
Imports FAME.Advantage.Common

Public Class RolesDB
    ''' <summary>
    ''' Retrieve all roles from the syRoles table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetExistingRoles() As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        sb.Append("SELECT RoleId,Role " + vbCrLf)
        sb.Append("FROM syRoles " + vbCrLf)
        sb.Append("ORDER BY Role" + vbCrLf)
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
    End Function

    ''' <summary>
    ''' Retrieve all roles from the syRoles table
    ''' Filters are available to show active and inactive
    ''' TODO: Implement active and inactive
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetRoles(ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim MyAdvAppSettings As AdvAppSettings
        Dim isNotSupport As Boolean

        isNotSupport = (HttpContext.Current.Session("UserName") <> "Support")
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        sb.Append("SELECT   R.RoleId, R.Role, ST.StatusId, ST.Status " + vbCrLf)
        sb.Append("FROM     syRoles R " + vbCrLf)
        sb.Append("JOIN     syStatuses ST ON R.StatusId = ST.StatusId " + vbCrLf)
        If (isNotSupport)
            sb.Append("JOIN     sySysRoles SR ON R.SysRoleId = SR.SysRoleId " + vbCrLf)
            sb.Append("JOIN     syUserType UT ON SR.RoleTypeId = UT.UserTypeId " + vbCrLf)
            sb.Append("WHERE    UT.Code = 'Advantage' ")   'Not API ROLE
        End If
        '   Conditionally include only Active Items 
        If ShowActive And Not ShowInactive Then
            sb.Append(IF(isNotSupport,"AND ","WHERE "))
            sb.Append(" ST.Status = 'Active' ")
        ElseIf Not ShowActive And ShowInactive Then
            sb.Append(IF(isNotSupport,"AND ","WHERE "))
            sb.Append("  ST.Status = 'Inactive' ")
        End If
        sb.Append("ORDER BY Role" + vbCrLf)
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
    End Function

    ''' <summary>
    ''' Retrive foles by their SysRoleId
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetAdvantageRoles() As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        sb.Append("SELECT SysRoleId, Descrip ")
        sb.Append("FROM SySysRoles SR ")
        If (HttpContext.Current.Session("UserName") <> "Support")
            sb.Append("JOIN syUserType UT ON SR.RoleTypeId = UT.UserTypeId ")
            sb.Append("WHERE UT.Code = 'Advantage' ")
        End If 
        sb.Append("ORDER BY Descrip")
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
    End Function

    ''' <summary>
    ''' Get a RoleInfo object from the db given a roleid
    ''' </summary>
    ''' <param name="roleId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetRoleInfo(ByVal roleId As String) As RoleInfo
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        sb.Append("SELECT Code,Role,StatusId,SysRoleId,ModDate,ModUser ")
        sb.Append("FROM syRoles ")
        sb.Append("WHERE RoleId = ?")
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        db.AddParameter("@roleid", roleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        ' fill up the roleinfo
        Dim roleInfo As New RoleInfo
        If dr.Read() Then
            'Set properties with data from DataReader
            roleInfo.RoleId = roleId
            roleInfo.IsInDB = True
            roleInfo.Code = dr("Code")
            roleInfo.Description = dr("Role").ToString()
            roleInfo.StatusId = dr("StatusId").ToString()
            roleInfo.AdvantageRoleId = dr("SysRoleId").ToString
            roleInfo.ModUser = dr("ModUser").ToString()
            roleInfo.ModDate = dr("ModDate")
        End If

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return roleInfo
    End Function

    ''' <summary>
    ''' Persists a RoleInfo object to the database
    ''' </summary>
    ''' <param name="roleInfo"></param>
    ''' <param name="user"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function AddRoleInfo(ByVal roleInfo As RoleInfo, ByVal user As String) As String
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            Dim sb As New StringBuilder
            sb.Append("INSERT INTO syRoles (RoleId,Code,Role,StatusId,SysRoleId, ")
            sb.Append("ModUser, ModDate) ")
            sb.Append("VALUES (?,?,?,?,?,?,?) ")
            db.AddParameter("@roleid", roleInfo.RoleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@code", roleInfo.Code, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
            db.AddParameter("@descrip", roleInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@statusid", roleInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@sysroleid", roleInfo.AdvantageRoleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            If roleInfo.ModDate = "12:00 AM" Then
                db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@ModDate", roleInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            Return "" 'Return without errors
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    ''' <summary>
    ''' Updates a RoleInfo object to the database
    ''' </summary>
    ''' <param name="roleInfo"></param>
    ''' <param name="user"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function UpdateRoleInfo(ByVal roleInfo As RoleInfo, ByVal user As String) As String
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'do an insert
        Try
            'build the query
            Dim sb As New StringBuilder
            sb.Append("UPDATE syRoles ")
            sb.Append("SET Code = ?,Role = ?,StatusId = ?,SysRoleId = ?, ")
            sb.Append("ModUser = ?, ModDate = ? ")
            sb.Append("WHERE RoleId = ? ")

            db.AddParameter("@code", roleInfo.Code, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
            db.AddParameter("@descrip", roleInfo.Description, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@statusid", roleInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@sysroleid", roleInfo.AdvantageRoleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            roleInfo.ModDate = Date.Now
            db.AddParameter("@ModDate", roleInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@roleid", roleInfo.RoleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)
            Return "" '   return without errors            
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            db.CloseConnection()
        End Try
    End Function

    ''' <summary>
    ''' Deletes a role given a roleid
    ''' </summary>
    ''' <param name="roleId"></param>
    ''' <param name="modDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function DeleteRoleInfo(ByVal roleId As String, ByVal modDate As DateTime) As String
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'Do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            sb.Append("DELETE FROM syRoles ")
            sb.Append("WHERE RoleId = ? ")
            sb.Append(" AND ModDate = ? ;")
            sb.Append("Select count(*) from syRoles where RoleId = ? ")

            db.AddParameter("@roleid", roleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@roleid", roleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)
            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                Return "" 'Return without errors
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If
        Catch ex As OleDbException
            'Return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            db.CloseConnection()
        End Try
    End Function

    ''' <summary>
    ''' Get all the modules defined in the system
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetAllModules() As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        sb.Append("SELECT " & vbCrLf)
        sb.Append("			ResourceId as ModuleId, " & vbCrLf)
        sb.Append("			Resource as ModuleName " & vbCrLf)
        sb.Append("FROM		syResources M " & vbCrLf)
        sb.Append("WHERE	M.ResourceTypeId = 1 " & vbCrLf)
        sb.Append("ORDER BY	M.Resource ")
        Return db.RunSQLDataSet(sb.ToString)
    End Function

    ''' <summary>
    ''' Get all the valid modules for the given roleid
    ''' </summary>
    ''' <param name="roleId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetValidModules(ByVal roleId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        sb.Append("SELECT ")
        sb.Append("         RM.RoleModuleId, " + vbCrLf)
        sb.Append("         RM.RoleId, " + vbCrLf)
        sb.Append("         R.Role, " + vbCrLf)
        sb.Append("         RM.ModuleId, " + vbCrLf)
        sb.Append("         M.Resource as ModuleName " + vbCrLf)
        sb.Append("FROM     syRolesModules RM, syRoles R, syResources M " + vbCrLf)
        sb.Append("WHERE    RM.RoleId=R.RoleId AND RM.ModuleId=M.ResourceId AND M.ResourceTypeId=1 " + vbCrLf)
        If Not roleId Is Nothing AndAlso roleId <> "" Then
            sb.Append("         AND R.RoleId = ? " + vbCrLf)
            db.AddParameter("@roleId", roleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        sb.Append("ORDER BY ModuleName")
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function


    ''' <summary>
    ''' Get all the valid modules for the given user
    ''' </summary>
    ''' <param name="UserId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetValidModulesForUser(ByVal UserId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        sb.Append("SELECT " + vbCrLf)
        sb.Append("DISTINCT M.ResourceId as ModuleId,   " + vbCrLf)
        sb.Append("         M.Resource as ModuleName  " + vbCrLf)
        sb.Append("FROM     syUsersRolesCampGrps UR, syRolesModules RM, syRoles R, syResources M " + vbCrLf)
        sb.Append("WHERE    RM.RoleId=R.RoleId AND RM.ModuleId=M.ResourceId AND UR.RoleId=R.RoleId  AND M.ResourceTypeId=1 " + vbCrLf)
        If Not UserId Is Nothing AndAlso UserId <> "" Then
            sb.Append("         AND UR.UserId = ? " + vbCrLf)
            db.AddParameter("@userId", UserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        sb.Append("ORDER BY M.Resource")
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    ''' <summary>
    ''' Adds a valid module for the given roleid
    ''' </summary>
    ''' <param name="RoleId"></param>
    ''' <param name="ModuleId"></param>
    ''' <param name="user"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function AddValidModule(ByVal RoleId As String, ByVal ModuleId As String, ByVal user As String) As Boolean
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            sb.Append("INSERT syRolesModules (RoleId, ModuleId) ")
            sb.Append(" VALUES (?,?) ")

            db.AddParameter("@RoleId", RoleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModuleId", ModuleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.CloseConnection()
            '   return without errors
            Return True
        Catch ex As OleDbException
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Delete all the allowable modules for the given roleid
    ''' </summary>
    ''' <param name="roleId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function DeleteValidModules(ByVal roleId As String) As Boolean
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            '   build the query
            Dim sql As String = "DELETE syRolesModules WHERE RoleId = ? "
            db.AddParameter("@roleId", roleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Dim rowCount As Integer = db.RunParamSQLScalar(sql)
            db.CloseConnection()
            Return True
        Catch ex As OleDbException
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
        Return False
    End Function

    ''' <summary>
    ''' Get all users in this role
    ''' </summary>
    ''' <param name="roleId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetUsersInRole(ByVal roleId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        sb.Append("SELECT ")
        sb.Append("         DISTINCT U.UserId, " + vbCrLf)
        sb.Append("         U.FullName " + vbCrLf)
        sb.Append("FROM     syUsers U, syRoles R, syUsersRolesCampGrps UR " + vbCrLf)
        sb.Append("WHERE    UR.UserId = U.UserId AND UR.RoleId = R.RoleId " + vbCrLf)
        sb.Append("         AND UR.RoleId = ? " + vbCrLf)
        sb.Append("ORDER BY U.FullName")
        If Not roleId Is Nothing AndAlso roleId <> "" Then
            db.AddParameter("@roleId", roleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function CheckIfUserIsMappedToFrontDesk(ByVal UserId As String) As Boolean
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim strUser As String = ""
        With sb
            .Append(" SELECT  DISTINCT U.UserId ")
            .Append(" FROM     syUsers U, syRoles R, syUsersRolesCampGrps UR,sySysRoles SR ")
            .Append(" WHERE    UR.UserId = U.UserId AND UR.RoleId = R.RoleId and ")
            .Append(" R.SysRoleId = SR.SysRoleId and SR.SysRoleId=12 and U.UserId=? ")
        End With
        db.AddParameter("@UserId", UserId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strUser = db.RunParamSQLScalar(sb.ToString).ToString
            If Not strUser = "" Then
                Return True
            Else
                Return False
            End If
        Catch ex As System.Exception
            Return False
        End Try
    End Function
End Class
