Imports FAME.Advantage.Common

Public Class LeadDocsDB
    Public Function GetDocsLeadByLeadId(ByVal LeadId As String, ByVal campusId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim da6 As New OleDbDataAdapter
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select Distinct B.LeadId,A.LeadDocId,'(X) ' + (Select Descrip from adReqs where adReqId = A.DocumentId) ")
            .Append(" as DocumentDescription,(select OverRide from adEntrTestOverRide where EntrTestId=A.DocumentId and LeadId='" & LeadId & "') as OverRide,(select DocStatusDescrip from syDocStatuses where DocStatusId=A.DocStatusId) as DocumentStatus,B.FirstName,B.LastName,B.MiddleName from adLeadDocsReceived A,adLeads B where A.LeadId=B.LeadId and B.LeadId=? ")
            .Append(" and A.DocumentId not in (Select Distinct adReqId from adReqs WHERE StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ")
            .Append("  CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(")) ")
            .Append(" union ")
            .Append(" select Distinct B.LeadId,A.LeadDocId,(Select Descrip from adReqs where adReqId = A.DocumentId) ")
            .Append(" as DocumentDescription,(select OverRide from adEntrTestOverRide where EntrTestId=A.DocumentId and LeadId='" & LeadId & "') as OverRide,(select DocStatusDescrip from syDocStatuses where DocStatusId=A.DocStatusId) as DocumentStatus,B.FirstName,B.LastName,B.MiddleName from adLeadDocsReceived A,adLeads B where A.LeadId=B.LeadId and B.LeadId=? ")
            .Append(" and A.DocumentId in (Select Distinct adReqId from adReqs WHERE StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ")
            .Append("  CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(")) ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "LeadDT5")
        Catch ex As System.Exception
            Throw New System.Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetAllLeadsNamesById(ByVal LeadId As String) As DataSet

        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim da6 As New OleDbDataAdapter
        Dim ds As New DataSet
        'Build the sql query
        With sb
            .Append(" select Leadid,firstname,middlename,lastname from adLeads where LeadId=? ")
        End With
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        'Try
        da6.Fill(ds, "InstructorDT1")
        'Catch ex As System.Exception
        '    Throw New System.Exception(ex.InnerException.Message)
        'End Try
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetDocumentExtension(ByVal strFileName As String, ByVal strDocumentType As String) As String
        Dim db As New DataAccess
        Dim strExtension As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append(" Select FileExtension from syDocumentHistory where FileName = ? ")
            .Append("  and DocumentType=? ")
        End With
        db.AddParameter("@FileName", strFileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@DocumentType", strDocumentType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
        While dr.Read()
            strExtension = dr("FileExtension").ToString()
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return strExtension

    End Function
    Public Function GetLeadDetails(ByVal LeadDocId As String, ByVal LeadId As String) As StudentDocsInfo

        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append(" select Distinct SD.LeadId,SD.DocumentId,SD.DocStatusId,SD.ReceiveDate, ")
            .Append(" case when (select count(*) from adEntrTestOverRide where EntrTestId=SD.DocumentId and LeadId=SD.LeadId and OverRide=1) >=1 then 'True' else 'False' End as OverRide,")
            .Append(" (Select Distinct Descrip from adReqs where adReqId = SD.documentid) as DocumentDescrip, ")
            .Append(" (Select Distinct DocStatusDescrip from syDocStatuses where DocStatusId = SD.docstatusid) as DocumentStatus ")
            If MyAdvAppSettings.AppSettings("REGENT").ToString.ToLower = "yes" Then
                .Append(",trackcode,DueDate,NotifiedDate,CompletedDate,Notificationcode,Description")
            End If
            .Append(" from adLeadDocsReceived SD where LeadDocId =? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@LeadDocId", LeadDocId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim LeadDocs As New StudentDocsInfo

        While dr.Read()
            With LeadDocs
                .IsInDb = True
                If Not (dr("LeadId") Is System.DBNull.Value) Then .LeadId = CType(dr("LeadId"), Guid).ToString
                If Not (dr("DocumentDescrip") Is System.DBNull.Value) Then .DocumentDescrip = dr("DocumentDescrip") Else .DocumentDescrip = ""
                If Not (dr("DocumentId") Is System.DBNull.Value) Then .DocumentId = CType(dr("DocumentId"), Guid).ToString Else .DocumentId = ""
                If Not (dr("DocStatusId") Is System.DBNull.Value) Then .DocStatusId = CType(dr("DocStatusId"), Guid).ToString Else .DocStatusId = ""
                If Not (dr("DocumentStatus") Is System.DBNull.Value) Then .DocumentStatus = dr("DocumentStatus") Else .DocumentStatus = ""
                If MyAdvAppSettings.AppSettings("REGENT").ToString.ToLower = "yes" Then
                    If Not (dr("DueDate") Is System.DBNull.Value) Then .DueDate = dr("DueDate") Else .DueDate = ""
                    If Not (dr("NotifiedDate") Is System.DBNull.Value) Then .TransDate = dr("NotifiedDate") Else .TransDate = ""
                    If Not (dr("CompletedDate") Is System.DBNull.Value) Then .CompletedDate = dr("CompletedDate") Else .CompletedDate = ""
                    If Not (dr("Description") Is System.DBNull.Value) Then .DocDescrip = dr("Description") Else .DocDescrip = ""
                    If Not (dr("TrackCode") Is System.DBNull.Value) Then .TrackCode = dr("TrackCode") Else .TrackCode = ""
                End If
                If Not (dr("ReceiveDate") Is System.DBNull.Value) Then .ReceiveDate = dr("ReceiveDate") Else .ReceiveDate = ""
                If Not (dr("OverRide") Is System.DBNull.Value) Then
                    .Override = CType(dr("Override"), Boolean)
                Else
                    .Override = False
                End If
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return LeadDocs
    End Function
    Public Function DeleteLeadDocs(ByVal StudentDocId As String, ByVal DocumentId As String, ByVal LeadId As String) As Integer
        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM adLeadDocsReceived ")
                .Append("WHERE LeadDocId = ? ")
            End With

            '   add parameters values to the query

            '   BankId
            db.AddParameter("@LeadDocId", StudentDocId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            With sb
                .Append(" Delete from syDocumentHistory where LeadId=? and DocumentId=? ")
            End With
            db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@DocumentId", DocumentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            '   return without errors
            Return 0

        Catch ex As OleDbException
            '   return an error to the client
            Return -1
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetAllDocsStudentByStatus(ByVal DocumentStatusId As String, ByVal LeadId As String, ByVal CampusId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        Dim da6 As New OleDbDataAdapter
        Dim ds As New DataSet
        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        With sb
            'With subqueries
            .Append(" select Distinct SD.LeadDocId,SD.LeadId,'(X) ' + R.Descrip as DocumentDescription, ")
            .Append(" (Select Distinct DocStatusDescrip from syDocStatuses where DocStatusId=SD.DocStatusId) as DocumentStatus ")
            .Append(" from ")
            .Append("  adLeadDocsReceived  SD,(Select Distinct adReqId,Descrip,CampGrpId from adReqs) R, ")
            .Append("  adLeads B where SD.LeadId=B.LeadId  ")
            .Append("  and SD.DocumentId = R.adReqId ")
            .Append(" and SD.LeadId = ? ")
            .Append("  AND SD.DocumentId not in (Select Distinct adReqId from adReqs WHERE StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' AND ")
            .Append("  CampGrpId in 	('")
            .Append(strCampGrpId)
            .Append(")) ")
            If Not DocumentStatusId = "" Then
                .Append(" and SD.DocStatusId= ?  ")
            End If
            If Not strCampGrpId = "" Then
                .Append("   AND R.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Union ")
            .Append(" select A.LeadDocId,A.LeadId,R.Descrip ")
            .Append(" as DocumentDescription, ")
            .Append(" (Select Distinct DocStatusDescrip from syDocStatuses where DocStatusId=A.DocStatusId) as DocumentStatus ")
            .Append(" from adLeadDocsReceived A,adLeads B, ")
            .Append(" (Select Distinct adReqId,Descrip,CampGrpId from adReqs R where StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965F') R ")
            .Append(" where A.LeadId=B.LeadId and A.DocumentId=R.adReqId and A.LeadId=?  ")
            If DocumentStatusId <> "" Then
                .Append(" and A.DocStatusId = ? ")
            End If
            If Not strCampGrpId = "" Then
                .Append("   AND R.CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
        End With

        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If DocumentStatusId <> "" Then
            db.AddParameter("@DocStatusId", DocumentStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If DocumentStatusId <> "" Then
            db.AddParameter("@DocStatusId", DocumentStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.OpenConnection()
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "StudentDT5")
        Catch ex As System.Exception
            Throw New System.Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        Return ds
    End Function
    'Public Function GetModuleId(ByVal DocumentId As String) As Integer
    '    Dim db As New DataAccess
    '    Dim intModule As Integer

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


    '    Dim sb6 As New StringBuilder
    '    With sb6
    '        .Append("select ModuleId from cmDocuments where DocumentId=? ")
    '    End With
    '    db.AddParameter("@DocumentId", DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '    'Execute the query
    '    Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb6.ToString)
    '    While dr.Read()
    '        intModule = CType(dr("ModuleId"), Integer)
    '    End While

    '    If Not dr.IsClosed Then dr.Close()

    '    db.ClearParameters()
    '    sb6.Remove(0, sb6.Length)
    '    Return intModule
    'End Function
    Public Function GetStudentId(ByVal LeadId As String) As Integer
        Dim db As New DataAccess
        Dim intStudentCount As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        Dim sb6 As New StringBuilder
        With sb6
            .Append("select Count(*) as LeadCount from arStudent s1,arStuEnrollments s2 where s1.StudentId=s2.StudentId and s2.LeadId=? ")
        End With
        db.AddParameter("@LeadId", LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb6.ToString)
        Try
            While dr.Read()
                intStudentCount = CType(dr("LeadCount"), Integer)
            End While
        Catch ex As Exception
            Return 0
        Finally
            If Not dr.IsClosed Then dr.Close()
        End Try
        
        db.ClearParameters()
        sb6.Remove(0, sb6.Length)
        Return intStudentCount
    End Function

    Public Function InsertDocumentbyID(ByVal StrFileName As String, ByVal strFileExtension As String, ByVal User As String, ByVal strDocumentType As String, ByVal strLeadId As String, ByVal strDocumentId As String, ByVal strDisplayName As String) As String
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim intModule As Integer
        Dim sb6 As New StringBuilder
        With sb6
            .Append("select ModuleId from adReqs where adReqId=? and adReqTypeId=3 ")
        End With
        db.AddParameter("@DocumentId", strDocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb6.ToString)
        While dr.Read()
            intModule = CType(dr("ModuleId"), Integer)
        End While
        db.ClearParameters()
        sb6.Remove(0, sb6.Length)


        Dim sb As New StringBuilder
        With sb
            .Append("INSERT INTO syDocumentHistory (FileID, FileName,FileExtension, ModDate, ModUser,DocumentType,LeadId,DocumentId,ModuleId, DisplayName) ")
            .Append("VALUES(newID(),?,?,?,?,?,?,?,?,?)")

        End With

        'add parameters
        db.AddParameter("@FileName", StrFileName, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
        db.AddParameter("@FileExtension", strFileExtension, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DocumentType", strDocumentType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@LeadIDId", strLeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DocumentId", strDocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModuleId", intModule, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DisplayName", strDisplayName, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)


        '   execute query
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        Dim ds As New DataSet
        With sb
            .Append("Select FileID from syDocumentHistory where Filename=? and FileExtension=?  and ModUser=? and DocumentType=? and LeadID=? and DocumentId=? and ModuleId=? and DisplayName=? ")
        End With

        db.AddParameter("@FileName", StrFileName, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
        db.AddParameter("@FileExtension", strFileExtension, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DocumentType", strDocumentType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@LeadId", strLeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DocumentId", strDocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModuleId", intModule, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DisplayName", strDisplayName, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)

        ds = db.RunParamSQLDataSet(sb.ToString)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)(0).ToString
            End If
        End If

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return ""

    End Function
    Public Function UpdateDocument(ByVal FileID As String, ByVal strFileName As String, ByVal strFileExtension As String, ByVal User As String, ByVal strDocumentType As String, ByVal strStudentId As String, ByVal strDocumentId As String, ByVal strDisplayName As String) As Integer
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append("IF EXISTS( SELECT * from syDocumentHistory where FileID = '" & FileID & "')")
            .Append(" BEGIN ")
            .Append(" Update syDocumentHistory set ModUser = '" & User & "'," & " ModDate = '" & Date.Now & "', FileName= ?, FileExtension= ? , DocumentType= ? , LeadID= ? , DocumentId= ?   , DisplayName= ?   " & " where FileID = '" & FileID & "'")
            .Append(" END ")

        End With

        'add parameters

        db.AddParameter("@FileName", strFileName, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
        db.AddParameter("@FileExtension", strFileExtension, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DocumentType", strDocumentType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@StudentId", strStudentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DocumentId", strDocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ' db.AddParameter("@ModuleId", strModuleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DisplayName", strDisplayName, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)

        '   execute query
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)
        Return 0
        'Close Connection
        db.CloseConnection()
    End Function

    Public Function DeleteDocumentbyID(ByVal FileID As String) As String
        Dim db As New DataAccess
        Dim strExtension As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            Dim sb As New StringBuilder
            With sb
                .Append(" Delete from syDocumentHistory where FileID = ?  ")
            End With
            db.AddParameter("@FileID", FileID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            'Execute the query
            db.RunParamSQLDataReader(sb.ToString)
            db.CloseConnection()
            Return ""
        Catch ex As Exception
            Return "File Cannot Be Deleted"
        End Try
    End Function
    Public Function InsertDocument(ByVal strFileName As String, ByVal strFileExtension As String, ByVal User As String, ByVal strDocumentType As String, ByVal strLeadId As String, ByVal strDocumentId As String, ByVal strDisplayName As String) As Integer
        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim intModule As Integer
        Dim sb6 As New StringBuilder
        With sb6
            .Append("select ModuleId from adReqs where adReqId=? and adReqTypeId=3 ")
        End With
        db.AddParameter("@DocumentId", strDocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb6.ToString)
        While dr.Read()
            intModule = CType(dr("ModuleId"), Integer)
        End While
        db.ClearParameters()
        sb6.Remove(0, sb6.Length)

        Dim sb As New StringBuilder
        With sb
            .Append("IF EXISTS( SELECT * from syDocumentHistory where FileName = '" & strFileName & "'" & " AND DocumentType = '" & strDocumentType & "')")
            .Append(" BEGIN ")
            .Append(" Update syDocumentHistory set ModUser = '" & User & "'," & " ModDate = '" & Date.Now & "'" & " where FileName = '" & strFileName & "'" & " AND DocumentType = '" & strDocumentType & "'")
            .Append(" END ")
            .Append(" ELSE ")
            .Append(" BEGIN ")
            .Append("INSERT INTO syDocumentHistory (FileID, FileName,FileExtension, ModDate, ModUser,DocumentType,LeadId,DocumentId,ModuleId,DisplayName) ")
            .Append("VALUES(?,?,?,?,?,?,?,?,?,?)")
            .Append(" END ")
        End With

        'add parameters
        db.AddParameter("@FileId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@FileName", strFileName, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
        db.AddParameter("@FileExtension", strFileExtension, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DocumentType", strDocumentType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@LeadId", strLeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@DocumentId", strDocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModuleId", intModule, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@DisplayName", strDisplayName, DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)

        '   execute query
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        Return 0

    End Function
    Public Function GetAllDocumentsByStudentDocType(ByVal FileName As String, ByVal DocumentType As String) As DataSet
        Dim db As New DataAccess
        '' Dim strExtension As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append(" select 'Document' as Document,DisplayName,FileName, ")
            .Append(" upper(replace(FileExtension,'.','')) as DocumentCategory, ")
            .Append(" FileExtension,DocumentType from syDocumentHistory where ")
            .Append(" FileName like  + ? + '%'")
            .Append(" and DocumentType = ? ")
        End With
        db.AddParameter("@FileName", FileName.Substring(0, FileName.IndexOf("_")), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@DocumentType", DocumentType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Return db.RunParamSQLDataSet(sb.ToString)
        db.CloseConnection()
    End Function
    Public Function GetAllDocumentsByStudentAndDocType(ByVal DocumentType As String) As DataSet
        Dim db As New DataAccess
        ''  Dim strExtension As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append(" select 'Document' as Document,DisplayName,FileName, ")
            .Append(" case FileExtension when '.gif' then 'Image' when '.doc' then 'Word Document' when '.jpeg' then 'Image' when '.xls' then 'Excel' end as DocumentCategory, ")
            .Append(" FileExtension,DocumentType from syDocumentHistory where ")
            .Append(" DocumentType = ? ")
        End With
        db.AddParameter("@DocumentType", DocumentType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Return db.RunParamSQLDataSet(sb.ToString)
        db.CloseConnection()
    End Function
    Public Function GetAllDocuments1() As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select Distinct SG.DocumentDescrip,SG.DocumentId ")
            .Append(" from cmDocuments SG,syStatuses ST ")
            .Append(" where SG.StatusId = St.StatusId and ST.Status = 'Active' ")
            .Append("Order By SG.DocumentDescrip ")
        End With

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllDocumentsByLeadGroup(ByVal PrgVerId As String, ByVal LeadId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" select Distinct t1.adReqId as DocumentId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,t2.Descrip as DocumentDescrip ")
            .Append(" from adPrgVerTestDetails t1,adReqs t2,adReqLeadGroups t3  ")
            .Append(" where t1.adReqId = t2.adReqId and t2.adReqId = t3.adReqId and t1.PrgVerId='" & PrgVerId & "' ")
            .Append(" and t2.adReqTypeId = 3 and t1.ReqGrpId is null and t3.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
            .Append(" union ")
            .Append(" select t5.adReqId as DocumentId,t4.ReqGrpId,t5.Descrip as DocumentDescrip ")
            .Append(" from  adReqGrpDef t4,adReqs t5,adReqLeadGroups t6  ")
            .Append(" where t4.adReqId = t5.adReqId and t5.adReqId = t6.adReqId and t6.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
            .Append(" and t5.adReqTypeId = 3 ")
            .Append(" and ReqGrpId in (select t1.ReqGrpId from adPrgVerTestDetails t1 where t1.PrgVerId='" & PrgVerId & "' and ReqGrpId is not null) ")

            .Append(" union ")
            'Get Documents assigned to lead group but not part of a requirement group or assigned to
            'program version

            .Append(" select Distinct B1.adReqId as DocumentId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,B1.Descrip as DocumentDescrip ")
            .Append(" from adReqs B1,adReqLeadGroups B2 where B1.adReqId = B2.adReqId  ")
            .Append(" and B2.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
            .Append(" and B1.adReqTypeId=3 and B1.adReqId not in ")
            .Append(" (select adReqId from adPrgVerTestDetails where PrgVerId='" & PrgVerId & "' and adReqId is not null) ")
            .Append(" and B1.adReqId not in  ")
            .Append(" (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails ")
            .Append(" where PrgVerId='" & PrgVerId & "' and adReqId is NULL)) ")

            'Get all mandatory requirements
            .Append(" union ")
            .Append(" select Distinct adReqId as DocumentId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,Descrip as DocumentDescrip ")
            .Append(" from adReqs where adReqTypeId=3 and AppliesToAll=1 ")
            .Append(" order by Descrip  ")
        End With

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllStandardDocumentsByLeadGroup(ByVal LeadId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            'Get Documents assigned to lead group but not part of a requirement group or assigned to
            'program version

            .Append(" select Distinct B1.adReqId as DocumentId,B1.Descrip as DocumentDescrip ")
            .Append(" from adReqs B1,adReqLeadGroups B2 where B1.adReqId = B2.adReqId  ")
            .Append(" and B2.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
            .Append(" and B1.adReqTypeId=3 and B1.adReqId not in ")
            .Append(" (select adReqId from adPrgVerTestDetails where adReqId is not null) ")
            .Append(" and B1.adReqId not in  ")
            .Append(" (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails ")
            .Append(" where adReqId is NULL)) ")

            'Get all mandatory requirements
            .Append(" union ")
            .Append(" select Distinct adReqId as DocumentId,Descrip as DocumentDescrip ")
            .Append(" from adReqs where adReqTypeId=3 and AppliesToAll=1 ")
            .Append(" order by Descrip  ")
        End With

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllStandardDocumentsByLeadGroupAndStatus(ByVal LeadId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            'Get Documents assigned to lead group but not part of a requirement group or assigned to
            'program version

            .Append(" select Distinct B1.adReqId as DocumentId,B1.Descrip as DocumentDescrip, ")
            .Append(" case when B2.IsRequired = 1 then 'Yes' else 'No' end as Required, ")
            .Append(" case when (select Count(*) from adLeadDocsReceived where DocumentId=B1.adReqId and LeadId='" & LeadId & "')>=1 then 'Yes' else 'No' end as Submitted,  ")
            .Append(" case when (select V1.sysDocStatusId  from sySysDocStatuses V1,syDocStatuses V2,adLeadDocsReceived V3  ")
            .Append(" where V1.SysDocStatusId = V2.SysDocStatusId and V2.DocStatusId = V3.DocStatusId and V3.DocumentId=B1.adReqId and V3.LeadId='" & LeadId & "' ) = 1 then 'Approved' else 'Not Approved' end as DocumentStatus  ")
            .Append(" from adReqs B1,adReqLeadGroups B2 where B1.adReqId = B2.adReqId  ")
            .Append(" and B2.LeadGrpId = (select Distinct LeadGrpId from adLeads where LeadId='" & LeadId & "') ")
            .Append(" and B1.adReqTypeId=3 and B1.adReqId not in ")
            .Append(" (select adReqId from adPrgVerTestDetails where adReqId is not null) ")
            .Append(" and B1.adReqId not in  ")
            .Append(" (select Distinct adReqId from adReqGrpDef where ReqGrpId in (select ReqGrpId from adPrgVerTestDetails ")
            .Append(" where adReqId is NULL)) ")
            'Get all mandatory requirements
            .Append(" union ")
            .Append(" select Distinct adReqId as DocumentId,Descrip as DocumentDescrip, ")
            .Append(" 'Yes' as Required, ")
            .Append(" case when (select Count(*) from adLeadDocsReceived where DocumentId=adReqId and LeadId='" & LeadId & "' )>=1 then 'Yes' else 'No' end as Submitted,  ")
            .Append(" case when (select V1.sysDocStatusId  from sySysDocStatuses V1,syDocStatuses V2,adLeadDocsReceived V3  ")
            .Append(" where V1.SysDocStatusId = V2.SysDocStatusId and V2.DocStatusId = V3.DocStatusId and V3.DocumentId=adReqId and V3.LeadId='" & LeadId & "' ) = 1 then 'Approved' else 'Not Approved' end as DocumentStatus  ")
            .Append(" from adReqs where adReqTypeId=3 and AppliesToAll=1 ")
            .Append(" order by Descrip  ")
        End With

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllDocumentsByStudent(ByVal CampusId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder
        Dim dsGetCmpGrps As New DataSet
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid

        dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(CampusId)
        Dim strCampGrpId As String
        If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
            For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
                strCampGrpId &= row("CampGrpId").ToString & "','"
            Next
            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        End If

        With sb
            .Append(" select Distinct adReqId as DocumentId,Descrip as DocumentDescrip from adReqs where adReqTypeId=3 and StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' ")
            If Not strCampGrpId = "" Then
                .Append(" and CampGrpId in 	('")
                .Append(strCampGrpId)
                .Append(") ")
            End If
            .Append(" Order by Descrip ")
        End With

        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetAllDocsByLeadGroupandStudent(ByVal ModuleId As String, ByVal StudentId As String) As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder

        With sb1
            .Append(" select Count(*) from arStuEnrollments where StudentId=? and LeadId is not null ")
        End With
        db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim intPrgVerId As Integer
        intPrgVerId = db.RunParamSQLScalar(sb1.ToString)
        sb1.Remove(0, sb1.Length)
        db.ClearParameters()

        If intPrgVerId >= 1 Then
            With sb
                .Append(" select DISTINCT adReqId as DocumentId,Descrip as DocumentDescrip from adReqs t1,adLeadDocsReceived t2 ")
                .Append(" WHERE t1.adReqTypeId = 3 And t1.adReqId = t2.DocumentId ")
                .Append(" AND t2.LeadId in (select Distinct LeadId from arStuEnrollments where StudentId='" & StudentId & "' and LeadId is not null) ")
                .Append(" union ")
                .Append(" select Distinct adReqId as DocumentId,Descrip as DocumentDescrip from adReqs where adReqTypeId=3 and ModuleId=? ")
                .Append(" and adReqId not in (select Distinct DocumentId from adLeadDocsReceived where LeadId in (select Distinct LeadId from arStuEnrollments where StudentId='" & StudentId & "' and LeadId is not null)) ")
            End With
        Else
            With sb
                .Append(" select Distinct adReqId as DocumentId,Descrip as DocumentDescrip from adReqs where adReqTypeId=3 and ModuleId=? ")
            End With
        End If
        db.AddParameter("@ModuleId", ModuleId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        'With sb
        '    .Append(" select t1.adReqId as DocumentId,'00000000-0000-0000-0000-000000000000' as ReqGrpId,t2.Descrip as DocumentDescrip ")
        '    .Append(" from adPrgVerTestDetails t1,adReqs t2,adReqLeadGroups t3  ")
        '    .Append(" where t1.adReqId = t2.adReqId and t2.adReqId = t3.adReqId and t1.PrgVerId in (select PrgVerId from arStuEnrollments where StudentId='" & StudentId & "') and t2.ModuleId=? ")
        '    .Append(" and t2.adReqTypeId = 3 and t1.ReqGrpId is null and t3.LeadGrpId = (select Distinct StudentGrpId from arStudent where StudentId='" & StudentId & "') ")
        '    .Append(" union ")
        '    .Append(" select t5.adReqId as DocumentId,t4.ReqGrpId,t5.Descrip as DocumentDescrip ")
        '    .Append(" from  adReqGrpDef t4,adReqs t5,adReqLeadGroups t6  ")
        '    .Append(" where t4.adReqId = t5.adReqId and t5.adReqId = t6.adReqId and t6.LeadGrpId = (select Distinct StudentGrpId from arStudent where StudentId='" & StudentId & "') ")
        '    .Append(" and t5.adReqTypeId = 3 and t5.ModuleId=? ")
        '    .Append(" and ReqGrpId in (select t1.ReqGrpId from adPrgVerTestDetails t1 where t1.PrgVerId in (select PrgVerId from arStuEnrollments where StudentId='" & StudentId & "') and ReqGrpId is not null) ")
        '    .Append(" order by Descrip  ")
        'End With
        'db.AddParameter("@ModuleId", ModuleId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function DeleteDocument(ByVal strFileName As String) As String
        Dim db As New DataAccess
        ''  Dim strExtension As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            Dim sb As New StringBuilder
            With sb
                .Append(" Delete from syDocumentHistory where FileName = ?  ")
            End With
            db.AddParameter("@FileName", strFileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute the query
            db.RunParamSQLDataReader(sb.ToString)
            db.CloseConnection()
            Return ""
        Catch ex As Exception
            Return "File Cannot Be Deleted"
        End Try
    End Function
    Public Function CheckIfDocumentStatusMapsToApprovedStatus(ByVal DocStatusId As String) As Integer
        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        'Do an Update
        '    'Build The Query
        Dim sb As New StringBuilder
        Dim intRecordCount As Integer = 0
        With sb
            .Append(" Select Count(*) from syDocStatuses where DocStatusId=? and SysDocStatusId=1 ")
        End With
        db.AddParameter("@DocStatusId", DocStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            intRecordCount = db.RunParamSQLScalar(sb.ToString)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        End Try
        Return intRecordCount
    End Function
    Public Function AddStudentDocs(ByVal StudentDocsInfo As StudentDocsInfo, ByVal user As String) As String

        ''Connect To The Database
        Dim db As New DataAccess

        Dim myTrans As OleDbTransaction

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim myConn As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString"))
        myConn.Open()


        'Do an Update
        Try
            myTrans = myConn.BeginTransaction()
            '    'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Insert adLeadDocsReceived(LeadDocId,LeadId,DocumentId,")
                .Append("DocStatusId,ReceiveDate,")
                .Append("ModUser,ModDate")
                If MyAdvAppSettings.AppSettings("REGENT").ToString.ToLower = "yes" Then
                    .Append(",trackcode,DueDate,NotifiedDate,CompletedDate,Notificationcode,Description")
                End If
                .Append(")")
                .Append(" Values(?,?,?,?,?,?,?")
                If MyAdvAppSettings.AppSettings("REGENT").ToString.ToLower = "yes" Then
                    .Append(",?,?,?,?,?,?")
                End If
                .Append(")")
            End With

            'Get StudentDocsId
            db.AddParameter("@StudentDocId", StudentDocsInfo.StudentDocsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'StudentId
            db.AddParameter("@StudentId", StudentDocsInfo.LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'DocumentId
            If StudentDocsInfo.DocumentId = Guid.Empty.ToString Or StudentDocsInfo.DocumentId = "" Then
                db.AddParameter("@DocumentId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@DocumentId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'DocStatusId
            If StudentDocsInfo.DocStatusId = Guid.Empty.ToString Or StudentDocsInfo.DocStatusId = "" Then
                db.AddParameter("@DocStatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@DocStatusId", StudentDocsInfo.DocStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ReceivedDate
            If StudentDocsInfo.ReceiveDate = "" Then
                db.AddParameter("@ReceiveDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ReceiveDate", StudentDocsInfo.ReceiveDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If



            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            If MyAdvAppSettings.AppSettings("REGENT").ToString.ToLower = "yes" Then
                If StudentDocsInfo.TrackCode = "" Then
                    db.AddParameter("@TrackCode", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@TrackCode", StudentDocsInfo.TrackCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If StudentDocsInfo.DueDate = "" Then
                    db.AddParameter("@DueDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@DueDate", StudentDocsInfo.DueDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If StudentDocsInfo.TransDate = "" Then
                    db.AddParameter("@TransDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@TransDate", StudentDocsInfo.TransDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If StudentDocsInfo.CompletedDate = "" Then
                    db.AddParameter("@CompletedDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@CompletedDate", StudentDocsInfo.CompletedDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If StudentDocsInfo.NotificationCode = "" Then
                    db.AddParameter("@NotificationCode", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@NotificationCode", StudentDocsInfo.NotificationCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                If StudentDocsInfo.DocDescrip = "" Then
                    db.AddParameter("@Description", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@Description", StudentDocsInfo.DocDescrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
            End If
            'Override
            'If StudentDocsInfo.Override = True Then
            '    db.AddParameter("@Override", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            'Else
            '    db.AddParameter("@Override", 0, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            'End If
            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            With sb
                .Append("delete from adEntrTestOverRide where LeadId=? and EntrTestId=? ")
            End With
            db.AddParameter("@LeadId", StudentDocsInfo.LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@EntrTestId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'check if document status maps to approved status, if so then uncheck override checkbox
            Dim intRecordCount As Integer = 0
            Dim intOverride As Integer
            If StudentDocsInfo.Override = True Then
                intOverride = 1
            Else
                intOverride = 0
            End If

            If StudentDocsInfo.DocStatusId = Guid.Empty.ToString Or StudentDocsInfo.DocStatusId = "" Then
            Else
                intRecordCount = CheckIfDocumentStatusMapsToApprovedStatus(StudentDocsInfo.DocStatusId)
                If intRecordCount >= 1 Then 'means the status maps to approved status
                    intOverride = 0 ' reset the override checkbox to 0
                End If
            End If

            With sb
                .Append(" Insert into adEntrTestOverRide(EntrTestOverRideId,LeadId,EntrTestId,OverRide,ModUser,ModDate) ")
                .Append(" values(?,?,?,?,?,?) ")
            End With
            db.AddParameter("@LeadEntrTestId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@LeadId", StudentDocsInfo.LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@EntrTestId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@OverRide", intOverride, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            StudentDocsInfo.IsInDb = True

            Return ""
            'Catch ex As System.Exception
            '    'Return an Error To Client
            '    Return -1
        Catch ex As OleDbException
            myTrans.Rollback()
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            myConn.Close()
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateStudentDocs(ByVal StudentDocsInfo As StudentDocsInfo, ByVal user As String, ByVal StudentDocsId As String) As String

        ''Connect To The Database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            '    'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append(" Update adLeadDocsReceived set LeadId=?,DocumentId=?, ")
                .Append(" DocStatusId=?,ReceiveDate=?, ")
                .Append(" ModUser=?, ModDate=?,OverRide=? ")
                .Append(" Where LeadDocId = ? ")
            End With

            'StudentId
            db.AddParameter("@StudentId", StudentDocsInfo.LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'DocumentId
            If StudentDocsInfo.DocumentId = Guid.Empty.ToString Or StudentDocsInfo.DocumentId = "" Then
                db.AddParameter("@DocumentId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@DocumentId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'DocStatusId
            If StudentDocsInfo.DocStatusId = Guid.Empty.ToString Or StudentDocsInfo.DocStatusId = "" Then
                db.AddParameter("@DocStatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@DocStatusId", StudentDocsInfo.DocStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ReceivedDate
            If StudentDocsInfo.ReceiveDate = "" Then
                db.AddParameter("@ReceiveDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ReceiveDate", StudentDocsInfo.ReceiveDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)


            If StudentDocsInfo.Override = True Then
                db.AddParameter("@Override", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Else
                db.AddParameter("@Override", 0, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End If



            'StudentDocId
            db.AddParameter("@StudentDocId", StudentDocsId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)


            With sb
                .Append("delete from adEntrTestOverRide where LeadId=? and EntrTestId=? ")
            End With
            db.AddParameter("@LeadId", StudentDocsInfo.LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@EntrTestId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'check if document status maps to approved status, if so then uncheck override checkbox
            Dim intRecordCount As Integer = 0
            Dim intOverride As Integer
            If StudentDocsInfo.Override = True Then
                intOverride = 1
            Else
                intOverride = 0
            End If

            If StudentDocsInfo.DocStatusId = Guid.Empty.ToString Or StudentDocsInfo.DocStatusId = "" Then
            Else
                intRecordCount = CheckIfDocumentStatusMapsToApprovedStatus(StudentDocsInfo.DocStatusId)
                If intRecordCount >= 1 Then 'means the status maps to approved status
                    intOverride = 0 ' reset the override checkbox to 0
                End If
            End If

            With sb
                .Append(" Insert into adEntrTestOverRide(EntrTestOverRideId,LeadId,EntrTestId,OverRide,ModUser,ModDate) ")
                .Append(" values(?,?,?,?,?,?) ")
            End With
            db.AddParameter("@LeadEntrTestId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@LeadId", StudentDocsInfo.LeadId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@EntrTestId", StudentDocsInfo.DocumentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@OverRide", intOverride, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)


            StudentDocsInfo.IsInDb = True

            Return ""
            'Catch ex As System.Exception
            '    'Return an Error To Client
            '    Return -1
        Catch ex As OleDbException
            '   return an error to the client
            Return ex.Message
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
        '     End Try
    End Function
    Public Function GetAllDocumentStatus() As DataSet
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        With sb
            .Append(" Select SG.DocStatusDescrip,SG.DocStatusId ")
            .Append(" from syDocStatuses SG,syStatuses ST  ")
            .Append(" where SG.StatusId = St.StatusId and ST.Status = 'Active' ")
            .Append(" Order By SG.DocStatusDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function CheckIfDocHasBeenApproved(ByVal DocStatusId As String) As Integer
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim intDocApproved As Integer = 0
        With sb
            .Append(" Select Count(*) from syDocStatuses t1,sySysDocStatuses t2 ")
            .Append(" where t1.SysDocStatusId = t2.SysDocStatusId And t2.SysDocStatusId = 1 and t1.DocStatusId=?")
        End With
        db.ClearParameters()
        db.AddParameter("@DocStatusId", DocStatusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            intDocApproved = db.RunParamSQLScalar(sb.ToString)
            If intDocApproved >= 1 Then
                Return intDocApproved
            Else
                Return 0
            End If
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Public Function GetAllDocumentsByLeadIdandDocumentID(ByVal LeadID As String, ByVal DocumentID As String) As DataSet
        Dim db As New DataAccess
        Dim strExtension As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb


            .Append("     SELECT  FileID ,  ")
            .Append(" 'Document' AS Document , ")
            .Append("   DisplayName , ")
            .Append("    FileName , ")
            .Append("    ( SELECT    VALUE ")
            .Append("        FROM dbo.syConfigAppSetValues ")
            .Append("        WHERE(SettingId = 32) ")
            .Append("     ) + DocumentType + '\' + CONVERT(VARCHAR(50), FileID) + FileExtension AS FileURL , ")
            .Append("      UPPER(REPLACE(FileExtension, '.', '')) AS DocumentCategory , ")
            .Append("          FileExtension, ")
            .Append("         DocumentType ")
            .Append("         FROM syDocumentHistory ")
            .Append(" WHERE   ( LeadID = ? ")
            .Append("     OR StudentID = ( SELECT   studentID ")
            .Append("      FROM  dbo.arStuEnrollments  ")
            .Append("                    WHERE    LeadID = ? ")
            .Append("                 )")
            .Append("     ) ")
            If DocumentID <> "" Then
                .Append("      AND DocumentId = ? ")
            End If
            '.Append(" select FileID,'Document' as Document, DisplayName,FileName, ")
            '.Append("  (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=32)+DocumentType+'\'+CONVERT(varchar(50),FileID)+FileExtension  AS FileURL, ")
            '.Append(" upper(replace(FileExtension,'.','')) as DocumentCategory, ")
            '.Append(" FileExtension,DocumentType from syDocumentHistory where ")
            ' ''.Append(" FileName like  + ? + '%'")
            '.Append("   studentid = ? ")
            'If DocumentID <> "" Then
            '    .Append(" and DocumentID = ? ")
            'End If

        End With
        ''    db.AddParameter("@FileName", FileName.Substring(0, FileName.IndexOf("_")), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.AddParameter("@LeadID", LeadID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@LeadID", LeadID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If DocumentID <> "" Then
            db.AddParameter("@DocumentID", DocumentID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        'Execute the query
        Return db.RunParamSQLDataSet(sb.ToString)
        db.CloseConnection()
    End Function

    ''' <summary>
    ''' The GetDocumentTypes method takes campusId as parameter and gets DocumentTypes from database return a dataset of with fields DocumentTypeId and DocumentTypeDescription.
    ''' </summary>
    ''' <param name="campusId"></param>
    ''' <returns></returns>
    Public Function GetDocumentTypes(ByVal campusId As String) As DataSet
        'connect to the database
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Dim strSql As String
        strSql = " SELECT t1.adReqId AS DocumentTypeId, "
        strSql += "        t1.Descrip AS DocumentTypeDescription "
        strSql += " FROM dbo.adReqs t1 "
        strSql += "     INNER JOIN dbo.syCmpGrpCmps t2 "
        strSql += "         ON t2.CampGrpId = t1.CampGrpId "
        strSql += "     INNER JOIN dbo.syStatuses t3"
        strSql += "         ON t3.StatusId = t1.StatusId "
        strSql += " WHERE t3.StatusCode = 'A'"
        strSql += "       AND t2.CampusId=? "
        strSql += " ORDER BY t1.Descrip; "

        db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(strSql)
        Try
            da.Fill(ds, "DocumentTypes")
        Finally
            db.CloseConnection()
        End Try
        Return ds
    End Function
End Class
