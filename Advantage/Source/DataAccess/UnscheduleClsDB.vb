Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common
Public Class UnscheduleClsDB
    Public Function GetAllTerms() As DataSet
        Dim ds As DataSet

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.TermId,t1.TermDescrip ")
                .Append("FROM arTerm t1,syStatuses t2 ")
                .Append("WHERE t2.Status = 'Active' and t1.StatusId = t2.StatusId ")
                .Append("ORDER BY t1.StartDate ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetCourses(ByVal Term As String, ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT distinct(a.ReqId), b.Descrip ")
                .Append("FROM arClassSections a, arReqs b ")
                .Append("WHERE a.TermId = ? and a.ReqId = b.ReqId ")
                .Append("AND a.CampusId = ?")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@TermId", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "TermCourses")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetInstructors(ByVal Course As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT distinct(a.InstructorId), b.FullName ")
                .Append("FROM arClassSections a, syUsers b ")
                .Append("WHERE a.ReqId = ? and a.InstructorId = b.UserId ")
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@TermId", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "Instructors")

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetDataGridInfo(ByVal SDate As String, ByVal EDate As String, ByVal CampusId As String) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim da As New OleDbDataAdapter
        Dim ds As New DataSet
        Dim count As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT  b.ClsSectionId, b.StartDate, b.EndDate,b.TermId,b.ReqId,b.InstructorId,b.ClsSection,c.Code,c.Descrip,d.TermDescrip, ")
            .Append("(Select FullName from syUsers where UserId = b.InstructorId) as FullName ")
            .Append("FROM  arClassSections b, arReqs c, arTerm d, syStatuses e ")
            .Append("WHERE b.EndDate >= ? and b.StartDate <= ? and b.ReqId = c.ReqId and b.TermId = d.TermId ")
            .Append("AND e.Status = 'Active' and d.StatusId = e.StatusId ")
            .Append("AND b.CampusId = ? ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@sdate", SDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@edate", EDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@cmpid", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query       
        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "Unschedule")

        Catch ex As System.Exception

        End Try
        sb.Remove(0, sb.Length)
        db.ClearParameters()

        'Close Connection
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetDataGridInfoOnItmCmd(ByVal SDate As String, ByVal EDate As String) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim da1 As New OleDbDataAdapter
        Dim ds As New DataSet
        Dim count As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("SELECT  b.ClsSectionId, b.StartDate, b.EndDate,b.TermId,b.ReqId,b.InstructorId,b.ClsSection,c.Code,c.Descrip,d.TermDescrip, ")
            .Append("(Select FullName from syUsers where UserId = b.InstructorId) as FullName ")
            .Append("FROM  arClassSections b, arReqs c, arTerm d ")
            .Append("WHERE b.StartDate <= ? and b.EndDate >= ? and b.ReqId = c.ReqId and b.TermId = d.TermId ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@sdate", SDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@edate", EDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        '   Execute the query       
        db.OpenConnection()
        da1 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da1.Fill(ds, "Unschedule")

        Catch ex As System.Exception

        End Try
        sb.Remove(0, sb.Length)
        db.ClearParameters()

        Dim da2 As New OleDbDataAdapter

        With sb
            .Append("SELECT a.ClsSectionId ")
            .Append("FROM arUnschedClosures a ")
            .Append("WHERE a.StartDate = ? and a.EndDate = ? ")
        End With
        'db.OpenConnection()

        db.AddParameter("@sdate", SDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@edate", EDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


        da2 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da2.Fill(ds, "UnschedClsSects")
        Catch ex As System.Exception

        End Try
        sb.Remove(0, sb.Length)
        'Close Connection
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetClsSectDataList(ByVal Term As String, ByVal Course As String, ByVal Instructor As String, ByVal campusId As String) As OleDbDataAdapter
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim objClsSect As New OleDbDataAdapter
        Dim ds As New DataSet
        Dim count As Integer


        With sb
            .Append("SELECT distinct a.StartDate,a.EndDate from arUnschedClosures a, arClassSections b ")
            .Append("where a.ClsSectionId=b.ClsSectionId and b.TermId = ?  ")
            .Append("and b.CampusId = ? ")
            If Course <> "" Then
                .Append(" and (b.ReqId = ?) ")
            End If
            If Instructor <> "" Then
                .Append(" and (b.InstructorId = ?) ")
            End If
        End With
        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@termId", Term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@cmpid", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If Course <> "" Then
            db.AddParameter("@courseId", Course, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        If Instructor <> "" Then
            db.AddParameter("@Instructor", Instructor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If

        db.OpenConnection()
        Try
            objClsSect = db.RunParamSQLDataAdapter(sb.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New System.Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        db.ClearParameters()
        Return objClsSect
    End Function

    Public Function PopulateClsSectDataList(ByVal Term As String, ByVal Course As String, ByVal Instructor As String, ByVal campusId As String, Optional ByVal dataset As DataSet = Nothing) As DataSet
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Try
            da = GetClsSectDataList(Term, Course, Instructor, campusId)
        Catch ex As System.Exception
            Throw New System.Exception(ex.Message)
        End Try
        If dataset Is Nothing Then
            da.Fill(ds, "UnschedDates")
            Return ds
        Else
            da.Fill(dataset, "UnschedDates")
            Return dataset
        End If
        Return ds
    End Function

    Public Function InsertUnschedDate(ByVal UnschedObj As UnscheduleClsInfo, ByVal user As String)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim clssectid As String
        Dim UnschedId As String
        Dim sDate As Integer
        Dim eDate As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If



        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("INSERT INTO arUnschedClosures(ClsSectionId,StartDate,EndDate,ModUser,ModDate) ")
            .Append("VALUES(?,?,?,?,?)")

            db.AddParameter("@clssectid", UnschedObj.ClsSectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@startdate", UnschedObj.StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            db.AddParameter("@enddate", UnschedObj.EndDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        End With

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function

    Public Function DeleteUnschedDate(ByVal UnschedObj As UnscheduleClsInfo)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim clssectid As String
        Dim StudentId As String
        Dim GrdBkWgtDetailId As String
        Dim score As Integer
        Dim Comments As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        'Try
        With sb
            .Append("DELETE FROM arUnschedClosures ")
            .Append("WHERE ClsSectionId = ? ")
            .Append("AND StartDate = ? ")
            .Append("AND EndDate = ? ")
        End With

        db.AddParameter("@clssectid", UnschedObj.ClsSectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@startdate", UnschedObj.StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@enddate", UnschedObj.EndDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close(Connection)
        db.CloseConnection()

        '    '   execute the query
        '    Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

        '    '   If the row was not deleted then there was a concurrency problem
        '    If rowCount = 0 Then
        '        '   return without errors
        '        Return ""
        '    Else
        '        Return DALExceptions.BuildConcurrencyExceptionMessage()
        '    End If

        'Catch ex As OleDbException
        '    '   return an error to the client
        '    Return DALExceptions.BuildErrorMessage(ex)
        'Finally
        '    'Close Connection
        '    db.CloseConnection()
        'End Try
    End Function
    'Public Function CancelClassesAndReSchedule(ByVal dsCancelledClasses As DataSet)
    '    Dim db As New DataAccess
    '    Dim sb As New System.Text.StringBuilder
    '    Dim ds As DataSet
    '    Dim clssectid As String
    '    Dim UnschedId As String
    '    Dim sDate As Integer
    '    Dim eDate As String

    '    'Set the connection string
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")


    '    With sb
    '        .Append("INSERT INTO arUnschedClosures(ClsSectionId,StartDate,EndDate,ModUser,ModDate) ")
    '        .Append("VALUES(?,?,?,?,?)")

    '        db.AddParameter("@clssectid", UnschedObj.ClsSectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '        db.AddParameter("@startdate", UnschedObj.StartDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
    '        db.AddParameter("@enddate", UnschedObj.EndDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
    '        db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '    End With

    '    db.RunParamSQLExecuteNoneQuery(sb.ToString)
    '    db.ClearParameters()
    '    sb.Remove(0, sb.Length)

    '    'Close Connection
    '    db.CloseConnection()
    'End Function
    Public Sub SetupRules(ByVal xmlRules As String)
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@RuleValues", xmlRules, SqlDbType.VarChar, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("USP_CancelClasses_Insert", Nothing)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
    Public Function CheckIfAttendancePostedOnCancelledDate(ByVal ClsMeetingId As String, ByVal StartDate As Date, ByVal EndDate As Date) As Boolean
        Dim db As New SQLDataAccess
        Dim intAttendancePosted As Integer = 0

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@ClsSectMeetingId", ClsMeetingId, SqlDbType.VarChar, , ParameterDirection.Input)
            db.AddParameter("@StartDate", StartDate, SqlDbType.Date, , ParameterDirection.Input)
            db.AddParameter("@EndDate", EndDate, SqlDbType.Date, , ParameterDirection.Input)
            intAttendancePosted = CType(db.RunParamSQLScalar_SP("USP_AttendancePostedOnCancellationDate_Check"), Integer)
            If intAttendancePosted >= 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As System.Exception
            Return False
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
    Public Sub SetupMeetings(ByVal xmlRules As String)
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@RuleValues", xmlRules, SqlDbType.VarChar, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("USP_CopyClasses_Insert", Nothing)
        Catch ex As System.Exception
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Sub
    Public Function RescheduleClassMeetings(ByVal xmlRules As String) As Integer
        Dim db As New SQLDataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            'Call the procedure to insert more than one record all at once
            db.AddParameter("@RuleValues", xmlRules, SqlDbType.VarChar, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery_SP("USP_RescheduleClasses_Insert", Nothing)
            Return 1 'success
        Catch ex As System.Exception
            Return 0 'failure
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
    End Function
End Class
