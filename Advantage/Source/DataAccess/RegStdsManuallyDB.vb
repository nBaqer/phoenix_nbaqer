Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Data.SqlClient
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class RegStdsManuallyDB
    Public SqlConn As New SqlConnection()
    ''All the classSections except the ones the student has already taken

    'check if term is part to campus user is logged in or term is assigned to ALL Campus group

    Public Function GetCourseClassSections(ByVal ReqId As String, ByVal StuEnrollId As String, ByVal ChkAllCourses As Boolean, ByVal userId As String, Optional ByVal CampusId As String = "") As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        Try
            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With sb
                '.Append("select * from ((")
                .Append("Select distinct t1.ClsSectionId, t1.ClsSection, (Select t2.ShiftDescrip from arShifts t2 where t2.ShiftId = t1.ShiftId) AS ShiftDescrip")
                .Append("     , t1.EndDate,t2.StartDate AS TermStartDate, t2.TermDescrip ")
                .Append("     , (select CampDescrip from syCampuses where CampusId=t1.CampusId) AS CampDescrip ")
                .Append("     , t1.TermId, t1.Startdate as ClassStartDate, ISNULL(SU.FullName, '') AS Instructor ")
                .Append("     , (SELECT TOP 1 TotalCount  ")
                .Append("        FROM ( ")
                .Append("			   SELECT COUNT(*) AS TotalCount ")
                .Append("			   FROM  arResults ")
                .Append("			   WHERE StuEnrollId = ?  ")
                .Append("			     AND TestId=t1.ClsSectionId ")
                .Append("			     AND (Score IS NOT NULL OR GrdSysDetailId IS NOT null) ")
                .Append("		       UNION ")
                .Append("			   SELECT COUNT(*) AS TotalCount ")
                .Append("			   FROM  arTransferGrades Grades ")
                .Append("			   WHERE ReqId=t1.ReqId AND TermId = t1.TermId AND StuEnrollId = ? ")
                .Append("			     AND (Score IS NOT NULL OR GrdSysDetailId IS NOT null) ")
                .Append("	          ) AS tblDerived ")
                .Append("		 ORDER BY tblDerived.TotalCount DESC ")
                .Append("       ) AS IsClassAttempted ")
                .Append("FROM arClassSections AS t1 ")
                .Append("    INNER JOIN arTerm AS t2 ON t2.TermId = t1.TermId ")
                .Append("    INNER JOIN syStatuses AS t3 ON t3.StatusId = t2.StatusId ")
                .Append("    LEFT JOIN syUsers AS SU ON SU.UserId = t1.InstructorId ")
                .Append("WHERE ReqId = ? ")
                .Append("   ")
                ''' Code Added by kamalesh on 22 Jan 2010 to resolve mantis issue id 17912 ''''
                If ChkAllCourses = True Then
                    If CampusId <> "" Then
                        .Append("  AND ((t2.CampGrpId IN (select distinct CampGrpId  from syUsersRolesCampGrps where UserId='" + userId + "' ))")
                        .Append(" or t2.CampGrpId in (select distinct CampGrpId from syCmpGrpCmps where CampusId  in (select CampusId from syCmpGrpCmps where CampGrpId in(select distinct CG.CampGrpId from syCampGrps CG,syUsersRolesCampGrps UR where CampGrpDescrip='All'  and UR.UserId='" + userId + "' and CG.CampGrpId=UR.CampGrpId)  ))")
                        .Append(" or t1.Campusid in (select CampusId from syCmpGrpCmps where CampGrpId in(select distinct CG.CampGrpId from syCampGrps CG,syUsersRolesCampGrps UR ")
                        .Append(" where  UR.UserId='" + userId + "' and CG.CampGrpId=UR.CampGrpId)) )")
                    End If
                Else
                    .Append(" and (t2.CampGrpId IN (SELECT CampGrpId ")
                    .Append("FROM syCmpGrpCmps ")
                    .Append("WHERE CampusId = '")
                    .Append(CampusId)
                    .Append("')) ")
                End If
                '''''''''''''''''''
                If ChkAllCourses = False Then
                    .Append(" and t1.campusid=? ")
                End If
                .Append(" and (t1.EndDate > getdate() or t3.Status='Active')  and t2.EndDate >= getdate() ORDER BY IsClassAttempted desc ")
  
            End With

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            ' Add the PrgVerId and ChildId to the parameter list

            If Not String.IsNullOrEmpty(StuEnrollId) Then
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If


            db.AddParameter("@ReqId", ReqId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If ChkAllCourses = False Then
                db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
  
            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "CourseClsSects")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function
    Public Function GetCourseClassSectionsByClassSectionId(ByVal clsSectionId As String, ByVal StuEnrollId As String, ByVal ChkAllCourses As Boolean, ByVal userId As String, Optional ByVal CampusId As String = "", Optional ByVal termIds As String = "") As DataSet
        Dim ds As New DataSet
        ' Dim db As New DataAccess
        Dim da As SqlDataAdapter
        Dim conn As SqlConnection
        Dim sqlCommand As SqlCommand
        Dim sb As New StringBuilder

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        'for some reason this term id is being passed from the page to the facade to the data layer with tic marks around the value - remove them before using
        termIds = If(termIds = String.Empty, "00000000-0000-0000-0000-000000000000", termIds.Substring(1, termIds.Length - 2))

        Try
            With sb
                '.Append("select * from ((")
                .Append("Select distinct t1.ClsSectionId, t1.ClsSection " + Chr(10))
                .Append("     , (Select t2.ShiftDescrip from arShifts t2 where t2.ShiftId = t1.ShiftId) AS ShiftDescrip " + Chr(10))
                .Append("     , t1.EndDate, t2.StartDate AS TermStartDate " + Chr(10))
                .Append("     , t2.TermDescrip " + Chr(10))
                .Append("     , (SELECT CampDescrip from syCampuses where CampusId = t1.CampusId) AS CampDescrip " + Chr(10))
                .Append("     , t1.TermId, t1.Startdate as ClassStartDate  " + Chr(10))
                .Append("     , ISNULL(SU.FullName, '') AS Instructor " + Chr(10))
                .Append("     , (SELECT TOP 1 TotalCount  " + Chr(10))
                .Append("        FROM ( " + Chr(10))
                .Append("			   SELECT COUNT(*) AS TotalCount  " + Chr(10))
                .Append("			   FROM  arResults AS AR " + Chr(10))
                .Append("			   WHERE AR.TestId = t1.ClsSectionId " + Chr(10))
                .Append("			     AND (AR.Score IS NOT NULL OR AR.GrdSysDetailId IS NOT null) " + Chr(10))
                .Append("			     AND AR.StuEnrollId = @StuEnrollId_01 " + Chr(10))
                .Append("		       UNION " + Chr(10))
                .Append("			   SELECT COUNT(*) AS TotalCount " + Chr(10))
                .Append("			   FROM  arTransferGrades AS TG " + Chr(10))
                .Append("			   WHERE TG.ReqId = t1.ReqId AND TG.TermId = t1.TermId " + Chr(10))
                .Append("			     AND (TG.Score IS NOT NULL OR TG.GrdSysDetailId IS NOT null) " + Chr(10))
                .Append("			     AND TG.StuEnrollId = @StuEnrollId_02 " + Chr(10))
                .Append("	          ) AS TOT " + Chr(10))
                .Append("		 ORDER BY TOT.TotalCount DESC " + Chr(10))
                .Append("       ) AS IsClassAttempted " + Chr(10))
                .Append("FROM arClassSections AS t1 " + Chr(10))
                .Append("    INNER JOIN arTerm AS t2 ON t2.TermId = t1.TermId " + Chr(10))
                .Append("    INNER JOIN syStatuses AS t3 ON t3.StatusId = t2.StatusId " + Chr(10))
                .Append("    LEFT JOIN syUsers AS SU ON SU.UserId = t1.InstructorId " + Chr(10))
                .Append("WHERE t1.ClsSectionId = @ClsSectionId " + Chr(10))
                .Append("  AND (@TermId_01 = '00000000-0000-0000-0000-000000000000' OR t2.TermId = @TermId_02 )" + Chr(10))
                .Append("   " + Chr(10))
                ''' Code Added by kamalesh on 22 Jan 2010 to resolve mantis issue id 17912 ''''
                If ChkAllCourses = True Then
                    If CampusId <> "" Then
                        .Append("  AND ((t2.CampGrpId IN (select distinct CampGrpId  from syUsersRolesCampGrps where UserId='" + userId + "' ))" + Chr(10))
                        .Append(" or t2.CampGrpId in (select distinct CampGrpId from syCmpGrpCmps where CampusId  in (select CampusId from syCmpGrpCmps where CampGrpId in(select distinct CG.CampGrpId from syCampGrps CG,syUsersRolesCampGrps UR where CampGrpDescrip='All'  and UR.UserId='" + userId + "' and CG.CampGrpId=UR.CampGrpId)  ))" + Chr(10))
                        .Append(" or t1.Campusid in (select CampusId from syCmpGrpCmps where CampGrpId in(select distinct CG.CampGrpId from syCampGrps CG,syUsersRolesCampGrps UR " + Chr(10))
                        .Append(" where  UR.UserId='" + userId + "' and CG.CampGrpId=UR.CampGrpId)) )" + Chr(10))
                    End If
                Else
                    .Append(" and (t2.CampGrpId IN (SELECT CampGrpId " + Chr(10))
                    .Append("                       FROM syCmpGrpCmps " + Chr(10))
                    .Append("                       WHERE CampusId = @CampusId_01 ) " + Chr(10))
                    .Append("     ) " + Chr(10))
                    .Append(" and t1.campusid = @CampusId_02 " + Chr(10))
                End If
                .Append(" and (t1.EndDate > getdate() or t3.Status='Active')  and t2.EndDate >= getdate() ORDER BY IsClassAttempted desc " + Chr(10))

            End With

            conn = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString"))
            sqlCommand = New SqlCommand(sb.ToString(), conn)


            If Not String.IsNullOrEmpty(StuEnrollId) Then
                sqlCommand.Parameters.AddWithValue("@StuEnrollId_01", StuEnrollId)
                sqlCommand.Parameters.AddWithValue("@StuEnrollId_02", StuEnrollId)
            End If
            sqlCommand.Parameters.AddWithValue("@ClsSectionId", clsSectionId)
            sqlCommand.Parameters.AddWithValue("@TermId_01", termIds)
            sqlCommand.Parameters.AddWithValue("@TermId_02", termIds)
            If ChkAllCourses = False Then
                sqlCommand.Parameters.AddWithValue("@CampusId_01", CampusId)
                sqlCommand.Parameters.AddWithValue("@CampusId_02", CampusId)
            End If
            da = New SqlDataAdapter(sqlCommand)
            conn.Open()
            Try
                da.Fill(ds, "CourseClsSects")

            Catch ex As Exception
                Throw New BaseException(ex.Message)
            Finally
                conn.Close()
                sb.Remove(0, sb.Length)
            End Try
        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally

        End Try

        'Return the datatable in the dataset
        Return ds
    End Function
    Public Function GetPeriodClassRoom(ByVal clsSectionId As String) As List(Of PeriodClassRoom)

        Dim myAdvAppSettings As AdvAppSettings
        Dim sqlConnection As SqlConnection
        Dim sqlCommand As SqlCommand
        Dim sqlDr As SqlDataReader
        Dim sb As StringBuilder = New StringBuilder()
        Dim periodClassRoom As PeriodClassRoom
        Dim periodClassRoomList As List(Of PeriodClassRoom) = New List(Of PeriodClassRoom)()


        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If
        sqlConnection = New SqlConnection(myAdvAppSettings.AppSettings("ConnectionString"))
        sqlConnection.Open()
        Try

            'Create the query
            With sb
                .Append("SELECT SP.PeriodId, SP.PeriodDescrip " + Chr(10))
                .Append("     , AR.RoomId, AR.Descrip AS RoomDescrip " + Chr(10))
                .Append("FROM arClassSections AS ACS " + Chr(10))
                .Append("    INNER JOIN arClsSectMeetings AS ACSM ON ACSM.ClsSectionId = ACS.ClsSectionId " + Chr(10))
                .Append("    LEFT JOIN syPeriods AS SP ON SP.PeriodId = ACSM.PeriodId " + Chr(10))
                .Append("    LEFT JOIN arRooms   AS AR ON AR.RoomId = ACSM.RoomId " + Chr(10))
                .Append("WHERE ACS.ClsSectionId = @ClsSectionId " + Chr(10))
                .Append("ORDER BY ACSM.StartDate, ACSM.EndDate " + Chr(10))
            End With

            sqlCommand = sqlConnection.CreateCommand()
            sqlCommand.CommandText = sb.ToString
            sqlCommand.Parameters.AddWithValue("@clsSectionId", clsSectionId)
            sqlDr = sqlCommand.ExecuteReader

            If sqlDr.HasRows Then
                While (sqlDr.Read())
                    periodClassRoom = New PeriodClassRoom(IIf(sqlDr("PeriodId") Is DBNull.Value, String.Empty, sqlDr("PeriodId").ToString()) _
                                                         , IIf(sqlDr("PeriodDescrip") Is DBNull.Value, String.Empty, sqlDr("PeriodDescrip").ToString()) _
                                                         , IIf(sqlDr("RoomId") Is DBNull.Value, String.Empty, sqlDr("RoomId").ToString()) _
                                                         , IIf(sqlDr("RoomDescrip") Is DBNull.Value, String.Empty, sqlDr("RoomDescrip").ToString())
                                                         )
                    periodClassRoomList.Add(periodClassRoom)
                End While
            End If

            Return periodClassRoomList
        Finally
            sqlConnection.Close()
        End Try
    End Function

    Public Function IsEnrollmentInSchoolStatus(ByVal StuEnrollId As String) As Boolean
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim isInSchool As Integer

        Try
            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            With sb
                .Append("SELECT ss.InSchool ")
                .Append("FROM dbo.arStuEnrollments se ")
                .Append("INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId ")
                .Append("INNER JOIN dbo.sySysStatus ss ON ss.SysStatusId = sc.SysStatusId ")
                .Append("WHERE se.StuEnrollId = ? ")
            End With

            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.OpenConnection()

            isInSchool = CInt(db.RunParamSQLScalar(sb.ToString))

            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        If isInSchool = 1 Then
            Return True
        Else
            Return False
        End If
    End Function

End Class
