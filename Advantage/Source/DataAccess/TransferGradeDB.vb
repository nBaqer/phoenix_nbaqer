
Imports FAME.Advantage.Common

Public Class TransferGradeDB

    ''' <summary>
    ''' Application Stting for get some other differents values from config.
    ''' </summary>
    ''' <remarks></remarks>
    Private ReadOnly myAdvAppSettings As AdvAppSettings

    Sub New()
        myAdvAppSettings = AdvAppSettings.GetAppSettings()

    End Sub



    Public Function GetAllClassSectionByTermId(ByVal TermId As String, ByVal CampusId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("select t1.ClsSectionId,t1.ClsSection,t2.Code,t2.Descrip, ")
            .Append(" case t1.IsGraded when 1 then 'Yes' else 'No'  end as Grade ")
            .Append("from arClassSections t1,arReqs t2 ")
            .Append("where t1.ReqId = t2.reqId and  ")
            .Append("t1.TermId = ? ")
            .Append("and t1.CampusId = ? ")
            .Append(" order by t1.ClsSection ")
        End With

        'Add the TermId to the parameter list
        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function CheckIfFinalGradePostedForCombination(ByVal StuEnrollid As String, ByVal ReqId As String) As Integer
        Dim db As New DataAccess
        'Dim ds As New DataSet

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim intCheckIfFinalGradePostedForCombination As Integer = 0

        With sb
            .Append(" select Count(*) from arResults t1,arClassSections t2  where ")
            .Append(" t1.StuEnrollId=? and t1.TestId=t2.ClsSectionid and t2.ReqId=?")
            .Append(" and isClinicsSatisfied=1")
        End With
        db.AddParameter("@StuEnrollId", StuEnrollid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ReqId", ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Try
            intCheckIfFinalGradePostedForCombination = db.RunParamSQLScalar(sb.ToString)
        Catch ex As Exception
            intCheckIfFinalGradePostedForCombination = 0
        End Try
        Return intCheckIfFinalGradePostedForCombination
    End Function
    Public Function isCourseALabWorkOrLabHourCourseCombination(ByVal ReqId As String) As Boolean
        Dim db As New DataAccess
        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim intCourseHasOnlyLabWorkAndLabHours As Integer
        Dim intCourseACombination As Integer

        With sb
            .Append(" select distinct Count(GC.Descrip) from arGrdBkWeights GBW,arGrdComponentTypes GC, arGrdBkWgtDetails GD  where ")
            .Append(" GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId ")
            .Append(" and     GBW.ReqId = ? and GC.SysComponentTypeID is not null ")
            .Append(" and GC.SysComponentTypeID in ")
            .Append(" (select Distinct ResourceId from syResources where Resource in ('Lab Work','Lab Hours'))")
        End With
        db.AddParameter("@ReqId", ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        intCourseHasOnlyLabWorkAndLabHours = db.RunParamSQLScalar(sb.ToString)
        If intCourseHasOnlyLabWorkAndLabHours >= 1 Then
            sb.Remove(0, sb.Length)
            db.ClearParameters()

            With sb
                .Append(" select distinct Count(GC.Descrip) from arGrdBkWeights GBW,arGrdComponentTypes GC, arGrdBkWgtDetails GD  where ")
                .Append(" GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId ")
                .Append(" and     GBW.ReqId = ? and GC.SysComponentTypeID is not null ")
                .Append(" and GC.SysComponentTypeID in (499,501,502,533,544) ")
            End With
            db.AddParameter("@ReqId", ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            intCourseACombination = db.RunParamSQLScalar(sb.ToString)
            If intCourseACombination >= 1 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
    Public Function isCourseALabWorkOrLabHourCourseCombination_SP(ByVal ReqId As String) As Boolean

        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Dim boolCourseHasOnlyLabWorkAndLabHours As Boolean = isCourseALabWorkOrLabHourCourse_SP(ReqId)
        Dim intCourseACombination As Integer = 0


        If boolCourseHasOnlyLabWorkAndLabHours Then
            db.AddParameter("@reqId", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            intCourseACombination = db.RunParamSQLScalar_SP("dbo.USP_isCourseALabWorkOrLabHourCourseCombination")
            If intCourseACombination >= 1 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
    Public Function isCourseALabWorkOrLabHourCourse(ByVal ReqId As String) As Boolean
        Dim db As New DataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim intCourseHasOnlyLabWorkAndLabHours As Integer = 0

        With sb
            .Append(" select distinct Count(GC.Descrip) from arGrdBkWeights GBW,arGrdComponentTypes GC, arGrdBkWgtDetails GD  where ")
            .Append(" GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId ")
            .Append(" and     GBW.ReqId = ? and GC.SysComponentTypeID is not null ")
            .Append(" and GC.SysComponentTypeID in ")
            .Append(" (select Distinct ResourceId from syResources where Resource in ('Lab Work','Lab Hours'))")
        End With
        db.AddParameter("@ReqId", ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        intCourseHasOnlyLabWorkAndLabHours = db.RunParamSQLScalar(sb.ToString)
        If intCourseHasOnlyLabWorkAndLabHours >= 1 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function isCourseALabWorkOrLabHourCourse_SP(ByVal ReqId As String) As Boolean
        'Dim ds As DataSet
        Dim db As New SQLDataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Dim intCourseHasOnlyLabWorkAndLabHours As Integer = 0

        db.AddParameter("@reqId", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        'add cutOff date parameter

        intCourseHasOnlyLabWorkAndLabHours = db.RunParamSQLScalar_SP("dbo.usp_isCourseALabWorkOrLabHourCourse")
        Try
            If intCourseHasOnlyLabWorkAndLabHours >= 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
        Finally
            db.CloseConnection()
        End Try

    End Function

    Public Function LabWorkOrLabHourCourseCount(ByVal ReqId As String) As Integer
        Dim db As New DataAccess

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim intCourseHasOnlyLabWorkAndLabHours As Integer = 0

        With sb
            .Append(" select distinct Count(GC.Descrip) from arGrdBkWeights GBW,arGrdComponentTypes GC, arGrdBkWgtDetails GD  where ")
            .Append(" GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId ")
            .Append(" and     GBW.ReqId = ? and GC.SysComponentTypeID is not null ")
            .Append(" and GC.SysComponentTypeID in ")
            .Append(" (select Distinct ResourceId from syResources where Resource in ('Lab Work','Lab Hours'))")
        End With
        db.AddParameter("@ReqId", ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        intCourseHasOnlyLabWorkAndLabHours = db.RunParamSQLScalar(sb.ToString)
        Return intCourseHasOnlyLabWorkAndLabHours
    End Function
    Public Function LabWorkOrLabHourCourseCount_SP(ByVal ReqId As String) As Integer

        Dim intCourseHasOnlyLabWorkAndLabHours As Integer = 0
        Dim db As New SQLDataAccess

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")
        db.AddParameter("@reqId", New Guid(ReqId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        intCourseHasOnlyLabWorkAndLabHours = db.RunParamSQLScalar_SP("dbo.USP_LabWorkOrLabHourCourseCount")
        Return intCourseHasOnlyLabWorkAndLabHours
    End Function
    Public Function isCourseALabWorkOrLabHourCourseByClass(ByVal TermId As String, ByVal ClsSectionId As String) As Boolean
        Dim db As New DataAccess

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim intCourseHasOnlyLabWorkAndLabHours As Integer

        With sb
            .Append(" select distinct Count(GC.Descrip) from arGrdBkWeights GBW,arGrdComponentTypes GC, arGrdBkWgtDetails GD,arClassSections R  where ")
            .Append(" GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId ")
            .Append(" and     GBW.ReqId = R.ReqId and R.ClsSectionId=? and R.TermId=? and GC.SysComponentTypeID is not null ")
            .Append(" and GC.SysComponentTypeID in ")
            .Append(" (select Distinct ResourceId from syResources where Resource in ('Lab Work','Lab Hours'))")
        End With
        db.AddParameter("@ClsSectionId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        intCourseHasOnlyLabWorkAndLabHours = db.RunParamSQLScalar(sb.ToString)
        If intCourseHasOnlyLabWorkAndLabHours >= 1 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function CheckIfGraded(ByVal ClsSectionId As String, ByVal totalweights As Integer) As String

        '   connect to the database
        Dim db As New DataAccess
        Dim sbGetStuEnrollId As New StringBuilder
        Dim strStuEnrollId As String
        Dim sb2 As New StringBuilder
        Dim intValExists As Integer
        Dim strStatus As String

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sbGetStuEnrollId
            .Append(" select Distinct StuEnrollId from arResults where TestId=?  ")
        End With
        db.AddParameter("ClsSectionId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim drgetStuEnrollId As OleDbDataReader = db.RunParamSQLDataReader(sbGetStuEnrollId.ToString)

        While drgetStuEnrollId.Read
            strStuEnrollId = CType(drgetStuEnrollId("StuEnrollId"), Guid).ToString
            With sb2
                .Append(" select Count(*) from arGrdBkResults where StuEnrollId = ? and ClsSectionId=? ")
            End With
            db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ClsSectionId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                intValExists = db.RunParamSQLScalar(sbGetStuEnrollId.ToString)
            Catch ex As Exception
                intValExists = 0
            End Try
            If intValExists = totalweights Then
                strStatus = "Yes"
            Else
                strStatus = "No"
                Exit While
            End If
        End While
        If Not drgetStuEnrollId.IsClosed Then drgetStuEnrollId.Close()
        db.ClearParameters()
        sbGetStuEnrollId.Remove(0, sbGetStuEnrollId.Length)
        Return strStatus
    End Function


    Public Function TransferGradeByTermClassSection(ByVal TermId As String, ByVal ClsSectionId() As String, ByVal clsSectionDescrip() As String) As String
        Dim db As New DataAccess

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder

        Dim x As Integer
        Dim intMinVal, intMaxVal As Integer
        Dim strGrdSysDetailId As String
        Dim sb1 As New StringBuilder
        Dim sb3 As New StringBuilder
        Dim intInitialScore As Decimal
        Dim intWeight As Integer
        Dim intFinalScore As Integer
        Dim strStuEnrollId As String
        Dim intClsSectionId As Integer
        Dim errorMessage As String
        Dim strStudExists As String
        Dim strClsSectionMessage As String
        Dim intTotalWeightsForClsSection As Integer

        Try
            intClsSectionId = ClsSectionId.Length
            While x < intClsSectionId
                Dim sbgetStudent As New StringBuilder
                Dim strFirstName, strLastName As String

                'Get the student names that are registered for this classsection
                With sbgetStudent
                    .Append(" select t1.StuEnrollId,t3.FirstName,t3.LastName from  ")
                    .Append(" arResults t1,arStuEnrollments t2,arStudent t3 ")
                    .Append(" where t1.StuEnrollId = t2.StuEnrollId and t2.StudentId = t3.StudentId and  ")
                    .Append(" TestId = ? ")
                    .Append(" order by t3.FirstName ")
                End With
                db.AddParameter("@ClassSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Dim drgetStudent As OleDbDataReader = db.RunParamSQLDataReader(sbgetStudent.ToString)
                db.ClearParameters()
                sbgetStudent.Remove(0, sbgetStudent.Length)

                While drgetStudent.Read
                    strStudExists = "Yes"
                    strStuEnrollId = CType(drgetStudent("StuEnrollId"), Guid).ToString
                    strFirstName = drgetStudent("FirstName")
                    strLastName = drgetStudent("LastName")

                    'Get The Weights Taken
                    Dim sbGradeWeightsTaken As New StringBuilder
                    Dim intWeightTakenCount As Integer
                    With sbGradeWeightsTaken
                        .Append(" select Count(*) as WeightsTaken from arGrdBkResults where ")
                        .Append(" StuEnrollID = ? and ClsSectionId = ? ")
                    End With
                    db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Dim drGradeWeightsTaken As OleDbDataReader = db.RunParamSQLDataReader(sbGradeWeightsTaken.ToString)

                    'Check If Student has been Graded
                    While drGradeWeightsTaken.Read
                        Try
                            intWeightTakenCount = drGradeWeightsTaken("WeightsTaken")
                        Catch ex As Exception
                            intWeightTakenCount = 0
                        End Try
                    End While
                    If Not drGradeWeightsTaken.IsClosed Then drGradeWeightsTaken.Close()
                    db.ClearParameters()
                    sbGradeWeightsTaken.Remove(0, sbGradeWeightsTaken.Length)

                    'Check if the student has taken total weights defined on classsection
                    'If totalweights is 2 and if student has taken 1 then consider that student 
                    'is not completly graded. changed on 06/22/2006
                    If intWeightTakenCount >= 1 Then
                        Try
                            intTotalWeightsForClsSection = GetWeightsByClassSectionStudentEnrollment(TermId, DirectCast(ClsSectionId.GetValue(x), String))
                        Catch ex As Exception
                            intTotalWeightsForClsSection = 0
                        End Try

                        If intWeightTakenCount < intTotalWeightsForClsSection Then
                            intWeightTakenCount = -1
                        End If
                    End If


                    'If intWeightTakenCount < intWeightCount Then
                    'If the Student has not been graded then get the students name
                    If intWeightTakenCount <= 0 Then


                        ' Modified by Balaji on 02/10/2006

                        With sb1
                            .Append(" SELECT  A.GrdScaleDetailId,A.GrdScaleId,A.MinVal,A.MaxVal,A.GrdSysDetailId, ")
                            .Append(" (select Distinct IsPass from arGradeSystemDetails where GrdSysDetailId=A.GrdSysDetailId) as Pass, ")
                            .Append(" (Select Distinct GrdSystemId from arGradeSystemDetails where GrdSysDetailId=A.GrdSysDetailId) as GrdSystemId ")
                            .Append(" FROM arGradeScaleDetails A WHERE A.GrdScaleId In ")
                            .Append(" (Select GrdScaleId from arClassSections where TermId = ? and ClsSectionId = ?) ")
                            .Append(" order by MinVal ")
                        End With

                        'add parameters
                        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        'execute query
                        Dim drGrade As OleDbDataReader = db.RunParamSQLDataReader(sb1.ToString)
                        db.ClearParameters()
                        sb1.Remove(0, sb1.Length)

                        Dim intPass As Integer
                        Dim strGradeSystemId As String


                        While drGrade.Read()
                            intMinVal = drGrade("MinVal")
                            intMaxVal = drGrade("MaxVal")
                            intPass = drGrade("Pass")
                            If intFinalScore >= intMinVal And intFinalScore <= intMaxVal Then
                                strGrdSysDetailId = CType(drGrade("GrdSysDetailId"), Guid).ToString
                                strGradeSystemId = CType(drGrade("GrdSystemId"), Guid).ToString
                                Exit While
                            End If
                        End While
                        If Not drGrade.IsClosed Then drGrade.Close()

                        'Check If The Grade Is InComplete
                        Dim intInComplete As Boolean
                        Dim sb76 As New StringBuilder
                        With sb76
                            .Append(" select Distinct IsInComplete  from arResults  ")
                            .Append(" where StuEnrollId = ? and TestId = ? ")
                        End With

                        'add parameters
                        db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                        Dim drInComplete As OleDbDataReader = db.RunParamSQLDataReader(sb76.ToString)
                        While drInComplete.Read
                            Try
                                intInComplete = drInComplete("IsInComplete")
                                If intInComplete = False Then
                                    errorMessage &= "     " & strFirstName & " " & strLastName & vbLf
                                End If
                            Catch ex As Exception
                                intInComplete = False
                                errorMessage &= "     " & strFirstName & " " & strLastName & vbLf
                            End Try
                        End While
                        If Not drInComplete.IsClosed Then drInComplete.Close()
                        db.ClearParameters()
                        sb76.Remove(0, sb76.Length)

                        'Check If its InComplete
                        Dim sb67 As New StringBuilder
                        If intInComplete = True Then
                            With sb67
                                .Append(" select GrdSysDetailId from arGradeSystemDetails where GrdSystemId= ?  and IsInComplete=1 ")
                            End With
                            db.AddParameter("@GrdSystemId", strGradeSystemId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                            Dim dr45 As OleDbDataReader = db.RunParamSQLDataReader(sb67.ToString)
                            While dr45.Read
                                Try
                                    strGrdSysDetailId = CType(dr45("GrdSysDetailId"), Guid).ToString
                                Catch ex As Exception
                                    strGrdSysDetailId = DBNull.Value.ToString
                                End Try
                            End While
                            If Not dr45.IsClosed Then dr45.Close()
                            db.ClearParameters()
                            sb1.Remove(0, sb1.Length)
                        Else
                            strGrdSysDetailId = DBNull.Value.ToString
                        End If


                        With sb3
                            .Append("Update arResults set GrdSysDetailId = ? where StuEnrollId = ? and TestId =? and GrdSysDetailId is Null ")
                        End With
                        If strGrdSysDetailId = "" Then
                            db.AddParameter("@GrdSysDetailId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        ElseIf strGrdSysDetailId = DBNull.Value.ToString Then
                            db.AddParameter("@GrdSysDetailId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db.AddParameter("@GrdSysDetailId", strGrdSysDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        End If
                        db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.RunParamSQLExecuteNoneQuery(sb3.ToString)
                        db.ClearParameters()
                        sb3.Remove(0, sb3.Length)
                        'Modification ends here

                    Else
                        'Get The Score
                        With sb
                            .Append(" select A.StuEnrollId,A.ClsSectionId,Sum(A.Score * B.Weight * .01) as FinalScore, ")
                            .Append(" Sum(B.Weight) as TotalWeight from arGrdBkResults A,arGrdBkWgtDetails B where ")
                            .Append(" B.InstrGrdBkWgtDetailId = A.InstrGrdBkWgtDetailId and ")
                            .Append(" StuEnrollId = ? ")
                            .Append(" and ClsSectionId = ? ")
                            .Append(" group by A.StuEnrollId,A.ClsSectionId ")
                        End With

                        'add parameters
                        db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                        'execute query
                        Dim drScore As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                        db.ClearParameters()
                        sb.Remove(0, sb.Length)

                        Dim sb76 As New StringBuilder
                        Dim strSu As Boolean

                        'Check If Course Has Been Marked as Satisfactory/Unsatisfactory
                        With sb76
                            .Append(" select t2.SU as SU from arClassSections t1,arReqs t2 ")
                            .Append(" where  t1.ReqId = t2.ReqId and t1.ClsSectionId = ? ")
                        End With

                        'add parameters
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                        Dim drSatisfactory As OleDbDataReader = db.RunParamSQLDataReader(sb76.ToString)
                        While drSatisfactory.Read
                            Try
                                strSu = drSatisfactory("SU")
                                'strPassFAil = drSatisfactory("PF")
                            Catch Ex As Exception
                                strSu = False
                                'strPassFAil = 2
                            End Try
                        End While
                        If Not drSatisfactory.IsClosed Then drSatisfactory.Close()
                        db.ClearParameters()
                        sb76.Remove(0, sb76.Length)


                        With sb1
                            .Append(" SELECT  A.GrdScaleDetailId,A.GrdScaleId,A.MinVal,A.MaxVal,A.GrdSysDetailId, ")
                            .Append(" (select Distinct IsPass from arGradeSystemDetails where GrdSysDetailId=A.GrdSysDetailId) as Pass, ")
                            .Append(" (Select Distinct GrdSystemId from arGradeSystemDetails where GrdSysDetailId=A.GrdSysDetailId) as GrdSystemId ")
                            .Append(" FROM arGradeScaleDetails A WHERE A.GrdScaleId In ")
                            .Append(" (Select GrdScaleId from arClassSections where TermId = ? and ClsSectionId = ?) ")
                            .Append(" order by MinVal ")
                        End With

                        'add parameters
                        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        'execute query
                        Dim drGrade As OleDbDataReader = db.RunParamSQLDataReader(sb1.ToString)
                        db.ClearParameters()
                        sb1.Remove(0, sb1.Length)

                        Dim intPass As Integer
                        Dim strGradeSystemId As String

                        While drScore.Read()
                            intInitialScore = drScore("FinalScore")
                            intWeight = drScore("TotalWeight")
                            intFinalScore = Math.Round((intInitialScore / intWeight) * 100, 2)
                            ' strStuEnrollId = CType(drScore("StuEnrollId"), Guid).ToString
                            While drGrade.Read()
                                intMinVal = drGrade("MinVal")
                                intMaxVal = drGrade("MaxVal")
                                intPass = drGrade("Pass")
                                If intFinalScore >= intMinVal And intFinalScore <= intMaxVal Then
                                    strGrdSysDetailId = CType(drGrade("GrdSysDetailId"), Guid).ToString
                                    strGradeSystemId = CType(drGrade("GrdSystemId"), Guid).ToString
                                    Exit While
                                End If
                            End While
                            If Not drGrade.IsClosed Then drGrade.Close()
                        End While
                        If Not drScore.IsClosed Then drScore.Close()

                        'Check If The Grade Is InComplete
                        Dim intInComplete As Boolean
                        With sb76
                            .Append(" select Distinct IsInComplete  from arResults  ")
                            .Append(" where StuEnrollId = ? and TestId = ? ")
                        End With

                        'add parameters
                        db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                        Dim drInComplete As OleDbDataReader = db.RunParamSQLDataReader(sb76.ToString)
                        While drInComplete.Read
                            Try
                                intInComplete = drInComplete("IsInComplete")
                            Catch ex As Exception
                                intInComplete = False
                            End Try
                        End While
                        If Not drInComplete.IsClosed Then drInComplete.Close()
                        db.ClearParameters()
                        sb76.Remove(0, sb76.Length)

                        'Check If its InComplete
                        Dim sb67 As New StringBuilder
                        If intInComplete = True Then
                            With sb67
                                .Append(" select GrdSysDetailId from arGradeSystemDetails where GrdSystemId= ?  and IsInComplete=1 ")
                            End With
                            db.AddParameter("@GrdSystemId", strGradeSystemId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                            Dim dr45 As OleDbDataReader = db.RunParamSQLDataReader(sb67.ToString)
                            While dr45.Read
                                Try
                                    strGrdSysDetailId = CType(dr45("GrdSysDetailId"), Guid).ToString
                                Catch ex As Exception
                                    strGrdSysDetailId = DBNull.Value.ToString
                                End Try
                            End While
                            If Not dr45.IsClosed Then dr45.Close()
                            db.ClearParameters()
                            sb1.Remove(0, sb1.Length)
                        Else
                            'If Grade falls under Pass Category and If Course Marked as 
                            'Satisfactory/UnSatisfactory
                            If intPass = 1 And strSu = True Then
                                With sb1
                                    '.Append(" select GrdSysDetailId from arGradeSystemDetails where GrdSystemId= ? and IsPass=1 and IsSatisfactory=1 ")
                                    .Append(" select GrdSysDetailId from arGradeSystemDetails where GrdSystemId= ? and IsPass=1 and Grade='S' ")
                                End With
                                db.AddParameter("@GrdSystemId", strGradeSystemId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                Try
                                    Dim drPassSatisfactory As OleDbDataReader = db.RunParamSQLDataReader(sb1.ToString)
                                    While drPassSatisfactory.Read
                                        strGrdSysDetailId = drPassSatisfactory("GrdSysDetailId")
                                    End While
                                    If Not drPassSatisfactory.IsClosed Then drPassSatisfactory.Close()
                                Catch ex As Exception
                                    strGrdSysDetailId = DBNull.Value.ToString
                                End Try
                                db.ClearParameters()
                                sb1.Remove(0, sb1.Length)

                                'If Grade falls under Fail Category and If Course Marked as 
                                'Satisfactory/UnSatisfactory
                            ElseIf intPass = 0 And strSu = True Then
                                With sb1
                                    '.Append(" select GrdSysDetailId from arGradeSystemDetails where GrdSystemId= ? and IsPass=0 and IsUnSatisfactory=0 ")
                                    .Append(" select GrdSysDetailId from arGradeSystemDetails where GrdSystemId= ? and IsPass=0 and Grade='U' ")
                                End With
                                db.AddParameter("@GrdSystemId", strGradeSystemId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                Try
                                    Dim drPassSatisfactory As OleDbDataReader = db.RunParamSQLDataReader(sb1.ToString)
                                    While drPassSatisfactory.Read
                                        strGrdSysDetailId = drPassSatisfactory("GrdSysDetailId")
                                    End While
                                    If Not drPassSatisfactory.IsClosed Then drPassSatisfactory.Close()
                                Catch ex As Exception
                                    strGrdSysDetailId = DBNull.Value.ToString
                                End Try
                                db.ClearParameters()
                                sb1.Remove(0, sb1.Length)
                            End If
                        End If

                        With sb3
                            .Append("Update arResults set GrdSysDetailId = ? where StuEnrollId = ? and TestId =? and GrdSysDetailId is Null ")
                        End With
                        If strGrdSysDetailId = "" Then
                            db.AddParameter("@GrdSysDetailId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db.AddParameter("@GrdSysDetailId", strGrdSysDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        End If
                        db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.RunParamSQLExecuteNoneQuery(sb3.ToString)
                        db.ClearParameters()
                        sb3.Remove(0, sb3.Length)
                    End If
                End While

                If Not drgetStudent.IsClosed Then drgetStudent.Close()

                'For Student Enrollments
                'Increment value of x and y
                Dim sbUpdate As New StringBuilder
                Try
                    If errorMessage.Length >= 1 Then
                        With sbUpdate
                            .Append("Update arClassSections set IsGraded = 0 where ClsSectionId = ? and TermId =? ")
                        End With
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.RunParamSQLExecuteNoneQuery(sbUpdate.ToString)
                        db.ClearParameters()
                        sbUpdate.Remove(0, sbUpdate.Length)
                    End If
                Catch ex As Exception
                    With sbUpdate
                        .Append("Update arClassSections set IsGraded = 1 where ClsSectionId = ? and TermId =? ")
                    End With
                    db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.RunParamSQLExecuteNoneQuery(sbUpdate.ToString)
                    db.ClearParameters()
                    sbUpdate.Remove(0, sbUpdate.Length)
                End Try
                Try
                    If errorMessage.Length >= 1 Then
                        strClsSectionMessage &= vbLf
                        strClsSectionMessage &= DirectCast(clsSectionDescrip.GetValue(x), String) & vbLf & vbLf
                        strClsSectionMessage &= errorMessage
                        errorMessage = ""
                    End If
                Catch ex As Exception
                End Try
                x = x + 1
            End While


            Try
                If strClsSectionMessage.Length >= 1 Then
                    Dim strReturnMessage As String = "The following students have not been graded" & vbLf
                    Return strReturnMessage & strClsSectionMessage
                Else
                    Return ""
                End If
            Catch ex As Exception
            End Try
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function GetGradedByTermId(ByVal TermId As String, ByVal CampusId As String) As DataSet
        Dim ds As DataSet
        '   connect to the database
        Dim db As New DataAccess
        'Dim sbGetStuEnrollId As New StringBuilder
        'Dim strStuEnrollId, strClsSectionId As String
        'Dim strIsGraded As String

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb

            'Modified by Michelle R. Rodriguez on 07/11/2006
            '**********************************************************
            '********* Instructor Level Grade Book Weight *************
            '**********************************************************
            '
            'The query below retrieves all the class section in a particular term and campus:
            '
            '   TotalWeightsForClsSection column
            '       - total count of grade book weights defined for the class sections on the arGrdBkWgtDetails table
            '       - its computed by counting the records in arGrdBkWgtDetails tables per class section
            '   MinimumWeightsAttempted column
            '       - minimum number of weights attempted for the class section by a registered student 
            '       - its computed by counting the records from arGrdBkResults
            '       - if a student is marked as IsIncomplete, MinimumWeightsAttempted=TotalWeightsForClsSection
            '   GradesComplete column
            '       - Yes or No value
            '       - Yes when MinimumWeightsAttempted=TotalWeightsForClsSection and the number of students not grades equals 0
            '   Grade column
            '       - Yes or No value
            '       - IsGraded column from arClassSections
            '       - No when IsGrades=0 or IsGrades is Null
            '******************************************************
            '
            '
            'The first query retrieves all the class section in the particular term and campus
            'that DO HAVE corresponding entries in table arGrdBkResults table

            'Get The Maximum Number of weights for each classsection
            'Get the number of weights by class section and student enrollment
            'If the student enrollment has an incomplete grade assume the student enrollment
            'as completely graded so instead of min weights attempted put the maximum number
            'of credits for incomplete grade

            'Check if Max Number of credits is equal to min number taken,If they are equal 
            ' then Grade has been completed else NOT GRADED.

            .Append(" select Code,Descrip,ClsSection,IsGrades,Testcls,TotalWeightsForClsSection,MinimumWeightsAttempted, ")
            .Append(" Case when MinimumWeightsAttempted >= TotalWeightsForClsSection  and ")
            .Append(" (			 ")
            .Append("     Select Distinct Count(*) as CountOfStudentsNotGraded from arClassSections C2,arResults C3 where ")
            .Append("     C2.ClsSectionId = Testcls and C2.ClsSectionId = C3.TestId and C2.TermId = '" & TermId & "' ")
            .Append("     and C2.CampusId='" & CampusId & "'  and StuEnrollId not in ")
            .Append("       ( ")
            .Append("         select  Distinct C1.StuEnrollId from    arGrdBkResults C1,arClassSections C2,arResults C3 ")
            .Append("         where   c1.ClsSectionId = c2.ClsSectionId and C2.ClsSectionId = C3.TestId and C2.ClsSectionId = Testcls and ")
            .Append("        C2.TermId = '" & TermId & "'   and C2.CampusId='" & CampusId & "' ")
            .Append("       ) and GrdSysDetailId is  null and (IsInComplete = 0 or IsInComplete is NULL)) = 0 ")
            .Append("      then 'Yes' else 'No' end as GradesComplete,Grade ")
            .Append(" from ")
            .Append(" (  ")
            .Append(" select Distinct  ")
            .Append("              	Code,Descrip,ClsSection,IsGrades,GetMinCreditAttempted.ClsSectionId as TestCls, ")
            'This query is to bring Total weights set for Class Section at Course Level
            .Append(" Case when TotalWeightsForClsSection is NULL or TotalWeightsForClsSection = 0 then ")
            .Append("                		( ")
            .Append(" select ")
            .Append("   Sum(t3.Number) ")
            .Append(" from ")
            .Append("				arGrdBkWeights t1,arClassSections t2,arGrdBkWgtDetails  t3,arGrdComponentTypes t4 ")
            .Append(" where ")
            .Append("				t1.ReqId=t2.ReqId and t1.EffectiveDate <= t2.StartDate and ")
            .Append("				t2.ClsSectionId=GetMinCreditAttempted.ClsSectionId and ")
            .Append("				t3.InstrGrdBkWgtId=t1.InstrGrdBkWgtId and ")
            .Append("               t3.GrdComponentTypeId = t4.GrdComponentTypeId ")


            .Append("               		) else  TotalWeightsForClsSection end as TotalWeightsForClsSection, ")
            .Append("           MinimumWeightsAttempted, ")
            .Append("                	Case when (IsGrades=0 or IsGrades is Null) then 'No' else 'Yes' end as Grade ")
            .Append("                from ")
            .Append("                	( ")
            .Append("                		select Distinct Code,Descrip,ClsSectionId,ClsSection,IsGraded as IsGrades, ")
            .Append("                		( ")
            .Append("                			select  ")
            '.Append("                				Count(t3.Descrip)   ")
            .Append("                				Count(t3.Weight)   ")
            .Append("                			from  ")
            .Append("                				arClassSections t1,arGrdBkWeights t2,arGrdBkWgtDetails t3 ")
            .Append("                			where  ")
            .Append("                				t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId and  ")
            .Append("               			t2.InstrGrdBkWgtId = t3.InstrGrdBkWgtId and ")
            .Append("                				t1.ClsSectionId = b2.ClsSectionId and ")
            .Append("                				t1.TermId = '" & TermId & "'  ")
            .Append("                				and t1.CampusId='" & CampusId & "'   ")
            .Append("               		) as TotalWeightsForClsSection ")
            .Append("               		from ")
            .Append("                			arReqs b1,arClassSections b2,arResults b3 ")
            .Append("               		where 	")
            .Append("               			b1.ReqId = b2.ReqId and b2.ClsSectionId = b3.TestId and ")
            .Append("               			b2.TermId = '" & TermId & "'      ")
            .Append("                			and b2.CampusId='" & CampusId & "'  ")
            .Append("                			AND b1.IsAttendanceOnly = 0 ")
            .Append("                	) ")
            .Append("                	GetMaxGrdAtt, ")
            .Append("                	( ")
            .Append("                		Select Distinct ")
            .Append("                			ClsSectionId, ")
            .Append("                			Min(MinWeightsAttempted) as MinimumWeightsAttempted ")
            .Append("                		from ")
            .Append("                			( ")
            .Append("                				select Distinct ")
            .Append("                					C2.ClsSectionId, ")
            .Append("                					c2.ClsSection, ")
            .Append("                					C1.StuEnrollId, ")
            .Append("                					Case when ")
            .Append("                						(select IsInComplete from arResults ")
            .Append("                							where TestId = C2.ClsSectionId and ")
            .Append("                							StuEnrollId = C1.StuEnrollId ")
            .Append("                						) =  1 ")
            .Append("                					then ")
            .Append("                						(select  ")
            '.Append("                							Count(t3.Descrip)  ")
            .Append("                							Count(t3.Weight)  ")
            .Append("                						from  ")
            .Append("                					arClassSections t1,arGrdBkWeights t2,arGrdBkWgtDetails t3 ")
            .Append("                						where  ")
            .Append("                						t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId and  ")
            .Append("                						t2.InstrGrdBkWgtId = t3.InstrGrdBkWgtId and ")
            .Append("                						t1.ClsSectionId = C2.ClsSectionId and ")
            .Append("                						t1.TermId = '" & TermId & "' ")
            .Append("                						and   t1.CampusId='" & CampusId & "' ")
            .Append("                						)  ")
            .Append("                					else ")
            .Append("                					(select ")
            .Append("                							count(*) from arGrdBkResults ")
            .Append("                						where ")
            .Append("               							ClsSectionId=c2.ClsSectionId and ")
            .Append("                							StuEnrollId=C1.StuEnrollId and Score is not null ")
            .Append("                					) ")
            .Append("                					end ")
            .Append("                					as MinWeightsAttempted ")
            .Append("                				from ")
            .Append("                					arGrdBkResults C1,arClassSections C2,arResults C3 ")
            .Append("               				where ")
            .Append("               			c1.ClsSectionId = c2.ClsSectionId and C2.ClsSectionId = C3.TestId and ")
            .Append("                			C2.TermId = '" & TermId & "'  ")
            .Append("               			and    C2.CampusId='" & CampusId & "' ")
            .Append("                           and C1.StuEnrollId=C3.StuEnrollId ")
            .Append("                           and (")
            .Append("                                  c3.GrdSysDetailId Is null ")
            .Append("                                               OR ")
            .Append("                                  C3.GrdSysDetailId not in(select GrdSysDetailId from arGradeSystemDetails gsd where gsd.IsDrop=1) ")
            .Append("                               )")
            .Append("               			) ")
            .Append("               			GetMinGrdAtt ")
            .Append("               			group by ClsSectionId ")
            .Append("              	) ")
            .Append("               	GetMinCreditAttempted ")
            .Append("               where GetMaxGrdAtt.ClsSectionId = GetMinCreditAttempted.ClsSectionId ")
            .Append("   ) ")
            .Append("   TTT ")
            .Append("   union all ")
            .Append("   ( ")
            .Append("                 select Distinct Code,Descrip,ClsSection,Null,ClsSectionId as TestCls,null as ")
            .Append(" TotalWeightsForClsSection,null as MinimumWeightsAttempted,  ")
            .Append("                Case when (StudentCount = InCompleteGrade) then 'Yes' else 'No' end as GradesComplete,Grade ")
            .Append("                from ")
            .Append("                ( ")
            .Append("               select Distinct t2.Code,t2.Descrip,t1.ClsSection,Null  as IsGrades,t1.ClsSectionId,null as ")
            .Append(" TotalWeightsForClsSection,null as MinimumWeightsAttempted, ")
            .Append("             (select count(*) from arResults where TestId=t1.ClsSectionId and IsInComplete=1) as ")
            .Append(" InCompleteGrade,  ")
            .Append("               (select count(*) from arResults where TestId=t1.ClsSectionId) as StudentCount, ")
            .Append("               'No' as Grade  ")
            .Append(" from arClassSections t1,arReqs  t2,arResults t3 where t1.ReqId = t2.ReqId ")
            .Append(" and t1.ClsSectionId =  t3.TestId and  ")
            .Append(" TermId = '" & TermId & "'   and CampusId='" & CampusId & "'  ")
            .Append("                			AND t2.IsAttendanceOnly = 0 ")
            .Append(" and t1.CLsSectionId not in (Select ClsSectionId from arGrdBkResults) ")
            .Append("               ) ")
            .Append("   GetTT ")
            .Append("   ) ")
            .Append("                  order by Descrip ")

        End With
        'Return db.RunParamSQLDataSet(sb.ToString)
        ds = db.RunParamSQLDataSet(sb.ToString)


        Return ds
    End Function
    Public Function GetWeightsByClassSectionStudentEnrollment(ByVal TermId As String, ByVal ClsSectionId As String) As Integer

        '   connect to the database
        Dim db As New DataAccess

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        Dim intTotalWeights As Integer
        With sb

            'Comment added by Balaji on 10/14/2005

            'Get The Maximum Number of weights for each classsection
            'Get the number of weights by class section and student enrollment
            'If the student enrollment has an incomplete grade assume the student enrollment
            'as completely graded so instead of min weights attempted put the maximum number
            'of credits for incomplete grade

            'Check if Max Number of credits is equal to min number taken,If they are equal 
            ' then Grade has been completed else NOT GRADED.

            .Append(" select  ")
            .Append("         Count(t3.Descrip) as TotalWeights ")
            .Append(" from ")
            .Append("       arClassSections t1,arGrdBkWeights t2,arGrdBkWgtDetails t3  ")
            .Append(" where ")
            .Append("			t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId and  ")
            .Append("           t2.InstrGrdBkWgtId = t3.InstrGrdBkWgtId and  ")
            .Append("           t1.ClsSectionId =?  and ")
            .Append("           t1.TermId = ? ")
            '.Append("           and t1.CampusId=?   ")
        End With
        db.AddParameter("@ClsSectionId", ClsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        'db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Try
            Dim drPassSatisfactory As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            While drPassSatisfactory.Read
                intTotalWeights = drPassSatisfactory("TotalWeights")
            End While
            If Not drPassSatisfactory.IsClosed Then drPassSatisfactory.Close()
        Catch ex As Exception
            intTotalWeights = 0
        End Try
        Return intTotalWeights
    End Function
    Public Function GetGradedByTermIdCourseLevel(ByVal TermId As String, ByVal CampusId As String) As DataSet

        '   connect to the database
        Dim ds As DataSet
        Dim db As New DataAccess


        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim maxNum As Integer = myAdvAppSettings.AppSettings("MaxNoOfGradeBookWeightings")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb

            'Modified by Michelle R. Rodriguez on 07/11/2006
            '******************************************************
            '********* Course Level Grade Book Weight *************
            '******************************************************
            '
            'The query below retrieves all the class section in a particular term and campus:
            '
            '   TotalWeightsForClsSection column
            '       - total count of grade book weights defined on the arGrdBkWgtDetails tables
            '       - its computed by adding up the Number column from the arGrdBkWgtDetails table per each grading component
            '   MinimumWeightsAttempted column
            '       - minimum number of grading component weight attempted for the class section by a student registered
            '       - its computed by adding up the records from arGrdBkResults
            '       - if a student is marked as IsIncomplete, MinimumWeightsAttempted=TotalWeightsForClsSection
            '   GradesComplete column
            '       - Yes or No value
            '       - Yes when MinimumWeightsAttempted=TotalWeightsForClsSection and the number of students not grades equals 0
            '   Grade column
            '       - Yes or No value
            '       - IsGraded column from arClassSections
            '       - No when IsGrades=0 or IsGrades is Null
            '******************************************************
            '
            '
            'The first query retrieves all the class section in the particular term and campus
            'that DO HAVE corresponding entries in table arGrdBkResults table
            .Append("select")
            .Append("	ReqId,Code,")
            .Append("   Descrip,")
            .Append("   ClsSection,")
            .Append("   IsGrades,")
            .Append("   Testcls,")
            .Append("   TotalWeightsForClsSection,")
            .Append("   MinimumWeightsAttempted,")
            .Append("	GradesComplete=(    Case when MinimumWeightsAttempted>=TotalWeightsForClsSection ")
            .Append("				                and 0=(	Select Distinct Count(*) as CountOfStudentsNotGraded ")
            .Append("						                from 	arClassSections C2,arResults C3 ")
            .Append("						                where 	C2.ClsSectionId=Testcls ")
            .Append("							                    and C2.ClsSectionId=C3.TestId ")
            .Append("							                    and C2.TermId='" & TermId & "' ")
            .Append("			    				                and C2.CampusId='" & CampusId & "' ")
            .Append("							                    and StuEnrollId not in (")
            .Append("			        				                            select  Distinct C1.StuEnrollId ")
            .Append("								                                from    arGrdBkResults C1,arClassSections C2,arResults C3 ")
            .Append("			        				                            where   c1.ClsSectionId = c2.ClsSectionId ")
            .Append("									                            and C2.ClsSectionId = C3.TestId ")
            .Append("									                            and C2.ClsSectionId = Testcls ")
            .Append("									                            and C2.TermId='" & TermId & "' ")
            .Append("									                            and C2.CampusId='" & CampusId & "') ")
            .Append("							                    and GrdSysDetailId is null ")
            .Append("							                    and (IsInComplete = 0 or IsInComplete is NULL)) ")
            .Append("	                    then 'Yes' else 'No' end),")
            .Append("   Grade ")
            .Append("from ")
            .Append("	(	select Distinct  ")
            .Append("		             	ReqId,Code,Descrip,ClsSection,IsGrades,GetMinCreditAttempted.ClsSectionId as TestCls,")
            .Append("				        TotalWeightsForClsSection=(")
            .Append("							    select	Sum(coalesce(t3.Number," & maxNum & ")) ")
            .Append("							    from 	arGrdBkWeights t1,arClassSections t2,arGrdBkWgtDetails t3,arGrdComponentTypes t4 ")
            .Append("							    where 	t1.ReqId=t2.ReqId and t1.EffectiveDate<=t2.StartDate ")
            .Append("			            				and t2.ClsSectionId=GetMaxGrdAtt.ClsSectionId ")
            .Append("			            				and t3.InstrGrdBkWgtId=t1.InstrGrdBkWgtId and t3.Weight>=0")
            .Append("		              				    and t3.GrdComponentTypeId=t4.GrdComponentTypeId), ")
            .Append("				        MinimumWeightsAttempted, ")
            .Append("               		Grade=(Case when (IsGrades=0 or IsGrades is Null) then 'No' else 'Yes' end) ")
            .Append("		from	(	select Distinct b1.Reqid,Code,Descrip,ClsSectionId,ClsSection,IsGraded as IsGrades ")
            .Append("              			from 	arReqs b1,arClassSections b2,arResults b3 ")
            .Append("              			where 	b1.ReqId=b2.ReqId and b2.ClsSectionId=b3.TestId and b1.IsAttendanceOnly=0 ")
            .Append("	              			    and b2.TermId='" & TermId & "' ")
            .Append("	               			    and b2.CampusId='" & CampusId & "' ")
            .Append("                			AND b1.IsAttendanceOnly = 0 ")
            'Code modified to fix issue 15545
            'The class section dropdown in Transfer Instructor Grade Book Results should not show Externship classes
            'modification starts here
            '.Append(" and b1.IsExternship <> 1 ")
            'modification ends here 
            .Append("               		)GetMaxGrdAtt, ")
            .Append("               		(	Select Distinct ClsSectionId,Min(MinWeightsAttempted) as MinimumWeightsAttempted ")
            .Append("               			from 	(	select Distinct C2.ClsSectionId,c2.ClsSection,C1.StuEnrollId,")
            .Append("               					            MinWeightsAttempted=(")
            .Append("							                        Case when 1=(	select 	IsInComplete from arResults ")
            .Append("               								                    where 	TestId = C2.ClsSectionId ")
            .Append("               									                and StuEnrollId = C1.StuEnrollId)")
            .Append("               						            then (	select	Sum(coalesce(t3.Number," & maxNum & ")) ")
            .Append("								                            from 	arGrdBkWeights t1,arClassSections t2,arGrdBkWgtDetails t3,arGrdComponentTypes t4 ")
            .Append("								                            where 	t1.ReqId=t2.ReqId and t1.EffectiveDate<=t2.StartDate ")
            .Append("			            					                        and t2.ClsSectionId=c1.ClsSectionId ")
            .Append("			            					                        and t3.InstrGrdBkWgtId=t1.InstrGrdBkWgtId and t3.Weight>=0")
            .Append("			              					                        and t3.GrdComponentTypeId = t4.GrdComponentTypeId) ")
            .Append("               						            else (	select 	count(*) ")
            .Append("								                            from 	arGrdBkResults ")
            .Append("               							                where 	ClsSectionId=c2.ClsSectionId ")
            .Append("               								                    and StuEnrollId=C1.StuEnrollId and Score is not null)")
            .Append("							                        end) ")
            .Append("               					    from 	arGrdBkResults C1,arClassSections C2,arResults C3 ")
            .Append("              					        where 	c1.ClsSectionId=c2.ClsSectionId ")
            .Append("							                    and C2.ClsSectionId=C3.TestId ")
            .Append("               						        and C2.TermId='" & TermId & "'  ")
            .Append("              						            and C2.CampusId='" & CampusId & "' ")
            .Append("              				        )GetMinGrdAtt ")
            .Append("              			    group by ClsSectionId ")
            .Append("                       )GetMinCreditAttempted ")
            .Append("		where GetMaxGrdAtt.ClsSectionId=GetMinCreditAttempted.ClsSectionId) TTT ")
            .Append("union all ")
            'The query below retrieves all the class section in the particular term and campus
            'that DO NOT have entries in table arGrdBkResults table
            .Append("select Distinct")
            .Append("   ReqId,Code,")
            .Append("   Descrip,")
            .Append("   ClsSection,")
            .Append("   null as IsGrades,")
            .Append("   ClsSectionId as TestCls,")
            .Append("	null as TotalWeightsForClsSection,")
            .Append("	null as MinimumWeightsAttempted, ")
            .Append("	GradesComplete=(Case when (StudentCount=InCompleteGrade) then 'Yes' else 'No' end),")
            .Append("	Grade ")
            .Append("from	(      select Distinct 	t2.ReqId,t2.Code,t2.Descrip,t1.ClsSection,Null as IsGrades,")
            .Append("				                t1.ClsSectionId,")
            .Append("				                null as TotalWeightsForClsSection,")
            .Append("				                null as MinimumWeightsAttempted, ")
            .Append("            			        InCompleteGrade=(select count(*) from arResults where TestId=t1.ClsSectionId and IsInComplete=1),  ")
            .Append("				                StudentCount=(select count(*) from arResults where TestId=t1.ClsSectionId), ")
            .Append("              			        'No' as Grade  ")
            .Append("		        from 	arClassSections t1,arReqs t2,arResults t3 ")
            .Append("		        where 	t1.ReqId=t2.ReqId and t2.IsAttendanceOnly=0 ")
            .Append("			            and t1.ClsSectionId= t3.TestId ")
            .Append("			            and TermId='" & TermId & "' ")
            .Append("                			AND t2.IsAttendanceOnly = 0 ")
            .Append("			            and CampusId='" & CampusId & "'  ")
            'Code modified to fix issue 15545
            'The class section dropdown in Transfer Instructor Grade Book Results should not show Externship classes
            'modification starts here
            '.Append(" and t2.IsExternship <> 1 ")
            'modification ends here 
            .Append("			            and t1.CLsSectionId not in (Select ClsSectionId from arGrdBkResults)")
            .Append("       )GetTT ")
            .Append("order by Descrip ")
        End With
        ds = db.RunParamSQLDataSet(sb.ToString)

        Try
            Dim dtCourses As DataTable = ds.Tables(0)
            If dtCourses.Rows.Count > 0 Then
                For Each row As DataRow In dtCourses.Rows
                    If (IsCourseALab(row("ReqId").ToString, "") = True) Then
                        row.Delete()
                    End If
                Next
            End If
            ds.AcceptChanges()
        Catch ex As Exception
        End Try
        Return ds
    End Function
    Public Function IsCourseALab(ByVal reqid As String, ByVal instrGrdBkWgtId As String) As Boolean
        'issue: should not show the class sections that have only lab work or lab hours as part of the course
        'mantis entry:11133
        Dim boolValue As Boolean = False

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        If myAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" Then
            boolValue = CheckIfCourseHasOnlyLabWorkAndLabHours(reqid)
        Else
            'boolValue = CheckIfCourseHasOnlyLabWorkAndLabHoursForInstructorLevel(instrGrdBkWgtId)
            ''Added by Saraswathi lakshmanan on march 31 2009
            ''To show the Class Sections belonging to the instructor , eventhough the courses are not applied.
            If instrGrdBkWgtId = "" Then
                boolValue = False
            Else
                boolValue = CheckIfCourseHasOnlyLabWorkAndLabHoursForInstructorLevel(instrGrdBkWgtId)
            End If

        End If
        Return boolValue
    End Function
    'Private Function GetCourseSystemComponentID(ByVal reqid As String) As String()
    '    Dim db As New DataAccess
    '    Dim ds As New DataSet

    '    Dim myAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        myAdvAppSettings = New AdvAppSettings
    '    End If

    '    db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
    '    Dim sb As New StringBuilder

    '    With sb
    '        .Append(" select distinct SysComponentTypeID from arGrdBkWeights GBW,arGrdComponentTypes GC, arGrdBkWgtDetails GD  where ")
    '        .Append(" GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId ")
    '        .Append(" and     GBW.ReqId = ? and SysComponentTypeID is not null ")
    '    End With
    '    db.AddParameter("@ReqId", reqid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

    '    ds = db.RunParamSQLDataSet(sb.ToString)
    '    Dim rtn(ds.Tables(0).Rows.Count - 1) As String
    '    For i As Integer = 0 To rtn.Length - 1
    '        rtn(i) = ds.Tables(0).Rows(i)("SysComponentTypeID").ToString
    '    Next
    '    Return rtn
    'End Function

    Private Function CheckIfCourseHasOnlyLabWorkAndLabHours(ByVal reqid As String) As Boolean
        Dim db As New DataAccess

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim intCourseHasOnlyLabWorkAndLabHours As Integer = 0

        With sb
            .Append(" select distinct Count(GC.Descrip) from arGrdBkWeights GBW,arGrdComponentTypes GC, arGrdBkWgtDetails GD  where ")
            .Append(" GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId ")
            .Append(" and     GBW.ReqId = ? and GC.SysComponentTypeID is not null ")
            .Append(" and GC.SysComponentTypeID not in ")
            .Append(" (select Distinct ResourceId from syResources where Resource in ('Lab Work','Lab Hours'))")
        End With
        db.AddParameter("@ReqId", reqid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        intCourseHasOnlyLabWorkAndLabHours = db.RunParamSQLScalar(sb.ToString)

        If Not CheckIfCourseHasGradeWeights(reqid) Then
            Return False
        ElseIf intCourseHasOnlyLabWorkAndLabHours >= 1 Then
            Return False
        Else
            Return True
        End If
    End Function
    Private Function CheckIfCourseHasGradeWeights(ByVal reqid As String) As Boolean
        Dim db As New DataAccess

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim intCourseHasGradeWeights As Integer = 0

        With sb
            .Append(" select distinct Count(GC.Descrip) from arGrdBkWeights GBW,arGrdComponentTypes GC, arGrdBkWgtDetails GD  where ")
            .Append(" GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId ")
            .Append(" and     GBW.ReqId = ? and GC.SysComponentTypeID is not null ")
        End With
        db.AddParameter("@ReqId", reqid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        intCourseHasGradeWeights = db.RunParamSQLScalar(sb.ToString)

        If intCourseHasGradeWeights >= 1 Then
            Return True 'If course has other grading components apart from lab work or lab hours, then it return false
        Else
            Return False 'if course has only lab work or lab hours, then it returns true
        End If
    End Function
    Private Function CheckIfCourseHasOnlyLabWorkAndLabHoursForInstructorLevel(ByVal InstrGrdBkWgtId As String) As Boolean
        Dim db As New DataAccess

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim intCourseHasOnlyLabWorkAndLabHours As Integer = 0

        With sb
            .Append(" select distinct Count(GC.Descrip) from arGrdBkWeights GBW,arGrdComponentTypes GC, arGrdBkWgtDetails GD  where ")
            .Append(" GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId ")
            .Append(" and     GBW.InstrGrdBkWgtId = ? and GC.SysComponentTypeID is not null ")
            .Append(" and GC.SysComponentTypeID not in ")
            .Append(" (select Distinct ResourceId from syResources where Resource in ('Lab Work','Lab Hours'))")
        End With
        db.AddParameter("@ReqId", InstrGrdBkWgtId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        intCourseHasOnlyLabWorkAndLabHours = db.RunParamSQLScalar(sb.ToString)

        If intCourseHasOnlyLabWorkAndLabHours >= 1 Then
            Return False 'If course has other grading components apart from lab work or lab hours, then it return false
        Else
            Return True 'if course has only lab work or lab hours, then it returns true
        End If
    End Function
    '   Private Function GetAllSystemComponentID() As String()
    '       Dim db As New DataAccess
    '       Dim ds As New DataSet

    '       Dim myAdvAppSettings As AdvAppSettings
    '       If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '           myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '       Else
    '           myAdvAppSettings = New AdvAppSettings
    '       End If

    '       db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
    '       Dim sb As New StringBuilder
    '       With sb
    '           .Append(" select ResourceId from syResources where ResourceId in (select distinct SysComponentTypeId from arGrdComponentTypes) ")
    '       End With

    '       ds = db.RunSQLDataSet(sb.ToString)
    '       Dim rtn(ds.Tables(0).Rows.Count - 1) As String
    '       For i As Integer = 0 To rtn.Length - 1
    '           rtn(i) = ds.Tables(0).Rows(i)("ResourceId").ToString
    '       Next
    '       Return rtn
    '   End Function

    'Private Function GetNonLabSystemComponentID() As String()
    '       Dim db As New DataAccess
    '       Dim ds As New DataSet

    '       Dim myAdvAppSettings As AdvAppSettings
    '       If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '           myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '       Else
    '           myAdvAppSettings = New AdvAppSettings
    '       End If

    '       db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
    '       Dim sb As New StringBuilder


    '       With sb
    '           .Append(" select ResourceId from syResources where ResourceId in (select distinct SysComponentTypeId from arGrdComponentTypes) and Resource not in('Lab Work','Lab Hours') ")
    '       End With

    '       ds = db.RunSQLDataSet(sb.ToString)
    '       Dim rtn(ds.Tables(0).Rows.Count - 1) As String
    '       For i As Integer = 0 To rtn.Length - 1
    '           rtn(i) = ds.Tables(0).Rows(i)("ResourceId").ToString
    '       Next
    '       Return rtn
    '   End Function

    Public Function GetTotalWeightsAtCourseLevel(ByVal TermId As String, ByVal CampusId As String, ByVal ClsSectionId As String) As Integer
        '   connect to the database
        Dim db As New DataAccess

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        Dim maxNum As Integer = myAdvAppSettings.AppSettings("MaxNoOfGradeBookWeightings")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb

            'Comment added by Balaji on 10/14/2005

            'Get The Maximum Number of weights for each classsection
            'Get the number of weights by class section and student enrollment
            'If the student enrollment has an incomplete grade assume the student enrollment
            'as completely graded so instead of min weights attempted put the maximum number
            'of credits for incomplete grade

            'Check if Max Number of credits is equal to min number taken,If they are equal 
            ' then Grade has been completed else NOT GRADED.

            .Append(" select TotalWeightsForClsSection ")
            .Append(" from ")
            .Append(" (  ")
            .Append(" select Distinct  ")
            .Append("              	Code,Descrip,ClsSection,IsGrades,GetMinCreditAttempted.ClsSectionId as TestCls, ")
            .Append("        	TotalWeightsForClsSection,MinimumWeightsAttempted, ")
            .Append("                	Case when (IsGrades=0 or IsGrades is Null) then 'No' else 'Yes' end as Grade ")
            .Append("                from ")
            .Append("                	( ")
            .Append("                		select Distinct Code,Descrip,ClsSectionId,ClsSection,IsGraded as IsGrades, ")
            .Append("                		( ")
            .Append(" select ")
            .Append("   Sum(coalesce(t3.Number," & maxNum & ")) ")
            .Append(" from ")
            .Append("				arGrdBkWeights t1,arClassSections t2,arGrdBkWgtDetails  t3,arGrdComponentTypes t4 ")
            .Append(" where ")
            .Append("				t1.ReqId=t2.ReqId and t1.EffectiveDate <= t2.StartDate and ")
            .Append("				t2.ClsSectionId=b2.ClsSectionId and ")
            .Append("				t3.InstrGrdBkWgtId=t1.InstrGrdBkWgtId and  ")

            .Append(" t3.Weight>=0 and ")
            .Append("               t3.GrdComponentTypeId = t4.GrdComponentTypeId ")
            .Append(" and t1.InstrGrdBkWgtId in  	(select Top 1 InstrGrdBkWgtId from arGrdBkWeights where ReqId=t1.ReqId and EffectiveDate<=t2.StartDate order by EffectiveDate desc) ")
            '.Append(" and t1.EffectiveDate = (select max(arGrdBkWeights.EffectiveDate) from arGrdBkWeights where ReqId=t2.ReqId) ")
            .Append("               		) as TotalWeightsForClsSection ")
            .Append("               		from ")
            .Append("                			arReqs b1,arClassSections b2,arResults b3 ")
            .Append("               		where 	")
            .Append("               			b1.ReqId = b2.ReqId and b2.ClsSectionId = b3.TestId and ")
            .Append("               			b2.TermId = '" & TermId & "'      ")
            .Append("                			and b2.CampusId='" & CampusId & "'  ")
            .Append("                	) ")
            .Append("                	GetMaxGrdAtt, ")
            .Append("                	( ")
            .Append("                		Select Distinct ")
            .Append("                			ClsSectionId, ")
            .Append("                			Min(MinWeightsAttempted) as MinimumWeightsAttempted ")
            .Append("                		from ")
            .Append("                			( ")
            .Append("                				select Distinct ")
            .Append("                					C2.ClsSectionId, ")
            .Append("                					c2.ClsSection, ")
            .Append("                					C1.StuEnrollId, ")
            .Append("                					Case when ")
            .Append("                						(select IsInComplete from arResults ")
            .Append("                							where TestId = C2.ClsSectionId and ")
            .Append("                							StuEnrollId = C1.StuEnrollId ")
            .Append("                						) =  1 ")
            .Append("                					then ")
            .Append("                					    (Select Sum(coalesce(t3.Number," & maxNum & ")) ")
            .Append("                					    from 	arGrdBkWeights t1,arClassSections t2,arGrdBkWgtDetails t3,arGrdComponentTypes t4 ")
            .Append("                					    where   t1.ReqId = t2.ReqId And t1.EffectiveDate <= t2.StartDate ")
            .Append("                					            and t2.ClsSectionId=c1.ClsSectionId ")
            .Append("                					            and t3.InstrGrdBkWgtId=t1.InstrGrdBkWgtId and t3.Weight>=0")
            .Append("                					            and t3.GrdComponentTypeId = t4.GrdComponentTypeId and t1.InstrGrdBkWgtId in ")
            .Append("                                (select Top 1 InstrGrdBkWgtId from arGrdBkWeights where ReqId=t1.ReqId and EffectiveDate<=t2.StartDate order by EffectiveDate desc) ) ")

            .Append("                					else ")
            .Append("                					(select ")
            .Append("                							count(*) from arGrdBkResults ")
            .Append("                						where ")
            .Append("               							ClsSectionId=c2.ClsSectionId and ")
            .Append("                							StuEnrollId=C1.StuEnrollId ")
            .Append("                					) ")
            .Append("                					end ")
            .Append("                					as MinWeightsAttempted ")
            .Append("                				from ")
            .Append("                					arGrdBkResults C1,arClassSections C2,arResults C3 ")
            .Append("               				where ")
            .Append("               			c1.ClsSectionId = c2.ClsSectionId and C2.ClsSectionId = C3.TestId and ")
            .Append("                			C2.TermId = '" & TermId & "'  ")
            .Append("               			and    C2.CampusId='" & CampusId & "' ")
            .Append("               			) ")
            .Append("               			GetMinGrdAtt ")
            .Append("               			group by ClsSectionId ")
            .Append("              	) ")
            .Append("               	GetMinCreditAttempted ")
            .Append("               where GetMaxGrdAtt.ClsSectionId = GetMinCreditAttempted.ClsSectionId ")
            .Append("               and GetMaxGrdAtt.ClsSectionId='" & ClsSectionId & "' ")
            .Append("   ) ")
            .Append("   TTT ")

        End With
        Dim intTotalWeights As Decimal = 0.0
        Try
            Dim drPassSatisfactory As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
            While drPassSatisfactory.Read
                If Not drPassSatisfactory("TotalWeightsForClsSection") Is DBNull.Value Then intTotalWeights = drPassSatisfactory("TotalWeightsForClsSection") Else intTotalWeights = 0
            End While
            If Not drPassSatisfactory.IsClosed Then drPassSatisfactory.Close()
        Catch ex As Exception
            intTotalWeights = 0
        End Try
        Return intTotalWeights
    End Function
    Public Function GetStudentsInClassSectionForCourseLevel(ByVal clsSectionId As String) As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Dim sbgetStudent As New StringBuilder

        '   build the sql query
        'Get the student names that are registered for this classsection
        With sbgetStudent

            .Append(" SELECT  StuEnrollId ,FirstName ,LastName ,IsInComplete , ")
            .Append(" GradesComplete = ( CASE WHEN MinWeightsAttempted >= TotalWeightsForClsSection THEN 'Yes'     ELSE 'No'  END )  ")
            .Append(", ExtnCount, PrgVerCode, PrgVerDescrip ")
            .Append("  FROM    ( ")
            .Append(" select    t1.StuEnrollId,t3.FirstName,t3.LastName,t1.IsInComplete, ")
            .Append(" MinWeightsAttempted = ( CASE WHEN 1 = ( SELECT ")
            .Append(" IsInComplete ")
            .Append(" FROM ")
            .Append(" arResults ")
            .Append(" WHERE ")
            .Append(" TestId = t1.TestId ")
            .Append(" AND StuEnrollId = t1.StuEnrollId ")
            .Append(" ) ")
            .Append(" THEN ( SELECT ")
            .Append(" SUM(COALESCE(G3.Number,100)) ")
            .Append(" FROM  arGrdBkWeights G1 , ")
            .Append(" arClassSections G2 , ")
            .Append(" arGrdBkWgtDetails G3 , ")
            .Append(" arGrdComponentTypes G4 ")
            .Append(" WHERE G1.ReqId = G2.ReqId ")
            .Append(" AND G1.EffectiveDate <= t2.StartDate ")
            .Append(" AND G2.ClsSectionId = t1.Testid ")
            .Append(" AND G3.InstrGrdBkWgtId = G1.InstrGrdBkWgtId ")
            .Append(" AND G3.Weight >= 0 ")
            .Append(" AND G3.GrdComponentTypeId = G4.GrdComponentTypeId ")
            .Append(" AND G1.InstrGrdBkWgtId IN ( ")
            .Append(" SELECT TOP 1 InstrGrdBkWgtId FROM arGrdBkWeights ")
            .Append(" WHERE ReqId = G1.ReqId AND EffectiveDate <= G2.StartDate ")
            .Append(" ORDER BY EffectiveDate DESC ) ")
            .Append(" ) ")
            .Append(" ELSE ( SELECT ")
            .Append(" COUNT(*) ")
            .Append(" FROM  arGrdBkResults ")
            .Append(" WHERE ClsSectionId = t1.TestId ")
            .Append(" AND StuEnrollId = t1.StuEnrollId ")
            .Append(" AND Score IS NOT NULL ")
            .Append(" ) ")
            .Append(" END ) , ")
            .Append(" TotalWeightsForClsSection = ( SELECT    SUM(COALESCE(G3.Number,100)) ")
            .Append("           FROM      arGrdBkWeights G1 , ")
            .Append("           arClassSections G2 , ")
            .Append(" arGrdBkWgtDetails G3 , ")
            .Append("                                                             arGrdComponentTypes G4 ")
            .Append("                                         WHERE     G1.ReqId = G2.ReqId ")
            .Append("                                         AND G1.EffectiveDate <= G2.StartDate ")
            .Append("                               AND G2.ClsSectionId = t1.Testid ")
            .Append("                     AND G3.InstrGrdBkWgtId = G1.InstrGrdBkWgtId ")
            .Append("           AND G3.Weight >= 0 ")
            .Append(" AND G3.GrdComponentTypeId = G4.GrdComponentTypeId AND G4.SysComponentTypeId NOT IN (544) ")
            .Append(" AND G1.InstrGrdBkWgtId IN ( ")
            .Append(" SELECT TOP 1 InstrGrdBkWgtId FROM arGrdBkWeights ")
            .Append(" WHERE ReqId = G1.ReqId AND EffectiveDate <= G2.StartDate ")
            .Append(" ORDER BY EffectiveDate DESC ) ")
            .Append(" ), ")
            .Append(" ExtnCount = ( SELECT    COUNT(*)  ")
            .Append("           FROM      arGrdBkWeights G1 , ")
            .Append("           arClassSections G2 , ")
            .Append(" arGrdBkWgtDetails G3 , ")
            .Append("                                                             arGrdComponentTypes G4 ")
            .Append("                                         WHERE     G1.ReqId = G2.ReqId ")
            .Append("                                         AND G1.EffectiveDate <= G2.StartDate ")
            .Append("                               AND G2.ClsSectionId = t1.Testid ")
            .Append("                     AND G3.InstrGrdBkWgtId = G1.InstrGrdBkWgtId ")
            .Append("           AND G3.Weight >= 0 ")
            .Append(" AND G3.GrdComponentTypeId = G4.GrdComponentTypeId AND G4.SysComponentTypeId =544 ")
            .Append(" AND G1.InstrGrdBkWgtId IN ( ")
            .Append(" SELECT TOP 1 InstrGrdBkWgtId FROM arGrdBkWeights ")
            .Append(" WHERE ReqId = G1.ReqId AND EffectiveDate <= G2.StartDate ")
            .Append(" ORDER BY EffectiveDate DESC ) ")
            .Append(" ) ")
            .Append(", t4.PrgVerCode, t4.PrgVerDescrip ")
            .Append(" from      arResults t1,arStuEnrollments t2,arStudent t3, arPrgVersions AS T4  ")
            .Append(" where     t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId AND t2.PrgVerId = T4.PrgVerId AND t1.IsCourseCompleted=0 ")
            .Append("           and TestId = ? ")
            .Append(" AND t2.StuEnrollId NOT IN ")
            .Append(" (SELECT StuEnrollId FROM arResults WHERE (GrdSysDetailId IS NOT NULL OR Score IS NOT NULL) ")
            .Append(" AND StuEnrollId=t2.StuEnrollId and TestId=t1.TestId and ")
            .Append(" StuEnrollId NOT in (SELECT StuEnrollId FROM arGrdBkResults WHERE StuEnrollId=t2.StuEnrollId AND ClsSectionId=t1.TestId)) ")
            .Append(" ) Res ORDER BY FirstName ")
        End With
        db.AddParameter("@ClassSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sbgetStudent.ToString)
        Return ds.Tables(0)
    End Function
    Public Function GetStudentsInClassSectionForInstructorLevel(ByVal clsSectionId As String) As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Dim sbgetStudent As New StringBuilder

        '   build the sql query
        'Get the student names that are registered for this classsection
        With sbgetStudent

            .Append(" SELECT  StuEnrollId ,FirstName ,LastName ,IsInComplete , ")
            .Append(" GradesComplete = ( CASE WHEN MinWeightsAttempted >= TotalWeightsForClsSection THEN 'Yes'     ELSE 'No'  END ) ")
            .Append(", PrgVerCode, PrgVerDescrip ")
            .Append(" FROM    ( ")
            .Append(" select    t1.StuEnrollId,t3.FirstName,t3.LastName,t1.IsInComplete, ")
            .Append(" CASE WHEN ( SELECT  IsInComplete ")
            .Append("                                 FROM    arResults ")
            .Append("                      WHERE   TestId = t4.ClsSectionId ")
            .Append("                   AND StuEnrollId = t2.StuEnrollId ")
            .Append("                               ) = 1 ")
            .Append("               THEN ( SELECT  COUNT(G3.Weight) ")
            .Append("           FROM    arClassSections G1 , ")
            .Append("        arGrdBkWeights G2 , ")
            .Append("                                         arGrdBkWgtDetails G3 ")
            .Append("                      WHERE   G1.InstrGrdBkWgtId = G2.InstrGrdBkWgtId ")
            .Append("                   AND G2.InstrGrdBkWgtId = G3.InstrGrdBkWgtId ")
            .Append("        AND G1.ClsSectionId = t1.TestId ")
            .Append("                                         AND G1.TermId = t4.TermId ")
            .Append("                              AND G1.CampusId = t2.CampusId ")
            .Append("         ) ")
            .Append("                          ELSE ( SELECT  COUNT(*) ")
            .Append("                      FROM    arGrdBkResults mwaGBR,arGrdBkWgtDetails mwaGBRD, arClassSections mwaCS ")
            .Append("           WHERE   mwaGBR.ClsSectionId = t1.TestId ")
            .Append("        AND mwaGBR.StuEnrollId = t2.StuEnrollId ")
            .Append("        AND mwaCS.ClsSectionId = mwaGBR.ClsSectionId ")
            .Append("        AND mwaCS.InstrGrdBkWgtId = mwaGBRD.InstrGrdBkWgtId ")
            .Append("        AND mwaGBR.InstrGrdBkWgtDetailId = mwaGBRD.InstrGrdBkWgtDetailId ")
            .Append("                                         AND mwaGBR.Score IS NOT NULL ")
            .Append("                    ) ")
            .Append("                     END AS MinWeightsAttempted , ")
            .Append("                     ( SELECT    COUNT(G3.Weight) ")
            .Append("                        FROM      arClassSections G1 , ")
            .Append("                       arGrdBkWeights G2 , ")
            .Append("            arGrdBkWgtDetails G3 ")
            .Append("                                   WHERE     G1.InstrGrdBkWgtId = G2.InstrGrdBkWgtId ")
            .Append("                                  AND G2.InstrGrdBkWgtId = G3.InstrGrdBkWgtId ")
            .Append("                       AND G1.ClsSectionId = t4.ClsSectionId ")
            .Append("            AND G1.TermId = t4.TermId ")
            .Append("                                             AND G1.CampusId = t2.CampusId ")
            .Append("                      ) AS TotalWeightsForClsSection ")
            .Append(", T5.PrgVerCode, T5.PrgVerDescrip ")
            .Append(" from      arResults t1, arStuEnrollments t2, arStudent t3, arClassSections t4, arPrgVersions AS T5  ")
            .Append(" where     t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId AND t1.TestId = t4.ClsSectionId AND t2.PrgVerId = T5.PrgVerId ")
            .Append("           and TestId = ? ")
            .Append(" AND t2.StuEnrollId NOT IN ")
            .Append(" (SELECT StuEnrollId FROM arResults WHERE (GrdSysDetailId IS NOT NULL OR Score IS NOT NULL) ")
            .Append(" AND StuEnrollId=t2.StuEnrollId and TestId=t1.TestId and ")
            .Append(" StuEnrollId NOT in (SELECT StuEnrollId FROM arGrdBkResults WHERE StuEnrollId=t2.StuEnrollId AND ClsSectionId=t1.TestId)) ")
            .Append(" AND (t1.GrdSysDetailId is null OR t1.GrdSysDetailId not in(select GrdSysDetailId from arGradeSystemDetails gsd where gsd.IsDrop=1)) ")
            .Append(" ) Res ORDER BY FirstName ")
        End With
        db.AddParameter("@ClassSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        ds = db.RunParamSQLDataSet(sbgetStudent.ToString)
        Return ds.Tables(0)
    End Function
    Public Function GradeWeightsTaken(ByVal stuEnrollId As String, ByVal clsSectionId As String) As Integer
        Dim db As New DataAccess
        Dim weightinglevel As String

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        weightinglevel = myAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower

        Dim sbGradeWeightsTaken As New StringBuilder
        Dim intWeightTakenCount As Integer = 0

        'Get The Weights Taken
        With sbGradeWeightsTaken
            If weightinglevel = "courselevel" Then
                .Append("   Select Distinct Count(*) as WeightsTaken ")
                .Append("   from    arGrdBkResults gbr ")
                .Append("           join arGrdBkWgtDetails gbwd ")
                .Append("               on gbwd.InstrGrdBkWgtDetailId = gbr.InstrGrdBkWgtDetailId ")
                .Append("           join (select top 1 gbw.InstrGrdBkWgtId, gbw.EffectiveDate ")
                .Append("                       from arGrdBkWeights gbw ")
                .Append("                           join  arClassSections cs on gbw.ReqId = cs.ReqId ")
                .Append("                       where gbw.EffectiveDate <= cs.StartDate ")
                .Append("                           and cs.ClsSectionId = ? ")
                .Append("                       order by gbw.EffectiveDate desc) effective_gbw ")
                .Append("               on effective_gbw.InstrGrdBkWgtId = gbwd.InstrGrdBkWgtId ")
                .Append("   where   gbr.ClsSectionId=? ")
                .Append("           and gbr.StuEnrollId=? ")
                .Append("			and	Score is NOT NULL ")
                .Append("			and	IsCompGraded = 1 ")
            Else
                .Append("   Select Distinct Count(*) as WeightsTaken ")
                .Append("   from	arGrdBkResults GBR, arGrdComponentTypes GCT, arGrdBkWgtDetails GBWD, arClassSections CS ")
                .Append("   where 	GBR.ClsSectionId=? and GBR.StuEnrollId=? ")
                .Append("   and	GBR.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
                .Append("   and	GBWD.GrdComponentTypeId = GCT.GrdComponentTypeId  and Score is NOT NULL ")
                .Append("   and	IsCompGraded=1   ")
                .Append("   and	CS.ClsSectionId=GBR.ClsSectionId ")
                .Append("   and	CS.InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId ")
            End If
        End With
        If weightinglevel = "courselevel" Then
            'add additional parameter
            db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        Dim drGradeWeightsTaken As OleDbDataReader = db.RunParamSQLDataReader(sbGradeWeightsTaken.ToString)
        'Check If Student has been Graded
        While drGradeWeightsTaken.Read
            Try
                intWeightTakenCount = drGradeWeightsTaken("WeightsTaken")
            Catch ex As Exception
                intWeightTakenCount = 0
            End Try
        End While
        If Not drGradeWeightsTaken.IsClosed Then drGradeWeightsTaken.Close()
        Return intWeightTakenCount
    End Function
    Public Function GradeWeightsTakenForClinics(ByVal stuEnrollId As String, ByVal clsSectionId As String) As Integer
        Dim db As New DataAccess
        Dim weightinglevel As String

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
        weightinglevel = myAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower

        Dim sbGradeWeightsTaken As New StringBuilder
        Dim intWeightTakenCount As Integer = 0

        'Get The Weights Taken
        With sbGradeWeightsTaken
            If weightinglevel = "courselevel" Then
                .Append("   Select Distinct Count(*) as WeightsTaken ")
                .Append("   from    arGrdBkResults gbr ")
                .Append("           join arGrdBkWgtDetails gbwd ")
                .Append("               on gbwd.InstrGrdBkWgtDetailId = gbr.InstrGrdBkWgtDetailId ")
                .Append("           join (select top 1 gbw.InstrGrdBkWgtId, gbw.EffectiveDate ")
                .Append("                       from arGrdBkWeights gbw ")
                .Append("                           join  arClassSections cs on gbw.ReqId = cs.ReqId ")
                .Append("                       where gbw.EffectiveDate <= cs.StartDate ")
                .Append("                           and cs.ClsSectionId = ? ")
                .Append("                       order by gbw.EffectiveDate desc) effective_gbw ")
                .Append("               on effective_gbw.InstrGrdBkWgtId = gbwd.InstrGrdBkWgtId ")
                .Append("           join arGrdComponentTypes gct ")
                .Append("               on gct.GrdComponentTypeId = gbwd.GrdComponentTypeId ")
                .Append("   where   gbr.ClsSectionId=? ")
                .Append("           and gbr.StuEnrollId=? ")
                .Append("			and	Score is NOT NULL ")
                .Append("			and	IsCompGraded = 1 ")
                .Append("           and gct.SysComponentTypeId not in(500,503,533,544) ")

            Else
                .Append("   Select Distinct Count(*) as WeightsTaken ")
                .Append("   from	arGrdBkResults GBR, arGrdComponentTypes GCT, arGrdBkWgtDetails GBWD, arClassSections CS ")
                .Append("   where 	GBR.ClsSectionId=? and GBR.StuEnrollId=? ")
                .Append("   and	GBR.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId ")
                .Append("   and	GBWD.GrdComponentTypeId = GCT.GrdComponentTypeId  and Score is NOT NULL AND  GCT.SysComponentTypeId NOT IN (500,503,544) ")
                .Append("   and	CS.ClsSectionId=GBR.ClsSectionId ")
                .Append("   and	CS.InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId ")
            End If
        End With
        If weightinglevel = "courselevel" Then
            'add additional parameter
            db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        Dim drGradeWeightsTaken As OleDbDataReader = db.RunParamSQLDataReader(sbGradeWeightsTaken.ToString)
        'Check If Student has been Graded
        While drGradeWeightsTaken.Read
            Try
                intWeightTakenCount = drGradeWeightsTaken("WeightsTaken")
            Catch ex As Exception
                intWeightTakenCount = 0
            End Try
        End While
        If Not drGradeWeightsTaken.IsClosed Then drGradeWeightsTaken.Close()
        Return intWeightTakenCount
    End Function
    Public Function GetGradeScaleDetails(ByVal termId As String, ByVal clsSectionId As String) As DataTable
        Dim db As New DataAccess
        Dim ds As DataSet

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Dim sb1 As New StringBuilder
        With sb1
            .Append(" SELECT    A.GrdScaleDetailId,A.GrdScaleId,A.MinVal,A.MaxVal,A.GrdSysDetailId, ")
            .Append("           (select Distinct IsPass from arGradeSystemDetails where GrdSysDetailId=A.GrdSysDetailId) as Pass, ")
            .Append("           (Select Distinct GrdSystemId from arGradeSystemDetails where GrdSysDetailId=A.GrdSysDetailId) as GrdSystemId ")
            .Append(" FROM      arGradeScaleDetails A ")
            .Append(" WHERE     A.GrdScaleId In ")
            .Append("               (Select GrdScaleId from arClassSections where TermId = ? and ClsSectionId = ?) ")
            .Append(" order by MinVal ")
        End With
        'add parameters
        db.AddParameter("@TermId", termId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'execute query
        ds = db.RunParamSQLDataSet(sb1.ToString)

        Return ds.Tables(0)
    End Function
    Public Function GetGradeScaleDetails_SP(ByVal termId As String, ByVal clsSectionId As String) As DataTable
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")


        db.AddParameter("@termId", New Guid(termId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        db.AddParameter("@clsSectionId", New Guid(clsSectionId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

        'execute query
        ds = db.RunParamSQLDataSet_SP("dbo.usp_GetGradeScaleDetails")
        Try
            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        Finally
            db.CloseConnection()
        End Try

    End Function
    Public Function GetIncompleteGrade(ByVal GradeSystemId As String) As String
        Dim db As New DataAccess

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append(" select GrdSysDetailId")
            .Append(" from arGradeSystemDetails")
            .Append(" where GrdSystemId=? and IsInComplete=1")
        End With
        db.AddParameter("@GrdSystemId", GradeSystemId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)

        If Not (obj Is Nothing) Then
            Return obj.ToString
        Else
            Return DBNull.Value.ToString
        End If
    End Function
    Public Function GetPassSatisfactoryGrade(ByVal GradeSystemId As String) As String
        Dim db As New DataAccess
        'Dim strGrdSysDetailId As String

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append(" select GrdSysDetailId")
            .Append(" from arGradeSystemDetails")
            .Append(" where GrdSystemId=? and IsPass=1 and Grade like 'S%'")
        End With
        db.AddParameter("@GrdSystemId", GradeSystemId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)

        If Not (obj Is DBNull.Value) Then
            Return obj.ToString
        Else
            Return DBNull.Value.ToString
        End If
    End Function
    Public Function GetNoPassUnSatisfactoryGrade(ByVal GradeSystemId As String) As String
        Dim db As New DataAccess
        'Dim strGrdSysDetailId As String

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append(" select GrdSysDetailId")
            .Append(" from arGradeSystemDetails")
            .Append(" where GrdSystemId=? and IsPass=0 and Grade like 'U%'")
        End With
        db.AddParameter("@GrdSystemId", GradeSystemId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)

        If Not (obj Is DBNull.Value) Then
            Return obj.ToString
        Else
            Return DBNull.Value.ToString
        End If
    End Function
    Public Function GetGrades_SP(ByVal GradeSystemId As String) As String()
        Dim rtn(4) As String
        Dim db As New SQLDataAccess

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")


        db.AddParameter("@grdSystemId", New Guid(GradeSystemId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Dim dr As SqlDataReader = db.RunParamSQLDataReader_SP("dbo.usp_GetGrades")
        While dr.Read
            rtn(0) = dr(0).ToString
            rtn(1) = dr(1).ToString
            rtn(2) = dr(2).ToString
            rtn(3) = dr(3).ToString
        End While

        Try
            Return rtn
        Catch ex As Exception
        Finally
            dr.Close()
            db.CloseConnection()
        End Try

    End Function
    Public Function IsCourseSU(ByVal clsSectionId As String) As Boolean
        Dim db As New DataAccess

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder

        'Check If Course Has Been Marked as Satisfactory/Unsatisfactory
        With sb
            .Append(" select    t2.SU as SU ")
            .Append(" from      arClassSections t1,arReqs t2 ")
            .Append(" where     t1.ReqId=t2.ReqId and t1.ClsSectionId=?")
        End With
        'add parameters
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)


        If obj Is DBNull.Value.ToString Then
            Return False
        Else
            Return Convert.ToBoolean(obj)
        End If
    End Function
    Public Function IsCoursePF(ByVal clsSectionId As String) As Boolean
        Dim db As New DataAccess
        'Dim strGrdSysDetailId As String

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder

        'Check If Course Has Been Marked as Pass/Fail
        With sb
            .Append(" select    t2.PF as PF ")
            .Append(" from      arClassSections t1,arReqs t2 ")
            .Append(" where     t1.ReqId=t2.ReqId and t1.ClsSectionId=?")
        End With
        'add parameters
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)


        If obj Is DBNull.Value.ToString Then
            Return False
        Else
            Return Convert.ToBoolean(obj)
        End If
    End Function
    Public Function IsCoursePFandSU_SP(ByVal clsSectionId As String) As Boolean()
        Dim rtn(2) As Boolean
        Dim db As New SQLDataAccess

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")


        db.AddParameter("@clsSectionId", New Guid(clsSectionId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Dim dr As SqlDataReader = db.RunParamSQLDataReader_SP("dbo.usp_IsCoursePFandSU")

        While dr.Read
            rtn(0) = dr(0)
            rtn(1) = dr(1)
        End While
        Try
            Return rtn
        Catch ex As Exception
        Finally
            dr.Close()
            db.CloseConnection()
        End Try


    End Function
    Public Function GetPassGrade(ByVal GradeSystemId As String) As String
        Dim db As New DataAccess
        'Dim strGrdSysDetailId As String

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append(" select GrdSysDetailId")
            .Append(" from arGradeSystemDetails")
            .Append(" where GrdSystemId=? and IsPass=1 and Grade like 'P%'")
        End With
        db.AddParameter("@GrdSystemId", GradeSystemId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)

        If Not (obj Is DBNull.Value) Then
            Return obj.ToString
        Else
            Return DBNull.Value.ToString
        End If
    End Function
    Public Function GetFailGrade(ByVal GradeSystemId As String) As String
        Dim db As New DataAccess
        'Dim strGrdSysDetailId As String

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append(" select GrdSysDetailId")
            .Append(" from arGradeSystemDetails")
            .Append(" where GrdSystemId=? and IsPass=0 and Grade like 'F%'")
        End With
        db.AddParameter("@GrdSystemId", GradeSystemId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim obj As Object = db.RunParamSQLScalar(sb.ToString)

        If Not (obj Is DBNull.Value) Then
            Return obj.ToString
        Else
            Return DBNull.Value.ToString
        End If
    End Function
    Public Function GetGBWResultsForCourseLevel(ByVal stuEnrollId As String, ByVal clsSectionId As String, Optional ByVal onlyScoresPosted As Boolean = False) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("select ")
            .Append("           gbr.InstrGrdBkWgtDetailId,")
            .Append("           gbr.Score,")
            .Append("           gbr.ResNum,")
            .Append("           gbwd.Weight,")
            .Append("           gbwd.GrdPolicyId,")
            .Append("           gbwd.Parameter, ")
            .Append("           gbr.PostDate ")
            .Append("from       arGrdBkResults gbr ")
            .Append("               join arGrdBkWgtDetails gbwd ")
            .Append("                   on gbwd.InstrGrdBkWgtDetailId = gbr.InstrGrdBkWgtDetailId ")
            .Append("               join (select top 1 gbw.InstrGrdBkWgtId, gbw.EffectiveDate ")
            .Append("                       from arGrdBkWeights gbw ")
            .Append("                           join  arClassSections cs on gbw.ReqId = cs.ReqId ")
            .Append("                       where gbw.EffectiveDate <= cs.StartDate ")
            .Append("                           and cs.ClsSectionId = ? ")
            .Append("                       order by gbw.EffectiveDate desc) effective_gbw ")
            .Append("                   on effective_gbw.InstrGrdBkWgtId = gbwd.InstrGrdBkWgtId ")
            .Append("where      gbr.StuEnrollId=?  ")
            .Append("           and gbr.ClsSectionId=? ")
            If onlyScoresPosted = True Then
                .Append(" and gbr.Score is not null ")
            End If
            .Append("order by   gbr.InstrGrdBkWgtDetailId ")
        End With
        'add parameters
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'execute query
        Dim ds As DataSet
        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function
    Public Function GetGBWResultsForCourseLevel_SP(ByVal stuEnrollId As String, ByVal clsSectionId As String) As DataTable
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")


        db.AddParameter("@stuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@clsSectionId", New Guid(clsSectionId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet_SP("dbo.usp_GetGBWResultsForCourseLevel")
        Try
            Return ds.Tables(0)
        Catch ex As Exception
        Finally
            db.CloseConnection()
        End Try

    End Function

    Public Function GetGBWResultsForCourseLevelForExams(ByVal stuEnrollId As String, ByVal clsSectionId As String, Optional ByVal onlyScoresPosted As Boolean = False) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("select ")
            .Append("           gbr.InstrGrdBkWgtDetailId,")
            .Append("           gbr.Score,")
            .Append("           gbr.ResNum,")
            .Append("           gbwd.Weight,")
            .Append("           gbwd.GrdPolicyId,")
            .Append("           gbwd.Parameter, ")
            .Append("           gbr.PostDate ")
            .Append("from       arGrdBkResults gbr ")
            .Append("               join arGrdBkWgtDetails gbwd ")
            .Append("                   on gbwd.InstrGrdBkWgtDetailId = gbr.InstrGrdBkWgtDetailId ")
            .Append("               join (select top 1 gbw.InstrGrdBkWgtId, gbw.EffectiveDate ")
            .Append("                       from arGrdBkWeights gbw ")
            .Append("                           join  arClassSections cs on gbw.ReqId = cs.ReqId ")
            .Append("                       where gbw.EffectiveDate <= cs.StartDate ")
            .Append("                           and cs.ClsSectionId = ? ")
            .Append("                       order by gbw.EffectiveDate desc) effective_gbw ")
            .Append("                   on effective_gbw.InstrGrdBkWgtId = gbwd.InstrGrdBkWgtId ")
            .Append("               join arGrdComponentTypes gct ")
            .Append("                   on gct.GrdComponentTypeId = gbwd.GrdComponentTypeId ")
            .Append("where      gbr.StuEnrollId=?  ")
            .Append("           and gbr.ClsSectionId=? ")
            .Append("           and gct.SysComponentTypeId not in(500,503,533,544) ")
            If onlyScoresPosted = True Then
                .Append(" and gbr.Score is not null ")
            End If
            .Append("order by   gbr.InstrGrdBkWgtDetailId")
        End With
        'add parameters
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'execute query
        Dim ds As DataSet
        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function
    Public Function GetGBWResultsForInstructorLevel(ByVal stuEnrollId As String, ByVal clsSectionId As String) As DataTable
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        With sb
            .Append(" select ")
            .Append("           A.StuEnrollId,")
            .Append("           A.ClsSectionId,")
            .Append("           Sum(A.Score * B.Weight * .01) as FinalScore,")
            .Append("           Sum(B.Weight) as TotalWeight")
            .Append(" from      arGrdBkResults A, arGrdBkWgtDetails B, arClassSections C")
            .Append(" where     B.InstrGrdBkWgtDetailId=A.InstrGrdBkWgtDetailId")
            .Append("           and C.InstrGrdBkWgtId=B.InstrGrdBkWgtId")
            .Append("           and C.ClsSectionId=A.ClsSectionId")
            .Append("           and StuEnrollId=?")
            .Append("           and A.ClsSectionId=?")
            .Append(" group by  A.StuEnrollId,A.ClsSectionId")
        End With
        'add parameters
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ClsSectionId", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'execute query
        Dim ds As DataSet
        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function
    Public Function GetGBWResultsForInstructorLevel_SP(ByVal stuEnrollId As String, ByVal clsSectionId As String) As DataTable
        Dim ds As DataSet
        Dim db As New SQLDataAccess

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")


        db.AddParameter("@stuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@clsSectionId", New Guid(clsSectionId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        ds = db.RunParamSQLDataSet_SP("dbo.usp_GetGBWResultsForInstructorLevel")
        Try
            Return ds.Tables(0)
        Catch ex As Exception
        Finally
            db.CloseConnection()
        End Try


    End Function
    Public Function TransferGradeByTermClassSectionCourseLevel(ByVal TermId As String, ByVal ClsSectionId() As String, ByVal clsSectionDescrip() As String, ByVal CampusId As String) As String
        Dim db As New DataAccess

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder

        Dim x As Integer
        Dim intMinVal, intMaxVal As Integer
        Dim strGrdSysDetailId As String
        Dim sb1 As New StringBuilder
        Dim sb3 As New StringBuilder
        Dim intInitialScore As Decimal
        Dim intWeight As Integer
        Dim intFinalScore As Integer
        Dim strStuEnrollId As String
        Dim intClsSectionId As Integer
        Dim errorMessage As String
        Dim strStudExists As String
        Dim strClsSectionMessage As String
        Dim intTotalWeightsForClsSection As Integer

        Try
            intClsSectionId = ClsSectionId.Length
            While x < intClsSectionId
                Dim sbgetStudent As New StringBuilder
                Dim strFirstName, strLastName As String

                'Get the student names that are registered for this classsection
                With sbgetStudent
                    .Append(" select t1.StuEnrollId,t3.FirstName,t3.LastName from  ")
                    .Append(" arResults t1,arStuEnrollments t2,arStudent t3 ")
                    .Append(" where t1.StuEnrollId = t2.StuEnrollId and t2.StudentId = t3.StudentId and  ")
                    .Append(" TestId = ? ")
                    .Append(" order by t3.FirstName ")
                End With
                db.AddParameter("@ClassSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Dim drgetStudent As OleDbDataReader = db.RunParamSQLDataReader(sbgetStudent.ToString)
                db.ClearParameters()
                sbgetStudent.Remove(0, sbgetStudent.Length)

                While drgetStudent.Read
                    strStudExists = "Yes"
                    strStuEnrollId = CType(drgetStudent("StuEnrollId"), Guid).ToString
                    strFirstName = drgetStudent("FirstName")
                    strLastName = drgetStudent("LastName")

                    'Get The Weights Taken
                    Dim sbGradeWeightsTaken As New StringBuilder
                    Dim intWeightTakenCount As Integer
                    With sbGradeWeightsTaken
                        .Append(" select Count(*) as WeightsTaken from arGrdBkResults where ")
                        .Append(" StuEnrollID = ? and ClsSectionId = ? ")
                    End With
                    db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Dim drGradeWeightsTaken As OleDbDataReader = db.RunParamSQLDataReader(sbGradeWeightsTaken.ToString)

                    'Check If Student has been Graded
                    While drGradeWeightsTaken.Read
                        Try
                            intWeightTakenCount = drGradeWeightsTaken("WeightsTaken")
                        Catch ex As Exception
                            intWeightTakenCount = 0
                        End Try
                    End While
                    If Not drGradeWeightsTaken.IsClosed Then drGradeWeightsTaken.Close()
                    db.ClearParameters()
                    sbGradeWeightsTaken.Remove(0, sbGradeWeightsTaken.Length)

                    'Check if the student has taken total weights defined on classsection
                    'If totalweights is 2 and if student has taken 1 then consider that student 
                    'is not completly graded. changed on 06/22/2006
                    If intWeightTakenCount >= 1 Then
                        Try
                            intTotalWeightsForClsSection = GetTotalWeightsAtCourseLevel(TermId, CampusId, DirectCast(ClsSectionId.GetValue(x), String))
                        Catch ex As Exception
                            intTotalWeightsForClsSection = 0
                        End Try

                        If intWeightTakenCount < intTotalWeightsForClsSection Then
                            intWeightTakenCount = -1
                        End If
                    End If


                    'If intWeightTakenCount < intWeightCount Then
                    'If the Student has not been graded then get the students name
                    If intWeightTakenCount <= 0 Then


                        ' Modified by Balaji on 02/10/2006

                        With sb1
                            .Append(" SELECT  A.GrdScaleDetailId,A.GrdScaleId,A.MinVal,A.MaxVal,A.GrdSysDetailId, ")
                            .Append(" (select Distinct IsPass from arGradeSystemDetails where GrdSysDetailId=A.GrdSysDetailId) as Pass, ")
                            .Append(" (Select Distinct GrdSystemId from arGradeSystemDetails where GrdSysDetailId=A.GrdSysDetailId) as GrdSystemId ")
                            .Append(" FROM arGradeScaleDetails A WHERE A.GrdScaleId In ")
                            .Append(" (Select GrdScaleId from arClassSections where TermId = ? and ClsSectionId = ?) ")
                            .Append(" order by MinVal ")
                        End With

                        'add parameters
                        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        'execute query
                        Dim drGrade As OleDbDataReader = db.RunParamSQLDataReader(sb1.ToString)
                        db.ClearParameters()
                        sb1.Remove(0, sb1.Length)

                        Dim strGradeSystemId As String

                        While drGrade.Read()
                            intMinVal = drGrade("MinVal")
                            intMaxVal = drGrade("MaxVal")
                            If intFinalScore >= intMinVal And intFinalScore <= intMaxVal Then
                                strGrdSysDetailId = CType(drGrade("GrdSysDetailId"), Guid).ToString
                                strGradeSystemId = CType(drGrade("GrdSystemId"), Guid).ToString
                                Exit While
                            End If
                        End While
                        If Not drGrade.IsClosed Then drGrade.Close()
                        'Check If The Grade Is InComplete
                        Dim intInComplete As Boolean
                        Dim sb76 As New StringBuilder
                        With sb76
                            .Append(" select Distinct IsInComplete  from arResults  ")
                            .Append(" where StuEnrollId = ? and TestId = ? ")
                        End With

                        'add parameters
                        db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                        Dim drInComplete As OleDbDataReader = db.RunParamSQLDataReader(sb76.ToString)
                        While drInComplete.Read
                            Try
                                intInComplete = drInComplete("IsInComplete")
                                If intInComplete = False Then
                                    errorMessage &= "     " & strFirstName & " " & strLastName & vbLf
                                End If
                            Catch ex As Exception
                                intInComplete = False
                                errorMessage &= "     " & strFirstName & " " & strLastName & vbLf
                            End Try
                        End While
                        If Not drInComplete.IsClosed Then drInComplete.Close()
                        db.ClearParameters()
                        sb76.Remove(0, sb76.Length)

                        'Check If its InComplete
                        Dim sb67 As New StringBuilder
                        If intInComplete = True Then
                            With sb67
                                .Append(" select GrdSysDetailId from arGradeSystemDetails where GrdSystemId= ?  and IsInComplete=1 ")
                            End With
                            db.AddParameter("@GrdSystemId", strGradeSystemId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                            Dim dr45 As OleDbDataReader = db.RunParamSQLDataReader(sb67.ToString)
                            While dr45.Read
                                Try
                                    strGrdSysDetailId = CType(dr45("GrdSysDetailId"), Guid).ToString
                                Catch ex As Exception
                                    strGrdSysDetailId = DBNull.Value.ToString
                                End Try
                            End While
                            If Not dr45.IsClosed Then dr45.Close()
                            db.ClearParameters()
                            sb1.Remove(0, sb1.Length)
                        Else
                            strGrdSysDetailId = DBNull.Value.ToString
                        End If


                        With sb3
                            .Append("Update arResults set GrdSysDetailId = ? where StuEnrollId = ? and TestId =? and GrdSysDetailId is Null ")
                        End With
                        If strGrdSysDetailId = "" Then
                            db.AddParameter("@GrdSysDetailId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        ElseIf strGrdSysDetailId = DBNull.Value.ToString Then
                            db.AddParameter("@GrdSysDetailId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db.AddParameter("@GrdSysDetailId", strGrdSysDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        End If
                        db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.RunParamSQLExecuteNoneQuery(sb3.ToString)
                        db.ClearParameters()
                        sb3.Remove(0, sb3.Length)
                        'Modification ends here
                    Else
                        'Get The Score
                        With sb
                            .Append(" select A.StuEnrollId,A.ClsSectionId,Sum(A.Score * B.Weight * .01) as FinalScore, ")
                            .Append(" Sum(B.Weight) as TotalWeight from arGrdBkResults A,arGrdBkWgtDetails B where ")
                            .Append(" B.InstrGrdBkWgtDetailId = A.InstrGrdBkWgtDetailId and ")
                            .Append(" StuEnrollId = ? ")
                            .Append(" and ClsSectionId = ? ")
                            .Append(" group by A.StuEnrollId,A.ClsSectionId ")
                        End With

                        'add parameters
                        db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                        'execute query
                        Dim drScore As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                        db.ClearParameters()
                        sb.Remove(0, sb.Length)

                        Dim sb76 As New StringBuilder
                        Dim strSU As Boolean

                        'Check If Course Has Been Marked as Satisfactory/Unsatisfactory
                        With sb76
                            .Append(" select t2.SU as SU from arClassSections t1,arReqs t2 ")
                            .Append(" where  t1.ReqId = t2.ReqId and t1.ClsSectionId = ? ")
                        End With
                        'add parameters
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                        Dim drSatisfactory As OleDbDataReader = db.RunParamSQLDataReader(sb76.ToString)
                        While drSatisfactory.Read
                            Try
                                strSU = drSatisfactory("SU")
                                'strPassFAil = drSatisfactory("PF")
                            Catch Ex As Exception
                                strSU = False
                                'strPassFAil = 2
                            End Try
                        End While
                        If Not drSatisfactory.IsClosed Then drSatisfactory.Close()
                        db.ClearParameters()
                        sb76.Remove(0, sb76.Length)


                        With sb1
                            .Append(" SELECT  A.GrdScaleDetailId,A.GrdScaleId,A.MinVal,A.MaxVal,A.GrdSysDetailId, ")
                            .Append(" (select Distinct IsPass from arGradeSystemDetails where GrdSysDetailId=A.GrdSysDetailId) as Pass, ")
                            .Append(" (Select Distinct GrdSystemId from arGradeSystemDetails where GrdSysDetailId=A.GrdSysDetailId) as GrdSystemId ")
                            .Append(" FROM arGradeScaleDetails A WHERE A.GrdScaleId In ")
                            .Append(" (Select GrdScaleId from arClassSections where TermId = ? and ClsSectionId = ?) ")
                            .Append(" order by MinVal ")
                        End With

                        'add parameters
                        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        'execute query
                        Dim drGrade As OleDbDataReader = db.RunParamSQLDataReader(sb1.ToString)
                        db.ClearParameters()
                        sb1.Remove(0, sb1.Length)

                        Dim intPass As Integer
                        Dim strGradeSystemId As String

                        While drScore.Read()
                            intInitialScore = drScore("FinalScore")
                            intWeight = drScore("TotalWeight")
                            intFinalScore = Math.Round((intInitialScore / intWeight) * 100, 2)
                            ' strStuEnrollId = CType(drScore("StuEnrollId"), Guid).ToString
                            While drGrade.Read()
                                intMinVal = drGrade("MinVal")
                                intMaxVal = drGrade("MaxVal")
                                intPass = drGrade("Pass")
                                If intFinalScore >= intMinVal And intFinalScore <= intMaxVal Then
                                    strGrdSysDetailId = CType(drGrade("GrdSysDetailId"), Guid).ToString
                                    strGradeSystemId = CType(drGrade("GrdSystemId"), Guid).ToString
                                    Exit While
                                End If
                            End While
                        End While
                        If Not drScore.IsClosed Then drScore.Close()

                        'Check If The Grade Is InComplete
                        Dim intInComplete As Boolean
                        With sb76
                            .Append(" select Distinct IsInComplete  from arResults  ")
                            .Append(" where StuEnrollId = ? and TestId = ? ")
                        End With

                        'add parameters
                        db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                        Dim drInComplete As OleDbDataReader = db.RunParamSQLDataReader(sb76.ToString)
                        While drInComplete.Read
                            Try
                                intInComplete = drInComplete("IsInComplete")
                            Catch ex As Exception
                                intInComplete = False
                            End Try
                        End While
                        If Not drInComplete.IsClosed Then drInComplete.Close()
                        db.ClearParameters()
                        sb76.Remove(0, sb76.Length)

                        'Check If its InComplete
                        Dim sb67 As New StringBuilder
                        If intInComplete = True Then
                            With sb67
                                .Append(" select GrdSysDetailId from arGradeSystemDetails where GrdSystemId= ?  and IsInComplete=1 ")
                            End With
                            db.AddParameter("@GrdSystemId", strGradeSystemId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                            Dim dr45 As OleDbDataReader = db.RunParamSQLDataReader(sb67.ToString)
                            While dr45.Read
                                Try
                                    strGrdSysDetailId = CType(dr45("GrdSysDetailId"), Guid).ToString
                                Catch ex As Exception
                                    strGrdSysDetailId = DBNull.Value.ToString
                                End Try
                            End While
                            If Not dr45.IsClosed Then dr45.Close()
                            db.ClearParameters()
                            sb1.Remove(0, sb1.Length)
                        Else
                            'If Grade falls under Pass Category and If Course Marked as 
                            'Satisfactory/UnSatisfactory
                            If intPass = 1 And strSU = True Then
                                With sb1
                                    '.Append(" select GrdSysDetailId from arGradeSystemDetails where GrdSystemId= ? and IsPass=1 and IsSatisfactory=1 ")
                                    .Append(" select GrdSysDetailId from arGradeSystemDetails where GrdSystemId= ? and IsPass=1 and Grade='S' ")
                                End With
                                db.AddParameter("@GrdSystemId", strGradeSystemId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                Try
                                    Dim drPassSatisfactory As OleDbDataReader = db.RunParamSQLDataReader(sb1.ToString)
                                    While drPassSatisfactory.Read
                                        strGrdSysDetailId = drPassSatisfactory("GrdSysDetailId")
                                    End While
                                    If Not drPassSatisfactory.IsClosed Then drPassSatisfactory.Close()
                                Catch ex As Exception
                                    strGrdSysDetailId = DBNull.Value.ToString
                                End Try
                                db.ClearParameters()
                                sb1.Remove(0, sb1.Length)

                                'If Grade falls under Fail Category and If Course Marked as 
                                'Satisfactory/UnSatisfactory
                            ElseIf intPass = 0 And strSU = True Then
                                With sb1
                                    '.Append(" select GrdSysDetailId from arGradeSystemDetails where GrdSystemId= ? and IsPass=0 and IsUnSatisfactory=0 ")
                                    .Append(" select GrdSysDetailId from arGradeSystemDetails where GrdSystemId= ? and IsPass=0 and Grade='U' ")
                                End With
                                db.AddParameter("@GrdSystemId", strGradeSystemId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                Try
                                    Dim drPassSatisfactory As OleDbDataReader = db.RunParamSQLDataReader(sb1.ToString)
                                    While drPassSatisfactory.Read
                                        strGrdSysDetailId = drPassSatisfactory("GrdSysDetailId")
                                    End While
                                Catch ex As Exception
                                    strGrdSysDetailId = DBNull.Value.ToString
                                End Try
                                db.ClearParameters()
                                sb1.Remove(0, sb1.Length)

                            End If
                        End If

                        With sb3
                            .Append("Update arResults set GrdSysDetailId = ? where StuEnrollId = ? and TestId =? and GrdSysDetailId is Null ")
                        End With
                        If strGrdSysDetailId = "" Then
                            db.AddParameter("@GrdSysDetailId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db.AddParameter("@GrdSysDetailId", strGrdSysDetailId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        End If
                        db.AddParameter("@StuEnrollId", strStuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.RunParamSQLExecuteNoneQuery(sb3.ToString)
                        db.ClearParameters()
                        sb3.Remove(0, sb3.Length)
                    End If
                End While
                If Not drgetStudent.IsClosed Then drgetStudent.Close()

                'For Student Enrollments
                'Increment value of x and y
                Dim sbUpdate As New StringBuilder
                Try
                    If errorMessage.Length >= 1 Then
                        With sbUpdate
                            .Append("Update arClassSections set IsGraded = 0 where ClsSectionId = ? and TermId =? ")
                        End With
                        db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.RunParamSQLExecuteNoneQuery(sbUpdate.ToString)
                        db.ClearParameters()
                        sbUpdate.Remove(0, sbUpdate.Length)
                    End If
                Catch ex As Exception
                    With sbUpdate
                        .Append("Update arClassSections set IsGraded = 1 where ClsSectionId = ? and TermId =? ")
                    End With
                    db.AddParameter("@ClsSectionId", DirectCast(ClsSectionId.GetValue(x), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@TermId", TermId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.RunParamSQLExecuteNoneQuery(sbUpdate.ToString)
                    db.ClearParameters()
                    sbUpdate.Remove(0, sbUpdate.Length)
                End Try
                Try
                    If errorMessage.Length >= 1 Then
                        strClsSectionMessage &= vbLf
                        strClsSectionMessage &= DirectCast(clsSectionDescrip.GetValue(x), String) & vbLf & vbLf
                        strClsSectionMessage &= errorMessage
                        errorMessage = ""
                    End If
                Catch ex As Exception
                End Try
                x = x + 1
            End While


            Try
                If strClsSectionMessage.Length >= 1 Then
                    Const strReturnMessage As String = "The following students have not been graded" & vbLf
                    Return strReturnMessage & strClsSectionMessage
                Else
                    Return ""
                End If
            Catch ex As Exception
            End Try
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function TransferGBW(ByVal termId As String, _
                                    ByVal updateResults As ArrayList, _
                                    ByVal isNotGradedClsSection As ArrayList, _
                                    ByVal gradedClsSection As ArrayList, _
                                    ByVal user As String, _
                                    Optional ByVal GradesFormat As String = "") As String

        Dim db As New DataAccess
        Dim infoObj As TransferGBWInfo

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        '   we must encapsulate all DB updates in one transaction
        Dim groupTrans As OleDbTransaction = db.StartTransaction()

        Try

            Dim sb As New StringBuilder
            With sb
                .Append(" UPDATE    arResults")
                .Append(" SET     IsCourseCompleted=1,isClinicsSatisfied=1, GrdSysDetailId=?,ModDate=?,ModUser=?")
                If GradesFormat.ToString.ToLower = "numeric" Then
                    .Append(" ,Score=? ")
                End If
                .Append(" WHERE     StuEnrollId=? AND TestId=? ")
                'Commented by Balaji on 11/20/2009 to fix issue 17987
                '.Append(" AND GrdSysDetailId IS NULL ")
            End With

            Dim now As Date = Date.Now

            For i As Integer = 0 To updateResults.Count - 1
                infoObj = DirectCast(updateResults(i), TransferGBWInfo)
                If infoObj.GrdSysDetailId = "" Or infoObj.GrdSysDetailId = DBNull.Value.ToString Or infoObj.GrdSysDetailId = Guid.Empty.ToString Then
                    db.AddParameter("GrdSysDetailId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("GrdSysDetailId", infoObj.GrdSysDetailId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                If GradesFormat.ToString.ToLower = "numeric" Then
                    db.AddParameter("@Score", infoObj.Score, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                End If
                db.AddParameter("StuEnrollId", infoObj.StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("ClsSectionId", infoObj.ClsSectionId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
            Next

            sb = New StringBuilder
            With sb
                .Append(" UPDATE    arClassSections")
                .Append(" SET       IsGraded=0,ModDate=?,ModUser=?")
                .Append(" WHERE     ClsSectionId=? and TermId=?")
            End With

            For i As Integer = 0 To isNotGradedClsSection.Count - 1
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("ClsSectionId", isNotGradedClsSection(i).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
            Next

            sb = New StringBuilder
            With sb
                .Append(" UPDATE    arClassSections")
                .Append(" SET       IsGraded=1,ModDate=?,ModUser=?")
                .Append(" WHERE     ClsSectionId=? and TermId=?")
            End With

            For i As Integer = 0 To gradedClsSection.Count - 1
                db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("ClsSectionId", gradedClsSection(i).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("TermId", termId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
            Next

            '   commit transaction 
            groupTrans.Commit()
            '   return without errors
            Return ""

        Catch ex As Exception
            '   rollback transaction
            groupTrans.Rollback()

            '   return an error message
            Return DALExceptions.BuildConcurrencyExceptionMessage()

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function IsStudentCompletedExternShipHours(ByVal stuEnrollId As String, classSectionID As String) As Boolean

        Dim ds As DataSet
        Dim db As New SQLDataAccess

        'Dim myAdvAppSettings As AdvAppSettings
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If

        db.ConnectionString = myAdvAppSettings.AppSettings("ConnectionString")

        db.AddParameter("@StuEnrollId", New Guid(stuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@ClassSectionID", New Guid(classSectionID), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        Try
            ds = db.RunParamSQLDataSet_SP("dbo.usp_GetExtershipHours")
        Catch ex As Exception
            Return False
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)("RemainingHours") > 0 Then
                    Return False
                Else
                    Return True
                End If
            Else
                Return False

            End If
        Else
            Return False
        End If
    End Function
End Class

