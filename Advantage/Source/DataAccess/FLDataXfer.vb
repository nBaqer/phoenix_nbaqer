' ===============================================================================
' FAME.AdvantageV1.FLDB
'
' FLDB.vb
'
' database classes for FameLink data conversion processing
' ===============================================================================
' Copyright (C) 2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
Imports FAME.Advantage.Common

Public Class FLDataXfer
    Public m_ExceptionMessage As String

#Region " Methods"
    Public Function GetAwardTypeID(ByRef db As DataAccess, ByRef AwardTypeID As String, ByVal strFund As String, ByVal strAwdyr As String) As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardTypeID As New System.Text.StringBuilder(1000)
        Dim blnYYYY As Boolean = False
        Dim intFund As Integer

        'GetAwardTypeID = False
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(strAwdyr) Then
            Return "Unable to get award type as award year is missing"
            Exit Function
        End If

        If String.IsNullOrEmpty(strFund) Then
            Return "Unable to get award type as fund provided is empty"
            Exit Function
        End If

        Try
            '   connect to the database
            If Not IsNumeric(strFund) Then
                Return "Unable to get award type as fund value provided is invalid"
                Exit Function
            End If
            intFund = CInt(strFund)

            sbAwardTypeID = New StringBuilder
            With sbAwardTypeID
                .Append("SELECT FundSourceId, FundSourceCode, AwardYear, advFundSourceId, ")
                .Append("(CASE WHEN (SELECT COUNT(*) FROM saFundSources WHERE advFundSourceId= ? ) > 1 THEN 'True' ELSE 'False' END) AS useAwdyr ")
                .Append("FROM  saFundSources ")
                .Append("WHERE (advFundSourceId = ")
                .Append("(SELECT TOP 1 AdvFundSourceId ")
                .Append("FROM syAdvFundSources ")
                .Append("WHERE (AdvFundSourceId = ?))) ")
                .Append("ORDER BY AwardYear ASC ")
            End With
            db.ClearParameters()
            db.AddParameter("@advFundSourceId", intFund, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@advFundSourceId", intFund, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            '   Execute the query
            dr = db.RunParamSQLDataReader(sbAwardTypeID.ToString())
            'check for exception - no data
            If Not (dr.HasRows) Then
                'EXCEPTION REPORT for this query
                Return "Unable to match fundsource records for the given fund type during " & strAwdyr
                Exit Function
            End If
            While dr.Read()
                If Not (dr("FundSourceId") Is System.DBNull.Value) Then
                    If dr("useAwdyr") = "False" Then  'only one record
                        AwardTypeID = dr("FundSourceId").ToString
                    Else
                        If Not (dr("AwardYear") Is System.DBNull.Value) Then blnYYYY = (strAwdyr = dr("AwardYear"))
                        AwardTypeID = dr("FundSourceId").ToString
                        If blnYYYY Then Exit While
                    End If
                End If
            End While
            Return ""

            'Catch e As OleDbException
            '    m_ExceptionMessage = DALExceptions.BuildErrorMessage(e)
            'Catch e As ArgumentNullException
            '    m_ExceptionMessage = e.Message
            'Catch e As ArgumentOutOfRangeException
            '    m_ExceptionMessage = e.Message
            'Catch e As System.Exception
            '    m_ExceptionMessage = e.Message
            '    Throw
            'End Try
            'Return GetAwardTypeID
        Finally
            dr.Close()
        End Try
    End Function
    Public Function GetFundSourceID(ByRef db As DataAccess, ByRef FundSourceID As String, ByRef RefundTypeID As Integer, ByVal strFund As String) As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardTypeID As New System.Text.StringBuilder(1000)
        Dim intFund As Integer

        'GetFundSourceID = False
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(strFund) Then
            Return "Unable to get fund source as the provided fund type is not found"
            Exit Function
        End If
        Try
            '   connect to the database
            If Not IsNumeric(strFund) Then
                Return "Unable to get fund source as the provided fund type must be a number"
                Exit Function
            End If
            intFund = CInt(strFund)
            sbAwardTypeID = New StringBuilder
            With sbAwardTypeID
                .Append("SELECT Top 1  FundSourceId, advFundSourceId, AwardTypeId ")
                .Append("FROM saFundSources ")
                .Append("WHERE advFundSourceId = ")
                .Append("(SELECT TOP 1 AdvFundSourceId ")
                .Append("FROM syAdvFundSources ")
                .Append("WHERE AdvFundSourceId = ?) ")
            End With
            db.ClearParameters()
            db.AddParameter("@advFundSourceId", intFund, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            '   Execute the query
            dr = db.RunParamSQLDataReader(sbAwardTypeID.ToString())
            'check for exception - no data
            If Not (dr.HasRows) Then
                Return "Unable to match fund source data found for the given fund type"
                Exit Function
            End If
            While dr.Read()
                If Not (dr("FundSourceId") Is System.DBNull.Value) Then
                    FundSourceID = dr("FundSourceId").ToString
                    If Not (dr("AwardTypeId") Is System.DBNull.Value) Then
                        RefundTypeID = dr("advFundSourceId").ToString
                    End If
                    Return ""
                End If
            End While

            'Catch e As OleDbException
            '    m_ExceptionMessage = DALExceptions.BuildErrorMessage(e)
            'Catch e As ArgumentNullException
            '    m_ExceptionMessage = e.Message
            'Catch e As ArgumentOutOfRangeException
            '    m_ExceptionMessage = e.Message
            'Catch e As System.Exception
            '    m_ExceptionMessage = e.Message
            '    Throw
        Finally
            dr.Close()
        End Try
        'Return GetFundSourceID
    End Function
    Public Function GetAwardScheduleID(ByRef db As DataAccess, ByRef AwardScheduleID As String, ByVal strStudentAwardID As String, Optional ByVal StudentName As String = "") As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbScheduleID As New System.Text.StringBuilder(1000)

        'GetAwardScheduleID = False
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(strStudentAwardID) Then
            Return "Unable to get award schedule as student award provided for student " & StudentName & " was not found"
            Exit Function
        End If
        '   connect to the database
        sbScheduleID = New StringBuilder
        With sbScheduleID
            .Append(" SELECT Top 1 ")
            .Append(" StudentAwardId, ExpectedDate, AwardScheduleID ")
            .Append(" FROM  faStudentAwardSchedule ")
            .Append(" WHERE(StudentAwardId =  ? )")
            .Append(" ORDER BY ExpectedDate ASC ")
        End With
        Try
            db.ClearParameters()
            db.AddParameter("@StudentAwardID", strStudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            dr = db.RunParamSQLDataReader(sbScheduleID.ToString())
            'check for exception - no data
            If Not (dr.HasRows) Then
                'EXCEPTION REPORT for this query
                Return "Unable to match award schedule data for the student " & StudentName
                Exit Function
            End If
            While dr.Read()
                If Not (dr("AwardScheduleID") Is System.DBNull.Value) Then
                    AwardScheduleID = dr("AwardScheduleID").ToString
                    Return ""
                End If
            End While

            'Catch e As OleDbException
            '    m_ExceptionMessage = DALExceptions.BuildErrorMessage(e)
            'Catch e As ArgumentNullException
            '    m_ExceptionMessage = e.Message
            'Catch e As ArgumentOutOfRangeException
            '    m_ExceptionMessage = e.Message
            'Catch e As System.Exception
            '    m_ExceptionMessage = e.Message
            '    Throw
        Catch ex As System.Exception
            Return "Unable to match award schedule data for the student " & StudentName
            Exit Function
        Finally
            dr.Close()
        End Try
        'Return GetAwardScheduleID
    End Function
    Public Function GetStudentAwardID(ByRef db As DataAccess, ByRef StudentAwardID As String, ByVal strStuEnrollID As String, ByVal strFAID As String, Optional ByVal StudentName As String = "") As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardID As New System.Text.StringBuilder(1000)

        If String.IsNullOrEmpty(strFAID) Then
            Return "Unable to get student award for student " & StudentName & " as the FAID provided is empty"
            Exit Function
        End If

        If String.IsNullOrEmpty(strStuEnrollID) Then
            Return "Unable to find student enrollment information for the student " & StudentName
            Exit Function
        End If

        sbAwardID = New StringBuilder
        With sbAwardID
            .Append(" SELECT ")
            .Append(" StudentAwardId, fa_id, StuEnrollId ")
            .Append(" FROM  faStudentAwards ")
            .Append(" WHERE(faStudentAwards.fa_id  = ? ) ")
            'commented by balaji on 11/05/2009 as fa_id is unique and we don't need stuenrollid to search student awards
            '.Append(" AND (StuEnrollId = ? ) ")
        End With

        Try
            db.ClearParameters()
            db.AddParameter("@FAID", strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'db.AddParameter("@STUENROLLID", strStuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            dr = db.RunParamSQLDataReader(sbAwardID.ToString())
            'check for exception - no data
            If Not (dr.HasRows) Then
                Return ("Unable to find student awards for the student " & StudentName & " and FA Identifier")
                Exit Function
            End If
            While dr.Read()
                If Not (dr("StudentAwardID") Is System.DBNull.Value) Then
                    StudentAwardID = dr("StudentAwardID").ToString
                    Return ""
                Else
                    Return "Unable to find a matching student award information for the student" & StudentName
                    Exit Function
                End If
            End While

            'Catch e As OleDbException
            '    m_ExceptionMessage = DALExceptions.BuildErrorMessage(e)
            'Catch e As ArgumentNullException
            '    m_ExceptionMessage = e.Message
            'Catch e As ArgumentOutOfRangeException
            '    m_ExceptionMessage = e.Message
            'Catch e As System.Exception
            '    m_ExceptionMessage = e.Message
            '    Throw
        Catch ex As System.Exception
            Return "Unable to find student awards for the student " & StudentName & " and FA Identifier"
            Exit Function
        Finally
            dr.Close()
        End Try
    End Function
    Public Function GetStudentAwardIDRCVD(ByRef db As DataAccess, ByRef StudentAwardID As String, ByVal strStuEnrollID As String, ByVal strFAID As String, Optional ByVal StudentName As String = "", Optional ByVal strFund As String = "") As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardID As New System.Text.StringBuilder(1000)
        Dim sbAcademicYear As New StringBuilder
        Dim strAwardYear As String
        Dim strAwardYearInput As String
        Dim strLoanBeginDate, strLoanEndDate As String


        If String.IsNullOrEmpty(strFAID) Then
            Return "Unable to get student award for student " & StudentName & " as the FAID provided is empty"
            Exit Function
        End If
        If String.IsNullOrEmpty(strStuEnrollID) Then
            Return "Unable to find student enrollment information for the student" & StudentName
            Exit Function
        End If
        Dim strFaidCount As Integer = strFAID.Length
        Dim intFund As Integer
        intFund = CInt(strFund)
        If strFund = "02" Or strFund = "03" Or strFund = "04" Then
            strAwardYearInput = Mid(strFAID, strFaidCount, 1)
            strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
            'strAwardYearInput = "200" & (strAwardYearInput - 1)
        Else
            strAwardYearInput = Mid(strFAID, 11, 1)
            strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
            'strAwardYearInput = "200" & (strAwardYearInput - 1)
        End If
        If strAwardYearInput = "" Then
            Return "Unable to match the award year from Fame ESP with Advantage Award Year for student " & StudentName
            Exit Function
        End If
        With sbAcademicYear
            .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
        End With
        db.ClearParameters()
        db.AddParameter("@AcademicYearDescrip", strAwardYearInput, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            dr = db.RunParamSQLDataReader(sbAcademicYear.ToString())
            If Not (dr.HasRows) Then
                Return "Unable to create student award as the award year was not found in the database for student " & StudentName
                Exit Function
            End If
            While dr.Read()
                If Not (dr("AcademicYearId") Is System.DBNull.Value) Or Not (dr("AcademicYearId").ToString = Guid.Empty.ToString) Then
                    strAwardYear = dr("AcademicYearId").ToString
                Else
                    Return "Unable to create student award as the award year was not found in the database for student " & StudentName
                    Exit Function
                End If
            End While
        Catch ex As System.Exception
            Return "Unable to create student award as the award year was not found in the database for student " & StudentName
            Exit Function
        Finally
            dr.Close()
        End Try

        sbAwardID = New StringBuilder
        With sbAwardID
            .Append(" select Distinct Top 1 StudentAwardId from faStudentAwards where StuEnrollId=? and ")
            .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
            Try
                If strAwardYear.Length >= 30 Then
                    .Append(" and AcademicYearId = ? ")
                End If
            Catch ex As System.Exception
            End Try
            .Append(" and FA_ID is NULL ")
        End With

        Try
            db.ClearParameters()
            db.AddParameter("@STUENROLLID", strStuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FundSourceId", intFund, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                If strAwardYear.Length >= 30 Then
                    db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
            Catch ex As System.Exception
            End Try
            dr = db.RunParamSQLDataReader(sbAwardID.ToString())
            If Not (dr.HasRows) Then
                Return "Unable to find a matching student award information for the student" & StudentName
                Exit Function
            End If
            While dr.Read()
                If Not (dr("StudentAwardID") Is System.DBNull.Value) Then
                    StudentAwardID = dr("StudentAwardID").ToString
                    Return ""
                Else
                    Return "Unable to find a matching student award information for the student" & StudentName
                    Exit Function
                End If
            End While
        Catch ex As System.Exception
            Return "Unable to find a matching student award information for the student" & StudentName
            Exit Function
        Finally
            dr.Close()
        End Try
    End Function
    Public Function GetStuEnrollIDbySSN(ByRef db As DataAccess, ByRef stuEnrollID As String, _
                                        ByVal strMsgSSN As String, Optional ByVal msgType As String = "", _
                                        Optional ByVal strPrgVerId As String = "", _
                                        Optional ByVal Reprocess As Boolean = False) As String
        'returns the StuEnrollmentID given a valid SSN, else returns blank string
        Dim ExceptionObject As System.Exception = Nothing
        Dim dr As OleDbDataReader = Nothing
        Dim sbEnrollID As New System.Text.StringBuilder(1000)
        Dim strStudentID As String = ""
        Dim strEnrollId As String = ""
        Dim strInSchoolMessage As String = ""
        GetStuEnrollIDbySSN = False
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(strMsgSSN) Then
            Return "Student enrollment information not found as ssn is empty"
            Exit Function
        End If
        Try
            sbEnrollID = New StringBuilder
            With sbEnrollID
                .Append(" SELECT Distinct ")
                .Append(" StudentID ")
                .Append(" FROM ")
                .Append(" arStudent ")
                .Append(" WHERE ")
                .Append(" SSN = ? ")
            End With
            db.ClearParameters()
            db.AddParameter("@SSN", strMsgSSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            Try
                strStudentID = CType(db.RunParamSQLScalar(sbEnrollID.ToString), Guid).ToString
                If Not strStudentID = "" And Not strStudentID = Guid.Empty.ToString Then
                    Dim sbID As New StringBuilder
                    Dim boolStudentEnrolledInAnyProgram As Boolean = CheckIfStudentIsCurrentlyEnrolled(strStudentID)
                    If boolStudentEnrolledInAnyProgram = True Then
                        Dim boolMultipleEnrollments As Boolean = CheckIfStudentIsEnrolledinMultiplePrograms(strStudentID)
                        If boolMultipleEnrollments = False Then
                            strInSchoolMessage = GetStuEnrollIdForSingleEnrollment(strStudentID, strMsgSSN)
                            Return strInSchoolMessage
                            Exit Function
                        Else
                            'If Student is currently enrolled in multiple programs and if data pull is historical
                            'then send record to exception

                            'If the msgType is FAID or HEAD file then validate for historical data and send the data to exception
                            'If the message type is DISB,RCVD,REFU or CHNG then get the student enrollment id
                            If msgType = "FAID" Or msgType = "HEAD" Then
                                'If user wants to reprocess and user selects the program version
                                If Reprocess = True And Not strPrgVerId = "" Then
                                    strInSchoolMessage = GetStuEnrollIdByProgramVersion(strStudentID, strMsgSSN, strPrgVerId)
                                    Return strInSchoolMessage
                                    Exit Function
                                Else
                                    strInSchoolMessage = "The award data was not imported as the student with ssn: " & strMsgSSN & " was enrolled in multiple programs " & vbCrLf
                                    strInSchoolMessage &= " and the awards need to be manually created for this student."
                                    Return strInSchoolMessage
                                    Exit Function
                                End If
                            Else
                                strInSchoolMessage = GetStuEnrollIdForSingleEnrollment(strStudentID, strMsgSSN)
                                Return strInSchoolMessage
                                Exit Function
                            End If
                        End If
                    Else
                        'If the student is currently not enrolled in any program
                        strInSchoolMessage = "The award data was not imported as the student with ssn: " & strMsgSSN & " is currently not enrolled in any program. " & vbCrLf
                        Return strInSchoolMessage
                        Exit Function
                    End If
                Else
                    Return "Student not found for the given ssn:" & strMsgSSN
                    Exit Function
                End If
            Catch ex As System.Exception
                Return "Student not found for the given ssn:" & strMsgSSN
                Exit Function
            End Try
        Catch ex As System.Exception
            Return "Student not found for the given ssn:" & strMsgSSN
            Exit Function
        End Try


        'EXECUTE THE QUERY
        'dr = db.RunParamSQLDataReader(sbEnrollID.ToString())
        'CHECK FOR EXCEPTIONS
        'If Not (dr.HasRows) Then
        '    'EXCEPTION REPORT for this SSN
        '    Throw New ArgumentOutOfRangeException("SSN", strMsgSSN, "Unable to match financial aid data with student enrollment records")
        'End If
        'While dr.Read()
        '    strStudentID = dr("StudentId").ToString
        'End While
        'If Not IsNumeric(strStudentID) Then Throw New ArgumentOutOfRangeException("strStudentID", strStudentID, "The derived STUDENTID is non numeric")


        'PREPARE THE NEXT QUERY 
        'sbEnrollID = New StringBuilder
        'With sbEnrollID
        '    .Append("SELECT Top 1 ")
        '    .Append(" StuEnrollId, startdate, studentId ")
        '    .Append("FROM ")
        '    .Append(" arStuEnrollments ")
        '    .Append("WHERE ")
        '    .Append(" studentId = ?")
        'End With

        'db.ClearParameters()
        'db.AddParameter("@StudentId", strStudentID, DataAccess.OleDbDataType.OleDbString, 0, ParameterDirection.Input)

        ''EXECUTE THE QUERY
        'dr = db.RunParamSQLDataReader(sbEnrollID.ToString)
        'If Not (dr.HasRows) Then
        '    'EXCEPTION REPORT for this studentid
        '    Throw New ArgumentOutOfRangeException("Student Enrollment Id", sbEnrollID, "Unable to match financial aid data with student enrollment records")
        'End If

        'While dr.Read()
        '    stuEnrollID = dr("stuEnrollId").ToString
        '    GetStuEnrollIDbySSN = True
        'End While

        'Catch e As OleDbException
        '    m_ExceptionMessage = DALExceptions.BuildErrorMessage(e)
        'Catch e As ArgumentNullException
        '    m_ExceptionMessage = e.Message
        'Catch e As ArgumentOutOfRangeException
        '    m_ExceptionMessage = e.Message
        'Catch e As System.Exception
        '    m_ExceptionMessage = e.Message
        '    Throw
        'End Try
    End Function
    Private Function GetStuEnrollIdForSingleEnrollment(ByVal strStudentId As String, ByVal strMsgSSN As String) As String
        Dim db As New DataAccess
        Dim strEnrollId As String = ""
        Dim strInSchoolMessage As String = ""
        'db.ConnectionString = ConfigurationManager.AppSettings("ConString")
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        Dim sbID As New StringBuilder
        With sbID
            .Append("SELECT Top 1 ")
            .Append(" StuEnrollId ")
            .Append("FROM ")
            .Append(" arStuEnrollments t1,syStatusCodes t2,sySysStatus t3  ")
            .Append("WHERE ")
            .Append(" t1.StatusCodeId=t2.StatusCodeId and t2.SysStatusId=t3.SysStatusId and ")
            'for single enrollments there is no need to check for historical data or regular data
            'we just have to associate the awards with the program the student is associated with
            'If SingletonAppSettings.AppSettings("IgnoreInSchoolStatusValidationForFamelink").ToString.ToLower = "no" Then
            '    .Append(" t3.InSchool = 1 And ")
            'End If
            .Append(" studentId = ?")
            .Append(" order by EnrollDate Desc ")
        End With
        db.ClearParameters()
        db.AddParameter("@StudentId", strStudentId, DataAccess.OleDbDataType.OleDbString, 0, ParameterDirection.Input)
        Try
            strEnrollId = CType(db.RunParamSQLScalar(sbID.ToString), Guid).ToString
            If Not strEnrollId = "" And Not strEnrollId = Guid.Empty.ToString Then
                Return strEnrollId
                Exit Function
            Else
                strInSchoolMessage = "The award data was not imported as the student with ssn: " & strMsgSSN & " is not currently enrolled in any program " & vbCrLf
                Return strInSchoolMessage
                Exit Function
            End If
        Catch ex As System.Exception
            strInSchoolMessage = "The award data was not imported as either the student with ssn: " & strMsgSSN & " is not currently enrolled in any program " & vbCrLf
            Return strInSchoolMessage
            Exit Function
        End Try
    End Function
    Private Function GetStuEnrollIdByProgramVersion(ByVal strStudentId As String, ByVal strMsgSSN As String, ByVal strPrgVerId As String) As String
        Dim db As New DataAccess
        Dim strEnrollId As String = ""
        Dim strInSchoolMessage As String = ""
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        Dim sbID As New StringBuilder
        With sbID
            .Append("SELECT Top 1 ")
            .Append(" StuEnrollId ")
            .Append("FROM ")
            .Append(" arStuEnrollments t1,syStatusCodes t2,sySysStatus t3  ")
            .Append("WHERE ")
            .Append(" t1.StatusCodeId=t2.StatusCodeId and t2.SysStatusId=t3.SysStatusId and ")
            'for single enrollments there is no need to check for historical data or regular data
            'we just have to associate the awards with the program the student is associated with
            'If SingletonAppSettings.AppSettings("IgnoreInSchoolStatusValidationForFamelink").ToString.ToLower = "no" Then
            '    .Append(" t3.InSchool = 1 And ")
            'End If
            .Append(" studentId = ? and t1.StuEnrollId=? ")
            .Append(" order by EnrollDate Desc ")
        End With
        db.ClearParameters()
        db.AddParameter("@StudentId", strStudentId, DataAccess.OleDbDataType.OleDbString, 0, ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", strPrgVerId, DataAccess.OleDbDataType.OleDbString, 0, ParameterDirection.Input)
        Try
            strEnrollId = CType(db.RunParamSQLScalar(sbID.ToString), Guid).ToString
            If Not strEnrollId = "" And Not strEnrollId = Guid.Empty.ToString Then
                Return strEnrollId
                Exit Function
            Else
                strInSchoolMessage = "The award data was not imported as the student with ssn: " & strMsgSSN & " is not currently enrolled in any program " & vbCrLf
                Return strInSchoolMessage
                Exit Function
            End If
        Catch ex As System.Exception
            strInSchoolMessage = "The award data was not imported as either the student with ssn: " & strMsgSSN & " is not currently enrolled in any program " & vbCrLf
            Return strInSchoolMessage
            Exit Function
        End Try
    End Function
    Private Function GetStuEnrollIdForMultipleEnrollment(ByVal strStudentId As String, ByVal strMsgSSN As String) As String
        Dim db As New DataAccess
        Dim strEnrollId As String = ""
        Dim strInSchoolMessage As String = ""
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        Dim sbID As New StringBuilder
        With sbID
            .Append("SELECT Top 1 ")
            .Append(" StuEnrollId ")
            .Append("FROM ")
            .Append(" arStuEnrollments t1,syStatusCodes t2,sySysStatus t3  ")
            .Append("WHERE ")
            .Append(" t1.StatusCodeId=t2.StatusCodeId and t2.SysStatusId=t3.SysStatusId and ")
            .Append(" t3.InSchool = 1 And ")
            .Append(" studentId = ?")
            .Append(" order by EnrollDate Desc ")
        End With
        db.ClearParameters()
        db.AddParameter("@StudentId", strStudentId, DataAccess.OleDbDataType.OleDbString, 0, ParameterDirection.Input)
        Try
            strEnrollId = CType(db.RunParamSQLScalar(sbID.ToString), Guid).ToString
            If Not strEnrollId = "" And Not strEnrollId = Guid.Empty.ToString Then
                Return strEnrollId
                Exit Function
            Else
                strInSchoolMessage = "The award data was not imported as either the student with ssn: " & strMsgSSN & " is not currently enrolled in any program or " & vbCrLf
                strInSchoolMessage &= " the student might be currently placed in an Out of School status (Example: Suspended, Dismissed,etc)."
                Return strInSchoolMessage
                Exit Function
            End If
        Catch ex As System.Exception
            strInSchoolMessage = "The award data was not imported as either the student with ssn: " & strMsgSSN & " is not currently enrolled in any program or " & vbCrLf
            strInSchoolMessage &= " the student might be currently placed in an Out of School status (Example: Suspended, Dismissed,etc)."
            Return strInSchoolMessage
            Exit Function
        End Try
    End Function
    Public Function GetStudentInfo(ByVal strMsgSSN As String) As String
        Dim ExceptionObject As System.Exception = Nothing
        Dim dr As OleDbDataReader = Nothing
        Dim sb As New StringBuilder
        Dim dt As New DataTable
        Dim db As New DataAccess
        Dim strStudentName As String = ""
        Dim str As New StringBuilder

        
        Try
            With str
                .Append("Select Distinct FirstName,LastName from arStudent where SSN = ?")
            End With
            db.ClearParameters()
            db.AddParameter("@SSN", strMsgSSN, DataAccess.OleDbDataType.OleDbString, 12, ParameterDirection.Input)
            dr = db.RunParamSQLDataReader(str.ToString)
            'check for exception - no data
            If Not (dr.HasRows) Then
                Return "Student not found for the given SSN " & strMsgSSN
                Exit Function
            End If
            While dr.Read()
                strStudentName = dr("FirstName").ToString + " " + dr("LastName").ToString
                Return strStudentName
            End While
        Catch ex As System.Exception
            Return "Student not found for the given SSN " & strMsgSSN
            Exit Function
        Finally
            dr.Close()
        End Try
    End Function

    Public Function CheckForDuplicateTransactions(ByVal StuEnrollId As String, _
                                                  ByVal TransDate As Date, _
                                                  ByVal Transreference As String, _
                                                  ByVal AcademicYearId As String, _
                                                  ByVal TransAmount As Decimal, _
                                                  ByVal TransDescrip As String) As Integer
        'Connect To The Database
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        Dim sb As New StringBuilder
        Dim intRowCount As Integer = 0
        Try
            With sb
                .Append(" select Count(*) as RecordCount from saTransactions where StuEnrollId=? and TransDate=? and Transreference=? ")
                .Append(" and AcademicyearId=? and TransDescrip=? and TransAmount=? ")
            End With


            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TransDate", TransDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@TransReference", Transreference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AcademicyearId", AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TransDescrip", TransDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TransAmount", TransAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            'execute the query
            intRowCount = db.RunParamSQLScalar(sb.ToString)

            Return intRowCount

        Catch ex As OleDbException
            '   return an error to the client
            Return 0
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetTransactionId(ByVal StuEnrollId As String, _
                                                ByVal TransDate As Date, _
                                                ByVal Transreference As String, _
                                                ByVal AcademicYearId As String, _
                                                ByVal TransAmount As Decimal, _
                                                ByVal TransDescrip As String) As String
        'Connect To The Database
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        Dim sb As New StringBuilder
        Dim intRowCount As Integer = 0
        Dim dr As OleDbDataReader
        Dim strTransactionId As String = ""
        Try
            With sb
                .Append(" select Top 1 TransactionId from saTransactions where StuEnrollId=? and TransDate=? and Transreference=? ")
                .Append(" and AcademicyearId=? and TransDescrip=? and TransAmount=? ")
            End With


            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TransDate", TransDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@TransReference", Transreference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AcademicyearId", AcademicYearId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TransDescrip", TransDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TransAmount", TransAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            'execute the query
            dr = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read
                strTransactionId = CType(dr("TransactionId"), Guid).ToString
            End While

            If Not dr.IsClosed Then dr.Close()

            Return strTransactionId
        Catch ex As OleDbException
            '   return an error to the client
            Return ""
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function Add_saTransaction(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef TransactionID As String, ByVal StuEnrollID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "", Optional ByVal SourceToTarget As String = "") As String
        Dim sbTransaction As New System.Text.StringBuilder(1000)
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(StuEnrollID) Then
            Return "Unable to match financial aid data with student " & StudentName & " enrollment information"
            Exit Function
        End If
        TransactionID = Guid.NewGuid.ToString

        'Update the Scheduled Disbursement Date with Actual Disbursement Date
        Dim sbUpdateDisbDate As New StringBuilder
        Dim strReference As String
        Dim strFundDescription As String
        Dim sbFundDescrip As New StringBuilder
        Dim strTransDescrip As String

        Dim sbAcademicYear As New StringBuilder
        Dim strAwardYear As String
        Dim strAwardYearInput As String
        Dim strLoanBeginDate, strLoanEndDate As String
        Dim strFaidCount As Integer = msgEntry.strFAID.Length
        'If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
        '    strAwardYearInput = Mid(msgEntry.strFAID, strFaidCount, 1)
        '    strAwardYearInput = "200" & (strAwardYearInput - 1)
        'Else
        '    strAwardYearInput = Mid(msgEntry.strFAID, 11, 1)
        '    strAwardYearInput = "200" & (strAwardYearInput - 1)
        'End If
        If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
            strAwardYearInput = Mid(msgEntry.strFAID, strFaidCount, 1)
            strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
            'strAwardYearInput = "200" & (strAwardYearInput - 1)
        Else
            strAwardYearInput = Mid(msgEntry.strFAID, 11, 1)
            strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
            'strAwardYearInput = "200" & (strAwardYearInput - 1)
        End If
        If strAwardYearInput = "" Then
            Return "Unable to match the award year from Fame ESP with Advantage Award Year for student " & StudentName
            Exit Function
        End If
        With sbAcademicYear
            .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
        End With
        db.ClearParameters()
        db.AddParameter("@AcademicYearDescrip", strAwardYearInput, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strAwardYear = CType(db.RunParamSQLScalar(sbAcademicYear.ToString), Guid).ToString
        Catch ex As System.Exception
            strAwardYear = System.DBNull.Value.ToString
        End Try

        With sbFundDescrip
            .Append("select Top 1 FundSourceDescrip from saFundSources where AdvFundSourceId=?")
        End With
        db.ClearParameters()
        db.AddParameter("@AdvFundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        Try
            strFundDescription = db.RunParamSQLScalar(sbFundDescrip.ToString)
        Catch ex As System.Exception
            strFundDescription = ""
        End Try

        strTransDescrip = "Famelink"
        If Not strFundDescription = "" Then
            strTransDescrip &= "-" & strFundDescription
        End If

        'Get the Reference should be a combination of FameLink-FundSource-CheckNumber
        'FameLink-Perkins-1234
        strReference = "Famelink"
        If Not strFundDescription = "" Then
            strReference &= "-" & strFundDescription
        End If
        If Not msgEntry.strCheckNo = "" Then
            strReference &= "-" & Trim(msgEntry.strCheckNo)
        End If

        Try
            Dim TransTypeID As Integer = 2
            Dim IsPosted As Boolean = True
            Dim IsAutomatic As Boolean = False
            Select Case msgEntry.strMsgType
                Case "RCVD"
                    TransTypeID = 2
                Case "REFU"
                    TransTypeID = 1  'Adjustments
            End Select

            Dim intDuplicates As Integer = 0
            Dim sbDuplicates As New StringBuilder
            Dim sbgetTransactionId As New StringBuilder
            Dim strAmount As Decimal
            If TransTypeID = 2 Then
                strAmount = msgEntry.decAmount1 * (-1.0)
            Else
                strAmount = msgEntry.decAmount1
            End If

            'This section of code was commented on 10/16/2007 by balaji
            'Reason:There may be instances where a student may have multiple disbursements with same amount
            'and they should considered as separate transaction and not as one transaction, so we need to
            'remove validation for duplicates otherwise second transaction will be considered a duplicate

            'Go into this if condition only during initial import and not when file is reprocessed
            'if moduser <> 'famelink' means that data came from school and not from FameESP.
            'If Not Trim(SourceToTarget) = "exceptiontotarget" Then
            '    With sbDuplicates
            '        .Append("select Count(*) from saTransactions where StuEnrollId=? and AcademicYearId=? and TransAmount=? and (moduser <> 'famelink' or modUser is NULL) ")
            '    End With
            '    db.ClearParameters()
            '    db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    If TransTypeID = 2 Then
            '        db.AddParameter("@TransAmount", strAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            '    Else
            '        db.AddParameter("@TransAmount", strAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            '    End If
            '    Try
            '        intDuplicates = db.RunParamSQLScalar(sbDuplicates.ToString)
            '        If intDuplicates >= 1 Then
            '            sbDuplicates.Remove(0, sbDuplicates.Length)
            '            With sbDuplicates
            '                .Append("select Top 1 TransactionId from saTransactions where StuEnrollId=? and AcademicYearId=? and TransAmount=? and (moduser <> 'famelink' or ModUser is NULL) ")
            '            End With
            '            db.ClearParameters()
            '            db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '            db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '            If TransTypeID = 2 Then
            '                db.AddParameter("@TransAmount", strAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            '            Else
            '                db.AddParameter("@TransAmount", strAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            '            End If
            '            Try
            '                TransactionID = CType(db.RunParamSQLScalar(sbDuplicates.ToString), Guid).ToString
            '            Catch ex As System.Exception
            '            End Try
            '            Return ""
            '            Exit Function
            '        Else
            '            intDuplicates = 0
            '        End If
            '    Catch ex As System.Exception
            '        intDuplicates = 0
            '    End Try
            'End If

            ' groupTrans = myconn.BeginTransaction()
            'If intDuplicates = 0 Then

            Dim intCheckForDuplicates As Integer = 0
            If TransTypeID = 2 Then
                intCheckForDuplicates = CheckForDuplicateTransactions(StuEnrollID, msgEntry.dateDate1, strReference, strAwardYear, msgEntry.decAmount1 * (-1.0), strTransDescrip)
            Else
                intCheckForDuplicates = CheckForDuplicateTransactions(StuEnrollID, msgEntry.dateDate1, strReference, strAwardYear, msgEntry.decAmount1, strTransDescrip)
            End If
            If intCheckForDuplicates = 0 Then
                With sbTransaction
                    .Append("INSERT INTO saTransactions(TransactionID,StuEnrollID,TransDate,TransAmount,CreateDate,TransReference,TransDescrip,moduser,moddate,TransTypeID,IsPosted,IsAutomatic,AcademicYearId) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@TransDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If TransTypeID = 2 Then
                    db.AddParameter("@TransAmount", msgEntry.decAmount1 * (-1.0), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                Else
                    db.AddParameter("@TransAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                End If
                db.AddParameter("@CreateDate", msgEntry.dateDate2, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransReference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@TransDescrip", strTransDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransTypeID", TransTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsPosted", IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@IsAutomatic", IsAutomatic, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'execute the query
                Try
                    db.RunParamSQLExecuteNoneQuery(sbTransaction.ToString)
                    Return ""
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to apply payment for " & StudentName
                    Exit Function
                End Try
            Else
                'If there are duplicates return the transactionid
                If TransTypeID = 2 Then
                    TransactionID = GetTransactionId(StuEnrollID, msgEntry.dateDate1, strReference, strAwardYear, msgEntry.decAmount1 * (-1.0), strTransDescrip)
                Else
                    TransactionID = GetTransactionId(StuEnrollID, msgEntry.dateDate1, strReference, strAwardYear, msgEntry.decAmount1, strTransDescrip)
                End If
            End If
            'End If
            'Catch e As OleDbException
            '    m_ExceptionMessage = DALExceptions.BuildErrorMessage(e)
            '    msgEntry.strError = m_ExceptionMessage
            'Catch e As ArgumentNullException
            '    m_ExceptionMessage = e.Message
            'Catch e As System.Exception
            '    m_ExceptionMessage = e.Message
            '    Throw
        Finally
        End Try
    End Function
    Public Function Add_saPayments(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal TransactionID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "", Optional ByVal StudentAwardId As String = "", Optional ByVal PmtDisbRelId As String = "") As String
        Dim sbPayments As New System.Text.StringBuilder(1000)
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(TransactionID) Then
            Return "Unable to apply payments for student " & StudentName & " as transaction is not found"
            Exit Function
        End If

        Dim intDuplicates As Integer = 0
        Dim sbAwardScheduleId As New StringBuilder
        Dim sbDuplicates As New StringBuilder
        Dim sbgetTransactionId As New StringBuilder
        Dim strAwardScheduleValueId As String
        Dim PaymentTypeID As Integer = 4 'EFT
        Dim SchedulePayment As Boolean = True
        Dim IsDeposited As Boolean = True
        With sbDuplicates
            .Append(" select Count(*) from saPayments where TransactionId=? and PaymentTypeId=?")
        End With
        db.ClearParameters()
        db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PaymentTypeID", PaymentTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        Try
            intDuplicates = db.RunParamSQLScalar(sbDuplicates.ToString)
            If intDuplicates >= 1 Then
                Return ""
                Exit Function
            End If
        Catch ex As System.Exception
        End Try

        If intDuplicates = 0 Then
            Try
                With sbPayments
                    .Append("INSERT INTO saPayments(TransactionID, CheckNumber, ScheduledPayment, PaymentTypeID, IsDeposited, moduser, moddate) ")
                    .Append("VALUES(?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CheckNumber", Trim(msgEntry.strCheckNo), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ScheduledPayment", SchedulePayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@PaymentTypeID", PaymentTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsDeposited", SchedulePayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                Try
                    db.RunParamFLSQLExecuteNoneQuery(sbPayments.ToString)
                    'Update the Scheduled Disbursement Date with Actual Disbursement Date
                    Dim sbUpdateDisbDate As New StringBuilder
                    Dim strReference As String
                    Dim strFundDescription As String
                    Dim sbFundDescrip As New StringBuilder
                    With sbFundDescrip
                        .Append("select Top 1 FundSourceDescrip from saFundSources where AdvFundSourceId=?")
                    End With
                    db.ClearParameters()
                    db.AddParameter("@AdvFundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                    Try
                        strFundDescription = db.RunParamSQLScalar(sbFundDescrip.ToString)
                    Catch ex As System.Exception
                        strFundDescription = ""
                    End Try

                    'Get the Reference should be a combination of FameLink-FundSource-CheckNumber
                    'FameLink-Perkins-1234
                    strReference = "Famelink"
                    If Not strFundDescription = "" Then
                        strReference &= "-" & strFundDescription
                    End If
                    If Not msgEntry.strCheckNo = "" Then
                        strReference &= "-" & Trim(msgEntry.strCheckNo)
                    End If

                    strAwardScheduleValueId = ""
                    With sbAwardScheduleId
                        .Append(" select Top 1  AwardScheduleId from faStudentAwardSchedule ")
                        .Append(" where StudentAwardId=? and (Reference is Null or Reference='') ")
                        .Append(" and Amount=? and ExpectedDate=? order by ExpectedDate ")
                    End With
                    db.ClearParameters()
                    db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    db.AddParameter("@ExpectedDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    Try
                        strAwardScheduleValueId = CType(db.RunParamSQLScalar(sbAwardScheduleId.ToString), Guid).ToString
                    Catch ex As System.Exception
                        strAwardScheduleValueId = ""
                    End Try

                    If Not strAwardScheduleValueId.Trim = Guid.Empty.ToString Then
                        'means there exist a disbursement with scheduled date that matches the disbursement date
                        With sbUpdateDisbDate
                            .Append(" update faStudentAwardSchedule set ExpectedDate=?,Reference=? where AwardScheduleId=(select Top 1  AwardScheduleId from faStudentAwardSchedule where StudentAwardId=? and (Reference is Null or Reference='') and Amount=? and ExpectedDate=? order by ExpectedDate) ")
                        End With
                        db.ClearParameters()
                        db.AddParameter("@ExpectedDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                        db.AddParameter("@Reference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                        db.AddParameter("@ExpectedDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                        Try
                            db.RunParamSQLExecuteNoneQuery(sbUpdateDisbDate.ToString)
                        Catch ex As System.Exception
                            'Can put a message here but may disrupt the flow of RCVD Messages
                            'so do nothing in the exception
                            Return "Unable to update the reference for student " & StudentName
                            Exit Function
                        End Try
                        sbUpdateDisbDate.Remove(0, sbUpdateDisbDate.Length)
                    Else
                        ' When disbursement date does not match the scheduled date
                        ' search by award and amount
                        With sbUpdateDisbDate
                            .Append(" update faStudentAwardSchedule set ExpectedDate=?,Reference=? where AwardScheduleId=(select Top 1  AwardScheduleId from faStudentAwardSchedule where StudentAwardId=? and (Reference is Null or Reference='') and Amount=?  order by ExpectedDate) ")
                        End With
                        db.ClearParameters()
                        db.AddParameter("@ExpectedDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                        db.AddParameter("@Reference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                        Try
                            db.RunParamSQLExecuteNoneQuery(sbUpdateDisbDate.ToString)
                        Catch ex As System.Exception
                            'Can put a message here but may disrupt the flow of RCVD Messages
                            'so do nothing in the exception
                            Return "Unable to post payment for " & StudentName
                            Exit Function
                        End Try
                        sbUpdateDisbDate.Remove(0, sbUpdateDisbDate.Length)
                    End If



                    'With sbAwardScheduleId
                    '    .Append(" select Top 1 AwardScheduleId from faStudentAwardSchedule where StudentAwardId=? and Amount=? and ExpectedDate=? and Reference=? ")
                    'End With
                    'db.ClearParameters()
                    'db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    'db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    'db.AddParameter("@ExpectedDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    'db.AddParameter("@Reference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                    'Try
                    '    strAwardScheduleValueId = CType(db.RunParamSQLScalar(sbAwardScheduleId.ToString), Guid).ToString
                    'Catch ex As System.Exception
                    '    strAwardScheduleValueId = ""
                    'End Try
                    'sbAwardScheduleId.Remove(0, sbAwardScheduleId.Length)


                    'If Not strAwardScheduleValueId = "" Then
                    '    With sbUpdateDisbDate
                    '        .Append(" update saPmtDisbRel set AwardScheduleId=? where PmtDisbRelId=? ")
                    '    End With
                    '    db.ClearParameters()
                    '    db.AddParameter("@AwardScheduleId", strAwardScheduleValueId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    '    db.AddParameter("@PmtDisbRelId", PmtDisbRelId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                    '    Try
                    '        db.RunParamSQLExecuteNoneQuery(sbUpdateDisbDate.ToString)
                    '    Catch ex As System.Exception
                    '        'Can put a message here but may disrupt the flow of RCVD Messages
                    '        'so do nothing in the exception
                    '    End Try
                    'End If
                    Return ""
                Catch ex As System.Exception
                    Return "Unable to apply payment for the selected transaction for student" & StudentName
                    Exit Function
                End Try



                'Catch e As OleDbException
                '    m_ExceptionMessage = DALExceptions.BuildErrorMessage(e)
                '    msgEntry.strError = m_ExceptionMessage
                'Catch e As ArgumentNullException
                '    m_ExceptionMessage = e.Message
                'Catch e As System.Exception
                '    m_ExceptionMessage = e.Message
                '    Throw
            Finally
            End Try
        End If
        'Return Add_saPayments
    End Function
    Public Function Add_saRefunds(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal TransactionID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "") As String
        Dim sbRefunds As New System.Text.StringBuilder(1000)
        Dim sbDuplicates As New StringBuilder
        Dim intDuplicates As Integer = 0

        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(TransactionID) Then
            Return "Unable to apply refund as transaction provided for student " & StudentName & " is not found"
            Exit Function
        End If
        Try
            Dim FundSourceID As String = " "
            Dim RefundTypeID As Integer = 1
            Dim strGetFunSourceMessage As String = ""
            strGetFunSourceMessage = GetFundSourceID(db, FundSourceID, RefundTypeID, msgEntry.strFund)
            If Not strGetFunSourceMessage = "" Then
                Return "Unable to apply refund as the provided fund source for student " & StudentName & " was not found"
                Exit Function
            End If

            With sbDuplicates
                .Append(" select Count(*) from saRefunds where TransactionId=? and RefundTypeId=?")
            End With
            db.ClearParameters()
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@RefundTypeId", RefundTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Try
                intDuplicates = db.RunParamSQLScalar(sbDuplicates.ToString)
                If intDuplicates >= 1 Then
                    Return ""
                    Exit Function
                End If
            Catch ex As System.Exception
                intDuplicates = 0
            End Try

            If intDuplicates = 0 Then
                With sbRefunds
                    .Append("INSERT INTO saRefunds(TransactionID, RefundTypeID, FundSourceID, moduser, moddate) ")
                    .Append("VALUES(?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@RefundTypeID", RefundTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@FundSourceID", FundSourceID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                Try
                    db.RunParamSQLExecuteNoneQuery(sbRefunds.ToString)
                    Return ""
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to apply refund for student " & StudentName
                    Exit Function
                End Try
            End If
            'Catch e As OleDbException
            '    m_ExceptionMessage = DALExceptions.BuildErrorMessage(e)
            '    msgEntry.strError = m_ExceptionMessage
            'Catch e As ArgumentNullException
            '    m_ExceptionMessage = e.Message
            'Catch e As System.Exception
            '    m_ExceptionMessage = e.Message
            '    Throw
        Finally
        End Try

        'Return Add_saRefunds
    End Function
    Public Function Add_saPmtDisbRel(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef PmtDisbRelID As String, ByVal TransactionID As String, ByVal StuAwardID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "") As String
        Dim sbPmtDisbRel As New System.Text.StringBuilder(1000)
        Dim sbPmtDisbRel1 As New System.Text.StringBuilder(1000)
        Dim intDisbCount As Integer = 0
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(TransactionID) Then
            Return "Unable to add payment disbursement for student" & StudentName & " as provided transaction Id is not found"
            Exit Function
        End If
        If String.IsNullOrEmpty(StuAwardID) Then
            Return "Unable to add payment disbursement as student award is missing for student " & StudentName
            Exit Function
        End If
        PmtDisbRelID = Guid.NewGuid.ToString
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        Dim myconn As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString").ToString)
        myconn.Open()



        Try
            With sbPmtDisbRel
                .Append("Select Count(*) from saPmtDisbRel where TransactionID=? and Amount=? and StuAwardID=? ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                intDisbCount = db.RunParamSQLScalar(sbPmtDisbRel.ToString)
                sbPmtDisbRel.Remove(0, sbPmtDisbRel.Length)
                If intDisbCount >= 1 Then
                    With sbPmtDisbRel
                        .Append("Select Top 1 PmtDisbRelId from saPmtDisbRel where TransactionID=? and Amount=? and StuAwardID=? ")
                    End With
                    'Add params
                    db.ClearParameters()
                    db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Try
                        PmtDisbRelID = CType(db.RunParamSQLScalar(sbPmtDisbRel.ToString), Guid).ToString
                        sbPmtDisbRel.Remove(0, sbPmtDisbRel.Length)
                        Return ""
                        Exit Function
                    Catch ex As System.Exception
                    End Try
                End If
            Catch ex As System.Exception
                Return "Unable to apply payment for student " & StudentName
                Exit Function
            End Try

            Dim sbAwardScheduleId As New StringBuilder
            Dim strAwardScheduleValueId As String
            With sbAwardScheduleId
                .Append(" select Top 1 AwardScheduleId from faStudentAwardSchedule where StudentAwardId=? and (Reference is Null or Reference='') and Amount=? AND ExpectedDate=? order by ExpectedDate ")
            End With
            db.ClearParameters()
            db.AddParameter("@StudentAwardId", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@ExpectedDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Try
                strAwardScheduleValueId = CType(db.RunParamSQLScalar(sbAwardScheduleId.ToString), Guid).ToString
            Catch ex As System.Exception
                strAwardScheduleValueId = ""
            End Try
            sbAwardScheduleId.Remove(0, sbAwardScheduleId.Length)

            If strAwardScheduleValueId = "" Or strAwardScheduleValueId = Guid.Empty.ToString Then
                With sbAwardScheduleId
                    .Append(" select Top 1 AwardScheduleId from faStudentAwardSchedule where StudentAwardId=? and (Reference is Null or Reference='') and Amount=?  order by ExpectedDate ")
                End With
                db.ClearParameters()
                db.AddParameter("@StudentAwardId", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

                Try
                    strAwardScheduleValueId = CType(db.RunParamSQLScalar(sbAwardScheduleId.ToString), Guid).ToString
                Catch ex As System.Exception
                    strAwardScheduleValueId = ""
                End Try
                sbAwardScheduleId.Remove(0, sbAwardScheduleId.Length)
            End If


            With sbPmtDisbRel1
                .Append("INSERT INTO saPmtDisbRel(PmtDisbRelID,TransactionID,Amount,StuAwardID,AwardScheduleId,moduser,moddate) ")
                .Append("VALUES(?,?,?,?,?,?,?) ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@PmtDisbRelID", PmtDisbRelID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not strAwardScheduleValueId = "" And Not strAwardScheduleValueId = Guid.Empty.ToString Then
                db.AddParameter("@AwardScheduleId", strAwardScheduleValueId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AwardScheduleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sbPmtDisbRel1.ToString)
                Return ""
                Exit Function
            Catch ex As System.Exception
                Dim sbGetAmount As New StringBuilder
                Dim strGetAmount As String = ""
                Dim sMessage As String = ""
                With sbGetAmount
                    .Append(" select Top 1 Amount from faStudentAwardSchedule where StudentAwardId=? and (Reference is Null or Reference='') and ExpectedDate=? order by ExpectedDate ")
                End With
                db.ClearParameters()
                db.AddParameter("@StudentAwardId", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ExpectedDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Try
                    strGetAmount = CType(db.RunParamSQLScalar(sbGetAmount.ToString), Decimal).ToString
                Catch ex9 As Exception
                    strGetAmount = ""
                End Try
                sMessage = "Unable to apply payment as the disbursement amount did not match for student " & StudentName & "." & vbLf
                If Not strGetAmount = "" Then
                    sMessage &= "Disbursement amount in Advantage : " & CType(strGetAmount, Decimal).ToString("#0.00")
                End If
                Return sMessage
                Exit Function
            End Try

            'Catch e As OleDbException
            '    m_ExceptionMessage = DALExceptions.BuildErrorMessage(e)
            '    msgEntry.strError = m_ExceptionMessage
            'Catch e As ArgumentNullException
            '    m_ExceptionMessage = e.Message
            'Catch e As System.Exception
            '    m_ExceptionMessage = e.Message
            '    Throw
        Finally
        End Try
        'Return Add_saPmtDisbRel
    End Function
    Public Function Add_faAwardSchedule(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef AwardScheduleID As String, ByVal StudentAwardID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal ExceptionGUID As String = "") As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwards As New StringBuilder
        Dim sbCheckDuplicateAwardsNoDate As New StringBuilder
        Dim intDuplicatesNoDate As Integer = 0
        Dim intDuplicates As Integer = 0
        m_ExceptionMessage = ""
        'Add_faAwardSchedule = False
        If String.IsNullOrEmpty(StudentAwardID) Then
            Return "Unable to add disbursement as student award for student " & StudentName & " cannot be empty"
            Exit Function
        End If
        AwardScheduleID = Guid.NewGuid.ToString
        Try
            'Check for duplicates based on AwardId, amount and expected date
            Dim intCheckDuplicatesByAwardIdExpectedDateAndAmount As Integer = GetRecordCountByAwardIdDateAndAmount(StudentAwardID, msgEntry.decAmount1, msgEntry.dateDate1)
            If intCheckDuplicatesByAwardIdExpectedDateAndAmount >= 1 Then
                Return ""
                Exit Function
            End If

            If intCheckDuplicatesByAwardIdExpectedDateAndAmount = 0 Then
                Dim strReturn As String = ""
                strReturn = InsertAwardSchedule(db, AwardScheduleID, StudentAwardID, msgEntry.dateDate1, msgEntry.decAmount1, msgEntry.strModUser, msgEntry.dateModDate, StudentName)
            End If
        Finally
        End Try
    End Function
    Public Function GetExpectedDateByAwardIdAndAmount(ByRef db As DataAccess, _
                                                      ByVal StudentAwardID As String, _
                                                      ByVal decAmount As Decimal, _
                                                      ByVal FAID As String, _
                                                      ByVal ExceptionGUID As String) As String
        Dim sbGetScheduledDisbursementDate As New StringBuilder
        Dim drDiffDisbDate As OleDbDataReader
        Dim strExpectedDate As String = ""
        Dim strDisbAwardScheduleId As String = ""
        Dim strReturnMessage As String = ""
        With sbGetScheduledDisbursementDate
            .Append(" Select Distinct ExpectedDate,AwardScheduleId from faStudentAwardSchedule where ")
            .Append(" StudentAwardID=? and Amount=?   ")
            'a new field isProcessed has been added to faStudentAwardSchedule table on 06/10/2009
            'If there are two disbursements with same amount for a student award, the exception needs to capture the dates
            'its not possible to capture the dates with out this isProcessed field.In the absence of this field the exception
            'always captures the first date even if there is a second date
            .Append(" and (Reference is NULL or Reference='') and isProcessed=0 ")
            If Not ExceptionGUID = "" Then
                .Append(" and ExpectedDate not in (select Distinct DisbursementDate from syFameESPExceptionReport where FAID=? and ExceptionGUID='" & ExceptionGUID & "') ")
            End If
            .Append(" order by ExpectedDate ")
        End With

        'Add params
        db.ClearParameters()
        db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@Amount", decAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
        db.AddParameter("@FAID", FAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            drDiffDisbDate = db.RunParamSQLDataReader(sbGetScheduledDisbursementDate.ToString())
            If Not (drDiffDisbDate.HasRows) Then
                strExpectedDate = ""
            End If
            While drDiffDisbDate.Read()
                'get the expected date by ordering by expected date and then check if the date is not in exception report
                strExpectedDate = CType(drDiffDisbDate("ExpectedDate"), Date).ToString
                strDisbAwardScheduleId = CType(drDiffDisbDate("AwardScheduleId"), Guid).ToString
                strReturnMessage = UpdateProcessedFieldInAwardSchedule(strDisbAwardScheduleId)
                Return strExpectedDate
                Exit While
            End While
        Catch ex As System.Exception
            Return ""
        Finally
            drDiffDisbDate.Close()
        End Try
    End Function
    Public Function GetExpectedDateByFAIDandGUID(ByRef db As DataAccess, _
                                                    ByVal FAID As String, _
                                                    ByVal ExceptionGUID As String) As String
        Dim sbGetScheduledDisbursementDate As New StringBuilder
        Dim drDiffDisbDate As OleDbDataReader
        Dim strExpectedDate As String = ""
        Dim strDisbAwardScheduleId As String = ""
        Dim strReturnMessage As String = ""
        With sbGetScheduledDisbursementDate
            .Append(" select Distinct DisbursementDate from syFameESPExceptionReport where FAID=? and ExceptionGUID='" & ExceptionGUID & "' and (DisbursementDate is NOT NULL or DisbursementDate <> '') ")
        End With

        'Add params
        db.ClearParameters()
        db.AddParameter("@FAID", FAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            drDiffDisbDate = db.RunParamSQLDataReader(sbGetScheduledDisbursementDate.ToString())
            If Not (drDiffDisbDate.HasRows) Then
                strExpectedDate = ""
            End If
            While drDiffDisbDate.Read()
                'get the expected date by ordering by expected date and then check if the date is not in exception report
                strExpectedDate = CType(drDiffDisbDate("DisbursementDate"), Date).ToString
                Return strExpectedDate
                Exit While
            End While
        Catch ex As System.Exception
            Return ""
        Finally
            drDiffDisbDate.Close()
        End Try
    End Function
    Public Function InsertAwardSchedule(ByRef db As DataAccess, ByVal AwardScheduleID As String, _
                                        ByVal StudentAwardID As String, _
                                        ByVal dateDate As Date, _
                                        ByVal decAmount As Decimal, _
                                        ByVal ModUser As String, _
                                        ByVal ModDate As Date, _
                                        ByVal StudentName As String) As String
        Dim sbAwardSchedule As New StringBuilder
        With sbAwardSchedule
            .Append("INSERT INTO faStudentAwardSchedule(AwardScheduleID,StudentAwardID,ExpectedDate,Amount,moduser,moddate) ")
            .Append("VALUES(?,?,?,?,?,?) ")
        End With

        'Add params
        db.ClearParameters()
        db.AddParameter("@AwardScheduleID", AwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ExpectedDate", dateDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@Amount", decAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
        db.AddParameter("@moduser", ModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@moddate", ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

        Try
            db.RunParamFLSQLExecuteNoneQuery(sbAwardSchedule.ToString)
            Return ""
            Exit Function
        Catch ex As System.Exception
            Return "Unable to add disbursement for the selected student award for student" & StudentName
            Exit Function
        End Try
    End Function
    Public Function UpdateAwardSchedule(ByRef db As DataAccess, ByVal AwardScheduleID As String, _
                                     ByVal StudentAwardID As String, _
                                     ByVal dateDate As Date, _
                                     ByVal decAmount As Decimal, _
                                     ByVal ModDate As Date, _
                                     ByVal StudentName As String) As String
        Dim sbAwardSchedule As New StringBuilder
        With sbAwardSchedule
            .Append(" Update faStudentAwardSchedule set StudentAwardID=?,ExpectedDate=?, ")
            .Append(" Amount=?,moddate=? ")
            .Append(" WHERE AwardScheduleId=? ")
        End With

        'Add params
        db.ClearParameters()
        db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ExpectedDate", dateDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@Amount", decAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
        db.AddParameter("@moddate", ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@AwardScheduleId", AwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            db.RunParamFLSQLExecuteNoneQuery(sbAwardSchedule.ToString)
            Return ""
            Exit Function
        Catch ex As System.Exception
            Return "Unable to update schedules for the selected student award for student" & StudentName
            Exit Function
        End Try
    End Function
    Public Function Add_ChangefaAwardSchedule(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef AwardScheduleID As String, ByVal StudentAwardID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "") As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwards As New StringBuilder
        Dim intDuplicates As Integer
        m_ExceptionMessage = ""
        'Add_faAwardSchedule = False
        If String.IsNullOrEmpty(StudentAwardID) Then
            Return "Unable to add award schedule as student award for student " & StudentName & " cannot be empty"
            Exit Function
        End If

        AwardScheduleID = Guid.NewGuid.ToString
        'Troy: 7/25/2012
        'The orginal code was doing a duplicates check to see if there was an existing scheduled disbursement for the award for
        'the same amount and date. If there was a match it would simply skipped the record by returning an empty string. In the UI
        'this ended up being seen as the import was successful which was misleading since the scheduled disbursement record was
        'never created. The school can add a new scheduled disbursement with the same date and amount as an existing scheduled
        'disbursement on an award. That validation was therefore deleted. We are simply inserting the record since it is coming from
        'ESP and the school can add the new scheduled disbursement for whatever date and amount appropriate based on the student's
        'situation.
        
        With sbAwardSchedule
            .Append("INSERT INTO faStudentAwardSchedule(AwardScheduleID,StudentAwardID,ExpectedDate,Amount,moduser,moddate) ")
            .Append("VALUES(?,?,?,?,?,?) ")
        End With

        'Add params
        db.ClearParameters()
        db.AddParameter("@AwardScheduleID", AwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ExpectedDate", msgEntry.dateDate2, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@Amount", msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
        db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)


        Try
            db.RunParamFLSQLExecuteNoneQuery(sbAwardSchedule.ToString)
            Return ""
            Exit Function
        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Return "Unable to add scheduled disbursement for the selected student award for student" & StudentName & " Reason:" & ex.Message.ToString
            Else
                Return "Unable to add scheduled disbursement for the selected student award for student" & StudentName & " Reason:" & ex.InnerException.Message.ToString
            End If

            Exit Function
        End Try
       
    End Function
    Public Function Add_faStudentAwards(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef StudentAwardID As String, ByVal StuEnrollID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal strStudentName As String = "", Optional ByVal SourceToTarget As String = "") As String
        Dim sbStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbUpdateStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchool As New System.Text.StringBuilder(1000)
        Dim sbUpdateStudentAwards1 As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchoolDiffLoanPeriods As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchoolWithDiffGrossAmount As New System.Text.StringBuilder(1000)
        Dim intDuplicatesBySchoolDiffGrossAmount As Integer = 0
        Dim intDuplicatesBySchoolSameAwardYearDiffLoanPeriods As Integer = 0



        Dim AwardTypeID As String = " "
        Dim getAwardTypeMessage As String = ""
        Dim Disbursements As Integer = 0
        Dim LoanID As String = " "
        Dim intDuplicates As Integer = 0
        Dim intDuplicatesBySchool As Integer = 0
        Dim intDuplicatesBySchoolDiffLoanPeriods As Integer = 0

        Dim sbAcademicYear As New StringBuilder
        Dim strAwardYear As String
        Dim strLoanBeginDate, strLoanEndDate As String
        Dim dr As OleDbDataReader
        m_ExceptionMessage = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'db.OpenConnection()

        If String.IsNullOrEmpty(StuEnrollID) Or StuEnrollID = Guid.Empty.ToString Then
            Return "Unable to find enrollment information for the student" & strStudentName
            'db.CloseConnection()
            Exit Function
        End If

        If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
            strLoanBeginDate = "07/01/" + Mid(msgEntry.strAwdyr, 1, 4)
            strLoanEndDate = "06/30/" + Mid(msgEntry.strAwdyr, 6, 2)
        Else
            strLoanBeginDate = msgEntry.dateDate1
            strLoanEndDate = msgEntry.dateDate2
        End If


        'Look for matching records
        StudentAwardID = GetMatchingStudentAwards(db, StuEnrollID, msgEntry.strFAID, msgEntry.decAmount1, strLoanBeginDate, strLoanEndDate, strStudentName, msgEntry.strFund)
        If Not StudentAwardID = "" Then
            'Update existing record with this FAID
            With sbUpdateStudentAwards
                .Append("Update faStudentAwards set FA_Id=?,moduser=?,moddate=? where StudentAwardId=? ")
            End With
            db.ClearParameters()
            db.AddParameter("@FA_ID", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moduser", "famelink", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@StudentAwardId", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Try
                db.RunParamSQLExecuteNoneQuery(sbUpdateStudentAwards.ToString)
                Return ""
                'db.CloseConnection()
                Exit Function
            Catch ex As System.Exception
                Return "Unable to match/update FAID with student awards for student " & strStudentName
                'db.CloseConnection()
                Exit Function
            End Try
        Else
            StudentAwardID = Guid.NewGuid.ToString
        End If

        'Before Creating new awards check to see if the Award Start Date falls after the AwardsCuttOffDate
        'in web.config
        Try
            getAwardTypeMessage = GetAwardTypeID(db, AwardTypeID, msgEntry.strFund, msgEntry.strYYYY)
            If Not getAwardTypeMessage = "" Then
                Return "Unable to add student awards for student " & strStudentName & " as the provided award type was not found"
                'db.CloseConnection()
                Exit Function
            End If

            Dim strAwardYearInput As String
            Dim strFaidCount As Integer = msgEntry.strFAID.Length
            If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                strAwardYearInput = Mid(msgEntry.strFAID, strFaidCount, 1)
                strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
                'strAwardYearInput = "200" & (strAwardYearInput - 1)
            Else
                strAwardYearInput = Mid(msgEntry.strFAID, 11, 1)
                strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
                'strAwardYearInput = "200" & (strAwardYearInput - 1)
            End If
            If strAwardYearInput = "" Then
                Return "Unable to match the award year from Fame ESP with Advantage Award Year for student " & strStudentName
                Exit Function
            End If
            With sbAcademicYear
                .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
            End With
            db.ClearParameters()
            db.AddParameter("@AcademicYearDescrip", strAwardYearInput, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                dr = db.RunParamSQLDataReader(sbAcademicYear.ToString())
                If Not (dr.HasRows) Then
                    Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
                    Exit Function
                End If
                While dr.Read()
                    If Not (dr("AcademicYearId") Is System.DBNull.Value) Or Not (dr("AcademicYearId").ToString = Guid.Empty.ToString) Then
                        strAwardYear = dr("AcademicYearId").ToString
                    Else
                        Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
                        Exit Function
                    End If
                End While
            Catch ex As System.Exception
                Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
                Exit Function
            Finally
                dr.Close()
            End Try

            'With sbAcademicYear
            '    .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
            'End With
            'db.ClearParameters()
            'db.AddParameter("@AcademicYearDescrip", Mid(msgEntry.strAwdyr, 1, 4), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ''Try
            ''    strAwardYear = CType(db.RunParamSQLScalar(sbAcademicYear.ToString), Guid).ToString
            ''Catch ex As System.Exception
            ''    strAwardYear = System.DBNull.Value.ToString
            ''End Try
            'Try
            '    dr = db.RunParamSQLDataReader(sbAcademicYear.ToString())
            '    If Not (dr.HasRows) Then
            '        Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
            '        Exit Function
            '    End If
            '    While dr.Read()
            '        If Not (dr("AcademicYearId") Is System.DBNull.Value) Or Not (dr("AcademicYearId").ToString = Guid.Empty.ToString) Then
            '            strAwardYear = dr("AcademicYearId").ToString
            '        Else
            '            Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
            '            Exit Function
            '        End If
            '    End While
            'Catch ex As System.Exception
            '    Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
            '    Exit Function
            'Finally
            '    dr.Close()
            'End Try


            If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                strLoanBeginDate = "07/01/" + Mid(msgEntry.strAwdyr, 1, 4)
                strLoanEndDate = "06/30/" + Mid(msgEntry.strAwdyr, 6, 2)
            Else
                strLoanBeginDate = msgEntry.dateDate1
                strLoanEndDate = msgEntry.dateDate2
            End If

            Dim intCutOffDates As Integer
            Dim dtCuttOfDate As Date = CDate(MyAdvAppSettings.AppSettings("AwardsCutOffDate").ToString)
            Dim dtAwardStartDate As Date = CDate(strLoanBeginDate)
            intCutOffDates = DateDiff(DateInterval.Day, dtCuttOfDate, dtAwardStartDate)
            If intCutOffDates < 0 Then
                Return "The award start date " & dtAwardStartDate.ToString & " is earlier than the award cut off date (" & MyAdvAppSettings.AppSettings("AwardsCutOffDate").ToString & "), so student award will not be created for " & strStudentName
                ' db.CloseConnection()
                Exit Function
            End If

            'Checks to see if the school has put in an award for the same fund and award year
            'but for different gross amount
            'example: an award may be created by school for $4500 for Direct Loan 05-06
            'but from Fame ESP we may get a award data for $3500 and $1000 for same year
            'and this case we need to send the data($3500 and $1000) from Fame ESP to exception
            'while checking ignore Gross Amount and look for records with null FAID and moduser
            'not equal to famelink which identifies that record was put in by school
            'Mantis # 12077.
            'If Not Trim(SourceToTarget) = "exceptiontotarget" Then
            '    With sbCheckDuplicateAwardsBySchool
            '        .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
            '        .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
            '        If strAwardYear.Length >= 30 Then
            '            .Append(" and AcademicYearId = ? ")
            '        End If
            '        '.Append(" and FA_ID is NULL ")
            '        If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
            '            .Append(" and AwardStartDate=? ")
            '            .Append(" and AwardEndDate=? ")
            '        End If
            '        .Append(" and ModUser <> 'famelink' ")
            '    End With
            '    db.ClearParameters()
            '    db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            '    db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '    Try
            '        intDuplicatesBySchool = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchool.ToString)
            '        If intDuplicatesBySchool >= 1 Then
            '            Return "Unable to create student award for student " & strStudentName & " as the school has already created an award with a different gross amount for same fund source and award year"
            '            Exit Function
            '        Else
            '            intDuplicatesBySchool = 0
            '        End If
            '    Catch ex As System.Exception
            '        intDuplicatesBySchool = 0
            '    End Try

            '    'Use Case: School would have created a student award with same amount but for a different loan period
            '    'Example:Student Thomas Piehko Award Created for $3500.00
            '    'School Created an award for 11/14/ 2007 but ESP created it for 11/15/2007

            '    With sbCheckDuplicateAwardsBySchoolDiffLoanPeriods
            '        .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
            '        .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
            '        If strAwardYear.Length >= 30 Then
            '            .Append(" and AcademicYearId = ? ")
            '        End If
            '        ' .Append(" and FA_ID is NULL ")
            '        .Append(" and ModUser <> 'famelink' ")
            '    End With
            '    db.ClearParameters()
            '    db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '    Try
            '        intDuplicatesBySchoolDiffLoanPeriods = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchoolDiffLoanPeriods.ToString)
            '        If intDuplicatesBySchoolDiffLoanPeriods >= 1 Then
            '            Return "Unable to create student award for student " & strStudentName & " as the school has already created an award with a same gross amount for same fund source and award year, but for a different award period"
            '            Exit Function
            '        Else
            '            intDuplicatesBySchoolDiffLoanPeriods = 0
            '        End If
            '    Catch ex As System.Exception
            '        intDuplicatesBySchoolDiffLoanPeriods = 0
            '    End Try
            'End If

            'Checks to see if the school has put in an award for the same fund and award year
            'but for different gross amount
            'example: an award may be created by school for $4500 for Direct Loan 05-06
            'but from Fame ESP we may get a award data for $3500 and $1000 for same year
            'and this case we need to send the data($3500 and $1000) from Fame ESP to exception
            'while checking ignore Gross Amount and look for records with null FAID and moduser
            'not equal to famelink which identifies that record was put in by school
            'Mantis # 12077.
            If Not Trim(SourceToTarget) = "exceptiontotarget" Then
                'Modified by balaji on 09/27/2007 to take gross amount into consideration
                'Check to see if award exists with same gross amount and same award year and period
                With sbCheckDuplicateAwardsBySchool
                    .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
                    .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
                    If strAwardYear.Length >= 30 Then
                        .Append(" and AcademicYearId = ? ")
                    End If
                    .Append(" and GrossAmount=? ")
                    If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                        .Append(" and AwardStartDate=? ")
                        .Append(" and AwardEndDate=? ")
                    End If
                    ' .Append(" and (ModUser <> 'famelink' or ModUser is NULL) ")
                End With
                db.ClearParameters()
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                    db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If

                Try
                    intDuplicatesBySchool = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchool.ToString)
                    If intDuplicatesBySchool >= 1 Then
                        'if award exists with same gross amount and same award year and period
                        'ignore this record as Fa_id has already been updated and thats all this record can do
                        'and process the next record
                        Return ""
                        Exit Function
                    Else
                        'if award does not exists then check for different gross amount and same award year and period
                        'If so then alert message and go to exception else ignore and go to next record
                        'and process the next record
                        intDuplicatesBySchool = 0
                        With sbCheckDuplicateAwardsBySchoolWithDiffGrossAmount
                            .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
                            .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
                            If strAwardYear.Length >= 30 Then
                                .Append(" and AcademicYearId = ? ")
                            End If
                            .Append(" and GrossAmount <> ? ")
                            If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                                .Append(" and AwardStartDate=? ")
                                .Append(" and AwardEndDate=? ")
                            End If
                            ' due to issue 13840 the following line is commented 
                            '.Append(" and (ModUser <> 'famelink' or ModUser is NULL) ")
                        End With
                        db.ClearParameters()
                        db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                        If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                            db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                            db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                        End If
                        Try
                            intDuplicatesBySchoolDiffGrossAmount = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchoolWithDiffGrossAmount.ToString)
                            If intDuplicatesBySchoolDiffGrossAmount >= 1 Then
                                Dim sbGetGrossAmount As New StringBuilder
                                Dim drDiffGrossAmount As OleDbDataReader
                                Dim strAdvantageGrossAmount As String = ""
                                With sbGetGrossAmount
                                    .Append(" select Distinct GrossAmount from faStudentAwards where StuEnrollId=? and ")
                                    .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
                                    If strAwardYear.Length >= 30 Then
                                        .Append(" and AcademicYearId = ? ")
                                    End If
                                    .Append(" and GrossAmount <> ? ")
                                    If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                                        .Append(" and AwardStartDate=? ")
                                        .Append(" and AwardEndDate=? ")
                                    End If
                                    '.Append(" and (ModUser <> 'famelink' or ModUser is NULL) ")
                                End With
                                db.ClearParameters()
                                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                                If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                                    db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                                    db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                                End If
                                Try
                                    drDiffGrossAmount = db.RunParamSQLDataReader(sbGetGrossAmount.ToString())
                                    If Not (drDiffGrossAmount.HasRows) Then
                                        strAdvantageGrossAmount = ""
                                    End If
                                    While drDiffGrossAmount.Read()
                                        strAdvantageGrossAmount = CType(drDiffGrossAmount("GrossAmount"), Decimal).ToString
                                    End While
                                Catch ex As System.Exception
                                    strAdvantageGrossAmount = ""
                                Finally
                                    drDiffGrossAmount.Close()
                                End Try
                                Dim sReturnMessage As String = "Unable to create student award for student " & strStudentName & " as the school has already created an award with a different gross amount." & vbLf
                                If Not strAdvantageGrossAmount = "" Then
                                    sReturnMessage &= vbLf
                                    sReturnMessage &= " Gross Amount in Advantage: " & CType(strAdvantageGrossAmount, Decimal).ToString("#0.00")
                                End If
                                Return sReturnMessage
                                Exit Function
                                'Return "Unable to create student award for student " & strStudentName & " as the school has already created an award with a different gross amount for same fund source and award year"
                                'Dim sReturnMessage As String = "Unable to create student award for student " & strStudentName & " as the school has already created an award with a different gross amount for same fund source and award year."
                                'sReturnMessage &= " Gross Amount in Fame ESP: " & msgEntry.decAmount1
                                'Return sReturnMessage
                                'Exit Function
                            Else
                                intDuplicatesBySchoolDiffGrossAmount = 0
                            End If
                        Catch ex5 As System.Exception
                            intDuplicatesBySchoolDiffGrossAmount = 0
                        End Try
                    End If
                Catch ex As System.Exception
                    intDuplicatesBySchool = 0
                End Try

                'Troy: 2/24/2012 Modifications to handle multiple loans for an award year
                'The orignal code was written on the assumption that a student could only have one award type during
                'an award year. However, we now know that this is not true. For eg. AMC has a student with one DL Sub
                'for 9/7/2010 to 4/22/2011 and another one for 4/25/2011 to 12/17/2011. Notice that the last award cuts
                'across 7/1/2011 which is normally considered the start of an award year.
                'In the above example, when the second award came in on a HEAD file it was returned with an exception
                'message that the school had already created an award for it with a different loan period. 
                'The new logic is to check whether or not the loan period of the new award overlaps with any existing
                'award in the database. In the above example, there was no overlap so the second award would have been
                'created. However, if the award in the file was for 3/16/2011 to 12/17/2011 then the loan period overlaps
                'and an exception should be returned.
                Dim sbCheckDuplicateAwardsBySchoolDiffAwardYear As New StringBuilder
                With sbCheckDuplicateAwardsBySchoolDiffAwardYear
                    .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
                    .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")

                    'Troy: 2/24/2012 Added the if statement to handle just non-DL awards here. The academic year is not relevant
                    'when doing the check for DL since we have the loan periods and all we are interested in is whether or
                    'not there is an overlap in the loan periods. However, for non-DL awards we don't have the loan periods
                    'in the FAMELink file.
                    If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                        If strAwardYear.Length >= 30 Then
                            .Append(" and AcademicYearId = ? ")
                        End If
                    End If

                    'Troy: 2/24/2012 Added the if statement to handle just non-DL award here. See note above.
                    If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                        .Append(" and GrossAmount=? ")
                    End If

                    'Troy: 2/24/2012 The original code simply checked if the award start or end dates were different.
                    'Code is modified to check if there is any overlap. 
                    If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                        '.Append(" and (AwardStartDate <> ? or AwardEndDate <> ?) ")
                        .Append(" and ((AwardStartDate < ? and AwardEndDate > ?) or (AwardStartDate > ? and AwardEndDate < ?)) ")
                    End If
                    'check for FA_ID  to see if FA Identifier matches
                    .Append(" and FA_ID=? ")
                    '.Append(" and (ModUser <> 'famelink' or ModUser is NULL) ")
                End With
                db.ClearParameters()
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                    db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                    db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                End If

                If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                    db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@AwardEndDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                db.AddParameter("@fa_id", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    intDuplicatesBySchoolSameAwardYearDiffLoanPeriods = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchoolDiffAwardYear.ToString)
                    If intDuplicatesBySchoolSameAwardYearDiffLoanPeriods >= 1 Then
                        Dim sbGetGrossAmount As New StringBuilder
                        Dim strAwardStartDate As String = ""
                        Dim strAwardEndDate As String = ""
                        Dim drDiffLoanPeriod As OleDbDataReader
                        If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                            With sbGetGrossAmount
                                .Append(" select Distinct AwardStartDate,AwardEndDate from faStudentAwards where StuEnrollId=? and ")
                                .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
                                'Troy 2/24/2012 Commented out the two parameters below. The only thing we are interested in is whehter
                                'or not there are any overlapping loan periods. 
                                'If strAwardYear.Length >= 30 Then
                                '    .Append(" and AcademicYearId = ? ")
                                'End If
                                '.Append(" and GrossAmount=? ")
                                If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                                    '.Append(" and (AwardStartDate <> ? or AwardEndDate <> ?) ")
                                    .Append(" and ((AwardStartDate < ? and AwardEndDate > ?) or (AwardStartDate > ? and AwardEndDate < ?) ) ")
                                End If
                                ' .Append(" and (ModUser <> 'famelink' or ModUser is NULL) ")
                            End With
                            db.ClearParameters()
                            db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            'db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            'db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                            If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                                db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                                db.AddParameter("@AwardEndDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                                db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                                db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                            End If
                            Try
                                drDiffLoanPeriod = db.RunParamSQLDataReader(sbGetGrossAmount.ToString())
                                If Not (drDiffLoanPeriod.HasRows) Then
                                    strAwardStartDate = ""
                                    strAwardEndDate = ""
                                End If
                                While drDiffLoanPeriod.Read()
                                    strAwardStartDate = CType(drDiffLoanPeriod("AwardStartDate"), Date).ToString
                                    strAwardEndDate = CType(drDiffLoanPeriod("AwardEndDate"), Date).ToString
                                End While
                            Catch ex As System.Exception
                                strAwardStartDate = ""
                                strAwardEndDate = ""
                            Finally
                                drDiffLoanPeriod.Close()
                            End Try
                        End If
                        Dim sReturnMessage As String
                        sReturnMessage = "Unable to create student award for student " & strStudentName & " as the school has already created an award with a different loan period." & vbLf
                        sReturnMessage &= vbLf
                        sReturnMessage &= " Award Period from FAME ESP:" & " (" & strLoanBeginDate & " - " & strLoanEndDate & ")." & vbLf
                        If Not strAwardStartDate = "" And Not strAwardEndDate = "" Then
                            sReturnMessage &= vbLf
                            sReturnMessage &= " Award Period from advantage:" & " (" & CDate(strAwardStartDate) & " - " & CDate(strAwardEndDate) & ")."
                        End If
                        Return sReturnMessage
                        Exit Function
                        'Dim sReturnMessage As String = "Unable to create student award for student " & strStudentName & " as the school has already created an award with a same gross amount for same fund source and same award year but different loan period."
                        'sReturnMessage &= " Award Period from Fame ESP:" & " (" & strLoanBeginDate & " - " & strLoanEndDate & ")"
                        'Return sReturnMessage
                        'Exit Function
                    Else
                        intDuplicatesBySchoolSameAwardYearDiffLoanPeriods = 0
                    End If
                Catch ex9 As System.Exception
                    intDuplicatesBySchoolSameAwardYearDiffLoanPeriods = 0
                End Try
            End If

            'Check for duplicates before executing
            With sbCheckDuplicateAwards
                .Append(" Select Count(*) from faStudentAwards where ")
                .Append(" StuEnrollID=? and AwardTypeID=? and fa_id=? and GrossAmount=? ")
                If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                    .Append(" and AwardStartDate=? and AwardEndDate=? ")
                End If
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardTypeID", AwardTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@fa_id", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            Try
                intDuplicates = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
                If intDuplicates >= 1 Then
                    With sbUpdateStudentAwards1
                        .Append("Update faStudentAwards set LoanFees=? where  ")
                        .Append(" StuEnrollID=? and AwardTypeID=? and fa_id=? and GrossAmount=? ")
                        If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                            .Append(" and AwardStartDate=? and AwardEndDate=? ")
                        End If
                    End With
                    db.ClearParameters()
                    If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                        db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    Else
                        If msgEntry.decAmount2.ToString.Length >= 1 And Mid(msgEntry.decAmount2.ToString, 1) <> "0" Then
                            db.AddParameter("@LoanFees", msgEntry.decAmount1 - msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                            'db.AddParameter("@LoanFees", msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                        Else
                            db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                        End If
                    End If
                    db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardTypeID", AwardTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@fa_id", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                        db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                        db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    End If

                    Try
                        db.RunParamSQLExecuteNoneQuery(sbUpdateStudentAwards1.ToString)
                        Return ""
                        Exit Function
                    Catch ex As System.Exception
                        Return "Unable to update Loan Fees for the existing student award data for student " & strStudentName
                        Exit Function
                    End Try
                End If
            Catch ex As System.Exception
                intDuplicates = 0
            End Try



            'If there are no duplicates create a student award
            If intDuplicates = 0 Then
                With sbStudentAwards
                    .Append("INSERT INTO faStudentAwards(StudentAwardID,StuEnrollID,AwardTypeID,AcademicYearId,fa_id,GrossAmount,AwardStartDate,AwardEndDate,LoanFees,moduser,moddate,Disbursements,LoanID) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardTypeID", AwardTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@fa_id", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                    db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                Else
                    If msgEntry.decAmount2.ToString.Length >= 1 And Mid(msgEntry.decAmount2.ToString, 1) <> "0" Then
                        db.AddParameter("@LoanFees", msgEntry.decAmount1 - msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                        'db.AddParameter("@LoanFees", msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    End If
                End If
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Disbursements", Disbursements, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@LoanID", LoanID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                '   execute the query
                Try
                    db.RunParamSQLExecuteNoneQuery(sbStudentAwards.ToString)
                    Return ""
                    'db.CloseConnection()
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to create student awards for student " & strStudentName
                    'db.CloseConnection()
                    Exit Function
                End Try
            End If
            'Catch e As OleDbException
            '    m_ExceptionMessage = DALExceptions.BuildErrorMessage(e)
            '    msgEntry.strError = m_ExceptionMessage
            'Catch e As ArgumentNullException
            '    m_ExceptionMessage = e.Message
            '    msgEntry.strError = e.Message
            'Catch e As System.Exception
            '    m_ExceptionMessage = e.Message
            '    msgEntry.strError = e.Message
            '    'Throw
        Finally
        End Try
        'Return Add_faStudentAwards
    End Function
    'Public Function ReProcess_Add_faStudentAwards(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, _
    '                                              ByRef StudentAwardID As String, ByVal StuEnrollID As String, _
    '                                              ByVal msgEntry As FLMessageInfo, Optional ByVal strStudentName As String = "", _
    '                                              Optional ByVal SourceToTarget As String = "", _
    '                                              Optional ByVal strSSN As String = "", _
    '                                                 Optional ByVal strFAID As String = "", _
    '                                                 Optional ByVal strFund As String = "", _
    '                                                 Optional ByVal decGrossAmount As Decimal = 0, _
    '                                                 Optional ByVal decLoanFees As Decimal = 0, _
    '                                                 Optional ByVal strAwardYear As String = "", _
    '                                                 Optional ByVal dtDate1 As String = "1/1/1900", _
    '                                                 Optional ByVal dtDate2 As String = "1/1/1900") As String
    '    Dim sbStudentAwards As New System.Text.StringBuilder(1000)
    '    Dim sbUpdateStudentAwards As New System.Text.StringBuilder(1000)
    '    Dim sbCheckDuplicateAwards As New System.Text.StringBuilder(1000)
    '    Dim sbCheckDuplicateAwardsBySchool As New System.Text.StringBuilder(1000)
    '    Dim sbUpdateStudentAwards1 As New System.Text.StringBuilder(1000)
    '    Dim sbCheckDuplicateAwardsBySchoolDiffLoanPeriods As New System.Text.StringBuilder(1000)
    '    Dim sbCheckDuplicateAwardsBySchoolWithDiffGrossAmount As New System.Text.StringBuilder(1000)
    '    Dim intDuplicatesBySchoolDiffGrossAmount As Integer = 0
    '    Dim intDuplicatesBySchoolSameAwardYearDiffLoanPeriods As Integer = 0



    '    Dim AwardTypeID As String = " "
    '    Dim getAwardTypeMessage As String = ""
    '    Dim Disbursements As Integer = 0
    '    Dim LoanID As String = " "
    '    Dim intDuplicates As Integer = 0
    '    Dim intDuplicatesBySchool As Integer = 0
    '    Dim intDuplicatesBySchoolDiffLoanPeriods As Integer = 0

    '    Dim sbAcademicYear As New StringBuilder

    '    Dim strLoanBeginDate, strLoanEndDate As String
    '    Dim dr As OleDbDataReader
    '    m_ExceptionMessage = ""

    '    'db.OpenConnection()

    '    If String.IsNullOrEmpty(StuEnrollID) Or StuEnrollID = Guid.Empty.ToString Then
    '        Return "Unable to find enrollment information for the student" & strStudentName
    '        'db.CloseConnection()
    '        Exit Function
    '    End If

    '    If strFund = "02" Or strFund = "03" Or strFund = "04" Then
    '        strLoanBeginDate = "07/01/" + Mid(strAwardYear, 1, 4)
    '        strLoanEndDate = "06/30/" + Mid(strAwardYear, 6, 2)
    '    Else
    '        strLoanBeginDate = CDate(dtDate1)
    '        strLoanEndDate = CDate(dtDate2)
    '    End If


    '    'Look for matching records
    '    StudentAwardID = GetMatchingStudentAwards(db, StuEnrollID, strFAID, decGrossAmount, strLoanBeginDate, strLoanEndDate, strStudentName, strFund)
    '    If Not StudentAwardID = "" Then
    '        'Update existing record with this FAID
    '        With sbUpdateStudentAwards
    '            .Append("Update faStudentAwards set FA_Id=?,moduser=?,moddate=? where StudentAwardId=? ")
    '        End With
    '        db.ClearParameters()
    '        db.AddParameter("@FA_ID", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@moduser", "famelink", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@moddate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.AddParameter("@StudentAwardId", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        Try
    '            db.RunParamSQLExecuteNoneQuery(sbUpdateStudentAwards.ToString)
    '            Return ""
    '            'db.CloseConnection()
    '            Exit Function
    '        Catch ex As System.Exception
    '            Return "Unable to match/update FAID with student awards for student " & strStudentName
    '            'db.CloseConnection()
    '            Exit Function
    '        End Try
    '    Else
    '        StudentAwardID = Guid.NewGuid.ToString
    '    End If

    '    'Before Creating new awards check to see if the Award Start Date falls after the AwardsCuttOffDate
    '    'in web.config
    '    Try
    '        getAwardTypeMessage = GetAwardTypeID(db, AwardTypeID, msgEntry.strFund, msgEntry.strYYYY)
    '        If Not getAwardTypeMessage = "" Then
    '            Return "Unable to add student awards for student " & strStudentName & " as the provided award type was not found"
    '            'db.CloseConnection()
    '            Exit Function
    '        End If

    '        Dim strAwardYearInput As String
    '        Dim strFaidCount As Integer = msgEntry.strFAID.Length
    '        If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
    '            strAwardYearInput = Mid(msgEntry.strFAID, strFaidCount, 1)
    '            strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
    '            'strAwardYearInput = "200" & (strAwardYearInput - 1)
    '        Else
    '            strAwardYearInput = Mid(msgEntry.strFAID, 11, 1)
    '            strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
    '            'strAwardYearInput = "200" & (strAwardYearInput - 1)
    '        End If
    '        If strAwardYearInput = "" Then
    '            Return "Unable to match the award year from Fame ESP with Advantage Award Year for student " & strStudentName
    '            Exit Function
    '        End If
    '        With sbAcademicYear
    '            .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
    '        End With
    '        db.ClearParameters()
    '        db.AddParameter("@AcademicYearDescrip", strAwardYearInput, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Try
    '            dr = db.RunParamSQLDataReader(sbAcademicYear.ToString())
    '            If Not (dr.HasRows) Then
    '                Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
    '                Exit Function
    '            End If
    '            While dr.Read()
    '                If Not (dr("AcademicYearId") Is System.DBNull.Value) Or Not (dr("AcademicYearId").ToString = Guid.Empty.ToString) Then
    '                    strAwardYear = dr("AcademicYearId").ToString
    '                Else
    '                    Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
    '                    Exit Function
    '                End If
    '            End While
    '        Catch ex As System.Exception
    '            Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
    '            Exit Function
    '        Finally
    '            dr.Close()
    '        End Try

    '        'With sbAcademicYear
    '        '    .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
    '        'End With
    '        'db.ClearParameters()
    '        'db.AddParameter("@AcademicYearDescrip", Mid(msgEntry.strAwdyr, 1, 4), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        ''Try
    '        ''    strAwardYear = CType(db.RunParamSQLScalar(sbAcademicYear.ToString), Guid).ToString
    '        ''Catch ex As System.Exception
    '        ''    strAwardYear = System.DBNull.Value.ToString
    '        ''End Try
    '        'Try
    '        '    dr = db.RunParamSQLDataReader(sbAcademicYear.ToString())
    '        '    If Not (dr.HasRows) Then
    '        '        Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
    '        '        Exit Function
    '        '    End If
    '        '    While dr.Read()
    '        '        If Not (dr("AcademicYearId") Is System.DBNull.Value) Or Not (dr("AcademicYearId").ToString = Guid.Empty.ToString) Then
    '        '            strAwardYear = dr("AcademicYearId").ToString
    '        '        Else
    '        '            Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
    '        '            Exit Function
    '        '        End If
    '        '    End While
    '        'Catch ex As System.Exception
    '        '    Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
    '        '    Exit Function
    '        'Finally
    '        '    dr.Close()
    '        'End Try


    '        If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
    '            strLoanBeginDate = "07/01/" + Mid(msgEntry.strAwdyr, 1, 4)
    '            strLoanEndDate = "06/30/" + Mid(msgEntry.strAwdyr, 6, 2)
    '        Else
    '            strLoanBeginDate = msgEntry.dateDate1
    '            strLoanEndDate = msgEntry.dateDate2
    '        End If

    '        Dim intCutOffDates As Integer
    '        Dim dtCuttOfDate As Date = CDate(SingletonAppSettings.AppSettings("AwardsCutOffDate").ToString)
    '        Dim dtAwardStartDate As Date = CDate(strLoanBeginDate)
    '        intCutOffDates = DateDiff(DateInterval.Day, dtCuttOfDate, dtAwardStartDate)
    '        If intCutOffDates < 0 Then
    '            Return "The award start date " & dtAwardStartDate.ToString & " is earlier than the award cut off date (" & SingletonAppSettings.AppSettings("AwardsCutOffDate").ToString & "), so student award will not be created for " & strStudentName
    '            ' db.CloseConnection()
    '            Exit Function
    '        End If

    '        'Checks to see if the school has put in an award for the same fund and award year
    '        'but for different gross amount
    '        'example: an award may be created by school for $4500 for Direct Loan 05-06
    '        'but from Fame ESP we may get a award data for $3500 and $1000 for same year
    '        'and this case we need to send the data($3500 and $1000) from Fame ESP to exception
    '        'while checking ignore Gross Amount and look for records with null FAID and moduser
    '        'not equal to famelink which identifies that record was put in by school
    '        'Mantis # 12077.
    '        'If Not Trim(SourceToTarget) = "exceptiontotarget" Then
    '        '    With sbCheckDuplicateAwardsBySchool
    '        '        .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
    '        '        .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
    '        '        If strAwardYear.Length >= 30 Then
    '        '            .Append(" and AcademicYearId = ? ")
    '        '        End If
    '        '        '.Append(" and FA_ID is NULL ")
    '        '        If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
    '        '            .Append(" and AwardStartDate=? ")
    '        '            .Append(" and AwardEndDate=? ")
    '        '        End If
    '        '        .Append(" and ModUser <> 'famelink' ")
    '        '    End With
    '        '    db.ClearParameters()
    '        '    db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        '    db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        '    db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        '    db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        '    db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

    '        '    Try
    '        '        intDuplicatesBySchool = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchool.ToString)
    '        '        If intDuplicatesBySchool >= 1 Then
    '        '            Return "Unable to create student award for student " & strStudentName & " as the school has already created an award with a different gross amount for same fund source and award year"
    '        '            Exit Function
    '        '        Else
    '        '            intDuplicatesBySchool = 0
    '        '        End If
    '        '    Catch ex As System.Exception
    '        '        intDuplicatesBySchool = 0
    '        '    End Try

    '        '    'Use Case: School would have created a student award with same amount but for a different loan period
    '        '    'Example:Student Thomas Piehko Award Created for $3500.00
    '        '    'School Created an award for 11/14/ 2007 but ESP created it for 11/15/2007

    '        '    With sbCheckDuplicateAwardsBySchoolDiffLoanPeriods
    '        '        .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
    '        '        .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
    '        '        If strAwardYear.Length >= 30 Then
    '        '            .Append(" and AcademicYearId = ? ")
    '        '        End If
    '        '        ' .Append(" and FA_ID is NULL ")
    '        '        .Append(" and ModUser <> 'famelink' ")
    '        '    End With
    '        '    db.ClearParameters()
    '        '    db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        '    db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        '    db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

    '        '    Try
    '        '        intDuplicatesBySchoolDiffLoanPeriods = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchoolDiffLoanPeriods.ToString)
    '        '        If intDuplicatesBySchoolDiffLoanPeriods >= 1 Then
    '        '            Return "Unable to create student award for student " & strStudentName & " as the school has already created an award with a same gross amount for same fund source and award year, but for a different award period"
    '        '            Exit Function
    '        '        Else
    '        '            intDuplicatesBySchoolDiffLoanPeriods = 0
    '        '        End If
    '        '    Catch ex As System.Exception
    '        '        intDuplicatesBySchoolDiffLoanPeriods = 0
    '        '    End Try
    '        'End If

    '        'Checks to see if the school has put in an award for the same fund and award year
    '        'but for different gross amount
    '        'example: an award may be created by school for $4500 for Direct Loan 05-06
    '        'but from Fame ESP we may get a award data for $3500 and $1000 for same year
    '        'and this case we need to send the data($3500 and $1000) from Fame ESP to exception
    '        'while checking ignore Gross Amount and look for records with null FAID and moduser
    '        'not equal to famelink which identifies that record was put in by school
    '        'Mantis # 12077.
    '        If Not Trim(SourceToTarget) = "exceptiontotarget" Then
    '            'Modified by balaji on 09/27/2007 to take gross amount into consideration
    '            'Check to see if award exists with same gross amount and same award year and period
    '            With sbCheckDuplicateAwardsBySchool
    '                .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
    '                .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
    '                If strAwardYear.Length >= 30 Then
    '                    .Append(" and AcademicYearId = ? ")
    '                End If
    '                .Append(" and GrossAmount=? ")
    '                If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
    '                    .Append(" and AwardStartDate=? ")
    '                    .Append(" and AwardEndDate=? ")
    '                End If
    '                .Append(" and (ModUser <> 'famelink' or ModUser is NULL) ")
    '            End With
    '            db.ClearParameters()
    '            db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '            If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
    '                db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '                db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            End If

    '            Try
    '                intDuplicatesBySchool = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchool.ToString)
    '                If intDuplicatesBySchool >= 1 Then
    '                    'if award exists with same gross amount and same award year and period
    '                    'ignore this record as Fa_id has already been updated and thats all this record can do
    '                    'and process the next record
    '                    Return ""
    '                    Exit Function
    '                Else
    '                    'if award does not exists then check for different gross amount and same award year and period
    '                    'If so then alert message and go to exception else ignore and go to next record
    '                    'and process the next record
    '                    intDuplicatesBySchool = 0
    '                    With sbCheckDuplicateAwardsBySchoolWithDiffGrossAmount
    '                        .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
    '                        .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
    '                        If strAwardYear.Length >= 30 Then
    '                            .Append(" and AcademicYearId = ? ")
    '                        End If
    '                        .Append(" and GrossAmount <> ? ")
    '                        If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
    '                            .Append(" and AwardStartDate=? ")
    '                            .Append(" and AwardEndDate=? ")
    '                        End If
    '                        .Append(" and (ModUser <> 'famelink' or ModUser is NULL) ")
    '                    End With
    '                    db.ClearParameters()
    '                    db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '                    db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '                    db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '                    db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '                    If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
    '                        db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '                        db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '                    End If
    '                    Try
    '                        intDuplicatesBySchoolDiffGrossAmount = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchoolWithDiffGrossAmount.ToString)
    '                        If intDuplicatesBySchoolDiffGrossAmount >= 1 Then
    '                            Dim sbGetGrossAmount As New StringBuilder
    '                            Dim drDiffGrossAmount As OleDbDataReader
    '                            Dim strAdvantageGrossAmount As String = ""
    '                            With sbGetGrossAmount
    '                                .Append(" select Distinct GrossAmount from faStudentAwards where StuEnrollId=? and ")
    '                                .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
    '                                If strAwardYear.Length >= 30 Then
    '                                    .Append(" and AcademicYearId = ? ")
    '                                End If
    '                                .Append(" and GrossAmount <> ? ")
    '                                If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
    '                                    .Append(" and AwardStartDate=? ")
    '                                    .Append(" and AwardEndDate=? ")
    '                                End If
    '                                .Append(" and (ModUser <> 'famelink' or ModUser is NULL) ")
    '                            End With
    '                            db.ClearParameters()
    '                            db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '                            db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '                            db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '                            db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '                            If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
    '                                db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '                                db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '                            End If
    '                            Try
    '                                drDiffGrossAmount = db.RunParamSQLDataReader(sbGetGrossAmount.ToString())
    '                                If Not (drDiffGrossAmount.HasRows) Then
    '                                    strAdvantageGrossAmount = ""
    '                                End If
    '                                While drDiffGrossAmount.Read()
    '                                    strAdvantageGrossAmount = CType(drDiffGrossAmount("GrossAmount"), Decimal).ToString
    '                                End While
    '                            Catch ex As System.Exception
    '                                strAdvantageGrossAmount = ""
    '                            Finally
    '                                drDiffGrossAmount.Close()
    '                            End Try
    '                            Dim sReturnMessage As String = "Unable to create student award for student " & strStudentName & " as the school has already created an award with a different gross amount." & vbLf
    '                            If Not strAdvantageGrossAmount = "" Then
    '                                sReturnMessage &= vbLf
    '                                sReturnMessage &= " Gross Amount in Advantage: " & CType(strAdvantageGrossAmount, Decimal).ToString("#0.00")
    '                            End If
    '                            Return sReturnMessage
    '                            Exit Function
    '                            'Return "Unable to create student award for student " & strStudentName & " as the school has already created an award with a different gross amount for same fund source and award year"
    '                            'Dim sReturnMessage As String = "Unable to create student award for student " & strStudentName & " as the school has already created an award with a different gross amount for same fund source and award year."
    '                            'sReturnMessage &= " Gross Amount in Fame ESP: " & msgEntry.decAmount1
    '                            'Return sReturnMessage
    '                            'Exit Function
    '                        Else
    '                            intDuplicatesBySchoolDiffGrossAmount = 0
    '                        End If
    '                    Catch ex5 As System.Exception
    '                        intDuplicatesBySchoolDiffGrossAmount = 0
    '                    End Try
    '                End If
    '            Catch ex As System.Exception
    '                intDuplicatesBySchool = 0
    '            End Try

    '            Dim sbCheckDuplicateAwardsBySchoolDiffAwardYear As New StringBuilder
    '            With sbCheckDuplicateAwardsBySchoolDiffAwardYear
    '                .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
    '                .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
    '                If strAwardYear.Length >= 30 Then
    '                    .Append(" and AcademicYearId = ? ")
    '                End If
    '                .Append(" and GrossAmount=? ")
    '                If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
    '                    .Append(" and (AwardStartDate <> ? or AwardEndDate <> ?) ")
    '                End If
    '                .Append(" and (ModUser <> 'famelink' or ModUser is NULL) ")
    '            End With
    '            db.ClearParameters()
    '            db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '            If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
    '                db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '                db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            End If
    '            Try
    '                intDuplicatesBySchoolSameAwardYearDiffLoanPeriods = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchoolDiffAwardYear.ToString)
    '                If intDuplicatesBySchoolSameAwardYearDiffLoanPeriods >= 1 Then
    '                    Dim sbGetGrossAmount As New StringBuilder
    '                    Dim strAwardStartDate As String = ""
    '                    Dim strAwardEndDate As String = ""
    '                    Dim drDiffLoanPeriod As OleDbDataReader
    '                    If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
    '                        With sbGetGrossAmount
    '                            .Append(" select Distinct AwardStartDate,AwardEndDate from faStudentAwards where StuEnrollId=? and ")
    '                            .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
    '                            If strAwardYear.Length >= 30 Then
    '                                .Append(" and AcademicYearId = ? ")
    '                            End If
    '                            .Append(" and GrossAmount=? ")
    '                            If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
    '                                .Append(" and (AwardStartDate <> ? or AwardEndDate <> ?) ")
    '                            End If
    '                            .Append(" and (ModUser <> 'famelink' or ModUser is NULL) ")
    '                        End With
    '                        db.ClearParameters()
    '                        db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '                        db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '                        db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '                        db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '                        If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
    '                            db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '                            db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '                        End If
    '                        Try
    '                            drDiffLoanPeriod = db.RunParamSQLDataReader(sbGetGrossAmount.ToString())
    '                            If Not (drDiffLoanPeriod.HasRows) Then
    '                                strAwardStartDate = ""
    '                                strAwardEndDate = ""
    '                            End If
    '                            While drDiffLoanPeriod.Read()
    '                                strAwardStartDate = CType(drDiffLoanPeriod("AwardStartDate"), Date).ToString
    '                                strAwardEndDate = CType(drDiffLoanPeriod("AwardEndDate"), Date).ToString
    '                            End While
    '                        Catch ex As System.Exception
    '                            strAwardStartDate = ""
    '                            strAwardEndDate = ""
    '                        Finally
    '                            drDiffLoanPeriod.Close()
    '                        End Try
    '                    End If
    '                    Dim sReturnMessage As String
    '                    sReturnMessage = "Unable to create student award for student " & strStudentName & " as the school has already created an award with a different loan period." & vbLf
    '                    sReturnMessage &= vbLf
    '                    sReturnMessage &= " Award Period from FAME ESP:" & " (" & strLoanBeginDate & " - " & strLoanEndDate & ")." & vbLf
    '                    If Not strAwardStartDate = "" And Not strAwardEndDate = "" Then
    '                        sReturnMessage &= vbLf
    '                        sReturnMessage &= " Award Period from advantage:" & " (" & CDate(strAwardStartDate) & " - " & CDate(strAwardEndDate) & ")."
    '                    End If
    '                    Return sReturnMessage
    '                    Exit Function
    '                    'Dim sReturnMessage As String = "Unable to create student award for student " & strStudentName & " as the school has already created an award with a same gross amount for same fund source and same award year but different loan period."
    '                    'sReturnMessage &= " Award Period from Fame ESP:" & " (" & strLoanBeginDate & " - " & strLoanEndDate & ")"
    '                    'Return sReturnMessage
    '                    'Exit Function
    '                Else
    '                    intDuplicatesBySchoolSameAwardYearDiffLoanPeriods = 0
    '                End If
    '            Catch ex9 As System.Exception
    '                intDuplicatesBySchoolSameAwardYearDiffLoanPeriods = 0
    '            End Try
    '        End If

    '        'Check for duplicates before executing
    '        With sbCheckDuplicateAwards
    '            .Append(" Select Count(*) from faStudentAwards where ")
    '            .Append(" StuEnrollID=? and AwardTypeID=? and fa_id=? and GrossAmount=? ")
    '            If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
    '                .Append(" and AwardStartDate=? and AwardEndDate=? ")
    '            End If
    '        End With
    '        'Add params
    '        db.ClearParameters()
    '        db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@AwardTypeID", AwardTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@fa_id", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '        If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
    '            db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        End If

    '        Try
    '            intDuplicates = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
    '            If intDuplicates >= 1 Then
    '                With sbUpdateStudentAwards1
    '                    .Append("Update faStudentAwards set LoanFees=? where  ")
    '                    .Append(" StuEnrollID=? and AwardTypeID=? and fa_id=? and GrossAmount=? ")
    '                    If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
    '                        .Append(" and AwardStartDate=? and AwardEndDate=? ")
    '                    End If
    '                End With
    '                db.ClearParameters()
    '                If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
    '                    db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '                Else
    '                    If msgEntry.decAmount2.ToString.Length >= 1 And Mid(msgEntry.decAmount2.ToString, 1) <> "0" Then
    '                        db.AddParameter("@LoanFees", msgEntry.decAmount1 - msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '                        'db.AddParameter("@LoanFees", msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '                    Else
    '                        db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '                    End If
    '                End If
    '                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '                db.AddParameter("@AwardTypeID", AwardTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '                db.AddParameter("@fa_id", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '                db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '                If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
    '                    db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '                    db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '                End If

    '                Try
    '                    db.RunParamSQLExecuteNoneQuery(sbUpdateStudentAwards1.ToString)
    '                    Return ""
    '                    Exit Function
    '                Catch ex As System.Exception
    '                    Return "Unable to update Loan Fees for the existing student award data for student " & strStudentName
    '                    Exit Function
    '                End Try
    '            End If
    '        Catch ex As System.Exception
    '            intDuplicates = 0
    '        End Try


    '        'If there are no duplicates create a student award
    '        If intDuplicates = 0 Then
    '            With sbStudentAwards
    '                .Append("INSERT INTO faStudentAwards(StudentAwardID,StuEnrollID,AwardTypeID,AcademicYearId,fa_id,GrossAmount,AwardStartDate,AwardEndDate,LoanFees,moduser,moddate,Disbursements,LoanID) ")
    '                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ")
    '            End With

    '            'Add params
    '            db.ClearParameters()
    '            db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@AwardTypeID", AwardTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@fa_id", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '            db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
    '                db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '            Else
    '                If msgEntry.decAmount2.ToString.Length >= 1 And Mid(msgEntry.decAmount2.ToString, 1) <> "0" Then
    '                    db.AddParameter("@LoanFees", msgEntry.decAmount1 - msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '                    'db.AddParameter("@LoanFees", msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '                Else
    '                    db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '                End If
    '            End If
    '            db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            db.AddParameter("@Disbursements", Disbursements, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
    '            db.AddParameter("@LoanID", LoanID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '            '   execute the query
    '            Try
    '                db.RunParamSQLExecuteNoneQuery(sbStudentAwards.ToString)
    '                Return ""
    '                'db.CloseConnection()
    '                Exit Function
    '            Catch ex As System.Exception
    '                Return "Unable to create student awards for student " & strStudentName
    '                'db.CloseConnection()
    '                Exit Function
    '            End Try
    '        End If
    '        'Catch e As OleDbException
    '        '    m_ExceptionMessage = DALExceptions.BuildErrorMessage(e)
    '        '    msgEntry.strError = m_ExceptionMessage
    '        'Catch e As ArgumentNullException
    '        '    m_ExceptionMessage = e.Message
    '        '    msgEntry.strError = e.Message
    '        'Catch e As System.Exception
    '        '    m_ExceptionMessage = e.Message
    '        '    msgEntry.strError = e.Message
    '        '    'Throw
    '    Finally
    '    End Try
    '    'Return Add_faStudentAwards
    'End Function
    'Public Function Delete_faAwardSchedule(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal StudentAwardID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "") As String
    '    Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
    '    Dim sbCheckDisbursements As New StringBuilder
    '    Dim intCheckDisb As Integer = 0
    '    m_ExceptionMessage = ""
    '    '    Delete_faAwardSchedule = False

    '    If String.IsNullOrEmpty(StudentAwardID) Then
    '        Return "Unable to delete award schedule as student award is missing for student " & StudentName
    '        Exit Function
    '    End If

    '    Dim ExpectedDate As Date = msgEntry.dateDate1
    '    Dim OrigAmount As Double = msgEntry.decAmount1

    '    With sbCheckDisbursements
    '        .Append(" Select Count(*) from faStudentAwardSchedule ")
    '        .Append(" WHERE StudentAwardID = ? AND ExpectedDate=? AND Amount=? ")
    '    End With
    '    db.ClearParameters()
    '    db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@ExpectedDate", ExpectedDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '    db.AddParameter("@Amount", OrigAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '    Try
    '        intCheckDisb = db.RunParamSQLScalar(sbCheckDisbursements.ToString)
    '        If intCheckDisb >= 1 Then
    '            intCheckDisb = 1
    '        Else
    '            intCheckDisb = 0
    '            Return "Unable to delete award schedule as the scheduled disbursement is not available for the specified date and amount for student " & StudentName
    '            Exit Function
    '        End If
    '    Catch ex As Exception
    '        intCheckDisb = 0
    '        Return "Unable to delete award schedule as the scheduled disbursement is not available for the specified date and amount for student " & StudentName
    '        Exit Function
    '    End Try

    '    Try

    '        If intCheckDisb = 1 Then
    '            '   build the query
    '            With sbAwardSchedule
    '                .Append("DELETE FROM faStudentAwardSchedule ")
    '                .Append(" WHERE ExpectedDate = ? ")
    '                .Append(" AND Amount = ? ;")
    '                .Append(" SELECT count(*) FROM faStudentAwardSchedule WHERE StudentAwardId = ? ")
    '            End With
    '            '   add parameters values to the query
    '            '   ExpectedDate
    '            db.ClearParameters()
    '            db.AddParameter("@ExpectedDate", ExpectedDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '            '   Amount
    '            db.AddParameter("@Amount", OrigAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '            '   StudentAwardID
    '            db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


    '            Try
    '                db.RunParamFLSQLExecuteNoneQuery(sbAwardSchedule.ToString)
    '                Return ""
    '                Exit Function
    '            Catch ex As Exception
    '                Return "Unable to delete student award schedule for student " & StudentName
    '                Exit Function
    '            End Try
    '        End If
    '    Finally
    '    End Try
    '    'Return Delete_faAwardSchedule
    'End Function
    Public Function Update_faAwardSchedule(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal StudentAwardID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "") As String
        Dim errMessage As String = String.Empty
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sMessage As String = ""
        Dim sMessage1 As String = ""
        Dim sbCheckDisbursements As New StringBuilder
        Dim sbCheckDisbursementsNoDate As New StringBuilder
        Dim sbUpdateDisbursementsNoDate As New StringBuilder
        Dim sbUpdateDisbursementsNoDateAndAmount As New StringBuilder
        Dim sbUpdateDisbursementsDateAndAmount As New StringBuilder
        Dim intCheckDisb As Integer = 0
        Dim intCheckDisbNoDateAmt As Integer = 0
        Dim strAwardScheduleId As String
        m_ExceptionMessage = ""

        If String.IsNullOrEmpty(StudentAwardID) Then
            Return "Unable to update award schedule as student award is missing for student" & StudentName
            Exit Function
        End If

        'Look for matching StudentAward,Amount and Date
        With sbCheckDisbursements
            .Append(" Select Count(*) from faStudentAwardSchedule ")
            .Append(" WHERE StudentAwardID = ? AND ExpectedDate=? AND Amount=? ")
            '.Append(" and (Reference is NULL or Reference = '') ")
        End With
        db.ClearParameters()
        db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ExpectedDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
        Try
            intCheckDisb = db.RunParamSQLScalar(sbCheckDisbursements.ToString)
            If intCheckDisb >= 1 Then
                intCheckDisb = 1
                Dim sbCheckDisbursementsNoDate1 As New StringBuilder
                With sbCheckDisbursementsNoDate1
                    .Append(" Select Top 1 AwardScheduleId from faStudentAwardSchedule ")
                    .Append(" WHERE StudentAwardID = ? AND ExpectedDate=? AND Amount=? ")
                    '.Append(" and (Reference is NULL or Reference = '') ")
                    .Append(" order by ExpectedDate Desc ")
                End With
                db.ClearParameters()
                db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ExpectedDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
                db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                Try
                    strAwardScheduleId = CType(db.RunParamSQLScalar(sbCheckDisbursementsNoDate1.ToString), Guid).ToString
                Catch ex5 As System.Exception
                    strAwardScheduleId = ""
                End Try

                With sbUpdateDisbursementsDateAndAmount
                    .Append(" Update faStudentAwardSchedule set Amount=?, ModUser=?, ModDate=? ")
                    .Append(" WHERE AwardScheduleId=? ")
                End With
                db.ClearParameters()
                db.AddParameter("@Amount", msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                db.AddParameter("@ModUser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModDate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
                db.AddParameter("@AwardScheduleId", strAwardScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


                Try
                    db.RunParamFLSQLExecuteNoneQuery(sbUpdateDisbursementsDateAndAmount.ToString)
                    Return ""
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to update disbursement as payment has already been posted for student " & StudentName
                    Exit Function
                End Try
            Else
                intCheckDisb = 0
                'Look for matching student award and amount
                With sbUpdateDisbursementsNoDateAndAmount
                    .Append(" Select Count(*) from faStudentAwardSchedule ")
                    .Append(" WHERE StudentAwardID = ? AND Amount=? ")
                    '.Append(" and (Reference is NULL or Reference = '') ")
                    .Append(" order by ExpectedDate Desc ")
                End With
                db.ClearParameters()
                db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                Try
                    intCheckDisbNoDateAmt = db.RunParamSQLScalar(sbUpdateDisbursementsNoDateAndAmount.ToString)
                    If intCheckDisbNoDateAmt >= 1 Then
                        intCheckDisbNoDateAmt = 1

                        'Check if there is any disbursement with the specified amount for student award
                        With sbCheckDisbursementsNoDate
                            .Append(" Select Top 1 AwardScheduleId from faStudentAwardSchedule ")
                            .Append(" WHERE StudentAwardID = ? AND Amount=? ")
                            '.Append(" and (Reference is NULL or Reference = '') ")
                            .Append(" and DateDiff(day,'" & msgEntry.dateDate1 & "',ExpectedDate) = ")
                            'Based on the disb date from chng file look at the latest date in advantage disbursement
                            .Append(" (select Min(Abs(DateDiff(day,'" & msgEntry.dateDate1 & "',ExpectedDate))) from faStudentAwardSchedule where ")
                            .Append(" StudentAwardID = ? AND Amount=?) ")
                        End With
                        db.ClearParameters()
                        db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                        db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                        Try
                            strAwardScheduleId = CType(db.RunParamSQLScalar(sbCheckDisbursementsNoDate.ToString), Guid).ToString

                            With sbUpdateDisbursementsNoDate
                                .Append(" Update faStudentAwardSchedule set Amount=?, ModUser=?, ModDate=? ")
                                .Append(" WHERE AwardScheduleId=? ")
                            End With
                            db.ClearParameters()
                            db.AddParameter("@Amount", msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                            db.AddParameter("@ModUser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                            db.AddParameter("@ModDate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
                            db.AddParameter("@AwardScheduleId", strAwardScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                            Try
                                db.RunParamFLSQLExecuteNoneQuery(sbUpdateDisbursementsNoDate.ToString)
                                Return ""
                                Exit Function
                            Catch ex As System.Exception
                                Return "Unable to update disbursement as either amount does not match or payment has already been posted for student " & StudentName
                                Exit Function
                            End Try
                        Catch ex As System.Exception
                            If Trim(msgEntry.decAmount1) = "" Then
                                sMessage1 = " as Original Amount is empty"
                            End If
                            Return "Unable to update disbursement as either amount does not match or payment has already been posted for student " & StudentName
                            Exit Function
                        End Try
                    Else
                        intCheckDisbNoDateAmt = 0
                        'Return "Unable to update disbursement as either scheduled date/amount does not match or payment has already been posted for student " & StudentName
                        'Exit Function
                        'code uncommented by balaji on 10/18/2007 
                        'for a change record date or amount may not match

                        'Check if there is any disbursement with just student award and no date or amount
                        With sbCheckDisbursementsNoDate
                            .Append(" Select Top 1 AwardScheduleId from faStudentAwardSchedule ")
                            .Append(" WHERE StudentAwardID = ? ")
                            .Append(" and Abs(DateDiff(day,'" & msgEntry.dateDate1 & "',ExpectedDate)) = ")
                            'Based on the disb date from chng file look at the latest date in advantage disbursement
                            .Append(" (select Min(Abs(DateDiff(day,'" & msgEntry.dateDate1 & "',ExpectedDate))) from faStudentAwardSchedule where ")
                            .Append(" StudentAwardID = ?) ")

                            'Commented by balaji on 10/23/2007 as the disb needs to be applied to the latest disbursement date
                            '.Append(" and (Reference is NULL or Reference = '') ")
                            '.Append(" order by ExpectedDate Desc ")
                        End With
                        db.ClearParameters()
                        db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Try
                            strAwardScheduleId = CType(db.RunParamSQLScalar(sbCheckDisbursementsNoDate.ToString), Guid).ToString

                            'If empty guid is returned then it means that there are no scheduled disbursements for this award.
                            'In that case we need to send that message back to the user. DE6335.
                            If strAwardScheduleId.StartsWith("00000000") Then
                                Return "Unable to update student award as there are no scheduled disbursements for student " & StudentName
                            Else
                                With sbUpdateDisbursementsNoDate
                                    .Append(" Update faStudentAwardSchedule set Amount=?, ModUser=?, ModDate=? ")
                                    .Append(" WHERE AwardScheduleId=? ")
                                End With
                                db.ClearParameters()
                                db.AddParameter("@Amount", msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                                db.AddParameter("@ModUser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                                db.AddParameter("@ModDate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
                                db.AddParameter("@AwardScheduleId", strAwardScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                                Try
                                    db.RunParamFLSQLExecuteNoneQuery(sbUpdateDisbursementsNoDate.ToString)
                                    Return ""
                                    Exit Function
                                Catch ex As System.Exception
                                    Return "Unable to update disbursement as payment has already been posted for student " & StudentName
                                    Exit Function
                                End Try

                            End If

                        Catch ex As System.Exception
                            If Trim(msgEntry.decAmount1) = "" Then
                                sMessage1 = " as Original Amount is empty"
                            End If
                            Return "Unable to update award schedule as the scheduled disbursement is not available for student " & StudentName
                            Exit Function
                        End Try
                    End If
                Catch ex As System.Exception
                    Return "Unable to update award schedule as the scheduled disbursement is not available for student " & StudentName
                    Exit Function
                End Try
            End If
        Catch ex As System.Exception
            Return "Unable to update award schedule as the scheduled disbursement is not available for student " & StudentName
            Exit Function
        End Try
        '    End If
        'Catch ex As Exception
        '    intCheckDisb = 0
        '    'Check if there is any disbursement with the specified amount for student award
        '    With sbCheckDisbursementsNoDate
        '        .Append(" Select Top 1 AwardScheduleId from faStudentAwardSchedule ")
        '        .Append(" WHERE StudentAwardID = ? AND Amount=? ")
        '    End With
        '    db.ClearParameters()
        '    db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '    db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
        '    Try
        '        strAwardScheduleId = CType(db.RunParamSQLScalar(sbCheckDisbursementsNoDate.ToString), Guid).ToString
        '        With sbUpdateDisbursementsNoDate
        '            .Append(" Update faStudentAwardSchedule set Amount=?, ModUser=?, ModDate=? ")
        '            .Append(" WHERE AwardScheduleId=? ")
        '        End With
        '        db.ClearParameters()
        '        db.AddParameter("@Amount", msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
        '        db.AddParameter("@ModUser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '        db.AddParameter("@ModDate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        '        db.AddParameter("@AwardScheduleId", strAwardScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '        Try
        '            db.RunParamFLSQLExecuteNoneQuery(sbUpdateDisbursementsNoDate.ToString)
        '            Return ""
        '            Exit Function
        '        Catch ex1 As System.Exception
        '            Return "Unable to update student award schedules to amount " & msgEntry.decAmount2 & " for student " & StudentName
        '            Exit Function
        '        End Try
        '    Catch ex2 As System.Exception
        '        If Trim(msgEntry.decAmount1) = "" Then
        '            sMessage1 = " as Original Amount is empty"
        '        End If
        '        Return "Unable to update award schedule as the scheduled disbursement is not available for student " & StudentName & sMessage1
        '        Exit Function
        '    End Try
        'End Try


        'Try
        '    If intCheckDisb = 1 Then
        '        With sbAwardSchedule
        '            .Append(" Update faStudentAwardSchedule set ExpectedDate=?, Amount=?, ModUser=?, ModDate=? ")
        '            .Append(" WHERE StudentAwardID = ? AND ExpectedDate=? AND Amount=? ")
        '        End With
        '        db.ClearParameters()
        '        db.AddParameter("@ExpectedDate", msgEntry.dateDate2, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        '        db.AddParameter("@Amount", msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
        '        db.AddParameter("@ModUser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '        db.AddParameter("@ModDate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        '        db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '        db.AddParameter("@ExpectedDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        '        db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)

        '        If Trim(msgEntry.dateDate1) = "" Or Year(msgEntry.dateDate1) < 1990 Then
        '            sMessage1 = " as Original Date "
        '        End If
        '        If Trim(msgEntry.decAmount1) = "" Then
        '            If Not sMessage1 = "" Then
        '                sMessage1 &= " and "
        '            Else
        '                sMessage1 = " as "
        '            End If
        '            sMessage1 &= " Original Amount"
        '        End If
        '        If Not sMessage1 = "" Then
        '            sMessage1 &= " is empty "
        '        End If
        '        If Not sMessage1 = "" Then
        '            Return "Unable to update student award schedules for student " & StudentName & sMessage1
        '            Exit Function
        '        End If

        '        Try
        '            db.RunParamFLSQLExecuteNoneQuery(sbAwardSchedule.ToString)
        '            Return ""
        '            Exit Function
        '        Catch ex As System.Exception
        '            Return "Unable to update student award schedules for student " & StudentName
        '            Exit Function
        '        End Try
        '    End If

        'Catch e As ArgumentNullException
        '    m_ExceptionMessage = e.Message
        '    msgEntry.strError = m_ExceptionMessage
        'Catch e As System.Exception
        '    m_ExceptionMessage = e.Message
        '    Throw
        'Finally
        'End Try
    End Function
    Public Function BuildExceptionReport(ByVal SSN As String, ByVal FAID As String, ByVal Fund As String, ByVal msgEntry As FLMessageInfo, _
                                         ByVal FileName As String, ByVal ImportedDate As String, ByVal Message As String, _
                                         Optional ByVal ExceptionGUID As String = "", Optional ByVal AwardCutOffDate As String = "", _
                                         Optional ByVal DisbDate As String = "", Optional ByVal intRecordPosition As Integer = 0) As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbInsertDuplicates As New StringBuilder
        Dim intDuplicates As Integer
        Dim AwardScheduleID As String
        Dim db As New DataAccess

        Try
            With sbAwardSchedule
                .Append("INSERT INTO syFameESPExceptionReport(ExceptionReportId,SSN,FAID,Fund,GrossAmount,FileName,Message,moduser,moddate,ExceptionGUID,IsInitialImportSuccessful,isEligibleForReprocess,DisbursementDate,awardyear,loanfees,loanstartdate,loanenddate,RecordPosition) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@ExceptionReportId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FAID", FAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Fund", Fund, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If (msgEntry.strChgCode.ToLower = "c" Or msgEntry.strChgCode.ToLower = "n") Then
                db.AddParameter("@GrossAmount", msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Else
                db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            End If
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Message", Message, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moduser", "famelink", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@IsInitialImportSuccessful", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@isEligibleForReprocess", 0, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            If Not DisbDate = "" Then
                db.AddParameter("@DisbursementDate", DisbDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DisbursementDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@awardyear", msgEntry.strAwdyr, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Loanfees", msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Dim strLoanBeginDate, strLoanEndDate As Date

            Try
                If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                    strLoanBeginDate = "07/01/" + Mid(msgEntry.strAwdyr, 1, 4)
                    strLoanEndDate = "06/30/" + Mid(msgEntry.strAwdyr, 6, 2)
                Else
                    strLoanBeginDate = msgEntry.dateDate1
                    strLoanEndDate = msgEntry.dateDate2
                End If
            Catch ex As System.Exception
                Dim strAwardYearInput As String
                Dim strFaidCount As Integer = msgEntry.strFAID.Length
                Dim strAwardYr As String = ""
                If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                    strAwardYearInput = Mid(msgEntry.strFAID, strFaidCount, 1)
                    strAwardYr = GetAwardYearByFameESPYearReturnFullYear(strAwardYearInput)
                    strLoanBeginDate = "07/01/" + Mid(strAwardYr, 1, 4)
                    strLoanEndDate = "06/30/" + Mid(strAwardYr, 6, 4)
                End If
            End Try

            db.AddParameter("@loanstartdate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@loanenddate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'If AwardCutOffDate = "yes" Then
            '    db.AddParameter("@isPriorToCutOffDate", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            'Else
            '    db.AddParameter("@isPriorToCutOffDate", 0, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            'End If

            db.AddParameter("@RecPos", intRecordPosition, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            Try
                db.RunParamSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                Return ""
                Exit Function
            Catch ex As System.Exception
            End Try
        Finally
        End Try
        'End If
        '        Return Add_faAwardSchedule
    End Function
    Public Function BuildExceptionReport_Reprocess(ByVal SSN As String, ByVal FAID As String, ByVal Fund As String, _
                                                   ByVal grossAmount As Decimal, ByVal FileName As String, _
                                                   ByVal ImportedDate As String, ByVal Message As String, _
                                                   Optional ByVal ExceptionGUID As String = "", _
                                                   Optional ByVal AwardCutOffDate As String = "", Optional ByVal DisbDate As String = "", _
                                                   Optional ByVal decLoanFees As Decimal = 0, _
                                                   Optional ByVal strAwardYear As String = "", _
                                                   Optional ByVal LoanBeginDate As Date = #1/1/1900#, _
                                                   Optional ByVal LoanEndDate As Date = #1/1/1900#) As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbInsertDuplicates As New StringBuilder
        Dim intDuplicates As Integer = 0
        Dim AwardScheduleID As String
        Dim db As New DataAccess

        Try
            With sbAwardSchedule
                .Append(" Select Count(*) as RowCounter from syFameESPExceptionReport ")
                .Append(" Where  SSN=? and FAID=? and Fund=? and FileName=? and GrossAmount=? and Message=? ")
            End With
            db.ClearParameters()
            db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FAID", FAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Fund", Fund, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@GrossAmount", grossAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@Message", Message, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                intDuplicates = db.RunParamSQLScalar(sbAwardSchedule.ToString)
                If intDuplicates >= 1 Then
                    Return ""
                    Exit Function
                End If
            Catch ex As System.Exception
                Return ""
                Exit Function
            Finally
                db.ClearParameters()
                sbAwardSchedule.Remove(0, sbAwardSchedule.Length)
            End Try

            With sbAwardSchedule
                .Append("INSERT INTO syFameESPExceptionReport(ExceptionReportId,SSN,FAID,Fund,GrossAmount,FileName,Message,moduser,moddate,ExceptionGUID,IsInitialImportSuccessful,isEligibleForReprocess,DisbursementDate,LoanFees,AwardYear,LoanStartDate,LoanEndDate) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@ExceptionReportId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FAID", FAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Fund", Fund, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@GrossAmount", grossAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Message", Message, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moduser", "famelink", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@IsInitialImportSuccessful", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@isEligibleForReprocess", 0, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            If Not DisbDate = "" Then
                db.AddParameter("@DisbursementDate", DisbDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DisbursementDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@Loanfees", decLoanFees, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@awardyear", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not Year(LoanBeginDate) = 1900 Then
                db.AddParameter("@loanstartdate", LoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@loanstartdate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            If Not Year(LoanEndDate) = 1900 Then
                db.AddParameter("@loanenddate", LoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@loanenddate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            Try
                db.RunParamSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                Return ""
                Exit Function
            Catch ex As System.Exception
            End Try
        Finally
        End Try
        'End If
        '        Return Add_faAwardSchedule
    End Function
    Public Sub UpdatePriorToCuttOffDateCount(ByVal ExceptionGUID As String)
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbInsertDuplicates As New StringBuilder
        Dim intDuplicates As Integer
        Dim AwardScheduleID As String
        Dim db As New DataAccess
        Dim intAwardCutOffDateCount As Integer = 0

        Try
            With sbAwardSchedule
                .Append(" Select max(AwardCutOffDateCount) from syFameESPAwardCutOffDate where AwardCutOffDateGUID=?")
            End With
            db.ClearParameters()
            db.AddParameter("@AwardCutOffDateGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                intAwardCutOffDateCount = db.RunParamSQLScalar(sbAwardSchedule.ToString)
                If intAwardCutOffDateCount = 0 Then
                    intAwardCutOffDateCount = 1
                Else
                    intAwardCutOffDateCount += 1
                End If
            Catch ex As System.Exception
                If intAwardCutOffDateCount = 0 Or IsDBNull(intAwardCutOffDateCount) Then
                    intAwardCutOffDateCount = 1
                End If
            End Try
            sbAwardSchedule.Remove(0, sbAwardSchedule.Length)

            If intAwardCutOffDateCount = 1 Then
                With sbAwardSchedule
                    .Append("INSERT INTO syFameESPAwardCutOffDate(AwardCutOffDateGUID,AwardCutOffDateCount) ")
                    .Append("VALUES(?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@AwardCutOffDateGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardCutOffDateCount", intAwardCutOffDateCount, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                Try
                    db.RunParamSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                Catch ex As System.Exception
                End Try
            Else
                With sbAwardSchedule
                    .Append("Update syFameESPAwardCutOffDate Set AwardCutOffDateCount=? where  AwardCutOffDateGUID=? ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@AwardCutOffDateCount", intAwardCutOffDateCount, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@AwardCutOffDateGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    db.RunParamSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                Catch ex As System.Exception
                End Try
            End If
        Finally
        End Try
    End Sub
    Public Function CountByAwardDate(ByVal AwardCutOffDateGUID As String) As Integer
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbInsertDuplicates As New StringBuilder
        Dim intDuplicates As Integer
        Dim AwardScheduleID As String
        Dim db As New DataAccess
        Dim intAwardCutOffDateCount As Integer = 0
        Dim ds As DataSet

        Try

            db.AddParameter("@AwardCutOffDateGUID", AwardCutOffDateGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                ds = db.RunParamSQLDataSetUsingSP("USP_FL_GetAwardCutOffDateCount")
                intAwardCutOffDateCount = ds.Tables(0).Rows(0)("AwardCutOffDateCount")
                Return intAwardCutOffDateCount
            Catch ex As System.Exception
                Return 0
            End Try
        Finally
        End Try
    End Function
    'Public Function BuildExceptionReport(ByRef db As DataAccess, ByVal SSN As String, ByVal FAID As String, ByVal Fund As String, ByVal AwardYear as , ByVal FileName As String, ByVal ImportedDate As String, ByVal Message As String, Optional ByVal ExceptionGUID As String = "") As String
    '    Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
    '    Dim sbInsertDuplicates As New StringBuilder
    '    Dim intDuplicates As Integer
    '    Dim AwardScheduleID As String

    '    Try
    '        With sbAwardSchedule
    '            .Append("INSERT INTO syFameESPExceptionReport(ExceptionReportId,SSN,FAID,Fund,GrossAmount,FileName,Message,moduser,moddate,ExceptionGUID) ")
    '            .Append("VALUES(?,?,?,?,?,?,?,?,?,?) ")
    '        End With

    '        'Add params
    '        db.ClearParameters()
    '        db.AddParameter("@ExceptionReportId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@FAID", FAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@Fund", Fund, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
    '        db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@Message", Message, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@moduser", "famelink", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        db.AddParameter("@moddate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
    '        db.AddParameter("@ExceptionGUID", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '        Try
    '            db.RunParamSQLExecuteNoneQuery(sbAwardSchedule.ToString)
    '            Return ""
    '            Exit Function
    '        Catch ex As System.Exception
    '        End Try
    '    Finally
    '    End Try
    '    'End If
    '    '        Return Add_faAwardSchedule
    'End Function
    Public Sub DeleteRCVDFromExceptionReport_Reprocess(ByRef db As DataAccess, ByVal SSN As String, ByVal FAID As String, _
                                                       ByVal Fund As String, ByVal GrossAmount As Decimal, _
                                                       ByVal FileName As String, Optional ByVal OverrideMessage As String = "")
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbInsertDuplicates As New StringBuilder
        Dim intDuplicates As Integer
        Dim AwardScheduleID As String

        Try
            With sbAwardSchedule
                .Append("Delete from  syFameESPExceptionReport where SSN=? and FAID=? and Fund=? and GrossAmount=? and FileName=? ")
                If Not OverrideMessage = "" Then
                    .Append(" and Message=? ")
                End If
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FAID", FAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Fund", Fund, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@GrossAmount", GrossAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not OverrideMessage = "" Then
                db.AddParameter("@Message", OverrideMessage, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            Try
                db.RunParamSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                Exit Sub
            Catch ex As System.Exception
            End Try
        Finally
        End Try
    End Sub
    Public Sub DeleteRCVDFromExceptionReport(ByRef db As DataAccess, ByVal SSN As String, ByVal FAID As String, ByVal Fund As String, ByVal msgEntry As FLMessageInfo, ByVal FileName As String, Optional ByVal OverrideMessage As String = "")
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbInsertDuplicates As New StringBuilder
        Dim intDuplicates As Integer
        Dim AwardScheduleID As String

        Try
            With sbAwardSchedule
                .Append("Delete from  syFameESPExceptionReport where SSN=? and FAID=? and Fund=? and GrossAmount=? and FileName=? ")
                If Not OverrideMessage = "" Then
                    .Append(" and Message=? ")
                End If
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FAID", FAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Fund", Fund, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If (msgEntry.strChgCode.ToLower = "c" Or msgEntry.strChgCode.ToLower = "n") Then
                db.AddParameter("@GrossAmount", msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Else
                db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            End If
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not OverrideMessage = "" Then
                db.AddParameter("@Message", OverrideMessage, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            Try
                db.RunParamSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                Exit Sub
            Catch ex As System.Exception
            End Try
        Finally
        End Try
    End Sub

    Public Sub UpdateExceptionRecord(ByRef db As DataAccess, ByVal strFileName As String, ByVal msgEntry As FLMessageInfo)
        Dim strUpdate As New System.Text.StringBuilder(1000)
        Dim strFileNameOnly As String = ""

        Dim intStartIndex As Integer = InStrRev(msgEntry.strFileNameSource, "/")
        If intStartIndex >= 0 Then
            strFileNameOnly = msgEntry.strFileNameSource.Substring(intStartIndex)
        End If

        With strUpdate
            .Append("update syFameESPExceptionReport ")
            .Append("set Message = ? ")
            .Append(", ModUser = ? ")
            .Append(", ModDate = ? ")
            If strFileNameOnly = "" Then
                .Append("where FileName like '%" & strFileName & "%' and RecordPosition = ?")
            Else
                .Append("where (FileName like '%" & strFileName & "%' or FileName like '%" & strFileName & "%') and RecordPosition = ?")
            End If

        End With

        db.ClearParameters()
        db.AddParameter("@Message", msgEntry.Message, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ModUser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ModDate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@RecPos", msgEntry.RecordPosition, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(strUpdate.ToString)
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error updating record " & msgEntry.RecordPosition & " in file " & strFileName & " - " & ex.Message)
            Else
                Throw New Exception("Error updating record " & msgEntry.RecordPosition & " in file " & strFileName & " - " & ex.InnerException.Message)
            End If
        End Try
    End Sub

    Public Sub DeleteExceptionRecord(ByRef db As DataAccess, ByVal strFileName As String, ByVal msgEntry As FLMessageInfo)
        Dim strDelete As New System.Text.StringBuilder(1000)
        Dim strFileNameOnly As String = ""
        Dim intStartIndex As Integer

        intStartIndex = InStrRev(msgEntry.strFileNameSource, "/")
        If intStartIndex > 0 Then
            strFileNameOnly = msgEntry.strFileNameSource.Substring(intStartIndex)
        Else
            intStartIndex = InStrRev(msgEntry.strFileNameSource, "\")
            If intStartIndex > 0 Then
                strFileNameOnly = msgEntry.strFileNameSource.Substring(intStartIndex)
            End If
        End If

        With strDelete
            .Append("delete from syFAMEESPExceptionReport ")
            .Append("where RecordPosition = ? ")
            If strFileNameOnly = "" Then
                .Append("and FileName like '%" & strFileName & "%' ")
            Else
                .Append("and (FileName like '%" & strFileName & "%' or FileName like '%" & strFileNameOnly & "%')")
            End If

        End With

        db.ClearParameters()
        db.AddParameter("@RecPos", msgEntry.RecordPosition, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(strDelete.ToString)
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error deleting record " & msgEntry.RecordPosition & " in file " & strFileName & " - " & ex.Message)
            Else
                Throw New Exception("Error deleting record " & msgEntry.RecordPosition & " in file " & strFileName & " - " & ex.InnerException.Message)
            End If
        End Try
    End Sub


    Public Function getExceptionReportByFileName(Optional ByVal FileName As String = "", Optional ByVal sortby As String = "") As DataSet
        Dim db As New DataAccess
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbInsertDuplicates As New StringBuilder
        Dim intDuplicates As Integer
        Dim AwardScheduleID As String
        Dim ds As New DataSet
        ds = Nothing

        With sbInsertDuplicates
            .Append(" select * from syFameESPExceptionReport ")
            '.Append(" where isPriorToCutOffDate=0 ")
            If Not FileName = "" Then
                .Append(" WHERE FileName='" & FileName & "' ")
            End If
            If Not sortby = "" Then
                .Append(" order by " + sortby)
            Else
                .Append("order by SSN,moddate ")
            End If

        End With
        Try
            ds = db.RunParamSQLDataSet(sbInsertDuplicates.ToString)
            Return ds
        Catch ex As System.Exception
            Return ds
        End Try
    End Function
    Public Function getExceptionReport(Optional ByVal ExceptionGUID As String = "") As DataSet
        Dim db As New DataAccess
        'Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbInsertDuplicates As New StringBuilder
        Dim intDuplicates As Integer
        Dim AwardScheduleID As String
        Dim ds As New DataSet
        ds = Nothing

        Try
            If ExceptionGUID = "" Then
                ds = db.RunParamSQLDataSetUsingSP("USP_FL_GetExceptionReport")
            Else
                db.AddParameter("@exguid", ExceptionGUID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                ds = db.RunParamSQLDataSetUsingSP("USP_FL_GetExceptionReport_By_ExceptionGuid")
            End If

            Return ds
        Catch ex As System.Exception
            Return ds
        End Try
    End Function
    Public Function getExceptionReportAndSort(Optional ByVal SortBy As String = "") As DataSet
        Dim db As New DataAccess
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbInsertDuplicates As New StringBuilder
        Dim intDuplicates As Integer
        Dim AwardScheduleID As String
        Dim ds As New DataSet
        ds = Nothing

        With sbInsertDuplicates
            .Append(" select * from syFameESPExceptionReport ")
            If Not SortBy = "" Then
                .Append(" Order By " + SortBy)
            End If
        End With
        Try
            ds = db.RunParamSQLDataSet(sbInsertDuplicates.ToString)
            Return ds
        Catch ex As System.Exception
            Return ds
        End Try
    End Function
    'Public Function getExceptionReportWithCutOffDate(Optional ByVal ExceptionGUID As String = "") As DataSet
    '    Dim db As New DataAccess
    '    Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
    '    Dim sbInsertDuplicates As New StringBuilder
    '    Dim intDuplicates As Integer
    '    Dim AwardScheduleID As String
    '    Dim ds As New DataSet
    '    ds = Nothing

    '    With sbInsertDuplicates
    '        .Append(" select * from syFameESPExceptionReport ")
    '        .Append(" where isPriorToCutOffDate=1 ")
    '        If Not ExceptionGUID = "" Then
    '            .Append(" and ExceptionGUID='" & ExceptionGUID & "' ")
    '        End If
    '        .Append("order by SSN,moddate ")
    '    End With
    '    Try
    '        ds = db.RunParamSQLDataSet(sbInsertDuplicates.ToString)
    '        Return ds
    '    Catch ex As System.Exception
    '        Return ds
    '    End Try
    'End Function
    Public Function getExceptionReport(ByVal SSN As String, ByVal MessageType As String, ByVal strFileName As String, Optional ByVal sortby As String = "", Optional ByVal FundSource As String = "", Optional ByVal ShowRemoved As Integer = 0, Optional ByVal CampusId As String = "") As DataSet
        Dim db As New DataAccess
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbInsertDuplicates As New StringBuilder
        Dim intDuplicates As Integer
        Dim AwardScheduleID As String
        Dim ds As New DataSet
        ds = Nothing

        With sbInsertDuplicates
            .Append("( ")
            .Append(" SELECT * FROM syFameESPExceptionReport ")
            .Append(" WHERE FileName like + '%' + ? + '%'")

            If ShowRemoved = 0 Then
                .Append(" AND Hide = ? ")
            End If

            If CampusId <> "" Then
                .Append(" and exists( ")
                .Append("select st.ssn ")
                .Append("from arStudent st, arStuEnrollments se ")
                .Append("where st.StudentId=se.StudentId ")
                .Append("and st.SSN=syFameESPExceptionReport.ssn ")
                .Append("and se.CampusId=?) ")
            End If

            .Append(") ")

            'We need to get any records with invalid SSNs
            'These will be excluded by the first select statement above

            .Append("UNION ")
            .Append("( ")
            .Append("SELECT * FROM syFameESPExceptionReport ")
            .Append(" WHERE FileName like + '%' + ? + '%'")

            If ShowRemoved = 0 Then
                .Append(" AND Hide = ? ")
            End If

            .Append("and not exists( ")
            .Append("       select st.SSN ")
            .Append("       from arStudent st ")
            .Append("       where st.SSN=syFameESPExceptionReport.ssn ")
            .Append(") ")
            .Append(") ")

        End With
        db.ClearParameters()
        db.AddParameter("@FileName", strFileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If ShowRemoved = 0 Then
            db.AddParameter("@showremoved", ShowRemoved, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        End If

        If CampusId <> "" Then
            db.AddParameter("@campusid", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        db.AddParameter("@FileName", strFileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        If ShowRemoved = 0 Then
            db.AddParameter("@showremoved", ShowRemoved, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        End If

        Try
            ds = db.RunParamSQLDataSet(sbInsertDuplicates.ToString)
            Return ds
        Catch ex As System.Exception
            Return ds
        End Try
    End Function
    'Public Function getExceptionReportAfterOverload(ByVal ExceptionGUID As String, Optional ByVal strFileName As String = "", Optional ByVal SSN As String = "", Optional ByVal FundSource As String = "", Optional ByVal sortby As String = "") As DataSet
    '    Dim db As New DataAccess
    '    Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
    '    Dim sbInsertDuplicates As New StringBuilder
    '    Dim intDuplicates As Integer
    '    Dim AwardScheduleID As String
    '    Dim ds As New DataSet
    '    ds = Nothing

    '    With sbInsertDuplicates
    '        .Append(" SELECT * FROM syFameESPExceptionReport ")
    '        If Not ExceptionGUID = "" Or Not SSN = "" Or Not strFileName = "" Or Not FundSource = "" Then
    '            .Append(" WHERE  ")
    '        End If
    '        If Not ExceptionGUID = "" Then
    '            .Append(" ExceptionGUID='" & ExceptionGUID & "' ")
    '        End If
    '        If Not SSN = "" Then
    '            If Not ExceptionGUID = "" Then
    '                .Append(" AND ")
    '            End If
    '            .Append("  SSN=? ")
    '        End If
    '        If Not MessageType = "" Then
    '            If Not SSN = "" Then
    '                .Append(" AND ")
    '            End If
    '            .Append(" FileName like  + ? + '%'")
    '        End If
    '        If Not strFileName = "" Then
    '            If Not SSN = "" Or Not MessageType = "" Then
    '                .Append(" AND ")
    '            End If
    '            .Append(" FileName like  + ? + '%'")
    '        End If
    '        If Not FundSource = "" Then
    '            If Not SSN = "" Or Not MessageType = "" Or Not strFileName = "" Then
    '                .Append(" AND ")
    '            End If
    '            .Append(" Fund in ")
    '            .Append(FundSource)
    '        End If
    '        If Not sortby = "" Then
    '            .Append(" order by " + sortby)
    '        Else
    '            .Append(" order by SSN,moddate ")
    '        End If
    '    End With
    '    db.ClearParameters()
    '    If Not SSN = "" Then
    '        db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    End If
    '    If Not MessageType = "" Then
    '        db.AddParameter("@MessageType", MessageType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    End If
    '    If Not strFileName = "" Then
    '        db.AddParameter("@FileName", strFileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
    '    End If

    '    Try
    '        ds = db.RunParamSQLDataSet(sbInsertDuplicates.ToString)
    '        Return ds
    '    Catch ex As System.Exception
    '        Return ds
    '    End Try
    'End Function
    Public Sub updateExceptionCount(ByVal FileName As String, ByVal Success As Integer, ByVal Failed As Integer)
        'Dim db As New FAME.DataAccessLayer.SQLDataAccess
        Dim db As New DataAccess
        Dim sbUpdate As New StringBuilder
        Dim str As New StringBuilder

        Try
            With str
                .Append("Update syFameESPExceptionReport ")
                .Append("set Success = ?, ")
                .Append("Failed = ? ")
                .Append("where FileName = ? ")
            End With

            db.AddParameter("@filename", FileName, DataAccess.OleDbDataType.OleDbString, 100, ParameterDirection.Input)
            db.AddParameter("@success", Success, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@failed", Failed, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery(str.ToString)
        Catch ex As System.Exception
        End Try
    End Sub
    Public Function getFileNameWithStatus(ByVal FileName As String) As DataSet
        Dim db As New DataAccess
        Dim sbUpdate As New StringBuilder
        Dim ds As New DataSet
        With sbUpdate
            .Append(" Select Top 1 ModDate,Success,Failed from syFameESPExceptionReport where FileName='" & FileName & "' ")
            .Append(" and Success is not null and Failed is not null ")
        End With
        ds = db.RunParamSQLDataSet(sbUpdate.ToString)
        Return ds
    End Function
    Public Sub DeleteFromExceptionReport(ByVal FileName As String)
        Dim db As New DataAccess
        Dim sbUpdate As New StringBuilder
        With sbUpdate
            .Append(" Delete from syFameESPExceptionReport where FileName='" & FileName & "' ")
        End With
        Try
            db.RunParamFLSQLExecuteNoneQuery(sbUpdate.ToString)
        Catch ex As System.Exception
        End Try
    End Sub
    Public Function getAllFilesNamesFromExceptionReport(Optional ByVal MessageType As String = "") As DataSet
        Dim db As New DataAccess
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbInsertDuplicates As New StringBuilder
        Dim intDuplicates As Integer
        Dim AwardScheduleID As String
        Dim ds As New DataSet
        ds = Nothing

        With sbInsertDuplicates
            .Append(" select Distinct FileName from syFameESPExceptionReport ")
            '.Append(" where isPriorToCutOffDate=0 ")
            If Not MessageType = "" Then
                .Append(" WHERE FileName like  + ? + '%'")
            End If
        End With
        If Not MessageType = "" Then
            db.ClearParameters()
            db.AddParameter("@MessageType", MessageType, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        Try
            ds = db.RunParamSQLDataSet(sbInsertDuplicates.ToString)
            Return ds
        Catch ex As System.Exception
            Return ds
        End Try
    End Function
    Public Function CheckDateExistsInExceptionReport(ByVal SSN As String, ByVal strFAID As String, ByVal strFund As String, ByVal strMessageType As String) As Integer
        'Check for duplicates before executing
        Dim sbCheckDuplicateAwards As New StringBuilder
        Dim intDuplicates As Integer
        Dim db As New DataAccess

        With sbCheckDuplicateAwards
            .Append(" Select Count(*) from syfameespexceptionreport where ")
            .Append(" SSN='" & SSN & "' and FAID='" & strFAID & "' and Fund='" & strFund & "' and FileName like '" & strMessageType & "%'  ")
            'commented by balaji on 11/6/2009
            '.Append(" and isEligibleForReprocess=1  ")
            '.Append(" and isPriorToCutOffDate=0 ")
        End With
        Try
            intDuplicates = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
            If intDuplicates >= 1 Then
                Return 1
            Else
                Return 0
            End If
        Catch ex As System.Exception
            Return 0
        End Try
    End Function

    Public Function RecordExistsInExceptionReport(ByVal strFileName As String, ByVal intRecordPosition As Integer) As Integer
        'Check for duplicates before executing
        Dim sbCheckDuplicateAwards As New StringBuilder
        Dim intDuplicates As Integer
        Dim db As New DataAccess

        With sbCheckDuplicateAwards
            .Append(" Select Count(*) from syfameespexceptionreport where ")
            .Append(" FileName like '" & strFileName & "%' and RecordPosition = " & intRecordPosition)
        End With
        Try
            intDuplicates = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
            If intDuplicates >= 1 Then
                Return 1
            Else
                Return 0
            End If
        Catch ex As System.Exception
            Return 0
        End Try
    End Function

    Public Function RecordIsMarkedAsHidden(ByVal strFileName As String, ByVal intRecordPosition As Integer) As Integer
        Dim sbCheckDuplicateAwards As New StringBuilder
        Dim intDuplicates As Integer
        Dim db As New DataAccess

        With sbCheckDuplicateAwards
            .Append(" Select Count(*) from syfameespexceptionreport where ")
            .Append(" FileName like '" & strFileName & "%' and RecordPosition = " & intRecordPosition & " and Hidden = 1")
        End With
        Try
            intDuplicates = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
            If intDuplicates >= 1 Then
                Return 1
            Else
                Return 0
            End If
        Catch ex As System.Exception
            Return 0
        End Try
    End Function


    Public Function ReturnMessageForDISB(ByVal SSN As String, ByVal strFAID As String, ByVal strFund As String, ByVal strMessageType As String) As String
        'Check for duplicates before executing
        Dim sbCheckDuplicateAwards As New StringBuilder
        Dim strExceptionMessage As String = ""
        Dim db As New DataAccess

        With sbCheckDuplicateAwards
            .Append(" Select Top 1 Message from syFameESPExceptionReport where ")
            .Append(" SSN='" & SSN & "' and FAID='" & strFAID & "' and Fund='" & strFund & "' and FileName like '" & strMessageType & "%'  ")
            .Append(" and isEligibleForReprocess=1  ")
        End With
        Try
            strExceptionMessage = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
            If Not strExceptionMessage = "" Then
                Return strExceptionMessage
            Else
                Return ""
            End If
        Catch ex As System.Exception
            Return ""
        End Try
    End Function
    Public Function CheckDateExistsInExceptionReportByFileName(ByVal strMessageType As String) As Integer
        'Check for duplicates before executing
        Dim sbCheckDuplicateAwards As New StringBuilder
        Dim intDuplicates As Integer
        Dim db As New DataAccess

        With sbCheckDuplicateAwards
            .Append(" Select Count(*) from syFameESPExceptionReport where ")
            .Append(" FileName = '" & strMessageType & "'  ")
            .Append(" and isEligibleForReprocess=0  ")
            '.Append("and isPriorToCutOffDate=0")
        End With
        Try
            intDuplicates = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
            If intDuplicates >= 1 Then
                Return 1
            Else
                Return 0
            End If
        Catch ex As System.Exception
            Return 0
        End Try
    End Function
    Public Function GetMatchingStudentAwards(ByRef db As DataAccess, ByVal strStuEnrollID As String, ByVal strFAID As String, ByVal GrossAmount As Decimal, ByVal AwardStartDate As DateTime, ByVal AwardEndDate As DateTime, Optional ByVal StudentName As String = "", Optional ByVal strFund As String = "") As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardID As New System.Text.StringBuilder(1000)
        Dim sbAcademicYear As New StringBuilder
        Dim strAwardYear As String
        Dim strAwardYearInput As String
        Dim strLoanBeginDate, strLoanEndDate As String
        Dim StudentAwardID As String

        If String.IsNullOrEmpty(strFAID) Then
            Return "Unable to get student award for student " & StudentName & " as the FAID provided is empty"
            Exit Function
        End If
        If String.IsNullOrEmpty(strStuEnrollID) Then
            Return "Unable to find student enrollment information for the student" & StudentName
            Exit Function
        End If
        Dim strFaidCount As Integer = strFAID.Length
        Dim intFund As Integer
        intFund = CInt(strFund)
        'If strFund = "02" Or strFund = "03" Or strFund = "04" Then
        '    strAwardYearInput = Mid(strFAID, strFaidCount, 1)
        '    strAwardYearInput = "200" & (strAwardYearInput - 1)
        'Else
        '    strAwardYearInput = Mid(strFAID, 11, 1)
        '    strAwardYearInput = "200" & (strAwardYearInput - 1)
        'End If
        If strFund = "02" Or strFund = "03" Or strFund = "04" Then
            strAwardYearInput = Mid(strFAID, strFaidCount, 1)
            strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
            'strAwardYearInput = "200" & (strAwardYearInput - 1)
        Else
            strAwardYearInput = Mid(strFAID, 11, 1)
            strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
            'strAwardYearInput = "200" & (strAwardYearInput - 1)
        End If
        If strAwardYearInput = "" Then
            Return "Unable to match the award year from Fame ESP with Advantage Award Year for student " & StudentName
            Exit Function
        End If
        With sbAcademicYear
            .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
        End With
        db.ClearParameters()
        db.AddParameter("@AcademicYearDescrip", strAwardYearInput, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'Try
        '    strAwardYear = CType(db.RunParamSQLScalar(sbAcademicYear.ToString), Guid).ToString
        'Catch ex As System.Exception
        '    strAwardYear = System.DBNull.Value.ToString
        'End Try
        Try
            dr = db.RunParamSQLDataReader(sbAcademicYear.ToString())
            If Not (dr.HasRows) Then
                Return "Unable to create student award as the award year was not found in the database for student " & StudentName
                Exit Function
            End If
            While dr.Read()
                If Not (dr("AcademicYearId") Is System.DBNull.Value) Or Not (dr("AcademicYearId").ToString = Guid.Empty.ToString) Then
                    strAwardYear = dr("AcademicYearId").ToString
                Else
                    Return "Unable to create student award as the award year was not found in the database for student " & StudentName
                    Exit Function
                End If
            End While
        Catch ex As System.Exception
            Return "Unable to create student award as the award year was not found in the database for student " & StudentName
            Exit Function
        Finally
            dr.Close()
        End Try

        sbAwardID = New StringBuilder
        With sbAwardID
            .Append(" select Distinct Top 1 StudentAwardId from faStudentAwards where StuEnrollId=? and ")
            .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
            If strAwardYear.Length >= 30 Then
                .Append(" and AcademicYearId = ? ")
            End If
            .Append(" and GrossAmount=? ")
            .Append(" and (FA_ID is NULL or FA_ID ='') ")
            If strFund = "06" Or strFund = "07" Or strFund = "08" Then
                .Append(" and AwardStartDate=? ")
                .Append(" and AwardEndDate=? ")
            End If
        End With

        Try
            db.ClearParameters()
            db.AddParameter("@STUENROLLID", strStuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FundSourceId", intFund, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If strAwardYear.Length >= 30 Then
                db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@GrossAmount", GrossAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            If strFund = "06" Or strFund = "07" Or strFund = "08" Then
                db.AddParameter("@AwardStartDate", AwardStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@AwardEndDate", AwardEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            dr = db.RunParamSQLDataReader(sbAwardID.ToString())
            If Not (dr.HasRows) Then
                Return ""
                Exit Function
            End If
            While dr.Read()
                Try
                    StudentAwardID = dr("StudentAwardID").ToString
                    Return StudentAwardID
                Catch ex As System.Exception
                    Return ""
                    Exit Function
                End Try
            End While
        Catch ex As System.Exception
            Return ""
            Exit Function
        Finally
            dr.Close()
        End Try
    End Function
    Public Function Add_faStudentAwardsFromFAID(ByRef groupTrans As OleDbTransaction, ByRef StudentAwardID As String, ByVal StuEnrollID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal strStudentName As String = "", Optional ByVal SourceToTarget As String = "") As String
        Dim sbStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbUpdateStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchool As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchoolDiffLoanPeriods As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchoolWithDiffGrossAmount As New System.Text.StringBuilder(1000)


        Dim AwardTypeID As String = " "
        Dim getAwardTypeMessage As String = ""
        Dim Disbursements As Integer = 0
        Dim LoanID As String = " "
        Dim intDuplicates As Integer = 0
        Dim intDuplicatesBySchool As Integer = 0
        Dim intDuplicatesBySchoolDiffLoanPeriods As Integer = 0
        Dim intDuplicatesBySchoolDiffGrossAmount As Integer = 0
        Dim intDuplicatesBySchoolSameAwardYearDiffLoanPeriods As Integer = 0

        Dim sbAcademicYear As New StringBuilder
        Dim strAwardYear As String
        Dim strLoanBeginDate, strLoanEndDate As String
        Dim dr As OleDbDataReader
        Dim db As New DataAccess
        m_ExceptionMessage = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'db.OpenConnection()

        If String.IsNullOrEmpty(StuEnrollID) Or StuEnrollID = Guid.Empty.ToString Then
            Return "Unable to find enrollment information for the student" & strStudentName
            Exit Function
        End If

        If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
            strLoanBeginDate = "07/01/" + Mid(msgEntry.strAwdyr, 1, 4)
            strLoanEndDate = "06/30/" + Mid(msgEntry.strAwdyr, 6, 2)
        Else
            strLoanBeginDate = msgEntry.dateDate1
            strLoanEndDate = msgEntry.dateDate2
        End If


        'Look for matching records
        Try
            StudentAwardID = GetMatchingStudentAwards(db, StuEnrollID, msgEntry.strFAID, msgEntry.decAmount1, strLoanBeginDate, strLoanEndDate, strStudentName, msgEntry.strFund)
            If Not StudentAwardID = "" Then
                'Update existing record with this FAID
                With sbUpdateStudentAwards
                    .Append("Update faStudentAwards set FA_Id=?,modUser=?,moddate=? where StudentAwardId=? ")
                End With
                db.ClearParameters()
                db.AddParameter("@FA_ID", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", "famelink", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@StudentAwardId", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                Try
                    db.RunParamSQLExecuteNoneQuery(sbUpdateStudentAwards.ToString)
                    Return ""
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to match/update FAID with student awards for student " & strStudentName
                    Exit Function
                End Try
            Else
                StudentAwardID = Guid.NewGuid.ToString
            End If
        Catch ex As System.Exception
            StudentAwardID = Guid.NewGuid.ToString
        End Try

        'Before Creating new awards check to see if the Award Start Date falls after the AwardsCuttOffDate
        'in web.config
        Try
            getAwardTypeMessage = GetAwardTypeID(db, AwardTypeID, msgEntry.strFund, msgEntry.strYYYY)
            If Not getAwardTypeMessage = "" Then
                Return "Unable to add student awards for student " & strStudentName & " as the provided award type was not found"
                Exit Function
            End If

            Dim strAwardYearInput As String
            Dim strFaidCount As Integer = msgEntry.strFAID.Length
            If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                strAwardYearInput = Mid(msgEntry.strFAID, strFaidCount, 1)
                strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
                'strAwardYearInput = "200" & (strAwardYearInput - 1)
            Else
                strAwardYearInput = Mid(msgEntry.strFAID, 11, 1)
                strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
                'strAwardYearInput = "200" & (strAwardYearInput - 1)
            End If
            If strAwardYearInput = "" Then
                Return "Unable to match the award year from Fame ESP with Advantage Award Year for student " & strStudentName
                Exit Function
            End If
            With sbAcademicYear
                .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
            End With
            db.ClearParameters()
            db.AddParameter("@AcademicYearDescrip", strAwardYearInput, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                dr = db.RunParamSQLDataReader(sbAcademicYear.ToString())
                If Not (dr.HasRows) Then
                    Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
                    Exit Function
                End If
                While dr.Read()
                    If Not (dr("AcademicYearId") Is System.DBNull.Value) Or Not (dr("AcademicYearId").ToString = Guid.Empty.ToString) Then
                        strAwardYear = dr("AcademicYearId").ToString
                    Else
                        Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
                        Exit Function
                    End If
                End While
            Catch ex As System.Exception
                Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
                Exit Function
            Finally
                dr.Close()
            End Try

            'With sbAcademicYear
            '    .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
            'End With
            'db.ClearParameters()
            'db.AddParameter("@AcademicYearDescrip", Mid(msgEntry.strAwdyr, 1, 4), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'Try
            '    dr = db.RunParamSQLDataReader(sbAcademicYear.ToString())
            '    If Not (dr.HasRows) Then
            '        Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
            '        Exit Function
            '    End If
            '    While dr.Read()
            '        If Not (dr("AcademicYearId") Is System.DBNull.Value) Or Not (dr("AcademicYearId").ToString = Guid.Empty.ToString) Then
            '            strAwardYear = dr("AcademicYearId").ToString
            '            Exit While
            '        Else
            '            Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
            '            Exit Function
            '        End If
            '    End While
            'Catch ex As System.Exception
            '    Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
            '    Exit Function
            'Finally
            '    dr.Close()
            'End Try

            'If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
            '    strLoanBeginDate = "07/01/" + Mid(msgEntry.strAwdyr, 1, 4)
            '    strLoanEndDate = "06/30/" + Mid(msgEntry.strAwdyr, 6, 2)
            'Else
            '    strLoanBeginDate = msgEntry.dateDate1
            '    strLoanEndDate = msgEntry.dateDate2
            'End If

            Dim intCutOffDates As Integer
            Dim dtCuttOfDate As Date = CDate(MyAdvAppSettings.AppSettings("AwardsCutOffDate").ToString)
            Dim dtAwardStartDate As Date = CDate(strLoanBeginDate)
            intCutOffDates = DateDiff(DateInterval.Day, dtCuttOfDate, dtAwardStartDate)
            If intCutOffDates < 0 Then
                Return "The award start date " & dtAwardStartDate.ToString & " is earlier than the award cut off date (" & MyAdvAppSettings.AppSettings("AwardsCutOffDate").ToString & "), so student award will not be created for " & strStudentName
                Exit Function
            End If

            'Checks to see if the school has put in an award for the same fund and award year
            'but for different gross amount
            'example: an award may be created by school for $4500 for Direct Loan 05-06
            'but from Fame ESP we may get a award data for $3500 and $1000 for same year
            'and this case we need to send the data($3500 and $1000) from Fame ESP to exception
            'while checking ignore Gross Amount and look for records with null FAID and moduser
            'not equal to famelink which identifies that record was put in by school
            'Mantis # 12077.
            If Not Trim(SourceToTarget) = "exceptiontotarget" Then
                'Modified by balaji on 09/27/2007 to take gross amount into consideration
                'Check to see if award exists with same gross amount and same award year and period
                With sbCheckDuplicateAwardsBySchool
                    .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
                    .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
                    If strAwardYear.Length >= 30 Then
                        .Append(" and AcademicYearId = ? ")
                    End If
                    .Append(" and GrossAmount=? ")
                    If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                        .Append(" and AwardStartDate=? ")
                        .Append(" and AwardEndDate=? ")
                    End If
                    ' .Append(" and (ModUser <> 'famelink' or ModUser is NULL) ")
                End With
                db.ClearParameters()
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                    db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If

                Try
                    intDuplicatesBySchool = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchool.ToString)
                    If intDuplicatesBySchool >= 1 Then
                        'if award exists with same gross amount and same award year and period
                        'ignore this record as Fa_id has already been updated and thats all this record can do
                        'and process the next record
                        Return ""
                        Exit Function
                    Else
                        'if award does not exists then check for different gross amount and same award year and period
                        'If so then alert message and go to exception else ignore and go to next record
                        'and process the next record
                        intDuplicatesBySchool = 0
                        With sbCheckDuplicateAwardsBySchoolWithDiffGrossAmount
                            .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
                            .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
                            If strAwardYear.Length >= 30 Then
                                .Append(" and AcademicYearId = ? ")
                            End If
                            .Append(" and GrossAmount <> ? ")
                            If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                                .Append(" and AwardStartDate=? ")
                                .Append(" and AwardEndDate=? ")
                            End If
                            '  .Append(" and (ModUser <> 'famelink' or ModUser is NULL) ")
                        End With
                        db.ClearParameters()
                        db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                        If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                            db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                            db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                        End If
                        Try
                            intDuplicatesBySchoolDiffGrossAmount = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchoolWithDiffGrossAmount.ToString)
                            If intDuplicatesBySchoolDiffGrossAmount >= 1 Then
                                Dim sbGetGrossAmount As New StringBuilder
                                Dim drDiffGrossAmount As OleDbDataReader
                                Dim strAdvantageGrossAmount As String = ""
                                With sbGetGrossAmount
                                    .Append(" select Distinct GrossAmount from faStudentAwards where StuEnrollId=? and ")
                                    .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
                                    If strAwardYear.Length >= 30 Then
                                        .Append(" and AcademicYearId = ? ")
                                    End If
                                    .Append(" and GrossAmount <> ? ")
                                    If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                                        .Append(" and AwardStartDate=? ")
                                        .Append(" and AwardEndDate=? ")
                                    End If
                                    ' .Append(" and (ModUser <> 'famelink' or ModUser is NULL) ")
                                End With
                                db.ClearParameters()
                                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                                db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                                If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                                    db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                                    db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                                End If
                                Try
                                    drDiffGrossAmount = db.RunParamSQLDataReader(sbGetGrossAmount.ToString())
                                    If Not (drDiffGrossAmount.HasRows) Then
                                        strAdvantageGrossAmount = ""
                                    End If
                                    While drDiffGrossAmount.Read()
                                        strAdvantageGrossAmount = CType(drDiffGrossAmount("GrossAmount"), Decimal).ToString
                                    End While
                                Catch ex As System.Exception
                                    strAdvantageGrossAmount = ""
                                Finally
                                    drDiffGrossAmount.Close()
                                End Try
                                Dim sReturnMessage As String = "Unable to create student award for student " & strStudentName & " as the school has already created an award with a different gross amount." & vbLf
                                If Not strAdvantageGrossAmount = "" Then
                                    sReturnMessage &= vbLf
                                    sReturnMessage &= " Gross Amount in Advantage: " & CType(strAdvantageGrossAmount, Decimal).ToString("#0.00")
                                End If
                                Return sReturnMessage
                                Exit Function
                            Else
                                intDuplicatesBySchoolDiffGrossAmount = 0
                            End If
                        Catch ex5 As System.Exception
                            intDuplicatesBySchoolDiffGrossAmount = 0
                        End Try
                    End If
                Catch ex As System.Exception
                    intDuplicatesBySchool = 0
                End Try

                'Use Case: School would have created a student award with same amount but for a different Award year
                'Example:Student Thomas Piehko Award Created for $3500.00
                'School Created an award for 11/15
                'With sbCheckDuplicateAwardsBySchoolDiffLoanPeriods
                '    .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
                '    .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
                '    If strAwardYear.Length >= 30 Then
                '        .Append(" and AcademicYearId = ? ")
                '    End If
                '    .Append(" and GrossAmount=? ")
                '    .Append(" and ModUser <> 'famelink' ")
                'End With
                'db.ClearParameters()
                'db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                'db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                'Try
                '    intDuplicatesBySchoolDiffLoanPeriods = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchoolDiffLoanPeriods.ToString)
                '    If intDuplicatesBySchoolDiffLoanPeriods >= 1 Then
                '        Return "Unable to create student award for student " & strStudentName & " as the school has already created an award with a same gross amount for same fund source and different award year"
                '        Exit Function
                '    Else
                '        intDuplicatesBySchoolDiffLoanPeriods = 0
                '    End If
                'Catch ex As System.Exception
                '    intDuplicatesBySchoolDiffLoanPeriods = 0
                'End Try
                Dim sbCheckDuplicateAwardsBySchoolDiffAwardYear As New StringBuilder
                With sbCheckDuplicateAwardsBySchoolDiffAwardYear
                    .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
                    .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
                    If strAwardYear.Length >= 30 Then
                        .Append(" and AcademicYearId = ? ")
                    End If
                    .Append(" and GrossAmount=? ")
                    If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                        .Append(" and (AwardStartDate <> ? or AwardEndDate <> ?) ")
                    End If
                    '.Append(" and (ModUser <> 'famelink' or ModUser is NULL) ")
                    .Append(" and FA_ID = ? ")
                End With
                db.ClearParameters()
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                    db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                End If
                db.AddParameter("@fa_id", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    intDuplicatesBySchoolSameAwardYearDiffLoanPeriods = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchoolDiffAwardYear.ToString)
                    If intDuplicatesBySchoolSameAwardYearDiffLoanPeriods >= 1 Then
                        Dim sbGetGrossAmount As New StringBuilder
                        Dim strAwardStartDate As String = ""
                        Dim strAwardEndDate As String = ""
                        Dim drDiffLoanPeriod As OleDbDataReader
                        If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                            With sbGetGrossAmount
                                .Append(" select Distinct AwardStartDate,AwardEndDate from faStudentAwards where StuEnrollId=? and ")
                                .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
                                If strAwardYear.Length >= 30 Then
                                    .Append(" and AcademicYearId = ? ")
                                End If
                                .Append(" and GrossAmount=? ")
                                If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                                    .Append(" and (AwardStartDate <> ? or AwardEndDate <> ?) ")
                                End If
                                ' .Append(" and (ModUser <> 'famelink' or ModUser is NULL) ")
                            End With
                            db.ClearParameters()
                            db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                            db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                            If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                                db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                                db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                            End If
                            Try
                                drDiffLoanPeriod = db.RunParamSQLDataReader(sbGetGrossAmount.ToString())
                                If Not (drDiffLoanPeriod.HasRows) Then
                                    strAwardStartDate = ""
                                    strAwardEndDate = ""
                                End If
                                While drDiffLoanPeriod.Read()
                                    strAwardStartDate = CType(drDiffLoanPeriod("AwardStartDate"), Date).ToString
                                    strAwardEndDate = CType(drDiffLoanPeriod("AwardEndDate"), Date).ToString
                                End While
                            Catch ex As System.Exception
                                strAwardStartDate = ""
                                strAwardEndDate = ""
                            Finally
                                drDiffLoanPeriod.Close()
                            End Try
                        End If
                        Dim sReturnMessage As String
                        sReturnMessage = "Unable to create student award for student " & strStudentName & " as the school has already created an award with a different loan period." & vbLf
                        sReturnMessage &= vbLf
                        sReturnMessage &= " Award period from FAME ESP:" & " (" & strLoanBeginDate & " - " & strLoanEndDate & ")." & vbLf
                        If Not strAwardStartDate = "" And Not strAwardEndDate = "" Then
                            sReturnMessage &= vbLf
                            sReturnMessage &= " Award period from Advantage:" & " (" & CDate(strAwardStartDate) & " - " & CDate(strAwardEndDate) & ")."
                        End If
                        Return sReturnMessage
                        Exit Function
                    Else
                        intDuplicatesBySchoolSameAwardYearDiffLoanPeriods = 0
                    End If
                Catch ex9 As System.Exception
                    intDuplicatesBySchoolSameAwardYearDiffLoanPeriods = 0
                End Try
            End If

            'Check for duplicates before executing
            With sbCheckDuplicateAwards
                .Append(" Select Count(*) from faStudentAwards where ")
                .Append(" StuEnrollID=? and AwardTypeID=? and fa_id=? and GrossAmount=? ")
                If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                    .Append(" and AwardStartDate=? and AwardEndDate=? ")
                End If
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AwardTypeID", AwardTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@fa_id", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
                db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            Try
                intDuplicates = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
                If intDuplicates >= 1 Then
                    Return ""
                    Exit Function
                Else
                    intDuplicates = 0
                End If
            Catch ex As System.Exception
                intDuplicates = 0
            End Try


            'If there are no duplicates create a student award
            If intDuplicates = 0 Then
                With sbStudentAwards
                    .Append("INSERT INTO faStudentAwards(StudentAwardID,StuEnrollID,AwardTypeID,AcademicYearId,fa_id,GrossAmount,AwardStartDate,AwardEndDate,LoanFees,moduser,moddate,Disbursements,LoanID) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardTypeID", AwardTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@fa_id", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                    db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                Else
                    If msgEntry.decAmount2.ToString.Length >= 1 And Mid(msgEntry.decAmount2.ToString, 1) <> "0" Then
                        db.AddParameter("@LoanFees", msgEntry.decAmount1 - msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    End If
                End If
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Disbursements", Disbursements, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@LoanID", LoanID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                '   execute the query
                Try
                    db.RunParamSQLExecuteNoneQuery(sbStudentAwards.ToString)
                    Return ""
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to create student awards for student " & strStudentName
                    'db.CloseConnection()
                    Exit Function
                End Try
            End If
        Catch ex As System.Exception
            Return "Unable to create student award for student " & strStudentName
            Exit Function
        Finally
        End Try
        'Return Add_faStudentAwards
    End Function
    Public Function CheckIfInitialImportSuccessful(ByVal FileName As String) As Integer
        'Check for duplicates before executing
        Dim sbCheckDuplicateAwards As New StringBuilder
        Dim intDuplicates As Integer
        Dim db As New DataAccess

        With sbCheckDuplicateAwards
            .Append(" Select Count(*) from syFameESPExceptionReport where ")
            .Append(" SUBSTRING(FileName,1,8) = ? AND IsInitialImportSuccessful=1  ")
            'AND isPriorToCuttOffDate=0
        End With
        Try
            db.ClearParameters()
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            intDuplicates = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
            If intDuplicates >= 1 Then
                Return intDuplicates
            Else
                Return 0
            End If
        Catch ex As System.Exception
            Return 0
        End Try
    End Function
    Public Function CheckIfFileTypeExistsInException(ByVal FileType As String) As DataSet
        'Check for duplicates before executing
        Dim sbCheckDuplicateAwards As New StringBuilder
        Dim intDuplicates As Integer
        Dim db As New DataAccess
        Dim ds As New DataSet

        With sbCheckDuplicateAwards
            .Append(" select Distinct FileName from syFameESPExceptionReport where filename like '%" & FileType & "%' ")
        End With
        Try
            ds = db.RunParamSQLDataSet(sbCheckDuplicateAwards.ToString)
            Return ds
        Catch ex As System.Exception
            Return Nothing
        End Try
    End Function
    Public Function CheckIfFileIsInException(ByVal FileName As String) As Integer
        'Check for duplicates before executing
        Dim sbCheckDuplicateAwards As New StringBuilder
        Dim intDuplicates As Integer
        Dim db As New DataAccess

        With sbCheckDuplicateAwards
            .Append(" Select Count(*) from syFameESPExceptionReport where ")
            .Append(" SUBSTRING(FileName,1,8) = ? AND IsInitialImportSuccessful=1  ")
            'AND isPriorToCuttOffDate=0
        End With
        Try
            db.ClearParameters()
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            intDuplicates = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
            If intDuplicates >= 1 Then
                Return intDuplicates
            Else
                Return 0
            End If
        Catch ex As System.Exception
            Return 0
        End Try
    End Function
    Public Function MarkEligibleRecordsToReprocess(ByVal FAID() As String, ByVal SSN() As String, ByVal FileName() As String, ByVal Fund() As String, ByVal Message() As String) As String
        Dim intArrayCount As Integer
        Dim intLoop As Integer = 0
        Dim db As New DataAccess
        Dim strSSN, strFAID, strFileName As String
        Dim sbUpdateExceptionReport As New StringBuilder
        intArrayCount = SSN.Length
        While intLoop < intArrayCount
            With sbUpdateExceptionReport
                .Append(" Update syFameESPExceptionReport set isEligibleForReprocess=1 where FAID=? and SSN=? and FileName=? and Fund=? and Message=? ")
            End With
            db.ClearParameters()
            db.AddParameter("@FAID", FAID.GetValue(intLoop).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@SSN", SSN.GetValue(intLoop).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FileName", FileName.GetValue(intLoop).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Fund", Fund.GetValue(intLoop).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Message", Message.GetValue(intLoop).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Try
                db.RunParamSQLExecuteNoneQuery(sbUpdateExceptionReport.ToString)
            Catch ex As System.Exception
            End Try
            sbUpdateExceptionReport.Remove(0, sbUpdateExceptionReport.Length)
            intLoop += 1
        End While
        Return ""
    End Function
    Public Function MarkEligibleRecordsToDelete(ByVal FAID() As String, ByVal SSN() As String, ByVal FileName() As String, ByVal Fund() As String) As String
        Dim intArrayCount As Integer
        Dim intLoop As Integer = 0
        Dim db As New DataAccess
        Dim strSSN, strFAID, strFileName As String
        Dim sbUpdateExceptionReport As New StringBuilder
        intArrayCount = SSN.Length
        While intLoop < intArrayCount
            With sbUpdateExceptionReport
                .Append(" Delete from syFameESPExceptionReport where FAID=? and SSN=? and FileName=? and Fund=? ")
            End With
            db.ClearParameters()
            db.AddParameter("@FAID", FAID.GetValue(intLoop).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@SSN", SSN.GetValue(intLoop).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FileName", FileName.GetValue(intLoop).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Fund", Fund.GetValue(intLoop).ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Try
                db.RunParamSQLExecuteNoneQuery(sbUpdateExceptionReport.ToString)
            Catch ex As System.Exception
            End Try
            sbUpdateExceptionReport.Remove(0, sbUpdateExceptionReport.Length)
            intLoop += 1
        End While
        Return ""
    End Function
    'Public Function CreateStudentAwards(ByVal SSN() As String, ByVal FAID() As String, ByVal Fund() As String, ByVal AwardYear() As String, ByVal LoanBeginDate() As String, ByVal LoanEndDate() As String, ByVal GrossAmount() As String, ByVal FileName As String)
    '    Dim intArrayCount As Integer
    '    Dim intLoop As Integer
    '    Dim db As New DataAccess
    '    intArrayCount = SSN.Length
    '    While intLoop < intArrayCount
    '        Dim strSSN As String = SSN.GetValue(intLoop).ToString
    '        Dim strFAID As String = FAID.GetValue(intLoop).ToString
    '        Dim strFund As String = Fund.GetValue(intLoop).ToString
    '        Dim AwardYear As String = AwardYear.GetValue(intLoop).ToString
    '        Dim LoanBeginDate As String = LoanBeginDate.GetValue(intLoop).ToString
    '        Dim LoanEndDate As String = LoanEndDate.GetValue(intLoop).ToString
    '        Dim GrossAmount As String = GrossAmount.GetValue(intLoop).ToString


    '        'Check if Student available for SSN
    '        Dim strStudentName As String = getStudentNameAndSSN()
    '        If strStudentName = "" Then
    '            'Raise Exception and add to exception
    '            BuildExceptionReport(db,strSSN,strFAID,strFund,

    '        End If

    '        intLoop = intLoop + 1
    '    End While

    'End Function
    'Public Function GetStudentInfo(ByVal strMsgSSN As String) As String
    '    Dim ExceptionObject As System.Exception = Nothing
    '    Dim db As New DataAccess
    '    Dim dr As OleDbDataReader = Nothing
    '    Dim sb As New StringBuilder
    '    Dim dt As New DataTable
    '    Dim strStudentName As String = ""

    '    With sb
    '        .Append(" Select Distinct FirstName,LastName from arStudent where SSN='" & strMsgSSN & "' ")
    '    End With
    '    '   Execute the query
    '    dr = db.RunParamSQLDataReader(sb.ToString())
    '    'check for exception - no data
    '    If Not (dr.HasRows) Then
    '        Return "Student not found for the given SSN " & strMsgSSN
    '        Exit Function
    '    End If
    '    While dr.Read()
    '        strStudentName = dr("FirstName").ToString + " " + dr("LastName").ToString
    '        Return strStudentName
    '    End While
    'End Function
    'Public Function getStudentNameAndSSN(ByVal SSN As String) As String
    '    Dim SSNMask As String
    '    Dim zipMask As String
    '    Dim strMaskSSN As String
    '    Dim dr As DataRow
    '    Dim strStudentName As String = GetStudentInfo(SSN)
    '    If InStr(strStudentName, "Student not found") >= 1 Then
    '        Return ""
    '        Exit Function
    '    End If
    '    If SSN.ToString.Length >= 1 Then
    '        Return strStudentName & " (" + SSN & ")"
    '    Else
    '        Return ""
    '    End If
    'End Function
    Public Function Update_faStudentAwardsFromFAID(ByRef groupTrans As OleDbTransaction, ByRef StudentAwardID As String, ByVal StuEnrollID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal strStudentName As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal Override As String = "") As String
        Dim sbStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbUpdateStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchool As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchoolDiffLoanPeriods As New System.Text.StringBuilder(1000)

        Dim AwardTypeID As String = " "
        Dim getAwardTypeMessage As String = ""
        Dim Disbursements As Integer = 0
        Dim LoanID As String = " "
        Dim intDuplicates As Integer = 0
        Dim intDuplicatesBySchool As Integer = 0
        Dim intDuplicatesBySchoolDiffLoanPeriods As Integer = 0

        Dim sbAcademicYear As New StringBuilder
        Dim strAwardYear As String
        Dim strLoanBeginDate, strLoanEndDate As String
        Dim dr As OleDbDataReader
        Dim db As New DataAccess
        m_ExceptionMessage = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'db.OpenConnection()

        If String.IsNullOrEmpty(StuEnrollID) Or StuEnrollID = Guid.Empty.ToString Then
            Return "Unable to find enrollment information for the student" & strStudentName
            Exit Function
        End If

        If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
            strLoanBeginDate = "07/01/" + Mid(msgEntry.strAwdyr, 1, 4)
            strLoanEndDate = "06/30/" + Mid(msgEntry.strAwdyr, 6, 2)
        Else
            strLoanBeginDate = msgEntry.dateDate1
            strLoanEndDate = msgEntry.dateDate2
        End If


        'Look for matching records
        Try
            StudentAwardID = GetMatchingStudentAwardsForUpdate(db, StuEnrollID, msgEntry.strFAID, msgEntry.decAmount1, strLoanBeginDate, strLoanEndDate, strStudentName, msgEntry.strFund)
            If Not StudentAwardID = "" Then
                'Update existing record with this FAID
                With sbUpdateStudentAwards
                    .Append("Update faStudentAwards set FA_Id=?,moduser=?,moddate=? where StudentAwardId=? ")
                End With
                db.ClearParameters()
                db.AddParameter("@FA_ID", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", "famelink", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@StudentAwardId", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                Try
                    db.RunParamSQLExecuteNoneQuery(sbUpdateStudentAwards.ToString)
                    'Return ""
                    ''db.CloseConnection()
                    'Exit Function
                Catch ex As System.Exception
                    Return "Unable to match/update FAID with student awards for student " & strStudentName
                    'db.CloseConnection()
                    Exit Function
                End Try
            Else
                StudentAwardID = Guid.NewGuid.ToString
            End If
        Catch ex As System.Exception
            StudentAwardID = Guid.NewGuid.ToString
        End Try

        'Before Creating new awards check to see if the Award Start Date falls after the AwardsCuttOffDate
        'in web.config
        Try
            getAwardTypeMessage = GetAwardTypeID(db, AwardTypeID, msgEntry.strFund, msgEntry.strYYYY)
            If Not getAwardTypeMessage = "" Then
                Return "Unable to add student awards for student " & strStudentName & " as the provided award type was not found"
                Exit Function
            End If

            Dim strAwardYearInput As String
            Dim strFaidCount As Integer = msgEntry.strFAID.Length
            If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                strAwardYearInput = Mid(msgEntry.strFAID, strFaidCount, 1)
                strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
                'strAwardYearInput = "200" & (strAwardYearInput - 1)
            Else
                strAwardYearInput = Mid(msgEntry.strFAID, 11, 1)
                strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
                'strAwardYearInput = "200" & (strAwardYearInput - 1)
            End If
            If strAwardYearInput = "" Then
                Return "Unable to match the award year from Fame ESP with Advantage Award Year for student " & strStudentName
                Exit Function
            End If

            With sbAcademicYear
                .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
            End With
            db.ClearParameters()
            db.AddParameter("@AcademicYearDescrip", strAwardYearInput, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'With sbAcademicYear
            '    .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
            'End With
            'db.ClearParameters()
            'db.AddParameter("@AcademicYearDescrip", Mid(msgEntry.strAwdyr, 1, 4), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Try
                dr = db.RunParamSQLDataReader(sbAcademicYear.ToString())
                If Not (dr.HasRows) Then
                    Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
                    Exit Function
                End If
                While dr.Read()
                    If Not (dr("AcademicYearId") Is System.DBNull.Value) Or Not (dr("AcademicYearId").ToString = Guid.Empty.ToString) Then
                        strAwardYear = dr("AcademicYearId").ToString
                        Exit While
                    Else
                        Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
                        Exit Function
                    End If
                End While
            Catch ex As System.Exception
                Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
                Exit Function
            Finally
                dr.Close()
            End Try

            'If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
            '    strLoanBeginDate = "07/01/" + Mid(msgEntry.strAwdyr, 1, 4)
            '    strLoanEndDate = "06/30/" + Mid(msgEntry.strAwdyr, 6, 2)
            'Else
            '    strLoanBeginDate = msgEntry.dateDate1
            '    strLoanEndDate = msgEntry.dateDate2
            'End If

            Dim intCutOffDates As Integer
            Dim dtCuttOfDate As Date = CDate(MyAdvAppSettings.AppSettings("AwardsCutOffDate").ToString)
            Dim dtAwardStartDate As Date = CDate(strLoanBeginDate)
            intCutOffDates = DateDiff(DateInterval.Day, dtCuttOfDate, dtAwardStartDate)
            If intCutOffDates < 0 Then
                Return "The award start date " & dtAwardStartDate.ToString & " is earlier than the award cut off date (" & MyAdvAppSettings.AppSettings("AwardsCutOffDate").ToString & "), so student award will not be created for " & strStudentName
                Exit Function
            End If

            'Checks to see if the school has put in an award for the same fund and award year
            'but for different gross amount
            'example: an award may be created by school for $4500 for Direct Loan 05-06
            'but from Fame ESP we may get a award data for $3500 and $1000 for same year
            'and this case we need to send the data($3500 and $1000) from Fame ESP to exception
            'while checking ignore Gross Amount and look for records with null FAID and moduser
            'not equal to famelink which identifies that record was put in by school
            'Mantis # 12077.
            'If Not Trim(SourceToTarget) = "exceptiontotarget" Then
            '    With sbCheckDuplicateAwardsBySchool
            '        .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
            '        .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
            '        If strAwardYear.Length >= 30 Then
            '            .Append(" and AcademicYearId = ? ")
            '        End If
            '        ' .Append(" and FA_ID is NULL ")
            '        If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
            '            .Append(" and AwardStartDate=? ")
            '            .Append(" and AwardEndDate=? ")
            '        End If
            '        .Append(" and ModUser <> 'famelink' ")
            '    End With
            '    db.ClearParameters()
            '    db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
            '        db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            '        db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            '    End If

            '    Try
            '        intDuplicatesBySchool = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchool.ToString)
            '        If intDuplicatesBySchool >= 1 Then
            '            Return "Unable to create student award for student " & strStudentName & " as the school has already created an award with a different gross amount for same fund source and award year"
            '            Exit Function
            '        Else
            '            intDuplicatesBySchool = 0
            '        End If
            '    Catch ex As System.Exception
            '        intDuplicatesBySchool = 0
            '    End Try

            '    'Use Case: School would have created a student award with same amount but for a different loan period
            '    'Example:Student Thomas Piehko Award Created for $3500.00
            '    'School Created an award for 11/15

            '    With sbCheckDuplicateAwardsBySchoolDiffLoanPeriods
            '        .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
            '        .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
            '        If strAwardYear.Length >= 30 Then
            '            .Append(" and AcademicYearId = ? ")
            '        End If
            '        '.Append(" and FA_ID is NULL ")
            '        .Append(" and ModUser <> 'famelink' ")
            '    End With
            '    db.ClearParameters()
            '    db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '    Try
            '        intDuplicatesBySchoolDiffLoanPeriods = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchoolDiffLoanPeriods.ToString)
            '        If intDuplicatesBySchoolDiffLoanPeriods >= 1 Then
            '            Return "Unable to create student award for student " & strStudentName & " as the school has already created an award with a same gross amount for same fund source and award year, but for a different award period"
            '            Exit Function
            '        Else
            '            intDuplicatesBySchoolDiffLoanPeriods = 0
            '        End If
            '    Catch ex As System.Exception
            '        intDuplicatesBySchoolDiffLoanPeriods = 0
            '    End Try
            'End If

            'Check for duplicates before executing
            With sbCheckDuplicateAwards
                .Append(" Select Count(*) from faStudentAwards where ")
                .Append(" fa_id=?  ")
                'modified by balaji on 10/10/2007 as we are updating the moduser to "famelink" when
                'matching student record is found and fa_id is updated.looking it up based on moduser 
                'may lead to new issues, so moduser has been commented.
                '.Append(" and moduser <> 'famelink' ")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@fa_id", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                intDuplicates = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
                If intDuplicates >= 1 Then
                    intDuplicates = 1
                Else
                    intDuplicates = 0
                End If
            Catch ex As System.Exception
                intDuplicates = 0
            End Try


            'If there are no duplicates create a student award
            If intDuplicates >= 1 Then
                'Added by balaji on 10/10/2007 
                'get the message to find out if the issue has to do with 
                'diff gross amount - if so just update the gross amount
                'diff loan period - if so just update the award start date and award end date
                Dim sbGetMessage As New StringBuilder
                Dim intFindMessagePosition As Integer = 0
                Dim strGetMessage As String = ""
                With sbGetMessage
                    .Append("Select Top 1 Message from syFameESPExceptionReport ")
                    .Append(" where FAID=? ")
                End With
                db.ClearParameters()
                db.AddParameter("@FAID", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    strGetMessage = CType(db.RunParamSQLScalar(sbGetMessage.ToString), String)
                    If Trim(strGetMessage) = "" Then
                        Exit Try
                    Else
                        If InStr(strGetMessage, "gross") >= 1 Then
                            intFindMessagePosition = 2 'for different gross amount
                        ElseIf InStr(strGetMessage, "loan") >= 1 Then
                            intFindMessagePosition = 3 'for different loan period
                        Else
                            intFindMessagePosition = 0
                        End If
                    End If
                Catch ex As Exception
                    intFindMessagePosition = 0
                End Try
                With sbStudentAwards
                    .Append(" Update faStudentAwards set ")
                    If intFindMessagePosition = 2 Then     'different gross amount
                        .Append(" GrossAmount=?, ")
                    ElseIf intFindMessagePosition = 3 Then 'different loan period
                        .Append(" AwardStartDate=?,AwardEndDate=?, ")
                    Else
                        .Append(" GrossAmount=?,AwardStartDate=?,AwardEndDate=?, ")
                    End If
                    .Append(" moddate=? ")
                    .Append(" where FA_ID=? ")
                End With

                'Add params
                db.ClearParameters()
                If intFindMessagePosition = 2 Then     'different gross amount
                    db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                ElseIf intFindMessagePosition = 3 Then 'different loan period
                    db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@fa_id", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                '   execute the query
                Try
                    db.RunParamSQLExecuteNoneQuery(sbStudentAwards.ToString)
                    Return ""
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to override student awards for student " & strStudentName
                    Exit Function
                End Try
            Else
                With sbStudentAwards
                    .Append("INSERT INTO faStudentAwards(StudentAwardID,StuEnrollID,AwardTypeID,AcademicYearId,fa_id,GrossAmount,AwardStartDate,AwardEndDate,LoanFees,moduser,moddate,Disbursements,LoanID) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardTypeID", AwardTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@fa_id", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                    db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                Else
                    If msgEntry.decAmount2.ToString.Length >= 1 And Mid(msgEntry.decAmount2.ToString, 1) <> "0" Then
                        db.AddParameter("@LoanFees", msgEntry.decAmount1 - msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    End If
                End If
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Disbursements", Disbursements, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@LoanID", LoanID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                '   execute the query
                Try
                    db.RunParamSQLExecuteNoneQuery(sbStudentAwards.ToString)
                    Return ""
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to create student awards for student " & strStudentName
                    'db.CloseConnection()
                    Exit Function
                End Try
            End If
        Catch ex As System.Exception
            Return "Unable to override student awards for student " & strStudentName
            Exit Function
        Finally
        End Try
        'Return Add_faStudentAwards
    End Function
    Public Function Update_faStudentAwards(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef StudentAwardID As String, ByVal StuEnrollID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal strStudentName As String = "", Optional ByVal SourceToTarget As String = "") As String
        Dim sbStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbUpdateStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchool As New System.Text.StringBuilder(1000)
        Dim sbUpdateStudentAwards1 As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchoolDiffLoanPeriods As New System.Text.StringBuilder(1000)
        Dim AwardTypeID As String = " "
        Dim getAwardTypeMessage As String = ""
        Dim Disbursements As Integer = 0
        Dim LoanID As String = " "
        Dim intDuplicates As Integer = 0
        Dim intDuplicatesBySchool As Integer = 0
        Dim intDuplicatesBySchoolDiffLoanPeriods As Integer = 0

        Dim sbAcademicYear As New StringBuilder
        Dim strAwardYear As String
        Dim strLoanBeginDate, strLoanEndDate As String
        Dim dr As OleDbDataReader
        m_ExceptionMessage = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'db.OpenConnection()

        If String.IsNullOrEmpty(StuEnrollID) Or StuEnrollID = Guid.Empty.ToString Then
            Return "Unable to find enrollment information for the student" & strStudentName
            'db.CloseConnection()
            Exit Function
        End If

        If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
            strLoanBeginDate = "07/01/" + Mid(msgEntry.strAwdyr, 1, 4)
            strLoanEndDate = "06/30/" + Mid(msgEntry.strAwdyr, 6, 2)
        Else
            strLoanBeginDate = msgEntry.dateDate1
            strLoanEndDate = msgEntry.dateDate2
        End If


        'Look for matching records
        StudentAwardID = GetMatchingStudentAwardsForUpdate(db, StuEnrollID, msgEntry.strFAID, msgEntry.decAmount1, strLoanBeginDate, strLoanEndDate, strStudentName, msgEntry.strFund)
        If Not StudentAwardID = "" Then
            'Update existing record with this FAID
            With sbUpdateStudentAwards
                .Append("Update faStudentAwards set FA_Id=?,moduser=?,moddate=? where StudentAwardId=? ")
            End With
            db.ClearParameters()
            db.AddParameter("@FA_ID", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moduser", "famelink", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@StudentAwardId", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Try
                db.RunParamSQLExecuteNoneQuery(sbUpdateStudentAwards.ToString)
            Catch ex As System.Exception
                Return "Unable to match/update FAID with student awards for student " & strStudentName
                'db.CloseConnection()
                Exit Function
            End Try
        Else
            StudentAwardID = Guid.NewGuid.ToString
        End If

        'Before Creating new awards check to see if the Award Start Date falls after the AwardsCuttOffDate
        'in web.config
        Try
            getAwardTypeMessage = GetAwardTypeID(db, AwardTypeID, msgEntry.strFund, msgEntry.strYYYY)
            If Not getAwardTypeMessage = "" Then
                Return "Unable to add student awards for student " & strStudentName & " as the provided award type was not found"
                'db.CloseConnection()
                Exit Function
            End If

            Dim strAwardYearInput As String
            Dim strFaidCount As Integer = msgEntry.strFAID.Length
            If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                strAwardYearInput = Mid(msgEntry.strFAID, strFaidCount, 1)
                strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
                'strAwardYearInput = "200" & (strAwardYearInput - 1)
            Else
                strAwardYearInput = Mid(msgEntry.strFAID, 11, 1)
                strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
                'strAwardYearInput = "200" & (strAwardYearInput - 1)
            End If
            If strAwardYearInput = "" Then
                Return "Unable to match the award year from Fame ESP with Advantage Award Year for student " & strStudentName
                Exit Function
            End If

            With sbAcademicYear
                .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
            End With
            db.ClearParameters()
            db.AddParameter("@AcademicYearDescrip", strAwardYearInput, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'With sbAcademicYear
            '    .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
            'End With
            'db.ClearParameters()
            'db.AddParameter("@AcademicYearDescrip", Mid(msgEntry.strAwdyr, 1, 4), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'Try
            '    strAwardYear = CType(db.RunParamSQLScalar(sbAcademicYear.ToString), Guid).ToString
            'Catch ex As System.Exception
            '    strAwardYear = System.DBNull.Value.ToString
            'End Try
            Try
                dr = db.RunParamSQLDataReader(sbAcademicYear.ToString())
                If Not (dr.HasRows) Then
                    Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
                    Exit Function
                End If
                While dr.Read()
                    If Not (dr("AcademicYearId") Is System.DBNull.Value) Or Not (dr("AcademicYearId").ToString = Guid.Empty.ToString) Then
                        strAwardYear = dr("AcademicYearId").ToString
                    Else
                        Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
                        Exit Function
                    End If
                End While
            Catch ex As System.Exception
                Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
                Exit Function
            Finally
                dr.Close()
            End Try


            If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                strLoanBeginDate = "07/01/" + Mid(msgEntry.strAwdyr, 1, 4)
                strLoanEndDate = "06/30/" + Mid(msgEntry.strAwdyr, 6, 2)
            Else
                strLoanBeginDate = msgEntry.dateDate1
                strLoanEndDate = msgEntry.dateDate2
            End If

            Dim intCutOffDates As Integer
            Dim dtCuttOfDate As Date = CDate(MyAdvAppSettings.AppSettings("AwardsCutOffDate").ToString)
            Dim dtAwardStartDate As Date = CDate(strLoanBeginDate)
            intCutOffDates = DateDiff(DateInterval.Day, dtCuttOfDate, dtAwardStartDate)
            If intCutOffDates < 0 Then
                Return "The award start date " & dtAwardStartDate.ToString & " is earlier than the award cut off date (" & MyAdvAppSettings.AppSettings("AwardsCutOffDate").ToString & "), so student award will not be created for " & strStudentName
                ' db.CloseConnection()
                Exit Function
            End If

            'Checks to see if the school has put in an award for the same fund and award year
            'but for different gross amount
            'example: an award may be created by school for $4500 for Direct Loan 05-06
            'but from Fame ESP we may get a award data for $3500 and $1000 for same year
            'and this case we need to send the data($3500 and $1000) from Fame ESP to exception
            'while checking ignore Gross Amount and look for records with null FAID and moduser
            'not equal to famelink which identifies that record was put in by school
            'Mantis # 12077.
            'If Not Trim(SourceToTarget) = "exceptiontotarget" Then
            '    With sbCheckDuplicateAwardsBySchool
            '        .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
            '        .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
            '        If strAwardYear.Length >= 30 Then
            '            .Append(" and AcademicYearId = ? ")
            '        End If
            '        '.Append(" and FA_ID is NULL ")
            '        If msgEntry.strFund = "06" Or msgEntry.strFund = "07" Or msgEntry.strFund = "08" Then
            '            .Append(" and AwardStartDate=? ")
            '            .Append(" and AwardEndDate=? ")
            '        End If
            '        .Append(" and ModUser <> 'famelink' ")
            '    End With
            '    db.ClearParameters()
            '    db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            '    db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '    Try
            '        intDuplicatesBySchool = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchool.ToString)
            '        If intDuplicatesBySchool >= 1 Then
            '            Return "Unable to create student award for student " & strStudentName & " as the school has already created an award with a different gross amount for same fund source and award year"
            '            Exit Function
            '        Else
            '            intDuplicatesBySchool = 0
            '        End If
            '    Catch ex As System.Exception
            '        intDuplicatesBySchool = 0
            '    End Try

            '    'Use Case: School would have created a student award with same amount but for a different loan period
            '    'Example:Student Thomas Piehko Award Created for $3500.00
            '    'School Created an award for 11/14/ 2007 but ESP created it for 11/15/2007

            '    With sbCheckDuplicateAwardsBySchoolDiffLoanPeriods
            '        .Append(" select Count(*) from faStudentAwards where StuEnrollId=? and ")
            '        .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
            '        If strAwardYear.Length >= 30 Then
            '            .Append(" and AcademicYearId = ? ")
            '        End If
            '        ' .Append(" and FA_ID is NULL ")
            '        .Append(" and ModUser <> 'famelink' ")
            '    End With
            '    db.ClearParameters()
            '    db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    db.AddParameter("@FundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '    db.AddParameter("@AwardTypeID", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '    Try
            '        intDuplicatesBySchoolDiffLoanPeriods = db.RunParamSQLScalar(sbCheckDuplicateAwardsBySchoolDiffLoanPeriods.ToString)
            '        If intDuplicatesBySchoolDiffLoanPeriods >= 1 Then
            '            Return "Unable to create student award for student " & strStudentName & " as the school has already created an award with a same gross amount for same fund source and award year, but for a different award period"
            '            Exit Function
            '        Else
            '            intDuplicatesBySchoolDiffLoanPeriods = 0
            '        End If
            '    Catch ex As System.Exception
            '        intDuplicatesBySchoolDiffLoanPeriods = 0
            '    End Try
            'End If


            'Check for duplicates before executing
            With sbCheckDuplicateAwards
                .Append(" Select Count(*) from faStudentAwards where ")
                .Append(" fa_id=?  ")
                'See the section for Update_faStudentAwardsfromFAID function for the additional information
                '.Append(" and moduser <> 'famelink' ")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@fa_id", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


            Try
                intDuplicates = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
                If intDuplicates >= 1 Then
                    intDuplicates = 1
                Else
                    intDuplicates = 0
                End If
            Catch ex As System.Exception
                intDuplicates = 0
            End Try


            'If there are no duplicates create a student award
            If intDuplicates >= 1 Then
                'Added by balaji on 10/10/2007 
                'get the message to find out if the issue has to do with 
                'diff gross amount - if so just update the gross amount
                'diff loan period - if so just update the award start date and award end date
                Dim sbGetMessage As New StringBuilder
                Dim intFindMessagePosition As Integer = 0
                Dim strGetMessage As String = ""
                With sbGetMessage
                    .Append("Select Top 1 Message from syFameESPExceptionReport ")
                    .Append(" where FAID=? ")
                End With
                db.ClearParameters()
                db.AddParameter("@FAID", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    strGetMessage = CType(db.RunParamSQLScalar(sbGetMessage.ToString), String)
                    If Trim(strGetMessage) = "" Then
                        Exit Try
                    Else
                        If InStr(strGetMessage, "gross") >= 1 Then
                            intFindMessagePosition = 2 'for different gross amount
                        ElseIf InStr(strGetMessage, "loan") >= 1 Then
                            intFindMessagePosition = 3 'for different loan period
                        Else
                            intFindMessagePosition = 0
                        End If
                    End If
                Catch ex As Exception
                    intFindMessagePosition = 0
                End Try
                With sbStudentAwards
                    .Append(" Update faStudentAwards Set  ")
                    If intFindMessagePosition = 2 Then     'different gross amount
                        .Append(" GrossAmount=?,LoanFees=?, ")
                    ElseIf intFindMessagePosition = 3 Then 'different loan period
                        .Append(" AwardStartDate=?,AwardEndDate=?, ")
                    Else
                        .Append(" GrossAmount=?,LoanFees=?,AwardStartDate=?,AwardEndDate=?, ")
                    End If
                    .Append(" moddate=?, ")
                    '.Append(" Disbursements=?,LoanID=?  ")
                    .Append(" where FA_ID=? ")
                End With

                'Add params
                db.ClearParameters()
                If intFindMessagePosition = 2 Then     'different gross amount
                    db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                        db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    Else
                        If msgEntry.decAmount2.ToString.Length >= 1 And Mid(msgEntry.decAmount2.ToString, 1) <> "0" Then
                            db.AddParameter("@LoanFees", msgEntry.decAmount1 - msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                        Else
                            db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                        End If
                    End If
                ElseIf intFindMessagePosition = 3 Then 'different loan period
                    db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                        db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    Else
                        If msgEntry.decAmount2.ToString.Length >= 1 And Mid(msgEntry.decAmount2.ToString, 1) <> "0" Then
                            db.AddParameter("@LoanFees", msgEntry.decAmount1 - msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                        Else
                            db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                        End If
                    End If
                    db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                'db.AddParameter("@Disbursements", Disbursements, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                'db.AddParameter("@LoanID", LoanID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@fa_id", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    db.RunParamSQLExecuteNoneQuery(sbStudentAwards.ToString)
                    Return ""
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to override student awards for student " & strStudentName
                    'db.CloseConnection()
                    Exit Function
                End Try
            Else
                With sbStudentAwards
                    .Append("INSERT INTO faStudentAwards(StudentAwardID,StuEnrollID,AwardTypeID,AcademicYearId,fa_id,GrossAmount,AwardStartDate,AwardEndDate,LoanFees,moduser,moddate,Disbursements,LoanID) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardTypeID", AwardTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@fa_id", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                    db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                Else
                    If msgEntry.decAmount2.ToString.Length >= 1 And Mid(msgEntry.decAmount2.ToString, 1) <> "0" Then
                        db.AddParameter("@LoanFees", msgEntry.decAmount1 - msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                        'db.AddParameter("@LoanFees", msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    End If
                End If
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Disbursements", Disbursements, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@LoanID", LoanID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                '   execute the query
                Try
                    db.RunParamSQLExecuteNoneQuery(sbStudentAwards.ToString)
                    Return ""
                    'db.CloseConnection()
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to create student awards for student " & strStudentName
                    'db.CloseConnection()
                    Exit Function
                End Try
            End If
        Finally
        End Try
        'Return Add_faStudentAwards
    End Function
    Public Function Reprocess_Update_faStudentAwards(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, _
                                                     ByRef StudentAwardID As String, ByVal StuEnrollID As String, _
                                                     ByVal msgEntry As FLMessageInfo, Optional ByVal strStudentName As String = "", _
                                                     Optional ByVal SourceToTarget As String = "", _
                                                     Optional ByVal strSSN As String = "", _
                                                     Optional ByVal strFAID As String = "", _
                                                     Optional ByVal strFund As String = "", _
                                                     Optional ByVal decGrossAmount As Decimal = 0, _
                                                     Optional ByVal decLoanFees As Decimal = 0, _
                                                     Optional ByVal strAwardYear As String = "", _
                                                     Optional ByVal dtDate1 As Date = #1/1/1900#, _
                                                     Optional ByVal dtDate2 As Date = #1/1/1900#) As String
        Dim sbStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbUpdateStudentAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwards As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchool As New System.Text.StringBuilder(1000)
        Dim sbUpdateStudentAwards1 As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwardsBySchoolDiffLoanPeriods As New System.Text.StringBuilder(1000)
        Dim AwardTypeID As String = " "
        Dim getAwardTypeMessage As String = ""
        Dim Disbursements As Integer = 0
        Dim LoanID As String = " "
        Dim intDuplicates As Integer = 0
        Dim intDuplicatesBySchool As Integer = 0
        Dim intDuplicatesBySchoolDiffLoanPeriods As Integer = 0

        Dim sbAcademicYear As New StringBuilder
        Dim strLoanBeginDate, strLoanEndDate As String
        Dim dr As OleDbDataReader
        m_ExceptionMessage = ""

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'db.OpenConnection()

        If String.IsNullOrEmpty(StuEnrollID) Or StuEnrollID = Guid.Empty.ToString Then
            Return "Unable to find enrollment information for the student" & strStudentName
            'db.CloseConnection()
            Exit Function
        End If

        'If strFund = "02" Or strFund = "03" Or strFund = "04" Then
        '    strLoanBeginDate = "07/01/" + Mid(strAwardYear, 1, 4)
        '    strLoanEndDate = "06/30/" + Mid(strAwardYear, 6, 2)
        'Else
        '    strLoanBeginDate = CDate(dtDate1)
        '    strLoanEndDate = CDate(dtDate2)
        'End If
        If Not Year(dtDate1) = 1900 Then
            strLoanBeginDate = dtDate1
        End If
        If Not Year(dtDate2) = 1900 Then
            strLoanEndDate = dtDate2
        End If
        'Look for matching records
        'StudentAwardID = GetMatchingStudentAwardsForUpdate(db, StuEnrollID, strFAID, decGrossAmount, strLoanBeginDate, strLoanEndDate, strStudentName, strFund)
        StudentAwardID = GetMatchingStudentAwardsForUpdate_Reprocess(db, StuEnrollID, strFAID, decGrossAmount, strLoanBeginDate, strLoanEndDate, strStudentName, strFund)
        If Not StudentAwardID = "" Then
            'Update existing record with this FAID
            With sbUpdateStudentAwards
                .Append("Update faStudentAwards set FA_Id=?,moduser=?,moddate=? where StudentAwardId=? ")
            End With
            db.ClearParameters()
            db.AddParameter("@FA_ID", strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moduser", "famelink", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@StudentAwardId", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Try
                db.RunParamSQLExecuteNoneQuery(sbUpdateStudentAwards.ToString)
            Catch ex As System.Exception
                Return "Unable to match/update FAID with student awards for student " & strStudentName
                'db.CloseConnection()
                Exit Function
            End Try
        Else
            StudentAwardID = Guid.NewGuid.ToString
        End If

        'Before Creating new awards check to see if the Award Start Date falls after the AwardsCuttOffDate
        'in web.config
        Try
            getAwardTypeMessage = GetAwardTypeID(db, AwardTypeID, strFund, msgEntry.strYYYY)
            If Not getAwardTypeMessage = "" Then
                Return "Unable to add student awards for student " & strStudentName & " as the provided award type was not found"
                'db.CloseConnection()
                Exit Function
            End If

            Dim strAwardYearInput As String
            Dim strFaidCount As Integer = strFAID.Length
            If strFund = "02" Or strFund = "03" Or strFund = "04" Then
                strAwardYearInput = Mid(strFAID, strFaidCount, 1)
                strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
                'strAwardYearInput = "200" & (strAwardYearInput - 1)
            Else
                strAwardYearInput = Mid(strFAID, 11, 1)
                strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
                'strAwardYearInput = "200" & (strAwardYearInput - 1)
            End If
            If strAwardYearInput = "" Then
                Return "Unable to match the award year from Fame ESP with Advantage Award Year for student " & strStudentName
                Exit Function
            End If

            With sbAcademicYear
                .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
            End With
            db.ClearParameters()
            db.AddParameter("@AcademicYearDescrip", strAwardYearInput, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'With sbAcademicYear
            '    .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
            'End With
            'db.ClearParameters()
            'db.AddParameter("@AcademicYearDescrip", Mid(msgEntry.strAwdyr, 1, 4), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'Try
            '    strAwardYear = CType(db.RunParamSQLScalar(sbAcademicYear.ToString), Guid).ToString
            'Catch ex As System.Exception
            '    strAwardYear = System.DBNull.Value.ToString
            'End Try
            Try
                dr = db.RunParamSQLDataReader(sbAcademicYear.ToString())
                If Not (dr.HasRows) Then
                    Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
                    Exit Function
                End If
                While dr.Read()
                    If Not (dr("AcademicYearId") Is System.DBNull.Value) Or Not (dr("AcademicYearId").ToString = Guid.Empty.ToString) Then
                        strAwardYear = dr("AcademicYearId").ToString
                    Else
                        Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
                        Exit Function
                    End If
                End While
            Catch ex As System.Exception
                Return "Unable to create student award as the award year was not found in the database for student " & strStudentName
                Exit Function
            Finally
                dr.Close()
            End Try


            Dim intCutOffDates As Integer
            Dim dtCuttOfDate As Date = CDate(MyAdvAppSettings.AppSettings("AwardsCutOffDate").ToString)
            Dim dtAwardStartDate As Date = CDate(strLoanBeginDate)
            intCutOffDates = DateDiff(DateInterval.Day, dtCuttOfDate, dtAwardStartDate)
            If intCutOffDates < 0 Then
                Return "The award start date " & dtAwardStartDate.ToString & " is earlier than the award cut off date (" & MyAdvAppSettings.AppSettings("AwardsCutOffDate").ToString & "), so student award will not be created for " & strStudentName
                ' db.CloseConnection()
                Exit Function
            End If


            'Check for duplicates before executing
            With sbCheckDuplicateAwards
                .Append(" Select Count(*) from faStudentAwards where ")
                .Append(" fa_id=?  ")
                .Append(" and GrossAmount=? ")
                'See the section for Update_faStudentAwardsfromFAID function for the additional information
                '.Append(" and moduser <> 'famelink' ")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@fa_id", strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@GrossAmount", decGrossAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            Try
                intDuplicates = db.RunParamSQLScalar(sbCheckDuplicateAwards.ToString)
                If intDuplicates >= 1 Then
                    intDuplicates = 1
                Else
                    intDuplicates = 0
                End If
            Catch ex As System.Exception
                intDuplicates = 0
            End Try


            'If there are no duplicates create a student award
            If intDuplicates = 0 Then
                With sbStudentAwards
                    .Append("INSERT INTO faStudentAwards(StudentAwardID,StuEnrollID,AwardTypeID,AcademicYearId,fa_id,GrossAmount,AwardStartDate,AwardEndDate,LoanFees,moduser,moddate,Disbursements,LoanID) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardTypeID", AwardTypeID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@fa_id", strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@GrossAmount", decGrossAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@AwardStartDate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@AwardEndDate", strLoanEndDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If strFund = "02" Or strFund = "03" Or strFund = "04" Then
                    db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                Else
                    If msgEntry.decAmount2.ToString.Length >= 1 And Mid(msgEntry.decAmount2.ToString, 1) <> "0" Then
                        db.AddParameter("@LoanFees", decGrossAmount - decLoanFees, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@LoanFees", "0.00", DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    End If
                End If
                db.AddParameter("@moduser", "famelink", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", Date.Now.ToShortDateString, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@Disbursements", Disbursements, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@LoanID", LoanID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                '   execute the query
                Try
                    db.RunParamSQLExecuteNoneQuery(sbStudentAwards.ToString)
                    Return ""
                    'db.CloseConnection()
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to create student awards for student " & strStudentName
                    'db.CloseConnection()
                    Exit Function
                End Try
            End If
        Finally
        End Try
        'Return Add_faStudentAwards
    End Function
    Public Function UpdateAndOverride_faAwardSchedule(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef AwardScheduleID As String, ByVal StudentAwardID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal OverrideUpdateMessage As String = "") As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbCheckDuplicateAwards As New StringBuilder
        Dim sbCheckDuplicateAwardsNoDate As New StringBuilder
        Dim sbAwardScheduleNoDate As New StringBuilder
        Dim intDuplicatesNoDate As Integer = 0
        Dim intDuplicates As Integer = 0
        m_ExceptionMessage = ""
        'Add_faAwardSchedule = False
        If String.IsNullOrEmpty(StudentAwardID) Then
            Return "Unable to add award schedule as student award for student " & StudentName & " cannot be empty"
            Exit Function
        End If

        AwardScheduleID = Guid.NewGuid.ToString
        Try
            Try
                intDuplicates = GetRecordCountByAwardIdDateAmountAndReferenceNULL(StudentAwardID, msgEntry.decAmount1, msgEntry.dateDate1)
                If intDuplicates >= 1 Then
                    intDuplicates = 1
                    Dim dtDateinFameESP As Date = "01/01/1900"
                    Dim dtDateinAdvantage As Date = "01/01/1900"
                    Dim strDISBMessage As String = ""
                    Dim strAwardScheduleId As String = ""
                    Dim strReturnUpdateMessage As String = ""
                    Dim sbgetAwardScheduleId As New StringBuilder
                    strAwardScheduleId = GetAwardScheduleIdByAwardIdDateAmountAndReferenceNULL(StudentAwardID, msgEntry.decAmount1, msgEntry.dateDate1)

                    If Not strAwardScheduleId = "" Then
                        If dtDateinFameESP = "01/01/1900" Then
                            strReturnUpdateMessage = UpdateAwardSchedule(db, AwardScheduleID, StudentAwardID, msgEntry.dateDate1, msgEntry.decAmount1, msgEntry.dateModDate, StudentName)
                        Else
                            strReturnUpdateMessage = UpdateAwardSchedule(db, AwardScheduleID, StudentAwardID, dtDateinFameESP, msgEntry.decAmount1, msgEntry.dateModDate, StudentName)
                        End If

                        Try
                            Return strReturnUpdateMessage
                            Exit Function
                        Catch ex As System.Exception
                            Return "Unable to update schedules for the selected student award for student" & StudentName
                            Exit Function
                        End Try
                    Else
                        Return "Unable to update schedules for the selected student award for student" & StudentName
                        Exit Function
                    End If
                End If
            Catch ex As System.Exception
                intDuplicates = 0
            End Try

            'If Not Trim(SourceToTarget) = "exceptiontotarget" Then
            With sbCheckDuplicateAwardsNoDate
                .Append(" Select Count(*) from faStudentAwardSchedule  ")
                .Append(" WHERE StudentAwardID=? and Amount=?  ")
                '.Append(" and (moduser <> 'famelink' or moduser is NULL) ")
                .Append(" and (Reference is NULL or Reference='') ")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@studentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Try
                intDuplicatesNoDate = db.RunParamSQLScalar(sbCheckDuplicateAwardsNoDate.ToString)
                If intDuplicatesNoDate >= 1 Then
                    intDuplicatesNoDate = 1
                    Dim dtDateinFameESP As Date = "01/01/1900"
                    Dim dtDateinAdvantage As Date = "01/01/1900"
                    Dim strDISBMessage As String = ""
                    Dim strAwardScheduleId As String = ""
                    If Not OverrideUpdateMessage = "" Then
                        Dim intESPPos As Integer = InStr(OverrideUpdateMessage, ":") ' look for ESP:
                        Dim intAdvPos As Integer = InStrRev(OverrideUpdateMessage, ":")
                        If intESPPos > 1 Then
                            strDISBMessage = Mid(OverrideUpdateMessage, intESPPos + 2, 10)
                            strDISBMessage = strDISBMessage.Replace(".", "")
                            Try
                                dtDateinFameESP = CDate(Trim(strDISBMessage))
                            Catch ex17 As System.Exception
                                dtDateinFameESP = "01/01/1900"
                            End Try
                        End If

                        If intAdvPos > 1 Then
                            strDISBMessage = Mid(OverrideUpdateMessage, intAdvPos + 2, 10)
                            strDISBMessage = strDISBMessage.Replace(".", "")
                            Try
                                dtDateinAdvantage = CDate(Trim(strDISBMessage))
                            Catch ex18 As System.Exception
                                dtDateinAdvantage = "01/01/1900"
                            End Try
                        End If
                    End If
                    Dim sbgetAwardScheduleId As New StringBuilder
                    Try
                        If dtDateinFameESP = "01/01/1900" Then
                            strAwardScheduleId = GetAwardScheduleIdByAwardIdDateAmountAndReferenceNULL(StudentAwardID, msgEntry.decAmount1, msgEntry.dateDate1)
                        Else
                            strAwardScheduleId = GetAwardScheduleIdByAwardIdDateAmountAndReferenceNULL(StudentAwardID, msgEntry.decAmount1, dtDateinAdvantage)
                        End If
                    Catch ex As System.Exception
                        strAwardScheduleId = ""
                    End Try
                    If Not strAwardScheduleId = "" Then
                        Dim strUpdateMessage As String = ""
                        Try
                            If dtDateinFameESP = "01/01/1900" Then
                                strUpdateMessage = UpdateAwardSchedule(db, strAwardScheduleId, StudentAwardID, msgEntry.dateDate1, msgEntry.decAmount1, msgEntry.dateModDate, StudentName)
                            Else
                                strUpdateMessage = UpdateAwardSchedule(db, strAwardScheduleId, StudentAwardID, dtDateinFameESP, msgEntry.decAmount1, msgEntry.dateModDate, StudentName)
                            End If
                            Return strUpdateMessage
                            Exit Function
                        Catch ex As System.Exception
                            Return "Unable to update schedules for the selected student award for student" & StudentName
                            Exit Function
                        End Try
                    Else
                        Return "Unable to find a matching disbursement record for the selected student award,date and amount for student " & StudentName
                        Exit Function
                    End If
                Else
                    Return "Unable to update disbursement as either scheduled date/amount does not match or payment has already been posted for student " & StudentName
                    Exit Function
                End If
            Catch ex As System.Exception
                intDuplicatesNoDate = 0
            End Try
            'End If
        Finally
        End Try
    End Function
    Public Function Update_saTransaction(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef TransactionID As String, ByVal StuEnrollID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "", Optional ByVal SourceToTarget As String = "") As String
        Dim sbTransaction As New System.Text.StringBuilder(1000)
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(StuEnrollID) Then
            Return "Unable to match financial aid data with student " & StudentName & " enrollment information"
            Exit Function
        End If
        TransactionID = Guid.NewGuid.ToString

        'Update the Scheduled Disbursement Date with Actual Disbursement Date
        Dim sbUpdateDisbDate As New StringBuilder
        Dim sbGetTransactionIdUpdate As New StringBuilder
        Dim strReference As String
        Dim strFundDescription As String
        Dim sbFundDescrip As New StringBuilder
        Dim strTransDescrip As String

        Dim sbAcademicYear As New StringBuilder
        Dim strAwardYear As String
        Dim strAwardYearInput As String
        Dim strLoanBeginDate, strLoanEndDate As String
        Dim strFaidCount As Integer = msgEntry.strFAID.Length
        'If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
        '    strAwardYearInput = Mid(msgEntry.strFAID, strFaidCount, 1)
        '    strAwardYearInput = "200" & (strAwardYearInput - 1)
        'Else
        '    strAwardYearInput = Mid(msgEntry.strFAID, 11, 1)
        '    strAwardYearInput = "200" & (strAwardYearInput - 1)
        'End If
        If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
            strAwardYearInput = Mid(msgEntry.strFAID, strFaidCount, 1)
            strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
            'strAwardYearInput = "200" & (strAwardYearInput - 1)
        Else
            strAwardYearInput = Mid(msgEntry.strFAID, 11, 1)
            strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
            'strAwardYearInput = "200" & (strAwardYearInput - 1)
        End If
        If strAwardYearInput = "" Then
            Return "Unable to match the award year from Fame ESP with Advantage Award Year for student " & StudentName
            Exit Function
        End If
        With sbAcademicYear
            .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
        End With
        db.ClearParameters()
        db.AddParameter("@AcademicYearDescrip", strAwardYearInput, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strAwardYear = CType(db.RunParamSQLScalar(sbAcademicYear.ToString), Guid).ToString
        Catch ex As System.Exception
            strAwardYear = System.DBNull.Value.ToString
        End Try

        With sbFundDescrip
            .Append("select Top 1 FundSourceDescrip from saFundSources where AdvFundSourceId=?")
        End With
        db.ClearParameters()
        db.AddParameter("@AdvFundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        Try
            strFundDescription = db.RunParamSQLScalar(sbFundDescrip.ToString)
        Catch ex As System.Exception
            strFundDescription = ""
        End Try

        strTransDescrip = "Famelink"
        If Not strFundDescription = "" Then
            strTransDescrip &= "-" & strFundDescription
        End If

        'Get the Reference should be a combination of FameLink-FundSource-CheckNumber
        'FameLink-Perkins-1234
        strReference = "Famelink"
        If Not strFundDescription = "" Then
            strReference &= "-" & strFundDescription
        End If
        If Not msgEntry.strCheckNo = "" Then
            strReference &= "-" & Trim(msgEntry.strCheckNo)
        End If

        Try
            Dim TransTypeID As Integer = 2
            Dim IsPosted As Boolean = True
            Dim IsAutomatic As Boolean = False
            Select Case msgEntry.strMsgType
                Case "RCVD"
                    TransTypeID = 2
                Case "REFU"
                    TransTypeID = 1  'Adjustments
            End Select

            Dim intDuplicates As Integer = 0
            Dim sbDuplicates As New StringBuilder
            Dim sbgetTransactionId As New StringBuilder
            Dim strAmount As Decimal
            If TransTypeID = 2 Then
                strAmount = msgEntry.decAmount1 * (-1.0)
            Else
                strAmount = msgEntry.decAmount1
            End If

            'Go into this if condition only during initial import and not when file is reprocessed
            'if moduser <> 'famelink' means that data came from school and not from FameESP.
            'If Not Trim(SourceToTarget) = "exceptiontotarget" Then
            With sbDuplicates
                .Append("select Count(*) from saTransactions where StuEnrollId=? and TransDate=? and Transreference=? ")
                .Append(" and AcademicyearId=? and TransDescrip=? and TransAmount=? ")
            End With
            db.ClearParameters()
            db.AddParameter("@StuEnrollId", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TransDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@TransReference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@AcademicyearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TransDescrip", strTransDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TransAmount", strAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Try
                intDuplicates = db.RunParamSQLScalar(sbDuplicates.ToString)
                If intDuplicates >= 1 Then
                    With sbGetTransactionIdUpdate
                        .Append(" Select Top 1 TransactionId from saTransactions where StuEnrollId=? and TransDate=? and Transreference=? ")
                        .Append(" and AcademicyearId=? and TransDescrip=? and TransAmount=? ")
                    End With
                    db.ClearParameters()
                    db.AddParameter("@StuEnrollId", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@TransDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@TransReference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@AcademicyearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@TransDescrip", strTransDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@TransAmount", strAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    Try
                        TransactionID = CType(db.RunParamSQLScalar(sbGetTransactionIdUpdate.ToString), Guid).ToString
                    Catch ex As System.Exception
                    End Try

                    'Return "Unable to post payment as school has already posted payment for the disbursement for an amount of " & strAmount & " for student " & StudentName
                    'Exit Function
                    With sbTransaction
                        .Append(" Update saTransactions set TransDate=?,TransAmount=?,")
                        .Append(" CreateDate=?,TransReference=?,TransDescrip=?,moddate=?, ")
                        .Append(" TransTypeID=?,IsPosted=?,IsAutomatic=?,AcademicYearId=? ")
                        .Append(" where TransactionID=? ")
                    End With

                    'Add params
                    db.ClearParameters()
                    db.AddParameter("@TransDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    If TransTypeID = 2 Then
                        db.AddParameter("@TransAmount", msgEntry.decAmount1 * (-1.0), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@TransAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    End If
                    db.AddParameter("@CreateDate", msgEntry.dateDate2, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@TransReference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@TransDescrip", strTransDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@TransTypeID", TransTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    db.AddParameter("@IsPosted", IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                    db.AddParameter("@IsAutomatic", IsAutomatic, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                    db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    'execute the query
                    Try
                        db.RunParamSQLExecuteNoneQuery(sbTransaction.ToString)
                        Return ""
                        Exit Function
                    Catch ex As System.Exception
                        Return "Unable to update transaction for student " & StudentName
                        Exit Function
                    End Try
                Else
                    intDuplicates = 0
                    With sbTransaction
                        .Append("INSERT INTO saTransactions(TransactionID,StuEnrollID,TransDate,TransAmount,CreateDate,TransReference,TransDescrip,moduser,moddate,TransTypeID,IsPosted,IsAutomatic,AcademicYearId) ")
                        .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                    End With

                    'Add params
                    db.ClearParameters()
                    db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@TransDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    If TransTypeID = 2 Then
                        db.AddParameter("@TransAmount", msgEntry.decAmount1 * (-1.0), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@TransAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    End If
                    db.AddParameter("@CreateDate", msgEntry.dateDate2, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@TransReference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@TransDescrip", strTransDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@TransTypeID", TransTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    db.AddParameter("@IsPosted", IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                    db.AddParameter("@IsAutomatic", IsAutomatic, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                    db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    'execute the query
                    Try
                        db.RunParamSQLExecuteNoneQuery(sbTransaction.ToString)
                        Return ""
                        Exit Function
                    Catch ex As System.Exception
                        Return "Unable to add a new transaction for student " & StudentName
                        Exit Function
                    End Try
                End If
            Catch ex As System.Exception
                intDuplicates = 0
                With sbTransaction
                    .Append("INSERT INTO saTransactions(TransactionID,StuEnrollID,TransDate,TransAmount,CreateDate,TransReference,TransDescrip,moduser,moddate,TransTypeID,IsPosted,IsAutomatic,AcademicYearId) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@TransDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If TransTypeID = 2 Then
                    db.AddParameter("@TransAmount", msgEntry.decAmount1 * (-1.0), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                Else
                    db.AddParameter("@TransAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                End If
                db.AddParameter("@CreateDate", msgEntry.dateDate2, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransReference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@TransDescrip", strTransDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransTypeID", TransTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsPosted", IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@IsAutomatic", IsAutomatic, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'execute the query
                Try
                    db.RunParamSQLExecuteNoneQuery(sbTransaction.ToString)
                    Return ""
                    Exit Function
                Catch ex3 As System.Exception
                    Return "Unable to add a new transaction for student " & StudentName
                    Exit Function
                End Try
            End Try
            'End If

            ' groupTrans = myconn.BeginTransaction()
            'If intDuplicates = 0 Then

            'End If
            'Catch e As OleDbException
            '    m_ExceptionMessage = DALExceptions.BuildErrorMessage(e)
            '    msgEntry.strError = m_ExceptionMessage
            'Catch e As ArgumentNullException
            '    m_ExceptionMessage = e.Message
            'Catch e As System.Exception
            '    m_ExceptionMessage = e.Message
            '    Throw
        Finally
        End Try
    End Function
    Public Function Update_saPmtDisbRel(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef PmtDisbRelID As String, ByVal TransactionID As String, ByVal StuAwardID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "") As String
        Dim sbPmtDisbRel As New System.Text.StringBuilder(1000)
        Dim sbPmtDisbRel1 As New System.Text.StringBuilder(1000)
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(TransactionID) Then
            Return "Unable to update payment disbursement for student" & StudentName & " as provided transaction Id is not found"
            Exit Function
        End If
        If String.IsNullOrEmpty(StuAwardID) Then
            Return "Unable to update payment disbursement as student award is missing for student " & StudentName
            Exit Function
        End If
        PmtDisbRelID = Guid.NewGuid.ToString
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        Dim myconn As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString").ToString)
        myconn.Open()

        Try
            With sbPmtDisbRel
                .Append(" Select Top 1 PmtDisbRelId from saPmtDisbRel WHERE TransactionId=? and StuAwardId=? ")
            End With
            'Add params
            db.ClearParameters()
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Try
                PmtDisbRelID = CType(db.RunParamSQLScalar(sbPmtDisbRel.ToString), Guid).ToString
            Catch ex As System.Exception
                PmtDisbRelID = Guid.NewGuid.ToString
                Dim sbAwardScheduleId As New StringBuilder
                Dim strAwardScheduleValueId As String
                With sbAwardScheduleId
                    .Append(" select Top 1 AwardScheduleId from faStudentAwardSchedule where StudentAwardId=? and (Reference is Null or Reference='') and Amount=? order by ExpectedDate ")
                End With
                db.ClearParameters()
                db.AddParameter("@StudentAwardId", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

                Try
                    strAwardScheduleValueId = CType(db.RunParamSQLScalar(sbAwardScheduleId.ToString), Guid).ToString
                Catch ex2 As System.Exception
                    strAwardScheduleValueId = ""
                End Try
                sbAwardScheduleId.Remove(0, sbAwardScheduleId.Length)


                With sbPmtDisbRel1
                    .Append("INSERT INTO saPmtDisbRel(PmtDisbRelID,TransactionID,Amount,StuAwardID,AwardScheduleId,moduser,moddate) ")
                    .Append("VALUES(?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@PmtDisbRelID", PmtDisbRelID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If Not strAwardScheduleValueId = "" Then
                    db.AddParameter("@AwardScheduleId", strAwardScheduleValueId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@AwardScheduleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Try
                    db.RunParamSQLExecuteNoneQuery(sbPmtDisbRel1.ToString)
                    Return ""
                    Exit Function
                Catch ex3 As System.Exception
                    Return "Unable to add payment disbursement information for student " & StudentName
                    Exit Function
                End Try
            End Try
            sbPmtDisbRel.Remove(0, sbPmtDisbRel.Length)

            If PmtDisbRelID = Guid.Empty.ToString Then
                Dim sbAwardScheduleId As New StringBuilder
                Dim strAwardScheduleValueId As String
                PmtDisbRelID = Guid.NewGuid.ToString
                With sbAwardScheduleId
                    .Append(" select Top 1 AwardScheduleId from faStudentAwardSchedule where StudentAwardId=? and (Reference is Null or Reference='') and Amount=? order by ExpectedDate ")
                End With
                db.ClearParameters()
                db.AddParameter("@StudentAwardId", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

                Try
                    strAwardScheduleValueId = CType(db.RunParamSQLScalar(sbAwardScheduleId.ToString), Guid).ToString
                Catch ex As System.Exception
                    strAwardScheduleValueId = ""
                End Try
                sbAwardScheduleId.Remove(0, sbAwardScheduleId.Length)


                With sbPmtDisbRel1
                    .Append("INSERT INTO saPmtDisbRel(PmtDisbRelID,TransactionID,Amount,StuAwardID,AwardScheduleId,moduser,moddate) ")
                    .Append("VALUES(?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@PmtDisbRelID", PmtDisbRelID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                If Not strAwardScheduleValueId = "" Then
                    db.AddParameter("@AwardScheduleId", strAwardScheduleValueId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Else
                    db.AddParameter("@AwardScheduleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                End If

                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Try
                    db.RunParamSQLExecuteNoneQuery(sbPmtDisbRel1.ToString)
                    Return ""
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to add payment disbursement information for student " & StudentName
                    Exit Function
                End Try

            End If


            'Dim sbAwardScheduleId As New StringBuilder
            'Dim strAwardScheduleValueId As String
            'With sbAwardScheduleId
            '    .Append(" select Top 1 AwardScheduleId from faStudentAwardSchedule where StudentAwardId=? and (Reference is Null or Reference='') and Amount=? order by ExpectedDate ")
            'End With
            'db.ClearParameters()
            'db.AddParameter("@StudentAwardId", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            'Try
            '    strAwardScheduleValueId = CType(db.RunParamSQLScalar(sbAwardScheduleId.ToString), Guid).ToString
            'Catch ex As System.Exception
            '    strAwardScheduleValueId = System.DBNull.Value.ToString
            'End Try
            'sbAwardScheduleId.Remove(0, sbAwardScheduleId.Length)


            With sbPmtDisbRel
                .Append("Update saPmtDisbRel Set Amount=?,moddate=? ")
                .Append("WHERE TransactionId=? and StuAwardId=? ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Try
                db.RunParamSQLExecuteNoneQuery(sbPmtDisbRel.ToString)
                Return ""
                Exit Function
            Catch ex As System.Exception
                Return "Unable to update payment disbursement information for student " & StudentName
                Exit Function
            End Try
        Finally
        End Try
    End Function
    Public Function Update_saPayments(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal TransactionID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "", Optional ByVal StudentAwardId As String = "", Optional ByVal PmtDisbRelId As String = "") As String
        Dim sbPayments As New System.Text.StringBuilder(1000)
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(TransactionID) Then
            Return "Unable to apply payments for student " & StudentName & " as transaction is not found"
            Exit Function
        End If

        Dim intDuplicates As Integer = 0
        Dim sbDuplicates As New StringBuilder
        Dim sbgetTransactionId As New StringBuilder
        Dim sbAwardScheduleId As New StringBuilder
        Dim PaymentTypeID As Integer = 4 'EFT
        Dim SchedulePayment As Boolean = True
        Dim strAwardScheduleValueId As String
        Dim IsDeposited As Boolean = True
        With sbDuplicates
            .Append(" select Count(*) from saPayments where TransactionId=? and PaymentTypeId=?")
        End With
        db.ClearParameters()
        db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PaymentTypeID", PaymentTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        Try
            intDuplicates = db.RunParamSQLScalar(sbDuplicates.ToString)
            If intDuplicates >= 1 Then
                intDuplicates = 1
            Else
                intDuplicates = 0
            End If
        Catch ex As System.Exception
            intDuplicates = 0
        End Try

        If intDuplicates >= 1 Then
            Try
                With sbPayments
                    .Append("Update saPayments Set CheckNumber=?, ScheduledPayment=?, ")
                    .Append("PaymentTypeID=?, IsDeposited=?, moddate=? ")
                    .Append("WHERE  TransactionID=?")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@CheckNumber", Trim(msgEntry.strCheckNo), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ScheduledPayment", SchedulePayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@PaymentTypeID", PaymentTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsDeposited", SchedulePayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    db.RunParamFLSQLExecuteNoneQuery(sbPayments.ToString)

                    'Update the Scheduled Disbursement Date with Actual Disbursement Date
                    Dim sbUpdateDisbDate As New StringBuilder
                    Dim strReference As String
                    Dim strFundDescription As String
                    Dim sbFundDescrip As New StringBuilder
                    With sbFundDescrip
                        .Append("select Top 1 FundSourceDescrip from saFundSources where AdvFundSourceId=?")
                    End With
                    db.ClearParameters()
                    db.AddParameter("@AdvFundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    Try
                        strFundDescription = db.RunParamSQLScalar(sbFundDescrip.ToString)
                    Catch ex As System.Exception
                        strFundDescription = ""
                    End Try

                    'Get the Reference should be a combination of FameLink-FundSource-CheckNumber
                    'FameLink-Perkins-1234
                    strReference = "Famelink"
                    If Not strFundDescription = "" Then
                        strReference &= "-" & strFundDescription
                    End If
                    If Not msgEntry.strCheckNo = "" Then
                        strReference &= "-" & Trim(msgEntry.strCheckNo)
                    End If


                    With sbUpdateDisbDate
                        .Append(" update faStudentAwardSchedule set ExpectedDate=?,Reference=? where AwardScheduleId=(select Top 1  AwardScheduleId from faStudentAwardSchedule where StudentAwardId=? and (Reference is Null or Reference='') and Amount=? order by ExpectedDate) ")
                    End With
                    db.ClearParameters()
                    db.AddParameter("@ExpectedDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@Reference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

                    Try
                        db.RunParamSQLExecuteNoneQuery(sbUpdateDisbDate.ToString)
                        ' Return ""
                    Catch ex As System.Exception
                        'Can put a message here but may disrupt the flow of RCVD Messages
                        'so do nothing in the exception
                    End Try
                    sbUpdateDisbDate.Remove(0, sbUpdateDisbDate.Length)


                    Return ""
                Catch ex As System.Exception
                    Return "Unable to apply payment for the selected transaction for student" & StudentName
                    Exit Function
                End Try
            Finally
            End Try
        Else
            Try
                With sbPayments
                    .Append("INSERT INTO saPayments(TransactionID, CheckNumber, ScheduledPayment, PaymentTypeID, IsDeposited, moduser, moddate) ")
                    .Append("VALUES(?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CheckNumber", Trim(msgEntry.strCheckNo), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ScheduledPayment", SchedulePayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@PaymentTypeID", PaymentTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsDeposited", SchedulePayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                Try
                    db.RunParamFLSQLExecuteNoneQuery(sbPayments.ToString)
                    'Update the Scheduled Disbursement Date with Actual Disbursement Date
                    Dim sbUpdateDisbDate As New StringBuilder
                    Dim strReference As String
                    Dim strFundDescription As String
                    Dim sbFundDescrip As New StringBuilder
                    With sbFundDescrip
                        .Append("select Top 1 FundSourceDescrip from saFundSources where AdvFundSourceId=?")
                    End With
                    db.ClearParameters()
                    db.AddParameter("@AdvFundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                    Try
                        strFundDescription = db.RunParamSQLScalar(sbFundDescrip.ToString)
                    Catch ex As System.Exception
                        strFundDescription = ""
                    End Try

                    'Get the Reference should be a combination of FameLink-FundSource-CheckNumber
                    'FameLink-Perkins-1234
                    strReference = "Famelink"
                    If Not strFundDescription = "" Then
                        strReference &= "-" & strFundDescription
                    End If
                    If Not msgEntry.strCheckNo = "" Then
                        strReference &= "-" & Trim(msgEntry.strCheckNo)
                    End If



                    With sbUpdateDisbDate
                        .Append(" update faStudentAwardSchedule set ExpectedDate=?,Reference=? where AwardScheduleId=(select Top 1  AwardScheduleId from faStudentAwardSchedule where StudentAwardId=? and (Reference is Null or Reference='') and Amount=? order by ExpectedDate) ")
                    End With
                    db.ClearParameters()
                    db.AddParameter("@ExpectedDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@Reference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

                    Try
                        db.RunParamSQLExecuteNoneQuery(sbUpdateDisbDate.ToString)
                    Catch ex As System.Exception
                        'Can put a message here but may disrupt the flow of RCVD Messages
                        'so do nothing in the exception
                    End Try
                    sbUpdateDisbDate.Remove(0, sbUpdateDisbDate.Length)


                    Return ""
                Catch ex As System.Exception
                    Return "Unable to apply payment for the selected transaction for student" & StudentName
                    Exit Function
                End Try

            Finally
            End Try
        End If
    End Function
    Public Function Update_saRefunds(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal TransactionID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "") As String
        Dim sbRefunds As New System.Text.StringBuilder(1000)
        Dim sbDuplicates As New StringBuilder
        Dim intDuplicates As Integer = 0

        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(TransactionID) Then
            Return "Unable to apply refund as transaction provided for student " & StudentName & " is not found"
            Exit Function
        End If
        Try
            Dim FundSourceID As String = " "
            Dim RefundTypeID As Integer = 1
            Dim strGetFunSourceMessage As String = ""
            strGetFunSourceMessage = GetFundSourceID(db, FundSourceID, RefundTypeID, msgEntry.strFund)
            If Not strGetFunSourceMessage = "" Then
                Return "Unable to apply refund as the provided fund source for student " & StudentName & " was not found"
                Exit Function
            End If

            With sbDuplicates
                .Append(" select Count(*) from saRefunds where TransactionId=? and RefundTypeId=?")
            End With
            db.ClearParameters()
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@RefundTypeId", RefundTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Try
                intDuplicates = db.RunParamSQLScalar(sbDuplicates.ToString)
                If intDuplicates >= 1 Then
                    intDuplicates = 1
                Else
                    intDuplicates = 0
                End If
            Catch ex As System.Exception
                intDuplicates = 0
            End Try

            If intDuplicates >= 1 Then
                With sbRefunds
                    .Append("Update saRefunds set RefundTypeID=?, FundSourceID=?, moddate=? ")
                    .Append("WHERE TransactionID=? ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@RefundTypeID", RefundTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@FundSourceID", FundSourceID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                Try
                    db.RunParamSQLExecuteNoneQuery(sbRefunds.ToString)
                    Return ""
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to update refund for student " & StudentName
                    Exit Function
                End Try
            Else
                With sbRefunds
                    .Append("INSERT INTO saRefunds(TransactionID, RefundTypeID, FundSourceID, moduser, moddate) ")
                    .Append("VALUES(?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@RefundTypeID", RefundTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@FundSourceID", FundSourceID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                Try
                    db.RunParamSQLExecuteNoneQuery(sbRefunds.ToString)
                    Return ""
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to apply refund for student " & StudentName
                    Exit Function
                End Try
            End If
        Finally
        End Try

        'Return Add_saRefunds
    End Function
    Public Function GetMatchingStudentAwardsForUpdate_Reprocess(ByRef db As DataAccess, ByVal strStuEnrollID As String, ByVal strFAID As String, ByVal GrossAmount As Decimal, ByVal AwardStartDate As DateTime, ByVal AwardEndDate As DateTime, Optional ByVal StudentName As String = "", Optional ByVal strFund As String = "") As String

        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardID As New System.Text.StringBuilder(1000)
        Dim sbAcademicYear As New StringBuilder
        Dim strAwardYear As String
        Dim strAwardYearInput As String
        Dim strLoanBeginDate, strLoanEndDate As String
        Dim StudentAwardID As String
        Dim strAwardIdDiffAwardPeriod As String = ""

        If String.IsNullOrEmpty(strFAID) Then
            Return "Unable to get student award for student " & StudentName & " as the FAID provided is empty"
            Exit Function
        End If
        If String.IsNullOrEmpty(strStuEnrollID) Then
            Return "Unable to find student enrollment information for the student" & StudentName
            Exit Function
        End If
        Dim strFaidCount As Integer = strFAID.Length
        Dim intFund As Integer
        intFund = CInt(strFund)
        'If strFund = "02" Or strFund = "03" Or strFund = "04" Then
        '    strAwardYearInput = Mid(strFAID, strFaidCount, 1)
        '    strAwardYearInput = "200" & (strAwardYearInput - 1)
        'Else
        '    strAwardYearInput = Mid(strFAID, 11, 1)
        '    strAwardYearInput = "200" & (strAwardYearInput - 1)
        'End If
        If strFund = "02" Or strFund = "03" Or strFund = "04" Then
            strAwardYearInput = Mid(strFAID, strFaidCount, 1)
            strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
            'strAwardYearInput = "200" & (strAwardYearInput - 1)
        Else
            strAwardYearInput = Mid(strFAID, 11, 1)
            strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
            'strAwardYearInput = "200" & (strAwardYearInput - 1)
        End If
        If strAwardYearInput = "" Then
            Return "Unable to match the award year from Fame ESP with Advantage Award Year for student " & StudentName
            Exit Function
        End If
        With sbAcademicYear
            .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
        End With
        db.ClearParameters()
        db.AddParameter("@AcademicYearDescrip", strAwardYearInput, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'Try
        '    strAwardYear = CType(db.RunParamSQLScalar(sbAcademicYear.ToString), Guid).ToString
        'Catch ex As System.Exception
        '    strAwardYear = System.DBNull.Value.ToString
        'End Try
        Try
            dr = db.RunParamSQLDataReader(sbAcademicYear.ToString())
            If Not (dr.HasRows) Then
                Return "Unable to create student award as the award year was not found in the database for student " & StudentName
                Exit Function
            End If
            While dr.Read()
                If Not (dr("AcademicYearId") Is System.DBNull.Value) Or Not (dr("AcademicYearId").ToString = Guid.Empty.ToString) Then
                    strAwardYear = dr("AcademicYearId").ToString
                Else
                    Return "Unable to create student award as the award year was not found in the database for student " & StudentName
                    Exit Function
                End If
            End While
        Catch ex As System.Exception
            Return "Unable to create student award as the award year was not found in the database for student " & StudentName
            Exit Function
        Finally
            dr.Close()
        End Try

        sbAwardID = New StringBuilder
        With sbAwardID
            .Append(" select Distinct Top 1 StudentAwardId from faStudentAwards where StuEnrollId=? and ")
            .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
            If strAwardYear.Length >= 30 Then
                .Append(" and AcademicYearId = ? ")
            End If
            .Append(" and GrossAmount=? ")
            '.Append(" and FA_ID is NULL ")
            If strFund = "06" Or strFund = "07" Or strFund = "08" Then
                .Append(" and AwardStartDate=? ")
                .Append(" and AwardEndDate=? ")
            End If
        End With


        Try
            db.ClearParameters()
            db.AddParameter("@STUENROLLID", strStuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FundSourceId", intFund, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If strAwardYear.Length >= 30 Then
                db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@GrossAmount", GrossAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            If strFund = "06" Or strFund = "07" Or strFund = "08" Then
                db.AddParameter("@AwardStartDate", AwardStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@AwardEndDate", AwardEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            dr = db.RunParamSQLDataReader(sbAwardID.ToString())
            If Not (dr.HasRows) Then
                Dim dr1 As OleDbDataReader
                sbAwardID.Remove(0, sbAwardID.Length)
                With sbAwardID
                    .Append(" select Distinct Top 1 StudentAwardId from faStudentAwards where StuEnrollId=? and ")
                    .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
                    If strAwardYear.Length >= 30 Then
                        .Append(" and AcademicYearId = ? ")
                    End If
                    .Append(" and GrossAmount=? ")
                    '.Append(" and FA_ID is NULL ")
                    'If strFund = "06" Or strFund = "07" Or strFund = "08" Then
                    '    .Append(" and AwardStartDate=? ")
                    '    .Append(" and AwardEndDate=? ")
                    'End If
                End With
                Try
                    db.ClearParameters()
                    db.AddParameter("@STUENROLLID", strStuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@FundSourceId", intFund, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    If strAwardYear.Length >= 30 Then
                        db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    db.AddParameter("@GrossAmount", GrossAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    dr1 = db.RunParamSQLDataReader(sbAwardID.ToString)
                    If Not (dr1.HasRows) Then
                        Return ""
                        Exit Function
                    End If
                    While dr1.Read()
                        Try
                            strAwardIdDiffAwardPeriod = dr("StudentAwardID").ToString
                            Return strAwardIdDiffAwardPeriod
                        Catch ex As System.Exception
                            Return ""
                            Exit Function
                        End Try
                    End While
                    dr1.Close()
                    Exit Function
                Catch ex As System.Exception
                    Return ""
                    Exit Function
                End Try
            End If
            While dr.Read()
                Try
                    StudentAwardID = dr("StudentAwardID").ToString
                    Return StudentAwardID
                Catch ex As System.Exception
                    Return ""
                    Exit Function
                End Try
            End While
        Catch ex As System.Exception
            Return ""
            Exit Function
        Finally
            dr.Close()
        End Try
    End Function
    Public Function GetMatchingStudentAwardsForUpdate(ByRef db As DataAccess, ByVal strStuEnrollID As String, ByVal strFAID As String, ByVal GrossAmount As Decimal, ByVal AwardStartDate As DateTime, ByVal AwardEndDate As DateTime, Optional ByVal StudentName As String = "", Optional ByVal strFund As String = "") As String

        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardID As New System.Text.StringBuilder(1000)
        Dim sbAcademicYear As New StringBuilder
        Dim strAwardYear As String
        Dim strAwardYearInput As String
        Dim strLoanBeginDate, strLoanEndDate As String
        Dim StudentAwardID As String
        Dim strAwardIdDiffAwardPeriod As String = ""

        If String.IsNullOrEmpty(strFAID) Then
            Return "Unable to get student award for student " & StudentName & " as the FAID provided is empty"
            Exit Function
        End If
        If String.IsNullOrEmpty(strStuEnrollID) Then
            Return "Unable to find student enrollment information for the student" & StudentName
            Exit Function
        End If
        Dim strFaidCount As Integer = strFAID.Length
        Dim intFund As Integer
        intFund = CInt(strFund)
        'If strFund = "02" Or strFund = "03" Or strFund = "04" Then
        '    strAwardYearInput = Mid(strFAID, strFaidCount, 1)
        '    strAwardYearInput = "200" & (strAwardYearInput - 1)
        'Else
        '    strAwardYearInput = Mid(strFAID, 11, 1)
        '    strAwardYearInput = "200" & (strAwardYearInput - 1)
        'End If
        If strFund = "02" Or strFund = "03" Or strFund = "04" Then
            strAwardYearInput = Mid(strFAID, strFaidCount, 1)
            strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
            'strAwardYearInput = "200" & (strAwardYearInput - 1)
        Else
            strAwardYearInput = Mid(strFAID, 11, 1)
            strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
            'strAwardYearInput = "200" & (strAwardYearInput - 1)
        End If
        If strAwardYearInput = "" Then
            Return "Unable to match the award year from Fame ESP with Advantage Award Year for student " & StudentName
            Exit Function
        End If
        With sbAcademicYear
            .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
        End With
        db.ClearParameters()
        db.AddParameter("@AcademicYearDescrip", strAwardYearInput, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'Try
        '    strAwardYear = CType(db.RunParamSQLScalar(sbAcademicYear.ToString), Guid).ToString
        'Catch ex As System.Exception
        '    strAwardYear = System.DBNull.Value.ToString
        'End Try
        Try
            dr = db.RunParamSQLDataReader(sbAcademicYear.ToString())
            If Not (dr.HasRows) Then
                Return "Unable to create student award as the award year was not found in the database for student " & StudentName
                Exit Function
            End If
            While dr.Read()
                If Not (dr("AcademicYearId") Is System.DBNull.Value) Or Not (dr("AcademicYearId").ToString = Guid.Empty.ToString) Then
                    strAwardYear = dr("AcademicYearId").ToString
                Else
                    Return "Unable to create student award as the award year was not found in the database for student " & StudentName
                    Exit Function
                End If
            End While
        Catch ex As System.Exception
            Return "Unable to create student award as the award year was not found in the database for student " & StudentName
            Exit Function
        Finally
            dr.Close()
        End Try

        sbAwardID = New StringBuilder
        With sbAwardID
            .Append(" select Distinct Top 1 StudentAwardId from faStudentAwards where StuEnrollId=? and ")
            .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
            If strAwardYear.Length >= 30 Then
                .Append(" and AcademicYearId = ? ")
            End If
            '.Append(" and GrossAmount=? ")
            '.Append(" and FA_ID is NULL ")
            If strFund = "06" Or strFund = "07" Or strFund = "08" Then
                .Append(" and AwardStartDate=? ")
                .Append(" and AwardEndDate=? ")
            End If
        End With


        Try
            db.ClearParameters()
            db.AddParameter("@STUENROLLID", strStuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FundSourceId", intFund, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If strAwardYear.Length >= 30 Then
                db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            'db.AddParameter("@GrossAmount", GrossAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            If strFund = "06" Or strFund = "07" Or strFund = "08" Then
                db.AddParameter("@AwardStartDate", AwardStartDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@AwardEndDate", AwardEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If
            dr = db.RunParamSQLDataReader(sbAwardID.ToString())
            If Not (dr.HasRows) Then
                Dim dr1 As OleDbDataReader
                sbAwardID.Remove(0, sbAwardID.Length)
                With sbAwardID
                    .Append(" select Distinct Top 1 StudentAwardId from faStudentAwards where StuEnrollId=? and ")
                    .Append(" AwardTypeId in (select Distinct FundSourceId from saFundSources where AdvFundSourceId=?)  ")
                    If strAwardYear.Length >= 30 Then
                        .Append(" and AcademicYearId = ? ")
                    End If
                    .Append(" and GrossAmount=? ")
                    '.Append(" and FA_ID is NULL ")
                    'If strFund = "06" Or strFund = "07" Or strFund = "08" Then
                    '    .Append(" and AwardStartDate=? ")
                    '    .Append(" and AwardEndDate=? ")
                    'End If
                End With
                Try
                    db.ClearParameters()
                    db.AddParameter("@STUENROLLID", strStuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@FundSourceId", intFund, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    If strAwardYear.Length >= 30 Then
                        db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    End If
                    db.AddParameter("@GrossAmount", GrossAmount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    dr1 = db.RunParamSQLDataReader(sbAwardID.ToString)
                    If Not (dr1.HasRows) Then
                        Return ""
                        Exit Function
                    End If
                    While dr1.Read()
                        Try
                            strAwardIdDiffAwardPeriod = dr("StudentAwardID").ToString
                            Return strAwardIdDiffAwardPeriod
                        Catch ex As System.Exception
                            Return ""
                            Exit Function
                        End Try
                    End While
                    dr1.Close()
                    Exit Function
                Catch ex As System.Exception
                    Return ""
                    Exit Function
                End Try
            End If
            While dr.Read()
                Try
                    StudentAwardID = dr("StudentAwardID").ToString
                    Return StudentAwardID
                Catch ex As System.Exception
                    Return ""
                    Exit Function
                End Try
            End While
        Catch ex As System.Exception
            Return ""
            Exit Function
        Finally
            dr.Close()
        End Try
    End Function
    Public Function CheckIfSchoolHasPostedAllPayments(ByVal StudentAwardId As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "") As String
        Dim db As New DataAccess
        Dim sbAwardScheduleId As New StringBuilder
        Dim strAwardScheduleValueId As String
        Dim intCount As Integer = 0
        With sbAwardScheduleId
            .Append(" select Count(*) from faStudentAwardSchedule where StudentAwardId=? and (Reference is Null or Reference='')  ")
            'Commented by balaji on 10/15/2007 for mantis#
            '.Append(" and Amount=? ")
        End With
        db.ClearParameters()
        db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

        Try
            intCount = db.RunParamSQLScalar(sbAwardScheduleId.ToString)
            If intCount >= 1 Then
                intCount = 1
            Else
                intCount = 0
            End If
        Catch ex As System.Exception
            intCount = 0
        End Try
        If intCount = 0 Then
            Return "Unable to apply payment for student " & StudentName & ", as payment has already been posted for this disbursement"
            Exit Function
        Else
            Return ""
            Exit Function
        End If
        sbAwardScheduleId.Remove(0, sbAwardScheduleId.Length)
    End Function
    Public Function CheckIfDisbursementExists(ByVal StudentAwardId As String, Optional ByVal StudentName As String = "") As String
        Dim db As New DataAccess
        Dim sbAwardScheduleId As New StringBuilder
        Dim strAwardScheduleValueId As String
        Dim intCount As Integer = 0
        With sbAwardScheduleId
            .Append(" select Count(*) from faStudentAwardSchedule where StudentAwardId=? ")
        End With
        db.ClearParameters()
        db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            intCount = db.RunParamSQLScalar(sbAwardScheduleId.ToString)
            If intCount >= 1 Then
                intCount = 1
            Else
                intCount = 0
            End If
        Catch ex As System.Exception
            intCount = 0
        End Try
        If intCount = 0 Then
            Return "Unable to apply payments  for student " & StudentName & ", as no disbursement exists for this student award"
            Exit Function
        Else
            Return ""
            Exit Function
        End If
        sbAwardScheduleId.Remove(0, sbAwardScheduleId.Length)
    End Function
    Public Function getFundSources() As DataSet
        Dim db As New DataAccess
        Dim sbFundSources As New StringBuilder
        Dim strAwardScheduleValueId As String
        Dim intCount As Integer = 0

        With sbFundSources
            .Append(" select Distinct AdvFundSourceId,FundSourceId,FundSourceDescrip from saFundSources where AdvFundSourceId is not null ")
            .Append(" order by FundSourceDescrip ")
        End With
        Return db.RunParamSQLDataSet(sbFundSources.ToString)
    End Function
    Public Function BuildArchiveReport(ByVal FileName As String, ByVal moduser As String) As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbInsertDuplicates As New StringBuilder
        Dim intDuplicates As Integer
        Dim AwardScheduleID As String
        Dim db As New DataAccess

        Try
            With sbAwardSchedule
                .Append("INSERT INTO syFameESPArchivedFiles(ArchiveId,FileName,IsSuccessfullyProcessed,moduser,moddate) ")
                .Append("VALUES(?,?,?,?,?) ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@ArchiveId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@IsSuccessfullyProcessed", 1, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            db.AddParameter("@moduser", moduser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                Return ""
                Exit Function
            Catch ex As System.Exception
                Return ex.Message
            End Try
        Finally
        End Try
    End Function
    Public Function GetArchiveReportByFileName(ByVal FileName As String) As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbInsertDuplicates As New StringBuilder
        Dim intDuplicates As Integer
        Dim AwardScheduleID As String
        Dim db As New DataAccess
        Dim intFileExists As Integer = 0
        Try
            With sbAwardSchedule
                .Append("Select Count(*) from syFameESPArchivedFiles where filename=? ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@FileName", FileName, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                intFileExists = db.RunParamSQLScalar(sbAwardSchedule.ToString)
                If intFileExists >= 1 Then
                    Return "Already Archived"
                    Exit Function
                Else
                    Return ""
                    Exit Function
                End If
            Catch ex As System.Exception
                Return ""
            End Try
        Finally
        End Try
    End Function
    Public Function GetStudentAwardIDRCVDForFAID(ByRef db As DataAccess, ByRef StudentAwardID As String, ByVal strStuEnrollID As String, ByVal strFAID As String, Optional ByVal StudentName As String = "", Optional ByVal strFund As String = "") As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardID As New System.Text.StringBuilder(1000)

        If String.IsNullOrEmpty(strFAID) Then
            Return "Unable to get student award for student " & StudentName & " as the FAID provided is empty"
            Exit Function
        End If

        If String.IsNullOrEmpty(strStuEnrollID) Then
            Return "Unable to find student enrollment information for the student" & StudentName
            Exit Function
        End If

        sbAwardID = New StringBuilder
        With sbAwardID
            .Append(" SELECT ")
            .Append(" StudentAwardId, fa_id, StuEnrollId ")
            .Append(" FROM  faStudentAwards ")
            .Append(" WHERE(faStudentAwards.fa_id  = ? )  ")
            '.Append(" AND (StuEnrollId = ? ) ")
        End With

        Try
            db.ClearParameters()
            db.AddParameter("@FAID", strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            'db.AddParameter("@STUENROLLID", strStuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            dr = db.RunParamSQLDataReader(sbAwardID.ToString())
            If Not (dr.HasRows) Then
                Return ("Unable to find student awards for the student " & StudentName & " and FA Identifier")
                Exit Function
            End If
            While dr.Read()
                If Not (dr("StudentAwardID") Is System.DBNull.Value) Then
                    StudentAwardID = dr("StudentAwardID").ToString
                    Return ""
                Else
                    Return "Unable to find a matching student award information for the student " & StudentName
                    Exit Function
                End If
            End While
        Catch ex As System.Exception
            Return "Unable to find student awards for the student " & StudentName & " and FA Identifier"
            Exit Function
        Finally
            dr.Close()
        End Try
    End Function
    Public Function GetStudentAwardIDByFAID(ByRef db As DataAccess, ByRef StudentAwardID As String, _
                                            ByVal strFAID As String, ByRef StuEnrollId As String, _
                                            Optional ByVal StudentName As String = "", _
                                            Optional ByVal strFund As String = "") As String
        Dim dr As OleDbDataReader
        Dim ExceptionObject As System.Exception = Nothing
        Dim sbAwardID As New System.Text.StringBuilder(1000)

        If String.IsNullOrEmpty(strFAID) Then
            Return "Unable to get student award for student " & StudentName & " as the FAID provided is empty"
            Exit Function
        End If

        sbAwardID = New StringBuilder
        With sbAwardID
            .Append(" SELECT ")
            .Append(" StudentAwardId, fa_id, StuEnrollId ")
            .Append(" FROM  faStudentAwards ")
            .Append(" WHERE(faStudentAwards.fa_id  = ? )  ")
        End With

        Try
            db.ClearParameters()
            db.AddParameter("@FAID", strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            dr = db.RunParamSQLDataReader(sbAwardID.ToString())
            If Not (dr.HasRows) Then
                Return ("Unable to find student awards for the student " & StudentName & " and FA Identifier")
                Exit Function
            End If
            While dr.Read()
                If Not (dr("StudentAwardID") Is System.DBNull.Value) Then
                    StudentAwardID = dr("StudentAwardID").ToString
                    StuEnrollId = dr("StuEnrollId").ToString
                    Return ""
                Else
                    Return "Unable to find a matching student award information for the student " & StudentName
                    Exit Function
                End If
            End While
        Catch ex As System.Exception
            Return "Unable to find student awards for the student " & StudentName & " and FA Identifier"
            Exit Function
        Finally
            dr.Close()
        End Try
    End Function
    Public Function Delete_faAwardSchedule(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal StudentAwardID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "") As String
        Dim errMessage As String = String.Empty
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sMessage As String = ""
        Dim sMessage1 As String = ""
        Dim sbCheckDisbursements As New StringBuilder
        Dim sbCheckDisbursementsNoDate As New StringBuilder
        Dim sbUpdateDisbursementsNoDate As New StringBuilder
        Dim sbUpdateDisbursementsNoDateAndAmount As New StringBuilder
        Dim sbUpdateDisbursementsDateAndAmount As New StringBuilder
        Dim intCheckDisb As Integer = 0
        Dim intCheckDisbNoDateAmt As Integer = 0
        Dim strAwardScheduleId As String
        Dim dr As OleDbDataReader
        m_ExceptionMessage = ""

        If String.IsNullOrEmpty(StudentAwardID) Then
            Return "Unable to delete award schedule as student award is missing for student" & StudentName
            Exit Function
        End If

        'Look for matching StudentAward,Amount and Date
        With sbCheckDisbursements
            .Append(" Select Count(*) from faStudentAwardSchedule ")
            .Append(" WHERE StudentAwardID = ? AND ExpectedDate=? AND Amount=? ")
            .Append(" and (Reference is NULL or Reference = '') ")
        End With
        db.ClearParameters()
        db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@ExpectedDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
        db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
        Try
            intCheckDisb = db.RunParamSQLScalar(sbCheckDisbursements.ToString)
            If intCheckDisb >= 1 Then
                intCheckDisb = 1
                Dim sbCheckDisbursementsNoDate1 As New StringBuilder
                With sbCheckDisbursementsNoDate1
                    .Append(" Select Top 1 AwardScheduleId from faStudentAwardSchedule ")
                    .Append(" WHERE StudentAwardID = ? AND ExpectedDate=? AND Amount=? ")
                    .Append(" and (Reference is NULL or Reference = '') ")
                    .Append(" order by ExpectedDate Desc ")
                End With
                db.ClearParameters()
                db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ExpectedDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
                db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                Try
                    strAwardScheduleId = CType(db.RunParamSQLScalar(sbCheckDisbursementsNoDate1.ToString), Guid).ToString
                Catch ex5 As System.Exception
                    strAwardScheduleId = ""
                End Try
                With sbUpdateDisbursementsDateAndAmount
                    .Append(" Delete from faStudentAwardSchedule ")
                    .Append(" WHERE AwardScheduleId=? ")
                End With
                db.ClearParameters()
                db.AddParameter("@AwardScheduleId", strAwardScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                Try
                    db.RunParamFLSQLExecuteNoneQuery(sbUpdateDisbursementsDateAndAmount.ToString)
                    Return ""
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to delete student award schedule as the date and amount did not match or payment has already been posted for the disbursement for student " & StudentName
                    Exit Function
                End Try
            Else
                intCheckDisb = 0
                'Look for matching student award and amount
                With sbUpdateDisbursementsNoDateAndAmount
                    .Append(" Select Count(*) from faStudentAwardSchedule ")
                    .Append(" WHERE StudentAwardID = ? AND Amount=? ")
                    .Append(" and (Reference is NULL or Reference = '') ")
                End With
                db.ClearParameters()
                db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                Try
                    intCheckDisbNoDateAmt = db.RunParamSQLScalar(sbUpdateDisbursementsNoDateAndAmount.ToString)
                    If intCheckDisbNoDateAmt >= 1 Then
                        intCheckDisbNoDateAmt = 1
                        'Check if there is any disbursement with the specified amount for student award
                        With sbCheckDisbursementsNoDate
                            .Append(" Select Top 1 AwardScheduleId from faStudentAwardSchedule ")
                            .Append(" WHERE StudentAwardID = ? AND Amount=? ")
                            .Append(" and (Reference is NULL or Reference = '') ")
                            .Append(" order by ExpectedDate Desc ")
                        End With
                        db.ClearParameters()
                        db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                        dr = db.RunParamSQLDataReader(sbCheckDisbursementsNoDate.ToString())
                        If Not (dr.HasRows) Then
                            Return ("Unable to find student award schedule for the student " & StudentName)
                            Exit Function
                        End If
                        While dr.Read()
                            If Not (dr("AwardScheduleId") Is System.DBNull.Value) Then
                                strAwardScheduleId = dr("AwardScheduleId").ToString
                            Else
                                Return "Unable to find a matching student award schedule information for the student " & StudentName
                                Exit Function
                            End If
                        End While

                        If Not dr.IsClosed Then dr.Close()
                        
                        Try
                            strAwardScheduleId = CType(db.RunParamSQLScalar(sbCheckDisbursementsNoDate.ToString), Guid).ToString
                            With sbUpdateDisbursementsNoDate
                                .Append(" Delete from faStudentAwardSchedule ")
                                .Append(" WHERE AwardScheduleId=? ")
                            End With
                            db.ClearParameters()
                            db.AddParameter("@AwardScheduleId", strAwardScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                            Try
                                db.RunParamFLSQLExecuteNoneQuery(sbUpdateDisbursementsNoDate.ToString)
                                Return ""
                                Exit Function
                            Catch ex As System.Exception
                                Return "Unable to delete disbursement as amount does not match or payment has already been posted for the disbursement for student  " & StudentName
                                Exit Function
                            End Try
                        Catch ex As System.Exception
                            Return "Unable to delete disbursement as the amount does not match or payment has already been posted for the disbursement for student " & StudentName
                            Exit Function
                        End Try
                    Else
                        intCheckDisbNoDateAmt = 0
                        Return "Unable to update disbursement as either scheduled date/amount does not match or payment has already been posted for student " & StudentName
                        Exit Function

                        'Check if there is any disbursement with just student award and no date or amount
                        'With sbCheckDisbursementsNoDate
                        '    .Append(" Select Top 1 AwardScheduleId from faStudentAwardSchedule ")
                        '    .Append(" WHERE StudentAwardID = ? ")
                        '    .Append(" and (Reference is NULL or Reference = '') ")
                        '    .Append(" order by ExpectedDate Desc ")
                        'End With
                        'db.ClearParameters()
                        'db.AddParameter("@StudentAwardID", StudentAwardID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        'Try
                        '    strAwardScheduleId = CType(db.RunParamSQLScalar(sbCheckDisbursementsNoDate.ToString), Guid).ToString
                        '    With sbUpdateDisbursementsNoDate
                        '        .Append(" Delete from faStudentAwardSchedule  ")
                        '        .Append(" WHERE AwardScheduleId=? ")
                        '    End With
                        '    db.ClearParameters()
                        '    db.AddParameter("@AwardScheduleId", strAwardScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        '    Try
                        '        db.RunParamFLSQLExecuteNoneQuery(sbUpdateDisbursementsNoDate.ToString)
                        '        Return ""
                        '        Exit Function
                        '    Catch ex As System.Exception
                        '        Return "Unable to delete scheduled disbursement as payment was already posted for the disbursement for student " & StudentName
                        '        Exit Function
                        '    End Try
                        'Catch ex As System.Exception
                        '    Return "Unable to delete scheduled disbursement as payment was already posted for the disbursement for student " & StudentName
                        '    Exit Function
                        'End Try
                    End If
                Catch ex As System.Exception
                    Return "Unable to delete scheduled disbursement for student " & StudentName
                    Exit Function
                End Try
            End If
        Catch ex As System.Exception
            Return "Unable to delete scheduled disbursement for student " & StudentName
            Exit Function
        End Try

    End Function
    Public Function UpdateProcessedFieldInAwardSchedule(ByVal AwardScheduleId As String) As String
        'Connect To The Database
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        'Do an Update
        'Try
        'Build The Query
        Dim sb As New StringBuilder
        Try
            With sb
                .Append("Update faStudentAwardSchedule set isProcessed=1 where AwardScheduleId=? ")
            End With

            'LeadID
            db.AddParameter("@AwardScheduleId", AwardScheduleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            'execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            Return ""

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetRecordCountByAwardAndAmount(ByVal StudentAwardId As String, ByVal Amount As Decimal) As Integer
        'Connect To The Database
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        Dim sb As New StringBuilder
        Dim intRowCount As Integer = 0
        Try
            With sb
                .Append("Select Count(*) as RecordCount from faStudentAwardSchedule  where StudentAwardId=? and Amount=? ")
            End With

            'AwardScheduleId
            db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", Amount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            'execute the query
            intRowCount = db.RunParamSQLScalar(sb.ToString)

            Return intRowCount

        Catch ex As OleDbException
            '   return an error to the client
            Return 0
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetRecordCountByAwardAndAmountWithNULLReference(ByVal StudentAwardId As String, ByVal Amount As Decimal) As Integer
        'Connect To The Database
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim intRowCount As Integer = 0
        Try
            With sb
                .Append("Select Count(*) as RecordCount from faStudentAwardSchedule  where StudentAwardId=? and Amount=? ")
                .Append(" and (Reference is NULL or reference='') ")
            End With

            'AwardScheduleId
            db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", Amount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            'execute the query
            intRowCount = db.RunParamSQLScalar(sb.ToString)

            Return intRowCount

        Catch ex As OleDbException
            '   return an error to the client
            Return 0
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAwardYearByFameESPYear(ByVal FameESPYear As Integer) As String
        'Connect To The Database
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        Dim sb As New StringBuilder
        Dim dr As OleDbDataReader
        Dim strAwardYear As String = ""
        Try
            With sb
                .Append("Select AdvantageAwardYear from tblAwardYearMapping where FameESPAwardYear=? ")
            End With

            'AwardScheduleId
            db.AddParameter("@FameESPYear", FameESPYear, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'execute the query
            dr = db.RunParamSQLDataReader(sb.ToString)

            While (dr.Read())
                strAwardYear = Mid(dr("AdvantageAwardYear").ToString, 1, 4)
            End While
            Return strAwardYear
        Catch ex As OleDbException
            Return ""
        Finally
            'Close Connection
            dr.Close()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAwardYearByFameESPYearReturnFullYear(ByVal FameESPYear As Integer) As String
        'Connect To The Database
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        Dim sb As New StringBuilder
        Dim dr As OleDbDataReader
        Dim strAwardYear As String = ""
        Try
            With sb
                .Append("Select AdvantageAwardYear from tblAwardYearMapping where FameESPAwardYear=? ")
            End With

            'AwardScheduleId
            db.AddParameter("@FameESPYear", FameESPYear, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

            'execute the query
            dr = db.RunParamSQLDataReader(sb.ToString)

            While (dr.Read())
                strAwardYear = dr("AdvantageAwardYear").ToString
            End While
            Return strAwardYear
        Catch ex As OleDbException
            Return ""
        Finally
            'Close Connection
            dr.Close()
            db.CloseConnection()
        End Try
    End Function
    Public Function GetRecordCountByAwardIdDateAndAmount(ByVal StudentAwardId As String, ByVal Amount As Decimal, ByVal dtDate As Date) As Integer
        'Connect To The Database
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        Dim sb As New StringBuilder
        Dim intRowCount As Integer = 0
        Try
            With sb
                .Append("Select Count(*) as RecordCount from faStudentAwardSchedule  where StudentAwardId=? and Amount=? and ExpectedDate=? ")
            End With
            'AwardScheduleId
            db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", Amount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@ExpectedDate", dtDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'execute the query
            intRowCount = db.RunParamSQLScalar(sb.ToString)
            Return intRowCount
        Catch ex As OleDbException
            '   return an error to the client
            Return 0
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetRecordCountByAwardIdDateAmountAndReferenceNULL(ByVal StudentAwardId As String, ByVal Amount As Decimal, ByVal dtDate As Date) As Integer
        'Connect To The Database
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        Dim sb As New StringBuilder
        Dim intRowCount As Integer = 0
        Try
            With sb
                .Append("Select Count(*) as RecordCount from faStudentAwardSchedule  where StudentAwardId=? and Amount=? and ExpectedDate=? ")
                .Append(" and (Reference is NULL or Reference='') ")
            End With
            'AwardScheduleId
            db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", Amount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@ExpectedDate", dtDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'execute the query
            intRowCount = db.RunParamSQLScalar(sb.ToString)
            Return intRowCount
        Catch ex As OleDbException
            '   return an error to the client
            Return 0
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
    Public Function GetAwardScheduleIdByAwardIdDateAmountAndReferenceNULL(ByVal StudentAwardId As String, ByVal Amount As Decimal, ByVal dtDate As Date) As String
        'Connect To The Database
        Dim db As New DataAccess
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        Dim sb As New StringBuilder
        Dim intRowCount As Integer = 0
        Dim dr As OleDbDataReader
        Dim strAwardScheduleId As String = ""
        Try
            With sb
                .Append("Select Top 1 AwardScheduleId from faStudentAwardSchedule  where StudentAwardId=? and Amount=? and ExpectedDate=? ")
                .Append(" and (Reference is NULL or Reference='') order by expecteddate ")
            End With
            'AwardScheduleId
            db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", Amount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@ExpectedDate", dtDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            'execute the query
            dr = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read
                strAwardScheduleId = CType(dr("AwardScheduleId"), Guid).ToString
                Return strAwardScheduleId
                Exit Function
            End While

            If Not dr.IsClosed Then dr.Close()

        Catch ex As OleDbException
            '   return an error to the client
            Return ""
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
        Return strAwardScheduleId
    End Function
    Public Function CheckIfStudentIsEnrolledinMultiplePrograms(ByVal StudentId As String) As Boolean
        Dim ExceptionObject As System.Exception = Nothing
        Dim dr As OleDbDataReader = Nothing
        Dim sb As New StringBuilder
        Dim dt As New DataTable
        Dim db As New DataAccess
        Dim intEnrollmentCount As Integer = 0
        Dim boolEnrollmentCount As Boolean = False
        With sb
            .Append(" Select Count(*) as EnrollmentCount from arStuEnrollments where StudentId=? ")
        End With
        Try
            db.ClearParameters()
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            intEnrollmentCount = db.RunParamSQLScalar(sb.ToString)
            If intEnrollmentCount > 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As System.Exception
            Return False
            Exit Function
        Finally
        End Try
    End Function
    Public Function CheckIfStudentIsCurrentlyEnrolled(ByVal StudentId As String) As Boolean
        Dim ExceptionObject As System.Exception = Nothing
        Dim dr As OleDbDataReader = Nothing
        Dim sb As New StringBuilder
        Dim dt As New DataTable
        Dim db As New DataAccess
        Dim intEnrollmentCount As Integer = 0
        Dim boolEnrollmentCount As Boolean = False
        With sb
            .Append(" Select Count(*) as EnrollmentCount from arStuEnrollments where StudentId=?  ")
        End With
        Try
            db.ClearParameters()
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            intEnrollmentCount = db.RunParamSQLScalar(sb.ToString)
            If intEnrollmentCount >= 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As System.Exception
            Return False
            Exit Function
        Finally
        End Try
    End Function
    Public Function CheckIfStudentsIsinInSchoolStatus(ByVal StudentId As String) As Boolean
        Dim ExceptionObject As System.Exception = Nothing
        Dim dr As OleDbDataReader = Nothing
        Dim sb As New StringBuilder
        Dim dt As New DataTable
        Dim db As New DataAccess
        Dim intEnrollmentCount As Integer = 0
        With sb
            .Append("SELECT Count(t1.*) as EnrollmentCount ")
            .Append("FROM ")
            .Append(" arStuEnrollments t1,syStatusCodes t2,sySysStatus t3  ")
            .Append("WHERE ")
            .Append(" t1.StatusCodeId=t2.StatusCodeId and t2.SysStatusId=t3.SysStatusId and ")
            .Append(" t3.InSchool = 1 And ")
            .Append(" studentId = ?")
        End With
        Try
            db.ClearParameters()
            db.AddParameter("@StudentId", StudentId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            intEnrollmentCount = db.RunParamSQLScalar(sb.ToString)
            If intEnrollmentCount > 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As System.Exception
            Return False
            Exit Function
        Finally
        End Try
    End Function
    Public Function GetEnrollmentsDDLBySSN(ByVal SSN As String) As DataSet
        Dim ExceptionObject As System.Exception = Nothing
        Dim dr As OleDbDataReader = Nothing
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        With sb
            .Append("SELECT Distinct t1.StuEnrollId,t3.PrgVerDescrip + ' (' + t2.StatusCodeDescrip  + ')' as PrgVerDescrip,t1.EnrollDate,t1.StudentId ")
            .Append("FROM ")
            .Append(" arStuEnrollments t1,syStatusCodes t2,arPrgVersions t3,arStudent t4  ")
            .Append("WHERE ")
            .Append(" t1.StudentId=t4.StudentId and t1.StatusCodeId=t2.StatusCodeId and t1.PrgVerId=t3.PrgVerId and ")
            .Append(" t4.SSN = ?")
            .Append(" order by t1.EnrollDate,t1.StudentId ")
        End With
        Try
            db.ClearParameters()
            db.AddParameter("@SSN", SSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Return (db.RunParamSQLDataSet(sb.ToString))
        Catch ex As System.Exception
            Return Nothing
            Exit Function
        Finally
        End Try
    End Function
    ''New Code Added By Vijay Ramteke On August 24, 2010
    Private Function GetStuEnrollIdForMultipleEnrollment(ByVal strStudentId As String, ByVal strMsgSSN As String, ByVal msgEntry As FLMessageInfo) As String
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strEnrollId As String = ""
        Dim strInSchoolMessage As String = ""
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        Dim sbID As New StringBuilder
        Dim dtAwardStartDate As DateTime
        Dim dtAwardEndDate As DateTime
        If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
            dtAwardStartDate = "07/01/" + Mid(msgEntry.strAwdyr, 1, 4)
            dtAwardEndDate = "06/30/" + Mid(msgEntry.strAwdyr, 6, 2)
        Else
            dtAwardStartDate = msgEntry.dateDate1.ToString("MM/dd/yyyy")
            dtAwardEndDate = msgEntry.dateDate2.ToString("MM/dd/yyyy")
        End If

        With sbID
            .Append("SELECT ")
            .Append(" t1.StuEnrollId, t3.SysStatusId, t1.StartDate, t1.EnrollDate, t1.ExpGradDate, 0 as TransactionCount, 0 as GradePostedCount  ")
            .Append("FROM ")
            .Append(" arStuEnrollments t1,syStatusCodes t2,sySysStatus t3  ")
            .Append("WHERE ")
            .Append(" t1.StatusCodeId=t2.StatusCodeId and t2.SysStatusId=t3.SysStatusId and ")
            .Append(" studentId = ?")
            .Append(" order by EnrollDate Desc ")
        End With
        db.ClearParameters()
        db.AddParameter("@StudentId", strStudentId, DataAccess.OleDbDataType.OleDbString, 0, ParameterDirection.Input)
        Try
            ds = db.RunParamSQLDataSet(sbID.ToString, "StuEnrollments")
            If ds.Tables(0).Rows.Count > 0 Then
                Dim tempDRSEDate() As DataRow
                ''Check if Student Enrollments Between Award Start Date And Award End Date
                tempDRSEDate = ds.Tables(0).Select("StartDate<='" + dtAwardStartDate + "' and ExpGradDate>='" + dtAwardEndDate + "'")
                If (tempDRSEDate.Length > 0) Then
                    If tempDRSEDate.Length = 1 Then
                        '' Only One Enrollment Between Award Start Date And Award End Date
                        strEnrollId = tempDRSEDate(0)("StuEnrollId").ToString
                        Return strEnrollId
                        Exit Function
                    Else
                        '' Check For Currently Attending ie In Schoool
                        Dim tempDRInSchool() As DataRow
                        tempDRInSchool = ds.Tables(0).Select("SysStatusId=9")
                        If tempDRInSchool.Length = 1 Then
                            '' Only One Enrollment For Currently Attending is In School
                            strEnrollId = tempDRInSchool(0)("StuEnrollId").ToString
                            Return strEnrollId
                            Exit Function
                        Else
                            '' Check For Graduated Student
                            Dim tempDRGraduated() As DataRow
                            tempDRGraduated = ds.Tables(0).Select("SysStatusId=14")
                            If tempDRGraduated.Length = 1 Then
                                '' Only One Enrollment For Graduated Student
                                strEnrollId = tempDRGraduated(0)("StuEnrollId").ToString
                                Return strEnrollId
                                Exit Function
                            Else
                                '' Check For Transactions And Grads Posted
                                For Each dr As DataRow In ds.Tables(0).Rows
                                    CheckStudentTransactions(dr("StuEnrollId").ToString, dr)
                                Next
                                ds.AcceptChanges()
                                Dim tempDRTransactionCount() As DataRow
                                tempDRTransactionCount = ds.Tables(0).Select("TransactionCount > 0")
                                If tempDRTransactionCount.Length = 1 Then
                                    '' Only One Enrollment Has Transactions
                                    strEnrollId = tempDRTransactionCount(0)("StuEnrollId").ToString
                                    Return strEnrollId
                                    Exit Function
                                Else
                                    '' Check For Grads Posted
                                    For Each dr As DataRow In ds.Tables(0).Rows
                                        CheckStudentGradePosted(dr("StuEnrollId").ToString, dr)
                                    Next
                                    ds.AcceptChanges()
                                    Dim tempDRGradePostedCount() As DataRow
                                    tempDRGradePostedCount = ds.Tables(0).Select("GradePostedCount > 0")
                                    If tempDRGradePostedCount.Length = 1 Then
                                        '' Only One Enrollment Has Grade Posted
                                        strEnrollId = tempDRGradePostedCount(0)("StuEnrollId").ToString
                                        Return strEnrollId
                                        Exit Function
                                    Else
                                        '' Post the Enrollment in Exception
                                        strInSchoolMessage = "The award data was not imported as the student with ssn: " & strMsgSSN & " was enrolled in multiple programs " & vbCrLf
                                        strInSchoolMessage &= " and the awards need to be manually created for this student."
                                        Return strInSchoolMessage
                                        Exit Function
                                    End If
                                End If
                            End If
                        End If
                    End If
                Else
                    '' Check For Currently Attending ie In Schoool
                    Dim tempDRInSchool() As DataRow
                    tempDRInSchool = ds.Tables(0).Select("SysStatusId=9")
                    If tempDRInSchool.Length = 1 Then
                        '' Only One Enrollment For Currently Attending is In School
                        strEnrollId = tempDRInSchool(0)("StuEnrollId").ToString
                        Return strEnrollId
                        Exit Function
                    Else
                        '' Check For Graduated Student
                        Dim tempDRGraduated() As DataRow
                        tempDRGraduated = ds.Tables(0).Select("SysStatusId=14")
                        If tempDRGraduated.Length = 1 Then
                            '' Only One Enrollment For Graduated Student
                            strEnrollId = tempDRGraduated(0)("StuEnrollId").ToString
                            Return strEnrollId
                            Exit Function
                        Else
                            '' Check For Transactions And Grads Posted
                            For Each dr As DataRow In ds.Tables(0).Rows
                                CheckStudentTransactions(dr("StuEnrollId").ToString, dr)
                            Next
                            ds.AcceptChanges()
                            Dim tempDRTransactionCount() As DataRow
                            tempDRTransactionCount = ds.Tables(0).Select("TransactionCount > 0")
                            If tempDRTransactionCount.Length = 1 Then
                                '' Only One Enrollment Has Transactions
                                strEnrollId = tempDRTransactionCount(0)("StuEnrollId").ToString
                                Return strEnrollId
                                Exit Function
                            Else
                                '' Check For Grads Posted
                                For Each dr As DataRow In ds.Tables(0).Rows
                                    CheckStudentGradePosted(dr("StuEnrollId").ToString, dr)
                                Next
                                ds.AcceptChanges()
                                Dim tempDRGradePostedCount() As DataRow
                                tempDRGradePostedCount = ds.Tables(0).Select("GradePostedCount > 0")
                                If tempDRGradePostedCount.Length = 1 Then
                                    '' Only One Enrollment Has Grade Posted
                                    strEnrollId = tempDRGradePostedCount(0)("StuEnrollId").ToString
                                    Return strEnrollId
                                    Exit Function
                                Else
                                    '' Post the Enrollment in Exception
                                    strInSchoolMessage = "The award data was not imported as the student with ssn: " & strMsgSSN & " was enrolled in multiple programs " & vbCrLf
                                    strInSchoolMessage &= " and the awards need to be manually created for this student."
                                    Return strInSchoolMessage
                                    Exit Function
                                End If
                            End If
                        End If
                    End If
                    ''strInSchoolMessage = "The award data was not imported as the student with ssn: " & strMsgSSN & " was enrolled in multiple programs " & vbCrLf
                    ''strInSchoolMessage &= " and the awards need to be manually created for this student."
                    ''Return strInSchoolMessage
                    ''Exit Function
                End If
            End If
            If Not strEnrollId = "" And Not strEnrollId = Guid.Empty.ToString Then
                Return strEnrollId
                Exit Function
            Else
                strInSchoolMessage = "The award data was not imported as the student with ssn: " & strMsgSSN & " was enrolled in multiple programs " & vbCrLf
                strInSchoolMessage &= " and the awards need to be manually created for this student."
                Return strInSchoolMessage
                Exit Function
            End If
        Catch ex As System.Exception
            strInSchoolMessage = "The award data was not imported as the student with ssn: " & strMsgSSN & " was enrolled in multiple programs " & vbCrLf
            strInSchoolMessage &= " and the awards need to be manually created for this student."
            Return strInSchoolMessage
            Exit Function
        End Try
    End Function
    Private Sub CheckStudentTransactions(ByVal StuEnrollId As String, ByRef dr As DataRow)
        Dim ExceptionObject As System.Exception = Nothing
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim intTransactionCount As Integer = 0
        With sb
            .Append(" Select Count(*) as TransactionCount from saTransactions where StuEnrollId=? and voided=0 ")
        End With
        Try
            db.ClearParameters()
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            intTransactionCount = db.RunParamSQLScalar(sb.ToString)
            dr("TransactionCount") = intTransactionCount
        Catch ex As System.Exception
            dr("TransactionCount") = 0
            Exit Sub
        Finally
        End Try
    End Sub
    Private Sub CheckStudentGradePosted(ByVal StuEnrollId As String, ByRef dr As DataRow)
        Dim ExceptionObject As System.Exception = Nothing
        Dim sb As New StringBuilder
        Dim db As New DataAccess
        Dim intGradePostedCount As Integer = 0
        With sb
            ''.Append(" Select Count(*) as GradePostedCount from arResults where StuEnrollId=?  ")
            .Append(" Select MAX(GradePostedCount) as GradePostedCount From (  ")
            .Append(" Select Count(*) as GradePostedCount from arResults where where StuEnrollId=?  ")
            .Append(" Union  ")
            .Append(" Select Count(*) as GradePostedCount from arTransferGrades where StuEnrollId=?  ")
            .Append(" Union  ")
            .Append(" Select Count(*) as GradePostedCount from arGrdBkResults where StuEnrollId=?  ")
            .Append(" )t ")
        End With
        Try
            db.ClearParameters()
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            intGradePostedCount = db.RunParamSQLScalar(sb.ToString)
            dr("GradePostedCount") = intGradePostedCount
        Catch ex As System.Exception
            dr("GradePostedCount") = 0
            Exit Sub
        Finally
        End Try
    End Sub
    Public Function GetStuEnrollIDbySSN(ByRef db As DataAccess, ByRef stuEnrollID As String, _
                                        ByVal strMsgSSN As String, ByVal msgEntry As FLMessageInfo, Optional ByVal msgType As String = "", _
                                        Optional ByVal strPrgVerId As String = "", _
                                        Optional ByVal Reprocess As Boolean = False) As String
        'returns the StuEnrollmentID given a valid SSN, else returns blank string
        Dim ExceptionObject As System.Exception = Nothing
        Dim dr As OleDbDataReader = Nothing
        Dim sbEnrollID As New System.Text.StringBuilder(1000)
        Dim strStudentID As String = ""
        Dim strEnrollId As String = ""
        Dim strInSchoolMessage As String = ""
        GetStuEnrollIDbySSN = False
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(strMsgSSN) Then
            Return "Student enrollment information not found as ssn is empty"
            Exit Function
        End If
        Try
            sbEnrollID = New StringBuilder
            With sbEnrollID
                .Append(" SELECT Distinct ")
                .Append(" StudentID ")
                .Append(" FROM ")
                .Append(" arStudent ")
                .Append(" WHERE ")
                .Append(" SSN = ? ")
            End With
            db.ClearParameters()
            db.AddParameter("@SSN", strMsgSSN, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            Try
                strStudentID = CType(db.RunParamSQLScalar(sbEnrollID.ToString), Guid).ToString
                If Not strStudentID = "" And Not strStudentID = Guid.Empty.ToString Then
                    Dim sbID As New StringBuilder
                    Dim boolStudentEnrolledInAnyProgram As Boolean = CheckIfStudentIsCurrentlyEnrolled(strStudentID)
                    If boolStudentEnrolledInAnyProgram = True Then
                        Dim boolMultipleEnrollments As Boolean = CheckIfStudentIsEnrolledinMultiplePrograms(strStudentID)
                        If boolMultipleEnrollments = False Then
                            strInSchoolMessage = GetStuEnrollIdForSingleEnrollment(strStudentID, strMsgSSN)
                            Return strInSchoolMessage
                            Exit Function
                        Else
                            'If Student is currently enrolled in multiple programs and if data pull is historical
                            'then send record to exception

                            'If the msgType is FAID or HEAD file then validate for historical data and send the data to exception
                            'If the message type is DISB,RCVD,REFU or CHNG then get the student enrollment id
                            If msgType = "FAID" Or msgType = "HEAD" Then
                                'If user wants to reprocess and user selects the program version
                                If Reprocess = True And Not strPrgVerId = "" Then
                                    strInSchoolMessage = GetStuEnrollIdByProgramVersion(strStudentID, strMsgSSN, strPrgVerId)
                                    Return strInSchoolMessage
                                    Exit Function
                                Else
                                    strInSchoolMessage = GetStuEnrollIdForMultipleEnrollment(strStudentID, strMsgSSN, msgEntry)
                                    Return strInSchoolMessage
                                    Exit Function
                                End If
                            Else
                                strInSchoolMessage = GetStuEnrollIdForMultipleEnrollment(strStudentID, strMsgSSN, msgEntry)
                                Return strInSchoolMessage
                                Exit Function
                            End If

                        End If
                    Else
                        'If the student is currently not enrolled in any program
                        strInSchoolMessage = "The award data was not imported as the student with ssn: " & strMsgSSN & " is currently not enrolled in any program. " & vbCrLf
                        Return strInSchoolMessage
                        Exit Function
                    End If
                Else
                    Return "Student not found for the given ssn:" & strMsgSSN
                    Exit Function
                End If
            Catch ex As System.Exception
                Return "Student not found for the given ssn:" & strMsgSSN
                Exit Function
            End Try
        Catch ex As System.Exception
            Return "Student not found for the given ssn:" & strMsgSSN
            Exit Function
        End Try
    End Function
    ''New Code Added By Vijay Ramteke On August 24, 2010
    '' New Code Added By Vijay Ramteke On August 25, 2010
    Function GetStudentAwardScheduleIdByAmountAndDate(ByRef db As DataAccess, ByVal StudentAwardId As String, ByVal Amount As Decimal, ByVal ExpectedDate As DateTime) As String
        Dim sbAwardScheduleId As New StringBuilder
        Dim strAwardScheduleValueId As String
        With sbAwardScheduleId
            .Append(" Select Top 1 AwardScheduleId From faStudentAwardSchedule ")
            .Append(" Where StudentAwardId=? And Amount=? And ExpectedDate=? ")
            .Append(" And AwardScheduleId not in (Select AwardScheduleId From saPmtDisbRel where StuAwardId=?) ")
            .Append(" order by ExpectedDate ")
        End With
        db.ClearParameters()
        db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@Amount", Amount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
        db.AddParameter("@ExpectedDate", ExpectedDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strAwardScheduleValueId = CType(db.RunParamSQLScalar(sbAwardScheduleId.ToString), Guid).ToString
        Catch ex As System.Exception
            strAwardScheduleValueId = ""
        End Try
        Return strAwardScheduleValueId
    End Function
    Function GetStudentAwardScheduleIdByDate(ByRef db As DataAccess, ByVal StudentAwardId As String, ByVal ExpectedDate As DateTime) As String
        Dim sbAwardScheduleId As New StringBuilder
        Dim strAwardScheduleValueId As String
        With sbAwardScheduleId
            .Append(" Select Top 1 AwardScheduleId From faStudentAwardSchedule ")
            .Append(" Where StudentAwardId=? And ExpectedDate=? ")
            .Append(" And AwardScheduleId not in (Select AwardScheduleId From saPmtDisbRel where StuAwardId=?) ")
            .Append(" order by ExpectedDate ")
        End With
        db.ClearParameters()
        db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@ExpectedDate", ExpectedDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strAwardScheduleValueId = CType(db.RunParamSQLScalar(sbAwardScheduleId.ToString), Guid).ToString
        Catch ex As System.Exception
            strAwardScheduleValueId = ""
        End Try
        Return strAwardScheduleValueId
    End Function
    Function GetStudentAwardScheduleIdByAmount(ByRef db As DataAccess, ByVal StudentAwardId As String, ByVal Amount As Decimal) As String
        Dim sbAwardScheduleId As New StringBuilder
        Dim strAwardScheduleValueId As String
        With sbAwardScheduleId
            .Append(" Select Top 1 AwardScheduleId From faStudentAwardSchedule ")
            .Append(" Where StudentAwardId=? And Amount=? ")
            .Append(" And AwardScheduleId not in (Select AwardScheduleId From saPmtDisbRel where StuAwardId=?) ")
            .Append(" order by ExpectedDate ")
        End With
        db.ClearParameters()
        db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@Amount", Amount, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
        db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strAwardScheduleValueId = CType(db.RunParamSQLScalar(sbAwardScheduleId.ToString), Guid).ToString
        Catch ex As System.Exception
            strAwardScheduleValueId = ""
        End Try
        Return strAwardScheduleValueId
    End Function
    Function GetStudentAwardScheduleId(ByRef db As DataAccess, ByVal StudentAwardId As String) As String
        Dim sbAwardScheduleId As New StringBuilder
        Dim strAwardScheduleValueId As String
        With sbAwardScheduleId
            .Append(" Select Top 1 AwardScheduleId From faStudentAwardSchedule ")
            .Append(" Where StudentAwardId=? ")
            ''' Code modified by K






            ''amalesh Ahuja on 1st Sept 2010 to resolcve mantis issue id 19670 
            ''.Append(" And AwardScheduleId not in (Select AwardScheduleId From saPmtDisbRel where StuAwardId=?) ")
            .Append(" And AwardScheduleId not in (Select AwardScheduleId From saPmtDisbRel where AwardScheduleId is not null ) ")
            ''''''''''
            .Append(" order by ExpectedDate ")
        End With
        db.ClearParameters()
        db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ''' Code modified by Kamalesh Ahuja on 1st Sept 2010 to resolcve mantis issue id 19670 
        ''db.AddParameter("@StudentAwardId", StudentAwardId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '''''''''''''
        Try
            strAwardScheduleValueId = CType(db.RunParamSQLScalar(sbAwardScheduleId.ToString), Guid).ToString
        Catch ex As System.Exception
            strAwardScheduleValueId = ""
        End Try
        Return strAwardScheduleValueId
    End Function
    Public Function Add_saTransaction_New(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef TransactionID As String, ByVal StuEnrollID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "", Optional ByVal SourceToTarget As String = "") As String
        Dim sbTransaction As New System.Text.StringBuilder(1000)
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(StuEnrollID) Then
            Return "Unable to match financial aid data with student " & StudentName & " enrollment information"
            Exit Function
        End If
        TransactionID = Guid.NewGuid.ToString

        'Update the Scheduled Disbursement Date with Actual Disbursement Date
        Dim sbUpdateDisbDate As New StringBuilder
        Dim strReference As String
        Dim strFundDescription As String
        Dim sbFundDescrip As New StringBuilder
        Dim strTransDescrip As String

        Dim sbAcademicYear As New StringBuilder
        Dim strAwardYear As String
        Dim strAwardYearInput As String
        Dim strLoanBeginDate, strLoanEndDate As String
        Dim strFaidCount As Integer = msgEntry.strFAID.Length

        Dim sbFundSourceId As New StringBuilder
        Dim strFundSourceId As String
        Dim sbTransCodeId As New StringBuilder
        Dim strTransCodeId As String

        If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
            strAwardYearInput = Mid(msgEntry.strFAID, strFaidCount, 1)
            strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
        Else
            strAwardYearInput = Mid(msgEntry.strFAID, 11, 1)
            strAwardYearInput = GetAwardYearByFameESPYear(strAwardYearInput)
        End If
        If strAwardYearInput = "" Then
            Return "Unable to match the award year from Fame ESP with Advantage Award Year for student " & StudentName
            Exit Function
        End If
        With sbAcademicYear
            .Append(" Select Distinct AcademicYearId from saAcademicYears where SUBSTRING(AcademicYearDescrip,1,4) = ? ")
        End With
        db.ClearParameters()
        db.AddParameter("@AcademicYearDescrip", strAwardYearInput, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strAwardYear = CType(db.RunParamSQLScalar(sbAcademicYear.ToString), Guid).ToString
        Catch ex As System.Exception
            strAwardYear = System.DBNull.Value.ToString
        End Try

        With sbFundDescrip
            .Append("select Top 1 FundSourceDescrip from saFundSources where AdvFundSourceId=?")
        End With
        db.ClearParameters()
        db.AddParameter("@AdvFundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        Try
            strFundDescription = db.RunParamSQLScalar(sbFundDescrip.ToString)
        Catch ex As System.Exception
            strFundDescription = ""
        End Try

        strTransDescrip = "Famelink"
        If Not strFundDescription = "" Then
            strTransDescrip &= "-" & strFundDescription
        End If

        'Get the Reference should be a combination of FameLink-FundSource-CheckNumber
        'FameLink-Perkins-1234
        strReference = "Famelink"
        If Not strFundDescription = "" Then
            strReference &= "-" & strFundDescription
        End If
        If Not msgEntry.strCheckNo = "" Then
            strReference &= "-" & Trim(msgEntry.strCheckNo)
        End If

        'Troy:2/28/2012
        'We need to add the FundSourceId and TransCodeId as well. These are needed for the 90/10 and 1098T to return accurate results.
        'For eg, without the TransCodeId the 90/10 cannot tell if a payment was applied to an institutional charge or not.

        'FundSourceId
        With sbFundSourceId
            .Append("select Top 1 FundSourceId ")
            .Append("from saFundSources fs, syStatuses s, syCampGrps cg, syCmpGrpCmps cgc ")
            .Append("where fs.StatusId=s.StatusId ")
            .Append("and fs.CampGrpId=cg.CampGrpId ")
            .Append("and cg.CampGrpId=cgc.CampGrpId ")
            .Append("and s.Status='Active' ")
            .Append("and fs.AdvFundSourceId=? ")
            .Append("and cgc.CampusId=(select CampusId from arStuEnrollments se where se.StuEnrollId=?) ")
        End With
        db.ClearParameters()
        db.AddParameter("@AdvFundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@StuEnrollId", StuEnrollID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            strFundSourceId = (db.RunParamSQLScalar(sbFundSourceId.ToString)).ToString
        Catch ex As System.Exception
            strFundSourceId = ""
        End Try

        'TransCodeId for Tuition (SysTransCodeId=1)
        With sbTransCodeId
            .Append("select Top 1 TransCodeId ")
            .Append("from saTransCodes tc, syStatuses s, syCampGrps cg, syCmpGrpCmps cgc ")
            .Append("where tc.StatusId=s.StatusId ")
            .Append("and tc.CampGrpId=cg.CampGrpId ")
            .Append("and cg.CampGrpId=cgc.CampGrpId ")
            .Append("and s.Status='Active' ")
            .Append("and SysTransCodeId=1 ")
            .Append("and cgc.CampusId=(select CampusId from arStuEnrollments se where se.StuEnrollId=?) ")
            .Append("and tc.TransCodeDescrip like '%Tuition%' ")
        End With
        db.ClearParameters()
        db.AddParameter("@StuEnrollId", StuEnrollID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Try
            strTransCodeId = (db.RunParamSQLScalar(sbTransCodeId.ToString)).ToString
        Catch ex As System.Exception
            strTransCodeId = ""
        End Try

        Try
            Dim TransTypeID As Integer = 2
            Dim IsPosted As Boolean = True
            Dim IsAutomatic As Boolean = False
            Select Case msgEntry.strMsgType
                Case "RCVD"
                    TransTypeID = 2
                Case "REFU"
                    TransTypeID = 1  'Adjustments
            End Select

            Dim intDuplicates As Integer = 0
            Dim sbDuplicates As New StringBuilder
            Dim sbgetTransactionId As New StringBuilder
            Dim strAmount As Decimal
            If TransTypeID = 2 Then
                strAmount = msgEntry.decAmount1 * (-1.0)
            Else
                strAmount = msgEntry.decAmount1
            End If

            Dim intCheckForDuplicates As Integer = 0
            ''If TransTypeID = 2 Then
            ''    intCheckForDuplicates = CheckForDuplicateTransactions(StuEnrollID, msgEntry.dateDate1, strReference, strAwardYear, msgEntry.decAmount1 * (-1.0), strTransDescrip)
            ''Else
            ''    intCheckForDuplicates = CheckForDuplicateTransactions(StuEnrollID, msgEntry.dateDate1, strReference, strAwardYear, msgEntry.decAmount1, strTransDescrip)
            ''End If
            If intCheckForDuplicates = 0 Then
                With sbTransaction
                    .Append("INSERT INTO saTransactions(TransactionID,StuEnrollID,TransDate,TransAmount,CreateDate,TransReference,TransDescrip,moduser,moddate,TransTypeID,IsPosted,IsAutomatic,AcademicYearId,FundSourceId,TransCodeId) ")
                    .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollID", StuEnrollID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@TransDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                If TransTypeID = 2 Then
                    db.AddParameter("@TransAmount", msgEntry.decAmount1 * (-1.0), DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                Else
                    db.AddParameter("@TransAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                End If
                db.AddParameter("@CreateDate", msgEntry.dateDate2, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransReference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@TransDescrip", strTransDescrip, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@TransTypeID", TransTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsPosted", IsPosted, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@IsAutomatic", IsAutomatic, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@AcademicYearId", strAwardYear, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                If strFundSourceId <> "" Then
                    db.AddParameter("@FundSourceId", strFundSourceId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@AcademicYearId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                If strTransCodeId <> "" Then
                    db.AddParameter("@TransCodeId", strTransCodeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@TransCodeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If

                'execute the query
                Try
                    db.RunParamSQLExecuteNoneQuery(sbTransaction.ToString)
                    Return ""
                    Exit Function
                Catch ex As System.Exception
                    Return "Unable to apply payment for " & StudentName
                    Exit Function
                End Try
            Else
                'If there are duplicates return the transactionid
                If TransTypeID = 2 Then
                    TransactionID = GetTransactionId(StuEnrollID, msgEntry.dateDate1, strReference, strAwardYear, msgEntry.decAmount1 * (-1.0), strTransDescrip)
                Else
                    TransactionID = GetTransactionId(StuEnrollID, msgEntry.dateDate1, strReference, strAwardYear, msgEntry.decAmount1, strTransDescrip)
                End If
            End If
        Finally
        End Try
    End Function
    Public Function Add_saPmtDisbRel_New(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByRef PmtDisbRelID As String, ByVal TransactionID As String, ByVal StuAwardID As String, ByVal StuAwardScheduleID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "") As String
        Dim sbPmtDisbRel As New System.Text.StringBuilder(1000)
        Dim sbPmtDisbRel1 As New System.Text.StringBuilder(1000)
        Dim intDisbCount As Integer = 0
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(TransactionID) Then
            Return "Unable to add payment disbursement for student" & StudentName & " as provided transaction Id is not found"
            Exit Function
        End If
        If String.IsNullOrEmpty(StuAwardID) Then
            Return "Unable to add payment disbursement as student award is missing for student " & StudentName
            Exit Function
        End If
        PmtDisbRelID = Guid.NewGuid.ToString
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        Dim myconn As New OleDbConnection(MyAdvAppSettings.AppSettings("ConString").ToString)
        myconn.Open()

        Try
            With sbPmtDisbRel
                .Append("Select Count(*) from saPmtDisbRel where TransactionID=? and Amount=? and StuAwardID=? and AwardScheduleId=? ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuAwardScheduleID", StuAwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                intDisbCount = db.RunParamSQLScalar(sbPmtDisbRel.ToString)
                sbPmtDisbRel.Remove(0, sbPmtDisbRel.Length)
                If intDisbCount >= 1 Then
                    With sbPmtDisbRel
                        .Append("Select Top 1 PmtDisbRelId from saPmtDisbRel where TransactionID=? and Amount=? and StuAwardID=? and AwardScheduleId=?  ")
                    End With
                    'Add params
                    db.ClearParameters()
                    db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@StuAwardScheduleID", StuAwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Try
                        PmtDisbRelID = CType(db.RunParamSQLScalar(sbPmtDisbRel.ToString), Guid).ToString
                        sbPmtDisbRel.Remove(0, sbPmtDisbRel.Length)
                        Return ""
                        Exit Function
                    Catch ex As System.Exception
                    End Try
                End If
            Catch ex As System.Exception
                Return "Unable to apply payment for student " & StudentName
                Exit Function
            End Try

            With sbPmtDisbRel1
                .Append("INSERT INTO saPmtDisbRel(PmtDisbRelID,TransactionID,Amount,StuAwardID,AwardScheduleId,moduser,moddate) ")
                .Append("VALUES(?,?,?,?,?,?,?) ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@PmtDisbRelID", PmtDisbRelID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Amount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            db.AddParameter("@StuAwardID", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not StuAwardScheduleID = "" And Not StuAwardScheduleID = Guid.Empty.ToString Then
                db.AddParameter("@AwardScheduleId", StuAwardScheduleID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@AwardScheduleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sbPmtDisbRel1.ToString)
                Return ""
                Exit Function
            Catch ex As System.Exception
                ''Dim sbGetAmount As New StringBuilder
                ''Dim strGetAmount As String = ""
                Dim sMessage As String = "exception"
                ''With sbGetAmount
                ''    .Append(" select Top 1 Amount from faStudentAwardSchedule where StudentAwardId=? and (Reference is Null or Reference='') and ExpectedDate=? order by ExpectedDate ")
                ''End With
                ''db.ClearParameters()
                ''db.AddParameter("@StudentAwardId", StuAwardID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                ''db.AddParameter("@ExpectedDate", msgEntry.dateDate1, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                ''Try
                ''    strGetAmount = CType(db.RunParamSQLScalar(sbGetAmount.ToString), Decimal).ToString
                ''Catch ex9 As Exception
                ''    strGetAmount = ""
                ''End Try
                ''sMessage = "Unable to apply payment as the disbursement amount did not match for student " & StudentName & "." & vbLf
                ''If Not strGetAmount = "" Then
                ''    sMessage &= "Disbursement amount in Advantage : " & CType(strGetAmount, Decimal).ToString("#0.00")
                ''End If
                Return sMessage
                Exit Function
            End Try


        Finally
        End Try
    End Function
    Public Function Add_saPayments_New(ByRef db As DataAccess, ByRef groupTrans As OleDbTransaction, ByVal TransactionID As String, ByVal msgEntry As FLMessageInfo, Optional ByVal StudentName As String = "", Optional ByVal StudentAwardId As String = "", Optional ByVal AwardScheduleId As String = "", Optional ByVal PmtDisbRelId As String = "") As String
        Dim sbPayments As New System.Text.StringBuilder(1000)
        m_ExceptionMessage = ""
        If String.IsNullOrEmpty(TransactionID) Then
            Return "Unable to apply payments for student " & StudentName & " as transaction is not found"
            Exit Function
        End If

        Dim intDuplicates As Integer = 0
        Dim sbAwardScheduleId As New StringBuilder
        Dim sbDuplicates As New StringBuilder
        Dim sbgetTransactionId As New StringBuilder
        Dim strAwardScheduleValueId As String
        Dim PaymentTypeID As Integer = 4 'EFT
        Dim SchedulePayment As Boolean = True
        Dim IsDeposited As Boolean = True
        With sbDuplicates
            .Append(" select Count(*) from saPayments where TransactionId=? and PaymentTypeId=?")
        End With
        db.ClearParameters()
        db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PaymentTypeID", PaymentTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        Try
            intDuplicates = db.RunParamSQLScalar(sbDuplicates.ToString)
            If intDuplicates >= 1 Then
                Return ""
                Exit Function
            End If
        Catch ex As System.Exception
        End Try

        If intDuplicates = 0 Then
            Try
                With sbPayments
                    .Append("INSERT INTO saPayments(TransactionID, CheckNumber, ScheduledPayment, PaymentTypeID, IsDeposited, moduser, moddate) ")
                    .Append("VALUES(?,?,?,?,?,?,?) ")
                End With

                'Add params
                db.ClearParameters()
                db.AddParameter("@TransactionID", TransactionID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@CheckNumber", Trim(msgEntry.strCheckNo), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ScheduledPayment", SchedulePayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@PaymentTypeID", PaymentTypeID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@IsDeposited", SchedulePayment, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgEntry.strModUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@moddate", msgEntry.dateModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                Try
                    db.RunParamFLSQLExecuteNoneQuery(sbPayments.ToString)
                    'Update the Scheduled Disbursement Date with Actual Disbursement Date
                    Dim sbUpdateDisbDate As New StringBuilder
                    Dim strReference As String
                    Dim strFundDescription As String
                    Dim sbFundDescrip As New StringBuilder
                    With sbFundDescrip
                        .Append("select Top 1 FundSourceDescrip from saFundSources where AdvFundSourceId=?")
                    End With
                    db.ClearParameters()
                    db.AddParameter("@AdvFundSourceId", CInt(msgEntry.strFund), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                    Try
                        strFundDescription = db.RunParamSQLScalar(sbFundDescrip.ToString)
                    Catch ex As System.Exception
                        strFundDescription = ""
                    End Try

                    'Get the Reference should be a combination of FameLink-FundSource-CheckNumber
                    'FameLink-Perkins-1234
                    strReference = "Famelink"
                    If Not strFundDescription = "" Then
                        strReference &= "-" & strFundDescription
                    End If
                    If Not msgEntry.strCheckNo = "" Then
                        strReference &= "-" & Trim(msgEntry.strCheckNo)
                    End If

                    With sbUpdateDisbDate
                        .Append(" update faStudentAwardSchedule set Reference=? where AwardScheduleId=? ")
                    End With
                    db.ClearParameters()
                    db.AddParameter("@Reference", strReference, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@StudentAwardId", AwardScheduleId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Try
                        db.RunParamSQLExecuteNoneQuery(sbUpdateDisbDate.ToString)
                    Catch ex As System.Exception
                        'Can put a message here but may disrupt the flow of RCVD Messages
                        'so do nothing in the exception
                        Return "Unable to post payment for " & StudentName
                        Exit Function
                    End Try
                    sbUpdateDisbDate.Remove(0, sbUpdateDisbDate.Length)
                    Return ""
                Catch ex As System.Exception
                    Return "Unable to apply payment for the selected transaction for student" & StudentName
                    Exit Function
                End Try
            Finally
            End Try
        End If
    End Function
    '' New Code Added By Vijay Ramteke On August 25, 2010
    '' New Code Added By Vijay Ramteke On September 03, 2010
    Public Function GetStuEnrollId(ByVal strSSN As String, ByVal dtAwardStartDate As DateTime, ByVal dtAwardEndDate As DateTime) As String
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strEnrollId As String = ""
        Dim strInSchoolMessage As String = ""
        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString").ToString
        Dim sbID As New StringBuilder

        With sbID
            .Append("SELECT ")
            .Append(" t1.StuEnrollId, t3.SysStatusId, t1.StartDate, t1.EnrollDate, t1.ExpGradDate, 0 as TransactionCount, 0 as GradePostedCount  ")
            .Append("FROM ")
            .Append(" arStuEnrollments t1,syStatusCodes t2,sySysStatus t3  ")
            .Append("WHERE ")
            .Append(" t1.StatusCodeId=t2.StatusCodeId and t2.SysStatusId=t3.SysStatusId and ")
            .Append(" studentId = (Select StudentId from arStudent Where SSN= ? )")
            .Append(" order by EnrollDate Desc ")
        End With
        db.ClearParameters()
        db.AddParameter("@SSN", strSSN, DataAccess.OleDbDataType.OleDbString, 0, ParameterDirection.Input)
        Try
            ds = db.RunParamSQLDataSet(sbID.ToString, "StuEnrollments")
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows.Count = 1 Then
                    strEnrollId = ds.Tables(0).Rows(0)("StuEnrollId").ToString
                Else
                    Dim tempDRSEDate() As DataRow
                    ''Check if Student Enrollments Between Award Start Date And Award End Date
                    tempDRSEDate = ds.Tables(0).Select("StartDate<='" + dtAwardStartDate + "' and ExpGradDate>='" + dtAwardEndDate + "'")
                    If (tempDRSEDate.Length > 0) Then
                        If tempDRSEDate.Length = 1 Then
                            '' Only One Enrollment Between Award Start Date And Award End Date
                            strEnrollId = tempDRSEDate(0)("StuEnrollId").ToString
                            Return strEnrollId
                            Exit Function
                        Else
                            '' Check For Currently Attending ie In Schoool
                            Dim tempDRInSchool() As DataRow
                            tempDRInSchool = ds.Tables(0).Select("SysStatusId=9")
                            If tempDRInSchool.Length = 1 Then
                                '' Only One Enrollment For Currently Attending is In School
                                strEnrollId = tempDRInSchool(0)("StuEnrollId").ToString
                                Return strEnrollId
                                Exit Function
                            Else
                                '' Check For Graduated Student
                                Dim tempDRGraduated() As DataRow
                                tempDRGraduated = ds.Tables(0).Select("SysStatusId=14")
                                If tempDRGraduated.Length = 1 Then
                                    '' Only One Enrollment For Graduated Student
                                    strEnrollId = tempDRGraduated(0)("StuEnrollId").ToString
                                    Return strEnrollId
                                    Exit Function
                                Else
                                    '' Check For Transactions And Grads Posted
                                    For Each dr As DataRow In ds.Tables(0).Rows
                                        CheckStudentTransactions(dr("StuEnrollId").ToString, dr)
                                    Next
                                    ds.AcceptChanges()
                                    Dim tempDRTransactionCount() As DataRow
                                    tempDRTransactionCount = ds.Tables(0).Select("TransactionCount > 0")
                                    If tempDRTransactionCount.Length = 1 Then
                                        '' Only One Enrollment Has Transactions
                                        strEnrollId = tempDRTransactionCount(0)("StuEnrollId").ToString
                                        Return strEnrollId
                                        Exit Function
                                    Else
                                        '' Check For Grads Posted
                                        For Each dr As DataRow In ds.Tables(0).Rows
                                            CheckStudentGradePosted(dr("StuEnrollId").ToString, dr)
                                        Next
                                        ds.AcceptChanges()
                                        Dim tempDRGradePostedCount() As DataRow
                                        tempDRGradePostedCount = ds.Tables(0).Select("GradePostedCount > 0")
                                        If tempDRGradePostedCount.Length = 1 Then
                                            '' Only One Enrollment Has Grade Posted
                                            strEnrollId = tempDRGradePostedCount(0)("StuEnrollId").ToString
                                            Return strEnrollId
                                            Exit Function
                                        Else
                                            '' Post the Enrollment in Exception
                                            Return ""
                                            Exit Function
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Else
                        '' Check For Currently Attending ie In Schoool
                        Dim tempDRInSchool() As DataRow
                        tempDRInSchool = ds.Tables(0).Select("SysStatusId=9")
                        If tempDRInSchool.Length = 1 Then
                            '' Only One Enrollment For Currently Attending is In School
                            strEnrollId = tempDRInSchool(0)("StuEnrollId").ToString
                            Return strEnrollId
                            Exit Function
                        Else
                            '' Check For Graduated Student
                            Dim tempDRGraduated() As DataRow
                            tempDRGraduated = ds.Tables(0).Select("SysStatusId=14")
                            If tempDRGraduated.Length = 1 Then
                                '' Only One Enrollment For Graduated Student
                                strEnrollId = tempDRGraduated(0)("StuEnrollId").ToString
                                Return strEnrollId
                                Exit Function
                            Else
                                '' Check For Transactions And Grads Posted
                                For Each dr As DataRow In ds.Tables(0).Rows
                                    CheckStudentTransactions(dr("StuEnrollId").ToString, dr)
                                Next
                                ds.AcceptChanges()
                                Dim tempDRTransactionCount() As DataRow
                                tempDRTransactionCount = ds.Tables(0).Select("TransactionCount > 0")
                                If tempDRTransactionCount.Length = 1 Then
                                    '' Only One Enrollment Has Transactions
                                    strEnrollId = tempDRTransactionCount(0)("StuEnrollId").ToString
                                    Return strEnrollId
                                    Exit Function
                                Else
                                    '' Check For Grads Posted
                                    For Each dr As DataRow In ds.Tables(0).Rows
                                        CheckStudentGradePosted(dr("StuEnrollId").ToString, dr)
                                    Next
                                    ds.AcceptChanges()
                                    Dim tempDRGradePostedCount() As DataRow
                                    tempDRGradePostedCount = ds.Tables(0).Select("GradePostedCount > 0")
                                    If tempDRGradePostedCount.Length = 1 Then
                                        '' Only One Enrollment Has Grade Posted
                                        strEnrollId = tempDRGradePostedCount(0)("StuEnrollId").ToString
                                        Return strEnrollId
                                        Exit Function
                                    Else
                                        '' Post the Enrollment in Exception
                                        Return ""
                                        Exit Function
                                    End If
                                End If
                            End If
                        End If
                        ''strInSchoolMessage = "The award data was not imported as the student with ssn: " & strMsgSSN & " was enrolled in multiple programs " & vbCrLf
                        ''strInSchoolMessage &= " and the awards need to be manually created for this student."
                        ''Return strInSchoolMessage
                        ''Exit Function
                    End If
                End If

            End If
            If Not strEnrollId = "" And Not strEnrollId = Guid.Empty.ToString Then
                Return strEnrollId
                Exit Function
            Else
                Return ""
                Exit Function
            End If
        Catch ex As System.Exception
            Return ""
            Exit Function
        End Try
    End Function
    '' New Code Added By Vijay Ramteke On September 03, 2010
    ''Added by Kamalesh Ahuja on May 27 2010 to resolve mantis issue id 18652
    Public Function CheckIsRemoteServer(ByVal CampusId As String) As Boolean
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim dr As OleDbDataReader
        Dim ISRemoteServer As Boolean = False

        Try

            With sb
                .Append(" Select IsRemoteServerFL from syCampuses where CampusId=? ")

            End With
            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            dr = db.RunParamSQLDataReader(sb.ToString)

            While dr.Read
                ISRemoteServer = CType(dr("IsRemoteServerFL"), Boolean).ToString
                Return ISRemoteServer
                Exit Function
            End While

            If Not dr.IsClosed Then dr.Close()

        Catch ex As Exception
            Return ISRemoteServer
        End Try

    End Function
    ''Added by Kamalesh Ahuja on May 27 2010 to resolve mantis issue id 18652
    Public Function GetFlSourcePath(ByVal CampusId As String, Optional ByRef SourcePath As String = "", Optional ByRef TargetPath As String = "", Optional ByRef ExceptionPath As String = "", Optional ByRef RemoteUserName As String = "", Optional ByRef RemotePasswd As String = "")
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim SourceFolderLocation As String = ""

        Try

            db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            ds = db.RunParamSQLDataSetUsingSP("USP_FL_GetFlSourcePath")

            If ds.Tables(0).Rows.Count > 0 Then
                SourcePath = ds.Tables(0).Rows(0)("FLSourcePath").ToString()
                TargetPath = ds.Tables(0).Rows(0)("FLTargetPath").ToString()
                ExceptionPath = ds.Tables(0).Rows(0)("FLExceptionPath").ToString()
                RemoteUserName = ds.Tables(0).Rows(0)("RemoteServerUsrNmFL").ToString()
                RemotePasswd = ds.Tables(0).Rows(0)("RemoteServerPwdFL").ToString()
                Return ""
                Exit Function
            End If

        Catch ex As Exception
            Return ""
        End Try

    End Function

    Public Function GetFAMELinkFilesWithExceptions(ByVal CampusId As String) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As New DataSet

        db.AddParameter("@campusid", CampusId, SqlDbType.VarChar, 50, ParameterDirection.Input)

        Try
            ds = db.RunParamSQLDataSet_SP("USP_FL_GetFAMELinkFilesWithExceptions")
            Return ds
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

    End Function

    Public Function GetFLExceptionDetails(ByVal FileId As String, ByVal ShowHidden As Boolean) As DataSet
        Dim db As New SQLDataAccess
        Dim ds As New DataSet

        db.AddParameter("@fileid", FileId, SqlDbType.VarChar, 50, ParameterDirection.Input)

        Try
            If ShowHidden = True Then
                ds = db.RunParamSQLDataSet_SP("USP_FL_GetAllFAMELinkExceptionDetails")
            Else
                ds = db.RunParamSQLDataSet_SP("USP_FL_GetShowItemsFAMELinkExceptionDetails")
            End If

            Return ds
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try
    End Function

    Public Function AddFLExceptionRecord(ByVal msgEntry As FLMessageInfo, Optional ByVal AwardCutOffDate As String = "", _
                                         Optional ByVal DisbDate As String = "", Optional ByVal msgFile As FLFileInfo = Nothing) As String
        Dim sbAwardSchedule As New System.Text.StringBuilder(1000)
        Dim sbFile As New System.Text.StringBuilder(1000)
        Dim sbInsertDuplicates As New StringBuilder
        Dim intDuplicates As Integer
        Dim AwardScheduleID As String
        Dim db As New DataAccess

        Try

            'If the parent file is passed in then we need to create the parent record in the syFAMELinkProcessedFiles table.
            If Not msgFile Is Nothing Then
                With sbFile
                    .Append("INSERT INTO syFAMELinkProcessedFiles(FLProcessedFileId,FileName,FileCreationDate,ModUser,ModDate,Success,Failed) ")
                    .Append("VALUES(?,?,?,?,?,?,?) ")
                End With

                'add params
                db.ClearParameters()
                db.AddParameter("@fileid", msgFile.FileId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@filename", msgFile.strFileShortName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@cdate", Date.MinValue, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@moduser", msgFile.networkuser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@moddate", Date.MinValue, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@success", 0, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@failed", 0, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                Try
                    db.RunParamSQLExecuteNoneQuery(sbFile.ToString)
                    Return ""
                    Exit Function
                Catch ex As System.Exception
                End Try

            End If

            With sbAwardSchedule
                .Append("INSERT INTO syFameESPExceptionReport(FLExceptionId,FLFileProcessedId,SSN,FAID,Fund,GrossAmount,Message,DisbursementDate, AwardYear,loanfees,loanstartdate,loanenddate,Show) ")
                .Append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            'Add params
            db.ClearParameters()
            db.AddParameter("@ExceptionReportId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FileId", msgEntry.FileId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@SSN", msgEntry.strSSN, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@FAID", msgEntry.strFAID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Fund", msgEntry.strFund, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If (msgEntry.strChgCode.ToLower = "c" Or msgEntry.strChgCode.ToLower = "n") Then
                db.AddParameter("@GrossAmount", msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            Else
                db.AddParameter("@GrossAmount", msgEntry.decAmount1, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
            End If
            db.AddParameter("@Message", msgEntry.Message, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not DisbDate = "" Then
                db.AddParameter("@DisbursementDate", DisbDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Else
                db.AddParameter("@DisbursementDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@awardyear", msgEntry.strAwdyr, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@Loanfees", msgEntry.decAmount2, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)

            Dim strLoanBeginDate, strLoanEndDate As Date

            Try
                If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                    strLoanBeginDate = "07/01/" + Mid(msgEntry.strAwdyr, 1, 4)
                    strLoanEndDate = "06/30/" + Mid(msgEntry.strAwdyr, 6, 2)
                Else
                    strLoanBeginDate = msgEntry.dateDate1
                    strLoanEndDate = msgEntry.dateDate2
                End If
            Catch ex As System.Exception
                Dim strAwardYearInput As String
                Dim strFaidCount As Integer = msgEntry.strFAID.Length
                Dim strAwardYr As String = ""
                If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                    strAwardYearInput = Mid(msgEntry.strFAID, strFaidCount, 1)
                    strAwardYr = GetAwardYearByFameESPYearReturnFullYear(strAwardYearInput)
                    strLoanBeginDate = "07/01/" + Mid(strAwardYr, 1, 4)
                    strLoanEndDate = "06/30/" + Mid(strAwardYr, 6, 4)
                End If
            End Try

            db.AddParameter("@loanstartdate", strLoanBeginDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@loanenddate", strLoanEndDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@show", msgEntry.Show, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sbAwardSchedule.ToString)
                Return ""
                Exit Function
            Catch ex As System.Exception
            End Try
        Finally
        End Try
    End Function

    Public Function UpdateHiddenForFile(ByVal FileName As String, ByVal Value As Integer) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder(1000)

        With sb
            .Append("Update syFameESPExceptionReport set Hide = ? where FileName = ?")
        End With

        'add params
        db.ClearParameters()
        db.AddParameter("@hide", Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@filename", FileName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            Return ""
            Exit Function
        Catch ex As System.Exception
        End Try


    End Function

    Public Function UpdateHiddenByExceptionReportId(ByVal ExceptionReportId As String, ByVal Value As Integer) As String
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder(1000)

        With sb
            .Append("Update syFameESPExceptionReport set Hide = ? where ExceptionReportId = ?")
        End With

        'add params
        db.ClearParameters()
        db.AddParameter("@hide", Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@exguid", ExceptionReportId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            Return ""
            Exit Function
        Catch ex As System.Exception
        End Try
    End Function

    Public Function GetCountOfHiddenForFile(ByVal FileName As String) As Integer
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder(1000)
        Dim intRecs As Integer

        With sb
            .Append("select count(*) from syFameESPExceptionReport where FileName = ? and Hide = 1")
        End With

        'add params
        db.ClearParameters()
        db.AddParameter("@exguid", FileName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        Try
            intRecs = db.RunParamSQLScalar(sb.ToString)
            Return intRecs
            Exit Function
        Catch ex As System.Exception
        End Try
    End Function

    Public Function GetCountOfExceptions(Optional ByVal CampusId As String = "") As Integer
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder(1000)
        Dim intRecs As Integer

        Try
            With sb
                If CampusId = "" Then
                    .Append("select count(*) from syFameESPExceptionReport")
                Else
                    .Append("select COUNT(*) ")
                    .Append("from syFameESPExceptionReport er, arStudent st, arStuEnrollments se ")
                    .Append("where er.SSN=st.SSN ")
                    .Append("and st.StudentId=se.StudentId ")
                    .Append("and se.CampusId in( ")
                    .Append("       select cm.CampusId ")
                    .Append("       from syCampuses cm ")
                    .Append("       where cm.CampusId=se.CampusId ")
                    .Append("       and cm.FLSourcePath=(select FLSourcePath from syCampuses where CampusId=?) ")
                    .Append("       and cm.FLTargetPath=(select FLTargetPath from syCampuses where CampusId=?) ")
                    .Append("       and cm.FLExceptionPath=(select FLExceptionPath from syCampuses where CampusId=?) ")
                    .Append("   ) ")
                End If

            End With


            If CampusId <> "" Then
                db.AddParameter("@campusid1", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@campusid2", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@campusid3", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            intRecs = db.RunParamSQLScalar(sb.ToString)
            Return intRecs
            Exit Function
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error getting count of FAMELink exceptions - " & ex.Message)
            Else
                Throw New Exception("Error getting count of FAMELink exceptions - " & ex.InnerException.Message)
            End If
        End Try
    End Function

    Public Function GetAllCampuses() As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim sb As New System.Text.StringBuilder(1000)

        Try
            With sb
                .Append("select CampusId, CampDescrip from syCampuses order by CampDescrip")
            End With

            ds = db.RunParamSQLDataSet(sb.ToString)

            Return ds
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error getting all campuses - " & ex.Message)
            Else
                Throw New Exception("Error getting all campuses - " & ex.InnerException.Message)
            End If
        End Try
    End Function

    Public Function FileHasEnrollmentsForCampus(ByVal FileName As String, ByVal CampusId As String) As Boolean
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim sb As New System.Text.StringBuilder(1000)
        Dim intRecs As Integer

        Try
            With sb
                .Append("select COUNT(er.FileName) ")
                .Append("from syFameESPExceptionReport er, arStudent st, arStuEnrollments se ")
                .Append("where er.SSN=st.SSN ")
                .Append("and st.StudentId=se.StudentId ")
                .Append("and ((er.FileName = ?) or (er.FileName like '%\' + ?) or (er.FileName like '%/' + ?)) ")
                .Append("and se.CampusId = ? ")
            End With

            db.ClearParameters()
            db.AddParameter("@filename1", FileName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@filename2", FileName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@filename3", FileName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@campus", CampusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            intRecs = db.RunParamSQLScalar(sb.ToString)

            If intRecs = 0 Then
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error getting campuses - " & ex.Message)
            Else
                Throw New Exception("Error getting campuses - " & ex.InnerException.Message)
            End If
        End Try
    End Function

    Public Function FileHasEnrollmentsForNoCampuses(ByVal FileName As String) As Boolean
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim sb As New System.Text.StringBuilder(1000)
        Dim intRecs As Integer

        Try
            With sb
                .Append("select COUNT(er.FileName) ")
                .Append("from syFameESPExceptionReport er, arStudent st, arStuEnrollments se ")
                .Append("where er.SSN=st.SSN ")
                .Append("and st.StudentId=se.StudentId ")
                .Append("and ((er.FileName = ?) or (er.FileName like '%\' + ?) or (er.FileName like '%/' + ?)) ")
            End With

            db.ClearParameters()
            db.AddParameter("@filename1", FileName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@filename2", FileName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@filename3", FileName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            intRecs = db.RunParamSQLScalar(sb.ToString)

            If intRecs = 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error checking if file has only invalid SSNS - " & ex.Message)
            Else
                Throw New Exception("Error checking if file has only invalid SSNS - " & ex.InnerException.Message)
            End If
        End Try
    End Function

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function


#End Region
End Class

