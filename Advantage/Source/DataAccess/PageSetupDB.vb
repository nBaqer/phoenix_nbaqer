Imports FAME.Advantage.Common

Public Class PageSetupDB

    Public Function GetResourceDef(ByVal resourceId As Integer, ByVal languageName As String) As DataTable
        Dim ds As New DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT t1.ResDefId,t2.TblFldsId,t2.FldId,t3.FldName,t1.Required,t3.DDLId,t3.FldLen,t4.FldType,t6.Caption,t1.ControlName ")
            .Append("FROM syResTblFlds t1,syTblFlds t2,syFields t3,syFieldTypes t4,syTables t5,syFldCaptions t6,syLangs t7 ")
            .Append("WHERE t1.TblFldsId=t2.TblFldsId ")
            .Append("AND t2.FldId=t3.FldId ")
            .Append("AND t2.TblId=t5.TblId ")
            .Append("AND t3.FldTypeId=t4.FldTypeId ")
            .Append("AND t2.FldId=t6.FldId ")
            .Append("AND t6.LangId=t7.LangId ")
            .Append("AND t1.ResourceId=? ")
            .Append("AND t7.LangName=?")
        End With

        db.AddParameter("@resid", resourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.AddParameter("@langid", languageName, DataAccess.OleDbDataType.OleDbString, 5, ParameterDirection.Input)

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)

    End Function
    Public Function GetResourceForQuickLeads() As DataTable
        Dim ds As New DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append(" Select Distinct ")
            .Append("   t6.ResDefId,t7.TblFldsId,t3.FldId,t3.FldName,t6.Required,t3.DDLId,t3.FldLen,t8.FldType, ")
            .Append("   t4.Caption, t6.ControlName ")
            .Append("   from ")
            .Append("       adQuickLeadSections t1,adExpQuickLeadSections t2,syFields t3, ")
            .Append("       syFldCaptions t4,adLeadFields t5,syResTblFlds t6,syTblFlds t7, ")
            .Append("       syFieldTypes t8 ")
            .Append("   where ")
            .Append("       t1.SectionId = t2.SectionId and t2.FldId = t3.FldId and t3.FldId=t4.FldId and ")
            .Append("       t3.FldId=t5.LeadId and t5.LeadField=t1.SectionId and t6.TblFldsId = t7.TblFldsId ")
            .Append("       and t7.FldId=t3.FldId and t7.TblId = 183 and t3.FldTypeId=t8.FldTypeId and ResourceId=206 ")
        End With
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds.Tables(0)
    End Function
    Public Function GetDataDictionaryReqFieldsForQuickLeads() As DataTable
        Dim ds As New DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append(" SELECT FldId ")
            .Append(" FROM syInstFldsDDReq")
            .Append(" where FldId in (select Distinct LeadId from adLeadFields) ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds.Tables(0)
    End Function
    Public Function GetDataDictionaryReqFields() As DataTable
        Dim ds As New DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT FldId ")
            .Append("FROM syInstFldsDDReq")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function

    Public Function GetSchlRequiredFieldsForResource(ByVal resourceId As Integer) As DataTable
        Dim ds As New DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT t1.TblFldsId,t2.FldId ")
            .Append("FROM syInstResFldsReq t1, syTblFlds t2 ")
            .Append("WHERE ResourceId = ? ")
            .Append("AND t1.TblFldsId=t2.TblFldsId ")
        End With

        db.AddParameter("@resid", resourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        ds = db.RunParamSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function
    Public Function GetSchlRequiredFieldsForResourceForQuickLeads(ByVal resourceId As Integer) As DataTable
        Dim ds As New DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'DE6156 3/1/2012 Janet Robinson added resourceid to query because required fields set for New Leads and Info were showing up as required on quick leads
        With sb
            .Append("SELECT t1.TblFldsId,t2.FldId ")
            .Append("FROM syInstResFldsReq t1, syTblFlds t2 ")
            .Append("WHERE  ")
            .Append("t1.TblFldsId=t2.TblFldsId ")
            .Append("AND t1.ResourceId=" & CType(resourceId, String))
            .Append(" AND FldId in (select Distinct LeadId from adLeadFields) ")
        End With
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        ds = db.RunParamSQLDataSet(sb.ToString)
        Return ds.Tables(0)
    End Function
    Public Function GetIPEDSFields() As DataTable
        Dim ds As New DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT distinct t2.FldId ")
            .Append("FROM syRptAgencyFields t1, syRptFields t2 ")
            .Append("WHERE t1.RptFldId=t2.RptFldId ")
            .Append("AND t1.RptAgencyId=1 ")
            .Append("AND t2.FldId IS NOT NULL ")
        End With

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        ds = db.RunSQLDataSet(sb.ToString)

        Return ds.Tables(0)
    End Function
    Public Function GetIPEDSFieldsForQuickLeads() As DataTable
        Dim ds As New DataSet
        Dim sb As New System.Text.StringBuilder
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With sb
            .Append("SELECT distinct t2.FldId ")
            .Append("FROM syRptAgencyFields t1, syRptFields t2 ")
            .Append("WHERE t1.RptFldId=t2.RptFldId ")
            .Append("AND t1.RptAgencyId=1 ")
            .Append("AND t2.FldId IS NOT NULL ")
            .Append("AND t2.FldId in (select Distinct LeadId from adLeadFields) ")
        End With
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        ds = db.RunSQLDataSet(sb.ToString)
        Return ds.Tables(0)
    End Function
End Class
