' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' RacesDB.vb
'
' RacesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================

Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class PrgTypesDB
    Public Function GetProgTypId() As Integer
        Dim ds As DataSet
        Dim maxindex As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder
            Dim obj As New Object

            '   build the sql query
            With sb
                .Append("SELECT MAX(ProgTypId) ")
                .Append("FROM arProgTypes")
            End With

            '   Execute the query
            maxindex = db.RunParamSQLScalar(sb.ToString)

            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return maxindex

    End Function
    Public Function GetAllProgramTypes() As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT   PT.ProgTypId, ")
            .Append("         PT.Description ")
            .Append("FROM     arProgTypes PT, syStatuses ST ")
            .Append("WHERE    PT.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY PT.Description ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
End Class
