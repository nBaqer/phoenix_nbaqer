Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllCSummaryObjectDB

	Public Shared Function GetReportDataSetRaw(ByVal rptParamInfo As ReportParamInfoIPEDS) As DataSet

		' get list of students to include, based on report parameters
		Dim StudentList As String = IPEDSDB.GetStudentList(rptParamInfo)

		' if there are no students to include, return empty dataset
		If StudentList = "" Then
			Return New DataSet
		End If

		Dim sb As New System.Text.StringBuilder
		Dim dsRaw As New DataSet

		With sb
			.Append("SELECT DISTINCT arStudent.StudentId, ")

			' retrieve all other needed columns
			.Append("(SELECT TOP 1 plStudentEducation.EducationInstType ")
			.Append("	FROM plStudentEducation ")
			.Append("	WHERE plStudentEducation.StudentId = arStudent.StudentID ")
			.Append("	ORDER BY plStudentEducation.GraduatedDate DESC) AS LastEdInstType, ")
			.Append("(SELECT TOP 1 plStudentEducation.GraduatedDate ")
			.Append("	FROM plStudentEducation ")
			.Append("	WHERE plStudentEducation.StudentId = arStudent.StudentID ")
			.Append("	ORDER BY plStudentEducation.GraduatedDate DESC) AS LastEdGradDate, ")
			.Append("adLeads.Address1, ")
			.Append("(SELECT syStates.StateDescrip ")
			.Append("	FROM syStates, adLeads ")
			.Append("	WHERE adLeads.StateId = syStates.StateId AND ")
			.Append("         adLeads.LeadId = arStuEnrollments.LeadId) AS StateDescripAddress, ")
			.Append("(SELECT syStates.FIPSCode ")
			.Append("	FROM syStates, adLeads ")
			.Append("	WHERE adLeads.StateId = syStates.StateId AND ")
			.Append("         adLeads.LeadId = arStuEnrollments.LeadId) AS _FIPSCodeAddress, ")
			.Append("(SELECT syStates.StateDescrip ")
			.Append("	FROM syStates, adLeads ")
			.Append("	WHERE adLeads.DrivLicStateId = syStates.StateId AND ")
			.Append("	      adLeads.LeadId = arStuEnrollments.LeadId) AS StateDescripDL, ")
			.Append("(SELECT syStates.FIPSCode ")
			.Append("	FROM syStates, adLeads ")
			.Append("	WHERE adLeads.DrivLicStateId = syStates.StateId AND ")
			.Append("         adLeads.LeadId = arStuEnrollments.LeadId) AS _FIPSCodeDL, ")
			.Append("adLeads.ForeignZip ")
			.Append("FROM ")
			.Append("arStudent, arStuEnrollments, adLeads ")

			' establish necessary relationships
			.Append("WHERE ")
			.Append("arStudent.StudentId = arStuEnrollments.StudentId AND ")
			.Append("arStuEnrollments.LeadId = adLeads.LeadId AND ")

			' append list of appropriate students to include
			.Append("arStudent.StudentId IN (" & StudentList & ") ")

			' get list of enrollments and relevant info for same list of students
            .Append(";" & IPEDSDB.GetSQL_EnrollmentInfo(StudentList, rptParamInfo.FilterProgramIDs))
		End With

		' run query, add returned data to raw dataset
		dsRaw = IPEDSDB.DataAccessIPEDS().RunSQLDataSet(sb.ToString)

		' set table names for each part of returned data
		With dsRaw
			.Tables(0).TableName = TblNameStudents
			.Tables(1).TableName = TblnameEnrollments
		End With

		'return the raw dataset
		Return dsRaw

	End Function

End Class