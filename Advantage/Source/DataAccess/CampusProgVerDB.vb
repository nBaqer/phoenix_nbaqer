Imports FAME.Advantage.Common

Public Class CampusProgVerDB
    ''' <summary>
    ''' Store the connection string for this class
    ''' </summary>
    ''' <remarks></remarks>
    Private ReadOnly conString As String
    Private ReadOnly sqlConnectionString As String

    ''' <summary>
    ''' Application Setting for get some other different values from config.
    ''' </summary>
    ''' <remarks></remarks>                                                                                                             
    Private myAdvAppSettings As AdvAppSettings

    Sub New()
        myAdvAppSettings = AdvAppSettings.GetAppSettings()
        conString = myAdvAppSettings.AppSettings("ConString")
        sqlConnectionString = myAdvAppSettings.AppSettings("ConnectionString").ToString
    End Sub


    ''' <summary>
    ''' This method is to fetch the R2T4 Calculation Period Types.
    ''' </summary>
    ''' <returns>Returns the R2T4 Calculation Period Type Id and Description</returns>
    Public Function GetR2T4CalculationPeriodTypes() As DataSet
        '   connect to the database
        Dim db As New DataAccess
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet

        db.ConnectionString = conString ' myAdvAppSettings.AppSettings("ConString")
        '   build the sql query
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT CalculationPeriodTypeId, Description ")
            .Append(" FROM syR2T4CalculationPeriodTypes ")
        End With

        ' Execute the query
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "R2T4CalculationPeriodTypes")

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function

    ''' <summary>
    ''' This method inserts campus program versions details.
    ''' </summary>
    ''' <param name="campusPrgVersion"></param>
    ''' <param name="campusId"></param>
    ''' <returns></returns>
    Public Function InsertIntoCampusPrgVerTable(campusPrgVersion As Dictionary(Of String , Object), campusId As String) As String
        Dim db As New SQLDataAccess

        db.ConnectionString = sqlConnectionString ' myAdvAppSettcampusPrgVersion.CalculationPeriodTypeId.ToString()ings.AppSettings("ConnectionString")

        db.AddParameter("@PrgVerID", New Guid(campusPrgVersion.Item("PrgVerId").ToString()), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@CampusId", New Guid(campusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@IsTitleIV", campusPrgVersion.Item("IsTitleIV"), SqlDbType.Bit, , ParameterDirection.Input)
        db.AddParameter("@IsFAMEApproved", campusPrgVersion.Item("IsFAMEApproved"), SqlDbType.Bit, , ParameterDirection.Input)
        db.AddParameter("@IsSelfPaced", If(campusPrgVersion.Item("IsSelfPaced"), DBNull.Value), SqlDbType.Bit, , ParameterDirection.Input)
        db.AddParameter("@CalculationPeriodTypeId", If(campusPrgVersion.Item("CalculationPeriodTypeId") = Nothing, DBNull.Value, New Guid(campusPrgVersion.Item("CalculationPeriodTypeId").ToString())), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
        db.AddParameter("@AllowExcusAbsPerPayPrd", If(campusPrgVersion.Item("AllowExcusAbsPerPayPrd"), DBNull.Value), SqlDbType.Float, , ParameterDirection.Input)
        db.AddParameter("@TermSubEqualInLen", If(campusPrgVersion.Item("TermSubEqualInLen"), DBNull.Value), SqlDbType.Bit, , ParameterDirection.Input)
        db.AddParameter("@ModUser", campusPrgVersion.Item("ModUser"), SqlDbType.VarChar, , ParameterDirection.Input)
        db.AddParameter("@ModDate", campusPrgVersion.Item("ModDate"), SqlDbType.DateTime, , ParameterDirection.Input)

        Try
            db.RunParamSQLExecuteNoneQuery_SP("dbo.usp_SaveCampusPrgVersionsDetails")
        Finally
            db.ClearParameters()
            db.CloseConnection()
        End Try
        Return String.Empty
    End Function

    ''' <summary>
    ''' This method fetches the campus program version details.
    ''' </summary>
    ''' <param name="prgVerId"></param>
    ''' <param name="campusId"></param>
    ''' <returns></returns>
    Public Function GetCampusPrgVersionData(prgVerId As String, campusId As String) As DataSet
        Dim db As New DataAccess
        Dim da As OleDbDataAdapter
        Dim ds As New DataSet

        db.ConnectionString = conString ' myAdvAppSettings.AppSettings("ConString")
        ' build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT Distinct ")
            .Append(" IsTitleIV, IsFAMEApproved, IsSelfPaced, CalculationPeriodTypeId, AllowExcusAbsPerPayPrd, TermSubEqualInLen ")
            .Append(" FROM arCampusPrgVersions ")
            .Append(" WHERE PrgVerId = ? ")
            .Append(" AND CampusId = ? ")
        End With

        db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Execute the query
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "CampusPrgVersions")
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function

    ''' <summary>
    ''' This method deletes the campus program versions details.
    ''' </summary>
    ''' <param name="prgVerId"></param>
    ''' <param name="campusId"></param>
    Public Sub DeleteCampusProgramVersion(ByVal prgVerId As String, ByVal campusId As String)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("DELETE FROM arCampusPrgVersions WHERE PrgVerId = ? AND CampusId = ?")
        End With
        db.AddParameter("@PrgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
    End Sub

    ''' <summary>
    ''' This method updates the campus program versions details.
    ''' </summary>
    ''' <param name="tblName"></param>
    ''' <param name="pkId"></param>
    ''' <param name="value"></param>
    ''' <param name="fldvalue1"></param>
    ''' <param name="campusId"></param>
    ''' <param name="modUser"></param>
    ''' <returns></returns>
    Public Function UpdateCampusPrgVersionDetails(ByVal tblName As String, ByVal pkId As String,
                                            ByVal value As String,
                                            ByVal fldvalue1 As String,
                                            ByVal campusId As String,
                                            ByVal modUser As String) As String
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        db.OpenConnection()
        With sb
            .Append("update " & tblName & " set " & fldvalue1 & "=?,ModDate=?,ModUser=? where PrgVerId = ? and CampusId = ?")
        End With
        If value = "" Then
            db.AddParameter("@fldvalue1", DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Else
            db.AddParameter("@fldvalue1", value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
        db.AddParameter("@ModUser", modUser, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@PrgVerId", pkId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Try
            db.RunParamFLSQLExecuteNoneQuery(sb.ToString)
            Return ""
        Catch ex As Exception
            Return Nothing
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function

    ''' <summary>
    ''' This method returns the values for Self Paced dropdown list. 
    ''' </summary>
    ''' <param name="tblname">The tblname is the table name where the self paced values stored.</param>
    ''' <param name="fldName">The fldName is the self paced column name</param>
    ''' <param name="pKName">The pKName is the primary key program version Id.</param>
    ''' <param name="pKValue">The pKValue is the primary key value</param>
    ''' <returns>Returns self paced value of the selected item for self paced dropdownlist</returns>
    Public Function GetSelfPacedValue(ByVal tblname As String, ByVal fldName As String, ByVal pKName As String, ByVal pKValue As String, ByVal campusId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim strGetRegentTerm As Integer = -1
        db.OpenConnection()
        With sb
            .Append("SELECT CASE WHEN " & fldName & " IS NULL THEN '' WHEN " & fldName & " = 0 THEN 1 WHEN " & fldName & " = 1 THEN 2 END FROM " & tblname & " WHERE " & pKName & "= ? " & " AND CampusId = ?")
        End With
        db.AddParameter("@prgVerId", pKValue, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        Try
            strGetRegentTerm = db.RunParamSQLScalar(sb.ToString)
            Return strGetRegentTerm
        Finally
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            If db.Connection.State = ConnectionState.Open Then
                db.CloseConnection()
            End If
        End Try
    End Function

    ''' <summary>
    ''' This method checkd if the campus program versions details already exists for the specified campus.
    ''' </summary>
    ''' <param name="prgVerId"></param>
    ''' <param name="campusId"></param>
    ''' <returns>Returns a boolean value whether the campus program version exists or not</returns>
    Public Function DoesCampusProgVerAlreadyExists(prgVerId As String, campusId As String) As Integer
        Dim db As New DataAccess
        Dim sb As New StringBuilder
        Dim result

        Try
            With sb
                'Query to retrieve number of attendance records belonging to a particular campus Program Version
                .Append(" select count(*) from arCampusPrgVersions where prgVerId = ? and campusId= ? ")
            End With

            'add parameters
            db.AddParameter("@prgVerId", prgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            result = CType(db.RunParamSQLScalar(sb.ToString), Integer)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

        Return result
    End Function
End Class