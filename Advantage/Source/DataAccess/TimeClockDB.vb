' ===============================================================================
' TimeClockDB.vb
' DataAccess classes for Programs
' ===============================================================================
' Copyright (C) 2006,2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports FAME.AdvantageV1.Common.TimeClock
Imports FAME.AdvantageV1.Common.AR
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Namespace TimeClock

    Public Class TimeClockDB
#Region "Sql helpers"
        Protected Shared Function GetAddOrUpdateSql(ByVal info As TimeClockPunchInfo) As String
            Dim sb As New StringBuilder()
            sb.Append(GetUpdateSql(info))
            sb.Append("if @@rowcount = 0 " + vbCrLf)
            sb.Append("begin " + vbCrLf)
            sb.Append(GetExistSql_PunchPrimaryKey(info))
            sb.Append("begin " + vbCrLf)
            sb.Append(GetInsertSql(info))
            sb.Append("end " + vbCrLf)
            sb.Append("end " + vbCrLf)
            sb.Append(GetUpdateStatusSql(info))

            Return sb.ToString()
        End Function


        Protected Shared Function GetAddOrUpdateSql_byClass(ByVal info As TimeClockPunchInfo) As String
            Dim sb As New StringBuilder()
            sb.Append(GetUpdateSql_byClass(info))
            sb.Append("if @@rowcount = 0 " + vbCrLf)
            sb.Append("begin " + vbCrLf)
            sb.Append(GetExistSql_PunchPrimaryKey(info))
            sb.Append("begin " + vbCrLf)
            sb.Append(GetInsertSql(info))
            sb.Append("end " + vbCrLf)
            sb.Append("end " + vbCrLf)
            sb.Append(GetUpdateStatusSql_byClass(info))

            Return sb.ToString()
        End Function

        Protected Shared Function GetUpdateSql_byClass(ByVal info As TimeClockPunchInfo) As String
            Dim sb As New StringBuilder
            sb.Append("UPDATE [arStudentTimeClockPunches] " + vbCrLf)
            sb.Append("SET " + vbCrLf)
            sb.AppendFormat("     ClockId = '{0}', {1}", info.ClockId, vbCrLf)
            sb.AppendFormat("     Status = {0}, {1}", CType(info.Status, Integer), vbCrLf)
            If info.StuEnrollId = "" Then
                sb.AppendFormat("     StuEnrollId = null {0}", vbCrLf)
            Else
                sb.AppendFormat("     StuEnrollId = '{0}' {1}", info.StuEnrollId, vbCrLf)
            End If
            sb.AppendFormat("WHERE " + vbCrLf)
            sb.AppendFormat("     BadgeId = '{0}' {1}", info.BadgeId, vbCrLf)
            sb.AppendFormat("     and PunchTime = '{0}' {1}", info.PunchTime.ToString(), vbCrLf)
            sb.AppendFormat("     and PunchType = {0} {1}", CType(info.PunchType, Integer), vbCrLf)
            sb.AppendFormat("     and ClsSectmeetingId = '{0}' {1}", info.ClsSectMeetingId, vbCrLf)
            sb.AppendFormat("     and SpecialCode = '{0}' {1}", info.SpecialCode, vbCrLf)
            sb.Append(vbCrLf)
            Return sb.ToString()
        End Function
        Protected Shared Function GetExistSql_PunchPrimaryKey(ByVal info As TimeClockPunchInfo) As String
            Dim sb As New StringBuilder
            sb.Append("IF NOT EXISTS (SELECT * FROM [dbo].[arStudentTimeClockPunches] " + vbCrLf)
            sb.AppendFormat("WHERE " + vbCrLf)
            sb.AppendFormat("     BadgeId = '{0}' {1}", info.BadgeId, vbCrLf)
            sb.AppendFormat("     and PunchTime = '{0}' {1}", info.PunchTime.ToString(), vbCrLf)
            sb.AppendFormat("     and PunchType = {0} {1}", CType(info.PunchType, Integer), vbCrLf)
            sb.Append(") " + vbCrLf)
            Return sb.ToString()
        End Function
        Protected Shared Function GetErrorMessage(ByVal info As TimeClockPunchInfo) As String
            Dim sb As New StringBuilder
            sb.Append(vbCrLf + "Error Processing ")
            sb.AppendFormat("BadgeId = " + info.BadgeId)
            sb.AppendFormat(", PunchTime = " + info.PunchTime.ToString())
            sb.AppendFormat(", PunchType = " + CType(info.PunchType, Integer).ToString)
            Return sb.ToString()
        End Function

        Protected Shared Function GetUpdateSql(ByVal info As TimeClockPunchInfo) As String
            Dim sb As New StringBuilder
            sb.Append("UPDATE [arStudentTimeClockPunches] " + vbCrLf)
            sb.Append("SET " + vbCrLf)
            sb.AppendFormat("     ClockId = '{0}', {1}", info.ClockId, vbCrLf)
            sb.AppendFormat("     Status = {0}, {1}", CType(info.Status, Integer), vbCrLf)
            If info.StuEnrollId = "" Then
                sb.AppendFormat("     StuEnrollId = null {0}", vbCrLf)
            Else
                sb.AppendFormat("     StuEnrollId = '{0}' {1}", info.StuEnrollId, vbCrLf)
            End If
            sb.AppendFormat("WHERE " + vbCrLf)
            sb.AppendFormat("     BadgeId = '{0}' {1}", info.BadgeId, vbCrLf)
            sb.AppendFormat("     and PunchTime = '{0}' {1}", info.PunchTime.ToString(), vbCrLf)
            sb.AppendFormat("     and PunchType = {0} {1}", CType(info.PunchType, Integer), vbCrLf)
            sb.Append(vbCrLf)
            Return sb.ToString()
        End Function


        Protected Shared Function GetUpdateStatusSql(ByVal info As TimeClockPunchInfo) As String
            Dim sb As New StringBuilder
            sb.Append("UPDATE [arStudentTimeClockPunches] " + vbCrLf)
            sb.Append("SET " + vbCrLf)
            sb.AppendFormat("     Status = 0, {0}", vbCrLf)
            If info.StuEnrollId = "" Then
                sb.AppendFormat("     StuEnrollId = null {0}", vbCrLf)
            Else
                sb.AppendFormat("     StuEnrollId = '{0}' {1}", info.StuEnrollId, vbCrLf)
            End If
            sb.AppendFormat("WHERE " + vbCrLf)
            sb.AppendFormat("     BadgeId = '{0}' {1}", info.BadgeId, vbCrLf)
            sb.AppendFormat("   and Datepart(yyyy,PunchTime)=Datepart(yyyy,'" + info.PunchTime.ToString() + "')")
            sb.AppendFormat("and Datepart(mm,PunchTime)=Datepart(mm,'" + info.PunchTime.ToString() + "')")
            sb.AppendFormat("and Datepart(dd,PunchTime)=Datepart(dd,'" + info.PunchTime.ToString() + "')")
            sb.AppendFormat("and (Status = 5 or Status = 8)  ")
            sb.Append(vbCrLf)
            Return sb.ToString()
        End Function

        Protected Shared Function GetUpdateStatusSql_byClass(ByVal info As TimeClockPunchInfo) As String
            Dim sb As New StringBuilder
            sb.Append("UPDATE [arStudentTimeClockPunches] " + vbCrLf)
            sb.Append("SET " + vbCrLf)
            sb.AppendFormat("     Status = 0, {0}", vbCrLf)
            If info.StuEnrollId = "" Then
                sb.AppendFormat("     StuEnrollId = null {0}", vbCrLf)
            Else
                sb.AppendFormat("     StuEnrollId = '{0}' {1}", info.StuEnrollId, vbCrLf)
            End If
            sb.AppendFormat("WHERE " + vbCrLf)
            sb.AppendFormat("     BadgeId = '{0}' {1}", info.BadgeId, vbCrLf)
            sb.AppendFormat("   and Datepart(yyyy,PunchTime)=Datepart(yyyy,'" + info.PunchTime.ToString() + "')")
            sb.AppendFormat("and Datepart(mm,PunchTime)=Datepart(mm,'" + info.PunchTime.ToString() + "')")
            sb.AppendFormat("and Datepart(dd,PunchTime)=Datepart(dd,'" + info.PunchTime.ToString() + "')")
            sb.AppendFormat("and (Status = 5 or Status = 8)  ")
            sb.AppendFormat("  and    ClsSectMeetingID = '{0}' {1}", info.ClsSectMeetingId, vbCrLf)
            sb.AppendFormat("  and    SpecialCode = '{0}' {1}", info.SpecialCode, vbCrLf)

            sb.Append(vbCrLf)
            Return sb.ToString()
        End Function

        Protected Shared Function UpdateProcessedandUnProcessedPunchestoStatus10(ByVal info As TimeClockPunchInfo) As String
            Dim sb As New StringBuilder

            sb.Append(" IF EXISTS ( SELECT  * ")
            sb.Append("         FROM arStudentTimeClockPunches ")
            sb.Append("     WHERE   StuEnrollid = '" + info.StuEnrollId + "' ")
            sb.Append("            AND CONVERT(VARCHAR, PunchTime, 101) = CONVERT(VARCHAR,'" + Format(info.PunchTime, "MM/dd/yyyy") + "', 101) ")
            sb.Append("          AND ClsSectmeetingID = '" + info.ClsSectMeetingId + "'  ")
            sb.Append("         AND Status = 1 )  ")
            sb.Append("     BEGIN ")
            sb.Append("    IF EXISTS ( SELECT  * ")
            sb.Append(" FROM arStudentTimeClockPunches ")
            sb.Append("              WHERE   StuEnrollid = '" + info.StuEnrollId + "' ")
            sb.Append("                      AND CONVERT(VARCHAR, PunchTime, 101) = CONVERT(VARCHAR,'" + Format(info.PunchTime, "MM/dd/yyyy") + "', 101) ")
            sb.Append("                      AND ClsSectmeetingID = '" + info.ClsSectMeetingId + "' ")
            sb.Append("                      AND Status NOT IN ( 0, 1 ) )  ")
            sb.Append("       BEGIN ")
            sb.Append("       UPDATE arStudentTimeClockPunches ")
            sb.Append("    Set   Status = 10 ")
            sb.Append("          WHERE   StuEnrollid  = '" + info.StuEnrollId + "' ")
            sb.Append("                   AND CONVERT(VARCHAR, PunchTime, 101) = CONVERT(VARCHAR,'" + Format(info.PunchTime, "MM/dd/yyyy") + "', 101) ")
            sb.Append("                 AND ClsSectmeetingID = '" + info.ClsSectMeetingId + "'   ")
            sb.Append(vbCrLf)
            sb.Append("       UPDATE atClsSectAttendance ")
            sb.Append("    Set   Actual = 9999 ")
            sb.Append("          WHERE   StuEnrollid  = '" + info.StuEnrollId + "' ")
            sb.Append("                   AND CONVERT(VARCHAR, MeetDate, 101) = CONVERT(VARCHAR,'" + Format(info.PunchTime, "MM/dd/yyyy") + "', 101) ")
            sb.Append("                 AND ClsSectmeetingID = '" + info.ClsSectMeetingId + "'   ")
            sb.Append(vbCrLf)

            sb.Append("       End ")
            sb.Append(vbCrLf)
            sb.Append("      End ")
            sb.Append(vbCrLf)
            Return sb.ToString()
        End Function

        Public Shared Function GetPunchesInsertedandExceptions_ByClass() As DataTable

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     P.BadgeId, " & vbCrLf)
            sb.Append("     P.PunchTime, " & vbCrLf)
            sb.Append("     P.ClockId, " & vbCrLf)
            sb.Append("     P.PunchType, " + vbCrLf)
            sb.Append("     P.Status, " + vbCrLf)
            sb.Append("     P.StuEnrollId, " + vbCrLf)
            sb.Append("     P.FromSystem,P.ClsSectMeetingID,P.SpecialCode, " + vbCrLf)
            sb.Append("    SE.StartDate,SE.ExpGradDate,Id,  " + vbCrLf)

            sb.Append(" SE.CampusID, CONVERT(VARCHAR,T1.TimeIntervalDescrip,108)AS StartTime,CONVERT(VARCHAR,T2.TimeIntervalDescrip,108)AS EndTime   ")

            sb.Append("FROM " + vbCrLf)
            sb.Append("     arStudentTimeClockPunches P, arStuEnrollments SE " + vbCrLf)

            sb.Append("  ,dbo.arClsSectMeetings CSM, Syperiods SP,dbo.cmTimeInterval T1, dbo.cmTimeInterval T2 ")

            sb.Append("WHERE " + vbCrLf)
            sb.Append("   P.Status IN (2,5,6,7,8,9,10)  ")
            sb.Append("     AND  P.StuEnrollid=SE.StuEnrollId and P.ClsSectMeetingID is not Null " + vbCrLf)

            sb.Append(" And P.ClsSectMeetingID=CSM.ClsSectMeetingID And ")
            sb.Append("  CSM.PeriodId=SP.PeriodId AND SP.StartTimeId=T1.TimeIntervalId AND SP.EndTimeId=T2.TimeIntervalId  ")
            sb.Append("ORDER BY P.StuEnrollId,P.ClsSectMeetingID, P.PunchTime ASC, P.PunchType ASC " + vbCrLf)
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
        End Function
        Protected Shared Function GetInsertSql(ByVal info As TimeClockPunchInfo) As String
            Dim sb As New StringBuilder
            sb.Append("INSERT INTO [arStudentTimeClockPunches] " + vbCrLf)
            sb.Append("         ([BadgeId],[PunchTime],[PunchType] " + vbCrLf)
            sb.Append("         ,[ClockId],[Status],[StuEnrollId] " + vbCrLf)
            sb.Append("         ,[FromSystem],[ClsSectMeetingID],[SpecialCode]) " + vbCrLf)
            sb.Append("VALUES " + vbCrLf)
            sb.AppendFormat("     ('{0}', {1}", info.BadgeId, vbCrLf)
            sb.AppendFormat("     '{0}', {1}", info.PunchTime.ToString(), vbCrLf)
            sb.AppendFormat("     {0}, {1}", CType(info.PunchType, Integer), vbCrLf)
            sb.AppendFormat("     '{0}', {1}", info.ClockId, vbCrLf)
            sb.AppendFormat("     {0}, {1}", CType(info.Status, Integer), vbCrLf)
            If info.StuEnrollId = "" Then
                sb.AppendFormat("     {0}, {1}", "null", vbCrLf)
            Else
                sb.AppendFormat("     '{0}', {1}", info.StuEnrollId, vbCrLf)

            End If
            sb.AppendFormat("     '{0}' {1}", info.FromSystem, vbCrLf)
            If info.ClsSectMeetingId = "" Then
                sb.AppendFormat("    , {0}, {1}", "null", vbCrLf)
            Else
                sb.AppendFormat("    , '{0}', {1}", info.ClsSectMeetingId, vbCrLf)

            End If

            If info.SpecialCode = "" Then
                sb.AppendFormat("     {0}) {1}", "null", vbCrLf)
            Else
                sb.AppendFormat("     '{0}') {1}", info.SpecialCode, vbCrLf)

            End If

            sb.Append(vbCrLf)
            Return sb.ToString()
        End Function
#End Region

        Public Shared Function SaveTimeClockPunches(ByVal info() As TimeClockPunchInfo) As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim connstr = MyAdvAppSettings.AppSettings("ConString")
            Dim conn As New OleDbConnection(connstr)
            Dim sErrDisp As String
            Try
                conn.Open()
                Dim trans As OleDb.OleDbTransaction = conn.BeginTransaction()
                For Each i As TimeClockPunchInfo In info
                    Dim sql As String = GetAddOrUpdateSql(i)
                    Dim cmd As New OleDbCommand(sql, conn, trans)
                    sErrDisp = GetErrorMessage(i)
                    cmd.ExecuteNonQuery()
                    sql = String.Empty
                Next

                trans.Commit()
                conn.Close()
                Return ""
            Catch ex As System.Exception
                conn.Close()
                Return ex.Message
            End Try
            Return "Unspecified Error"
        End Function

        Public Shared Function SaveTimeClockPunches_ByClass(ByVal info() As TimeClockPunchInfo) As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim connstr = MyAdvAppSettings.AppSettings("ConString")
            Dim conn As New OleDbConnection(connstr)
            Dim trans As OleDb.OleDbTransaction
            Dim sErrDisp As String
            Try
                conn.Open()

                Dim icount As Integer

                trans = conn.BeginTransaction()
                For Each i As TimeClockPunchInfo In info
                    Dim sql As String = GetAddOrUpdateSql_byClass(i)
                    Dim cmd As New OleDbCommand(sql, conn, trans)
                    sErrDisp = GetErrorMessage(i)
                    cmd.ExecuteNonQuery()
                    sql = String.Empty
                    sErrDisp = String.Empty
                Next

                trans.Commit()

                trans = conn.BeginTransaction()
                For Each i As TimeClockPunchInfo In info
                    Dim cmd As New OleDbCommand(UpdateProcessedandUnProcessedPunchestoStatus10(i), conn, trans)
                    cmd.ExecuteNonQuery()
                Next
                trans.Commit()
                conn.Close()
                Return ""
            Catch ex As System.Exception
                conn.Close()
                Return ex.Message + sErrDisp
            End Try
            Return "Unspecified Error"
        End Function
        Public Shared Function CheckIfStudentIsConversionStudent(ByVal StuEnrollId As String) As Integer
            Dim sb As New StringBuilder
            Dim db As New DataAccess
            Dim intRowCount As Integer = 0
            db.OpenConnection()
            With sb
                .Append(" Select Count(*) from atConversionAttendance where StuEnrollId=? ")
            End With
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                intRowCount = db.RunParamSQLScalar(sb.ToString)
            Catch ex As System.Exception
                intRowCount = 0
            Finally
                sb.Remove(0, sb.Length)
                db.ClearParameters()
                db.CloseConnection()
            End Try
            Return intRowCount
        End Function
        Public Function GetAttendanceByDay(ByVal StuEnrollId As String, ByVal totalHours As Decimal, ByVal day As DateTime) As String
            Dim sb As New StringBuilder
            Dim intRowCount As Integer = 0
            intRowCount = CheckIfStudentIsConversionStudent(StuEnrollId)
            If intRowCount >= 1 Then
            Else
            End If
        End Function

        Public Shared Function InsertConversionAttendance(ByVal StuEnrollId As String, ByVal totalHours As Decimal, ByVal day As DateTime) As String
            Dim db As New DataAccess
            Dim intRecCount As Integer = 0
            Dim sb As New StringBuilder
            Dim sb1 As New StringBuilder
            db.OpenConnection()

            With sb
                .Append("Select Count(*) from atConversionAttendance where StuEnrollId=? and MeetDate=? ")
            End With
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@MeetDate", day, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Try
                intRecCount = db.RunParamSQLScalar(sb.ToString)
                If intRecCount >= 1 Then
                    With sb1
                        .Append(" Update atConversionAttendance set Actual=? where StuEnrollId=? and MeetDate=? ")
                    End With
                    db.AddParameter("@Actual", totalHours, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@MeetDate", day, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb1.ToString)
                    Catch ex As System.Exception
                    Finally
                        db.ClearParameters()
                        sb1.Remove(0, sb1.Length)
                    End Try
                Else
                    With sb1
                        .Append(" Insert into atConversionAttendance(AttendanceId,StuEnrollId,MeetDate,Actual, ")
                        .Append(" Tardy,Excused,Schedule,PostByException)  ")
                        .Append(" values(?,?,?,?,?,?,?,?) ")
                    End With
                    db.AddParameter("@AttendanceId", Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@MeetDate", day, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@Actual", totalHours, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    db.AddParameter("@Tardy", 0, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                    db.AddParameter("@Excused", 0, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                    db.AddParameter("@Schedule", 0.0, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    db.AddParameter("@PostByException", "no", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb1.ToString)
                    Catch ex As System.Exception
                    Finally
                        db.ClearParameters()
                        sb1.Remove(0, sb1.Length)
                    End Try
                End If
            Catch ex As System.Exception
                intRecCount = 0
            Finally
                db.ClearParameters()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
                sb.Remove(0, sb.Length)
            End Try
        End Function
        Public Shared Function dsTardyDate(ByVal PrgVerId As String, ByVal StuEnrollId As String) As DataSet
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim dsPunch As New DataSet
            Dim arrCheckTardyIn As New ArrayList

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                With sb
                    .Append("   Select Distinct t2.dw,t4.PunchedInTime,t2.DeadLineBeforeConsideredTardy, " + vbCrLf)
                    .Append("   t2.TimeTakenAsPunchIn,PunchInDay " + vbCrLf)
                    .Append("   from arProgSchedules t1, " + vbCrLf)
                    .Append("   (select Distinct ScheduleId,dw, " + vbCrLf)
                    .Append("   Convert(char(10),Max_beforeTardy,108) as DeadLineBeforeConsideredTardy, " + vbCrLf)
                    .Append("   Convert(char(10),tardy_intime,108) as TimeTakenAsPunchIn " + vbCrLf)
                    .Append("   from arProgScheduleDetails where Check_TardyIn=1) t2,arStudentSchedules t3, " + vbCrLf)
                    .Append(" (select Distinct Convert(char(10),PunchTime,101) as PunchInDay,StuEnrollId," + vbCrLf)
                    .Append(" (Select Top 1 Convert(char(10),PunchTime,108) from arStudentTimeClockPunches where PunchType=1 and " + vbCrLf)
                    .Append(" Convert(DATE,PunchTime,111)=Convert(DATE,t5.PunchTime,111) and StuEnrollId=t5.StuEnrollId  " + vbCrLf)
                    .Append(" ) as PunchedInTime " + vbCrLf)
                    .Append("   from arStudentTimeClockPunches t5 where PunchType=1) t4 " + vbCrLf)
                    .Append("   where t1.ScheduleId=t2.ScheduleId and t2.ScheduleId=t3.ScheduleId and " + vbCrLf)
                    .Append("   t3.StuEnrollId=t4.StuEnrollId and t1.PrgVerId=? and " + vbCrLf)
                    .Append("   t3.StuEnrollId=? " + vbCrLf)
                    .Append("   and t4.PunchedInTime > t2.DeadLineBeforeConsideredTardy " + vbCrLf)
                    .Append("   Order by t2.dw ")
                End With
                db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                dsPunch = db.RunParamSQLDataSet(sb.ToString)
                Return dsPunch
            Catch ex As System.Exception
            Finally
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
        End Function

        Public Shared Function dsTardyDate_byClass(ByVal ClSSectMeetingID As String, ByVal StuEnrollId As String) As DataSet
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim dsPunch As New DataSet
            Dim arrCheckTardyIn As New ArrayList

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                With sb
                    .Append("  SELECT DISTINCT   ")
                    .Append("  t2.dw ,	")
                    .Append("  t4.PunchedInTime ,	")
                    .Append("  t2.DeadLineBeforeConsideredTardy ,	")
                    .Append("  t2.TimeTakenAsPunchIn ,	")
                    .Append("  PunchInDay	")
                    .Append("  FROM      dbo.arClsSectionTimeClockPolicy t1 ,	")
                    .Append("  ( SELECT  DISTINCT	")
                    .Append("  csm.ClsSectMeetingId ,	")
                    .Append("  WD.ViewOrder dw ,	")
                    .Append("  CONVERT(CHAR(10), CSTCP.MaxInBeforeTardy, 108) AS DeadLineBeforeConsideredTardy ,	")
                    .Append("  CONVERT(CHAR(10), AssignTardyInTime, 108) AS TimeTakenAsPunchIn ,	")
                    .Append("  Cstcp.AssignTardyInTime	")
                    .Append("  FROM      dbo.arClsSectMeetings CSM ,	")
                    .Append("  dbo.arClassSections CS ,	")
                    .Append("  dbo.syPeriods Sy ,	")
                    .Append("  syPeriodsWorkDays SYWD ,	")
                    .Append("  dbo.plWorkDays WD ,	")
                    .Append("  dbo.cmTimeInterval C1 ,	")
                    .Append("  dbo.cmTimeInterval C2 ,	")
                    .Append("  dbo.arClsSectionTimeClockPolicy CSTCP	")
                    .Append("  WHERE     cs.ClsSectionId = csm.ClsSectionId	")
                    .Append("  AND CSTCP.ClsSectionId = Cs.ClsSectionId	")
                    .Append("  AND Sy.PeriodId = csm.PeriodId	")
                    .Append("  AND C1.TimeIntervalId = Sy.StartTimeId	")
                    .Append("  AND C2.TimeIntervalId = Sy.EndTimeId	")
                    .Append("  AND Sy.PeriodId = SYWD.PeriodId	")
                    .Append("  AND SYWD.WorkDayId = wd.WorkDaysId	")
                    .Append("  AND CSm.ClsSectMeetingId = ?	")
                    .Append("  ) t2 ,	")
                    .Append("  dbo.arClsSectMeetings t3 ,	")
                    .Append("  ( SELECT DISTINCT	")
                    .Append("  CONVERT(CHAR(10), PunchTime, 101) AS PunchInDay ,	")
                    .Append("  StuEnrollId ,	")
                    .Append("  ( SELECT TOP 1	")
                    .Append("  CONVERT(CHAR(10), PunchTime, 108)	")
                    .Append("  FROM      arStudentTimeClockPunches	")
                    .Append("  WHERE     PunchType = 1	")
                    .Append("  AND CONVERT(DATE, PunchTime, 111) = CONVERT(DATE), t5.PunchTime, 111)	")
                    .Append("  AND StuEnrollId = t5.StuEnrollId	")
                    .Append("  AND ClsSectMeetingID = ?	")
                    .Append("  ) AS PunchedInTime ,	")
                    .Append("  ClsSectMeetingID	")
                    .Append("  FROM      arStudentTimeClockPunches t5	")
                    .Append("  WHERE     PunchType = 1	")
                    .Append("  ) t4	")
                    .Append("  WHERE     t1.ClsSectionId = t3.ClsSectionId	")
                    .Append("  AND t2.ClsSectMeetingId = t3.ClsSectMeetingId	")
                    .Append("  AND t4.ClsSectMeetingID = t3.ClsSectMeetingId	")
                    .Append("  AND t3.ClsSectMeetingId = ?		")
                    .Append("  AND t4.StuEnrollId = ?	")
                    .Append("  AND t4.PunchedInTime > t2.DeadLineBeforeConsideredTardy	")
                    .Append("  ORDER BY  t2.dw  ")


                End With
                db.AddParameter("@ClsSectMeetingId", ClSSectMeetingID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ClsSectMeetingId", ClSSectMeetingID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ClsSectMeetingId", ClSSectMeetingID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                dsPunch = db.RunParamSQLDataSet(sb.ToString)
                Return dsPunch
            Catch ex As System.Exception
            Finally
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
            End Try
        End Function

        Public Shared Function InsertStudentAttendance(ByVal StuEnrollId As String, ByVal totalHours As Decimal, ByVal day As DateTime) As String
            Dim db As New DataAccess
            Dim intRecCount As Integer = 0
            Dim sb As New StringBuilder
            Dim sb1 As New StringBuilder
            db.OpenConnection()

            With sb
                .Append("Select Count(*) from arStudentClockAttendance where StuEnrollId=? and RecordDate=? ")
            End With
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@MeetDate", day, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Try
                intRecCount = db.RunParamSQLScalar(sb.ToString)
                If intRecCount >= 1 Then
                    With sb1
                        .Append(" Update arStudentClockAttendance set ActualHours=? where StuEnrollId=? and RecordDate=? ")
                    End With
                    db.AddParameter("@Actual", totalHours, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@MeetDate", day, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb1.ToString)
                    Catch ex As System.Exception
                    Finally
                        db.ClearParameters()
                        sb1.Remove(0, sb1.Length)
                    End Try
                Else
                    With sb1
                        .Append(" Insert into arStudentClockAttendance(StuEnrollId,ScheduleId,RecordDate,SchedHours,ActualHours, ")
                        .Append(" ModDate,ModUser,IsTardy,PostByException)  ")
                        .Append(" values(?,?,?,?,?,?,?,?) ")
                    End With
                    db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@ScheduleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@RecordDate", day, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@SchedHours", totalHours, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    db.AddParameter("@ActualHours", totalHours, DataAccess.OleDbDataType.OleDbDecimal, , ParameterDirection.Input)
                    db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@ModUser", "sa", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    db.AddParameter("@IsTardy", 0, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                    db.AddParameter("@PostByException", "no", DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb1.ToString)
                    Catch ex As System.Exception
                    Finally
                        db.ClearParameters()
                        sb1.Remove(0, sb1.Length)
                    End Try
                End If
            Catch ex As System.Exception
                intRecCount = 0
            Finally
                db.ClearParameters()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
                sb.Remove(0, sb.Length)
            End Try
        End Function
        Public Shared Function SetPunchStatus(ByVal drPunches() As DataRow) As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim connstr = MyAdvAppSettings.AppSettings("ConString")
            Dim conn As New OleDbConnection(connstr)

            Try
                conn.Open()
                Dim trans As OleDb.OleDbTransaction = conn.BeginTransaction()
                For Each dr As DataRow In drPunches
                    Dim sb As New StringBuilder
                    sb.Append("update arStudentTimeClockPunches " + vbCrLf)
                    sb.AppendFormat("set Status = {0} {1}", CType(dr("Status"), Integer), vbCrLf)
                    sb.AppendFormat("where BadgeId='{0}' and PunchTime='{1}' {2}", dr("BadgeId"), dr("PunchTime"), vbCrLf)

                    Dim cmd As New OleDbCommand(sb.ToString, conn, trans)
                    cmd.ExecuteNonQuery()
                Next

                trans.Commit()
                conn.Close()
                Return ""
            Catch ex As Exception
                conn.Close()
                Return ex.Message
            End Try
            Return "Unspecified Error"
        End Function
        Public Shared Function SetPunchStatus_ByClass(ByVal drPunches() As DataRow) As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim connstr = MyAdvAppSettings.AppSettings("ConString")
            Dim conn As New OleDbConnection(connstr)

            Try
                conn.Open()
                Dim trans As OleDb.OleDbTransaction = conn.BeginTransaction()
                For Each dr As DataRow In drPunches
                    Dim sb As New StringBuilder
                    sb.Append("update arStudentTimeClockPunches " + vbCrLf)
                    sb.AppendFormat(" set Status = {0} {1}", CType(dr("Status"), Integer), vbCrLf)
                    sb.AppendFormat(" where BadgeId='{0}' and PunchTime='{1}'  and ClsSectMeetingID='{2}'{3}   and SpecialCode='{4}'{5}", dr("BadgeId"), dr("PunchTime"), dr("ClsSectMeetingID"), vbCrLf, dr("SpecialCode"), vbCrLf)

                    sb.Append(";" + vbCrLf)
                    Dim cmd As New OleDbCommand(sb.ToString, conn, trans)
                    cmd.ExecuteNonQuery()
                Next

                trans.Commit()

                trans = conn.BeginTransaction()
                For Each dr As DataRow In drPunches
                    Dim info As New TimeClockPunchInfo
                    info.StuEnrollId = dr("StuEnrollId").ToString
                    info.ClsSectMeetingId = dr("ClsSectMeetingID").ToString
                    info.PunchTime = dr("PunchTime")
                    Dim cmd As New OleDbCommand(UpdateProcessedandUnProcessedPunchestoStatus10(info), conn, trans)
                    cmd.ExecuteNonQuery()
                Next
                trans.Commit()
                conn.Close()




                Return ""
            Catch ex As Exception
                conn.Close()
                Return ex.Message
            End Try
            Return "Unspecified Error"
        End Function

        ''' <summary>
        ''' Commits total daily hours to the database
        ''' </summary>
        ''' <param name="StuEnrollId"></param>
        ''' <param name="ScheduleId"></param>
        ''' <param name="hrs"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function SaveTotalDailyHours(ByVal curDate As DateTime, ByVal StuEnrollId As String, ByVal ScheduleId As String,
                                                    ByVal hrs As Decimal, ByVal Tardy As Boolean, ByVal schedHours As Decimal, ByVal PrgVerId As String, ByVal UserName As String, ByVal CAmpusID As String) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim info As New ClockHourAttendanceInfo
                Dim sql As String = ""
                info.RecordDate = CType(curDate.ToShortDateString(), DateTime)
                info.StuEnrollId = StuEnrollId
                info.ScheduleId = ScheduleId
                info.ActualHours = hrs
                info.TardyProcessed = 0

                Dim dsPunch As New DataSet
                Dim strWeekDays As String = ""
                dsPunch = dsTardyDate(PrgVerId, StuEnrollId)
                If dsPunch.Tables(0).Rows.Count >= 1 Then
                    For Each drPunch As DataRow In dsPunch.Tables(0).Rows
                        strWeekDays += CType(drPunch("dw"), Integer).ToString & ","
                    Next
                    If Not strWeekDays = "" Then
                        strWeekDays = "(" + Mid(strWeekDays, 1, InStrRev(strWeekDays, ",") - 1).ToString + ")"
                    End If
                End If
                Dim decTardy As Decimal = 0.0
                Dim dw As Integer = Weekday(info.RecordDate, FirstDayOfWeek.Sunday)
                decTardy = CheckTardyIn(dsPunch, dw - 1, CType(info.RecordDate, Date))
                If decTardy > 0.0 Then
                    info.Tardy = 1
                Else
                    info.Tardy = 0
                End If
                info.SchedHours = schedHours
                sql = AR.AttendanceDB.GetAddOrUpdateSql(info, UserName, CAmpusID)

                db.RunParamSQLExecuteNoneQuery(sql)

                Dim strUpdateStatus As String = ""
                Try
                    strUpdateStatus = ChangeStatusToCurrentlyAttending(StuEnrollId)
                Catch ex As System.Exception
                End Try
                Return ""
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message + vbCrLf + "Error Saving Total Hours for Student Enroll Id: " + StuEnrollId
            End Try
            Return "Unhandled error"
        End Function


        Public Shared Function GetStartTimeForClassSectionMeeting_byClass(ByVal ClSSectMeetingID As String, ByVal CampusId As String) As String
            Dim sql As String = ""
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim StartTime As String
            If MyAdvAppSettings.AppSettings("SchedulingMethod", CampusId).ToString = "Traditional" Then
                Dim dt As New DataTable
                db.ClearParameters()
                db.AddParameter("@ClsSectMeetingID", ClSSectMeetingID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                dt = db.RunParamSQLDataSet("usp_GetStartTimeForClassSectionMeetingwithoutPeriods", Nothing, "SP").Tables(0)
                db.ClearParameters()
                StartTime = dt.Rows(0)(0).ToString
            Else
                Dim dt As New DataTable
                db.ClearParameters()
                db.AddParameter("@ClsSectMeetingID", ClSSectMeetingID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                dt = db.RunParamSQLDataSet("dbo.usp_GetStartTimeForClassSectionMeetingwithPeriods", Nothing, "SP").Tables(0)
                db.ClearParameters()
                StartTime = dt.Rows(0)(0).ToString
            End If

            Return StartTime
        End Function
        Public Shared Function GetStartTimeForClassSectionMeeting(ByVal ClSSectMeetingID As String, ByVal CampusId As String, selectWk As Integer) As String
            Dim sql As String = ""
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim StartTime As String
            If MyAdvAppSettings.AppSettings("SchedulingMethod", CampusId).ToString = "Traditional" Then
                Dim dt As New DataTable
                db.ClearParameters()
                db.AddParameter("@ClsSectMeetingID", ClSSectMeetingID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                dt = db.RunParamSQLDataSet("usp_GetStartTimeForClassSectionMeetingwithoutPeriods", Nothing, "SP").Tables(0)
                db.ClearParameters()
                StartTime = dt.Rows(0)(0).ToString
            Else
                Dim dt As New DataTable
                db.ClearParameters()
                db.AddParameter("@ClsSectMeetingID", ClSSectMeetingID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@selectWeek", selectWk, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                dt = db.RunParamSQLDataSet("dbo.usp_GetStartTimeForClassSectionMeeting", Nothing, "SP").Tables(0)
                db.ClearParameters()
                StartTime = dt.Rows(0)(0).ToString
            End If

            Return StartTime
        End Function


        Public Shared Function SaveTotalDailyHours_ByClass(ByVal curDate As DateTime, ByVal StuEnrollId As String, ByVal ClSSectMeetingID As String,
                                                  ByVal hrs As Decimal, ByVal Tardy As Boolean, ByVal schedHours As Decimal, ByVal UserName As String, ByVal CAmpusID As String, ByVal UnitTypeID As String, ByVal drpunches() As DataRow) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim info As New ClsSectAttendanceInfo
                Dim sql As String = ""

                Dim StartTime As String
                If MyAdvAppSettings.AppSettings("SchedulingMethod", CAmpusID).ToString = "Traditional" Then
                    Dim dt As New DataTable
                    db.ClearParameters()
                    db.AddParameter("@ClsSectMeetingID", ClSSectMeetingID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    dt = db.RunParamSQLDataSet("usp_GetStartTimeForClassSectionMeetingwithoutPeriods").Tables(0)
                    db.ClearParameters()
                    StartTime = dt.Rows(0)(0).ToString
                Else
                    Dim dt As New DataTable
                    db.ClearParameters()
                    db.AddParameter("@ClsSectMeetingID", ClSSectMeetingID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    dt = db.RunParamSQLDataSet("dbo.usp_GetStartTimeForClassSectionMeetingwithPeriods", Nothing, "SP").Tables(0)
                    db.ClearParameters()
                    StartTime = dt.Rows(0)(0).ToString
                End If

                Dim dtStart As System.DateTime = DateTime.Parse(Format(curDate, "MM/dd/yyyy") + " " + StartTime)

                info.MeetDate = dtStart
                info.StuEnrollId = New Guid(StuEnrollId)
                info.ClsSectMeetingId = New Guid(ClSSectMeetingID)
                info.Actual = hrs
                If UnitTypeID = AdvantageCommonValues.MinutesGuid Then
                    info.UnitTypeId = "minutes"
                ElseIf UnitTypeID = AdvantageCommonValues.ClockHoursGuid Then
                    info.UnitTypeId = "hours"
                ElseIf UnitTypeID = AdvantageCommonValues.PresentAbsentGuid Then
                    info.UnitTypeId = "present"
                End If



                Dim dsPunch As New DataSet
                Dim strWeekDays As String = ""
                dsPunch = dsTardyDate_byClass(ClSSectMeetingID, StuEnrollId)
                If Not dsPunch Is Nothing AndAlso dsPunch.Tables(0).Rows.Count >= 1 Then
                    For Each drPunch As DataRow In dsPunch.Tables(0).Rows
                        strWeekDays += CType(drPunch("dw"), Integer).ToString & ","
                    Next
                    If Not strWeekDays = "" Then
                        strWeekDays = "(" + Mid(strWeekDays, 1, InStrRev(strWeekDays, ",") - 1).ToString + ")"
                    End If
                End If
                Dim decTardy As Decimal = 0.0
                Dim dw As Integer = Weekday(info.MeetDate, FirstDayOfWeek.Sunday)
                decTardy = CheckTardyIn(dsPunch, dw - 1, CType(info.MeetDate, Date))
                If decTardy > 0.0 Then
                    info.Tardy = 1
                Else
                    info.Tardy = 0
                End If
                info.Scheduled = schedHours
                sql = AR.AttendanceDB.GetAddOrUpdateSql_byClass(info, UserName, CAmpusID, drpunches)

                db.RunParamSQLExecuteNoneQuery(sql)

                Dim strUpdateStatus As String = ""
                Try
                    strUpdateStatus = ChangeStatusToCurrentlyAttending(StuEnrollId)
                Catch ex As System.Exception
                    Console.WriteLine(ex.Message)
                End Try
                Return "" ' return success
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message + vbCrLf + "Error Saving Total Hours for Student Enroll Id: " + StuEnrollId
            End Try
            Return "Unhandled error"
        End Function


        Public Shared Function markStudentAbsent(ByVal StuEnrollId As String, ByVal ScheduleId As String, ByVal RecordDate As Date, ByVal UserName As String) As String
            Dim db As New DataAccess
            Dim sb As New System.Text.StringBuilder

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim groupTrans As OleDbTransaction = db.StartTransaction()
            Try

                With sb

                    .Append("Update arStudentClockAttendance set ActualHours=0.0, ")
                    .Append(" ModDate=? ,ModUser= ?  where ")
                    .Append(" StuEnrollId=? and ScheduleId=? and RecordDate=? ;")

                    .Append("Update arStudentClockAttendance set TardyProcessed=1, ")
                    .Append(" ModDate=? ,ModUser= ?  where ")
                    .Append(" StuEnrollId=? and ScheduleId=? and isTardy=1  ;")

                End With

                db.AddParameter("@ModDate", DateTime.Now.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModUser", UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@stdenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ScheduleId", ScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@RecordDate", RecordDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                db.AddParameter("@ModDate", DateTime.Now.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ModUser", UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@stdenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ScheduleId", ScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
                groupTrans.Commit()
                Return ""
            Catch ex As OleDbException
                groupTrans.Rollback()
                If Not groupTrans.Connection Is Nothing Then
                End If
                Return DALExceptions.BuildErrorMessage(ex)
                db.CloseConnection()
            End Try
        End Function
        Public Shared Function GetDefaultCurrentlyAttendingStatus(ByVal StuEnrollId As String) As String
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim currentAttendingDefaultStatusQuery As New StringBuilder
            Dim programVersionIdQuery As New StringBuilder
            Dim campusGroupIdQuery As New StringBuilder
            Dim campusGroupId = ""
            Dim programVersionId = ""
            Dim currentlyAttendingStatusId = ""

            With programVersionIdQuery
                .Append("SELECT PrgVerId FROM dbo.arStuEnrollments WHERE StuEnrollId = ?")
            End With

            Try
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Dim programVersionIdObject = db.RunParamSQLScalar(programVersionIdQuery.ToString)
                programVersionId = Convert.ToString(programVersionIdObject)
                db.ClearParameters()
            Catch ex As Exception
            End Try

            With campusGroupIdQuery
                .Append("SELECT CampGrpId FROM dbo.arPrgVersions WHERE PrgVerId  = ? ")
            End With

            Try
                db.AddParameter("@PrgVerId", programVersionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Dim campusGroupIdObject = db.RunParamSQLScalar(campusGroupIdQuery.ToString)
                campusGroupId = Convert.ToString(campusGroupIdObject)
                db.ClearParameters()
            Catch ex As Exception
            End Try

            With currentAttendingDefaultStatusQuery
                .Append("SELECT dbo.GetCurrentlyAttendingDefaultStatusIdForCampusGroup( ? )")
            End With

            Try
                db.AddParameter("@campusGroupId1", campusGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Dim currentAttendingDefaultStatusQueryIdObject = db.RunParamSQLScalar(currentAttendingDefaultStatusQuery.ToString)
                currentlyAttendingStatusId = Convert.ToString(currentAttendingDefaultStatusQueryIdObject)
                db.ClearParameters()
            Catch ex As Exception
            End Try
            Return currentlyAttendingStatusId
        End Function
        Public Shared Function ChangeStatusToCurrentlyAttending(ByVal StuEnrollId As String) As String
            Dim db As New DataAccess
            Dim sb As New System.Text.StringBuilder
            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim connString As String = MyAdvAppSettings.AppSettings("ConnectionString")
            Dim conn As New SqlConnection(connString)

            Try
                Dim cmd1 As SqlCommand = New SqlCommand("USP_ShouldStudentBeCurrentlyAttending", conn)
                cmd1.CommandType = CommandType.StoredProcedure
                cmd1.Parameters.Add("@EnrollmentId", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = StuEnrollId
                cmd1.Parameters.Add(New SqlParameter("@ShouldStudentBeCurrentlyAttending", SqlDbType.Bit))
                cmd1.Parameters("@ShouldStudentBeCurrentlyAttending").Direction = ParameterDirection.Output
                conn.Open()
                cmd1.ExecuteNonQuery()

                Dim shouldStudentBeCurrentlyAttending = False
                shouldStudentBeCurrentlyAttending = Convert.ToBoolean(cmd1.Parameters("@ShouldStudentBeCurrentlyAttending").Value)

                If (Not shouldStudentBeCurrentlyAttending) Then
                    Return ""
                End If

            Catch ex As Exception
                conn.Close()
                Return ""
            Finally
                conn.Close()
            End Try

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim groupTrans As OleDbTransaction = db.StartTransaction()
            Try
                Dim currentlyAttendingStatusId = GetDefaultCurrentlyAttendingStatus(StuEnrollId)
                With sb
                    .Append("UPDATE arStuEnrollments Set StatusCodeId = ? ")
                    .Append("WHERE StuEnrollId = ? and StatusCodeId in (SELECT  A.StatusCodeID AS StatusCodeID FROM   syStatusCodes A WHERE  A.sysStatusId IN (10, 7))  ")
                End With
                db.AddParameter("@currentlyAttendingStatusId1", currentlyAttendingStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@stdenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
                With sb
                    .Append("UPDATE arStuEnrollments SET StartDate = ExpStartDate ")
                    .Append("WHERE StuEnrollId = ? AND StartDate IS NULL  ")
                End With
                db.AddParameter("@stdenrollid", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                Dim existingStatusQuery As New StringBuilder
                Dim campusQuery As New StringBuilder
                Dim startDateQuery As New StringBuilder
                Dim campusId = ""
                Dim existingStatusId = ""
                Dim startdate As DateTime
                With existingStatusQuery
                    .Append("SELECT TOP 1 NewStatusId FROM syStudentStatusChanges WHERE StuEnrollId = ? ORDER BY DateOfChange DESC")
                End With

                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Dim existingStatusIdObject = db.RunParamSQLScalar(existingStatusQuery.ToString, groupTrans)
                existingStatusId = Convert.ToString(existingStatusIdObject)
                db.ClearParameters()

                With campusQuery
                    .Append("SELECT CampusId FROM arStuEnrollments WHERE StuEnrollId = ?")
                End With
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Dim campusIdObject = db.RunParamSQLScalar(campusQuery.ToString, groupTrans)
                campusId = Convert.ToString(campusIdObject)
                db.ClearParameters()

                With startDateQuery
                    .Append("SELECT StartDate FROM arStuEnrollments WHERE StuEnrollId = ?")
                End With
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Dim startDateObject = db.RunParamSQLScalar(startDateQuery.ToString, groupTrans)
                Dim res As Date
                startdate = If(Date.TryParse(Convert.ToString(startDateObject), res), res, Date.Now)
                db.ClearParameters()

                If (existingStatusId <> currentlyAttendingStatusId) Then
                    With sb
                        .Append("INSERT INTO syStudentStatusChanges (StudentStatusChangeId, StuEnrollId, OrigStatusId, NewStatusId, CampusId, ModDate, ModUser, IsReversal,DropReasonId, DateOfChange, Lda, CaseNumber, RequestedBy, HaveBackup, HaveClientConfirmation) ")
                        .Append("VALUES (?,?, ?, ?, ?, ?, ?, ?,NULL, ?,NULL,NULL,NULL,NULL,NULL)")
                    End With
                    db.AddParameter("@statusChangeId", Guid.NewGuid(), DataAccess.OleDbDataType.OleDbGuid, 50, ParameterDirection.Input)
                    db.AddParameter("@stuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@oldStatusId", existingStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@newStatusId", currentlyAttendingStatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@modDate", DateTime.Now, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
                    db.AddParameter("@modUser", HttpContext.Current.Session("UserName"), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@isReversal", False, DataAccess.OleDbDataType.OleDbBoolean, 50, ParameterDirection.Input)
                    db.AddParameter("@dateOfChange", startdate, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
                    db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                End If
                groupTrans.Commit()
                Return ""
            Catch ex As OleDbException
                groupTrans.Rollback()
                If Not groupTrans.Connection Is Nothing Then
                End If
                Return DALExceptions.BuildErrorMessage(ex)
                db.CloseConnection()
            End Try
        End Function
        Public Shared Function CheckTardyIn(ByVal dsPunch As DataSet, ByVal dw As Integer, ByVal dtDate As Date) As Decimal
            If Not dsPunch Is Nothing AndAlso dsPunch.Tables(0).Rows.Count >= 1 Then
                Dim dtPunch As DataTable = dsPunch.Tables(0)
                Dim keys(2) As DataColumn
                keys(0) = dtPunch.Columns("PunchInDay")
                keys(1) = dtPunch.Columns("dw")
                dtPunch.PrimaryKey = keys

                Dim findTheseVals(1) As Object

                findTheseVals(0) = dtDate.ToString("d", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                findTheseVals(1) = dw

                Dim drRow As DataRow = dsPunch.Tables(0).Rows.Find(findTheseVals)
                If Not drRow Is Nothing Then
                    If dw = CType(drRow("dw"), Integer) Then
                        Dim TimeToBeConsideredPunchInTime As Date = "00:00:00"
                        Dim ActualPunchInTime As Date = "00:00:00"
                        Dim DeadLineBeforeConsideredTardy As Date = "00:00:00"
                        If Not drRow("PunchedInTime") Is System.DBNull.Value Then ActualPunchInTime = CType(drRow("PunchedInTime"), DateTime)
                        If Not drRow("DeadLineBeforeConsideredTardy") Is System.DBNull.Value Then DeadLineBeforeConsideredTardy = CType(drRow("DeadLineBeforeConsideredTardy"), DateTime)
                        If Not drRow("TimeTakenAsPunchIn") Is System.DBNull.Value Then TimeToBeConsideredPunchInTime = CType(drRow("TimeTakenAsPunchIn"), DateTime)
                        Dim decTardy As Decimal = ComputeTardy(ActualPunchInTime, DeadLineBeforeConsideredTardy, TimeToBeConsideredPunchInTime)
                        Return decTardy
                        Exit Function
                    Else
                        Return 0.0
                        Exit Function
                    End If
                End If
            Else
                Return 0.0
                Exit Function
            End If
        End Function
        Public Shared Function ComputeTardy(ByVal ActualPunchInTime As Date, ByVal DeadLineBeforeConsideredTardy As Date, ByVal TimeToBeConsideredPunchInTime As Date) As Decimal
            Dim decTardyValue As Decimal = 0.0
            If Not ActualPunchInTime = "00:00:00" And Not TimeToBeConsideredPunchInTime = "00:00:00" And TimeToBeConsideredPunchInTime > ActualPunchInTime Then
                decTardyValue = ((TimeToBeConsideredPunchInTime - ActualPunchInTime).TotalHours)
            ElseIf Not ActualPunchInTime = "00:00:00" And Not TimeToBeConsideredPunchInTime = "00:00:00" And TimeToBeConsideredPunchInTime < ActualPunchInTime Then
                decTardyValue = ((ActualPunchInTime - TimeToBeConsideredPunchInTime).TotalHours)
            ElseIf Not ActualPunchInTime = "00:00:00" And TimeToBeConsideredPunchInTime = "00:00:00" And Not DeadLineBeforeConsideredTardy = "00:00:00" And DeadLineBeforeConsideredTardy > ActualPunchInTime Then
                decTardyValue = ((DeadLineBeforeConsideredTardy - ActualPunchInTime).TotalHours)
            ElseIf Not ActualPunchInTime = "00:00:00" And TimeToBeConsideredPunchInTime = "00:00:00" And Not DeadLineBeforeConsideredTardy = "00:00:00" And DeadLineBeforeConsideredTardy < ActualPunchInTime Then
                decTardyValue = ((ActualPunchInTime - DeadLineBeforeConsideredTardy).TotalHours)
            End If
            Return decTardyValue
        End Function
        ''' <summary>
        ''' Returns all the punches.  StuEnrollId is optional and can be left blank.
        ''' </summary>
        ''' <param name="StuEnrollId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetPunches(ByVal StuEnrollId As String) As DataTable
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     SE.StuEnrollId, " & vbCrLf)
            sb.Append("     (select t.SysStatusId from syStatusCodes t where t.StatusCodeId=SE.StatusCodeId) as SysStatusId, " & vbCrLf)
            sb.Append("     STP.PunchTime, " & vbCrLf)
            sb.Append("     STP.ClockId, " + vbCrLf)
            sb.Append("     STP.PunchType, " + vbCrLf)
            sb.Append("     STP.Status " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arStudentTimeClockPunches STP, arStuEnrollments SE " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     STP.StuEnrollId = SE.StuEnrollId " + vbCrLf)

            If StuEnrollId IsNot Nothing AndAlso StuEnrollId <> "" Then
                sb.AppendFormat("     SE.StuEnrollId = '{0}' {1}", StuEnrollId, vbCrLf)
            End If
            sb.Append("ORDER BY " & vbCrLf)
            sb.Append("     SE.StuEnrollId, STP.PunchTime ")
            Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
        End Function

        ''' <summary>
        ''' Gets all punches for the specified status
        ''' </summary>
        ''' <param name="Status"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetPunches(ByVal Status As PunchStatus) As DataTable
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     P.BadgeId, " & vbCrLf)
            sb.Append("     P.PunchTime, " & vbCrLf)
            sb.Append("     P.ClockId, " & vbCrLf)
            sb.Append("     P.PunchType, " + vbCrLf)
            sb.Append("     P.Status, " + vbCrLf)
            sb.Append("     P.StuEnrollId, " + vbCrLf)
            sb.Append("     P.FromSystem, " + vbCrLf)
            sb.Append("     SS.ScheduleId,SE.StartDate,SE.ExpGradDate,Id  " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arStudentTimeClockPunches P, arStudentSchedules SS , arStuEnrollments SE " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.AppendFormat(" (    P.Status = {0} {1}", CType(Status, Integer), vbCrLf)
            sb.AppendFormat("   or   P.Status = 8 )")

            sb.Append("     AND SS.StuEnrollid = P.StuEnrollId  and SS.StuEnrollid=SE.StuEnrollId " + vbCrLf)
            sb.Append("     AND SS.Active = 1 " + vbCrLf)
            sb.Append("ORDER BY P.StuEnrollId, P.PunchTime ASC, P.PunchType ASC " + vbCrLf)
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
        End Function

        Public Shared Function GetPunches_ByClass(ByVal Status As PunchStatus) As DataTable
            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     P.BadgeId, " & vbCrLf)
            sb.Append("     P.PunchTime, " & vbCrLf)
            sb.Append("     P.ClockId, " & vbCrLf)
            sb.Append("     P.PunchType, " + vbCrLf)
            sb.Append("     P.Status, " + vbCrLf)
            sb.Append("     P.StuEnrollId, " + vbCrLf)
            sb.Append("     P.FromSystem,P.ClsSectMeetingID,P.SpecialCode, " + vbCrLf)
            sb.Append("    SE.StartDate,SE.ExpGradDate,Id  " + vbCrLf)
            sb.Append("FROM " + vbCrLf)
            sb.Append("     arStudentTimeClockPunches P, arStuEnrollments SE " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.AppendFormat(" (    P.Status = {0} {1}", CType(Status, Integer), vbCrLf)
            sb.AppendFormat("   or   P.Status = 8 )")
            sb.Append("     AND  P.StuEnrollid=SE.StuEnrollId and P.ClsSectMeetingID is not Null " + vbCrLf)
            sb.Append("ORDER BY P.StuEnrollId,P.ClsSectMeetingID, P.PunchTime ASC, P.PunchType ASC " + vbCrLf)
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
        End Function


        ''' <summary>
        ''' Update a TimeClockPunchInfo object to the database.
        ''' </summary>
        ''' <param name="info"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function UpdatePunch(ByVal info As TimeClockPunchInfo) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sb As New StringBuilder
                sb.Append("update " & vbCrLf)
                sb.Append("     arStudentTimeClockPunches " + vbCrLf)
                sb.Append("set " & vbCrLf)
                sb.Append("     ClockId = ?, " & vbCrLf)
                sb.Append("     PunchType = ?, " & vbCrLf)
                sb.Append("     Status = ? " & vbCrLf)
                sb.Append("where " + vbCrLf)
                sb.Append("     StuEnrollId = ? " + vbCrLf)
                sb.Append("     and PunchTime = ? " + vbCrLf)

                db.AddParameter("@ClockId", info.ClockId, DataAccess.OleDbDataType.OleDbString, 20, ParameterDirection.Input)
                db.AddParameter("@PunchType", CType(info.PunchType, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@Status", CType(info.Status, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchTime", info.PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString)

                Return ""
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function

        Public Shared Function UpdatePunch_byClass(ByVal info As TimeClockPunchInfo) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sb As New StringBuilder
                sb.Append("update " & vbCrLf)
                sb.Append("     arStudentTimeClockPunches " + vbCrLf)
                sb.Append("set " & vbCrLf)
                sb.Append("     ClockId = ?, " & vbCrLf)
                sb.Append("     PunchType = ?, " & vbCrLf)
                sb.Append("     Status = ? " & vbCrLf)
                sb.Append("where " + vbCrLf)
                sb.Append("     StuEnrollId = ? " + vbCrLf)
                sb.Append("     and PunchTime = ? " + vbCrLf)
                sb.Append("     and ClsSectMeetingID = ? " + vbCrLf)

                db.AddParameter("@ClockId", info.ClockId, DataAccess.OleDbDataType.OleDbString, 20, ParameterDirection.Input)
                db.AddParameter("@PunchType", CType(info.PunchType, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@Status", CType(info.Status, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ClsSectMeetingId", info.ClsSectMeetingId, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString)

                Return ""
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function

        ''' <summary>
        ''' Adds a new punch object to the database
        ''' </summary>
        ''' <param name="info"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function AddPunch(ByVal info As TimeClockPunchInfo) As String

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim sb As New StringBuilder

                sb.Append("DELETE arStudentTimeClockPunches " + vbCrLf)
                If info.StuEnrollId = "" Then
                    sb.AppendFormat("WHERE StuEnrollId = null ")
                Else
                    sb.AppendFormat("WHERE StuEnrollId ='{0}'", info.StuEnrollId)
                End If
                sb.AppendFormat(" and  PunchTime='{0}' and PunchType={1} {2}",
                                     info.PunchTime, CType(info.PunchType, Integer), vbCrLf)

                sb.Append("INSERT arStudentTimeClockPunches (StuEnrollId, BadgeId, PunchTime, ClockId, PunchType, Status,FromSystem) " + vbCrLf)
                sb.Append("VALUES (?,?,?,?,?,?,?) ")

                If info.StuEnrollId = "" Then
                    db.AddParameter("@StuEnrollId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@StuEnrollId", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@BadgeId", info.BadgeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchTime", info.PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ClockId", info.ClockId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchType", CType(info.PunchType, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@Processed", CType(info.Status, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@FromSystem", info.FromSystem, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()

                Return "" '
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function

        Public Shared Function AddPunch_byClass(ByVal info As TimeClockPunchInfo, ByVal CampusID As String, ByRef Exceptionmessages As StringBuilder) As String

            Dim db As New DataAccess
            Dim ClsSectmeetingID As String = "Exception"

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


            Exceptionmessages.Append("")
            Try
                Dim sb As New StringBuilder

                Dim dbS As New SQLDataAccess

                dbS.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                Dim dsClsSectionId As New DataSet

                dbS.AddParameter("@StuEnrollId", info.StuEnrollId, SqlDbType.VarChar, 50, ParameterDirection.Input)
                dbS.AddParameter("@punch", info.PunchTime, SqlDbType.DateTime, , ParameterDirection.Input)
                dbS.AddParameter("@PunchSpecialCode", info.SpecialCode, SqlDbType.VarChar, 10, ParameterDirection.Input)
                dbS.AddParameter("@CampusID", CampusID, SqlDbType.VarChar, 50, ParameterDirection.Input)

                If Not info.StuEnrollId = "" Then
                    dsClsSectionId = dbS.RunParamSQLDataSet_SP("dbo.usp_GetStudentClsSectMeetingfromStudentPunchinfo")
                    If dsClsSectionId.Tables.Count > 0 Then

                        If dsClsSectionId.Tables(0).Rows.Count > 0 Then
                            ClsSectmeetingID = dsClsSectionId.Tables(0).Rows(0)(0).ToString

                        End If
                    End If
                End If

                dbS.ClearParameters()


                If ClsSectmeetingID = "Exception" Then

                    Exceptionmessages.Append("Badge Id:" + info.BadgeId.ToString + ", Punch Time : " + info.PunchTime.ToString.PadLeft(9, "0"c) + ", Exception: Class Meeting Not Identified " + vbCrLf)
                    sb.Append("INSERT arStudentTimeClockPunches (StuEnrollId, BadgeId, PunchTime, ClockId, PunchType, Status,FromSystem,ClsSectMeetingID,SpecialCode) " + vbCrLf)
                    sb.Append("VALUES (?,?,?,?,?,?,?,Null,?) ")

                    If info.StuEnrollId = "" Then
                        db.AddParameter("@StuEnrollId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Else
                        db.AddParameter("@StuEnrollId", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    End If
                    db.AddParameter("@BadgeId", info.BadgeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@PunchTime", info.PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@ClockId", info.ClockId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@PunchType", CType(info.PunchType, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    If info.StuEnrollId = "" Then
                        db.AddParameter("@Processed", PunchStatus.Inserted, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    Else
                        db.AddParameter("@Processed", PunchStatus.UnIdentifiedMeeting, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    End If

                    db.AddParameter("@FromSystem", info.FromSystem, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                    db.AddParameter("@SpecialCode", info.SpecialCode, DataAccess.OleDbDataType.OleDbString, 10, ParameterDirection.Input)
                Else

                    sb.Append("DELETE arStudentTimeClockPunches " + vbCrLf)
                    If info.StuEnrollId = "" Then
                        sb.AppendFormat("WHERE StuEnrollId = null ")
                    Else
                        sb.AppendFormat("WHERE StuEnrollId ='{0}'", info.StuEnrollId)
                    End If
                    sb.AppendFormat(" and  PunchTime='{0}' and PunchType={1} ",
                                         info.PunchTime, CType(info.PunchType, Integer))

                    sb.AppendFormat(" and  ClsSectmeetingID='{0}' and SpecialCode='{1}' {2}",
                                      ClsSectmeetingID, info.SpecialCode, vbCrLf)

                    sb.Append(";INSERT arStudentTimeClockPunches (StuEnrollId, BadgeId, PunchTime, ClockId, PunchType, Status,FromSystem,ClsSectMeetingID,SpecialCode) " + vbCrLf)
                    sb.Append("VALUES (?,?,?,?,?,?,?,?,?) ")

                    If info.StuEnrollId = "" Then
                        db.AddParameter("@StuEnrollId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Else
                        db.AddParameter("@StuEnrollId", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    End If
                    db.AddParameter("@BadgeId", info.BadgeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@PunchTime", info.PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@ClockId", info.ClockId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@PunchType", CType(info.PunchType, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    db.AddParameter("@Processed", CType(info.Status, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    db.AddParameter("@FromSystem", info.FromSystem, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                    db.AddParameter("@ClsSectMeetingID", ClsSectmeetingID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@SpecialCode", info.SpecialCode, DataAccess.OleDbDataType.OleDbString, 10, ParameterDirection.Input)


                End If

                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()

                Return ""
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return ""

        End Function


        Public Shared Function AddPunch_byClass_PopUp(ByVal info As TimeClockPunchInfo) As String

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim sb As New StringBuilder



                sb.Append("DELETE arStudentTimeClockPunches " + vbCrLf)
                If info.StuEnrollId = "" Then
                    sb.AppendFormat("WHERE StuEnrollId = null ")
                Else
                    sb.AppendFormat("WHERE StuEnrollId ='{0}'", info.StuEnrollId)
                End If
                sb.AppendFormat(" and  PunchTime='{0}' and PunchType={1} ",
                                     info.PunchTime, CType(info.PunchType, Integer))

                sb.AppendFormat(" and  ClsSectmeetingID='{0}' and SpecialCode='{1}' {2}",
                                  info.ClsSectMeetingId, info.SpecialCode, vbCrLf)

                sb.Append(";INSERT arStudentTimeClockPunches (StuEnrollId, BadgeId, PunchTime, ClockId, PunchType, Status,FromSystem,ClsSectMeetingID,SpecialCode) " + vbCrLf)
                sb.Append("VALUES (?,?,?,?,?,?,?,?,?) ")

                If info.StuEnrollId = "" Then
                    db.AddParameter("@StuEnrollId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@StuEnrollId", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@BadgeId", info.BadgeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchTime", info.PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ClockId", info.ClockId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchType", CType(info.PunchType, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@Processed", CType(info.Status, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@FromSystem", info.FromSystem, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                db.AddParameter("@ClsSectMeetingID", info.ClsSectMeetingId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@SpecialCode", info.SpecialCode, DataAccess.OleDbDataType.OleDbString, 10, ParameterDirection.Input)


                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()

                Return ""
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function
        ''' <summary>
        ''' Deletes a punch from the database
        ''' </summary>
        ''' <param name="StuEnrollId"></param>
        ''' <param name="PunchTime"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DeletePunch(ByVal StuEnrollId As String, ByVal PunchTime As DateTime) As String

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim Sql As String = "DELETE arStudentTimeClockPunches WHERE StuEnrollId = ? AND PunchTime = ? " + vbCrLf
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchTime", PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(Sql)
                db.CloseConnection()

                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function

        ''' <summary>
        ''' retrieves all active students (currently enrolled, loa or ??)
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetActiveStudents(ByVal Optional campusId As String = "") As DataTable
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            sb.Append("SELECT ")
            sb.Append("     SE.StuEnrollId, " & vbCrLf)
            sb.Append("     SE.StatusCodeId, " + vbCrLf)
            sb.Append("     SC.SysStatusId, " & vbCrLf)
            sb.Append("     abs(SE.BadgeNumber) BadgeNumber , " + vbCrLf)
            sb.Append("     SS.ScheduleId, " + vbCrLf)
            sb.Append("     SE.CampusId " + vbCrLf)
            sb.Append(" FROM " + vbCrLf)
            sb.Append("     arStuEnrollments SE, syStatusCodes SC, arStudentSchedules SS " + vbCrLf)
            sb.Append(" WHERE " + vbCrLf)
            sb.Append("     SC.StatusCodeId=SE.StatusCodeId " + vbCrLf)
            sb.Append("     AND SC.SysStatusId in (7,9,10,11,20) " + vbCrLf)
            sb.Append("     AND SS.StuEnrollId=SE.StuEnrollid " + vbCrLf)
            sb.Append(" and SS.ScheduleId in (Select PSD.ScheduleId from arProgScheduleDetails PSD, arProgSchedules PS, arprgversions PV  where PS.PrgVerId=PV.PrgVerId  and PSD.ScheduleId=PS.ScheduleId and PV.UseTimeClock=1 and   TimeIN is not null) ")

            'If(Not String.IsNullOrEmpty(campusId)) then
            '    sb.Append("     AND SE.CampusId = ? " + vbCrLf)
            '    db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'End if

            sb.Append(" ORDER BY " & vbCrLf)
            sb.Append("     SE.StuEnrollId")

            Dim ds As DataSet = db.RunSQLDataSet(sb.ToString())
            Return ds.Tables(0)
        End Function

        Public Function GetCountOfActiveEnrollment(ByVal badgeId As String, ByVal campusId As String)
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder

            sb.AppendLine("SELECT     COUNT(StudentId) as CountOfActiveEnrollments ")
            sb.AppendLine("    FROM       dbo.arStuEnrollments enrollments ")
            sb.AppendLine("INNER JOIN dbo.syStatusCodes codes ON codes.StatusCodeId = enrollments.StatusCodeId ")
            sb.AppendLine("WHERE      codes.SysStatusId IN ( 7, 9, 10, 11, 20 ) ")
            sb.AppendLine("AND enrollments.BadgeNumber = ? ")
            sb.AppendLine("               AND enrollments.CampusId = ?;")

            db.AddParameter("@badgeId", badgeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@campusId", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Dim ds As DataSet = db.RunParamSQLDataSet(sb.ToString())
            Return ds.Tables(0)
        End Function
        Public Function GetPunchesForaStudentonaGivenDate(ByVal StuEnrollid As String, ByVal SchDate As Date) As DataSet

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            With sb
                .Append("SELECT DISTINCT ")
                .Append("     BadgeId,ClockId,PunchTime,PunchType,Status,ST.StuEnrollId,FromSystem,SCA.ScheduleId ")
                .Append("  FROM       arStudentTimeClockPunches ST INNER JOIN dbo.arStudentClockAttendance SCA ON SCA.StuEnrollId = ST.StuEnrollId INNER JOIN dbo.arStudentSchedules SS ON SS.ScheduleId = SCA.ScheduleId ")
                .Append(" where  ST.StuEnrollId='")
                .Append(StuEnrollid + "'   and ST.Status=1 and SCA.ActualHours<>9999.0 and SCA.ActualHours<>999.0 and SCA.RecordDate = '" + SchDate + "' AND SS.Active = 1 ")
                .Append("   and Datepart(yyyy,PunchTime)=Datepart(yyyy,'" + SchDate + "')")
                .Append("and Datepart(mm,PunchTime)=Datepart(mm,'" + SchDate + "')")
                .Append("and Datepart(dd,PunchTime)=Datepart(dd,'" + SchDate + "')")
                .Append("Order By PunchTime;")


                .Append("Select Distinct StuEnrollId,ScheduleId,RecordDate,SchedHours,")
                .Append("ActualHours,isTardy from arStudentClockAttendance where StuEnrollId='" + StuEnrollid + "'")
                .Append(" and ActualHours<>9999.0 and ActualHours<>999.0 and ActualHours is not null  and Datepart(yyyy,RecordDate)=Datepart(yyyy,'" + SchDate + "')")
                .Append("and Datepart(mm,RecordDate)=Datepart(mm,'" + SchDate + "')")
                .Append("and Datepart(dd,RecordDate)=Datepart(dd,'" + SchDate + "');")

                .Append("Select total from arProgScheduleDetails PSD,arStudentSchedules SS where ")
                .Append(" SS.StuEnrollId='" + StuEnrollid + "'  and ")
                .Append(" SS.ScheduleId=PSD.ScheduleId and SS.Active=1 ")
                .Append(" and dw=(DatePart(dw,'" + SchDate + "')-1);")

                .Append("Select Distinct StuEnrollId ")
                .Append("  from arStudentClockAttendance where StuEnrollId='" + StuEnrollid + "'")
                .Append(" and Datepart(yyyy,RecordDate)=Datepart(yyyy,'" + SchDate + "')")
                .Append("and Datepart(mm,RecordDate)=Datepart(mm,'" + SchDate + "')")
                .Append("and Datepart(dd,RecordDate)=Datepart(dd,'" + SchDate + "');")

            End With

            Dim ds As New DataSet
            ds = db.RunParamSQLDataSet(sb.ToString)
            ds.Tables(0).TableName = "StudentTimeClockPunches"
            ds.Tables(1).TableName = "StudentClockAttendance"
            ds.Tables(2).TableName = "StudentSchedule"
            ds.Tables(3).TableName = "PunchesEntered"
            Return ds

        End Function

        Public Function GetPunchesforClsSectMeetingForaStudentonaGivenDate_ByClass(ByVal StuEnrollid As String, ByVal SchDate As Date, ByVal ClsSectMeetingID As String) As DataSet

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            With sb
                .Append("SELECT DISTINCT ")
                .Append("     BadgeId,ClockId,PunchTime,PunchType,Status,ST.StuEnrollId,FromSystem,ClsSectMeetingID,SpecialCode ")
                .Append("  FROM       arStudentTimeClockPunches ST  ")
                .Append(" where  ST.StuEnrollId='")
                .Append(StuEnrollid + "'  and ST.Status=1 ")
                .Append("   and ClsSectMeetingID='" + ClsSectMeetingID + "'")
                .Append("   and Datepart(yyyy,PunchTime)=Datepart(yyyy,'" + SchDate + "')")
                .Append("and Datepart(mm,PunchTime)=Datepart(mm,'" + SchDate + "')")
                .Append("and Datepart(dd,PunchTime)=Datepart(dd,'" + SchDate + "');")


                .Append("Select Distinct StuEnrollId,ClsSectMeetingID,MeetDate,Scheduled,")
                .Append("Actual,Tardy from atClsSectAttendance where StuEnrollId='" + StuEnrollid + "'")
                .Append(" and Actual<>9999.0 and Actual<>999.0 and Actual is not null  ")
                .Append("   and ClsSectMeetingID='" + ClsSectMeetingID + "'")
                .Append(" and Datepart(yyyy,MeetDate)=Datepart(yyyy,'" + SchDate + "')")
                .Append("and Datepart(mm,MeetDate)=Datepart(mm,'" + SchDate + "')")
                .Append("and Datepart(dd,MeetDate)=Datepart(dd,'" + SchDate + "');")


                .Append("Select Distinct StuEnrollId ")
                .Append("  from atClsSectAttendance where StuEnrollId='" + StuEnrollid + "'")
                .Append("   and ClsSectMeetingID='" + ClsSectMeetingID + "'")
                .Append(" and Datepart(yyyy,MeetDate)=Datepart(yyyy,'" + SchDate + "')")
                .Append("and Datepart(mm,MeetDate)=Datepart(mm,'" + SchDate + "')")
                .Append("and Datepart(dd,MeetDate)=Datepart(dd,'" + SchDate + "');")

            End With

            Dim ds As New DataSet
            ds = db.RunParamSQLDataSet(sb.ToString)
            ds.Tables(0).TableName = "StudentTimeClockPunches"
            ds.Tables(1).TableName = "StudentClockAttendance"
            ds.Tables(2).TableName = "PunchesEntered"
            Return ds

        End Function


        Public Function GetStudentCompleteDetails(ByVal StuEnrollid As String) As DataSet

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            With sb

                .Append("Select SE.BadgeNumber,ISNULL(SE.StartDate,SE.ExpStartDate) AS StartDate,SE.ExpGradDate,SS.ScheduleId from arstuEnrollments SE")
                .Append(",arStudentSchedules SS where SS.StuEnrollId=SE.StuEnrollId and  ")
                .Append("SE.StuEnrollId=? and SS.Active=1;")

                .Append("Select StartDate,EndDate from arSTudentLOAs where StuEnrollid=?;")

                .Append("Select StartDate,EndDate from arStdSuspensions where StuEnrollid=?;")


                .Append(";SELECT ")
                .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
                .Append("	      CCT.HolidayId, ")
                .Append("	      CCT.StatusId, ")
                .Append("	      CCT.HolidayCode, ")
                .Append("         CCT.HolidayStartDate, ")
                .Append("         CCT.HolidayEndDate, ")
                .Append("	      CCT.HolidayDescrip ")
                .Append("FROM     syHolidays CCT, syStatuses ST ")
                .Append("WHERE    CCT.StatusId = ST.StatusId ")
                .Append("AND (CCT.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId in (Select CampusID from arStuEnrollMents where StuEnrollid= ? ) ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR CCT.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')); ")

            End With
            db.AddParameter("@StuEnrollId", StuEnrollid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            Dim ds As New DataSet
            ds = db.RunParamSQLDataSet(sb.ToString)
            ds.Tables(0).TableName = "StudentSchedule"
            ds.Tables(1).TableName = "STudentLOAs"
            ds.Tables(2).TableName = "StdSuspensions"
            ds.Tables(3).TableName = "syHolidays"

            Return ds

        End Function
        Public Function GetStudentCompleteDetails_ByClass(ByVal StuEnrollid As String) As DataSet

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            With sb

                .Append("Select SE.BadgeNumber,SE.StartDate,SE.ExpGradDate,SE.CampusID from arstuEnrollments SE")
                .Append(" where  ")
                .Append("SE.StuEnrollId=?;")

                .Append("Select StartDate,EndDate from arSTudentLOAs where StuEnrollid=?;")

                .Append("Select StartDate,EndDate from arStdSuspensions where StuEnrollid=?;")


                .Append(";SELECT ")
                .Append("	      (Case ST.Status when 'Active' then 1 else 0 end) As Status, ")
                .Append("	      CCT.HolidayId, ")
                .Append("	      CCT.StatusId, ")
                .Append("	      CCT.HolidayCode, ")
                .Append("         CCT.HolidayStartDate, ")
                .Append("         CCT.HolidayEndDate, ")
                .Append("	      CCT.HolidayDescrip ")
                .Append("FROM     syHolidays CCT, syStatuses ST ")
                .Append("WHERE    CCT.StatusId = ST.StatusId ")
                .Append("AND (CCT.CampGrpId IN(SELECT CampGrpId ")
                .Append("FROM syCmpGrpCmps ")
                .Append("WHERE CampusId in (Select CampusID from arStuEnrollMents where StuEnrollid= ? ) ")
                .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("OR CCT.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')); ")

            End With
            db.AddParameter("@StuEnrollId", StuEnrollid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            Dim ds As New DataSet
            ds = db.RunParamSQLDataSet(sb.ToString)
            ds.Tables(0).TableName = "StudentSchedule"
            ds.Tables(1).TableName = "STudentLOAs"
            ds.Tables(2).TableName = "StdSuspensions"
            ds.Tables(3).TableName = "syHolidays"

            Return ds

        End Function

        Public Function SaveTardyMarked(ByVal stuEnrollID As String, ByVal ScheduleId As String, ByVal RecordDate As Date) As String
            Dim db As New DataAccess
            Dim sb As New System.Text.StringBuilder


            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim groupTrans As OleDbTransaction = db.StartTransaction()
            Try

                With sb
                    .Append("UPDATE arStudentClockAttendance Set isTardy=1 ")
                    .Append("WHERE StuEnrollId = ? and ScheduleId= ? and RecordDate=? ")
                End With
                db.AddParameter("@stdenrollid", stuEnrollID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@ScheduleId", ScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@RecordDate", RecordDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
                groupTrans.Commit()
                Return ""
            Catch ex As OleDbException
                groupTrans.Rollback()
                If Not groupTrans.Connection Is Nothing Then
                End If
                Return DALExceptions.BuildErrorMessage(ex)
                db.CloseConnection()
            End Try

        End Function
        Public Function GetHrsforaStudent(ByVal StuEnrollid As String, ByVal SchDate As Date) As DataSet

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder

            With sb
                .Append("Select Distinct DatePart(dw,RecordDate)-1 as DOW,StuEnrollId,ScheduleId,RecordDate,SchedHours,")
                .Append("ActualHours,isTardy from arStudentClockAttendance where  ActualHours<>999.0 and ActualHours<>9999.0 and ActualHours is not null and StuEnrollId='" + StuEnrollid + "'")
                .Append(" and  RecordDate  between '" + SchDate + "' and Dateadd(day,6,'" + SchDate + "') order by RecordDate")
            End With

            Dim ds As New DataSet
            ds = db.RunParamSQLDataSet(sb.ToString)
            Return ds

        End Function
        Public Shared Function SaveTotalDailyHoursforTimeClock(ByVal curDate As DateTime, ByVal StuEnrollId As String, ByVal ScheduleId As String,
                                                          ByVal hrs As Decimal, ByVal Tardy As Boolean, ByVal schedHours As Decimal, ByVal PrgVerId As String, ByVal Tardymarked As Boolean, ByVal CampusID As String) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim info As New ClockHourAttendanceInfo
                Dim sql As String = ""
                info.RecordDate = CType(curDate.ToShortDateString(), DateTime)
                info.StuEnrollId = StuEnrollId
                info.ScheduleId = ScheduleId
                info.ActualHours = hrs
                info.TardyProcessed = 0
                If Tardymarked = True Then
                    info.Tardy = 1
                Else
                    info.Tardy = 0
                End If
                info.SchedHours = schedHours
                sql = AR.AttendanceDB.GetAddOrUpdateSql(info, "sa", CampusID)

                db.RunParamSQLExecuteNoneQuery(sql)

                Dim strUpdateStatus As String = ""
                Try
                    strUpdateStatus = ChangeStatusToCurrentlyAttending(StuEnrollId)
                Catch ex As System.Exception
                End Try
                Return ""
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function

        Public Shared Function SaveTotalDailyHoursforTimeClock_byClass(ByVal curDate As DateTime, ByVal StuEnrollId As String, ByVal ClsSectMeetingID As String, ByVal hrs As Decimal, ByVal Tardy As Boolean, ByVal schedHours As Decimal, ByVal PrgVerId As String, ByVal Tardymarked As Boolean, ByVal CampusID As String, ByVal UnitTypeId As String, ByVal drpunches() As DataRow, Optional selectWk As Integer = 0) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim info As New ClsSectAttendanceInfo
                Dim sql As String = ""

                Dim StartTime As String
                StartTime = GetStartTimeForClassSectionMeeting(ClsSectMeetingID, CampusID, selectWk)
                Dim dtStart As System.DateTime = DateTime.Parse(Format(curDate, "MM/dd/yyyy") + " " + StartTime)

                info.MeetDate = dtStart
                info.StuEnrollId = New Guid(StuEnrollId)
                info.ClsSectMeetingId = New Guid(ClsSectMeetingID)
                info.Actual = hrs

                If UnitTypeId = AdvantageCommonValues.MinutesGuid Or UnitTypeId.ToString.ToUpper = "MINUTES" Then
                    info.UnitTypeId = "minutes"
                ElseIf UnitTypeId = AdvantageCommonValues.ClockHoursGuid Or UnitTypeId.ToString.ToUpper = "CLOCK HOURS" Then
                    info.UnitTypeId = "hours"
                ElseIf UnitTypeId = AdvantageCommonValues.PresentAbsentGuid Then
                    info.UnitTypeId = "present"
                Else
                    info.UnitTypeId = "present"
                End If

                If Tardymarked = True Then
                    info.Tardy = 1
                Else
                    info.Tardy = 0
                End If
                info.Scheduled = schedHours
                sql = AR.AttendanceDB.GetAddOrUpdateSqlForEnteredTimeCLock_byClass(info, "sa", CampusID, drpunches)

                db.RunParamSQLExecuteNoneQuery(sql)

                Dim strUpdateStatus As String = ""
                Try
                    strUpdateStatus = ChangeStatusToCurrentlyAttending(StuEnrollId)
                Catch ex As System.Exception
                End Try
                Return ""
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function

        Public Shared Function DeletePunchfortimeclock(ByVal dtpunch As DataTable) As String

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim Sql As String = "DELETE arStudentTimeClockPunches WHERE Status=1 and StuEnrollId = ? " + vbCrLf

                Sql += " and Datepart(yyyy,PunchTime)=Datepart(yyyy,?) "
                Sql += " and Datepart(mm,PunchTime)=Datepart(mm,?) "
                Sql += " and Datepart(dd,PunchTime)=Datepart(dd,?)  "

                Dim StuEnrollId As String = dtpunch.Rows(0)("StuEnrollId").ToString
                Dim BadgeId As String = dtpunch.Rows(0)("BadgeId").ToString

                Dim PunchTime As Date = CDate(dtpunch.Rows(0)("PunchTime").ToString)
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.AddParameter("@PunchTime", PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@PunchTime", PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@PunchTime", PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(Sql)
                db.CloseConnection()

                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function

        Public Shared Function DeletePunchfortimeclock_ByClass(ByVal dtpunch As DataTable) As String

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim Sql As String = "DELETE arStudentTimeClockPunches WHERE Status=1 and StuEnrollId = ? AND BadgeId = ? " + vbCrLf

                Sql += " and Datepart(yyyy,PunchTime)=Datepart(yyyy,?) "
                Sql += " and Datepart(mm,PunchTime)=Datepart(mm,?) "
                Sql += " and Datepart(dd,PunchTime)=Datepart(dd,?)  "
                Sql += " and ClsSectMeetingID=?  "

                Dim StuEnrollId As String = dtpunch.Rows(0)("StuEnrollId").ToString
                Dim BadgeId As String = dtpunch.Rows(0)("BadgeId").ToString
                Dim ClsSectMeetingID As String = dtpunch.Rows(0)("ClsSectMeetingID").ToString

                Dim PunchTime As Date = CDate(dtpunch.Rows(0)("PunchTime").ToString)
                db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@BadgeId", BadgeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.AddParameter("@PunchTime", PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@PunchTime", PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@PunchTime", PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ClsSectMeetingID", ClsSectMeetingID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(Sql)
                db.CloseConnection()

                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function



        Public Function GetResourceRoles(ByVal resource As String) As DataTable
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim ds As New DataSet

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            With sb
                .Append("SELECT Distinct t1.RoleId, t1.AccessLevel,t2.ResourceURL ")
                .Append("FROM syRlsResLvls t1, syResources t2 ")
                .Append("WHERE t1.ResourceID = t2.ResourceID ")
                .Append("AND t2.Resource like '" + resource + "';")
            End With

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


            ds = db.RunParamSQLDataSet(sb.ToString)

            Return ds.Tables(0)
        End Function


        Public Function GetUserRolesForCurrentCampus(ByVal userId As String, ByVal campusId As String) As DataTable
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim ds As New DataSet

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            With sb
                .Append("SELECT t1.RoleId ")
                .Append("FROM syUsersRolesCampGrps t1 ")
                .Append("WHERE t1.UserId = ? ")
                .Append("AND t1.CampGrpId IN ")
                .Append("(SELECT t2.CampGrpId ")
                .Append("FROM syCmpGrpCmps t2 ")
                .Append("WHERE t2.CampusId = ?)")
            End With

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@campusid", campusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ds = db.RunParamSQLDataSet(sb.ToString)

            Return ds.Tables(0)

        End Function

        Public Function GetUserRoles(ByVal userId As String) As DataTable
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            Dim ds As New DataSet

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            With sb
                .Append("SELECT t1.RoleId ")
                .Append("FROM syUsersRolesCampGrps t1 ")
                .Append("WHERE t1.UserId = ? ")
            End With

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            db.AddParameter("@userid", userId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ds = db.RunParamSQLDataSet(sb.ToString)

            Return ds.Tables(0)
        End Function

        Public Function IsStudentTardy(ByVal StuEnrollId As String, ByVal hdate As Date, ByVal PrgVerId As String) As DataSet
            Dim sb As New StringBuilder
            Dim db As New DataAccess
            Dim ds As New DataSet

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                .Append(" Select Count(StuEnrollId) from arStudentClockAttendance where istardy =1 and " + vbCrLf)
                .Append(" StuEnrollId=? and Recorddate=?; ")

                .Append("Select TrackTardies from arPrgversions where PrgVerId=?")
            End With
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@HDate", hdate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PrgVerId", PrgVerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            ds = db.RunParamSQLDataSet(sb.ToString)
            ds.Tables(0).TableName = "arStudentClockAttendance"
            ds.Tables(1).TableName = "arPrgversions"
            Return ds
        End Function

        Public Function GetTimeClockPunchExceptionforaStudentforgivendate(ByVal StuEnrollId As String, ByVal FromDate As String, ByVal ToDate As String) As DataSet
            Dim sb As New StringBuilder
            Dim db As New DataAccess
            Dim ds As New DataSet

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                .Append(" SELECT     BadgeId,Status,  convert(varchar,PunchTime,101) Date,")
                .Append(" (substring(Convert(Varchar,PunchTime,100),13,5)+ ' ' + substring(Convert(Varchar,PunchTime,100),18,2))PunchTime1, ")
                .Append(" PunchType,ClockId,STC.StuEnrollId,FromSystem,ScheduleId,Id ")
                .Append(" FROM arStudenttimeClockPunches STC ,((Select distinct convert(varchar,PunchTime,101) pntime, ")
                .Append(" BadgeId BdgID from arStudentTimeClockPunches) except  (Select distinct convert(varchar,PunchTime,101) pntime, ")
                .Append(" BadgeId BdgID  from arStudentTimeClockPunches where status=1)) as T3,arStudentSchedules SS    ")
                .Append(" where(t3.PnTime = Convert(varchar, STC.PunchTime, 101)) and STC.BadgeId=t3.BdgID and ")
                .Append(" STC.STuEnrollId= ? and STC.StuEnrollId=SS.StuEnrollId")
                If FromDate <> "" And ToDate <> "" Then
                    .Append(" and convert(varchar,PunchTime,101) Between ? and ?")
                End If
                .Append(" union all select  BadgeId,Status,  convert(varchar,PunchTime,101) Date,")
                .Append(" (substring(Convert(Varchar,PunchTime,100),13,5)+ ' ' + substring(Convert(Varchar,PunchTime,100),18,2)) PunchTime1, ")
                .Append(" PunchType,ClockId,SS.StuEnrollId,FromSystem,ScheduleId,Id from ")
                .Append(" arStudenttimeClockPunches STC,arStudentSchedules SS where STC.StuEnrollId is Null and BadgeId in(Select")
                .Append(" BadgeNumber from arStuEnrollMents where StuEnrollId=?  )and Status in (5,8)  and SS.StuEnrollId=?")
                If FromDate <> "" And ToDate <> "" Then
                    .Append(" and convert(varchar,PunchTime,101) Between ? and ?")
                End If

                .Append(" ; Select SE.BadgeNumber,SE.StartDate,SE.ExpGradDate,SS.ScheduleId,SY.StatusCodeDescrip, PR.PrgVerDescrip from arstuEnrollments SE")
                .Append(",arStudentSchedules SS,arPrgVersions PR,syStatusCodes SY where SS.StuEnrollId=SE.StuEnrollId and  ")
                .Append("SE.StuEnrollId=? and SS.Active=1 and SE.StatusCodeID=SY.StatusCodeID and SE.PrgVerId=PR.PrgVerId;")

            End With
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If FromDate <> "" And ToDate <> "" Then
                db.AddParameter("@FromDate", FromDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ToDate", ToDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            If FromDate <> "" And ToDate <> "" Then
                db.AddParameter("@FromDate", FromDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ToDate", ToDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            ds = db.RunParamSQLDataSet(sb.ToString)

            ds.Tables(0).TableName = "TimeClockExceptionPunch"
            ds.Tables(1).TableName = "StudentTimeClockData"


            If ds.Tables(1).Rows.Count > 0 Then
                For i = 0 To ds.Tables(1).Rows.Count - 1

                    If Not ds.Tables(1).Rows(i)("BadgeNumber") Is DBNull.Value Then
                        If Not ds.Tables(1).Rows(i)("BadgeNumber") = String.Empty Then
                            If IsNumeric(ds.Tables(1).Rows(i)("BadgeNumber")) Then
                                ds.Tables(1).Rows(i)("BadgeNumber") = Math.Abs(CInt(ds.Tables(1).Rows(i)("BadgeNumber")))
                                ds.AcceptChanges()

                            End If
                        End If

                    End If
                Next

            End If



            Return ds

        End Function



        Public Function GetTimeClockPunchExceptionforaStudentforgivendate_ByClass(ByVal StuEnrollId As String, ByVal FromDate As String, ByVal ToDate As String) As DataSet
            Dim sb As New StringBuilder
            Dim db As New DataAccess
            Dim ds As New DataSet

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If


            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                .Append(" SELECT     BadgeId,Status,  convert(varchar,PunchTime,101) Date,")
                .Append(" (substring(Convert(Varchar,PunchTime,100),13,5)+ ' ' + substring(Convert(Varchar,PunchTime,100),18,2))PunchTime1, ")
                .Append(" PunchType,ClockId,STC.StuEnrollId,FromSystem,Id, IsNull(ClsSectMeetingID,(SELECT CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER))) as ClsSectMeetingID ,SpecialCode,")
                .Append(" (SELECT startdate FROM dbo.arClsSectMeetings WHERE ClsSectMeetingId=STC.ClsSectMeetingId) AS startdate, ")
                .Append(" (SELECT Enddate FROM dbo.arClsSectMeetings WHERE ClsSectMeetingId=STC.ClsSectMeetingId) AS Enddate ")
                .Append(" FROM arStudenttimeClockPunches STC")

                .Append(" Where STC.STuEnrollId = ?       AND Status IN (2,5,6,7,9,10)  ")
                If FromDate <> "" And ToDate <> "" Then
                    .Append(" and convert(varchar,PunchTime,101) Between ? and ?")
                End If

                .Append(" union all select  BadgeId,Status,  convert(varchar,PunchTime,101) Date,")
                .Append(" (substring(Convert(Varchar,PunchTime,100),13,5)+ ' ' + substring(Convert(Varchar,PunchTime,100),18,2)) PunchTime1, ")
                .Append(" PunchType,ClockId,?,FromSystem,Id,IsNull(ClsSectMeetingID,(SELECT CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER))) as ClsSectMeetingID,SpecialCode, ")
                .Append(" (SELECT startdate FROM dbo.arClsSectMeetings WHERE ClsSectMeetingId=STC.ClsSectMeetingId) AS startdate, ")
                .Append(" (SELECT Enddate FROM dbo.arClsSectMeetings WHERE ClsSectMeetingId=STC.ClsSectMeetingId) AS Enddate ")

                .Append(" from arStudenttimeClockPunches STC where STC.StuEnrollId is Null and BadgeId in(Select")
                .Append(" BadgeNumber from arStuEnrollMents where StuEnrollId=?  )and Status in (5,8,9) ")
                If FromDate <> "" And ToDate <> "" Then
                    .Append(" and convert(varchar,PunchTime,101) Between ? and ?")
                End If
                .Append(" ; Select SE.BadgeNumber,SE.StartDate,SE.ExpGradDate,SY.StatusCodeDescrip, PR.PrgVerDescrip from arstuEnrollments SE")
                .Append(",arPrgVersions PR,syStatusCodes SY where   ")
                .Append("SE.StuEnrollId=?  and SE.StatusCodeID=SY.StatusCodeID and SE.PrgVerId=PR.PrgVerId;")

            End With
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If FromDate <> "" And ToDate <> "" Then
                db.AddParameter("@FromDate", FromDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ToDate", ToDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            If FromDate <> "" And ToDate <> "" Then
                db.AddParameter("@FromDate", FromDate, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ToDate", ToDate, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            ds = db.RunParamSQLDataSet(sb.ToString)

            ds.Tables(0).TableName = "TimeClockExceptionPunch"
            ds.Tables(1).TableName = "StudentTimeClockData"

            If ds.Tables(1).Rows.Count > 0 Then
                For i = 0 To ds.Tables(1).Rows.Count - 1

                    If Not ds.Tables(1).Rows(i)("BadgeNumber") Is DBNull.Value Then
                        If Not ds.Tables(1).Rows(i)("BadgeNumber") = String.Empty Then
                            If IsNumeric(ds.Tables(1).Rows(i)("BadgeNumber")) Then
                                ds.Tables(1).Rows(i)("BadgeNumber") = Math.Abs(CInt(ds.Tables(1).Rows(i)("BadgeNumber")))
                                ds.AcceptChanges()

                            End If
                        End If

                    End If
                Next

            End If


            Return ds

        End Function
        Public Shared Function AddPunchTimeClockException(ByVal info As TimeClockPunchInfo) As String

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim sb As New StringBuilder

                sb.Append("DELETE arStudentTimeClockPunches " + vbCrLf)

                sb.AppendFormat("WHERE BadgeId ='{0}'", info.BadgeId)

                sb.AppendFormat(" and  PunchTime='{0}' and PunchType={1} {2}",
                                     info.PunchTime, CType(info.PunchType, Integer), vbCrLf)

                sb.Append("INSERT arStudentTimeClockPunches (StuEnrollId, BadgeId, PunchTime, ClockId, PunchType, Status,FromSystem) " + vbCrLf)
                sb.Append("VALUES (?,?,?,?,?,?,?) ")

                If info.StuEnrollId = "" Then
                    db.AddParameter("@StuEnrollId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@StuEnrollId", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@BadgeId", info.BadgeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchTime", info.PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ClockId", info.ClockId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchType", CType(info.PunchType, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@Processed", CType(info.Status, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@FromSystem", info.FromSystem, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()

                Return ""
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function

        Public Shared Function AddPunchTimeClockException_ByClass(ByVal info As TimeClockPunchInfo) As String

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim sb As New StringBuilder
                If Not info.ClsSectMeetingId = System.Guid.Empty.ToString Then
                    sb.Append("DELETE arStudentTimeClockPunches " + vbCrLf)

                    sb.AppendFormat("WHERE BadgeId ='{0}'", info.BadgeId)

                    If info.ClsSectMeetingId = System.Guid.Empty.ToString Then
                        sb.AppendFormat(" AND ClsSectMeetingID is Null ", System.DBNull.Value)
                    Else
                        sb.AppendFormat(" AND ClsSectMeetingID ='{0}'", info.ClsSectMeetingId)
                    End If

                    sb.AppendFormat(" AND SpecialCode ='{0}'", info.SpecialCode)

                    sb.AppendFormat(" and  PunchTime='{0}' and PunchType={1} {2}",
                                         info.PunchTime, CType(info.PunchType, Integer), vbCrLf)

                    sb.Append("INSERT arStudentTimeClockPunches (StuEnrollId, BadgeId, PunchTime, ClockId, PunchType, Status,FromSystem,ClsSectMeetingID,SpecialCode) " + vbCrLf)
                    sb.Append("VALUES (?,?,?,?,?,?,?,?,?) ")

                    If info.StuEnrollId = "" Then
                        db.AddParameter("@StuEnrollId", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Else
                        db.AddParameter("@StuEnrollId", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    End If
                    db.AddParameter("@BadgeId", info.BadgeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@PunchTime", info.PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                    db.AddParameter("@ClockId", info.ClockId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    db.AddParameter("@PunchType", CType(info.PunchType, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    db.AddParameter("@Processed", CType(info.Status, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                    db.AddParameter("@FromSystem", info.FromSystem, DataAccess.OleDbDataType.OleDbBoolean, , ParameterDirection.Input)
                    If info.ClsSectMeetingId = System.Guid.Empty.ToString Then
                        db.AddParameter("@ClsSectMeetingID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    Else
                        db.AddParameter("@ClsSectMeetingID", info.ClsSectMeetingId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    End If
                    db.AddParameter("@SpecialCode", info.SpecialCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    db.RunParamSQLExecuteNoneQuery(sb.ToString)

                End If
                db.CloseConnection()

                Return ""
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function


        Public Shared Function UpdatePunchTimeClockException(ByVal info As TimeClockPunchInfo) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sb As New StringBuilder
                sb.Append("update " & vbCrLf)
                sb.Append("     arStudentTimeClockPunches " + vbCrLf)
                sb.Append("set " & vbCrLf)
                sb.Append("     ClockId = ?, " & vbCrLf)
                sb.Append("     PunchType = ?, " & vbCrLf)
                sb.Append("     Status = ? " & vbCrLf)
                sb.Append("where " + vbCrLf)
                sb.Append("     BadgeId = ? " + vbCrLf)
                sb.Append("     and PunchTime = ? " + vbCrLf)

                db.AddParameter("@ClockId", info.ClockId, DataAccess.OleDbDataType.OleDbString, 20, ParameterDirection.Input)
                db.AddParameter("@PunchType", CType(info.PunchType, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@Status", CType(info.Status, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@BadgeId", info.BadgeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchTime", info.PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString)

                Return ""
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function

        Public Shared Function UpdatePunchTimeClockException_ByClass(ByVal info As TimeClockPunchInfo) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                Dim sb As New StringBuilder
                sb.Append("update " & vbCrLf)
                sb.Append("     arStudentTimeClockPunches " + vbCrLf)
                sb.Append("set " & vbCrLf)
                sb.Append("     ClockId = ?, " & vbCrLf)
                sb.Append("     PunchType = ?, " & vbCrLf)
                sb.Append("     Status = ?, " & vbCrLf)
                sb.Append(" ClsSectMeetingID = ?, " & vbCrLf)
                sb.Append(" SpecialCode = ? " & vbCrLf)
                sb.Append("where " + vbCrLf)
                sb.Append("     BadgeId = ? " + vbCrLf)
                sb.Append("     and PunchTime = ? " + vbCrLf)

                db.AddParameter("@ClockId", info.ClockId, DataAccess.OleDbDataType.OleDbString, 20, ParameterDirection.Input)
                db.AddParameter("@PunchType", CType(info.PunchType, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@Status", CType(info.Status, Integer), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                If info.ClsSectMeetingId = System.Guid.Empty.ToString Then
                    db.AddParameter("@ClsSectMeetingID", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@ClsSectMeetingID", info.ClsSectMeetingId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
                db.AddParameter("@SpecialCode", info.SpecialCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.AddParameter("@BadgeId", info.BadgeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchTime", info.PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString)

                Return ""
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function


        Public Shared Function DeletePunchfortimeclockEditPendingPunches(ByVal BadgeId As String, ByVal PunchType As Integer, ByVal PunchTime As Date) As String

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim sb As New StringBuilder

                sb.Append("DELETE arStudentTimeClockPunches " + vbCrLf)

                sb.AppendFormat("WHERE BadgeId ='{0}'", BadgeId)

                sb.AppendFormat(" and  PunchTime='{0}' and PunchType={1} {2}",
                                    PunchTime, PunchType, vbCrLf)
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()

                Return ""
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function

        Public Shared Function DeletePunchfortimeclockEditPendingPunches_ByClass(ByVal BadgeId As String, ByVal PunchType As Integer, ByVal PunchTime As Date, ByVal ClsSectMeetingID As String, ByVal SpecialCode As String) As String
            '   Connect to the database
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim sb As New StringBuilder

                sb.Append("DELETE arStudentTimeClockPunches " + vbCrLf)

                sb.AppendFormat("WHERE BadgeId ='{0}'", BadgeId)
                If ClsSectMeetingID = System.Guid.Empty.ToString Then
                    sb.AppendFormat(" AND ClsSectMeetingID is Null ")
                Else
                    sb.AppendFormat(" AND ClsSectMeetingID ='{0}'", ClsSectMeetingID)
                End If

                sb.AppendFormat(" AND SpecialCode ='{0}'", SpecialCode)

                sb.AppendFormat(" and  PunchTime='{0}' and PunchType={1} {2}",
                                    PunchTime, PunchType, vbCrLf)

                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.CloseConnection()

                Return ""
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return ex.Message
            End Try
            Return "Unhandled error"
        End Function

        Public Function DeletePunchandUpdateClockAttendance(ByVal stuEnrollid As String, ByVal PunchTime As Date, ByVal Scheduledhours As Decimal) As String

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim Sql As String = "DELETE arStudentTimeClockPunches WHERE StuEnrollId = ? AND " + vbCrLf

                Sql += " Convert(varchar,PunchTime,101)=? "

                db.AddParameter("@StuEnrollId", stuEnrollid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchTime", PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(Sql)
                db.ClearParameters()

                Sql = "Update arStudentClockAttendance Set SchedHours=?, ActualHours=9999.0 , istardy=0 , postbyexception =0 , ModDate=getdate()"
                Sql = Sql + " where StuEnrollId=? and Convert(varchar,RecordDate,101)=?"
                db.AddParameter("@SchedHours", Scheduledhours, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", stuEnrollid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchTime", PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(Sql)
                db.CloseConnection()

                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function

        Public Function DeletePunchandUpdateClockAttendance_byClass(ByVal stuEnrollid As String, ByVal PunchTime As Date, ByVal Scheduledhours As Decimal, ByVal ClsSectMeetingID As String) As String
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim Sql As String = "DELETE arStudentTimeClockPunches WHERE StuEnrollId = ? AND " + vbCrLf

                Sql += " Convert(varchar,PunchTime,101)= Convert(varchar,?,101) "
                Sql += " and ClsSectMeetingID=? "

                db.AddParameter("@StuEnrollId", stuEnrollid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchTime", PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ClsSectMeetingID", ClsSectMeetingID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)


                db.RunParamSQLExecuteNoneQuery(Sql)
                db.ClearParameters()

                Sql = "Update atClsSectAttendance Set Scheduled=?, Actual=9999.0 , Tardy=0  "
                Sql = Sql + " where StuEnrollId=? and Convert(varchar,MeetDate,101)= Convert(varchar,?,101) "
                Sql += " and ClsSectMeetingID=? "
                db.AddParameter("@SchedHours", Scheduledhours, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", stuEnrollid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchTime", PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@ClsSectMeetingID", ClsSectMeetingID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(Sql)
                db.CloseConnection()

                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function
        Public Function UpdateClockAttendance(ByVal stuEnrollid As String, ByVal PunchTime As Date, ByVal Scheduledhours As Decimal) As String

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim Sql As String

                Sql = "Update arStudentClockAttendance Set SchedHours=?, ActualHours=9999.0 , istardy=0 , postbyexception =0 , ModDate=getdate()"
                Sql = Sql + " where StuEnrollId=? and Convert(varchar,RecordDate,101)=?"
                db.AddParameter("@SchedHours", Scheduledhours, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                db.AddParameter("@StuEnrollId", stuEnrollid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchTime", PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(Sql)
                db.CloseConnection()

                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function

        Public Function UpdateScheduledHours(ByVal stuEnrollid As String, ByVal PunchTime As Date, ByVal Scheduledhours As Decimal, ByVal CampusID As String, ByVal TardyMarked As Boolean) As String
            Dim db As New DataAccess
            Dim intRecCount As Integer = 0
            Dim sb As New StringBuilder

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


            With sb
                .Append("Select Count(*) from arStudentClockAttendance where StuEnrollId=? and Convert(varchar,RecordDate,101)=? ")
            End With
            db.AddParameter("@StuEnrollId", stuEnrollid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@PunchTime", PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Try
                intRecCount = db.RunParamSQLScalar(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
                If intRecCount >= 1 Then
                    Try
                        Dim Sql As String

                        Sql = "Update arStudentClockAttendance Set SchedHours=?"
                        Sql = Sql + " where StuEnrollId=? and Convert(varchar,RecordDate,101)=?"
                        db.AddParameter("@SchedHours", Scheduledhours, DataAccess.OleDbDataType.OleDbDecimal, 50, ParameterDirection.Input)
                        db.AddParameter("@StuEnrollId", stuEnrollid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@PunchTime", PunchTime, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                        db.RunParamSQLExecuteNoneQuery(Sql)
                        db.CloseConnection()

                        Return ""
                    Catch ex As OleDbException
                        If db.Connection.State = ConnectionState.Open Then
                            db.CloseConnection()
                        End If
                        Return DALExceptions.BuildErrorMessage(ex)
                    End Try
                Else
                    Try
                        Dim scheduleId As Guid

                        With sb
                            .Append("Select Top 1 ScheduleId from arStudentSchedules " + vbCrLf)
                            .Append("where StuEnrollId=?")
                        End With
                        db.AddParameter("@StuEnrollId", stuEnrollid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                        scheduleId = db.RunParamSQLScalar(sb.ToString)
                        db.ClearParameters()

                        Try
                            Dim info As New ClockHourAttendanceInfo
                            Dim sql As String = ""
                            info.RecordDate = CType(PunchTime.ToShortDateString(), DateTime)
                            info.StuEnrollId = stuEnrollid
                            info.ScheduleId = scheduleId.ToString()
                            info.ActualHours = 0
                            info.TardyProcessed = 0
                            If TardyMarked = True Then
                                info.Tardy = 1
                            Else
                                info.Tardy = 0
                            End If
                            info.SchedHours = Scheduledhours

                            sql = AR.AttendanceDB.GetAddOrUpdateSql(info, "sa", CampusID)
                            db.RunParamSQLExecuteNoneQuery(sql)
                            db.CloseConnection()

                            Return ""
                        Catch ex As OleDbException
                            If db.Connection.State = ConnectionState.Open Then
                                db.CloseConnection()
                            End If
                            Return DALExceptions.BuildErrorMessage(ex)
                        End Try

                        Return ""
                    Catch ex As OleDbException
                        If db.Connection.State = ConnectionState.Open Then
                            db.CloseConnection()
                        End If
                        Return DALExceptions.BuildErrorMessage(ex)
                    End Try
                End If
            Catch ex As System.Exception
                intRecCount = 0
            Finally
                db.ClearParameters()
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
                sb.Remove(0, sb.Length)
            End Try
            Return "Unhandled Error"
        End Function


        Public Shared Function DeleteAllPunches(ByVal info As TimeClockPunchInfo) As String

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim Sql As String = "DELETE arStudentTimeClockPunches WHERE StuEnrollId = ? AND Convert(varchar,PunchTime,101) = ? " + vbCrLf
                db.AddParameter("@StuEnrollId", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchTime", Format(info.PunchTime, "MM/dd/yyyy"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(Sql)
                db.CloseConnection()

                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function


        Public Function GetTimeClockSourceandTargetLocations(ByVal Campusid As String) As DataSet

            Dim sb As New StringBuilder
            Dim db As New DataAccess
            Dim ds As New DataSet

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            With sb
                .Append(" Select TCSourcePath,TCTargetPath,ISRemoteServer,RemoteServerUsrNm,RemoteServerPwd,SourceFolderLoc,TargetFolderLoc from syCampuses where CampusId=?")
            End With
            db.AddParameter("@Campusid", Campusid, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            ds = db.RunParamSQLDataSet(sb.ToString)
            Return ds
        End Function


        Public Shared Function UpdateAllPunchesToDuplicate(ByVal info As TimeClockPunchInfo) As String

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try
                Dim Sql As String = "Update arStudentTimeClockPunches set Status=8 WHERE StuEnrollId = ? AND Convert(varchar,PunchTime,101) = ? " + vbCrLf
                db.AddParameter("@StuEnrollId", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchTime", Format(info.PunchTime, "MM/dd/yyyy"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                Dim Sql1 As String = ";Delete from  arStudentTimeClockPunches  WHERE StuEnrollId = ? AND Convert(varchar,PunchTime,101) = ?  and FromSystem=1" + vbCrLf
                db.AddParameter("@StuEnrollId", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchTime", Format(info.PunchTime, "MM/dd/yyyy"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

                Dim Sql2 As String = "; Update arStudentClockAttendance Set ActualHours=9999.0 , istardy=0 , postbyexception =0 , ModDate=getdate()"
                Sql2 = Sql2 + " where StuEnrollId=? and Convert(varchar,RecordDate,101)=?"
                db.AddParameter("@StuEnrollId", info.StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@PunchTime", Format(info.PunchTime, "MM/dd/yyyy"), DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)


                db.RunParamSQLExecuteNoneQuery(Sql + Sql1 + Sql2)
                db.CloseConnection()

                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function

        Public Shared Function UpdateAllPunchesToDuplicate_byClass(ByVal info As TimeClockPunchInfo) As String

            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Try



                db.AddParameter("@StuEnrollId", info.StuEnrollId, SqlDbType.VarChar, 50, ParameterDirection.Input)
                db.AddParameter("@PunchTime", info.PunchTime, SqlDbType.DateTime, , ParameterDirection.Input)
                db.AddParameter("@SpecialCode", info.SpecialCode, SqlDbType.VarChar, 10, ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery_SP("dbo.usp_UpdateAllPunchesToDuplicateandResetActualHrs")

                db.CloseConnection()

                Return ""
            Catch ex As OleDbException
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return DALExceptions.BuildErrorMessage(ex)
            End Try
            Return "Unhandled Error"
        End Function


        Public Function GetAllClsCategories_SP() As DataTable
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Return db.RunParamSQLDataSet("dbo.usp_GetAllClsCategories", Nothing, "SP").Tables(0)
        End Function



#Region "TimeClockSpecialCode"

        Public Function GetAllClockSpecialCodes() As DataSet

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            With sb
                .Append("SELECT   TC.TCSId, ")
                .Append("         CASE WHEN (Select CampDescrip from syCampuses where CampusId=TC.CampusId) IS NULL THEN ")
                .Append("         TC.TCSpecialCode ELSE ")
                .Append("         TC.TCSpecialCode + ' - ' + (Select CampDescrip from syCampuses where CampusId=TC.CampusId) END AS TCSpecialCode , ")
                .Append("         (Case ST.Status when 'Active' then 1 else 0 end) As Status ")
                .Append("FROM     arTimeClockSpecialCode TC, syStatuses ST ")
                .Append("WHERE    TC.StatusId = ST.StatusId ")
                .Append("ORDER BY ST.Status,TC.TCSpecialCode asc")
            End With

            Return db.RunSQLDataSet(sb.ToString)

        End Function

        Public Function GetClockSpecialCodeInfo(ByVal TCSId As String) As TimeClockSpecialCodeInfo

            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            Dim sb As New StringBuilder
            With sb
                .Append("SELECT TC.TCSId, ")
                .Append("       TC.TCSpecialCode, ")
                .Append("       TC.StatusId, ")
                .Append("       (Select Status from syStatuses where StatusId=TC.StatusId) As Status, ")
                .Append("       TC.TCSPunchType, ")
                .Append("       TC.CampusId, ")
                .Append("       (Select CampDescrip from syCampuses where CampusId=TC.CampusId) As CampDescrip, ")
                .Append("       TC.InstructionTypeId, ")
                .Append("       (Select InstructionTypeDescrip from arInstructionType where InstructionTypeId=TC.InstructionTypeId) As InstructionTypeDescrip, ")
                .Append("       TC.ModUser, ")
                .Append("       TC.ModDate ")
                .Append("FROM  arTimeClockSpecialCode TC ")
                .Append("WHERE TC.TCSId= ? ")
            End With

            db.AddParameter("@TCSId", TCSId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

            Dim TimeClockSpecialCodeInfo As New TimeClockSpecialCodeInfo

            While dr.Read()

                With TimeClockSpecialCodeInfo
                    .TCSId = TCSId
                    .OrigTCSId = TCSId
                    .TCSpecialCode = dr("TCSPecialCode")
                    .IsInDB = True
                    .StatusId = CType(dr("StatusId"), Guid).ToString
                    .Status = dr("Status")
                    .PunchType = dr("TCSPunchType")
                    If Not (dr("CampusId") Is System.DBNull.Value) Then .CampusId = CType(dr("CampusId"), Guid).ToString
                    If Not (dr("CampDescrip") Is System.DBNull.Value) Then .CampDescrip = dr("CampDescrip")
                    If Not (dr("InstructionTypeId") Is System.DBNull.Value) Then .InstructionTypeId = CType(dr("InstructionTypeId"), Guid).ToString
                    If Not (dr("InstructionTypeDescrip") Is System.DBNull.Value) Then .InstructionTypeDescrip = dr("InstructionTypeDescrip")
                    .ModUser = dr("ModUser")
                    .ModDate = dr("ModDate")
                End With

            End While

            If Not dr.IsClosed Then dr.Close()
            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

            Return TimeClockSpecialCodeInfo

        End Function

        Public Function UpdateClockSpecialCodeInfo(ByVal TimeClockSpecialCodeInfo As TimeClockSpecialCodeInfo, ByVal user As String) As String

            Dim db As New SQLDataAccess
            Dim dt As New DataTable

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If



            Try

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

                db.AddParameter("@TCSId", New Guid(TimeClockSpecialCodeInfo.TCSId.ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@TCSpecialCode", TimeClockSpecialCodeInfo.TCSpecialCode, SqlDbType.VarChar, 50, ParameterDirection.Input)
                db.AddParameter("@StatusId", New Guid(TimeClockSpecialCodeInfo.StatusId.ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@CampusId", New Guid(TimeClockSpecialCodeInfo.CampusId.ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@TCSPunchType", TimeClockSpecialCodeInfo.PunchType, SqlDbType.SmallInt, , ParameterDirection.Input)
                db.AddParameter("@InstructionTypeId", New Guid(TimeClockSpecialCodeInfo.InstructionTypeId.ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@UserName", user, SqlDbType.VarChar, 50, ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_AR_TimeClockSpecialCode_Update")

            Catch ex As System.Exception
                Throw New BaseException(ex.Message)
            Finally
                db.ClearParameters()
                db.CloseConnection()
            End Try



        End Function


        Public Function AddClockSpecialCodeInfo(ByVal TimeClockSpecialCodeInfo As TimeClockSpecialCodeInfo, ByVal user As String) As String

            Dim db As New SQLDataAccess
            Dim dt As New DataTable

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

                db.AddParameter("@TCSId", New Guid(TimeClockSpecialCodeInfo.TCSId.ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@TCSpecialCode", TimeClockSpecialCodeInfo.TCSpecialCode, SqlDbType.VarChar, 50, ParameterDirection.Input)
                db.AddParameter("@StatusId", New Guid(TimeClockSpecialCodeInfo.StatusId.ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@CampusId", New Guid(TimeClockSpecialCodeInfo.CampusId.ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@TCSPunchType", TimeClockSpecialCodeInfo.PunchType, SqlDbType.SmallInt, , ParameterDirection.Input)
                db.AddParameter("@InstructionTypeId", New Guid(TimeClockSpecialCodeInfo.InstructionTypeId.ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
                db.AddParameter("@UserName", user, SqlDbType.VarChar, 50, ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_AR_TimeClockSpecialCode_Insert")

            Catch ex As System.Exception
                Throw New BaseException(ex.Message)
            Finally
                db.ClearParameters()
                db.CloseConnection()
            End Try

        End Function
        Public Function DeleteClockSpecialCodeInfo(ByVal TCSId As String, ByVal modDate As DateTime) As String


            Dim db As New SQLDataAccess
            Dim dt As New DataTable
            Dim msg As String = String.Empty

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


                db.AddParameter("@TCSId", New Guid(TCSId.ToString), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)


                db.RunParamSQLExecuteNoneQuery_SP("dbo.USP_AR_TimeClockSpecialCode_Delete")

            Catch ex As System.Exception
                Return ex.ToString
            Finally
                db.ClearParameters()
                db.CloseConnection()
            End Try

        End Function

        Public Function CheckTimeClockSpecialCodeDup(ByVal TCSpecialCode As String, ByVal CampusId As String) As String
            Dim db As New DataAccess
            Dim sb As New System.Text.StringBuilder
            Dim ds As DataSet

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try

                With sb
                    .Append("SELECT count(*) FROM arTimeClockSpecialCode WHERE TCSpecialCode = ? AND CampusId = ? ")
                End With

                db.AddParameter("@TCSpecialCode", TCSpecialCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.AddParameter("@CampusId", CampusId, DataAccess.OleDbDataType.OleDbString, 128, ParameterDirection.Input)

                Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

                '   
                If rowCount = 0 Then
                    Return ""
                Else
                    Return "Time Clock Special Code already exists"
                End If

            Catch ex As OleDbException
                Return DALExceptions.BuildErrorMessage(ex)
            Finally
                db.CloseConnection()
            End Try
        End Function

        Public Function CheckTimeClockSpecialCodePunchDup(ByVal TCSpecialCode As String, ByVal PunchType As Integer) As String
            Dim db As New DataAccess
            Dim sb As New System.Text.StringBuilder
            Dim ds As DataSet
            Dim intPunch As Integer
            Dim strPunchDesc As String

            If PunchType = 1 Then
                intPunch = 2
            Else
                intPunch = 1
            End If

            If intPunch = 1 Then
                strPunchDesc = "In"
            Else
                strPunchDesc = "Out"
            End If

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try

                With sb
                    .Append("SELECT count(*) FROM arTimeClockSpecialCode WHERE TCSpecialCode = ? AND TCSPunchType = ? ")
                End With

                db.AddParameter("@TCSpecialCode", TCSpecialCode, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.AddParameter("@TCSPunchType", intPunch, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                '   execute the quer
                Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

                '   
                If rowCount = 0 Then
                    Return ""
                Else
                    Return "Time Clock Special Code has already been used for " & strPunchDesc & " Punch Type"
                End If

            Catch ex As OleDbException
                Return DALExceptions.BuildErrorMessage(ex)
            Finally
                db.CloseConnection()
            End Try
        End Function

#End Region

#Region "Import Punches"

        Public Shared Function GetEnrolmentforBadgeNumbers(ByVal listofbadgeNumber As String) As DataTable
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


            db.AddParameter("@BadgeNumber", listofbadgeNumber, SqlDbType.VarChar, , ParameterDirection.Input)

            Return db.RunParamSQLDataSet_SP("dbo.usp_GetStudenEnrollmentinfoforBadgeNumber").Tables(0)



        End Function


        Public Shared Function GetPunchTypeForSpecialCode(ByVal SpecialCode As String, ByVal CampusID As String) As TimeClockPunchType
            Dim db As New SQLDataAccess
            Dim ds As New DataSet
            Dim PunchType As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


            db.AddParameter("@SpecialCode", SpecialCode, SqlDbType.VarChar, , ParameterDirection.Input)
            db.AddParameter("@CampusID", CampusID, SqlDbType.VarChar, , ParameterDirection.Input)



            ds = db.RunParamSQLDataSet_SP("dbo.usp_GetStudentPunchTypefromSpecialCodes")

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    PunchType = ds.Tables(0).Rows(0)(0).ToString
                    If PunchType = 1 Then
                        Return TimeClockPunchType.PunchIn

                    Else
                        Return TimeClockPunchType.PunchOut
                    End If
                End If

            End If
            Return TimeClockPunchType.PunchIn


        End Function
#End Region


        Public Shared Function GetClsSectionMeetingScheduleInfo(ByVal ClsSectMeetingID As String) As ClsSectionMeetingScheduleInfo
            Dim db As New SQLDataAccess
            Dim dr As SqlDataReader
            Dim PunchType As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim info As New ClsSectionMeetingScheduleInfo
            Try
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

                db.AddParameter("@ClsSectMeetingID", ClsSectMeetingID, SqlDbType.VarChar, 50, ParameterDirection.Input)

                dr = db.RunParamSQLDataReader_SP("dbo.usp_GetClsSectionScheduleInfo")


                If dr.Read() Then
                    info.ClsSectMeetingID = ClsSectMeetingID
                    info.IsInDB = True
                    If Not (dr("ClsSectionId") Is System.DBNull.Value) Then info.ClsSectionID = dr("ClsSectionId").ToString()
                    If Not (dr("Code") Is System.DBNull.Value) Then info.Code = dr("Code")
                    If Not (dr("Descrip") Is System.DBNull.Value) Then info.Descrip = dr("Descrip")
                    If Not (dr("UnitTypeID") Is System.DBNull.Value) Then info.UnitType = dr("UnitTypeID").ToString
                    If Not (dr("StartDate") Is System.DBNull.Value) Then info.ClassStartDate = dr("StartDate").ToString
                    If Not (dr("EndDate") Is System.DBNull.Value) Then info.ClassEndDate = dr("EndDate").ToString

                    info.DetailsDT = GetDetailsDT(ClsSectMeetingID)

                    Dim c As Integer = info.DetailsDT.Rows.Count
                    Dim details(c - 1) As ClsSectMeetingScheduledetailsInfo
                    For dow As Integer = 0 To c - 1
                        Dim drs() As DataRow = info.DetailsDT().Select("dw=" + dow.ToString())
                        If drs.Length > 0 Then
                            details(dow) = GetDetailInfo(drs(0))
                        End If
                    Next
                    info.Details = details
                End If

                db.CloseConnection()
                Return info
            Catch ex As System.Exception
            End Try
            Return Nothing



        End Function
        Public Shared Function GetDetailsDT(ByVal ClsSectMeetingID As String) As DataTable
            Dim db As New SQLDataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            db.AddParameter("@ClsSectMeetingID", ClsSectMeetingID, SqlDbType.VarChar, 50, ParameterDirection.Input)

            Dim dt As DataTable = db.RunParamSQLDataSet_SP("usp_GetClsSectionScheduleDetailInfo").Tables(0)

            Dim dtnewdwDatatable As DataTable
            dtnewdwDatatable = dt.Clone

            If dt.Rows.Count = 0 Then
                For i As Integer = 0 To 6
                    Dim dr As DataRow = dt.NewRow
                    dr("dw") = i
                    dt.Rows.Add(dr)
                Next
                Return dt
            ElseIf dt.Rows.Count = 6 Then
                Return dt
            Else
                For i As Integer = 0 To 6
                    Dim dr As DataRow = dtnewdwDatatable.NewRow

                    Dim row() As DataRow
                    row = dt.Select("dw=" + i.ToString)
                    If row.Length = 1 Then
                        dtnewdwDatatable.ImportRow(row(0))

                    Else
                        dr("dw") = i
                        dr("ClsSectMeetingID") = ClsSectMeetingID
                        dr("timein") = DateTime.MinValue
                        dr("timeout") = DateTime.MinValue
                        dr("total") = 0
                        dr("allow_earlyin") = False
                        dr("allow_lateout") = False
                        dr("allow_extrahours") = False
                        dr("Check_tardyin") = False
                        dr("max_beforetardy") = DateTime.MinValue
                        dr("tardy_intime") = DateTime.MinValue


                        dtnewdwDatatable.Rows.Add(dr)
                    End If


                Next
                Return dtnewdwDatatable

            End If

        End Function

        Protected Shared Function GetDetailInfo(ByVal dr As DataRow) As ClsSectMeetingScheduledetailsInfo
            Dim info As New ClsSectMeetingScheduledetailsInfo
            Try
                info.ClsSectMeetingID = dr("ClsSectMeetingID").ToString()
                If Not DBNull.Value.Equals(dr("dw")) Then info.dw = dr("dw")
                If Not DBNull.Value.Equals(dr("total")) Then info.Total = dr("total")
                If Not DBNull.Value.Equals(dr("timein")) Then info.TimeIn = dr("timein")
                If Not DBNull.Value.Equals(dr("timeout")) Then info.TimeOut = dr("timeout")
                If Not DBNull.Value.Equals(dr("allow_earlyin")) Then info.Allow_EarlyIn = dr("allow_earlyin")
                If Not DBNull.Value.Equals(dr("allow_lateout")) Then info.Allow_LateOut = dr("allow_lateout")
                If Not DBNull.Value.Equals(dr("allow_extrahours")) Then info.Allow_ExtraHours = dr("allow_extrahours")
                If Not DBNull.Value.Equals(dr("check_tardyin")) Then info.Check_TardyIn = dr("check_tardyin")
                If Not DBNull.Value.Equals(dr("max_beforetardy")) Then info.MaxBeforeTardy = dr("max_beforetardy")
                If Not DBNull.Value.Equals(dr("tardy_intime")) Then info.TardyInTime = dr("tardy_intime")
            Catch ex As System.Exception
            End Try

            Return info
        End Function


        Public Function GetClsSectMeetingsforaStudent(ByVal StuEnrollId As String, ByVal CampusID As String) As DataTable
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            If MyAdvAppSettings.AppSettings("SchedulingMethod", CampusID).ToString = "Traditional" Then

                Return db.RunParamSQLDataSet("dbo.usp_GetClsSectMeetingsforaStudentwithoutPeriods", Nothing, "SP").Tables(0)
            Else
                Return db.RunParamSQLDataSet("dbo.usp_GetClsSectMeetingsforaStudentwithPeriods", Nothing, "SP").Tables(0)

            End If

        End Function



        Public Function GetSpecialCodesforgivenCampus(ByVal CampusID As String) As DataTable
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            db.AddParameter("@CampusID", CampusID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            Return db.RunParamSQLDataSet("dbo.usp_GetSpecialCodesforgivenCampus", Nothing, "SP").Tables(0)

        End Function

        Public Function GetSpecialCodesAndClsSectionMeetingforStudent(ByVal StuEnrollId As String, ByVal CampusID As String) As DataTable
            Dim db As New DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            If MyAdvAppSettings.AppSettings("SchedulingMethod", CampusID).ToString = "Traditional" Then

                Return db.RunParamSQLDataSet("dbo.usp_GetClsSectMeetingsandPunchCodeforaStudentwithoutPeriods", Nothing, "SP").Tables(0)
            Else
                Return db.RunParamSQLDataSet("dbo.usp_GetClsSectMeetingsandPunchCodeforaStudentwithPeriods", Nothing, "SP").Tables(0)

            End If
        End Function

        Public Function IsClsSectionUsesTimeClock(ByVal clsSectionId As String) As Boolean
            Dim db As New DataAccess
            Dim sb As New System.Text.StringBuilder
            Dim numRecords As Integer

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            With sb
                .Append(" SELECT ClsSectionID FROM dbo.arClassSections  A Inner JOIN arreqs B ON A.ReqID=B.ReqId ")
                .Append(" WHERE ClsSectionId = ? AND UseTimeClock =1 ")

            End With

            db.AddParameter("clssectid", clsSectionId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Dim dt As New DataTable
            dt = db.RunParamSQLDataSet(sb.ToString).Tables(0)

            If dt.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
            Return False
        End Function


    End Class




End Namespace

