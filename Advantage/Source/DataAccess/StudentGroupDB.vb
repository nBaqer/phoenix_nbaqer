Option Explicit On 
Imports System.Xml
Public Class StudentGroupDB
    'Public Sub AddStudentGroup(ByVal StudentGroupInfo As StudentGroupInfo, ByVal Details As DataTable)
    '    Dim db As New DataAccess
    '    Dim strSQL As New StringBuilder
    '    With strSQL
    '        .Append("INSERT INTO sySGroups")
    '        .Append("(SGroupId, StatusId, Descrip, EmpId, IsPublic, SGroupTypeId, SQLStatement, ModUser, ModDate) ")
    '        .Append("VALUES(?,?,?,?,?,?,?,?,?)")
    '    End With
    '    db.AddParameter("@Param1", StudentGroupInfo.SGroupId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@Param2", StudentGroupInfo.StatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@Param3", StudentGroupInfo.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@Param4", StudentGroupInfo.EmpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@Param5", StudentGroupInfo.IsPublic, DataAccess.OleDbDataType.OleDbBoolean, 1, ParameterDirection.Input)
    '    db.AddParameter("@Param6", StudentGroupInfo.SGroupTypeId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
    '    db.AddParameter("@Param7", StudentGroupInfo.SQLStatement, DataAccess.OleDbDataType.OleDbString, 1000, ParameterDirection.Input)
    '    db.AddParameter("@Param8", "Corey", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@Param9", Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '    Try
    '        db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    db.ClearParameters()
    '    strSQL.Remove(0, strSQL.Length)
    '    If Not IsNothing(Details) Then
    '        InsertUpdateDeleteStudentInGroup(StudentGroupInfo.SGroupId, Details)
    '    End If
    'End Sub
    'Public Sub UpdateStudentGroup(ByVal StudentGroupInfo As StudentGroupInfo, ByVal Details As DataTable)
    '    Dim db As New DataAccess
    '    Dim strSQL As New StringBuilder
    '    With strSQL
    '        .Append("UPDATE sySGroups SET ")
    '        .Append("StatusId = ?")
    '        .Append(",Descrip = ?")
    '        .Append(",EmpId = ?")
    '        .Append(",IsPublic = ?")
    '        .Append(",SGroupTypeId = ?")
    '        .Append(",SQLStatement = ?")
    '        .Append(",ModUser = ?")
    '        .Append(",ModDate = ?")
    '        .Append(" WHERE SGroupId = ? ")
    '    End With
    '    db.AddParameter("@Param2", StudentGroupInfo.StatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@Param3", StudentGroupInfo.Descrip, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@Param4", StudentGroupInfo.EmpId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@Param5", StudentGroupInfo.IsPublic, DataAccess.OleDbDataType.OleDbBoolean, 1, ParameterDirection.Input)
    '    db.AddParameter("@Param6", StudentGroupInfo.SGroupTypeId, DataAccess.OleDbDataType.OleDbInteger, 4, ParameterDirection.Input)
    '    db.AddParameter("@Param7", StudentGroupInfo.SQLStatement, DataAccess.OleDbDataType.OleDbString, 1000, ParameterDirection.Input)
    '    db.AddParameter("@Param8", "Corey", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
    '    db.AddParameter("@Param9", Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '    db.AddParameter("@Param1", StudentGroupInfo.SGroupId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    Try
    '        db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    db.ClearParameters()
    '    strSQL.Remove(0, strSQL.Length)
    '    If Not IsNothing(Details) Then
    '        InsertUpdateDeleteStudentInGroup(StudentGroupInfo.SGroupId, Details)
    '    End If
    'End Sub
    'Public Sub DeleteStudentGroup(ByVal SGroupId As Guid)
    '    Dim db As New DataAccess
    '    Dim strSQL As New StringBuilder
    '    With strSQL
    '        .Append("DELETE FROM sySGroups WHERE SGroupId = ?")
    '    End With
    '    db.AddParameter("@Param1", SGroupId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    Try
    '        db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    db.ClearParameters()
    '    strSQL.Remove(0, strSQL.Length)
    'End Sub

    'Public Sub InsertUpdateDeleteStudentInGroup(ByVal SGroupId As Guid, ByVal Details As DataTable)
    '    'Get the current list of awardschedules
    '    ' Get the changes between the current and what the user modified
    '    Dim currentdt As DataTable
    '    Dim newdt As New DataTable
    '    currentdt = Details
    '    newdt = currentdt.GetChanges(DataRowState.Added Or DataRowState.Deleted)
    '    Dim dr As DataRow
    '    If Not IsNothing(newdt) Then
    '        For Each dr In newdt.Rows
    '            Select Case dr.RowState
    '                Case DataRowState.Added
    '                    Dim StudentGroupStudentInfo As New StudentGroupStudentInfo
    '                    StudentGroupStudentInfo.SSGroupsRelId = dr("SSGroupsRelId")
    '                    StudentGroupStudentInfo.SGroupId = dr("SGroupId")
    '                    StudentGroupStudentInfo.StudentId = dr("StudentId")
    '                    StudentGroupStudentInfo.DateAdded = CDate(dr("DateAdded"))
    '                    StudentGroupStudentInfo.AddedBy = dr("AddedById")
    '                    StudentGroupStudentInfo.DateRemoved = CDate(dr("DateRemoved"))
    '                    StudentGroupStudentInfo.RemovedBy = dr("RemovedById")
    '                    AddStudentToStudentGroup(StudentGroupStudentInfo)
    '                Case DataRowState.Deleted
    '                    RemoveStudentFromStudentGroup(XmlConvert.ToGuid(dr("SSGroupsRelId").ToString))
    '            End Select
    '        Next
    '    End If
    'End Sub
    'Public Sub AddStudentToStudentGroup(ByVal StudentGroupStudentInfo As StudentGroupStudentInfo)
    '    Dim db As New DataAccess
    '    Dim strSQL As New StringBuilder
    '    With strSQL
    '        .Append("INSERT INTO syStudentSGroupsRel")
    '        .Append("(SSGroupsRelId, SGroupId, StudentId, DateAdded, AddedById, DateRemoved, RemovedById) ")
    '        .Append("VALUES(?,?,?,?,?,?,?)")
    '    End With

    '    db.AddParameter("@Param1", StudentGroupStudentInfo.SSGroupsRelId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@Param2", StudentGroupStudentInfo.SGroupId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@Param3", StudentGroupStudentInfo.StudentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@Param4", StudentGroupStudentInfo.DateAdded, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '    db.AddParameter("@Param5", StudentGroupStudentInfo.AddedBy, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@Param6", StudentGroupStudentInfo.DateRemoved, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '    db.AddParameter("@Param7", StudentGroupStudentInfo.RemovedBy, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    Try
    '        db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    db.ClearParameters()
    '    strSQL.Remove(0, strSQL.Length)
    'End Sub
    'Public Sub UpdateStudentToStudentGroup(ByVal StudentGroupStudentInfo As StudentGroupStudentInfo)
    '    Dim db As New DataAccess
    '    Dim strSQL As New StringBuilder
    '    With strSQL
    '        .Append("UPDATE syStudentSGroupsRel SET ")
    '        .Append("SGroupId = ?")
    '        .Append(",StudentId = ?")
    '        .Append(",DateAdded = ?")
    '        .Append(",AddedBy = ?")
    '        .Append(",DateRemoved = ?")
    '        .Append(",RemovedBy = ?")
    '        .Append(" WHERE SSGroupsRelId = ? ")
    '    End With


    '    db.AddParameter("@Param2", StudentGroupStudentInfo.SGroupId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@Param3", StudentGroupStudentInfo.StudentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@Param4", StudentGroupStudentInfo.DateAdded, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '    db.AddParameter("@Param5", StudentGroupStudentInfo.AddedBy, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@Param6", StudentGroupStudentInfo.DateRemoved, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
    '    db.AddParameter("@Param7", StudentGroupStudentInfo.RemovedBy, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    db.AddParameter("@Param1", StudentGroupStudentInfo.SSGroupsRelId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    Try
    '        db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    db.ClearParameters()
    '    strSQL.Remove(0, strSQL.Length)
    'End Sub
    'Public Sub RemoveStudentFromStudentGroup(ByVal SSGroupsRelId As Guid)
    '    Dim db As New DataAccess
    '    Dim strSQL As New StringBuilder
    '    With strSQL
    '        .Append("DELETE FROM syStudentSGroupsRel WHERE SSGroupsRelId = ?")
    '    End With
    '    db.AddParameter("@Param1", SSGroupsRelId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    Try
    '        db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    db.ClearParameters()
    '    strSQL.Remove(0, strSQL.Length)
    'End Sub
    'Public Function GetStudentGroups() As DataSet
    '    Dim db As New DataAccess
    '    Dim ds As New DataSet
    '    Dim da As New OleDbDataAdapter
    '    Dim strSQL As New StringBuilder

    '    With strSQL
    '        .Append(" SELECT SGroupId, StatusId, Descrip, EmpId, IsPublic, SGroupTypeId, SQLStatement")
    '        .Append(" FROM sySGroups")
    '    End With
    '    db.OpenConnection()
    '    Try
    '        da = db.RunParamSQLDataAdapter(strSQL.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da.Fill(ds, "StudentGroupList")
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try

    '    db.ClearParameters()

    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    strSQL.Remove(0, strSQL.Length)

    '    ' Return the dataset
    '    Return ds

    'End Function
    'Public Function GetStudentsInGroup(ByVal SGroupId As Guid) As DataSet
    '    Dim db As New DataAccess
    '    Dim ds As New DataSet
    '    Dim da As New OleDbDataAdapter
    '    Dim strSQL As New StringBuilder

    '    With strSQL
    '        .Append("SELECT a.SSGroupsRelId, a.SGroupId, a.StudentId, b.LastName, b.FirstName, b.MiddleName, d.PrgVerDescrip, e.Status, f.CampDescrip")
    '        .Append(" FROM syStudentSGroupsRel a, arStudent b, arStuEnrollments c, arPrgVersions d, syStatuses e, syCampuses f")
    '        .Append(" WHERE a.SGroupId = ?  AND")
    '        .Append(" a.StudentId = b.StudentId  AND")
    '        .Append(" a.StudentId = c.StudentId AND")
    '        .Append(" b.StudentStatus = e.StatusId AND")
    '        .Append(" c.CampusId = f.CampusId AND")
    '        .Append(" c.PrgVerId = d.PrgVerId")
    '    End With
    '    db.OpenConnection()
    '    db.AddParameter("@Param1", SGroupId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    Try
    '        da = db.RunParamSQLDataAdapter(strSQL.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da.Fill(ds, "StudentsInGroupList")
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try

    '    db.ClearParameters()

    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    strSQL.Remove(0, strSQL.Length)

    '    ' Return the dataset
    '    Return ds
    'End Function
    'Public Function GetDropDownLists() As DataSet
    '    Dim db As New DataAccess
    '    Dim ds As New DataSet
    '    Dim da As New OleDbDataAdapter
    '    Dim strSQLString As New StringBuilder

    '    With strSQLString
    '        .Append("SELECT * ")
    '        .Append(" FROM syStatuses")
    '    End With
    '    db.OpenConnection()
    '    Try
    '        da = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da.Fill(ds, "StatusList")
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    db.ClearParameters()
    '    strSQLString.Remove(0, strSQLString.Length)

    '    Dim da2 As New OleDbDataAdapter

    '    With strSQLString
    '        .Append("SELECT EmpId, LastName, FirstName, MI ")
    '        .Append(" FROM hrEmployees")
    '    End With
    '    db.OpenConnection()
    '    Try
    '        da2 = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da2.Fill(ds, "OwnerList")
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Dim da3 As New OleDbDataAdapter

    '    db.ClearParameters()
    '    strSQLString.Remove(0, strSQLString.Length)
    '    With strSQLString
    '        .Append("SELECT * ")
    '        .Append(" FROM sySGroupTypes")
    '    End With
    '    db.OpenConnection()
    '    Try
    '        da3 = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da3.Fill(ds, "TypeList")
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try

    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    Return ds

    'End Function
    Public Function GetAllStudents() As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder

        With strSQLString
            .Append("SELECT StudentId, FirstName, LastName, MiddleName ")
            .Append(" FROM arStudent")
        End With
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "StudentList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()

        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        strSQLString.Remove(0, strSQLString.Length)
        Return ds
    End Function
    'Public Function GetGroupsForAStudent(ByVal StudentId As Guid) As DataSet
    '    Dim db As New DataAccess
    '    Dim ds As New DataSet
    '    Dim da As New OleDbDataAdapter
    '    Dim strSQLString As New StringBuilder

    '    With strSQLString
    '        .Append("SELECT a.AddedById, a.StudentId, a.SGroupId, a.SSGroupsRelId, a.DateAdded, a.DateRemoved, b.Descrip, c.LastName as AddedLastName, c.FirstName as AddedFirstName, c.MI as AddedMI, a.AddedById, a.RemovedById")
    '        .Append(" FROM syStudentSGroupsRel a, sySGroups b, hrEmployees c")
    '        .Append(" WHERE a.StudentId = ? AND")
    '        .Append(" a.SGroupId = b.SGroupId AND")
    '        .Append(" a.AddedById = c.EmpId")
    '        '.Append(" a.RemovedById = d.EmpId")
    '    End With
    '    db.OpenConnection()
    '    db.AddParameter("@Param1", StudentId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
    '    Try
    '        da = db.RunParamSQLDataAdapter(strSQLString.ToString)
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Try
    '        da.Fill(ds, "GroupsForStudentList")
    '    Catch ex As System.Exception
    '        'Redirect to error page.
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    db.ClearParameters()

    '    If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

    '    strSQLString.Remove(0, strSQLString.Length)
    '    Return ds
    'End Function
End Class
