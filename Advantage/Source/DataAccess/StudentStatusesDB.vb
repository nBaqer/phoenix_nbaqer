Option Strict On
Public Class StudentStatusesDB

    Public Sub AddStatusChangeCode(ByVal StatusChangeInfo As StatusChangeInfo)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("INSERT INTO syStatusChanges")
            .Append("(StatusChangeId, OrigStatusId, NewStatusId, RoleId, StatusId, ModDate, ModUser) ")
            .Append("VALUES(?,?,?,?,?,?,?)")
        End With
        db.AddParameter("@StatusChangeId", System.Guid.NewGuid, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@OriginalStatusId", StatusChangeInfo.OriginalStatus, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@NewStatusId", StatusChangeInfo.NewStatus, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@RoleId", StatusChangeInfo.Role, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@StatusId", StatusChangeInfo.Status, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@ModDate", Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        db.AddParameter("@ModUser", "Corey", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
    End Sub
    Public Sub UpdateStatusChangeCode(ByVal StatusChangeInfo As StatusChangeInfo)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("UPDATE syStatusChanges SET ")
            .Append("OrigStatusId = ?")
            .Append(",NewStatusId = ?")
            .Append(",RoleId = ?")
            .Append(",StatusId = ?")
            .Append(",ModDate = ?")
            .Append(",ModUser = ?")
            .Append(" WHERE StatusChangeId = ? ")
        End With

        db.AddParameter("@OrigStatusId", StatusChangeInfo.OriginalStatus, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@NewStatusId", StatusChangeInfo.NewStatus, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@RoleId", StatusChangeInfo.Role, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@StatusId", StatusChangeInfo.Status, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.AddParameter("@ModDate", Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
        db.AddParameter("@ModUser", "Corey", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@StatusChangeId", StatusChangeInfo.StatusChangeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)

    End Sub
    Public Sub DeleteStatusChangeCode(ByVal StatusChangeId As Guid)
        Dim db As New DataAccess
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("DELETE FROM syStatusChanges WHERE StatusChangeId = ?")
        End With
        db.AddParameter("@StatusChangeId", StatusChangeId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        Try
            db.RunParamSQLExecuteNoneQuery(strSQL.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.ClearParameters()
        strSQL.Remove(0, strSQL.Length)
    End Sub
    Public Function PopulateStudentStatusDropDownList() As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da1 As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder
        With strSQLString
            .Append("SELECT StatusCodeId, StatusCodeDescrip FROM syStatusCodes")
        End With
        db.OpenConnection()
        Try
            da1 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da1.Fill(ds, "OrigNewList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Dim da2 As New OleDbDataAdapter
        strSQLString.Remove(0, strSQLString.Length)

        With strSQLString
            .Append("SELECT RoleId, Role FROM syRoles")
        End With
        'db.OpenConnection()
        Try
            da2 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da2.Fill(ds, "RoleList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)

        Dim da3 As New OleDbDataAdapter
        strSQLString.Remove(0, strSQLString.Length)

        With strSQLString
            .Append("SELECT * FROM syStatuses")
        End With
        'db.OpenConnection()
        Try
            da3 = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da3.Fill(ds, "Status")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        strSQLString.Remove(0, strSQLString.Length)

        db.CloseConnection()
        Return ds
    End Function
    Public Function GetStatusChanges() As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim strSQLString As New StringBuilder

        With strSQLString
            .Append("SELECT a.StatusChangeId, a.OrigStatusId, a.NewStatusId, a.RoleId, a.StatusId, b.StatusCodeDescrip as OrigStatusDescrip, c.StatusCodeDescrip as NewStatusDescrip, d.Role ")
            .Append(" FROM syStatusChanges a, syStatusCodes b, syStatusCodes c, syRoles d")
            .Append(" WHERE a.OrigStatusId = b.StatusCodeId AND")
            .Append(" a.NewStatusId = c.StatusCodeId AND")
            .Append(" a.RoleId = d.RoleId")
        End With
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(strSQLString.ToString)
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            da.Fill(ds, "StatusChangesList")
            Return ds
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        Finally

            If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        End Try

    End Function
    Public Function GetStatusCodesChangesAuthorized(ByVal UserName As String, ByVal CurrentStudentStatusId As Guid) As DataSet

        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim strSQL As New StringBuilder
        With strSQL
            .Append("SELECT DISTINCT e.OrigStatusId, e.NewStatusId, f.StatusCodeDescrip FROM syusers a, hremployees b, hremproles c, syRoles d, syStatusChanges e, syStatusCodes f")
            .Append(" WHERE a.username = b.username and")
            .Append(" b.EmpId = c.empid and")
            .Append(" c.roleid = d.roleid and")
            .Append(" d.RoleId = e.RoleId and")
            .Append(" e.NewStatusId = f.StatusCodeId and")
            .Append(" a.username = ? and")
            .Append(" e.OrigStatusId = ?")
        End With

        db.AddParameter("@UserName", UserName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@CurrentStudentStatus", CurrentStudentStatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
        db.OpenConnection()
        Try
            ds = db.RunParamSQLDataSet(strSQL.ToString, "StudentStatusList")
        Catch ex As System.Exception
            'Redirect to error page.
            Throw New Exception(ex.Message, ex)
        End Try
        db.CloseConnection()
        db.ClearParameters()
        Return ds
    End Function
    
End Class
