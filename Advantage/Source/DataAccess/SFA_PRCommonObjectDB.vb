' ****************************************************************
' Utility routines and objects used for report custom preferences 
' for both detail and summary reports
' ****************************************************************

Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer

Public Class SFA_PRCommonObjectDB

	' Table used to store report's custom preferences
	Private Const PrefTableName As String = "syRptPrefsIPEDS_SFAPR"

	Public Shared Function GetFundSources(ByVal CampusId As String) As DataTable
	' Gets a list of appropriate Fund Sources to be presented to user, to allow 
	'	user to optionally assign each Fund Source to a report category

		Dim sb As New System.Text.StringBuilder

		With sb
			.Append("SELECT ")

			' get needed columns 
			.Append("saFundSources.FundSourceId,  ")
			.Append("saFundSources.FundSourceCode, ")
			.Append("saFundSources.FundSourceDescrip ")
			.Append("FROM ")
			.Append("saFundSources, syCampuses, syCampGrps, syCmpGrpCmps ")

			' establish relationship and filter to ensure we only get 
			'	Fund Sources assigned to the Campus Group to which the 
			'	selected Campus belongs
			.Append("WHERE ")
			.Append("saFundSources.CampGrpId = syCampGrps.CampGrpId AND ")
			.Append("syCampGrps.CampGrpId = syCmpGrpCmps.CampGrpId AND ")
			.Append("syCmpGrpCmps.CampusId = syCampuses.CampusId AND ")
			.Append("syCampuses.CampusId = '" & CampusId & "' AND ")

			.Append("(")
			' get Fund Sources which are Loans or Grants, and are NOT Title IV
			.Append(" (")
			.Append("  (SELECT saAwardTypes.Descrip ")
			.Append("   FROM saAwardTypes ")
			.Append("   WHERE saAwardTypes.AwardTypeId = saFundSources.AwardTypeId)IN ('Loan','Grant') AND ")
			.Append("  (saFundSources.TitleIV IS NULL OR saFundSources.TitleIV <> 1) ")
			.Append(" ) ")
			.Append(" OR ")
			' get Fund Sources which are NOT Loans or Grants
			.Append(" (")
			.Append("  saFundSources.AwardTypeId IS NULL OR ")
			.Append("  (SELECT saAwardTypes.Descrip ")
			.Append("   FROM saAwardTypes ")
			.Append("   WHERE saAwardTypes.AwardTypeId = saFundSources.AwardTypeId) NOT IN ('Loan','Grant') ")
			.Append(" )")
			.Append(")")

			' sort list by Fund Source description and code
			.Append("ORDER BY ")
			.Append("saFundSources.FundSourceDescrip, saFundSources.FundSourceCode")
		End With

		' run query and return DataTable
		Return IPEDSDB.DataAccessIPEDS().RunSQLDataSet(sb.ToString).Tables(0).Copy

	End Function

	Public Shared Sub AddUserPref(ByVal PrefId As String, ByVal dtRptCats As DataTable)
	' Save a list of Fund Sources and the report categories to which they are assigned -
	'	the user's preference is saved under a general Preference Id, as are standard
	'	report preferences

		Dim db As DataAccess = IPEDSDB.DataAccessIPEDS()
		Dim groupTrans As OleDbTransaction
		Dim sb As New System.Text.StringBuilder
		Dim dr As DataRow

		With sb
			.Append("INSERT INTO " & PrefTableName & " ")
			.Append("(PrefId, FundSourceId, RptCatId) ")
			.Append("VALUES(?,?,?)")
		End With

		' encapsulate all INSERTs in one transaction
		groupTrans = db.StartTransaction

		Try
			For Each dr In dtRptCats.Rows
				With db
					.AddParameter("@PrefId", PrefId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
					.AddParameter("@FundSourceId", dr("FundSourceId"), DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
					.AddParameter("@RptCatId", dr("RptCatId"), DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
					.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
					.ClearParameters()
				End With
			Next

			' commit transaction 
			groupTrans.Commit()

		Catch ex As OleDbException
			' rollback transaction if there were errors
			groupTrans.Rollback()
			' report the exception
			Throw New BaseException(DALExceptions.BuildErrorMessage(ex))

		Catch ex As Exception
			' rollback transaction if there were errors
			groupTrans.Rollback()
			' report the exception
			Throw New BaseException(ex.Message())

		Finally
			db.Dispose()

		End Try

	End Sub

	Public Shared Sub DeleteUserPref(ByVal PrefId As String)
	' Deletes a user's custom report preference

		Dim db As DataAccess = IPEDSDB.DataAccessIPEDS()
		Dim sb As New System.Text.StringBuilder

		With sb
			.Append("DELETE FROM " & PrefTableName & " ")
			.Append("WHERE PrefId = ?")
		End With

		With db
			.AddParameter("@PrefId", PrefId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
			.RunParamSQLExecuteNoneQuery(sb.ToString)
			.Dispose()
		End With
	End Sub

	Public Shared Function GetUserPref(ByVal PrefId As String) As DataTable
	 ' Gets a list of Fund Sources and the report categories to which they 
	 '	are assigned, as saved by the user

		Dim db As DataAccess = IPEDSDB.DataAccessIPEDS()
		Dim sb As New System.Text.StringBuilder

		With sb
			.Append("SELECT FundSourceId, RptCatId ")
			.Append("FROM " & PrefTableName & " ")
			.Append("WHERE PrefId = ?")
		End With

		Try
			With db
				.AddParameter("@PrefId", PrefId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
				Return .RunParamSQLDataSet(sb.ToString).Tables(0).Copy
			End With

		Catch ex As Exception
			Throw New BaseException("Error getting custom report preference: " & ex.Message)

		Finally
			db.Dispose()

		End Try

	End Function

End Class
