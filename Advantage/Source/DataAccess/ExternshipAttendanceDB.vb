Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' ExternshipAttendanceDB.vb
'
' ExternshipAttendanceDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class ExternshipAttendanceDB
    Public Function GetExternshipAttendanceDS(ByVal stuEnrollId As String) As DataSet

        '   create dataset
        Dim ds As New DataSet


        '   build select query for the ExternshipAttendanceystems data adapter
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT ")
            .Append("       EA.ExternshipAttendanceId, ")
            .Append("       EA.StuEnrollId, ")
            .Append("       EA.GrdComponentTypeId, ")
            .Append("       EA.AttendedDate, ")
            .Append("       EA.HoursAttended,EA.Comments, ")
            .Append("       EA.ModUser, ")
            .Append("       EA.ModDate ")
            .Append("FROM   arExternshipAttendance EA ")
            .Append("WHERE  EA.StuEnrollId = ? ")
        End With


        '   build select command
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(GetAdvAppSettings.AppSettings("ConString")))

        '   add StuEnrollId parameter
        sc.Parameters.Add(New OleDbParameter("StuEnrollId", stuEnrollId))

        '   Create adapter to handle ExternshipAttendance table
        Dim da As New OleDbDataAdapter(sc)

        '   Fill ExternshipAttendanceystems table
        da.Fill(ds, "arExternshipAttendance")

        '   create primary and foreign key constraints

        '   set primary key for arExternshipAttendance table
        Dim pk0(0) As DataColumn
        pk0(0) = ds.Tables("arExternshipAttendance").Columns("ExternshipAttendanceId")
        ds.Tables("arExternshipAttendance").PrimaryKey = pk0

        '   return dataset
        Return ds

    End Function
    Public Function UpdateExternshipAttendanceDS(ByVal ds As DataSet, ByVal user As String) As String


        '   all updates must be encapsulated as one transaction
        Dim connection As New OleDbConnection(GetAdvAppSettings.AppSettings("ConString"))
        connection.Open()
        Dim groupTrans As OleDbTransaction = connection.BeginTransaction()

        Try
            'update (if exists) record in arGrdBookConversionResults
            UpdateGradeBookConversionResults(ds.Tables("arExternshipAttendance"), groupTrans, user)

            '   build select query for the ExternshipAttendanceystems data adapter
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("       EA.ExternshipAttendanceId, ")
                .Append("       EA.StuEnrollId, ")
                .Append("       EA.GrdComponentTypeId, ")
                .Append("       EA.AttendedDate, ")
                .Append("       EA.HoursAttended,EA.Comments, ")
                .Append("       EA.ModUser, ")
                .Append("       EA.ModDate ")
                .Append("FROM   arExternshipAttendance EA ")
            End With

            '   build select command
            Dim ExternshipAttendanceSelectCommand As New OleDbCommand(sb.ToString, connection, groupTrans)

            '   Create adapter to handle ExternshipAttendance table
            Dim ExternshipAttendanceDataAdapter As New OleDbDataAdapter(ExternshipAttendanceSelectCommand)

            '   build insert, update and delete commands for ExternshipAttendance table
            Dim cb As New OleDbCommandBuilder(ExternshipAttendanceDataAdapter)

            '   insert added rows in ExternshipAttendance table
            ExternshipAttendanceDataAdapter.Update(ds.Tables("arExternshipAttendance").Select(Nothing, Nothing, DataViewRowState.CurrentRows))
            ExternshipAttendanceDataAdapter.Update(ds.Tables("arExternshipAttendance").Select(Nothing, Nothing, DataViewRowState.Deleted))

            '   everything went fine - commit transaction
            groupTrans.Commit()

            '   return no errors
            Return ""

        Catch ex As OleDb.OleDbException
            '   something went wrong
            '   rollback transaction
            groupTrans.Rollback()

            '   return error message
            Return DALExceptions.BuildErrorMessage(ex)
        Finally

            '   close connection
            connection.Close()
        End Try

    End Function
    Private Sub UpdateGradeBookConversionResults(ByVal dt As DataTable, ByVal groupTrans As OleDbTransaction, ByVal user As String)
        If dt.Rows.Count = 0 Then Return
        Dim netChange As Decimal = 0.0
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim row As DataRow = dt.Rows(i)
            Select Case row.RowState
                Case DataRowState.Added
                    netChange += row("HoursAttended")
                Case DataRowState.Deleted
                    netChange -= row("HoursAttended", DataRowVersion.Original)
                Case DataRowState.Modified
                    Dim originalValue As Decimal = row("HoursAttended", DataRowVersion.Original)
                    Dim updatedValue As Decimal = row("HoursAttended", DataRowVersion.Current)
                    netChange += updatedValue - originalValue
            End Select
        Next
        Dim rows() As DataRow = dt.Select(Nothing, "AttendedDate desc", DataViewRowState.CurrentRows)
        Dim latestDate As Date
        Dim stuEnrollId As Guid
        Dim grdComponentTypeId As Guid
        If rows.Length > 0 Then
            latestDate = rows(0)("AttendedDate")
            stuEnrollId = rows(0)("StuEnrollId")
            grdComponentTypeId = rows(0)("GrdComponentTypeId")
        Else
            latestDate = Date.MinValue
            stuEnrollId = dt.Rows(0)("StuEnrollId", DataRowVersion.Original)
            grdComponentTypeId = dt.Rows(0)("GrdComponentTypeId", DataRowVersion.Original)
        End If

        Dim sb As New StringBuilder()
        With sb
            .Append("update arGrdBkConversionResults ")
            .Append("       set Score = (case when (ISNULL(Score,0) + ?) = 0 then null else (ISNULL(Score,0) + ?) end), PostDate = ?, ModUser = ?, ModDate = ? ")
            .Append("where StuEnrollId= ? and GrdComponentTypeId = ? ")
        End With

        Dim updateCommand As New OleDbCommand(sb.ToString(), groupTrans.Connection, groupTrans)

        updateCommand.Parameters.Add(New OleDbParameter("@Adjustment", netChange))
        updateCommand.Parameters.Add(New OleDbParameter("@Adjustment1", netChange))
        If Not (latestDate = Date.MinValue) Then
            updateCommand.Parameters.Add(New OleDbParameter("@PostDate", latestDate))
        Else
            updateCommand.Parameters.Add(New OleDbParameter("@PostDate", System.DBNull.Value))
        End If
        updateCommand.Parameters.Add(New OleDbParameter("@ModUser", user))
        updateCommand.Parameters.Add(New OleDbParameter("@ModDate", Date.Now))
        updateCommand.Parameters.Add(New OleDbParameter("@StuEnrollId", stuEnrollId))
        updateCommand.Parameters.Add(New OleDbParameter("@GrdComponentTypeId", grdComponentTypeId))
        updateCommand.ExecuteNonQuery()
    End Sub
    Public Function GetRequiredHoursForExternships(ByVal stuEnrollId As String) As String
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            '.Append(" select   ")
            '.Append("        Coalesce(GCT.Descrip, '') + ';' +  ")
            '.Append("		Coalesce((Select descrip from arReqs where ReqId=(Select ReqId from arClassSections where ClsSectionId=GBR.ClsSectionId)), '') + ';' +  ")
            '.Append("		Cast(Coalesce(GBWD.Number, 0.00) As varchar) + ';' +   ")
            '.Append("        Cast(GCT.GrdComponentTypeId as varchar(36))    ")
            '.Append("from	arStuEnrollments SE, arGrdBkResults GBR, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT   ")
            '.Append("where	   ")
            '.Append("		SE.StuEnrollId=GBR.StuEnrollId   ")
            '.Append("and	GBR.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId   ")
            '.Append("and	GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId   ")
            '.Append("and	GCT.SysComponentTypeId=544   ")
            '.Append("and	SE.StuEnrollId = ? ")
            '.Append("and (select PrgVerId from arStuEnrollments where StuEnrollId=GBR.StuEnrollId) in (select PrgVerId from arProgVerDef where ReqId=(Select GrpId from arReqGrpDef where	ReqId=(Select ReqId from arGrdBkWeights where InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId   and GCT.SysComponentTypeId=544 )))  ")
            '.Append("union all  ")
            '.Append("select   ")
            '.Append("        Coalesce(GCT.Descrip, '') + ';' +  ")
            '.Append("		Coalesce((Select descrip from arReqs where ReqId=GBCR.ReqId), '') + ';' +  ")
            '.Append("		Cast(Coalesce(GBWD.Number, 0.00) As varchar) + ';' +   ")
            '.Append("        Cast(GCT.GrdComponentTypeId as varchar(36))    ")
            '.Append("from	arGrdBkConversionResults GBCR, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT   ")
            '.Append("where	   ")
            '.Append("	GBCR.GrdComponentTypeId=GCT.GrdComponentTypeId   ")
            '.Append("and	GBCR.GrdComponentTypeId=GBWD.GrdComponentTypeId   ")
            '.Append("and	GCT.SysComponentTypeId=544   ")
            '.Append("and	GBCR.StuEnrollId = ? ")
            '.Append("and (select PrgVerId from arStuEnrollments where StuEnrollId=GBCR.StuEnrollId) in (select PrgVerId from arProgVerDef where ReqId=(Select GrpId from arReqGrpDef where	ReqId=(Select ReqId from arGrdBkWeights where InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId   and GCT.SysComponentTypeId=544 )))  ")

            'Troy 7/1/2013 To Fix DE9771: QA: Externship hours not shown on the Post Externship attendance page for a certain course.
            'Code below was commented out.
            '.Append(" select   ")
            '.Append("        Coalesce(GCT.Descrip, '') + ';' +  ")
            '.Append("		Coalesce((Select descrip from arReqs where ReqId=(Select ReqId from arClassSections where ClsSectionId=GBR.ClsSectionId)), '') + ';' +  ")
            '.Append("		Cast(Coalesce(GBWD.Number, 0.00) As varchar) + ';' +   ")
            '.Append("        Cast(GCT.GrdComponentTypeId as varchar(36))    ")
            '.Append("from	arStuEnrollments SE, arGrdBkResults GBR, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT   ")
            '.Append("where	   ")
            '.Append("		SE.StuEnrollId=GBR.StuEnrollId   ")
            '.Append("and	GBR.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId   ")
            '.Append("and	GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId   ")
            '.Append("and	GCT.SysComponentTypeId=544   ")
            '.Append("and	SE.StuEnrollId = ? ")
            '.Append("and (select PrgVerId from arStuEnrollments where StuEnrollId=GBR.StuEnrollId) in ")
            '.Append(" (select PrgVerId from arProgVerDef where ReqId=(Select GrpId from arReqGrpDef where	ReqId=(Select ReqId from arGrdBkWeights where InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId   and GCT.SysComponentTypeId=544 ))  ")
            '.Append(" Union Select PrgVerId from arProgVerDef where ReqId=(Select ReqId from arGrdBkWeights where InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId and GCT.SysComponentTypeId=544) ")
            '.Append(" ) ")
            '.Append("union all  ")
            '.Append("select   ")
            '.Append("        Coalesce(GCT.Descrip, '') + ';' +  ")
            '.Append("		Coalesce((Select descrip from arReqs where ReqId=GBCR.ReqId), '') + ';' +  ")
            '.Append("		Cast(Coalesce(GBWD.Number, 0.00) As varchar) + ';' +   ")
            '.Append("        Cast(GCT.GrdComponentTypeId as varchar(36))    ")
            '.Append("from	arGrdBkConversionResults GBCR, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT   ")
            '.Append("where	   ")
            '.Append("	GBCR.GrdComponentTypeId=GCT.GrdComponentTypeId   ")
            '.Append("and	GBCR.GrdComponentTypeId=GBWD.GrdComponentTypeId   ")
            '.Append("and	GCT.SysComponentTypeId=544   ")
            '.Append("and	GBCR.StuEnrollId = ? ")
            '.Append("and (select PrgVerId from arStuEnrollments where StuEnrollId=GBCR.StuEnrollId) in (select PrgVerId from arProgVerDef where ReqId=(Select GrpId from arReqGrpDef where	ReqId=(Select ReqId from arGrdBkWeights where InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId   and GCT.SysComponentTypeId=544 ))  ")
            '.Append(" Union Select PrgVerId from arProgVerDef where ReqId=(Select ReqId from arGrdBkWeights where InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId and GCT.SysComponentTypeId=544) ")
            '.Append(" ) ")
            'Troy 7/1/2013 To Fix DE9771: QA: Externship hours not shown on the Post Externship attendance page for a certain course.
            'Code below was added. The original code was written with an Equi Join against the arGrdBkResults table. This created a problem if the student did not have any entry in that table for the course.
            .Append("SELECT  COALESCE(GCT.Descrip, '') + ';' ")
            .Append("       + COALESCE(RQ.Descrip, '') + ';' + CAST(COALESCE(GBWD.Number, 0.00) AS VARCHAR) ")
            .Append("       + ';' + CAST(GCT.GrdComponentTypeId AS VARCHAR(36)) ")
            .Append("FROM    arStuEnrollments SE ,dbo.arResults RS,arClasssections CS,dbo.arGrdBkWeights GBW,arGrdBkWgtDetails GBWD,arGrdComponentTypes GCT,arReqs RQ ")
            .Append("WHERE   SE.StuEnrollId = RS.StuEnrollId ")
            .Append("        AND RS.TestId=CS.ClsSectionId ")
            .Append("        AND CS.ReqId=GBW.ReqId ")
            .Append("        AND CS.ReqId=RQ.ReqId ")
            .Append("        AND GBW.InstrGrdBkWgtId = GBWD.InstrGrdBkWgtId ")
            .Append("        AND GBWD.GrdComponentTypeId = GCT.GrdComponentTypeId ")
            .Append("        AND GCT.SysComponentTypeId = 544 ")
            .Append("        AND SE.StuEnrollId = ? ")
            .Append("        AND GBW.EffectiveDate = (SELECT MAX(EffectiveDate) ")
            .Append("                                 FROM dbo.arGrdBkWeights GBW2 ")
            .Append("                                 WHERE GBW2.ReqId=CS.ReqId ")
            .Append("                                 AND CS.StartDate >= GBW2.EffectiveDate) ")
            .Append("        and (select PrgVerId from arStuEnrollments where StuEnrollId=RS.StuEnrollId) in  ")
            .Append("        (select PrgVerId from arProgVerDef where ReqId IN (Select GrpId from arReqGrpDef where	ReqId=(Select ReqId from arGrdBkWeights where InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId   and GCT.SysComponentTypeId=544 )) ")
            .Append("          Union Select PrgVerId from arProgVerDef where ReqId =(Select ReqId from arGrdBkWeights where InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId and GCT.SysComponentTypeId=544) ")
            .Append(" ) ")
        End With

        'StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'StuEnrollId
        'db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'Required Hours of externship
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString())
        Dim firstRecord As String = String.Empty
        If dr.Read() Then
            firstRecord = dr(0)
        End If
        dr.Close()
        Return firstRecord

    End Function
    Public Function GetCompletedHoursForExternships(ByVal stuEnrollId As String) As String
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append("select ")
            .Append("         Coalesce(GCT.Descrip, '') + ';' +  ")
            .Append(" 		Coalesce((Select descrip from arReqs where ReqId=GBCR.ReqId), '') + ';' +  ")
            .Append(" 		Cast(Coalesce(GBCR.Score, 0.00) As varchar) + ';' +  ")
            .Append("         Cast(GCT.GrdComponentTypeId as varchar(36))   ")
            .Append(" from	arGrdBkConversionResults GBCR, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT  ")
            .Append(" where	  ")
            .Append(" 	GBCR.GrdComponentTypeId=GCT.GrdComponentTypeId  ")
            .Append(" and	GBCR.GrdComponentTypeId=GBWD.GrdComponentTypeId  ")
            .Append(" and	GCT.SysComponentTypeId=544  ")
            .Append(" and	GBCR.StuEnrollId = ? ")
            .Append("and (select PrgVerId from arStuEnrollments where StuEnrollId=GBCR.StuEnrollId) in (select PrgVerId from arProgVerDef where ReqId=(Select GrpId from arReqGrpDef where	ReqId=(Select ReqId from arGrdBkWeights where InstrGrdBkWgtId=GBWD.InstrGrdBkWgtId)))  ")

        End With

        'StuEnrollId
        db.AddParameter("@StuEnrollId", stuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        'Required Hours of externship
        Return db.RunParamSQLScalar(sb.ToString())

    End Function
    Public Function GetExternshipStatusCode(ByVal campusId As String) As String
        'Connect To The Database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim intRowCount As Integer = 0
        Dim dr As OleDbDataReader
        Dim strStatusCodeId As String = ""
        Try
            With sb
                .Append(" Select Top 1 StatusCodeId FROM syStatusCodes t1,sySysStatus t2 WHERE t1.SysStatusId=t2.SysStatusId ")
                .Append(" and t2.SysStatusId=22 and t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) ")
            End With
            db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            dr = db.RunParamSQLDataReader(sb.ToString)
            While dr.Read()
                strStatusCodeId = CType(dr("StatusCodeId"), Guid).ToString
            End While
            Return strStatusCodeId
        Catch ex As OleDbException
            '   return an error to the client
            Return strStatusCodeId
        Finally
            'Close Connection
            dr.Close()
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            db.CloseConnection()
        End Try
    End Function
    Public Function GetExternshipStatusDescrip(ByVal campusId As String, ByVal statusCodeId As String) As String
        'Connect To The Database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim intRowCount As Integer = 0
        Dim dr As OleDbDataReader
        Dim strStatusCodeId As String = ""
        Try
            With sb
                .Append(" Select Top 1 StatusCodeDescrip FROM syStatusCodes t1,sySysStatus t2 WHERE t1.SysStatusId=t2.SysStatusId ")
                .Append(" and t2.SysStatusId=22 and t1.CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?) and  t1.StatusCodeid <> ? ")
            End With
            db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@statusCodeId", statusCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            dr = db.RunParamSQLDataReader(sb.ToString)
            While dr.Read()
                strStatusCodeId = dr("StatusCodeDescrip").ToString
            End While
            Return strStatusCodeId
        Catch ex As OleDbException
            '   return an error to the client
            Return strStatusCodeId
        Finally
            'Close Connection
            dr.Close()
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            db.CloseConnection()
        End Try
    End Function
    Public Function UpdateStudentEnrollmentStatusWithExternshipStatus(ByVal StuEnrollId As String, ByVal campusId As String, ByVal statusCodeId As String) As String
        'Connect To The Database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim intRowCount As Integer = 0
        Dim dr As OleDbDataReader
        Dim strStatusCodeId As String = GetExternshipStatusCode(campusId)
        Dim strStatusCodeDescrip As String = GetExternshipStatusDescrip(campusId, statusCodeId)

        If Not strStatusCodeId = "" Then
            'If student is presently in currently attending or academic probation status move student to status mapped to "Externship" System status
            With sb
                .Append(" UPDATE arStuEnrollments SET StatusCodeId = ? WHERE StuEnrollId = ? ")
                .Append(" and StatusCodeId in (SELECT Distinct StatusCodeId FROM syStatusCodes WHERE ")
                'commented by balaji on 09/21/2009 as advantage management deceided that 
                'the status should be changed to externship only if the status is currently attending.
                'modification starts here
                '.Append(" (SysStatusId = 9 or SysStatusId=20) and ")
                .Append(" (SysStatusId = 9) and ")
                'modification ends here
                .Append(" CampGrpId in (Select CampGrpId from syCmpGrpCmps where CampusId = ?)) ")
            End With
            db.AddParameter("@StatusCodeId", strStatusCodeId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CampusId", campusId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Try
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                Return strStatusCodeDescrip
            Catch ex As System.Exception
                Return ""
            Finally
                db.CloseConnection()
            End Try
        End If
    End Function
    Public Sub SetIsCourseCompleted(ByVal StuEnrollId As String)
        Dim db As New SQLDataAccess
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Try
            db.OpenConnection()
            db.AddParameter("@StuEnrollId", New Guid(StuEnrollId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)

            db.RunParamSQLExecuteNoneQuery_SP("USP_IsCourseCompleted_ForExternship")
        Catch ex As System.Exception

        Finally
            db.CloseConnection()
        End Try
    End Sub
#Region "Get AdvAppsetting for Manage Config entry"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
End Class
