Imports System.Data
Imports System.Data.OleDb
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class PrgMinorCertDB
    Public Function GetAllDepts() As DataSet
        Dim ds As DataSet

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.DeptId,DeptDescrip ")
                .Append("FROM arDepartments t1 ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function

    Public Function GetMinorsCerts(ByVal DeptId As String) As DataSet
        Dim ds As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New System.Text.StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT t1.PrgVerId,PrgVerDescrip ")
            .Append("FROM arPrgVersions t1 ")
            .Append("WHERE t1.DeptId = ? and t1.advProgTypId in (3,4,8,9) ")
            .Append("ORDER BY t1.PrgVerDescrip")
        End With

        db.AddParameter("@DeptId", DeptId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds

    End Function

    Public Function GetAvailSelectProgs(ByVal DeptId As String, ByVal MinorCertId As String) As DataSet
        'Get the Activities DataList
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim da As New OleDbDataAdapter
        Dim ds As New DataSet
        Dim count As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        With sb
            .Append("SELECT t1.PrgVerId,PrgVerDescrip ")
            .Append("FROM arPrgVersions t1 ")
            .Append("WHERE t1.advProgTypId NOT IN (3,4,8,9) and ")
            .Append("t1.PrgVerId not in (Select ParentId from arPrgMinorCerts t3 where ChildId = ?) ")
            .Append("ORDER BY t1.PrgVerDescrip")
        End With

        db.AddParameter("@ChildId", MinorCertId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query       
        db.OpenConnection()
        da = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da.Fill(ds, "AvailProgs")

        Catch ex As System.Exception

        End Try
        sb.Remove(0, sb.Length)
        db.ClearParameters()

        Dim da2 As New OleDbDataAdapter

        With sb
            .Append("SELECT t1.PrgMinorCertId, t1.ParentId,t1.ChildId, t2.PrgVerDescrip ")
            .Append("FROM arPrgMinorCerts t1, arPrgVersions t2 ")
            .Append("WHERE t1.ChildId = ? and t1.ParentId=t2.PrgVerId ")
            .Append("ORDER BY t2.PrgVerDescrip")
        End With
        'db.OpenConnection()

        db.AddParameter("@clssectId", MinorCertId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        da2 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da2.Fill(ds, "SelectProgs")
            count = ds.Tables("SelectProgs").Rows.Count
        Catch ex As System.Exception

        End Try
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
        Return ds
    End Function
    Public Function GetAvailProgs(ByVal DeptId As String, ByVal MinorCertId As String) As DataSet
        Dim ds As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New System.Text.StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT t1.PrgVerId,PrgVerDescrip ")
            .Append("FROM arPrgVersions t1 ")
            .Append("WHERE t1.DeptId = ? and t1.advProgTypId NOT IN (3,4,8,9) ")
            .Append("ORDER BY t1.PrgVerDescrip")
        End With

        db.AddParameter("@DeptId", DeptId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '   Execute the query
        ds = db.RunParamSQLDataSet(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()

        'Return the dataset
        Return ds

    End Function

    Public Function InsertPrgMinorCert(ByVal PrgMinorCertObj As PrgMinorCertInfo)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim ParentId As String
        Dim ChildId As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        ParentId = PrgMinorCertObj.ParentId
        ChildId = PrgMinorCertObj.ChildId

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("INSERT INTO arPrgMinorCerts(ParentId,ChildId) ")
            .Append("VALUES(?,?)")

            db.AddParameter("@courseid", ParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@transcodeid", ChildId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        End With

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function

    Public Function UpdatePrgMinorCert(ByVal PrgMinorCertObj As PrgMinorCertInfo)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet
        Dim childTyp As String
        Dim childSeq As Integer
        Dim req As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")


        With sb
            .Append("UPDATE arPrgMinorCerts Set ParentId = ?, ChildId = ? ")
            .Append("WHERE  ParentId = ? and ChildId = ?")


            db.AddParameter("@transcodeid", PrgMinorCertObj.ParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@courseid", PrgMinorCertObj.ChildId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        End With

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function

    Public Function DeletePrgMinorCert(ByVal PrgMinorCertObj As PrgMinorCertInfo)
        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the connection string
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        With sb
            .Append("DELETE FROM arPrgMinorCerts ")
            .Append("WHERE ParentId = ? ")
            .Append("AND ChildId = ? ")
        End With

        db.AddParameter("@courseid", PrgMinorCertObj.ParentId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.AddParameter("@transcodeid", PrgMinorCertObj.ChildId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        'Close Connection
        db.CloseConnection()
    End Function
End Class
