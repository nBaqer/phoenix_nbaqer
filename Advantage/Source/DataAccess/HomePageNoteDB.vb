Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' HomePageNotesDB.vb
'
' HomePageNotesDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class HomePageNoteDB
    Public Function GetAllHomePageNotes(ByVal showActiveOnly As String, ByVal ModuleName As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   SHPN.HomePageNoteID, SHPN.ModuleID, SHPN.StatusId, ST.Status, ")
            .Append("SHPN.Title, SHPN.Description, SHPN.ModDate, SHPN.ModUser ")
            .Append("FROM     syHomePageNotes SHPN, syStatuses ST, syModules SM ")
            .Append("WHERE    SHPN.StatusId = ST.StatusId AND SHPN.ModuleID = SM.ModuleID ")
            If Not ModuleName Is Nothing Then
                .Append(" AND   SM.ModuleName = ? ")
                db.AddParameter("@ModuleName", ModuleName, DataAccess.OleDbDataType.OleDbString, 70, ParameterDirection.Input)
            End If
            If showActiveOnly = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                '.Append("ORDER BY SHPN.Description ")
            ElseIf showActiveOnly = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                '.Append("ORDER BY SHPN.Description ")
            Else
                '.Append("ORDER BY ST.Status,SHPN.Description asc")
                .Append("ORDER BY ST.Status ")
            End If
        End With

        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function
    Public Function GetHomePageNoteInfo(ByVal HomePageNoteID As String) As HomePageNoteInfo

        '   connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            '   with subqueries
            .Append("SELECT SHPN.HomePageNoteID, ")
            .Append("    SHPN.ModuleID, ")
            .Append("    SHPN.StatusId, ")
            .Append("    SHPN.CampGrpId, ")
            .Append("    (Select Status from syStatuses where StatusId=SHPN.StatusId) As Status, ")
            .Append("    SHPN.Title, ")
            .Append("    SHPN.Description, ")
            .Append("    SHPN.ModUser, ")
            .Append("    SHPN.ModDate ")
            .Append("FROM  syHomePageNotes SHPN ")
            .Append("WHERE SHPN.HomePageNoteID= ? ")
        End With

        ' Add the HomePageNoteId to the parameter list
        db.AddParameter("@HomePageNoteID", HomePageNoteID, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim HomePageNoteInfo As New HomePageNoteInfo

        While dr.Read()

            '   set properties with data from DataReader
            With HomePageNoteInfo
                .IsInDB = True
                .HomePageNoteID = HomePageNoteID
                .ModuleID = dr("ModuleID")
                .StatusId = CType(dr("StatusId"), Guid).ToString
                .CampGrpId = CType(dr("CampGrpId"), Guid).ToString
                .Status = dr("Status")
                .Title = dr("Title")
                .Description = dr("Description")
                .ModUser = dr("ModUser")
                .ModDate = dr("ModDate")
            End With

        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        '   Return HomePageNoteInfo
        Return HomePageNoteInfo

    End Function
    Public Function UpdateHomePageNoteInfo(ByVal HomePageNoteInfo As HomePageNoteInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an update
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("UPDATE syHomePageNotes Set HomePageNoteID = ?, ModuleID = ?, ")
                .Append(" StatusId = ?, CampGrpId = ?, Title = ?, Description = ?, ")
                .Append(" ModUser = ?, ModDate = ? ")
                .Append("WHERE HomePageNoteID = ? ")
                .Append("AND ModDate = ? ;")
                .Append("Select count(*) from syHomePageNotes where ModDate = ? ")
            End With

            '   add parameters values to the query

            '   HomePageNoteID
            db.AddParameter("@HomePageNoteID", HomePageNoteInfo.HomePageNoteID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModuleID
            db.AddParameter("@ModuleID", HomePageNoteInfo.ModuleID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", HomePageNoteInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If HomePageNoteInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", HomePageNoteInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Title
            db.AddParameter("@Title", HomePageNoteInfo.Title, DataAccess.OleDbDataType.OleDbString, 150, ParameterDirection.Input)

            '   Description
            db.AddParameter("@Description", HomePageNoteInfo.Description, DataAccess.OleDbDataType.OleDbString, 2000, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            Dim now As Date = Date.Now
            db.AddParameter("@ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   HomePageNoteId
            db.AddParameter("@AdmDepositId", HomePageNoteInfo.HomePageNoteID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Original_ModDate", HomePageNoteInfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@Updated_ModDate", now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If there were no updated rows then there was a concurrency problem
            If rowCount = 1 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function AddHomePageNoteInfo(ByVal HomePageNoteInfo As HomePageNoteInfo, ByVal user As String) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("INSERT syHomePageNotes (HomePageNoteID, ModuleID, StatusId, ")
                .Append("   CampGrpId, Title, Description, ModUser, ModDate) ")
                .Append("VALUES (?,?,?,?,?,?,?,?) ")
            End With

            '   add parameters values to the query

            '   HomePageNoteID
            db.AddParameter("@HomePageNoteID", HomePageNoteInfo.HomePageNoteID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModuleID
            db.AddParameter("@ModuleID", HomePageNoteInfo.ModuleID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   StatusId
            db.AddParameter("@StatusId", HomePageNoteInfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   CampGrpId
            If HomePageNoteInfo.CampGrpId = Guid.Empty.ToString Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", HomePageNoteInfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            '   Title
            db.AddParameter("@Title", HomePageNoteInfo.Title, DataAccess.OleDbDataType.OleDbString, 150, ParameterDirection.Input)

            '   Description
            db.AddParameter("@Description", HomePageNoteInfo.Description, DataAccess.OleDbDataType.OleDbString, 2000, ParameterDirection.Input)

            '   ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute the query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return without errors
            Return ""
        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function DeleteHomePageNoteInfo(ByVal HomePageNoteId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        '   do a delete
        Try
            '   build the query
            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM syHomePageNotes ")
                .Append("WHERE HomePageNoteID = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM syHomePageNotes WHERE HomePageNoteID = ? ")
            End With

            '   add parameters values to the query

            '   HomePageNoteID
            db.AddParameter("@HomePageNoteID", HomePageNoteId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   ModDate
            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   HomePageNoteId
            db.AddParameter("@HomePageNoteId", HomePageNoteId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            '   If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function
End Class