Imports FAME.Advantage.Common

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' EmployerFeeDB.vb
'
' EmployerFeeDB Data Access Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class EmployerJobsDB
    Public Function GetAllEmployerJobs() As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("select JC.JobCatId,JC.JobCatDescrip ")
            .Append("FROM     PLJobCats JC,syStatuses ST ")
            .Append("WHERE    JC.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append(" Order By JC.JobCatDescrip")
        End With
        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllEmployerJobsByCategory(ByVal JobCatId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("select JC.TitleId,JC.TitleDescrip ")
            .Append("FROM     adTitles JC,syStatuses ST ")
            .Append("WHERE    JC.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' and JC.JobCatId=? ")
            .Append(" Order By JC.TitleDescrip")
        End With
        db.AddParameter("@JobCatId", JobCatId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        '   return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllEmployerGroup() As DataSet

        '   connect to the database
        Dim db As New DataAccess

      

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("select Emp.EmployerId,Emp.EmployerDescrip ")
            .Append("from PlEmployers Emp,SyStatuses ST ")
            .Append("WHERE    Emp.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' and Emp.GroupName=1")
            .Append(" Order By Emp.EmployerDescrip")
        End With
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllJobTitles() As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("select JT.TitleId,JT.TitleDescrip ")
            .Append("FROM   adTitles JT,syStatuses ST ")
            .Append("WHERE  JT.StatusId = ST.StatusId ")
            .Append(" AND   ST.Status = 'Active' ")
            .Append("ORDER BY JT.TitleDescrip ")
        End With

        '   return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllJobGroups() As DataSet
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("select JT.JobGroupId,JT.JobGroupDescrip ")
            .Append("FROM     plJobGroup JT,syStatuses ST ")
            .Append("WHERE    JT.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY JT.JobGroupDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllJobType() As DataSet

        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("select JT.JobGroupId,JT.JobGroupDescrip ")
            .Append("FROM     plJobType JT,syStatuses ST ")
            .Append("WHERE    JT.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY JT.JobGroupDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllJobBenefits() As DataSet

        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("select JT.JobBenefitId,JT.JobBenefitDescrip ")
            .Append("FROM     plJobBenefit JT,syStatuses ST ")
            .Append("WHERE    JT.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY JT.JobBenefitDescrip Desc")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllJobSchedule() As DataSet

        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("select JT.JobScheduleId,JT.JobScheduleDescrip ")
            .Append("FROM     plJobSchedule JT,syStatuses ST ")
            .Append("WHERE    JT.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY JT.JobScheduleDescrip ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function
    Public Function GetAllJobWorkDays() As DataSet

        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("select JW.WorkDaysId,JW.WorkDaysDescrip ")
            .Append("FROM     plWorkDays JW ")
            .Append("ORDER BY JW.WorkDaysId ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)

    End Function

    Public Function GetAllJobContacts() As DataSet

        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("select JT.FirstName + space(2) +  JT.MiddleName + space(2) + JT.LastName as EmployerName,JT.EmployerContactId ")
            .Append("FROM     plEmployerContact JT,syStatuses ST ")
            .Append("WHERE    JT.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' ")
            .Append("ORDER BY JT.FirstName ")
        End With

        'return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllJobContactsByEmployer(ByVal EmployerId As String) As DataSet


        'connect to the database
        Dim db As New DataAccess
        Dim da6 As New OleDbDataAdapter
        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        'build the sql query
        db.OpenConnection()

        'Dim dsGetCmpGrps As New DataSet

        'Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        'Dim strInActiveGUID As String = AdvantageCommonValues.InactiveGuid
        'dsGetCmpGrps = (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusid)
        'Dim strCampGrpId As String
        'If dsGetCmpGrps.Tables(0).Rows.Count >= 1 Then
        '    For Each row As DataRow In dsGetCmpGrps.Tables(0).Rows
        '        strCampGrpId &= row("CampGrpId").ToString & "','"
        '    Next
        '    strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
        'End If

        With sb
            .Append("select JT.FirstName,JT.MiddleName,JT.LastName,JT.EmployerContactId ")
            .Append("FROM     plEmployerContact JT,syStatuses ST ")
            .Append("WHERE    JT.StatusId = ST.StatusId ")
            .Append(" AND     ST.Status = 'Active' and JT.EmployerId = ? ")
            .Append("ORDER BY JT.FirstName ")
        End With
        db.AddParameter("@EmployerID", EmployerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        'return dataset
        da6 = db.RunParamSQLDataAdapter(sb.ToString)
        Try
            da6.Fill(ds, "InstructorDT")
        Catch ex As System.Exception
            Throw New System.Exception(ex.InnerException.Message)
        End Try
        db.CloseConnection()
        Return ds
    End Function
    Public Function UpdateEmployerInfo(ByVal employerinfo As EmployerJobsInfo, ByVal User As String, ByVal JobPostedDate As String) As String

        'Connect To The Database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        'Do an Update
        Try
            'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Update plEmployerJobs set Code=?,StatusId=?,JobTitleId=?,")
                .Append(" JobDescription=?,JobGroupId=?,")
                .Append(" ContactId=?,TypeId=?,AreaId=?,")
                .Append(" CampGrpId=?,WorkDays=?,FeeId=?,")
                .Append(" NumberOpen=?,NumberFilled=?,OpenedFrom=?,OpenedTo=?,")
                .Append(" Start=?,SalaryFrom=?,SalaryTo=?,")
                .Append(" BenefitsId=?,ScheduleId=?,HoursFrom=?,HoursTo=?,Notes=?,ModUser=?,ModDate=?,EmployerId=?, ")
                .Append(" JobRequirements=?,SalaryTypeID=?,EmployerJobTitle=?,JobPostedDate=?,ExpertiseId=? ")
                .Append(" Where EmployerJobId=? ")
            End With

            'Code
            If employerinfo.Code = "" Then
                db.AddParameter("@code", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@code", employerinfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'StatusId
            Dim strInActiveStatus As String = "1AF592A6-8790-48EC-9916-5412C25EF49F"
            Dim strNumberOpen As String = employerinfo.NumberOpen
            Dim strNumberFilled As String = employerinfo.NumberFilled
            If strNumberOpen = "" Then
                strNumberOpen = 0
            End If
            If strNumberFilled = "" Then
                strNumberFilled = 0
            End If

            If Not strNumberOpen = "" And (Trim(strNumberOpen) = Trim(strNumberFilled)) And Trim(strNumberOpen) >= 1 Then
                db.AddParameter("@StatusId", strInActiveStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                If employerinfo.StatusId = Guid.Empty.ToString Or employerinfo.StatusId = "" Then
                    db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@StatusId", employerinfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
            End If

            'JobTitleId
            If employerinfo.JobTitleId = "" Or employerinfo.StatusId = "" Then
                db.AddParameter("@JobTitleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@JobTitleId", employerinfo.JobTitleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Job Description
            If employerinfo.JobDescription = "" Then
                db.AddParameter("@JobDescription", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@JobDescription", employerinfo.JobDescription, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If

            'JobGroupId
            If employerinfo.JobGroupId = Guid.Empty.ToString Or employerinfo.JobGroupId = "" Then
                db.AddParameter("@JobGroupId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@JobGroupId", employerinfo.JobGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ContactId
            If employerinfo.ContactId = Guid.Empty.ToString Or employerinfo.ContactId = "" Then
                db.AddParameter("@ContactId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ContactId", employerinfo.ContactId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            'TypeId
            If employerinfo.TypeId = Guid.Empty.ToString Or employerinfo.TypeId = "" Then
                db.AddParameter("@TypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TypeId", employerinfo.TypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Area Id
            If employerinfo.AreaId = Guid.Empty.ToString Or employerinfo.AreaId = "" Then
                db.AddParameter("@AreaId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@AreaId", employerinfo.AreaId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'CampGrpId
            If employerinfo.CampGrpId = Guid.Empty.ToString Or employerinfo.CampGrpId = "" Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", employerinfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'WorkDays
            db.AddParameter("@workdays", "Monday", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'FeeId
            If employerinfo.FeeId = Guid.Empty.ToString Or employerinfo.FeeId = "" Then
                db.AddParameter("@FeeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@FeeId", employerinfo.FeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Number Open
            Try
                db.AddParameter("@NumberOpen", employerinfo.NumberOpen, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Catch ex As Exception
                db.AddParameter("@NumberOpen", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End Try

            'Number Filled
            Try
                db.AddParameter("@NumberFilled", employerinfo.NumberFilled, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Catch ex As Exception
                db.AddParameter("@NumberFilled", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End Try

            'OpenedFrom
            If employerinfo.OpenedFrom = "" Then
                db.AddParameter("@OpenedFrom", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@OpenedFrom", employerinfo.OpenedFrom, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            End If

            'Opened To
            If employerinfo.OpenedTo = "" Then
                db.AddParameter("@OpenedTo", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@OpenedTo", employerinfo.OpenedTo, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            End If

            'Start
            If employerinfo.Start = "" Then
                db.AddParameter("@Start", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Start", employerinfo.Start, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            End If

            'SalaryFrom
            If employerinfo.SalaryFrom = "" Then
                db.AddParameter("@SalaryFrom", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SalaryFrom", employerinfo.SalaryFrom, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'SalaryTo
            If employerinfo.SalaryTo = "" Then
                db.AddParameter("@SalaryTo", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SalaryTo", employerinfo.SalaryTo, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'BenefitsId
            If employerinfo.BenefitsId = Guid.Empty.ToString Or employerinfo.BenefitsId = "" Then
                db.AddParameter("@BenefitsId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@BenefitsId", employerinfo.BenefitsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'scheduleId
            If employerinfo.ScheduleId = Guid.Empty.ToString Or employerinfo.ScheduleId = "" Then
                db.AddParameter("@scheduleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@scheduleId", employerinfo.ScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Hours From
            If employerinfo.HoursFrom = "" Then
                db.AddParameter("@HoursFrom", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HoursFrom", employerinfo.HoursFrom, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Hours To
            If employerinfo.HoursTo = "" Then
                db.AddParameter("@HoursTo", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HoursTo", employerinfo.HoursTo, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Notes
            If employerinfo.Notes = "" Then
                db.AddParameter("@Notes", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@Notes", employerinfo.Notes, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If


            'ModUser
            db.AddParameter("@ModUser", User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", employerinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'JOb Title Description
            'If employerinfo.JObTitleDescription = "" Then
            '    db.AddParameter("@JobTitleDescription", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'Else
            '    db.AddParameter("@JobTitleDescription", employerinfo.JObTitleDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'End If

            'EmployerId
            If employerinfo.EmployerId = Guid.Empty.ToString Or employerinfo.EmployerId = "" Then
                db.AddParameter("@EmployerId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@EmployerId", employerinfo.EmployerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Job Requirements
            If employerinfo.Requirement = "" Then
                db.AddParameter("@JobRequirements", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@JobRequirements", employerinfo.Requirement, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If


            'Salary Type
            If employerinfo.SalaryType = Guid.Empty.ToString Or employerinfo.SalaryType = "" Then
                db.AddParameter("@SalaryTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SalaryTypeId", employerinfo.SalaryType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Employer Job Title
            If employerinfo.EmployerJobTitle = "" Then
                db.AddParameter("@EmployerJobTitle", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@EmployerJobTitle", employerinfo.EmployerJobTitle, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Job Posted Date
            If employerinfo.JobPostedDate = "" Then
                db.AddParameter("@JobPostedDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@JobPostedDate", JobPostedDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            'ExpertiseLevel Id
            If employerinfo.ExpertiseLevelId = Guid.Empty.ToString Or employerinfo.ExpertiseLevelId = "" Then
                db.AddParameter("@ExpertiseLevelId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ExpertiseLevelId", employerinfo.ExpertiseLevelId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'EmployerJobId
            db.AddParameter("@EmployerJobId", employerinfo.EmployerJobId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            'Retun Without Errors
            Return ""
        Catch ex As OleDbException
            'Return an Error To Client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            '  db.CloseConnection()
        End Try

    End Function
    Public Function AddEmployerInfo(ByVal employerinfo As EmployerJobsInfo, ByVal user As String, ByVal JobPostedDate As String) As String
        'Connect To The Database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'Do an Insert
        Try
            'Build The Query
            Dim sb As New StringBuilder
            With sb
                .Append("Insert plEmployerJobs(EmployerJobId,Code,StatusId,JobTitleId,")
                .Append(" JobDescription,JobGroupId,")
                .Append(" ContactId,TypeId,AreaId,")
                .Append(" CampGrpId,WorkDays,FeeId,")
                .Append(" NumberOpen,NumberFilled,OpenedFrom,OpenedTo,")
                .Append(" Start,SalaryFrom,SalaryTo,")
                .Append(" BenefitsId,ScheduleId,HoursFrom,HoursTo,Notes,ModUser,ModDate,EmployerId,JobRequirements,SalaryTypeID,EmployerJobTitle,JobPostedDate,ExpertiseId) ")
                .Append(" Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
            End With

            'Add Parameters To Query

            'EmployerId
            db.AddParameter("@EmployerJobId", employerinfo.EmployerJobId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'Code
            If employerinfo.Code = "" Then
                db.AddParameter("@code", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@code", employerinfo.Code, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'StatusId
            Dim strInActiveStatus As String = "1AF592A6-8790-48EC-9916-5412C25EF49F"

            If Not employerinfo.NumberOpen = 0 And (Trim(employerinfo.NumberOpen) = Trim(employerinfo.NumberFilled)) And Trim(employerinfo.NumberOpen) >= 1 Then
                db.AddParameter("@StatusId", strInActiveStatus, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                If employerinfo.StatusId = Guid.Empty.ToString Or employerinfo.StatusId = "" Then
                    db.AddParameter("@StatusId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Else
                    db.AddParameter("@StatusId", employerinfo.StatusId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                End If
            End If

            'JobTitleId
            If employerinfo.JobTitleId = "" Or employerinfo.StatusId = "" Then
                db.AddParameter("@JobTitleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@JobTitleId", employerinfo.JobTitleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Job Description
            If employerinfo.JobDescription = "" Then
                db.AddParameter("@JobDescription", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@JobDescription", employerinfo.JobDescription, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If

            'JobGroupId
            If employerinfo.JobGroupId = Guid.Empty.ToString Or employerinfo.JobGroupId = "" Then
                db.AddParameter("@JobGroupId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@JobGroupId", employerinfo.JobGroupId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'ContactId
            If employerinfo.ContactId = Guid.Empty.ToString Or employerinfo.ContactId = "" Then
                db.AddParameter("@ContactId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ContactId", employerinfo.ContactId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If
            'TypeId
            If employerinfo.TypeId = Guid.Empty.ToString Or employerinfo.TypeId = "" Then
                db.AddParameter("@TypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@TypeId", employerinfo.TypeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Area Id
            If employerinfo.AreaId = Guid.Empty.ToString Or employerinfo.AreaId = "" Then
                db.AddParameter("@AreaId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@AreaId", employerinfo.AreaId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'CampGrpId
            If employerinfo.CampGrpId = Guid.Empty.ToString Or employerinfo.CampGrpId = "" Then
                db.AddParameter("@CampGrpId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@CampGrpId", employerinfo.CampGrpId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'WorkDays
            db.AddParameter("@workdays", "Monday", DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'FeeId
            If employerinfo.FeeId = Guid.Empty.ToString Or employerinfo.FeeId = "" Then
                db.AddParameter("@FeeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@FeeId", employerinfo.FeeId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Number Open
            Try
                db.AddParameter("@NumberOpen", employerinfo.NumberOpen, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Catch ex As Exception
                db.AddParameter("@NumberOpen", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End Try

            'Number Filled
            Try
                db.AddParameter("@NumberFilled", employerinfo.NumberFilled, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            Catch ex As Exception
                db.AddParameter("@NumberFilled", System.DBNull.Value, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
            End Try

            'OpenedFrom
            If employerinfo.OpenedFrom = "" Then
                db.AddParameter("@OpenedFrom", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@OpenedFrom", employerinfo.OpenedFrom, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            End If

            'Opened To
            If employerinfo.OpenedTo = "" Then
                db.AddParameter("@OpenedTo", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@OpenedTo", employerinfo.OpenedTo, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            End If

            'Start
            If employerinfo.Start = "" Then
                db.AddParameter("@Start", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@Start", employerinfo.Start, DataAccess.OleDbDataType.OleDbDateTime, 50, ParameterDirection.Input)
            End If

            'SalaryFrom
            If employerinfo.SalaryFrom = "" Then
                db.AddParameter("@SalaryFrom", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SalaryFrom", employerinfo.SalaryFrom, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'SalaryTo
            If employerinfo.SalaryTo = "" Then
                db.AddParameter("@SalaryTo", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SalaryTo", employerinfo.SalaryTo, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'BenefitsId
            If employerinfo.BenefitsId = Guid.Empty.ToString Or employerinfo.BenefitsId = "" Then
                db.AddParameter("@BenefitsId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@BenefitsId", employerinfo.BenefitsId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'scheduleId
            If employerinfo.ScheduleId = Guid.Empty.ToString Or employerinfo.ScheduleId = "" Then
                db.AddParameter("@scheduleId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@scheduleId", employerinfo.ScheduleId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Hours From
            If employerinfo.HoursFrom = "" Then
                db.AddParameter("@HoursFrom", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HoursFrom", employerinfo.HoursFrom, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Hours To
            If employerinfo.HoursTo = "" Then
                db.AddParameter("@HoursTo", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@HoursTo", employerinfo.HoursTo, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Notes
            If employerinfo.Notes = "" Then
                db.AddParameter("@Notes", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@Notes", employerinfo.Notes, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If


            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", employerinfo.ModDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            'JOb Title Description
            'If employerinfo.JObTitleDescription = "" Then
            '    db.AddParameter("@JobTitleDescription", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'Else
            '    db.AddParameter("@JobTitleDescription", employerinfo.JObTitleDescription, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            'End If


            'EmployerId
            If employerinfo.EmployerId = Guid.Empty.ToString Or employerinfo.EmployerId = "" Then
                db.AddParameter("@EmployerId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@EmployerId", employerinfo.EmployerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Job Requirements
            If employerinfo.Requirement = "" Then
                db.AddParameter("@JobRequirements", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            Else
                db.AddParameter("@JobRequirements", employerinfo.Requirement, DataAccess.OleDbDataType.OleDbString, 300, ParameterDirection.Input)
            End If


            'Salary Type
            If employerinfo.SalaryType = Guid.Empty.ToString Or employerinfo.SalaryType = "" Then
                db.AddParameter("@SalaryTypeId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@SalaryTypeId", employerinfo.SalaryType, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Employer Job Title
            If employerinfo.EmployerJobTitle = "" Then
                db.AddParameter("@EmployerJobTitle", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@EmployerJobTitle", employerinfo.EmployerJobTitle, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If

            'Job Posted Date
            If employerinfo.JobPostedDate = "" Then
                db.AddParameter("@JobPostedDate", System.DBNull.Value, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            Else
                db.AddParameter("@JobPostedDate", JobPostedDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            End If

            'ExpertiseLevel Id
            If employerinfo.ExpertiseLevelId = Guid.Empty.ToString Or employerinfo.ExpertiseLevelId = "" Then
                db.AddParameter("@ExpertiseLevelId", System.DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            Else
                db.AddParameter("@ExpertiseLevelId", employerinfo.ExpertiseLevelId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            'Execute The Query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            employerinfo.IsInDB = True

            'Retun Without Errors
            Return ""

        Catch ex As OleDbException
            'Return an Error To Client
            'Return an Error To Client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            ' db.CloseConnection()
        End Try
    End Function
    Public Function GetEmployerJobsInfo(ByVal EmployerId As String) As EmployerJobsInfo
        'connect to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT     B.StatusId, ")
            .Append("    (Select Status from syStatuses where StatusId=B.StatusId) as Status, ")
            .Append("    B.Code, ")
            .Append("    B.JobTitleId, ")
            .Append("    (Select TitleDescrip from adTitles where TitleId=B.JobTitleId) As JobTitle, ")
            .Append("    B.JobDescription, ")
            .Append("    B.JobGroupId, ")
            .Append("    (Select JobCatDescrip from plJobCats where JobCatId=B.JobGroupId) As JobGroup, ")
            .Append(" B.ContactId, ")
            .Append("    (Select LastName from plEmployerContact where EmployerContactId=B.ContactId) As Contact, ")
            .Append(" B.TypeId, ")
            .Append("    (Select JobGroupDescrip from plJobType where JobGroupId=B.JobGroupId) As Type, ")
            .Append(" B.AreaId, ")
            .Append("    (Select CountyDescrip from adCounties where CountyId=B.AreaId) As Area, ")
            .Append(" B.WorkDays, ")
            .Append(" B.NumberOpen, ")
            .Append(" B.NumberFilled, ")
            .Append(" B.OpenedFrom, ")
            .Append(" B.OpenedTo, ")
            .Append(" B.SalaryFrom, ")
            .Append(" B.SalaryTo, ")
            .Append(" B.HoursFrom, ")
            .Append(" B.HoursTo, ")
            .Append(" B.Notes, ")
            .Append("B.Start, ")
            .Append("B.FeeId, ")
            .Append("    (Select FeeDescrip from plFee where FeeId=B.FeeId) As Fee, ")
            .Append("B.BenefitsId, ")
            .Append("    (Select JobBenefitDescrip from plJobBenefit where JobBenefitId=B.BenefitsId) As Benefits, ")
            .Append("B.ScheduleId, ")
            .Append("    (Select JobScheduleDescrip from plJobSchedule where JobScheduleId=B.ScheduleId) As Schedule, ")
            .Append("B.EmployerId, ")
            .Append("    B.CampGrpId, ")
            .Append("    (Select CampGrpDescrip from syCampGrps where CampGrpId=B.CampGrpId) As CampGrpDescrip, ")
            .Append(" B.JobRequirements, ")
            .Append("    B.SalaryTypeID, ")
            .Append("    (Select SalaryTypeDescrip from plSalaryType where SalaryTypeId=B.SalaryTypeId) As SalaryType, ")
            .Append("    B.ModDate, ")
            .Append("    B.EmployerJobTitle, ")
            .Append("    B.JobPostedDate, ")
            .Append("    B.ExpertiseId, ")
            .Append("    (Select ExpertiseDescrip from adExpertiseLevel where ExpertiseId=B.ExpertiseId) As Expertise ")
            .Append("FROM  plEmployerJobs B ")
            .Append("WHERE B.EmployerJobId= ? ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@EmployerJobId", EmployerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim employerinfo As New EmployerJobsInfo

        While dr.Read()

            'set properties with data from DataReader
            With employerinfo
                .IsInDB = True
                .EmployerJobId = EmployerId
                If Not (dr("StatusId") Is System.DBNull.Value) Then .StatusId = CType(dr("StatusId"), Guid).ToString Else .StatusId = Guid.Empty.ToString
                If Not (dr("FeeId") Is System.DBNull.Value) Then .FeeId = CType(dr("FeeId"), Guid).ToString Else .FeeId = Guid.Empty.ToString
                If Not (dr("Fee") Is System.DBNull.Value) Then .Fee = dr("Fee")
                If Not (dr("JobTitleId") Is System.DBNull.Value) Then .JobTitleId = CType(dr("JobTitleId"), Guid).ToString
                If Not (dr("JobTitle") Is System.DBNull.Value) Then .JobTitle = dr("JobTitle")
                If Not (dr("JobGroupId") Is System.DBNull.Value) Then .JobGroupId = CType(dr("JobGroupId"), Guid).ToString
                If Not (dr("JobGroup") Is System.DBNull.Value) Then .JobGroup = dr("JobGroup")
                If Not (dr("ContactId") Is System.DBNull.Value) Then .ContactId = CType(dr("ContactId"), Guid).ToString Else .ContactId = Guid.Empty.ToString
                If Not (dr("Contact") Is System.DBNull.Value) Then .Contact = dr("Contact")
                If Not (dr("TypeId") Is System.DBNull.Value) Then .TypeId = CType(dr("TypeId"), Guid).ToString Else .TypeId = Guid.Empty.ToString
                If Not (dr("Type") Is System.DBNull.Value) Then .Type = dr("Type")
                If Not (dr("AreaId") Is System.DBNull.Value) Then .AreaId = CType(dr("AreaId"), Guid).ToString Else .AreaId = Guid.Empty.ToString
                If Not (dr("Area") Is System.DBNull.Value) Then .Area = dr("Area")
                If Not (dr("CampGrpId") Is System.DBNull.Value) Then .CampGrpId = CType(dr("CampGrpId"), Guid).ToString Else .CampGrpId = Guid.Empty.ToString
                If Not (dr("CampGrpDescrip") Is System.DBNull.Value) Then .CampGrpDescrip = dr("CampGrpDescrip")
                If Not (dr("BenefitsId") Is System.DBNull.Value) Then .BenefitsId = CType(dr("BenefitsId"), Guid).ToString Else .BenefitsId = Guid.Empty.ToString
                If Not (dr("Benefits") Is System.DBNull.Value) Then .Benefits = dr("Benefits")
                If Not (dr("ScheduleId") Is System.DBNull.Value) Then .ScheduleId = CType(dr("ScheduleId"), Guid).ToString Else .ScheduleId = Guid.Empty.ToString
                If Not (dr("Schedule") Is System.DBNull.Value) Then .Schedule = dr("Schedule")
                If Not (dr("EmployerId") Is System.DBNull.Value) Then .EmployerId = CType(dr("EmployerId"), Guid).ToString Else .EmployerId = Guid.Empty.ToString

                'Get Code
                If Not (dr("Code") Is System.DBNull.Value) Then .Code = dr("Code") Else .Code = ""
                If Not (dr("JobDescription") Is System.DBNull.Value) Then .JobDescription = dr("JobDescription") Else .JobDescription = ""
                If Not (dr("NumberOpen") Is System.DBNull.Value) Then .NumberOpen = CType(dr("NumberOpen"), Integer) Else .NumberOpen = 0
                If Not (dr("NumberFilled") Is System.DBNull.Value) Then .NumberFilled = CType(dr("NumberFilled"), Integer) Else .NumberFilled = 0
                If Not (dr("OpenedFrom") Is System.DBNull.Value) Then .OpenedFrom = dr("OpenedFrom") Else .OpenedFrom = ""
                If Not (dr("OpenedTo") Is System.DBNull.Value) Then .OpenedTo = dr("OpenedTo") Else .OpenedTo = ""
                If Not (dr("Start") Is System.DBNull.Value) Then .Start = dr("Start") Else .Start = ""
                If Not (dr("SalaryFrom") Is System.DBNull.Value) Then .SalaryFrom = dr("SalaryFrom") Else .SalaryFrom = ""
                If Not (dr("SalaryTo") Is System.DBNull.Value) Then .SalaryTo = dr("SalaryTo") Else .SalaryTo = ""
                If Not (dr("HoursFrom") Is System.DBNull.Value) Then .HoursFrom = dr("HoursFrom") Else .HoursFrom = ""
                If Not (dr("HoursTo") Is System.DBNull.Value) Then .HoursTo = dr("HoursTo") Else .HoursTo = ""
                If Not (dr("Notes") Is System.DBNull.Value) Then .Notes = dr("Notes") Else .Notes = ""
                If Not (dr("JobRequirements") Is System.DBNull.Value) Then .Requirement = dr("JobRequirements") Else .Requirement = ""
                If Not (dr("SalaryTypeID") Is System.DBNull.Value) Then .SalaryTypeId = CType(dr("SalaryTypeID"), Guid).ToString
                If Not (dr("SalaryType") Is System.DBNull.Value) Then .SalaryType = dr("SalaryType")
                If Not (dr("EmployerJobTitle") Is System.DBNull.Value) Then .EmployerJobTitle = dr("EmployerJobTitle") Else .EmployerJobTitle = ""
                'If Not (dr("JobPostedDate") Is System.DBNull.Value) Then .JobPostedDate = dr("JobPostedDate") Else .JobPostedDate = Date.MinValue
                If Not (dr("JobPostedDate") Is System.DBNull.Value) Then .JobPostedDate = dr("JobPostedDate") Else .JobPostedDate = ""
                If Not (dr("ExpertiseId") Is System.DBNull.Value) Then .ExpertiseLevelId = CType(dr("ExpertiseId"), Guid).ToString
                If Not (dr("Expertise") Is System.DBNull.Value) Then .ExpertiseLevel = dr("Expertise")
                .ModDate = dr("ModDate")
            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return employerinfo
    End Function
    Public Function GetPlacementHistoryInfo(ByVal PlacementId As String) As PlacementHistoryInfo
        'connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        'build the sql query
        Dim sb As New StringBuilder
        With sb
            'With subqueries
            .Append("SELECT  B.Supervisor,A.firstname,A.LastName,A.MiddleName, ")
            .Append("    (Select JobStatusDescrip from plJobStatus where JobStatusId=B.JobStatusId) as JobStatus, ")
            '.Append("    (Select JobGroupDescrip from plJobType where JobGroupId=B.JobTypeId) as JobType, ")
            .Append("    (Select EmployerJobTitle from plEmployerJobs t2 where t2.EmployerJobId=B.EmployerJobId) as JobTitle, ")
            .Append("    (Select Code from plEmployerJobs  where EmployerJobId=B.EmployerJobId) as JobCode, ")
            .Append("    (Select JobScheduleDescrip from plJobSchedule where JobScheduleId=B.ScheduleId) as JobSchedule, ")
            .Append("    (Select JobBenefitDescrip from plJobBenefit where JobBenefitId=B.BenefitsId) as JobBenefit, ")
            .Append("     (Select InterviewDescrip from plInterview where InterviewId=B.InterviewId) as InterviewDescrip, ")
            .Append("    (Select EmployerDescrip from plEmployers t1,plEmployerJobs t2 where t1.EmployerId=t2.EmployerId and t2.EmployerJobId=B.EmployerJobId) as Employer, ")
            .Append("    (Select SSN from arStudent where StudentId=A.StudentId) as SSN, ")
            .Append("    (Select FeeDescrip from plFee where FeeId=B.Fee) as Fee, ")
            .Append("  (Select HowPlacedDescrip from plHowPlaced where HowPlacedId=B.HowPlacedId) as HowPlaced, ")
            .Append("    B.EmployerJobId, ")
            .Append("    B.JobDescrip, ")
            .Append("    B.TerminationReason, ")
            .Append("    B.TerminationDate, ")
            .Append("    B.WorkDaysId, ")
            .Append("    B.Salary, ")
            .Append("    B.Reason, ")
            .Append("    B.StartDate, ")
            .Append("    B.BenefitsId, ")
            .Append("    B.ScheduleId, ")
            .Append("    B.HowPlacedId, ")
            .Append("    (select Distinct fullname from syUsers where UserId=B.PlacementRep) as PlacementRep, ")
            .Append("    (select  SalaryTypeDescrip from plSalaryType where SalaryTypeId=B.SalaryTypeId) as SalaryType, ")
            .Append("    B.PlacedDate,(select Distinct FldStudyDescrip from plFldStudy where FldStudyId=B.FldStudyId) as FldStudy ")
            .Append(" ,D.PrgVerDescrip ")
            .Append(" FROM  arStudent A,plStudentsPlaced B,arStuEnrollments C,arPrgVersions D ")
            .Append(" WHERE B.PlacementId= ? ")
            .Append(" and B.StuEnrollId = C.StuEnrollId and A.StudentId = C.StudentId and C.PrgVerId = D.PrgVerId ")
        End With

        'Add the EmployerContactId the parameter list
        db.AddParameter("@PlacementId", PlacementId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        Dim placementhistoryinfo As New PlacementHistoryInfo

        While dr.Read()

            'set properties with data from DataReader
            With placementhistoryinfo
                .IsInDB = True
                .PlacementId = PlacementId

                'Get Job Status
                If Not (dr("JobStatus") Is System.DBNull.Value) Then .JobStatus = CType(dr("JobStatus"), String).ToString Else .JobStatus = ""

                'Get Job Title
                If Not (dr("EmployerJobId") Is System.DBNull.Value) Then .JobTitleId = CType(dr("JobTitle"), String).ToString Else .JobTitleId = ""


                'Get JobType
                'If Not (dr("JobTypeId") Is System.DBNull.Value) Then .JobTypeId = CType(dr("JobTypeId"), String).ToString Else .JobTypeId = ""

                'Fee
                If Not (dr("Fee") Is System.DBNull.Value) Then .Fee = dr("Fee") Else .Fee = ""


                'Get Job Benefit
                If Not (dr("JobBenefit") Is System.DBNull.Value) Then .BenefitsId = CType(dr("JobBenefit"), String).ToString Else .BenefitsId = ""

                'Get JobSchedule
                If Not (dr("JobSchedule") Is System.DBNull.Value) Then .ScheduleId = CType(dr("JobSchedule"), String).ToString Else .ScheduleId = ""


                'Get Supervisor
                If Not (dr("Supervisor") Is System.DBNull.Value) Then .Supervisor = CType(dr("Supervisor"), String).ToString Else .Supervisor = ""


                'StartDate
                If Not (dr("StartDate") Is System.DBNull.Value) Then .StartDate = dr("StartDate") Else .StartDate = ""

                'JobCode 
                If Not (dr("JobCode") Is System.DBNull.Value) Then .JobTypeId = CType(dr("JobCode"), String).ToString Else .JobTypeId = ""

                'Salary
                If Not (dr("Salary") Is System.DBNull.Value) Then .Salary = dr("Salary") Else .Salary = ""


                'Termination Reason
                If Not (dr("TerminationReason") Is System.DBNull.Value) Then .TerminationReason = dr("TerminationReason") Else .TerminationReason = ""

                'Termination Date
                If Not (dr("TerminationDate") Is System.DBNull.Value) Then .TerminationDate = dr("TerminationDate") Else .TerminationDate = ""


                'Notes
                If Not (dr("Reason") Is System.DBNull.Value) Then .Notes = dr("Reason") Else .Notes = ""


                'Job Description
                If Not (dr("JobDescrip") Is System.DBNull.Value) Then .JobDescrip = dr("JobDescrip") Else .JobDescrip = ""

                'HowPlaced Id
                If Not (dr("HowPlaced") Is System.DBNull.Value) Then .HowPlaced = dr("HowPlaced") Else .HowPlaced = ""

                'Placement Rep
                If Not (dr("PlacementRep") Is System.DBNull.Value) Then .PlacementRep = CType(dr("PlacementRep"), String).ToString Else .PlacementRep = ""


                'Employer
                If Not (dr("Employer") Is System.DBNull.Value) Then .Employer = dr("Employer") Else .Employer = ""

                'SSN 
                If Not (dr("SSN") Is System.DBNull.Value) Then .SSN = dr("SSN") Else .SSN = ""

                'StudentName
                'If Not (dr("StudentName") Is System.DBNull.Value) Then .StudentName = dr("StudentName") Else .StudentName = ""

                'InterviewDescrip
                If Not (dr("InterviewDescrip") Is System.DBNull.Value) Then .Interview = dr("InterviewDescrip") Else .Interview = ""

                If Not (dr("FirstName") Is System.DBNull.Value) Then .FirstName = dr("FirstName") Else .FirstName = ""

                If Not (dr("MiddleName") Is System.DBNull.Value) Then .MiddleName = dr("MiddleName") Else .MiddleName = ""

                If Not (dr("LastName") Is System.DBNull.Value) Then .LastName = dr("LastName") Else .LastName = ""

                If Not (dr("SalaryType") Is System.DBNull.Value) Then .SalaryType = dr("SalaryType") Else .SalaryType = ""

                If Not (dr("PlacedDate") Is System.DBNull.Value) Then .PlacedDate = dr("PlacedDate") Else .PlacedDate = ""

                If Not (dr("PrgVerDescrip") Is System.DBNull.Value) Then .Enrollment = dr("PrgVerDescrip") Else .Enrollment = ""

                If Not (dr("FldStudy") Is System.DBNull.Value) Then .FldStudy = dr("FldStudy") Else .FldStudy = ""




            End With
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State = ConnectionState.Open Then db.CloseConnection()

        'Return BankInfo
        Return placementhistoryinfo
    End Function
    Public Function GetAllPlacementHistory() As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("Select B.StudentFirstName,B.StudentMiddleName,B.StudentLastName , ")
            .Append("    (Select TitleDescrip from adTitles where TitleId=B.JobTitleId) as JobTitle,B.PlacementId ")
            .Append(" FROM     plStudentsPlaced B where B.EmployerId=? ")
            .Append(" ORDER BY B.StudentFirstName ")
        End With

        ' return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function

    Public Function GetAllPlacementHistory1(ByVal EmployerID As String, ByVal strLastName As String, ByVal strFirstName As String, ByVal strEmployerJobId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select Distinct firstname,middlename,lastname,PlacementId,C.EmployerJobTitle ")
            .Append(" from ")
            .Append(" arStudent A,plStudentsPlaced B,plEmployerJobs C,arStuEnrollments D ")
            .Append(" where A.StudentId = D.StudentId and B.StuEnrollId = D.StuEnrollId and B.EmployerJobId = C.EmployerJobId and C.EmployerId = ? ")
            If Not strLastName = "" Then
                .Append(" and A.LastName like  + ? + '%'")
            End If
            If Not strFirstName = "" Then
                .Append(" and A.FirstName like  + ? + '%'")
            End If
            If Not strEmployerJobId = "" Then
                .Append(" and B.EmployerJobId = ?")
            End If
        End With
        db.AddParameter("@EmployerID", EmployerID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        If Not strLastName = "" Then
            db.AddParameter("@LastName", strLastName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If Not strFirstName = "" Then
            db.AddParameter("@FirstName", strFirstName, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        If Not strEmployerJobId = "" Then
            db.AddParameter("@EmployerJobId", strEmployerJobId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If

        ' return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function

    Public Function GetAllEmployerJobsDesc(ByVal showActiveOnly As Boolean) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   BC.JobDescription,BC.EmployerJobId,T.TitleDescrip AS JobTitleDescription,EmployerJobTitle ")
            .Append("FROM     plEmployerJobs BC, syStatuses ST, adTitles T ")
            .Append("WHERE    BC.StatusId = ST.StatusId AND BC.JobTitleId=T.TitleId")


            '   Conditionally include only Active Items 
            If showActiveOnly Then
                .Append(" AND     ST.Status = 'Active' ")
            Else
                .Append(" AND ST.Status = 'Inactive' ")
            End If
            .Append("ORDER BY BC.JobDescription ")
        End With

        ' return dataset
        Return db.RunSQLDataSet(sb.ToString)
    End Function
    Public Function GetAllEmployerJobsDescByEmployer(ByVal showActiveOnly As String, ByVal EmployerId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append("SELECT   BC.JobDescription,BC.EmployerJobId,T.TitleDescrip AS JobTitleDescription,ST.Status,ST.StatusId,BC.EmployerJobTitle ")
            .Append("FROM     plEmployerJobs BC, syStatuses ST, adTitles T ")
            .Append("WHERE    BC.StatusId = ST.StatusId AND BC.EmployerId = ? AND BC.JobTitleId=T.TitleId ")
            '   Conditionally include only Active Items 
            If showActiveOnly = "True" Then
                .Append("AND    ST.Status = 'Active' ")
                .Append("ORDER BY BC.JobDescription ")
            ElseIf showActiveOnly = "False" Then
                .Append("AND    ST.Status = 'Inactive' ")
                .Append("ORDER BY BC.JobDescription ")
            Else
                .Append("ORDER BY ST.Status,BC.JobDescription asc")
            End If
        End With
        db.AddParameter("@EmployerId", EmployerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        ' return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function UpdateEmployerJobWorkDays(ByVal empId As String, ByVal user As String, ByVal selectedDegrees() As String)
        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   First we have to delete all existing selections
        '   build the query
        Dim sb As New StringBuilder
        With sb
            .Append("DELETE FROM plJobWorkDays WHERE EmployerJobId = ?")
        End With

        '   delete all selected items
        db.AddParameter("@EmployerJobId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        '   Insert one record per each Item in the Selected Group
        Dim i As Integer
        For i = 0 To selectedDegrees.Length - 1
            '   build query
            With sb
                .Append("INSERT INTO plJobWorkDays (EmployerJobId, WorkDayId, ModDate, ModUser) ")
                .Append("VALUES(?,?,?,?)")
            End With

            '   add parameters
            db.AddParameter("@EmployerJobId", empId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@WorkDayId", DirectCast(selectedDegrees.GetValue(i), String), DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
        Next

        'Close Connection
        db.CloseConnection()

    End Function
    Public Function GetWorkdays(ByVal empId As String) As DataSet

        '   get the connection to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        '   build the sql query
        With sb
            .Append("SELECT EmployerJobId, WorkDayId ")
            .Append("FROM   plJobWorkDays ")
            .Append("WHERE  EmployerJobId = ? ")
            .Append("and  WorkDayId is Not Null")
        End With

        ' Add the parameter
        db.AddParameter("@EmployerJobId", empId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Return the dataset
        Return db.RunParamSQLDataSet(sb.ToString)

    End Function

    Public Function GetValidWorkDays(ByVal empId As String) As Integer
        '   get the connection to the database
        Dim db As New DataAccess

        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder

        'build the sql query
        With sb
            .Append("SELECT Count(*) ")
            .Append("FROM   plJobWorkDays ")
            .Append("WHERE  EmployerJobId = ? ")
            .Append("and  WorkDayId is Not Null")
        End With

        ' Add the parameter
        db.AddParameter("@EmployerJobId", empId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        Dim JobCount As Integer

        '   Return the dataset
        JobCount = db.RunParamSQLScalar(sb.ToString)
        Return JobCount
    End Function
    Public Function DeleteEmployerJobs(ByVal EmployerId As String, ByVal modDate As DateTime) As String

        '   Connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   do an insert
        Try
            '   build the query

            Dim sb4 As New StringBuilder
            With sb4
                .Append(" DELETE FROM plJobWorkDays ")
                .Append(" WHERE EmployerJobId = ? ")
            End With
            db.AddParameter("@EmployerJobId", EmployerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb4.ToString)
            db.ClearParameters()
            sb4.Remove(0, sb4.Length)

            Dim sb As New StringBuilder
            With sb
                .Append("DELETE FROM plEmployerJobs ")
                .Append("WHERE EmployerJobId = ? ")
                .Append(" AND ModDate = ? ;")
                .Append("SELECT count(*) FROM plEmployerJobs WHERE EmployerJobId = ? ")
            End With

            '   add parameters values to the query
            db.AddParameter("@EmployerJobId", EmployerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            db.AddParameter("@ModDate", modDate, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            db.AddParameter("@EmployerJobId", EmployerId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)


            'execute the query
            Dim rowCount As Integer = db.RunParamSQLScalar(sb.ToString)

            'If the row was not deleted then there was a concurrency problem
            If rowCount = 0 Then
                '   return without errors
                Return ""
            Else
                Return DALExceptions.BuildConcurrencyExceptionMessage()
            End If

        Catch ex As OleDbException
            '   return an error to the client
            Return DALExceptions.BuildErrorMessage(ex)
        Finally
            'Close Connection
            db.CloseConnection()
        End Try

    End Function
    Public Function GetEmpPlacementHistory(ByVal EmployerId As String) As DataSet

        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select firstname,middlename,lastname ")
            .Append(" from arStudent, plStudentsPlaced ")
            .Append(" where arStudent.StudentId = plStudentsPlaced.Studentid ")
            .Append(" and plstudentsPlaced.EmployerId= ? ")
            .Append(" order by firstname ")
        End With

        db.AddParameter("@EmployerId", EmployerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Public Function GetJobTitlesByEmployer(ByVal EmployerId As String) As DataSet
        '   connect to the database
        Dim db As New DataAccess


        db.ConnectionString = GetAdvAppSettings.AppSettings("ConString")

        '   build the sql query
        Dim sb As New StringBuilder
        With sb
            .Append(" select Distinct t1.EmployerJobTitle,t1.EmployerJobId from ")
            .Append(" plEmployerJobs t1,plStudentsPlaced t2 ")
            .Append(" where t1.EmployerJobId = t2.EmployerJobId and ")
            .Append(" t1.EmployerId= ? ")
        End With
        db.AddParameter("@EmployerId", EmployerId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' return dataset
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
#Region "Get AdvAppsetting for Manage Config entry"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
End Class

