Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports System.Text
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class OverridePreReqDB

    Public Function GetAvailSelCoursesForEnrollment(ByVal StuEnrollId As String) As DataSet
        Dim db As New DataAccess
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            '   connect to the database
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            '   build the sql query
            Dim sb As New StringBuilder
            With sb
                .Append("SELECT ")
                .Append("           C.ReqId,(SELECT Descrip FROM arReqs R WHERE R.ReqId=C.ReqId) AS Descrip, ")
                .Append("           (SELECT Code FROM arReqs R WHERE R.ReqId=C.ReqId) AS Code ")
                .Append("FROM       arProgVerDef C,arReqs R ")
                .Append("WHERE     C.ReqId=R.Reqid and   R.ReqTYpeId=1 and    C.PrgVerId=(SELECT A.PrgVerId FROM arStuEnrollments A ")
                .Append("                       WHERE A.StuEnrollId = ? ) ")
                .Append("           AND EXISTS (SELECT * FROM arCourseReqs A,arReqs B ")
                .Append("                       WHERE  A.CourseReqTypId = 1 AND A.PreCoReqId = B.ReqId AND A.ReqId = C.ReqId) ")
                .Append("           AND NOT EXISTS (SELECT * FROM arResults A, arClassSections B ")
                .Append("                           WHERE A.StuEnrollId = ? ")
                .Append("                           AND A.TestId = B.ClsSectionId AND B.ReqId = C.ReqId) ")
                .Append("           AND NOT EXISTS (SELECT * FROM arOverridenPreReqs A ")
                .Append("                           WHERE A.StuEnrollId = ? AND A.ReqId = C.ReqId) ")

                .Append(" union ")
                .Append(" select ResultTable.Reqid,arReqs.Descrip, arReqs.Code from ")
                .Append(" (SELECT           Q.Reqid  as Reqid ")
                .Append(" FROM       arProgVerDef C,arReqs R,arReqGrpDef P ,arCourseReqs Q  where ")
                .Append("   P.Grpid= C.ReqId and P.ReqId=Q.ReqId and     C.ReqId=R.Reqid and   R.ReqTYpeId=2 and C.PrgVerId=(SELECT A.PrgVerId FROM  ")
                .Append(" arStuEnrollments A                        WHERE A.StuEnrollId = ? )   ")
                .Append(" AND Q.Reqid  IN (SELECT ISNULL(A.ReqId,C.ReqId) ")
                .Append(" FROM arCourseReqs A, ")
                .Append(" (SELECT t1.ReqId,ReqSeq,IsRequired,t2.Descrip,t2.Code,t2.ReqTypeId,t2.Hours,t2.Credits,t1.ModUser,t1.ModDate ")
                .Append(" FROM arReqGrpDef t1, arReqs t2 ")
                .Append(" WHERE t1.ReqId = t2.ReqId and t1.GrpId in ( ")
                .Append(" select distinct t0.ReqId from arReqs t0,arProgVerDef t1,arReqGrpDef t2 where PrgVerId= ")
                .Append(" (select PrgVerId from arStuEnrollments where StuEnrollId = ? ))) ")
                .Append(" B                        WHERE  A.CourseReqTypId = 1 AND A.PreCoReqId = B.ReqId ")
                .Append(" or  A.ReqId  =C.ReqId) ")
                .Append(" AND NOT EXISTS (SELECT * FROM arResults A, arClassSections B               ")
                .Append(" WHERE A.StuEnrollId = ? AND ")
                .Append(" A.TestId = B.ClsSectionId AND B.ReqId = C.ReqId)        ")
                .Append(" AND NOT EXISTS (SELECT * FROM arOverridenPreReqs A                    ")
                .Append(" WHERE A.StuEnrollId = ? AND A.ReqId = C.ReqId) ) ResultTable ,arReqs where ")
                .Append(" ResultTable.Reqid = arReqs.Reqid ")

                .Append("ORDER BY   Descrip")

            End With

            ' Add StuEnrollId to the parameter list
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "AvailCourses")

            '   Set Primary Key
            Dim pk(1) As DataColumn
            pk(0) = ds.Tables("AvailCourses").Columns("ReqId")
            ds.Tables("AvailCourses").PrimaryKey = pk

            db.ClearParameters()
            sb.Remove(0, sb.Length)

            '   build the sql query
            With sb
                .Append("SELECT ")
                .Append("           A.ReqId,(SELECT Descrip FROM arReqs R WHERE R.ReqId=A.ReqId) AS Descrip, ")
                .Append("           (SELECT Code FROM arReqs R WHERE R.ReqId=A.ReqId) AS Code ")
                .Append("FROM       arOverridenPreReqs A ")
                .Append("WHERE      A.StuEnrollId = ? ")
                .Append("ORDER BY   Descrip")
            End With

            ' Add StuEnrollId to the parameter list
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            'db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "SelCourses")

            '   Set Primary Key
            pk(0) = ds.Tables("SelCourses").Columns("ReqId")
            ds.Tables("SelCourses").PrimaryKey = pk

            db.ClearParameters()
            sb.Remove(0, sb.Length)

            '   build the sql query
            With sb
                .Append("SELECT ")
                .Append("           A.TestId,B.ClsSection,B.ReqId,C.Descrip ")
                .Append("FROM       arResults A, arClassSections B, arReqs C ")
                .Append("WHERE      A.StuEnrollId = ? AND A.TestId = B.ClsSectionId ")
                '.Append("           AND B.ReqId=C.ReqId AND GrdSysDetailId IS NOT NULL ")
                .Append("           AND B.ReqId=C.ReqId  ")
                .Append("ORDER BY   Descrip")
            End With

            ' Add StuEnrollId to the parameter list
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

            '   Execute the query
            'db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "CoursesAlreadyTaken")


            'Close Connection
            db.CloseConnection()

        Catch ex As System.Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return dataset
        Return ds
    End Function

    Public Function AddPreReqOverride(ByVal StuEnrollId As String, ByVal ReqId As String, ByVal user As String) As String
        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("INSERT INTO arOverridenPreReqs (OverridenPreReqId, StuEnrollId, ReqId, ModUser, ModDate) ")
                .Append("VALUES(?,?,?,?,?)")
            End With

            db.AddParameter("@OverridenPreReqId", System.Guid.NewGuid.ToString, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ReqId", ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModUser
            db.AddParameter("@ModUser", user, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            'ModDate
            db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)

            '   execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return with no errors
            Return ""

        Catch ex As OleDbException
            '   report an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function

    Public Function DeletePreReqOverride(ByVal StuEnrollId As String, ByVal ReqId As String) As String
        '   Connect to the database
        Dim db As New DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Try
            Dim sb As New System.Text.StringBuilder

            '   build the sql query
            With sb
                .Append("DELETE FROM arOverridenPreReqs ")
                .Append("WHERE StuEnrollId=? AND ReqId=? ")
            End With

            db.AddParameter("@StuEnrollId", StuEnrollId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ReqId", ReqId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

            '   execute query
            db.RunParamSQLExecuteNoneQuery(sb.ToString)

            '   return with no errors
            Return ""

        Catch ex As OleDbException
            '   report an error to the client
            Return DALExceptions.BuildErrorMessage(ex)

        Finally
            'Close Connection
            db.CloseConnection()
        End Try
    End Function


End Class
