Imports System.Data.OleDb
Imports System.Data
Imports System.Web
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.Advantage.Common

Public Class AttchGbwToClsSectsDB

    Public Function GetAllTerms(Optional ByVal campusid As String = "") As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter

        Try
            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = GetConnectionString()
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.TermId,t1.TermDescrip ")
                .Append("FROM arTerm t1, syStatuses t2  ")
                .Append("WHERE t2.Status = 'Active' and t1.StatusId = t2.StatusId ")

                If campusid <> "" Then
                    .Append("AND (t1.CampGrpId IN(SELECT CampGrpId ")
                    .Append("FROM syCmpGrpCmps ")
                    .Append("WHERE CampusId = ? ")
                    .Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                    .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                End If


                .Append("ORDER BY t1.StartDate,t1.termDescrip ")
            End With

            If campusid <> "" Then
                db.AddParameter("cmpid", campusid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            End If


            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "Terms")
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function


    Public Function GetAllStatuses() As DataSet
        Dim ds As DataSet

        Try
            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = GetConnectionString()
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT t1.StatusId,t1.Status ")
                .Append("FROM syStatuses t1 ")
            End With

            '   Execute the query
            ds = db.RunSQLDataSet(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds

    End Function
    Public Function GetGradeBkWeights(ByVal status As String, ByVal instructor As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim strUserName As String = (New UserSecurityDB).GetUserInfo(instructor).UserName.ToString.ToLower
        Dim isAdvantageSuperUser As Boolean = (New UserSecurityDB).GetUserInfo(instructor).IsAdvantageSuperUser

                

        Try
            '   connect to the database
            Dim db As New DataAccess
            db.ConnectionString = GetConnectionString()
            Dim sb As New StringBuilder

            '   build the sql query
            With sb
                .Append("SELECT Distinct a.InstrGrdBkWgtId, a.Descrip ")
                .Append("FROM arGrdBkWeights a ")
                .Append("WHERE a.StatusId = ?  ")
                'this will bring only the Grade Book Weights set at Instructor Level
                .Append(" and InstructorId is not null ")
                If Not isAdvantageSuperUser Or Not strUserName = "sa" Then
                    .Append("and a.InstructorId = ? ")
                End If
            End With

            ' Add the PrgVerId and ChildId to the parameter list
            db.AddParameter("@Status", status, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not isAdvantageSuperUser Or Not strUserName = "sa" Then
                db.AddParameter("@Instr", instructor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If

            '   Execute the query
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "InstrGrdBkWgts")

            'Close Connection
            db.CloseConnection()

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function

    Public Function GetAvailSelectedClsSects(ByVal instructor As String, ByVal term As String, ByVal grdBkWeight As String, ByVal campId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim isAdvantageSuperUser As Boolean = (New UserSecurityDB).GetUserInfo(instructor).IsAdvantageSuperUser

        If (Guid.TryParse(grdBkWeight, New Guid()) = False) Then
            Throw New ApplicationException("Grade Book has bad format in AttchGbwToClsSectsDB - GetAvailSelectedClsSects")
        End If

        ' connect to the database
        Dim db As New DataAccess
        Dim connStr As String = GetConnectionString()
        db.ConnectionString = connStr
        Dim sb As New StringBuilder

        With sb
            .Append("SELECT DISTINCT c.ClsSectionId, a.ReqId, a.ClsSection, ")
            .Append("'(' + b.code + ') ' + b.Descrip as Descrip ")
            .Append("FROM arClassSections a, arReqs b, arClassSectionTerms c ")
            .Append("WHERE c.TermId = ? and c.ClsSectionId = a.ClsSectionId ")
            If Not isAdvantageSuperUser Then
                .Append("and a.InstructorId = ? ")
            End If
            .Append("and a.InstrGrdBkWgtId is null and a.ReqId = b.ReqId and a.CampusId = ? ")
        End With

        ' Add the PrgVerId and ChildId to the parameter list
        db.AddParameter("@TermId", term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        If Not isAdvantageSuperUser Then
            db.AddParameter("@InstructorId", instructor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
        End If
        db.AddParameter("@CampusId", campId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

        '   Execute the query
        db.OpenConnection()
        Try
            da = db.RunParamSQLDataAdapter(sb.ToString)
            da.Fill(ds, "AvailClsSects")
            sb.Remove(0, sb.Length)
            db.ClearParameters()

            With sb
                .Append("SELECT DISTINCT a.ClsSectionId, a.ReqId, a.ClsSection, ")
                .Append("'(' + b.code + ') ' + b.Descrip as Descrip ")
                .Append("FROM arClassSections a, arReqs b, arClassSectionTerms c ")
                .Append("WHERE c.TermId = ? and c.ClsSectionId = a.ClsSectionId and ")
                If Not isAdvantageSuperUser Then
                    .Append(" a.InstructorId = ? and ")
                End If
                .Append(" a.InstrGrdBkWgtId = ? and ")
                .Append(" a.ReqId = b.ReqId and a.CampusId = ? ")
            End With

            db.AddParameter("@TermId", term, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            If Not isAdvantageSuperUser Then
                db.AddParameter("@instr", instructor, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            End If
            db.AddParameter("@grdbkwgt", grdBkWeight, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@CampusId2", campId, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            Dim da2 As OleDbDataAdapter = db.RunParamSQLDataAdapter(sb.ToString)
            da2.Fill(ds, "SelectedClsSects")

        Catch ex As Exception
            Throw New BaseException(ex.Message)
        Finally
            db.CloseConnection()
        End Try

        'Return the datatable in the dataset
        Return ds
    End Function


    Public Sub UpdateClsSection(ByVal sGrdBkWgtId As String, ByVal clsSectId As String)
        Dim db As New DataAccess
        Dim sb As New StringBuilder

        db.ConnectionString = GetConnectionString()
        With sb
            .Append("UPDATE arClassSections ")
            .Append("SET InstrGrdBkWgtId = ? ")
            .Append("WHERE ClsSectionId = ? ")
        End With

        If (sGrdBkWgtId = "") Then
            db.AddParameter("@sgrdbkwgtidid", DBNull.Value, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        Else
            db.AddParameter("@sgrdbkwgtidid", sGrdBkWgtId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        End If
        db.AddParameter("@clssectid", clsSectId, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        db.ClearParameters()
    End Sub

    ''' <summary>
    ''' Get the connection string
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetConnectionString() As String
        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        Dim conn As String = myAdvAppSettings.AppSettings("ConString")
        Return conn
    End Function
End Class
